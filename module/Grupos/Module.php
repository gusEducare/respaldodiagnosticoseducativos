<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Grupos;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;
use Grupos\Model\Grupo;
use Grupos\Model\Debug;
use Grupos\Model\CargamasivaModel;
use Grupos\Model\CargamasivaExamenModel;
use Grupos\Model\Catalogo;

use Zend\Session\Container;

class Module
{
    //Inicia bloque para control de acceso GRUPOS
    public function onBootstrap(MvcEvent $e) {        
        $sharedEvents = $e->getApplication()->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'verAcl'));
    }
    
    private  function toCamelCase($str, array $noStrip = array())
    {
            $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
            $str = trim($str);
            
            $str = ucwords($str);
            $str = str_replace(" ", "", $str);
            $str = lcfirst($str);

            return $str;
    }
    
    public function verAcl(MvcEvent $e) {
        $config = include __DIR__ . '/../../config/autoload/global.php';
        $this->datos_sesion = new Container('user');
        $_intUserRole = ($this->datos_sesion->perfil) ? $this->datos_sesion->perfil : $config['constantes']['ID_PERFIL_INVITATO'];
        $_strRoute = $e -> getRouteMatch() -> getMatchedRouteName();
        $_strAccion = $e -> getRouteMatch() ->getParam('action');
        $_strAccion = $this->toCamelCase($_strAccion);
        
        if (!$e -> getViewModel() -> acl -> isAllowed($_intUserRole, $_strRoute,$_strAccion)) {
            $response = $e -> getResponse();
            $response -> getHeaders() -> addHeaderLine('Location', $e -> getRequest() -> getBaseUrl() . '/404');
            $response -> setStatusCode($config ['constantes']['ERROR_PERMISO_DENEGADO']);
            
            //error_log($_intUserRole.'<-NO PERMITIDO '.$_strRoute.'/'.$_strAccion);

        }else{
             //error_log($_intUserRole.'<-PERMITIDO '.$_strRoute.'/'.$_strAccion);
        }
    }
    //Termina bloque para control de acceso
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    
    public function getServiceConfig(){
    	
    	return Array('factories'=>
    			array(
    					'Debug'=>function($sm){
    						$model 	   = new Debug($sm->get('config'));
    						return $model;
    					},
    					'Grupos\Model\Grupo'=>function($sm){
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$model 	   = new Grupo($dbAdapter, $sm->get('config'));
    						return $model;
    					},
    					'Grupos\Model\CargamasivaModel' => function($sm){
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$table = new CargamasivaModel($dbAdapter, $sm->get('config'));
    						return $table;
    					
    					},
                                        ' Grupos\Model\CargamasivaExamenModel' => function($sm){
                                                $config = $sm->get('config');
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$table = new CargamasivaExamenModel($dbAdapter, $config);
    						return $table;
    					
    					},
                                        ' Grupos\Model\Catalogo' => function($sm){
                                                $config = $sm->get('config');
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$table = new Catalogo($dbAdapter, $config);
    						return $table;
    					
    					}
                                       
    					
    			)
    	);
    }
    
}
