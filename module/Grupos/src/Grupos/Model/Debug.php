<?php
namespace Grupos\Model;


use Zend\Log\Writer\Stream;
use Zend\Log\Logger;

class Debug
{

	/**
	 *
	 * @var Logger $_objLogger .
	 */
	protected $_objLogger;

	protected $config;

	public function __construct($config){
		$this->config = $config;
		if($this->config['AMBIENTE'] == "DESARROLLO"){
			$this->_objLogger = new Logger();
			$this->_objLogger->addWriter(new Stream('php://stderr'));
		}
	}
	
	public function debug($message){
		
		if($this->config['AMBIENTE'] == "DESARROLLO"){			
			$this->_objLogger->debug($message);
		}
		
	}
	
}


