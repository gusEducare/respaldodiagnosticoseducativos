<?php
/**
 * Modelo para logica del cliente para ws PortalGE.
 * @author oreyes
 * @copyright Grupo Educare SA de CV derechos reservados.
 */

namespace Grupos\Model;

use Zend\Soap\Client;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;

/**
 * Clase ClientWSPortalGE.
 * @author oreyes
 *
 */
class ClientWSPortalGE extends Client{
	
	/**
	 * Objeto Logger para rastreo de errores.
	 * @var Logger $_objLogger 
	 */
	public $_objLogger;
	
	/**
	 * Variable para la direccion del ws.
	 * @var string $wsdl
	 */
	public $wsdl;
	
	/**
	 * Objeto para instancia de clase.
	 * @var $_instancia;
	 */
	private static $_instancia;
	
	/**
	 * Constructor de la clase.
	 * @param WSDL $wsdl : variable para cargar el wsdl y consumir el WS.
	 * @param Array $options
	 */
	public function __construct($wsdl, $options = array()) {
		$this->wsdl = $wsdl;
		$this->options = $options;
		//parent::__construct($wsdl, $options);		
	}
	
	
	public function setWsdl($wsdl)
	{
		$this->wsdl = $wsdl;
	}
	/**
	 * Funcion que hace un llamado a la operacion ActivarLicencia del WS se introduce el id de servicio el id de usuario
	 * la llave el correo y el nombre del usuario que estaran ligados al examen.
	 * 
	 * @param int $_intIdServicio : id del servicio por default 6 para portal GE.
	 * @param int $_intUsuarioServicio : id del usuario en el servicio.
	 * @param String $_strLlave : numero de llave.
	 * @param String $_strCorreo : nombre de usuario.
	 * @param String $_strNombre : nombre real del alumno.
	 * @return string
	 */
	public function activaLicenciaUsuario($_intIdServicio, $_intIdUsuarioPerfil, $_strLlave, $_strNombreUsuario, $_strNombre) 
	{
		$client = new \SoapClient($this->wsdl,$this->options);
		$_objResponce= $client->activarLicencia($_intIdServicio, $_intIdUsuarioPerfil, $_strLlave, $_strNombreUsuario , $_strNombre );
		
		if($_objResponce == "false"){
				$_arrResp[0] = "false";
				$_arrResp[1] = 'Ocurri&oacute; un error al procesar la solicitud.';
		}else{
				$_arrResp[0] = $_objResponce;
				$_arrResp[1] = 'Registro correcto ahora estas ligado a tu examen';
		}
		return $_arrResp;
	}
	
	/**
	 * Funcion que hace un llamado a la operacion llaveValidaMensaje 
	 * del WS para saber si la llave esta disponible.
	 * 
	 * @param String $_strLicencia
	 * @param Int $_intIdServicio
	 * @return array : Respuesta con booleano y respuesta acerca del estado de la llave.
	 */
	public function llaveValidaMensaje($_strLicencia, $_intIdServicio)
	{
		$client = new \SoapClient($this->wsdl,$this->options);
		$_objResponce= $client->llaveValidaMensaje($_strLicencia, $_intIdServicio);
		
		return $_objResponce;
	}
	
	/**
	 * funcion que hace una instacia del log para posteriormente usarlo para rastro de errores.
	 * @return Logger $_objLogger.
	 */
	public function getLog(){
		$this->_objLogger = new Logger();
		$this->_objLogger->addWriter(new Stream('php://stderr'));
		return $this->_objLogger;
	}

}
