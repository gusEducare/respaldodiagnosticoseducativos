<?php
namespace Grupos\Model;



use Grupos\Model\Debug;
use Grupos\Model\Codigo;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;

class Catalogo
{
	
	/**
	 * 
	 * @var Adapter $adapter.
	 */
	protected $adapter;
	/**
	 * 
	 * @var Logger $_objLogger .
	 */
	protected $_objLogger;
	
	protected $config;
	
	public function __construct(Adapter $adapter, $config){
		$this->adapter = $adapter;
		$this->config = $config;
		$this->_objLogger = new Debug($config);
                $this->sql 	   = new Sql($this->adapter);
	}
	
	public function getMaterias($bloque,$grado){
            $bloq = '%'.$bloque.'%';
            $_strQueryMaterias = "SELECT * FROM t12examen WHERE t12nombre LIKE :bloque AND t36id_todo_grado_escolaridad=:grado;";
            
            $statement = $this->adapter->query($_strQueryMaterias);
		$resultsMaterias = $statement->execute(array(
				":bloque" => $bloq,
				":grado" => $grado,
		));
		if($resultsMaterias->count() < 1){
			return false;
		}
		$resultSet = new ResultSet;
		$resultSet->initialize($resultsMaterias);
		$_arrDataMaterias = $resultSet->toArray();
		return $_arrDataMaterias;
        }
	
}