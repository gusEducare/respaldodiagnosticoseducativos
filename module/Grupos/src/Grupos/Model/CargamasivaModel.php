<?php
namespace Grupos\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

use Grupos\Model\ClientWSPortalGE;

class CargamasivaModel{
    
    
    
    /**
        *  Instancia Zend\Db\Sql\Sql
        * @var Sql $sql 
        */
       public $sql;

    /**
        * Adapter Zend\Db\Adapter\Adapter
        * @var Adapter $adapter
        */
       public $adapter;
    
    /**
        * Array de configuracion 
        * @var array  $config
        */
       public $config;
       
       
	/**
	 * 
	 * @param Adapter $adapter
	 */
	public function __construct(Adapter $adapter, $config){
            $this->adapter = $adapter;
            $this->sql= new Sql($adapter);
            $this->config = $config;
	
	}
	
	/**
	 * Esta funcion verifica que el usuario existe.
	 *
	 * @param  String _strCorreo
	 * @return array $_arrRespuesta regresamos array de datos 
	 */
	function existeUsuario( $_strUsuario, $validarMail = false )
	{
		$_arrRespuesta = array();
			
		$select = $this->sql->select();
		$select->from(array('t01'=>'t01usuario'))
		 	 	->where('t01.t01correo ='. "'".$_strUsuario."'");
		
		//$select->columns(array());

		$statement = $this->sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		$_arrRespuesta = $resultSet->toArray();

		return $_arrRespuesta;
	}
	
	
	
        
        
        public function registrarUsuario($_arrUserData, $_idGrupo){
            
            //verificar licencia
            $_intIdServicio = $this->config["constantes"]["ID_SERVICIO_EVALUACIONES"];
            $_objWs =  new ClientWSPortalGE($this->config["constantes"]["WSDL_PORTALGE"]);
            $_arrLlave = $_objWs->llaveValidaMensaje($_arrUserData[4],$_intIdServicio);
            if($_arrLlave['valida']){
                try{
                    $this->adapter->getDriver()->getConnection()->beginTransaction();
                    //insert user 
                    $insertUser= $this->sql->insert();
                    $insertUser->into('t01usuario');
                    $insertUser->columns(array('t01nombre', 't01apellidos', 't01correo', 't01contrasena', 't01estatus'));
                    $insertUser->values(array(
                                        't01nombre' => trim($_arrUserData[0]),
                                        't01apellidos'=> trim($_arrUserData[1]),
                                        't01correo'=> trim($_arrUserData[2]),
                                        't01contrasena'=> sha1(trim($_arrUserData[3])),
                                        't01estatus' => $this->config['constantes']['ESTATUS_ACTIVO']
                            ));
                    $statement 	= $this->sql->prepareStatementForSqlObject($insertUser);
                    $resultSetInterface = $statement->execute();
                    $_idUsuario = $resultSetInterface->getGeneratedValue();
                    if(!isset($_idUsuario)){
                        $this->adapter->getDriver()->getConnection()->rollback();
                        return false;
                    }
                    //insert perfil 
                    $idPerfil = $this->config['constantes']['ID_PERFIL_ALUMNO'];
                    $insertPerfil = $this->sql->insert();
                    $insertPerfil->into('t02usuario_perfil');
                    $insertPerfil->columns(array(
                                        'c01id_perfil',
                                        't01id_usuario'));
                    $insertPerfil->values(array(
                                    'c01id_perfil'	=> $idPerfil,
                                    't01id_usuario'	=> $_idUsuario));

                    $statementPerfil 	= $this->sql->prepareStatementForSqlObject($insertPerfil);
                    $resultSetInterfacePerfil = $statementPerfil->execute();
                    $idUsuarioPerfil = $resultSetInterfacePerfil->getGeneratedValue();

                    if(!isset($idUsuarioPerfil)){
                        $this->adapter->getDriver()->getConnection()->rollback();
                        return false;
                    }

                    //agregar al grupo 

                    $insertGrupo= $this->sql->insert();
                    $insertGrupo->into('t08administrador_grupo');
                    $insertGrupo->columns(array(
                                    't02id_usuario_perfil',
                                    't05id_grupo',
                                    't08usuario_ligado',
                                    't08fecha_registro',
                                    't08fecha_actualiza'));
                    $insertGrupo->values(array(  
                                    't02id_usuario_perfil'  => $idUsuarioPerfil,
                                    't05id_grupo'           => $_idGrupo,
                                    't08usuario_ligado'     => $this->config['constantes']['ESTATUS_LIGADO'],
                                    't08fecha_registro'     => date("Y-m-d H:i:s"),
                                    't08fecha_actualiza'    => date("Y-m-d H:i:s")
                            ));

                    $statementGrupo = $this->sql->prepareStatementForSqlObject($insertGrupo);
                    $resultSetInterfaceGrupo = $statementGrupo->execute();
                    $idAlumnoGrupo = $resultSetInterfaceGrupo->getGeneratedValue();
                    if(!isset($idAlumnoGrupo)){
                        $this->adapter->getDriver()->getConnection()->rollback();
                        return false;
                    }




                    //activar la licencia para obtener el id del examen 
                    $_strNombre = $_arrUserData[0].' '. $_arrUserData[1];
                    $_strNombreUsuario = $_arrUserData[2];
                    $_arrResp = $_objWs->activaLicenciaUsuario($_intIdServicio, $idUsuarioPerfil, trim($_arrUserData[4]), $_strNombreUsuario, $_strNombre);
                    if($_arrResp[0] == false){
                        $this->adapter->getDriver()->getConnection()->rollback();
                        return false;
                    }

                    $_arrVigencia = explode('_', $_arrResp[0]);
                    $_intDias = $_arrVigencia[0];
                    $_intIdExamenPaquete = $_arrVigencia[1];


                    //agregar llave  al usuario 
                    $insertLlave = $this->sql->insert();
                    $insertLlave->into('t03licencia_usuario');
                    $insertLlave->columns(array(
                                    't21id_paquete',
                                    't02id_usuario_perfil',
                                    't03licencia',
                                    't03fecha_activacion',
                                    't03dias_duracion',
                                    't03estatus'));
                    $insertLlave->values(array(                    
                                    't21id_paquete'         => 1, // Hardcodeo el id del paquete para que siempre se inserte uno generico, hasta que decidamos utilizarlo
                                    't02id_usuario_perfil'  => $idUsuarioPerfil,
                                    't03licencia'           => trim($_arrUserData[4]),
                                    't03fecha_activacion'   => date("Y-m-d H:i:s"),
                                    't03dias_duracion'      => $_intDias,
                                    't03estatus'            => $this->config['constantes']['ESTATUS_DISPONIBLE']
                            ));			
                    
                    $statementLlave = $this->sql->prepareStatementForSqlObject($insertLlave);
                    $resultSetInterfaceLlave = $statementLlave->execute();
                    $id_LicenciaUsuario = $resultSetInterfaceLlave->getGeneratedValue();
                    
                    if(!isset($id_LicenciaUsuario)){
                        $this->adapter->getDriver()->getConnection()->rollback();
                        return false;
                    }
                    
                    $insertExamen= $this->sql->insert();
                    $insertExamen->into('t04examen_usuario');
                    $insertExamen->columns(array(
                                        't12id_examen',
                                        't03id_licencia_usuario',
                                        't04estatus'));
                    $insertExamen->values(array(        
                                        //'t12id_examen'          => $data['t12id_examen'], 
                                        't12id_examen'          => $_intIdExamenPaquete, 
                                        't03id_licencia_usuario'=> $id_LicenciaUsuario,
                                        't04estatus'            => 'NOPRESENTADO'
                                ));
                    $statementExamen = $this->sql->prepareStatementForSqlObject($insertExamen);
                    $resultSetInterfaceExamen = $statementExamen->execute();
                    $idExamenUsuario = $resultSetInterfaceExamen->getGeneratedValue();
                    
                    if(!isset($idExamenUsuario)){
                        $this->adapter->getDriver()->getConnection()->rollback();
                        return false;
                    }
                    
                    $this->adapter->getDriver()->getConnection()->commit();
                    return true ;

                }
                catch (Exception $e) {
                    $this->adapter->getDriver()->getConnection()->rollback();
                    return false;
                }

            }
            else{
                 return false;
            }
            
            
                
                
            
        }
        
        
        public function getUserData($idUsuarioServicio){
            $_strQuery = " SELECT t01.* 
                            FROM 
                            t01usuario t01
                            INNER JOIN t02usuario_perfil t02 ON (t01.t01id_usuario = t02.t01id_usuario)
                            WHERE t02.t02id_usuario_perfil = :idUsuarioPerfil ";
            $statement = $this->adapter->query($_strQuery);
            $results = $statement->execute(array(
                ":idUsuarioPerfil" => $idUsuarioServicio,
            ));
            if($results->count() < 1){
                    return false;
            }
            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $_arrData = $resultSet->toArray();
            return $_arrData;
            
        }
	
	
}