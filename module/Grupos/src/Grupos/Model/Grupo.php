<?php
namespace Grupos\Model;



use Grupos\Model\Debug;
use Grupos\Model\Codigo;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;

class Grupo
{
	
	/**
	 * 
	 * @var Adapter $adapter.
	 */
	protected $adapter;
	/**
	 * 
	 * @var Logger $_objLogger .
	 */
	protected $_objLogger;
	
	protected $config;
	
	public function __construct(Adapter $adapter, $config){
		$this->adapter = $adapter;
		$this->config = $config;
		$this->_objLogger = new Debug($config);
                $this->sql 	   = new Sql($this->adapter);
	}
	
	/**
	 * Obtenerlos grupos del Profesor
	 * @access public
	 * @author LAFC lfcelaya@grupoeducare.com
	 * @param  Interger $_intIdUsuarioPerfilProfesor id del profesors
	 * @return Array $_arrDataGrupos array asociativo con los datos de los grupos
	 */
	public function getGrupos($_intIdUsuarioPerfilProfesor, $_consEstatus =  null){
		
		if($_consEstatus == null){
			$_consEstatus =  $this->config['constantes']['ESTATUS_ACTIVO'];
			$_strOrder = " ORDER BY t08fecha_registro DESC";
		}
		else{
			$_strOrder = " ORDER BY t08fecha_actualiza DESC";
		}
		
		$_strQueryGrupos = " SELECT *
								FROM t08administrador_grupo
								INNER JOIN t05grupo ON (t08administrador_grupo.t05id_grupo = t05grupo.t05id_grupo)
								LEFT JOIN t07codigo ON (t07codigo.t05id_grupo = t05grupo.t05id_grupo)
								WHERE 
									t02id_usuario_perfil  = :idUsuarioPerfil
									AND t05estatus = :consEstatus ".$_strOrder;
		
		
		
		$statement = $this->adapter->query($_strQueryGrupos);
		$resultsGrupos = $statement->execute(array(
				":idUsuarioPerfil" => $_intIdUsuarioPerfilProfesor,
				":consEstatus" => $_consEstatus,
		));
		if($resultsGrupos->count() < 1){
			return false;
		}
		$resultSet = new ResultSet;
		$resultSet->initialize($resultsGrupos);
		$_arrDataGrupos = $resultSet->toArray();
		return $_arrDataGrupos;
		
	}
        
        public function getDataGrupo($_idGrupo, $_intIdUsuarioPerfilProfesor){
            $_strQueryGrupos = " SELECT t05grupo.*
								FROM t08administrador_grupo
								INNER JOIN t05grupo ON (t08administrador_grupo.t05id_grupo = t05grupo.t05id_grupo)
								LEFT JOIN t07codigo ON (t07codigo.t05id_grupo = t05grupo.t05id_grupo)
								WHERE 
									t02id_usuario_perfil  = :idUsuarioPerfil
									AND t05grupo.t05id_grupo = :idGrupo ";
            
            $statement = $this->adapter->query($_strQueryGrupos);
		$resultsGrupos = $statement->execute(array(
				":idUsuarioPerfil" => $_intIdUsuarioPerfilProfesor,
				":idGrupo" => $_idGrupo,
		));
		if($resultsGrupos->count() < 1){
			return false;
		}
		$resultSet = new ResultSet;
		$resultSet->initialize($resultsGrupos);
		$_arrDataGrupo = $resultSet->toArray();
		return $_arrDataGrupo;
        }
        
        
        public function updateGrupo($_intIdUsuarioPerfilProfesor, $_idGrupo, $_strCiclo, $_strDescripcion){
            
            //verificar que el grupo pertenece al profesor 
            $_arrDataGrupo = $this->getDataGrupo($_idGrupo, $_intIdUsuarioPerfilProfesor);
            
            if(count($_arrDataGrupo)> 0){
                $_strQueryUpdate = 'UPDATE t05grupo '
                        . 'SET t05descripcion = :_strDescripcion , t05ciclo = :_strCiclo '
                        . 'WHERE  t05id_grupo= :idGrupo';
                $statement = $this->adapter->query($_strQueryUpdate);
                try{
                    $results = $statement->execute(array(
                                    ":_strDescripcion" => $_strDescripcion,
                                    ":_strCiclo" => $_strCiclo,
                                    ":idGrupo" => $_idGrupo
                    ));
                }
                catch (Exception $e){
                        return false;
                }

                if($results->getAffectedRows() > 0){
                        return true;			
                }
                else{
                        return false;
                }
            }
            else{
                return false;
            }
		
        }

                /**
	 * Obtener los usuarios que pertenecen al grupo
	 * @access public
	 * @author LAFC lfcelaya@grupoeducare.com
	 * @param  int $_intIdGrupo id del grupo
	 * @param  int $_intIdUsuarioPerfilProfesor id del profesor, este parametro se utiliza para verificar que el grupo pertenezca a quien a lo solicita
	 * @return mixed $_mixDataUsuarios array data usuarios | false cuando no hay usuarios con perfil de alumno vinculados  o se esta intentando obtener los usuarios de un grupo que no le pertenece a quien solicita
	 */
	public function getUsuariosGrupo($_intIdGrupo, $_intIdUsuarioPerfilProfesor){
		
		//obtener a todos los usuarios vinculados a este grupo, incluyendo al profesor/admistrador del grupo
		$_strQuery = "SELECT *
						FROM t08administrador_grupo t08
							INNER JOIN t02usuario_perfil t02 ON (t02.t02id_usuario_perfil = t08.t02id_usuario_perfil)
							INNER JOIN t01usuario t01 ON (t01.t01id_usuario = t02.t01id_usuario)
						WHERE
							t08.t05id_grupo = :idGrupo
							AND  t08usuario_ligado =  ".$this->config['constantes']['ESTATUS_LIGADO'] .
                                                " ORDER BY c01id_perfil DESC, t01apellidos ";
		
		$statement = $this->adapter->query($_strQuery);
		$results = $statement->execute(array(
			":idGrupo" => $_intIdGrupo
		));
		
		if($results->count() < 1){
			return false;
		}
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
		$_arrDataUsuarios = $resultSet->toArray();
		
		
		//verificar que el grupo pertenece al usuario que lo solicita
		$_boolPertenece = false;
		$_arrReturn = array();
		foreach ($_arrDataUsuarios as $_rowUser){
			//agregar al array final solo a los usuarios con perfil alumno
			if($_rowUser['c01id_perfil'] == $this->config['constantes']['ID_PERFIL_ALUMNO']){
				array_push($_arrReturn, $_rowUser);
			}
			else if($_rowUser['c01id_perfil'] == $this->config['constantes']['ID_PERFIL_PROFESOR']){
				if($_rowUser['t02id_usuario_perfil'] == $_intIdUsuarioPerfilProfesor){
					$_boolPertenece = true;
				}
			}
		}
		
		if($_boolPertenece != true){
			return false;
		}
		
		return $_arrReturn;
		
	}
	
	
	public function getUserData($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo, $boolReportesProfesor = null){
		
		$_arrDataUsuariosGrupo = $this->getUsuariosGrupo($_intIdGrupo, $_intIdUsuarioPerfilProfesor );
		if($_arrDataUsuariosGrupo === false){
			return false;
		}
		//extraer data del usuario solicitado
		$_arrayDataResponse = array();
		foreach($_arrDataUsuariosGrupo as $user){
                        $_strIndice = 't01id_usuario';
                        if(isset($boolReportesProfesor) && $boolReportesProfesor == true){
                            $_strIndice = 't02id_usuario_perfil';
                        }
			if($user[$_strIndice] == $_intIdAlumno){
				$_arrayDataResponse = $user;
			}
		}
		return $_arrayDataResponse;
	} 
	
	public function updateUserField($_intIdAlumno,$_strFieldName, $_strValue){
		
		$_strQueryUpdate = 'UPDATE t01usuario SET '.$_strFieldName.' = :_strValue  WHERE t01id_usuario = :_intIdUsuario ';
		$statement = $this->adapter->query($_strQueryUpdate);
		try{
			$results = $statement->execute(array(
					":_strValue" => $_strValue,
					":_intIdUsuario" => $_intIdAlumno
			));			
		}
		catch (Exception $e){
			return false;
		}
		
		if($results->getAffectedRows() > 0){
			return true;			
		}
		else{
			return false;
		}
	}
	
	
	public function updatePassword($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo, $_strNewPass) {
        //verificar que el usuario pertenece al grupo y el grupo pertenece al profesor
        $_arrDataAlumno = $this->getUserData($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo);
        if ($_arrDataAlumno === false || count($_arrDataAlumno) < 1) {
            return false;
        }

        $_strQueryUpdate = 'UPDATE t01usuario SET t01contrasena=:_strNewPass WHERE t01id_usuario = :_intIdAlumno ';
        $statement = $this->adapter->query($_strQueryUpdate);
        try {
            $results = $statement->execute(array(
                ":_strNewPass" => sha1($_strNewPass),
                ":_intIdAlumno" => $_arrDataAlumno['t01id_usuario']
            ));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    public function deleteUserGroup($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo){
		
		//obtener el usuario perfil 
		$_arrDataAlumno = $this->getUserData($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo );
		if($_arrDataAlumno === false || count($_arrDataAlumno) < 1 ){
			return false;
		}
		
		$_strQueryUpdateEstatus = 'UPDATE 
									t08administrador_grupo 
								SET 
									t08usuario_ligado = :estatus_desligado  
								WHERE 
									t02id_usuario_perfil = :id_usuario_perfil  
									AND  t05id_grupo = :idGrupo';
		
		$statement = $this->adapter->query($_strQueryUpdateEstatus);
		try{
			$results = $statement->execute(array(
					":estatus_desligado" => $this->config['constantes']['ESTATUS_NO_LIGADO'],
					":id_usuario_perfil" => $_arrDataAlumno['t02id_usuario_perfil'],
					":idGrupo" => $_intIdGrupo
			));
		}
		catch (Exception $e){
			return false;
		}
		
		return true;
		
		
	}
	
	public function changeEstatusGroup($_intIdUsuarioPerfilProfesor, $_intIdGrupo, $_strEstatus){
		
		if($_strEstatus == $this->config['constantes']['ESTATUS_ACTIVO']){
			$_strEstatusConsultar = $this->config['constantes']['ESTATUS_INACTIVO'];
		}
		else{
			$_strEstatusConsultar = $this->config['constantes']['ESTATUS_ACTIVO'];
		}
		$_arrGrupos = $this->getGrupos($_intIdUsuarioPerfilProfesor,$_strEstatusConsultar);
		$this->_objLogger->debug("_arrGrupos --> ".print_r($_arrGrupos,true));
		$_arrTargetGroup = array();
		
		
		foreach($_arrGrupos as $grupo){
			
			
			if($grupo['t05id_grupo'] == $_intIdGrupo){
				$_arrTargetGroup[0] = $grupo;
			}
		}
		
		$this->_objLogger->debug("_arrTargetGroup --> ".print_r($_arrTargetGroup,true));
		if(count($_arrTargetGroup) < 1){
			return false;
		}
		
		$_strQueryUpdateEstatus = 'UPDATE t05grupo SET t05estatus = :ESTATUS WHERE t05id_grupo = :idGrupo ';
		$_strQueryUpdateFechaMod = 'UPDATE t08administrador_grupo 
										SET t08fecha_actualiza = NOW() 
										WHERE t02id_usuario_perfil = :_intIdUsuarioPerfilProfesor 
										AND  t05id_grupo =  :idGrupo ';
		
		$statement = $this->adapter->query($_strQueryUpdateEstatus);
		try{
			$results = $statement->execute(array(
					":ESTATUS" => $_strEstatus,
					":idGrupo" => $_intIdGrupo
			));
			$statement = $this->adapter->query($_strQueryUpdateFechaMod);
			$results = $statement->execute(array(
					':_intIdUsuarioPerfilProfesor' => $_intIdUsuarioPerfilProfesor,
					':idGrupo' => $_intIdGrupo
			));
			
		}
		catch (Exception $e){
			return false;
		}
		
		return true;
		
	}
	
	
	public function addGrupo($_intIdUsuarioPerfilProfesor,$_strDescripcion,$_strCiclo, $_idColegio ){
		
		try {
			$connection = $this->adapter->getDriver()->getConnection();
			$connection->beginTransaction();
			//insertar elregistro del grupo
			$_strQueryInsertGrupo = 'INSERT INTO 
										t05grupo (t05descripcion, t05ciclo, t05estatus, t05id_colegio_portalge) 
										VALUES (:_strDescripcion, :_strCiclo, :_consEstatusActivo, :_idColegio)';
			
			
			$statement = $this->adapter->query($_strQueryInsertGrupo);
			$results = $statement->execute(array(
					':_strDescripcion' => $_strDescripcion, 
					':_strCiclo' => $_strCiclo, 
					':_consEstatusActivo' => $this->config['constantes']['ESTATUS_ACTIVO'], 
					':_idColegio' => $_idColegio
				));
			
			$_intIdGrupo = $results->getGeneratedValue();
			
			$this->_objLogger->debug("id grupo generado ---> ".$_intIdGrupo);
			
			if($_intIdGrupo == null){
				$connection->rollback();
				return false;
			}
			
			//insertar el registro del administrador del grupo
			
			$_strQUeryInsertAdmin = 'INSERT INTO 
									t08administrador_grupo(t02id_usuario_perfil,t05id_grupo, t08usuario_ligado, t08fecha_registro, t08fecha_actualiza) 
									VALUES (:_intIdUsuarioPerfilProfesor, :_intIdGrupo, :_consUsuarioLigado, NOW(), NOW()) ';
			
			$statement = $this->adapter->query($_strQUeryInsertAdmin);
			$results = $statement->execute(array(
					'_intIdUsuarioPerfilProfesor' => $_intIdUsuarioPerfilProfesor,
					':_intIdGrupo' => $_intIdGrupo,
					':_consUsuarioLigado' => $this->config['constantes']['ESTATUS_LIGADO']
			));
			$_intIdAdministrador = $results->getGeneratedValue();

			$this->_objLogger->debug("_intIdAdministrador generated ---> ".$_intIdAdministrador);
			
			if($_intIdAdministrador == null){
				$connection->rollback();
				return false;
			}
			
			//instancia dela clase Codigo
			$_modelCodigo = new Codigo($this->adapter, $this->config );
			
			//insertar el codigo de grupo 
			$_boolResultado = $_modelCodigo->addCodigo($_intIdGrupo);
			
			if($_boolResultado === false){
				$connection->rollback();
				return false;
			}
			
			$connection->commit();
			return  true;
			
		}
		catch (\Exception $e){
			$this->_objLogger->debug("fatal error  ---> ".$e);
			$connection->rollback();
			return false;
		}
		
	}
        
        public function getGrupoCodigo($_codigoGrupo) {
                $datos = array();
		$_select = $this->sql->select();
		
		$_select->from(array('t07'  => 't07codigo'))
                    ->where('t07.t07codigo = "'.$_codigoGrupo.'"')
                    ->columns(
                            array(
                                   't07id_codigo',
                                   't05id_grupo',
                                   't07codigo',
                                   't07tipo',
                                   't07ultima_actualizacion',
                                   't07estatus'));

		$_stmt 	 = $this->sql->prepareStatementForSqlObject($_select);
		$_result = $_stmt->execute();
		
		$_rsSet  = new ResultSet();
		$_rsSet->initialize($_result);
		
		$datos = $_rsSet->toArray();
                $datos = $datos[0];
                $datos['t07estatus'] = $datos['t07estatus'] ==  'ACTIVO' ? 1 : 0;
                return $datos;	
        }
        
        public function ligarGrupo($_idGrupo,$_idUsuario) {
            try {
                
                //verificar si el usuario ya pertenece al grupo 
                $_strQueryAlumnoGrupo = 'SELECT * FROM '
                        . 't08administrador_grupo '
                        . 'WHERE t02id_usuario_perfil = :_inteIdUsuario AND t05id_grupo = :_intIdGrupo';
                $statementSelect = $this->adapter->query($_strQueryAlumnoGrupo);
                $resultsSelect = $statementSelect->execute(array(
                                ':_inteIdUsuario' => $_idUsuario, 
                                ':_intIdGrupo' => $_idGrupo
                        ));
                if($resultsSelect->count() > 0){
                    $_strQueryInsertGrupo = ' UPDATE t08administrador_grupo '
                            . 'SET t08usuario_ligado= :_consUsuarioLigado '
                            . 'WHERE t02id_usuario_perfil = :_inteIdUsuario AND t05id_grupo = :_intIdGrupo';
                }
                else{
                    
                    //insertar elregistro del grupo
                    $_strQueryInsertGrupo = 'INSERT INTO t08administrador_grupo (t02id_usuario_perfil,t05id_grupo,t08usuario_ligado,t08fecha_registro,t08fecha_actualiza) 
                                                VALUES (:_inteIdUsuario, :_intIdGrupo, :_consUsuarioLigado, NOW(), NOW())';
                }
                
                $statement = $this->adapter->query($_strQueryInsertGrupo);
                $results = $statement->execute(array(
                                ':_inteIdUsuario' => $_idUsuario, 
                                ':_intIdGrupo' => $_idGrupo, 
                                ':_consUsuarioLigado' => $this->config['constantes']['ESTATUS_LIGADO'], 
                        ));

               $registrosAfectados = $results->getAffectedRows();

                $this->_objLogger->debug("Registros afectados ---> ".$registrosAfectados);

                if($registrosAfectados < 1){

                        return false;
                }

                return  true;
                
            }
            catch (\Exception $e){
                    $this->_objLogger->debug("fatal error  ---> ".$e);
                    return false;
            }
        }
	
	public function actualizaNombreGrupo($_intIdGrupo, $_strNombreGrupo){
            
		$_strQueryUpdate = 'UPDATE t05grupo SET t05descripcion =:_strNombreGrupo  WHERE t05id_grupo=:_intIdGrupo';
		$statement = $this->adapter->query($_strQueryUpdate);
		try{
			$results = $statement->execute(array(
					":_intIdGrupo" => $_intIdGrupo,
					":_strNombreGrupo" => $_strNombreGrupo
			));
		}
		catch (Exception $e){
			return false;
		}
		if($results->getAffectedRows() > 0){
			return true;			
		}
		else{
			return false;
		}	
	}
        
        
        
        public function isAlumno($_idUsuarioPerfilAlumno, $_idUsuarioPerfilProfesor){
            
            $strQuery = '';
            
        }
	
        
        public function getReporteExamen($idGrupo, $idProfesor){
            
            //verificar que el grupo pertenece al profesor 
            
            $_arrGrupos = $this->getGrupos($idProfesor);
            $_boolPerteneceProfesor = false;
            foreach ($_arrGrupos as $grupo){
                if($grupo['t05id_grupo'] ==  $idGrupo){
                    $_boolPerteneceProfesor = true;
                }
            }
            
            if($_boolPerteneceProfesor === false){
                return false;
            }
            
            
            $_strQuery = 'SELECT
                            t02.t02id_usuario_perfil, 
                             #t02.c01id_perfil,
                            t01.t01id_usuario as "ID", 
                            t01.t01nombre as "Nombre", 
                            t01.t01apellidos as "Apellidos",
                            t01.t01correo as "Correo",
                            t01.t01contrasena,
                            #t01.t01estatus, 
                            #t03.t03id_licencia_usuario,
                            t03.t03licencia as "Licencia", 
                            t03.t03fecha_activacion as "Fecha de activacion", 
                            #t03.t03dias_duracion,
                            #t08.t05id_grupo,
                            grupos_profesor.grupo as "Grupo",
                            grupos_profesor.profesor as "Profesor",
                            #lic.t04id_examen_usuario, 
                            lic.t12id_examen, 
                            t12.t12nombre as "Examen", 
                            #lic.t03id_licencia_usuario, 
                            lic.t04calificacion as "Calificacion", 
                            lic.t04num_intento as "No. Intentos", 
                            lic.t04estatus  as "Estatus"

                            FROM t02usuario_perfil t02
                            join t01usuario t01 on t01.t01id_usuario = t02.t01id_usuario
                            join t03licencia_usuario t03 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil 
                            join t08administrador_grupo t08 on t02.t02id_usuario_perfil = t08.t02id_usuario_perfil 
                            join(
                                SELECT 
                                t05.t05id_grupo ,t05.t05descripcion as grupo, t08.t02id_usuario_perfil as profesor
                                FROM t08administrador_grupo t08, t05grupo t05
                                where t08.t05id_grupo = t05.t05id_grupo
                                and t08.t02id_usuario_perfil = :idProfesor  #ID PROFESOR
                                and t05.t05id_grupo = :idGrupo # ID GRUPO
                                ) grupos_profesor on t08.t05id_grupo = grupos_profesor.t05id_grupo 
                            left join (select * from t04examen_usuario where t03id_licencia_usuario in (select t03id_licencia_usuario from t03licencia_usuario)) lic on t03.t03id_licencia_usuario = lic.t03id_licencia_usuario
                            left join t12examen t12 ON lic.t12id_examen = t12.t12id_examen
                            WHERE t02.c01id_perfil = :idPerfil
                            and t08usuario_ligado = :estatusLigado
                            order by t12.t12id_examen asc, t01apellidos asc;';
            
            $statement = $this->adapter->query($_strQuery);
            
            try{
            $results = $statement->execute(array(
                    ':idProfesor' => $idProfesor,
                    ":idGrupo" => $idGrupo,
                    ":idPerfil" => $this->config['constantes']['ID_PERFIL_ALUMNO'],
                    ":estatusLigado" => $this->config['constantes']['ESTATUS_LIGADO']
            ));
            }  catch (\Exception $e){
                error_log($e->getMessage());
            }

            if($results->count() < 1){
                    return false;
            }
            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $_arrResults = $resultSet->toArray();
            
            return $_arrResults;
            
            
        }
	
}