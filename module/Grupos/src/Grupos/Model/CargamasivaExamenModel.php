<?php
namespace Grupos\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class CargamasivaExamenModel{
    
    
    
    	/**
	 * 
	 * @param Adapter $adapter
	 */
	public function __construct(Adapter $adapter,$config){
                $this->config = $config;
		$this->adapter = $adapter;
		$this->sql= new Sql($adapter);
	
	}
        function nombreGrupo($_intidgrupo){
            $_strQueryUsuarioLogeado='SELECT t05descripcion FROM t05grupo where t05id_grupo=:_grupo' ;
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_grupo'=> $_intidgrupo,
                                
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return $_arrResultado;
        }
        function nombreExamen($_intidexamen){
            $_strQueryUsuarioLogeado='SELECT t12nombre_corto_clave FROM t12examen where t12id_examen=:_examen' ;
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_examen'=> $_intidexamen,
                               
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return $_arrResultado;
        }
    function listadoExamenesGrupo ( $_grupo)
	{
	
			
	
		$_strQueryUsuarioLogeado='select distinct t12.t12id_examen, t12.t12nombre from t05grupo t05
									inner join t08administrador_grupo t08 on t08.t05id_grupo = t05.t05id_grupo
									inner join  t02usuario_perfil t02 on t02.t02id_usuario_perfil = t08.t02id_usuario_perfil
									inner join t03licencia_usuario t03 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil
									inner join t04examen_usuario t04 on t04.t03id_licencia_usuario = t03.t03id_licencia_usuario
									inner join t12examen t12 on t12.t12id_examen = t04.t12id_examen
									inner join t01usuario t01 on t01.t01id_usuario = t02.t01id_usuario
									where t05.t05id_grupo=:_grupo and t01estatus = :_t01estatus and t08.t08usuario_ligado= "LIGADO"' ;
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_grupo'=> $_grupo,
                                ':_t01estatus'=> $this->config['constantes']['ESTATUS_ACTIVO']
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return $_arrResultado;
	}
	
	function getNameFromNumber($num) { 
		$numeric = ($num - 1) % 26; 
		$letter = chr(65 + $numeric); 
		$num2 = intval(($num - 1) / 26); 
		
		if ($num2 > 0) { 
			return $this->getNameFromNumber($num2) . $letter; 
		} 
		
		else { 
			return $letter;
		 } 
	
	}
	
	function numeroPreguntas ( $_examen)
	{
	
			
	
		$_strQueryUsuarioLogeado='select count(t11.t11pregunta) as total from t12examen t12
									inner join t20seccion_examen t20 on t20.t12id_examen = t12.t12id_examen
									inner join t16seccion_pregunta t16 on t16.t20id_seccion_examen = t20.t20id_seccion_examen
									inner join t11pregunta t11 on t11.t11id_pregunta = t16.t11id_pregunta
									where t12.t12id_examen= :_examen';
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_examen'=> $_examen
	
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return 	$_arrResultado[0]['total'];
	}
        function alumnosGrupo ( $_grupo,$_intIdExamen)
	{
	
			
	
		$_strQueryUsuarioLogeado='select  t05.t05id_grupo, t01.t01nombre,t03.t03licencia, t02.t02id_usuario_perfil, t12id_examen  from t05grupo t05  
                                                        inner join t08administrador_grupo t08 on t08.t05id_grupo = t05.t05id_grupo                                                                                      
                                                        inner join  t02usuario_perfil t02 on t02.t02id_usuario_perfil = t08.t02id_usuario_perfil                                                                        
                                                        inner join t03licencia_usuario t03 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil                                                                       
                                                        inner join t01usuario t01 on t01.t01id_usuario = t02.t01id_usuario   
                                                        inner join t04examen_usuario t04 on t04.t03id_licencia_usuario= t03.t03id_licencia_usuario    
                                                        where t05.t05id_grupo=:_grupo and t02.c01id_perfil=1 and t01estatus = :_t01estatus and t08.t08usuario_ligado= "LIGADO" and t12id_examen=:IdExamen';
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_grupo'=> $_grupo,
                               'IdExamen' =>$_intIdExamen,
                               ':_t01estatus'=> $this->config['constantes']['ESTATUS_ACTIVO']
	
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return $_arrResultado;
	}
        
        public function validalicencia ($licencia) {
            
	
                $_strQueryExisteCorreo='SELECT 
                                            t03licencia,
                                            t03fecha_activacion,
                                            t03dias_duracion,t03estatus,
                                            DATE_ADD(t03fecha_activacion,
                                            INTERVAL t03dias_duracion DAY) ,
                                            DATEDIFF(DATE_ADD(t03fecha_activacion,
                                            INTERVAL t03dias_duracion DAY),
                                            NOW()) as dias_restantes
                                            FROM
                                            evaluaciones.t03licencia_usuario
                                            where
                                            t03licencia = :licencia';



                $statement = $this->adapter->query($_strQueryExisteCorreo);


                $results = $statement->execute(array(
                                ':licencia'=> $licencia
                ));

                $resultSet = new ResultSet;
                                $resultSet->initialize($results);


                                $_arrResultado = $resultSet->toArray();

                                     

               return $_arrResultado;
          
        }
	
    
}