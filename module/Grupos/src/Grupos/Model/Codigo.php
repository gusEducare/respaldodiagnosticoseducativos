<?php 

namespace Grupos\Model;



use Grupos\Model\Debug;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;

class Codigo
{

	/**
	 *
	 * @var Adapter $adapter.
	 */
	protected $adapter;
	/**
	 *
	 * @var Logger $_objLogger .
	 */
	protected $_objLogger;

	protected $config;

	public function __construct(Adapter $adapter, $config){
		$this->adapter = $adapter;
		$this->config = $config;
		$this->_objLogger = new Debug($config);
	}
	
	/**
	 * Genera un codigo de grupo 
	 *
	 * @return string
	 */
	public function generarCodigo() {
		
		do {
			$_intLongitudCode		= 5;
			$_arrValoresAceptados	= array('2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
			$_arrSeries				= array('a','b','c','d','e');
			$_intMax				= count($_arrValoresAceptados) - 1;
			$_intInicio			= rand(1, $_intMax);
			$_arrTemporal[0]	= $_arrValoresAceptados[$_intInicio];
			
			for($i = 1; $i < $_intLongitudCode; $i++){
				$_arrTemporal[$i] = $_arrValoresAceptados[rand(1, $_intMax)];
			}
			$_intIndiceSerie	= (($_intInicio % count($_arrSeries) == 0) ? 5 :  $_intInicio % count($_arrSeries)) -1;
			$_charSerie			= $_arrSeries[$_intIndiceSerie];
			$_arrLlaves[0]		= $_arrTemporal[0];
			$_strCodigo = "";
			$_arrNumeros = range(0,$_intLongitudCode-1);
			shuffle($_arrNumeros );
			for($i=0; $i<$_intLongitudCode; $i++){
				$_arrLlaves[$i] = $_arrTemporal[$_arrNumeros [$i]];
			}
			foreach($_arrLlaves as &$pos){
				$_strCodigo .= $pos;
			}
			
			$_strCodigo = "G-".$_strCodigo;
			
		} while ($this->getCodigoData($_strCodigo) != false);
	
		return $_strCodigo;
	}
	
	
	
	public function getCodigoData($_strCodigo){
		
		$_strQuery = "SELECT * 
						FROM t07codigo 
						WHERE :_strCodigo ";
		
		$statement = $this->adapter->query($_strQuery);
		$resultsGrupos = $statement->execute(array(
				":_strCodigo" => $_strCodigo
		));
		
		if($resultsGrupos->count() < 1){
			return false;
		}
		$resultSet = new ResultSet;
		$resultSet->initialize($resultsGrupos);
		$_arrDataCodigo = $resultSet->toArray();
		return $_arrDataCodigo;
	}
	
	public function addCodigo($_intIdGrupo){
		//generar el código 
		$_strCodigo = $this->generarCodigo();
		//almacenarlo en la BD y vincularlo con el grupo 
		$_strQueryInsert = "INSERT INTO 
								t07codigo (`t05id_grupo`, `t07codigo`, `t07tipo`, `t07ultima_actualizacion`, `t07estatus`) 
								VALUES (:_intIdGrupo, :_strCodigo, :_consTipo, NOW(), :_consEstatus) ";
		$statement = $this->adapter->query($_strQueryInsert);
		$results = $statement->execute(array(
				':_intIdGrupo' => $_intIdGrupo, 
				':_strCodigo' => $_strCodigo, 
				':_consTipo' => $this->config['constantes']['CODIGO_TIPO_GRUPO'], 
				':_consEstatus' => $this->config['constantes']['ESTATUS_ACTIVO']
		));
		$this->_objLogger->debug("codigo generated ---> ".$_strCodigo);
		$this->_objLogger->debug("codigo generated insert value---> ".$results->getGeneratedValue());
		if($results->getGeneratedValue() == null ){
			return false;
		}
		else{
			return true;
		}
	}
}