<?php

namespace Grupos\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;


class CatalogoController extends AbstractActionController{
	
	protected $_objLogger;
	protected $datos_sesion;
	
	public function __construct(){
		$this->datos_sesion = new Container('user');
	}
        
        public function menudescargaAction(){
    	
    	$_intIdUsuarioPerfilProfesor  = $this->datos_sesion->idUsuarioPerfil;
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
        $config = $this->getServiceLocator()->get('Config');
    	$_arrDataGrupos = $_modelGrupos->getGrupos($_intIdUsuarioPerfilProfesor);

    	$view = new ViewModel(array('_arrDataGrupos'=>$_arrDataGrupos,'config' =>$config ));
    	return $view;	
    }
    
    public function impresosTodoAction(){
    	
    	$_intIdUsuarioPerfilProfesor  = $this->datos_sesion->idUsuarioPerfil;
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
        $config = $this->getServiceLocator()->get('Config');
    	$_arrDataGrupos = $_modelGrupos->getGrupos($_intIdUsuarioPerfilProfesor);
    	$view = new ViewModel(array('_arrDataGrupos'=>$_arrDataGrupos,'config' =>$config ));
    	return $view;	
    }
    
    public function examenesImpresosAction(){
        $cadena = $this->params()->fromRoute('id');
        $separacion = explode('-',$cadena);
        $bloque = $separacion[0];
        $grado = $separacion[1];
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Catalogo');
    	$_arrDataMaterias = $_modelGrupos->getMaterias($bloque,$grado);

    	$view = new ViewModel(array('_arrDataMaterias'=>$_arrDataMaterias));
    	return $view;	
    }
    
    public function descargarAction(){
        $nombre= $_POST['download'];
        $url = 'http://evaluaciones.local.com/examenesTODO/';
        $nombrePDF = $nombre.'.pdf';
        $rutaPDF = $url.$nombre.'.pdf';
        header("Content-disposition: attachment; filename=$nombrePDF");
        header("Content-type: application/pdf");
        readfile($rutaPDF);
        exit;
    }
}