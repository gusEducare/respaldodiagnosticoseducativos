<?php
namespace Grupos\Controller;



//use Login\Model\RegistroModel;


use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;

use PHPExcel;
use \PHPExcel_IOFactory;
use \PHPExcel_Style_Protection;
use \PHPExcel_Style_Fill;
use \PHPExcel_Style_Alignment;
use \PHPExcel_Worksheet;
use \PHPExcel_Style_Border;



class GruposController extends AbstractActionController{
	/**
	 * Instancia de la clase Logger para controlador.
	 * @var Logger $log;
	 */
	protected $_objLogger;
	protected $datos_sesion;
	
	public function __construct(){
		//$sm = $this->getServiceLocator();
		//$this->_objLogger = $sm->get('Debug');
		$this->datos_sesion = new Container('user');
	}
	
    public function indexAction(){

    	//TODO: obtener el id del profesor de sesion
    	$_intIdUsuarioPerfilProfesor  = $this->datos_sesion->idUsuarioPerfil;
    	//revisar que tenga perfil de profesor
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
        $_modelReportes = $this->getServiceLocator()->get('Reportes\Model\Reportes');
        $examModel  =  $this->getServiceLocator()->get('/Examenes/Model/ExamenModel');
    	$_arrDataGrupos = $_modelGrupos->getGrupos($_intIdUsuarioPerfilProfesor);
    	//si no existen grupos redireccionar a la vista de agregar grupos
    	if($_arrDataGrupos ===  false){
    		if(isset($dataJsonFormat) && $dataJsonFormat == 1){
    			$jsonModel = new JsonModel();
    			$jsonModel->setVariables(false);
    			return $jsonModel;
    		}
    		else{
    			return $this->redirect()->toRoute('grupos', array('controller'=>'grupo', 'action'=>'add-grupo'));
    		}
    	}
    	$_arrResponseGrupos = array(
    			'targetContainer' => 'grupos',
    			'classRow'  => 'row',
    			'classLarge'=> 'large-6',
    			'classSmall'=> 'small-12',
    			'classColum' => 'columns',
    			'classEnd' => 'end',
    			'subTitleClass' => 'subtitleTable', 
    			'tooltipTitle' => 'Nombre del grupo',
    			'classIconTitle' => 'fa fa-users fa-2x',
    			'classIconDelete' => 'fa fa-trash-o ',
    			'tooltipDelete' => 'Haz clic para eliminar el grupo',
    			'tooltipContent1' => '',
    			'classIconContent1' => '',
    			'classContent1' => 'text-center',
    			'tooltipContent2' => 'Código de grupo. Comparte este código con tus alumnos para agregarlos al grupo.',
    			'classIconContent2' => 'fa fa-key fa-2x',
    			'classContent2' => 'codigoGrupo text-center',
    			'dataGroup' => array()
    	);
    	foreach($_arrDataGrupos as $rowGrupo){
            
            
            //agregar boton de mostrar subelementos solo cuando haya alumnos en el grupo  
            //agregar carga masiva de licencias solo cuando haya alumnos en el grupo
            $_arrUsuarios = $_modelGrupos->getUsuariosGrupo(intval($rowGrupo['t05id_grupo']), $_intIdUsuarioPerfilProfesor );
            $_arrayButtonShowElemets = null;
            $_arrayButtonAddLicencias = null;
            if(count($_arrUsuarios) > 0 ){
                $_arrayButtonShowElemets = array(
                                            'id' => 'btnShowUser-'.$rowGrupo['t05id_grupo'],
                                            'clase' => 'btnShowUser',
                                            'type' => 'showSubTable',
                                            'classIcon' => 'fa fa-list ',
                                            'text' => '',
                                            'toolTip' => 'Haz clic para mostrar / ocultar la lista de alumnos',
                                            'action' => $this->url()->fromRoute('grupos', array()),
                                            'subElements' => null
    					);
                $_arrayButtonAddLicencias = array(
                                            'id' => 'btnAddLicencias-'.$rowGrupo['t05id_grupo'],
                                            'clase' => 'btnAddLicencias',
                                            'type' => 'button',
                                            'classIcon' => 'fa fa-key buttonGrupo',
                                            'text' => '',
                                            'toolTip' => 'Haz clic para agregar licencias a los usuarios del grupo.',
                                            'action' => 'addLicenciasGrupo('.$rowGrupo['t05id_grupo'].' );',
                                            'subElements' => null    
    					);
                
            }
            //agregar boton de reportes solo a los grupos que tengan examenes contestados 
            $_arrayButtonReportes = null;
            $_arrCalificaciones = null;
            $_arrExamenes = $_modelReportes->getExamenesResueltos($rowGrupo['t05id_grupo']);
            if(count($_arrExamenes) > 0 ){
                $_arrayButtonReportes = array(
                                        'id' => 'btnGraficas-'.$rowGrupo['t05id_grupo'],
                                        'clase' => 'btnGraficas',
                                        'type' => 'button',
                                        'classIcon' => 'fa fa-bar-chart buttonGrupo',
                                        'text' => '',
                                        'toolTip' => 'Haz clic para ver el listado de reportes para el programa de Intellectus',
                                        'action' => 'showReportesOptions('.$rowGrupo['t05id_grupo'].');',
                                        'subElements' => null    
                                    );
            }
            
                $_arrayButtonReportesIC4 = null;
                //verificar si hay calificaciones
                $_arrCalificaciones = $examModel->obtenerCalificacionesGrupo($rowGrupo['t05id_grupo']);
                //$this->getServiceLocator()->get('debug')->debug($rowGrupo['t05id_grupo'].'--->'.$rowGrupo['t05descripcion'].'------->'.print_r($_arrCalificaciones,true));
                if(count($_arrCalificaciones) > 0 ){
                    $_arrayButtonReportesIC4 =  array(
                        'id' => 'btnCalif-'.$rowGrupo['t05id_grupo'],
                        'clase' => 'btnCalif',
                        'type' => 'button',
                        'classIcon' => 'fa fa-table btnShowListUser',
                        'text' => 'Ver Reportes',
                        'toolTip' => 'Haz clic para ver tabla con las calificaciones de los alumnos que pertenecen al grupo',
                        'action' => 'muestraCalificaciones('.$rowGrupo['t05id_grupo'].')',
                        'subElements' => null
                    );
                }
                
            
            $_arrExamenesInterrumpidos = $examModel->obtenerExamenesInterrumpidos($rowGrupo['t05id_grupo']);
            //$this->getServiceLocator()->get('debug')->debug($rowGrupo['t05descripcion'].'------->'.print_r($_arrExamenesInterrumpidos,true));
            $_btnExamenesInterrumpidos = null;
            if(count($_arrExamenesInterrumpidos)>0){
                $_btnExamenesInterrumpidos =  array(
                                    'id' => 'btnUnblock-'.$rowGrupo['t05id_grupo'],
                                    'clase' => 'btnUnblock',
                                    'type' => 'button',
                                    'classIcon' => 'fa fa-unlock',
                                    'text' => 'Habilitar exámenes',
                                    'toolTip' => 'Reabrir exámenes de alumnos',
                                    'action' => 'desbloqueaExamenes('.$rowGrupo['t05id_grupo'].')',
                                    'subElements' => null
                                    );
            }
            
            //revisar si el grupo tiene examenes asignados 
             
            $_arrResponseGrupos['dataGroup'][] = array(
                            'id' => $rowGrupo['t05id_grupo'],
                            'title' =>  $rowGrupo['t05descripcion'],
                            'subTitle' => 'Ciclo: '.$rowGrupo['t05ciclo'],
                            'content1' => '<small>Fecha creación: '.$rowGrupo['t08fecha_registro'].'<br/>'.'Fecha última actualización: '.$rowGrupo['t08fecha_actualiza'].'</small>',
                            'content2' => $rowGrupo['t07codigo'],
                            'urlDeleteElement' => $this->url()->fromRoute('grupos', array('controller'=>'grupo', 'action'=>'delete', 'id'=> $rowGrupo['t05id_grupo'])),
                            'buttons' => array(
                                0 => array(
                                    'id' => 'btnReporte-'.$rowGrupo['t05id_grupo'],
                                    'clase' => 'btnReporte',
                                    'type' => 'link',
                                    'classIcon' => 'fa fa-book',
                                    'text' => 'Registro exámenes',
                                    'toolTip' => 'Haz clic para ver el registro del estatus en los exámenes asignados a este grupo',
                                    'action' => $this->url()->fromRoute('grupos', array('controller'=>'grupo', 'action'=>'reporteEstatus', 'id'=> $rowGrupo['t05id_grupo'])),
                                    'subElements' => null
                                    ),
                                1 => array(
                                    'id' => 'btnCarga-'.$rowGrupo['t05id_grupo'],
                                    'clase' => 'btnCarga',
                                    'type' => 'button',
                                    'classIcon' => 'fa fa-upload',
                                    'text' => 'Carga masiva',
                                    'toolTip' => 'Haz clic para realizar la carga masiva de usuarios para este grupo',
                                    'action' => 'cargaMasivaGrupo('.$rowGrupo['t05id_grupo'].')' ,
                                    'subElements' => null
                                    ),
                                2 => $_arrayButtonAddLicencias,
                                3 => $_btnExamenesInterrumpidos,
                                4 => $_arrayButtonReportes,
                                5=> $_arrayButtonReportesIC4,
                                6 => array(
                                    'id' => 'btnOpciones-'.$rowGrupo['t05id_grupo'],
                                    'clase' => 'btnOpciones',
                                    'type' => 'link',
                                    'classIcon' => 'fa fa fa-pencil-square buttonGrupo',
                                    'text' => '',
                                    'toolTip' => 'Haz clic para editar datos del grupo',
                                    'action' => $this->url()->fromRoute('grupos', array('controller'=>'grupo', 'action'=>'editGrupo', 'id'=> $rowGrupo['t05id_grupo'])),
                                    'subElements' => null
                                ),
                                7=> $_arrayButtonShowElemets,
                                
                            ), 
                            'urlSubTable' => $this->url()->fromRoute('grupos', array('controller'=>'grupos', 'action'=>'get-alumnos')) ,
                            'postData' =>  json_encode(array('idGrupo' => $rowGrupo['t05id_grupo']))
            );
    	}
    	
    	$dataJsonFormat = $this->params()->fromPost('json');
    	if(isset($dataJsonFormat) && $dataJsonFormat == 1){
    		$jsonModel = new JsonModel();
    		$jsonModel->setVariables($_arrResponseGrupos);
    		 
    		return $jsonModel;
    	}
    	
    	return new ViewModel(array( 'json_data' => json_encode($_arrResponseGrupos)));
    	
    }
    
    public function getAlumnosAction(){
    	
    	$this->_objLogger = $this->getServiceLocator()->get('Debug');
    	
    	
    	//obtener el id del grupo
    	$_intIdGrupo = $this->params()->fromPost('idGrupo'); 
   
    	$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
	    $_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
	    $_arrUsuarios = $_modelGrupos->getUsuariosGrupo(intval($_intIdGrupo), $_intIdUsuarioPerfilProfesor );
	    
	    $this->_objLogger->debug(print_r($_arrUsuarios,true));
	    
	    $_arrResponseUsuarios = array(
	    	'classRow'  => 'row',
	    	'classLarge'=> 'large-12',
	    	'classSmall'=> 'small-12',
	    	'classSizeContainerButtons' => 'small-3',
	    	'classColum' => 'columns',
	    	'classEnd' => 'end',
	    	'inlineEdit'=> true,
	    	'dataSubElement' => array()
	    );
	    
	    foreach($_arrUsuarios as $user){
	    	$_arrResponseUsuarios['dataSubElement'][] = array(
	    		'id'=> $user['t01id_usuario'],
	    		'fields'=> array(
	    			0 => array(
	    				'id' => 'nombre-'.$user['t01id_usuario'],
	    				'classSize' => 'small-3',
	    				'iconClass' => 'fa fa-user',
	    				'labelText' => '',
	    				'fieldName' => 't01nombre',
	    				'name' => 'Nombre',
	    				'value' => $user['t01nombre'],
	    				'editInline' => true,
	    				'urlEdit'=> $this->url()->fromRoute('grupos', array('controller'=>'grupos', 'action'=>'edit-user')),
	    				'postData' => json_encode(array('idGrupo' => $_intIdGrupo, 'idUser' => $user['t01id_usuario'],'field_name' => 't01nombre')),
	    			),
	    			1 => array(
	    				'id' => 'apellido-'.$user['t01id_usuario'],
	    				'classSize' => 'small-3',
	    				'iconClass' => 'fa fa-user',
	    				'labelText' => 'Apellidos',
	    				'fieldName' => 't01apellidos',
	    				'name' => 'Apellidos',
	    				'value' => $user['t01apellidos'],
	    				'editInline' => true,
    					'urlEdit'=> $this->url()->fromRoute('grupos', array('controller'=>'grupos', 'action'=>'edit-user')),
    					'postData' => json_encode(array('idGrupo' => $_intIdGrupo, 'idUser' => $user['t01id_usuario'],'field_name' => 't01apellidos'))
	    			),
    				2 => array(
    					'id' => 'correo-'.$user['t01id_usuario'],
    					'classSize' => 'small-3',
    					'iconClass' => 'fa fa-at',
    					'labelText' => '',
    					'fieldName' => 't01correo',
    					'name' => 'Correo',
    					'value' => $user['t01correo'],
    					'editInline' => true,
    					'urlEdit'=> $this->url()->fromRoute('grupos', array('controller'=>'grupos', 'action'=>'edit-user')),
    					'postData' => json_encode(array('idGrupo' => $_intIdGrupo, 'idUser' => $user['t01id_usuario'],'field_name' => 't01correo')),
    				)
	    			
	    		),
	    		'buttons' => array(
	    				0 => array(
	    						'id' => 'btnModPass-'.$user['t01id_usuario'],
	    						'tooltip' => 'Haz clic para modificar la contraseña',
	    						'class'=> 'btnModPass',
	    						'type' => 'button',
	    						'iconClass' => 'fa fa-key',
	    						'toolTip' => 'Haz clic para modificar la contraseña',
	    						'action' => 'modificarPass('.$_intIdGrupo.','.intval($user['t01id_usuario']).');'
	    				),
	    				1 => array(
	    						'id' => 'btnDeleteUser-'.$user['t01id_usuario'],
	    						'tooltip' => 'Haz clic para eliminar alumno',
	    						'class'=> 'btnDeleteUser',
	    						'type' => 'button',
	    						'iconClass' => 'fa fa-times',
	    						'toolTip' => 'Haz clic para eliminar alumno',
	    						'action' => 'deleteUser('.$_intIdGrupo.','.intval($user['t01id_usuario']).');'
	    				),
	    		) 
	    	);
	    }
    	

    	//$this->_objLogger->debug('_arrUsuarios------>'.print_r($_arrUsuarios,true));

	    $jsonModel = new JsonModel();
	    $jsonModel->setVariables($_arrResponseUsuarios);
	    
	    return $jsonModel;
    	
    }
    
    public function editGrupoAction(){
        $this->_objLogger = $this->getServiceLocator()->get('Debug');
        $_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
        $_idGrupo = $this->params()->fromPost('txtIdGrupo');
        $modelGrupo = $this->getServiceLocator()->get('Grupos\Model\Grupo');
        if(isset($_idGrupo) && $_idGrupo != ''){
            //guardar grupo
            
            $_strCiclo = trim($this->params()->fromPost('txtInicioCiclo')).' - '. trim($this->params()->fromPost('txtFinCiclo'));
            $_strDescripcion = trim($this->params()->fromPost('txtDescripcion'));
            $_boolResult = $modelGrupo->updateGrupo($_intIdUsuarioPerfilProfesor, $_idGrupo, $_strCiclo, $_strDescripcion);
            return $this->redirect()->toRoute('grupos', array('controller'=>'grupo', 'action'=>'index'));
        }
        else{
        
            $_idGrupo = $_intIdGrupo = $this->params()->fromRoute('id');
            if(!isset($_idGrupo) && $_idGrupo != ''){
                return $this->redirect()->toRoute('grupos', array('controller'=>'grupo', 'action'=>'index'));
            }
            $_arrDataGrupo = $modelGrupo->getDataGrupo($_idGrupo, $_intIdUsuarioPerfilProfesor);
            $this->_objLogger->debug(print_r($_arrDataGrupo,true));
            $view = new ViewModel(array('_arrDataGrupo' => $_arrDataGrupo));
            $view->setTemplate('grupos/grupos/add-grupo');
            return $view;
        }
        
    }
    
    
    public function editUserAction()
    {
    	
    	$this->_objLogger = $this->getServiceLocator()->get('Debug');
    	
    	//verificar que el usuario pertenece al grupo y el grupo pertenece a quien solicita
    	//get params
    	$_intIdGrupo = $this->params()->fromPost('idGrupo');
    
    	$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
    	//get id alumno
    	$_intIdAlumno = $this->params()->fromPost('idUser');
    	//get  id grupo
    	$_intIdGrupo = $this->params()->fromPost('idGrupo');
    	$_strFieldName = $this->params()->fromPost('field_name');
        
    	$_strValue = trim($this->params()->fromPost('value'));
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    	
    	$_arrDataUser = $_modelGrupos->getUserData($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo );
    	//$_arrDataUser = $_modelGrupos->actualizaUsuarioGrupo($_intIdAlumno, $_strValue);
    	if($_arrDataUser == false){
    		$_arrResponse = array(
    				'result' => false,
    				'iconClassError' => 'fa fa-exclamation',
    				'msj' => 'Ocurrió un error inesperado, por favor intenta más tarde.'
    		);
                
    		$jsonModel = new JsonModel();
    		$jsonModel->setVariables($_arrResponse);
    		return $jsonModel;
    	}
    	//verificar si el campo solicitado cambio 
    	$this->_objLogger->debug('_arrDataUser------>'.print_r($_arrDataUser,true));
        if(isset($_arrDataUser[$_strFieldName])){
            if($_arrDataUser[$_strFieldName]  !=  trim($_strValue)){
                    //ejecutar el update
                    //el nombre de usuario requiere una validacion verificar que sea correcto
                    if($_strFieldName == 't01correo'){
                            if(strpos(trim($_strValue), ' ') > 0){
                                    $_arrResponse = array(
                                                    'result' => false,
                                                    'iconClassError' => 'fa fa-exclamation',
                                                    'msj' => 'El nombre de usuario no puede contener caracteres en blanco(espacios)'
                                    );

                                    $jsonModel = new JsonModel();
                                    $jsonModel->setVariables($_arrResponse);
                                    return $jsonModel;
                            }

                            //verificar si ese nombre de usuario esta disponible 
                            $_modelRegistro = $this->getServiceLocator()->get('Login\Model\RegistroModel');
                            $_arrValidaCorreo = $_modelRegistro->validarcorreo($_strValue);
                            $this->_objLogger->debug('arr validar correo ---> '.print_r($_arrValidaCorreo,true));

                            if(count($_arrValidaCorreo) > 0){
                                    $_arrResponse = array(
                                                    'result' => false,
                                                    'iconClassError' => 'fa fa-exclamation',
                                                    'msj' => 'Este nombre de usuario no se encuentra disponible.'
                                    );

                                    $jsonModel = new JsonModel();
                                    $jsonModel->setVariables($_arrResponse);
                                    return $jsonModel;
                            }
                    }

                    $_boolResponseUpdate = $_modelGrupos->updateUserField($_intIdAlumno,$_strFieldName, $_strValue);
                    if($_boolResponseUpdate === true){
                            $_arrResponse = array(
                                            'result' => true,
                                            'iconClassError' => '',
                                            'msj' => ''
                            );
                    }
                    else{
                            $_arrResponse = array(
                                            'result' => false,
                                            'iconClassError' => 'fa fa-exclamation',
                                            'msj' => 'Ocurrió un error inesperado, por favor intenta más tarde.'
                            );
                    }

            }
            else{
                    $_arrResponse = array(
                                    'result' => true,
                                    'iconClassError' => '',
                                    'msj' => ''
                    );
            }
        }  else {
            
	    	$_arrResponse = array(
	    			'result' => true,
	    			'iconClassError' => '',
	    			'msj' => ''
	    	);
        }
    	
    	$jsonModel = new JsonModel();
    	$jsonModel->setVariables($_arrResponse);
    	return $jsonModel;
    	
    }
    
    public function changePassAction(){
    	
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    	
    	$_intIdAlumno = $this->params()->fromPost('idAlumno');
    	$_intIdGrupo = $this->params()->fromPost('idGrupo');
    	$_strNewPass = $this->params()->fromPost('newPass');
    	$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
    	$_boolUpdate = false;
    	$_boolUpdate = $_modelGrupos->updatePassword($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo,$_strNewPass);
    	if($_boolUpdate === true){
    		$_strHtmlMsj = '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-check fa-stack-1x fa-inverse text-green"></i></span>&nbsp;Se actualizó correctamente la información.';
    	}
    	else{
    		$_strHtmlMsj = '<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-exclamation fa-stack-1x fa-inverse text-red"></i></span>&nbsp;Ocurrió un error al procesar la solicitud, por favor intenta más tarde.';
    	}
    	$_arrResponse = array(
    			'result' => $_boolUpdate,
    			'msj' => $_strHtmlMsj
    	);
    	$jsonModel = new JsonModel();
    	$jsonModel->setVariables($_arrResponse);
    	return $jsonModel;
    	
    }
    
    public function deleteUserAction(){
    	
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    	 
    	$_intIdAlumno = $this->params()->fromPost('idAlumno');
    	$_intIdGrupo = $this->params()->fromPost('idGrupo');    	
    	$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
    	
    	$_boolDelete = $_modelGrupos->deleteUserGroup($_intIdAlumno, $_intIdUsuarioPerfilProfesor, $_intIdGrupo); 
    	
    	$_arrResponse = array(
    			'result' => $_boolDelete,
    			'msj' => '<div data-alert class="alert-box no-margin"><i class="fa fa-exclamation"></i>&nbsp;El usuario ha sido eliminado de este grupo<a href="#" class="close">&times;</a> </div>'
    	);
    	$jsonModel = new JsonModel();
    	$jsonModel->setVariables($_arrResponse);
    	return $jsonModel;
    }
    
    public function deleteAction(){
    	
    	$_intIdGrupo = $this->params()->fromRoute('id');
    	$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    	$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
    	$config = $this->getServiceLocator()->get('config');
    	$_strEstatus = $config['constantes']['ESTATUS_INACTIVO'];
    	$_boolDelete = $_modelGrupos->changeEstatusGroup($_intIdUsuarioPerfilProfesor, $_intIdGrupo, $_strEstatus);
    	    	
    	return $this->redirect()->toRoute('grupos', array('controller'=>'grupo', 'action'=>'index'));
    }
    
    public function addGrupoAction(){
    	
    	$_strDescripcion = $this->params()->fromPost('txtDescripcion');
    	$_strCiclo = $this->params()->fromPost('txtInicioCiclo').' - '.$this->params()->fromPost('txtFinCiclo');
    	
    	if(isset($_strDescripcion) && isset($_strCiclo)){
    		$_strDescripcion = $this->params()->fromPost('txtDescripcion');
    		$_strCiclo = $this->params()->fromPost('txtInicioCiclo').' - '.$this->params()->fromPost('txtFinCiclo');
    		$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
    		$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    		//TODO: obtener id de colegio de sesion
    		$_idColegio = 1;
    		$_boolCreateGrupo = $_modelGrupos->addGrupo($_intIdUsuarioPerfilProfesor,$_strDescripcion,$_strCiclo,$_idColegio );
    		if($_boolCreateGrupo === false){
    			$view = new ViewModel();
    			$view->setTemplate('exception_template');
    			return $view;
    		}
    		else{
    			return $this->redirect()->toRoute('grupos', array('controller'=>'grupo', 'action'=>'index'));
    		}
    		
    	}
    	
    	$addLink = $this->params()->fromQuery('addLink');
    	
    	$view = new ViewModel(array('addLink' => $addLink));
    	return $view;	
    }
    
    
    
    public function restoreGrupoAction(){
    	
    	$_arrResponse = array(
    			'result' => false,
    	);
    	
    	$consulta = $this->params()->fromPost('consulta');
    	//$this->_objLogger = $this->getServiceLocator()->get('Debug')->debug('consulta-->'.$consulta);
    	
    	
    	if($consulta == 1){
    		$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
    		//revisar que tenga perfil de profesor
    		$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    		$config = $this->getServiceLocator()->get('config');
    		$_arrDataGrupos = $_modelGrupos->getGrupos($_intIdUsuarioPerfilProfesor, $config['constantes']['ESTATUS_INACTIVO']);
    		//construir tabla
    		if($_arrDataGrupos != false ){
    			$_strHtmlResponse = '<table id="tableDeletedGrupos" role="grid">';
    				
    				$_strHtmlResponse .= '<thead><tr>';
    				$_strHtmlResponse .= '<th>'.$this->getServiceLocator()->get('translator')->translate('').'</th>';
    				$_strHtmlResponse .= '<th>'.$this->getServiceLocator()->get('translator')->translate('Grupos').'</th>';
      				$_strHtmlResponse .= '<th>'.$this->getServiceLocator()->get('translator')->translate('Ciclo').'</th>';
      				$_strHtmlResponse .= '<th>'.$this->getServiceLocator()->get('translator')->translate('Código de grupo').'</th>';
      				$_strHtmlResponse .= '<th>'.$this->getServiceLocator()->get('translator')->translate('Fecha eliminado').'</th>';
    				$_strHtmlResponse .= '</tr></thead>';
    				$_strHtmlResponse .= '<tbody>';
    				foreach ($_arrDataGrupos as $grupo){
    					$_strHtmlResponse .= '<tr id="elementGrupo-'.$grupo['t05id_grupo'].'">';
    						$_strHtmlResponse .= '<td><button class="button small" onclick="restoreGrupo('.intval($grupo['t05id_grupo']).');"><i class="fa fa-undo" ></i></button></td>';
    						$_strHtmlResponse .= '<td>'.$grupo['t05descripcion'].'</td>';
    						$_strHtmlResponse .= '<td>'.$grupo['t05ciclo'].'</td>';
    						$_strHtmlResponse .= '<td>'.$grupo['t07codigo'].'</td>';
    						$_strHtmlResponse .= '<td>'.$grupo['t08fecha_actualiza'].'</td>';
    						
    					$_strHtmlResponse .= '</tr>';
    				}
    				$_strHtmlResponse .= '</tbody>';
    			$_strHtmlResponse .= '</table>';
    			
    			$_arrResponse = array(
    				'result' => true,
    				'html' => $_strHtmlResponse
    			); 
    		}
    		else{
    			$_arrResponse = array(
    				'result' => false,
    				'html' => '<p class="lead">'.$this->getServiceLocator()->get('translator')->translate('No existen grupos en la papelera.').'</p>'
    			);
    		}
    		
    		
    	}
    	
    	$_intIdGrupo = $this->params()->fromPost('idGrupo');
    	$this->_objLogger = $this->getServiceLocator()->get('Debug')->debug('_intIdGrupo-->'.$_intIdGrupo);
    	
    	
    	if($_intIdGrupo != null){
    		
    		$_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    		$_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
    		$config = $this->getServiceLocator()->get('config');
    		$_strEstatus = $config['constantes']['ESTATUS_ACTIVO'];
    		$_boolUpdate = $_modelGrupos->changeEstatusGroup($_intIdUsuarioPerfilProfesor, $_intIdGrupo, $_strEstatus);
    		$_msjHtml = "";
    		if($_boolUpdate){
    			$_msjHtml = '<td colspan="5"><div data-alert class="alert-box">'.$this->getServiceLocator()->get('translator')->translate('El grupo ha sido restaurado.').' <a href="#" class="close">&times;</a> </div></td>';
    		}
    		
    		$_arrResponse = array(
    				'result' => $_boolUpdate,
    				'msj' => $_msjHtml
    		);
    		
    	}
    	
    	$jsonModel = new JsonModel();
    	$jsonModel->setVariables($_arrResponse);
    	return $jsonModel;
    }
    
    
    /**
     *
     * Funcion dirigida a las cargas masivas de alumnos primera evalua que el usuario este logueado
     * de lo contrario lo direcciona a la vista de logueo, de estar logueado se crea un objeto de tipo request
     * para extraer los datos del archivo excel y el codigo del grupo al que se le van asignar los grupos
     * una vez extraidos estos datos se valida con dos foreach el objPHPExcel para validar que cada correo de los alumnos
     * no este ligado a este grupo posterior a la validacion,
     * se envia a la vista el nombre del usuario, el apellido, el codigo del grupo al que se le van asignar los alumnos
     * y un parametro para evaluar si algun alumno ya pertenece a ese grupo.
     *
     * @return ViewModel CargaMasiva.phtml : Vista para la edicion de erorres en la carga masiva.
     */
    public function cargamasivaAction(){
    	$_boolExit = false;
    	$_arrUsuarioExiste = array();
    	$config = $this->getServiceLocator()->get('Config');
    	$_objWs = new Webservice($config['constantes']['wsdl_portalGE']);
    
    	if(!$this->sesion->issetSesionVar('cerEstaLogeado') ){
    		return $this->redirect()->toRoute('base');
    	}
    
    	$request  	= 	$this->getRequest();
    	if(isset($_FILES['carga_masiva']) && ($_FILES['carga_masiva']['type'] == "application/octet-stream" || $_FILES['carga_masiva']['type'] == "application/vnd.ms-excel" || $_FILES['carga_masiva']['type'] == "vnd.oasis.opendocument.spreadsheet")){
    		$_objPHPExcel = PHPExcel_IOFactory::load($_FILES['carga_masiva']['tmp_name']);
    		$_strCodigo =  $request->getPost('_strCodigoGrupo');
    		$_iteradorRow = 1;
    		$_arrUsuarios = array();
    		$_intSalir = 0;
    
    		foreach($_objPHPExcel->getActiveSheet()->getRowIterator(2) as $row){
    			$_iteradorCell = $row->getCellIterator();
    			$_iteradorCell->setIterateOnlyExistingCells(false);
    			$itr4Cell = 1;
    			$arrDatos = array();
    			foreach ($_iteradorCell as $cell){
    				$_strValorCelda = $cell->getValue();
    				$arrDatos[$itr4Cell]	= $_strValorCelda;
    				if($itr4Cell == 3 && $_strValorCelda != ""){
    					$_arrResp = $this->getUsuarioTable()->existeCorreo( $_strValorCelda );
    					if(count($_arrResp) > 0){
    						//se crea una arreglo con los nombres de usuarios que ya existen en la base de datos para poder indicarlo en la vista
    						array_push($_arrUsuarioExiste, strtolower($_arrResp[0]['ge_t01correo']));
    					}
    				}
    				$itr4Cell++;
    			}
    			array_push($_arrUsuarios,$arrDatos);
    			$_iteradorRow++;
    		}
    	}
    	return new ViewModel(array(
    			'_strNombre'				=> $this->datos_sesion->nombreUsuario,
                        '_strApellido'				=> $this->datos_sesion->apellidos,
    			'_arrUsuarios'				=> $_arrUsuarios,
    			'_codigoGrupo'  			=> $_strCodigo,
    			'_arrUsuarioExiste'			=> $_arrUsuarioExiste,
    			'_objWs'					=> $_objWs,
    			'_SERVICIO_CERTIFICACION_GE'=>$config['constantes']['SERVICIO_CERTIFICACION_GE']));
    }
    
    public function ligaGrupoAction() {
        $config = $this->getServiceLocator()->get('Config');
        
        $request 	= $this->getRequest();
        $_codigoGrupo 	= trim ($request->getPost("strCodigo"));
        $_idUsuario	= $this->datos_sesion->idUsuarioPerfil;
        
        $_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
    	//Recibe el código de liga y regresa los datos del grupo al que pertenece
    	$_datosGrupo = $_modelGrupos->getGrupoCodigo($_codigoGrupo);
        
        if($_datosGrupo['t07estatus'] == $config['constantes']['ESTATUS_ACTIVO']){
            $_bolLiga = $_modelGrupos->ligarGrupo($_datosGrupo['t05id_grupo'],$_idUsuario);
        }else{
            $_bolLiga= false;
        }
         
        $response= $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($_bolLiga));
        return $response;
    }
    
    public function actualizaNombreGrupoajaxAction(){
        
        $request 	= $this->getRequest();
        $_idGrupo 	= trim ($request->getPost("idGrupo"));
        $_nombreGrupo 	= trim ($request->getPost("nombre"));
        
        $_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
        $_miConsulta   = $_modelGrupos->actualizaNombreGrupo($_idGrupo, $_nombreGrupo); 
        $response= $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($_miConsulta));
        return $response;
    }
     
    public function actualizaUsuarioGrupoajaxAction(){
        
        $request 	= $this->getRequest();
        $_idUsuario 	= trim ($request->getPost("idUsuario"));
        $_nombreUsuario = trim ($request->getPost("nombre"));
        $_strValues     = trim ($request->getPost("field_name"));
        
        $_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
        $_miConsulta   = $_modelGrupos->actualizaUsuarioGrupo($_intIdUsuario, $_strValue,$_strFieldName); 
        $response= $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($_miConsulta));
        return $response;
    }
    
    public function getListReportesAction(){
        $_intIdGrupo = $this->getRequest()->getPost('intIdGrupo');
        $_modelReportes = $this->getServiceLocator()->get('Reportes\Model\Reportes');
        $_arrExamenes = $_modelReportes->getExamenesResueltos($_intIdGrupo);
        
        $_arrResponse = array(
            'result' => null,
            'html' => ''
        );
        
        if(count($_arrExamenes) > 0){
            $_arrResponse['result'] = true;
            $_arrResponse['html'] .= '<ul class="fa-ul listExamenes">';
            foreach ($_arrExamenes as $exam){
                $_strLink = $this->url()->fromRoute('reportes', array('controller'=>'reportes', 'action'=>'reporteGrupo')).'?group='.$_intIdGrupo.'&exam='.$exam['t12id_examen'];
                $_arrResponse['html'] .= ' <li><i class="fa-li fa fa-arrow-circle-right"></i><a target="_blank" href="'.$_strLink.'">'.$exam['t12nombre'].'</a></li>';
            }
            $_strLink = $this->url()->fromRoute('reportes', array('controller'=>'reportes', 'action'=>'reporteGrupoComparativo')).'?group='.$_intIdGrupo;
            $_arrResponse['html'] .= ' <li><i class="fa-li fa fa-arrow-circle-right"></i><a target="_blank" href="'.$_strLink.'">Comparativo Grupos VS Grupos</a></li>'; 
            $_arrResponse['html'] .= '</ul>';
        }
        else{
            $_arrResponse = array(
                'result' => false,
                'html' => 'No hay información que mostrar.'
            );
        }
        $response= $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($_arrResponse));
        return $response;
    }
    
    
    
    
    
    /**
	 * Generar un archivo de plantilla en extension XLS para carga masiva de licencias
	 * la plantilla contiene: 
	 * 		- Id usuario
         *              - Nombre.
	 * 		- Espacio en balnco para licencia.
         *              
         *                 
	 * 
	 * @author lfcelaya@grupoeducare.com
	 * @since 14-marzo-2016
	 * @return Plantilla.xls 
	 */
    public function plantillaLicenciasAction(){
        
        
        $this->_objLogger = $this->getServiceLocator()->get('Debug');
        
        $request = $this->getRequest();
        //obtener el id del grupo
        $_intIdGrupo = $request->getQuery('idGrupo') ;
        //obtener usuarios del grupo
        $_intIdUsuarioPerfilProfesor = $this->datos_sesion->idUsuarioPerfil;
        $_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
        $_arrUsuarios = $_modelGrupos->getUsuariosGrupo(intval($_intIdGrupo), $_intIdUsuarioPerfilProfesor );

        $this->_objLogger->debug(print_r($_arrUsuarios,true));
        
        // Crear nuevo objeto PHPExcel.
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("GED")
                ->setLastModifiedBy("GED")
                ->setTitle("PlantillaLicenciasPlataformaEvaluaciones-".date('d_m_Y-h_i_s_A'))
                ->setSubject("Plataforma_Evaluaciones_Licencias")
                ->setDescription("Archivo autogenerado para la captura de licencias")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Archivo carga masiva");
	
        //$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
        //$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
        //$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
		
                
		
		
		
        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A1', 'Id')
                                ->setCellValue('B1', 'Nombre')
                                ->setcellValue('C1', 'Licencia');


        $objPHPExcel->getActiveSheet()
                                ->getStyle('A1:C1')
                                ->getFill()
                                ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('FFE5E5FF');

        $borders = array(
                        'borders' =>array(
                                        'allborders' =>array(
                                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                                                        'color' => array('argb' => 'FF000000'),
                        )
                ),
        );
		
	$contadorFilas = 2;
        for ($i=0 ;$i< count($_arrUsuarios);$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setcellValue('A'.$contadorFilas, $_arrUsuarios[$i]['t02id_usuario_perfil']);
            $objPHPExcel->setActiveSheetIndex(0)->setcellValue('B'.$contadorFilas, $_arrUsuarios[$i]['t01nombre'].' '.$_arrUsuarios[$i]['t01apellidos']);
            if (!($i%2==0)){
                $objPHPExcel->getActiveSheet()
					->getStyle('A'.$contadorFilas.':C'.$contadorFilas)
					->getFill()
					->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFEEEEEE');
            }
            $objPHPExcel->getActiveSheet()
					->getStyle('A'.$contadorFilas.':C'.$contadorFilas)
					->applyFromArray($borders);
            $contadorFilas++;
            
        }
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
        
        $objPHPExcel->getActiveSheet()->getStyle('A2:A'.$contadorFilas)->getProtection()->setLocked( PHPExcel_Style_Protection::PROTECTION_PROTECTED );
		
			
		
		
		$objPHPExcel->getActiveSheet()
					->getStyle('A1:C1')
					->applyFromArray($borders);
		
		
                header('Content-Type: application/vnd.ms-excel');
                ob_get_clean();
                if (ob_get_level()>1) {
                    ob_end_flush();                    
                }
                //ob_end_flush();
                header('Content-Disposition: attachment;filename="PlantillaLicenciasPlataformaEvaluaciones'.date('d_m_Y-h_i').'.xls"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
		
		$objWriter->save('php://output');
	
		
	}
        
    public function reporteEstatusAction(){
        
        $_intIdUsuarioPerfilProfesor  = $this->datos_sesion->idUsuarioPerfil;
        
        $_idGrupo = $this->params()->fromRoute('id');
        
        $_arrData = $this->getServiceLocator()->get('Grupos\Model\Grupo')->getReporteExamen($_idGrupo, $_intIdUsuarioPerfilProfesor);
        
        
        
        $_arrExamenes = array();
        if($_arrData != false || count($_arrData) > 0){
            foreach ($_arrData as $data){
                if(!isset($_arrExamenes[$data['t12id_examen']])){
                    $_arrExamenes[$data['t12id_examen']]['id'] = $data['t12id_examen'];
                    $_arrExamenes[$data['t12id_examen']]['nombre'] = $data['Examen'];
                    $_arrExamenes[$data['t12id_examen']]['users'] = array();
                }
                $_arrExamenes[$data['t12id_examen']]['users'][] = $data;
                
            }
        }
        
        $this->getServiceLocator()->get('Debug')->debug(print_r($_arrExamenes,true));
        
        
        return new ViewModel(array(
            '_intIdUsuarioPerfilProfesor' => $_intIdUsuarioPerfilProfesor,
            '_idGrupo'=> $_idGrupo,
            '_arrExamenes' => $_arrExamenes
        ));
        
        
    }
    
    
}
