<?php
/**
 * @author oreyes
 * @author jpgomez
 * @since 02/12/2014
 * @version  Examenes1.0
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Grupos\Controller\Grupos'    => 'Grupos\Controller\GruposController',
        	'Grupos\Controller\Cargamasiva'    => 'Grupos\Controller\CargamasivaController',
                    'Grupos\Controller\Catalogo'    => 'Grupos\Controller\CatalogoController',
                    
            ),
        ),
    'router' => array(
        'routes' => array(
            'grupos' => array(
                'type' =>  'Segment',
                'options' => array(
                	'route'       => '/grupos[/:action][/:id]',
                	'constraints' => array(
                		'controller'=> '[a-zA-Z][a-zA-Z0-9_-]*',
                		'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                	),
                	'defaults' => array(
                		'controller' => 'Grupos\Controller\Grupos',
                		'action'     => 'index',
                	),
            	),
        	),
            'preguntas' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'         => '/preguntas[/[:action]][/:id]',
                    'defaults'          => array(
                        'controller'    => 'preguntas',
                        'action'        => 'index',
                    ),
                ),
            ), 
           'catalogo' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'         => '/catalogo[/[:action]][/:id]',
                    'defaults'          => array(
                        'controller'    => 'Grupos\Controller\Catalogo',
                        'action'        => 'menudescarga',
                    ),
                ),
            ), 
        		'cargamasiva' => array(
        				'type' =>  'Segment',
        				'options' => array(
        						'route'       => '/cargamasiva[/:action][/:id]',
        						'constraints' => array(
        								'controller'=> '[a-zA-Z][a-zA-Z0-9_-]*',
        								'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
        						),
        						'defaults' => array(
        								'controller' => 'Grupos\Controller\Cargamasiva',
        								'action'     => 'index',
        						),
        				),
        		),
    	),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'grupos/grupos/index' => __DIR__ . '/../view/grupos/index.phtml',
            'grupos/grupos/add-grupo' => __DIR__ . '/../view/grupos/add-grupo.phtml',
            'grupos/grupos/restore-grupo' => __DIR__ . '/../view/grupos/restore-grupo.phtml',
            'grupos/cargamasiva/index' =>  __DIR__ . '/../view/cargamasiva/index.phtml',
            'grupos/cargamasiva/validarplantilla' =>  __DIR__ . '/../view/cargamasiva/validarplantilla.phtml',
            'grupos/cargamasiva/validacmlicencias' =>  __DIR__ . '/../view/cargamasiva/validacmlicencias.phtml',
            'grupos/cargamasiva/validarplantillaexamen' =>  __DIR__ . '/../view/cargamasiva/validarplantillaexamen.phtml',
            'grupos/catalogo/menudescarga' => __DIR__ . '/../view/catalogo/menudescarga.phtml',
            'grupos/catalogo/impresos-todo' => __DIR__ . '/../view/catalogo/impresos-todo.phtml',
            'grupos/catalogo/examenes-impresos' => __DIR__ . '/../view/catalogo/examenes-impresos.phtml',
        ),
        'template_path_stack' => array(
            'grupos' => __DIR__ . '/../view',
        ),
        'strategies' => array(
        	'ViewJsonStrategy',
        ),
    ),
);
