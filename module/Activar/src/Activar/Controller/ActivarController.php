<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Activar\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Json\Json;
use Zend\Session\Container;



class ActivarController extends AbstractActionController{
    
    protected $datos_sesion;
    
    
    public function __construct(){
        $this->datos_sesion = new Container('activar');
        
    }
    
    public function indexAction(){
        
        if(isset($this->datos_sesion)){
            $this->datos_sesion = new Container('activar');
        }
        
        
        $_modelActivar = $this->getServiceLocator()->get('Activar\Model\Activar');
        $config = $this->getServiceLocator()->get('Config');
        
        $_arrUsuariosKinder = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_KINDER'],false,false,true);
        
        $_arrUsuariosPrimaria1 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_1'],false,false,true);
        $_arrUsuariosPrimaria2 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_2'],false,false,true);
        $_arrUsuariosPrimaria3 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_3'],false,false,true);
        $_arrUsuariosPrimaria4 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_4'],false,false,true);
        $_arrUsuariosPrimaria5 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_5'],false,false,true);
        $_arrUsuariosPrimaria6 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_6'],false,false,true);
        
        $_arrUsuariosSecundaria1 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_SECUNDARIA_1'],false,false,true);
        $_arrUsuariosSecundaria2 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_SECUNDARIA_2'],false,false,true);
        $_arrUsuariosSecundaria3 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_SECUNDARIA_3'],false,false,true);
        
        $_arrUsuariosPreparatoria1 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PREPARATORIA_1'],false,false,true);
        $_arrUsuariosPreparatoria2 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PREPARATORIA_2'],false,false,true);
        $_arrUsuariosPreparatoria3 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PREPARATORIA_3'],false,false,true);
        
        
        
        $this->datos_sesion->cantidadKinder = $_arrUsuariosKinder[0]['cantidad'];
        
        $this->datos_sesion->cantidadPrimaria1 = $_arrUsuariosPrimaria1[0]['cantidad'];
        $this->datos_sesion->cantidadPrimaria2 = $_arrUsuariosPrimaria2[0]['cantidad'];
        $this->datos_sesion->cantidadPrimaria3 = $_arrUsuariosPrimaria3[0]['cantidad'];
        $this->datos_sesion->cantidadPrimaria4 = $_arrUsuariosPrimaria4[0]['cantidad'];
        $this->datos_sesion->cantidadPrimaria5 = $_arrUsuariosPrimaria5[0]['cantidad'];
        $this->datos_sesion->cantidadPrimaria6 = $_arrUsuariosPrimaria6[0]['cantidad'];
       
        $this->datos_sesion->cantidadSecundaria1 = $_arrUsuariosSecundaria1[0]['cantidad'];
        $this->datos_sesion->cantidadSecundaria2 = $_arrUsuariosSecundaria2[0]['cantidad'];
        $this->datos_sesion->cantidadSecundaria3 = $_arrUsuariosSecundaria3[0]['cantidad'];
        
        $this->datos_sesion->cantidadPreparatoria1 = $_arrUsuariosPreparatoria1[0]['cantidad'];
        $this->datos_sesion->cantidadPreparatoria2 = $_arrUsuariosPreparatoria2[0]['cantidad'];
        $this->datos_sesion->cantidadPreparatoria3 = $_arrUsuariosPreparatoria3[0]['cantidad'];
        
        $this->datos_sesion->contKinder = 0;
        
        $this->datos_sesion->contPrimaria1 = 0;
        $this->datos_sesion->contPrimaria2 = 0;
        $this->datos_sesion->contPrimaria3 = 0;
        $this->datos_sesion->contPrimaria4 = 0;
        $this->datos_sesion->contPrimaria5 = 0;
        $this->datos_sesion->contPrimaria6 = 0;
        
        $this->datos_sesion->contSecundaria1 = 0;
        $this->datos_sesion->contSecundaria2 = 0;
        $this->datos_sesion->contSecundaria3 = 0;
        
        $this->datos_sesion->contPreparatoria1 = 0;
        $this->datos_sesion->contPreparatoria2 = 0;
        $this->datos_sesion->contPreparatoria3 = 0;
        
        return new ViewModel(array(
            '_arrUsuariosKinder' => $_arrUsuariosKinder,
            '_arrUsuariosPrimaria1' => $_arrUsuariosPrimaria1,
            '_arrUsuariosPrimaria2' => $_arrUsuariosPrimaria2,
            '_arrUsuariosPrimaria3' => $_arrUsuariosPrimaria3,
            '_arrUsuariosPrimaria4' => $_arrUsuariosPrimaria4,
            '_arrUsuariosPrimaria5' => $_arrUsuariosPrimaria5,
            '_arrUsuariosPrimaria6' => $_arrUsuariosPrimaria6,
            '_arrUsuariosSecundaria1' => $_arrUsuariosSecundaria1,
            '_arrUsuariosSecundaria2' => $_arrUsuariosSecundaria2,
            '_arrUsuariosSecundaria3' => $_arrUsuariosSecundaria3,
            '_arrUsuariosPreparatoria1' => $_arrUsuariosPreparatoria1,
            '_arrUsuariosPreparatoria2' => $_arrUsuariosPreparatoria2,
            '_arrUsuariosPreparatoria3' => $_arrUsuariosPreparatoria3,
            'config' => $config
                ));
    }
    
    
    public function activarpostAction(){
        
        
        $_arrUsuariosInsertar = array();
        
        if(!isset($this->datos_sesion)){
            $this->datos_sesion = new Container('activar');
        }
        
        $regModel 	= $this->getServiceLocator()->get('/Login/Model/RegistroModel');  
        $_modelActivar = $this->getServiceLocator()->get('Activar\Model\Activar');
        $config = $this->getServiceLocator()->get('Config');
        
        $start = 0;
        $limit = 10;
        
        $_arrUsuariosKinder = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_KINDER'],$start,$limit,false);
        
        if(count($_arrUsuariosKinder) > 0 ){
            $_arrUsuariosInsertar = $_arrUsuariosKinder;
            $_intIdExamen = $config['constantes']['ID_INTELLECTUS_KINDER_POST'];
            
            if(!isset($this->datos_sesion->contKinder)){
                $this->datos_sesion->contKinder = 0;
            }
            $this->datos_sesion->contKinder += count($_arrUsuariosInsertar);
            
            $porcentaje  = ($this->datos_sesion->contKinder *100) / $this->datos_sesion->cantidadKinder;
            $barra = 'meter-1';
            $done = false;
            
        }else{
            
            $_arrUsuariosPrimaria1 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_1'],$start,$limit,false);
            
            if(count($_arrUsuariosPrimaria1) > 0){
                $_arrUsuariosInsertar = $_arrUsuariosPrimaria1;
                $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST'];

                if(!isset($this->datos_sesion->contPrimaria1)){
                    $this->datos_sesion->contPrimaria1 = 0;
                }
                $this->datos_sesion->contPrimaria1 += count($_arrUsuariosInsertar);

                $porcentaje  = ($this->datos_sesion->contPrimaria1 *100) / $this->datos_sesion->cantidadPrimaria1;
                $barra = 'meter-2';
                $done = false;

            }else{
                
                
                $_arrUsuariosPrimaria2 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_2'],$start,$limit,false);
                
                if(count($_arrUsuariosPrimaria2) > 0){
                    $_arrUsuariosInsertar = $_arrUsuariosPrimaria2;
                    $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST'];

                    if(!isset($this->datos_sesion->contPrimaria2)){
                        $this->datos_sesion->contPrimaria2 = 0;
                    }
                    $this->datos_sesion->contPrimaria2 += count($_arrUsuariosInsertar);

                    $porcentaje  = ($this->datos_sesion->contPrimaria2 *100) / $this->datos_sesion->cantidadPrimaria2;
                    $barra = 'meter-3';
                    $done = false;

                }else{
                    
                    $_arrUsuariosPrimaria3 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_3'],$start,$limit,false);
                    if(count($_arrUsuariosPrimaria3) > 0){
                        $_arrUsuariosInsertar = $_arrUsuariosPrimaria3;
                        $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST'];

                        if(!isset($this->datos_sesion->contPrimaria3)){
                            $this->datos_sesion->contPrimaria3 = 0;
                        }
                        $this->datos_sesion->contPrimaria3 += count($_arrUsuariosInsertar);

                        $porcentaje  = ($this->datos_sesion->contPrimaria3 *100) / $this->datos_sesion->cantidadPrimaria3;
                        $barra = 'meter-4';
                        $done = false;

                    }else{
                        $_arrUsuariosPrimaria4 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_4'],$start,$limit,false);
                        if(count($_arrUsuariosPrimaria4) > 0){
                            $_arrUsuariosInsertar = $_arrUsuariosPrimaria4;
                            $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST'];

                            if(!isset($this->datos_sesion->contPrimaria4)){
                                $this->datos_sesion->contPrimaria4 = 0;
                            }
                            $this->datos_sesion->contPrimaria4 += count($_arrUsuariosInsertar);

                            $porcentaje  = ($this->datos_sesion->contPrimaria4 *100) / $this->datos_sesion->cantidadPrimaria4;
                            $barra = 'meter-5';
                            $done = false;

                        }else{
                            $_arrUsuariosPrimaria5 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_5'],$start,$limit,false);
                            if(count($_arrUsuariosPrimaria5) > 0){
                                $_arrUsuariosInsertar = $_arrUsuariosPrimaria5;
                                $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST'];

                                if(!isset($this->datos_sesion->contPrimaria5)){
                                    $this->datos_sesion->contPrimaria5 = 0;
                                }
                                $this->datos_sesion->contPrimaria5 += count($_arrUsuariosInsertar);

                                $porcentaje  = ($this->datos_sesion->contPrimaria5 *100) / $this->datos_sesion->cantidadPrimaria5;
                                $barra = 'meter-6';
                                $done = false;

                            }else{
                                $_arrUsuariosPrimaria6 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PRIMARIA_6'],$start,$limit,false);
                                if(count($_arrUsuariosPrimaria6) > 0){
                                    $_arrUsuariosInsertar = $_arrUsuariosPrimaria6;
                                    $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST'];

                                    if(!isset($this->datos_sesion->contPrimaria6)){
                                        $this->datos_sesion->contPrimaria6 = 0;
                                    }
                                    $this->datos_sesion->contPrimaria6 += count($_arrUsuariosInsertar);

                                    $porcentaje  = ($this->datos_sesion->contPrimaria6 *100) / $this->datos_sesion->cantidadPrimaria6;
                                    $barra = 'meter-7';
                                    $done = false;

                                }else{
                                    $_arrUsuariosSecundaria1 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_SECUNDARIA_1'],$start,$limit,false);
                                    if(count($_arrUsuariosSecundaria1) > 0){
                                        $_arrUsuariosInsertar = $_arrUsuariosSecundaria1;
                                        $_intIdExamen = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST'];

                                        if(!isset($this->datos_sesion->contSecundaria1)){
                                            $this->datos_sesion->contSecundaria1 = 0;
                                        }
                                        $this->datos_sesion->contSecundaria1 += count($_arrUsuariosInsertar);

                                        $porcentaje  = ($this->datos_sesion->contSecundaria1 *100) / $this->datos_sesion->cantidadSecundaria1;
                                        $barra = 'meter-8';
                                        $done = false;

                                    }else{
                                        $_arrUsuariosSecundaria2 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_SECUNDARIA_2'],$start,$limit,false);
                                        if(count($_arrUsuariosSecundaria2) > 0){
                                            $_arrUsuariosInsertar = $_arrUsuariosSecundaria2;
                                            $_intIdExamen = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST'];

                                            if(!isset($this->datos_sesion->contSecundaria2)){
                                                $this->datos_sesion->contSecundaria2 = 0;
                                            }
                                            $this->datos_sesion->contSecundaria2 += count($_arrUsuariosInsertar);

                                            $porcentaje  = ($this->datos_sesion->contSecundaria2 *100) / $this->datos_sesion->cantidadSecundaria2;
                                            $barra = 'meter-9';
                                            $done = false;

                                        }else{
                                            $_arrUsuariosSecundaria3 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_SECUNDARIA_3'],$start,$limit,false);
                                            if(count($_arrUsuariosSecundaria3) > 0){
                                                $_arrUsuariosInsertar = $_arrUsuariosSecundaria3;
                                                $_intIdExamen = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST'];

                                                if(!isset($this->datos_sesion->contSecundaria3)){
                                                    $this->datos_sesion->contSecundaria3 = 0;
                                                }
                                                $this->datos_sesion->contSecundaria3 += count($_arrUsuariosInsertar);

                                                $porcentaje  = ($this->datos_sesion->contSecundaria3 *100) / $this->datos_sesion->cantidadSecundaria3;
                                                $barra = 'meter-10';
                                                $done = false;

                                            }else{
                                                $_arrUsuariosPreparatoria1 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PREPARATORIA_1'],$start,$limit,false);
                                                if(count($_arrUsuariosPreparatoria1) > 0){
                                                    $_arrUsuariosInsertar = $_arrUsuariosPreparatoria1;
                                                    $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST'];

                                                    if(!isset($this->datos_sesion->contPreparatoria1)){
                                                        $this->datos_sesion->contPreparatoria1 = 0;
                                                    }
                                                    $this->datos_sesion->contPreparatoria1 += count($_arrUsuariosInsertar);

                                                    $porcentaje  = ($this->datos_sesion->contPreparatoria1 *100) / $this->datos_sesion->cantidadPreparatoria1;
                                                    $barra = 'meter-10';
                                                    $done = false;

                                                }else{
                                                    $_arrUsuariosPreparatoria2 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PREPARATORIA_2'],$start,$limit,false);
                                                    if(count($_arrUsuariosPreparatoria2) > 0){
                                                        $_arrUsuariosInsertar = $_arrUsuariosPreparatoria2;
                                                        $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST'];

                                                        if(!isset($this->datos_sesion->contPreparatoria2)){
                                                            $this->datos_sesion->contPreparatoria2 = 0;
                                                        }
                                                        $this->datos_sesion->contPreparatoria2 += count($_arrUsuariosInsertar);

                                                        $porcentaje  = ($this->datos_sesion->contPreparatoria2 *100) / $this->datos_sesion->cantidadPreparatoria2;
                                                        $barra = 'meter-11';
                                                        $done = false;

                                                    }else{
                                                        $_arrUsuariosPreparatoria3 = $_modelActivar->getUsuariosPost($config['constantes']['ID_INTELLECTUS_PREPARATORIA_3'],$start,$limit,false);
                                                        if(count($_arrUsuariosPreparatoria3) > 0){
                                                            $_arrUsuariosInsertar = $_arrUsuariosPreparatoria3;
                                                            $_intIdExamen = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST'];

                                                            if(!isset($this->datos_sesion->contPreparatoria3)){
                                                                $this->datos_sesion->contPreparatoria3 = 0;
                                                            }
                                                            $this->datos_sesion->contPreparatoria3 += count($_arrUsuariosInsertar);

                                                            $porcentaje  = ($this->datos_sesion->contPreparatoria3 *100) / $this->datos_sesion->cantidadPreparatoria3;
                                                            $barra = 'meter-12';
                                                            $done = false;

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
        
        if(count($_arrUsuariosInsertar) > 0){
            
            
            
            foreach ($_arrUsuariosInsertar as $user){
               $idExamenUsuario = $regModel->insertarExamenesUsuario($user['t02id_usuario_perfil'], $_intIdExamen, $user['t03licencia'].'-POST', $user['t03dias_duracion']);
            }
            $_arrRespuesta =  array(
                'porcentaje' => $porcentaje,
                'barra' => $barra,
                'done' => false
            );
            
        }
        else{
            $_arrRespuesta =  array(
                'porcentaje' => null,
                'barra' => null,
                'done' => true
            );
        }
        
        
        
        $response = $this->getResponse();
    	$response->setContent(Json::encode($_arrRespuesta));
    	return $response;
        
        
    }
    
    
    public function executeOrder66Action(){
        
        $_modelActivar = $this->getServiceLocator()->get('Activar\Model\Activar');
        $config = $this->getServiceLocator()->get('Config');
        $_arrUsuariosInsertar = array();
        
        //kinder
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_KINDER_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_KINDER'])
        );
        
        
        //primaria        
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PRIMARIA_1'])
        );
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PRIMARIA_2'])
        );
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PRIMARIA_3'])
        ); 
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PRIMARIA_4'])
        ); 
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PRIMARIA_5'])
        );
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PRIMARIA_6'])
        );
        
        //secundaria 
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_SECUNDARIA_1'])
        );
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_SECUNDARIA_2'])
        );
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_SECUNDARIA_3'])
        );
        
        
        //preparatoria
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PREPARATORIA_1'])
        );
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PREPARATORIA_2'])
        );
        $_arrUsuariosInsertar[] = array(
                'idExamenPost' => $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST'],
                'users' => $_modelActivar->getPost15Days($config['constantes']['ID_INTELLECTUS_PREPARATORIA_3'])
        );
        
        $regModel 	= $this->getServiceLocator()->get('/Login/Model/RegistroModel');  
        if(count($_arrUsuariosInsertar) > 0){
            foreach ($_arrUsuariosInsertar as $data){
                if(count($data['users']) > 0){
                   foreach ($data['users'] as $user){
                       $idExamenUsuario = $regModel->insertarExamenesUsuario($user['t02id_usuario_perfil'], $data['idExamenPost'], $user['t03licencia'].'-POST', $user['t03dias_duracion']);
                       if( $idExamenUsuario !== false){
                           echo $user['t02id_usuario_perfil'].' OK \n';
                       }
                       else{
                           echo $user['t02id_usuario_perfil'].' FAIL \n';
                       }
                   }
               }
            }
        }
        
    }
    
    
}
