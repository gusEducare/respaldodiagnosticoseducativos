<?php
namespace Activar\Model;



use Grupos\Model\Debug;

use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;

final class examenIntellectus {

   

}

Class Activar {

    protected $config;
    protected $_objLog;
    
    public function __construct(Adapter $adapter, $config) {
        $this->adapter = $adapter;
        $this->config = $config;
        
    }

    /**
     * La funcion se encarga de obtener los usuarios que tienen examen INTELECTUS PRE resuelto y no se les haya asignado el POST aún 
     * @access public
     * @author lfcelaya
     * @since 3/05/2016
     * @access public
     * @param int $idExamenPre id del examen pre
     * @param int $start row de inicio para el query 
     * @param int $limit row de limite para el query
     * @param int $isCount agregar la funcion count al query 
     * @return Array $_arrData array con el resultado
     */
    public function getUsuariosPost($idExamenPre, $start = false, $limit = false, $isCount = false ){
        if($isCount == false){
            $colums = ' t02.t02id_usuario_perfil, 
                        t03.t03licencia, 
                        t03dias_duracion  ';
        }
        else{
            $colums = 'COUNT(*) as cantidad';
        }
        
        
        $limitSql = "";
        if($limit != false ){
            $limitSql = 'LIMIT '.$start.', '.$limit;
        }
        
        
        
        $_idExamenPost = null;
        switch ($idExamenPre){
            case $this->config['constantes']['ID_INTELLECTUS_KINDER']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_KINDER_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_1']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_2']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_3']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_4']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_5']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_6']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_1']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_2']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_3']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_1']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_2']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_3']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST'];
                break;
            default :
                return false;
                break;
        }
        $_strQuery = 'SELECT 
                        '.$colums.'
                        FROM t04examen_usuario t04
                        INNER JOIN t03licencia_usuario t03 ON (t03.t03id_licencia_usuario = t04.t03id_licencia_usuario)
                        INNER JOIN t02usuario_perfil t02 ON ( t02.t02id_usuario_perfil = t03.t02id_usuario_perfil)
                        INNER JOIN t01usuario t01 ON (t01.t01id_usuario = t02.t01id_usuario)
                        WHERE t04.t12id_examen =  '.$idExamenPre.' 
                            AND t04.t04estatus <> 3
                        AND t02.t02id_usuario_perfil NOT IN (
                                SELECT t002.t02id_usuario_perfil FROM t04examen_usuario t004
                                INNER JOIN t03licencia_usuario t003 ON (t003.t03id_licencia_usuario = t004.t03id_licencia_usuario)
                                INNER JOIN t02usuario_perfil t002 ON ( t002.t02id_usuario_perfil = t003.t02id_usuario_perfil)
                                INNER JOIN t01usuario t001 ON (t001.t01id_usuario = t002.t01id_usuario)
                                WHERE t004.t12id_examen = '.$_idExamenPost.' 
                        )  '.$limitSql ;
        
        
        
        $statement = $this->adapter->query($_strQuery);
		$results = $statement->execute();
                /*
		if($results->count() < 1){
			return array();
		}*/
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
		$_arrData = $resultSet->toArray();
		return $_arrData;
    }
    
    
    public function getPost15Days($idExamenPre){
        
        
        $_idExamenPost = null;
        switch ($idExamenPre){
            case $this->config['constantes']['ID_INTELLECTUS_KINDER']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_KINDER_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_1']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_2']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_3']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_4']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_5']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_6']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_1']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_2']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_3']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_1']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_2']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST'];
                break;
            case $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_3']:
                $_idExamenPost = $this->config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST'];
                break;
            default :
                return false;
                break;
        }
        
        $_strQuery = 'SELECT 
                        t02.t02id_usuario_perfil, 
                        t03.t03licencia, 
                        t03dias_duracion
                        FROM t04examen_usuario t04
                        INNER JOIN t03licencia_usuario t03 ON (t03.t03id_licencia_usuario = t04.t03id_licencia_usuario)
                        INNER JOIN t02usuario_perfil t02 ON ( t02.t02id_usuario_perfil = t03.t02id_usuario_perfil)
                        INNER JOIN t01usuario t01 ON (t01.t01id_usuario = t02.t01id_usuario)
                        WHERE t04.t12id_examen =  '.$idExamenPre.' 
                            AND t04.t04estatus <> 3
                            AND t04fecha_fin  <  (CURDATE() - INTERVAL 15 DAY)
                        AND t02.t02id_usuario_perfil NOT IN (
                                SELECT t002.t02id_usuario_perfil FROM t04examen_usuario t004
                                INNER JOIN t03licencia_usuario t003 ON (t003.t03id_licencia_usuario = t004.t03id_licencia_usuario)
                                INNER JOIN t02usuario_perfil t002 ON ( t002.t02id_usuario_perfil = t003.t02id_usuario_perfil)
                                INNER JOIN t01usuario t001 ON (t001.t01id_usuario = t002.t01id_usuario)
                                WHERE t004.t12id_examen = '.$_idExamenPost.' 
                        )  ';
        
        $statement = $this->adapter->query($_strQuery);
        $results = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($results);
        $_arrData = $resultSet->toArray();
        return $_arrData;
    }
}
