<?php
return array(
		'router' => array(
				'routes' => array(
						
						'activar' => array(
								'type' =>  'Segment',
								'options' => array(
										'route'         => '/activar[/[:action]][/:id]',
										'constraints'   => array(
												'controller'=> '[a-zA-Z][a-zA-Z0-9_-]*',
												'action'    =>  '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'        =>  '[0-9]*',
										),
										'defaults'          => array(
												'controller'    => 'activar',
												'action'        => 'index',
											
										),
								),
						),
						
		
	),
),

		'service_manager' => array(
				'abstract_factories' => array(
						'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
						'Zend\Log\LoggerAbstractServiceFactory',
				),
				'aliases' => array(
						'translator' => 'MvcTranslator',
				),
		),

		

		'controllers' => array(
                        'invokables' => array(
			   'activar' => 'Activar\Controller\ActivarController',
			)
		),

		'view_manager' => array(
				'display_not_found_reason' => true,
				'display_exceptions'       => true,
				'doctype'                  => 'HTML5',
				'not_found_template'       => 'error/404',
				'exception_template'       => 'error/index',
				'template_map' => array(
						
				),
				'template_path_stack' => array(
						'agenda' => __DIR__ . '/../view',
						__DIR__ . '/../view',
				),
		),
		// Placeholder for console routes
		
                'console' => array(
                    'router' => array(
                        'routes' => array(
                            'activar' => array(
                                'options' => array(
                                    'route'    => 'executeOrder66',
                                    'defaults' => array(
                                        'controller' => 'activar',
                                        'action' => 'executeOrder66'
                                    )
                                )
                            )
                        )
                    )
                )
);




?>