<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Activar;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;

use Activar\Model\Activar;


use Zend\Session\Container;

class Module
{
    
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    
    public function getServiceConfig(){
    	
    	return Array('factories'=>
    			array(
    					
                                        'Activar\Model\Activar' => function($sm){
                                                $config = $sm->get('config');
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$model = new Activar($dbAdapter, $config);
    						return $model;
    					
    					}
                                       
    					
    			)
    	);
    }
    
}
