<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Acciones;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;
use Zend\Session\Container;
use Acciones\Model\AccionesModel;

class Module{
    
    public function onBootstrap(MvcEvent $e) {        
        $sharedEvents = $e->getApplication()->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'verAcl'));
    }
    
    private  function toCamelCase($str, array $noStrip = array())
    {
            $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
            $str = trim($str);
            
            $str = ucwords($str);
            $str = str_replace(" ", "", $str);
            $str = lcfirst($str);
            return $str;
    }
  
    public function verAcl(MvcEvent $e) {
        $config = include __DIR__ . '/../../config/autoload/global.php';
        $this->datos_sesion = new Container('user');
        $_intUserRole = ($this->datos_sesion->perfil) ? $this->datos_sesion->perfil : $config['constantes']['ID_PERFIL_INVITATO'];
        $_strRoute = $e -> getRouteMatch() -> getMatchedRouteName();
        $_strAccion = $e -> getRouteMatch() ->getParam('action');
        $_strAccion = $this->toCamelCase($_strAccion);
        
        if (!$e -> getViewModel() -> acl -> isAllowed($_intUserRole, $_strRoute,$_strAccion)) {
            $response = $e -> getResponse();
            $response -> getHeaders() -> addHeaderLine('Location', $e -> getRequest() -> getBaseUrl() . '/404');
            $response -> setStatusCode($config ['constantes']['ERROR_PERMISO_DENEGADO']);
            
            //error_log($_intUserRole.'<-NO PERMITIDO '.$_strRoute.'/'.$_strAccion);

        }else{
             //error_log($_intUserRole.'<-PERMITIDO '.$_strRoute.'/'.$_strAccion);
        }
    }
    
    public function getConfig(){
        return include __DIR__.'/config/module.config.php';
    }
    
    
    
    public function getServiceConfig(){
        return array('factories' =>
            array(
               
                'Acciones\Model\AccionesModel' => function($sm){
                    $config    = $sm->get('config');
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table   = new AccionesModel($dbAdapter,  $config);
                    return $table;
                }
            )
        );
    }
    public function getAutoloaderConfig(){
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            )
        );
    }
}