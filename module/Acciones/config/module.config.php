<?php
/**
 * @author rjuarez@grupoeducare.com
 * @since  12/julio/2016
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Acciones\Controller\Acciones'   => 'Acciones\Controller\AccionesController',
        )
    ),
    'router' => array(
        'routes' => array(
            'acciones' => array(
                'type' =>  'Segment',
                'options' => array(
                	'route'       => '/acciones[/:action][/:id]',
                	'constraints' => array(
                		'controller'=> '[a-zA-Z][a-zA-Z0-9_-]*',
                		'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                	),
                	'defaults' => array(
                		'controller' => 'Acciones\Controller\Acciones',
                		'action'     => 'index',
                	),
            	),
            )
        )
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'acciones/acciones/index' => __DIR__ . '/../view/acciones/index.phtml',
        ),
        'template_path_stack' => array(
            'agenda' => __DIR__ . '/../view',
            __DIR__ . '/../view',
        ),
        'strategies' => array(
        	'ViewJsonStrategy',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);


?>