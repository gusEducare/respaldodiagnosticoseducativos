<?php

namespace Acciones\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;

class AccionesController extends AbstractActionController{
    protected $_objLogger;
    protected $datos_session;
    
    public function indexAction(){
        
        
        $view = new ViewModel(
                array(
                )
        );
    
        $view->setTerminal(true);
        return $view;
        
    }
    
    public function registrarusuarioAction(){
        $config = $this->getServiceLocator()->get('config');
        
        $nombre            = 'Rodolfo';
        $apellidos         = 'juarez maximino';
        $correo            = 'rjuarez6prof@grupoeducare.com';
        $pass              = sha1('123456') ;
        $bloqueada         = 1;
        $timeBloqueo       = '0000-00-00 00:00:00';
        $fecharegistro     = '2016-08-08 00:00:00';
        $fechaActualizada  = '2014-01-01 00:00:00';
        $demo              = 1;
        $accept_conditions = 1;
        $providerId        = 'provider id';
        $provider        = 'provider';
        $estatus           = '1';
        $idPerfilUsuario   = 2;    //perfil de profesor
//        $idPerfilUsuario   = 1;  //perfil de alumno
        $codigoGrupo       = 'AH1N1'; 
        $_strLlave         = 'ABCDEF';
        $_diasLicencia     = 30;
        $modelAcciones = $this->getServiceLocator()->get('Grupos\Model\Acciones');
        $arrayGenerated = array();
        if($config['constantes']['ID_PERFIL_PROFESOR'] === $idPerfilUsuario){//inserta perfil profesor 
            $_intIdUsuarioPerfil      = $modelAcciones->registrarusuario($nombre,$apellidos,$correo,$pass,$bloqueada,$timeBloqueo,$fecharegistro,$fechaActualizada,$demo,$accept_conditions,$providerId,$provider,$estatus,$idPerfilUsuario);
            $_intIdLicenciaProfesor   = $modelAcciones->insertarLicenciaProfesor($_intIdUsuarioPerfil,$_strLlave,$_diasLicencia);
            $arrayGenerated[0] = $_intIdUsuarioPerfil;
            $arrayGenerated[1] = $_intIdLicenciaProfesor;
        }elseif($config['constantes']['ID_PERFIL_ALUMNO'] === $idPerfilUsuario){//inserta perfil alumno
            $_intIdUsuarioPerfil      = $modelAcciones->registrarusuario($nombre,$apellidos,$correo,$pass,$bloqueada,$timeBloqueo,$fecharegistro,$fechaActualizada,$demo,$accept_conditions,$providerId,$provider,$estatus,$idPerfilUsuario);
            $idExamenUusuario = $modelAcciones->insertarExamenesUsuario($_intIdUsuarioPerfil, 1, $_strLlave, 350);
            $idGrupo  = $modelAcciones->ligarUsuarioGrupo($_intIdUsuarioPerfil,$codigoGrupo);
            $arrayGenerated[0] = $_intIdUsuarioPerfil;
            $arrayGenerated[1] = $idExamenUusuario;
            $arrayGenerated[2] = $idGrupo;
        }
        $response =  $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($arrayGenerated));
        return $response;
    }
    
    public function updateusuarioAction(){
        $config = $this->getServiceLocator()->get('config');
        $nombre            = 'Rodolfo';
        $apellidos         = 'juarez maximino';
        $correo            = 'rjuarez4updated@grupoeducare.com';
        $pass              = sha1('123456') ;
        $bloqueada         = 1;
        $timeBloqueo       = '0000-00-00 00:00:00';
        $fecharegistro     = '2016-08-08 00:00:00';
        $fechaActualizada  = '2014-01-01 00:00:00';
        $demo              = 1;
        $accept_conditions = 1;
        $providerId        = 'provider id';
        $provider        = 'provider';
        $estatus           = '1';
        $idPerfilUsuario   = 2;    //perfil de profesor
//        $idPerfilUsuario   = 1;  //perfil de alumno
        $_strLlave         = 'ABCDEF';
        $_diasLicencia     = 30;
        $_idUsuario        = 42616;
        $id_paquete         = 1;
        $modelAcciones = $this->getServiceLocator()->get('Grupos\Model\Acciones');
        if($config['constantes']['ID_PERFIL_PROFESOR'] === $idPerfilUsuario){//modifica profesor
            $modificaUsuario = $modelAcciones->updateUsuario($_idUsuario, $nombre,$apellidos,$correo,$pass,$bloqueada,$timeBloqueo,$fecharegistro,$fechaActualizada,$demo,$accept_conditions,$providerId,$provider,$estatus);
            $actualizarLicencia = $modelAcciones->updateLicencia($id_paquete,$idPerfilUsuario, $_strLlave, $_diasLicencia);
        }elseif($config['constantes']['ID_PERFIL_ALUMNO'] === $idPerfilUsuario){//modifica alumno
            $modificaUsuario = $modelAcciones->updateUsuario($_idUsuario, $nombre,$apellidos,$correo,$pass,$bloqueada,$timeBloqueo,$fecharegistro,$fechaActualizada,$demo,$accept_conditions,$providerId,$provider,$estatus);
        }
        $reponse = $this->getResponse();
        $reponse->setContent(\Zend\Json\Json::encode($modificaUsuario));
        return $reponse;
    }
    
}