<?php

namespace Acciones\Model;

use Zend\Db\Sql\Sql;
use Zend\Log\Logger;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\Pdo\Connection;
use Zend\Db\ResultSet\ResultSet;


class Acciones{
    
    /**
     *
     * @var Sql $sql 
     */
    protected $sql;
    
    /**
     *
     * @var Adapter $adapter
     */
    protected $adapter;
    
    /**
     *
     * @var Logger $_obLogger
     */
    protected $_objLogger;
    
    /**
     *
     * @var Logger $log
     */
    protected $log;
    
    /**
     *
     * @var Connection $connection
     */
    protected $connection;
    protected $config;
    
    public function __construct(Adapter $adapter, $config) {
        $this->adapter = $adapter;
        $this->config = $config;
        $this->sql        = new Sql($adapter);
        $this->_objLogger = new Debug($config);
        $this->conection  = $this->adapter->getDriver()->getConnection();
        $this->log        = new Logger();
    }
    
    public function registrarusuario($nombre, $apellido, $correo, $contrasena, $bloqueado, $timeBloqueado, $fecha_registro, $fecha_actualiza, $demo, $aceptacondiciones, $provider_id, $provider, $estatus, $idPerfil){
        $this->conection->beginTransaction();
        try {
            $idUsuario = $this->insertarusuario($nombre, $apellido, $correo, $contrasena, $bloqueado, $timeBloqueado, $fecha_registro, $fecha_actualiza, $demo, $aceptacondiciones, $provider_id, $provider, $estatus);
            $idUsarioPerfil = $this->insertarPerfil($idUsuario, $idPerfil);
            $this->conection->commit();
        } catch (InvalidadQueryException $e) {
            $this->conection->rollback();
        }
        return $idUsarioPerfil;
    }
    /**
     * Funcion para insertar un usuario recibe un objeto de tipo Usuario
     * 
     * @access public.
     * @author rjuarez@grupoeducare.com
     * @since 11/07/2016
     * 
     * @param $nombre            nombre de usuario
     * @param $apellido          apellidos del usuario
     * @param $correo            correo del usuario
     * @param $contrasena        contrasena del usuario
     * @param $bloqueado         indica si el usuario esta bloqueado
     * @param $timeBloqueado     tiempo de bloqueo
     * @param $fecha_registro    fecha de registro usuario
     * @param $fecha_actualiza   fecha de actualizacion
     * @param $demo              indica si es demo o no
     * @param $aceptacondiciones 
     * @param $provider_id
     * @param $provider
     * @param $estatus
     */
    public function insertarusuario($nombre,$apellido,$correo,$contrasena,$bloqueado,$timeBloqueado,$fecha_registro,$fecha_actualiza,$demo,$aceptacondiciones,$provider_id,$provider,$estatus){
        
        $_strQueryInsertGrupo = 'INSERT INTO  t01usuario (t01nombre,t01apellidos,t01correo,t01contrasena,t01bloqueada,t01tiempo_bloqueo,t01fecha_registro,t01fecha_actualiza,t01demo,t01acepto_condiciones,t01provider_id,t01provider,t01estatus) 
                                 VALUES(:_nombre, :_apellido, :_correo, :_contrasena, :_bloqueada, :_tiempoBloqueo, :_fechaRegistro, :_fechaActualiza, :_demo, :_aceptaCondiciones, :_providerId, :_provider, :_estatus)';
        $statement = $this->adapter->query($_strQueryInsertGrupo);
        $results = $statement->execute(array(
            ':_nombre' => $nombre,
            ':_apellido' => $apellido,
            ':_correo' => $correo,
            ':_contrasena' => $contrasena,
            ':_bloqueada' => $bloqueado,
            ':_tiempoBloqueo' => $timeBloqueado,
            ':_fechaRegistro' => $fecha_registro,
            ':_fechaActualiza' => $fecha_actualiza,
            ':_demo' => $demo,
            ':_aceptaCondiciones' => $aceptacondiciones,
            ':_providerId' => $provider_id,
            ':_provider' => $provider,
            ':_estatus' => $estatus
        ));
        $_idUsuario = $results->getGeneratedValue();
        //$this->_objLogger->debug("id grupo generado ---> " . $_idUsuario);
        return $_idUsuario;
    }
    /**
     * Funcion para insertar el perfil de un alumno se agrega el id usuario
     * para asociarle un perifl de alumno.
     * 
     * @access public.
     * @author rjuarez@grupoeducare.com
     * @since 11/07/2016
     * @param int $idUsuario
     * @param int $idPerfil
     * @return int $idUsuarioPerfil.
     */
    public function insertarPerfil($idUsuario, $idPerfil){
        $_strQueryInsertaPerfil =   'INSERT INTO t02usuario_perfil(c01id_perfil,t01id_usuario)
                                    VALUES (:_idPerfil, :_idUsuario)';
        $statement = $this->adapter->query($_strQueryInsertaPerfil);
        $results = $statement->execute(array(
            ':_idPerfil' => $idPerfil,
            '_idUsuario' => $idUsuario
        ));
        $idUsuarioPerfil = $results->getGeneratedValue();
        return $idUsuarioPerfil;
    }    
    
    /**
     * 
     * @param $idUsuarioPerfil  perfil del usuario.
     * @param $_intIdExamen     id del examen.
     * @param $_strLlave        licencia de usuario
     * @param $_intDias         dias de duracion licencia
     * @return type             id del examen usuario
     */
    public function insertarExamenesUsuario($idUsuarioPerfil, $_intIdExamen, $_strLlave, $_intDias){
        $insert = $this->sql->insert();
        $insert->into('t03licencia_usuario');
        $insert->columns(array(
            't21id_paquete',
            't02id_usuario_perfil',
            't03licencia',
            't03fecha_activacion',
            't03dias_duracion',
            't03estatus'));
        $insert->values(array(
            't21id_paquete' =>  1 ,//hardcodeo el id del paquete para que siempre se inserte un generico, hasta decidamos utilizarlo
            't02id_usuario_perfil'=> $idUsuarioPerfil,
            't03licencia'         => $_strLlave,
            't03fecha_activacion' => date("Y-m-d H:i:s"),
            't03dias_duracion'    => $_intDias,
            't03estatus'          => 1
        ));
        $statement = $this->sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        $id_LicenciaUsuario  = $this->conection->getLastGeneratedValue();
        
        $ins  = $this->sql->insert();
        $ins->into('t04examen_usuario');
        $ins->columns(array(
            't12id_examen',
            't03id_licencia_usuario',
            't04estatus'));
        $ins->values(array(
            't12id_examen'          => $_intIdExamen,
            't03id_licencia_usuario'=> $id_LicenciaUsuario,
            't04estatus'            => 'NOPRESENTADO'
        ));
        $statement2 = $this->sql->prepareStatementForSqlObject($ins);
        $statement2->execute();
        
        $idExamenUsuario = $this->conection->getLastGeneratedValue();
        return $idExamenUsuario;
    }
    
    /**
     * funcion que se encarga de ligar un usuario al grupo del cual ingreso el codigo en el registro
     * @param $_intIdUsuarioPerfil  perfil del usuario
     * @param $_strCodigoGrupo      String codigo de grupo
     */
    public function ligarUsuarioGrupo($_intIdUsuarioPerfil, $_strCodigoGrupo) {
        $_idGrupo = $this->obtenerIdGrupo($_strCodigoGrupo);
        $insert = $this->sql->insert();
        $insert->into('t08administrador_grupo');
        $insert->columns(array(
            't02id_usuario_perfil',
            't05id_grupo',
            't08usuario_ligado',
            't08fecha_registro',
            't08fecha_actualiza'));
        $insert->values(array(
            't02id_usuario_perfil' => $_intIdUsuarioPerfil,
            't05id_grupo' => $_idGrupo,
            't08usuario_ligado' => 1,
            't08fecha_registro' => date("Y-m-d H:i:s"),
            't08fecha_actualiza' => date("Y-m-d H:i:s")
        ));

        $statement = $this->sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        $idExamenUsuario = $this->conection->getLastGeneratedValue();
        return $idExamenUsuario;
    }

    /**
     * Funcion encargada de obtener el id del Grupo, este servira para
     * almacenarlo en la tabala t08
     * @param  $_strCodigoGrupo codigfo de grupo.
     * @return id de grupo.
     */
    protected function obtenerIdGrupo($_strCodigoGrupo) {
        $select = $this->sql->select()
                ->from(array('t07' => 't07codigo'), array('t05id_grupo'))
                ->where(array('t07codigo' => $_strCodigoGrupo));
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultSet = new ResultSet();
        $respGrupo = $resultSet->initialize($result)->toArray();
        $_idGrupo = $respGrupo[0]['t05id_grupo'];
        return $_idGrupo;
    }
    
    /**
     * funcion que se encarga de insertar los registros nesesarios para que 
     * el usuario pueda entrar a contestar examenes, guarda la informacion 
     * de la licencia, ademas de los examenes ligados al paquete correspondiente.
     * 
     * @param type $idUsuarioPerfil
     * @param type $_strLlave
     * @param type $_intDias
     * @return type id_LicenciaUsuario;
     */
    public function insertarLicenciaProfesor($idUsuarioPerfil, $_strLlave, $_intDias) {

        $insert = $this->sql->insert();
        $insert->into('t03licencia_usuario');
        $insert->columns(array(
            't21id_paquete',
            't02id_usuario_perfil',
            't03licencia',
            't03fecha_activacion',
            't03dias_duracion',
            't03estatus'));
        $insert->values(array(
            't21id_paquete' => 1, // Hardcodeo el id del paquete para que siempre se inserte uno generico, hasta que decidamos utilizarlo
            't02id_usuario_perfil' => $idUsuarioPerfil,
            't03licencia' => $_strLlave,
            't03fecha_activacion' => date("Y-m-d H:i:s"),
            't03dias_duracion' => $_intDias,
            't03estatus' => 1
        ));
        $statement = $this->sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        $id_LicenciaUsuario = $this->conection->getLastGeneratedValue();

        return $id_LicenciaUsuario;
    }
    
    //***********MODIFICAR USUARIO*******************************//
    public function updateUsuario($idUsuarioPerfil, $nombre,$apellido,$correo,$contrasena,$bloqueado,$timeBloqueado,$fecha_registro,$fecha_actualiza,$demo,$aceptacondiciones,$provider_id,$provider,$estatus){
        $_strQueryUpdate = 'UPDATE t01usuario set 
                            t01nombre             = :_strNombre, 
                            t01apellidos          = :_strApellidos, 
                            t01correo             = :_strCorreo, 
                            t01contrasena         = :_strContrasena ,  
                            t01bloqueada          = :_bloqueda      ,
                            t01tiempo_bloqueo     = :_timebloqueo   ,
                            t01fecha_registro     = :_fecha_registro,
                            t01fecha_actualiza    = :_fechaactualiza,
                            t01demo               = :_demo          ,
                            t01acepto_condiciones = :_aceptoCondiciones,
                            t01provider_id        = :_providerId,
                            t01provider           = :_provider,
                            t01estatus            = :_estatus
                        where t01id_usuario = :_int_idUsuario';
        $statement  = $this->adapter->query($_strQueryUpdate);
        try {
            $results = $statement->execute(array(
                ":_int_idUsuario"  => $idUsuarioPerfil,
                ":_strNombre"      => $nombre,
                ":_strApellidos"   => $apellido,
                ":_strCorreo"      => $correo,
                ":_strContrasena"   => $contrasena,
                ":_bloqueda"        => $bloqueado,
                ":_timebloqueo"     =>$timeBloqueado,
                ":_fecha_registro"  =>$fecha_registro,
                ":_fechaactualiza"  =>$fecha_actualiza,
                ":_demo"            =>$demo,
                ":_aceptoCondiciones"=>$aceptacondiciones,
                ":_providerId"       =>$provider_id,
                ":_provider"         =>$provider,
                ":_estatus"         =>$estatus
                
            ));
        } catch (Exception $ex) {
            return false;
        }
        if($results->getAffectedRows()> 0){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 
     * @param type $id_paquete
     * @param type $idUsuarioPerfil
     * @param type $_strLlave
     * @param type $_intDias
     * @return boolean
     */
    public function updateLicencia($id_paquete,$idUsuarioPerfil, $_strLlave, $_intDias){
        $boolOk;
        $strQueryUpdate = 'UPDATE t03licencia_usuario set
                             t21id_paquete          = :_idPaquete,
                             t02id_usuarioperfil    = :_usuarioPerfil,
                             t03licencia            = :_licencia,
                             t03fecha_activacion    = :_fechaActivacion,
                             t03dias_duracion       = :_diasDuracion,
                             t03estatus=            :_activo';
        $statement  = $this->adapter->query($strQueryUpdate);
        try {
            $results = $statement->execute(array(
                ":_idPaquete"       => $id_paquete,
                ":_usuarioPerfil"   => $idUsuarioPerfil,
                ":_licencia"        => $_strLlave,
                ":_fechaActivacion" => $_intDias,
            ));
        } catch (Exception $ex) {
            return false;
        }
        if($results->getAffectedRows() > 0 ){
            $boolOk = true;
            return $boolOk;
        }else{
            $boolOk = false;
            return $boolOk;
        }
        return $boolOk;
    }
}