<?php 
return array(
	'controllers' => array(
		'invokables' => array(
			'Misdatos\Controller\Misdatos'  => 'Misdatos\Controller\MisdatosController',
		),
	),
	
	'router' => array(
		'routes' => array(
			'misdatos' => array(
				'type' =>  'Segment',
				'options' => array(
					'route'    => '/misdatos[/[:action]]',
					'constraints' => array(
							'action'  =>  '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Misdatos\Controller\Misdatos',
						'action'     => 'index',
					),
				),
			),
		),
	),
	
	'view_manager' => array(
		'display_not_found_reason' => true,
		'display_exceptions'       => true,
		'doctype'                  => 'HTML5',
		'not_found_template'       => 'error/404',
		'exception_template'       => 'error/index',
		'template_map' => array(
			'misdatos/misdatos/index' => __DIR__ . '/../view/misdatos/index.phtml',
		),
		'template_path_stack' => array(
			'misdatos' => __DIR__ . '/../view',
		),
	),
);
