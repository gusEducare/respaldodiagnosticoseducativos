<?php
namespace Misdatos\Controller;


//use Application\Model\Sesion;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Misdatos\Model\MisdatosModel;
use Zend\Session\Container;
use Misdatos\Model\WsCliente;
use Login\Model\RegistroModel;

//Librerias log
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

/**
 * 
 * @author oreyes@grupoeducare.com
 * @since 13/10/2014
 *
 */
class MisdatosController extends AbstractActionController {
	
	/**
	 * Para los mensajes log.
	 * @var Logger $log : instancia de la clase Logger
	 */
	protected $log;
	
	/**
	 * Para instanciar modelo de misdatos
	 * @var MisdatosModel
	 */
	protected $misdatosModel;
	
	/**
	 * Para instanciar y manejar las variables de session.
	 * @var Sesion sesion;
	 */
	protected $sesion;
	
	/**
	 * Constructor de la clase instanciar el log e inicializarlo
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 13/10/2014
	 * 
	 */
        
        /**
         * Instancia del Cliente de WebService SOAP del PortalGE
         * @var type 
         */
        protected $wsCliente;
        
	public function __construct()
	{
		$this->log = New Logger();
		$this->log->addWriter(New Stream('php://stderr'));
		//$this->sesion = new Sesion();
                $this->datos_sesion = new Container('user');
                
	}
	
	/**
	 * @author oreyes@grupoeducare.com
	 * @since 13/10/2014
	 */
	public function indexAction()
	{
		$config       = $this->getServiceLocator()->get("config");
		
		$_correo      = $this->datos_sesion->correo;
		$_intIdUSPC   = $this->datos_sesion->idUsuarioPerfil;
		$id_perfil    = $this->datos_sesion->perfil;		
		$_arrDatosUsr = $this->getinstanceModel()->buscarCorreo( $_correo );
		
		if($id_perfil == $config['constantes']['ID_PERFIL_ALUMNO'])
		{
			$_arrGrupos = $this->getinstanceModel()->getGruposActivos($_intIdUSPC);
                        $_arrUsuarioExamen = $this->getinstanceModel()->getLicenciasActivas($_intIdUSPC);
			return new ViewModel(array(
					"_arrDatosUsr"	=>$_arrDatosUsr,
					"id_perfil" 	=>$id_perfil,
					"config"		=>$config ,
					"_arrGrupos"	=>$_arrGrupos,
                                        "_arrUsuarioExamen"	=>$_arrUsuarioExamen,
					"_intIdUSPC"	=>$_intIdUSPC
					));
		} else if($id_perfil == $config['constantes']['ID_PERFIL_PROFESOR']) {
                    $_arrUsuarioExamen = $this->getinstanceModel()->getLicenciaProfesor($_intIdUSPC);
			return new ViewModel(array(
					"_arrDatosUsr"	=>$_arrDatosUsr,
					"id_perfil" 	=>$id_perfil,
                                        "_arrUsuarioExamen"	=>$_arrUsuarioExamen,
					"config"		=>$config ));
		} else {
			return new ViewModel(array(
					"_arrDatosUsr"	=>$_arrDatosUsr,
					"id_perfil" 	=>$id_perfil,
					"config"		=>$config ));
		}
	}
	
	/**
	 * Misdatos actualizar nombre y apellidos
	 * 
	 * @access public
	 * @author oreyes@grupoeducare.com
	 * @since 16/10/2014
	 *
	 */
	public function actualizarnombreAction()
	{       
                $response= $this->getResponse();
		$request 	= $this->getRequest();
		$_nombreNuevo 	= trim ($request->getPost("strNombre"));
		$_apellidoNuevo = trim ($request->getPost("strApellido"));
                //Datos actualies del usuario
                $_idUsuario	= $this->datos_sesion->idUsuario;
                $_nombre	= $this->datos_sesion->nombreUsuario;
                $_apellido	= $this->datos_sesion->apellidos;
                
		if($_nombre != $_nombreNuevo || $_apellido != $_apellidoNuevo){
                    $_boolResp = $this->getinstanceModel()->actualizarNombre($_idUsuario, $_nombreNuevo, $_apellidoNuevo);
                    if($_boolResp === true){
                        $this->datos_sesion->nombreUsuario = $_nombreNuevo;
                        $this->datos_sesion->apellidos = $_apellidoNuevo;
                        $_arr_datos = array('nombre' => $_nombreNuevo, 'apellido' => $_apellidoNuevo, 'mensaje' => 'Tus datos se han actualizado correctamente.');
                    }
                }else{
                    $_arr_datos = array('mensaje' => 'Tu información debe ser distinta de la actual.');
                }
                
                $response->setContent(\Zend\Json\Json::encode($_arr_datos));
                return $response;
	}
	
	
	
	/**
	 * Modificar el nombre de usuario o correo.
	 * 
	 * @access public
	 * @author oreyes@grupoeducare.com
	 * @since 16/10/2014
	 *
	 */
	public function actualizarusuarioAction(){
                $response= $this->getResponse();
		$request 	  = $this->getRequest();
                $_boolResp = false;
                $_idUsuario = $this->datos_sesion->idUsuario;
                $_usuario = $this->datos_sesion->correo;
		$_usuarioNuevo 	  = trim ($request->getPost("strUserName"));
		if($_usuario != $_usuarioNuevo){
                    $_arrResp 	  = $this->getinstanceModel()->buscarCorreo( $_usuario );
                    if(isset($_usuarioNuevo) && count($_arrResp) != 0){                            
                            $_boolResp = $this->getinstanceModel()->actualizarUsuario($_idUsuario, $_usuarioNuevo);
                            if($_boolResp === true){
                                    //actualizar variables de sesion
                                    $this->datos_sesion->correo = $_usuarioNuevo;
                                    $_arr_datos = array('update' => $_boolResp,'strUserName' => $_usuarioNuevo, 'mensaje' => 'Tu nombre de usuario ha sido actualizado.');
                            }
                    }
                }else{
                 $_arr_datos = array('update' => $_boolResp, 'mensaje' => 'Tu nuevo nombre de usuario debe ser distinto de la actual.');
                }
                $response->setContent(\Zend\Json\Json::encode($_arr_datos));
                        
                return $response;
	
	}
	
	/**
	 * Modificar contraseña desde seccion mis datos.
	 * 
	 * @access public
	 * @author oreyes@grupoeducare.com
	 * @since 10/11/2014
	 *
	 */
	public function actualizarContrasenaAction()
	{
		$request = $this->getRequest();
		$config  = $this->getServiceLocator()->get("config");
		
		$_strPassActual = trim ($request->getPost("_strPassActual"));
		$_strNewPass 	= trim ($request->getPost("_strNewPass"));
		$id_servicio 	= $config['constantes']['ID_SERVICIO_EVALUACIONES'];
	
		$_strNombreUsuario = $this->datos_sesion->correo;
		//verificar que el password actual sea correcto
		$_arrDatosUser     = $this->getinstanceModel()->buscarUsuarioContrasena( $_strNombreUsuario, $_strPassActual, $id_servicio);
	
		$translate = $this->getServiceLocator()->get('viewhelpermanager')->get('translate');
		
		//validar si la contrasena y el usuario son correctos.
		if(count($_arrDatosUser) > 0){
			$_intIdUser  = $this->datos_sesion->idUsuario;
			$_boolRespuesta = $this->getinstanceModel()->actualizarContrasena($_intIdUser, $_strNewPass);
			//validar si se actualiza la contrasena
			if($_boolRespuesta === true){
				$_arrResp = array(
						"success" => true,
						"msj" => $translate("actualizado")
				);
			}else{
				$_arrResp = array(
						"success" => false,
						"msj" => $translate("Ocurri&oacute; un error al intentar actualizar la informaci&oacute;n, por favor intenta m&aacute;s tarde.")
				);
			}
		}else{
			$_arrResp = array(
					"success" => false,
					"msj" => $translate("La contrase&ntilde;a es incorrecta")
			);
		}
	
		$response= $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_arrResp));
		return $response;
	}
	
	public function checkHijoAjaxAction(){
	
		$request 		= $this->getRequest()->getContent();
		$_strValor		= $_GET['value'];
		$_strField		= $_GET['field'];
			
		$_arrResp = $this->getinstanceModel()->existeCorreo( $_strValor );
		//obtener translate
		$translate = $this->getServiceLocator()->get('viewhelpermanager')->get('translate');
	
		if(count($_arrResp) >= 1){
			$_arrResp =  array(
					"value" => $_REQUEST["value"],
					"valid" => true,
					"message" => $translate("El nombre de usuario es correcto.")
			);
		}
		else{
			$_arrResp =  array(
					"value" => $_REQUEST["value"],
					"valid" => false,
					"message" => $translate("El nombre de usuario no existe.")
			);
		}
	
		$response= $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_arrResp));
		return $response;
	}
	
	public function checkGrupoAjaxAction(){
	
		$request 		= $this->getRequest()->getContent();
		$_strValor		= $_GET['value'];
		$_strField		= $_GET['field'];
	
	
	
		$_alumnosTable = $this->getServiceLocator()->get("Application\Model\AlumnosTable");
	
		$_boolCodigoValido = $_alumnosTable->consultaCodigoGrupo($_strValor);
	
	
		//obtener translate
		$translate = $this->getServiceLocator()->get('viewhelpermanager')->get('translate');
	
		if($_boolCodigoValido === true){
			$_arrResp =  array(
					"value" => $_REQUEST["value"],
					"valid" => true,
					"message" => $translate("El nombre de usuario es correcto.")
			);
		}
		else{
			$_arrResp =  array(
					"value" => $_REQUEST["value"],
					"valid" => false,
					"message" => $translate("El nombre de usuario no existe.")
			);
		}
	
		$response= $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_arrResp));
		return $response;
	
	}
	
	/**
	 * verificar si el nombre disponible
	 * @access public
	 * @author LAFC
	 *
	 */
	public function checkUserAjaxAction(){
		$request 		= $this->getRequest()->getContent();
		$_strValor		= $_GET['value'];
		$_strField		= $_GET['field'];
			
		$_arrResp = $this->getinstanceModel()->existeCorreo( $_strValor );
	
		//obtener translate
		$translate = $this->getServiceLocator()->get('viewhelpermanager')->get('translate');
	
		if(count($_arrResp) >= 1){
			$_arrResp =  array(
					"value" => $_REQUEST["value"],
					"valid" => false,
					"message" => $translate("El nombre de usuario no está disponible.")
			);
		}
		else{
			$_arrResp =  array(
					"value" => $_REQUEST["value"],
					"valid" => true,
					"message" => $translate("nombre de usuario disponible.")
			);
		}
	
		$response= $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_arrResp));
		return $response;
	}
	
	/**
	 * Obtener instancia de la clase MisdatosModel.
	 *
	 * @author oreyes@grupoeducare.com
	 * @since 13/10/2014
	 *
	 */
	public function getinstanceModel() 
	{
		if (!$this->misdatosModel) {
			$sm = $this->getServiceLocator();
			$this->misdatosModel = $sm->get('Misdatos\Model\MisdatosModel');
		}
		return $this->misdatosModel;
	}
        
        public function activaLicenciaAction() {
            $config       = $this->getServiceLocator()->get("config");
            $this->wsCliente = new WsCliente($config['constantes']['WSDL_PORTALGE']);
            $request = $this->getRequest();
            
            $_strNombre =  $this->datos_sesion->nombreUsuario.' '.$this->datos_sesion->apellidos;
            $_strNombreUsuario =  $this->datos_sesion->correo;
            $_intIdServicio	= $config['constantes']['ID_SERVICIO_EVALUACIONES'];
            $_intIdUsuarioPerfil = intval($this->datos_sesion->idUsuarioPerfil);
            $_strLlave = strtoupper($request->getPost('strCodigoLicencia'));
            
            $_validaLicencia = $this->wsCliente->validarlicencia($_strLlave, $_intIdServicio);
            if($_validaLicencia['valida'] == true){
               
               $_fechaVigencia = $this->wsCliente->activaLicenciaUsuario($_strNombre, $_strNombreUsuario, $_intIdServicio, $_intIdUsuarioPerfil, $_strLlave);

                $_arrVigencia = explode('_',$_fechaVigencia);
                $_intDias =  $_arrVigencia[0];
                $_intIdExamen = $_validaLicencia['id_paquete'];

                $regModel 	= $this->getRegistroModel();
                $idExamenUsuario = $regModel->insertarExamenesUsuario($_intIdUsuarioPerfil, $_intIdExamen, $_strLlave, $_intDias);
                $_bolLiga = true; 
                
            }else{
                $_bolLiga = false; 
            }
            
            $response= $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolLiga));
            return $response;
        }
        
        /**
        * Traer una instancia del modelo RegistroModel.php
        * 
        * @access public.
        * @author oreyes
        * @since 22/12/2014
        * 
        * @return RegistroModel $regModel.
        */
        public function getRegistroModel()
        {
            $sm = $this->getServiceLocator();
            $regModel = $sm->get('/Login/Model/RegistroModel');
            return $regModel;
        }
        
        /**
         * Controlador para activar las licencias para el perfil PROFESOR.
         * @return type
         */
        public function activaLicenciaProfesorAction(){
            $config = $this->getServiceLocator()->get('config');
            $this->wsCliente = new WsCliente($config['constantes']['WSDL_PORTALGE']);
            $request        = $this->getRequest();
            $_strLicencia   =  $request->getPost('strCodigoLicencia');
            $_intIdServicio = $config['constantes']['ID_SERVICIO_EVALUACIONES'];
            $_strNombre     =  $this->datos_sesion->nombreUsuario.' '.$this->datos_sesion->apellidos;
            $_strCorreo     =  $this->datos_sesion->correo;
            $_intIdUsuarioPerfil = intval($this->datos_sesion->idUsuarioPerfil);
            
            $boolValidaLicencia = $this->wsCliente->validarlicencia($_strLicencia, $_intIdServicio);//Valido Licencia
            
            if($boolValidaLicencia['valida'] == true){
                $_fechaVigencia = $this->wsCliente->activaLicenciaUsuario($_strNombre, $_strCorreo, $_intIdServicio, $_intIdUsuarioPerfil, $_strLicencia);
                $_arrVigencia = explode('_', $_fechaVigencia);
                $_intDias = $_arrVigencia[0];
                $regModel 	= $this->getRegistroModel();
                $_intIdLicenciaProfesor = $regModel->insertarLicenciaProfesor($_intIdUsuarioPerfil, $_strLicencia, $_intDias);
                $respuesta = true;
            }else{
                $respuesta = false;
            }
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($respuesta));
            return $response;
        }    
}