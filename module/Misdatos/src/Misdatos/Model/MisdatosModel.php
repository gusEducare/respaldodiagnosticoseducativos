<?php
namespace Misdatos\Model;

//objetos para el log de errores
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;

//objetos para consultas sql.
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Exception\InvalidQueryException;

/**
 * Modelo de modulo misdatos.
 * 
 * @author oreyes@grupoeducare.com
 * @since 12/11/2014
 * @copyright Todos los derechos reservados para Grupo Educare SA de CV
 *
 */
class MisdatosModel {
	
	/**
	 * Para habilitar la conexion con bd y ejecutar consultas sql.
	 * @var Adapter $adapter : 
	 */
	protected  $adapter;
	
	/**
	 * Para generar objetos SQL.
	 * @var Sql $sql :
	 */
	protected  $sql;
	
	/**
	 * Para el log de errores de apache.
	 * @var Logger $log : 
	 */
	protected  $log;
	
        protected $config;
	/**
	 * Constructor de la clase.
	 * 
	 * @access public
	 * @author oreyes@grupoeducare.com
	 * @since 14/10/2014
	 * @param Adapter $_adapter
	 */
	public function __construct(Adapter $_adapter,$config){
		$this->adapter = $_adapter;
		$this->sql 	   = new Sql($this->adapter);
		$this->config = $config;
		$this->log = new Logger();
		$this->log->addWriter(new Stream('php://stderr'));
	}
	
	/**
	 * Obtiene los datos del usuario.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 14/10/2014
	 * @param string $usuario
	 * @return array{
	 * 				t02id_usuario_servicio,
	 * 				t01id_usuario,
	 * 				t01nombre,
	 * 				t01apellidos,
	 * 				t01estatus}
	 */
	function buscarCorreo( $_correo )
	{
		$datos = Array();
		$_select = $this->sql->select();
		$_select->from(
					Array('t01'=>'t01usuario'))
			   ->join(
			   		Array('t02'=>'t02usuario_perfil'),
			   		        't01.t01id_usuario = t02.t01id_usuario',
			   		Array())
			   	->where('t01.t01correo ='."'".$_correo."'")
			   	->columns(
			   		Array(
			   			't01id_usuario',
						't01nombre',
						't01apellidos',
						't01estatus',
						't01contrasena',
						't01correo'));

		$_stmt   = $this->sql->prepareStatementForSqlObject($_select);
		$_result = $_stmt->execute();
		$_rsSet  = new ResultSet;
		$_rsSet->initialize($_result);
		
		$datos = $_rsSet->toArray();
		return $datos;
	}
	
	/**
	 * Esto funcion verifica que el usuario y la contrasena existan
	 *  y sean las correctas.
	 *
	 * @access public
	 * @author oreyes@grupoeducare.com
	 * @since 10/11/2014
	 * @param  string _strCorreo
	 * @param  string _strPassword
	 * @param  int _id_servicio
	 *
	 * @return array _arrRespuesta : arreglo de datos del usuario
	 * 
	 * */
	function buscarUsuarioContrasena( $_strUsuario, $_strPassword, $_id_servicio)
	{
		$_arrRespuesta = array();
			
		$select = $this->sql->select();
		$select->from(array('t01'=>'t01usuario'))
				->where('t01.t01correo ='. "'".$_strUsuario."'")
				->where('t01.t01contrasena ='. "'".sha1($_strPassword)."'")
				->columns(array(	't01id_usuario',
								't01correo',
								't01nombre',
								't01apellidos',
								't01estatus'));
	
		$statement = $this->sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		$_arrRespuesta = $resultSet->toArray();

		return $_arrRespuesta;
	
	}
	
	/**
	 * Obtener los grupos a los que pertenece el alumno
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 14/10/2014
	 * @param $_intIdUSPC el id_usuario_servicio_perfil_colegio del alumno
	 * @return $_arrGrupos un array grupos
	 */
	public function getGruposActivos( $_intIdUSPC ){
                $_strQueryGruposUsuario= 'SELECT 
                                                t05.t05id_grupo,
                                                t05.t05descripcion AS t05descripcion,
                                                profesor.t01correo AS profesor,
                                                t05.t05ciclo AS t05ciclo,
                                                t07.t07codigo AS t07codigo,
                                                t08.t02id_usuario_perfil AS t02id_usuario_perfil,
                                                t02 . *
                                            FROM
                                                t05grupo AS t05
                                                    INNER JOIN
                                                t07codigo AS t07 ON t05.t05id_grupo = t07.t05id_grupo
                                                    INNER JOIN
                                                t08administrador_grupo AS t08 ON t05.t05id_grupo = t08.t05id_grupo
                                                    INNER JOIN
                                                t02usuario_perfil AS t02 ON t08.t02id_usuario_perfil = t02.t02id_usuario_perfil
                                                    INNER JOIN
                                                t01usuario AS t01 ON t01.t01id_usuario = t02.t01id_usuario
                                                    INNER JOIN
                                                (select t05.t05id_grupo, t01.t01correo 
                                                from t01usuario t01
                                                join t02usuario_perfil t02 on (t01.t01id_usuario = t02.t01id_usuario )
                                                join t08administrador_grupo t08 on (t02.t02id_usuario_perfil = t08.t02id_usuario_perfil)
                                                join t05grupo t05 on (t05.t05id_grupo=t08.t05id_grupo)
                                                where c01id_perfil = 2) as profesor on(t05.t05id_grupo = profesor.t05id_grupo)
                                            WHERE
                                                t08.t08usuario_ligado = "LIGADO" AND t07.t07estatus = 1 AND t08.t02id_usuario_perfil = :_intIdUSPC;';


            $statement = $this->adapter->query($_strQueryGruposUsuario);
            $results = $statement->execute(array(':_intIdUSPC' => $_intIdUSPC,));

            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $arrGruposUsuario = $resultSet->toArray();
            
            return $arrGruposUsuario;
	}
	
	/**
	 * 
	 * Actualizar nombre completo de cualquier usuario registrado.
	 * @author oreyes@grupoeducare.com
	 * @since 14/10/2014
	 * 
	 * @param Int $_idUsuario
	 * @param String $_nombre
	 * @param String $_apellido
	 * 
	 * @return boolean $_result
	 */
	public function actualizarNombre($_idUsuario, $_nombre, $_apellido)
	{
		$_success = false;
		$_update = $this->sql->update();
		$_update->table('t01usuario')
				->set(array(
						't01nombre'	  =>$_nombre,
						't01apellidos' =>$_apellido))
				->where(array(
						't01id_usuario'=>$_idUsuario));
		try{
			$_stmt 	 	  = $this->sql->prepareStatementForSqlObject($_update);
			$_result 	  = $_stmt->execute();
			$_rowAffected = $_result->getAffectedRows(); 
			
			if($_rowAffected > 0){
				$_success = true;
			}

		}catch (Exception $e){
			$this->log->debug($e->getMessage());
			$_success = false;
		
		}catch (Zend_Exception $e){
			$this->log->debug($e->getMessage());
			$_success = false;
		}
		return $_success;
	}
	
	/**
	 * Actualizar nombre de usuario.
	 * 
	 * @param int $_idUsuario
	 * @param String $_usuario
	 */
	public function actualizarUsuario($_idUsuario, $_usuario)
	{
		$_success = false;
		$_update = $this->sql->update();
		$_update->table('t01usuario')
				->set(array(
						't01correo'	   => $_usuario))
				->where(array(
						't01id_usuario' => $_idUsuario));
		try {
			$_stmt   	= $this->sql->prepareStatementForSqlObject($_update);
			$_result 	= $_stmt->execute();
			$_rowAffect	= $_result->getAffectedRows();
			if($_rowAffect > 0){
				return true;
			}
		} catch (Zend_Exception $e) {
			$this->log->debug($e->getMessage());
			$_success = false;
		}
	  	return $_success;
	}
	
	/**
	 * Actualizar la contraseña de un usuario.
	 * 
	 * @since 11/11/2014
	 * @author oreyes@grupoeducare.com
	 * 
	 * @param int $_intIdUser : clave para modificar registro.
	 * @param string $_strNewPass : cadena con la nueva contrasena.
	 * @return boolean $success.
	 */
	function actualizarContrasena($_intIdUser, $_strNewPass) 
	{
		$success = false; 
		$update  = $this->sql->update();
		$update ->table('t01usuario')
			    ->set(array(
			    		't01contrasena'=>sha1($_strNewPass)))
			    ->where(array(
			    		't01id_usuario'=>$_intIdUser));
		try {
			$stmt      = $this->sql->prepareStatementForSqlObject($update);
			$result    = $stmt->execute();
			$rowAffect = $result->getAffectedRows();
			
			if($rowAffect > 0){
				$success = true; 
			}
		} catch(Zend_Exception $e) {
			$this->log->debug($e->getMessage());
			$success = false;
		}
		return $success;
	}
        
        function getLicenciasActivas($_intIdUSPC) { 
            #--- Exámenes por usuario
            $_strQueryExamenUsuario = 'SELECT 
                                        t04.t12id_examen,
                                        t12.t12nombre,
                                        t12.t12num_intentos,
                                        t03.t03estatus,
                                        t04.t03id_licencia_usuario,
                                        t03.t03licencia,
                                        CONCAT(DATE_FORMAT(t03.t03fecha_activacion,"%d/%m/%Y")," - ",DATE_FORMAT(DATE_ADD(t03.t03fecha_activacion, INTERVAL t03.t03dias_duracion DAY),"%d/%m/%Y")) as periodo_vigencia
                                        FROM t03licencia_usuario t03
                                        join t04examen_usuario t04 on t03.t03id_licencia_usuario = t04.t03id_licencia_usuario
                                        join t12examen t12 on t04.t12id_examen = t12.t12id_examen
                                        where t03.t02id_usuario_perfil = :_intIdUSPC
                                        and t03.t03estatus <> "INACTIVA"
                                        group by t04.t12id_examen,t04.t03id_licencia_usuario
                                        order by periodo_vigencia asc, t03.t03id_licencia_usuario asc, t04.t12id_examen asc';


            $statement = $this->adapter->query($_strQueryExamenUsuario);
            $results = $statement->execute(array(':_intIdUSPC' => $_intIdUSPC,));

            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $arrUsuarioExamen = $resultSet->toArray();   
            
            #----Intentos por examen
            $p = 0;
            foreach ($arrUsuarioExamen as $ap){
                $_strQueryIntentosExamen = 'SELECT 
                                            t04.t12id_examen,
                                            t04.t03id_licencia_usuario,
                                            t03.t02id_usuario_perfil,
                                            t04.t04num_intento
                                            from t04examen_usuario t04
                                            join t03licencia_usuario t03 on t04.t03id_licencia_usuario = t03.t03id_licencia_usuario
                                            where t04.t03id_licencia_usuario = :_intIdLicUsr
                                            and t04.t12id_examen = :_intIdExamen
                                            order by t04.t04num_intento desc
                                            limit 1;';
                
                $statement = $this->adapter->query($_strQueryIntentosExamen);
                $results = $statement->execute(array(':_intIdLicUsr' => $ap['t03id_licencia_usuario'],':_intIdExamen' => $ap['t12id_examen']));

                $resultSet = new ResultSet;
                $resultSet->initialize($results);
                $arrIntentosEx = $resultSet->toArray(); 
                $arrUsuarioExamen[$p]['num_intentos'] = $arrIntentosEx[0]['t04num_intento'];
                
                $p++;
            }
            #=========================================================
            #--- Paquetes por usuario
            $_strQueryPaquetesActivos = 'SELECT 
                                        distinct(t03.t21id_paquete),
                                        t21.t21descripcion_paquete
                                        FROM t03licencia_usuario t03
                                        inner join t21paquete t21 on t03.t21id_paquete = t21.t21id_paquete
                                        where t03.t02id_usuario_perfil = :_intIdUSPC 
                                        and t03.t03estatus = "DISPONIBLE"
                                        order by t03fecha_activacion desc;';


            $statement = $this->adapter->query($_strQueryPaquetesActivos);
            $results = $statement->execute(array(':_intIdUSPC' => $_intIdUSPC,));

            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $arrPaqLicencia = $resultSet->toArray(); 

            #----Licencias por paquete
            $p = 0;
            $k = 0;
            foreach ($arrPaqLicencia as $ap){
                $_strQueryLicenciaActiva = 'SELECT
                                            distinct(t03.t03licencia),
                                            CONCAT(DATE_FORMAT(t03fecha_activacion,"%d/%m/%Y")," - ",DATE_FORMAT(DATE_ADD(t03fecha_activacion, INTERVAL 365 DAY),"%d/%m/%Y")) as periodo_vigencia
                                            FROM t03licencia_usuario t03
                                            where t03.t02id_usuario_perfil = :_intIdUSPC 
                                            and t03.t21id_paquete = :_intIdPaq
                                            and t03.t03estatus = "DISPONIBLE"
                                            order by t03fecha_activacion desc;';


                $statement = $this->adapter->query($_strQueryLicenciaActiva);
                $results = $statement->execute(array(':_intIdUSPC' => $_intIdUSPC,':_intIdPaq' => $ap['t21id_paquete']));

                $resultSet = new ResultSet;
                $resultSet->initialize($results);
                $arrLicenciaActiva = $resultSet->toArray(); 
                $arrPaqLicencia[$p]['licencia'] = $arrLicenciaActiva;

                #---Exámenes por licencia
                foreach ($arrLicenciaActiva as $ap){

                    $_strQueryExamen = 'SELECT
                                        t12.t12nombre
                                        FROM t03licencia_usuario t03
                                        inner join t22examen_paquete t22 on t03.t21id_paquete = t22.t21id_paquete
                                        inner join t12examen t12 on t22.t12id_examen = t12.t12id_examen 
                                        where t03.t03licencia = :_strLlave
                                        and t03.t03estatus = "DISPONIBLE"
                                        order by t03fecha_activacion desc;';


                    $statement = $this->adapter->query($_strQueryExamen);
                    $results = $statement->execute(array(':_strLlave' => $ap['t03licencia']));

                    $resultSet = new ResultSet;
                    $resultSet->initialize($results);
                    $arrExamen = $resultSet->toArray(); 

                    $arrPaqLicencia[$p]['licencia'][$k]['examen'] = $arrExamen;

                    $k++;
                }
                $k = 0;
                $p++;
            }
                
                return $arrUsuarioExamen;
        }
        /**
         * Regresa licencia y vigencia del profesor
         * @param type $_intIdUSPC
         */
        public function getLicenciaProfesor($_intIdUSPC) {
            #--- Exámenes por usuario
            $_strQueryExamenUsuario = 'SELECT 
            t03.t03estatus,
            t03.t03licencia,
            CONCAT(DATE_FORMAT(t03.t03fecha_activacion,"%d/%m/%Y")," - ",DATE_FORMAT(DATE_ADD(t03.t03fecha_activacion, INTERVAL t03.t03dias_duracion DAY),"%d/%m/%Y")) as periodo_vigencia
            FROM t03licencia_usuario t03
            where t03.t02id_usuario_perfil = :_intIdUSPC
            and t03.t03estatus <> "INACTIVA"
            order by periodo_vigencia asc, t03.t03id_licencia_usuario asc';


            $statement = $this->adapter->query($_strQueryExamenUsuario);
            $results = $statement->execute(array(':_intIdUSPC' => $_intIdUSPC,));

            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $arrUsuarioExamen = $resultSet->toArray();
            
            return $arrUsuarioExamen;

        }
} 