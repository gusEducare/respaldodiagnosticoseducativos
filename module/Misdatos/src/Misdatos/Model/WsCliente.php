<?php
namespace Misdatos\Model;

use Zend\Soap\Client;

/**
 * @since 11/02/2015
 * @author oreyes
 * @copyright Grupoeducare SA de CV
 */
class WsCliente extends Client
{
	/**
	 * Constructor de la clase inicializa los valores de wsdl y options
	 * que construyen al cliente para saber a donde tiene que hacer las operaciones.
	 * 
	 * @access public
	 * @since 11/02/2015
	 * @param String $wsdl
	 * @param String $options
	 * 
	 */
	public function __construct($wsdl, $options = array() )
	{
		$this->wsdl = $wsdl;
		$this->options = $options;
	}
	
	/**
	 * Funcion para validar las licencias de examenes consume una operacion 
         * de un servicio web.
	 *
	 * @access public
	 * @author oreyes
	 * @since 11/02/2015
	 * @param String $_strLicencia
	 * @param int $_intIdServicio
	 * @return mixed $result
	 */
	public function validarlicencia( $_strLicencia, $_intIdServicio )
	{
		$cliente = new Client($this->wsdl,$this->options);
		$result  = $cliente->llaveValidaMensaje($_strLicencia, $_intIdServicio);
		
		return $result;
	}

        /**
         * Funcion que activa una licencia nueva para el usuario
         * por medio de un servicio web
         * @param Usuario $loguinUsuario
         * @return type
         */
        public function activaLicenciaUsuario( $_strNombre, $_strNombreUsuario, $_intIdServicio, $_intIdUsuarioPerfil, $_strLlave)
        {   
            $cliente = new Client($this->wsdl,$this->options);
            $result  = $cliente->activarLicencia($_intIdServicio, $_intIdUsuarioPerfil, $_strLlave, $_strNombreUsuario, $_strNombre);
            return $result;          
        }
}