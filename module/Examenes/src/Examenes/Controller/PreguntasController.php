<?php
namespace Examenes\Controller;

use Examenes\Entity\Respuesta;
use Zend\View\Model\ViewModel;
use Examenes\Model\PreguntaModel;
use Examenes\Model\RespuestaModel;
use Examenes\Entity\Pregunta;
use Zend\Log\Writer\Stream;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Log\Logger;
use Zend\Log\Writer\FirePhp;
use Examenes\Form\PreguntaForm;
use Examenes\Form\RespuestaForm;
use Zend\Session\Container;

/** 
 * Controlador Preguntas:
 * -Guarda las preguntas y respuestas de examenes.
 * - ...
 * 
 * @author oreyes
 * @since 03/12/2014
 * @copyright Grupo Educare SA. de CV. derechos reservados.
 * 
 */
Class PreguntasController extends AbstractActionController
{
	/**
	 * Para crear instancia del objeto logger.
	 * @var Logger $log;
	 */
	protected $log;
	
	/**
	 * Para crear instancia del objeto logger para firePHP.
	 * @var Logger $console
	 */
	protected $console;
	
	/**
	 * Instancia de la clase Container para el manejo de datos de sesion.
	 * @var Container $datos_sesion.
	 */
	protected $datos_sesion;
        
	/**
	 * Instancia de la clase Container para el manejo de datos de sesion.
	 * @var Container $sesion_examen.
	 */
	protected $sesion_examen;
        
	/**
	 * Constructor de la clase hace una instancia del objeto logger
	 * y agrega el Writer que se encarga de grabar datos en el log 
	 * de apache, se inicia instancia de _writer para FirePHP (Firebug log 
	 * para navegador) se inicia el objeto console y se agrega el _writer encargado
	 * de grabar en consola de firebug.
	 *  
	 */
	public function __construct()
	{
            $this->log = new Logger();
            $this->log->addWriter(new Stream("php://stderr"));

            $_writer = new FirePhp();
            $this->console = new Logger();
            $this->console->addWriter($_writer);
            $this->datos_sesion = new Container('user');
            $this->sesion_examen = new Container('examen');
		
	}
	
	/**
	 * Funcion que pinta la vista para las preguntas.
	 * 
	 * @author oreyes
	 * @since 03/12/2014
	 * @access public.
	 * @return ViewModel Pregunta.phtml
	 */
	public function indexAction()
	{			
            $_arrTipos          = $this->getPreguntaModel()->obtenerTipos();        
            $_arrPreguntas      = $this->getPreguntaModel()->obtenerPreguntas();
            $_arrPreguntasTipos  = $this->getExamenModel() ->mezclaArreglos( 
                                           $_arrTipos,      'tipo', 
                                           $_arrPreguntas,  'preguntas'
                                           );
            
            
            /*$imagenes=array(0=>"http://localhost/otros/File/origen/Zonificador.png", 
                1=>"http://2.bp.blogspot.com/_EZ16vWYvHHg/S-Bl2fuyyWI/AAAAAAAAMKc/DNayYJK8mEo/s1600/www.BancodeImagenesGratuitas.com-Fantasticas-20.jpg", 
                2=>"ui-bg_highlight-soft_75_cccccc_1x100.png",
                3=>"https://upload.wikimedia.org/wikipedia/en/3/3a/ClubAmericaLogo-1.png");
            $fun=$this->getPreguntaModel()->copiarImagenes($imagenes);*/
            
            return new ViewModel(array(
                                    'preguntas'  => $_arrPreguntasTipos
                                    )
                                );        
	}
        
        public function muestratiposAction()
        {
        $_arrTipos = $this->getPreguntaModel()->obtenerTiposPreguntas();
        $view = new ViewModel(array(
            'tipos' => $_arrTipos
                )
        );
        $view->setTerminal(true);
        return $view;
        }

    public function buscadorpreguntasAction(){
             //$_arrTipos          = $this->getPreguntaModel()->obtenerTiposPreguntas();                  
             $view = new ViewModel(array(
                  //                  'tipos'  => $_arrTipos
                                    )
                                );
             $view->setTerminal(true);
            return $view;
        }
        
        public function obtenertipopreguntaajaxAction(){
            $request = $this->getRequest();
            $_intTipoPregunta = $request->getPost('idTipoPregunta');
            
            $formPregunta   = new PreguntaForm('Pregunta', array('tipo' => $_intTipoPregunta));
            $preg           = new Pregunta();
            $formRespuesta  = new RespuestaForm('Respuesta', array('tipo' => $_intTipoPregunta));
            $respuesta      = new Respuesta();
            $arrTipos       = $this->getPreguntaModel()->obtenerTipos();
            $arrNumeracion  = $this->getPreguntaModel()->obtenerNumeracion();
            $cols           = $preg->obtenerColumnasPregunta();        
            $colsResp       = $respuesta->obtenerColumnasRespuesta();  
            $_arrPregunta   = $this->getPreguntaModel()->crearArregloPregunta($formPregunta, $cols);
            $_arrRespuesta  = $this->getRespuestaModel()->crearArregloRespuesta($formRespuesta, $colsResp);
            $_arrMinMaxResp = $this->getRespuestaModel()->obtenerMinimoMaximoRespuestas($_intTipoPregunta);
            $_arrResponse = array(
                        'formPreg'      => $_arrPregunta,
                        'formResp'      => $_arrRespuesta,
                        'arrTipos'      => $arrTipos,
                        'arrNumeracion' => $arrNumeracion,
                        'cols'          => $cols,
                        'colsResp'      => $colsResp,
                        'minimo_maximo' => $_arrMinMaxResp
                    );
            
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrResponse));
            return $response;            
        }

        public function addAction()
	{	
            $formPregunta   = new PreguntaForm();
            $preg           = new Pregunta();
            $formRespuesta  = new RespuestaForm();
            $respuesta      = new Respuesta();
            $arrTipos       = $this->getPreguntaModel()->obtenerTipos();
            $arrNumeracion  = $this->getPreguntaModel()->obtenerNumeracion();
            $cols           = $preg->obtenerColumnasPregunta();        
            $colsResp       = $respuesta->obtenerColumnasRespuesta();   
            $_arrMinMaxResp = $this->getRespuestaModel()->obtenerMinimoMaximoRespuestas(); 
            return new ViewModel(
                    array(
                        'formPreg'      => $formPregunta,
                        //'pregunta'    => $preg,
                        'formResp'      => $formRespuesta,
                        //'respuesta'   => $respuesta,
                        'arrTipos'      => $arrTipos,
                        'arrNumeracion' => $arrNumeracion,
                        'cols'          => $cols,
                        'colsResp'      => $colsResp,
                        'minimo_maximo' => $_arrMinMaxResp
                    )
            );		
	}
        
	public function editAction()
	{
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!$id) {
                return $this->redirect()->toRoute('preguntas', array(
                    'action' => 'add'
                ));
            }
            
            $preguntaProp = $_bolOk = $this->getPreguntaModel()->existePreguntaInExamen($id);
            if($preguntaProp === true){
                return $this->redirect()->toRoute('preguntas', array('action' => 'index'));
            }
            // Get the Examen with the specified id.  An exception is thrown
            // if it cannot be found, in which case go to the index page.
            try {
                $pregunta = $this->getPreguntaTable()->obtenPregunta($id);
            }
            catch (\Exception $ex) {
                return $this->redirect()->toRoute('preguntas', array(
                    'action' => 'index'
                ));
            }
            
            $form  = new PreguntaForm();
            $form->bind($pregunta);
            $form->get('almacenar')->setAttribute('value', 'Editar')->setAttribute('id', 'boton-editar');
            
            $preg = new Pregunta();
            $formRespuesta = new RespuestaForm();
            $respuesta = new Respuesta();
            $arrTipos = $this->getPreguntaModel()->obtenerTipos();
            $arrDatosTipos       = $this->getPreguntaModel()->obtenerTiposPreguntas();
            foreach ($arrDatosTipos as $tipo){
                $arrClaveTipo[$tipo['c03clave_tipo']]=$tipo['c03id_tipo_pregunta'];
            }
            $_arrPreguntas  = $this->getPreguntaModel()->obtenerPreguntas();
            $cols           = $preg->obtenerColumnasPregunta(); 
            $colsResp       = $respuesta->obtenerColumnasRespuesta();          
            $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $id);
            $_arrMinMaxResp = $this->getRespuestaModel()->obtenerMinimoMaximoRespuestas();
            return new ViewModel(
                array(
                    'id'        => $id,
                    'formPreg'  => $form,
                    'pregunta'  => $pregunta,
                    'formResp'  => $formRespuesta,
                    'respuestas'=> $_arrRespuestas,
                    'arrTipos'  => $arrTipos,
                    'arrClaveTipo'    => $arrClaveTipo,
                    'cols'      => $cols,
                    'colsResp'  => $colsResp,
                    'preguntas' => $_arrPreguntas,
                    'minimo_maximo' => $_arrMinMaxResp
                )
            );		            
	}
	
	public function deleteAction()
	{
            $request    = $this->getRequest();
            $_idPreg    = $request->getPost('id_preg');
            $_bolOk     = $this->getPreguntaModel()->eliminarPregunta($_idPreg);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolOk));
            return $response;	
	}
        
        /**
         * 
         * @return type 
         * obtiene todas las preguntas para la opcion de busqueda general
         */
        public function obtenerpreguntasajaxAction(){            
            $_arrPreguntas = $this->getPreguntaModel()->obtenerPreguntas();
            
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrPreguntas));
            return $response;
        }
        
        /**
         * 
         * @return type
         * filtra los tipos de preguntas 
         */
        public function obtenertiposajaxAction(){            
            $_arrTipos = $this->getPreguntaModel()->obtenerTipos();
            $_arrTipos['length'] = count( $_arrTipos );
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrTipos));
            return $response;
        }
        
        /**
         * 
         * @return type
         * filtra las preguntas que no estan guardadas en el examen recibiendo como  parametro el 
         * id del examen a la que pertenece y el tipo de pregunta 
         */
        public function obtenerpreguntastipoajaxAction(){
            $request   = $this->getRequest();
            $idExamen = $request->getPost('idExamen');
            $_arridCategoria = $request->getPost('idCategoria');
            $idTipo = $request->getPost('tipoPreg');
            $_arrPreguntas = $this->getPreguntaModel()->obtenerTipoPregunta($idExamen,$_arridCategoria,$idTipo);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrPreguntas));
            return $response;
        }
        
        public function obtenerNumeracionajaxAction(){
            $_arrNumeracion = $this->getPreguntaModel()->obtenerNumeracion();
            $_arrNumeracion['length'] = count($_arrNumeracion);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrNumeracion));
            return $response;
        }


        public function obtenerrespuestasajaxAction(){
            $request = $this->getRequest();
            $_intTipoPregunta = $request->getPost('tipoPregunta');
            $_intIdPregunta = $request->getPost('idPergunta');
            
            $_arrInfoRespuestas = $this->getRespuestaModel()->obtenerRespuestas($_intTipoPregunta, $_intIdPregunta);
            
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrInfoRespuestas));
            return $response;
        }
        
	/**
	 * funcion para crear o actualizar preguntas
	 * - Recibe peticion con los siguientes datos:
         *  objJson, idPregunta
         * El idPregunta sirve para saber si sólo se va a actualizar la pregunta
	 * - Llena la pregunta con sus respectivas respuestas
	 * - Llena la relacion de preguntas y respuestas con la opcion correcta.
	 *
	 *  @author oreyes.
	 *  @since 03/12/2014
	 *  @access public.
	 *
	 */
	public function creapreguntaajaxAction()
	{
            $request   = $this->getRequest();
            $jsonPregunta = $request->getPost('arrObjPregunta');
             $this->log->debug('El arreglo: ' .  print_r($jsonPregunta, true));
            $str_Categoria = $jsonPregunta['pregunta']['textAreaCategoria'];
            $str_Etiqueta = $jsonPregunta['pregunta']['textAreaEtiqueta'];
            $idPregunta = $request->getPost('idPregunta'); 
            // Faltara una variable me servirá para saber si sólo voy a actualizar
            $_bolActualiza = false;
            $pregunta = New Pregunta();
            $pregunta->intercambiarArray($jsonPregunta['pregunta']);
            if(!$_bolActualiza){
                $_idPregunta = $this->getPreguntaModel()->guardarPregunta($pregunta,$str_Categoria,$str_Etiqueta);
                foreach ($jsonPregunta['respuestas'] as $_respuesta)
                {	
                        $resp = new Respuesta();
                        $resp->intercambiarArray($_respuesta);
                        $preg = new Pregunta();
                        //$preg->intercambiarArray($_respuesta);
                        try {
                            $_idRespuesta = $this->getRespuestaModel()->guardarRespuesta($resp, $_idPregunta, $preg);
                            $bolGuarda = true;
                        } catch (Exception $ex) {
                            $response = $this->getResponse();
                            $response->setContent(\Zend\Json\Json::encode(false));

                            return $response;
                        }
                }
            }else{ 
                $_idRespuesta = $this->getPreguntaModel()->actualizarPregunta($pregunta, $idPregunta);
            }
            $_return = array('bolGuarda'=>$bolGuarda,'_idPregunta'=>$_idPregunta);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_return));

            return $response;
	}
	
        /*
        public function guardarespuestaajaxAction(){  
            $request   = $this->getRequest();
            $jsonRespuesta = $request->getPost('objRespuesta');
            $idPregunta = $request->getPost('idPregunta');
            //$idPregunta
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($jsonRespuesta));

            return $response;
        }
         * 
         */
                
        
	/**
	 * Funcion que llama al service manager
	 * y trae la clase PreguntasModel.
	 * 
	 * @author oreyes.
	 * @since 03/12/2014
	 * @access public.
	 * @return PreguntaModel $pregModel
	 */
	public function getPreguntaModel()
	{
	    $sm         = $this->getServiceLocator();
            $pregModel  = $sm->get('/Examenes/Model/PreguntaModel');
            return $pregModel;  

	}
	
	/**
	 * Funcion que llama al service manager
	 * y trae la clase ExamenModel.
	 * 
	 * @author jpgomez.
	 * @since 21/01/2015
	 * @access public.
	 * @return ExamenModel $examModel.
	 */
	function getExamenModel() 
	{
            $sm         = $this->getServiceLocator();
            $examModel  =  $sm->get('/Examenes/Model/ExamenModel');
            return $examModel;
	}
        
	/**
	 * Funcion que llama al service manager
	 * y trae la clase RespuestasModel.
	 * 
	 * @author oreyes.
	 * @since 03/12/2014
	 * @access public.
	 * @return RespuestaModel $respModel.
	 */
	public function getRespuestaModel() 
	{
            $sm         = $this->getServiceLocator();
            $respModel  =  $sm->get('/Examenes/Model/RespuestaModel');
            return $respModel;
	}
        /**
	 * Funcion que llama al service manager
	 * y trae la clase ExamenTable.
	 * 
	 * @author sabeltran.
	 * @since 28/07/2015
	 * @access public.
	 * @return ExamenTable $respTable.
	 */
	public function getExamenTable() 
	{
            $sm         = $this->getServiceLocator();
            $respTable  =  $sm->get('/Examenes/Model/ExamenTable');
            return $respTable;
	}
	
	/**
	 * Funcion que llama al service manager
	 * y trae la clase PreguntaTable.
	 * 
	 * @author jpgomez.
	 * @since 06/02/2015
	 * @access public.
	 * @return PreguntaTable $pregTable.
	 */
	function getPreguntaTable() 
	{
            $sm         = $this->getServiceLocator();
            $pregTable  =  $sm->get('/Examenes/Model/PreguntaTable');
            return $pregTable;
	}        
        
	function ejemplodragdropAction()
	{
		$view = new ViewModel();
		
		
		return $view;
	}
        
        function marcarpreguntaajaxAction(){
            $request                = $this->getRequest();
            $_intIdPregunta         = $request->getPost('idPregunta');
            $_bolMarcada            = $request->getPost('estatus');            
            $_intIdExamen           = $this->sesion_examen->idExamen;
            $_intIdUsuario          = $this->datos_sesion->idUsuarioPerfil;
            $_intIdUsuarioLicencia  = $this->getExamenModel()->obtenerUsuarioLicencia($_intIdUsuario, $_intIdExamen);
            $_intNumIntento         = 1; // TODO: Obtener de la tabla t10
            $_statusMarcada = $this->getPreguntaModel()->marcarDesmarcarPregunta($_intIdPregunta, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_bolMarcada);            
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_statusMarcada));

            return $response;        
            
        }
        
        /**
         * Controlador que se encarga de almacenar las respuestas de un examen
         * por medio de una carga masiva, esto servirá para los examenes que 
         * se realicen de manera tradicional (Impresos)
         */
        public function almacenarespuestastotalajaxAction(){
            $id = (int) $this->params()->fromRoute('id', 0);
            $request = $this->getRequest();
            $_objRespuestas = $request->getPost('objRespuestas');           
            $_intIdExamen   = $request->getPost('idExamen');    
            $_strLlave      = $request->getPost('strLlave');    
            $i=0;       
            $_bolResultadoFinal=true;
            $_arrPreguntas =  $this->getPreguntaModel()->obtenerPreguntasExamen($_intIdExamen, $id);
            $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_strLlave, $_intIdExamen, true);
            $_intNumIntento  = 1; // TODO: Obtener de la tabla t04, t03
            if(!$id){
                $this->getExamenModel()->iniciaExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento); //Se desactiva para demostración con Miriam                
            }
            $_bolExisteDato = $this->getExamenModel()->existeRegistroExamenUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            $_bolAlmacenadas = true;
             $_bolAleatorioRespuestas = false; // $_arrExamen[$_intIdExamen]['t12aleatorio_respuestas']; // Comento porque no se de donde obtener este dato
            if(!$_bolExisteDato){
                foreach($_arrPreguntas['todas'] as $index=>$row){
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $index);                            
                    $_bolAlmacenadas = $this->getRespuestaModel()->guardarRespuestaUsuarioNueva($_arrRespuestas, $_intIdUsuarioLicencia, $_intIdExamen, $index, $_intNumIntento, $_bolAleatorioRespuestas); 
                   $_bolResultado=$this->getRespuestaModel()->guardarRespuestaUsuario($_objRespuestas[$i], $_intIdUsuarioLicencia, $_intIdExamen, $index, $_intNumIntento);
                    $i++;
                    if(!$_bolResultado){
                        $_bolResultadoFinal=false;
                    }
                }                
            }else{
                foreach($_arrPreguntas['todas'] as $index=>$row){
                  $_bolResultado=$this->getRespuestaModel()->guardarRespuestaUsuario($_objRespuestas[$i], $_intIdUsuarioLicencia, $_intIdExamen, $index, $_intNumIntento);
                    $i++;
                    if(!$_bolResultado){
                        $_bolResultadoFinal=false;
                    }
                }
            }
            if($_bolResultadoFinal){
                 try {
                        $examen = $this->getExamenTable()->obtenExamen($_intIdExamen);
                    }
                    catch (\Exception $ex) {
                        $this->log->debug($ex->getMessage());
                        return $this->redirect()->toRoute('examenes', array(
                            'action' => 'index'
                        ));
                    }
                $_bolOk = $this->getExamenModel()->finalizaExamen( $_intIdExamen, $examen, $_intIdUsuarioLicencia, $_intNumIntento );
                
            }
                 $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolOk));

            return $response;             
        }
        
        public function preguntamultipleAction()
        {            
            $id = (int) $this->params()->fromRoute('id', 0);
            $request = $this->getRequest();
            $_objRespuestas = $request->getPost('objRespuestas');
            $_strBoton      = $request->getPost('botonSeleccionado');
            $_preguntaIr    = $request->getPost('idPregunta');
            
            $_bolActualizaPregunta = false;
            $_intIdExamen   = $this->sesion_examen->idExamen;
            $_strExamen     = $this->sesion_examen->nombreExamen;
            $_strTiempo     = $this->sesion_examen->Tiempo;
            $_intIdNivel    = $this->sesion_examen->idNivel;
            $_intIdUsuario  = $this->datos_sesion->idUsuarioPerfil;
            $_strActual     = false;
            //$this->log->debug('El POST: ' .  print_r($_POST, true));
                if(!$_intIdExamen || ( !$_strBoton && $id )){                
                    return $this->redirect()->toRoute('examenes', array( 'action' => 'misexamenes' ));
                }
            $_arrPreguntas =  $this->getPreguntaModel()->obtenerPreguntasExamen($_intIdExamen, $id);
            $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_intIdUsuario, $_intIdExamen);
            $demo = false;
            $_intNumIntento  = $demo ? 1:$this->sesion_examen->numeroIntento; 
            if(!$id){
                $this->getExamenModel()->iniciaExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento); //Se desactiva para demostración con Miriam                
            }
            $_bolExisteDato = $this->getExamenModel()->existeRegistroExamenUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            $_bolAlmacenadas = true;
            // TODO: Falta validar que exista el mismo numero de preguntas en la t10 y en la t17
            if(!$_bolExisteDato){
                $_guardarExamenInicio = $this->getExamenModel()->guardarExamenInicio($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                $_arrExamen = $this->getExamenModel()->obtenerExamenes( $_intIdExamen, true );
                //$this->log->debug('arrExamen: ' . print_r($_arrExamen, true));                
                $_bolAleatorioRespuestas = $_arrExamen[$_intIdExamen]['t12aleatorio_respuestas'];
                foreach($_arrPreguntas['todas'] as $index=>$row){
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $index);
                        $_bolAlmacenadas = $this->getRespuestaModel()->guardarRespuestaUsuarioNueva(
                                $_arrRespuestas, $_intIdUsuarioLicencia, $_intIdExamen, $index, $_intNumIntento, $_bolAleatorioRespuestas);
                    }
                $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_arrPreguntas['actual'], $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                $_strActual  = $_arrPreguntas['actual'];
            }else{
                if($_objRespuestas){
                   $_guardarRespuestasUsuario = $this->getRespuestaModel()->guardarRespuestaUsuario($_objRespuestas, $_intIdUsuarioLicencia, $_intIdExamen, $_arrPreguntas['actual'], $_intNumIntento);
                    if($_strBoton === 'irA'){ // Se agrega la condicion para la navegacion entre preguntas
                        $_strActual  = $_preguntaIr;
                    }else{
                        $_strActual  = $_arrPreguntas[$_strBoton];
                    }
                    $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento,$_arrPreguntas['actual']);
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_strActual, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento); 
                    $_arrAntSig = $this->getPreguntaModel()->obtenerAnteriorSiguiente($_arrPreguntas['todas'], $_strActual);
                    $_bolActualizaPregunta = true;
                }else{
                    if($_strBoton){
                        if($_strBoton === 'irA'){ // Se agrega la condicion para la navegacion entre preguntas
                            $_strActual  = $_preguntaIr;
                        }else{
                            $_strActual  = $_arrPreguntas[$_strBoton];
                        }
                        $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento,$_arrPreguntas['actual']);
                        
                        $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_strActual, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);                        
                        $_arrAntSig = $this->getPreguntaModel()->obtenerAnteriorSiguiente($_arrPreguntas['todas'], $_strActual);
                        $_bolActualizaPregunta = true;
                    }else{
                        $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento,$_arrPreguntas['actual']);
                        $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_arrPreguntas['actual'], $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                        $_strActual  = $_arrPreguntas['actual'];
                    }
                    $_bolAlmacenadas = true;
                }
                $_guardarFechaRegistro = $this->getRespuestaModel()->guardarPreguntaRegistro($_intIdUsuarioLicencia,$_intIdExamen,$_strActual);
            }
            $_arrSeccionPregunta = $this->getPreguntaModel()->obtenerSeccionPregunta($_intIdExamen, $_strActual);
            $_strSeccionPregunta = $_arrSeccionPregunta['t15seccion'];
            $_obtenerDiferenciaTiempo = $this->getRespuestaModel()->obtenerDiferenciaTiempo($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_arrPreguntas['actual']);
                $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestaUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_arrRespuestas);
            
            $_dblAvance = $this->getExamenModel()->obtenerAvanceExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            $_intNumPregunta = $this->getExamenModel()->obtenerNumeroPregunta($_arrPreguntas['todas'], $_strActual);
            $arrPregunta = $this->getRespuestaModel()->formateaObjetoRespuesta($_arrRespuestas);
            $this->log->debug(print_r($arrPregunta,true));
            //$this->getPreguntaModel()->marcarDesmarcarPregunta($_intIdPregunta, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_bolMarcada);
            $_strMarcada = $this->getPreguntaModel()->estaPreguntaMarcada($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_strActual);
            $_arrPreguntas['todas'] = $this->getPreguntaModel()->marcaPreguntaInit($_arrPreguntas['todas'], $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            if($_bolActualizaPregunta){
                $objPregunta = \Zend\Json\Json::encode($arrPregunta);
                $respuesta =  array
                            (
                                'jsonPregunta'  =>  $objPregunta,   
                                'idNivel'       =>  $_intIdNivel,
                                'nombreExamen'  =>  $_strExamen,
                                'pctAvance'     =>  $_dblAvance,
                                'pregsExam'     =>  $_arrPreguntas['todas'],
                                'numPreg'       =>  $_intNumPregunta,
                                'total'         =>  count($_arrPreguntas['todas']),
                                'actual'        =>  $_strActual ? $_strActual : $_arrPreguntas['actual'],
                                'marcada'       =>  $_strMarcada,
                                'tiempo'        =>  $_strTiempo,
                                'anterior'      =>  $_arrAntSig['anterior'],
                                'siguiente'     =>  $_arrAntSig['siguiente'],
                                'primera'       =>  $_arrAntSig['primera'],
                                'ultima'        =>  $_arrAntSig['ultima'],
                                'seccion'       =>  $_arrSeccionPregunta['t15seccion']
                            );
                
                
                $response = $this->getResponse();
                $response->setContent(\Zend\Json\Json::encode($respuesta));

                return $response;                                
            }
            $response = \Zend\Json\Json::encode($arrPregunta);
            $view = new ViewModel( array
                            (
                                'jsonPregunta'  =>  $response,  
                                'idNivel'       =>  $_intIdNivel,
                                'nombreExamen'  =>  $_strExamen,
                                'pctAvance'     =>  $_dblAvance,
                                'pregsExam'     =>  $_arrPreguntas['todas'],
                                'numPreg'       =>  $_intNumPregunta,
                                'total'         =>  count($_arrPreguntas['todas']),
                                'actual'        =>  $_arrPreguntas['actual'],
                                'marcada'       =>  $_strMarcada,
                                'tiempo'        =>  $_strTiempo,
                                'anterior'      =>  $_arrPreguntas['anterior'],
                                'siguiente'     =>  $_arrPreguntas['siguiente'],
                                'primera'       =>  $_arrPreguntas['primera'],
                                'ultima'        =>  $_arrPreguntas['ultima'],
                                'seccion'       =>  $_arrSeccionPregunta['t15seccion']
                            ));
            //$view->setTerminal(true);
            return $view;
        }
        
        public function muestrapreguntaAction()
        {            
            $id = (int) $this->params()->fromRoute('id', 0);
            $request = $this->getRequest();
            //$_objRespuestas = $request->getPost('objRespuestas');
            $_strBoton      = $request->getPost('botonSeleccionado');
            //$_preguntaIr    = $request->getPost('idPregunta');
            $_bolActualizaPregunta = false;
            //$_intIdExamen   = $this->sesion_examen->idExamen;
           // $_strExamen     = $this->sesio    n_examen->nombreExamen;
           // $_strTiempo     = $this->sesion_examen->Tiempo;
           // $_intIdNivel    = $this->sesion_examen->idNivel;
            //$_intIdUsuario  = $this->datos_sesion->idUsuarioPerfil;
            $_strActual     = false;
           
            //$this->log->debug('El POST: ' .  print_r($_POST, true));
//            if(!$_intIdExamen || ( !$_strBoton && $id )){                
//                return $this->redirect()->toRoute('examenes', array( 'action' => 'misexamenes' ));
//            }
            $_arrPreguntas =  $this->getPreguntaModel()->obtenerPreguntasExamen($id, $id);
            $_intIdUsuarioLicencia =2;    //$this->getExamenModel()->obtenerUsuarioLicencia($_intIdUsuario, $_intIdExamen);
            //$_intNumIntento  = $this->sesion_examen->numeroIntento; 
//            if(!$id){
//                $this->getExamenModel()->iniciaExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento); //Se desactiva para demostración con Miriam                
//            } 
            $_arrExamen = $this->getExamenModel()->obtenerExamenes( $id, true );
            $_bolAleatorioRespuestas =0; //$_arrExamen[$_intIdExamen]['t12aleatorio_respuestas'];
            $_intNumIntento=1;
            foreach($_arrPreguntas['todas'] as $index=>$row){
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $index);
                    $this->getRespuestaModel()->guardarRespuestaUsuarioNueva(
                            $_arrRespuestas, $_intIdUsuarioLicencia, $id, $index, $_intNumIntento, $_bolAleatorioRespuestas);
                }                
            
            $i=0;
            foreach($_arrPreguntas['todas'] as $index=>$row){
                
                $_arrRespuestasTotal[$i] = $this->getRespuestaModel()->obtenerRespuestas(0, $index);
                if(!$i){
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $index);
                    $_strActual  = $index;
                }
                //$_arrRespuestasTotal[$i] = $this->getRespuestaModel()->obtenerRespuestaUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_arrRespuestasTotal[$i]);
                //Para Sebas: Crear funcion donde se eliminen los datos innecesarios
                $_arrPreguntasTotal[$i] = $this->getRespuestaModel()->formateaObjetoRespuestaJSON($_arrRespuestasTotal[$i]);
                $_arrImagenes[$i]=$_arrPreguntasTotal[$i]['imagenes'];
                $_boolEstado = $this->getPreguntaModel()->copiarImagenes($_arrImagenes[$i]);
              
                unset($_arrPreguntasTotal[$i]['imagenes']);
//                $_arrImagenesruta[$i]= $this->getRespuestaModel()->obtenerRutaImagen($_arrRespuestasTotal[$i]);
                $i++;   
                
                /*$_bolAlmacenadas = $this->getRespuestaModel()->guardarRespuestaUsuarioNueva(
                        $_arrRespuestas, $_intIdUsuarioLicencia, $_intIdExamen, $index, $_intNumIntento, $_bolAleatorioRespuestas);*/
            } 
              $this->getPreguntaModel()->generarZip('examenprebas');
            
            /*
            $_bolExisteDato = $this->getExamenModel()->existeRegistroExamenUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            $_bolAlmacenadas = true;
           
            if(!$_bolExisteDato){
                $_guardarExamenInicio = $this->getExamenModel()->guardarExamenInicio($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                $_arrExamen = $this->getExamenModel()->obtenerExamenes( $_intIdExamen, true );
                //$this->log->debug('arrExamen: ' . print_r($_arrExamen, true));                
                $_bolAleatorioRespuestas = $_arrExamen[$_intIdExamen]['t12aleatorio_respuestas'];
                foreach($_arrPreguntas['todas'] as $index=>$row){
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $index);
                    $_bolAlmacenadas = $this->getRespuestaModel()->guardarRespuestaUsuarioNueva(
                            $_arrRespuestas, $_intIdUsuarioLicencia, $_intIdExamen, $index, $_intNumIntento, $_bolAleatorioRespuestas);
                }                
                $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_arrPreguntas['actual'], $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                $_strActual  = $_arrPreguntas['actual'];
            }else{
                if($_objRespuestas){
                   $_guardarRespuestasUsuario = $this->getRespuestaModel()->guardarRespuestaUsuario($_objRespuestas, $_intIdUsuarioLicencia, $_intIdExamen, $_arrPreguntas['actual'], $_intNumIntento);
                    if($_strBoton === 'irA'){ // Se agrega la condicion para la navegacion entre preguntas
                        $_strActual  = $_preguntaIr;
                    }else{
                        $_strActual  = $_arrPreguntas[$_strBoton];
                    }
                    $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento,$_arrPreguntas['actual']);
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_strActual, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento); 
                    $_arrAntSig = $this->getPreguntaModel()->obtenerAnteriorSiguiente($_arrPreguntas['todas'], $_strActual);
                    $_bolActualizaPregunta = true;
                }else{
                    if($_strBoton){
                        if($_strBoton === 'irA'){ // Se agrega la condicion para la navegacion entre preguntas
                            $_strActual  = $_preguntaIr;
                        }else{
                            $_strActual  = $_arrPreguntas[$_strBoton];
                        }
                        $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento,$_arrPreguntas['actual']);
                        
                        $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_strActual, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);                        
                        $_arrAntSig = $this->getPreguntaModel()->obtenerAnteriorSiguiente($_arrPreguntas['todas'], $_strActual);
                        $_bolActualizaPregunta = true;
                    }else{
                        $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento,$_arrPreguntas['actual']);
                        $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_arrPreguntas['actual'], $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                        $_strActual  = $_arrPreguntas['actual'];
                    }
                    $_bolAlmacenadas = true;
                }
                $_guardarFechaRegistro = $this->getRespuestaModel()->guardarPreguntaRegistro($_intIdUsuarioLicencia,$_intIdExamen,$_strActual);
            }            
             */
            //$_arrSeccionPregunta = $this->getPreguntaModel()->obtenerSeccionPregunta($_intIdExamen, $_strActual);
            //$_strSeccionPregunta = $_arrSeccionPregunta['t15seccion'];
            //$_obtenerDiferenciaTiempo = $this->getRespuestaModel()->obtenerDiferenciaTiempo($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_arrPreguntas['actual']);
            $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestaUsuario($_intIdUsuarioLicencia, $id, $_intNumIntento, $_arrRespuestas);
            $_dblAvance = $this->getExamenModel()->obtenerAvanceExamen($_intIdUsuarioLicencia, $id, $_intNumIntento);
            $_intNumPregunta = $this->getExamenModel()->obtenerNumeroPregunta($_arrPreguntas['todas'], $_strActual);
            
            
            
            //$this->getPreguntaModel()->marcarDesmarcarPregunta($_intIdPregunta, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_bolMarcada);
            $_strMarcada = $this->getPreguntaModel()->estaPreguntaMarcada($_intIdUsuarioLicencia, $id, $_intNumIntento, $_strActual);
            $_arrPreguntas['todas'] = $this->getPreguntaModel()->marcaPreguntaInit($_arrPreguntas['todas'], $_intIdUsuarioLicencia, $id, $_intNumIntento);
           
            $response = \Zend\Json\Json::encode($_arrRespuestas);
            $preguntasExam = \Zend\Json\Json::encode($_arrRespuestasTotal);
            
            // esto solo sirve para la creacion de los paquetes json
            /*
                 $ar=fopen("PRUEBA_PRIMEROIC41.json","a") or
                die("Problemas en la creacion");
                
                fputs($ar,$preguntasExam);
                echo "Los datos se cargaron correctamente.";
                
                
                
                
                
                
              
                $this->log->debug('array---->: ' .  print_r($_arrPreguntasTotal, true));
                 $response2 = json_encode($_arrPreguntasTotal,JSON_PRETTY_PRINT);
                 $decodificado=json_decode($response2);
                 $this->log->debug('$decodificado---->: ' .  print_r($decodificado, true));
                         $arr=fopen("PRUEBA.json","a") or
                die("Problemas en la creacion");
                
                fputs($arr,$response2);
                echo "Los datos se cargaron correctamente.";
                
                
                
              */  
                error_log('#######_arrRespuestasTotal########');
                foreach ($_arrRespuestasTotal as $key => $value) {
                    error_log($key);
                    foreach ($value as $key2 => $value2) {
                        error_log($key2);
                        error_log(print_r($value2,true));
                    }                    
                }
//            $file = 'clientes.json';
//            $fp = fopen("clientes.json","w+"); 
//            fwrite($fp, $response);
            $_arrResponse=array
                            (
                                'jsonPregunta'  =>  $response,  
                                //'idNivel'       =>  $_intIdNivel,
          //                      'nombreExamen'  =>  $_strExamen,
                                'pctAvance'     =>  $_dblAvance,
                                'preguntasExam' =>  $preguntasExam,
                                'pregsExam'     =>  $_arrPreguntas['todas'],
                                'numPreg'       =>  $_intNumPregunta,
                                'total'         =>  count($_arrPreguntas['todas']),
                                'actual'        =>  $_arrPreguntas['actual'],
                                'marcada'       =>  $_strMarcada,
                               // 'tiempo'        =>  $_strTiempo,
//                                'anterior'      =>  $_arrPreguntas['anterior'],
//                                'siguiente'     =>  $_arrPreguntas['siguiente'],
//                                'primera'       =>  $_arrPreguntas['primera'],
//                                'ultima'        =>  $_arrPreguntas['ultima'],
                                //'seccion'       =>  $_arrSeccionPregunta['t15seccion']
                            );
            $view = new ViewModel($_arrResponse );
            //$view->setTerminal(true);
            return $view;
        }   
        public function muestraExamenLibroAction()
        {            
            $id = (int) $this->params()->fromRoute('id', 0);
            error_log($id);
            $demo = $id === 34 || $id === 35 || $id === 36 ? true:false;
            error_log($demo);
            $_intIdExamen   = $demo ? 34:$this->sesion_examen->idExamen;
            $_strExamen     = $demo ? "Demo":$this->sesion_examen->nombreExamen;
            error_log('_intIdExamen: '.$_intIdExamen);
            error_log('_strExamen: '.$_strExamen);
            $_strTiempo     = array_values($demo ? "90":$this->sesion_examen->Tiempo);
            error_log('_strTiempo: '.$_strTiempo);
            $_arrTiepo = json_encode($_strTiempo);
            $_intIdUsuario  = $demo ? "2":$this->datos_sesion->idUsuarioPerfil;
            if(!$demo){
                $_intIdExamen   = $this->sesion_examen->idExamen;
                $_objPreguntas = $this->getPreguntaModel()->obtenerPreguntasExamenTodo($_intIdExamen);
                $arrPreguntaRespuestas = $this->getRespuestaModel()->obtenerRespuestasTodo($_objPreguntas);
                $_intNumIntento  = $demo ? 1:$this->sesion_examen->numeroIntento; 
                $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_intIdUsuario, $_intIdExamen);
                $_bolExisteDato = $this->getExamenModel()->existeRegistroExamenUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                $this->getExamenModel()->iniciaExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                $_bolAlmacenadas = true;
                if(!$_bolExisteDato){
                    $_guardarExamenInicio = $this->getExamenModel()->guardarExamenInicio($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                    $_arrExamen = $this->getExamenModel()->obtenerExamenes( $_intIdExamen, true );              
                    $_bolAleatorioRespuestas = false;
                    foreach($arrPreguntaRespuestas as $index=>$row){
                        $_strOrdenInicial = $this->obtenerSecuenciaRespuestas($row);
                        $_arrRespuestasShuffle = $this->shuffleArrayController($row["respuestas"], $_strOrdenInicial);
                        $_idPregunta = $row["pregunta"]["t11id_pregunta"];
                        
                        $arrPreguntaRespuestas[$index]["respuestas"] = $_arrRespuestasShuffle;
                        $_strCorrecta = $this->getRespuestaModel()->obtenerRespuestaCorrecta($arrPreguntaRespuestas[$index], $_strOrdenInicial);    
                        $_bolAlmacenadas = $this->getRespuestaModel()->guardarRespuestaUsuarioNueva($_intIdUsuarioLicencia, $_intIdExamen, $_idPregunta, $_intNumIntento, $_bolAleatorioRespuestas, $_strCorrecta, $_strOrdenInicial);
                        $objRespuestasUsuario = json_encode(array());
                    }                
                }else{
                    $_arrOrdenUsuario = $this->getRespuestaModel()->obtenerStringsRespuestasUsuario($_intIdUsuarioLicencia, $_intIdExamen, 't10orden');
                    foreach($arrPreguntaRespuestas as $index=>$row){
                        $_idPregunta = $row["pregunta"]["t11id_pregunta"];
                        $_strOrden = $_arrOrdenUsuario[$index];
                        $_arrRespuestasShuffle = $this->shuffleArrayController($row["respuestas"], $_strOrden);
                        $arrPreguntaRespuestas[$index]["respuestas"] = $_arrRespuestasShuffle;
                        $_guardarFechaRegistro = $this->getRespuestaModel()->guardarPreguntaRegistro($_intIdUsuarioLicencia,$_intIdExamen,$_idPregunta);
                    }
                    $objRespuestasUsuario = json_encode($this->getRespuestaModel()->obtenerStringsRespuestasUsuario($_intIdUsuarioLicencia, $_intIdExamen, 't10respuesta_usuario'));
                }
                $_intIdJson = "json = ".json_encode($arrPreguntaRespuestas);
            }else{
                switch ($id){
                    case "34":
                            $_intIdJson = '/motorEvaluaciones/js/demo.js';
                            $_strExamen = 'demo IC4';
                        break;
                    case "35":
                            $_intIdJson = '/motorEvaluaciones/js/demoTODO.js';
                        $_strExamen = 'demo Biología';
                        break;
                    case "36":
                            $_intIdJson = '/motorEvaluaciones/js/demoTODO_intellectus.js';
                        $_strExamen = 'Demo Intellectus Post';
                        break;
                }
                
                $objRespuestasUsuario = json_encode(array());
            }
            


            $_arrResponse=array
                        (
                            'pctAvance'     =>  0,
                            'actual'        =>  1,
                            'marcada'       =>  false,
                            'json'          => $_intIdJson,
                            'respuestasUsuario' => $objRespuestasUsuario,
                            'tiempo'    => $_arrTiepo,
                            'demo'    =>  $demo,
                            'nombreExamen'  =>  $_strExamen

                        );
            
            $view = new ViewModel($_arrResponse );
            return $view;
        }   
        
        /**
         * Actualiza una pregunta y sus instrucciones, así como de sus respuestas.
         * @return type
         */
        public function editaRespPreguntaAjaxAction() {
            $request = $this->getRequest();
            $arrPregResp= $request->getPost('_objPregunta');
            $return = false;
            $pregunta = New Pregunta();
            $pregunta->intercambiarArray($arrPregResp['pregunta']);
            $categoria = $arrPregResp['pregunta']['textAreaCategoria'];
            $etiqueta = $arrPregResp['pregunta']['textAreaEtiqueta']; 
            $idPregunta = $arrPregResp['pregunta']['id_pregunta']; 
            $_idRespuesta = $this->getPreguntaModel()->actualizarPregunta($pregunta, $idPregunta,$categoria,$etiqueta);
            
            foreach ($arrPregResp['respuesta'] as $_respuesta)
            {
                if($_respuesta['id'] != 'nuevo'){
                    try {
                        $_idRespuesta = $this->getRespuestaModel()->actualizaRespuesta($_respuesta,$idPregunta);
                        $return = true;
                        
                    } catch (Exception $ex) {
                        $return = false;
                    }
                }
                else{     
                    if($_respuesta['estatus'] == 1){
                        $resp = new Respuesta();
                        $resp->intercambiarArray($_respuesta);
                        $preg = new Pregunta();
                        $preg->intercambiarArray($arrPregResp['pregunta']);
                        try {
                            $this->getRespuestaModel()->guardarRespuesta($resp, $idPregunta, $preg);
                            $_idRespuesta = true;
                        } catch (Exception $ex) {
                            $_idRespuesta = false;
                        }
                    }   
                }
            }
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($return));

            return $response;
        }
        
        
        function vistapreviaAction()
        {            
            $response = \Zend\Json\Json::encode($arrPregunta);
            $view = new ViewModel( array
                            (
                                'jsonPregunta'  =>  $response,
                                'nombreExamen'  =>  $$nombre,
                                'pctAvance'     =>  0,
                                'pregsExam'     =>  $_arrPregunta,
                                'numPreg'       =>  1,
                                'total'         =>  1,
                                'actual'        =>  1,
                                'marcada'       =>  "Desmarcada",
                                'anterior'      =>  1,
                                'siguiente'     =>  1,
                                'primera'       =>  1,
                                'ultima'        =>  1
                            ));
            //$view->setTerminal(true);
            return $view;
        }
        
        /**
         * obtiene las categorias que pertenecen a las preguntas
         * @return type
         */
        public function obtenercategoriaajaxAction(){
            $_arrCategorias = $this->getPreguntaModel()->obtenerCategorias();
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrCategorias));
            return $response;
        }
        
        /**
         * obtiene las categorias que pertencen a la pregunta seleccionada a partir de un id
         * @return type
         */
        public function obtenerCategoriaPreguntaajaxAction(){
            $request   = $this->getRequest();
            $_idExamen = $request->getPost('idExamen');
            $_arridCategoria = $request->getPost('idCategoria');
            $_arridTipo = $request->getPost('idTipos');
            $_arrPreguntas = $this->getPreguntaModel()->obtenerCategoriaPregunta($_idExamen,$_arridCategoria,$_arridTipo);
            $response = $this->getResponse();
             $response->setContent(\Zend\Json\Json::encode($_arrPreguntas));
            return $response;
        }
        /**
         * obtiene las etiquetas
         * @return type
         */
        public function obtenerEtiquetaajaxAction(){
            $_arrEtiquetas = $this->getPreguntaModel()->obtenerEtiquetas();
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrEtiquetas));
            return $response;
        }
        
        public function obtenerEtiquetaPreguntaajaxAction(){
            $request    = $this->getRequest();
            $_idExamen = $request->getPost('idExamen');
            $_arridCategoria = $request->getPost('idCategoria');
            $_arridTipo = $request->getPost('idTipos');
            $_arridEtiqueta = $request->getPost('idEtiqueta');
            $_arrPreguntas = $this->getPreguntaModel()->obtenerEtiquetaPregunta($_idExamen, $_arridCategoria, $_arridTipo,$_arridEtiqueta);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrPreguntas));
            return $response;
        }
        
        public function obtenerCategoriasEditAction(){
            $request = $this->getRequest();
            $idPregunta = $request->getPost('idPregunta');
            $_bolOk = $this->getPreguntaModel()->getCategoriasEdit($idPregunta);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolOk));
            return $response;
        }         
        public function obtenerEtiquetaEditAction(){
            $request = $this->getRequest();
            $idPregunta = $request->getPost('idPregunta');
            $_bolOk = $this->getPreguntaModel()->getEtiquetasEdit($idPregunta);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolOk));
            return $response;
        }         
        
        public function copiarpreguntaAction(){
            $request   = $this->getRequest();
            $idPregunta= $request->getPost('_idPregunta');
            $idPreguntaSeccion= $request->getPost('_idPreguntaSeccion');
            
            //obtiene los datos de la pregunta para clonar
            $_pregunta = $this->getPreguntaModel()->obtenerPreguntas($idPregunta,true);
            $arrTmp = array_keys($_pregunta);
            $_pregunta1 = $_pregunta[$arrTmp[0]];
            //obtiene todas las relaciones de preguntas con categoria 
            $_arrCategoria = $this->getPreguntaModel()->getCategoriasEdit($idPregunta);
            //obtiene todas las relaciones de pregunta etiquetas
            $_arrEtiqueta = $this->getPreguntaModel()->getEtiquetasEdit($idPregunta);
            //obtiene las repsuestas de la pregunta
            $_arrInfoRespuestas = $this->getRespuestaModel()->obtenerRespuestas($_pregunta[$idPregunta]['c03id_tipo_pregunta'], $idPregunta);
            if($idPreguntaSeccion === null){
                //realiza un acopia de la pregunta desde la vista de banco de preguntas
                $_IdPregClon = $this->getPreguntaModel()->clonarPregunta($_pregunta1,$_arrCategoria,$_arrEtiqueta);
            }else{
                //realiza un acopia de la pregunta desde la vista ligarpreguntas examen
                $_IdPregClon = $this->getPreguntaModel()->clonarPregunta($_pregunta1,$_arrCategoria,$_arrEtiqueta,$idPreguntaSeccion);
            }
            foreach ($_arrInfoRespuestas['respuestas'] as $_respuesta)
            {
                unset($_respuesta['t13id_respuesta_parent'],$_respuesta['t13id_respuesta'],$_respuesta['t13fecha_registro'],$_respuesta['t13fecha_actualiza'],$_respuesta['t13estatus']);
                foreach ($_respuesta as $index => $value){
                    if($index == 't17correcta'){
                        $cut = 't17';
                    }else{
                        $cut = 't13' ;
                    }
                    $trimmed = str_replace($cut, '', $index);
                    $_respuesta[$trimmed]= $value;
                    unset($_respuesta[$index]);
                }
                try {
                    $resp = new Respuesta();
                    
                    $resp->intercambiarArray($_respuesta);
                    $preg = new Pregunta();
                    $_idRespuesta = $this->getRespuestaModel()->guardarRespuesta($resp, $_IdPregClon, $preg);
                    } catch (Exception $ex) {
                        $response = $this->getResponse();
                        $response->setContent(\Zend\Json\Json::encode(false));
                        return $response;
                        }
            }            
            $_return = array('bolGuarda'=>$_idRespuesta,'_idPregunta'=>$_IdPregClon);
            $response = $this->getResponse();
            return $response->setContent(\Zend\Json\Json::encode($_return));
        }
        
        public function ifPreguntaInExamAction(){
            $request   = $this->getRequest();
            $idPregunta= $request->getPost('_idPregunta');
            $idExamen= $request->getPost('_idExamen');
            //verifica si la pregunta se encuentra en algun examen
            $_bolOk = $this->getPreguntaModel()->existePreguntaInExamen($idPregunta,$idExamen);
            $response = $this->getResponse();
            return $response->setContent(\Zend\Json\Json::encode($_bolOk));
        }
        
        public function tipoPreguntaAction(){
            $_arrTipos          = $this->getPreguntaModel()->obtenerTipos();        
            $_arrPreguntas      = $this->getPreguntaModel()->obtenerPreguntas();
            $_arrPreguntasTipos  = $this->getExamenModel() ->mezclaArreglos( 
                                           $_arrTipos,      'tipo', 
                                           $_arrPreguntas,  'preguntas'
                                           );
            $response = $this->getResponse();
            return $response->setContent(\Zend\Json\Json::encode($_arrPreguntasTipos));
        }
        
        private function leerArchivo($archivo){
            $file=$_SERVER["DOCUMENT_ROOT"].'/'.$archivo;
            $gestor = fopen($file, "r");
            $contenido = fread($gestor, filesize($file));
            //$response = $this->getResponse();
            //$response->setContent(\Zend\Json\Json::encode($contenido));
            return $contenido;
        }

        public function preguntatodoAction()
        {           
            $idPregunta = (int) $this->params()->fromRoute('id', 0);
            $request = $this->getRequest();
            $_strRespuestaUsuario = $request->getPost('objRespuestas');
            $_bolActualizaPregunta = false;
            $_intIdExamen   = $this->sesion_examen->idExamen;
            $_strExamen     = $this->sesion_examen->nombreExamen;
            $_strTiempo     = implode(":", $this->sesion_examen->Tiempo);
            $_intIdNivel    = $this->sesion_examen->idNivel;
            $_intIdUsuario  = $this->datos_sesion->idUsuarioPerfil;
            $_strActual     = false;

            $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_intIdUsuario, $_intIdExamen);
            $_intNumIntento  = $this->sesion_examen->numeroIntento; 
            $_bolAlmacenadas = true;
            
                if($_strRespuestaUsuario){
                    $_guardarRespuestasUsuario = $this->getRespuestaModel()->guardarRespuestaUsuario($_strRespuestaUsuario, $_intIdUsuarioLicencia, $_intIdExamen, $idPregunta, $_intNumIntento);

                    $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento, $idPregunta);//Actualiza fecha cada vez se se navega
                    $_arrExamenInfo = $this->getExamenModel()->obtenerExamenes( $this->sesion_examen->idExamen, true);
                    $_strTiempoInicial = $this->getExamenModel()->conversorSegundosHoras($_arrExamenInfo[$this->sesion_examen->idExamen]['t12tiempo']);
                    $_obtenerDiferenciaTiempo = $this->getRespuestaModel()->obtenerDiferenciaTiempo($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $idPregunta, $_strTiempoInicial, $_strTiempo);
                    $_bolActualizaPregunta = true;
                }else{

                    $_guardarFechaActualiza = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia,$_intIdExamen,$_intNumIntento,$_arrPreguntas['actual']);
                    $_arrRespuestas = $this->getRespuestaModel()->obtenerRespuestas(0, $_arrPreguntas['actual'], $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
                    $_strActual  = $_arrPreguntas['actual'];
                    $_bolAlmacenadas = true;
                }
            

            $_dblAvance = $this->getExamenModel()->obtenerAvanceExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            $_intNumPregunta = 0;
            $_strMarcada = $this->getPreguntaModel()->estaPreguntaMarcada($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_strActual);
            //$_arrPreguntas['todas'] = $this->getPreguntaModel()->marcaPreguntaInit($_arrPreguntas['todas'], $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            if($_bolActualizaPregunta){
                $respuesta =  array
                            ( 
                                'idNivel'       =>  $_intIdNivel,
                                'pctAvance'     =>  $_dblAvance,
                                'marcada'       =>  $_strMarcada,
                                'tiempo'        =>  $_strTiempo,
                            );
                
                
                $response = $this->getResponse();
                $response->setContent(\Zend\Json\Json::encode($respuesta));

                return $response;                                
            }
            $response = \Zend\Json\Json::encode($arrPregunta);
            $view = new ViewModel( array
                            (
                                'jsonPregunta'  =>  $response,  
                                'pctAvance'     =>  $_dblAvance,
                                'marcada'       =>  $_strMarcada,
                                'tiempo'        =>  $_strTiempo,
                                'seccion'       =>  $_arrSeccionPregunta['t15seccion']
                            ));
            return $view;
        }
        
        public function obtenerRespuestaCorrecta($_intNumPregunta){
            $_objPregunta = $this->getRespuestaModel()->obtenerContenidoExamenJson('1001.json');
            $_strTipo = $_objPregunta[$_intNumPregunta]['pregunta']['c03id_tipo_pregunta'];
            $_strRespuestaCorrecta="";
            $_intObjSize = count($_objPregunta[$_intNumPregunta]['respuestas'])-1;
            switch ($_strTipo){
                case '1': case '2': case '3': case '8':
                    foreach ($_objPregunta[$_intNumPregunta]['respuestas'] as $keyObj=>$obj){
                            $_strRespuestaCorrecta .= $obj['t17correcta'];
                    }
                    break;
                case '5':
                    $_strVariante = $this->determinaVarianteAC($_objPregunta[$_intNumPregunta]);
                    $_strSeparadorTipo = $_strVariante === 'matrizHorizontal' || $_strVariante === 'arrastraMatriz' ? "|":"";
                    foreach ($_objPregunta[$_intNumPregunta]['respuestas'] as $keyObj=>$obj){
                        $_strSeperador = $keyObj<$_intObjSize ? $_strSeparadorTipo:"";
                        $_strRespuestaCorrecta .= $obj['t17correcta'].$_strSeperador;
                    }
                    break;
                case '13':
                    foreach ($_objPregunta[$_intNumPregunta]['preguntas'] as $keyObj=>$obj){
//                        $_strSeperador = $keyObj<$_intObjSize ? "|":"";
                        $_strCorrecta = strpos($obj['correcta'], ',') !== false ? '['.$obj['correcta'].']':$obj['correcta'];
                        $_strRespuestaCorrecta .= $_strCorrecta;
                    }
                    break;
                case '15': case '16':
                    if($_strTipo === '16'){
                        sort($_objPregunta[$_intNumPregunta]['respuestas']);
                    }
                    foreach ($_objPregunta[$_intNumPregunta]['respuestas'] as $keyObj=>$obj){
                        $_strSeperador = $keyObj<$_intObjSize ? "|":"";
                        $_strPalabra = $this->eliminaParrafos($obj['t13respuesta']);
                        $_strRespuestaCorrecta .= $_strPalabra.$_strSeperador;
                    }
                    break;
                case '18':
                    foreach ($_objPregunta[$_intNumPregunta]['respuestas'] as $keyObj=>$obj){
                        $_strSeperador = $keyObj<$_intObjSize ? "/":"";
                        $_intRespDerecha = $keyObj+1;
                            $_strRespuestaCorrecta .= $obj['t17correcta'].'_'.$_intRespDerecha.$_strSeperador;
                    }
                    break;
            }
            return $_strRespuestaCorrecta;
        }
        
        public function determinaVarianteAC($objPregunta){
            $variante="";
            if($objPregunta['pregunta']['tipo']===null || $objPregunta['pregunta']['tipo']==='vertical'){
                $variante = 'vertical';
            }
            if ($objPregunta["contenedoresFilas"]!==null) {
                $variante = "arrastraMatriz";
            }
            if($objPregunta['pregunta']['tipo'] === 'horizontal'){
                $variante = 'horizontal';
            }
            if($objPregunta['pregunta']['tipo'] === 'matrizHorizontal') {
                $variante = 'matrizHorizontal';
            }
            if($objPregunta['pregunta']['tipo'] === 'ordenar'){
                $variante = 'ordenar';
            }
            return $variante; 
        }
        public function eliminaParrafos($_strElemento){
            $_strSinParrafo = str_replace(array('<p>', '</p>'), "", $_strElemento);
            return $_strSinParrafo;
        }
        public function shuffleArrayController($array, $cadena) {
            $arrayShuffle = array();
            if ($cadena !== 'NA'){
            $cadenaSplit = explode(',', $cadena);
            for ($i = 0; $i < count($cadenaSplit); $i++) {
                $arrayShuffle[$i] = $array[$cadenaSplit[$i]];
            }
            $array = $arrayShuffle;
            }

            return $array;
        }
        
        public function obtenerSecuenciaRespuestas($_arrPregunta){
            $_arrOrdenInicial = array();
            foreach ($_arrPregunta["respuestas"] as $key => $value) {
                $_arrOrdenInicial[$key] = $key;
            }
            shuffle($_arrOrdenInicial);
            $_strOrdenInicial = implode(",", $_arrOrdenInicial);
            return $_strOrdenInicial;
           
        }

}
