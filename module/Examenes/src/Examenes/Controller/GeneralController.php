<?php
namespace Examenes\Controller;

use Zend\View\Model\ViewModel;
use Zend\Log\Writer\Stream;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Log\Logger;
//use Zend\View\Model\JsonModel;

Class GeneralController extends AbstractActionController
{
    public function __construct(){
        $this->log = new Logger();
        $this->log->addWriter(new Stream("php://stderr"));

        //$_writer = new FirePhp();
        //$this->console = new Logger();
        //$this->console->addWriter($_writer);
        //$this->datos_sesion = new Container('user');
        //$this->sesion_examen = new Container('examen');

    }
    public function indexAction(){
        $id = (int) $this->params()->fromRoute('id', 0);
        $_objServicio = $this->getServiceLocator()->get('Examenes/Model/General');
        $_arrPreguntas = $_objServicio->getPreguntas($id);
        $_arrPreguntasRespuestas = $_objServicio->getRespuestas($id,$_arrPreguntas);
        $count=count($_arrPreguntas);
        $result =  new ViewModel(array(
                        'id'=>$id, 
                        'numero_preguntas'=>$count,
                        'preguntas'=>	$_arrPreguntasRespuestas			
            ));
        return $result;
	}        
}
