<?php
namespace Examenes\Controller;

use Examenes\Entity\Examen;
//use Examenes\Entity\Pregunta;
//use Examenes\Entity\Respuesta;
//use Examenes\Entity\Pregunta;
use Zend\Json\Json;
use Examenes\Model\ExamenModel;
use Examenes\Form\ExamenForm;
use Zend\View\Model\ViewModel;
use Zend\Log\Writer\Stream;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Log\Logger;
use Zend\Log\Writer\FirePhp;
use Zend\Session\Container;
use \HTML2PDF;

/** 
 * Controlador Examenes:
 * -Guarda los examenes con todas sus configuracones.
 * - ...
 * 
 * @author jpgomez
 * @since 21/01/2015
 * @copyright Grupo Educare SA. de CV. derechos reservados.
 * 
 */
class ExamenesController extends AbstractActionController
{
        protected $misdatosModel;
	/**
	 * Para crear instancia del objeto logger.
	 * @var Logger $log;
	 */
	protected $log;
	
	/**
	 * Para crear instancia del objeto logger para firePHP.
	 * @var Logger $console
	 */
	protected $console;
	
	/**
	 * Instancia de la clase Container para el manejo de datos de sesion.
	 * @var Container $datos_sesion.
	 */
	protected $datos_sesion;
        
	/**
	 * Instancia de la clase Container para el manejo de datos de sesion.
	 * @var Container $sesion_examen.
	 */
	protected $sesion_examen;
        
        protected $examenTable;

        /**
	 * Constructor de la clase hace una instancia del objeto logger
	 * y agrega el Writer que se encarga de grabar datos en el log 
	 * de apache, se inicia instancia de _writer para FirePHP (Firebug log 
	 * para navegador) se inicia el objeto console y se agrega el _writer encargado
	 * de grabar en consola de firebug.
	 *  
	 */
	public function __construct()
	{
            $this->log = new Logger();
            $this->log->addWriter(new Stream("php://stderr"));
            
            $_writer = new FirePhp();
            $this->console = new Logger();
            $this->console->addWriter($_writer);
            $this->datos_sesion = new Container('user');
            $this->sesion_examen = new Container('examen');
		
	}
	
	/**
	 * Funcion que pinta la vista para los examenes.
	 * 
	 * @author oreyes
	 * @since 03/12/2014
	 * @access public.
	 * @return ViewModel examen/index.phtml
	 */
	/*public function indexAction()
	{	$config = $this->getServiceLocator()->get('config');
//            $_arrTipos          = $this->getExamenModel()->obtenerTipos();        
  //          $_arrExamenes       = $this->getExamenModel()->obtenerExamenes(0, false, null,$config['constantes']['ESTATUS_ACTIVO']);
    //        $_arrExamenesTipos  = $this->getExamenModel()->mezclaArreglos( $_arrTipos, 'tipo', $_arrExamenes, 'examenes');
            
           // $this->datos_sesion->idUsuarioPerfil = 2 ; // Hardcodeo el Usuario 2, obtener desde el logueo            
            
            return new ViewModel(array(
              //                      'examenes'  => $_arrExamenesTipos
                                    )
                                );
	}*/
	public function indexAction()
	{
            //$config = $this->getServiceLocator()->get('config');     
            
            $_arrTipos          = $this->getExamenModel()->obtenerCategorias();
            $setArrayTipos = array_values($_arrTipos);
            
            foreach ($setArrayTipos as $idCategoria => $val){
                $_arrExamenes   = $this->getExamenModel()->obtenerExamenes( 0, false, intval($val));
            }
        $_arrExamenesTipos  = $this->getExamenModel()->mezclaArreglos($_arrTipos, 'tipo',$_arrExamenes,'examenes');
            return new ViewModel(array(
                                    'examenes'  => $_arrExamenesTipos
                                    )
                                );
	}
                
        public function getDataExamenesAction(){
            //$_arrTipos = $this->getExamenModel()->obtenerTipos();
            $_arrCategorias = $this->getExamenModel()->obtenerCategorias();
            $_arrIdCategorias = array_keys($_arrCategorias);
            
            $_arrResponseTable = array(
                'targetContainer' => 'tipos',
                'classRow'  => 'row',
                'classLarge'=> 'large-4',
                'classSmall'=> 'small-12',
                'classColum' => 'columns',
                'classEnd' => 'end',
                'subTitleClass' => 'subtitleTable',
                'tooltipTitle' => 'Tipo Examen',
                'classIconTitle' => 'fa fa-file',
                'classIconDelete' => '',
                'tooltipDelete' => '',
                'tooltipContent1' => '',
                'classIconContent1' => '',
                'classContent1' => '',
                'tooltipContent2' => '',
                'classIconContent2' => '',
                'classContent2' => '',
                'dataGroup' => array()
                    );
            foreach ($_arrIdCategorias as $idCategoria){
                $_arrResponseTable['dataGroup'][] = array(
                    'id' => $idCategoria,
                    'title' =>  $_arrCategorias[$idCategoria],
                    'subTitle' => '',
                    'content1' => '',
                    'content2' => '',
                    'urlDeleteElement' => '',
                    'urlSubTable' => $this->url()->fromRoute('examenes', array('controller'=>'examenes', 'action'=>'get-examenes-tipo')) ,
                    'postData' =>  json_encode(array('idTipo' => intval($idCategoria))),
                    'buttons' => array(
                        0 => array(
                            'id' => 'btnShowUser-'.$idCategoria,
                            'clase' => 'btnShowUser',
                            'type' => 'showSubTable',
                            'classIcon' => 'fa fa-list ',
                            'text' => '',
                            'toolTip' => 'Mostrar lista de examenes',
                            'action' => '',
                            'subElements' => null
                            )
                        )
                        );
                }
                
                $response = $this->getResponse();
                $response->setContent(Json::encode($_arrResponseTable));
                return $response;
        }
        
        /**
         * 
         */
        public function ligarpreguntasexamenAction(){
            $id = (int) $this->params()->fromRoute('id', 0);
            if (!$id) {
                return $this->redirect()->toRoute('examenes', array(
                    'action' => 'index'
                    ));
            }
            //si el examen ya fue publicada no dejara entrar a la misma y sera redireccionado al index examen
            $examenProp = $this->getExamenTable()->obtenExamen($id);
            $estatus = $examenProp->estatus;
            if ($estatus === 'PUBLICADO')
            {
                return $this->redirect()->toRoute('examenes', array('action' => 'index'));
            }
            //Obtiene todas las relaciones del examen con seccion
            $_arrExamenSeccion = $this->getExamenModel()->obtenerExamenSeccion($id);
            
            //Formatea array de preguntas sección, ayuda a presentar la vista de examen->sección->pregunta.
            $cont = 0;
                foreach ($_arrExamenSeccion as $seccion)
                        {
                            $_arrPregExamen = $this->getPreguntaModel()->obtenerPreguntasExamen($id);
                            $preguntas = $_arrPregExamen['todas'];

                            $_arrPreguntasSeccion[$cont]['seccion']['t20id_seccion_examen'] = $seccion['t20id_seccion_examen'];
                            $_arrPreguntasSeccion[$cont]['seccion']['t15id_seccion'] = $seccion['t15id_seccion'];
                            $_arrPreguntasSeccion[$cont]['seccion']['t15seccion'] = $seccion['t15seccion'];

                            $_arrPreguntasSeccion[$cont]['seccion']['t15valor_porcentaje'] = $seccion['t15valor_porcentaje'];
                            $tipoEv = $seccion['t15resp_total_parcial'] == 'Total' ? 1 : 2;
                            $_arrPreguntasSeccion[$cont]['seccion']['t15resp_total_parcial'] = $tipoEv;
                            //Obtiene las preguntas relacionadas a la seccion original
                            $_arrPregSeccion = $this->getExamenModel()->obtenerSeccionPregunta($seccion['t20id_seccion_examen']);
                            $cont2 = 0;
                            if ($_arrPregSeccion){
                                foreach ($_arrPregSeccion as $value){
                                    $_arrPreguntasSeccion[$cont]['preguntas'][$cont2] ['t11id_pregunta'] = $value['t11id_pregunta'];
                                    $_arrPreguntasSeccion[$cont]['preguntas'][$cont2] ['t11pregunta'] = $preguntas[$value['t11id_pregunta']];
                                    $cont2++;
                                }
                            }

                            $cont++;
                        }
                        
                        
            $_arrPreguntas  = $this->getPreguntaModel()->obtenerPreguntas(0, true);          
            $_arrNombreExam = $this->getExamenModel()->obtenerExamenes($id);
            $_strNombre     = $_arrNombreExam[$id]['value'];
            return new ViewModel(
                array(
                    'preguntas'     => $_arrPreguntas,
                    //'pregsExam'     => $_arrPregExamen['todas'], //se omite para mejroar la vista
                    'pregsExam'     => $_arrPreguntasSeccion,
                    'idExamen'      => $id,
                    'nombre_examen' => $_strNombre
                    )
            );            
        }
        /**
         * @autor rjuarez
         * @return type
         * muestra todas las preguntas(seccion banco de preguntas) en la vista de ligarpreguntasexamen
         */
        public function mostrarpreguntasajaxAction(){
            $_arrPreguntas  = $this->getPreguntaModel()->obtenerPreguntas(0, true);
            $response = $this->getResponse();
            $response->setContent(Json::encode($_arrPreguntas));
            return $response;
             
        }
        /**
         * 
         * @return obtiene todas las preguntas para el banco de preguntas en la opcion de busqueda general
         */
        public function obtenerpreguntagralajaxAction(){
            $request   = $this->getRequest();
            $idExamen = $request->getPost('idExamen'); 
            $_arrPreguntas = $this->getPreguntaModel()->obtenerPreguntaGral($idExamen);
            $response = $this->getResponse();
            $response->setContent(Json::encode($_arrPreguntas));
            return $response;
            
        }
        
        /*public function ligarpreguntasexamenAction(){
            $id = (int) $this->params()->fromRoute('id', 0);
            $_arrPreguntas  = $this->getPreguntaModel()->obtenerPreguntas(0, true);             
            $_arrPregExamen = $this->getPreguntaModel()->obtenerPreguntasExamen($id);
                        
            return new ViewModel(
                array(
                    'preguntas'     => $_arrPreguntas,
                    'pregsExam'     => $_arrPregExamen['todas'],
                    'idExamen'      => $id
                    )
            );            
        }*/
                
	/**
	 * Funcion que pinta la vista para los examenes.
	 * 
	 * @author oreyes
	 * @since 03/12/2014
	 * @access public.
	 * @return ViewModel examen/index.phtml
	 */
	public function misexamenesAction()
	{
            $_intIdUsuarioPerfil      = $this->datos_sesion->idUsuarioPerfil;
            $_correo                  = $this->datos_sesion->correo;
            $_arrGrupos = $this->getExamenModel()->getGruposActivos($_intIdUsuarioPerfil);
            $_arrDatosUsr       = $this->getMisdatosModel()->buscarCorreo($_correo);
            $_arridexausr    = $this->getExamenModel()->getDatosExamendesactivado($_intIdUsuarioPerfil);//trae el idexamenusuario trae 
            if(count($_arrGrupos)>0){
                $_arrRespExamenes   = $this->getExamenModel()->obtenerExamenesUsuario( $_intIdUsuarioPerfil, $_arrGrupos );
            }else{
                $_arrRespExamenes =false;
            }
            
            return new ViewModel(array(
                'examenes'          => $_arrRespExamenes,
                "_arrGrupos"        =>$_arrGrupos,
                "_arrDatosUsr"      =>$_arrDatosUsr,
                '_idUsuarioPerfil'  =>$_intIdUsuarioPerfil,
                '_arridexausr'      =>$_arridexausr)
            );
        }
	
	
	public function getExamenesTipoAction(){
            $config = $this->getServiceLocator()->get('config');
            $_intIdUsuario  = $this->datos_sesion->idUsuarioPerfil;
            $_intIdPerfil  = $this->datos_sesion->perfil;       
            $idCategoria = $this->params()->fromPost('idTipo');
            $_arrExamenes       = $this->getExamenModel()->obtenerExamenes( 0, false, intval($idCategoria),$config['constantes']['ESTATUS_ACTIVO']);
            $_arrEstatusExamenes= $this->getExamenModel();
            $_arrResponseExamenes = array(
                'classRow'  => 'row',
                'classLarge'=> 'large-12',
                'classSmall'=> 'small-12',
                'classSizeContainerButtons' => 'small-3',
                'classColum' => 'columns',
                'classEnd' => 'end',
                'inlineEdit'=> false,
                'dataSubElement' => array()
                    );
            $_arrIdExamenes = array_keys($_arrExamenes);
            foreach($_arrIdExamenes as $id_examen){
                $_arrResponseExamenes['dataSubElement'][] = array(
                    'id'=> $id_examen,
                    'fields'=> array(
                        0 => array(
                            'id' => 'nombre-'.$id_examen,
                            'classSize' => 'small-9',
                            'iconClass' => 'fa fa-file',
                            'labelText' => '',
                            'fieldName' => 'nombre',
                            'name' => 'Nombre',
                            'value' => $_arrExamenes[$id_examen]['value'],
                            'editInline' => false,
                            'urlEdit'=> '',
                            'postData' => '',
                            )
                    ),
                    'buttons' => array(
                        0 => array(
                            'id' => 'btnEdit-'.$id_examen,
                            'tooltip' => 'Editar Examen',
                            'clase'=> '',
                            'type' => 'button',
                            'iconClass' => 'fa fa-pencil',
                            'action' => $this->url()->fromRoute('examenes', array('controller'=>'examenes', 'action' => 'edit', 'id' => $id_examen))
                            ),
                        1 => array(
                            'id' => 'btnDelete-'.$id_examen,
                            'tooltip' => 'Eliminar Examen',
                            'clase'=> 'lnkDeleteExamen',
                            'type' => 'button',
                            'iconClass' => 'fa fa-trash',
                            'action' => 'eliminarExamen('.intval($id_examen).')'
                        ),
                        2 => array(
                            'id' => 'btnPlay-'.$id_examen,
                            'tooltip' => 'Play?',
                            'clase'=> '',
                            'type' => 'link',
                            'iconClass' => 'fa fa-play',
                            'action' => $this->url()->fromRoute('examenes', array('controller'=>'examenes', 'action' => 'examen', 'id' => $id_examen))
                            ),
                        ), 
                    );
                }
	
		$_arrExamenes   = $this->getExamenModel()->obtenerExamenes( 0, false, intval($idCategoria));
		$_arrResponseExamenes = array(
				'classRow'  => 'row',
				'classLarge'=> 'large-12',
				'classSmall'=> 'small-12',
				'classSizeContainerButtons' => 'small-3',
				'classColum' => 'columns',
				'classEnd' => 'end',
				'inlineEdit'=> false,
				'dataSubElement' => array()
		);
		$_arrIdExamenes = array_keys($_arrExamenes);
		foreach($_arrIdExamenes as $id_examen){
                    $_arrResponseExamenes['dataSubElement'][] = array(
                        'id'=> $id_examen,
                        'fields'=> array(
                            0 => array(
                                'id' => 'nombre-'.$id_examen,
				'classSize' => 'small-6',
				'iconClass' => 'fa fa-file',
				'labelText' => '',
                                'fieldName' => 'nombre',
                                'name' => 'Nombre',
                                'value' => $_arrExamenes[$id_examen]['value'],
                                'editInline' => false,
                                'urlEdit'=> '',
                                'postData' => '',
                                ),
                            1 => array(
                                'value' => $_arrExamenes[$id_examen]['estatusExamen'],
                                'classSize' => 'small-2',
                            )
                        ),
                        'buttons' => array(			
                            0 => array(
                                'id' => 'btnAddQuestion-'.$id_examen,
                                'tooltip' => 'Editar Examen',
                                'clase'=> '',
                                'type' => 'button',
                                'iconClass' => 'fa fa-pencil',
                                'action' => 'siExamPublicadaEdit('.intval($id_examen).')'
                                //'action' => $this->url()->fromRoute('examenes', array('controller'=>'examenes', 'action' => 'edit', 'id' => $id_examen))
                            ),
                            1 => array(
                                'id' => 'btnPlay-'.$id_examen,
                                'tooltip' => 'Iniciar evaluación',
                                'clase'=> '',
                                'type' => 'link',
                                'iconClass' => 'fa fa-play',
                                'action' => $this->url()->fromRoute('examenes', array('controller'=>'examenes', 'action' => 'examen', 'id' => $id_examen))
                                ),
                            2 => array( 
                                'id' => 'btnAddQuestion-'.$id_examen,
                                'tooltip' => 'Agregar preguntas',
                                'clase'=> '',
                                //'type' => 'link',
                                'type' => 'button',
                                'iconClass' => 'fa fa-plus',
                                            'action' => 'siExamPublicada('.intval($id_examen).')'
                                            //'action' => $this->url()->fromRoute('examenes', array('controller'=>'examenes', 'action' => 'ligarpreguntasexamen', 'id' => $id_examen))
                            ),
                            3 => array(
                                'id' => 'btnCopia-'.$id_examen,
                                'tooltip' => 'Copiar examen',
                                'clase'=> 'copia-examen-'.$id_examen,
                                'type' => 'button',
                                'iconClass' => 'fa fa-files-o',
                                'action' => 'abrirModalCopiar('.intval($id_examen).')'
                                ),
                            4 => array( 
                                'id' => 'btnJson-'.$id_examen,
                                'tooltip' => 'JSON',
                                'clase'=> '',
                                'type' => 'link',
                                'iconClass' => 'fa fa-file-code-o',
                                            'action' => $this->url()->fromRoute('preguntas', array('controller'=>'preguntas', 'action' => 'muestrapregunta', 'id' => $id_examen))
                            ),
                            5 => array( 
                                'id' => 'btnrebel-'.$id_examen,
                                'tooltip' => 'rebel',
                                'clase'=> '',
                                'type' => 'link',
                                'iconClass' => 'fa fa fa-rebel',
                                            'action' => $this->url()->fromRoute('cargapreguntas', array('controller'=>'cargapreguntas', 'action' => 'muestracargapregunta'))
                            ),
                            )
			);
		}
                if( $_intIdUsuario && $_intIdPerfil!= $config['constantes']['ID_PERFIL_CONSTRUCTOR']){
                    $_arrResponseExamenes = $this->getExamenModel()->filtrarExamenesUsuario( $_arrResponseExamenes, $_intIdUsuario );
                }
		
		$response = $this->getResponse();
		$response->setContent(Json::encode($_arrResponseExamenes));
		return $response;
	}
        
        public function addAction(){
            
            $formExamen     = new ExamenForm();
            $examen         = new Examen();
            $arrTipos       = $this->getExamenModel()->obtenerTipos();        
            $arrEscalas     = $this->getExamenModel()->obtenerEscalas();        
            $arrNiveles     = $this->getExamenModel()->obtenerNiveles();            
            $cols           = $examen->obtenerColumnasExamen();         
            $_arrPreguntas  = $this->getPreguntaModel()->obtenerPreguntas(0, true);             
            //$formExamen->get('creaexamen')->setAttribute('value', $this->url('examenes', array('action' =>'creaexamenajax')));
            //$formExamen->get('cancelar')->setAttribute('href', $this->url('examenes', array('action' => 'index')));
            return new ViewModel(
                array(
                    'formExam'      => $formExamen,
                    'examen'        => $examen,
                    'arrTipos'      => $arrTipos,
                    'arrEscalas'    => $arrEscalas,
                    'arrNiveles'    => $arrNiveles,
                    'cols'          => $cols,
                    'preguntas'     => $_arrPreguntas
                )
           );
        }
        
    public function actualizarCatEtiquetaAction(){
        $requests = $this->getRequest();
        $idExamen           = $requests->getPost('urlIdExamen');
        $arrStrEtiquetas    = $requests->getPost('arrStrEtiquetas');
        $arr_strCategoria   = $requests->getPost('arrStrCategorias');
        $arrStrCategoria =$this->getExamenModel()->actualizaCategoria($idExamen, $arr_strCategoria);
        $arrStrEtiqueta =$this->getExamenModel()->actualizaEtiqueta($idExamen, $arrStrEtiquetas);
        
        $response = $this->getResponse()->setContent(Json::encode(array('arrStrEtiqueta'=>$arrStrCategoria,'arrStrEtiqueta'=>$arrStrEtiqueta)));

            return $response;
    }
        
    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id)
        {
            return $this->redirect()->toRoute('examenes', array(
                'action' => 'add'
            ));
        }
        //si el examen ya fue publicada no dejara entrar a la misma y sera redireccionado al index examen
        $examenProp = $this->getExamenTable()->obtenExamen($id);
        $estatus = $examenProp->estatus;
        if($estatus === 'PUBLICADO'){
            return $this->redirect()->toRoute('examenes', array(    'action' => 'index'));
        }
        // Get the Examen with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $examen = $this->getExamenTable()->obtenExamen($id);
        } catch (\Exception $ex) {
            return $this->redirect()->toRoute('examenes', array(
                        'action' => 'index'
            ));   
        }
        
        $form = new ExamenForm();
        $form->bind($examen);
        $form->get('almacenar')->setAttribute('value', 'Editar')->setAttribute('id', 'boton-editar')->setAttribute('type', 'submit');
        
        //$form->get('cancelarevento')->setAttribute('value', $this->url('examenes', array('action' => 'index')));
        $request = $this->getRequest();
        if ($request->isPost())
        {
            $examen1 = new Examen();
            $form->setInputFilter($examen1->getInputFilter());
            $form->setData($request->getPost());
            //$getpost =$request->getPost();
            $columnas = $examen1->obtenerColumnasExamen();
            foreach ($columnas as $data){
                if($request->getPost($data)){
                    $arrExamen[$data] = $request->getPost($data);
                }          
            }
            
            if ($form->isValid())
            {
                $this->getExamenTable()->guardaExamen($arrExamen,$id); 
                // Redirect to list of albums
                return $this->redirect()->toRoute('examenes');
                //return $this->redirect()->refresh();
            }
        }

        $arrTipos = $this->getExamenModel()->obtenerTipos();
        $arrEscalas = $this->getExamenModel()->obtenerEscalas();
        $arrNiveles = $this->getExamenModel()->obtenerNiveles();
        $exam = new Examen();
        $cols = $exam->obtenerColumnasExamen();
        $_arrPreguntas = $this->getPreguntaModel()->obtenerPreguntas(0, true);
        $_arrPregExamen = $this->getPreguntaModel()->obtenerPreguntasExamen($id);

        $view = new ViewModel(
                array(
            'formExam' => $form,
            'id' => $id,
            //'form'          => $form,
            'arrTipos' => $arrTipos,
            'arrEscalas' => $arrEscalas,
            'arrNiveles' => $arrNiveles,
            'cols' => $cols,
            'preguntas' => $_arrPreguntas,
            'pregsExam' => $_arrPregExamen['todas']
                )
        );
        $view->setTemplate('examenes/examenes/add');
        return $view;
    }

    public function deleteAction(){
            $examen = $this->getExamenModel();
            $id=(int) $this->params()->fromRoute('id',0);
            $examen->eliminarExamen($id);
            
            return $this->redirect()->toRoute('examenes');
            //return new ViewModel(array());
        }  
        
        public function examenAction(){
             $id = (int) $this->params()->fromRoute('id', 0);
             $this->sesion_examen->idExamen = $id ;
            $_arrExamen       = $this->getExamenModel()->obtenerExamenes( $this->sesion_examen->idExamen, true  );            
            $this->log->debug('numIntento: ' . print_r($_arrExamen, true) );
            if(isset($_arrExamen[$id]['c02id_tipo_examen'])){
                $_idUsuario = $this->datos_sesion->idUsuarioPerfil;
                $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_idUsuario, $id);
                 $this->sesion_examen->nombreExamen = $_arrExamen[$id]['t12nombre'];
                 $this->sesion_examen->idNivel = (int)$_arrExamen[$id]['c05id_nivel'];
                 $arrTipos       = $this->getExamenModel()->obtenerTipos();  
                 $_arrExamen[$id]['c02tipo_examen'] = $arrTipos[$_arrExamen[$id]['c02id_tipo_examen']];
                 $_arrExam = $_arrExamen[$id];
                 
                $_intNumIntento = $this->getExamenModel()->obtenerNumeroIntentos($_intIdUsuarioLicencia, $id );
                $this->log->debug('numIntento real shit: ' . print_r($_intNumIntento, true) );
                $_intNumIntento = 1;
                if($_intNumIntento){
                    $this->sesion_examen->numeroIntento = $_intNumIntento;
                    $_arrExam = $_arrExamen[$id];
                    //$this->log->debug( 'tiempo restante: ' . print_r($_arrExam['t12tiempo'], true) );
                    $this->sesion_examen->Tiempo = $_arrExam['t12tiempo'] === '0' ? 
                            $_arrExam['t12tiempo'] : 
                            $this->getExamenModel()->obtenerTiempoRestante($_arrExam['t12tiempo'],$_intNumIntento,$_intIdUsuarioLicencia,$id);
                    $this->log->debug( 'tiempo restante: ' . print_r($this->sesion_examen->Tiempo, true) );
                    $_arrTextos = $this->getExamenModel()->obtenerTextos($_arrExam, $_intNumIntento, $this->sesion_examen->Tiempo);
                    $_arrResponse = array(
                                        'examen'        => $_arrExam,
                                        'textos'        => $_arrTextos,
                                        'numIntentos'   => $_intNumIntento,
                         );                   
                }else{
                    $_arrResponse = $this->getExamenModel()->obtenerTextosError( $_arrExam );
                    $this->sesion_examen->getManager()->getStorage()->clear('examen');
                }
            }else{                 
                $_arrResponse = $this->getExamenModel()->obtenerTextosError();
                $this->sesion_examen->getManager()->getStorage()->clear('examen');
            }
            $view = new ViewModel( $_arrResponse );   
            return $view;
        }
        
        public function migracionAction(){
            
            $config   = $this->getServiceLocator()->get('config');
            $strRuta  = $config['constantes']['RUTA_SITIO'];
            
            /**
             * Paso 1: Obtener examenes de la plataforma de certificaciones
             */
            $examAccithia = $this->getExamenAccithia()->obtenerExamenesAccithia();
            $objExamenes = $this->getExamenAccithia()->convertirExamenes($examAccithia);
            
            /**
             * Paso 2: Insertar examenes en la base de datos de evaluaciones
             */
            /*
            foreach($objExamenes as $elem){
                $_bolActualiza = false;

                $examen = New Examen();
                $examen->intercambiarArray($elem);

                if(!$_bolActualiza){
                    $_idExamen = $this->getExamenModel()->guardarExamen($examen);
                }else{ 
                    //$_idExamen = $this->getExamenModel()->actualizarExamen($examen, $idExamen);
                }            
            }
            */             
            /**
             * Paso 3: Obtener preguntas de la plataforma de certificaciones
             */
            $pregAccithia = $this->getExamenAccithia()->obtenerPreguntasAccithia($strRuta);
            $objPreguntas = $this->getExamenAccithia()->convertirPreguntas($pregAccithia);
 
            /**
             * Paso 4: insertar preguntas y respeustas en la base da datos de evaluaciones
             */  
            /*
            foreach($objPreguntas as $elemPreg){
                $_bolActualiza = false;

                $pregunta = New Pregunta();
                $pregunta->intercambiarArray($elemPreg['pregunta']);

                if(!$_bolActualiza){
                    $_idPregunta = $this->getPreguntaModel()->guardarPregunta($pregunta);
                    foreach ($elemPreg['respuestas'] as $_respuesta)
                    {	
                            $resp = new Respuesta();
                            $resp->intercambiarArray($_respuesta);
                            $_idRespuesta = $this->getRespuestaModel()->guardarRespuesta($resp, $_idPregunta);
                    }      
                }else{ 
                    //$_idExamen = $this->getExamenModel()->actualizarExamen($examen, $idExamen);
                }            
            }
            */
            
            /**
             * Paso 5: Obtener ligas de examenes preguntas de Accithia
             */
            $_arrLigas = $this->getExamenAccithia()->obtenerLigaExamenPregunta();
            
            /**
             * Paso 6: Obtiene preguntas y examenes de valuaciones
             */
            $_arrExamenes       = $this->getExamenModel()->obtenerExamenes(0, true);      
            $_arrPreguntas  = $this->getPreguntaModel()->obtenerPreguntas(0,true);
            
            /**
             * Paso 7: Convierte los ids de accithia a ids de evaluaciones en un 
             * array que nos sirva para ejecutar la funcion de ligar
             */
            $_arrNuevoLigas = $this->getExamenModel()->convierteAccithiaEvaluaciones($_arrLigas, $pregAccithia, $examAccithia, $_arrPreguntas, $_arrExamenes);
            
            /**
             * Paso 8: ligar examenes con preguntas
             */ 
            /*
            foreach( $_arrNuevoLigas as $id=>$datos){
                $arrExamen = array_keys($datos);
                $idExamen = $arrExamen[0];
                $_idLiga = $this->getExamenModel()->ligaPreguntasExamen( $idExamen, $datos[$idExamen] );
            }
             */
             
            return new ViewModel(
                array(
                    'preguntas'     => $_arrNuevoLigas 
                )
            );
            
        }
        
	/**
	 * funcion para crear o actualizar preguntas
	 * - Recibe peticion con los siguientes datos:
         *  objJson, idPregunta
         * El idPregunta sirve para saber si sólo se va a actualizar la pregunta
	 * - Llena la pregunta con sus respectivas respuestas
	 * - Llena la relacion de preguntas y respuestas con la opcion correcta.
	 *
	 *  @author oreyes.
	 *  @since 03/12/2014
	 *  @access public.
	 *
	 */
	public function creaexamenajaxAction()
	{
            $request   = $this->getRequest();
            $arrDataExamen = $request->getPost('arrDataExamen');
            $idExamen = $request->getPost('idExamen');
            $_idExamen  = $this->getExamenModel()->guardarExamen($arrDataExamen['data']);
            $this->getExamenModel()->insertaSeccion($_idExamen, 'General', 0, 0);
            
            
            //$this->console->debug(print_r($_bolOk,true));
                
            $response = $this->getResponse();
            $response->setContent(Json::encode($_idExamen));

            return $response;
	}
        

        function guardarPreguntasAction(){
            $request   = $this->getRequest();
            $arrPreguntas = $request->getPost('objPreguntas');
            $_idExamen = $request->getPost('idExamen');
            foreach( $arrPreguntas as $id => $seccion){
                if(isset($seccion['preguntas'])){
                    $_bolOk = $this->getExamenModel()->ligaPreguntasExamen(
                            $_idExamen,
                            $seccion['preguntas'],
                            $id,
                            $seccion['seccion'],
                            $seccion['porcentaje'],
                            $seccion['tipoEv']);
                }else{
                    $_bolOk = $this->getExamenModel()->ligaPreguntasExamen(
                            $_idExamen,
                            null,
                            $id,
                            $seccion['seccion'],
                            $seccion['porcentaje'],
                            $seccion['tipoEv']);
                }
            }
            $response = $this->getResponse();
            return   $response->setContent(Json::encode($_bolOk));
        }
        
        function eliminaRelPregExamAction(){
            $request    = $this->getRequest();
            $_idPreg    = $request->getPost('id_preg');
            $_idSeccion = $request->getPost('id_seccion');
            $_idExamen = $request->getPost('id_examen');
            
            if($_idExamen){
                $_bolOk = $this->getExamenModel()->eliminaSeccionExamen($_idExamen, $_idSeccion);
            }
            if(is_array($_idPreg)){
                //recibe un arr de preguntas y las manda al banco
                $_bolOk = $this->getExamenModel()->eliminaRelPregExam($_idPreg, $_idSeccion);
            }else{
                //elimina una pregunta y la manda al banco
                $_bolOk = $this->getExamenModel()->eliminaRelPregExam($_idPreg, $_idSeccion);
            }
            
            $response = $this->getResponse();
            return $response->setContent(Json::encode($_bolOk));
        }
	
        public  function finalizaexamenajaxAction(){
            $request   = $this->getRequest();
            $_intIdUsuario  = $this->datos_sesion->idUsuarioPerfil;
            $_intIdExamen   = $this->sesion_examen->idExamen;
            $_intNumIntento  = $this->sesion_examen->numeroIntento; 
            $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_intIdUsuario, $_intIdExamen);
            $_intIdPreguntaActual = $request->getPost('idPreguntaActual');
            $_objRespuesta = $request->getPost('objRespuestas');
            //$_intIdExamen   = $this->sesion_examen->idExamen;
//            //$_intNumIntento = 1;
            
             //$_intNumIntento = $this->getExamenModel()->obtenerNumeroIntentos($_intIdUsuarioLicencia, $_intIdExamen);
             
            
            try {
                $examen = $this->getExamenTable()->obtenExamen($_intIdExamen);
            }
            catch (\Exception $ex) {
                $this->log->debug($ex->getMessage());
                return $this->redirect()->toRoute('examenes', array(
                    'action' => 'index'
                ));
            }
            //$_obtenerDiferenciaTiempo = $this->getRespuestaModel()->obtenerDiferenciaTiempo($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPreguntaActual);
            $this->getRespuestaModel()->guardarRespuestaUsuario($_objRespuesta, $_intIdUsuarioLicencia, $_intIdExamen, $_intIdPreguntaActual, $_intNumIntento);
            $_obtenerFechaFinal = $this->getRespuestaModel()->guardarPreguntaActualiza($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPreguntaActual);
            $_obtenerDiferenciaTiempo = $this->getRespuestaModel()->obtenerDiferenciaTiempo($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPreguntaActual);
            $_bolOk = $this->getExamenModel()->finalizaExamen( $_intIdExamen, $examen, $_intIdUsuarioLicencia, $_intNumIntento);
            
            $response = $this->getResponse();
            return   $response->setContent(Json::encode($_bolOk));            
        }
            
        public function obtenerinterrumpidosajaxAction(){
            $request   = $this->getRequest();
            $_intIdGrupo = $request->getPost('idGrupo');
            $_arrExamenes = $this->getExamenModel()->obtenerExamenesInterrumpidos($_intIdGrupo);
            $response = $this->getResponse();
            return   $response->setContent(Json::encode($_arrExamenes ));              
        }
        
        public function obtenercalificacionesajaxAction(){
            $request   = $this->getRequest();
            $_intIdGrupo = $request->getPost('idGrupo');
            $_arrCalificaciones = $this->getExamenModel()->obtenerCalificacionesGrupo($_intIdGrupo);
            $response = $this->getResponse();
            return   $response->setContent(Json::encode( $_arrCalificaciones ));
        }
        
        public function desbloquearexamenajaxAction(){
            $request   = $this->getRequest();
            $_intIdGrupo = $request->getPost('idGrupo');
            $_intIdExamenUsuario = $request->getPost('idExamenUsuario');
            $this->getExamenModel()->desbloquearExamen($_intIdExamenUsuario);
            $_arrExamenes = $this->getExamenModel()->obtenerExamenesInterrumpidos($_intIdGrupo);
            $response = $this->getResponse();
            return   $response->setContent(Json::encode($_arrExamenes ));              
        }
        
        public function resetearExamenAjaxAction(){
            $this->config['AMBIENTE'] == "DESARROLLO";
            $request   = $this->getRequest();
            $_intIdGrupo = $request->getPost('idGrupo');
            $_intIdExamenUsuario = $request->getPost('idExamenUsuario');
            $this->getExamenModel()->resetearExamen($_intIdExamenUsuario);
            $_arrExamenes = $this->getExamenModel()->obtenerExamenesInterrumpidos($_intIdGrupo);
            $response = $this->getResponse();
            return   $response->setContent(Json::encode($_arrExamenes ));              
        }
        
        public function resetExamAction(){
            $this->config['AMBIENTE'] == "DESARROLLO";
//            $llave = $this->params()->fromRoute('id', 0);
            $request = $this->getRequest();
            $llave = $request->getPost('strLlave');
            $operacion = $this->getExamenModel()->resetExam($llave);
            $response = $this->getResponse();
            return   $response->setContent(Json::encode($operacion));       
	}
        
        function getExamenAccithia()
        {
            $sm         = $this->getServiceLocator();
            $examModel  =  $sm->get('/Examenes/Model/ExamenAccithia');
            return $examModel;
            
        }
     /**
      * 
      * @return \Zend\View\Model\ViewModel
      */ 
     public function plantillaAction()
     {
     	$view = new ViewModel();
     	$view->setTerminal(true);
     	return $view;	
     }
     
     /**
      * se genera funcion temporal con vista que inicia a un examen
      * al introducir funcionalidad se debe cambiar este comentario.
      * 
      * @author oreyes
      * @since 16/02/2015
      * @return \Zend\View\Model\ViewModel
      */
     public function iniciarexamenAction()
     {
     	$view = new ViewModel();
     	$view->setTerminal(true);
     	return $view;
     }
     
     /**
      * Se genera funcion con un comentario temporal hasta el momento en el que se introduce funcionalidad
      * la siguiente funcion crea un examen desde la vista del constructor.
      * @author oreyes
      * @since 16/02/2015
      * @return \Zend\View\Model\ViewModel
      * 
      */
     public function crearexamenAction()
     {
     	$view = new ViewModel();
     	
     	return $view;
     }
     
	/**
	 * Funcion que llama al service manager
	 * y trae la clase ExamenModel.
	 * 
	 * @author jpgomez.
	 * @since 21/01/2015
	 * @access public.
	 * @return ExamenModel $examModel.
	 */
	function getExamenModel() 
	{
            $sm         = $this->getServiceLocator();
            $examModel  =  $sm->get('/Examenes/Model/ExamenModel');
            return $examModel;
	}
        
	/**
	 * Funcion que llama al service manager
	 * y trae la clase PreguntasModel.
	 * 
	 * @author oreyes.
	 * @since 03/12/2014
	 * @access public.
	 * @return PreguntaModel $pregModel
	 */
	public function getPreguntaModel()
	{
	    $sm         = $this->getServiceLocator();
            $pregModel  = $sm->get('/Examenes/Model/PreguntaModel');
            return $pregModel;  

	}
        
        /**
	 * Funcion que llama al service manager
	 * y trae la clase RespuestasModel.
	 * 
	 * @author oreyes.
	 * @since 03/12/2014
	 * @access public.
	 * @return RespuestaModel $respModel.
	 */
	public function getRespuestaModel() 
	{
            $sm         = $this->getServiceLocator();
            $respModel  =  $sm->get('/Examenes/Model/RespuestaModel');
            return $respModel;
	}
        
	/**
	 * Funcion que llama al service manager
	 * y trae la clase ExamenTable.
	 * 
	 * @author jpgomez.
	 * @since 03/02/2015
	 * @access public.
	 * @return ExamenTable $examTable.
	 */
	function getExamenTable() 
	{
            if(!$this->examenTable){
                $sm = $this->getServiceLocator();
                $this->examenTable= $sm->get('Examenes\Model\ExamenTable');
            }
            return $this->examenTable;
            /*$sm         = $this->getServiceLocator();
            $examTable  =  $sm->get('/Examenes/Model/ExamenTable');
            return $examTable;*/
	}
        
        /**
         * Action que controla la copia parcial/completa de un examen.
         * 1.- Copia Parcial - Es una réplica del examen sin copiar las preguntas del original.
         * 2.- Copia completa - Duplica el examen junto con las preguntas del original.
         * 
         * @author lcencieros.
	 * @since 29/05/2015
	 * @access public.
         * @return boolean
         */
        function copiarexamenAction() 
	{
            
            $_bolExClon = false;
            $request   = $this->getRequest();
            $idExamen = $request->getPost('_idExamen');
            $opCopia = $request->getPost('_opCopia');
            
            //Obtiene todos los datos del examen para clonar
            $_examen = $this->getExamenModel()->obtenerExamenes($idExamen,true);
            $arrTmp = array_keys($_examen);
            $_examen = $_examen[$arrTmp[0]];
            //Obtiene todas las relaciones del examen con seccion
            $_arrExamenSeccion = $this->getExamenModel()->obtenerExamenSeccion($idExamen);
            //Obtiene las secciones del examen
            $_arrSeccion = $this->getExamenModel()->obtenerSeccion($idExamen);
            $_arrRelCategoria = $this->getExamenModel()->getCategoriasEdit($idExamen);
            
            $_arrRelEtiqueta  = $this->getExamenModel()->getEtiquetasEdit($idExamen);
            
            $_bolExClon  = $this->getExamenModel()->clonarExamen($_examen, $_arrSeccion, $opCopia,$_arrRelCategoria,$_arrRelEtiqueta);
           
            switch ($opCopia) {
                case '1':
                    if($_bolExClon == true){
                        $msg ='Copia parcial terminada satisfactoriamente.';
                    }else{
                        $msg ='Hubo problemas, no se completó la copia.';
                    }
                    $jsonRepuesta = array('_bolClon' => $_bolExClon, '_msg' => $msg);
                    break;
                    
                case '2':
                    if($_bolExClon == true){
                        $msg ='Copia completa terminada satisfactoriamente.';
                    }else{
                        $msg ='Hubo problemas, no se completó la copia.';
                    }
                    $jsonRepuesta = array('_bolClon' => $_bolExClon, '_msg' => $msg);
                    break;
                    
                default:
                    $msg ='Opción de copia no encontrada.';
                    
                    $jsonRepuesta = array('_bolClon' => false, '_msg' => 'Evento default');
                    break;
            }
            
            $response = $this->getResponse();
            return   $response->setContent(Json::encode($jsonRepuesta));
	}

    public function getRegistroModel()
    {
    	$sm = $this->getServiceLocator();
    	$regModel = $sm->get('/Login/Model/RegistroModel');
    	return $regModel;
    }
    public function getinstanceModel() 
	{
		if (!$this->misdatosModel) {
			$sm = $this->getServiceLocator();
			$this->misdatosModel = $sm->get('Misdatos\Model\MisdatosModel');
		}
		return $this->misdatosModel;
	}
        public function reactivaExamAction() {
//            error_log('entra');
//              AND I'M FEELING GOOD
//		$request = $this->getRequest ();
//		$_strCorreo = $request->getPost ( '_strCorreo' );
//                			$url = $this->url ()->fromRoute ( 'login', array (
//					'controller' => "login",
//					"action" => "reactivar-exam" 
//			), array (
//					'force_canonical' => true 
//			) );
//              $resp=$this->getExamenModel()->enviaCorreo ($_strCorreo, $url )
		$request = $this->getRequest ();
		$_strCorreo = $request->getPost ( '_strCorreo' );
                $_strDatosalumno = $request->getPost ( '_strDatosalumno' );
                $_strCorreoalumno = $request->getPost ( '_strCorreoalumno' );
                $_stridExaUser= $request->getPost ('_stridExaUser');
                $_stridUserPer= $request->getPost ( '_stridUserPer');
		$_bolRecuperaPass = true;
		$_strCorreo = trim ( $_strCorreo );
		
		// validar que exista corre
		$_arrExisteCorreo = $this->getRegistroModel()->existeCorreo ( $_strCorreo, $_bolRecuperaPass );
		
		//$this->_objLogger->debug ( "VALOR DE EXISTE CORREO-> " . count ( $_arrExisteCorreo ) );
		
		$_boolExisteCorreoUser = $this->getRegistroModel()->existeCorreoUser ( $_strCorreo );
		
		//$this->_objLogger->debug ( " CORREO  2-> " . print_r ( $_boolExisteCorreoUser, true ) );
		$_strMensaje = isset ( $_boolExisteCorreoUser ) && count ( $_boolExisteCorreoUser ) > 0 ? true : false;
		
		if (isset ( $_arrExisteCorreo ) && count ( $_arrExisteCorreo ) > 0) {
			//$this->_objLogger->debug ( "PASO 1 " );
			// obtener la ruta absoluta y pasarla al model para envarla en el correo
			$url = $this->url ()->fromRoute ( 'examenes', array (
					'controller' => "examenes",
					"action" => "reactivaexamen" 
			), array (
					'force_canonical' => true 
			) );
			
			$_arrDatosUsuario = $this->getRegistroModel()->generaCodigoRecuperacion ( $_strCorreo,$_strDatosalumno,$_strCorreoalumno,$_stridExaUser,$_stridUserPer, $url,false );
			
			// $this->_objLogger->debug("PASO 2 ".print_r($_arrDatosUsuario,true));
			
			if (isset ( $_arrDatosUsuario ) && count ( $_arrDatosUsuario ) > 0) {
				$_strnombre = $_arrDatosUsuario ['nombre'];
				$_strCodigo = $_arrDatosUsuario ['codigo'];
				$_arrRespuesta ['respuesta'] = true;
				$_arrRespuesta ['msg'] = "Se ha enviado un e-mail a tu cuenta de correo con las instrucciones necesarias para resetear tu contrase&ntilde;a.";
			} else {
				
				//$this->_objLogger->debug ( "PASO else 2 " );
				$_arrRespuesta ['respuesta'] = false;
				$_arrRespuesta ['msg'] = "Hubo un error en el sistema, favor de intentar m&aacutes tarde";
			}
		} else {
			$_arrRespuesta ['respuesta'] = false;
			$_arrRespuesta ['msg'] = "La direcci&oacute;n de correo electronico ingresada no existe en nuestros registros.";
		}
		
		// Se manda llamar el correo
		$response = $this->getResponse ();
		$response->setContent ( \Zend\Json\Json::encode ( $_arrRespuesta ) );
		return $response;
	}
    
        
        /**
         * 
         * vista para cambiar el pass
         */
        function reactivaexamenAction() {
		$request = $this->getRequest ();
		$_strCodigo = $request->getQuery ( 'idCodigo' );
                $_stridExaUser= $request->getQuery ( 'idExaUser');
                $_stridUserPer= $request->getQuery ( 'idUserPer');
                $arrDatosUser=$this->getExamenModel()->getDatosAlumno($_stridUserPer);
		// verificar que exista el codigo en la tabla
		$arrRespCodigo = $this->getRegistroModel()->checkCodeRecuperacion ( $_strCodigo );
		if (count ( $arrRespCodigo ) > 0) {
			//$this->_objLogger->debug ( "VALOR DE CODIGO " . $_strCodigo );
			$viewModel = new ViewModel ( array (
					'_strCodigo' => $_strCodigo,
                                        'idUserPer'=>$_stridUserPer,
                                        'idExaUser'=>$_stridExaUser,
                                        'DatosUser'=>$arrDatosUser,
			) );
			//$viewModel->setTerminal ( true );
			return $viewModel;
		} else {
			$viewModel = new ViewModel ();
			//$viewModel->setTerminal ( true );
			$viewModel->setTemplate ( "error/errorReactiva" );
			return $viewModel;
		}
	}
        
        /**
	 * reactiva examen desde correo
	 * 
	 * @access public
	 * @author LAFC
	 *        
	 */
	public function reactivaExamenCorreoAction() {
		$request = $this->getRequest ();
		$_intIdExamenUsuario = $request->getPost ( "_idExaUser" );
		$_strCode = $request->getPost ( "_strCodigo" );
		
		// volver a verificar si existe el codigo y obbtener los datos del usuario
		$_arrCodigo = $this->getRegistroModel()->checkCodeRecuperacion ( $_strCode );
               $_boolCodigo = $this->getRegistroModel()->changeCodeRectiva($_strCode);
		if (isset ( $_intIdExamenUsuario )  && count ( $_arrCodigo ) > 0) {
			$_boolResp = $this->getExamenModel()->desbloquearExamen ( $_intIdExamenUsuario );
			
			
                        $response = $this->getResponse ();
			$response->setContent ( \Zend\Json\Json::encode ( $_boolResp,$_boolCodigo ) );
			return $response;
		}
		
		// TODO : regresar error
		$response = $this->getResponse ();
		$response->setContent ( \Zend\Json\Json::encode ( false ) );
		return $response;
	}
        
        
        public function getCategoriasAction(){
            
            $like = trim($this->params()->fromQuery('term'));
            
            $arrCategorias = $this->getExamenModel()->getCategorias($like);
            $_arrRespuesta = array();
            foreach ($arrCategorias as $cat){
                array_push($_arrRespuesta, $cat['c08nombre']);
            }
            $response = $this->getResponse ();
            $response->setContent ( \Zend\Json\Json::encode ( $_arrRespuesta ) );
            return $response;
        }
	public function generarPdfAjaxAction(){
            $request = $this->getRequest();
            $_arrDatos = array();
            $_arrDatos['nombre_completo']   = $request->getQuery('_strNombreCompleto');
            $_arrDatos['grado']             = $request->getQuery('_intGrado');
            $_arrDatos['llave']             = $request->getQuery('_strLlave');
            $_strNombreDoc = 'Reconocimiento';
            if($_arrDatos['grado'] == 6){
                $_objImg1 = __DIR__.'/../../../../../public/images/6prim/Titulo.png';
                $_objImg3 = __DIR__.'/../../../../../public/images/6prim/Pleca_lateral.png';
                $_strReconocimiento = "reconocimiento_6_pdf";
                $_strNombreDoc = 'Certificacion';
            }else{
                if($_arrDatos['grado'] < 6){
                    $_objImg1 = __DIR__.'/../../../../../public/images/1_5prim/Titulo.png';
                    $_objImg3 = __DIR__.'/../../../../../public/images/1_5prim/Placa_lateral.png';
                    $_strReconocimiento = "reconocimiento_pdf";                    
                }
            }
            $_objImg2 = __DIR__.'/../../../../../public/images/1_5prim/slogan.png';
            $_objImg4 = __DIR__.'/../../../../../public/images/1_5prim/Logo_GE.png';
            $_objImg5 = __DIR__.'/../../../../../public/images/1_5prim/firma_RCR.jpg';
            $_objImg6 = __DIR__.'/../../../../../public/images/1_5prim/placa_logos.png';

            $html2pdf = new HTML2PDF('L', 'LETTER', 'es', true, 'UTF-8', array(3,3,3,3));
            ob_start();

            require  __DIR__ . '/../../../view/examenes/examenes/'.$_strReconocimiento.'.phtml';

            $content = ob_get_clean();

            $_strNombreArchivo =  $_strNombreDoc . "_".$_arrDatos['nombre_completo']."_".$_arrDatos['llave']."_".date("d-m-Y__H-i-s").".pdf";
            //$_urlArchivo   =  __DIR__ ."/../../../../../public/uploads/".$_strNombreArchivo;
            //$_objArchivoPdf = array("nombre" =>$_strNombreArchivo,"direccion" => $_urlArchivo);
            try{
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($content, false);
                //$html2pdf->Output($_urlArchivo,'F');
                $html2pdf->Output($_strNombreArchivo, 'D');
            } catch(HTML2PDF_exception $e) {
                echo $e;
                exit();
                $_strNombreArchivo =  false;
            }
	}
        
        
        public function getTagsAction(){
            
            $like = trim($this->params()->fromQuery('term'));
            
            $arrTags = $this->getExamenModel()->getEtiquetas($like);
            $_arrRespuesta = array();
            foreach ($arrTags as $tag){
                array_push($_arrRespuesta, $tag['c09nombre']);
            }
            $response = $this->getResponse ();
            $response->setContent ( \Zend\Json\Json::encode ( $_arrRespuesta ) );
            return $response;
        }
        public function obtenerCategoriasEditAction(){
            $request = $this->getRequest();
            $idExamen = $request->getPost('idPregunta');
            $_bolOk = $this->getExamenModel()->getCategoriasEdit($idExamen);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolOk));
            return $response;
        }         
        public function obtenerEtiquetaEditAction(){
            $request = $this->getRequest();
            $idExamen= $request->getPost('idPregunta');
            $_bolOk = $this->getExamenModel()->getEtiquetasEdit($idExamen);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolOk));
            return $response;
        }
        
        public function publicarExamenAction(){
            $request = $this->getRequest();
            $idExamen = $request->getPost('idExamen');
            $_bolOk = $this->getExamenModel()->publicarExamen($idExamen);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_bolOk));
            return $response;
        }
        
        public function obtenercalificacionExamenAction(){
    	$request = $this->getRequest();
    	$_intIdExamen = $request->getPost('_idexamen');
    	$_intIdExamenUsuario = $request->getPost('_idExamenUsuario');
        $_strClaveEstatus  = $request->getPost('_claveEstatus');
        $_intIdGrupo = $request->getPost('_idGrupo');
        if($_strClaveEstatus == 'APROBADO' || $_strClaveEstatus == 'REPROBADO'){
            $_calificacionExamen = $this->getExamenModel()->obtenerCalificacion($_intIdExamen,$_intIdExamenUsuario);
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_calificacionExamen));
        }else if($_strClaveEstatus == 'NODISPONIBLE' || 
                 $_strClaveEstatus == 'INTERRUMPIDOVENCIDO' ||
                 $_strClaveEstatus == 'INTERRUMPIDORESOLVER'){
                        $_strFechaExamen = $this->getExamenModel()->obtenerFechaExamen($_intIdExamen, $_intIdGrupo);
                        $response = $this->getResponse();
                        $response->setContent(\Zend\Json\Json::encode($_strFechaExamen));
        }
        return $response;
    }
	public function getMisdatosModel()
	{
	    $sm         = $this->getServiceLocator();
            $pregModel  = $sm->get('/Misdatos/Model/MisdatosModel');
            return $pregModel;  

    }
    
    /**
     * Funcion que carga la vista de instrucciones para exámenes todo
     * @author lceniceros
     *
     * @return obj view
     */
    public function examenTodoAction(){
        if($this->datos_sesion->sesionActiva){ 
            $idExamen = (int) $this->params()->fromRoute('id', 0);

            $_arrExamen       = $this->getExamenModel()->obtenerExameneTODO( $idExamen, true  );    

        $this->log->debug('numIntento: ' . print_r($_arrExamen, true) );
            $_idUsuario = $this->datos_sesion->idUsuarioPerfil;
            $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_idUsuario, $idExamen);
            $this->sesion_examen->nombreExamen = $_arrExamen[$idExamen]['t12nombre'];
            //$this->sesion_examen->idNivel = (int)$_arrExamen[$idExamen]['nivel_escolar'];

            $arrTipos       = $this->getExamenModel()->obtenerTipos();  
            $_arrExamen[$idExamen]['c02tipo_examen'] = $arrTipos[$_arrExamen[$idExamen]['c02id_tipo_examen']];
            $_arrExam = $_arrExamen[$idExamen];
            
            $_intNumIntento = $this->getExamenModel()->obtenerNumeroIntentos($_intIdUsuarioLicencia, $idExamen );
            $this->log->debug('numIntento real shit: ' . print_r($_intNumIntento, true) );

            if($_intNumIntento){
                $this->sesion_examen->numeroIntento = $_intNumIntento;
                $_arrExam = $_arrExamen[$idExamen];
                //$this->log->debug( 'tiempo restante: ' . print_r($_arrExam['t12tiempo'], true) );
                $this->sesion_examen->Tiempo = $_arrExam['t12tiempo'] === '0' ? 
                        $_arrExam['t12tiempo'] : 
                        $this->getExamenModel()->obtenerTiempoRestante($_arrExam['t12tiempo'],$_intNumIntento,$_intIdUsuarioLicencia,$idExamen);
                $this->log->debug( 'tiempo restante: ' . print_r($this->sesion_examen->Tiempo, true) );
                $_arrTextos = $this->getExamenModel()->obtenerTextos($_arrExam, $_intNumIntento, $this->sesion_examen->Tiempo);
                $_arrResponse = array(
                                    'examen'        => $_arrExam,
                                    'textos'        => $_arrTextos,
                                    'numIntentos'   => $_intNumIntento,
                                    'examenTodo' => true
                    );                   
            }else{
                $_arrResponse = $this->getExamenModel()->obtenerTextosError( $_arrExam );
                $this->sesion_examen->getManager()->getStorage()->clear('examen');
            }

            $view = new ViewModel($_arrResponse);
            return $view;
        }else{
            $view = new ViewModel();
            $view->setTemplate('error/404');
            return $view;
        }
    }

    /**
     * Carga la vista para responder examen
     *
     * @return obj view
     */
    public function completarExamenTodoAction(){
        if($this->datos_sesion->sesionActiva){
            $idExamen = (int) $this->params()->fromRoute('id', 0);
            $_arrExamen       = $this->getExamenModel()->obtenerExameneTODO( $idExamen, true  );
            $_intIdUsuario  = $this->datos_sesion->idUsuarioPerfil;

            $_intIdUsuarioLicencia = $this->getExamenModel()->obtenerUsuarioLicencia($_intIdUsuario, $idExamen);

            $_strTiempo     = $this->sesion_examen->Tiempo;
           // error_log('_strTiempo: '.print_r($_strTiempo,true));
            $_arrTiepo = json_encode($_strTiempo);
            
                    
            $strtIdActividad = $this->getExamenModel()->obtenerIdActividad($idExamen);
            $_strExamen     = $_arrExamen[$idExamen]['t12nombre'];

            if(isset($strtIdActividad)){
                $_arrExamenAlumno       = $this->getExamenModel()->getPMessage( $_intIdUsuarioLicencia );
                
                if($_arrExamenAlumno['t34estatus'] == null || $_arrExamenAlumno['t34estatus'] == 'Activo'){
                    $this->getExamenModel()->iniciaExamen($_intIdUsuarioLicencia, $idExamen, $_arrExamen[$idExamen]['t12num_intentos']);     
                }
                $_arrResponse=array
                            (
                                'pctAvance'     =>  0,
                                'actual'        =>  1,
                                'marcada'       =>  false,
                                'strtIdActividad'          => $strtIdActividad,
                                'tiempo'    => $_arrTiepo,
                                'nombreExamen'  =>  $_strExamen,
                                '_intIdUsuarioLicencia'  =>  $_intIdUsuarioLicencia,

                            );
                
                $view = new ViewModel($_arrResponse );
                //$view ->setTerminal(true);
                return $view;
                
            }else{
                $view = new ViewModel();
                $view->setTemplate('error/404');
                return $view;
            }
        }else{
            $view = new ViewModel();
            $view->setTemplate('error/404');
            return $view;
        }
    }

    /**
     * Función que regresa meta datos del examemen en ejecución.
     * respuestaUsuario y orden se regresan vacíos cuando el examen recien se ha iniciado.
     * @return json PMessage
     */
    public function getPMessageAjaxAction(){
        $request           = $this->getRequest();
        $intIdLicenciaUsr =trim($request->getPost('idLicenciaUsr'));        

        $_arrExamen       = $this->getExamenModel()->getPMessage( $intIdLicenciaUsr );

        if($_arrExamen['t34estatus'] == null || $_arrExamen['t34estatus'] == 'Activo'){
            $arrMeta['orden'] = '';
            $arrMeta['correcta'] = '';
            $arrMeta['respuesta'] = '';
            $arrMeta['timer'] = $_arrExamen['t12tiempo'];
            
            $boolExamen       = $this->getExamenModel()->insertMetaExamen( $intIdLicenciaUsr, $arrMeta );
            $_arrExamen       = $this->getExamenModel()->getPMessage( $intIdLicenciaUsr );
        }
        
        $boolExamen =  count($_arrExamen) > 0 ? true : false;

        if($boolExamen){
            $responseData = array(
                'bool' => $boolExamen,
                'calificacion' => $_arrExamen['t34calificacion'],
                'respuestaUsuario' => $_arrExamen['t34respuesta'],
                'correcta' => $_arrExamen['t34correcta'],
                'orden' => $_arrExamen['t34orden']
            );
        }else{
            $responseData = array(
                'bool' => $boolExamen
            );
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($responseData));
        return $response;
    }

    /**
     * Función que iserta meta datos del examemen en ejecución, cuando recien se ha iniciado,
     * actualiza meta datos del examemen en ejecución, cuando es un examen ya iniciado.
     *
     * @return json respuesta de evento (bool -> true/false, msj->mensaje)
     */
    public function setPMessageAjaxAction(){
        $request           = $this->getRequest();
        $intIdLicenciaUsr =trim($request->getPost('idLicenciaUsr'));        
        $arrObjMeta =$request->getPost('objMeta');        

        $_arrExamen       = $this->getExamenModel()->getPMessage( $intIdLicenciaUsr );

        if($_arrExamen['t34estatus'] != null){
            $_respuestaMeta       = $this->getExamenModel()->updtMetaExamen( $intIdLicenciaUsr, $arrObjMeta );
        }else{
            $_respuestaMeta = array('bool'=>false, 'msj'=> 'Reg no actualizado.');
        }

        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($_respuestaMeta));
        return $response;
    }

    /**
     * Finaliza el examen resuelto, calcula calificación promedio y actualiza meta datos de examen.
     *
     * @return json respuesta de evento (bool -> true/false, calif->calificación final de examen, msj->mensaje)
     */
    public function finalizaExamenTodoAjaxAction(){
        $request           = $this->getRequest();
        $intIdLicenciaUsr =trim($request->getPost('idLicenciaUsr'));            
        $calificacion =trim($request->getPost('metaCal'));    //TODO: variable recibida temporalmente desde front
        
        $_arrExamen = $this->getExamenModel()->getPMessage( $intIdLicenciaUsr );  

        //TODO: reactivar cuando la calificacion no se recibida desde front
       //$calificacion = $this->evaluarExamen($_arrExamen['t34respuesta'], $_arrExamen['t34correcta']); 

        $_boolCalifExamen = $this->getExamenModel()->finalizarExamenTodo( $intIdLicenciaUsr, $calificacion, $_arrExamen['t12minimo_aprobatorio']);
        
        $_respuesta = array(
            'bool' => $_boolCalifExamen['bool'],
            'calif' => $calificacion,
            'msj' => $_boolCalifExamen['msj'],
        );

        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($_respuesta));
        return $response;
    }

    /**
     * Calcula promedio de examen resuelto
     *
     * @param [string] $strRespuesta
     * @param [string] $strCorrecta
     * @return void
     */
    public function evaluarExamen($strRespuesta, $strCorrecta)
    {
        $reactivoOk = 0;
        $reactivoFail = 0;
        $ponderacion= 100; //TODO: definir como constante o hacer dinámico

        $arrRspuesta = explode('-',$strRespuesta);
        $arrCorrecta = explode('-',$strCorrecta);

        foreach ($arrRspuesta as $key => $value) {
            if($arrCorrecta[$key] == $value){
                $reactivoOk++;
            }
        }

        $total = $reactivoOk*$ponderacion;//
        $promedio = number_format ($total / (count($arrCorrecta)),1);

        return $promedio;
    }
}
