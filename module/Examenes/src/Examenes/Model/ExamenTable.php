<?php
namespace Examenes\Model;
use Zend\Db\TableGateway\TableGateway;
class ExamenTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function extraeTodo(){
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function obtenExamen($id_exam) {
        $id = (int) $id_exam;
        error_log('id: ' . $id_exam);
        $rowset = $this->tableGateway->select(array('t12id_examen' => $id));
        $row = $rowset->current();
        if(!$row){
            throw new \Exception("No se encontró el registro $id");
        }
        return $row;
    }
    
    public function guardaExamen($examen, $id_examen) {
        
        $data = array(
            //'t12id_examen'             => $examen['id_examen'],
            'c02id_tipo_examen'        => $examen['id_tipo_examen'],
            'c04id_escala'             => $examen['id_escala'],
            'c05id_nivel'              => $examen['id_nivel'],
            't12nombre'                => $examen['nombre'],
            't12nombre_corto_clave'    => $examen['nombre_corto_clave'],
            't12instrucciones'         => $examen['instrucciones'],
            't12descripcion'           => $examen['descripcion'],
            't12fecha_registro'        => $examen['fecha_registro'],
            't12fecha_actualiza'       => $examen['fecha_actualiza'],
            't12num_intentos'          => $examen['num_intentos'],
            't12aleatorio_preguntas'   => $examen['aleatorio_preguntas'],
            't12demo'                  => $examen['demo'],
            't12minimo_aprobatorio'    => $examen['minimo_aprobatorio'],
            't12tiempo'                => $examen['tiempo'],
            //'t12estatus'             => $examen[estatus]
        );
        
        //$id = (int) $examen['id_examen'];
        $id = (int) $id_examen;
        //$id = (int) $examen->id_examen;
        //$this->tableGateway->select(array('t12id_examen' => $id));
        if ($id == 0) {
            
            //$this->tableGateway->insert($data);
        } else {
            if ($this->obtenExamen($id)) {
                $this->tableGateway->update($data, array('t12id_examen' => $id));
            } else {
                throw new \Exception('El examen no existe');
            }
        }
    }
    
    public function borrarExamen($id){
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}