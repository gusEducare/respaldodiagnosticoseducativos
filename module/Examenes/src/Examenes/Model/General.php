<?php
namespace Examenes\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Log\Logger;
use Zend\Db\Sql\Sql;
use Zend\Log\Writer\Stream;
use Zend\Db\ResultSet\ResultSet;

class General {
	
	protected $sql;
	protected $adapter;
	protected $_objLogger;
	protected $config;
	
	
	public function __construct(Adapter $adapter,$config){
		$this->adapter = $adapter;
		$this->sql= new Sql($adapter);
		$this->config = $config;
		$this->_objLogger = new Logger();
		$this->_objLogger->addWriter(new Stream('php://stderr'));
		
	}
        public function getPreguntas($_strName){
                $_strQuery = 'select p.t11pregunta, p.t11id_pregunta from
                                        t11pregunta p
                                        inner join t16seccion_pregunta s on s.t11id_pregunta = p.t11id_pregunta
                                        inner join t20seccion_examen se on se.t20id_seccion_examen =  s.t20id_seccion_examen
                                        inner join t12examen e on e.t12id_examen = se.t12id_examen
                                        where e.t12id_examen = :_strName 
                                order by s.t16orden_pregunta,p.t11nombre_corto_clave' ;

                $statement = $this->adapter->query($_strQuery);
                $results = $statement->execute(array(
                                ":_strName"=> $_strName
                ));
                $resultSet = new ResultSet;
                $resultSet->initialize($results);
                $_arrPreguntas = $resultSet->toArray();
                return $_arrPreguntas;
        }

        public function getRespuestas($_strName,$_arrPreguntas){
                $_strQuery = 'select p.t11id_pregunta,p.t11pregunta,r.t13respuesta, pr.t17correcta from  t17pregunta_respuesta pr 
                                        inner join t11pregunta p on p.t11id_pregunta = pr.t11id_pregunta 
                                        inner join t13respuesta r on pr.t13id_respuesta = r.t13id_respuesta
                                        inner join t16seccion_pregunta s on s.t11id_pregunta = p.t11id_pregunta
                                        inner join t20seccion_examen se on se.t20id_seccion_examen =  s.t20id_seccion_examen
                                        inner join t12examen e on e.t12id_examen = se.t12id_examen
                                        where e.t12id_examen = :_strName 
                                        order by t11nombre_corto_clave, t13orden' ;

                $statement = $this->adapter->query($_strQuery);
                $results = $statement->execute(array(
                                ":_strName"=> $_strName
                ));
                $resultSet = new ResultSet;
                $resultSet->initialize($results);
                $_arrRespuesta = $resultSet->toArray();

                foreach ($_arrPreguntas as $indice=>$preguntas){
                        $_arrRespuestasPregunta=array();
                         $_arrRespuestasPreguntaCorrecta=array();
                        $i=0;
                        foreach ($_arrRespuesta as $respuesta){
                                if($preguntas['t11id_pregunta']==$respuesta['t11id_pregunta']){

                                        $_arrRespuestasPregunta[$i]=$respuesta['t13respuesta'];
                                        $_arrRespuestasPreguntaCorrecta[$i]=$respuesta['t17correcta'];
                                        
                                        $i++;
                                }	

                        }
                        $_arrPreguntas[$indice]['respuestas']=$_arrRespuestasPregunta;
                        $_arrPreguntas[$indice]['correcta']=$_arrRespuestasPreguntaCorrecta;
                }
                return $_arrPreguntas;
        }
		
}