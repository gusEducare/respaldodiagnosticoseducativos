<?php

namespace Examenes\Model;

//use Examenes\Entity\Pregunta;
use Examenes\Form\PreguntaForm;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Log\Writer\FirePhp;
use Examenes\Model\ExamenModel;
use ZipArchive; 

final class EstatusPregunta{
    const ACTIVA = 1;
    const INACTIVA = 2;
}

final class EstatusPreguntaMarcada{
    const MARCADA = 'Marcada';
    const DESMARCADA = 'Desmarcada';
}

/**
 * Clase encargada en la creación de preguntas, con todos sus metodos y
 * atributos.
 *
 * @author jpgomez@grupoeducare.com.
 * @author oreyes@grupoeducare.com.
 * @since 02-12-2014
 * @copyright Grupo Educare SA de CV derechos reservados.
 *
 */
class PreguntaModel{
	
	/**
	 * Instancia de de la clase de SQL util para la conexión a la Base de datos
	 * @var Sql $sql  :
	 */
	protected $sql;

	/**
	 * Instancia de la clase Logger util para poder depurar en la consola de
	 * apache
	 * @var  Logger
	 */
	protected $_objLog;
	
	/**
	 * Instancia de la clase logger para firebug.
	 * @var Logger $log
	 */
	protected $log;
        /**
	 * 
	 * @var Adapter $adapter.
	 */
	protected $adapter;
	
	/**
	 * 
	 * @var Connection $conection.
	 */
	protected $conection;
        
        /**
	 * 
	 * @var Array configuracion plataforma 
	 */
	protected $config;


	/**
	 * Constructor de clase inicia el adapter, crea instancia del objeto Logger.
	 *
	 * @param Adapter $adapter
	 */
        
	public function __construct(Adapter $adapter, $config){
		$this->adapter = $adapter;
		$this->sql = new Sql($adapter);
		$this->conection = $this->adapter->getDriver()->getConnection();
		$this->_objLog = new Logger();
		$this->_objLog->addWriter(new Stream('php://stderr'));
		$this->config = $config;
		$_writer = new FirePHP();
		$this->log = new Logger();
		$this->log->addWriter($_writer);
	}
	
	/**
	 * Funcion para guardar objetos pregunta en base de datos.
	 * @param unknown $_pregunta
	 * @return \Zend\Db\Adapter\Driver\StatementInterface
	 */
	public function guardarPregunta(\Examenes\Entity\Pregunta $_pregunta,$str_Categoria,$str_Etiqueta,$_idPregunta = null)
	{
            $this->adapter->getDriver()->getConnection()->beginTransaction();
            $_boolReturn = true;
            
            //crear categorias si no existen
            $modelExamen = new \Examenes\Model\ExamenModel($this->adapter, $this->config);
            $_arrIdsCategorias = $modelExamen->createCategorias($str_Categoria);
            
            if(count($_arrIdsCategorias) < 1){
                $this->adapter->getDriver()->getConnection()->rollback();
                return false;
            }
            
            //crear etiquetas si no existen
            $_arrIdsEtiquetas = $modelExamen->createEtiquetas($str_Etiqueta);
            if(count($_arrIdsEtiquetas) < 1){
                $this->adapter->getDriver()->getConnection()->rollback();
                return false;
            }
            if(isset($idExamen) && $idExamen != '' && $idExamen != '-1'){
                //actualizar la pregunta
                $this->_objLog->debug('Modificar -->'.$_idPregunta);
            }else{
                //crear la pregunta
                $this->_objLog->debug('Nuevo -->'.$_idPregunta);
                $insertPregunta = $this->sql->insert();
                $insertPregunta->into('t11pregunta');
                $insertPregunta->columns($_pregunta->obtenerColumnasPregunta());
                $insertPregunta->values(array(
                    't11id_pregunta_parent'	=>$_pregunta->__get('id_pregunta_parent'),
                    'c03id_tipo_pregunta'	=>$_pregunta->__get('id_tipo_pregunta'),
                    //'t11pregunta'		=>$_pregunta->__get('pregunta'),
                    't11pregunta'           => $this->eliminaParrafoEdit($_pregunta->__get('pregunta')),
                    't11nombre_corto_clave'	=>$_pregunta->__get('nombre_corto_clave'),
                    //'t11instruccion'            =>$_pregunta->__get('instruccion'),
                    't11instruccion'	=> $this->eliminaParrafoEdit($_pregunta->__get('instruccion')),
                    't11tiempo_limite'          =>$_pregunta->__get('tiempo_limite'),
                    //'t11columnas'		=>$_pregunta->__get('id_tipo_pregunta'),
                    'c06id_numeracion'		=>$_pregunta->__get('id_numeracion'),
                    't11fecha_registro'         =>date('Y/m/d H:i:s'),
                    't11fecha_actualiza'	=>date('Y/m/d H:i:s'),
                    't11estatus'		=>  EstatusPregunta::ACTIVA));
                
                $result 	 = $this->sql->prepareStatementForSqlObject($insertPregunta);
                $_idPregunta = $result->execute()->getGeneratedValue();
                
                if(!isset($_idPregunta)){
                    $this->adapter->getDriver()->getConnection()->rollback();
                    return false;
                }
                //hacer inserts en las tablas relacionales de categorias y etiquetas
                foreach ($_arrIdsCategorias as $idCat){
                    $insertCategoria = $this->sql->insert()
                            ->into('t28pregunta_categoria')
                            ->columns(array('c08id_categoria','t11id_pregunta','t28estatus'))
                            ->values(array('c08id_categoria' =>$idCat,
                                't11id_pregunta' => $_idPregunta,
                                't28estatus' => $this->config['constantes']['ESTATUS_ACTIVO']
                            ));
                    
                    $result = $this->sql->prepareStatementForSqlObject($insertCategoria);
                    $_generatedId = $result->execute()->getGeneratedValue();
                    if(!isset($_generatedId)){
                        $_boolReturn = false;
                        $this->adapter->getDriver()->getConnection()->rollback();
                        break;
                    }
                }
                foreach ($_arrIdsEtiquetas as $idEtiqueta){
                    $insertaEtiqueta = $this->sql->insert()->into('t29pregunta_etiqueta')->columns(array(
                        'c09id_etiqueta',
                        't11id_pregunta',
                        't29estatus'
                    ));
                    $insertaEtiqueta->values(array(
                        'c09id_etiqueta' => $idEtiqueta,
                        't11id_pregunta' => $_idPregunta,
                        't29estatus' => $this->config['constantes']['ESTATUS_ACTIVO']
                    ));
                    $result = $this->sql->prepareStatementForSqlObject($insertaEtiqueta);
                    $_generatedId = $result->execute()->getGeneratedValue();
                    if(!isset($_generatedId)){
                        $_boolReturn = false;
                        $this->adapter->getDriver()->getConnection()->rollback();
                        break;
                    }
                }
            }
            if($_boolReturn){
                $this->adapter->getDriver()->getConnection()->commit();
            }
            /**
            $insertPregunta = $this->sql->insert();
            $insertPregunta->into('t11pregunta');
            $insertPregunta->columns($_pregunta->obtenerColumnasPregunta());
            $insertPregunta->values(array(
                            't11id_pregunta_parent'	=>$_pregunta->__get('id_pregunta_parent'),
                            'c03id_tipo_pregunta'	=>$_pregunta->__get('id_tipo_pregunta'),
//                            't11pregunta'		=>$_pregunta->__get('pregunta'),
                            't11pregunta'               => $_pregunta->__get('pregunta'),
                            't11nombre_corto_clave'	=>$_pregunta->__get('nombre_corto_clave'),
                            //'t11instruccion'            =>$_pregunta->__get('instruccion'),
                            't11instruccion'            => $_pregunta->__get('instruccion'),
                            't11tiempo_limite'          =>$_pregunta->__get('tiempo_limite'),
                            //'t11columnas'		=>$_pregunta->__get('id_tipo_pregunta'),
                            'c06id_numeracion'		=>$_pregunta->__get('id_numeracion'),
                            't11fecha_registro'         =>date('Y/m/d H:i:s'),
                            't11fecha_actualiza'	=>date('Y/m/d H:i:s'),
                            't11estatus'		=>  EstatusPregunta::ACTIVA));

            $result 	 = $this->sql->prepareStatementForSqlObject($insertPregunta);
            $_idPregunta = $result->execute()->getGeneratedValue();
**/
            return $_idPregunta; 
	}
        
        /**
	 * Funcion para guardar objetos pregunta en base de datos.
	 * @param unknown $_pregunta
	 * @return \Zend\Db\Adapter\Driver\StatementInterface
	 */
	public function actualizarPregunta(\Examenes\Entity\Pregunta $_pregunta, $_intIdPregunta,$arr_strCategoria,$arr_strEtiqueta)
	{
            $this->actualizaCategoria($_intIdPregunta, $arr_strCategoria);
            $this->actualizaEtiqueta($_intIdPregunta, $arr_strEtiqueta);
            $updatePregunta = $this->sql->update('t11pregunta');
            $updatePregunta ->set( array(
				't11id_pregunta_parent'	=> $_pregunta->__get('id_pregunta_parent'),
				//'t11pregunta'		=> $_pregunta->__get('pregunta'),
                                't11pregunta'           => $_pregunta->__get('pregunta'),
				't11nombre_corto_clave'	=> $_pregunta->__get('nombre_corto_clave'),
				//'t11instruccion'	=> $_pregunta->__get('instruccion'),
                                't11instruccion'	=> $_pregunta->__get('instruccion'),
				't11tiempo_limite'	=> $_pregunta->__get('tiempo_limite'),
				//'t11columnas'		=> $_pregunta->__get('id_tipo_pregunta'),
				'c06id_numeracion'      => $_pregunta->__get('id_numeracion'),				
				't11fecha_actualiza'	=> date('Y/m/d H:i:s'),
				't11estatus'		=> $_pregunta->__get('estatus')) )			
                    ->where(array('t11id_pregunta'      => $_intIdPregunta));
            
		
		$result 	 = $this->sql->prepareStatementForSqlObject($updatePregunta);
		$_idPregunta    = $result->execute()->getGeneratedValue();
	
		return $_idPregunta; 
	}
       
        protected function eliminaParrafoEdit( $_strDato ){
            //strip_tags = (string de entrada,'parametro opcional para especificar cuales etiquetas no deber ser retiradas')
            $_strTextoLimpio = strip_tags ($_strDato, '<strong></strong><em></em><a><img><video>');
            return $_strTextoLimpio;
        }
        
        public function obtenerNumeracion(){
            $_arrData = array(
                'c06id_numeracion',
                'c06numeracion',
                'c06estatus'
            );

            $select = $this->sql->select();
            $select->from(
                    array('c06'=>'c06numeracion'))
                    ->columns($_arrData);
            $_arrNumeracion = $this->obtenerRegistros($select,$_arrData[0], $_arrData[1]);
            return $_arrNumeracion;
        }


        public function obtenerTipos(){    
            $_arrData = array( 
                                'c03id_tipo_pregunta',
                                'c03tipo_pregunta',
                                'c03clave_tipo',
                                'c03url_icono_tipo'
                        );
            $select = $this->sql->select();
            $select ->from( 
                        array( 'c03'=>'c03tipo_pregunta') )
                    ->columns( $_arrData );
            
             $_arrTipos = $this->obtenerRegistros($select, $_arrData[0], $_arrData[1]);
            return $_arrTipos;
        }
        
        public function obtenerTiposPreguntas(){    
            $_arrData = array( 
                                'c03id_tipo_pregunta',
                                'c03tipo_pregunta',
                                'c03clave_tipo',
                                'c03url_icono_tipo'
                        );
            $select = $this->sql->select();        
            $select ->from( 
                        array( 'c03'=>'c03tipo_pregunta') )
                    ->columns( $_arrData );
            
             $_arrTipos = $this->obtenerRegistros($select, $_arrData[0]);
            return $_arrTipos;
        }
        
        /**
         * @param type $select
         * @param type $id
         * @param type $data
         * @param type $parent
         * @return type
         */
        protected function obtenerRegistros($select, $id = '', $data = '', $parent = ''){
                        
            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrData = $resultSet->toArray();
            
            $_arrDatos = $this->formatoArray($_arrData, $id, $data, $parent);
            return $_arrDatos;
        }
        
        /**
         * Funcion que se encarga de obtener las preguntas almacenadas, en caso 
         * de recibir un id de pregunta, solo se obtiene la informacion de la 
         * misma
         * @param type $_intIdPregunta
         * @param type $_sinParent
         * @param type $_bolInactivas
         * @return type
         */
        public function obtenerPreguntas( $_intIdPregunta = 0, $_sinParent = false, $_bolInactivas = false ){           
            $select = $this->sql->select();
            $preg = new \Examenes\Entity\Pregunta();
            $_arrData = $preg->obtenerColumnasPreg();
            $select ->from(array( 't11'=>'t11pregunta') )->columns( $_arrData );	
            if( $_intIdPregunta ){
                $select->where(array('t11id_pregunta'   => $_intIdPregunta));
            }
            if( !$_bolInactivas ){
                $select->where(array('t11estatus'   => EstatusPregunta::ACTIVA));
            }
            $select->where(array('t11id_pregunta_parent = 0'));
            if(!$_sinParent){
                 $_arrPreguntas = $this->obtenerRegistros($select, $_arrData[0], $_arrData[3], $_arrData[2]);
            }else{
                 $_arrPreguntas = $this->obtenerRegistros($select, $_arrData[0]);
            }
            if( !$_intIdPregunta ){
                foreach($_arrPreguntas as $indice=>$data){
                    if(isset($data['value'])){
                        $_arrPreguntas[$indice]['value'] = $this->stripTags($data['value']);
                    }else{
                        if(isset($data['t11pregunta'])){
                            $_arrPreguntas[$indice]['t11pregunta'] = $this->stripTags($data['t11pregunta']);
                        }
                    }
                }
            }
            return $_arrPreguntas;
        }
                
        /**
         * Funcion que se encarga de obtener las preguntas de cada uno de los 
         * examenes
         * @param type $_intIdExamen
         * @param type $_intIdPregunta  indica cual es la prgunta actual, en caso 
         *                              de venir en 0, asume que la pregunta es la
         *                              primera del examen
         * @return type
         */
        public function obtenerPreguntasExamen($_intIdExamen, $_intIdPregunta = 0){
            $select = $this->sql->select();       
            $_arrData = array();    
            $select ->from(
                        array( 't12'=>'t12examen') )
                    ->columns( $_arrData )
                    ->join( array( 't20' =>'t20seccion_examen'),
                            't20.t12id_examen = t12.t12id_examen ',
                            array( 't20id_seccion_examen' ))
                    ->join( array( 't16' =>'t16seccion_pregunta'),
                            't20.t20id_seccion_examen = t16.t20id_seccion_examen',
                            array('t11id_pregunta'))
                    ->join( array( 't15' =>'t15seccion'),
                            't15.t15id_seccion = t20.t15id_seccion',
                            array( 't15seccion' ))
                    ->join( array( 't11' =>'t11pregunta'),
                            't11.t11id_pregunta = t16.t11id_pregunta',
                            array('t11pregunta' => 
                                new Expression("if(`t11`.`t11pregunta` = '', `t11`.`t11instruccion`, `t11`.`t11pregunta`)")))   //  AS `t11pregunta`
                     ->order('t16orden_pregunta')
                    ->order('t11nombre_corto_clave');
            $select->where(array('t12.t12id_examen'   => $_intIdExamen));           
            
            $_arrPreguntas = $this->obtenerRegistros($select, 't11id_pregunta', 't11pregunta');
            
            //$_intIdPregunta = $this->datos_sesion->idPregunta;
            
            $_arrKeys = array_keys($_arrPreguntas);
            // Retorno falso si el examen no tiene preguntas
            if(!count($_arrKeys)){
                return false;
            }
            if( !$_intIdPregunta ){                
                //$this->datos_sesion->idPregunta = $_arrPreguntas[$_arrKeys[0]];
                $_intIdPregunta = $_arrKeys[0];
            }
            if(count($_arrPreguntas)){
                $_arrAntSig = $this->obtenerAnteriorSiguiente($_arrPreguntas, $_intIdPregunta);                
            }else{
                $_arrAntSig = array('anterior' => null, 'siguiente' => null);
            }
            $i=1;
            foreach($_arrPreguntas as $index => $preg){
                $_arrPreguntas[$index] = $this->stripTags($preg);
                $_arrPreguntas[$index] = $this->enumerarPregunta($_arrPreguntas[$index], $i);
                $i++;
            
            }
            $_idPregAnterior    = $_arrAntSig['anterior'];
            $_idPregSiguiente   = $_arrAntSig['siguiente'];
            $_bolEsPrimera      = $_arrAntSig['primera'];
            $_bolEsUltima      = $_arrAntSig['ultima'];
            $_arrResp = array(
                            'actual'    => $_intIdPregunta,
                            'anterior'  => $_idPregAnterior,
                            'siguiente' => $_idPregSiguiente,
                            'todas'     => $_arrPreguntas,
                            'primera'   => $_bolEsPrimera,
                            'ultima'    => $_bolEsUltima
                        );
            return $_arrResp;
        }
        public function obtenerPreguntasExamenTodo($_intIdExamen){
            $whereSeccion = $_intIdExamen == 34 ? array('t20.t12id_examen'   => $_intIdExamen):array('t20.t12id_examen'   => $_intIdExamen, 't15.t15seccion'=>"todo1");
            $select = $this->sql->select();       
            $_arrData = array();    
            $select ->from(
                        array( 't15'=>'t15seccion') )
                    ->columns( $_arrData )
                    ->join( array( 't20' =>'t20seccion_examen'),
                            't15.t15id_seccion = t20.t15id_seccion ',
                            array())
                    ->join( array( 't16' =>'t16seccion_pregunta'),
                            't20.t20id_seccion_examen = t16.t20id_seccion_examen',
                            array())
                    ->join( array( 't11' =>'t11pregunta'),
                            't16.t11id_pregunta = t11.t11id_pregunta',
                            array('t11pregunta', 't11id_pregunta', 'c03id_tipo_pregunta'))
                    ;
            $select->where($whereSeccion);           
            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrData = $resultSet->toArray();
            
            //$_intIdPregunta = $this->datos_sesion->idPregunta;
            
            $_arrKeys = array_keys($_arrData);
            // Retorno falso si el examen no tiene preguntas
            if(!count($_arrKeys)){
                return false;
            }

            return $_arrData;
        }
        public function preguntaRespuestaTodo($_objData){
            $idsPreguntas = array();
            $contPreguntas = 0;
            foreach ($_objData as $keyObj => $valueObj) {
                $idsPreguntas[$valueObj['t11id_pregunta']] = $valueObj['t11pregunta'];
            }
            $arrPreguntas = array();
            foreach ($idsPreguntas as $key => $value) {
                $count = 0;
                //$arrPreguntas[$key]['pregunta']['c03id_tipo_pregunta'] = 
                foreach ($_objData as $keyObj => $valueObj) {
                    if(intval($valueObj['t11id_pregunta']) === $key){
                        
                        $arrPreguntas[$contPreguntas]['respuestas'][$count]['t13respuesta']=$valueObj['t13respuesta'];
                        $arrPreguntas[$contPreguntas]['respuestas'][$count]['t17correcta']="0";
                        $tipo = $valueObj['c03id_tipo_pregunta'];
                        $_strPregunta = $valueObj['t11pregunta'];
                        $idPregunta = $valueObj['t11id_pregunta'];
                        $count++;
                    }
                }
                $arrPreguntas[$contPreguntas]['pregunta']['c03id_tipo_pregunta'] = $tipo;
                $arrPreguntas[$contPreguntas]['pregunta']['t11pregunta'] = $_strPregunta;
                $arrPreguntas[$contPreguntas]['pregunta']['t11id_pregunta'] = $idPregunta;
                $contPreguntas++;
            }
            
            return $arrPreguntas;
        }
        /**
         * Funcion que se encarga de marcar  o desmarcar una pregunta, esto 
         * servira para que el usuario guarde las preguntas en las que pueda 
         * llegar a tener dudas y poder identificarlas facilmente
         * @param type $_intIdPregunta
         * @param type $_intIdUsuarioLicencia
         * @param type $_intIdExamen
         * @param type $_intNumIntento
         * @param type $_marcada
         * @return type Estatus de la preguinta
         */
        public function marcarDesmarcarPregunta($_intIdPregunta, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_marcada){
            $_estatusMarcada = $_marcada !== 'false' ? EstatusPreguntaMarcada::DESMARCADA : EstatusPreguntaMarcada::MARCADA;
            $updatePregunta = $this->sql->update('t10respuesta_usuario_examen');
            $updatePregunta ->set( array( 't10marcada'	=> $_estatusMarcada) )
                    ->where(array(
                        't11id_pregunta'            => $_intIdPregunta,
                        't03id_licencia_usuario'    => $_intIdUsuarioLicencia,
                        't12id_examen'              => $_intIdExamen,
                        't10num_intento'            => $_intNumIntento));
            $result 	 = $this->sql->prepareStatementForSqlObject($updatePregunta);
            $result->execute()->getGeneratedValue();
            return $_estatusMarcada;
        }
        
        /**
         * Funcion que se encarga de obtener el estatus de la pregunta en cierto
         * examen
         * @param type $_intIdUsuarioLicencia
         * @param type $_intIdExamen
         * @param type $_intNumIntento
         * @param type $_intIdPregunta
         * @return mixed
         */
        public function estaPreguntaMarcada($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPregunta = 0){
            $select = $this->sql->select();  
            $_arrData = array( 't10marcada' );
            $select->from(array( 't10'=>'t10respuesta_usuario_examen') )->columns( $_arrData );
            $select->where(array(
                        't03id_licencia_usuario'    => $_intIdUsuarioLicencia,
                        't12id_examen'              => $_intIdExamen,
                        't10num_intento'            => $_intNumIntento));
            if($_intIdPregunta){
                $select->where(array('t11id_pregunta' => $_intIdPregunta));                
            }
            $objSelect = $this->sql->prepareStatementForSqlObject($select);
            $result = $objSelect->execute();

            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrResultMarcada = $resultSet->toArray();
            $_mixRespuesta  = $_intIdPregunta ? $_arrResultMarcada[0]['t10marcada'] : $_arrResultMarcada;
            return $_mixRespuesta;
        }
        
        /**
         * Marca las preguntas que estan almacenadas en la base de datos
         * @param type $_arrPreguntas
         * @param type $_intIdUsuarioLicencia
         * @param type $_intIdExamen
         * @param type $_intNumIntento
         * @return type
         */
        public function marcaPreguntaInit($_arrPreguntas, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento){
            $_arrPregsMarcadas = $this->estaPreguntaMarcada($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
            $i=0;
            foreach($_arrPreguntas as $index => $data){
                if(isset($_arrPregsMarcadas[$i])){
                    if($_arrPregsMarcadas[$i]['t10marcada'] === EstatusPreguntaMarcada::MARCADA ){
                       $_arrPreguntas[$index] = '<span class="tiny alert fa fa-flag bandera-preguntas text-orange"></span>' . $_arrPreguntas[$index];
                   }else{
                       $_arrPreguntas[$index] = '<span class="tiny alert fa fa-flag desactiva bandera-preguntas text-orange"></span>' . $_arrPreguntas[$index];
                   }
                }
                   $i++;
                }
            return $_arrPreguntas;
        }
        
        /**
         * Obtiene la pregunta anterior y la pregunta siguiente del examen activo
         * @param type $_arrPreguntas
         * @param type $_intIdPregunta
         * @return array
         */
        public function obtenerAnteriorSiguiente($_arrPreguntas, $_intIdPregunta){
            $_arrKeys = array_keys($_arrPreguntas);
            foreach ( $_arrPreguntas as $index=>$data){
                $_arrTmpPreg[] = $index;
            }
            $_intPos = array_search($_intIdPregunta, $_arrTmpPreg);
            
            $_arrPregunta = array(
                                    'anterior'  => isset($_arrKeys[$_intPos -1]) ? $_arrKeys[$_intPos -1] : 0, 
                                    'siguiente' => isset($_arrKeys[$_intPos +1]) ? $_arrKeys[$_intPos +1] : 0,
                                    'primera'   => $_arrKeys[0] === (int)$_intIdPregunta ? 1 : '',
                                    'ultima'    => end($_arrTmpPreg) === (int)$_intIdPregunta ? 1 : ''
                    );
            return $_arrPregunta;
        }
        
        /**
         * Le quito las etiquetas html a las cadenas que me lleguen
         * @param type $_strtexto
         * @return type $_strTextoLimpio
         */
        protected function stripTags( $_strtexto ){
            $_strTextoLimpio = strip_tags ( $_strtexto );
            //$_strTextoFinal = preg_replace("/<img[^>]+\>/i", "(image) ", $_strTextoLimpio); 
            return $_strTextoLimpio;
        }
        
        /**
         * Función que se encarga de agregar el número de pregunta a la lista 
         * que se envía a la vista
         * @param type $_strPregunta
         * @param type $index
         * @return string
         */
        protected function enumerarPregunta($_strPregunta, $index){
            $_strPreguntaRespuesta = '<strong>' . $index . '.- </strong>' . $_strPregunta;
            return $_strPreguntaRespuesta ;
        }
        
        /**
         * formateo el array que obtuve desde la consulta para que se pueda leer 
         * mas facilmente
         * @param Array $_arrTipo datos que quiero formatear         
         * @param type $id el campo identificador de cada registro
         * @param type $dato el campo que se desea mostrar
         * @param type $parent en caso de que el resultado sea un subconjunto
         * @return Array $_arrRespuesta Array formateado
         */
        protected function formatoArray($_arrTipo, $id, $dato, $parent){
            $_arrRespuesta = array();
            if(!isset($_arrTipo[0])){
                return $_arrRespuesta;
            }
            $_arrIndices = array_keys($_arrTipo[0]);
            $it=0;
            foreach($_arrTipo as $index=>$data){
                //c03clave_tipo
                if($parent){
                    $_arrRespuesta[$data[$id]] = array( 
                                                        'id_parent' => $data[$parent], 
                                                        'value' => $data[$dato]);
                }else{
                    if($id && $dato){
                        $_arrRespuesta[$data[$id]] = $data[$dato];
                    }else{
                        //$_arrRespuesta[$data[$id]] = array(); //$data[$dato];
                        foreach($_arrIndices as $campo){
                            
                            if($id){
                                $_arrRespuesta[$data[$id]][$campo] = $data[$campo];
                            }else{
                                $_arrRespuesta[$it][$campo] = $data[$campo];                                
                            }
                        }
                        $it++;
                    }
                }
            }
            return $_arrRespuesta;
        }
        
        /**
         * Función que extrae la información del objeto PreguntaForm y lo 
         * convierte a array
         * @param PreguntaForm $form
         * @param type $columns
         * @return type
         */
        public function crearArregloPregunta(PreguntaForm $form, $columns){
            $_arrDatos[2] = $this->obtenerTipos();
            $_arrDatos[8] = $this->obtenerNumeracion();
            $_arrPregunta = array();
            for($i=0;$i<12;$i++){
                if($i!==9 && $i!==10 && $i!==0){
                    //if($i===2 || $i === 8){
                       // $_arrData = $form->get($columns[$i])->getValueOptions($_arrDatos[$i]);
                    //}else{
                        $_arrData = $form->get($columns[$i]);
                    //}
                    $_arrPregunta[$columns[$i]]['attributes']   = $_arrData->getAttributes();
                    $_arrPregunta[$columns[$i]]['label']        = $_arrData->getLabel();
                    $_arrPregunta[$columns[$i]]['messages']     = $_arrData->getMessages();
                    $_arrPregunta[$columns[$i]]['options']      = $_arrData->getOptions();
                }
            }
           // $_arrPregunta = array();
            /*             
                echo $this->formRow($form->get($columns[2])->setValueOptions($this->arrTipos));
                echo $this->formRow($form->get($columns[3]));
                echo $this->formRow($form->get($columns[4]));
                echo $this->formRow($form->get($columns[5]));   
                echo $this->formRow($form->get($columns[6]));   
                echo $this->formRow($form->get($columns[7]));  
                echo $this->formRow($form->get($columns[8])->setValueOptions($this->arrNumeracion));
                echo $this->formHidden($form->get($columns[11]));
             */
            return $_arrPregunta;
        }
        
        
        /**
         * funcion que se utiliza exclusivamente en ligarpreguntasexamen opcion de busqueda tipo 
         * @param type $id_examen               ->recibe el id del examen
         * @param type $_arrid_tipo_pregunta    ->rebice el valor de tipo de pregunta que se selecciono en la busqueda de tipos
         * @return boolean
         * obtiene un filtro de preguntas de acuerdo al id de tipo de pregunta que recibe y lo muestra en el banco de preguntas
         */
        public function obtenerTipoPregunta($id_examen,$_arridCategoria,$_arrid_tipo_pregunta){
            $_arrPreguntaTipo = array();
        foreach ($_arrid_tipo_pregunta as $id_tipo_pregunta){
            $selectSub1 = $this->sql->select();
            $_arrData1 = array(
                't20id_seccion_examen',
                't15id_seccion'
            );
            
            $selectSub1->from(array('t20'=>'t20seccion_examen'))
                        ->columns($_arrData1)
                        ->where('t12id_examen='.$id_examen);
            
            $selectSub2 = $this->sql->select();
            $_arrData2 = array(
                't11id_pregunta'
            );
            
            $selectSub2->from(array('t16'=>'t16seccion_pregunta'))
                        ->columns($_arrData2)
                    ->join(array('sec'=>$selectSub1),
                            't16.t20id_seccion_examen  =  sec.t20id_seccion_examen'
                            ,
                            array());
            
            $selectSub3 = $this->sql->select();
            $_arrData3 = array(
                't11id_pregunta',
                't11pregunta',
                'c03id_tipo_pregunta'
            );
            
            if(empty($_arridCategoria)){
                $selectSub3->from(array('t11'=>'t11pregunta'))
                    ->columns($_arrData3)
                    ->where('t11.c03id_tipo_pregunta='.$id_tipo_pregunta)
                    ->where('t11estatus='.EstatusPregunta::ACTIVA)
                    ->where->notIn('t11.t11id_pregunta',$selectSub2);
            }else{
                    $selectSub3->from(array('t11'=>'t11pregunta'))
                    ->columns($_arrData3)
                    ->join(array('t28' => 't28pregunta_categoria'), 't11.t11id_pregunta = t28.t11id_pregunta')
                    ->join(array('c08' => 'c08categoria'), 'c08.c08id_categoria = t28.c08id_categoria')
                    ->where('t11.c03id_tipo_pregunta='.$id_tipo_pregunta)
                    ->where('t11estatus='.EstatusPregunta::ACTIVA)
                    ->where->notIn('t11.t11id_pregunta',$selectSub2)
                    ->where->in('t28.c08id_categoria',$_arridCategoria);   
            }
            $strSelect = $this->sql->prepareStatementForSqlObject($selectSub3);
            $result = $strSelect->execute();

            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrExamenSeccion = $resultSet->toArray();
            foreach ($_arrExamenSeccion as $_arrPreg)
            {
                $_arrPreguntaTipo[$_arrPreg['t11id_pregunta']] = $_arrPreg;
            }
            if(!count($_arrPreguntaTipo) ){
                    $_arrPreguntaTipo = false;
                }
            }
            return $_arrPreguntaTipo;
            }
            
        /**
         * funcion que se utiliza exclusivamente en ligarpreguntasexamen opcion de busqueda general 
         * @param type $id_examen
         * @return boolean
         * obtiene todas las preguntas que no esten almacenadas en el examen guardado identificandolo con el id de examen
         */
        public function obtenerPreguntaGral($id_examen)
        {
            $selectSub1 = $this->sql->select();
            $_arrData1 = array(
                't20id_seccion_examen',
                't15id_seccion'
            );
            
            $selectSub1->from(array('t20'=>'t20seccion_examen'))
                        ->columns($_arrData1)
                        ->where('t12id_examen='.$id_examen);
            
            $selectSub2 = $this->sql->select();
            $_arrData2 = array(
                't11id_pregunta'
            );
            
            $selectSub2->from(array('t16'=>'t16seccion_pregunta'))
                    ->columns($_arrData2)
                    ->join(array('sec'=>$selectSub1),
                            't16.t20id_seccion_examen  =  sec.t20id_seccion_examen'
                            ,array());
            
            $selectSub3 = $this->sql->select();
            $_arrData3 = array(
                't11id_pregunta',
                't11pregunta',
                'c03id_tipo_pregunta'
            );
            
            $selectSub3->from(array('t11'=>'t11pregunta'))
                    ->columns($_arrData3)
                    ->where('t11estatus='.EstatusPregunta::ACTIVA)
                    ->where->notIn('t11.t11id_pregunta',$selectSub2);
            
            $strSelect = $this->sql->prepareStatementForSqlObject($selectSub3);
            $result = $strSelect->execute();

            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrExamenSeccion = $resultSet->toArray();
            
            if(!count($_arrExamenSeccion) ){
                $_arrExamenSeccion = false;
            }
            return $_arrExamenSeccion;
        } 
        
        /**
         * funcion que se utiliza exclusivamente en ligarpreguntasexamen opcion de busqueda categoria
         * @param type $_arrid_categpregunta : recibe el array de los ids de cada categoria existente 
         * @param type $_idExamen : recibe el id de examen
         * @return boolean
         */
        public function obtenerCategoriaPregunta($_idExamen, $_arrid_categpregunta,$_arrid_tipoPregunta )
        {
            $_arrPreguntaCateg = array();
            foreach ($_arrid_categpregunta as $id_categpregunta)
            {
                $selectSub1 = $this->sql->select();
                $_arrData1 = array(
                    't20id_seccion_examen',
                    't15id_seccion'
                );
                $selectSub1->from(array('t20' => 't20seccion_examen'))
                        ->columns($_arrData1)
                        ->where('t12id_examen=' . $_idExamen);

                $selectSub2 = $this->sql->select();
                $_arrData2 = array('t11id_pregunta');
                $selectSub2->from(array('t16' => 't16seccion_pregunta'))
                        ->columns($_arrData2)
                        ->join(array('sec' => $selectSub1), 't16.t20id_seccion_examen  =  sec.t20id_seccion_examen', array());
                $selectSub3 = $this->sql->select();
                $_arrData3 = array('t11pregunta');
                if(empty($_arrid_tipoPregunta)){
                        $selectSub3->from(array('t11' => 't11pregunta'))
                                ->columns($_arrData3)
                                ->join(array('t28' => 't28pregunta_categoria'), 't11.t11id_pregunta = t28.t11id_pregunta')
                                ->join(array('c08' => 'c08categoria'), 'c08.c08id_categoria = t28.c08id_categoria')
                                ->where('t11estatus=' . EstatusPregunta::ACTIVA)
                                ->where('t28.c08id_categoria=' . $id_categpregunta)
                                ->where->notIn('t11.t11id_pregunta', $selectSub2);
                }else{
                    foreach ($_arrid_tipoPregunta as $id_tipoPregunta)
                    {
                        $selectSub3->from(array('t11' => 't11pregunta'))
                                ->columns($_arrData3)
                                ->join(array('t28' => 't28pregunta_categoria'), 't11.t11id_pregunta = t28.t11id_pregunta')
                                ->join(array('c08' => 'c08categoria'), 'c08.c08id_categoria = t28.c08id_categoria')
                                ->where('t11estatus=' . EstatusPregunta::ACTIVA)
                                ->where('t28.c08id_categoria=' . $id_categpregunta)
                                ->where('t11.c03id_tipo_pregunta=' . $id_tipoPregunta)
                                ->where->notIn('t11.t11id_pregunta', $selectSub2);
                    }
                }
                
                $strSelect = $this->sql->prepareStatementForSqlObject($selectSub3);
                $result = $strSelect->execute();
                $resultSet = new ResultSet;
                $resultSet->initialize($result);
                $_arrPreguntaCategRes = $resultSet->toArray();
                foreach ($_arrPreguntaCategRes as $_arrPreg)
                {
                    $_arrPreguntaCateg[$_arrPreg['t11id_pregunta']] = $_arrPreg;
                }
                if (!count($_arrPreguntaCateg))
                {
                    $_arrPreguntaCateg = false;
                }
            }
            return $_arrPreguntaCateg;
        }
        
        /**
         * funcion que se utiliza exclusivamente en ligarpreguntasexamen Muestra la lista de categorias disponible para las preguntas
         * @return type
         */
        public function obtenerCategorias(){
            $_arrCategorias = array(
                'c08id_categoria',
                'c08nombre',
                'c08descripcion'
            );
            $select = $this->sql->select()->quantifier(\Zend\Db\Sql\Select::QUANTIFIER_DISTINCT);
            $select->from(
                    array('c08'=>'c08categoria'))
                    ->columns($_arrCategorias)
                    ->join(array('t28'=>'t28pregunta_categoria'), 
                            'c08.c08id_categoria=t28.c08id_categoria');
                          //  array( 'c08id_categoria' ));
            $_arrListCategorias = $this->obtenerRegistros($select,$_arrCategorias[1]);
            return $_arrListCategorias;
        }
        
        
        public function obtenerSeccionPregunta($_intIdExamen,$_intId_pregunta){
            $_arrCampos = array(
                't20id_seccion_examen',
                't11id_pregunta',
                
            );
            $select = $this->sql->select()->quantifier(\Zend\Db\Sql\Select::QUANTIFIER_DISTINCT);
            $select->from(array('t16'=>'t16seccion_pregunta'))
                    ->columns($_arrCampos)
                    ->join(array('t20'=>'t20seccion_examen'), 't16.t20id_seccion_examen=t20.t20id_seccion_examen')
                    ->join(array('t15'=>'t15seccion'), 't20.t15id_seccion=t15.t15id_seccion',  array('t15id_seccion','t15seccion','t15estatus'))
                    ->where('t20.t12id_examen='.$_intIdExamen)
                    ->where('t16.t11id_pregunta='.$_intId_pregunta)
                    ;
            $_arrSeccionPregunta = $this->obtenerRegistros($select);
            return $_arrSeccionPregunta[0];
        }
        /**
         *  funcion que se utiliza exclusivamente en ligarpreguntasexamen Muestra la lista de etiquetas disponible para las preguntas
         * @return type array 
         */
        public function obtenerEtiquetas(){
            $_arrEtiquetas = array(
                'c09id_etiqueta',
                'c09nombre'
            );
            $select = $this->sql->select()->quantifier(\Zend\Db\Sql\Select::QUANTIFIER_DISTINCT);
            $select->from(
                    array('c09' => 'c09etiqueta'))
                    ->columns($_arrEtiquetas)
                    ->join(array('t29' => 't29pregunta_etiqueta'), 
                            'c09.c09id_etiqueta = t29.c09id_etiqueta');
            $_arrListEtiquetas = $this->obtenerRegistros($select,$_arrEtiquetas[0],$_arrEtiquetas[1]);
            return $_arrListEtiquetas;
        }
        
        /**
         * funcion que se utiliza exclusivamente en ligarpreguntasexamen opcion de busqueda por etiqueta
         * muestra las preguntas que pertenecen a dicha etiqueta
         * @param type $_idExamen
         * @param type $_arrid_categpregunta
         * @param type $_arrid_tipoPregunta
         * @param type $_arrid_etiquetaPregunta
         * @return boolean
         */
        public function obtenerEtiquetaPregunta($_idExamen, $_arrid_categpregunta,$_arrid_tipoPregunta,$_arrid_etiquetaPregunta)
        {
            $_arrPreguntaEtiq = array();
            foreach ($_arrid_etiquetaPregunta as $id_etiquetpregunta)
            {
                $selectSub1 = $this->sql->select();
                $_arrData1 = array(
                    't20id_seccion_examen',
                    't15id_seccion'
                );
                $selectSub1->from(array('t20' => 't20seccion_examen'))
                        ->columns($_arrData1)
                        ->where('t12id_examen=' . $_idExamen);
                $selectSub2 = $this->sql->select();
                $_arrData2 = array('t11id_pregunta');
                $selectSub2->from(array('t16' => 't16seccion_pregunta'))
                        ->columns($_arrData2)
                        ->join(array('sec' => $selectSub1), 't16.t20id_seccion_examen  =  sec.t20id_seccion_examen', array());
                $selectSub3 = $this->sql->select();
                $_arrData3 = array(
                    't11pregunta'
                );
                if(empty($_arrid_tipoPregunta)){
                        $selectSub3->from(array('t11' => 't11pregunta'))
                                ->columns($_arrData3)
                                ->join(array('t29' => 't29pregunta_etiqueta'), 't11.t11id_pregunta = t29.t11id_pregunta')
                                ->join(array('c09' => 'c09etiqueta'), 'c09.c09id_etiqueta = t29.c09id_etiqueta')
                                ->where('t11estatus=' . EstatusPregunta::ACTIVA)
                                ->where('t29.c09id_etiqueta=' . $id_etiquetpregunta)
                                ->where->notIn('t11.t11id_pregunta', $selectSub2);
                }else{
                    foreach ($_arrid_tipoPregunta as $id_tipoPregunta){
                        $selectSub3->from(array('t11' => 't11pregunta'))
                                ->columns($_arrData3)
                                ->join(array('t29' => 't29pregunta_etiqueta'), 't11.t11id_pregunta = t29.t11id_pregunta')
                                ->join(array('c09' => 'c09etiqueta'), 'c09.c09id_etiqueta = t29.c09id_etiqueta')
                                ->where('t11estatus=' . EstatusPregunta::ACTIVA)
                                ->where('t29.c09id_etiqueta=' . $id_etiquetpregunta)
                                ->where('t11.c03id_tipo_pregunta=' . $id_tipoPregunta)
                                ->where->notIn('t11.t11id_pregunta', $selectSub2);
                    }
                }                
                
                $strSelect = $this->sql->prepareStatementForSqlObject($selectSub3);
                $result = $strSelect->execute();
                $resultSet = new ResultSet;
                $resultSet->initialize($result);
                $_arrPreguntaCategRes = $resultSet->toArray();
                foreach ($_arrPreguntaCategRes as $_arrPreg)
                {
                    $_arrPreguntaEtiq[$_arrPreg['t11id_pregunta']] = $_arrPreg;
                }
                if (!count($_arrPreguntaEtiq))
                {
                    $_arrPreguntaEtiq = false;
                }
            }
            return $_arrPreguntaEtiq;
        }
        
        public function eliminarPregunta($_intIdPre){
            $_actualizarPregunta = $this->sql->update('t11pregunta');
            $_actualizarPregunta->set(array('t11estatus'=>EstatusPregunta::INACTIVA))
                    ->where(array(
                         't11id_pregunta' => $_intIdPre));
            $result = $this->sql->prepareStatementForSqlObject($_actualizarPregunta);
            $_bolUpdt    = $result->execute()->getAffectedRows();
            return $_bolUpdt;
            //return 1;     
        }
        /**
         * 
         * @param type $idPreegunta
         * @return type muestra en la vista de editar pregunta las categorias guardadas en esa pregunta
         */
        public function getCategoriasEdit($idPreegunta){
            $_arrData = array(
                'c08id_categoria',
                'c08nombre'
            );
            $select = $this->sql->select();     
            $select ->from( array( 'c08'=>'c08categoria') )
                    ->columns($_arrData)
                    ->join(array('t28' => 't28pregunta_categoria'), 'c08.c08id_categoria = t28.c08id_categoria')
                    ->where('t28.t11id_pregunta ='.$idPreegunta)
                    ->where('t28.t28estatus='.$this->config['constantes']['ESTATUS_ACTIVO']);
           
            $arr_nombreCategoria = $this->obtenerRegistros($select,$_arrData[0]);
            return $arr_nombreCategoria;
        }
        /**
         * 
         * @param type $idPreegunta
         * @return type muestra en la vista de editar pregunta las etiquetas guardadas en esa pregunta
         */
        public function getEtiquetasEdit($idPreegunta){
            $_arrData = array(
                'c09id_etiqueta',
                'c09nombre'
            );
            $select = $this->sql->select();     
            $select ->from( array('c09'=>'c09etiqueta') )
                    ->columns($_arrData)
                    ->join(array('t29' => 't29pregunta_etiqueta'), 'c09.c09id_etiqueta = t29.c09id_etiqueta')
                    ->where('t29.t11id_pregunta='.$idPreegunta)
                    ->where('t29.t29estatus='.$this->config['constantes']['ESTATUS_ACTIVO']);
           
            $arr_nombreEtiqueta = $this->obtenerRegistros($select,$_arrData[0]);
            return $arr_nombreEtiqueta;
        }
        /**
         * actualiza las categorias en dado caso que la categoria no existiera en la base de datos
         * se inserta la nueva categoria regresando su id ademas de insertar la relacion entre pregunta categoria en la tabla relacional 
         * @param type $_intIdPregunta
         * @param type $arr_strCategoria
         */
        public function actualizaCategoria($_intIdPregunta,$arr_strCategoria){
            $modelExamen = new \Examenes\Model\ExamenModel($this->adapter, $this->config);
            $strCategoria = '';
            //concatena el array de los tags 
            foreach ($arr_strCategoria as $id){
                $strCategoria .= $id.',';
            }
            $strCat = substr($strCategoria,0,  strlen($strCategoria)-1);
            $_arrIdCategoria = $modelExamen->createCategorias($strCat);
            $select = $this->sql->select();
            $_arrData = array('c08id_categoria','t11id_pregunta',);
            $select->from(array('t28'=> 't28pregunta_categoria'))
                    ->columns($_arrData)
                    ->where(array(
                        't11id_pregunta'=>$_intIdPregunta,
                        't28estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']));
            $_arrRegistros = $this->obtenerRegistros($select,$_arrData[0],$_arrData[0]);
            
            foreach ($_arrRegistros as $midato){
                //si el id del registro no existe la actualiza 
                    $updateCategoria = $this->sql->update('t28pregunta_categoria');
                if(!in_array($midato, $_arrIdCategoria)){
                    $updateCategoria ->set(array('t28estatus'=>$this->config['constantes']['ESTATUS_INACTIVO']));
                }else{
                    $updateCategoria ->set(array('t28estatus'=>$this->config['constantes']['ESTATUS_ACTIVO']));
                }
                $updateCategoria->where(array('t11id_pregunta'=>$_intIdPregunta,'c08id_categoria'=>$midato));
                    $result = $this->sql->prepareStatementForSqlObject($updateCategoria);
                    $result->execute()->getGeneratedValue();
            }
            
            $arrDiffCat= array_diff($_arrIdCategoria,$_arrRegistros );
            //de mi variable $arrDiffCat crear un insert con ese id de categoria que me trae solo actuliza activo;
            foreach ($arrDiffCat AS $diffCat ){
                $insertRelCategoria = $this->sql->insert()
                                ->into('t28pregunta_categoria')
                                ->columns(array('c08id_categoria', 't11id_pregunta','t28estatus'))
                                ->values(array('c08id_categoria' => $diffCat,
                                    't11id_pregunta' => $_intIdPregunta,
                                    't28estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']   
                                ));
                        $statement = $this->sql->prepareStatementForSqlObject($insertRelCategoria);
                        $result = $statement->execute()->getAffectedRows();
            }
        }
        /**
         * actualiza las etiquetas en dado caso que la etiqueta no existiera en la base de datos
         * se inserta la nueva etiqueta regresando su id ademas de insertar la relacion entre pregunta etiqueta en la tabla relacional 
         * @param type $_intIdPregunta
         * @param type $arr_strEtiqueta
         */
        public function actualizaEtiqueta($_intIdPregunta,$arr_strEtiqueta){
            $modelExamen = new \Examenes\Model\ExamenModel($this->adapter, $this->config);
            $strEtiqueta = '';
            //concatena el array de los tags 
            foreach ($arr_strEtiqueta as $id){
                $strEtiqueta .= $id.',';
            }
            $strTag = substr($strEtiqueta,0,  strlen($strEtiqueta)-1);
            $_arrIdEtiqueta = $modelExamen->createEtiquetas($strTag);
            $select = $this->sql->select();
            $_arrData = array('c09id_etiqueta','t11id_pregunta',);
            $select->from(array('t29'=> 't29pregunta_etiqueta'))
                    ->columns($_arrData)
                    ->where(array(
                        't11id_pregunta'=>$_intIdPregunta,
                        't29estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']));
            $_arrRegistros = $this->obtenerRegistros($select,$_arrData[0],$_arrData[0]);
            
            foreach ($_arrRegistros as $midato){
                //si el id del registro no existe la actualiza 
                    $updateEtiqueta= $this->sql->update('t29pregunta_etiqueta');
                if(!in_array($midato, $_arrIdEtiqueta)){
                    $updateEtiqueta->set(array('t29estatus'=>$this->config['constantes']['ESTATUS_INACTIVO']));
                }else{
                    $updateEtiqueta->set(array('t29estatus'=>$this->config['constantes']['ESTATUS_ACTIVO']));
                }
                $updateEtiqueta->where(array('t11id_pregunta'=>$_intIdPregunta,'c09id_etiqueta'=>$midato));
                    $result = $this->sql->prepareStatementForSqlObject($updateEtiqueta);
                    $result->execute()->getGeneratedValue();
            }
            
            $arrDiffTag= array_diff($_arrIdEtiqueta,$_arrRegistros );
            //de mi variable $arrDiffCat crear un insert con ese id de categoria que me trae solo actuliza activo o inactivo;
            foreach ($arrDiffTag AS $diffTag){
                $insertRelEtiqueta= $this->sql->insert()
                                ->into('t29pregunta_etiqueta')
                                ->columns(array('c09id_etiqueta', 't11id_pregunta','t29estatus'))
                                ->values(array('c09id_etiqueta' => $diffTag,
                                    't11id_pregunta' => $_intIdPregunta,
                                    't29estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']   
                                ));
                        $statement = $this->sql->prepareStatementForSqlObject($insertRelEtiqueta);
                        $result = $statement->execute()->getAffectedRows();
            }
        }
        
        public function clonarPregunta($pregunta,$_arrCategoria,$_arrEtiqueta,$idSeccionExamen = null){
            $_arrResult[0] = false;
            
            //inicia una transaccion en la base de datos
            $_strTransaction = "START TRANSACTION";
            $statementTransaction = $this->adapter->query($_strTransaction);
            $resultTransaction = $statementTransaction->execute();
            $resultSetTransaction =  new ResultSet();
            $resultSetTransaction->initialize($resultTransaction);
            
            $nombreConcat = 'copia de la pregunta:';
            $conNombreCopia = $this->consultaCopiaExistente($nombreConcat.$pregunta['t11pregunta']);
            $nombreCopia = $conNombreCopia >= 1 ? $nombreConcat.$pregunta['t11pregunta']."($conNombreCopia)" : $nombreConcat.$pregunta['t11pregunta'];
            
            $insertPreg = $this->sql->insert()
                    ->into('t11pregunta')
                    ->columns(array(
                        "t11id_pregunta_parent",
                        "c03id_tipo_pregunta",
                        "t11pregunta",
                        "t11nombre_corto_clave",
                        "t11instruccion",
                        "t11tiempo_limite",
                        "c06id_numeracion",
                        "t11fecha_registro",
                        "t11fecha_actualiza",
                        "t11estatus",
                    ))
                    ->values(array(
                        "t11id_pregunta_parent" =>$pregunta['t11id_pregunta_parent'],
                        "c03id_tipo_pregunta"   =>$pregunta['c03id_tipo_pregunta'],
                        "t11pregunta"           =>$nombreCopia,
                        "t11nombre_corto_clave" =>$pregunta['t11nombre_corto_clave'],
                        "t11instruccion"        =>$pregunta['t11instruccion'],
                        "t11tiempo_limite"      =>$pregunta['t11tiempo_limite'],
                        "c06id_numeracion"      =>$pregunta['c06id_numeracion'],
                        "t11fecha_registro"     =>$pregunta['t11fecha_registro'],
                        "t11fecha_actualiza"    =>$pregunta['t11fecha_actualiza'],
                        "t11estatus"            =>$pregunta['t11estatus']
                    ));
            $strQuery1 = $this->sql->prepareStatementForSqlObject($insertPreg);
            $resultInsertPreg = $strQuery1->execute();
            $_bolEjecuta1 = $resultInsertPreg->getAffectedRows();
            if(!$_bolEjecuta1){
                $_strTransaction    ="ROLLBACK";
                $statementTransaction = $this->adapter->query($_strTransaction);
                $resultInsertPreg = $statementTransaction->execute();
                return array(false);
            }
            $idCopiaPreg = $resultInsertPreg->getGeneratedValue();           
            
            //genera la copia de la relacion de la categoria.
            if($_arrCategoria){
                $_arrCat = array_values($_arrCategoria);
                    $insertRelCategoria = $this->sql->insert()
                            ->into('t28pregunta_categoria')
                            ->columns(array('c08id_categoria', 't11id_pregunta', 't28estatus'))
                            ->values(array('c08id_categoria' => $_arrCat[0]['c08id_categoria'],
                        't11id_pregunta' => $idCopiaPreg,
                        't28estatus' => $this->config['constantes']['ESTATUS_ACTIVO']
                    ));
                    $statement1 = $this->sql->prepareStatementForSqlObject($insertRelCategoria);
                    $result = $statement1->execute()->getAffectedRows();
            }
            //genera la copia de la relacion de la etiqueta
            if($_arrEtiqueta){
                $_arrEti = array_values($_arrEtiqueta);
                $insertRelEtiqueta= $this->sql->insert()
                                ->into('t29pregunta_etiqueta')
                                ->columns(array('c09id_etiqueta', 't11id_pregunta','t29estatus'))
                                ->values(array('c09id_etiqueta' => $_arrEti[0]['c09id_etiqueta'],
                                    't11id_pregunta' => $idCopiaPreg,
                                    't29estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']   
                                ));
                        $statement2 = $this->sql->prepareStatementForSqlObject($insertRelEtiqueta);
                        $result = $statement2->execute()->getAffectedRows();
            }
            //quita la relacion de la pregunta actual y se la agrega a la pregunta copiada.
            if($idSeccionExamen  != null){                
                $deleteRel = $this->sql->delete()
                        ->from('t16seccion_pregunta')
                        ->where(array(
                            't20id_seccion_examen' => $idSeccionExamen,
                            't11id_pregunta' => $pregunta['t11id_pregunta']
                        ));
                $statement3 = $this->sql->prepareStatementForSqlObject($deleteRel);
                $result = $statement3->execute()->getAffectedRows();

                $addNewRelPregSeccion = $this->sql->insert()
                        ->into('t16seccion_pregunta')
                        ->columns(array('t20id_seccion_examen','t11id_pregunta','t16orden_pregunta'))
                        ->values(array(
                            't20id_seccion_examen' => $idSeccionExamen,
                            't11id_pregunta' => $idCopiaPreg
                            ));
                $statement4 = $this->sql->prepareStatementForSqlObject($addNewRelPregSeccion);
                $result = $statement4->execute()->getGeneratedValue();
            }
            
            $_strTransaction = "COMMIT";
            $statementTransaction = $this->adapter->query($_strTransaction);
            $resultTransaction = $statementTransaction->execute();
            return $idCopiaPreg;
        }
        
        public function consultaCopiaExistente($nombrePregunta){
            $select = $this->sql->select();
            
            $_arrData = array(
                'total' => new Expression("count(t11pregunta)")
            );
            
            $select->from(array('t11' => 't11pregunta'))
                    ->columns($_arrData)
                    ->where->like('t11pregunta', "%$nombrePregunta%");
            $arrTotal = $this->obtenerRegistros($select);
            return $arrTotal['0']['total'];
        }
        /**
         * @author rjuarez
         * @param type $idPregunta a editar
         * @return boolean en caso de que lapregunta esta relacionada en algun examen publicado
         */
        public function existePreguntaInExamen($idPregunta, $idExamen=null){
            $select = $this->sql->select();
            $_arrData = array('t11id_pregunta','t11pregunta','t11estatus');
            $select->from(array('t11'=>'t11pregunta'))->columns($_arrData)
            ->join(array('t16' => 't16seccion_pregunta'), 't11.t11id_pregunta=t16.t11id_pregunta')
            ->join(array('t20' => 't20seccion_examen'), 't16.t20id_seccion_examen=t20.t20id_seccion_examen')
            ->join(array('t12' => 't12examen'), 't12.t12id_examen=t20.t12id_examen')
            ->where('t12.t12estatus= "PUBLICADO"')
            ->where('t11estatus='.EstatusPregunta::ACTIVA);
            if($idExamen == null){
                $select->where('t11.t11id_pregunta='.$idPregunta);
            }else{
                $select->where('t12.t12id_examen='.$idExamen);
            }
            $_arrRegistros = $this->obtenerRegistros($select,$_arrData[0],$_arrData[0]);
            if(count($_arrRegistros) == 0){
                $bolOk = false;//pregunta no existe en examen publicado
            }else{
                $bolOk = true; //pregunta existe en examen publicado
            }
            return $bolOk;
        }
        
       /**
        * 
        * @param type $elementos
        * @return boolean
        */

        function copiarImagenes($elementos){
            
            $origen = $this->config['constantes']['RUTA_ORIGEN_IMAGENES'];
            $destino =$this->config['constantes']['RUTA_DESTINO_IMAGENES'];
            $indice=0;
            $listaErrores=array();
            if(file_exists($destino)){
                foreach ($elementos as $archivo){
                    if(substr($archivo, 0, 7)=="http://"||substr($archivo, 0, 8)=="https://"){
                        $imagen=$archivo;
                    }
                    else{
                        $imagen=$origen.$archivo;
                    }
                    $contenido=file_get_contents($imagen);
                    if(strlen($contenido)){
                        $guardarImagen = fopen($destino.basename($archivo), 'w');
                        fwrite($guardarImagen, $contenido);
                        fclose($guardarImagen);
                    }
                    else{
                        $listaErrores[$indice]=$imagen;
                        $indice++;
                    }                   
                }
            }
            if(count($listaErrores)>0){
                $estado=$listaErrores;
            }
            else{
                $estado=true;
            }
            return $estado; 
                
        }
        /**
         * Genera un archivo .zip a partir de un directorio especifico.
         * @param type $nombreArchivo nombre con el que se desea nombrar el archivo
         */
        
        public function generarZip($nombreArchivo){
            $origen = $this->config['constantes']['RUTA_ORIGEN_IMAGENES'];
            $destino =$this->config['constantes']['RUTA_DESTINO_IMAGENES'];

            if(file_exists($origen)&&file_exists($destino)){
                $zip = new ZipArchive;
                if(strpos($nombreArchivo, '.zip')){
                   $create = $zip->open($destino.$nombreArchivo, ZipArchive::CREATE); 
                }else{
                   $create = $zip->open($destino.$nombreArchivo.'.zip', ZipArchive::CREATE);
                }
                
                $extensionesImg=array('png','jpg','jpeg');
                $extensionesAudio=array('mp3','wma','wav');
                $extensionesVideo=array('avi','mpeg','wmv');
                $crearDirImg=$this->existeArchivo($extensionesImg, $origen);
                $crearDirAudio=$this->existeArchivo($extensionesAudio, $origen);
                $crearDirVideo=$this->existeArchivo($extensionesVideo, $origen);;
                if($crearDirImg==true)
                    $zip->addEmptyDir("img/");                
                if($crearDirAudio==true)
                    $zip->addEmptyDir("audio/");
                if($crearDirVideo==true)
                    $zip->addEmptyDir("video/");
                
                if ($create == true) {
                    foreach (glob($origen."*") as $nombre_fichero){
                        for($i=0; $i<count($extensionesAudio); $i++){
                            if(end(explode(".", $nombre_fichero))==$extensionesAudio[$i]){
                                    $zip->addFile($nombre_fichero, 'audio/'.basename($nombre_fichero));
                            }
                        }
                        for($i=0; $i<count($extensionesImg); $i++){
                            if(end(explode(".", $nombre_fichero))==$extensionesImg[$i]){                           
                                $zip->addFile($nombre_fichero, 'img/'.basename($nombre_fichero));
                            }
                        }
                        for($i=0; $i<count($extensionesVideo); $i++){
                            if(end(explode(".", $nombre_fichero))==$extensionesVideo[$i]){                           
                                $zip->addFile($nombre_fichero, 'video/'.basename($nombre_fichero));
                            }
                        }
                        
                        
                    }
                    $zip->close();

                }
            }
        }
        /**
         * Comprobar la existencia de un tipo de archivo en un directorio.
         * @param type $extensiones arreglo con las extensiones que se desean encontrar.
         * @param type $directorio
         * @return boolean true en caso de que encuentre al menos una coincidencia,
         * false en caso contrario.
         */
        protected function existeArchivo($extensiones, $directorio){
            $existe=false;
            foreach (glob($directorio."*") as $nombre_fichero){
                for($i=0; $i<count($extensiones); $i++){
                    if(end(explode(".", $nombre_fichero))==$extensiones[$i]){
                        $existe=true;
                    }
                }          
            }
            return $existe;
        }
        /**
         * Realiza la descarga de un archivo. 
         * @param type $archivo ruta del archivo que se desea descargar.
         */
        public function descargarArchivo($archivo){
            if (file_exists($archivo)){
                ob_clean();
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($archivo).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($archivo));
                readfile($archivo);
            }
        }
        /**
         * Eliminar Fichero, ya sea archivo o directorio.
         * @param type $fichero ruta del fichero que se desea eliminar.
         * @return boolean rue en caso de que el fichero se elimine, false en caso
         * contrario.
         */
        public function eliminarFichero($fichero){
            $estado=false;
            if (file_exists($fichero)){
                    if(is_file($fichero)){
                            $estado=unlink($fichero);
                    }
                    if(is_dir($fichero)){
                            $estado=rmdir($fichero);
                    }
            }
            return $estado;
        }
}
