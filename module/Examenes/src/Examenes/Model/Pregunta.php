<?php
namespace Examenes\Model;
class Pregunta
{

    /**
     * 
     * @var int 
     */
    protected $id_pregunta;

    /**
     * 
     * @var int
     */
    protected $id_pregunta_parent;

    /**
     * 
     * @var int
     */
    protected $id_tipo_pregunta;

    /**
     * 
     * @var string
     */
    protected $pregunta;

    /**
     * 
     * @var string
     */
    protected $nombre_corto_clave;

    /**
     * 
     * @var string
     */
    protected $instruccion;

    /**
     * 
     * @var string
     */
    protected $tiempo_limite;

    /**
     * 
     * @var int
     */
    protected $id_numeracion;

    /**
     * 
     * @var string
     */
    protected $fecha_registro;

    /**
     * 
     * @var string
     */
    protected $fecha_actualiza;

    /**
     * 
     * @var string
     */
    protected $estatus;

    
     public function exchangeArray($data)
     {         
        $this->id_pregunta          = (!empty($data['t11id_pregunta']))         ? $data['t11id_pregunta'] : null;
        $this->id_pregunta_parent   = (!empty($data['t11id_pregunta_parent']))  ? $data['t11id_pregunta_parent'] : null;
        $this->id_tipo_pregunta     = (!empty($data['c03id_tipo_pregunta']))    ? $data['c03id_tipo_pregunta'] : null;
        $this->pregunta             = (!empty($data['t11pregunta']))            ? $data['t11pregunta'] : null;
        $this->nombre_corto_clave   = (!empty($data['t11nombre_corto_clave']))  ? $data['t11nombre_corto_clave'] : null;
        $this->instruccion          = (!empty($data['t11instruccion']))         ? $data['t11instruccion'] : null;
        $this->tiempo_limite        = (!empty($data['t11tiempo_limite']))       ? $data['t11tiempo_limite'] : null;
        $this->columnas             = (!empty($data['t11columnas']))            ? $data['t11columnas'] : null;                
        $this->id_numeracion        = (!empty($data['c06id_numeracion']))       ? $data['c06id_numeracion'] : null;
        $this->fecha_registro       = (!empty($data['t11fecha_registro']))      ? $data['t11fecha_registro'] : null;
        $this->fecha_actualiza      = (!empty($data['t11fecha_actualiza']))     ? $data['t11fecha_actualiza'] : null;
        $this->estatus              = (!empty($data['t11estatus']))             ? $data['t11estatus'] : null;        
     }
     
    public function getArrayCopy(){
        return get_object_vars($this);
    }
}