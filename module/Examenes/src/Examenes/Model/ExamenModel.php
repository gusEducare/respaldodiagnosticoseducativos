<?php

namespace Examenes\Model;

//use Examenes\Entity\Examen;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

final class TipoExamen {

    const CUANTITATIVO = 1;
    const CUALITATIVO = 2;
    const ENCUESTA = 3;
    const TODO = 4;

}

final class TipoPregunta {

    const OPMULT = 1;
    const RESMUL = 2;
    const VERFAL = 3;
    const RELARRA = 4;
    const RELCON = 5;
    const PREGAB = 6;
    const PREGCO = 7;
    const ARRACO = 8;
    const ARRAOR = 9;
    const NUMORD = 10;
    const RELNUM = 11;
    const RELLIN = 12;
    const MATRIZ = 13;
    const ROMPEC = 14;
    const CRUCIG = 15;
    const SOPLET = 16;

}

final class EstatusExamen {

    const APROBADO = 'APROBADO';
    const REPROBADO = 'REPROBADO';
    const NOPRESENTADO = 'NOPRESENTADO';
    const NOAGENDADO = 'NOAGENDADO';
    const NODISPONIBLE = 'NODISPONIBLE';
    const FINALIZADO = 'FINALIZADO';
    const INICIADO = 'INICIADO';
    const LICENCIAVENCIDO = 'LICENCIA VENCIDO';
    const ACTIVO = 'Activo';
    const INACTIVO = 'Inactivo';

}

final class EstatusExamenAgenda {

    const NOAGENDADO = 'NOAGENDADO';
    const LISTORESOLVER = 'LISTORESOLVER';
    const NODISPONIBLE = 'NODISPONIBLE';
    const INTERRUMPIDO = 'INTERRUMPIDO';
    const INTERRUMPIDOVENCIDO = 'INTERRUMPIDOVENCIDO';
    const INTERRUMPIDORESOLVER = 'INTERRUMPIDORESOLVER';
    const FINALIZADO = 'FINALIZADO';
    const NOAGENDADO1 = 'NOAGENDADO1';
    const APROBADO = 'APROBADO';
    const REPROBADO = 'REPROBADO';

}

final class TipoCopia {

    const COPIA_PARCIAL = 1;
    const COPIA_COMPLETA = 2;
}

final class EstatusSeccion {

    const ACTIVA = 1;
    const INACTIVA = 2;
}

final class TipoEvaluacion {

    const TOTAL = 1;
    const PARCIAL = 2;
}

/**
 * Clase encargada en la creación de examenes, con todos sus metodos y
 * atributos.
 *
 * @author jpgomez@grupoeducare.com.
 * @author oreyes@grupoeducare.com.
 * @since 02-12-2014
 * @copyright Grupo Educare SA de CV derechos reservados.
 *
 */
class ExamenModel {

    /**
     * Id de la seccion a donde se almacenaran las preguntas que no tengan
     * asignada una seccion 
     */
    const ID_SECCION_GRAL = 1;

    /**
     * Instancia de de la clase de SQL util para la conexión a la Base de datos
     * @var Sql $sql  :
     */
    protected $sql;

    /**
     * Instancia de la clase Logger util para poder depurar en la consola de
     * apache
     * @var  Logger
     */
    protected $_objLog;

    /**
     * Instancia de la clase logger para firebug.
     * @var Logger $log
     */
    protected $log;

    /**
     *
     * @var type 
     */
    protected $config;

    /**
     * Constructor de clase inicia el adapter, crea instancia del objeto Logger.
     *
     * @param Adapter $adapter
     */
    protected $adapter;

    public function __construct(Adapter $adapter,$config){
        $this->adapter = $adapter;
        $this->sql = new Sql($adapter);
        $this->_objLog = new Logger();
        $this->_objLog->addWriter(new Stream('php://stderr'));
        $this->config = $config;
        $this->log = new Logger();
        $this->log->addWriter(new Stream('php://stderr'));
    }

    /*
     * funcion para eliminar el examen.
     */

    public function eliminarExamen($_intIdExamen){
        //$cambiarStatus = $this->sql->update()->table('')->set($values)
        $_instanciaUpdate = $this->sql->update()
                ->table('t12examen')
                ->set(array('t12estatus' => $this->config['constantes']['ESTATUS_INACTIVO']))
                ->where(array('t12id_examen' => $_intIdExamen));

        $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdate);
        if ($result->execute()->getAffectedRows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Funcion para actualizar objetos examen en base de datos.
     * @param unknown $_examen
     * @return \Zend\Db\Adapter\Driver\StatementInterface
     */
    public function actualizarExamen(Examen $_examen, $_intIdExamen) {
        $this->_objLog->debug("Quien es examen: " . $_examen);
        $updateExamen = $this->sql->update('t12examen');
        $updateExamen->set(array(
                    'c02id_tipo_examen' => $_examen->__get('id_tipo_examen'),
                    'c04id_escala' => $_examen->__get('id_escala'),
                    'c05id_nivel' => $_examen->__get('id_nivel'),
                    't12nombre' => $_examen->__get('nombre'),
                    't12nombre_corto_clave' => $_examen->__get('nombre_corto_clave'),
                    't12instrucciones' => $_examen->__get('instrucciones'),
                    't12descripcion' => $_examen->__get('descripcion'),
                    't12fecha_registro' => date('Y/m/d g:i:s'),
                    't12num_intentos' => $_examen->__get('num_intentos'),
                    't12aleatorio_preguntas' => $_examen->__get('aleatorio_preguntas'),
                    't12aleatorio_respuestas' => $_examen->__get('aleatorio_respuestas'),
                    't12demo' => $_examen->__get('demo'),
                    't12minimo_aprobatorio' => $_examen->__get('minimo_aprobatorio'),
                    't12tiempo' => $_examen->__get('tiempo'),
                    't12estatus' => $_examen->__get('estatus')))
                ->where(array('t11id_pregunta' => $_intIdExamen));


        $result = $this->sql->prepareStatementForSqlObject($updateExamen);
        return $result->execute()->getGeneratedValue();
    }

    /**
     * Funcion que obtiene los tipos de examenes disponibles en la base de datos
     * @return $_arrTipos
     */
    public function obtenerTipos() {
        $select = $this->sql->select();
        $_arrDatos = array('c02id_tipo_examen', 'c02tipo_examen', 'c02url_icono_tipo');
        $select->from(
                        array('c02' => 'c02tipo_examen'))
                ->columns($_arrDatos);
        $_arrTipos = $this->obtenerRegistros($select, $_arrDatos[0], $_arrDatos[1]);
        return $_arrTipos;
    }

    /**
     * Funcion que obtiene las categorias disponibles en la base de datos
     * @return $_arrCategorias
     */
    public function obtenerCategorias(){          
        $select = $this->sql->select();
        $_arrDatos = array( 'c08id_categoria','c08nombre');
        $select ->from( 
                    array( 'c08'=>'c08categoria') )
                ->columns( $_arrDatos )
                ->join(array('t26'=>'t26examen_categoria'),
                            't26.c08id_categoria=c08.c08id_categoria',
                            array())
                ->group('t26.c08id_categoria');	
        $_arrCategorias = $this->obtenerRegistros($select, $_arrDatos[0], $_arrDatos[1]);
        return $_arrCategorias;
        
    }
    
    /** 
     * Funcion que se encarga de obtener los textos que se pintarán en el examen
     * antes de comenzar a resolverse. El número de intento indica las 
     * oportunidades que tiene el usuario para resolver el examen, en caso de  
     * ser 0, no tinene posibilidades
     * @param type $_arrExamen
     * @param type $_intNumIntento
     * @return string
     */
    public function obtenerTextos($_arrExamen, $_intNumIntento, $_strTiempoRestante) {

        switch ($_arrExamen['c02id_tipo_examen']) {
            case TipoExamen::CUANTITATIVO:
                $_strMinimo = 'La calificación minima aprobatoria es de ' .
                        '<span class="text-bold">' . $_arrExamen['t12minimo_aprobatorio'] . '</span>.';
                break;
            case TipoExamen::CUALITATIVO:
                $_strMinimo = 'Al finalizar esta evaluación sabrás el nivel ' .
                        'de desarrollo en el que te encuentras.';
                break;
            case TipoExamen::ENCUESTA:
                $_strMinimo = 'Al finalizar esta encuesta se almacenarán tus ' .
                        'resultados.';
                break;
            case TipoExamen::TODO:
                $_strMinimo = 'Pon a prueba tus conocimientos y descubre ' .
                        'tus resultados.';
                break;
        }
        
        $timeRest = $_strTiempoRestante[1];
        $longitud = strlen($timeRest);
        if($longitud==1){
            $posicion1 = "0$timeRest";
            $tiempoRestante = array('0'=>$_strTiempoRestante[0],'1'=>$posicion1,'2'=>$_strTiempoRestante[2]);
            //error_log('tiemporestante-->'.print_r($tiempoRestante,true));
        }else{
            $tiempoRestante=$_strTiempoRestante;
        }
        $_strTiempo = $_arrExamen['t12tiempo'] === '0' ? 'Esta evaluación no cuenta ' .
                'con límite de tiempo.' : 'El tiempo para realizar la evaluación es de ' .
                '<span class="text-bold">' . $_arrExamen['t12tiempo'] . ' minutos</span> ' .
                ' del cual te resta: ' .
                '<span class="text-bold">' . implode(':', $tiempoRestante) . '</span> ' .
                'y comienza a correr una vez que des clic en el botón "Iniciar Evaluación".';
        //error_log('TIEMPO-->'.print_r($_strTiempoRestante,true));
        if ($_arrExamen['t12num_intentos'] === '1') {
            $_strVez = 'vez';
        } else {
            $_strVez = 'veces';
        }
        $_strVez = (int) $_arrExamen['t12num_intentos'] === 1 ? ' vez' : ' veces';
        $_strTextoIntentos = $_intNumIntento === 1 ? ' y este es tu primer intento' : ' y este es tu intento número: <span class="text-bold">' . $_intNumIntento . '</span>';
        if ($_intNumIntento === 0) {
            $_strIntentos = 'No tienes intentos disponibles para realizar esta ' .
                    'evaluación';
        } else {
            $_strIntentos = 'Esta evaluación se puede resolver <span class="text-bold">' .
                    $this->textoIntentos( $_arrExamen['t12num_intentos'] ) . 
                    '</span> ' . $_strTextoIntentos . '.';
        }
        $_arrTextos = array(
            'minimo' => $_strMinimo,
            'tiempo' => $_strTiempo,
            'intentos' => $_strIntentos,
        );
        return $_arrTextos;
    }

    /**
     * Funcion que obtiene las escalas  disponibles en la base de datos
     * @param $_intIdEscala Se utiliza este parametno para obtener una escala
     *                      especifica
     * @return $_arrTipos
     */
    public function obtenerEscalas($_intIdEscala = 0) {
        $select = $this->sql->select();
        $_arrDatos = array('c04id_escala', 'c04escala');
        $select->from(
                        array('c04' => 'c04escala'))
                ->columns($_arrDatos)
                ->where('c04estatus = "A"');

        if ($_intIdEscala) {
            $select->where('c04id_escala = ' . $_intIdEscala);
        }


        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $arrEscalas = $resultSet->toArray();
        if ($_intIdEscala) {
            $_strEscala = explode('-', $arrEscalas[0]['c04escala']);
            $_intEscala = (int) $_strEscala[1];
            return $_intEscala;
        }
        return $arrEscalas;
    }

    /**
     * Funcion que obtiene los tipos de examenes disponibles en la base de datos
     * @return $_arrTipos
     */
    public function obtenerNiveles() {
        $select = $this->sql->select();
        $_arrDatos = array('c05id_nivel', 'c05nivel');
        $select->from(
                        array('c05' => 'c05nivel'))
                ->columns($_arrDatos);
        $arrNiveles = $this->obtenerRegistros($select, $_arrDatos[0], $_arrDatos[1]);
        return $arrNiveles;
    }

    /**
     * Funcion que se encarga de obtener todos los examenes que ha registrado
     * el usuario que se encuentra actualmente logueado. Se manejan los estatus 
     * de la tabla t04examen_usuario mas otros que dependen de si el examen ya
     * fue agendado.
     * @param type $_intIdUsuario
     */
    public function obtenerExamenesUsuario($_intIdUsuario, $_arrGrupos) {

        $select = $this->sql->select();
        $selectSub = $this->sql->select();
        $_arrDatos = array(
            't04calificacion',
            't12id_examen',
            't04estatus',
            't04id_examen_usuario'
            );
        $select ->from(array( 't03'=>'t03licencia_usuario') )
                ->columns( array(
                             't03fecha_activacion'  => 't03fecha_activacion', 
                             't03licencia'          => 't03licencia'
                    ) )
                ->join(array('t04'=>'t04examen_usuario'),
                            't03.t03id_licencia_usuario = t04.t03id_licencia_usuario',
                            $_arrDatos)
                ->join(array('t02'=>'t02usuario_perfil'),
                            't02.t02id_usuario_perfil = t03.t02id_usuario_perfil',
                            array())
                ->join(array('t12'=>'t12examen'),
                            't12.t12id_examen =  t04.t12id_examen ',
                            array('t12nombre'))
                ->join(array('c02'=>'c02tipo_examen'),
                            't12.c02id_tipo_examen  =  c02.c02id_tipo_examen',
                            array('c02tipo_examen', 'c02id_tipo_examen'))  
                //se agrega join que nos indica si el examen es Intellectus para redireccionar a reportes especificos  LAFC  19/08/2015
                ->join(array('t26' => 't26examen_categoria'), 
                        new Expression(
                                't26.t12id_examen = t12.t12id_examen  AND t26.c08id_categoria IN (' . 
                                    intval($this->config['constantes']['ID_CATEGORIA_INTELLECTUS']) .', '.intval($this->config['constantes']['ID_CATEGORIA_INTELLECTUS_POST']) . 
                                    ')'), array('is_intellectus' => new Expression('IF(t26.t26id_examen_categoria IS NULL, 0, 1)')), Select::JOIN_LEFT)
                ->where(array('t02.t02id_usuario_perfil' => $_intIdUsuario))
                ->group('t04id_examen_usuario');
        //$this->log->debug('Consulta: ' . $select->getSqlString());
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        
        

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrExamenes = $resultSet->toArray();
        $_arrResp = array();
        $countEx = 0;
        foreach ($_arrExamenes as $data) {
            $_intIdUsuarioLicencia = $this->obtenerUsuarioLicencia($_intIdUsuario, $data['t12id_examen']);
            $_intNumIntentos = $this->obtenerNumeroIntentos($_intIdUsuarioLicencia, $data['t12id_examen']);
            if(EstatusExamen::NOPRESENTADO === $data['t04estatus'] || EstatusExamen::INICIADO === $data['t04estatus']){
                $data['t04estatus'] = $this->obtenerEstatusExamen( $data['t12id_examen'], $_arrGrupos, $data['t04estatus'], $_intIdUsuario);
            }
            $_arrTextoEstatus = $this->obtenerTextoEstatus($data['t04estatus']);
            $_strFecha = 'El examen no se encuentra actualmente agendado';
            $_arrResp[$_arrTextoEstatus['clave']]['tipo'] = $_arrTextoEstatus['texto'];
            $_arrResp[$_arrTextoEstatus['clave']]['examenes'][$data['t12id_examen']][$countEx] = 
                         array(
                            'id' => $data['t12id_examen'],
                            'nombre' => $data['t12nombre'],
                            'clave_estatus' => $data['t04estatus'],
                            'estatus' => $_arrTextoEstatus['texto'],
                            'fecha' => $_strFecha, //$data['t03fecha_activacion'],
                            'licencia' => $data['t03licencia'],
                            'intentos' => $_intNumIntentos,
                            'is_intellectus' => $data['is_intellectus'],
                            'calificacion' => $data['t04calificacion'],
                            'id_examen_usuario' => $data['t04id_examen_usuario'],
                            'c02id_tipo_examen' => $data['c02id_tipo_examen'],
                    );            
            $countEx++;
        }
        return $_arrResp;
    }

    protected function obtenerEstatusExamen( $_intIdExamen, $_arrGrupos, $_strEstatus, $_intIdUsuario ){
        $select = $this->sql->select();
        $_arrGruposForm = array();
        foreach( $_arrGrupos as $index => $grupos){
            $_arrGruposForm[$index] = $grupos['t05id_grupo'];
        }
        $_arrDatos = array( 't09id_agenda', 't04estatus' => new Expression("
                                IF(`t09`.`t09estatus` = 'CANCELADO',
                               'NOAGENDADO',
                               IF((`t09`.`t09fecha_inicio` < NOW()
                                       AND `t09`.`t09fecha_final` > NOW()),
                                   'LISTORESOLVER',
                                   IF(`t09`.`t09fecha_inicio` > NOW(),
                                       'NODISPONIBLE',
                                       'NOAGENDADO')))
                 "));
        $select ->from(array( 't09'=>'t09agenda') )
                ->columns($_arrDatos)
                ->join(array('t05'=>'t05grupo'),
                            't05.t05id_grupo = t09.t05id_grupo', array())
                ->join(array('t08'=>'t08administrador_grupo'),
                            't05.t05id_grupo = t08.t05id_grupo', array())
                ->where(array('t12id_examen' => $_intIdExamen))
                ->where(array('t05estatus' => EstatusExamen::ACTIVO))
                ->where(array('t08usuario_ligado' => $this->config['constantes']['ESTATUS_LIGADO']))
                ->where(array('t02id_usuario_perfil' => $_intIdUsuario))
                ->where(array('t05.t05id_grupo' => $_arrGruposForm));
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrAgenda = $resultSet->toArray();
        if(!count($_arrAgenda)){
            return EstatusExamenAgenda::NOAGENDADO;
        }
        foreach($_arrAgenda as $data){
            $_strEstatusT09 = $data['t04estatus'];
            $_strEstatusNuevo = '';            
            if( $_strEstatus === EstatusExamen::NOPRESENTADO ){
                if( $_strEstatusT09 === EstatusExamenAgenda::LISTORESOLVER ){
                    return $_strEstatusT09;
                }elseif( $_strEstatusT09 === EstatusExamenAgenda::NODISPONIBLE ){
                    $_strEstatusNuevo = $_strEstatusT09; // Se almacena el estatus de 
                }else{
                    if( $_strEstatusNuevo !== EstatusExamenAgenda::NODISPONIBLE ){
                        $_strEstatusNuevo = $_strEstatusT09; // Se regresa el estatus de no agendado, a menos que exista otro registro con estatus no disponible
                    }else{
                        //$this->_objLog->debug('Estatus que no entra: ' . $_strEstatusT09);                          
                    }
                }
            }else{                      
                if( $_strEstatusT09 === EstatusExamenAgenda::LISTORESOLVER ){
                    return EstatusExamenAgenda::INTERRUMPIDO;
                }elseif( $_strEstatusT09 === EstatusExamenAgenda::NODISPONIBLE ){
                    $_strEstatusNuevo = EstatusExamenAgenda::INTERRUMPIDORESOLVER; // Se almacena el estatus de 
                }else{
                    if( $_strEstatusNuevo !== EstatusExamenAgenda::NODISPONIBLE ){
                        $_strEstatusNuevo = EstatusExamenAgenda::INTERRUMPIDOVENCIDO; // Se regresa el estatus de no agendado, a menos que exista otro registro con estatus no disponible
                    }else{
                        $this->_objLog->debug('Estatus que no entra: ' . $_strEstatusT09);                          
                    }
                }                     
            }            
        }
        return $_strEstatusNuevo;
    }
    
    /**
     * Función que se encarga de obtener la lista de examenes interrumpidos de
     * acuerdo al grupo seleccionado
     * @param type $_intIdGrupo
     */
    public function obtenerExamenesInterrumpidos($_intIdGrupo) {
        $select = $this->sql->select();
        $_arrDatosT04 = array('t04id_examen_usuario', 't04fecha_inicio');
        $_arrDatosT01 = array('t01nombre', 't01apellidos');
        $_arrDatosT12 = array('t12nombre');
        $_arrDatosT03 = array('t03licencia');
        $_arrVacio = array();
        $select->from(array('t04' => 't04examen_usuario'))
                ->columns($_arrDatosT04)
                ->join(array('t03' => 't03licencia_usuario'), 't03.t03id_licencia_usuario = t04.t03id_licencia_usuario', $_arrDatosT03)
                ->join(array('t02' => 't02usuario_perfil'), 't03.t02id_usuario_perfil = t02.t02id_usuario_perfil', $_arrVacio)
                ->join(array('t01' => 't01usuario'), 't01.t01id_usuario = t02.t01id_usuario', $_arrDatosT01)
                ->join(array('t12' => 't12examen'), 't12.t12id_examen = t04.t12id_examen', $_arrDatosT12)
                ->join(array('t08' => 't08administrador_grupo'), 't08.t02id_usuario_perfil = t02.t02id_usuario_perfil', $_arrVacio)
                ->where(array('t05id_grupo' => $_intIdGrupo, 't04estatus' => EstatusExamen::INICIADO));

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrExamenes = $resultSet->toArray();

        return $_arrExamenes;
    }

    /**
     * Funcion que se encarga de obtener las calificaciones de un grupo
     * @param type $_intIdGrupo
     * @return type
     */
    public function obtenerCalificacionesGrupo($_intIdGrupo) {
        
        
        
        $select = $this->sql->select();
        $_arrDatosT04 = array('t04calificacion');
        $_arrDatosT01 = array('t01nombre', 't01apellidos');
        $_arrDatosT05 = array('t05descripcion');
        $_arrDatosT12 = array('t12nombre');
        $_arrDatosT03   = array('t03licencia');
        $_arrVacio = array();
        $select->from(array('t04' => 't04examen_usuario'))
                ->columns($_arrDatosT04)
                ->join(array('t03' => 't03licencia_usuario'), 't03.t03id_licencia_usuario = t04.t03id_licencia_usuario', $_arrDatosT03)
                ->join(array('t02' => 't02usuario_perfil'), 't03.t02id_usuario_perfil = t02.t02id_usuario_perfil', $_arrVacio)
                ->join(array('t01' => 't01usuario'), 't01.t01id_usuario = t02.t01id_usuario', $_arrDatosT01)
                ->join(array('t08' => 't08administrador_grupo'), 't08.t02id_usuario_perfil = t02.t02id_usuario_perfil', $_arrVacio)
                ->join(array('t05' => 't05grupo'), 't05.t05id_grupo = t08.t05id_grupo', $_arrDatosT05)
                ->join(array('t12' => 't12examen'), 't12.t12id_examen = t04.t12id_examen', $_arrDatosT12)
                ->where(array(
                    't05.t05id_grupo' => $_intIdGrupo,
                        //'t04estatus' => EstatusExamen::INICIADO
                ))
                ->group('t04.t12id_examen')
                ->group('t02.t02id_usuario_perfil');
        $select->where->isNotNull('t04calificacion');
        $select->where->notEqualTo('t04calificacion', '');
        $select->order('t01apellidos');
        
        $select->where->addPredicate(
                new \Zend\Db\Sql\Predicate\Expression(
                        't04.t12id_examen NOT IN (
                                SELECT t12id_examen FROM t26examen_categoria t26 WHERE (t26.c08id_categoria =  '.$this->config['constantes']['ID_CATEGORIA_INTELLECTUS']
                        .' OR  t26.c08id_categoria = '.$this->config['constantes']['ID_CATEGORIA_INTELLECTUS_POST'].'))'
                    )
                );


        $statement = $this->sql->prepareStatementForSqlObject($select);
        
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrCalificaciones = $resultSet->toArray();

        return $_arrCalificaciones;
    }

    /**
     * Funcion que se encarga de cambiar el estatus del examen a NOPRESENTADO
     * para que el usuario pueda volverlo a contestar
     * @param type $_intIdExamenUsuario
     * @return type
     */
    public function desbloquearExamen($_intIdExamenUsuario) {
        $cambiarStatus = $this->sql->update()
                ->table('t04examen_usuario')
                ->set(array('t04estatus' => EstatusExamen::NOPRESENTADO))
                ->where(array('t04id_examen_usuario' => $_intIdExamenUsuario));

        $result = $this->sql->prepareStatementForSqlObject($cambiarStatus);
        return $result->execute()->getAffectedRows() > 0;
    }

    /**
     * Funcion que se encarga de cambiar el estatus del examen a NOPRESENTADO
     * para que el usuario pueda volverlo a contestar
     * @param type $_intIdExamenUsuario
     * @return type
     */
    public function resetearExamen($_intIdExamenUsuario) {
        $this->config['AMBIENTE'] == "DESARROLLO";
        $cambiarStatus = $this->sql->update()
                ->table('t04examen_usuario')
                ->set(array('t04estatus' => EstatusExamen::NOPRESENTADO))
                ->where(array('t04id_examen_usuario' => $_intIdExamenUsuario));

        $result = $this->sql->prepareStatementForSqlObject($cambiarStatus);
        $result->execute()->getAffectedRows() > 0;
        
        $_strQueryDelete = 'DELETE FROM `evaluaciones`.`t34meta_examen` WHERE t03id_licencia_usuario in (SELECT t03id_licencia_usuario FROM t04examen_usuario
                             WHERE t04id_examen_usuario= :idExamenUsr);';
                            $statement = $this->adapter->query($_strQueryDelete);
                            try {
                                $results = $statement->execute(array(
                                    ":idExamenUsr" => $_intIdExamenUsuario
                                ));
                            } catch (Exception $e) {
                                return false;
                            }
        return true;
    }
    
    public function resetExam($llave) {
        $this->config['AMBIENTE'] == "DESARROLLO";
        
        $_strQueryUpdate = 'UPDATE t04examen_usuario set t04estatus="NOPRESENTADO" WHERE t03id_licencia_usuario in (SELECT t03id_licencia_usuario FROM t03licencia_usuario
                             WHERE t03licencia= :idLicencia);';
                            $statement = $this->adapter->query($_strQueryUpdate);
                            try {
                                $results = $statement->execute(array(
                                    ":idLicencia" => $llave
                                ));
                            } catch (Exception $e) {
                                return false;
                            }
        
        $_strQueryDelete = 'DELETE FROM `evaluaciones`.`t34meta_examen` WHERE t03id_licencia_usuario in (SELECT t03id_licencia_usuario FROM t03licencia_usuario
                             WHERE t03licencia= :idLicencia);';
                            $statement = $this->adapter->query($_strQueryDelete);
                            try {
                                $results = $statement->execute(array(
                                    ":idLicencia" => $llave
                                ));
                            } catch (Exception $e) {
                                return false;
                            }
        return true;
    }
    /**
     * Funcion que se encarga de obtener los examenes almacenados, en caso 
     * de recibir un id de examen, solo se obtiene la informacion del mismo
     * @param type $_intIdExamen
     * @return type
     */
    public function obtenerExamenes($_intIdExamen = 0, $_sinParent = false, $_intIdTipo = 0, $_consEstatus = null) {
        $select = $this->sql->select();
        $_arrData = array(
            't12id_examen',
            'c02id_tipo_examen',
            'c04id_escala',
            'c05id_nivel',
            't12nombre',
            't12nombre_corto_clave',
            't12instrucciones',
            't12descripcion',
            't12fecha_registro',
            't12fecha_actualiza',
            't12num_intentos',
            't12aleatorio_preguntas',
            't12aleatorio_respuestas',
            't12demo',
            't12minimo_aprobatorio',
            't12tiempo',
            't12estatus',
        );
        $_arrData1 = array(
            't26id_examen_categoria',
            'c08id_categoria',
            't12id_examen',
            't26estatus'
        );
        $select->from(array('t12' => 't12examen'))
                ->columns($_arrData)
                ->join(array('t26' => 't26examen_categoria'), 't26.t12id_examen = t12.t12id_examen',$_arrData1);
        if ($_consEstatus) {
            $select->where(array('t12.t12estatus' => $_consEstatus));
        }
        if ($_intIdExamen) {
            $select->where(array('t12.t12id_examen' => $_intIdExamen));
        }
        
        if( $_intIdTipo ){
            $select->join(array('t26' => 't26examen_categoria'), 't26.t12id_examen = t12.t12id_examen', array());
            $select->where(array('c08id_categoria' => $_intIdTipo));
        }
        
        if (!$_sinParent) {
            $_arrExamenes = $this->obtenerRegistros($select, $_arrData[0], $_arrData[4], $_arrData1[1],$_arrData[16]);
        } else {
            $_arrExamenes = $this->obtenerRegistros($select, $_arrData[0]);
        }
        //foreach($_arrExamenes as $indice=>$data){
        //    $_arrExamenes[$indice] = $this->stripTags($data);
        //}
        return $_arrExamenes;
    }
        
    /**
     * Funcion que se encarga de Filtrar los examenes que le corresponden al 
     * usuario, obtiene los examenes de la tablas correspondientes y el resto
     * los elimina
     * @param type $_arrResponseExamenes
     * @param type $_intIdUsuario
     * return $_arrExamenesFiltrados
     */
    public function filtrarExamenesUsuario($_arrResponseExamenes, $_intIdUsuario) {
        $select = $this->sql->select();
        $_arrDatos = array('t12id_examen', 't04estatus');
        $select->from(
                        array('t03' => 't03licencia_usuario'))
                ->columns(array())
                ->join(array('t04' => 't04examen_usuario'), 't03.t03id_licencia_usuario = t04.t03id_licencia_usuario', $_arrDatos)
                ->join(array('t02' => 't02usuario_perfil'), 't02.t02id_usuario_perfil = t03.t02id_usuario_perfil', array())
                ->where(array('t02.t02id_usuario_perfil' => $_intIdUsuario));
        $_arrExamenes = $this->obtenerRegistros($select, $_arrDatos[0], $_arrDatos[0]);
        foreach ($_arrExamenes as $ind => $exam) {
            $_arrExamenes[$ind] = (int) $exam;
        }
        //$_arrNuevo = array();
        if (isset($_arrResponseExamenes['dataSubElement'])) {
            foreach ($_arrResponseExamenes['dataSubElement'] as $index => $data) {
                if (!array_key_exists($data['id'], $_arrExamenes)) {
                    unset($_arrResponseExamenes['dataSubElement'][$index]);
                }
                //$_arrNuevo[] = $data;
            }
        } else { # El caso de los examenes que estan agrupados por tipo                
            foreach ($_arrResponseExamenes as $index => $data) {
                foreach ($data['examenes'] as $id => $examen) {
                    if (!array_key_exists($id, $_arrExamenes)) {
                        unset($_arrResponseExamenes[$index]['examenes'][$id]);
                    }
                }
                //$_arrNuevo[] = $data;
                if (!count($_arrResponseExamenes[$index]['examenes'])) {
                    unset($_arrResponseExamenes[$index]);
                }
            }
        }
        $_arrExamenesFiltrados = $_arrResponseExamenes;
        return $_arrExamenesFiltrados;
    }

    /**
     * Funcion que se encargara de ligar las preguntas asignadas a un examen
     * Recibo como parametros un array de preguntas y el id del examen al 
     * que se van ligar 
     * @param type $_idExamen
     * @param type $arrPreguntas
     * @return boolean retorna true si fue exitosa la migración
     */
    public function ligaPreguntasExamen($_idExamen, $arrPreguntas, $intIdSeccion, $nombreSeccion, $porcentaje, $tipoEv) {
        $idResult = false;
        // Si no recibo sección, debo de obtenerla, si aún así no existe,
        // me tengo que encargar de crearla
        if ($arrPreguntas != null) {
            $intIdSeccionExiste = $this->existeSeccion($_idExamen, $intIdSeccion);
            if (!$intIdSeccionExiste) {
                $intIdSeccion = $this->insertaSeccion($_idExamen, $nombreSeccion, $porcentaje, $tipoEv);
            } else {
                $bol = $this->actualizaSeccion($intIdSeccion, $nombreSeccion, $porcentaje, $tipoEv);
                //prepara array de preguntas en base de datos para el examen
                $arrPreguntaSeccion = $this->obtenerSeccionPregunta($intIdSeccion);
                if ($arrPreguntaSeccion) {
                    foreach ($arrPreguntaSeccion as $index => $preg) {
                        $arrpreguntasBD[$index] = $preg['t11id_pregunta'];
                    }
                    foreach ($arrPreguntas as $value => $data) {
                        $arrPreguntasVista[$value] = $data[1];
                    }

                    //compara las preguntas ordenadas y existentes en la vista del examen con las del array anterior
                    foreach ($arrpreguntasBD as $key => $id) {
                        $needle = array_search($id, $arrPreguntasVista);
                        if ($needle === false) {

                            $_strQueryDelete = 'DELETE
                                               FROM t16seccion_pregunta
                                               WHERE t11id_pregunta = :idPregunta  
                                               AND  t20id_seccion_examen = :idSeccEx';
                            $statement = $this->adapter->query($_strQueryDelete);
                            try {
                                $results = $statement->execute(array(
                                    ":idPregunta" => $id,
                                    ":idSeccEx" => $intIdSeccion
                                ));
                            } catch (Exception $e) {
                                return false;
                            }
                        }
                    }
                }
            }
                foreach ($arrPreguntas as $key => $data) {

                    /*
                     * validar si ya existe el registro y si ya existe actualiza el orden y si no existe inserta
                     * el registro 
                     */

                    $_bolExistePregunta = $this->existePreguntaExamen($intIdSeccion, $data[1]);

                    if ($_bolExistePregunta) {
                        $insertSeccion = $this->sql->update()
                                ->table('t16seccion_pregunta')
                                ->set(array('t16orden_pregunta' => $data[0]))
                                ->where(array(
                            't20id_seccion_examen' => $intIdSeccion,
                            't11id_pregunta' => $data[1],));
                    } else {


                        $insertSeccion = $this->sql
                                ->insert()
                                ->into('t16seccion_pregunta')
                                ->columns(array(
                                    't16orden_pregunta',
                                    't20id_seccion_examen',
                                    't11id_pregunta'))
                                ->values(array(
                            't16orden_pregunta' => $data[0],
                            't20id_seccion_examen' => $intIdSeccion,
                            't11id_pregunta' => $data[1]));
                    }
                    $result = $this->sql->prepareStatementForSqlObject($insertSeccion);
                    $idResult = $result->execute()->getAffectedRows();
                }
                $idResult = true;
            return $idResult;
        }else{
            $_strQueryDelete = 'DELETE
                FROM t16seccion_pregunta
                WHERE t20id_seccion_examen = :idSeccEx';
            $statement = $this->adapter->query($_strQueryDelete);
            try {
                $results = $statement->execute(array(
                    ":idSeccEx" => $intIdSeccion
                        ));
                } catch (Exception $e) {
                    return false;
                }
            
        }
    }

    /**
     * rompe la relacion que hay entre examen y la pregunta al momento de eliminar una pregunta
     * @param type $_idPre
     * @param type $_idSeccion
     * @return \Examenes\Model\Exception
     */
    public function eliminaRelPregExam($_idPre, $_idSeccion) {
        $_strQueryDelete = 'DELETE 
                    FROM t16seccion_pregunta
                    WHERE t11id_pregunta = :idPregunta  
                    AND  t20id_seccion_examen = :idSeccEx';

        $statement = $this->adapter->query($_strQueryDelete);
        try {
            if (is_array($_idPre)) {
                foreach ($_idPre as $_idPregunta) {
                    $results = $statement->execute(array(
                        ":idPregunta" => $_idPregunta,
                        ":idSeccEx" => $_idSeccion
                    ));
                }
                //DAR DE BAJA LA RELACION EXAMEN SECCION
                $_cambiarStatusSeccion = 'UPDATE t15seccion t15
                                          INNER JOIN t20seccion_examen t20
                                        SET t15.t15estatus = 2
                                        WHERE t20.t20id_seccion_examen = ' . $_idSeccion . '
                                        AND t20.t15id_seccion = t15.t15id_seccion';
                $_actualizo = $this->adapter->query($_cambiarStatusSeccion);
                $result = $_actualizo->execute($_cambiarStatusSeccion);
            } else {
                $results = $statement->execute(array(
                    ":idPregunta" => $_idPre,
                    ":idSeccEx" => $_idSeccion
                ));
            }
        } catch (Exception $e) {
            return $e;
        }
        return $results;
    }

    public function eliminaSeccionExamen($_idExamen, $_idSeccion) {
        $_cambiarStatusSeccion = 'UPDATE t15seccion t15
                                    INNER JOIN t20seccion_examen t20
                                    SET t15.t15estatus = 2
                                    WHERE t20.t20id_seccion_examen = ' . $_idSeccion . '
                                    AND t20.t15id_seccion = t15.t15id_seccion';
        $statement = $this->adapter->query($_cambiarStatusSeccion);
        $result = $statement->execute($_cambiarStatusSeccion);
        return $result;
    }

    /**
     * valida existe o no un registro que coincida con el id de examen y de pregunta
     * @param type $_idExamen
     * @param type $_idPregunta
     * @return type 
     */
    protected function existePreguntaExamen($_idSeccion, $_idPregunta) {

        $select = $this->sql->select();
        // $_dblPorcentaje = 0;
        $_arrData = array(
            't16orden_pregunta',
        );

        $select->from(array('t16' => 't16seccion_pregunta'))
                ->columns($_arrData)
                ->where(array(
                    't20id_seccion_examen' => $_idSeccion,
                    't11id_pregunta' => $_idPregunta,
        ));
        //$_arrRegistros = $this->obtenerRegistros($select, $_arrData[0], $_arrData[0]);
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRegistros = $resultSet->toArray();

        $_bolExistePregunta = count($_arrRegistros);
        return $_bolExistePregunta;
    }

    /**
     * Funcion que se encarga de revisar si existe una secion asignada al 
     * examen, en caso de que exista, regresa el id de la seccion, de lo 
     * contrario regresa un falso
     * @param type $_idExamen
     * @return type
     */
    public function existeSeccion($_idExamen, $intIdSeccion) {
        $select = $this->sql->select();
        $_arrData = array(
            't20id_seccion_examen',
            't12id_examen',
            't15id_seccion'
        );

        $select->from(array('t20' => 't20seccion_examen'))
                ->columns($_arrData)
                ->join(array('t15' => 't15seccion'), 't15.t15id_seccion = t20.t15id_seccion', array('t15seccion'))
                ->where(array('t12id_examen' => $_idExamen))
                ->where(array('t20.t20id_seccion_examen' => $intIdSeccion));
        $_arrSeccion = $this->obtenerRegistros($select, $_arrData[0]);
        if (count($_arrSeccion)) {
            $arrTmp = array_keys($_arrSeccion);
            $_idSeccion = $arrTmp[0]; // Envio siempre la primer pocisión 
        } else {
            $_idSeccion = false;
        }
        return $_idSeccion;
    }

    /**
     * Funcion que se encarga de asignar la seccion general a los examenes
     * donde no se les envien datos de seccion
     * 
     * @author lcencieros
     * @since 15/06/2015
     * @access public
     * 
     * @param int $_idExamen
     * @param string $seccion
     * @param int $porcentaje
     * @param int $respTotPar
     * @param int $estatus
     * 
     * @return type
     */
    public function actualizaSeccion($idSeccionEx, $seccion, $porcentaje, $respTotPar, $estatus = EstatusSeccion::ACTIVA) {

        $seccion = strip_tags($seccion);
        $seccion = trim($seccion);
        $seccion = strip_tags($seccion);

        $select = $this->sql->select();
        $_arrData = array('t15id_seccion');

        $select->from(array('t20' => 't20seccion_examen'))
                ->columns($_arrData)
                ->where(array('t20id_seccion_examen' => $idSeccionEx));
        $_arrSeccion = $this->obtenerRegistros($select, $_arrData[0]);

        $arrTmp = array_keys($_arrSeccion);
        $_idSeccion = $arrTmp[0]; // Envio siempre la primer pocisión 


        $_instanciaUpdate = $this->sql->update()
                ->table('t15seccion')
                ->set(array(
                    't15seccion' => $seccion,
                    't15valor_porcentaje' => $porcentaje,
                    't15resp_total_parcial' => $respTotPar,
                    't15estatus' => $estatus,
                ))
                ->where(array(
            't15id_seccion' => $_idSeccion));

        $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdate);
        return $result->execute()->getAffectedRows() > 0;
    }

    /**
     * Funcion que se encarga de asignar la seccion general a los examenes
     * donde no se les envien datos de seccion
     * @param type $_idExamen
     * @return type
     */
    public function insertaSeccion( $_idExamen,$seccion,$porcentaje, $respTotPar){  
        $insertaSeccion = $this->sql  ->insert()
                ->into('t15seccion')
                ->columns(array('t15seccion', 't15valor_porcentaje', 't15resp_total_parcial', 't15estatus'))
                ->values(array(
            't15seccion' => $seccion,
            't15valor_porcentaje' => $porcentaje,
            't15resp_total_parcial' => $respTotPar,
            't15estatus' => EstatusSeccion::ACTIVA
        ));
        $resul 	 = $this->sql->prepareStatementForSqlObject($insertaSeccion);
        $idSeccion = $resul->execute()->getGeneratedValue();

        $insertSeccion = $this->sql->insert()
                ->into('t20seccion_examen')
                ->columns(array('t12id_examen', 't15id_seccion'))
                ->values(array(
            't12id_examen' => $_idExamen,
            't15id_seccion' => $idSeccion));
        $result = $this->sql->prepareStatementForSqlObject($insertSeccion);
        return $result->execute()->getGeneratedValue();
    }

    /**
     * Esta funcion se encarga de mezclar la informacion de dos arrays, 
     * tomando como base el array padre, para que este array funcione, 
     * siempre tiene que coincidir el indice de los hijos, de lo contrario 
     * se devolvera un falso
     * @param type $_arrParent
     * @param type $_arrChildren
     * @return array
     */
    public function mezclaArreglos($_arrParent, $_strParent, $_arrChildren, $_strChildren) {
        $_arrResult = array();
        foreach ($_arrParent as $id => $dataP) {
            $_arrResult[$id] = array(
                $_strParent => $dataP,
                $_strChildren => array()
            );
            foreach ($_arrChildren as $idx => $dataC) {
                $_intidParent = (int) $_arrChildren[$idx]['id_parent'];
                if ($id === $_intidParent) {
                    if(isset($dataC['estatusExamen'])){
                        $_arrResult[$id][$_strChildren][$idx][0] = $this->stripTags($dataC['value']);
                        $_arrResult[$id][$_strChildren][$idx][1] = $this->stripTags($dataC['estatusExamen']);
                    }else{
                        $_arrResult[$id][$_strChildren][$idx] = $this->stripTags($dataC['value']);
                    }
                }
            }
        }
        return $_arrResult;
    }

    /**
     * Funcion que se encarga de obtener los registros de una consulta, el
     * parametro $data solo se recibe cuando el elemento es para llenar una 
     * lista( combobox)
     * @param type $select instancia del objeto select
     * @param type $id el campo identificador de cada registro
     * @param type $data el campo que se desea mostrar
     * @param type $parent en caso de que el resultado sea un subconjunto
     * @return type
     */
    protected function obtenerRegistros($select, $id = '', $data = '', $parent = '') {
        $_arrDatos = array();
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrData = $resultSet->toArray();
        if (count($_arrData)) {
            $_arrDatos = $this->formatoArray($_arrData, $id, $data, $parent);
        }
        return $_arrDatos;
    }

    /**
     * Le quito las etiquetas html a las cadenas que me lleguen
     * @param type $_strtexto
     * @return type $_strTextoLimpio
     */
    protected function stripTags($_strtexto) {
        $_strTextoLimpio = strip_tags($_strtexto);
        return $_strTextoLimpio;
    }

    /**
     * formateo el array que obtuve desde la consulta para que se pueda leer 
     * mas facilmente
     * @param Array $_arrTipo datos que quiero formatear         
     * @param type $id el campo identificador de cada registro
     * @param type $dato el campo que se desea mostrar
     * @param type $parent en caso de que el resultado sea un subconjunto
     * @return Array $_arrRespuesta Array formateado
     */
    protected function formatoArray($_arrTipo, $id, $dato, $parent) {
        $_arrRespuesta = array();
        $_arrIndices = array_keys($_arrTipo[0]);
        foreach ($_arrTipo as $data) {
            //c03clave_tipo
            if ($parent) {
                $_arrRespuesta[$data[$id]] = array( 
                    'id_parent' => $data[$parent],
                    'value' => $data[$dato],
                    'estatusExamen' => $data['t12estatus']
                        );
            } else {
                if ($id && $dato) {
                    $_arrRespuesta[$data[$id]] = $data[$dato];
                } else {
                    $_arrRespuesta[$data[$id]] = array(); //$data[$dato];
                    foreach ($_arrIndices as $campo) {
                        //if($campo !== $id){
                        $_arrRespuesta[$data[$id]][$campo] = $data[$campo];
                        //}
                    }
                }
            }
        }
        return $_arrRespuesta;
    }

    /**
     * *********************************************************************
     * Funciones para la migración de examenes
     */
    public function obtenerExamenesAccithia() {
        $select = $this->sql->select();
        $_arrData = array(
            'idExamen',
            'nomExamen',
            'descripcion',
            'tipoExamen',
            'tiempoExamen',
            'tipoTiempo',
            'tiempoIntentos',
            'tiempoIntentos2',
            'shuffePregunta',
            'shuffeRespuesta',
            'numIntentos',
            'pistaExamen',
            'metodoCalificacion',
            'penalidad',
            'formaCalificacion',
            'mostrar',
            'email',
            'minimo',
            'backrespuesta',
            'cEstatus'
        );
        $select->from(
                        array('ex' => 'examen'))
                ->columns($_arrData);


        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrExamenes = $resultSet->toArray();
        //foreach($_arrExamenes as   $indice=>$data){
        //    $_arrExamenes[$indice] = $this->stripTags($data);
        //}
        return $_arrExamenes;
    }

    public function obtenerPreguntasAccithia($strRuta) {
        // Multiples que se dividiran en opcion multiple y respuesta multiple            
        $select = $this->sql->select();
        $_arrData = array(
            'id_pregunta' => 'idMultiple',
            'nombre',
            'pregunta',
            'clave',
            'imagen',
            'instruccion',
            'numeracion'
        );
        for ($i = 1; $i <= 10; $i++) {
            $_arrData['isC' . $i] = new Expression("if(correcto" . $i . "='true', 1, 0)");
            $_arrData['isR' . $i] = new Expression("if(respuesta" . $i . "='void', 0, 1)");
            $_arrData['correcto' . $i] = 'correcto' . $i;
            $_arrData['respuesta' . $i] = 'respuesta' . $i;
        }
        $select->from(
                        array('pr' => 'multiple2'))
                ->columns($_arrData);


        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrPreguntas = $resultSet->toArray();

        foreach ($_arrPreguntas as $indice => $data) {
            $_sumaCorrectas = 0;
            $_sumaRespuestas = 0;
            $cont = 0;
            for ($i = 1; $i <= 10; $i++) {
                $_sumaCorrectas += (int) $data['isC' . $i];
                $_sumaRespuestas += (int) $data['isR' . $i];
                if ($data['respuesta' . $i] !== 'void') {
                    $_arrPreguntas[$indice]['resp'][$cont] = $data['respuesta' . $i];
                    $_arrPreguntas[$indice]['corr'][$cont] = $data['isC' . $i];
                    $cont ++;
                }
                //Elimino las pocisiones que no traen información
                unset($_arrPreguntas[$indice]['isR' . $i]);
                unset($_arrPreguntas[$indice]['isC' . $i]);
                unset($_arrPreguntas[$indice]['respuesta' . $i]);
                unset($_arrPreguntas[$indice]['correcto' . $i]);
            }
            $_arrPreguntas[$indice]['nombre'] = $_sumaCorrectas > 1 ?
                    TipoPregunta::RESMUL : TipoPregunta::OPMULT;
            $_arrPreguntas[$indice]['num_respuestas'] = $_sumaRespuestas;
            $_arrPreguntas[$indice]['pregunta'] = $this->adjuntaImagen($data['pregunta'], $data['imagen'], $strRuta);

            // Inserto en la base las preguntas
        }
        return $_arrPreguntas;
    }

    public function obtenerInstruccionesAccithia() {
        $select = $this->sql->select();
        $_arrData = array(
            'idInstruccion',
            'clave',
            'instruccion',
            'idExamen'
        );
        $select->from(
                        array('in' => 'instrucciones'))
                ->columns($_arrData);


        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrExamenes = $resultSet->toArray();
        //foreach($_arrExamenes as $indice=>$data){
        //    $_arrExamenes[$indice] = $this->stripTags($data);
        //}
        return $_arrExamenes;
    }

    public function convertirExamenes($examAccithia) {
        //return $examAccithia;
        $_arrResult = array();
        foreach ($examAccithia as $data) {
            switch ($data['tipoExamen']) {
                case '1': // Reconocimientos: Se manejaran como cuantitativos
                    $_strTipo = TipoExamen::CUANTITATIVO;
                    break;
                case '2': // Certificaciones: Se manejaran como cuantitativos
                    $_strTipo = TipoExamen::CUANTITATIVO;
                    break;
                case '3': // Diagnosticos: Se manejaran como cualitativos
                    $_strTipo = TipoExamen::CUALITATIVO;
                    break;
                default: // El resto se manajara como encuestas
                    $_strTipo = '';
            }
            $_arrResult[] = array(
                'descripcion' => $data['descripcion'],
                'estatus' => $data['cEstatus'],
                'id_escala' => '3', # Hardcodeo la escalaya que no se maneja
                'id_nivel' => '1', # Hardcodeo la escalaya que no se maneja
                'id_tipo_examen' => $_strTipo,
                'instrucciones' => '', # No hay instrucciones
                'nombre' => $data['nomExamen'],
                'nombre_corto_clave' => $data['idExamen'], # Inserto el id de examen como pivote
                'num_intentos' => $data['numIntentos'],
                'aleatorio_preguntas' => $data['shuffePregunta'] === 'si' ? 1 : 0,
                'aleatorio_respuestas' => $data['shuffeRespuesta'] === 'si' ? 1 : 0,
                'minimo_aprobatorio' => $data['minimo'],
                'demo' => 0, # No hay demos en Accithia
                'tiempo' => $data['tiempoExamen'] !== 'void' ? $data['tiempoExamen'] : 0
            );
        }
        return $_arrResult;
    }

    public function convertirPreguntas($pregAccithia) {
        //return $examAccithia;
        $_arrResult = array();
        $i = 0;
        foreach ($pregAccithia as $data) {

            $_arrResult[$i] = array(
                'pregunta' => array(
                    'id_pregunta_parent' => 0,
                    'id_tipo_pregunta' => $data['nombre'],
                    'pregunta' => $data['pregunta'],
                    'nombre_corto_clave' => $data['clave'], # Inserto la clave de la pregunta como pivote
                    'instruccion' => $data['instruccion'],
                    'tiempo_limite' => 0, # No hay tiempo limite
                    'estatus' => 'A', # Activo todas las preguntas y en cuanto las ligue a un examen, deshabilito las que no queden ligadas a nada
                )
            );
            $j = 0;
            foreach ($data['resp'] as $dataResp) {
                $_arrResult[$i]['respuestas'][$j]['respuesta'] = $dataResp;
                $_arrResult[$i]['respuestas'][$j]['correcta'] = $data['corr'][$j];
                $j++;
            }
            for ($cont = 1; $cont <= 20; $cont++) {
                $_arrResult[$i]['pregunta']['correcta_' . $cont] = 'correcta';
                $_arrResult[$i]['pregunta']['respuesta_' . $cont] = isset($data['resp'][$cont - 1]) ? $data['resp'][$cont - 1] : null;
            }

            $i++;
        }
        return $_arrResult;
    }

    public function obtenerLigaExamenPregunta() {

        $select = $this->sql->select();
        $_arrData = array(
            'idRelexamen',
            'idExamen',
            'nombre',
            'clave'
        );
        $select->from(
                        array('re' => 'relacionexamen'))
                ->columns($_arrData)
                ->where(array('cEstatus' => 'A'));

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrLigas = $resultSet->toArray();
        //foreach($_arrExamenes as $indice=>$data){
        //    $_arrExamenes[$indice] = $this->stripTags($data);
        //}
        return $_arrLigas;
    }

    public function convierteAccithiaEvaluaciones($_arrLigas, $pregAccithia, $examAccithia, $_arrPreguntas, $_arrExamenes) {
        $_arrLigasNueva = array();
        foreach ($examAccithia as $exam) {
            $_arrExamsNueva[$exam['idExamen']] = $this->obtenerIdExamen($exam['idExamen'], $_arrExamenes);
        }
        foreach ($pregAccithia as $preg) {
            $_arrPregsNueva[$preg['clave']] = $this->obtenerIdPregunta($preg['clave'], $_arrPreguntas);
        }
        /*
          foreach($_arrExamsNueva as $idE=>$exN){
          $_arrLigasNueva[$exN] = array();
          foreach ($_arrPregsNueva as $idP=>$prN){
          $_arrLigasNueva[$exN][$prN] = $idP;
          }
          }
         */
        foreach ($_arrLigas as $liga) {
            if (isset($_arrPregsNueva[$liga['clave']])) {
                $_arrLigasNueva[] = array(
                    'idExamen' => $_arrExamsNueva[$liga['idExamen']],
                    'idPregunta' => $_arrPregsNueva[$liga['clave']],
                );
            } else {
                //$this->_objLog->debug('Clave: '.$liga['clave'].'  Las preguntas de tipo ' . $liga['nombre'] . ' no se han migrado.');
            }
        }
        foreach ($_arrLigasNueva as $row) {
            $_arrRespuesta[] = array(
                $row['idExamen'] => array(
                    $row['idPregunta'] => $row['idPregunta']));
        }
        return $_arrRespuesta;
    }

    protected function obtenerIdExamen($idExamen, $_arrExamenes) {
        foreach ($_arrExamenes as $index => $data) {
            if ($data['t12nombre_corto_clave'] === $idExamen) {
                return $index;
            }
        }
    }

    protected function obtenerIdPregunta($clavePregunta, $_arrPreguntas) {
        foreach ($_arrPreguntas as $index => $data) {
            if ($data['t11nombre_corto_clave'] === $clavePregunta) {
                return $index;
            }
        }
    }

    protected function adjuntaImagen($_pregunta, $_imagenes, $strRuta) {
        //htmlentities
        if ($_imagenes !== 'void') {
            $_arrImagenes = explode('&', $_imagenes);
            if (count($_arrImagenes) > 1) {
                $_pregunta = $this->creaPreguntaImagenes($_arrImagenes, $_pregunta, $strRuta);
            } else {
                $_pregunta = $this->creaPreguntaImagen($_arrImagenes[0], $_pregunta, $strRuta);
            }
        } else {
            $_pregunta = htmlentities($_pregunta);
        }
        return $_pregunta;
    }

    protected function creaPreguntaImagenes($_arrImagenes, $_pregunta, $strRuta) {
        $_imagenes = '';
        foreach ($_arrImagenes as $image) {
            if ($image) {
                $image = str_replace('@', '', $image);
                $_imagenes .= '<img src="' . $strRuta . '/img/examenes/' . $image . '.jpg" >';
            }
        }
        $_imagenes = '<div class="imgsAnt">' . $_imagenes . '</div><br>';
        $_pregunta = $_imagenes . htmlentities($_pregunta);
        return $_pregunta;
    }

    protected function creaPreguntaImagen($_strImagen, $_pregunta, $strRuta) {
        $image = str_replace('@', '', $_strImagen);
        $_pregunta = '<div class="imgsAnt"><img src="' . $strRuta . '/img/examenes/' . $image . '" ></div>' . '<br>' . htmlentities($_pregunta);
        return $_pregunta;
    }

    /**
     * Fin de funciones para migración
     * *********************************************************************
     */
    public function obtenerAvanceExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento) {
        $select = $this->sql->select();
        // $_dblPorcentaje = 0;
        $_arrData = array(
            't12id_examen',
            'pct_avance' => new Expression("((count(t10respuesta_usuario) / count(1)) * 100) ")
        );

        $select->from(array('t10' => 't10respuesta_usuario_examen'))
                ->columns($_arrData)
                ->where(array(
                    't03id_licencia_usuario' => $_intIdUsuarioLicencia,
                    't12id_examen' => $_intIdExamen,
                    't10num_intento' => $_intNumIntento
        ));
        //$_arrRegistros = $this->obtenerRegistros($select, $_arrData[0], $_arrData[0]);
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRegistros = $resultSet->toArray();

        $_dblPorcentaje = round($_arrRegistros[0]['pct_avance'], 2);

        return $_dblPorcentaje;
    }

    /**
     * Funcion que se encarga de obtener el numero de pregunta actual
     * @param type $_arrPreguntas
     * @param type $_strActual
     * @return int
     */
    public function obtenerNumeroPregunta($_arrPreguntas, $_strActual) {
        //$_arrKeys = array_keys($_arrRespuestas);
        $j = 1;
        foreach ($_arrPreguntas as $index => $dato) {
            if ($index === (int) $_strActual) {
                return $j;
            }
            $j++;
        }
    }

    /**
     * Funcion que se encarga de evaluar el examen, cambiar el examen a estatus:
     * Finalizado, Reprobado, Aprobado, según sea el caso
     * @param type $_intIdExamen
     * @param type $_intIdUsuarioLicencia
     * @param type $_intNumIntento
     * @return boolean
     */
    public function finalizaExamen($_intIdExamen, $_objExamen, $_intIdUsuarioLicencia, $_intNumIntento) {
        $_intIdEscala = $_objExamen->id_escala;
        $_intEscala = $this->obtenerEscalas($_intIdEscala);
        $_intMinimo = $_objExamen->minimo_aprobatorio;
        $_intIdTipoExamen = $_objExamen->id_tipo_examen;
        $_intCalificacion = $this->obtenerCalificacionExamen($_intIdExamen, $_intIdUsuarioLicencia, $_intNumIntento, $_intEscala);
        if ((int) $_intIdTipoExamen !== TipoExamen::CUANTITATIVO) {
            $_intEstatus = EstatusExamen::FINALIZADO;
        } else {
            $_intEstatus = $_intCalificacion >= $_intMinimo ? EstatusExamen::APROBADO : EstatusExamen::REPROBADO;
        }
        $_instanciaUpdate = $this->sql->update()
                ->table('t04examen_usuario')
                ->set(array(
                    't04calificacion' => $_intCalificacion,
                    't04estatus' => $_intEstatus,
                    't04fecha_fin' => date('Y/m/d g:i:s'),
                    't04tiempo' => '01:00:00', //TODO: Agregar hora al examen para obtenerlo en este punto
                ))
                ->where(array(
            't12id_examen' => $_intIdExamen,
            't03id_licencia_usuario' => $_intIdUsuarioLicencia,
            't04num_intento' => $_intNumIntento));

        $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdate);
        return $result->execute()->getAffectedRows() > 0;
    }

    /**
     * Funcion que se encarga de obtener la calificación de un examen, en esta 
     * se tiene que obtener el tipo de pregunta para condicionar a las 
     * preguntas de tipo abiertas, no valide nada y marque como respuesta 
     * correcta cualquier cosa que el usuario escriba
     * @param type $_intIdExamen
     * @param type $_intIdUsuarioLicencia
     * @param type $_intNumIntento
     * @param type $_intEscala
     * @return int $_intCalif
     */
    protected function obtenerCalificacionExamen($_intIdExamen, $_intIdUsuarioLicencia, $_intNumIntento, $_intEscala) {
        $select = $this->sql->select();
        $_arrData = array(
            't12id_examen',
            'calificacion' => new Expression("
                        round((sum(if(t10respuesta_correcta like 				
                        if(c03id_tipo_pregunta like 6, '*', t10respuesta_usuario),
                        1, 0)) / count(t10respuesta_correcta)) * $_intEscala)")
        );

        $select->from(array('t10' => 't10respuesta_usuario_examen'))
                ->columns($_arrData)
                ->join(array('t11' => 't11pregunta'), 't10.t11id_pregunta = t11.t11id_pregunta', array())
                ->where(array(
                    't03id_licencia_usuario' => $_intIdUsuarioLicencia,
                    't12id_examen' => $_intIdExamen,
                    't10num_intento' => $_intNumIntento
                ))
                ->group('t12id_examen');
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRegistro = $resultSet->toArray();

        $_intCalif = $_arrRegistro[0]['calificacion'];

        return $_intCalif;
    }

    /**
     * Funcion que se encarga de cambiar el estatus del examen a iniciado 
     * @param int $_intIdUsuarioLicencia
     * @param int $_intIdExamen
     * @param int $_intNumIntento
     * @return boolean true si afecta cambios en la BD
     */
    public function iniciaExamen($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento) {
        $_instanciaUpdate = $this->sql->update()
                ->table('t04examen_usuario')
                ->set(array(
                    't04estatus' => 'INICIADO',
                    't04fecha_inicio' => date('Y/m/d g:i:s'),
                ))
                ->where(array(
            't12id_examen' => $_intIdExamen,
            't03id_licencia_usuario' => $_intIdUsuarioLicencia,
            't04num_intento' => $_intNumIntento));

        $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdate);
        return $result->execute()->getAffectedRows() > 0;
    }

    public function existeRegistroExamenUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento) {

        $select = $this->sql->select();
        $_arrData = array(
            't03id_licencia_usuario',
            't12id_examen',
            't10num_intento'
        );

        $select->from(array('t10' => 't10respuesta_usuario_examen'))
                ->columns($_arrData)
                ->where(array(
                    't03id_licencia_usuario' => $_intIdUsuarioLicencia,
                    't12id_examen' => $_intIdExamen,
                    't10num_intento' => $_intNumIntento
        ));
        $_arrRegistros = $this->obtenerRegistros($select, $_arrData[0], $_arrData[0]);
        if (count($_arrRegistros)) {
            $_bolRespuesta = true;
        } else {
            $_bolRespuesta = false;
        }
        return $_bolRespuesta;
    }

    /**
     * Funcion que se encarga de obtener el registro de la base de datos de la 
     * licencia a la que corresponde el examen seleccionado, el tercer parametro 
     * sirve para preguntar si el dato que recibo es una licencia en caso de ser
     * falso o no recibirla se asume que es un id de usuario 
     * @param type $_mixIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_bolEsLicencia
     * @return type
     */
    public function obtenerUsuarioLicencia($_mixIdUsuarioLicencia, $_intIdExamen, $_bolEsLicencia = false) {
        $_intDato = 0;
        $select = $this->sql->select();
        $_arrData = array(
            't03id_licencia_usuario'
        );

        $select->from(array('t03' => 't03licencia_usuario'))
                ->columns($_arrData)
                ->join(
                        array('t04' => 't04examen_usuario'), 't04.t03id_licencia_usuario=t03.t03id_licencia_usuario', array());
        if ($_bolEsLicencia) {
            $select->where(array(
                't03licencia' => $_mixIdUsuarioLicencia,
                't12id_examen' => $_intIdExamen));
        } else {
            $select->where(array(
                't02id_usuario_perfil' => $_mixIdUsuarioLicencia,
                't12id_examen' => $_intIdExamen));
        }


        $_arrPerfil = $this->obtenerRegistros($select, $_arrData[0], $_arrData[0]);
        foreach ($_arrPerfil as $_index => $data) {
            $_intDato = $_index;
        }
        return $_intDato;
    }

    /**
     * Funcion que se encarga de obtener el numero de intentos que ha realizado
     * un usuario en contestar un examen, cuando un examen es interrumpido o no
     * presentado regresara 0 
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_bolEsCargaMasiva
     * @return int
     */
    public function obtenerNumeroIntentos($_intIdUsuarioLicencia, $_intIdExamen, $_bolEsCargaMasiva = false) {
        //SELECT max(t04num_intento), t04estatus FROM t04examen_usuario where t03id_licencia_usuario = 83 ;
        $select = $this->sql->select();
        $_arrData = array(
            'num_intento' => new Expression("max(t04num_intento)"),
            't04estatus'
        );

        $select->from(array('t04' => 't04examen_usuario'))
                ->columns($_arrData)
                ->where(array(
                    't03id_licencia_usuario' => $_intIdUsuarioLicencia,
                    't12id_examen' => $_intIdExamen));

        $strSelect = $this->sql->prepareStatementForSqlObject($select);
        $result = $strSelect->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrExamen = $resultSet->toArray();
        if (EstatusExamen::FINALIZADO === $_arrExamen[0]['t04estatus'] ||
                EstatusExamen::INICIADO === $_arrExamen[0]['t04estatus'] ||
                EstatusExamen::APROBADO === $_arrExamen[0]['t04estatus'] ||
                EstatusExamen::REPROBADO === $_arrExamen[0]['t04estatus']) {
            if ($_bolEsCargaMasiva && EstatusExamen::INICIADO === $_arrExamen[0]['t04estatus']) { // Se soluciona el bug de los examenes interrumpidos al cargar el archivo de resultados                
                $_numIntento = (int) $_arrExamen[0]['num_intento'];
            } else {
                $_numIntento = (int) $_arrExamen[0]['num_intento'] - 1;
            }
        } else {
            $_numIntento = (int) $_arrExamen[0]['num_intento'];
        }
        return $_numIntento;
    }

    protected function obtenerTextoEstatus($_strEstatus) {
        switch ($_strEstatus) {            
            case EstatusExamenAgenda::LISTORESOLVER:
            case EstatusExamenAgenda::NODISPONIBLE:
            case EstatusExamenAgenda::NOAGENDADO:
                $_strRespuesta = 'Agendada';
                $_strClaveRespuesta = 'LISTAS POR RESOLVER';   
                break; 
            case EstatusExamenAgenda::APROBADO: 
            case EstatusExamenAgenda::REPROBADO:
            case EstatusExamenAgenda::FINALIZADO:
                $_strRespuesta = 'Concluida';
                //$_strClaveRespuesta = EstatusExamenAgenda::FINALIZADO;
                $_strClaveRespuesta = 'FINALIZADAS';
                break;
            case EstatusExamenAgenda::INTERRUMPIDO: 
            case EstatusExamenAgenda::INTERRUMPIDORESOLVER: 
            case EstatusExamenAgenda::INTERRUMPIDOVENCIDO:
                $_strRespuesta = 'Iniciada';
                //$_strClaveRespuesta = EstatusExamenAgenda::INTERRUMPIDO;
                $_strClaveRespuesta = 'INTERRUMPIDAS';
                break;
            case EstatusExamenAgenda::NOAGENDADO1:
                $_strEstatus = 'no agendado1';
                $_strClaveRespuesta = EstatusExamenAgenda::NOAGENDADO1;
                break;
            default:
                $_strRespuesta = 'Lista para resolver';
                $_strClaveRespuesta = EstatusExamenAgenda::LISTORESOLVER;
        }
        $_arrRespuesta = array('clave' => $_strClaveRespuesta, 'texto'=> $_strClaveRespuesta);
        return $_arrRespuesta;
    }

    /**
     * Regresa un array con los detalles de la relacion entre las secciones y el id de examen dado.
     * 
     * @author lcencieros.
     * @since 29/05/2015
     * @access public.
     * @param type $idExamen
     * @return array
     */
    public function obtenerExamenSeccion($idExamen) {
        $select = $this->sql->select();
        $_arrData = array(
            't20id_seccion_examen',
            't12id_examen',
            't15id_seccion'
        );

        $select->from(array('t20' => 't20seccion_examen'))
                ->columns($_arrData)
                ->join(array('t15' => 't15seccion'), 't15.t15id_seccion = t20.t15id_seccion', array('t15seccion', 't15valor_porcentaje', 't15resp_total_parcial'))
                ->where(array('t12id_examen' => $idExamen))
                ->where(array('t15estatus' => EstatusSeccion::ACTIVA));
        $_arrExamenSeccion = $this->obtenerRegistros($select, $_arrData[0]);
        if (!count($_arrExamenSeccion)) {
            $_arrExamenSeccion = false;
        }
        return $_arrExamenSeccion;
    }

    /**
     * La cantidad existente de copias de un examen
     * 
     * @author lcencieros.
     * @since 29/05/2015
     * @access public. 
     * @param type $nombreExamen
     * @return type
     */
    public function consultaCopiaExistente($nombreExamen) {
        $select = $this->sql->select();

        $_arrData = array(
            'total' => new Expression("count(t12nombre)")
        );

        $select->from(array('t12' => 't12examen'))
                ->columns($_arrData)
        ->where->like('t12nombre', "%$nombreExamen%");
        $arrTotal = $this->obtenerRegistros($select);

        return $arrTotal['']['total'];
    }

    /**
     * Regresa un array con los detalles de las secciones correspondientes
     * al id de examen dado.
     * 
     * @author lcencieros.
     * @since 29/05/2015
     * @access public.
     * @param type $idExamen
     * @return array
     */
    public function obtenerSeccion($idExamen) {
        $select = $this->sql->select();
        $_arrData = array(
            't15id_seccion',
            't15seccion',
            't15valor_porcentaje',
            't15resp_total_parcial',
            't15estatus'
        );

        $select->from(array('t15' => 't15seccion'))
                ->columns($_arrData)
                ->join(array('t20' => 't20seccion_examen'), 't15.t15id_seccion = t20.t15id_seccion')
                ->where(array('t12id_examen' => $idExamen));
        $_arrSeccion = $this->obtenerRegistros($select, $_arrData[0]);
        if (count($_arrSeccion)) {
            $arrTmp = array_keys($_arrSeccion);
        } else {
            $arrTmp = false;
        }
        return $_arrSeccion;
    }

    /**
     * Regresa un array con los detalles de la relacion entre las preguntas y la seccion
     * correspondiente al id de examen dado.
     * 
     * @author lcencieros.
     * @since 29/05/2015
     * @access public.
     * @param type $idSeccion
     * @return array
     */
    public function obtenerSeccionPregunta($idSeccion) {

        $selectSub = $this->sql->select()
                ->from(array('t20' => 't20seccion_examen'))
                ->columns(array('t20id_seccion_examen'))
                ->where('t20id_seccion_examen = ' . $idSeccion);

        $select = $this->sql->select();
        $_arrData = array(
            't20id_seccion_examen',
            't11id_pregunta',
            't16orden_pregunta'
        );
        $select->from(array('t16' => 't16seccion_pregunta'))
                ->columns($_arrData)
                ->join(array('t20' => $selectSub), 't20.t20id_seccion_examen  =  t16.t20id_seccion_examen', array('t20id_seccion_examen'))
                ->order('t16orden_pregunta');

        $strSelect = $this->sql->prepareStatementForSqlObject($select);
        $result = $strSelect->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrExamenSeccion = $resultSet->toArray();

        if (!count($_arrExamenSeccion)) {
            $_arrExamenSeccion = false;
        }
        return $_arrExamenSeccion;
    }

    /**
     * Recibe un id de $idSeccionexamen y lo realciona con el $_arrPreguntasSeccion.
     * Se encarga de insertar los registros en la tabla relacional de seccion pregunta
     * 
     * @author lcencieros.
     * @since 01/06/2015
     * @access public.
     * @param int $idSeccionexamen
     * @param array $_arrPreguntasSeccion
     * @return boolean
     */
    function relacionarPreguntasSeccion($idSeccionexamen, $_arrPreguntasSeccion) {
        foreach ($_arrPreguntasSeccion as $pregunta) {
            $insertSeccion = $this->sql->insert()
                    ->into('t16seccion_pregunta')
                    ->columns(array(
                        "t20id_seccion_examen",
                        "t11id_pregunta",
                        "t16orden_pregunta"
                    ))
                    ->values(array(
                "t20id_seccion_examen" => $idSeccionexamen,
                "t11id_pregunta" => $pregunta['t11id_pregunta'],
                "t16orden_pregunta" => $pregunta['t16orden_pregunta']
            ));
            $strQuery = $this->sql->prepareStatementForSqlObject($insertSeccion);
            $resultInsertSeccion = $strQuery->execute();
            $_bolejecuta = $resultInsertSeccion->getAffectedRows();
        }
        return $_bolejecuta;
    }

    /**
     * Action que copia un examen.
     * Se maneja con una transacción para evitar incongruencia de registros
     * 
     * @author lcencieros.
     * @since 29/05/2015
     * @access public.
     * @param array $examen
     * @param array $seccion
     * @return boolean
     */
    public function clonarExamen($examen, $seccion, $opCopia,$_arrRelCategoria,$_arrRelEtiqueta) {

        $_arrResult[0] = false;
        //inicia una transaccion en la BDD
        $_strTransaction = "START TRANSACTION";
        $statementTransaction = $this->adapter->query($_strTransaction);
        $resultTransaction = $statementTransaction->execute();
        $resultSetTransaction = new ResultSet();
        $resultSetTransaction->initialize($resultTransaction);

        $nombreConcat = $opCopia == TipoCopia::COPIA_PARCIAL ? 'Copia_parcial_de_' : 'Copia_completa_de_';
        $contNombreCopia = $this->consultaCopiaExistente($nombreConcat . $examen['t12nombre']);
        $nombreCopia = $contNombreCopia >= 1 ? $nombreConcat . $examen['t12nombre'] . "($contNombreCopia)" : $nombreConcat . $examen['t12nombre'];

        $insertEx = $this->sql->insert()
                ->into('t12examen')
                ->columns(array(
                    "c02id_tipo_examen",
                    "c04id_escala",
                    "c05id_nivel",
                    "t12nombre",
                    "t12nombre_corto_clave",
                    "t12instrucciones",
                    "t12descripcion",
                    "t12fecha_registro",
                    "t12fecha_actualiza",
                    "t12num_intentos",
                    "t12aleatorio_preguntas",
                    "t12aleatorio_respuestas",
                    "t12demo",
                    "t12minimo_aprobatorio",
                    "t12tiempo",
                    "t12estatus"
                ))
                ->values(array(
            'c02id_tipo_examen' => $examen['c02id_tipo_examen'],
            'c04id_escala' => $examen['c04id_escala'],
            'c05id_nivel' => $examen['c05id_nivel'],
            't12nombre' => $nombreCopia,
            't12nombre_corto_clave' => $examen['t12nombre_corto_clave'],
            't12instrucciones' => $examen['t12instrucciones'],
            't12descripcion' => $examen['t12descripcion'],
            't12fecha_registro' => $examen['t12fecha_registro'],
            't12fecha_actualiza' => $examen['t12fecha_actualiza'],
            't12num_intentos' => $examen['t12num_intentos'],
            't12aleatorio_preguntas' => $examen['t12aleatorio_preguntas'],
            't12aleatorio_respuestas' => $examen['t12aleatorio_respuestas'],
            't12demo' => $examen['t12demo'],
            't12minimo_aprobatorio' => $examen['t12minimo_aprobatorio'],
            't12tiempo' => $examen['t12tiempo'],
            //'t12estatus' => $examen['t12estatus']
            't12estatus' => 'NUEVO'
        ));
        $strQuery1 = $this->sql->prepareStatementForSqlObject($insertEx);
        $resultInsertEx = $strQuery1->execute();
        $_bolejecuta1 = $resultInsertEx->getAffectedRows();
        if (!$_bolejecuta1) {
            $_strTransaction = "ROLLBACK";
            $statementTransaction = $this->adapter->query($_strTransaction);
            $resultTransaction = $statementTransaction->execute();
            return array(false);
        }
        $idCopiaEx = $resultInsertEx->getGeneratedValue();
        //agrega la relacion de categoria del nuevo examen
        foreach ($_arrRelCategoria as $idCat) {
                $insertCategoria = $this->sql->insert()->into('t26examen_categoria')->columns(array(
                    'c08id_categoria', 't12id_examen','t26estatus'
                ));

                $insertCategoria->values(array(
                    'c08id_categoria' => $idCat['c08id_categoria'],
                    't12id_examen' => $idCopiaEx,
                    't26estatus' => 'ACTIVO'
                ));

                $result = $this->sql->prepareStatementForSqlObject($insertCategoria);
                $_generatedId = $result->execute()->getGeneratedValue();
                if (!isset($_generatedId)) {
                    $_boolReturn = false;
                    $this->adapter->getDriver()->getConnection()->rollback();
                    break;
                }
            }
            //agrega la relacion de etiqueta al nuevo examen
            foreach ($_arrRelEtiqueta as $idEtiqueta) {

                $insertEtiqueta = $this->sql->insert()->into('t27examen_etiqueta')->columns(array(
                    'c09id_etiqueta', 't12id_examen','t27estatus'
                ));

                $insertEtiqueta->values(array(
                    'c09id_etiqueta' => $idEtiqueta['c09id_etiqueta'],
                    't12id_examen' => $idCopiaEx,
                    't27estatus' => 'ACTIVO'
                ));

                $result = $this->sql->prepareStatementForSqlObject($insertEtiqueta);
                $_generatedId = $result->execute()->getGeneratedValue();
                if (!isset($_generatedId)) {
                    $_boolReturn = false;
                    $this->adapter->getDriver()->getConnection()->rollback();
                    break;
                }
            }

        //2.- Genera copia de sección y preguntas las si ser requeridas
        foreach ($seccion as $secc) {
            $insertSeccion = $this->sql->insert()
                    ->into('t15seccion')
                    ->columns(array(
                        "t15seccion",
                        "t15valor_porcentaje",
                        "t15resp_total_parcial",
                        "t15estatus"
                    ))
                    ->values(array(
                "t15seccion" => $secc['t15seccion'],
                "t15valor_porcentaje" => $secc['t15valor_porcentaje'],
                "t15resp_total_parcial" => $secc['t15resp_total_parcial'],
                "t15estatus" => $secc['t15estatus']));
            $strQuery2 = $this->sql->prepareStatementForSqlObject($insertSeccion);
            $resultInsertSeccion = $strQuery2->execute();
            $_bolejecuta2 = $resultInsertSeccion->getAffectedRows();
            if (!$_bolejecuta2) {
                $_strTransaction = "ROLLBACK";
                $statementTransaction = $this->adapter->query($_strTransaction);
                $resultTransaction = $statementTransaction->execute();
                return array(false);
            }
            $idCopiaSec = $resultInsertSeccion->getGeneratedValue();

            //3.- Completa relación entre la sección y el examen
            $insertRelacion = $this->sql->insert()
                    ->into('t20seccion_examen')
                    ->columns(array(
                        "t12id_examen",
                        "t15id_seccion"
                    ))
                    ->values(array(
                "t12id_examen" => $idCopiaEx,
                "t15id_seccion" => $idCopiaSec
            ));
            $strQuery3 = $this->sql->prepareStatementForSqlObject($insertRelacion);
            $resultInsertRelacion = $strQuery3->execute();
            $_bolejecuta3 = $resultInsertRelacion->getAffectedRows();

            if (!$_bolejecuta3) {
                $_strTransaction = "ROLLBACK";
                $statementTransaction = $this->adapter->query($_strTransaction);
                $resultTransaction = $statementTransaction->execute();
                return array(false);
            }
            $idSeccionexamen = $resultInsertRelacion->getGeneratedValue();

            //4.- Si la copia se deba hacer completa
            if (intval($opCopia) == TipoCopia::COPIA_COMPLETA) {
                //Obtiene las preguntas relacionadas a la seccion original
                $_arrPreguntasSeccion = $this->obtenerSeccionPregunta($secc['t20id_seccion_examen']);
                //Liga las preguntas obtenidas en $_arrPreguntasSeccion y las relaciona a $idSeccionexamen
                if ($_arrPreguntasSeccion) {
                    $_bolPregClon = $this->relacionarPreguntasSeccion($idSeccionexamen, $_arrPreguntasSeccion);

                    if (!$_bolPregClon) {
                        $_strTransaction = "ROLLBACK";
                        $statementTransaction = $this->adapter->query($_strTransaction);
                        $resultTransaction = $statementTransaction->execute();
                        return array(false);
                    }
                }
            }
        }

        //si todo se ejecutó correctamente, hace commit
        $_strTransaction = "COMMIT";
        //$_strTransaction		= "ROLLBACK";

        $statementTransaction = $this->adapter->query($_strTransaction);
        $resultTransaction = $statementTransaction->execute();

        return $_arrResult;
    }

    /**
     * Funcion que se encarga de retorna el tiempo restante del examen 
     * una vez iniciado
     * @param type $_strTiempo
     * @param type $_intNumIntento
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @return type
     */
    function obtenerTiempoRestante($_strTiempo, $_intNumIntento, $_intIdUsuarioLicencia, $_intIdExamen) {
        $_strTiempoTotal = $this->conversorSegundosHoras($_strTiempo);
        $_strTiempoRestante = $this->obtenerTiempoUtilizado($_intNumIntento, $_intIdUsuarioLicencia, $_intIdExamen, $_strTiempoTotal);
        $_arrTemporal = explode(':', $_strTiempoRestante);
        if (!isset($_arrTemporal[0]) || !isset($_arrTemporal[1]) || !isset($_arrTemporal[2])) {
            return explode(':', $_strTiempoTotal);
        } else {
            //$this->_objLog->debug(print_r($_arrTemporal, TRUE));

            $_arrTiempo = array('horas' => $_arrTemporal[0],
                'minutos' => $_arrTemporal[1],
                'segundos' => $_arrTemporal[2]);
            return $_arrTiempo;
        }
    }

    /**
     * Funcion que se encarga de sumar el tiempo total, y despues
     * hacer la diferencia entre el tiempo total y el tiempo del examen
     * @param type $_intNumIntento
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_strTiempoTotal
     * @return type
     */
    protected function obtenerTiempoUtilizado($_intNumIntento, $_intIdUsuarioLicencia, $_intIdExamen, $_strTiempoTotal) {
        $_select = $this->sql->select();
        $_select->from(array('t10' => 't10respuesta_usuario_examen'))
                ->columns(array('tiempo' => new Expression("timediff('" . $_strTiempoTotal . "',SEC_TO_TIME(sum(TIME_TO_SEC(t10tiempo))))")))
                ->where(array('t03id_licencia_usuario' => $_intIdUsuarioLicencia, 't12id_examen' => $_intIdExamen, 't10num_intento' => $_intNumIntento));


        $statement = $this->sql->prepareStatementForSqlObject($_select);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrTiempo = $resultSet->toArray();
        return $_arrTiempo[0]['tiempo'];
    }

    /**
     * Funcion que se encarga de convertir el tiempo en una cadena
     * 00:00:00
     * @param type $tiempo_en_segundos
     * @return string
     */
    function conversorSegundosHoras($tiempo_en_segundos) {
        $horas = floor($tiempo_en_segundos / 60);
        $minutos = floor(($tiempo_en_segundos - ($horas * 60)));
        $segundos = '00';
        $strTiempo = $horas . ':' . $minutos . ':' . $segundos;
        return $strTiempo;
    }

    /**
     * Funcion que se encarga de guardar la fecha de registro
     * al iniciar por primera vez el examen
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_intNumIntento
     * @return boolean
     */
    function guardarExamenInicio($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento) {
        $_instanciaUpdate = $this->sql->update()
                ->table('t04examen_usuario')
                ->set(array('t04fecha_inicio' => date('Y/m/d g:i:s')))
                ->where(array('t12id_examen' => $_intIdExamen, 't03id_licencia_usuario' => $_intIdUsuarioLicencia, 't04num_intento' => $_intNumIntento));

        $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdate);
        if ($result->execute()->getAffectedRows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Esta función solo me genera los textos para indicar que no es un
     * examen valido
     * @return type
     */
    public function obtenerTextosError($_arrExam = null) {
        return array(
            'examen' => $_arrExam ? $_arrExam : array(
                't12nombre' => 'N/A',
                't12descripcion' => 'N/A',
                'c02tipo_examen' => 'N/A',
                    ),
            'textos' => array(
                'intentos' => $_arrExam ? 'No tienes acceso a resolver esta evaluaci&oacute;n' : 'La evaluaci&oacute;n solicitada no existe',
                'tiempo' => 'N/A',
                'minimo' => 'N/A',
            ),
            'numIntentos' => 0
        );
    }

    /**
     * Funcion para guardar objetos examen en base de datos.
     * @param unknown $_examen
     * @return \Zend\Db\Adapter\Driver\StatementInterface
     */
    public function guardarExamen($_arrExamen, $_idExamen = null) {

        $this->_objLog->debug('arrExamen  --> ' . print_r($_arrExamen, true));
        $this->adapter->getDriver()->getConnection()->beginTransaction();
        $_boolReturn = true;

        //crear categorias si no existen
        $_arrIdscategorias = $this->createCategorias($_arrExamen['txtCategorias']);
        if (count($_arrIdscategorias) < 1) {
            $this->adapter->getDriver()->getConnection()->rollback();
            return false;
        }

        //crear etiquetas si no existen 
        $_arrIdsEtiquetas = $this->createEtiquetas($_arrExamen['txtEtiquetas']);
        if (count($_arrIdsEtiquetas) < 1) {
            $this->adapter->getDriver()->getConnection()->rollback();
            return false;
        }

        //Crear - Guardar Examen

        if (isset($idExamen) && $idExamen != '' && $idExamen != '-1') {
            //actualizar el examen 
            $this->_objLog->debug('Modificar --> ' . $_idExamen);
        } else {
            //crear el examen
            $this->_objLog->debug('Nuevo --> ' . $_idExamen);


            $insertExamen = $this->sql->insert()->into('t12examen')->columns(array(
                'c02id_tipo_examen', 'c04id_escala', 'c05id_nivel', 't12nombre',
                't12nombre_corto_clave', 't12instrucciones', 't12descripcion', 't12fecha_registro',
                't12fecha_actualiza', 't12num_intentos', 't12aleatorio_preguntas', 't12aleatorio_respuestas',
                't12demo', 't12minimo_aprobatorio', 't12tiempo', 't12estatus'
            ));

            $insertExamen->values(array(
                'c02id_tipo_examen' => $_arrExamen['id_tipo_examen'],
                'c04id_escala' => $_arrExamen['id_escala'],
                'c05id_nivel' => $_arrExamen['id_nivel'],
                't12nombre' => $_arrExamen['nombre'],
                't12nombre_corto_clave' => '', //TODO: generar automaticamente 
                't12instrucciones' => $_arrExamen['instrucciones'],
                't12descripcion' => $_arrExamen['descripcion'],
                't12fecha_registro' => date('Y/m/d g:i:s'),
                't12fecha_actualiza' => date('Y/m/d g:i:s'),
                't12num_intentos' => $_arrExamen['num_intentos'],
                't12aleatorio_preguntas' => $_arrExamen['aleatorio_preguntas'],
                't12aleatorio_respuestas' => $_arrExamen['aleatorio_respuestas'],
                't12demo' => $_arrExamen['demo'],
                't12minimo_aprobatorio' => $_arrExamen['minimo_aprobatorio'],
                't12tiempo' => $_arrExamen['tiempo'],
                't12estatus' => $_arrExamen['estatus']));

            $result = $this->sql->prepareStatementForSqlObject($insertExamen);
            //aqui se trae el id del examen nuevo
            $_idExamen =  $result->execute()->getGeneratedValue();
            
            if(!isset($_idExamen)){
                $this->adapter->getDriver()->getConnection()->rollback();
                return false;
            }

            // hacer inserts en las tablas relacionales de categorias y etiquetas
            foreach ($_arrIdscategorias as $idCat) {
                $insertCategoria = $this->sql->insert()->into('t26examen_categoria')->columns(array(
                    'c08id_categoria', 't12id_examen','t26estatus'
                ));

                $insertCategoria->values(array(
                    'c08id_categoria' => $idCat,
                    't12id_examen' => $_idExamen,
                    't26estatus' => 'ACTIVO'
                ));

                $result = $this->sql->prepareStatementForSqlObject($insertCategoria);
                $_generatedId = $result->execute()->getGeneratedValue();
                if (!isset($_generatedId)) {
                    $_boolReturn = false;
                    $this->adapter->getDriver()->getConnection()->rollback();
                    break;
                }
            }

            foreach ($_arrIdsEtiquetas as $idEtiqueta) {

                $insertEtiqueta = $this->sql->insert()->into('t27examen_etiqueta')->columns(array(
                    'c09id_etiqueta', 't12id_examen','t27estatus'
                ));

                $insertEtiqueta->values(array(
                    'c09id_etiqueta' => $idEtiqueta,
                    't12id_examen' => $_idExamen,
                    't27estatus' => 'ACTIVO'
                ));

                $result = $this->sql->prepareStatementForSqlObject($insertEtiqueta);
                $_generatedId = $result->execute()->getGeneratedValue();
                if (!isset($_generatedId)) {
                    $_boolReturn = false;
                    $this->adapter->getDriver()->getConnection()->rollback();
                    break;
                }
            }
        }

        if ($_boolReturn) {
            $this->adapter->getDriver()->getConnection()->commit();
        }


        return $_idExamen;
    }

    public function getCategorias($like = null, $_strCategoria = null) {
        $select = $this->sql->select();
        $select->from(array('c08' => 'c08categoria'));

        //si el parametro like esta presente agregar filtro. se usa para el autocomplete  de los tag editor
        if (isset($like)) {
            $where = new \Zend\Db\Sql\Where();
            $like = '%' . $like . '%';
            $where->like('c08nombre', $like);
            $select->where($where);
        }

        //si el parametro  _strCategoria esta presente agregar filtro  para obtener solo esa categoria y su id
        if (isset($_strCategoria)) {

            $_strCategoria = ucfirst(trim($_strCategoria));
            $select->where(array('c08nombre' => $_strCategoria));
        }

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrCategorias = $resultSet->toArray();
        return $_arrCategorias;
    }

    public function createCategorias($_strListCategorias) {
        //create array  categorias 
        $_arrListCategorias = array();
        if (strpos($_strListCategorias, ',') !== false) {
            $_arrListCategorias = explode(',', $_strListCategorias);
        } else {
            $_arrListCategorias[0] = $_strListCategorias;
        }

        $_arrRespuesta = array();
        //verificar si la categoria existe , de ser así, obtener su id
        foreach ($_arrListCategorias as $cat) {
            $_strCat = trim(ucfirst(strtolower($cat)));
            $_arrCat = $this->getCategorias(null, $_strCat);
            if (count($_arrCat) > 0) {
                //agregar id al array respuesta 
                array_push($_arrRespuesta, $_arrCat[0]['c08id_categoria']);
            } else {
                //si no existe crear la categoria 
                $queryCategoria = $this->sql->insert()
                        ->into('c08categoria')
                        ->columns(array("c08nombre"))
                        ->values(array("c08nombre" => $cat));
                $statement = $this->sql->prepareStatementForSqlObject($queryCategoria);
                $result = $statement->execute();
                $idCategoria = $result->getGeneratedValue();
                array_push($_arrRespuesta, $idCategoria);
            }
        }

        return $_arrRespuesta;
    }

    public function getEtiquetas($like = null, $_strEtiqueta = null) {
        $select = $this->sql->select();
        $select->from(array('c09' => 'c09etiqueta'));

        if (isset($like)) {
            $where = new \Zend\Db\Sql\Where();
            $like = '%' . $like . '%';
            $where->like('c09nombre', $like);
            $select->where($where);
        }


        //si el parametro  _strCategoria esta presente agregar filtro  para obtener solo esa categoria y su id
        if (isset($_strEtiqueta)) {

            $_strEtiqueta = ucfirst(trim($_strEtiqueta));
            $select->where(array('c09nombre' => $_strEtiqueta));
        }

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrCategorias = $resultSet->toArray();
        return $_arrCategorias;
    }

    public function createEtiquetas($_strListEtiquetas) {
        //create array  etiquetas 
        $_arrListEtiquetas = array();
        if (strpos($_strListEtiquetas, ',') !== false) {
            $_arrListEtiquetas = explode(',', $_strListEtiquetas);
        } else {
            $_arrListEtiquetas[0] = $_strListEtiquetas;
        }

        $_arrRespuesta = array();
        //verificar si la categoria existe , de ser así, obtener su id
        foreach ($_arrListEtiquetas as $etiqueta) {
            $_strEtiqueta = trim(ucfirst(strtolower($etiqueta)));
            $_arrEtiqueta = $this->getEtiquetas(null, $_strEtiqueta);
            if (count($_arrEtiqueta) > 0) {
                //agregar id al array respuesta 
                array_push($_arrRespuesta, $_arrEtiqueta[0]['c09id_etiqueta']);
            } else {
                //si no existe crear la categoria 
                $queryEtiqueta = $this->sql->insert()
                        ->into('c09etiqueta')
                        ->columns(array("c09nombre"))
                        ->values(array("c09nombre" => $_strEtiqueta));
                $statement = $this->sql->prepareStatementForSqlObject($queryEtiqueta);
                $result = $statement->execute();
                $idEtiqueta = $result->getGeneratedValue();
                array_push($_arrRespuesta, $idEtiqueta);
            }
        }

        return $_arrRespuesta;
    }

    public function getGruposActivos($_intIdUSPC) {
        $_strQueryGruposUsuario = 'SELECT 
                                                t05.t05id_grupo,
                                                t05.t05descripcion AS t05descripcion,
                                                profesor.t01correo AS profesor,
                                                t05.t05ciclo AS t05ciclo,
                                                t07.t07codigo AS t07codigo,
                                                t08.t02id_usuario_perfil AS t02id_usuario_perfil,
                                                t02 . *
                                            FROM
                                                t05grupo AS t05
                                                    INNER JOIN
                                                t07codigo AS t07 ON t05.t05id_grupo = t07.t05id_grupo
                                                    INNER JOIN
                                                t08administrador_grupo AS t08 ON t05.t05id_grupo = t08.t05id_grupo
                                                    INNER JOIN
                                                t02usuario_perfil AS t02 ON t08.t02id_usuario_perfil = t02.t02id_usuario_perfil
                                                    INNER JOIN
                                                t01usuario AS t01 ON t01.t01id_usuario = t02.t01id_usuario
                                                    INNER JOIN
                                                (select t05.t05id_grupo, t01.t01correo 
                                                from t01usuario t01
                                                join t02usuario_perfil t02 on (t01.t01id_usuario = t02.t01id_usuario )
                                                join t08administrador_grupo t08 on (t02.t02id_usuario_perfil = t08.t02id_usuario_perfil)
                                                join t05grupo t05 on (t05.t05id_grupo=t08.t05id_grupo)
                                                where c01id_perfil = 2) as profesor on(t05.t05id_grupo = profesor.t05id_grupo)
                                            WHERE
                                                t08.t08usuario_ligado = "LIGADO" AND t07.t07estatus = 1 AND t08.t02id_usuario_perfil = :_intIdUSPC;';


        $statement = $this->adapter->query($_strQueryGruposUsuario);
        $results = $statement->execute(array(':_intIdUSPC' => $_intIdUSPC,));

        $resultSet = new ResultSet;
        $resultSet->initialize($results);
        $arrGruposUsuario = $resultSet->toArray();

        return $arrGruposUsuario;
    }

    public function getdatosDesactivados($_intIdUsrPerfil) {
        $_strQuerydatosDesactivados = 'SELECT t02id_usuario_perfil FROM t02usuario_perfil where t01id_usuario=:_IdUsrPerfil';
        $statement = $this->adapter->query($_strQuerydatosDesactivados);
        $results = $statement->execute(array(':_IdUsrPerfil' => $_intIdUsrPerfil,));

        $resultSet = new ResultSet;
        $resultSet->initialize($results);
        $arrdatosDesactivados = $resultSet->toArray();

        return $arrdatosDesactivados;
    }

    public function getDatosExamendesactivado($_intUsuarioPerfil) {
        $_strQueryDatosExamendesactivado = 'select t04id_examen_usuario from t04examen_usuario  
                                where t03id_licencia_usuario in ( 
                                select t03id_licencia_usuario  
                                from t03licencia_usuario 
                                where t02id_usuario_perfil = :_UsuarioPerfil) ;';


        $statement = $this->adapter->query($_strQueryDatosExamendesactivado);
        $results = $statement->execute(array(':_UsuarioPerfil' => $_intUsuarioPerfil,));

        $resultSet = new ResultSet;
        $resultSet->initialize($results);
        $arrDatosExamendesactivado = $resultSet->toArray();

        return $arrDatosExamendesactivado;
    }

    public function getDatosAlumno($_stridUserPer) {
        $_strQueryDatosUserPer = 'SELECT t01.t01nombre,t01.t01correo FROM t02usuario_perfil t02 
                                            inner join t01usuario t01 on t01.t01id_usuario=t02.t01id_usuario
                                            where t02.t02id_usuario_perfil= :_UsuarioPerfil ;';


        $statement = $this->adapter->query($_strQueryDatosUserPer);
        $results = $statement->execute(array(':_UsuarioPerfil' => $_stridUserPer,));

            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $arrDatosExamendesactivado = $resultSet->toArray();
            
           return $arrDatosExamendesactivado;
        }
        
        /**
         * 
         * @param type $idExamen
         * @return type muestra en la vista de editar examen las categorias guardadas en ese examen
         */
        public function getCategoriasEdit($idExamen){
            $_arrData = array(
                'c08id_categoria',
                'c08nombre'
            );
            $select = $this->sql->select();     
            $select ->from( array( 'c08'=>'c08categoria') )
                    ->columns($_arrData)
                    ->join(array('t26' => 't26examen_categoria'), 'c08.c08id_categoria=t26.c08id_categoria')
                    ->where('t26.t12id_examen='.$idExamen)
                    ->where('t26.t26estatus='.$this->config['constantes']['ESTATUS_ACTIVO']);
           
            $arr_nombreCategoria = $this->obtenerRegistros($select,$_arrData[0]);
            return $arr_nombreCategoria;
        }
        /**
         * 
         * @param type $idExamen
         * @return type muestra en la vista de editar pregunta las etiquetas guardadas en esa pregunta
         */
        public function getEtiquetasEdit($idExamen){
            $_arrData = array(
                'c09id_etiqueta',
                'c09nombre'
            );
            $select = $this->sql->select();     
            $select ->from( array('c09'=>'c09etiqueta') )
                    ->columns($_arrData)
                    ->join(array('t27' => 't27examen_etiqueta'), 'c09.c09id_etiqueta=t27.c09id_etiqueta')
                    ->where('t27.t12id_examen='.$idExamen)
                    ->where('t27.t27estatus='.$this->config['constantes']['ESTATUS_ACTIVO']);
           
            $arr_nombreEtiqueta = $this->obtenerRegistros($select,$_arrData[0]);
            return $arr_nombreEtiqueta;
        }
        
        /**
         * actualiza las categorias en dado caso que la categoria no existiera en la base de datos
         * se inserta la nueva categoria regresando su id ademas de insertar la relacion entre pregunta categoria en la tabla relacional 
         * @param type $_intIdExamen
         * @param type $arr_strCategoria
         */
        public function actualizaCategoria($_intIdExamen,$arr_strCategoria){
            //$modelExamen = new \Examenes\Model\ExamenModel($this->adapter, $this->config);
            $strCategoria = '';
            //concatena el array de los tags 
            foreach ($arr_strCategoria as $id){
                $strCategoria .= $id.',';
            }
            $strCat = substr($strCategoria,0,  strlen($strCategoria)-1);
            $_arrIdCategoria = $this->createCategorias($strCat);
            $select = $this->sql->select();
            $_arrData = array('c08id_categoria','t12id_examen',);
            $select->from(array('t26'=> 't26examen_categoria'))
                    ->columns($_arrData)
                    ->where(array(
                        't12id_examen'=>$_intIdExamen,
                        't26estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']));
            $_arrRegistros = $this->obtenerRegistros($select,$_arrData[0],$_arrData[0]);
            
            foreach ($_arrRegistros as $midato){
                //si el id del registro no existe la actualiza 
                    $updateCategoria = $this->sql->update('t26examen_categoria');
                if(!in_array($midato, $_arrIdCategoria)){
                    $updateCategoria ->set(array('t26estatus'=>$this->config['constantes']['ESTATUS_INACTIVO']));
                }else{
                    $updateCategoria ->set(array('t26estatus'=>$this->config['constantes']['ESTATUS_ACTIVO']));
                }
                $updateCategoria->where(array('t12id_examen'=>$_intIdExamen,'c08id_categoria'=>$midato));
                    $result = $this->sql->prepareStatementForSqlObject($updateCategoria);
                    $result->execute()->getGeneratedValue();
            }
            
            $arrDiffCat= array_diff($_arrIdCategoria,$_arrRegistros );
            //de mi variable $arrDiffCat crear un insert con ese id de categoria que me trae solo actuliza activo;
            foreach ($arrDiffCat AS $diffCat ){
                $insertRelCategoria = $this->sql->insert()
                                ->into('t26examen_categoria')
                                ->columns(array('c08id_categoria', 't12id_examen','t26estatus'))
                                ->values(array('c08id_categoria' => $diffCat,
                                    't12id_examen' => $_intIdExamen,
                                    't26estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']   
                                ));
                        $statement = $this->sql->prepareStatementForSqlObject($insertRelCategoria);
                        $result = $statement->execute()->getAffectedRows();
            }
        }
        /**
         * actualiza las etiquetas en dado caso que la etiqueta no existiera en la base de datos
         * se inserta la nueva etiqueta regresando su id ademas de insertar la relacion entre pregunta etiqueta en la tabla relacional 
         * @param type $_intIdPregunta
         * @param type $arr_strEtiqueta
         */
        public function actualizaEtiqueta($_intIdExamen,$arr_strEtiqueta){
            //$modelExamen = new \Examenes\Model\ExamenModel($this->adapter, $this->config);
            $strEtiqueta = '';
            //concatena el array de los tags 
            foreach ($arr_strEtiqueta as $id){
                $strEtiqueta .= $id.',';
            }
            $strTag = substr($strEtiqueta,0,  strlen($strEtiqueta)-1);
            $_arrIdEtiqueta = $this->createEtiquetas($strTag);
            $select = $this->sql->select();
            $_arrData = array('c09id_etiqueta','t12id_examen',);
            $select->from(array('t27'=> 't27examen_etiqueta'))
                    ->columns($_arrData)
                    ->where(array(
                        't12id_examen'=>$_intIdExamen,
                        't27estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']));
            $_arrRegistros = $this->obtenerRegistros($select,$_arrData[0],$_arrData[0]);
            
            foreach ($_arrRegistros as $midato){
                //si el id del registro no existe la actualiza 
                    $updateEtiqueta= $this->sql->update('t27examen_etiqueta');
                if(!in_array($midato, $_arrIdEtiqueta)){
                    $updateEtiqueta->set(array('t27estatus'=>$this->config['constantes']['ESTATUS_INACTIVO']));
                }else{
                    $updateEtiqueta->set(array('t27estatus'=>$this->config['constantes']['ESTATUS_ACTIVO']));
                }
                $updateEtiqueta->where(array('t12id_examen'=>$_intIdExamen,'c09id_etiqueta'=>$midato));
                    $result = $this->sql->prepareStatementForSqlObject($updateEtiqueta);
                    $result->execute()->getGeneratedValue();
            }
            
            $arrDiffTag= array_diff($_arrIdEtiqueta,$_arrRegistros );
            //de mi variable $arrDiffCat crear un insert con ese id de categoria que me trae solo actuliza activo o inactivo;
            foreach ($arrDiffTag AS $diffTag){
                $insertRelEtiqueta= $this->sql->insert()
                                ->into('t27examen_etiqueta')
                                ->columns(array('c09id_etiqueta', 't12id_examen','t27estatus'))
                                ->values(array('c09id_etiqueta' => $diffTag,
                                    't12id_examen' => $_intIdExamen,
                                    't27estatus'=>  $this->config['constantes']['ESTATUS_ACTIVO']   
                                ));
                        $statement = $this->sql->prepareStatementForSqlObject($insertRelEtiqueta);
                        $result = $statement->execute()->getAffectedRows();
            }
        }
        
        public function publicarExamen($idExamen){
            $_updateStatusExamen = 'UPDATE t12examen
                                    SET t12estatus = "PUBLICADO"
                                    WHERE t12id_examen = '. $idExamen.'';
        $statement = $this->adapter->query($_updateStatusExamen);
        $result = $statement->execute($_updateStatusExamen)->getAffectedRows();
        return $result;
        }
        
        public function obtenerCalificacion ($_idExamen, $_idExamenUsuario ){
         $_idExamen = (int)$_idExamen;
         $_idExamenUsuario = (int)$_idExamenUsuario;
         $_arrData = array(
                't04calificacion'
            );
            $select = $this->sql->select();     
            $select ->from( array('t04'=>'t04examen_usuario') )
                    ->columns($_arrData)
                    ->where(array('t12id_examen' => $_idExamen, 't04id_examen_usuario' => $_idExamenUsuario));
            $arr_calificacionExamen = $this->obtenerRegistros($select,$_arrData[0]);
            foreach ($arr_calificacionExamen as $arrCalificacion){
                return $arrCalificacion;
            }
        }        
    
        public function obtenerFechaExamen( $_intIdExamen, $_intIdGrupo){
            $_select = $this->sql->select();
            $_arrDatos = array('t09fecha_inicio','t09fecha_final');
            $_select->from(array('t09' =>'t09agenda'))
		->columns($_arrDatos)
		->where(array('t12id_examen' => $_intIdExamen, 't05id_grupo'  => $_intIdGrupo));
            $arrFecha = $this->obtenerRegistros($_select , $_arrDatos[0]);
            foreach ($arrFecha as $_arrFechas){
                $_arrFechasFormInicio = date("d/m/Y H:i", strtotime($_arrFechas['t09fecha_inicio']));
                $_arrFechasFormFinal = date("d/m/Y H:i", strtotime($_arrFechas['t09fecha_final']));
                $_arrFechasInicio = explode(" ", $_arrFechasFormInicio);
                $_arrFechasFinal = explode(" ", $_arrFechasFormFinal);
                $_arrRespFechas= 
                         array(
                            'fechaInicio' => $_arrFechasInicio[0],
                            'horaInicio' => $_arrFechasInicio[1],
                            'fechaFinal' => $_arrFechasFinal[0],
                            'horaFinal' => $_arrFechasFinal[1]
                    ); 
            }
            return $_arrRespFechas;
        }
        
        protected function textoIntentos($_intNumIntentos){
            if ( $_intNumIntentos === '1') {
                $_strVez = 'sólo una vez';
            } else {
                //$_strVez = 'veces';
                $_strVez = $_intNumIntentos . ' veces';
            }
            return $_strVez;
        }
        
        public function existeExamen($idExamen){
            $camposSelect = array('id'  =>'t12id_examen');
            $select=$this->sql->select()->columns($camposSelect)->from('t12examen')->where('t12id_examen='.$idExamen);
            $statement=$this->sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            if($result->count() < 1){
                return false;
            }else{
                return true;
            }
        }
        
        public function getIdJson($_intIdExamen, $_intIdUsuarioPerfil) {
            $_select = $this->sql->select();
            $_select->from(array('t01' => 't01usuario'))
                    ->columns(array('nombreJson' => new Expression('CONCAT(t12nombre_corto_clave,"_", t09descripcion)')))
                    ->join(array('t02' => 't02usuario_perfil'), 't01.t01id_usuario = t02.t01id_usuario')
                    ->join(array('t08' => 't08administrador_grupo'), 't02.t02id_usuario_perfil = t08.t02id_usuario_perfil')
                    ->join(array('t09' => 't09agenda'), 't08.t05id_grupo = t09.t05id_grupo')
                    ->join(array('t12' => 't12examen'), 't12.t12id_examen = t09.t12id_examen')
                    ->where(array('t12.t12id_examen=' . $_intIdExamen, 't02.t02id_usuario_perfil=' . $_intIdUsuarioPerfil));
            $statement = $this->sql->prepareStatementForSqlObject($_select);
            $result = $statement->execute();
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrSelect = $resultSet->toArray();
            return $_arrSelect[0]['nombreJson'];
    }

    /**
     * Recibe id de examenTODO y obtiene nombre de actividad correspondiente en campo t12id_actividad
     * 
     * @param int $idExamen
     * @return strgin idActividad 
     */
    public function obtenerIdActividad($idExamen){

        $strQueryPadres = 'SELECT t12.t12id_examen,t12id_actividad
                            FROM t12examen t12 	
                            where t12.t12id_examen =  :idExamen';

        $statement = $this->adapter->query($strQueryPadres);
        $results = $statement->execute(array(
            ':idExamen' => $idExamen
        ));

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $arrData = $resultSet->toArray();

        return $arrData[0]['t12id_actividad'];
    }

    /**
     * Recibe id de examenTODO y que se encarga de obtener informacion del mismo
     * @param int $_intIdExamen
     * @return array $_arrExamenes
     */
    public function obtenerExameneTODO($_intIdExamen) {
        $select = $this->sql->select();
        $_arrData = array(
            't12id_examen',
            'c02id_tipo_examen',
            'c04id_escala',
            'c05id_nivel',
            't12nombre',
            't12nombre_corto_clave',
            't12instrucciones',
            't12descripcion',
            't12fecha_registro',
            't12fecha_actualiza',
            't12num_intentos',
            't12aleatorio_preguntas',
            't12aleatorio_respuestas',
            't12demo',
            't12minimo_aprobatorio',
            't12tiempo',
            't12estatus',
        );

        $select->from(array('t12' => 't12examen'))
                ->columns($_arrData)
                ->where(array('t12.t12id_examen' => $_intIdExamen));
                
        $_arrExamenes = $this->obtenerRegistros($select, $_arrData[0]);
        
        return $_arrExamenes;
    }

    /**
     * Obtiene meta datos de examenTODO
     *
     * @param int $idLicenciaUsr
     * @return array $arrData
     */
    public function getPMessage($idLicenciaUsr){
        $strQueryPadres = 'SELECT t12.t12id_examen,t12tiempo, t04.t04num_intento, t12.t12minimo_aprobatorio ,t12id_actividad , t34.t34respuesta, t34.t34orden, t34.t34timer, t34calificacion, t34correcta, t34estatus
                    FROM t04examen_usuario t04
                    join t03licencia_usuario t03 on t04.t03id_licencia_usuario = t03.t03id_licencia_usuario
                    join t12examen t12 on t04.t12id_examen = t12.t12id_examen
                    left join t34meta_examen t34 on t03.t03id_licencia_usuario	 = t34.t03id_licencia_usuario	
                    where t03.t03id_licencia_usuario = :idLicenciaUsr';

        $statement = $this->adapter->query($strQueryPadres);
        $results = $statement->execute(array(
            ':idLicenciaUsr' => $idLicenciaUsr
        ));

        $resultSet = new ResultSet();
        $resultSet->initialize($results);
        $arrData = $resultSet->toArray();

        $arrData = count($arrData) > 0 ? $arrData[0] : false; 

        return $arrData;
    }

    /**
     * Inserta meta datos para examenTODO, se llama sólo cuando el examen es iniciado.
     *
     * @param int $idLicenciaUsr
     * @param array $arrMeta (orden, correctas, respuesta, timer)
     * @return array boolInsert de evento (bool -> true/false, msj->mensaje)
     */
    public function insertMetaExamen($idLicenciaUsr, $arrMeta){     
        try { 
            $strQueryPadres = 'INSERT INTO t34meta_examen 
                                (t03id_licencia_usuario, t34orden, t34correcta, t34respuesta, t34timer, t34fecha_registro) 
                                VALUES (:idLicenciaUsuario, :orden, :correcta, :respuesta, :timer, now());';

            $statement = $this->adapter->query($strQueryPadres);
            $results = $statement->execute(array(
                ':idLicenciaUsuario' => $idLicenciaUsr,
                ':orden' => $arrMeta['orden'],
                ':correcta' => $arrMeta['correcta'],
                ':respuesta' => $arrMeta['respuesta'],
                ':timer' => $arrMeta['timer']
            ));

            $totalInsert =  $results->getAffectedRows();

            
            $boolInsert = $totalInsert > 0 ? array('bool'=>true, 'msj'=> 'Reg completo, examen iniciado.') : array('bool'=>false, 'msj'=> 'Reg fallido, examen no iniciado.');
        } catch (\Exception $e) {
            $boolUpdt = array('bool'=>NULL, 'msj'=> 'Accion no completada.');
            error_log('ERROR: ExamenModel->insertMetaExamen');
            error_log($e->getMessage());
        }
        return $boolInsert;
    }

    /**
     * Actualiza meta datos para examenTODO.
     *
     * @param int $idLicenciaUsr
     * @param array $arrMeta
     * @return array boolUpdt de evento (bool -> true/false, msj->mensaje)
     */
    public function updtMetaExamen($idLicenciaUsr, $arrMeta){      

        try {           
            $strQueryPadres = 'UPDATE t34meta_examen 
                                SET t34respuesta = :respuesta,
                                    t34correcta = :correcta,
                                    t34orden = :orden,
                                    t34timer = :timer,
                                    t34fecha_actualizacion = now(),
                                    t34calificacion = :calificacion
                                WHERE
                                    t03id_licencia_usuario = :idLicenciaUsuario;';

            $statement = $this->adapter->query($strQueryPadres);
            $results = $statement->execute(array(
                ':idLicenciaUsuario' => $idLicenciaUsr,
                ':respuesta' => $arrMeta['respuesta'],
                ':correcta' => $arrMeta['correcta'],
                ':orden' => $arrMeta['orden'],
                ':timer' => $arrMeta['timer'],
                ':calificacion' => $arrMeta['calificacion']
            ));

            $totalUpdt =  $results->getAffectedRows();

            
            $boolUpdt = $totalUpdt > 0 ? array('bool'=>true, 'msj'=> 'Reg actualizado.') : array('bool'=>false, 'msj'=> 'Reg no actualizado.');
        } catch (\Exception $e) {
            $boolUpdt = array('bool'=>NULL, 'msj'=> 'Accion no completada.');
            error_log('ERROR: ExamenModel->updtMetaExamen');
            error_log($e->getMessage());
        }
        return $boolUpdt;
    }

    /**
     * Convierte minutos(int) en formato string hora:min:seg
     *
     * @param int $time
     * @param string $format default '%02d:%02d'
     * @return string
     */
    function convertToHoursMins($time, $format = '%02d:%02d') {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);
        return sprintf($format, $hours, $minutes);
    }

    /**
     * Finaliza estatus de examen y actualiza calificación.
     *
     * @param int $idLicenciaUsr
     * @param float $calif
     * @param int $_intMinimo
     * @return array boolUpdt de evento (bool -> true/false, msj->mensaje)
     */
    public function finalizarExamenTodo($idLicenciaUsr, $calif, $_intMinimo){              
        try {           
            $strQueryPadres = 'UPDATE t34meta_examen 
                                SET t34calificacion = :calificacion,
                                    t34fecha_actualizacion = now()
                                WHERE
                                    t03id_licencia_usuario = :idLicenciaUsuario;';

            $statement = $this->adapter->query($strQueryPadres);
            $results = $statement->execute(array(
                ':idLicenciaUsuario' => $idLicenciaUsr,
                ':calificacion' => $calif,
            ));

            $totalUpdt =  $results->getAffectedRows();           

            if($totalUpdt > 0){
                $_intEstatus = $calif >= $_intMinimo ? EstatusExamen::APROBADO : EstatusExamen::REPROBADO;
                
                $strQueryPadres = 'UPDATE t04examen_usuario
                                        SET 
                                        t04calificacion = :calificacion,
                                        t04estatus = :estatusFIn,
                                        t04tiempo = time_format(timediff(  DATE_FORMAT(now(),"%Y-%m-%d %h:%i:%s")  , t04fecha_inicio),"%H:%i:%s"),
                                        t04fecha_fin = now()
                                    WHERE t03id_licencia_usuario = :idLicenciaUsuario
                                    AND t04estatus =:estatusIniciado;';

                $statement = $this->adapter->query($strQueryPadres);
                $results = $statement->execute(array(
                    ':idLicenciaUsuario' => $idLicenciaUsr,
                    ':calificacion' => $calif,
                    ':estatusFIn' => $_intEstatus,
                    ':estatusIniciado' => 'INICIADO',
                ));

                $totalUpdt =  $results->getAffectedRows();

                $boolUpdt = $totalUpdt > 0 ? array('bool'=>true, 'msj'=> 'Reg actualizado.') : array('bool'=>false, 'msj'=> 'Reg no actualizado t04examen_usuario.');
            }else{
                $boolUpdt = array('bool'=>false, 'msj'=> 'Reg no actualizado t34meta_examen.');
            }    
        } catch (\Exception $e) {
            $boolUpdt = array('bool'=>NULL, 'msj'=> 'Accion no completada.');
            error_log('ERROR: ExamenModel->updtMetaCalifExamen');
            error_log($e->getMessage());
        }

        return $boolUpdt;
    }
    

}
