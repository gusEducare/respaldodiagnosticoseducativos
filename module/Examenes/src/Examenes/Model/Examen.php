<?php
namespace Examenes\Model;



 use Zend\InputFilter\InputFilter;
 use Zend\InputFilter\InputFilterAwareInterface;
 use Zend\InputFilter\InputFilterInterface;
 
 
class Examen //implements InputFilterAwareInterface
{
    
    /**
     * 
     * @var int 
     */
    public $id_examen;

    /**
     * 
     * @var int
     */
    public $id_tipo_examen;

    /**
     * 
     * @var int
     */
    public $id_escala;

    /**
     * 
     * @var int
     */
    public $id_nivel;

    /**
     * 
     * @var string
     */
    public $nombre;

    /**
     * 
     * @var string
     */
    public $nombre_corto_clave;

    /**
     * 
     * @var string
     */
    public $instrucciones;

    /**
     * 
     * @var string
     */
    public $descripcion;

    /**
     * 
     * @var string
     */
    public $fecha_registro;

    /**
     * 
     * @var string
     */
    public $fecha_actualiza;

    /**
     * 
     * @var int
     */
    public $num_intentos;

    /**
     * 
     * @var int
     */
    public $aleatorio_preguntas;

    /**
     * 
     * @var int
     */
    public $aleatorio_respuestas;

    /**
     * 
     * @var int
     */
    public $demo;

    /**
     * 
     * @var int
     */
    public $minimo_aprobatorio;	

    /**
     * 
     * @var string
     */
    public $tiempo;

    /**
     * 
     * @var string
     */
    public $estatus;
    
    
    /**
	 *
	 * @var InputFilterInterface $inputFilter
	 */
	protected $inputFilter;
        /*
     public function exchangeArray($data)
     {
         $this->id_examen           = (!empty($data['t12id_examen'])) ? $data['t12id_examen'] : null;
         $this->id_tipo_examen      = (!empty($data['c02id_tipo_examen'])) ? $data['c02id_tipo_examen'] : null;
         $this->id_escala           = (!empty($data['c04id_escala'])) ? $data['c04id_escala'] : null;         
         $this->id_nivel            = (!empty($data['c05id_nivel'])) ? $data['c05id_nivel'] : null;
         $this->nombre              = (!empty($data['t12nombre'])) ? $data['t12nombre'] : null;
         $this->nombre_corto_clave  = (!empty($data['t12nombre_corto_clave'])) ? $data['t12nombre_corto_clave'] : null;
         $this->instrucciones       = (!empty($data['t12instrucciones'])) ? $data['t12instrucciones'] : null;
         $this->descripcion         = (!empty($data['t12descripcion'])) ? $data['t12descripcion'] : null;
         $this->fecha_registro      = (!empty($data['t12fecha_registro'])) ? $data['t12fecha_registro'] : null;
         $this->fecha_actualiza     = (!empty($data['t12fecha_actualiza'])) ? $data['t12fecha_actualiza'] : null;
         $this->num_intentos        = (!empty($data['t12num_intentos'])) ? $data['t12num_intentos'] : null;
         $this->aleatorio_preguntas = (!empty($data['t12aleatorio_preguntas'])) ? $data['t12aleatorio_preguntas'] : null;
         $this->aleatorio_respuestas= (!empty($data['t12aleatorio_respuestas'])) ? $data['t12aleatorio_respuestas'] : null;   
         $this->demo                = (!empty($data['t12demo'])) ? $data['t12demo'] : null;
         $this->minimo_aprobatorio  = (!empty($data['t12minimo_aprobatorio'])) ? $data['t12minimo_aprobatorio'] : null;
         $this->tiempo              = (!empty($data['t12tiempo'])) ? $data['t12tiempo'] : null;
         $this->estatus             = (!empty($data['t12estatus'])) ? $data['t12estatus'] : null;                  
     }*/
    
    
    public function exchangeArray($data)
    {
        $this->id_examen            = (isset($data['t12id_examen']))            ? $data['t12id_examen']             :null;
        $this->id_tipo_examen       = (isset($data['c02id_tipo_examen']))       ? $data['c02id_tipo_examen']        :null;
        $this->id_escala            = (isset($data['c04id_escala']))            ? $data['c04id_escala']             :null;
        $this->id_nivel             = (isset($data['c05id_nivel']))             ? $data['c05id_nivel']              :null;
        $this->nombre               = (isset($data['t12nombre']))               ? $data['t12nombre']                :null;
        $this->nombre_corto_clave   = (isset($data['t12nombre_corto_clave']))   ? $data['t12nombre_corto_clave']    :null;
        $this->instrucciones        = (isset($data['t12instrucciones']))        ? $data['t12instrucciones']         :null;
        $this->descripcion          = (isset($data['t12descripcion']))          ? $data['t12descripcion']           :null;
        $this->fecha_registro       = (isset($data['t12fecha_registro']))       ? $data['t12fecha_registro']        :null;
        $this->fecha_actualiza      = (isset($data['t12fecha_actualiza']))      ? $data['t12fecha_actualiza']       :null;
        $this->num_intentos         = (isset($data['t12num_intentos']))         ? $data['t12num_intentos']          :null;
        $this->aleatorio_preguntas  = (isset($data['t12aleatorio_preguntas']))  ? $data['t12aleatorio_preguntas']   :null;
        $this->aleatorio_respuestas = (isset($data['t12aleatorio_respuestas'])) ? $data['t12aleatorio_respuestas']  :null;
        $this->demo                 = (isset($data['t12demo']))                 ? $data['t12demo']                  :null;
        $this->minimo_aprobatorio   = (isset($data['t12minimo_aprobatorio']))   ? $data['t12minimo_aprobatorio']    :null;
        $this->tiempo               = (isset($data['t12tiempo']))               ? $data['t12tiempo']                :null;
        $this->estatus              = (isset($data['t12estatus']))              ? $data['t12estatus']               :null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}