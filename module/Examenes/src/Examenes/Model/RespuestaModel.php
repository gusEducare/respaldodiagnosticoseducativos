<?php

namespace Examenes\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Examenes\Entity\Respuesta;
//use Examenes\Entity\Pregunta;
use Zend\Db\ResultSet\ResultSet;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;
use Zend\Log\Writer\FirePhp;
use Zend\Db\Sql\Select;
use Examenes\Form\RespuestaForm;
use Zend\Db\Sql\Expression;

//use Examenes\Model\TipoPregunta;
/**
 * Añadi esta clase se encontraba en ExamenModel
 * no lo reconocia
 */
final class EstadoRespuesta {

    const ACTIVA = 1;
    const INACTIVA = 2;

}

final class TiposPregunta {

    const OPMULT = 1;
    const RESMUL = 2;
    const VERFAL = 3;
    const RELARRA = 4;
    const RELCON = 5;
    const PREGAB = 6;
    const PREGCO = 7;
    const ARRACO = 8;
    const ARRAOR = 9;
    const NUMORD = 10;
    const RELNUM = 11;
    const RELLIN = 12;
    const MATRIZ = 13;
    const ROMPEC = 14;
    const CRUCIG = 15;
    const SOPLET = 16;

}
/**
 * Clase RespuestaModel encargada de generar todas las funcionalidades con 
 * respecto a las respuestas contenidas en una pregunta.
 * @author oreyes@grupoeducare.com
 * @author jpgomez@grupoeducare.com
 * @since 11-12-2014   
 */
class RespuestaModel {

    /**
     * Instancia de sql.
     * @var Sql $sql
     */
    protected $sql;

    /**
     * instancia de Logger.
     * @var Logger $log
     */
    protected $log;

    /**
     * instancia de Logger.
     * @var Logger $console
     */
    protected $console;

    /**
     * Constructor de la clase RespuestaModel, en el cual solo creamos la 
     * instancia del objeto Sql y del Logger
     * @param Adapter $adapter
     */
    function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->sql = new Sql($adapter);
        $this->log = new Logger();
        $this->log->addWriter(new Stream('php://stderr'));

        $writer = new FirePhp();
        $this->console = new Logger();
        $this->console->addWriter($writer);
    }

    /**
     * Funcion que se encarga de guardar los datos de las respuestas en la
     * tabla de t13respuesta. En el caso de las preguntas de tipo verdadero
     * falso, no se guarda la respuesta, debido a que ya estan almacenadas
     * estas opciones por default
     * @param Respuesta $resp
     * @param int $_idPregunta
     * @return type
     */
    public function guardarRespuesta(Respuesta $resp, $_idPregunta, \Examenes\Entity\Pregunta $preg) {
        $_intTipo = $this->obtenerTipoPregunta($_idPregunta);
        if ($_intTipo !== TiposPregunta::VERFAL) {
            $_respParent = $this->obtenerRespuestaParent($_intTipo, $_idPregunta, $resp);
            $resp->__set('id_respuesta_parent', $_respParent['parent']);
            $resp->__set('correcta', $_respParent['correcta']);
            $colsRespuesta = $resp->obtenerColumnasRespuesta();
            $_mixRespuesta = $this->obtenerRespuestaTipo($_intTipo, $resp);
            $insResp = $this->sql->insert();
            $insResp->into('t13respuesta');
            $insResp->columns($colsRespuesta);
            $insResp->values(array(
                't13id_respuesta_parent' => $resp->__get('id_respuesta_parent'),
                't13respuesta' => $_mixRespuesta,
                't13orden' => $resp->__get('orden'),
                't13fecha_registro' => date('Y/m/d H:i:s'),
                't13fecha_actualiza' => date('Y/m/d H:i:s'),
                't13estatus' => EstadoRespuesta::ACTIVA));

            $resResp = $this->sql->prepareStatementForSqlObject($insResp);
            $_idRespuesta = $resResp->execute()->getGeneratedValue();
            $_idPregunta = $this->insertaPreguntasHijas($_intTipo, $_idPregunta, $resp, $preg);
            $_idResPreg = $this->guardaRespuestaPregunta($_idPregunta, $_idRespuesta, $resp->__get('correcta') ? 1 : 0);
        } else {
            $select = $this->sql->select();
            $select->from(array('t17' => 't17pregunta_respuesta'))
                    ->columns(array('t13id_respuesta'))
                    ->where(array('t17.t11id_pregunta' => $_idPregunta));

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrRespuestaPreg = $resultSet->toArray();
            if (count($_arrRespuestaPreg)) {
                $_idResPreg = $this->guardaRespuestaPregunta($_idPregunta, 3782, $resp->__get('correcta'), $resp->__get('orden'));
            } else {
                $_idResPreg = $this->guardaRespuestaPregunta($_idPregunta, 3781, $resp->__get('correcta'), $resp->__get('orden'));
            }
        }
        return $_idResPreg;
    }
    
    public function guardarRespuestaCopiar(){
        
    }

    /**
     * Función que se encarga de guardar la información en la tabla 
     * t17pregunta_respuesta con la relación de preguntas y respuestas, así 
     * como su opción correcta, si es que el tipo de pregunta lo amerita
     * @param int $_idPregunta
     * @param int $_idRespuesta
     * @param string $_strCorrecta
     * @return type
     */
    protected function guardaRespuestaPregunta($_idPregunta, $_idRespuesta, $_strCorrecta, $_intOrden = 0) {
        $_intCorrecta = (int) filter_var($_strCorrecta, FILTER_VALIDATE_BOOLEAN);

        $colsRespuestaPregunta = array(
            't11id_pregunta',
            't13id_respuesta',
            't17correcta',
            't17orden',
            't17fecha_registro',
            't17fecha_actualiza'
        );

        $insResPreg = $this->sql->insert();
        $insResPreg->into('t17pregunta_respuesta');
        $insResPreg->columns($colsRespuestaPregunta);
        $insResPreg->values(array(
            't11id_pregunta' => $_idPregunta,
            't13id_respuesta' => $_idRespuesta,
            't17correcta' => $_intCorrecta,
            't17orden' => $_intOrden,
            't17fecha_registro' => date('Y/m/d H:i:s'),
            't17fecha_actualiza' => date('Y/m/d H:i:s')));

        $resResPreg = $this->sql->prepareStatementForSqlObject($insResPreg);
        $_idResPreg = $resResPreg->execute()->getGeneratedValue();

        return $_idResPreg;
    }

    /**
     * Función que obtiene las respuestas, esta operación puede devolver 2 
     * arrays distintos, si recibe el segundo parametro en 0, devolvera un 
     * array con la informacion del tipo de pregunta elegido, en el caso
     * contrario, se devuelve un array con la informacion de la pregunta 
     * @param type $_intTipoPregunta
     * @param type $_intIdPregunta
     * @return array
     */
    public function obtenerRespuestas($_intTipoPregunta, $_intIdPregunta, $_intIdUsuarioLicencia = 0, $_intIdExamen = 0, $_intNumIntento = 0) {
        $_arrInfoRespuesta = array();
        $select = $this->sql->select();
        if ($_intIdPregunta) {
            $_arrInfoRespuesta = $this->obtenerRespuestasExistente($_intIdPregunta, $select, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
        } else {
            $_arrInfoRespuesta = $this->obtenerRespuestasNueva($_intTipoPregunta, $select);
            // Agrego el caso para las preguntas de tipo Verdadero Falso 
            // donde las respuestas nunca cambiarán
            if ($_intTipoPregunta === 3) {
                $_arrInfoRespuesta['respuestas'] = array();
            }
        }
        return $_arrInfoRespuesta;
    }

    /**
     * Funcion que se encarga de obtener el array con la pregunta de acuerdo
     * al id que se recibe
     * @param type $_intIdPregunta
     * @param type $select
     * @return array
     */
    protected function obtenerRespuestasExistente($_intIdPregunta, $select, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento) {
        $_intTipo = $this->obtenerTipoPregunta($_intIdPregunta);
        $_arrRespuesta = array();
        $_arrPregunta = array();
        $preg = new \Examenes\Entity\Pregunta();
        $resp = new Respuesta();
        $_arrColsPreg = $preg->obtenerColumnasPreg();
        $_arrColsResp = $resp->obtenerColumnasResp();
        $_strFiltro = $this->obtenerFiltroConsulta($_intIdPregunta, $_arrColsPreg);
        $_strInstruccionTipoPregunta = $this->obtenerInstruccionTipo($_intTipo);
        $select->from(array('t11' => 't11pregunta'))
                ->columns($_arrColsPreg)
                ->join(array('t17' => 't17pregunta_respuesta'), 't17.t11id_pregunta = t11.t11id_pregunta', array('t17correcta'), Select::JOIN_LEFT)
                ->join(array('t13' => 't13respuesta'), 't17.t13id_respuesta = t13.t13id_respuesta', $_arrColsResp, Select::JOIN_LEFT);

        $select->where(array(
            't11.' . $_strFiltro => $_intIdPregunta, 
            //'t13.t13estatus' => EstadoRespuesta::ACTIVA //TODO JPGA- Quito el filtro de estatus en respuesta porque afecta a las preguntas de 2 columnas
                ));
        if ($_intTipo !== TiposPregunta::OPMULT && $_intTipo !==
                TiposPregunta::RESMUL && $_intTipo !== TiposPregunta::VERFAL && $_intTipo !== TiposPregunta::RELCON) {
            $select->order('t17correcta DESC');  // Se corrige bug de ordenamiento en las respuestas que no tienen caja de texto al final	
        }
        $select->order('t13orden');
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRespuestaPreg = $resultSet->toArray();
        // Parche: cambio la ruta de las imagenes para que se manden llamar desde
        // la ruta 192.168.2.21 en vez de la 200.57.103.6:666
        foreach($_arrRespuestaPreg as $index => $data){
            $_arrRespuestaPreg[$index]['t11pregunta'] = $this->cambiaRuta($data['t11pregunta']);
            $_arrRespuestaPreg[$index]['t13respuesta'] = $this->cambiaRuta($data['t13respuesta']);
            $_arrRespuestaPreg[$index]['t11instruccion'] .= '<br>' . $_strInstruccionTipoPregunta;
            //->log->debug("Ins: ". $_arrRespuestaPreg[$index]['t11instruccion'] );
        }
        if ($_intIdUsuarioLicencia && $_intTipo !== TipoPregunta::MATRIZ) {
            $_arrOrden = $this->desordenarRespuestas($_intIdPregunta, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento);
        }else{
            $_arrOrden = $this->muestraRespuestasOrdenadas( $_arrRespuestaPreg );
        }
        // Se agrega la condición de las preguntas matriz donde es un tipo 
        // distinto de obtención de datos
        if ($_intTipo === TiposPregunta::MATRIZ) {
            $selectMatriz = $this->sql->select();
            $selectMatriz->from(array('t11' => 't11pregunta'))
                    ->columns($_arrColsPreg)
                    ->where(array(
                        't11id_pregunta_parent' => $_intIdPregunta
            ));
            $statement = $this->sql->prepareStatementForSqlObject($selectMatriz);
            $result = $statement->execute();

            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrPreguntasMatriz = $resultSet->toArray();
            //$_strOrden = $_arrRespuestaOrden[0]['t10orden'];
        }
        $i = 0;
        $j = 0;
        $_intNumRespuestas = count($_arrOrden);
        foreach ($_arrRespuestaPreg as $index => $data) {
            if($index < $_intNumRespuestas){
                foreach ($_arrColsResp as $rowsResp) {
                    if ($data[$rowsResp] || $data[$rowsResp] == 0) {
                        if (isset($_arrOrden)) {
                            $_arrRespuesta[(int) array_search($i, $_arrOrden)][$rowsResp] = $data[$rowsResp];
                        } else {
                            $_arrRespuesta[$i][$rowsResp] = $data[$rowsResp];
                        }
                    }
                }
            }
            foreach ($_arrColsPreg as $rowsPreg) {
                if ($_intTipo !== TiposPregunta::MATRIZ) {
                    $_arrPregunta[$i][$rowsPreg] = $data[$rowsPreg];
                }
            }
            // Agrego si es la ocpion correcta en caso de que se ocupe
            if (isset($_arrOrden)) {
                $_arrRespuesta[(int) array_search($i, $_arrOrden)]['t17correcta'] = $data['t17correcta'];
            } else {
                $_arrRespuesta[$i]['t17correcta'] = $data['t17correcta'];
            }
            $i++;
        }

        if ($_intTipo === TiposPregunta::MATRIZ) {
            foreach ($_arrPreguntasMatriz as $dataMatriz) {
                foreach ($_arrColsPreg as $rowsPreg) {
                    $_arrPregunta[$j][$rowsPreg] = $dataMatriz[$rowsPreg];
                }
                $j++;
            }
        }

        // = count($_arrRespuesta);
        $_arrInfoRespuesta = array(
            'numRespuestas' => $_intNumRespuestas,
            'respuestas' => $_arrRespuesta,
            'preguntas' => $_arrPregunta);
        $_arrRespuestaPregunta = $this->obtenerRespuestasPregunta($_arrRespuestaPreg, $_intIdPregunta, $_arrColsPreg);
        $_arrRespuestaPregunta[0]['t11pregunta'] = $this->cambiaRuta($_arrRespuestaPregunta[0]['t11pregunta']);
        foreach ($_arrColsPreg as $rowsPreg) {
            if ($_arrRespuestaPregunta[0][$rowsPreg] !== NULL) {
                $_arrInfoRespuesta['pregunta'][$rowsPreg] = $_arrRespuestaPregunta[0][$rowsPreg];
            } else {
                $_arrInfoRespuesta['pregunta'][$rowsPreg] = '';
            }
        }
        //$this->console->info( $_arrInfoRespuesta );
        return $_arrInfoRespuesta;
    }

    // SELECT * FROM t10respuesta_usuario_examen where t12id_examen = 70 and t03id_licencia_usuario = 29;
    /**
     * Funcion que se encarga de obtener el orden en el cual se almacenaron 
     * las respuestas para un examen en especifico
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_intNumIntento
     * @return type
     */
    protected function desordenarRespuestas($_intIdPregunta, $_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento) {
        //$_intTipo = $this->obtenerTipoPregunta($_intIdPregunta);
        $select = $this->sql->select();
        $select->from(array('t10' => 't10respuesta_usuario_examen'))
                ->columns(array('t10orden'))
                ->where(array(
                    't11id_pregunta' => $_intIdPregunta,
                    't03id_licencia_usuario' => $_intIdUsuarioLicencia,
                    't12id_examen' => $_intIdExamen,
                    't10num_intento' => $_intNumIntento,
        ));
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRespuestaOrden = $resultSet->toArray();
        $_strOrden = $_arrRespuestaOrden[0]['t10orden'];
        $_intLong = strlen($_strOrden);
        for ($i = 0; $i < $_intLong; $i++) {
//            if($_intTipo == TiposPregunta::RELNUM || $_intTipo == TiposPregunta::RELLIN || $_intTipo == TiposPregunta::ARRACO ){
//                    $_arrRespOrden[$i] = (int)$_strOrden[$i];                    
//                }else{
            $_arrRespOrden[$i] = (int) $_strOrden[$i] - 1;
//                }
        }
        return $_arrRespOrden;
    }
    
    /**
     * Funcion que se encarga de generar el array de las respuestas en orden 
     * como se registraron en el almacenamiento original de la pregunta
     * @param type $_arrRespuestaPreg
     * @return int
     */
    protected function muestraRespuestasOrdenadas( $_arrRespuestaPreg ){
        $_arrRespuesta = array();
        $i=0; 
        foreach($_arrRespuestaPreg as  $data){
            $_arrRespuesta[$i] = $i;
            $i++; 
        }
        return $_arrRespuesta;
    }

    /**
     * Función que se encarga de revolver el orden de aparicion de las 
     * respuestasde una pregunta.
     * @param type $_arrCorrecta
     * @param type $_arrOrden
     * @return type regresa un array con dos pocisiones, la primera es la 
     * cadena de la respuesta correcta, la segunda es la cadena de el orden
     * correcto
     */
    protected function desordenarArrayRespuestas($_arrCorrecta, $_arrOrden, $_intTipoPregunta, $_bolAleatorioRespuestas) {

        $_inLongitud = (count($_arrOrden) > count($_arrCorrecta)) ? count($_arrCorrecta) : count($_arrOrden);
        $_arrNums = range(0, $_inLongitud - 1);
        if ($_bolAleatorioRespuestas) {
            shuffle($_arrNums);
        }
        foreach ($_arrNums as $index => $data) {
            if (isset($_arrCorrecta[$data])) {
                $_arrCorrectasShuffle[$index] = $_arrCorrecta[$data];
            }
            if (isset($_arrOrden[$data])) {
                $_arrOrdenShuffle[$index] = $_arrOrden[$data];
            }
        }
        if (count($_arrOrdenShuffle) > count($_arrCorrectasShuffle)) {
            $this->log->debug('Orden Original: ' . print_r($_arrOrdenShuffle, true));
            $pos = array_search(count($_arrOrdenShuffle), $_arrOrdenShuffle);
            unset($_arrOrdenShuffle[$pos]);
            //$_arrOrdenShuffle = array_pop($_arrOrdenShuffle);
            $this->log->debug('Orden pos: ' . print_r($_arrOrdenShuffle, true));
        }
        if ($_intTipoPregunta === TiposPregunta::ARRACO || $_intTipoPregunta === TiposPregunta::ARRAOR ||
                $_intTipoPregunta === TiposPregunta::NUMORD || $_intTipoPregunta === TiposPregunta::RELNUM ||
                $_intTipoPregunta === TiposPregunta::RELLIN || $_intTipoPregunta === TiposPregunta::RELARRA) {
            $_arrCorrectasShuffle = $this->desordenarRespuestasRelacion($_arrCorrectasShuffle);
        }
        //$this->log->debug(print_r($_arrNums, true));
        $_arrRespuesta = array(
            'correctas' => $_arrCorrectasShuffle,
            'orden' => TiposPregunta::MATRIZ !== $_intTipoPregunta ? $_arrOrdenShuffle : $_arrOrden
        );
        return $_arrRespuesta;
    }

    /**
     * Funcion que se encarga de revolver el orden de las respuestas, esta
     * funcion nos servira para los tipos de preguntas donde existe relacion
     * u ordenamiento 
     * @param type $_arrCorrectasShuffle
     * @return type retornara el orden final que se almacena en la columna 
     * de t10correcta
     */
    protected function desordenarRespuestasRelacion($_arrCorrectasShuffle) {
        $_arrRespuesta = array();
        //$this->log->debug( 'Elementos: ' .  count($_arrCorrectasShuffle) );
        foreach ($_arrCorrectasShuffle as $index => $data) {
            $_arrRespuesta[(int) $data] = $index + 1;
        }
        ksort($_arrRespuesta);
        return $_arrRespuesta;
    }

    /**
     * Funcion que se encarga de obtener la informacion de la respuesta de 
     * acuerdo al tipo que se recibe
     * @param type $_intTipoPregunta
     * @param type $select
     * @return array
     */
    protected function obtenerRespuestasNueva($_intTipoPregunta, $select) {
        $select->from(
                        array(
                            'c03' => 'c03tipo_pregunta'))
                ->columns(
                        array(
                            'c03id_tipo_pregunta', 'c03clave_tipo', 'c03minimo_respuestas'))
                ->where(
                        array(
                            'c03id_tipo_pregunta' => $_intTipoPregunta));

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRespuesta = $resultSet->toArray();
        $_arrInfoRespuesta = array(
            'numRespuestas' => $_arrRespuesta[0]['c03minimo_respuestas'],
            'respuestas' => array()
        );
        return $_arrInfoRespuesta;
    }

    /**
     * Funcion que se encarga de guardar la calificación 
     * @param object $_objRespuesta
     * @param int $_intIdUsuario
     * @param int $_intIdExamen
     * @param string $_strLlave
     * @return boolean
     */
    public function guardarRespuestaUsuario($_objRespuesta, $_intIdUsuarioLicencia, $_intIdExamen, $_intIdPregunta, $_intNumIntento) {
        $_strTipoExamen = $this->obtenerTipoExamen($_intIdExamen);
        if( $_strTipoExamen === "4"){
            $_strUsuario = $_objRespuesta;
        }else{
            $_strUsuario = $this->concatenaRespuestaUsuario($_intIdPregunta, $_objRespuesta, $_intIdExamen);
            $_strUsuario = $this->formateaRespuestaUsuario($_strUsuario, $_intIdPregunta, $_intIdExamen);
            
        }
        $updateRespuestaUsuario = $this->sql->update('t10respuesta_usuario_examen');
        $updateRespuestaUsuario->set(array(
                    't10respuesta_usuario' => $_strUsuario ? $_strUsuario : NULL,
                    't10fecha_actualiza' => date('Y/m/d H:i:s'),
                        //'t10tiempo'               => '00:00:00')
                        //'t10tiempo'               => $_strTiempo)
                ))
                ->where(array(
                    't03id_licencia_usuario' => $_intIdUsuarioLicencia,
                    't12id_examen' => $_intIdExamen,
                    't11id_pregunta' => $_intIdPregunta,
                    't10num_intento' => $_intNumIntento
        ));

        $result = $this->sql->prepareStatementForSqlObject($updateRespuestaUsuario);
        $_bolRespuesta = $result->execute()->getAffectedRows();

        return $_bolRespuesta;
    }

    /**
     * Funcion que se encarga de guardar la respuesta que eligio el usuario
     * @param type $_arrRespuestas
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_intIdPregunta
     * @param type $_intNumIntento
     * @return type
     */
    public function guardarRespuestaUsuarioNueva($_arrRespuestas, $_intIdUsuarioLicencia, $_intIdExamen, $_intIdPregunta, $_intNumIntento, $_bolAleatorioRespuestas) {

        $_strUsuario = null;
        //$_intCorrecta = (int)filter_var($_strCorrecta, FILTER_VALIDATE_BOOLEAN); 
        $_arrRespOrdenCorrecta = $this->obtenerRespuestaOrdenCorrecta($_arrRespuestas, $_bolAleatorioRespuestas);
        $_strCorrecta = $_arrRespOrdenCorrecta['correcta'];
        $_strOrden = $_arrRespOrdenCorrecta['orden'];
        $colsRespuestaPregunta = array(
            't03id_licencia_usuario',
            't11id_pregunta',
            't12id_examen',
            't10num_intento',
            't10respuesta_usuario',
            't10respuesta_correcta',
            't10orden',
            't10fecha_registro',
            't10fecha_actualiza',
            't10tiempo'
        );

        $insResPreg = $this->sql->insert();
        $insResPreg->into('t10respuesta_usuario_examen');
        $insResPreg->columns($colsRespuestaPregunta);
        $insResPreg->values(array(
            't03id_licencia_usuario' => $_intIdUsuarioLicencia,
            't11id_pregunta' => $_intIdPregunta,
            't12id_examen' => $_intIdExamen,
            't10num_intento' => $_intNumIntento,
            't10respuesta_usuario' => $_strUsuario,
            't10respuesta_correcta' => $_strCorrecta,
            't10orden' => $_strOrden,
            't10fecha_registro' => date('Y/m/d H:i:s'),
            //'t10fecha_actualiza'        => date('Y/m/d g:i:s'),
            't10tiempo' => '00:00:00'));

        $resResPreg = $this->sql->prepareStatementForSqlObject($insResPreg);
        $_idResPreg = $resResPreg->execute()->getGeneratedValue();

        return $_idResPreg;
    }

    /**
     * Funcion que se encarga de obtener lo que el usuario respondió, se
     * reciben como paramentros el id del usuariolicencia, el id del examen,
     * el numero de intento, y las respuestas del usuario
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_intNumIntento
     * @param type $_arrRespuestas
     * @return type
     */
    public function obtenerRespuestaUsuario($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_arrRespuestas) {

        $select = $this->sql->select();
        //  Se agrega la validación de si es pregunta compuesta
        $_strIdPregunta = $_arrRespuestas['pregunta']['t11id_pregunta_parent'] !== "0" ?
                $_arrRespuestas['pregunta']['t11id_pregunta_parent'] :
                $_arrRespuestas['pregunta']['t11id_pregunta'];

        $_arrData = array('t10respuesta_usuario');

        $select->from(array('t10' => 't10respuesta_usuario_examen'))
                ->columns($_arrData)
                ->where(array(
                    't03id_licencia_usuario' => $_intIdUsuarioLicencia,
                    't12id_examen' => $_intIdExamen,
                    't10num_intento' => $_intNumIntento,
                    't11id_pregunta' => $_strIdPregunta));
        //$_arrRegistros = $this->obtenerRegistros($select, $_arrData[0], $_arrData[0]);
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRegistros = $resultSet->toArray();

        $_arrRespuestas['pregunta']['usuario'] = $this->obtenerRespuestaUsuarioTipo((int) $_strIdPregunta, $_arrRegistros, $_arrData);

        return $_arrRespuestas;
    }

    /**
     * Función que extrae la información del objeto RespuestaForm y lo 
     * convierte a array
     * @param RespuestaForm $form
     * @param type $columns
     * @return type
     */
    public function crearArregloRespuesta(RespuestaForm $form, $columns) {
        $_arrPos = array(2, 7);
        $_arrRespuesta = array();
        foreach ($_arrPos as $data) {

            $_arrData = $form->get($columns[$data]);
            $_arrRespuesta[$columns[$data]]['attributes'] = $_arrData->getAttributes();
            $_arrRespuesta[$columns[$data]]['label'] = $_arrData->getLabel();
            $_arrRespuesta[$columns[$data]]['messages'] = $_arrData->getMessages();
            $_arrRespuesta[$columns[$data]]['options'] = $_arrData->getOptions();
        }

        //$_arrRespuesta[$columns[2]] = $this->formRow($form->get($columns[2]));
        //$_arrRespuesta[$columns[7]] = $this->formRow($form->get($columns[7]));
        /*
          echo $this->formRow($form->get($columns[2])->setValueOptions($this->arrTipos));
          echo $this->formRow($form->get($columns[3]));
          echo $this->formRow($form->get($columns[4]));
          echo $this->formRow($form->get($columns[5]));
          echo $this->formRow($form->get($columns[6]));
          echo $this->formRow($form->get($columns[7]));
          echo $this->formRow($form->get($columns[8])->setValueOptions($this->arrNumeracion));
          echo $this->formHidden($form->get($columns[11]));
         */
        return $_arrRespuesta;
    }

    /**
     * Funcion que se encarga de obtener el tipo de la pregunta que se envia
     * como parametro
     * @param type $_intIdPregunta
     * @return type
     */
    protected function obtenerTipoPregunta($_intIdPregunta) {
        $select = $this->sql->select();
        $_arrData = array('c03id_tipo_pregunta');
        $select->from(array('t11' => 't11pregunta'))
                ->columns($_arrData)
                ->where(array('t11id_pregunta' => $_intIdPregunta));
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRegistros = $resultSet->toArray();
        $_intResp = (int) $_arrRegistros[0][$_arrData[0]];
        return $_intResp;
    }
    public function obtenerTipoPerguntaTodo($_intIdPregunta, $_idExamen){
        $_objExamenJson = $this->obtenerContenidoExamenJson("1002.json");
        $_intIdTipo = intVal($_objExamenJson[$_intIdPregunta - 1]['pregunta']['c03id_tipo_pregunta']);
        return $_intIdTipo;
    }
    /**
     * Obtiene de la base de datos la confugracion por defecto de cada tipo 
     * de pregunta
     * @param type $_intIdTipo
     * @return type
     */
    public function obtenerMinimoMaximoRespuestas($_intIdTipo = 1) {
        $select = $this->sql->select();
        $_arrData = array('c03id_tipo_pregunta', 'c03minimo_respuestas', 'c03maximo_respuestas');
        $select->from(array('c03' => 'c03tipo_pregunta'))
                ->columns($_arrData)
                ->where(array('c03id_tipo_pregunta' => $_intIdTipo));
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrRegistros = $resultSet->toArray();
        $_intRespMin = (int) $_arrRegistros[0][$_arrData[1]];
        $_intRespMax = (int) $_arrRegistros[0][$_arrData[2]];
        $_arrResp = array(
            'minimo' => $_intRespMin,
            'maximo' => $_intRespMax
        );
        return $_arrResp;
    }

    /**
     * Funcion que se encarga de obtener el campo por el cual se filtrará la 
     * consulta a ejecutar para obtener las respuestas de una pregunta.
     * Esta filtro nos sirve, para obtener los datos de distintos tipos de 
     * preguntas. Para los tipos donde existe una pregunta y N respuestas, 
     * se obtiene el id_pregunta, para los tipos donde existen N preguntas y
     * N respuestas, se obtienen todas las preguntas que contengan el campo 
     * id_pregunta_parent
     * @param type $_intIdPregunta
     * @param type $_arrColsPreg
     * @return type
     */
    protected function obtenerFiltroConsulta($_intIdPregunta, $_arrColsPreg) {
        $_intIdTipo = $this->obtenerTipoPregunta($_intIdPregunta);
        //error_log(print_r($_intIdTipo, true));
        //error_log(print_r(TiposPregunta::OPMULT, true));

        if ($_intIdTipo === TiposPregunta::RELNUM || $_intIdTipo === TiposPregunta::PREGCO ||
                $_intIdTipo === TiposPregunta::ARRACO || $_intIdTipo === TiposPregunta::RELLIN
                || $_intIdTipo === TiposPregunta::RELARRA ) {
            $_strFiltro = $_arrColsPreg[1];
        } else {
            $_strFiltro = $_arrColsPreg[0];
        }
        return $_strFiltro;
    }

    /**
     * Esta funcion obtieene todas las preguntas hijo de una pregunta de los
     * tipos N preguntas, N respuestas. En caso contrario, se regresa el 
     * mismo array que recibe
     * @param type $_arrRespuestaPreg
     * @param type $_intIdPregunta
     * @param type $_arrColsPreg
     * @return type
     */
    protected function obtenerRespuestasPregunta($_arrRespuestaPreg, $_intIdPregunta, $_arrColsPreg) {
        $_intIdTipo = $this->obtenerTipoPregunta($_intIdPregunta);
        if ($_intIdTipo === TiposPregunta::RELNUM || $_intIdTipo === TiposPregunta::PREGCO || $_intIdTipo === TiposPregunta::ARRACO || $_intIdTipo === TiposPregunta::RELLIN) {
            $sel = $this->sql->select();
            $sel->from(array('t11' => 't11pregunta'))
                    ->columns($_arrColsPreg);
            
            $sel->where(array('t11.t11id_pregunta' => $_intIdPregunta));

            $statement = $this->sql->prepareStatementForSqlObject($sel);
            $result = $statement->execute();

            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrRespuestaPreg = $resultSet->toArray();
            $_arrRespuestaPreg[0]['t11instruccion'] .= '<br>' . $this->obtenerInstruccionTipo($_intIdTipo);
        }
        return $_arrRespuestaPreg;
    }

    /**
     * Funcion que se encarga de concatenar la respuesta del usuario que 
     * luego será insertada a la base de datos, aqui se tiene que crear un
     * metodo distinto para cada tipo de pregunta para que funcione 
     * correctamente
     * @param type $_intIdPregunta
     * @param type $_objRespuesta
     * @return string
     */
    protected function concatenaRespuestaUsuario($_intIdPregunta, $_objRespuesta) {
            $_intIdTipo = $this->obtenerTipoPregunta($_intIdPregunta);
        $_objRespuesta['resp'] = $_intIdTipo === TiposPregunta::RELLIN ? $_objRespuesta : $_objRespuesta['resp'];
        $_strUsuario = '';
        if ($_intIdTipo === TiposPregunta::RELNUM || $_intIdTipo === TiposPregunta::PREGAB ||
                $_intIdTipo === TiposPregunta::PREGCO || $_intIdTipo === TiposPregunta::ARRACO ||
                $_intIdTipo === TiposPregunta::ARRAOR || $_intIdTipo === TiposPregunta::NUMORD ||
                $_intIdTipo === TiposPregunta::RELLIN || $_intIdTipo === TiposPregunta::MATRIZ ||
                $_intIdTipo === TiposPregunta::ROMPEC) {
            foreach ($_objRespuesta['resp'] as $data) {
                if (isset($data['selected'])) {
                    if ($data['selected'] !== '') {
                        $_strUsuario .= TiposPregunta::PREGCO === $_intIdTipo ? '&|' . $data['selected'] : $data['selected'];
                    } else {
                        $_strUsuario .= TiposPregunta::PREGAB === $_intIdTipo ? null : '&|';
                    }
                } else {
                    $_strUsuario .= '0';
                }
            }
        } else {
            foreach ($_objRespuesta['resp'] as $data) {
                if ($data['selected'] === 'true') {
                    $_strUsuario .= '1';
                } else {
                    $_strUsuario .= '0';
                }
            }
        }
        $_strRespUsuario = $_intIdTipo === TiposPregunta::PREGCO ? substr($_strUsuario, 2) : $_strUsuario;
        return $_strRespUsuario;
    }

    /**
     * 
     * @param type $_strUsuario
     * @return mixed
     */
    protected function formateaRespuestaUsuario($_strUsuario, $_intIdPregunta) {
        $_intIdTipo = $this->obtenerTipoPregunta($_intIdPregunta);
        $_bolContesto = false;
        switch ($_intIdTipo) {
            case TiposPregunta::RELCON: case TiposPregunta::RESMUL:
            case TiposPregunta::NUMORD: case TiposPregunta::RELNUM:
            case TiposPregunta::ARRACO: case TiposPregunta::OPMULT:
            case TiposPregunta::VERFAL:
                if ($_strUsuario) {
                    $cadena = strlen($_strUsuario);
                    for ($i = 0; $i < $cadena; $i++) {
                        if ((int) $_strUsuario[$i]) {
                            $_bolContesto = true;
                        }
                    }
                }
                break;
            case TiposPregunta::PREGCO:
                $_arrRespuestas = explode('&|', $_strUsuario);
                foreach ($_arrRespuestas as $data) {
                    if ($data) {
                        $_bolContesto = true;
                    }
                }
                break;
            default:
                $_bolContesto = true;
        }

        return $_bolContesto ? $_strUsuario : false;
    }

    /**
     * Funcion que sirve para obtener el campo de lo que contestó el usuario
     * para la pregunta especifica, se segmenta por cada tipo de preguntas
     * @param type $_intIdPregunta
     * @param type $_arrRegistros
     * @param type $_arrData
     * @return array
     */
    protected function obtenerRespuestaUsuarioTipo($_intIdPregunta, $_arrRegistros, $_arrData) {
        $_intIdTipo = $this->obtenerTipoPregunta($_intIdPregunta);
        $_arrRespuestaUsuario = array();
        $_mixResp = $_arrRegistros[0][$_arrData[0]];
        // Forzo las preguntas de tipo 6  para que no se iteren en este paso
        switch ($_intIdTipo) {
            case TiposPregunta::PREGAB:
                $_intLong = 0;
                $_arrRespuestaUsuario[0] = $_mixResp;
                break;
            case TiposPregunta::PREGCO:
                $_arrResps = explode('&|', $_mixResp);
                $_intLong = count($_arrResps);
                break;
            default :
                $_intLong = strlen($_mixResp);
        }
        for ($i = 0, $j = 0; $i < $_intLong; $i++) {
            switch ($_intIdTipo) {
                case TiposPregunta::RELNUM: case TiposPregunta::ARRACO:
                case TiposPregunta::ARRAOR: case TiposPregunta::NUMORD:
                case TiposPregunta::RELLIN: case TiposPregunta::MATRIZ:
                    //$_strCaracter = ;
                    $_strResp = $_mixResp[$i] != '0' ? $_mixResp[$i] : '';
                    $_arrRespuestaUsuario[$j] = $_strResp; // Se requiere el 0 para insertarlo en los lugares vacíos
                    $j++;
                    break;
                case TiposPregunta::PREGCO:
                    $_arrRespuestaUsuario[$i] = $_arrResps[$i];
                    break;
                default:
                    if ($_mixResp[$i] == 1) {
                        $_intPosRes = $i + 1;
                        $_arrRespuestaUsuario[$j] = $_intPosRes;
                        $j++;
                    }
            }
        }
        // Mando un array vacío en el indice usuario para indicar que no ha respondido
        if (!isset($_arrRespuestaUsuario)) {
            $_arrRespuestaUsuario = array();
        }
        return $_arrRespuestaUsuario;
    }

    /**
     * Obtiene las respuestas correctas de una pregunta y el orden en el 
     * que deben aparecer, para posteriormente guardarse en la base de datos
     * @param type $_arrRespuestas
     * @return type
     */
    protected function obtenerRespuestaOrdenCorrecta($_arrRespuestas, $_bolAleatorioRespuestas) {
        //$_strCorrecta = '';
        $_arrCorrecta = array();
        //$_strOrden = '';
        $_arrOrden = array();
        $_intTipo = (int) $_arrRespuestas['pregunta']['c03id_tipo_pregunta'];
        foreach ($_arrRespuestas['respuestas'] as $index => $data) {
            switch ($_intTipo) {
                case TiposPregunta::RELNUM: case TiposPregunta::NUMORD: case TiposPregunta::ARRACO:
                case TiposPregunta::ARRAOR: case TiposPregunta::RELLIN: case TiposPregunta::SOPLET:
                case TipoPregunta::RELARRA:
                    if (isset($data['t13orden'])) {
                        //$_strCorrecta .= $data['t13orden']; 
                        $_arrCorrecta [$index] = $data['t13orden'];
                    }
                    break;
                case TiposPregunta::PREGCO:
                    if (isset($data['t13respuesta'])) {
                        //$_strCorrecta .= strip_tags($data['t13respuesta']) . '&|'; // Delimitador
                        $_arrCorrecta [$index] = strip_tags($data['t13respuesta']);
                    }
                    break;
                case TiposPregunta::PREGAB:
                    //$_strCorrecta .= '*'; 
                    $_arrCorrecta [$index] = '*';
                    break;
                default:
                    //$_strCorrecta .= $data['t17correcta'];
                    $_arrCorrecta [$index] = $data['t17correcta'];
                    break;
            }
            //$_strOrden .= ($index + 1);
            $_arrOrden [$index] = ($index + 1);
        }
//            if(substr($_strCorrecta, -2) === '&|'){
//                $_strCorrecta = substr($_strCorrecta, 0, -2);
//            }
        // valido que el ultimo elemento lleva respuesta
        $_intUltimaRespuesta = isset($_arrCorrecta[$index]) ? $index : $index - 1;
        if (substr($_arrCorrecta[$_intUltimaRespuesta], -2) === '&|') {
            $_arrCorrecta[$_intUltimaRespuesta] = substr($_arrCorrecta[$index], 0, -2);
        }
        $_arrRespuestasOrden = $this->desordenarArrayRespuestas($_arrCorrecta, $_arrOrden, $_intTipo, $_bolAleatorioRespuestas);
        $_strDelimitador = TiposPregunta::PREGCO === $_intTipo ? '&|' : '';
        //$this->log->debug('Orden Funciona: ' . print_r($_arrRespuestasOrden['orden'], true));
        return array(
            'correcta' => implode($_strDelimitador, $_arrRespuestasOrden['correctas']),
            'orden' => implode('', $_arrRespuestasOrden['orden'])
        );
    }
//     public function obtenerRutaImagen ($_arrRespuestas){
//           
//          foreach ($_arrRespuestas['respuestas'] as $index => $data) {
//              $this->log->debug("error111111111111111111111".print_r($data['t13respuesta'],true));
//          $explode1 = explode("img src=", $data['t13respuesta']);
//         $this->log->debug("error/*/*/*/***/*/*/*/**/*/*//*/*/*/".print_r($explode1,true));
//          }
//         foreach ($_arrRespuestas['respuestas'] as $index => $data) {
//         $explode2 = explode("img src=",$data['t13respuesta']);
//          }
//           foreach ($_arrRespuestas['preguntas'] as $index => $data){
//               $explode1 = explode("img src=", $data['t11pregunta']);
//           }
//           return $rutaImg;
         //"<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/IC4-1P-monitor.jpg" alt="" data-mce-src="../../img/examenes/IC4-1P-monitor.jpg"></div>"
         //"<p>Da clic en el botón de la respuesta correcta.</p><br><img src="../../img/ayudaOpcionMultiple.png">"
     //}
     /**
     * Formateo arrPregunta para que lo pueda interpretar el json 
     * del examen, en esta función también se elimina las respuestas 
     * correctas para que no se puedan visualizar desde el navegador 
     * @param type $_arrRespuestas
     * @return type $arrPregunta
     */
    public function formateaObjetoRespuesta($_arrRespuestas) {
        // Formateo arrPregunta para que lo pueda interpretar el json 
        // del examen
        $_intIdTipo = (int) $_arrRespuestas['pregunta']['c03id_tipo_pregunta'];
        foreach ($_arrRespuestas['respuestas'] as $index => $data) {
            switch ($_intIdTipo) {
                case TiposPregunta::OPMULT:
                case TiposPregunta::RESMUL:
                case TiposPregunta::RELCON:
                case TiposPregunta::NUMORD:
                case TiposPregunta::RELNUM:
                    unset($data['t17correcta']);
                    break;
                case TiposPregunta::PREGCO:
                    unset($data['t13respuesta']);
                    break;
                default:
                    break;
            }
            $arrPregunta['respuestas'][$index] = $data;
        }
        foreach ($_arrRespuestas['preguntas'] as $index => $data) {
            //unset($data['t17correcta']);
            $arrPregunta['preguntas'][$index] = $data;
        }
        $arrPregunta['pregunta'] = $_arrRespuestas['pregunta'];
        return $arrPregunta;
    }

    /**
     * Formateo arrPregunta para que lo pueda interpretar el json 
     * de los libros interactivos. 
     * @param type $_arrRespuestas
     * @return type $arrPregunta
     */
    public function formateaObjetoRespuestaJSON($_arrRespuestas) {
        $_arrImagenes=array();
        $it=0;
        $t=1;
        $id_preg=$_arrRespuestas['pregunta']['t11id_pregunta'];
        $_intIdTipo = (int) $_arrRespuestas['pregunta']['c03id_tipo_pregunta'];
        unset($_arrRespuestas['pregunta']['t11fecha_registro']);
        unset($_arrRespuestas['pregunta']['t11id_pregunta']);
        unset($_arrRespuestas['pregunta']['t11id_pregunta_parent']);
        unset($_arrRespuestas['pregunta']['t11nombre_corto_clave']);
        unset($_arrRespuestas['pregunta']['t11instruccion']);
        unset($_arrRespuestas['pregunta']['t11tiempo_limite']);
        unset($_arrRespuestas['pregunta']['t11columnas']);
        unset($_arrRespuestas['pregunta']['c06id_numeracion']);
        unset($_arrRespuestas['pregunta']['t11fecha_actualiza']);
        unset($_arrRespuestas['pregunta']['t11estatus']);
//        $_arrRespuestas['pregunta']['t13respuesta']=str_replace("http://certificacionestics.com/img/examenes/","recursos/imagenes/",$_arrRespuestas['pregunta']['t13respuesta']);
//        $_arrRespuestas['pregunta']['t13respuesta']=str_replace('../../img/examenes/','recursos/imagenes/',$_arrRespuestas['pregunta']['t13respuesta']); 
//         $explode1 = explode("img src=", $_arrRespuestas['pregunta']['t11pregunta']);
//         $this->log->debug("error/*/*/*/***/*/*/*/**/*/*//*/*/*/".print_r($explode1,true));
        //"<div class="imgsAnt"><img src="recursos/imagenes/IC4SBE1L1~opcionB_goma.jpg" data-mce-src="recursos/imagenes/IC4SBE1L1~opcionB_goma.jpg" data-mce-selected="1"></div><div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: n-resize; margin: 0px; padding: 0px; left: 45.5px; top: 4.5px;"></div><div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: e-resize; margin: 0px; padding: 0px; left: 85.5px; top: 44.5px;"></div><div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: s-resize; margin: 0px; padding: 0px; left: 45.5px; top: 84.5px;"></div><div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: w-resize; margin: 0px; padding: 0px; left: 5.5px; top: 44.5px;"></div><div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: nw-resize; margin: 0px; padding: 0px; left: 5.5px; top: 4.5px;"></div><div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: ne-resize; margin: 0px; padding: 0px; left: 85.5px; top: 4.5px;"></div><div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: se-resize; margin: 0px; padding: 0px; left: 85.5px; top: 84.5px;"></div><div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="cursor: sw-resize; margin: 0px; padding: 0px; left: 5.5px; top: 84.5px;"></div>"

        switch ($_intIdTipo){
            case TiposPregunta::VERFAL: case TiposPregunta::OPMULT: case TiposPregunta::RESMUL: case TiposPregunta::RELCON: case TiposPregunta::ARRAOR: case TiposPregunta::MATRIZ: case TiposPregunta::SOPLET:  //agregar ARRAOR MATRIZ
                unset($_arrRespuestas['preguntas']);
                  break;
        }
        foreach ($_arrRespuestas['respuestas'] as $index => $data) {
            $data['t13respuesta']=str_replace("http://certificacionestics.com/img/examenes/","recursos/imagenes/",$data['t13respuesta']);
            $data['t13respuesta']=str_replace('../../img/examenes/','recursos/imagenes/',$data['t13respuesta']);
            preg_match_all("/(http:\/\/|recursos\/imagenes\/)+(:\/\/)?(\S*)\.(\w{2,4})/", $data['t13respuesta'],$_arrSalidaRespuesta,PREG_PATTERN_ORDER);
            foreach ($_arrSalidaRespuesta[3] as $indice => $dato){
                $_arrImagenes[$it]=$dato.'.'.$_arrSalidaRespuesta[4][$indice];
                $it++;
            }
//             $explode2 = explode("data-mce-src=",$data['t13respuesta']);
//             $explode3=explode('/imagenes/',$explode2[1]);
//             $explode4=explode('></div>',$explode3[1]);
//             $texto=str_replace('"','',$explode4[0]); 
         
           // $arrayImg[$index]=$texto;
            if( $data['t13respuesta']==""){
                //$this->log->debug("error/*/*/*/***/*/*/*/**/*/*//*/*/*/22 ".print_r($_arrRespuestas['respuestas'][$index],true));
                unset($_arrRespuestas['respuestas'][$index]);
            }else{
                switch ($_intIdTipo) {
                    case TiposPregunta::VERFAL: case TiposPregunta::OPMULT: case TiposPregunta::RESMUL: case TiposPregunta::RELCON: case TiposPregunta::NUMORD: case TiposPregunta::RELNUM: case TiposPregunta::PREGCO: case TiposPregunta::MATRIZ: case TiposPregunta::SOPLET:
                        unset($data['t13fecha_registro']);
                        unset($data['t13id_respuesta']);
                        unset($data['t13id_respuesta_parent']);
                        unset($data['t13orden']);
                        unset($data['t13fecha_actualiza']);
                        unset($data['t13estatus']);
                        
                        break;
                    case TiposPregunta::RELLIN:case TiposPregunta::ARRACO:case TiposPregunta::ARRAOR: case TiposPregunta::SOPLET:
                        
                        unset($data['t13fecha_registro']);
                        unset($data['t13id_respuesta']);
                        unset($data['t13id_respuesta_parent']);
                        unset($data['t13orden']);
                        unset($data['t13fecha_actualiza']);
                        unset($data['t13estatus']);
                        $_stra =(string)$t;
                        $data['t17correcta']= $_stra;
                        break;
                    
                  //  RELLIN ARRACO

                }
                $t++;
                $arrPregunta['respuestas'][$index] = $data;
             }
        }
        //  $this->log->debug("error/*/*/*/***/*/*/*/**/*/*//*/*/*/$id_preg".print_r($_arrImagenes,true));
        if(isset($_arrRespuestas['preguntas'])){
            foreach ($_arrRespuestas['preguntas'] as $index => $data) {
                        unset($data['t11fecha_registro']);
                        unset($data['t11id_pregunta']);
                        unset($data['t11id_pregunta_parent']);
                        unset($data['t11nombre_corto_clave']);
                        unset($data['t11instruccion']);
                        unset($data['t11tiempo_limite']);
                        unset($data['t11columnas']);
                        unset($data['c06id_numeracion']);
                        unset($data['t11fecha_actualiza']);
                        unset($data['t11estatus']);
                        $data['t11pregunta']=str_replace("http://certificacionestics.com/img/examenes/","recursos/imagenes/",$data['t11pregunta']);
                        $data['t11pregunta']=str_replace('../../img/examenes/','recursos/imagenes/',$data['t11pregunta']); 
                        
                        preg_match_all("/(http:\/\/|recursos\/imagenes\/)+(:\/\/)?(\S*)\.(\w{2,4})/", $data['t11pregunta'],$_arrSalidaRespuesta,PREG_PATTERN_ORDER);
                        foreach ($_arrSalidaRespuesta[3] as $indice => $dato){
                           $_arrImagenes[$it]=$dato.'.'.$_arrSalidaRespuesta[4][$indice];
                           $it++;
            }
                            
                            
                $arrPregunta['preguntas'][$index] = $data;
            }
            
        }
        $arrPregunta['pregunta'] = $_arrRespuestas['pregunta'];
        $arrPregunta['imagenes']= $_arrImagenes;
        return $arrPregunta;
    }

    /**
     * Función que se encarga a obtener la respuesta dependiendo del tipo de
     * pregunta, esto aplica para las preguntas que llevan dos columnas y se
     * comportan de distinta forma que el resto
     * @param type $_intTipo
     * @param type $resp
     * @return type Mixed
     */
    protected function obtenerRespuestaTipo($_intTipo, Respuesta $resp) {
        //$_strRespuesta = $_intTipo !== 6 ? $resp->__get('respuesta') : '*';
        switch ($_intTipo) {
            case 6:
                $_strRespuesta = '*';
                break;
            case 7: case 8: case 11: case 12: case 17:
                $_strRespuesta = $resp->__get('correcta');
                break;
            default:
                $_strRespuesta = $resp->__get('respuesta');
        }
        return $_strRespuesta;
    }

    /**
     * Función que se encarga de generar el objeto de tipo pregunta y despues
     * insertarlo en la tabla de preguntas, esto funciona para los tipos de 
     * preguntas donde son mas de una columna, en caso de que la pregunta no 
     * tenga esas caracteriticas, se regresa el mismo id de pregunta que se 
     * recibio
     * @param type $_intTipo
     * @param int $_idPregunta
     * @param Respuesta $resp
     * @param Pregunta $preg
     * @return int
     */
    protected function insertaPreguntasHijas($_intTipo, $_idPregunta, Respuesta $resp, \Examenes\Entity\Pregunta $preg) {
        $_bolInsert = false;
        switch ($_intTipo) {
            case 7: case 8: case 11: case 12: case 17:
                $_strPregunta = $resp->__get('respuesta');
                $preg->__set('pregunta', $_strPregunta);
                $preg->__set('id_tipo_pregunta', $_intTipo);
                $preg->__set('id_pregunta_parent', $_idPregunta);
                $preg->__set('estatus', 'A');
                $_bolInsert = true;
                break;
            default :
        }
        if ($_bolInsert) {
            $insertPregunta = $this->sql->insert();
            $insertPregunta->into('t11pregunta');
            $insertPregunta->columns(array(
                't11id_pregunta_parent',
                'c03id_tipo_pregunta',
                't11pregunta',
                't11fecha_registro',
                't11fecha_actualiza',
                't11estatus',
            ));
            $insertPregunta->values(array(
                't11id_pregunta_parent' => $preg->__get('id_pregunta_parent'),
                'c03id_tipo_pregunta' => $preg->__get('id_tipo_pregunta'),
                't11pregunta' => $preg->__get('pregunta'),
                't11fecha_registro' => date('Y/m/d H:i:s'),
                't11fecha_actualiza' => date('Y/m/d H:i:s'),
                't11estatus' => $preg->__get('estatus')));

            $result = $this->sql->prepareStatementForSqlObject($insertPregunta);
            $_idPregunta = $result->execute()->getGeneratedValue();
        }
        return $_idPregunta;
    }

    /**
     * Función que se encarga de obtener id parent de la respuesta y la 
     * opcion correcta, en realidad solo se verifica el tipo de pregunta y 
     * si es de columas, se regresa el dato del id pregunta y 1 en el indice
     * de correcta, de lo contrario se regresa 0 y el valor de correcta 
     * @param type $_intTipo
     * @param type $_idPregunta
     * @return mixed
     */
    protected function obtenerRespuestaParent($_intTipo, $_idPregunta, Respuesta $resp) {
        switch ($_intTipo) {
            case 7: case 9: case 10: case 8: case 11: case 12: case 17:
                $_idPreguntaParent = $_idPregunta;
                $_bolCorrecta = 1;
                break;
            default:
                $_idPreguntaParent = 0;
                $_bolCorrecta = $resp->__get('correcta');
        }
        if ($_intTipo === 7 || $_intTipo === 8 || $_intTipo === 11 || $_intTipo === 12 || $_intTipo === 17) { // Forzo la respuesta de la correcta en el tipo de pregunta 7
            $_bolCorrecta = $resp->__get('correcta');
        }
        return array(
            'parent' => $_idPreguntaParent, 'correcta' => $_bolCorrecta);
    }

    /**
     * Esta funcion actualiza la respuesta de una pregunta.
     * @param array $arrRespusta
     * @return boolean
     */
    public function actualizaRespuesta($arrRespusta, $idPregunta) {
        $_intTipoPreg = $this->obtenerTipoPregunta($idPregunta);
        switch ($_intTipoPreg) {
            case TiposPregunta::PREGCO:
            case TiposPregunta::ARRACO:
            case TiposPregunta::RELNUM:
            case TiposPregunta::RELLIN:
                $_instanciaUpdatet11 = $this->sql->update()
                        ->table('t11pregunta')
                        ->set(array('t11pregunta' => $arrRespusta['respuesta'], 't11fecha_actualiza' => date('Y/m/d H:i:s')))
                        ->where(array('t11id_pregunta' => $arrRespusta['idSubPreg']));

                $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdatet11);

                if ($result->execute()->getAffectedRows() > 0) {
                    $_instanciaUpdatet13 = $this->sql->update()
                            ->table('t13respuesta')
                            ->set(array('t13respuesta' => $arrRespusta['correcta'], 't13orden' => $arrRespusta['orden'], 't13fecha_actualiza' => date('Y/m/d H:i:s'), 't13estatus' => $arrRespusta['estatus']))
                            ->where(array('t13id_respuesta' => $arrRespusta['id']));


                    $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdatet13);

                    if ($result->execute()->getAffectedRows() > 0) {
                        $boolUpt = true;
                    } else {
                        $boolUpt = false;
                    }
                } else {
                    $boolUpt = false;
                }
                break;

            default :
                $_instanciaUpdatet13 = $this->sql->update()
                        ->table('t13respuesta')
                        ->set(array('t13respuesta' => $arrRespusta['respuesta'], 't13orden' => $arrRespusta['orden'], 't13fecha_actualiza' => date('Y/m/d H:i:s'), 't13estatus' => $arrRespusta['estatus']))
                        ->where(array('t13id_respuesta' => $arrRespusta['id']));

                $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdatet13);

                if ($result->execute()->getAffectedRows() > 0) {
                    $_instanciaUpdatet17 = $this->sql->update()
                            ->table('t17pregunta_respuesta')
                            ->set(array(
                                    't17correcta' => TiposPregunta::ARRAOR === $_intTipoPreg ? "1" : $arrRespusta['correcta'], 
                                    't17fecha_actualiza' => date('Y/m/d H:i:s')))
                            ->where(array('t11id_pregunta' => $idPregunta))
                            ->where(array('t13id_respuesta' => $arrRespusta['id']));

                    $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdatet17);
                    if ($result->execute()->getAffectedRows() > 0) {
                        $boolUpt = true;
                    } else {
                        $boolUpt = false;
                    }
                } else {
                    $boolUpt = false;
                }
                break;
        }
        return $boolUpt;
    }

    /**
     * Funcion que se encarga de obtener y actualizar el tiempo
     * que se hace entre una pregunta y otra.
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_intNumIntento
     * @param type $_intIdPregunta
     * @return type
     */
    function obtenerDiferenciaTiempo($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPregunta) {      
        $_dateRegistro = $this->obtenerFechaRegistro($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPregunta);
        $_dateActualiza = $this->obtenerFechaActualiza($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPregunta);
        
        if (isset($_dateRegistro) && isset($_dateActualiza)) {
            if ($_dateRegistro > $_dateActualiza) {
                $_updateTiempo = $this->sql->update()
                        ->table('t10respuesta_usuario_examen')
                ->set(array('t10tiempo' => new Expression("ADDTIME(t10tiempo, timediff(t10fecha_actualiza,t10fecha_registro))")))
                        ->where(array('t03id_licencia_usuario' => $_intIdUsuarioLicencia, 't12id_examen' => $_intIdExamen, 't10num_intento' => $_intNumIntento, 't11id_pregunta' => $_intIdPregunta));
                $result = $this->sql->prepareStatementForSqlObject($_updateTiempo);
                return $result->execute()->getAffectedRows() > 0;
            } else {
                $_updateTiempo = $this->sql->update()
                        ->table('t10respuesta_usuario_examen')
                        ->set(array('t10tiempo' => new Expression("ADDTIME(t10tiempo, timediff(t10fecha_actualiza , t10fecha_registro))")))
                        ->where(array('t03id_licencia_usuario' => $_intIdUsuarioLicencia, 't12id_examen' => $_intIdExamen, 't10num_intento' => $_intNumIntento, 't11id_pregunta' => $_intIdPregunta));
                $result = $this->sql->prepareStatementForSqlObject($_updateTiempo);
                return $result->execute()->getAffectedRows() > 0;
            }
        } else {
            return false;   
        }
    }

    function obtenerFechaRegistro($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPregunta){
        $_selectRegistro = $this->sql->select();
                $_selectRegistro->from(array('t10' =>'t10respuesta_usuario_examen'))
                        ->columns(array('hora' => new Expression("date_format(t10fecha_registro, '%H')")))
                        ->where(array('t03id_licencia_usuario' => $_intIdUsuarioLicencia, 't12id_examen' => $_intIdExamen, 't10num_intento' => $_intNumIntento, 't11id_pregunta' => $_intIdPregunta));
            $statement = $this->sql->prepareStatementForSqlObject($_selectRegistro);
            $result = $statement->execute();
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_dateRegistro = $resultSet->toArray();
            return $_dateRegistro;
    }
    
    function obtenerFechaActualiza($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPregunta){
         $_selectActualiza = $this->sql->select();
                $_selectActualiza->from(array('t10' =>'t10respuesta_usuario_examen'))
                        ->columns(array('hora' => new Expression("date_format(t10fecha_actualiza, '%H')")))
                        ->where(array('t03id_licencia_usuario' => $_intIdUsuarioLicencia, 't12id_examen' => $_intIdExamen, 't10num_intento' => $_intNumIntento, 't11id_pregunta' => $_intIdPregunta));    
            $statement = $this->sql->prepareStatementForSqlObject($_selectActualiza);
            $result = $statement->execute();
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_dateActualiza = $resultSet->toArray();
            return $_dateActualiza;
    }
        /**
     * Funcion que se encarga de obtener y actualizar la fecha actualizada
     * cada vez que se termina una pregunta
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_intNumIntento
     * @param type $_intIdPregunta
     * @return boolean
     */
    function guardarPreguntaActualiza($_intIdUsuarioLicencia, $_intIdExamen, $_intNumIntento, $_intIdPregunta) {
        $_instanciaUpdate = $this->sql->update()
                ->table('t10respuesta_usuario_examen')
                ->set(array('t10fecha_actualiza' => date('Y/m/d H:i:s')))
                ->where(array('t03id_licencia_usuario' => $_intIdUsuarioLicencia, 't12id_examen' => $_intIdExamen, 't10num_intento' => $_intNumIntento, 't11id_pregunta' => $_intIdPregunta));

        $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdate);
        if ($result->execute()->getAffectedRows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Funcion que se encarga de obtener y actualizar la fecha de registro
     * cada vez que se inicia una pregunta
     * @param type $_intIdUsuarioLicencia
     * @param type $_intIdExamen
     * @param type $_intActual
     * @return boolean
     */
    function guardarPreguntaRegistro($_intIdUsuarioLicencia, $_intIdExamen, $_intActual) {
        $_instanciaUpdate = $this->sql->update()
                ->table('t10respuesta_usuario_examen')
                ->set(array('t10fecha_registro' => date('Y/m/d H:i:s')))
                ->where(array('t03id_licencia_usuario' => $_intIdUsuarioLicencia, 't12id_examen' => $_intIdExamen, 't11id_pregunta' => $_intActual));

        $result = $this->sql->prepareStatementForSqlObject($_instanciaUpdate);
        if ($result->execute()->getAffectedRows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Funcion qu cambia las rutas de las imagenes para poder leerlas desde local
     * 
     * @param type $dato
     * @return type
     */
    public function cambiaRuta($dato){
        //$_strRespuesta = str_replace('200.57.103.6:666', '192.168.2.21', $dato);
        return $dato;
    }
    
    /**
     * Funcion que se encarga de obtener el texto de ayuda del tipo de pregunta
     * por el momento, solo es una imagen, en un futuro se pretende incluir un 
     * video explicativo.
     * @param type $_intTipo
     * @return type
     */
    public function obtenerInstruccionTipo($_intTipo){
        
        $_selectRegistro = $this->sql->select();
        $_selectRegistro->from(array('c03' =>'c03tipo_pregunta'))
                        ->columns(array('c03instruccion'))
                        ->where(array('c03id_tipo_pregunta' => $_intTipo));
        $statement = $this->sql->prepareStatementForSqlObject($_selectRegistro);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrTipo = $resultSet->toArray();
        $_strTipo = $_arrTipo[0]['c03instruccion'];
        return $_strTipo;
    }
    
    public function obtenerTipoExamen($examen) {
        $_selectTipo = $this->sql->select();
        $_selectTipo->from(array('t12' => 't12examen'))
                ->columns(array('c02id_tipo_examen'))
                ->where(array('t12id_examen' => $examen));
        $statement = $this->sql->prepareStatementForSqlObject($_selectTipo);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrTipo = $resultSet->toArray();
        $_strTipo = $_arrTipo[0]['c02id_tipo_examen'];
        return $_strTipo;
    }
    
    public function obtenerContenidoExamenJson($archivo) {
        $file = $_SERVER["DOCUMENT_ROOT"] . '/todo/' . $archivo;
        $contenido = json_decode(file_get_contents($file), true);
        return $contenido;
    }

    public function totalPreguntasTodo($_intIdExamen) {
        $_strNombreArchivo = $this->nombreArchivoExamenTodo($_intIdExamen);
        $_strNombreArchivoJson = $_strNombreArchivo.".json";
        $_objExamen = $this->obtenerContenidoExamenJson($_strNombreArchivoJson);
        $_intLengthExamen = count($_objExamen);
        return $_intLengthExamen;
    }
    
    public function nombreArchivoExamenTodo($_intIdExamen) {
        $_selectNombreCortoClave = $this->sql->select();
        $_selectNombreCortoClave->columns(array('nombreCortoClave' => 't12nombre_corto_clave'))
                ->from('t12examen')
                ->where(array('t12id_examen' => $_intIdExamen));
        $statement = $this->sql->prepareStatementForSqlObject($_selectNombreCortoClave);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrSelect = $resultSet->toArray();
        return $_arrSelect[0]['nombreCortoClave'];
    }
    
    public function obtenerOrdenPregunta($_intIdPregunta){
        $_selectNombreCortoClave = $this->sql->select();
        $_selectNombreCortoClave->columns(array('orden' => 't10orden'))
                ->from('t10respuesta_usuario_examen')
                ->where(array('t11id_pregunta' => $_intIdPregunta));
        $statement = $this->sql->prepareStatementForSqlObject($_selectNombreCortoClave);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrSelect = $resultSet->toArray();
        return $_arrSelect[0]['orden'];
    }
    
    public function obtenerRespuestasTodo($_objPreguntas){
        $_arrPreguntas = array();
        $respuestasParent = "";
        foreach ($_objPreguntas as $key => $value) {
            $tipoPregunta = $value["c03id_tipo_pregunta"];
            $idPregunta = $value['t11id_pregunta'];
            if($this->isParent($value['t11id_pregunta'])){
                $respuestas = $this->obtenerRespuestasParent($idPregunta);
                $respuestasParent = $this->respuestaParent($idPregunta);
            }else{
                $respuestas = $this->obtenerRespuestaPregunta($idPregunta);
            }
            if($respuestasParent && $tipoPregunta != "5"){
                $_arrPreguntas[$key]['preguntas'] = $respuestas;
            }
            $_arrPreguntas[$key]['respuestas'] = $respuestasParent ? $respuestasParent:$respuestas;
            $_arrPreguntas[$key]['pregunta'] = $value;
            if($tipoPregunta == "5"){
                $_arrPreguntas[$key]['contenedores'][0] = "Contenedor";
                foreach ($_arrPreguntas[$key]['respuestas'] as $keyT => $valueT) {
                    $_arrPreguntas[$key]['respuestas'][$keyT]['t17correcta'] = "0";
                    
                }
                $_arrPreguntas[$key]['pregunta']["tipo"] = "vertical";
            }
        }
        
        return $_arrPreguntas;
    }
    
    public function isParent($idPregunta) {
        $select = $this->sql->select()->from('t11pregunta')->where('t11id_pregunta_parent=' . $idPregunta);
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() < 1) {
            return false;
        } else {
            return true;
        }
    }
    public function obtenerRespuestasParent($idPregunta){
        $_select = $this->sql->select();
        $_select->columns(array('t11pregunta', 'c03id_tipo_pregunta'))
                ->from('t11pregunta')
                ->where(array('t11id_pregunta_parent' => $idPregunta));
        $statement = $this->sql->prepareStatementForSqlObject($_select);
        $result = $statement->execute();
        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrSelect = $resultSet->toArray();
        return $_arrSelect;
    }
    
    public function obtenerRespuestaPregunta($idPregunta){
        $select = $this->sql->select();       
            $_arrData = array('t13respuesta');    
            $select ->from(
                        array( 't13'=>'t13respuesta') )
                    ->columns( $_arrData )
                    ->join( array( 't17' =>'t17pregunta_respuesta'),
                            't13.t13id_respuesta = t17.t13id_respuesta ',
                            array('t17correcta'))
                    ->join( array( 't11' =>'t11pregunta'),
                            't17.t11id_pregunta = t11.t11id_pregunta',
                            array());
            $select->where(array('t11.t11id_pregunta'=>$idPregunta));           
            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrData = $resultSet->toArray();  
            return $_arrData;
    }
    
    public function respuestaParent($idPregunta){
        $select = $this->sql->select()->from('t13respuesta')->columns(array('t13respuesta'))->where('t13id_respuesta_parent=' . $idPregunta);
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() < 1) {
            return false;
        } else {
            $resultSet = new ResultSet;
            $resultSet->initialize($result);
            $_arrData = $resultSet->toArray(); 
            return $_arrData;
        }
    }

}
