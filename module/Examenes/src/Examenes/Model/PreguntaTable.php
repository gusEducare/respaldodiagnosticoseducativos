<?php
namespace Examenes\Model;
use Zend\Db\TableGateway\TableGateway;
class PreguntaTable
{
    protected $tableGateway;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }
    
    public function extraeTodo(){
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
    
    public function obtenPregunta($id_preg) {
        $id = (int) $id_preg;
        $rowset = $this->tableGateway->select(array('t11id_pregunta' => $id));
        $row = $rowset->current();
        if(!$row){
            throw new \Exception("No se encontró el registro $id");
        }
        return $row;
    }
    
    public function borrarPregunta($id) {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}
