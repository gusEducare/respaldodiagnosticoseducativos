<?php

namespace Examenes\Form;

use Examenes\Entity\Respuesta;
use Zend\Form\Form;
use Examenes\Model\TipoPregunta;

class RespuestaForm extends Form
{
    public function __construct($name = null, $options = array()) {
        parent::__construct('Respuesta');
        $pregunta = new Respuesta();
        $columns = $pregunta->obtenerColumnasRespuesta();
        $_arrCorrecta = $this->obtenerInputCorrecta($options);
        $_arrRespuesta = $this->obtenerInputRespuesta($options);
        $this->add(array(
        	'name' 		=>'obtenerpreguntas',
        	'attributes'=>array(
        		'type'  => 'hidden',
        		'value' => 'obtenerpreguntasajax',
        		'id'	=> 'urlObtenerPreguntas',
        	),
        ));    
        
        $this->add( array(
            'name'          => $columns[1],
            'attributes'    => array(
                'type'      => 'hidden',
        //        'placeholder'   => 'Respuesta Parent'
                'class'     => '_objPregResp t13respuesta edit contorno ',
//                'id'        => $columns[1],
                'value'     => 0
            ),
            'options'       => array()
        ));
                
        $this->add( array(
            'name'           => $columns[2],
            'attributes'    => array(
                'type'          => $_arrRespuesta['input'],
                'placeholder'     => $_arrRespuesta['placeholder'],
        //        'id'        => $columns[2],
                'class'     => $_arrRespuesta['class']
            ),
            'options'       => array()
        ));
        
        $this->add( array(
            'name'           => $columns[3],
            'attributes'    => array(
                'type'          => 'hidden',
            //    'placeholder'   => 'Orden',
            //    'id'        => $columns[4],
                'class'     => '_objPregResp t13respuesta edit contorno ',
                'value'     => '1'
            ),
            'options'       => array()
        ));
                
        $this->add(array(
            'name'		=> $columns[4],
            'attributes'=> array(
                'type'  => 'hidden',
                'value' => date('Y/m/d g:i:s'),
                //'id'        => $columns[4],
                'class'     => '_objPregResp t13respuesta edit contorno '
            ),
        ));
        
        $this->add(array(
            'name'		=> $columns[5],
            'attributes'=> array(
                'type'	=> 'hidden',
                'value' => date('Y/m/d g:i:s'),
              //  'id'        => $columns[5],
                'class'     => '_objPregResp t13respuesta edit contorno '
            ),
        ));        
        
        $this->add(array(
            'name'  => $columns[6],
            'attributes'=> array(
                'type'	=> 'hidden',
                //'value' => 1,
                'id'        => $columns[6],
                //'class'     => '_objPregResp t13respuesta edit contorno '
            ),
        ));
        
        $this->add(array(
            'name'  => $columns[7],
            'type'	=> $_arrCorrecta['input'],
            'attributes'=> array(                
            'class'     => $_arrCorrecta['class'],
            'placeholder'     => $_arrCorrecta['placeholder']
            ),
        ));
        
        $this->add(array(
            'name' => 'agregar',
            'attributes' => array(
                'type'  => 'button',
                'value' => 'Nueva Respuesta',
                'id' 	=> 'boton-nueva',
   //             'class' => 'ui-state-default ui-corner-all boton-gral',
            	'class' => 'button small alert radius boton-gral'
            ),
        ));        
             
        $this->add(array(
            'name' => 'borrar',
            'type'  => 'button',
            'options' => array('label'=>''),
            'attributes' => array(
             //   'value' => '',
                //'id' 	=> 'boton-nueva',
   //             'class' => 'ui-state-default ui-corner-all boton-gral',
            	'class' => 'alert fa fa-trash-o tiny boton-borrar'
            ),
        ));        
        
    }
    
    /**
     * Funcion que se encarga de indicar que tipo de input debe llevar el campo 
     * de respuesta, esta información se debe obtener desde la base de datos
     * pero aun no se configura
     * @param type $options
     * @return string
     */
    protected function obtenerInputRespuesta($options) {
        $_strInput = 'text';
        $_strClass = '_objPregResp t13respuesta respuesta';
        $_strPlaceholder = 'Respuesta';
        if(isset($options['tipo'])){
            switch((int)$options['tipo']){
                case 6:
                    $_strInput = 'hidden';
                    break;
                case 7:
                    $_strPlaceholder = 'Pregunta';
                    break;
                default:
                    $_strInput = 'text';
                    break;
            }
        }        
        $_arrRespuesta = array( 
            'input'         => $_strInput, 
            'class'         => $_strClass,
            'placeholder'   => $_strPlaceholder);
        return $_arrRespuesta;          
    }
    
    /**
     * Funcion que se encarga de indicar que tipo de input debe llevar el campo 
     * opcion correcta, esta información se debe obtener desde la base de datos
     * pero aun no se configura
     * @param type $options
     * @return string
     */
    protected function obtenerInputCorrecta($options) {
        $_strInput = 'radio';
        $_strClass = '_objPregResp correcta columns ';
        $_strPlaceholder = '';
        if(isset($options['tipo'])){
            switch((int)$options['tipo']){
                case 2: case 5:
                    $_strInput = 'checkbox';
                    break;
                case 6: case 9: case 10:
                    $_strInput = 'hidden';
                    break;
                case 7: case 8: case 11: case 12: case 17:
                    $_strInput = 'text';
                    //$_strClass = '_objPregResp correcta  small-6 large-6 columns ';
                    $_strPlaceholder = 'Respuesta';
                    break;
            }
        }
        $_arrRespuesta = array(
            'input'         => $_strInput, 
            'class'         => $_strClass,
            'placeholder'   => $_strPlaceholder);
        return $_arrRespuesta;        
    }
    
}