<?php

namespace Examenes\Form;

use Examenes\Entity\Pregunta;
use Zend\Form\Form;

class PreguntaForm extends Form
{
    public function __construct($name = null, $options = array()) {
        parent::__construct('Pregunta');
        $pregunta = new \Examenes\Entity\Pregunta();
        $columns = $pregunta->obtenerColumnasPregunta();
                   
        $this->add(array(
        	'name' 		=>'creapregunta',
        	'attributes'=>array(
        		'type'  => 'hidden',
        		'value' => 'creapreguntaajax',
        		'id'	=> 'urlCreaPregunta',
        	),
        ));
        
        $this->add(array(
        	'name' 		=>'obtenerrespuestas',
        	'attributes'=>array(
        		'type'  => 'hidden',
        		'value' => 'obtenerrespuestasajax',
        		'id'	=> 'urlObtenerRespuestas',
        	),
        ));
        
        $this->add(array(
        	'name' 		=>'obtenertipos',
        	'attributes'=>array(
        		'type'  => 'hidden',
        		'value' => 'obtenertiposajax',
        		'id'	=> 'urlObtenerTipos',
        	),
        ));
        
        $this->add(array(
        	'name' 		=>'obtenerpreguntas',
        	'attributes'=>array(
        		'type'  => 'hidden',
        		'value' => 'obtenerpreguntasajax',
        		'id'	=> 'urlObtenerPreguntas',
        	),
        ));
        
        $this->add(array(
        	'name' 		=>'obtenerNumeracion',
        	'attributes'=>array(
        		'type'  => 'hidden',
        		'value' => 'obtenerNumeracionAjax',
        		'id'	=> 'urlObtenerNumeracion',
        	),
        ));
        
        $this->add(array(
            'name'      => $columns[0],
            'attributes'=> array(
                'type'  => 'hidden',
                'id'	=> $columns[0],
            ),
        ));
        
        $this->add( array(
            'name'          => $columns[1],
            'attributes'    => array(
                'type'      => 'hidden',
        //        'placeholder'   => 'Pregunta Parent'
                'class'     => '_objPregResp',
                'id'        => $columns[1],
                'value'     => 0
            ),
            'options'       => array()
        ));
        
        $this->add( array(
            'name'           => $columns[2],
            'type'          => 'select',
            'attributes'    => array(
                'id'        => $columns[2],
                'class'     => '_objPregResp'
            ),            
            'options'       => array(
                'empty_option' => 'Elige el tipo de pregunta')
        ));
        
        $this->add( array(
            'name'           => $columns[3],
            //'type'          => 'textarea',
            'attributes'    => array(
                'type'          => 'text',
                'placeholder'   => 'Pregunta',
                'id'        => $columns[3],
                'class'     => '_objPregResp'
            )/*,
            'options'       => array(
              //  'label'     => 'Pregunta'
                )*/
        ));
        
        $this->add( array(
            'name'           => $columns[4],
            'attributes'        => array(
                'type'          => 'hidden',
                'placeholder'   => 'Nombre Corto / Clave',
                'id'        => $columns[4],
                'class'     => '_objPregResp'
            )/*,
            'options'       => array(
             //   'label'     => 'Nombre Corto / Clave'
                )*/
        ));
        
        $this->add( array(
            'name'           => $columns[5],
            'attributes'    => array(
                'type'          => 'text',
                'placeholder'   => 'Instrucción',
                'id'        => $columns[5],
                'class'     => '_objPregResp'
            )/*,
            'options'       => array(
              //  'label'     => 'Instrucción'
                )*/
        ));
        
        $this->add( array(
            'name'           => $columns[6],
            'attributes'    => array(
                'type'          => 'hidden',
                'placeholder'   => 'Tiempo Limite',
                'id'        => $columns[6],
                'class'     => '_objPregResp'
            )/*,
            'options'       => array(
            //    'label'     => 'Tiempo Limite'
                )*/
        ));
        
        $this->add( array(
            'name'           => $columns[7],
            'attributes'    => array(
                'type'          => 'hidden',
                'placeholder'   => 'Columnas',
                'id'        => $columns[7],
                'class'     => '_objPregResp'
            )/*,
            'options'       => array(
                'label'     => 'Columnas'
                )*/
        ));
        
        $this->add( array(
            'name'  => $columns[8],
            'type'	=> 'select',
            'options'   => array(
                'empty_option' => 'Elige el tipo de Numeracion',
                //'label'                 => 'Numeración',
                //'use_hidden_element'    => false,
                //'checked_value'         => 1,
                //'unchecked_value'       => 0
            ),
            'attributes'=> array(   
                'id'        => $columns[8],
                'class'     => '_objPregResp small-12 large-12 medium-12 columns'
            )
        ));
      
        $this->add(array(
            'name'		=> $columns[9],
            'attributes'=> array(
                'type'  => 'hidden',
                'value' => date('Y/m/d g:i:s'),
                'id'        => $columns[9],
                'class'     => '_objPregResp'
            ),
        ));
        
        $this->add(array(
            'name'		=> $columns[10],
            'attributes'=> array(
                'type'	=> 'hidden',
                'value' => date('Y/m/d g:i:s'),
                'id'        => $columns[10],
                'class'     => '_objPregResp'
            ),
        ));        
        
        $this->add(array(
            'name'  => $columns[11],
            'attributes'=> array(
                'type'	=> 'hidden',
                'value' => 'A',
                'id'        => $columns[11],
                'class'     => '_objPregResp'
            ),
        ));
        /*
        $this->add(array(
            'name'  => 'etiquetas',
            'attributes'=> array(
                'type'	=> 'hidden',
                'value' => '',
                'id'    => 'etiquetas',
                'class' => '_objPregResp'
            ),
        ));
        */
        $this->add(array(
            'name' => 'almacenar',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Almacenar',
                'id' 	=> 'boton-almacenar',
//                'class' => 'ui-state-default ui-corner-all boton-gral boton-accion',
            	'class' => 'button small success radius boton-gral boton-accion'
            ),
        ));
        
        $this->add(array(
        	'name' 		=>'cancelar',
        	'attributes'=>array(
        		'type'  => 'button',
        		'value' => 'Cancelar',
        		'id'	=> 'boton-reset',
//                        'class' => 'ui-state-default ui-corner-all boton-gral',
        		'class' => 'button small alert radius'
        	),
        ));   
        
        $this->add(array(
        	'name' 		=>'preview',
        	'attributes'=>array(
        		'type'  => 'button',
        		'value' => 'Vista previa',
        		'id'	=> 'boton-preview',
//                        'class' => 'ui-state-default ui-corner-all boton-gral',
        		'class' => 'button small alert radius'
        	),
        ));  
        
    }   
    
}