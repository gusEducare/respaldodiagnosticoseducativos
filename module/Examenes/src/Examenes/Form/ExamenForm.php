<?php

namespace Examenes\Form;

use Examenes\Entity\Examen;
use Zend\Form\Form;

class ExamenForm extends Form
{
    public function __construct($name = null, $options = array()) {
        parent::__construct('Examen');
        $pregunta = new Examen();
        $columns = $pregunta->obtenerColumnasExamen();
      
        $this->add(array(
            'name'      => $columns[0],
            'attributes'=> array(
                'type'  => 'hidden',
                'id'	=> $columns[0],
            ),
        ));
        $this->add( array(
            'name'          => $columns[1],
            'type'          => 'select',
            'attributes'    => array(
                'id'        => $columns[1],
                'class'     => 'fieldExamen '
            ),
            'options'       => array(
                'disable_inarray_validator' => true,
                'empty_option' => 'Elige el tipo de examen'
            ),
                //'label' => 'Elige el tipo de examen',
                //'empty_option' => 'Elige el tipo de examen')
        ));
        
        $this->add( array(
            'name'           => $columns[2],
            'type'          => 'select',
            'attributes'    => array(
                'id'        => $columns[2],
                'class'     => 'fieldExamen '
            ),
            'options'       => array(
                'disable_inarray_validator' => true,
                //'label' => 'Elige la escala',
                'empty_option' => 'Elige la escala')
        ));
        
        $this->add( array(
            'name'           => $columns[3],
            'type'          => 'select',
            'attributes'    => array(
                'id'        => $columns[3],
                'class'     => 'fieldExamen'
            ),
            'options'       => array(
                'disable_inarray_validator' => true,
             //   'label' => 'Elige el nivel',
                'empty_option' => 'Elige el nivel')
        ));
        
        $this->add( array(
            'name'           => $columns[4],
            'attributes'    => array(
                'type'          => 'text',
                'tabindex'      => '1',
                'placeholder'   => 'Nombre de la evaluaci&oacute;n',
                //'title'         => 'Nombre del Examen',
                //'data-tooltip aria-haspopup'=> 'true',
                'id'        => $columns[4],
                'class'     => 'fieldExamen'
            )/*,
            'options'       => array(
                'label'     => 'Nombre del examen'
            )*/
        ));
        
        $this->add( array(
            'name'           => $columns[5],
            'attributes'    => array(
                'type'          => 'text',
                'tabindex'      => '1',
                'placeholder'   => 'Nombre Corto / Clave',
                'id'        => $columns[5],
                'class'     => 'fieldExamen'
            )/*,
            'options'       => array(
                'label' => 'Clave',
            )*/
        ));
        
        $this->add( array(
            'name'           => $columns[6],
            'attributes'    => array(
                'type'          => 'textarea',
                'tabindex'          => '1',
                'placeholder'   => 'Estas son las Instrucciones',
                'id'        => $columns[6],
                'class'     => 'fieldExamen'
            )/*,
            'options'       => array(
                'label' => 'Instrucciones generales',
            )*/
        ));
                
        $this->add( array(
            'name'           => $columns[7],
            'attributes'    => array(
                'type'          => 'textarea',
                'tabindex'          => '1',
                'placeholder'   => 'Descripción',
                'id'        => $columns[7],
                'class'     => 'fieldExamen'
            )/*,
            'options'       => array(
                'label' => 'Descripción',
            )*/
        ));
        
        $this->add(array(
            'name'		=> $columns[8],
            'attributes'=> array(
                'type'  => 'hidden',
                'value' => date('Y/m/d g:i:s'),
                'id'        => $columns[8],
                'class'     => 'fieldExamen'
            ),
        ));
        
        $this->add(array(
            'name'		=> $columns[9],
            'attributes'=> array(
                'type'	=> 'hidden',
                'value' => date('Y/m/d g:i:s'),
                'id'        => $columns[9],
                'class'     => 'fieldExamen'
            ),
        ));    
        
        $this->add( array(
            'name'           => $columns[10],
            'attributes'    => array(
                'type'          => 'text',
                'placeholder'   => 'Número de intentos',
                //'title'   => 'Número de intentos',
                //'data-tooltip aria-haspopup'=> 'true',
                'id'        => $columns[10],
                'class'     => 'fieldExamen'
            )/*,
            'options'       => array(
                'label' => 'Número de intentos',
            )*/
        ));
       
        $this->add(array(
            'name'  => $columns[11],
            'type'	=> 'checkbox',
           /* 'options'   => array(                
                'label'                 => 'Preguntas aleatorias',
                'use_hidden_element'    => false,
                'checked_value'         => 1,
                'unchecked_value'       => 0
            ),*/
            'attributes'=> array(   
                'id'        => $columns[11],
                'class'     => 'fieldExamen small-12 large-12 medium-12 columns'
            ),                   
        ));
        
        $this->add(array(
            'name'  => $columns[12],
            'type'	=> 'checkbox',
            'attributes'=> array(   
                'id'        => $columns[12],
                'class'     => 'fieldExamen small-12 large-12 medium-12 columns'
            ),                   
        ));
        
        $this->add(array(
            'name'  => $columns[13],
            'type'	=> 'checkbox',
            'attributes'=> array(   
                'id'        => $columns[13],
                'class'     => 'fieldExamen small-12 large-12 medium-12 columns'
            ),                   
        ));
        
        $this->add( array(
            'name'           => $columns[14],
            'attributes'    => array(
                'type'                      => 'hidden',
                //'min'                     => '0',
                //'aria-valuemax'           => '1000',
                //'step'                    => '1',
                'id'                        => $columns[14],
                'class'                     => 'fieldExamen'
            )
        ));
        
        
         $this->add(array(
            'type' => 'select',
             'name'=> $columns[15],
             'options' => array(
                 'value_options' => array(
                     'default' => 'Selecciona un limite de tiempo',
                     '0' => 'Sin limite',
                     '1' => '30 minutos',
                     '2' => '1 hora',
                     '3' => '1:30 hrs',
                     '4' => '2:00 hrs',
                     '5' => '2:30 hrs',
                     '6' => '3:00 hrs'
                 ),
             ),
             'attributes' => array(
                 'id'=> $columns[15],
                 'class'     => 'fieldExamen ',
                 'value' => ''
             )
         ));
        
        
        
        $this->add(array(
            'name'  => $columns[16],
            'attributes'=> array(
                'type'	=> 'hidden',
                'value' => 'NUEVO',
                'id'        => $columns[16],
                'class'     => 'fieldExamen'
            ),
        ));
        
        $this->add(array(
            'name' => 'almacenar',
            'attributes' => array(
                'type'  => 'button',
                'value' => 'Almacenar',
                'id' 	=> 'boton-almacenar',
//                'class' => 'ui-state-default ui-corner-all boton-gral boton-accion',
            	'class' => 'button large success radius boton-gral boton-accion'
            ),
        ));
        
        $this->add(array(
        	'name' 		=>'cancelar',
        	'attributes'=>array(
        		'type'  => 'button',
        		'value' => 'Cancelar',
        		'id'	=> 'boton-reset',
//                        'class' => 'ui-state-default ui-corner-all boton-gral',
        		'class' => 'button large alert radius'
        	),
        ));        
    }   
    
}