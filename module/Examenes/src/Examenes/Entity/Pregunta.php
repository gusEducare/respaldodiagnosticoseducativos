<?php
/**
 * Clase encargada en la creación de preguntas, con todos sus metodos y
 * atributos.
 *
 * @author jpgomez@grupoeducare.com.
 * @author oreyes@grupoeducare.com.
 * @since 02-12-2014
 */
namespace Examenes\Entity;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

//use Zend\Log\Writer\Stream;
//use Zend\Log\Logger;

/**
 * Objeto de tipo pregunta.
 * @author oreyes
 * @author jpgomez
 *
 */

class Pregunta implements InputFilterAwareInterface
{
	/**
	 * 
	 * @var int 
	 */
	protected $id_pregunta;
	
	/**
	 * 
	 * @var int
	 */
	protected $id_pregunta_parent;
	
	/**
	 * 
	 * @var int
	 */
	protected $id_tipo_pregunta;
	
	/**
	 * 
	 * @var string
	 */
	protected $pregunta;
	
	/**
	 * 
	 * @var string
	 */
	protected $nombre_corto_clave;
	
	/**
	 * 
	 * @var string
	 */
	protected $instruccion;
	
	/**
	 * 
	 * @var string
	 */
	protected $tiempo_limite;
        
	/**
	 * 
	 * @var string
	 */
	protected $columnas;
	
	/**
	 * 
	 * @var int
	 */
	protected $id_numeracion;
	
	/**
	 * 
	 * @var string
	 */
	protected $fecha_registro;
	
	/**
	 * 
	 * @var string
	 */
	protected $fecha_actualiza;
	
	/**
	 * 
	 * @var string
	 */
	protected $estatus;
        
        /**
	 *
	 * @var InputFilterInterface $inputFilter
	 */
	protected $inputFilter;
        
	//protected $_objLog;

	/**
	 * Constructor de clase crea instancia del objeto Logger.
	 *
	 */
	public function __construct()
        {
            
	}
	
	/**
	 * Funcion para traer atributos de la clase.
	 *  
	 * @param mixed $property
	 */
	public function __get($property)
	{
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	/**
	 * Funcion para asignar valores a los atributos de la clase.
	 *  
	 * @param string $property
	 * @param mixed $value
	 * @return \Application\Model\Pregunta
	 */
	public function __set($property, $value)
	{
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
		
		return $this;
	}
	
        public function obtenerColumnasPreg(){
            $_arrData = array( 
                        't11id_pregunta', 
                        't11id_pregunta_parent', 
                        'c03id_tipo_pregunta', 
                        't11pregunta', 
                        't11nombre_corto_clave', 
                        't11instruccion', 
                        't11tiempo_limite', 
                        't11columnas', 
                        'c06id_numeracion', 
                        't11fecha_registro', 
                        't11fecha_actualiza', 
                        't11estatus', 
                            );
            return $_arrData;
        }
        
	function intercambiarArray( $data = array() ) 
	{
            /*
		$this->id_pregunta          = $data['t11id_pregunta'];
		$this->id_pregunta_parent   = $data['t11id_pregunta_parent'];
		$this->id_tipo_pregunta     = $data['c03id_tipo_pregunta'];
		$this->pregunta             = $data['t11pregunta'];
		$this->nombre_corto_clave   = $data['t11nombre_corto_clave'];
		$this->instruccion          = $data['t11instruccion'];
		$this->tiempo_limite        = $data['t11tiempo_limite'];
		$this->id_numeracion        = $data['c06id_numeracion'];
		$this->fecha_registro       = date('Y/m/d g:i:s');
		$this->fecha_actualiza      = date('Y/m/d g:i:s');
		$this->estatus              = $data['t11estatus'];             
             */
            
            $columns = $this->obtenerColumnasPregunta();
            foreach ($columns as $column)
            {
                    if(!empty($data[$column]))
                    {
                            $this->__set($column, $data[$column]);
                    }else{
                            $this->__set($column, '');
                    }
            }
	}
	
	public function obtenerColumnasPregunta()
	{
		//$this->_objLog->debug("Propiedades de la clase: " . print_r( get_class_vars(get_class($this)),true));
                /*
		return array('t11id_pregunta',
					't11id_pregunta_parent',
					'c03id_tipo_pregunta',	
					't11pregunta',
					't11nombre_corto_clave',	
					't11instruccion',
					't11tiempo_limite',
					't11columnas',
					'c06id_numeracion',
					't11fecha_registro',
					't11fecha_actualiza',
					't11estatus');                
                 */
		//return get_class_vars($this);
            
            $propiedades = get_class_vars(get_class($this));
            $propertys = array_keys($propiedades);

            unset($propertys[array_search('inputFilter',$propertys)]);
            return $propertys;
	}
        
        /**
         * Funcion que setea los valores a validar, por el momento solo se valida 
         * el numero minimo y maximo
         * @param type $columns
         * @return type
         */
        protected function getValidators( $columns ){
            foreach ($columns as $index => $data){
                switch($index){
                    case 0: case 8: case 9:
                        $_arrValidators[$index] = array('min' => 0, 'max' => 0);
                    break;
                    case 4: case 7:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 1000);
                    break;
                    case 5:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 100);
                    break;
                    case 6:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 500);
                    break;
                    case 1: case 2: case 3: case 10:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 11);
                    break;
                    default :
                        $_arrValidators[$index] = array('min' => 1, 'max' => 1);                        
                }
            }
            return $_arrValidators;
        }
        
        /**
         * 
         * @return InputFilter
         */
	public function getInputFilter()
	{
            $columns = $this->obtenerColumnasPregunta();
            //$validators = $this->getValidators( $columns );
            if(!$this->inputFilter){
                $inputFilter = new InputFilter();
                $factory     = new Factory();

                $inputFilter->add($factory->createInput(array(
                                'name'      => $columns[1],
                                'required'  => false,
                                'filters'   => array(
                                                array('name' => 'StripTags'),
                                                array('name' => 'StringTrim')
                                ),
                                'validators'    => array(
                                                array(
                                                        'name'    => 'StringLength',
                                                        'options' => array(
                                                                'encoding' => 'UTF-8',
                                                                'min'      => 1,
                                                                'max'      => 11,
                                                        ),
                                                ),
                                ),
                )));
                $inputFilter->add(array(
                                'name'      => $columns[2],
                               'required'  => false,
                                'filters'   => array(
                                                array('name' => 'Int'),
                                ),/*
                                'validators'    => array(
                                                array(
                                                        'name'    => 'StringLength',
                                                        'options' => array(
                                                                        'encoding' => 'UTF-8',
                                                                        'min'      => 1,
                                                                        'max'      => 11,
                                                        ),
                                                ),
                                )*/
                ));
                $inputFilter->add($factory->createInput(array(
                                'name'  => $columns[3],
                                'required'  => false,
                                'filters'   => array(
                                                array('name' => 'StripTags'),
                                                array('name' => 'StringTrim'),
                                ),
                                'validators'    => array(
                                                array(
                                                                'name'    => 'StringLength',
                                                                'options' => array(
                                                                                'encoding' => 'UTF-8',
                                                                                'min'      => 1,
                                                                                'max'      => 1000,
                                                                ),
                                                ),
                                ),
                )));
                $inputFilter->add($factory->createInput(array(
                                'name'      => $columns[4],
                                'required'  => false,
                                'filters'   => array(
                                                array('name' => 'StripTags'),
                                                array('name' => 'StringTrim'),
                                ),
                                'validators'    => array(
                                                array(
                                                                'name'    => 'StringLength',
                                                                'options' => array(
                                                                                'encoding' => 'UTF-8',
                                                                                'min'      => 1,
                                                                                'max'      => 100,
                                                                ),
                                                ),
                                ),
                )));

                $inputFilter->add($factory->createInput(array(
                                'name'      => $columns[5],
                                'required'  => false,
                                'filters'   => array(
                                                array('name' => 'StripTags'),
                                                array('name' => 'StringTrim'),
                                ),
                                'validators'    => array(
                                                array(
                                                                'name'    => 'StringLength',
                                                                'options' => array(
                                                                                'encoding' => 'UTF-8',
                                                                                'min'      => 1,
                                                                                'max'      => 10000,
                                                                ),
                                                ),
                                ),
                )));

                $inputFilter->add($factory->createInput(array(
                                'name'      => $columns[6],
                                'required'  => false,
                                'filters'   => array(
                                                array('name' => 'StripTags'),
                                                array('name' => 'StringTrim'),
                                ),
                                'validators'    => array(
                                                array(
                                                                'name'    => 'StringLength',
                                                                'options' => array(
                                                                                'encoding' => 'UTF-8',
                                                                                'min'      => 1,
                                                                                'max'      => 5,
                                                                ),
                                                ),
                                ),
                )));

                $inputFilter->add($factory->createInput(array(
                                'name'      => $columns[7],
                                'required'  => false,
                                'filters'   => array(
                                                array('name' => 'StripTags'),
                                                array('name' => 'StringTrim'),
                                ),
                                'validators'    => array(
                                                array(
                                                                'name'    => 'StringLength',
                                                                'options' => array(
                                                                                'encoding' => 'UTF-8',
                                                                                'min'      => 1,
                                                                                'max'      => 1,
                                                                ),
                                                ),
                                ),
                )));  
                
                $inputFilter->add($factory->createInput(array(
                                'name'      => $columns[8],
                                'required'  => false,
                                'filters'   => array(
                                                array('name' => 'StripTags'),
                                                array('name' => 'StringTrim'),
                                ),
                                'validators'    => array(
                                                array(
                                                                'name'    => 'StringLength',
                                                                'options' => array(
                                                                                'encoding' => 'UTF-8',
                                                                                'min'      => 1,
                                                                                'max'      => 1,
                                                                ),
                                                ),
                                ),
                )));  
                
                $this->inputFilter = $inputFilter;
            }
            return $this->inputFilter;
        }

        public function setInputFilter(InputFilterInterface $inputFilter) {
            throw new Exception('no usado: ' . $inputFilter);
        }

}