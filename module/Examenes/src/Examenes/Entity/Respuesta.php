<?php
/**
 * Clase encargada de crear respuestas, con todos sus metodos y atributos.
 * 
 * @author oreyes@grupoeducare.com
 * @author jpgomez@grupoeducare.com
 */
namespace Examenes\Entity;


use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;


class Respuesta implements InputFilterAwareInterface
{
	
    /**
     * 
     * @var int
     */
    protected $id_respuesta;

    /**
     * 
     * @var int
     */
    protected $id_respuesta_parent;

    /**
     * 
     * @var string
     */
    protected $respuesta;

    /**
     * 
     * @var int
     */
    protected $orden;

    /**
     * 
     * @var string
     */
    protected $fecha_registro;

    /**
     * 
     * @var string
     */
    protected $fecha_actualiza;

    /**
     * 
     * @var string
     */
    protected $estatus;

    /**
     * 
     * @var string
     */
    protected $correcta;

    function __construct() 
    {

    }

    /**
     * Funcion para traer atributos de la clase.
     *  
     * @param mixed $property
     */
    public function __get($property)
    {
            if(property_exists($this,$property))
            {
                    return $this->$property;
            }
    }

    /**
     * Funcion para asignar valores a los atributos de la clase.
     *  
     * @param string $property
     * @param mixed $value
     * @return \Application\Model\Pregunta
     */
    public function __set($property, $value)
    {
            if(property_exists($this,$property))
            {
                    $this->$property = $value;
            }
            return $this;
    }

    function intercambiarArray( $data = array() ) 
    {
        /*
            $this->id_respuesta         = $data['t13id_respuesta'];
            $this->id_respuesta_parent  = $data['t13id_respuesta_parent'];
            $this->respuesta            = $data['t13respuesta'];
            $this->orden                = $data['t13orden'];
            $this->fecha_registro       = date('Y/m/d g:i:s');
            $this->fecha_actualiza      = date('Y/m/d g:i:s');
            $this->estatus              = $data['t13estatus'];
            $this->correcta             = $data['correcta']; 
        */  


        $columns = $this->obtenerColumnasRespuesta();
        foreach ($columns as $column)
        {
                if(!empty($data[$column]))
                {
                        $this->__set($column, $data[$column]);
                }else{
                        $this->__set($column, '');
                }
        }
    }

    public function obtenerColumnasResp()
    {
        return array(
                            't13id_respuesta', 
                            't13id_respuesta_parent', 
                            't13respuesta',
                            't13orden', 
                            't13fecha_registro', 
                            't13fecha_actualiza',
                            't13estatus');
    }
    
    public function obtenerColumnasRespuesta()
    {
            //$this->log->debug("Propiedades de la clase: " . print_r( get_class_vars(get_class($this)),true));
    /*	return array(
                            't13id_respuesta_parent', 
                            't13respuesta',
                            't13orden', 
                            't13fecha_registro', 
                            't13fecha_actualiza',
                            't13estatus');
            //return get_class_vars($this);
      */      

        $propiedades = get_class_vars(get_class($this));
        $propertys = array_keys($propiedades);

        unset($propertys[array_search('inputFilter',$propertys)]);
        return $propertys;
    }

    /**
     * 
     * @return InputFilter
     */
    public function getInputFilter()
    {
        $columns = $this->obtenerColumnasRespuesta();

        if(!$this->inputFilter)
        {
            $inputFilter = new InputFilter();
            $factory     = new Factory();

            $inputFilter->add($factory->createInput(array(
                            'name'      => $columns[1],
                            'required'  => true,
                            'filters'   => array(
                                            array('name' => 'StripTags'),
                                            array('name' => 'StringTrim')
                            ),
                            'validators'    => array(
                                            array(
                                                    'name'    => 'StringLength',
                                                    'options' => array(
                                                            'encoding' => 'UTF-8',
                                                            'min'      => 1,
                                                            'max'      => 11,
                                                    ),
                                            ),
                            ),
            )));
            $inputFilter->add($factory->createInput(array(
                            'name'  => $columns[2],
                            'required'  => true,
                            'filters'   => array(
                                            array('name' => 'StripTags'),
                                            array('name' => 'StringTrim'),
                            ),
                            'validators'    => array(
                                            array(
                                                            'name'    => 'StringLength',
                                                            'options' => array(
                                                                            'encoding' => 'UTF-8',
                                                                            'min'      => 1,
                                                                            'max'      => 1000,
                                                            ),
                                            ),
                            ),
            )));
            $inputFilter->add($factory->createInput(array(
                            'name'      => $columns[3],
                            'required'  => true,
                            'filters'   => array(
                                            array('name' => 'StripTags'),
                                            array('name' => 'StringTrim'),
                            ),
                            'validators'    => array(
                                            array(
                                                            'name'    => 'StringLength',
                                                            'options' => array(
                                                                            'encoding' => 'UTF-8',
                                                                            'min'      => 1,
                                                                            'max'      => 3,
                                                            ),
                                            ),
                            ),
            )));

            $inputFilter->add($factory->createInput(array(
                            'name'      => $columns[7],
                            'required'  => true,
                            'filters'   => array(
                                            array('name' => 'StripTags'),
                                            array('name' => 'StringTrim'),
                            ),
                            'validators'    => array(
                                            array(
                                                            'name'    => 'StringLength',
                                                            'options' => array(
                                                                            'encoding' => 'UTF-8',
                                                                            'min'      => 1,
                                                                            'max'      => 1,
                                                            ),
                                            ),
                            ),
            )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new Exception('no usado: ' . $inputFilter);
    }

}
