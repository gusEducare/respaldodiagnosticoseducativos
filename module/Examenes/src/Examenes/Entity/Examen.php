<?php
/**
 * Clase encargada en la creación de examenes, con todos sus metodos y
 * atributos.
 *
 * @author jpgomez@grupoeducare.com.
 * @author oreyes@grupoeducare.com.
 * @since 21-01-2015
 */
namespace Examenes\Entity;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

//use Zend\Log\Writer\Stream;
//use Zend\Log\Logger;

/**
 * Objeto de tipo examen.
 * @author oreyes
 * @author jpgomez
 *
 */

class Examen implements InputFilterAwareInterface
{
	/**
	 * 
	 * @var int 
	 */
	protected $id_examen;
	
	/**
	 * 
	 * @var int
	 */
	protected $id_tipo_examen;
	
	/**
	 * 
	 * @var int
	 */
	protected $id_escala;
        
	/**
	 * 
	 * @var int
	 */
	protected $id_nivel;
	
	/**
	 * 
	 * @var string
	 */
	protected $nombre;
	
	/**
	 * 
	 * @var string
	 */
	protected $nombre_corto_clave;
	
	/**
	 * 
	 * @var string
	 */
	protected $instrucciones;
	
	/**
	 * 
	 * @var string
	 */
	protected $descripcion;
	
	/**
	 * 
	 * @var string
	 */
	protected $fecha_registro;
	
	/**
	 * 
	 * @var string
	 */
	protected $fecha_actualiza;
	
	/**
	 * 
	 * @var int
	 */
	protected $num_intentos;
	
	/**
	 * 
	 * @var int
	 */
	protected $aleatorio_preguntas;
	
	/**
	 * 
	 * @var int
	 */
	protected $aleatorio_respuestas;
	
	/**
	 * 
	 * @var int
	 */
	protected $demo;
	
	/**
	 * 
	 * @var int
	 */
	protected $minimo_aprobatorio;	
	
	/**
	 * 
	 * @var string
	 */
	protected $tiempo;
	
	/**
	 * 
	 * @var string
	 */
	protected $estatus;
           
	/**
	 *
	 * @var InputFilterInterface $inputFilter
	 */
	protected $inputFilter;
        
	//protected $_objLog;

	/**
	 * Constructor de clase 
	 *
	 */
	public function __construct()
        {
            
	}
	
	/**
	 * Funcion para traer atributos de la clase.
	 *  
	 * @param mixed $property
	 */
	public function __get($property)
	{
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	/**
	 * Funcion para asignar valores a los atributos de la clase.
	 *  
	 * @param string $property
	 * @param mixed $value
	 * @return \Application\Model\Pregunta
	 */
	public function __set($property, $value)
	{
            if (property_exists($this, $property)) {
                    $this->$property = $value;
            }

            return $this;
	}
	
	function intercambiarArray( $data = array() ) 
	{            
            $columns = $this->obtenerColumnasExamen();
            foreach ($columns as $column)
            {
                if(!empty($data[$column]))
                {
                        $this->__set($column, $data[$column]);
                }else{
                        $this->__set($column, '');
                }
            }
	}
	
	public function obtenerColumnasExamen()
	{            
            $propiedades = get_class_vars(get_class($this));
            $propertys = array_keys($propiedades);

            unset($propertys[array_search('inputFilter',$propertys)]);
            return $propertys;
	}
        
        /**
         * Funcion que setea los valores a validar, por el momento solo se valida 
         * el numero minimo y maximo
         * @param type $columns
         * @return type
         */
        protected function getValidators( $columns ){
            foreach ($columns as $index => $data){
                switch($index){
                    // Dejo en 0 el id y las fechas ya que estos se registran autmaticamente
                    case 8: case 9: 
                        $_arrValidators[$index] = array('min' => 0, 'max' => 0);
                    break;
                    case 4: case 7:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 1000);
                    break;
                    case 5:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 100);
                    break;
                    case 6:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 500);
                    break;
                    case 14:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 4);
                    break;                    
                    case 0: case 1: case 2: case 3: case 10: case 15: case 16:
                        $_arrValidators[$index] = array('min' => 1, 'max' => 11);
                    break;                     
                    default :
                        $_arrValidators[$index] = array('min' => 1, 'max' => 1);                        
                }
            }
            return $_arrValidators;
        }
        
        /**
         * 
         * @return InputFilter
         */
	public function getInputFilter()
	{
            $columns = $this->obtenerColumnasExamen();
            $validators = $this->getValidators( $columns );
            if(!$this->inputFilter){
                $inputFilter = new InputFilter();
                $factory     = new Factory();

                foreach ($columns as $id => $data){
                    if( $validators[$id]['min'] ){
                        $inputFilter->add($factory->createInput(array(
                                        'name'      => $data,
                                        'required'  => false,
                                        'filters'   => array(
                                                        array('name' => 'StripTags'),
                                                        array('name' => 'StringTrim')
                                        ),
                                        'validators'    => array(
                                                        array(
                                                                'name'    => 'StringLength',
                                                                'options' => array(
                                                                        'encoding' => 'UTF-8',
                                                                        'min'      => $validators[$id]['min'],
                                                                        'max'      => $validators[$id]['max'],
                                                                ),
                                                        ),
                                        ),
                        )));
                    }
                }
                if ($inputFilter->isValid())
            {
                echo "formulario validado";
            }
            else
            {
                foreach ($inputFilter->getInvalidInput() as $error)
                {
                    print_r($error->getMessages());
                }
            }
                $this->inputFilter = $inputFilter;
        }
            return $this->inputFilter;
        }

        public function setInputFilter(InputFilterInterface $inputFilter) {
            throw new Exception('no usado: ' . $inputFilter);
        }
}