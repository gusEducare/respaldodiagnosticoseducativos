<?php
/**
 *
 * @author oreyes
 * @author jpgomez
 * @since 02/12/2014
 * @version  Examenes1.0
 *
 */


return array(
    'controllers' => array(
            'invokables' => array(
                    'examenes' => 'Examenes\Controller\ExamenesController',
                    'preguntas' => 'Examenes\Controller\PreguntasController',
                    'general' => 'Examenes\Controller\GeneralController'
            )
    ),		
    'router' => array(
        'routes' => array(
            'examenes' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'         => '/examenes[/[:action]][/:id]',
                    'constraints'   => array(
                        'controller'=> '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'    =>  '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        =>  '[0-9]*',
                    ),
                    'defaults'          => array(
                        'controller'    => 'examenes',
                        'action'        => 'index',
                    ),
                ),
            ),
            
            'preguntas' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'         => '/preguntas[/[:action]][/:id]',
                    'defaults'          => array(
                        'controller'    => 'preguntas',
                        'action'        => 'index',
                    ),
                ),
            ), 
              'general' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'         => '/general[/:id]',
                    'defaults'          => array(
                        'controller'    => 'general',
                        'action'        => 'index',
                    ),
                ),
            ),                       
             
        ),
    ),

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    
    

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
   //         'examenes/preguntas/index' => __DIR__ . '/../view/examenes/pregunta.phtml',
  //          'examenes/preguntas/preguntas' => __DIR__ . '/../view/examenes/preguntas/preguntas.phtml',
 //           'examenes/preguntas/pregunta' => __DIR__ . '/../view/examenes/preguntas/pregunta.phtml',
            'examenes/preguntas/ejemplodragdrop'=>__DIR__.'/../view/examenes/preguntas/ejemplodragdrop.phtml',
            'examenes/preguntas/preguntamultiple'=>__DIR__.'/../view/examenes/preguntas/preguntaOpcionMultiple.phtml',
            'examenes/preguntas/muestrapregunta'=>__DIR__.'/../view/examenes/preguntas/muestraPregunta.phtml',

            'examenes/examenes/plantilla'=>__DIR__.'/../view/examenes/examenes/plantilla.phtml',
            'examenes/examenes/iniciarexamen'=>__DIR__.'/../view/examenes/examenes/iniciarExamen.phtml',
            'examenes/examenes/crearexamen'=>__DIR__.'/../view/examenes/constructor/crearexamen.phtml',
            'examenes/examenes/edit'=>__DIR__.'/../view/examenes/examenes/edit.phtml'

        ),
        'template_path_stack' => array(
            'examenes' => __DIR__ . '/../view',
            'preguntas' => __DIR__ . '/../view',
            'general' => __DIR__ .'/../view',
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);