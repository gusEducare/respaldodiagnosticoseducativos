<?php
namespace Examenes;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Examenes\Model\RespuestaModel;
use Examenes\Model\PreguntaModel;
//use Examenes\Model\RespuestaModel;
use Examenes\Model\PreguntaTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Examenes\Model\ExamenModel;
use Examenes\Model\ExamenTable;
use Examenes\Model\Examen;
use Examenes\Model\Pregunta;
use Examenes\Model\General;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

use Zend\Mvc\MvcEvent;
use Zend\Session\Container;

class Module 
{
    
    public function getconfig()
    {
            return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                //__DIR__ .'/autoload_classmap.php',
                
            ),                
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
   
    public function getServiceConfig()
    {
    	return array(
            'factories'=>array(                    
                'Examenes\Model\PreguntaTable' =>  function($sm) {
                    $tableGateway = $sm->get('PreguntaTableGateway');
                    $table = new PreguntaTable($tableGateway);
                    return $table;
                },      
                'PreguntaTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Pregunta());
                    return new TableGateway('t11pregunta', $dbAdapter, null, $resultSetPrototype);
                },                     
                'Examenes\Model\ExamenTable' =>  function($sm) {
                    $tableGateway = $sm->get('ExamenTableGateway');
                    $table = new ExamenTable($tableGateway);
                    return $table;
                },      
                'ExamenTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Examen());
                    return new TableGateway('t12examen', $dbAdapter, null, $resultSetPrototype);
                },  
                'Examenes/Model/PreguntaModel'=> function($sm){
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $config = $sm->get('Config');
                    $pregunta  = new PreguntaModel($dbAdapter,$config);
                    return $pregunta; 
                },

                'Examenes/Model/RespuestaModel'=>function($sm){
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $respuesta = new RespuestaModel($dbAdapter);
                    return $respuesta;
                },              
                'Examenes\Model\ExamenAccithia' =>  function($sm) {
                    $config = $sm->get('config');
                    $dbAdapter = new \Zend\Db\Adapter\Adapter($config['db2']);                    
                    $table = new ExamenModel($dbAdapter);
                    return $table;
                },		
                		
                'Examenes/Model/ExamenModel'=>function($sm){
                    $config = $sm->get('config');
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $respuesta = new ExamenModel($dbAdapter,$config);
                    return $respuesta;
                                       
                },
                'Examenes/Model/General'=>function($sm){
                	$config = $sm->get('config');
                	$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                	$respuesta = new General($dbAdapter,$config);
                	return $respuesta;
                
                
                }
            )
        );
    }
}
