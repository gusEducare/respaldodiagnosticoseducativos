<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Application\View\Helper\getUsuarioHelper;
use Application\View\Helper\getMenuHelper;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Application\Model\Webservice;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Db\Sql\Sql as sql;
use Zend\Db\ResultSet\ResultSet;
	

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class Module
{
        
    public function onBootstrap(MvcEvent $e)
    {
        $this -> initAcl($e);
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
    
    public function getRolesBd(MvcEvent $e){          
        $dbAdapter = $e->getApplication()->getServiceManager()->get('Zend\Db\Adapter\Adapter');
        $results = $dbAdapter->query('SELECT t06.t06id_menu_perfil, c01.c01id_perfil, c01.c01nombre, t06.c07id_vista, c07.c07url_controlador, c07.c07url_accion
                                    FROM evaluaciones.t06menu_perfil t06
                                    join evaluaciones.c01perfil c01 on t06.c01id_perfil = c01.c01id_perfil
                                    join evaluaciones.c07vista c07 on t06.c07id_vista = c07.c07id_vista
                                    where t06.t06estatus = 1
                                    order by c01.c01id_perfil asc, c07.c07url_controlador desc;');
	
	$results = $results->execute();
        
        $resultSet = new ResultSet;
        $resultSet->initialize($results);
			
        $_arrResultado = $resultSet->toArray();
        $_arrPermisos = array();
        $_arrAcion = array();

        $_strControlador = '';
        foreach($_arrResultado as $permiso){
            if($_strControlador != $permiso['c07url_controlador']){
                $_strControlador = $permiso['c07url_controlador'];
                $_intCont = 0;
            }
            $_arrPermisos[$permiso['c01id_perfil']][$_strControlador][$_intCont]= $permiso['c07url_accion'];
            $_intCont++;
        }

        return $_arrPermisos;
    }
    
      public function initAcl(MvcEvent $e) {
        //$config = include __DIR__ . '/../../config/autoload/global.php';
        $_arrPermisos = $this->getRolesBd($e);
        $acl = new Acl();
        foreach ($_arrPermisos as $role => $resources) {
            //Crea roles
            $role = new Role($role);
            $acl -> addRole($role);

            foreach ($resources as $resource => $acciones) {
                $_intTot = count($acciones);
                foreach ($acciones as $accion){
                    //agrega resources
                    if(!$acl ->hasResource($resource)){
                        $acl -> addResource(new Resource($resource));
                    }
                    //agrega permisos
                    if($_intTot > 1){
                       $acl -> allow($role, $resource,$accion); 
                    }else{
                        $acl -> allow($role, $resource);
                    }
                }
            }
        }        
        $e -> getViewModel() -> acl = $acl;
           
    }
    
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig(){
        return array(   
            'factories' => array(         
                'sql' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    //$resultSetPrototype = new ResultSet();
                    return new Sql($dbAdapter);
                },
                'Webservice' => function ($sm) {
                    $_arrIPConsultoras	= array(
                                    '127.0.0.1',
                                    '205.186.133.250',
                                    '192.168.2.94'
                    );
                    return new Webservice($sm->get('Logger'), $_arrIPConsultoras, $sm->get('sql'));
                }, 
                'Logger' => function (){
                        $_objLogger = new Logger();
                        $_objLogger->addWriter(new Stream('php://stderr'));
                        return $_objLogger;
                }
            )
        );
    }


    public function getViewHelperConfig()
    {
    	return array(
                'factories' => array(
    		// the array key here is the name you will call the view helper by in your view scripts
                    'getUsuarioHelper' => function($sm) {
    					//$locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
    					return new getUsuarioHelper();
    					},                                                
                    'getMenuHelper' => function($sm) {
    					$locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
    					return new getMenuHelper($locator->get('sql'));
    					},                                                                                           
    			),
    	);
    }
}
