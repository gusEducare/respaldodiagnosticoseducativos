<?php
/**
 * 
 * La clase menu se encargará de obtener todas las opciones que puede visualizar 
 * cada perfil de usuarios
 * @since 10-03-2015 
 * @author jpgomez@grupoeducare.com
 */
namespace Application\Model;
//use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;

class Menu{
    
        /**
         * Instancia de SQL
         * @var type 
         */
	protected $sql;
	
	public function __construct(Sql $sql){
		$this->sql= $sql;		
	}
	
	/**
         * Funcion que se encarga de obtener las acciones que puede realizar 
         * cada uno de los perfiles
         * @param type $_intPerfil
         * @return type
         */
	public function crearMenu( $_intPerfil ){		
            $_intIdPerfil = $_intPerfil === null ? 0 : $_intPerfil;
            $select = $this->sql->select();
            $select	->from( array('c07' => 'c07vista'))
                            ->join( array('t06' => 't06menu_perfil'),                                                                                    
                                        't06.c07id_vista = c07.c07id_vista',
                                    array('t06id_menu_perfil'))				
                            ->where('t06.c01id_perfil ='.$_intIdPerfil )
                            ->where('c07.c07menu_usuario = 1' )
                            ->where('t06.t06estatus = 1' )
                            ->columns(array('c07descripcion', 'c07url_accion', 'c07texto', 'c07url_controlador'));

            $statement = $this->sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();

            $resultSet = new ResultSet;
            $resultSet->initialize($result);

            $_arrRespuesta = $resultSet->toArray();

            return $_arrRespuesta;
		
	}
}