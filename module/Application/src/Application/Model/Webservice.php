<?php

/* 
 * Modelo de Webservice para implementacion de la logica en las operaciones del WS
 * @author jpgomez
 * @copyright Grupo Educare. Derechos Reservados
 */

namespace Application\Model;

/**
 * Clase Webservice
 * @author Juan Pablo Gomez<jpgomez@grupoeducare.com>
 */
class Webservice {
    
    protected $sql;
    
    protected $adapter;
    
    protected $objLogger;
    
    protected $arrIPConsultoras;
    
    /**
     * Constructor de la clase. inicializa variables.
     * @param Logger $logger
     * @param array $_arrIPConsultoras
     * @param Adapter $adapter
     */
    public function __construct($logger, $_arrIPConsultoras,  $adapter){
            $this->objLogger           = $logger;
            $this->adapter              = $adapter;
            $this->arrIPConsultoras    = $_arrIPConsultoras; 
            $this->objLogger->debug('IPs: ' . print_r($this->arrIPConsultoras, true) );
    }
    
    /**
     * Obtiene la IP origen de la peticion y la compara con el arreglo de IP's permitidas.
     *
     * @return boolean
     * @access privado para no publicar como operacion en el WS.
     */
    private function permitirAccesoIP(){
            $_bolRegresa = false;

            if($_SERVER['HTTP_X_FORWARDED_FOR'] != '' ){
                    $_strIPCliente = ( !empty($_SERVER['REMOTE_ADDR']) ) ?	$_SERVER['REMOTE_ADDR']	: ( (!empty($_ENV['REMOTE_ADDR'])) ? $_ENV['REMOTE_ADDR']:"unknown" );

                    // los proxys van añadiendo al final de esta cabecera
                    // las direcciones ip que van "ocultando". Para localizar la ip real
                    // del usuario se comienza a mirar por el principio hasta encontrar
                    // una dirección ip que no sea del rango privado. En caso de no
                    // encontrarse ninguna se toma como valor el REMOTE_ADDR

                    $entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);

                    reset($entries);
                    while (list(, $entry) = each($entries)){
                            $entry = trim($entry);
                            if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) ){
                                    $_arrIPsPrivadas = array(
                                                    '/^0\./',
                                                    '/^127\.0\.0\.1/',
                                                    '/^192\.168\..*/',
                                                    '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
                                                    '/^10\..*/');

                                    $found_ip = preg_replace($_arrIPsPrivadas, $_strIPCliente, $ip_list[1]);

                                    if ($_strIPCliente != $found_ip){
                                            $_strIPCliente = $found_ip;
                                            break;
                                    }
                            }
                    }
            }
            else{
                    $_strIPCliente = ( !empty($_SERVER['REMOTE_ADDR']) ) ?	$_SERVER['REMOTE_ADDR']	: ( (!empty($_ENV['REMOTE_ADDR']))?$_ENV['REMOTE_ADDR']:"unknown" );
            }

            foreach($this->arrIPConsultoras as &$ip){
                    if($ip == $_strIPCliente){
                            $_bolRegresa = true;
                            break;
                    }
            }
            //$this->_objLogger->debug("----------------------".$_strIPCliente."----------". $_bolRegresa. "-------". print_r($_APPLICATION_ENV,true));
            //return $_bolRegresa;
            return true;
    }    
}

