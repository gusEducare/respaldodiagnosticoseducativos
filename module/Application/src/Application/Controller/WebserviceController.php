<?php

namespace Application\Controller;

//use Zend\Console\Response;
use Zend\Mvc\Controller\AbstractActionController;
//use Application\Model\Webservice;
//use Zend\View\Model\ViewModel;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Server;

class WebserviceController extends AbstractActionController{
	
	public function paqueteAction(){
		$sm = $this->getServiceLocator();
		$_objLogger = $sm->get('Logger');
		$webservice = $sm->get('Webservice');
		$_strWsdlUrl = sprintf('http://%s/paquete', $_SERVER['HTTP_HOST']);
				
		// Si en la url esta presente la entrada ?wsdl
		if (strtolower($_SERVER['QUERY_STRING']) == "wsdl") {
			// Se incluye la clase AutoDiscover que es la encargada de generar en forma automática el WSDL
			$wsdl = new AutoDiscover();
			$wsdl->setClass('Application\Model\Webservice');
			$wsdl->setUri($_strWsdlUrl);
			$_respuestaXML =  $wsdl->toXml();
		}else {
			$server	= new Server($_strWsdlUrl."?wsdl");
			$server->setObject($webservice);
			$server->setReturnResponse(true);
			$_respuestaXML = $server->handle();
		}
		//$_objLogger->debug("---------------------------------------------------------------respuesta XML ". $_respuestaXML);
		
		$response = $this->getResponse();
		$response->setContent($_respuestaXML);
		return $response;
	}
	

		
}