<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Login\Form\LoginForm;
use Login\Form\RegistroForm;
use Login\Entity\Usuario;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

class IndexController extends AbstractActionController
{
	
    /**
     * 
     * @var Container.
     */
    protected $datos_sesion;

    public function __construct()
    {
        $this->log = new Logger();
        $this->log->addWriter(new Stream("php://stderr"));		
    }
	
    public function indexAction()
    {	
    	$config   = $this->getServiceLocator()->get('config');
    	$arrMsgs  = $config['constantes'];  	    	
        $form 	  = new RegistroForm();
        $usuario  = new Usuario();
        $formLogin = new LoginForm();      
        $this->datos_sesion = new Container('user');
        
    	if (isset($_REQUEST['logout'])) {
            $this->datos_sesion->getManager()->getStorage()->clear();
    	}
    	
        $this->log->debug( 'Sesion activa: ' . $this->datos_sesion->sesionActiva);
        
        // Redirecciono a la pagina de inicio del perfil cuando hay una sesion activa
        if($this->datos_sesion->sesionActiva){ 
            $arrRoute = $this->redireccionarPerfil();
            //$this->flashMessenger()->addMessage('El usuario se registr');
            return $this->redirect()->toRoute($arrRoute[0],$arrRoute[1]);
        }
        
        $view= new ViewModel(
                array(
                    'arrMsgs'  => $arrMsgs,
                    'form'     => $form,
                    'formLogin'=> $formLogin,
                    'usuario'  => $usuario,
                    'config'   => $config)
                );
        
        $view->setTerminal(true);
       	return $view;
    }
    
    
    /**
     * Funcion para buscar el perfil y hacer la redireccion a dicho perfil.
     * 
     * @access public.
     * @author oreyes
     * @since 12/01/2015
     * 
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|\Login\Controller\ViewModel
     */
    public function redireccionarPerfil()
    {
    	$routerPerfil = array("controller"=>"Index", "action"=>"index");
    	$config 	  = $this->getServiceLocator()->get('Config');    	
    	$perfilAlum = $config['constantes']['ID_PERFIL_ALUMNO'];
    	$perfilProf = $config['constantes']['ID_PERFIL_PROFESOR'];
        $perfilConstr = $config['constantes']['ID_PERFIL_CONSTRUCTOR'];
    	$arrRoute   = array();
    	
    	//REDIRECCIONAR DEPENDIENDO EL PERFIL QUE TENGA EL USUARIO.
    	switch($this->datos_sesion->perfil)
    	{
            case $perfilProf:
                $perfil 	  = 'grupos';
                $routerPerfil = array("controller"=>"grupos", "action"=>"index");
            break;
            case $perfilAlum:
                $perfil		  = 'examenes';
                $routerPerfil = array("controller"=>"examenes" , "action"=>"misexamenes");
            break;
        case $perfilConstr:
                    $perfil 	  = 'examenes';
                    $routerPerfil = array("controller"=>"examenes", "action"=>"index");
            break;
            default:

            break;
    	}
    	array_push($arrRoute, $perfil, $routerPerfil);
    	return $arrRoute;
    }
}
