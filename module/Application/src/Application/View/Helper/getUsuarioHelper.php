<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;

class getUsuarioHelper extends AbstractHelper
{
    /**
    * Instancia de la clase Container para el manejo de datos de sesion.
    * @var Container $datos_sesion.
    */
    protected $datos_sesion;
    
    public function __construct()
    {
        $this->datos_sesion = new Container('user');
    }

    public function __invoke()
    {
        if($this->datos_sesion->perfil){    
            echo '<p class="text-right">Bienvenido: <span class="usuario">' .     
                    $this->datos_sesion->nombreUsuario
                .'</span></p>';
            echo "<p class='text-right'><a class='logout' href='".
                    $this->getView()->url('home').
                "?logout'>Salir</a></p>";
        }
    }
}
