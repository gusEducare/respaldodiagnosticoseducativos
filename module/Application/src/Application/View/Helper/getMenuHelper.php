<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;
use Application\Model\Menu;

class getMenuHelper extends AbstractHelper
{
    /**
    * Instancia de la clase Container para el manejo de datos de sesion.
    * @var Container $datos_sesion.
    */
    protected $datos_sesion;
    
    /**
     * Instancia de la clase Adapter
     * @var type 
     */
    protected $sql;
    
    public function __construct($sql){
        $this->datos_sesion = new Container('user');
        $this->sql =$sql;
    }
    
    public function __invoke()
    {    	
        //$menu = new RegistroModel($this->adapter);
        $objMenu = new Menu($this->sql);
    	//echo 'Perfil: ' . $this->datos_sesion->perfil;
        $_arrMenu = $objMenu->crearMenu($this->datos_sesion->perfil);
        echo '
            <div class="large-12 columns">
                <div class="row">
                    <nav class="top-bar large-12 columns" data-topbar role="navigation" tag="info-nav">
                        <ul class="title-area">
                            <li class="name">
                                <h1 class="active"><a href="#"></a></h1>
                            </li>
                            <li class="toggle-topbar menu-icon">
                                <a href="#"><span>Menu</span></a>
                            </li>
                        </ul>
                        <section class="top-bar-section">                         
                            <ul class="left">
                                ';
        //obtener datos del menu a redireccionar
        if(isset( $_SERVER['REDIRECT_URL'])){
            $_menu_activo = substr($_SERVER['REDIRECT_URL'],1);
        }
        foreach ($_arrMenu as $data){
             $_strEstilo = '';
            if(isset($_menu_activo)){
                if($_menu_activo == $data['c07url_controlador'] || preg_match("/".$data['c07url_controlador']."/", $_menu_activo)){
                    $_strEstilo =  "class='active'";
                }
             
                            // Se elimina la i al final del patron para que force a que sea sensible a minusculas y mayusculas
                            // Ya que se da el caso en que distribucion aparece se activa tanto en personal como en personalDistribucion
                }
            echo '<li '.$_strEstilo.'> <a href="';
            if($data['c07url_controlador']){
               echo $this->view->url($data['c07url_controlador'],
                        array(
                            'controller'=>$data['c07url_controlador'],
                            'action'=>$data['c07url_accion']
                        ));                  
            }else{
               echo $this->view->url('application',
                        array(
                            'controller'=>'application',
                            'action'=>$data['c07url_accion']
                        ));                  
            }
            echo '" alt="'.
                    $data['c07descripcion'].
                    '">'.
                    $data['c07texto'].
                    '</a></li>';
        }                                
            echo '                </ul> 
                        </section>
                    </nav>
                </div>
            </div>
            <div class="spacer60 row"></div>';
    }
}
