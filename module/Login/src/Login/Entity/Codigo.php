<?php
namespace Login\Entity;

use Zend\InputFilter\Factory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Objeto Codigo para manipulacion de codigos de grupo y licencias.
 * 
 * @copyright Grupo Educare SA de CV
 * @since 30/12/2014
 * @author oreyes
 *
 */
class Codigo implements InputFilterAwareInterface
{
	/**
	 * 
	 * @var int $id_codigo 
	 */
	var $t07id_codigo;
	/**
	 * 
	 * @var String $codigo
	 */
	var $t07codigo;
	/**
	 * 
	 * @var String $tipo
	 */
	var $t07tipo;
	/**
	 * 
	 * @var String $ultima_actualizacion
	 */
	var $t07ultima_actualizacion;
	/**
	 * 
	 * @var String estatus
	 */
	var $t07estatus;
	/**
	 * 
	 * @var InputFilterInterface $inputFilter
	 */
	var $inputFilter;
	
	/**
	 * Constructor del objeto.
	 * 
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 * 
	 */
	public function __construct()
	{
		
	}
	
	/**
	 * Funcion para llamar una propiedad de la clase
	 * y extraer su valor.
	 * 
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 * @param mixed $_property
	 * 
	 * return property $property.
	 */
	public function __get($_property)
	{	   
		if(property_exists($this, $_property))
		{
			return $this->$_property;	
		}
	}
	
	/**
	 * Funcion para asignar valores a una propiedad de la clase.
	 * 
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 * @param mixed $_property
	 * @param mixed $value
	 * 
	 */
	public function __set($_property, $value)
	{
		if(property_exists($this,$_property))
		{
			$this->$_property = $value;
		}
		return $this;
	}
	
	public function intercambiarArray($data = array())
	{
		
	}
	
	/**
	 * Funcion para extraer todas las propiedades del objeto.
	 * 
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 * 
	 * @return array $propertys
	 */
	public function obtenerColumnasCodigo( $objeto )
	{
		$propertys = array();
		$propertys = get_class_vars(get_class( $objeto ));
		$propertys = array_keys($propertys);
		
		unset($propertys[array_search('inputFilter',$propertys)]);
		
		return $propertys;
	}
	
	public function getInputFilter()
	{
		if(!$this->inputFilter)
		{
			$_inputFilter = new InputFilter();
			$factory	  = new Factory();
			
			$_inputFilter->add($factory->createInput(array(
					'name'		 => 'codigogrupo',
					'required'	 => true,
					'filters'	 => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' =>	array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 6,
									),
							),
					),
			)));

			$_inputFilter->add($factory->createInput(array(
					'name'=>'licenciaexamen',
					'required'	=> true,
					'filters'	=> array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'=> array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 6,
									),
							),
					),
			)));

		$this->inputFilter = $_inputFilter;
		}
		return $this->inputFilter;
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter) 
	{
		throw new Exception('No usado');
	}
	
}