<?php
namespace Login\Entity;

interface UsuarioInterface
{
	/**
	 * Get t01id_usuario.
	 *
	 * @return string
	 */
	public function getT01idUsuario();
	
	/**
	 * Set t01id_usuario.
	 *
	 * @param string $t01id_usuario
	 * @return UserInterface
	 */
	public function setT01idUsuario($t01id_usuario);
	
	
	/**
	 * Get email.
	 *
	 * @return string
	 */
	public function getT01correo();
	
	/**
	 * Set email.
	 *
	 * @param string $email
	 * @return UserInterface
	 */
	public function setT01correo($t01correo);
	
	
	/**
	 * Get displayName.
	 *
	 * @return string
	 */
	public function getT01nombre();
	
	/**
	 * Set displayName.
	 *
	 * @param string $displayName
	 * @return UserInterface
	 */
	public function setT01nombre($t01nombre);
	
}