<?php
namespace Login\Entity;

use Login\Entity\UsuarioInterface;

/**
 * Entidad Usuario para encapsular 
 * informacion y propiedades del Usuario.
 * @author oreyes
 *
 */
class Usuario implements UsuarioInterface{
	/**
	 *
	 * @var int
	 */
	protected $t01id_usuario;
	/**
	 *
	 * @var String
	 */
	protected $t01nombre;
	/**
	 *
	 * @var String
	 */
	protected $t01apellidos;
	/**
	 *
	 * @var String
	 */
	protected $t01correo;
	/**
	 *
	 * @var String
	 */
	protected $t01contrasena;
	/**
	 *
	 * @var int
	 */
	protected $t01bloqueada;
	/**
	 *
	 * @var String
	 */
	protected $t01tiempo_bloqueo;
	/**
	 *
	 * @var String
	 */
	protected $t01fecha_registro;
	/**
	 *
	 * @var String
	 */
	protected $t01fecha_actualiza;
	/**
	 *
	 * @var int
	 */
	protected $t01demo;
	/**
	 *
	 * @var int
	 */
	protected $t01acepto_condiciones;
	/**
	 *
	 * @var String
	 */
	protected $t01provider_id;
	/**
	 *
	 * @var String
	 */
	protected $t01provider;
	/**
	 *
	 * @var String
	 */
	protected $t01estatus;
	
	
	/**
	 * Constructor del objeto.
	 *
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 *
	 */
	public function __construct()
	{

	}
	
	
	/**
	 * Get t01id_usuario.
	 *
	 * @return string
	 */
	public function getT01idUsuario()
	{
		return $this->t01id_usuario;
	}
	
	/**
	 * Set t01id_usuario.
	 *
	 * @param string $t01id_usuario
	 * @return UserInterface
	*/
	public function setT01idUsuario($t01id_usuario)
	{
		$this->t01id_usuario = $t01id_usuario;
		return $this;
	}
	
	
	/**
	 * Get email.
	 *
	 * @return string
	*/
	public function getT01correo()
	{
		return $this->t01correo;
	}
	
	/**
	 * Set email.
	 *
	 * @param string $t01correo
	 * @return UserInterface
	*/
	public function setT01correo($t01correo)
	{
		$this->t01correo = $t01correo;
		return $this;
	}
	
	
	/**
	 * Get displayName.
	 *
	 * @return string
	*/
	public function getT01nombre()
	{
		return $this->t01nombre;
	}
	
	/**
	 * Set displayName.
	 *
	 * @param string $t01nombre
	 * @return UserInterface
	*/
	public function setT01nombre($t01nombre)
	{
		$this->t01nombre = $t01nombre;
		return $this;
	}
	
	/**
	 * sett01correo
	 * 
	 * @param String $t01contrasena
	 * @return \Login\Entity\Usuario
	 */
	public function setT01contrasena($t01contrasena)
	{
		$this->t01contrasena = $t01contrasena;
		return $this;
	}
	
	/**
	 * gett01correo
	 *
	 *
	 * @return $t01contrasena
	 */
	public function getT01contrasena()
	{
		return $this->t01contrasena;
	}
	
	/**
	 * Funcion para llamar una propiedad de la clase
	 * y extraer su valor.
	 *
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 * @param mixed $property
	 *
	 * return property $property.
	 */
	public function __get($property)
	{
		if(property_exists($this,$property))
		{
			return $this->$property;
		}
	}
	
	/**
	 * Funcion para asignar valores a una propiedad de la clase.
	 *
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 * @param mixed $property
	 * @param mixed $value
	 *
	 */
	public function __set($property, $value)
	{
		if(property_exists($this,$property))
		{
			$this->$property = $value;
		}
		return $this;
	}
	
	/**
	 * Funcion para cambiar los datos de un array
	 * a los de la entidad Usuario.
	 * 
	 * @access public
     * @author oreyes
     * @since 14/01/2015
	 * @param Array $data
	 */
	public function intercambiarArray($data = array())
	{
		$columns = $this->obtenerColumnasUsuario();
		foreach ($columns as $column)
		{
			if(!empty($data[$column]))
			{
				$this->__set($column, $data[$column]);
			}else{
				$this->__set($column, '');
			}
		}
		$indiceContrasena = array_search( 't01contrasena' , $columns );
		$this->__set( $columns[$indiceContrasena], sha1($data[$columns[$indiceContrasena]]));
	}
        
        public function intercambiarArrayRedesSociales($data = array())
	{
		$columns = $this->obtenerColumnasUsuario();
		foreach ($columns as $column)
		{
			if(!empty($data[$column]))
			{
				$this->__set($column, $data[$column]);
			}else{
				$this->__set($column, '');
			}
		}
	}
	
	/**
	 * Funcion para extraer todas las propiedades del objeto.
	 *
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 *
	 * @return array $propertys
	 */
	public function obtenerColumnasUsuario()
	{
		$propertys = array();
		$propertys = get_class_vars(get_class($this));
		$propertys = array_keys($propertys);
		return $propertys;
	}
}