<?php
namespace Login\Entity; 

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;
use Login\Entity\Usuario;

class UsuarioFiltros extends Usuario implements InputFilterAwareInterface
{
	/**
	 *
	 * @var InputFilterInterface $inputFilter
	 */
	protected $inputFilter;
	
	/**
	 * Constructor del objeto.
	 *
	 * @access public.
	 * @author oreyes
	 * @since 22/01/2015
	 *
	 */
	public function __construct()
	{
	
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new Exception('no usado');
	}
	
	/**
	 * Funcion que se implementa para crear filtros
	 * a los objetos de tipo formulario y validar los campos
	 * del registro de usuario.
	 * @access public
	 * @author oreyes
	 * @since 14/01/2015
	 * @see \Zend\InputFilter\InputFilterAwareInterface::getInputFilter()
	 * @return InputFilter $inputFilter
	 */
	public function getInputFilter($bolGrupo=true)
	{
		$user = new Usuario();
		$columns = $user->obtenerColumnasUsuario();

		if(!$this->inputFilter)
		{
			$inputFilter = new InputFilter();
			$factory     = new Factory();
				
			//Filtros input para nombre.
			$inputFilter->add($factory->createInput(array(
					'name'      => $columns[1],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim')
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 45,
									),
							),
					),
			)));
				
			//Filtros input para apellidos.
			$inputFilter->add($factory->createInput(array(
					'name'      => $columns[2],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
				
			//Filtros input para correo o nombre de usuario.
			$inputFilter->add($factory->createInput(array(
					'name'  => $columns[3],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 255,
									),
							),
					),
			)));
				
			//Filtros input para contrasena.
			$inputFilter->add($factory->createInput(array(
					'name'      => $columns[4],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 255,
									),
							),
					),
			)));
				
			//Filtros input para fecha registro.
			$inputFilter->add($factory->createInput(array(
					'name'      => $columns[7],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 255,
									),
							),
					),
			)));
				
			//Filtros input para fecha actualiza.
			$inputFilter->add($factory->createInput(array(
					'name'      => $columns[8],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 255,
									),
							),
					),
			)));
				
			//Filtros input para estatus.
			$inputFilter->add($factory->createInput(array(
					'name'      => $columns[13],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 1,
									),
							),
					),
			)));
				
			//Filtros input para confirmacion de contrasena.
			$inputFilter->add($factory->createInput(array(
					'name'      => 'confPass',
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 255,
									),
							)
					),
			)));
				
			//Filtros input para verlicenciaexamen.
			$inputFilter->add($factory->createInput(array(
					'name' => 'verlicenciaexamen',
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 6,
									),
							),
					),
			)));
			if($bolGrupo){	
			//Filtros input para vercodigogrupo.
			$inputFilter->add($factory->createInput(array(
					'name' => 'vercodigogrupo',
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 7,
									),
							),
					),
			)));
                        }else{
                            //Filtros input para vercodigogrupo.
			$inputFilter->add($factory->createInput(array(
					'name' => 'vercodigogrupo',
					'required'  => false,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 7,
									),
							),
					),
			)));
                        }
			$this->inputFilter = $inputFilter;
		}
		return $this->inputFilter;
	}
	 
	/**
	 * Funcion que se implementa para crear filtros
	 *  a los objetos de tipo formulario y validar los campos
	 *  del formulario para el logeo.
	 *
	 * @access public
	 * @author oreyes
	 * @since 14/01/2015
	 * @return InputFilterInterface $inputFilter
	 */
	public function getInputFilterLogin()
	{
		$user = new Usuario();
		$columns = $user->obtenerColumnasUsuario();
		 
		if(!$this->inputFilter)
		{
			$inputFilter = new InputFilter();
			$factory     = new Factory();
			 
			//Filtros input para el correo.
			$inputFilter->add($factory->createInput(array(
					'name'  => $columns[3],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 255,
									),
							),
					),
			)));
			 
			//Filtros input para la contraseña.
			$inputFilter->add($factory->createInput(array(
					'name'      => $columns[4],
					'required'  => true,
					'filters'   => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'    => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 255,
									),
							),
					),
			)));
			$this->inputFilter = $inputFilter;
		}
		return $this->inputFilter;
	}
}