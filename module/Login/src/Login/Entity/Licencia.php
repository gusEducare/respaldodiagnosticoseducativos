<?php
namespace Login\Entity;

use Zend\InputFilter\Factory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Login\Entity\Codigo;

/**
 * Objeto Llave para manipulacion de licencias.
 *
 * @copyright Grupo Educare SA de CV
 * @since 30/12/2014
 * @author oreyes
 *
 */
class Licencia extends Codigo implements InputFilterAwareInterface
{
	/**
	 * 
	 * @var string $t03llave
	 */
	var $t03llave;
	/**
	 * 
	 * @var String $t03fecha_vigencia
	 */
	var $t03fecha_vigencia;
	
	/**
	 * 
	 * @var InputFilterInterface $inputFilter
	 */
	var $inputFilterLicencia;
	
	/**
	 * Constructor del objeto.
	 * 
	 * @access public.
	 * @author oreyes
	 * @since 30/12/2014
	 * 
	 */
	public function __construct()
	{
		
	}
	
	public function getInputFilterLicencia()
	{
		if(!$this->inputFilterLicencia)
		{
			$_inputFilter = new InputFilter();
			$factory	  = new Factory();
				
			$_inputFilter->add($factory->createInput(array(
					'name'=>'licenciaexamen',
					'required'	=> true,
					'filters'	=> array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators'=> array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 6,
											'max'      => 6,
									),
							),
					),
			)));
			$this->inputFilterLicencia = $_inputFilter; 
		}
		return $this->inputFilterLicencia;
	}
}