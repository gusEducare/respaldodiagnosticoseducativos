<?php

namespace Login\Form;

use Login\Entity\Usuario;
use Zend\Form\Form;
/**
 * 
 * Clase encargada de generar objetos de tipo Formulario
 * para mostrar los formularios de:
 *  - Registro de usuarios  
 *  - Registro de codigos(Grupo y Licencia)
 *  
 * @author oreyes
 *
 */
class RegistroForm  extends Form
{
	/**
	 * Constructor de la clase agrega los elementos 
	 * que componen al formulario.
	 * 
	 * @param string $name
	 */
    public function __construct($name = null) {
        parent::__construct('Registro');
        $this->setAttribute('method', 'post');
        
        $usuario = new Usuario();
        $columns = $usuario->obtenerColumnasUsuario();
        
        //Input para el nombre.
        $this->add(array(
            'name'      => $columns[1],
            'attributes'=> array(
                'type'   => 'text',
            	'placeholder'=>'Nombre'
             ),
            'options' => array(),
        ));
        
        //Input para los Apellidos.
        $this->add(array(
            'name'      => $columns[2],
            'attributes'=> array(
                'type'      => 'text',
            	'placeholder'=> 'Apellidos'
                ),
            'options' => array(),
        ));
        
        //Input para el correo.
        $this->add(array(
            'name'      => $columns[3],
            'attributes'=> array(
                'id'          => $columns[3],
                'type'      => 'text',
            	'placeholder'=> 'Correo Electrónico',
            	'id'      => $columns[3],
                ),
            'options' => array(),
        ));
        
        //Input para la contraseña de usuario.
        $this->add(array(
            'name'      => $columns[4],
            'attributes'=> array(
                'id'          => $columns[4], 
                'type'        => 'password',
            	'placeholder' => 'Contraseña',
                ),
            'options' => array(),
        ));
        
        //Input para el confirmar contraseña.
        $this->add(array(
            'name'      => 'confPass',
            'attributes'=> array(
            	'type'       => 'password',
            	'placeholder'=> 'Confirmar Contraseña'
            ),
            'options' => array(),
        ));
        
        //Input oculto para la fecha registro.
        $this->add(array(
        	'name'		=> $columns[7],
        	'attributes'=> array(
        		'type'	=> 'hidden',
        		'value' => date('Y/m/d g:i:s')
        	),
        ));
        
        //Input oculto para la fecha actualiza.
        $this->add(array(
        	'name'		=> $columns[8],
        	'attributes'=> array(
        		'type'	=> 'hidden',
        		'value' => date('Y/m/d g:i:s')
        	),
        ));
        
        //Input oculto para el estatus de usuario.
        $this->add(array(
        	'name'		=> $columns[13],
        	'attributes'=> array(
        		'type'	=> 'hidden',
        		'value' => '1'
        	),
        ));
        
        //Input para el codigo de grupo.
        $this->add(array(
        	'name'		=> 'codigogrupo',
        	'attributes'=> array(
        		'type' 		  => 'text',
        		'id'		  => 'codigogrupo',
        		'placeholder' => 'Código de Grupo',
                        'class'=>'inputHome'
        		),
        ));
        
        
        //Input para la licencia.
        $this->add(array(
        	'name' 		 => 'licenciaexamen',
        	'attributes' =>	array(
        		'type'		  => 'text',
        		'id'		  => 'licenciaexamen',
                        'class'           => 'licenciausuario inputHome',
        		'placeholder' => 'Ingresa tu Licencia'
        	),
        ));
        
        
       
     
        /***
         * 
         * 
         * 
         * 
         */
        
          //Input para recuperar correo

        $this->add(array(
            'name'      => 'contrasenarecupera',
            'attributes'=> array(
                'id'        => 'contrasenarecupera',
                'type'      => 'email',
            	'placeholder'=> 'Correo Electrónico',
            	
                ),
            'options' => array(),
        ));
        //Input para recuperar por lic

        $this->add(array(
            'name'      => 'contrasenarecuperalic',
            'attributes'=> array(
                'id'        => 'contrasenarecuperalic',
                'type'      => 'text',
            	'placeholder'=> 'licencia',
            	
                ),
            'options' => array(),
        ));
        
        //submit para recuperar por correo.
        $this->add(array(
        	'type' =>'Zend\Form\Element\Submit',
        	'name' => 'recuperacontrasena',
        	'attributes'=>array(
        		'value'  => 'Recuperar',
        		'id'	 => 'recuperacontrasena',
        		'class'	 => 'large-12 small-12 columns button success small radius'
        	),
        ));
                //submit para recuperar por lic.
        $this->add(array(
        	'type' =>'Zend\Form\Element\Submit',
        	'name' => 'recuperacontrasenalic',
        	'attributes'=>array(
        		'value'  => 'Recuperar',
        		'id'	 => 'recuperacontrasenalic',
        		'class'	 => 'large-12 small-12 columns button success small radius'
        	),
        ));
       

        //submit para recuperar por licencia.
        $this->add(array(
        	'type' =>'Zend\Form\Element\Submit',
        	'name' => 'recuperacontrasenalic',
        	'attributes'=>array(
        		'value'  => 'Recuperar con licencia',
        		'id'	 => 'recuperacontrasenalic',
        		'class'	 => 'large-12 small-12 columns button success small radius'
        	),
        ));
        
        //input hidden urlRecuperaCorreo.
        $this->add(array(
        	'type' => 'hidden',
        	'name' => 'urlRecuperaContrasena',
        	'attributes' => array(
        		'id'	=> 'urlRecuperaContrasena'
        	), 
         ));
                //input hidden urlRecuperaPasslic.
        $this->add(array(
        	'type' => 'hidden',
        	'name' => 'urlRecuperaContrasenaLic',
        	'attributes' => array(
        		'id'	=> 'urlRecuperaContrasenaLic'
        	), 
         ));
        /*
         * 
         * 
         * 
         * 
         */
        //Input para el codigo de grupo solo lectura.
        $this->add(array(
        	'name'		=> 'vercodigogrupo',
        	'attributes'=> array(
        		'type' 		  => 'text',
        		'id'		  => 'vercodigogrupo',
        		'class'		  => 'text-center',
        		'readonly'	  => 'true',
        		'style'	 	  => 'pointer-events:none'
        	),
        ));
        
        //Input para el codigo de grupo solo lectura.
        $this->add(array(
        	'name' 		 => 'verlicenciaexamen',
        	'attributes' =>	array(
        		'type'		  => 'text',
        		'id'		  => 'verlicenciaexamen',
        		'class'		  => 'text-center',
        		'readonly'	  => 'true',
        		'style'	 	  => 'pointer-events:none'
        	),
        ));
        
        //submit para validar codigos.
        $this->add(array(
        	'type' =>'Zend\Form\Element\Submit',
        	'name' => 'validarcodigos',
        	'attributes'=>array(
        		'value'  => 'Validar',
        		'id'	 => 'validarcodigos',
        		'class'	 => 'large-12 small-12 columns button  small radius circleBig'
        	),
        ));
        //boton para ocultar registro sin correo electronico
        $this->add(array(
            'name'      => 'yatengocorreo',
            'attributes'=> array(
            	'type'       => 'button',
                'id'        => 'yatengocorreo',
            	'value'=> 'No tengo correo electrónico',
                'class' => 'large-12 small-12 columns button small success radius'
            )
        ));
        //boton para Cancelar
        $this->add(array(
            'name'      => 'botonCancelar',
            'attributes'=> array(
            	'type'       => 'button',
                'id'        => 'botonCancelar',
            	'value'=> 'Cancelar',
                'class' => 'large-12 small-12 columns button small success radius'
            )
        ));
        
        //boton para registrar con Facebook
        $this->add(array(
        		'type' => 'Zend\Form\Element\Button',
        		'name' => 'registroFacebook',
        		'options'=>array(
        				'label'  => ' Registrarme',
        				//'formaction' => '/user/registro/facebook'
        		),
        		'attributes' => array(
        				'type'=>'submit',
        				'class' => 'fa fa-facebook-official fa-1x radius small large-12 columns redSocial',
        				'formaction' => '/login/registrarusuariosocial?provider=Facebook'
        		)
        ));
        
         //boton para logueo con twitter.
        $this->add(array(
        		'type' => 'Zend\Form\Element\Button',
        		'name' => 'registroTwitter',
        		'options'=>array(
        			'label'  => ' Registrarme'
        		),
        		'attributes' => array(
        				'type'=>'submit',
        				'class' => 'fa fa-twitter-square fa-1x radius small info large-12 columns redSocial',
                                        'formaction' => '/login/registrarusuariosocial?provider=Twitter'
        				//'formaction' => '/user/login/twitter'
        		)
        ));
        
        //boton para registrar con Google
        $this->add(array(
        		'type' => 'Zend\Form\Element\Button',
        		'name' => 'registroGoogle',
        		'options'=>array(
        				'label'  => ' Registrarme',
        				//'formaction' => '/user/registro/google'
        		),
        		'attributes' => array(
        				'type'=>'submit',
        				'class' => 'fa fa-google-plus fa-1x radius small alert large-12 columns redSocial',
        				'formaction' => '/login/registrarusuariosocial?provider=Google'
        		)
        ));
       
        //submit para registrar usuario.
        $this->add(array(
            'type'  => 'Zend\Form\Element\Submit',
            'name'  => 'registrate',
            'attributes' => array(
                'value' => 'Registrarse',
                'id' 	=> 'submitRegistro',
            	'class' => 'large-12 small-12 columns button small success radius'
            ),
        ));
        
        //input hidden urlValidarCodigos.
        $this->add(array(
        	'type' => 'hidden',
        	'name' => 'urlValidarCodigos',
        	'attributes' => array(
        		'id'	=> 'urlValidarCodigos',
                'class' => 'large-12 small-12 columns '
        	), 
         ));
        
        //input hidden idPaquete.
        $this->add(array(
        	'type' => 'hidden',
        	'name' => 'idPaquete',
        	'attributes' => array(
        		'id'	=> 'idPaquete'
        	), 
         ));
        
        //input hidden dias_duracion
        $this->add(array(
        	'type' => 'hidden',
        	'name' => 'dias_duracion',
        	'attributes' => array(
        		'id'	=> 'dias_duracion'
        	), 
         ));
    }
}
