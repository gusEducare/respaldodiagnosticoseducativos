<?php

namespace Login\Form;

use Login\Entity\Usuario;
use Zend\Form\Form; 
use Zend\Form\Element;
use Zend\Form\Factory;

/**
 * Clase encargada de generar objetos de tipo Formulario
 * para mostrar los formularios de:
 * - Login
 * @author oreyes
 *
 */
class LoginForm  extends Form
{  
	/**
	 * Constructor de la clase agrega los elementos
	 * que componen al formulario.
	 *
	 * @param string $name
	 */
    public function __construct($name = null) {
        parent::__construct('login');
        
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'frmLogin');
        
        $login = new Usuario();
        $columna = $login->obtenerColumnasUsuario();
        
        //input para nombre de usuario o correo.
        $this->add(array(
            'name'      => $columna[3],
            'attributes'=> array(
                'type'   => 'string',
                'id'     => 'usuario',
            	'placeholder'   => 'Correo Electrónico / Nombre de usuario',
                'class'=>'inputHome'
            )
        ));
        
        //input para la contraseña.
        $this->add(array(
            'name'      => $columna[4],
            'attributes'=> array(
            'type'      => 'password',
            'placeholder'=>'Contraseña',
            'class'=>'inputHome'
            )
        ));
        
        //boton para hacer el submit a ingresar
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Ingresar',
                'id' => 'submitbutton',
                'class' => 'circleBig'
            ),
        ));
        
        //boton para resetear la contraseña.
        $this->add(array(
        	'name'		=>'resetcontrasena',
        	'attributes'=>array(
        		'value'	=> 'Olvidé mi contraseña',
        		'id'	=> 'resetcontrasena',
        		'class' => 'large-12 small-12 columns hTitlePassword'
        			
        	)
        ));
        
        //boton para logueo con facebook.
        $this->add(array(
        	'type' => 'Zend\Form\Element\Button',
        	'name' => 'logeoFacebook',
        	'options'=>array(
        		'label'  => ''
        	),
        	'attributes' => array(
        			'type'=>'submit',
        			'class' => 'fa fa-facebook fa-2x large-4 columns',
                                 'formaction' => '/login/iniciarsesionsocial?provider=Facebook'
        			//'formaction' => '/user/login/facebook'
        	)
        ));
        
        //boton para logueo con twitter.
        $this->add(array(
        		'type' => 'Zend\Form\Element\Button',
        		'name' => 'logeoTwitter',
        		'options'=>array(
        			'label'  => ''
        		),
        		'attributes' => array(
        				'type'=>'submit',
        				'class' => 'fa fa-twitter-square fa-2x large-4 columns',
                                        'formaction' => '/login/iniciarsesionsocial?provider=Twitter'
        				//'formaction' => '/user/login/twitter'
        		)
        ));
        
        //boton para logueo con Google.
        $this->add(array(
        		'type' => 'Zend\Form\Element\Button',
        		'name' => 'logeoGoogle',
        		'options'=>array(
        				'label'  => '',
        				'formaction' => '/user/login/google'
        		),
        		'attributes' => array(
        				'type'=>'submit',
        				'class' => 'fa fa-google-plus fa-2x  large-4 columns',
        				'formaction' => '/login/iniciarsesionsocial?provider=Google'
                                        //'formaction' => '/user/login/Google'
        		)
        ));
               
        //boton para logueo con PortalGe.
        $this->add(array(
        		'type' => 'Zend\Form\Element\Button',
        		'name' => 'logeoPortalGE',
        		'options'=>array(
        				'label'  => ' Ingresar Profesor'
        		),
        		'attributes' => array(
        				'class' => 'fa fa-user fa-1x radius small success large-12 columns',
        				'formaction' => '/user/login/google'
        		)
        ));
    }       
}
?>