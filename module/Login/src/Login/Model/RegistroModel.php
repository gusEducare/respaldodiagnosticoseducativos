<?php
namespace Login\Model;


use Login\Entity\Usuario;
use Login\Entity\Codigo;
//Use Examenes\Model\EstatusExamen;
use Zend\Db\ResultSet\ResultSet;
use Zend\Log\Writer\Stream;
use Zend\Log\Writer\FirePhp;
use Zend\Log\Logger;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\Pdo\Connection;
use Zend\Db\Adapter\Exception\InvalidQueryException;

/**
 * 
 * @author oreyes
 *
 */
class RegistroModel{
	/**
	 * 
	 * @var Sql $sql. 
	 */
	protected $sql;
	
	/**
	 * 
	 * @var Adapter $adapter.
	 */
	protected $adapter;
	
	/**
	 * 
	 * @var Connection $conection.
	 */
	protected $conection;
	
	/**
	 * 
	 * @var Logger $log.
	 */
	protected $log;
        
         protected $config;
	
	/**
	 * 
	 * @var InputFilter
	 */
	protected $inputFilter;
	
	/**
	 * Constructor de la clase que inicia el adapter un objeto Connection
	 * el logger.
	 * 
	 * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 05/01/2015.
	 * @param Adapter $adapter
	 */
	public function __construct(Adapter $adapter,$config)
	{
                $this->config = $config;
		$this->adapter = $adapter;
		$this->conection = $this->adapter->getDriver()->getConnection();
		$this->sql 	   = new Sql($adapter);
		$this->log 	   = new Logger();
                $this->log->addWriter(new Stream('php://stderr'));
		//$writer    	   = new FirePhp();
		//$this->log->addWriter($writer);
	}
	
	/**
	 * Funcion para registrar un usuario se inicia una transaccion,
	 * que involucra: 
	 * 	- insertarusuario. 
	 * 	- insertarperfil
	 * en cualquier caso de falla se deberia hacer un rollback de todo lo registrado.
	 * 
	 * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 29/12/2014.
	 * @param Usuario $addusuario
	 * @param int $idPerfil
	 * @return string
	 */
	public function registrarusuario(Usuario $addusuario, $idPerfil)
	{	
		$this->conection->beginTransaction();
		$_res = array();
		
		try {
			$idUsuario          = $this->insertarUsuario( $addusuario );
			$idUsuarioPerfil    = $this->insertarPerfil( $idUsuario, $idPerfil );                        
                        
			$this->conection->commit();
			$_res['msg'] 				= "El usuario se registro correctamente.";
			$_res['lastGeneratedValue'] = array('idUsuario' => $idUsuario, 'idUsuarioPerfil' => $idUsuarioPerfil);
		} catch ( InvalidQueryException $e ) {
			$_res['msg'] = "Favor de reportar el siguiente error con Soporte GE: ".$e->getMessage();
				
			$this->conection->rollback();
		}
		return $_res;
	}
	
	/**
	 * Funcion para insertar un usuario recibe un objeto de tipo Usuario.
	 * 
	 * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 05/01/2015.
	 * @param Usuario $addusuario
	 * @return int $_idUsuario id_usuario generado en el registro
	 */
	public function insertarUsuario( Usuario $addusuario )
	{
		$arrAll 		= $this->toolValuesColumns($addusuario);
		$auxcolumns		= $arrAll['columns'];
		$values 		= $arrAll['values'];
		$insert 		= $this->sql->insert();
			
		$insert->into('t01usuario');
		$insert->columns(array($auxcolumns));
		$insert->values( $values );
			
		$statement 	= $this->sql->prepareStatementForSqlObject($insert);
		$statement->execute();
		
		$_idUsuario = $this->conection->getLastGeneratedValue();
		
		return $_idUsuario;
	}
	
	/**
	 * Funcion para insertar el perfil de un alumno se agrega el id usuario
	 * para asociarle un perfil de alumno.
	 * 
	 * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 05/01/2015.
	 * @param int $idUsuario
	 * @param int $idPerfil
	 * @return int $idUsuarioPerfil.
	 */
	public function insertarPerfil( $idUsuario, $idPerfil )
	{
		error_log('entro a insertar perfil');
		$insert 	= $this->sql->insert();
		$insert->into('t02usuario_perfil');
		$insert->columns(array(
				'c01id_perfil',
				't01id_usuario'));
		$insert->values(array(
				'c01id_perfil'	=> $idPerfil,
				't01id_usuario'	=> $idUsuario));
			
		$statement 	  	 = $this->sql->prepareStatementForSqlObject($insert);
		$statement->execute();
		$idUsuarioPerfil = $this->conection->getLastGeneratedValue();
		
		return $idUsuarioPerfil;
	}
	
        /**
         * Funcion que se encarga de insertar los registros necesarios para que 
         * el usuario pueda entrar a contestar examenes, guarda la información 
         * de la licencia, además de los examenes ligados al paquete correspondiente 
         * a la licencia 
         * 
         * @param type $idUsuarioPerfil
         * @param type $_intIdPaquete
         * @return boolean
         */
        public function insertarExamenesUsuario($idUsuarioPerfil, $_intIdExamen, $_strLlave, $_intDias) {
            
		$insert 	= $this->sql->insert();
		$insert->into('t03licencia_usuario');
		$insert->columns(array(
				't21id_paquete',
				't02id_usuario_perfil',
				't03licencia',
				't03fecha_activacion',
				't03dias_duracion',
				't03estatus'));
		$insert->values(array(                    
				't21id_paquete'         => 1, // Hardcodeo el id del paquete para que siempre se inserte uno generico, hasta que decidamos utilizarlo
				't02id_usuario_perfil'  => $idUsuarioPerfil,
				't03licencia'           => $_strLlave,
				't03fecha_activacion'   => date("Y-m-d H:i:s"),
				't03dias_duracion'      => $_intDias,
				't03estatus'            => 1                    
                        ));			
		$statement  = $this->sql->prepareStatementForSqlObject($insert);
		$statement->execute();
		$id_LicenciaUsuario = $this->conection->getLastGeneratedValue();
		                
           // $_arrExamenes = $this->obtenerExamenesPaquete($_intIdPaquete);
           // foreach( $_arrExamenes as $data){
                $ins 	= $this->sql->insert();
		$ins->into('t04examen_usuario');
		$ins->columns(array(
				't12id_examen',
				't03id_licencia_usuario',
				't04estatus'));
		$ins->values(array(        
				//'t12id_examen'          => $data['t12id_examen'], 
				't12id_examen'          => $_intIdExamen, 
				't03id_licencia_usuario'=> $id_LicenciaUsuario,
				't04estatus'            => 'NOPRESENTADO'
                        ));
			
		$statement2  = $this->sql->prepareStatementForSqlObject($ins);
		$statement2->execute();
           // }
            //$_bolExamenes = true;
            $idExamenUsuario = $this->conection->getLastGeneratedValue();
            return $idExamenUsuario ;
        }
        
        /**
         * Funcion que se encarga de ligar un usuario al grupo del cual ingreso
         * el codigo en el registro
         * @param type $_intIdUsuarioPerfil
         * @param type $_strCodigoGrupo
         * @return boolean
         */
        public function ligarUsuarioGrupo($_intIdUsuarioPerfil,  $_strCodigoGrupo){
            $_idGrupo = $this->obtenerIdGrupo( $_strCodigoGrupo );            
            $insert 	= $this->sql->insert();
            $insert->into('t08administrador_grupo');
            $insert->columns(array(
                            't02id_usuario_perfil',
                            't05id_grupo',
                            't08usuario_ligado',
                            't08fecha_registro',
                            't08fecha_actualiza'));
            $insert->values(array(  
                            't02id_usuario_perfil'  => $_intIdUsuarioPerfil,
                            't05id_grupo'           => $_idGrupo,
                            't08usuario_ligado'     => 1,
                            't08fecha_registro'     => date("Y-m-d H:i:s"),
                            't08fecha_actualiza'    => date("Y-m-d H:i:s")
                    ));

            $statement  = $this->sql->prepareStatementForSqlObject($insert);
            $statement->execute();
            $idExamenUsuario = $this->conection->getLastGeneratedValue();
            return $idExamenUsuario;
        }
        
        /**
         * Funcion que se encarga de obtener los examenes pertenecientes a un 
         * paquete
         * @param type $_intIdPaquete
         * @return type
         */
        public function obtenerExamenesPaquete($_intIdPaquete){
            
                $select = $this->sql-> select()
                        -> from(array('t22'=>'t22examen_paquete'),
                                array('t12id_examen'))
                        -> where(array('t21id_paquete' => $_intIdPaquete));
                $statement = $this->sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();
                $resultSet = new ResultSet();
                $respExamenes  = $resultSet->initialize($result)->toArray();
                
                return $respExamenes;
        }
        
        /**
         * Funcion encargada de obtener el id del grupo, este servira para 
         * almacenarlo en la tabla t08
         * @param type $_strCodigoGrupo
         * @return type
         */
        protected function obtenerIdGrupo( $_strCodigoGrupo ){
            
                $select = $this->sql-> select()
                        -> from(array('t07'=>'t07codigo'),
                                array('t05id_grupo'))
                        -> where(array('t07codigo' => $_strCodigoGrupo));
                $statement = $this->sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();
                $resultSet = new ResultSet();
                $respGrupo  = $resultSet->initialize($result)->toArray();
                $_idGrupo = $respGrupo[0]['t05id_grupo'];
                return $_idGrupo    ;
        }


        /**
	 *
	 * Funcion para buscar usuario y contraseña en la tabla de registro de usuarios
	 * validar el perfil con el que cuenta y cuales son sus ids.
	 *
	 * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 12/01/2015.
	 * @param Usuario $loginusuario
	 * @return array $respUsuario
	 */
	public function iniciarsesion(Usuario $loginusuario)
	{
            try {
                $select = $this->sql-> select()
                        -> from(array('t01'=>'t01usuario'),
                                array('t01id_usuario','t01nombre','t01apellidos','t01correo','t01contrasena','t01contrasena' ))
                        -> join(array('t02'=>'t02usuario_perfil'),
                                't01.t01id_usuario = t02.t01id_usuario',
                                array('t02id_usuario_perfil','c01id_perfil'))
                        -> where(array('t01correo'		=> $loginusuario->__get('t01correo') ,
                            't01contrasena'	=> $loginusuario->__get('t01contrasena') ,
                            't01estatus'		=> $this->config['constantes']['ESTATUS_ACTIVO']));
                $statement = $this->sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();
                $resultSet = new ResultSet();
                $respUsuario   = $resultSet->initialize($result)->toArray();
                
            }catch(InvalidQueryException $e){
                $respUsuario = "Favor de reportar el siguiente error con Soporte GE: ".$e->getMessage();
            }
            return $respUsuario;
	}
        
	/**
	 * validacion de correos para saber si existe un correo o usuario
	 * en la tabla de usuarios.
	 * 
	 * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 05/01/2015.
	 * @param string $_strCorreo
	 */
	function validarcorreo($_strCorreo) 
	{
		$select = $this->sql->select('t01usuario');
		$select->columns(array('t01correo','t01contrasena'))
			   ->where(array('t01correo'=>$_strCorreo));
		$statement  = $this->sql->prepareStatementForSqlObject($select);
		$result		= $statement->execute();
		$resultSet  = new ResultSet();
		$resp = $resultSet->initialize($result)->toArray();
		return $resp; 
	}
	
    /**
     * Validacion de codigos de Grupo para saber si existe o no el codigo de grupo
     * en la tabla de codigos.
     * 
     * @since 10-02-2015
     * @author rjuarez@grupoeducare.com
     * @param Codigo $codigogrupo
     * 
     * @return boolean $resp : retorna true cuando existe el codigo y false cuando no.
     */
    function validargrupo(Codigo $codigogrupo)
    {       
    	$resp = array();
    	
        $select = $this->sql->select()
                ->from(array('t07'=>'t07codigo'),
                       array('t05id_grupo','t07codigo','t07estaus'))
                ->join(array('t05'=>'t05grupo'),
                      't07.t05id_grupo = t05.t05id_grupo') 
                ->where (array('t07codigo'	 => $codigogrupo->__get('t07codigo'),
                            't07.t07estatus' => $codigogrupo->__get('t07estatus'),
                            't05.t05estatus' => $this->config['constantes']['ESTATUS_ACTIVO']));
            
            
        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result     = $statement->execute();
        $resultSet = new ResultSet();
        $resp      = $resultSet->initialize($result)->toArray();
        
        //valida resultado diferente de vacio.
        if(!empty($resp))
        {
        	$resp =  true;
        } else {
        	$resp = false;
        }
        
        return $resp;   
     }
        
        
        /**
	 * Funcion para separar las columnas y valores de un objeto 
	 * que se insertan con objetos SQLInsert en la base de datos.
	 * 
	 * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 30/12/2014.
	 * @param Usuario $_usuario
	 * 
	 * @return Array() $arrAll con los valores y columnas.  
	 */
	public function toolValuesColumns( Usuario $_usuario )
	{
		$arrAll		 = array(); 
		$_columns 	 = $_usuario->obtenerColumnasUsuario();
		$sizecolumns = count($_columns);
		$values		 = array();
		$auxcolumns  = array();
		
		$x = 0;
		for ($i = 1 ; $i < $sizecolumns; $i++)
		{
			$auxcolumns[$x] = $_columns[$i];
			$values[$_columns[$i]] = $_usuario->__get($_columns[$i]);
			$x++;
		}
		$arrAll['values']  = $values;
		$arrAll['columns'] = $auxcolumns;
		
		return $arrAll;
	}
        
        public function validalicencia ($licencia) {
            
	
	$_strQueryExisteCorreo='SELECT 
                                    t03fecha_activacion,
                                    t03dias_duracion,t03estatus,
                                    DATE_ADD(t03fecha_activacion,
                                    INTERVAL t03dias_duracion DAY),
                                    DATEDIFF(DATE_ADD(t03fecha_activacion,
                                    INTERVAL t03dias_duracion DAY),
                                    NOW())
                                    FROM
                                    evaluaciones.t03licencia_usuario
                                    where
                                    t03licencia = :licencia';



	$statement = $this->adapter->query($_strQueryExisteCorreo);


	$results = $statement->execute(array(
			':licencia'=> $licencia
	));

	$resultSet = new ResultSet;
		   	$resultSet->initialize($results);
		   	
		 
		   	$_arrResultado = $resultSet->toArray();

		 	

		   	return $_arrResultado;
          
        }
        
        public function iniciarsesionRedesSociales($correo)
	{
            try {
                $select = $this->sql-> select()
                        -> from(array('t01'=>'t01usuario'),
                                array('t01id_usuario','t01nombre','t01apellidos','t01correo','t01contrasena','t01contrasena' ))
                        -> join(array('t02'=>'t02usuario_perfil'),
                                't01.t01id_usuario = t02.t01id_usuario',
                                array('t02id_usuario_perfil','c01id_perfil'))
                        -> where(array('t01correo'		=> $correo,
                            't01estatus'		=> $this->config['constantes']['ESTATUS_ACTIVO'] ));
                $statement = $this->sql->prepareStatementForSqlObject($select);
                $result = $statement->execute();
                $resultSet = new ResultSet();
                $respUsuario   = $resultSet->initialize($result)->toArray();
                
            }catch(InvalidQueryException $e){
                $respUsuario = "Favor de reportar el siguiente error con Soporte GE: ".$e->getMessage();
            }
            return $respUsuario;
	}
	
        
        public function validalicenciarecupera ($licencia) {
            
	
	$_strQueryExisteCorreo='SELECT  t03.t03licencia,t03.t03estatus,t01.t01correo, t01.t01contrasena FROM evaluaciones.t03licencia_usuario t03
                                        inner join t02usuario_perfil t02 on t02.t02id_usuario_perfil=t03.t02id_usuario_perfil
                                        inner join t01usuario t01 on t01.t01id_usuario = t02.t01id_usuario
                                        where t03.t03licencia = :licencia';



	$statement = $this->adapter->query($_strQueryExisteCorreo);


	$results = $statement->execute(array(
			':licencia'=> $licencia
	));

	$resultSet = new ResultSet;
		   	$resultSet->initialize($results);
		   	
		 
		   	$_arrResultado = $resultSet->toArray();

		 	

		   	return $_arrResultado;
          
        }
        /*
         * modificar
         */
        
        function existeCorreo($_strUsuario) {
		$_arrRespuesta = array ();
		
		$select = $this->sql->select ();
		$select->from ( array (
				't01' => 't01usuario' 
		) )->where ( 't01.t01correo =' . "'" . $_strUsuario . "'" )->columns ( array (
				't01id_usuario',
				't01nombre',
				't01apellidos',
				't01estatus',
				't01contrasena',
				't01correo' 
		) );
		
		//$this->_objLogger->debug ( $select->getSqlString () );
		$statement = $this->sql->prepareStatementForSqlObject ( $select );
		
		$result = $statement->execute ();
		
		$resultSet = new ResultSet ();
		$resultSet->initialize ( $result );
		
		$_arrRespuesta = $resultSet->toArray ();
		return $_arrRespuesta;
	}
        
        function existeCorreoUser($_strCorreo) {
		$_bolExiste = false;
		
		$_arrRespuesta = array ();
		
		$select = $this->sql->select ();
		$select->from ( array (
				'u' => 't01usuario' 
		) )->where ( array (
				'u.t01correo ' => $_strCorreo 
		) );
		
		$statement = $this->sql->prepareStatementForSqlObject ( $select );
		//$this->_objLogger->debug ( $statement->getSql () );
		$result = $statement->execute ();
		
		$resultSet = new ResultSet ();
		$resultSet->initialize ( $result );
		$_arrRespuesta = $resultSet->toArray ();
		
		return $_arrRespuesta;
	}
        
        	function generaCodigoRecuperacion( $_strCorreo,$_strDatosalumno,$_strCorreoalumno,$_stridExaUser,$_stridUserPer,$url,$boolReactiva=true) {
                        $_arrResultado = array ();
                        $_bolEjecuto = false;
                        $_strQueryBuscarUsuario = "SELECT
                        t01usuario.t01id_usuario,
                        CONCAT(t01usuario.t01nombre,' ',t01usuario.t01apellidos) AS nombre
                        FROM
                        t01usuario
                        WHERE
                        t01usuario.t01correo = '$_strCorreoalumno'";
                        $stmtConsultar = $this->adapter->query ( $_strQueryBuscarUsuario );
                        $resultConsultar = $stmtConsultar->execute ();

                        $resultSetConsultar = new ResultSet ();
                        $resultSetConsultar->initialize ( $resultConsultar );
                        $_arrIdUsuario = $resultSetConsultar->toArray ();

                        $_arrId = $_arrIdUsuario [0] ['t01id_usuario'];
                        $_strNombre = $_arrIdUsuario [0] ['nombre'];

                    //    $this->_objLogger->debug ( "Consulta paso 1 => " . $_strQueryBuscarUsuario );

                        if (count ( $_arrIdUsuario ) > 0) {
                              //  $this->_objLogger->debug ( "Genera codigo paso 2 => " );
                                // genera codigo
                                $letras = chr ( rand ( ord ( "a" ), ord ( "z" ) ) );
                                $letras .= chr ( rand ( ord ( "a" ), ord ( "z" ) ) );
                                $letras .= chr ( rand ( ord ( "a" ), ord ( "z" ) ) );
                                $_strCodigo = uniqid ( $letras, true );

                                // Verificar que exista usuario con solicitud de cambiar contraseña en t30
                                $_strConsultaExisteSolicitud = "SELECT t30id_recuperar_password
                                                                                                        FROM t30recuperar_password
                                                                                                WHERE t01id_usuario= " . $_arrId;

                                $stmtConsultarExiste = $this->adapter->query ( $_strConsultaExisteSolicitud );
                                $resultConsultar = $stmtConsultarExiste->execute ();
                                $resultSetConsultar = new ResultSet ();
                                $resultSetConsultar->initialize ( $resultConsultar );
                                $_arrIdt30 = $resultSetConsultar->toArray ();

                              //  $this->_objLogger->debug ( "Existe solicitud paso 3 => " . $_strConsultaExisteSolicitud );
                                // Existe la peticion usuario
                                if (isset ( $_arrIdt30 ) && count ( $_arrIdt30 ) > 0) {

                                        $_strUpdate = "	UPDATE  t30recuperar_password
                                                        SET t30fecha_vencimiento = CURDATE(),
                                                        t30codigo_recuperacion ='$_strCodigo'
                                                        WHERE t30id_recuperar_password=" . $_arrIdt30 [0] ['t30id_recuperar_password'];

                                        $stmtConsultarUpdate = $this->adapter->query ( $_strUpdate );
                                        $resultConsultar = $stmtConsultarUpdate->execute ();
                                        $resultSetConsultar = new ResultSet ();
                                        $resultSetConsultar->initialize ( $resultConsultar );

                                        if ($resultConsultar->getAffectedRows () > 0) {
                                                $_bolEjecuto = true;
                                        }
                                } else {

                                        // no existe peticion,insertar
                                        $_strQueryInsertarCodigo = "INSERT INTO
                                                                t30recuperar_password(
                                                                t01id_usuario,
                                                                t30fecha_vencimiento,
                                                                t30codigo_recuperacion)
                                                                VALUES($_arrId ,
                                                                CURDATE(),
                                                                '$_strCodigo')";

                                        $stmtConsultarInsertar = $this->adapter->query ( $_strQueryInsertarCodigo );
                                        $resultConsultarInsertar = $stmtConsultarInsertar->execute ();
                                        $resultSetConsultarInsertar = new ResultSet ();
                                        $resultSetConsultarInsertar->initialize ( $resultConsultarInsertar );

                                        if ($resultConsultarInsertar->getAffectedRows () > 0) {
                                                $_bolEjecuto = true;
                                        }
                                }

                                if ($_bolEjecuto) {

                                        $_arrResultado = array ();
                                        $_arrResultado ['id'] = $_arrId;
                                        $_arrResultado ['codigo'] = $_strCodigo;
                                        $_arrResultado ['nombre'] = $_strNombre;
                                        if($boolReactiva){
                                            $_arrResultado ['correo'] = $this->enviaCorreo ( $_strCodigo, $_arrResultado ['nombre'], $_strCorreo, $url );
                                        }else{
                                            $_arrResultado ['correo'] = $this->enviaCorreoReactivacion ( $_strCodigo,$_strDatosalumno,$_stridExaUser,$_stridUserPer, $_strCorreo, $url ); 
                                        }
                                } else {
                                        $_arrResultado = NULL;
                                }
                        }

                        return $_arrResultado;
	}
        
                	function generaCodigoRecuperacionPass( $_strCorreo,$url) {
                        $_arrResultado = array ();
                        $_bolEjecuto = false;
                        $_strQueryBuscarUsuario = "SELECT
                        t01usuario.t01id_usuario,
                        CONCAT(t01usuario.t01nombre,' ',t01usuario.t01apellidos) AS nombre
                        FROM
                        t01usuario
                        WHERE
                        t01usuario.t01correo = '$_strCorreo'";
                        $stmtConsultar = $this->adapter->query ( $_strQueryBuscarUsuario );
                        $resultConsultar = $stmtConsultar->execute ();

                        $resultSetConsultar = new ResultSet ();
                        $resultSetConsultar->initialize ( $resultConsultar );
                        $_arrIdUsuario = $resultSetConsultar->toArray ();

                        $_arrId = $_arrIdUsuario [0] ['t01id_usuario'];
                        $_strNombre = $_arrIdUsuario [0] ['nombre'];

                    //    $this->_objLogger->debug ( "Consulta paso 1 => " . $_strQueryBuscarUsuario );

                        if (count ( $_arrIdUsuario ) > 0) {
                              //  $this->_objLogger->debug ( "Genera codigo paso 2 => " );
                                // genera codigo
                                $letras = chr ( rand ( ord ( "a" ), ord ( "z" ) ) );
                                $letras .= chr ( rand ( ord ( "a" ), ord ( "z" ) ) );
                                $letras .= chr ( rand ( ord ( "a" ), ord ( "z" ) ) );
                                $_strCodigo = uniqid ( $letras, true );

                                // Verificar que exista usuario con solicitud de cambiar contraseña en t30
                                $_strConsultaExisteSolicitud = "SELECT t30id_recuperar_password
                                                                                                        FROM t30recuperar_password
                                                                                                WHERE t01id_usuario= " . $_arrId;

                                $stmtConsultarExiste = $this->adapter->query ( $_strConsultaExisteSolicitud );
                                $resultConsultar = $stmtConsultarExiste->execute ();
                                $resultSetConsultar = new ResultSet ();
                                $resultSetConsultar->initialize ( $resultConsultar );
                                $_arrIdt30 = $resultSetConsultar->toArray ();

                              //  $this->_objLogger->debug ( "Existe solicitud paso 3 => " . $_strConsultaExisteSolicitud );
                                // Existe la peticion usuario
                                if (isset ( $_arrIdt30 ) && count ( $_arrIdt30 ) > 0) {

                                        $_strUpdate = "	UPDATE  t30recuperar_password
                                                        SET t30fecha_vencimiento = CURDATE(),
                                                        t30codigo_recuperacion ='$_strCodigo'
                                                        WHERE t30id_recuperar_password=" . $_arrIdt30 [0] ['t30id_recuperar_password'];

                                        $stmtConsultarUpdate = $this->adapter->query ( $_strUpdate );
                                        $resultConsultar = $stmtConsultarUpdate->execute ();
                                        $resultSetConsultar = new ResultSet ();
                                        $resultSetConsultar->initialize ( $resultConsultar );

                                        if ($resultConsultar->getAffectedRows () > 0) {
                                                $_bolEjecuto = true;
                                        }
                                } else {

                                        // no existe peticion,insertar
                                        $_strQueryInsertarCodigo = "INSERT INTO
                                                                t30recuperar_password(
                                                                t01id_usuario,
                                                                t30fecha_vencimiento,
                                                                t30codigo_recuperacion)
                                                                VALUES($_arrId ,
                                                                CURDATE(),
                                                                '$_strCodigo')";

                                        $stmtConsultarInsertar = $this->adapter->query ( $_strQueryInsertarCodigo );
                                        $resultConsultarInsertar = $stmtConsultarInsertar->execute ();
                                        $resultSetConsultarInsertar = new ResultSet ();
                                        $resultSetConsultarInsertar->initialize ( $resultConsultarInsertar );

                                        if ($resultConsultarInsertar->getAffectedRows () > 0) {
                                                $_bolEjecuto = true;
                                        }
                                }

                                if ($_bolEjecuto) {

                                        $_arrResultado = array ();
                                        $_arrResultado ['id'] = $_arrId;
                                        $_arrResultado ['codigo'] = $_strCodigo;
                                        $_arrResultado ['nombre'] = $_strNombre;
                                        
                                            $_arrResultado ['correo'] = $this->enviaCorreo ( $_strCodigo, $_arrResultado ['nombre'], $_strCorreo, $url );
                                        
                                } else {
                                        $_arrResultado = NULL;
                                }
                        }

                        return $_arrResultado;
	}
        
        /**
	 * Funcion para enviar correo
	 * 
	 * @param string $_strCodigo        	
	 * @param string $_strNombreDestino        	
	 * @param string $_strCorreoReceptor        	
	 * @param string $url        	
	 * @return bool
	 */
	public function enviaCorreo($_strCodigo, $_strNombreDestino, $_strCorreoReceptor, $url) {
		$to = $_strCorreoReceptor;
		$from = 'noresponder@educaredigital.com';
		$subject = utf8_decode ( 'Solicitud cambio de Password' );
		$_strHttpHost = $_SERVER ['HTTP_HOST'];
		
		// $_strValorURL = $_strHttpHost.$this->view->url('base',array('controller'=>'index','action'=>'modificarpassword')).'/?idCodigo='.$_strCodigo;
		$_strValorURL = $url . '?idCodigo=' . $_strCodigo;
		
		//$this->_objLogger->debug ( "RUTA DE MODFICART => " . $_strValorURL );
		ob_start ();
		require_once __DIR__ . '/../../../view/login/login/solicitud_password.phtml';
		$html = ob_get_clean ();
		
		// a random hash will be necessary to send mixed content
		
		// header principal (multipart mandatory)
		$headers = "From: " . $from . "\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		// header del html y codigo html
		$headers .= "Content-Type: text/html; charset=utf-8\r\n";
		$headers .= "Content-Transfer-Encoding: 8bit\r\n";
		$headers .= $html;
		
		// enviar
		$_boolResultado = mail ( $to, $subject, "", $headers );
		
		if ($_boolResultado) {
			$respuesta = true;
			// $respuesta = $_strValorURL;
			// array('resultado' => true, 'mensaje' => 'Se ha enviado un correo a los administradores con la solicitud de desactivación de personal.');
		} else {
			$respuesta = false;
			// $_arrRespuesta = array('resultado' => false, 'mensaje' => 'Hubo un error al enviar el correo, intente más tarde.');
		}
		return $respuesta;
	}
        
        
        public function enviaCorreoReactivacion($_strCodigo,$_strDatosalumno,$_stridExaUser,$_stridUserPer, $_strCorreoReceptor, $url) {
		$to = $_strCorreoReceptor;
		$from = 'noresponder@educaredigital.com';
		$subject = utf8_decode ( 'Solicitud reactivar examen' );
		$_strHttpHost = $_SERVER ['HTTP_HOST'];
		
		// $_strValorURL = $_strHttpHost.$this->view->url('base',array('controller'=>'index','action'=>'modificarpassword')).'/?idCodigo='.$_strCodigo;
		$_strValorURL = $url . '?idCodigo=' . $_strCodigo.'&idExaUser='.$_stridExaUser.'&idUserPer='.$_stridUserPer;

		//$this->_objLogger->debug ( "RUTA DE MODFICART => " . $_strValorURL );
		ob_start ();
		require_once __DIR__ . '/../../../view/login/login/solicitud_reactivar.phtml';
		$html = ob_get_clean ();
		
		// a random hash will be necessary to send mixed content
		
		// header principal (multipart mandatory)
		$headers = "From: " . $from . "\r\n";
		$headers .= "MIME-Version: 1.0" . "\r\n";
		// header del html y codigo html
		$headers .= "Content-Type: text/html; charset=utf-8\r\n";
		$headers .= "Content-Transfer-Encoding: 8bit\r\n";
		$headers .= $html;
		
		// enviar
		$_boolResultado = mail ( $to, $subject, "", $headers );
		
		if ($_boolResultado) {
			$respuesta = true;
			// $respuesta = $_strValorURL;
			// array('resultado' => true, 'mensaje' => 'Se ha enviado un correo a los administradores con la solicitud de desactivación de personal.');
		} else {
			$respuesta = false;
			// $_arrRespuesta = array('resultado' => false, 'mensaje' => 'Hubo un error al enviar el correo, intente más tarde.');
		}
		return $respuesta;
	}
        
        
        
        public function changeCodeRectiva($_strCodigo) {
		$_strQueryGruposUsuario= "UPDATE t30recuperar_password SET t30codigo_recuperacion='desactivado' WHERE t30codigo_recuperacion=:_Codigo";


            $statement = $this->adapter->query($_strQueryGruposUsuario);
            $results = $statement->execute(array(':_Codigo' => $_strCodigo,));

                   if($results->getAffectedRows() > 0){
                        $boolUpt = true;
                    } else {
                        $boolUpt = false;
                    }
            
            return $boolUpt;
	}
        public function checkCodeRecuperacion($_strCodigo) {
		$_arrRespuesta = array ();
		
		$select = $this->sql->select ();
		$select->from ( 't30recuperar_password' )->join ( "t01usuario", "t01usuario.t01id_usuario = t30recuperar_password.t01id_usuario" )->where ( array (
				't30codigo_recuperacion ' => $_strCodigo,
				"t30fecha_vencimiento = CURDATE()" 
		) );
		
		$statement = $this->sql->prepareStatementForSqlObject ( $select );
		//$this->log ->debug ("sql/*/*/*/ ".print_r($statement,true));
		
		$result = $statement->execute ();
		
		$resultSet = new ResultSet ();
		$resultSet->initialize ( $result );
		$_arrRespuesta = $resultSet->toArray ();
		
		return $_arrRespuesta;
	}
        
        
        /**
	 * actualizar el password
	 *
	 * @static
	 *
	 * @access public
	 * @author firstname and lastname of author, SABG
	 * @param string $_strPass        	
	 * @param int $_intIdUsuario        	
	 * @return bool $_boolResultado TRUE en caso de exito FALSE en caso contrario
	 */
	public function updatePassword($_intIdUsuario, $_strPass) {
		$update = $this->sql->update ( 't01usuario' );
		$update->set ( array (
				't01contrasena' => sha1 ( $_strPass ) 
		) );
		
		$update->where ( array (
				't01id_usuario' => $_intIdUsuario 
		) );
		$statement = $this->sql->prepareStatementForSqlObject ( $update );
		try {
			$result = $statement->execute ();
			$rowAffected = $result->getAffectedRows ();
			//$this->_objLogger->debug ( "ingresa al try " . print_r ( $rowAffected, true ) );
			return true;
		} 

		catch ( Zend_Exception $e ) {
			return false;
			//$this->_objLogger->debug ( "entra al catch   " . print_r ( $rowAffected, true ) );
		}
	}
        
        
       /**
         * Funcion que se encarga de insertar los registros necesarios para que 
         * el usuario pueda entrar a contestar examenes, guarda la información 
         * de la licencia, además de los examenes ligados al paquete correspondiente 
         * a la licencia 
         * 
         * @param type $idUsuarioPerfil
         * @param type $_intIdPaquete
         * @return boolean
         */
        public function insertarLicenciaProfesor($idUsuarioPerfil, $_strLlave, $_intDias) {
            
		$insert 	= $this->sql->insert();
		$insert->into('t03licencia_usuario');
		$insert->columns(array(
				't21id_paquete',
				't02id_usuario_perfil',
				't03licencia',
				't03fecha_activacion',
				't03dias_duracion',
				't03estatus'));
		$insert->values(array(                    
				't21id_paquete'         => 1, // Hardcodeo el id del paquete para que siempre se inserte uno generico, hasta que decidamos utilizarlo
				't02id_usuario_perfil'  => $idUsuarioPerfil,
				't03licencia'           => $_strLlave,
				't03fecha_activacion'   => date("Y-m-d H:i:s"),
				't03dias_duracion'      => $_intDias,
				't03estatus'            => 1                    
                        ));			
		$statement  = $this->sql->prepareStatementForSqlObject($insert);
		$statement->execute();
		$id_LicenciaUsuario = $this->conection->getLastGeneratedValue();
		                
                return $id_LicenciaUsuario ;
        }
        
}
