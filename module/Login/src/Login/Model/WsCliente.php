<?php
namespace Login\Model;

use Zend\Soap\Client;
use Login\Entity\Usuario;

/**
 * @since 11/02/2015
 * @author oreyes
 * @copyright Grupoeducare SA de CV
 */
class WsCliente extends Client
{
	/**
	 * Constructor de la clase inicializa los valores de wsdl y options
	 * que construyen al cliente para saber a donde tiene que hacer las operaciones.
	 * 
	 * @access public
	 * @since 11/02/2015
	 * @param String $wsdl
	 * @param String $options
	 * 
	 */
	public function __construct($wsdl, $options = array() )
	{
		$this->wsdl = $wsdl;
		$this->options = $options;
	}
	
	/**
	 * Funcion para validar las licencias de examenes consume una operacion 
         * de un servicio web.
	 *
	 * @access public
	 * @author oreyes
	 * @since 11/02/2015
	 * @param String $_strLicencia
	 * @param int $_intIdServicio
	 * @return mixed $result
	 */
	public function validarlicencia( $_strLicencia, $_intIdServicio )
	{
		$cliente = new Client($this->wsdl,$this->options);
		$result  = $cliente->llaveValidaMensaje($_strLicencia, $_intIdServicio);
		
		return $result;
	}
        
        /**
         * Funcion para validar la cuenta del profesor, consume una operacion de
         * un servicio web
         * @param Usuario $loguinUsuario
         * @return type
         */
        public function validarProfesor( Usuario $loguinUsuario )
        {       
            $_strCorreo = $loguinUsuario->__get('t01correo');
            $_strPassword = $loguinUsuario->__get('t01contrasena');
            $cliente = new Client($this->wsdl,$this->options);
            $result  = $cliente->validaDatosProfesor($_strCorreo, $_strPassword);            
            return $result;          
        }
        
        /**
         * Funcion que activa la licencia del usuario que se acaba de registrar
         * por medio de un servicio web
         * @param Usuario $loguinUsuario
         * @return type
         */
        public function activaLicenciaUsuario( Usuario $loguinUsuario, $_intIdServicio, $_intIdUsuarioPerfil, $_strLlave )
        {                   
            $_strNombre = $loguinUsuario->__get('t01nombre') . ' ' . $loguinUsuario->__get('t01apellidos');
            $_strNombreUsuario = $loguinUsuario->__get('t01correo');
            $cliente = new Client($this->wsdl,$this->options);
            $result  = $cliente->activarLicencia($_intIdServicio, $_intIdUsuarioPerfil, $_strLlave, $_strNombreUsuario, $_strNombre);
            return $result;          
        }
        
        /**
         * Funcion que Verifica el registro y obtiene los datos de usuario TID primaria
         * @param Usuario $correo
         * @param Usuario $contrasegna
         * @param Usuario $_bolSolicitaDatos
         * @return type
         */
        public function loginTid( $correo, $contrasegna,$_bolSolicitaDatos)
        {                   
            $cliente = new Client($this->wsdl,$this->options);
            $result  = $cliente->login($correo, $contrasegna, $_bolSolicitaDatos);
            return $result;          
        }
}