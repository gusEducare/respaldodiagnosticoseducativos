<?php
namespace Login\Controller;

use Login\Entity\Licencia;
use Login\Model\WsCliente;
use Zend\Session\Container;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Log\Writer\FirePhp;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;
use Zend\Json\Json;
use Login\Entity\Codigo;
use Login\Entity\UsuarioFiltros;
use Login\Entity\Usuario;
use Login\Form\RegistroForm;
use Login\Form\LoginForm;
use Login\Model\RegistroModel;

use Hybrid_Auth;
use ScnSocialAuth\Mapper\Exception as MapperException;
use ScnSocialAuth\Mapper\UserProviderInterface;
use ScnSocialAuth\Options\ModuleOptions;
use Zend\View\Model\ModelInterface;
use Zend\View\Model\ViewModel;


/**
 * Clase final para almacenar las constantes de los estatus de los grupos
 */
final class EstatusGrupo{
    const ACTIVO = 1;
    const INACTIVO = 2;
}
/**
 * 
 * @author oreyes
 *
 */
class LoginController extends AbstractActionController{
	/**
	 * Instancia de la clase Logger para controlador.
	 * @var Logger $log;
	 */
	protected $log;
    
	/**
	 * Instancia de la clase Container para el manejo de datos de sesion.
	 * @var Container $datos_sesion.
	 */
	protected $datos_sesion;
        
        /**
         * Instancia del Cliente de WebService SOAP del PortalGE
         * @var type 
         */
        protected $wsCliente;
        
        /**
         * Instancia del Cliente de WebService SOAP de TID
         * @var type 
         */
        protected $wsClienteTID;

        /**
         * Atributo que se utilizara para obtener los datos de configuracion a 
         * traves del service locator
         * @var type 
         */
        protected $config;
        
        /**
         * Instancia de Hybridauth
         * @var type 
         */
        protected $Hybrid_Auth;

        
        public function __construct()
	{
            
            $this->log = new Logger();
            $this->log->addWriter(new Stream("php://stderr"));

            $_writer = new FirePhp();
            $this->console = new Logger();
            $this->console->addWriter($_writer);
            
		$this->datos_sesion = new Container('user');

	//maesemaster@lmL.com
//                $this->config 	  = $this->getServiceLocator()->get('Config');
                $this->wsCliente = new WsCliente('http://portalpruebas.com/licencia?wsdl');
                $this->wsClienteTID = new WsCliente('http://tecno.educaredigital.com/webservice/login?wsdl');
                
//                $this->wsCliente = new WsCliente($this->config['constantes']['WSDL_PORTALGE']);
//                $this->wsClienteTID = new WsCliente($this->config['constantes']['WSDL_TID']);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
	 */
    public function indexAction()
    {
        $this->getRequest();
        $form 	 = new RegistroForm();
        $usuario = new Usuario();
        $formLogin = new LoginForm();
         
        $auth = $this->zfcUserAuthentication()->getAuthService();
        if($auth->hasIdentity())
        {
        	error_log(print_r($this->getIdentity(),true));
        	error_log(print_r($auth->getIdentity()->getT01correo(),true));
        }
    	return array(
    			'form'     => $form,
    			'formLogin'=> $formLogin,
    			'usuario'  => $usuario);
    }
    
    /**
     * Funcion para iniciar la sesion de un usuario.
     * 
     * @access public.
	 * @author oreyes@grupoeducare.
	 * @since 12/01/2015.
     * 
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>
     */
    public function iniciarsesionAction(){
    	$request 	 = $this->getRequest();
    	$columns	 = array();
    	$dataIds     = array();
    	$arrRoute[0] = 'home';
    	$arrRoute[1] = array('controller'=>'index','model'=>'index');
    	$resp['msg'] = 'El formulario de logueo no se lleno correctamente.';
    	$config 	  = $this->getServiceLocator()->get('Config');
    	if($request->isPost())
    	{
    		$loguinUsuario = new Usuario();
    		$usuarioFiltro = new UsuarioFiltros();
    		$formLoguin	   = new LoginForm();
    		$columns 	   = $loguinUsuario->obtenerColumnasUsuario();
    		
    		$formLoguin->setInputFilter($usuarioFiltro->getInputFilterLogin());
    		$formLoguin->setData($request->getPost());
    		if($formLoguin->isValid()) 
    		{
    			$loguinUsuario->intercambiarArray( $formLoguin->getData() );
    			$_strRespuesta = $this->getRegistroModel()->iniciarsesion( $loguinUsuario );
    			
    			if(isset($_strRespuesta) && !empty($_strRespuesta))
    			{
    				$loguinUsuario->intercambiarArray( $_strRespuesta[0] );
    				$dataIds['idUsuario'] 		= $_strRespuesta[0]['t01id_usuario'];
    				$dataIds['idUsuarioPerfil'] = $_strRespuesta[0]['t02id_usuario_perfil'];
    				$perfil 					= $_strRespuesta[0]['c01id_perfil'];
    				$this->registrarsesion($loguinUsuario, $dataIds, $perfil);
    				$arrRoute = $this->redireccionarPerfil();
    			}else{                            
                            $_strRes        = $this->getRegistroModel()->validarcorreo( $loguinUsuario->__get('t01correo') );
                            $_intTotal 	= count($_strRes);
                            if($_intTotal===0){
                                #Agrego la opcion de loguear a profesores desde el portalge                            
                                $respProfesor = $this->wsCliente->validarProfesor( $loguinUsuario );
                                if($respProfesor['valida'] !== 0){
                                    // El usuario cuenta con perfil de profesor en el Portal
                                    // Hay que registrarlo en la plataforma  
                                    $loguinUsuario->setT01nombre($respProfesor['nombre']);
                                    $loguinUsuario->__set('t01provider', 'PortalGE');
                                    $loguinUsuario->__set('t01provider_id',$respProfesor['id_profesor']);
                                    $loguinUsuario->__set('t01estatus', $config['constantes']['ESTATUS_ACTIVO']);
                                    $formRegistro	= new RegistroForm();
                                    $formRegistro->setInputFilter($usuarioFiltro->getInputFilter());
                                    $formRegistro->setData($request->getPost());
                                    $perfilProf	= $config['constantes']['ID_PERFIL_PROFESOR'];
                                    $regModel 	= $this->getRegistroModel();
                                    $resp       = $regModel->registrarusuario($loguinUsuario , $perfilProf);
                                    $this->registrarsesion( $loguinUsuario, $resp['lastGeneratedValue'], $perfilProf );
                                    $arrRoute = $this->redireccionarPerfil();
                                }else{//si no existe, se consulta en TID Primaria
                                    $postParams = $request->getPost();
                                    $usuarioTid = $this->wsClienteTID->loginTid($postParams['t01correo'],$postParams['t01contrasena'],true);
                                    if($usuarioTid){//si ususario, consulta vigencia de su licencia
                                        if(isset($usuarioTid) && $usuarioTid['tiene_licencia'] == true && isset($usuarioTid['ge_c26id_libros'])){//Si licencia es vigente, procede al registro
                                            $loguinUsuario->setT01nombre($usuarioTid['ge_t01nombre']);
                                            $loguinUsuario->__set('t01fecha_aregistro', date('Y/m/d g:i:s'));
                                            $loguinUsuario->__set('t01provider', 'TID');
                                            $loguinUsuario->__set('t01provider_id',$usuarioTid['ge_t01id_usuario']);
                                            $loguinUsuario->__set('t01estatus', $config['constantes']['ESTATUS_ACTIVO']);
                                            $formRegistro	= new RegistroForm();
                                            $formRegistro->setInputFilter($usuarioFiltro->getInputFilter());
                                            $formRegistro->setData($request->getPost());
                                            $perfilAlumno	= $config['constantes']['ID_PERFIL_ALUMNO'];
                                            $regModel 	= $this->getRegistroModel();
                                            $resp       = $regModel->registrarusuario($loguinUsuario , $perfilAlumno);//regstra usuario
                                            $_strLlave   = $this->generarLlaveTID();//se genera una llave de 7 dígitos, exclusivo para usuarios registrados de TID Primaria
                                            $this->registrarsesion( $loguinUsuario, $resp['lastGeneratedValue'], $perfilAlumno ); 
                                            $_intIdUsuarioPerfil = $resp['lastGeneratedValue']['idUsuarioPerfil']; 
                                            $_intDias =  $usuarioTid['vigencia'];
                                            $_intIdExamen = $usuarioTid['ge_c26id_libros'];
                                            $idExamenUsuario = $regModel->insertarExamenesUsuario($_intIdUsuarioPerfil, $_intIdExamen, $_strLlave, $_intDias);//regstra examen
                                            $arrRoute = $this->redireccionarPerfil();
                                        }else{
                                           $resp['msg'] = "Hola ".$usuarioTid['ge_t01nombre']." ".$usuarioTid['ge_t01apellidos'].".<br/>Eres usuario(a) de TID Primaria y para poder registrarte en Diagnósticos Educativos debes contar con una licencia vigente, además "
                                                        . "de tener un libro asignado.";
                                        }
                                    }else{
                                        $resp['msg'] = $respProfesor['mensaje'];
                                    }   
                                }
                            }else{
                                $resp['msg'] = 'Usuario o contraseña son incorrectos.';
                            }
    			}
    		}else{
    			$resp['msg'] = 'Algunos campos del formulario no fueron llenados correctamente.';
    		}
    	}
    	$this->flashMessenger()->addMessage($resp['msg']);
    	return $this->redirect()->toRoute($arrRoute[0],$arrRoute[1]);
    }
    
    /**
     * Funcion para registrar a un usuario.
     * 
     * @access public.
     * @author oreyes
     * @since 23/12/2014
     * 
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>
     */
    public function registrarusuarioAction() {
        $columns = array();
        $request = $this->getRequest();
        $config     = $this->getServiceLocator()->get('Config');
        
        $regModel = $this->getRegistroModel();
        $_intIdServicio = $config['constantes']['ID_SERVICIO_EVALUACIONES'];
        $_strCorreo = $request->getPost('t01correo');  
        $_strLlave = $request->getPost('verlicenciaexamen');
        $_strCodigoGrupo = $request->getPost('vercodigogrupo');
        
        $boofiltro = ($_strCodigoGrupo != "") ? true : false;
        $resplicencia = $this->wsCliente->validarlicencia($_strLlave, $config['constantes']['ID_SERVICIO_EVALUACIONES']);
        $resplic = $resplicencia['id_paquete'] == $config['constantes']['CODIGO_LICENCIA_PROFESOR'] ? $config['constantes']['ID_PERFIL_PROFESOR'] : false;
        
        if ($request->isPost()) {
            $addUsuario = new Usuario();
            $usuarioFiltro = new UsuarioFiltros();
            $columns = $addUsuario->obtenerColumnasUsuario();
            if ($request->getPost('confPass') == $request->getPost($columns[4])) {
                $formRegistro = new RegistroForm();
                $formRegistro->setInputFilter($usuarioFiltro->getInputFilter($boofiltro));
                $formRegistro->setData($request->getPost());
                if ($formRegistro->isValid()) {
                    $_strRes = $this->getRegistroModel()->validarcorreo($_strCorreo);
                    $_intTotal = count($_strRes);
                    $addUsuario->intercambiarArray($formRegistro->getData());
                    if ($_intTotal === 0) {
                        if ($_strCodigoGrupo && $resplic != $config['constantes']['ID_PERFIL_PROFESOR']) {//REGISTRA ALUMNO
                            
                            $perfilAlum = $config['constantes']['ID_PERFIL_ALUMNO'];
                            $resp = $regModel->registrarusuario($addUsuario, $perfilAlum);
                            $_intIdUsuarioPerfil = $resp['lastGeneratedValue']['idUsuarioPerfil'];
                            $_fechaVigencia = $this->wsCliente->activaLicenciaUsuario($addUsuario, $_intIdServicio, $_intIdUsuarioPerfil, $_strLlave);

                            $_arrVigencia = explode('_', $_fechaVigencia);
                            $_intDias = $_arrVigencia[0];
                            $_intIdPaquete = $_arrVigencia[1];
                            $idExamenUsuario = $regModel->insertarExamenesUsuario($_intIdUsuarioPerfil, $_intIdPaquete, $_strLlave, $_intDias);
                            $idGrupo = $regModel->ligarUsuarioGrupo($_intIdUsuarioPerfil, $_strCodigoGrupo);

                            $this->registrarsesion($addUsuario, $resp['lastGeneratedValue'], $perfilAlum);
                            $arrRoute = $this->redireccionarPerfil();
                            $this->flashMessenger()->addMessage($resp['msg']);
                            return $this->redirect()->toRoute($arrRoute[0], $arrRoute[1]);
                        } elseif($_strCodigoGrupo== "" && $resplic == $config['constantes']['ID_PERFIL_PROFESOR']) {
                            if ($resplic == $config['constantes']['ID_PERFIL_PROFESOR']) {
                                $perfilProf = $config['constantes']['ID_PERFIL_PROFESOR'];
                                $resp = $regModel->registrarusuario($addUsuario, $perfilProf);
                                $_intIdUsuarioPerfil = $resp['lastGeneratedValue']['idUsuarioPerfil'];
                                $_fechaVigencia = $this->wsCliente->activaLicenciaUsuario($addUsuario, $_intIdServicio, $_intIdUsuarioPerfil, $_strLlave);

                                $_arrVigencia = explode('_', $_fechaVigencia);
                                $_intDias = $_arrVigencia[0];
                                $_intIdPaquete = $_arrVigencia[1];
                                $_intIdLicenciaProfesor = $regModel->insertarLicenciaProfesor($_intIdUsuarioPerfil, $_strLlave, $_intDias);
                                
                                $this->registrarsesion($addUsuario, $resp['lastGeneratedValue'], $perfilProf);
                                $arrRoute = $this->redireccionarPerfil();

                                $this->flashMessenger()->addMessage($resp['msg']);

                                return $this->redirect()->toRoute($arrRoute[0], $arrRoute[1]);
                            } else {
                                $resp['msg'] = 'Esta licencia no puede ser registrada como profesor.';
                            }
                        }
                        if($resplic == $config['constantes']['ID_PERFIL_PROFESOR']) {
                            $resp['msg'] = 'Profesor, tus datos de registro son incorrectos, recuerda que no necesitas registrar un código de grupo.';
                        } else {
                            $resp['msg'] = 'Tus datos de registro son incorrectos.';
                        }
                    } else {
                        $resp['msg'] = 'Este correo ya fue registrado antes';
                    }
                }  else {
                    $resp['msg'] = 'Tus datos de registro son incorrectos.';
                }
            } else {
                $resp['msg'] = 'Los campos contraseña y confirmar contraseña no coinciden.';
            }
        }
        $this->flashMessenger()->addMessage($resp['msg']);
        return $this->redirect()->toRoute('home', array('controller' => 'index', 'action' => 'index'));
        //return $this->redirect()->toRoute($arrRoute[0],$arrRoute[1]);
    }

    /**
     * Funcion para registrar datos de sesion de un usuario.
     *
     * @access public.
     * @author oreyes
     * @since 07/01/2015
     * @param Usuario $usuario
     * @param Array $dataIds
     * @param int perfil
     * @return string
     */
    private function registrarsesion( Usuario $usuario, $dataIds, $perfil  )
    {
    	$this->datos_sesion->sesionActiva  		= true;
    	$this->datos_sesion->nombreUsuario 		= $usuario->__get('t01nombre');
    	$this->datos_sesion->apellidos	 		= $usuario->__get('t01apellidos');
    	$this->datos_sesion->correo		 		= $usuario->__get('t01correo');
    	$this->datos_sesion->idUsuario	 		= $dataIds['idUsuario'];
    	$this->datos_sesion->idUsuarioPerfil	= $dataIds['idUsuarioPerfil'];
    	$this->datos_sesion->perfil	 			= $perfil;
    }
    
    /**
     * Validar codigo de grupo.
     * @access public.
     * @author oreyes, rjuarez
     * @since 08/01/2015
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|\Zend\Stdlib\ResponseInterface
     */
    public function validarcodigosAction()
    {
    	$request = $this->getRequest();
    	if($request->isPost()){
    		$codigogrupo = $request->getPost('codigogrupo');
    		
	    	$codigo 	 = new Codigo();
	    	
            $codigo->__set('t07codigo',$codigogrupo);
            $codigo->__set('t07estatus',  EstatusGrupo::ACTIVO);
    		$formReg 	 = new RegistroForm();
    		$formReg->setInputFilter($codigo->getInputFilter());
    		$formReg->setData($request->getPost());
                    
    		$respgrupo = $this->getRegistroModel()->validargrupo( $codigo );
    		
    		if( $respgrupo){
    		//if($formReg->isValid() && $respgrupo){
    			$resp['success'] = true;
    		}else{
	    		$resp['success'] = false;
	    		error_log(print_r($formReg->getMessages(),true));
    		}
    	} 
        else {
    		$resp = 'El código de grupo no es válido.';
    		$this->flashMessenger()->addMessage($resp);
    		return $this->redirect()->toRoute('home',array('controller'=>'index', 'action'=>'index'));
    	}
    	$response = $this->getResponse();
    	$response->setContent(Json::encode($resp));
    	return $response;
    }
    
    /**
     * Validar Licencia en portalge.
     * @access public.
     * @author oreyes 
     * @since 12/02/2015
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|\Zend\Stdlib\ResponseInterface
     */
    public function validarlicenciaAction()
    {
    	$request    = $this->getRequest();
    	$config     = $this->getServiceLocator()->get('Config');    
    	if($request->isPost()){
    		$strLicencia  = $request->getPost('licenciaexamen');
    		
    		$objLicencia = new Licencia();
    		$formReg = new RegistroForm();
    		$formReg->setInputFilter($objLicencia->getInputFilterLicencia());
    		$formReg->setData($request->getPost());
    		
    		if($formReg->isValid())
    		{
	    		$resplicencia = $this->wsCliente->validarlicencia( $strLicencia, $config['constantes']['ID_SERVICIO_EVALUACIONES']);
	    		$resp['success'] = $resplicencia['valida'];
                        if(isset($resplicencia['id_paquete'])){
                            $resp['id_paquete'] = $resplicencia['id_paquete'];
                        }
    			
    		}else{
    			$resp['success'] = false;
    		}
    	}
    	else {
    		$resp = 'La licencia no es válida.';
    		$this->flashMessenger()->addMessage($resp);
    		return $this->redirect()->toRoute('home',array('controller'=>'index', 'action'=>'index'));
    	}
    	$response = $this->getResponse();
    	$response->setContent(Json::encode($resp));
    	return $response;
    }
    
    /**
     * validacion de correos para saber si existe un correo o usuario
	 * en la tabla de usuarios.
     * 
     * @access public.
     * @author oreyes
     * @since 08/01/2015
     * @return \Zend\Stdlib\ResponseInterface
     */
    public function validarcorreoAction()
    {
    	$request 		= $this->getRequest();
    	$_strUsuario	= $request->getPost('t01correo');
        $_strRes        = $this->getRegistroModel()->validarcorreo( $_strUsuario );
        $_strRespuesta  = array();
        $_intTotal 	= count($_strRes);
        if($_intTotal>0)
        {
            $_strRespuesta['accion']   = true;
            $_strRespuesta['correo']   = $_strRes[0]['t01correo'];
            $_strRespuesta['msg']      = 'Usuario ya registrado.';
    	}else{
            $_strRespuesta['correo']   = $_strUsuario;
            $_strRespuesta['accion']= false;
            $_strRespuesta['msg']   = 'Usuario disponible.';
    	}
        $response = $this->getResponse();
    	$response->setContent(Json::encode($_strRespuesta));
    	return $response;
    }
    /**
     * Funcion para buscar el perfil y hacer la redireccion a dicho perfil.
     * 
     * @access public.
     * @author oreyes
     * @since 12/01/2015
     * 
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>|\Login\Controller\ViewModel
     */
    public function redireccionarPerfil()
    {
    	$routerPerfil = array("controller"=>"Index", "action"=>"index");
    	$config 	  = $this->getServiceLocator()->get('Config');    	
    	$perfilAlum = $config['constantes']['ID_PERFIL_ALUMNO'];
    	$perfilProf = $config['constantes']['ID_PERFIL_PROFESOR'];
        $perfilConstr = $config['constantes']['ID_PERFIL_CONSTRUCTOR'];
    	$arrRoute   = array();
    	
    	//REDIRECCIONAR DEPENDIENDO EL PERFIL QUE TENGA EL USUARIO.
    	switch($this->datos_sesion->perfil)
    	{
                case $perfilProf:
    				$perfil 	  = 'grupos';
    				$routerPerfil = array("controller"=>"grupos", "action"=>"index");
    			break;
    		case $perfilAlum:
    				$perfil		  = 'examenes';
    				$routerPerfil = array("controller"=>"examenes" , "action"=>"misexamenes");
    			break;
                case $perfilConstr:
    				$perfil 	  = 'examenes';
    				$routerPerfil = array("controller"=>"examenes", "action"=>"index");
    			break;
    		default:
    			
    			break;
    	}
    	array_push($arrRoute, $perfil, $routerPerfil);
    	return $arrRoute;
    }
    
    /**
     * Traer una instancia del modelo RegistroModel.php
     * 
     * @access public.
     * @author oreyes
     * @since 22/12/2014
     * 
     * @return RegistroModel $regModel.
     */
    public function getRegistroModel()
    {
    	$sm = $this->getServiceLocator();
    	$regModel = $sm->get('/Login/Model/RegistroModel');
    	return $regModel;
    }

     /**
     * Funcion para iniciar la sesion con red social.
     * 
     * @access public.
	 * @author lceniceros@grupoeducare.
	 * @since 12/01/2015.
     * 
     * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>
     */
    public function iniciarSesionSocialAction(){ 
        
        $request 	 = $this->getRequest();
        $arrRoute[0] = 'home';
    	$arrRoute[1] = array('controller'=>'index','model'=>'index');
        
        $sm = $this->getServiceLocator();
        $this->hybridAuth = $sm->get("ScnSocialAuth\Authentication\Adapter\HybridAuth");
        $hybridAuth =  $this->hybridAuth->getHybridAuth();

        $provider = $this->params()->fromQuery('provider');

        $adapter = $hybridAuth->authenticate( $provider );

        //$this->iniciarsesionsocialAction($adapter);

        $user_profile = $adapter->getUserProfile();
        if($provider=='Twitter'){
            $correo = '@'.$user_profile->displayName ;
        }else{
             $correo = $user_profile->emailVerified ;
        }
       
        $_strRes        = $this->getRegistroModel()->validarcorreo( $correo);     
        $_intTotal 	= count($_strRes);
        if($_intTotal===1){
            $_strRespuesta = $this->getRegistroModel()->iniciarsesionRedesSociales($correo);
            

            if(isset($_strRespuesta) && !empty($_strRespuesta))
            {
                    $loguinUsuario = new Usuario();
                    $_arraUserPerfile = $this->formatoRedesSociales($user_profile);
                    //$loguinUsuario->intercambiarArrayRedesSociales($_arraUserPerfile);
                    $loguinUsuario->intercambiarArrayRedesSociales($_strRespuesta[0]);
                    $dataIds['idUsuario'] = $_strRespuesta[0]['t01id_usuario'];
                    $dataIds['idUsuarioPerfil'] = $_strRespuesta[0]['t02id_usuario_perfil'];
                    $perfil = $_strRespuesta[0]['c01id_perfil'];
                    $this->registrarsesion($loguinUsuario, $dataIds, $perfil);
                    $arrRoute = $this->redireccionarPerfil();
            }
        }
        else {
                $resp['msg'] = 'Usuario o contraseña son incorrectos.';
        }
                 
    	$this->flashMessenger()->addMessage($resp['msg']);
    	return $this->redirect()->toRoute($arrRoute[0],$arrRoute[1]);
    }
    
    /**
	 * Genera una llave para usuarios de TID Primaria
	 *
	 * @return string
	 */
	private function generarLlaveTID() {
		$_intLongitudLlave		= 7;
		$_arrValoresAceptados	= array('2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
		$_arrSeries				= array('a','b','c','d','e');
		$_intMax				= count($_arrValoresAceptados) - 1;
	
		$_intInicio			= rand(1, $_intMax);
		$_arrTemporal[0]	= $_arrValoresAceptados[$_intInicio];
	
		for($i = 1; $i < $_intLongitudLlave; $i++){
			$_arrTemporal[$i] = $_arrValoresAceptados[rand(1, $_intMax)];
		}
	
		$_intIndiceSerie	= (($_intInicio % count($_arrSeries) == 0) ? 5 :  $_intInicio % count($_arrSeries)) -1;
		$_charSerie			= $_arrSeries[$_intIndiceSerie];
		$_arrLlaves[0]		= $_arrTemporal[0];
		
		$_strLlave = "";
		$_arrNumeros = range(0,$_intLongitudLlave-1);
		shuffle($_arrNumeros );
		for($i=0; $i<$_intLongitudLlave; $i++){			
			$_arrLlaves[$i] = $_arrTemporal[$_arrNumeros [$i]];
		}
		foreach($_arrLlaves as &$pos){
			$_strLlave .= $pos;
		}
		
		return $_strLlave;
	}
        
        public function formatoRedesSociales($user_profile,$estatus,$timestamp,$provider) {
//            $arrUserPerfile = array(
                $arrUserPerfile['t01provider_id'] = $user_profile->identifier;
                $arrUserPerfile['t01perfil'] = $user_profile->profileURL;
                $arrUserPerfile['t01webSite'] = $user_profile->webSiteURL;
                $arrUserPerfile['t01foto'] = $user_profile->photoURL;
                $arrUserPerfile['t01display'] = $user_profile->displayName;
                $arrUserPerfile['t01description'] = $user_profile->description;
                $arrUserPerfile['t01nombre'] = $user_profile->firstName;
                $arrUserPerfile['t01apellidos']= $user_profile->lastName;
                $arrUserPerfile['t01genero'] = $user_profile->gender;
                $arrUserPerfile['t01idioma'] = $user_profile->language;
                $arrUserPerfile['t01provider'] = $provider;
                $arrUserPerfile['t01fecha_registro'] = $timestamp;
                $arrUserPerfile['t01estatus'] = $estatus;
                if($provider=='Twitter'){
                    $arrUserPerfile['t01correo'] = '@'.$user_profile->displayName ;
                }else{
                    $arrUserPerfile['t01correo'] = $user_profile->email;
                }
              
                $arrUserPerfile['t01correoVerificado'] = $user_profile->emailVerified;
                $arrUserPerfile['t01telefono'] = $user_profile->phone;
                $arrUserPerfile['t01pais'] = $user_profile->country;
                $arrUserPerfile['t01direccion'] = $user_profile->address;
                $arrUserPerfile['t01region'] = $user_profile->region;
                $arrUserPerfile['t01cuidad'] = $user_profile->city;
                $arrUserPerfile['t01codigoPostal'] = $user_profile->zip;
//            );
            //echo $arrUserPerfile;
            return $arrUserPerfile;
        }

    public function getMisDatosModel()
	{
		$sm = $this->getServiceLocator();
		$model = $sm->get('Misdatos\Model\MisdatosModel');
	
		return $model;
	}
        
        public function registrarusuariosocialAction() {
        $request = $this->getRequest();
        $config = $this->getServiceLocator()->get('Config');

        $_strLlave = $request->getPost('verlicenciaexamen') == null ? $this->datos_sesion->llave : $request->getPost('verlicenciaexamen');
        $_strCodigoGrupo = $request->getPost('vercodigogrupo') == null ? $this->datos_sesion->grupo : $request->getPost('vercodigogrupo');
        $provider = $this->params()->fromQuery('provider');

        $resplicencia = $this->wsCliente->validarlicencia($_strLlave, $config['constantes']['ID_SERVICIO_EVALUACIONES']);
        $resplic = $resplicencia['id_paquete'] == $config['constantes']['CODIGO_LICENCIA_PROFESOR'] ? $config['constantes']['ID_PERFIL_PROFESOR'] : false;

        $this->datos_sesion->perfil = $resplic;
        $this->datos_sesion->llave = $_strLlave;
        $this->datos_sesion->grupo = $_strCodigoGrupo;

        $arrRoute[0] = 'home';
        $arrRoute[1] = array('controller' => 'index', 'model' => 'index');
        $sm = $this->getServiceLocator();
        $this->hybridAuth = $sm->get("ScnSocialAuth\Authentication\Adapter\HybridAuth");
        $hybridAuth = $this->hybridAuth->getHybridAuth();
        $adapter = $hybridAuth->authenticate($provider);
        $user_profile = $adapter->getUserProfile();
        $estatus = $config['constantes']['ESTATUS_ACTIVO'];
        $timestamp = date("Y-m-d H:i:s");
        $_arraUserPerfile = $this->formatoRedesSociales($user_profile, $estatus, $timestamp, $provider);
        
        $_strRes = $this->getRegistroModel()->validarcorreo($_arraUserPerfile['t01correo'] );
        $_intTotal = count($_strRes);
        if ($_intTotal === 0) {

            if ($this->datos_sesion->perfil == $config['constantes']['ID_PERFIL_PROFESOR'] && $this->datos_sesion->grupo == null) {//VALIDA SI ES PROFESOR O USAURIO
                // SI ES PROFESOR INSERTA EL PERFIL
                $addUsuario = new Usuario();
                $formReg = new RegistroForm();
                
                $formReg->setData($request->getPost());
                if (isset($user_profile) && !empty($user_profile)) {
                    
                    $addUsuario->intercambiarArrayRedesSociales($_arraUserPerfile);

                    $perfilAlum = $config['constantes']['ID_PERFIL_PROFESOR'];
                    $_intIdServicio = $config['constantes']['ID_SERVICIO_EVALUACIONES'];
                    $regModel = $this->getRegistroModel();
                    $resp = $regModel->registrarusuario($addUsuario, $perfilAlum);
                    $_intIdUsuarioPerfil = $resp['lastGeneratedValue']['idUsuarioPerfil'];
                    $_fechaVigencia = $this->wsCliente->activaLicenciaUsuario($addUsuario, $_intIdServicio, $_intIdUsuarioPerfil, $this->datos_sesion->llave);

                    $_arrVigencia = explode('_', $_fechaVigencia);
                    $_intDias = $_arrVigencia[0];
                    $_intIdPaquete = $_arrVigencia[1];
                    $idExamenUsuario = $regModel->insertarLicenciaProfesor($_intIdUsuarioPerfil, $_strLlave, $_intDias);
                    
                    $this->datos_sesion->getManager()->getStorage()->clear();
                    $this->registrarsesion($addUsuario, $resp['lastGeneratedValue'], $perfilAlum);
                    $arrRoute = $this->redireccionarPerfil();
                    $this->flashMessenger()->addMessage($resp['msg']);
                    return $this->redirect()->toRoute($arrRoute[0], $arrRoute[1]);
                } else {
                    $resp['msg'] = 'La licencia es incorrecta.';
                }
            } elseif($this->datos_sesion->perfil != $config['constantes']['ID_PERFIL_PROFESOR'] && $this->datos_sesion->grupo) {// SI ES ALUMNO LO INSERTA           
                $addUsuario = new Usuario();
                $formReg = new RegistroForm();
                $formReg->setData($request->getPost());
                if (isset($user_profile) && !empty($user_profile)) {
                    
                    $_arraUserPerfile = $this->formatoRedesSociales($user_profile, $estatus, $timestamp, $provider);
                    $addUsuario->intercambiarArrayRedesSociales($_arraUserPerfile);
                    $perfilAlum = $config['constantes']['ID_PERFIL_ALUMNO'];
                    $_intIdServicio = $config['constantes']['ID_SERVICIO_EVALUACIONES'];
                    $regModel = $this->getRegistroModel();

                    $resp = $regModel->registrarusuario($addUsuario, $perfilAlum);
                    $_intIdUsuarioPerfil = $resp['lastGeneratedValue']['idUsuarioPerfil'];
                    $_fechaVigencia = $this->wsCliente->activaLicenciaUsuario($addUsuario, $_intIdServicio, $_intIdUsuarioPerfil, $this->datos_sesion->llave);

                    $_arrVigencia = explode('_', $_fechaVigencia);
                    $_intDias = $_arrVigencia[0];
                    $_intIdPaquete = $_arrVigencia[1];

                    $idExamenUsuario = $regModel->insertarExamenesUsuario($_intIdUsuarioPerfil, $_intIdPaquete, $this->datos_sesion->llave, $_intDias);
                    $idGrupo = $regModel->ligarUsuarioGrupo($_intIdUsuarioPerfil, $this->datos_sesion->grupo);
                    
                    $this->datos_sesion->getManager()->getStorage()->clear();
                    $this->registrarsesion($addUsuario, $resp['lastGeneratedValue'], $perfilAlum);
                    $arrRoute = $this->redireccionarPerfil();
                    $this->flashMessenger()->addMessage($resp['msg']);
                    return $this->redirect()->toRoute($arrRoute[0], $arrRoute[1]);
                } 
            } else {
                if ($this->datos_sesion->perfil == $config['constantes']['ID_PERFIL_PROFESOR']) {
                    $resp['msg'] = 'Profesor, tus datos de registro son incorrectos, recuerda que no necesitas registrar un código de grupo.';
                } else {
                    $resp['msg'] = 'Tus datos de registro son incorrectos.';
                }
            }
        }else{
             $resp['msg'] = 'Este correo ya fue registrado antes';
        }
        $this->flashMessenger()->addMessage($resp['msg']);
        return $this->redirect()->toRoute($arrRoute[0], $arrRoute[1]);
    }

    
    public function recuperaPassAction() {
		$request = $this->getRequest ();
		$_strCorreo = $request->getPost ( '_strCorreo' );
		$_bolRecuperaPass = true;
		$_strCorreo = trim ( $_strCorreo );
		
		// validar que exista corre
		$_arrExisteCorreo = $this->getRegistroModel()->existeCorreo ( $_strCorreo, $_bolRecuperaPass );
		
		//$this->_objLogger->debug ( "VALOR DE EXISTE CORREO-> " . count ( $_arrExisteCorreo ) );
		
		$_boolExisteCorreoUser = $this->getRegistroModel()->existeCorreoUser ( $_strCorreo );
		
		//$this->_objLogger->debug ( " CORREO  2-> " . print_r ( $_boolExisteCorreoUser, true ) );
		$_strMensaje = isset ( $_boolExisteCorreoUser ) && count ( $_boolExisteCorreoUser ) > 0 ? true : false;
		
		if (isset ( $_arrExisteCorreo ) && count ( $_arrExisteCorreo ) > 0) {
			//$this->_objLogger->debug ( "PASO 1 " );
			// obtener la ruta absoluta y pasarla al model para envarla en el correo
			$url = $this->url ()->fromRoute ( 'login', array (
					'controller' => "login",
					"action" => "modificarpassword" 
			), array (
					'force_canonical' => true 
			) );
			
			$_arrDatosUsuario = $this->getRegistroModel()->generaCodigoRecuperacionPass ( $_strCorreo, $url );
			
			// $this->_objLogger->debug("PASO 2 ".print_r($_arrDatosUsuario,true));
			
			if (isset ( $_arrDatosUsuario ) && count ( $_arrDatosUsuario ) > 0) {
				$_strnombre = $_arrDatosUsuario ['nombre'];
				$_strCodigo = $_arrDatosUsuario ['codigo'];
				$_arrRespuesta ['respuesta'] = true;
				$_arrRespuesta ['msg'] = "Se ha enviado un e-mail a tu cuenta de correo con las instrucciones necesarias para resetear tu contrase&ntilde;a.";
			} else {
				
				//$this->_objLogger->debug ( "PASO else 2 " );
				$_arrRespuesta ['respuesta'] = false;
				$_arrRespuesta ['msg'] = "Hubo un error en el sistema, favor de intentar m&aacutes tarde";
			}
		} else {
			$_arrRespuesta ['respuesta'] = false;
			$_arrRespuesta ['msg'] = "La direcci&oacute;n de correo electrónico ingresada no existe en nuestros registros.";
		}
		
		// Se manda llamar el correo
		$response = $this->getResponse ();
		$response->setContent ( \Zend\Json\Json::encode ( $_arrRespuesta ) );
		return $response;
	}
    
        
        /**
         * 
         * vista para cambiar el pass
         */
        function modificarpasswordAction() {
		$request = $this->getRequest ();
		$_strCodigo = $request->getQuery ( 'idCodigo' );
		
		// verificar que exista el codigo en la tabla
		$arrRespCodigo = $this->getRegistroModel()->checkCodeRecuperacion ( $_strCodigo );
		if (count ( $arrRespCodigo ) > 0) {
			//$this->_objLogger->debug ( "VALOR DE CODIGO " . $_strCodigo );
			$viewModel = new ViewModel ( array (
					'_strCodigo' => $_strCodigo 
			) );
			//$viewModel->setTerminal ( true );
			return $viewModel;
		} else {
			$viewModel = new ViewModel ();
			//$viewModel->setTerminal ( true );
			$viewModel->setTemplate ( "error/forbidden" );
			return $viewModel;
		}
	}
        
        /**
	 * cambiar el password usuario
	 * 
	 * @access public
	 * @author LAFC
	 *        
	 */
	public function cambiarpassajaxAction() {
		$request = $this->getRequest ();
		$_strNewPass = $request->getPost ( "_strPassword" );
		$_strCode = $request->getPost ( "_strCode" );
		
		// volver a verificar si existe el codigo y obbtener los datos del usuario
		$_arrCodigo = $this->getRegistroModel()->checkCodeRecuperacion ( $_strCode );
		if (isset ( $_strNewPass ) && $_strNewPass != "" && count ( $_arrCodigo ) > 0) {
			$_boolResp = $this->getRegistroModel()->updatePassword ( $_arrCodigo [0] ['t01id_usuario'], $_strNewPass );
			
			$response = $this->getResponse ();
			$response->setContent ( \Zend\Json\Json::encode ( $_boolResp ) );
			return $response;
		}
		
		// TODO : regresar error
		$response = $this->getResponse ();
		$response->setContent ( \Zend\Json\Json::encode ( false ) );
		return $response;
	}
        
        
        
}
