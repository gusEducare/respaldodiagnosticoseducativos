<?php
/**
 * @author oreyes
 * @author jpgomez
 * @since 02/12/2014
 * @version  Examenes1.0
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Login\Controller\Login'    => 'Login\Controller\LoginController'
            ),
        ),
    'router' => array(
        'routes' => array(
            'login' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'    => '/login[/[:action]]',
                    'constraints' => array(
                        'action'  =>  '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                        'controller' => 'Login\Controller\Login',
                        'action'     => 'index',
                        ),
                ),
                ),
            ),
        ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'login/login/index' => __DIR__ . '/../view/login/index.phtml'
            ),
        'template_path_stack' => array(
            'examenes' => __DIR__ . '/../view',
            ),
        ),
    );
