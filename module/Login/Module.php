<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Login;



use Login\Model\RegistroModel;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\Adapter\Adapter;
use Zend\Session\Container;
class Module
{
    //Inicia bloque para control de acceso LOGIN
    public function onBootstrap(MvcEvent $e) {        
        $sharedEvents = $e->getApplication()->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', array($this, 'verAcl'));
    }
    
    private  function toCamelCase($str, array $noStrip = array())
    {
            $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
            $str = trim($str);
            
            $str = ucwords($str);
            $str = str_replace(" ", "", $str);
            $str = lcfirst($str);

            return $str;
    }
    
    public function verAcl(MvcEvent $e) {
        $config = include __DIR__ . '/../../config/autoload/global.php';
        $this->datos_sesion = new Container('user');
        $_intUserRole = ($this->datos_sesion->perfil) ? $this->datos_sesion->perfil : $config['constantes']['ID_PERFIL_INVITATO'];
        $_strRoute = $e -> getRouteMatch() -> getMatchedRouteName();
        $_strAccion = $e -> getRouteMatch() ->getParam('action');
        $_strAccion = $this->toCamelCase($_strAccion);
        
        if (!$e -> getViewModel() -> acl -> isAllowed($_intUserRole, $_strRoute,$_strAccion)) {
            $response = $e -> getResponse();
            $response -> getHeaders() -> addHeaderLine('Location', $e -> getRequest() -> getBaseUrl() . '/404');
            $response -> setStatusCode($config ['constantes']['ERROR_PERMISO_DENEGADO']);
            
            //error_log($_intUserRole.'<-NO PERMITIDO '.$_strRoute.'/'.$_strAccion);

        }else{
             //error_log($_intUserRole.'<-PERMITIDO '.$_strRoute.'/'.$_strAccion);
        }
    }
    //Termina bloque para control de acceso
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    public function getServiceConfig()
    {
    	return Array('factories'=>
    			array('Login\model\RegistroModel'=>function($sm){
    				$config = $sm->get('config');
                                $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    				$model 	   = new RegistroModel($dbAdapter,$config);
    				return $model;
    				})
    	);	
    }


    
}
