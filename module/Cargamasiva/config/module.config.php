<?php

return array(
		'controllers' => array(
				'invokables' => array(
						'Cargamasiva\Controller\Cargamasiva'  => 'Cargamasiva\Controller\CargamasivaController',
				),
		),
		
		'router' => array(
			'routes' => array(
				'cargamasiva' => array(
					'type' =>  'Segment',
					'options' => array(
						'route'    => '/cargamasiva[/[:action]]',
						'constraints' => array(
								'action'  =>  '[a-zA-Z][a-zA-Z0-9_-]*',
						),
						'defaults' => array(
							'controller' => 'Grupos\Controller\Cargamasiva',
							'action'     => 'index',
						),
					),
				),
			),
		),
		
		'view_manager' => array(
			'display_not_found_reason' => true,
			'display_exceptions'       => true,
			'doctype'                  => 'HTML5',
			'not_found_template'       => 'error/404',
			'exception_template'       => 'error/index',
			'template_map' => array(
				'cargamasiva/cargamasiva/validarplantilla' => __DIR__ . '/../view/cargamasiva/cargamasiva/validarPlantilla.phtml',
				'cargamasiva/cargamasiva/verconfig'		   => __DIR__ . '/../view/cargamasiva/cargamasiva/verConfig.phtml',
			),
			'template_path_stack' => array(
				'cargamasiva' => __DIR__ . '/../view',
			),
		),
);
