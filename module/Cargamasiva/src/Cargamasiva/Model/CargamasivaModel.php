<?php
namespace Cargamasiva\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class CargamasivaModel{
	
	const SERVICIO_CERTIFICACION = 3;
	
	const ESTATUS_INACTIVO = "I";
	/**
	 * 
	 * @param Adapter $adapter
	 */
        protected $_objLogger;
	public function __construct(Adapter $adapter){
		$this->adapter = $adapter;
		$this->sql= new Sql($adapter);
                $this->_objLogger = new Debug($config);
	
	}
	
	
	/*function ejemplocarga( $_usuario)
	{
	
			
	
		$_strQueryUsuarioLogeado='SELECT t01nombre,t01correo,t01bloqueada,t01fecha_registro,t01acepto_condiciones 
									FROM evaluaciones.t01usuario where t01nombre =:_usuario' ;
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_usuario'=> $_usuario
				
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
		
			
		$_arrResultado = $resultSet->toArray();
		
		
		
		return $_arrResultado;
	}*/
	
	
	function nombreExamen ( $_grupo)
	{
	
			
	
		$_strQueryUsuarioLogeado='select distinct t12.t12id_examen, t12.t12nombre from t05grupo t05
									inner join t08administrador_grupo t08 on t08.t05id_grupo = t05.t05id_grupo
									inner join  t02usuario_perfil t02 on t02.t02id_usuario_perfil = t08.t02id_usuario_perfil
									inner join t03licencia_usuario t03 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil
									inner join t04examen_usuario t04 on t04.t03id_licencia_usuario = t03.t03id_licencia_usuario
									inner join t12examen t12 on t12.t12id_examen = t04.t12id_examen
									inner join t01usuario t01 on t01.t01id_usuario = t02.t01id_usuario
									where t05.t05id_grupo=:_grupo and t01estatus = "A" and t08.t08usuario_ligado= "LIGADO"' ;
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_grupo'=> $_grupo
	
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return $_arrResultado;
	}
	
	function alumnosGrupo ( $_grupo)
	{
	
			
	
		$_strQueryUsuarioLogeado='select  t05.t05id_grupo, t01.t01nombre,t03.t03licencia  from t05grupo t05 
									inner join t08administrador_grupo t08 on t08.t05id_grupo = t05.t05id_grupo
									inner join  t02usuario_perfil t02 on t02.t02id_usuario_perfil = t08.t02id_usuario_perfil
									inner join t03licencia_usuario t03 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil
									inner join t01usuario t01 on t01.t01id_usuario = t02.t01id_usuario
									where t05.t05id_grupo=:_grupo and t02.c01id_perfil=1 and t01estatus = "A" and t08.t08usuario_ligado= "LIGADO"';
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_grupo'=> $_grupo
	
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return $_arrResultado;
	}
	
	function getNameFromNumber($num) { 
		$numeric = ($num - 1) % 26; 
		$letter = chr(65 + $numeric); 
		$num2 = intval(($num - 1) / 26); 
		
		if ($num2 > 0) { 
			return $this->getNameFromNumber($num2) . $letter; 
		} 
		
		else { 
			return $letter;
		 } 
	
	}
	
	function numeroPreguntas ( $_examen)
	{
	
			
	
		$_strQueryUsuarioLogeado='select count(t11.t11pregunta) as total from t12examen t12
									inner join t20seccion_examen t20 on t20.t12id_examen = t12.t12id_examen
									inner join t16seccion_pregunta t16 on t16.t20id_seccion_examen = t20.t20id_seccion_examen
									inner join t11pregunta t11 on t11.t11id_pregunta = t16.t11id_pregunta
									where t12.t12id_examen= :_examen';
			
			
	
		$statement = $this->adapter->query($_strQueryUsuarioLogeado);
	
		$results = $statement->execute(array(
				':_examen'=> $_examen
	
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
	
			
		$_arrResultado = $resultSet->toArray();
	
	
	
		return 	$_arrResultado[0]['total'];
	}
	
	/**
	 * Esta funcion verifica que el usuario existe.
	 *
	 * @param  String _strCorreo
	 * @return array $_arrRespuesta regresamos array de datos 
	 */
	function existeUsuario( $_strUsuario, $validarMail = false )
	{
		$_arrRespuesta = array();
			
		$select = $this->sql->select();
		$select->from(array('c17'=>'c01perfil'))
			   ->join(array('t02'=>'t02usuario_perfil'),
							 'c17.c01id_perfil = t02.c01id_perfil',
					  array('t02id_usuario_perfil'))
			   ->join (array('t01'=>'t01usuario'),
						't02.t01id_usuario = t01.t01id_usuario',
						array(	't01id_usuario',
								't01correo',
								't01nombre',
								't01apellidos',
								't01estatus'))
			  ->where('t01.t01correo ='. "'".$_strUsuario."'");
			  
		if(!$validarMail){
		  $select->where("t01.t01estatus  <> '".CargamasivaModel::ESTATUS_INACTIVO."'");	  	
		}
		
		$select->columns(array());

		$statement = $this->sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		$_arrRespuesta = $resultSet->toArray();

		return $_arrRespuesta;
	}
	
	/**
	 * Funcion valida que el correo existe en la tabla user.
	 * @param unknown $_strCorreo
	 * @return boolean Return booleano, true en caso que exista, false caso contrario.
	 */
	function existeCorreoUser($_strCorreo)
	{
		$_bolExiste = false;
	
		$_arrRespuesta = array();
			
		$select = $this->sql->select();
		$select	->from(array('u'=>'user'))
				->where(array('u.email'=> $_strCorreo));
	
		$statement = $this->sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();
	
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrRespuesta = $resultSet->toArray();
	
		return $_arrRespuesta;
	}
	
	
	
	/**
	 * agregar usuario con su codigo a un grupo.
	 * @param unknown $_strCodigo
	 * @param unknown $_intIdServicio
	 * @param unknown $_intIdUsuarioServicio
	 * @return multitype:boolean string
	 */
	public function agregarUsuarioCodigoAgrupo($_strCodigo,$_intIdServicio,$_intIdUsuarioServicio){
	
		$_arrRespuesta = array();
		$_arrDatosCod = $this->decodificar($_strCodigo);
	
		if($_arrDatosCod['Id']== NULL){
			$_arrRespuesta['accion']= false;
			$_arrRespuesta['msg']= 'El cup&oacute;n no es valido';
		}elseif($_arrDatosCod['Servicio'] != $_intIdServicio){
			$_arrRespuesta['accion']= false;
			$_arrRespuesta['msg']= 'El cup&oacute;n no pertenece al servicio';
		}else{
	
			switch ($_arrDatosCod['IdTipo']){
				case GruposTable::GRUPO:
	
				$_boolExisteAlumnoGrupo = $this->verificarUsuarioGrupo($_strCodigo,$_intIdUsuarioServicio,GruposTable::TIPO_EXISTE_USUARIO);
	
	
				if($_boolExisteAlumnoGrupo){
						
					//Verificar estatus de usuario
					if($_boolExisteAlumnoGrupo[0]['ge_t08ligado_alumno'] == 'D' ){
						$_estatus = 'L';
						$_Resp =$this->cambiarEstatusUsuario($_arrDatosCod['Id'], $_boolExisteAlumnoGrupo[0]['ge_t03id_usuario_servicio_perfil_colegio'], $_estatus);
						if($_Resp){
							$_arrRespuesta['accion'] =true;
							$_arrRespuesta['msg'] ='Ahora perteneces al grupo.';
						}else{
							$_arrRespuesta['accion']= false;
							$_arrRespuesta['msg']= 'Ocurri&oacute; un error al procesar la solicitud, por favor intenta m&aacute;s tarde.';
						}

					}else{
						$_arrRespuesta['accion']= false;
						$_arrRespuesta['msg']= 'Tu cuenta de usuario ya tiene un perfil de Alumno ligado a este grupo.';
					}
						
				}else{
					//verificar que tenga el perfil de alumno.
					$_intExistePerfil = $this->verificaPerfilAlumno($_intIdUsuarioServicio);
						
					if(!$_intExistePerfil ){
						//insertarlo
						$_idUSPC = $this->insertaPerfil($_intIdUsuarioServicio,GruposTable::PERFIL_ALUMNO);
					}else{
						$_idUSPC = $_intExistePerfil;
					}
						
					//agregar en la tabla ge_t08alumno_grupo
					$_res=$this->insertarAlumnoGrupo($_idUSPC,$_arrDatosCod['Id'], $_arrDatosCod['IdGenerico']);
						
					if($_res){
						$_arrRespuesta['accion']= true;
						$_arrRespuesta['msg']= 'Ahora perteneces al grupo.';
					}else{
						$_arrRespuesta['accion']= false;
						$_arrRespuesta['msg']= 'Ocurri&oacute; un error al procesar la solicitud, por favor intenta m&aacute;s tarde.';
					}
				}
				break;		
			}
		}
		return $_arrRespuesta;
	}
	
	/**
	 * Obtiene los datos del usuario para este servicio
	 * @param string $_strCorreo
	 * @return array{
	 * 				ge_t02id_usuario_servicio,
	 * 				ge_t01id_usuario,
	 * 				ge_t01nombre,
	 * 				ge_t01apellidos,
	 * 				ge_t01estatus}
	 */
	function existeCorreo( $_strUsuario )
	{
		$_arrRespuesta = array();
			
		$select = $this->sql->select();
		$select->from(array('t01'=>'ge_t01usuario'))
			   ->join(array('t02'=>'ge_t02usuario_servicio'),
							't01.ge_t01id_usuario = t02.ge_t01id_usuario',
					  array('ge_t02id_usuario_servicio','ge_c17id_servicio'))
				->where('t01.ge_t01correo = '. "'".$_strUsuario."'")
				->where('t02.ge_c17id_servicio = '.CargamasivaModel::SERVICIO_CERTIFICACION)
				->columns(array('ge_t01id_usuario',
								'ge_t01nombre',
								'ge_t01apellidos',
								'ge_t01estatus',
								'ge_t01contrasena',
								'ge_t01correo'));
	
		$statement = $this->sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		$resultSet = new ResultSet;
		$resultSet->initialize($result);

		$_arrRespuesta = $resultSet->toArray();

		return $_arrRespuesta;
	}
	public function validalicencia ($licencia) {
            
	
                $_strQueryExisteCorreo='SELECT 
                                            t03licencia,
                                            t03fecha_activacion,
                                            t03dias_duracion,t03estatus,
                                            DATE_ADD(t03fecha_activacion,
                                            INTERVAL t03dias_duracion DAY) ,
                                            DATEDIFF(DATE_ADD(t03fecha_activacion,
                                            INTERVAL t03dias_duracion DAY),
                                            NOW()) as dias_restantes
                                            FROM
                                            evaluaciones.t03licencia_usuario
                                            where
                                            t03licencia = :licencia';



                $statement = $this->adapter->query($_strQueryExisteCorreo);


                $results = $statement->execute(array(
                                ':licencia'=> $licencia
                ));

                $resultSet = new ResultSet;
                                $resultSet->initialize($results);


                                $_arrResultado = $resultSet->toArray();

                                     

               return $_arrResultado;
          
        }
	
}