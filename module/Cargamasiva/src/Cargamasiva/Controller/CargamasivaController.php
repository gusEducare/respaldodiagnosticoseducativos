<?php

namespace Cargamasiva\Controller;

use Zend\Http\Client\Adapter\fwrite;

use Cargamasiva\Model\ClientWSPortalGE;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

//Librerias para manipular excel
use PHPExcel;
use \PHPExcel_IOFactory;
use \PHPExcel_Style_Protection;
use \PHPExcel_Style_Fill;
use \PHPExcel_Style_Alignment;
use \PHPExcel_Worksheet;
use \PHPExcel_Style_Border;

//librerias para objeto logger
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

class CargamasivaController extends AbstractActionController
{	
	
	/**
	 * Para los mensajes log.
	 * @var Logger $log : instancia de la clase Logger
	 */
	protected $log;
	
	/**
	 * Constructor de la clase.
	 * crea una instancia del objeto logger para mensajes en el log de apache.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 02/10/2014
	 * 
	 */
	public function __construct()
	{
		$this->log = new Logger();
		$this->log->addWriter(new Stream('php://stderr'));	
                $this->datos_sesion = new Container('user');
	}
	/**fwrite
	 * Funcion para mostrar la pantalla inicial de la carga masiva.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 17/09/2014
	 * @param $codigoGrupo : Codigo del grupo generado por GE.
	 * @param $nombreGrupo : Nombre o descripcion del grupo.
	 * @return View index.phtml : Vista principal para carga masiva.
	 */
	public function indexAction()
	{
		$request = $this->getRequest();

		$codigoGrupo 			= $this->request->getPost('_strCodigo');
 		$nombreGrupo 			= $this->request->getPost('_strGrupo');
		
		return new ViewModel(
				Array(
					'_intCodigoGrupo' => $codigoGrupo, 
					'_strGrupo'		  => $nombreGrupo
					));
	}
	
	/**
	 * Generar un archivo de plantilla en extension XLS
	 * y mostrarlo al usuario para que pueda cargar sus usuarios
	 * la plantilla contiene: 
	 * 		- Nombre.
	 * 		- Apellidos.
	 * 		- Usuario.
	 * 		- Contraseña.
	 * 		- Licencia.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 17/09/2014
	 * @return Plantilla.xls : Archivo XLS para carga de alumnos.
	 */
	public function descargarPlantillaAction()
	{	
		// Crear nuevo objeto PHPExcel.
		$objPHPExcel = new PHPExcel();
		
		// Setear las propiedades del documento
		$objPHPExcel->getProperties()->setCreator("Certificacion")
									 ->setLastModifiedBy("Certificacion")
									 ->setTitle("Certificacion Captura Usuarios -".date('d_m_Y-h_i_s_A'))
									 ->setSubject("Certificacion Captura Usuarios")
									 ->setDescription("Archivo Autogenerado para la captura de usuarios Certificacion")
									 ->setKeywords("office 2007 openxml php")
									 ->setCategory("Archivo Certificacion");
		
		//mostrar guias
		$objPHPExcel->getActiveSheet()->setShowGridlines(true);
		
		//Columnas
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A1', 'Nombre')
					->setCellValue('B1', 'Apellidos')
					->setCellValue('C1', 'Usuario')
					->setCellValue('D1', 'Contraseña')
					->setCellValue('E1', 'Licencia');
		
		//proteger las celdas de titulos
		$objPHPExcel->getActiveSheet()->getStyle("A1")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle("B1")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle("C1")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle("D1")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		$objPHPExcel->getActiveSheet()->getStyle("E1")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
		
		//establecer autosize para las columnas
		$objPHPExcel->getActiveSheet()->getColumnDimension("A")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("B")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("C")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("D")->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension("E")->setAutoSize(true);
		
		$style_header = array(
			'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb'=>'E1E0F7'),
			)
		);
		
		//agregar alineamiento para el encabezado
		$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
		
		//agregar color de fondo para el enzabezado
		$objPHPExcel->getActiveSheet()->getStyle("A1:E1")->applyFromArray( $style_header );
		
		$style_header = array(
			'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb'=>'E6E6E6'),
			)
		);
		
		//agregar selec para el sexo y colorear las filas
		for($itr = 2; $itr <=102 ; $itr++){
			if($itr % 2 == 0){
				$objPHPExcel->getActiveSheet()->getStyle("A".$itr.":e".$itr)->applyFromArray( $style_header );
			}
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('certificacion'.date('d_m_Y'));
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="formato_carga_masiva'.date('d_m_Y-h_i').'.xls"');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
		
		$objWriter->save('php://output');
	}
	
	/*public function descargarPlantillaExamenAction()
	{
		// Crear nuevo objeto PHPExcel.
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->getProperties()->setCreator("Certificacion")
									->setLastModifiedBy("Certificacion")
									->setTitle("Certificacion Captura Usuarios -".date('d_m_Y-h_i_s_A'))
									->setSubject("Certificacion Captura Resultados")
									->setDescription("Archivo Autogenerado para la captura de resultados Certificacion")
									->setKeywords("office 2007 openxml php")
									->setCategory("Archivo Certificacion");
		
		
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(12);
		
		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		
		$a=1;
		$y=3;
		$_examen=7;
		$i=1;
		
		$_intNumeroPreguntas = $this->getCargamasivaModel()->numeroPreguntas($_examen );
	
		
		
		
		$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$a, 'Examen')
					->setCellValue('A'.$y, 'Nombre')
					->setcellValue('B'.$y, 'Licencia');
		
	
			for ($j=1,$i=3;$j<=$_intNumeroPreguntas;$i++,$j++){
				
				$_strCaracter=$this->getCargamasivaModel()->getNameFromNumber($i);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setcellValue($_strCaracter.$y, 'P'.$j);
			}
			
		
		
			
		$objPHPExcel->getActiveSheet()
					->getStyle('A1:A3:E3')
					->getFill()
					->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()->setARGB('FFEEEEEE');
		
		$borders = array(
				'borders' =>array(
						'allborders' =>array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => 'FF000000'),
				)
			),
		);
		
		$objPHPExcel->getActiveSheet()
					->getStyle('B2:A3:E3')
					->applyFromArray($borders);
		
		//AQUI VA LA CONSULTA  ejemplocarga  Cargamasiva\model\CargamasivaModel
		$idgrupo='30';
		$_arrResp = $this->getCargamasivaModel()->nombreExamen($idgrupo );
		$_arrusuarios = $this->getCargamasivaModel()->alumnosGrupo($idgrupo );
		
		foreach ($_arrResp as $row){
			foreach ($_arrusuarios as $row2){
		//WHILE($row=mysql_fetch_array($_arrResp)){

			$y++;
			
			$objPHPExcel->setActiveSheetIndex(0)
						->getStyle('A'.$y,'E'.$y)
						->applyFromArray($borders);
			
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$a,$row['t12id_examen'])
						->setCellValue('C'.$a,$row['t12nombre'])
						
						->setCellValue('A'.$y,$row2['t01nombre'])
						->setCellValue('B'.$y,$row2['t03licencia']);

			}
		}
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;');
		header('Cache-Control: max-age=0');
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
		$path='public/';
		$filename="formato_carga_masiva_resultado'.date('d_m_Y-h_i').'.xls";
		$objWriter->save($path.$filename);
		
	}*/
	/**
	 *
	 * Funcion que crea un objeto de tipo request
	 * para extraer los datos del archivo excel y el codigo del grupo al que se le van asignar los alumnos
	 * una vez extraidos estos datos se valida con dos foreach el objPHPExcel para validar que cada correo de los alumnos
	 * no este ligado a este grupo posterior a la validacion,
	 * se envia a la vista el nombre del usuario, el apellido, el codigo del grupo al que se le van asignar los alumnos
	 * y un parametro para evaluar si algun alumno ya pertenece a ese grupo.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 17/09/2014
	 * @return View CargaMasiva.phtml : Vista para la edicion de erorres en la carga masiva.
	 */
	public function validarplantillaAction()
	{
		$_boolExit 			= false;
		$_arrUsuarioExiste 	= array();
		$_strCodigo 		= "";
		
		$config 			= $this->getServiceLocator()->get('Config');
		$clientWS = new ClientWSPortalGE($config['constantes']['WSDL_PORTALGE']);
		//$clientWS  			= ClientWSPortalGE::getInstance();
		//ClientWSPortalGE::setWsdl($config["constantes"]["wsdl_portalGE"]);
		
		$request  			= $this->getRequest();
		$validarCorreo		= true;
		$_arrUsuarios 		= array();
		
		if(isset($_FILES['carga_masiva']) && ($_FILES['carga_masiva']['type'] == "application/octet-stream" || $_FILES['carga_masiva']['type'] == "application/vnd.ms-excel" || $_FILES['carga_masiva']['type'] == "vnd.oasis.opendocument.spreadsheet")){
			$_objPHPExcel = PHPExcel_IOFactory::load($_FILES['carga_masiva']['tmp_name']);
			$_strCodigo =  "HYSFUIHASODIFJASDJFIOAS";//$request->getPost('_strCodigoGrupo');
			$_iteradorRow = 1;
			$_intSalir = 0;
			foreach($_objPHPExcel->getActiveSheet()->getRowIterator(2) as $row){
				$_iteradorCell = $row->getCellIterator();
				$_iteradorCell->setIterateOnlyExistingCells(false);
				$itr4Cell = 1;
				$arrDatos = array();
				foreach ($_iteradorCell as $cell){
					$_strValorCelda = $cell->getValue();
					$arrDatos[$itr4Cell]	= $_strValorCelda;
					if($itr4Cell == 3 && $_strValorCelda != ""){
						$_arrResp = $this->getCargamasivaModel()->existeUsuario( $_strValorCelda, $validarCorreo );
						if(count($_arrResp) > 0){
							//se crea una arreglo con los nombres de usuarios que ya existen en la bd 
							//para poder indicarlo en la vista
							array_push($_arrUsuarioExiste, strtolower($_arrResp[0]['ge_t01correo']));
						}
					}
					$itr4Cell++;
				}
				array_push($_arrUsuarios,$arrDatos);
				$_iteradorRow++;
			}
		}
		return new ViewModel(array(
				'_strNombre'				=> $this->datos_sesion->nombreUsuario,
				'_strApellido'				=> $this->datos_sesion->apellidos,
				'_arrUsuarios'				=> $_arrUsuarios,
				'_codigoGrupo'  			=> $_strCodigo,
				'_arrUsuarioExiste'			=> $_arrUsuarioExiste,
				'_objWs'					=> $clientWS,
				'_SERVICIO_CERTIFICACION_GE'=> $config['constantes']['SERVICIO_CERTIFICACION_GE']));
	}
	
        
        
        public function validarplantillaexamenAction()
	{
		//$_boolExit 			= false;
		//$_arrUsuarioExiste 	= array();
		$_strCodigo 		= "";
		
		$config 			= $this->getServiceLocator()->get('Config');
		$clientWS = new ClientWSPortalGE($config['constantes']['WSDL_PORTALGE']);
		//$clientWS  			= ClientWSPortalGE::getInstance();
		//ClientWSPortalGE::setWsdl($config["constantes"]["wsdl_portalGE"]);
		
		//$request  			= $this->getRequest();
		$validarCorreo		= true;
		$_arrUsuarios 		= array();
		
		if(isset($_FILES['carga_masiva']) && ($_FILES['carga_masiva']['type'] == "application/octet-stream" || $_FILES['carga_masiva']['type'] == "application/vnd.ms-excel" || $_FILES['carga_masiva']['type'] == "vnd.oasis.opendocument.spreadsheet")){
			$_objPHPExcel = PHPExcel_IOFactory::load($_FILES['carga_masiva']['tmp_name']);
			$_strCodigo =  "HYSFUIHASODIFJASDJFIOAS";//$request->getPost('_strCodigoGrupo');
			$_iteradorRow = 1;
			$_intSalir = 0;
			foreach($_objPHPExcel->getActiveSheet()->getRowIterator(2) as $row){
				$_iteradorCell = $row->getCellIterator();
				$_iteradorCell->setIterateOnlyExistingCells(false);
				$itr4Cell = 1;
				$arrDatos = array();
				foreach ($_iteradorCell as $cell){
					$_strValorCelda = $cell->getValue();
					$arrDatos[$itr4Cell]	= $_strValorCelda;
					if($itr4Cell == 3 && $_strValorCelda != ""){
						//$_arrResp = $this->getCargamasivaModel()->existeUsuario( $_strValorCelda, $validarCorreo );
						if(count($_arrResp) > 0){
							//se crea una arreglo con los nombres de usuarios que ya existen en la bd 
							//para poder indicarlo en la vista
							//array_push($_arrUsuarioExiste, strtolower($_arrResp[0]['ge_t01correo']));
						}
					}
					$itr4Cell++;
				}
				array_push($arrDatos);
				$_iteradorRow++;
			}
		}
		return new ViewModel(array(
				'_strNombre'				=> $this->datos_sesion->nombreUsuario,
				'_strApellido'				=> $this->datos_sesion->apellidos,
				'_arrUsuarios'				=> $arrDatos,
				'_codigoGrupo'  			=> $_strCodigo,
				//'_arrUsuarioExiste'			=> $_arrUsuarioExiste,
				'_objWs'					=> $clientWS));
				//'_SERVICIO_CERTIFICACION_GE'=> $config['constantes']['SERVICIO_CERTIFICACION_GE']
	}
	/**
	 * Funcion que procesa los datos en caso de estar correctos se crea un objeto request
	 * posteriormente se extrae el arreglo de alumnos y el codigo del grupo, 
	 * evalua que el arreglo de alumnos no este vacio y posteriormente recorrer a 
	 * todos los alumnos para saber si estan registrados en caso de que existan solo agrega los usuarios al grupo
	 * caso contrario registra a los usuarios, actualiza los datos y los agrega al grupo deseado.
	 *
	 * @author oreyes@grupoeducare.com
	 * @since 17/09/2014
	 * @return Response $response : response Json que nos muestra un arreglo con dos variables falso en caso de error o true caso contrario y la leyenda msg para saber las razones del error.
	 */
	public function procesarcargamasivaAction()
	{
		$config  = $this->getServiceLocator()->get("Config");
		$request = $this->getRequest();
		
		if($request->getPost('_boolValidarUsuario')){
			$_arrRespUser 	= array();
			$_strUser 		= $request->getPost('user');
			$arrResp 		= $this->getCargamasivaModel()->existeUsuario($_strUser);
			if(count($arrResp) > 0){
				$_arrRespUser["_boolResponse"] 	= true;
				$_arrRespUser["_strMsg"]		= "El usuario ya existe solicitar ayuda con soporte t&eacute;cnico GE.";
			} elseif (strlen($_strUser) < 6) {
				$_arrRespUser["_boolResponse"] 	= true;
				$_arrRespUser["_strMsg"]		= "El usuario debe contener 6 o m&aacute;s caracteres.";
			} elseif (preg_match("/\s+|\W+/", $_strUser)) {
				if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_strUser)){ 
					$_arrRespUser["_boolResponse"] 	= false;
					$_arrRespUser["_strMsg"]		= "Usuario correcto.";
				} else {
					$_arrRespUser["_boolResponse"] 	= true;
					$_arrRespUser["_strMsg"]		= "Usuario con caracteres especiales o espacios en blanco no permitidos.";
				}	
			
			} else {
				$_arrRespUser["_boolResponse"] 	= false;
				$_arrRespUser["_strMsg"]		= "Usuario correcto";
			}
			$response = $this->getResponse();
			$response->setContent(\Zend\Json\Json::encode($_arrRespUser));
			return $response;
		}else if($request->getPost('_boolValidarLlave')){
			$_boolResponse 	= false;
			$_strLicencia = $request->getPost('_strLicencia');
			$_objWs  = new ClientWSPortalGE($config["constantes"]["WSDL_PORTALGE"]);
			//$_objWs  = ClientWSPortalGE::getInstance();
			$_arrResponse = $_objWs->llaveValidaMensaje($_strLicencia,$config["constantes"]["SERVICIO_CERTIFICACION_GE"]);
				
			$response = $this->getResponse();
			$response->setContent(\Zend\Json\Json::encode($_arrResponse));
			return $response;
		}else{
			$_arrAlumnos = $request->getPost('_arrAlum');
			$_strCodigo = $request->getPost('_strCodigo');
			$_objWs  = new ClientWSPortalGE($config["constantes"]["WSDL_PORTALGE"]);
			//$_objWs  = ClientWSPortalGE::getInstance();
			if(isset($_arrAlumnos)){
				foreach ($_arrAlumnos as $_alum){
					$_arrRespUser = $this->getCargamasivaModel()->existeUsuario($_alum[2], $_alum[3],false, true);
					if( count($_arrRespUser) > 0){
						$_arrRespuesta = $this->getCargamasivaModel()->agregarUsuarioCodigoAgrupo($_strCodigo,GruposController::CERTIFICACION , $_arrRespUser[0]['ge_t02id_usuario_servicio']);
						$_intUsuarioServicio = $_arrRespUser[0]['ge_t03id_usuario_servicio_perfil_colegio'];
					}else{
							
						$_arrResp  		= $this->getCargamasivaModel()->registrarUsuario($_alum[2], $_alum[3]);
						$_arrResp2 		= $this->getCargamasivaModel()->actualizarDatosUsuario($_alum[0],$_alum[1], GruposController::ALUMNO,$_arrResp['ge_t02id_usuario_servicio'],$_arrResp['ge_t01id_1usuario']);
						$_arrRespuesta 	= $this->getCargamasivaModel()->agregarUsuarioCodigoAgrupo($_strCodigo,GruposController::CERTIFICACION,$_arrResp['ge_t02id_usuario_servicio']);
	
						//asignar licencia
						$_intUsuarioServicio = $_arrResp2[0]['ge_t03id_usuario_servicio_perfil_colegio'];
					}
						
					$_strFechaVencimineto = $_objWs->activarExamen($config["constantes"]["SERVICIO_CERTIFICACION_GE"], $_intUsuarioServicio, $_alum[4],$_alum[2], $_alum[0]." ".$_alum[1]);
						
					list($_strFin_vigencia, $_intId_Servicio, $_strNombre_Examen) =  split('[_]',$_strFechaVencimineto[0]);
					$this->getCargamasivaModel()->insertarLicenciaUsuario($_intUsuarioServicio, $_alum[4], $_intId_Servicio, $_strFin_vigencia, $_strNombre_Examen);
				}
			}else{
				$_arrRespuesta['accion']= false;
				$_arrRespuesta['msg']= 'No existen alumnos para ligar al grupo.';
			}
			$response = $this->getResponse();
			$response->setContent(\Zend\Json\Json::encode($_arrRespuesta));
			return $response;
		}
	}
	
	/**
	 * Funcion que regresa la clase CargamasivaModel (Modelo) instanciada en el service manager para asociar con el controlador.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 26/09/2014
	 * @return CargamasivaModel $model : Instancia de la clase UsuarioTable.
	 * 
	 */
	public function getCargamasivaModel()
	{
		$sm = $this->getServiceLocator();
		$model = $sm->get('Cargamasiva\Model\CargamasivaModel');
	
		return $model;
	}
}