 <?php
return array(
		'router' => array(
				'routes' => array(
						
						'agenda' => array(
								'type' =>  'Segment',
								'options' => array(
										'route'         => '/agenda[/[:action]][/:id]',
										'constraints'   => array(
												'controller'=> '[a-zA-Z][a-zA-Z0-9_-]*',
												'action'    =>  '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'        =>  '[0-9]*',
										),
										'defaults'          => array(
												'controller'    => 'agenda',
												'action'        => 'agenda',
											
										),
								),
						),
						'calendario' => array(
								'type' =>  'Segment',
								'options' => array(
										'route'         => '/calendario[/[:action]][/:id]',
										'defaults'          => array(
												'controller'    => 'calendario',
												'action'        => 'calendario',
										),
									), 
								),
						'mes' => array(
								'type' =>  'Segment',
								'options' => array(
										'route'         => '/mes[/[:action]][/:id]',
										'defaults'          => array(
												'controller'    => 'mes',
												'action'        => 'mes',
										),
								),
						),
						
				
		
// 		'agenda' => array(
// 				'type' =>  'Segment',
// 				'options' => array(
// 						'route'         => '/agenda[/[:action]][/:id]',
// 						'defaults'          => array(
// 								'controller'    => 'agenda',
// 								'action'        => 'agenda2',
// 						),
// 				),
// 		),
		
	),
),

		'service_manager' => array(
				'abstract_factories' => array(
						'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
						'Zend\Log\LoggerAbstractServiceFactory',
				),
				'aliases' => array(
						'translator' => 'MvcTranslator',
				),
		),

		'translator' => array(
				'locale' => 'en_US',
				'translation_file_patterns' => array(
						array(
								'type'     => 'gettext',
								'base_dir' => __DIR__ . '/../language',
								'pattern'  => '%s.mo',
						),
				),
		),

		'controllers' => array(
				'invokables' => array(
			   'agenda' => 'Agenda\Controller\AgendaController',
				'calendario' => 'Agenda\Controller\CalendarioController',
				'mes' => 'Agenda\Controller\MesController',
				'agenda2' => 'Agenda\Controller\AgendaController',
				)
		),

		'view_manager' => array(
				'display_not_found_reason' => true,
				'display_exceptions'       => true,
				'doctype'                  => 'HTML5',
				'not_found_template'       => 'error/404',
				'exception_template'       => 'error/index',
				'template_map' => array(
						//         'examenes/preguntas/index' => __DIR__ . '/../view/examenes/pregunta.phtml',
						//          'examenes/preguntas/preguntas' => __DIR__ . '/../view/examenes/preguntas/preguntas.phtml',
						//           'examenes/preguntas/pregunta' => __DIR__ . '/../view/examenes/preguntas/pregunta.phtml',
// 						'examenes/preguntas/ejemplodragdrop'=>__DIR__.'/../view/examenes/preguntas/ejemplodragdrop.phtml',
// 						'examenes/preguntas/preguntamultiple'=>__DIR__.'/../view/examenes/preguntas/preguntaOpcionMultiple.phtml',

// 						'examenes/examenes/plantilla'=>__DIR__.'/../view/examenes/examenes/plantilla.phtml',
						'agenda/agenda/agenda'=>__DIR__.'/../view/agenda/agenda/agenda.phtml',
						'agenda/mes/mes' => __DIR__ . '/../view/agenda/mes/mes.phtml',
						'agenda/calendario/calendario' => __DIR__ . '/../view/agenda/calendario/calendario.phtml',
						'agenda/agenda/agenda2' => __DIR__ . '/../view/agenda/agenda/agenda2.phtml',
				),
				'template_path_stack' => array(
						'agenda' => __DIR__ . '/../view',
						
						__DIR__ . '/../view',
				),
		),
		// Placeholder for console routes
		'console' => array(
				'router' => array(
						'routes' => array(
						),
				),
		),
);








// return array(
//      'controllers' => array(
//          'invokables' => array(
//              'Agenda\Controller\Agenda' => 'Agenda\Controller\AgendaController',
//          ),
//      ),

//      // The following section is new and should be added to your file
//      'router' => array(
//          'routes' => array(
//              'agenda' => array(
//                  'type'    => 'segment',
//                  'options' => array(
//                      'route'    => '/agenda[/][:action][/:id]',
//                      'constraints' => array(
//                          'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                          'id'     => '[0-9]+',
//                      ),
//                      'defaults' => array(
//                          'controller' => 'Agenda\Controller\Agenda',
//                          'action'     => 'agenda',
//                      ),
//                  ),
//              ),
//          ),
//      ),
	
// 		'service_manager' => array(
// 				'abstract_factories' => array(
// 						'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
// 						'Zend\Log\LoggerAbstractServiceFactory',
// 				),
// 				'aliases' => array(
// 						'translator' => 'MvcTranslator',
// 				),
// 		),
		
// 		'translator' => array(
// 				'locale' => 'en_US',
// 				'translation_file_patterns' => array(
// 						array(
// 								'type'     => 'gettext',
// 								'base_dir' => __DIR__ . '/../language',
// 								'pattern'  => '%s.mo',
// 						),
// 				),
// 		),
		

//      'view_manager' => array(
//      		'display_not_found_reason' => true,
//      		'display_exceptions'       => true,
//      		'doctype'                  => 'HTML5',
//      		'not_found_template'       => 'error/404',
//      		'exception_template'       => 'error/index',
//          'template_map' => array(
//         'agenda/agenda/agenda'=>__DIR__.'/../view/agenda/agenda/agenda.phtml',
         		 
//              'agenda' => __DIR__ . '/../view',
//          ),
//            'template_path_stack' => array(
//             'agenda' => __DIR__ . '/../view',
           
//             __DIR__ . '/../view',
//         ),
//     ),
// 		'console' => array(
// 				'router' => array(
// 						'routes' => array(
// 						),
// 				),

// 		),
//  );
?>