<?php
namespace Agenda\Model;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Log\Writer\Stream;
use Zend\Log\Logger;

final class EstatusAgenda {

    const HABILITADO = 2;
    const CANCELADO = 1;

}

Class AgendaModel {

    protected $config;
    protected $_objLog;
    protected $sql;

    public function __construct(Adapter $adapter, $config) {
        $this->adapter = $adapter;
        $this->sql = new Sql($adapter);
        $this->config = $config;
        $this->_objLog = new Logger();
        $this->_objLog->addWriter(new Stream('php://stderr'));
    }

    /**
     * La funcion se encarga de obtener los grupos del Profesor
     * @access public
     * @author mruiz 
     * @since 10/04/2015
     * @access public
     * @param int $_intIdUsuarioPerfilProfesor, str $_consEstatus
     * @return Array $_arrDataGrupos array asociativo con los datos de los grupos
     * @version 1.0
     * @package Agenda
     */
    public function getGrupos($_intIdUsuarioPerfilProfesor, $_consEstatus = null) {

        if ($_consEstatus == null) {
            $_consEstatus = $this->config['constantes']['ESTATUS_ACTIVO'];
            $_strOrder = " ORDER BY t08fecha_registro DESC";
        } else {
            $_strOrder = " ORDER BY t08fecha_actualiza DESC";
        }

        $_strQueryGrupos = "SELECT 
                                t05grupo.t05id_grupo as id, t05descripcion as name
                            FROM 
                                t08administrador_grupo
                            INNER JOIN 
                                t05grupo ON (t08administrador_grupo.t05id_grupo = t05grupo.t05id_grupo)
                            LEFT JOIN 
                                t07codigo ON (t07codigo.t05id_grupo = t05grupo.t05id_grupo)
                            WHERE
                                t02id_usuario_perfil = :idUsuarioPerfil
                                AND t05estatus = :consEstatus " . $_strOrder;

        $statement = $this->adapter->query($_strQueryGrupos);
        $resultsGrupos = $statement->execute(array(
            ":idUsuarioPerfil" => $_intIdUsuarioPerfilProfesor,
            ":consEstatus" => $_consEstatus,
        ));

        if ($resultsGrupos->count() < 1) {
            return false;
        }

        $resultSet = new ResultSet;
        $resultSet->initialize($resultsGrupos);
        $_arrDataGrupos = $resultSet->toArray();
        return $_arrDataGrupos;
    }

    /**
     * Funcion que obtiene los Eventos
     * para poder mostrarlos en la vista
     * @author mruiz.
     * @since 17/04/2015
     * @access public
     * @param int $_intIdUsuarioPerfilProfesor, int $_t12id_examen, int $_t05id_grupo, str $_consEstatus
     * @return Array con los eventos gurdados
     * @version 1.0
     * @package Agenda
     */
    public function getEventos($_intIdUsuarioPerfilProfesor, $_t12id_examen, $_t05id_grupo, $_consEstatus = EstatusAgenda::HABILITADO) {

        $select = $this->sql->select();

        $_arrData = array(
            'id' => 't09id_agenda',
            'start' => 't09fecha_inicio',
            'end' => 't09fecha_final',
            'body' => 't09descripcion',
            'examen' => 't12id_examen',
        );

        $select->from(
                        array('t09' => 't09agenda'))
                ->columns($_arrData)
                ->join(array('t05' => 't05grupo'), 't05.t05id_grupo=t09.t05id_grupo', array('grupo' => 't05id_grupo', 'nombreGrupo' => 't05descripcion'))
                ->join(array('t08' => 't08administrador_grupo'), 't05.t05id_grupo=t08.t05id_grupo', array())
                ->join(array('t02' => 't02usuario_perfil'), 't08.t02id_usuario_perfil=t02.t02id_usuario_perfil', array())
                ->join(array('t12' => 't12examen'), 't09.t12id_examen=t12.t12id_examen', array('title' => 't12nombre'))
                ->where(array('t02.t02id_usuario_perfil' => $_intIdUsuarioPerfilProfesor))
                ->where(array('t09estatus' => $_consEstatus));


        if ($_t12id_examen && $_t12id_examen !== '-1') {
            $select->where(array('t12.t12id_examen' => $_t12id_examen));
        }

        if ($_t05id_grupo && $_t05id_grupo !== '-1') {
            $select->where(array('t05.t05id_grupo' => $_t05id_grupo));
        }

        $statement = $this->sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new ResultSet;
        $resultSet->initialize($result);
        $_arrDataEventos = $resultSet->toArray();
        
        return $_arrDataEventos;
    }

    /**
     * Funcion que obtiene los Examenes de acuerdo al grupo y usuario que se 
     * logueo. 
     * @author mruiz.
     * @since 21/04/2015
     * @access public.
     * @param  int $_intIdGrupo
     * @param  int $_bolEsIntellectus Este segundo parametro sirve para limitar
     *              los examenes, para que solo muestre intellectus, solo aplica 
     *              en la carga masiva de resultados
     * @return Array con todos los examens correspondientes al gruopo y usuario
     * @version 1.0
     * @package Agenda
     */
    public function getExamenes($_intIdGrupo, $_bolEsIntellectus = false) {
        $_arrIn = array();
        $_cont = 0;
        if (!is_array($_intIdGrupo)) {
            $_intIdGrupo = $_intIdGrupo > 0 ? $_intIdGrupo : 0;
            $_strQueryExamenes = "SELECT 
                    distinct t04.t12id_examen as id,
                    t12nombre_corto_clave as clave,
                    t12.t12nombre as name
                FROM 
                    t05grupo t05
                    JOIN t08administrador_grupo as t08 on t05.t05id_grupo=t08.t05id_grupo
                    JOIN t02usuario_perfil as t02 on  t02.t02id_usuario_perfil=t08.t02id_usuario_perfil
                    JOIN t03licencia_usuario as t03  on t02.t02id_usuario_perfil=t03.t02id_usuario_perfil
                    JOIN t04examen_usuario as t04 on t03.t03id_licencia_usuario=t04.t03id_licencia_usuario
                    JOIN t12examen as t12 on t04.t12id_examen=t12.t12id_examen
                WHERE t05.t05id_grupo IN (" . $_intIdGrupo . ") and t08.t08usuario_ligado = 'LIGADO'";
            if ($_bolEsIntellectus) {
                $_strQueryExamenes .= " AND ( t04.t12id_examen > 6 AND t04.t12id_examen < 32 ) ";
            }
            $_strQueryExamenes .= " ORDER BY id";
            $statement = $this->adapter->query($_strQueryExamenes);
            $results = $statement->execute();
        } else {
            foreach ($_intIdGrupo as $_grupo) {
                $_arrIn[$_cont] = $_grupo['id'];
                $_cont++;
            }
            $_strIn = implode(',', $_arrIn);
            $_strQueryExamenes = "SELECT 
                            distinct( t12.t12id_examen) AS id,
                            t12.t12nombre AS name,
                            t09.t09estatus
                        FROM
                            t12examen AS t12
                            JOIN t09agenda as t09 on  t12.t12id_examen=t09.t12id_examen
                            JOIN t05grupo as t05 on t09.t05id_grupo=t05.t05id_grupo
                            WHERE t05.t05id_grupo IN (" . $_strIn . ")
                            AND t09.t09estatus=:_intEstatus;";
            $statement = $this->adapter->query($_strQueryExamenes);
            $results = $statement->execute(array(
                ':_intEstatus' => EstatusAgenda::HABILITADO
            ));
        }
        $resultSet = new ResultSet;
        $resultSet->initialize($results);
        $_arrExamenes = $resultSet->toArray();

        return $_arrExamenes;
    }

    /**
     * Funcion inserta los examenes a la base de datos
     * @author mruiz.
     * @since 23/04/2015
     * @access public.
     * @param  obj $_objcalEvent
     * @return Array con todos los examens correspondientes al gruopo y usuario
     * @version 1.0
     * @package Agenda
     */
    public function insertaExamen($_objcalEvent, $_dateInicio, $_dateFin) {
        $_intIdEvento = $_objcalEvent['id'];
        $_strFechaInicio = $_objcalEvent['start'];
        $_strFechaFin = $_objcalEvent['end'];
        $dateStart = $_dateInicio;
        $dateEnd = $_dateFin;
        $_bolStart = isset($dateStart) ? true : false;
        if ($_objcalEvent['accion'] == "eliminar") {
            $strEstatus = 'CANCELADO';
            $arrDatos = array(":t09estatus" => $strEstatus);
        } else {
            $strEstatus = 'HABILITADO';
            if ($_bolStart == 'true') {//Ya existe una hora de inicio para el evento y será editado
                $arrFechaInicio = explode(" ", $_strFechaInicio);
                $arrFechaFin = explode(" ", $_strFechaFin);

                $_strFechaInicio = $dateStart . " " . $arrFechaInicio[1];
                $_strFechaFin = $dateEnd . " " . $arrFechaFin[1];
            } else {//No existe una hora de inicio para el evento y será creado
                $_strFechaInicio = $_strFechaInicio;
                $_strFechaFin = $_strFechaFin;
            }

            $arrDatos = array(
                ":t12id_examen" => $_objcalEvent['examen'],
                ":t05id_grupo" => $_objcalEvent['title'],
                ":t09fecha_inicio" => $_strFechaInicio,
                ":t09fecha_final" => $_strFechaFin,
                ":t09estatus" => $strEstatus,
                ":t09descripcion" => $_objcalEvent['body']
            );
        }
        switch ($_objcalEvent['accion']) {
            case 'guardar':
                $_insertaEvento = "INSERT INTO 
                t09agenda(	
                `t12id_examen`,
                `t05id_grupo`,
                `t09fecha_inicio`,
                `t09fecha_final`,
                `t09estatus`,
                `t09descripcion`)
                VALUES(     
                :t12id_examen,
                :t05id_grupo,
                :t09fecha_inicio,
                :t09fecha_final,
                :t09estatus,
                :t09descripcion)";
                break;

            case 'eliminar':
                $_insertaEvento = "UPDATE t09agenda
                    	   SET    
                    	   `t09estatus`=:t09estatus
                    	   WHERE   t09id_agenda=:t09id_agenda";
                $arrDatos[':t09id_agenda'] = $_intIdEvento;
                break;

            case 'editar':
                $_insertaEvento = "UPDATE t09agenda
                    	   SET    `t12id_examen`=:t12id_examen,
                    	   `t05id_grupo`=:t05id_grupo,
                    	   `t09fecha_inicio`=:t09fecha_inicio,
                           `t09fecha_final`=:t09fecha_final,
                           `t09estatus`=:t09estatus,
                           `t09descripcion`=:t09descripcion
                           WHERE   t09id_agenda=:t09id_agenda";
                $arrDatos[':t09id_agenda'] = $_intIdEvento;
                break;
            default;
                break;
        }
        $statement = $this->adapter->query($_insertaEvento);
        $results = $statement->execute($arrDatos);
        return $results->getGeneratedValue();
    }
    
    public function obtenerExamenGrupo($_objcalEvent){    
        $_idExamen = $_objcalEvent['examen'];
        $_idGrupo = $_objcalEvent['title'];
           $_arrData = array( 
                                't09id_agenda',
                                't09fecha_inicio',
                                't09fecha_final',
                               't12id_examen',
                               't05id_grupo',
                               't09estatus'
                       );
           $select = $this->sql->select();
           $select->from(array('t09' => 't09agenda'))
                ->columns($_arrData)
                ->where(array(
                    't12id_examen' => $_idExamen,
                    't05id_grupo' => $_idGrupo,
                    //'t09estatus' => EstatusAgenda::HABILITADO
             ));
            $_arrTipos = $this->obtenerRegistros($select, $_arrData[0]);
            if (!count($_arrTipos)){
                $_arrRespuesta = false;
            }else{
                $_arrClave = array_keys($_arrTipos);
                $_arrRespuesta = array('id_examen' => $_arrTipos[$_arrClave[0]]['t12id_examen'],
                                   'id_grupo' => $_arrTipos[$_arrClave[0]]['t05id_grupo'],
                                   'fechainicio' => date("d/m/Y H:i:s",strtotime($_arrTipos[$_arrClave[0]]['t09fecha_inicio'])),
                                   'fechafinal' => date("d/m/Y H:i:s",strtotime($_arrTipos[$_arrClave[0]]['t09fecha_final'])),
                                   'id_agenda' => $_arrTipos[$_arrClave[0]]['t09id_agenda'],
                                   'estatus'=> $_arrTipos[$_arrClave[0]]['t09estatus'],
                                   'duplicado'=> $_arrTipos[$_arrClave[0]]['t09estatus'] == 'CANCELADO'? '0' : '1',
                                   
                );
            }
           return $_arrRespuesta;
       }
       
        protected function obtenerRegistros($select, $id = '', $data = '', $parent = ''){
                       
           $statement = $this->sql->prepareStatementForSqlObject($select);
           $result = $statement->execute();
           
           $resultSet = new ResultSet;
           $resultSet->initialize($result);
           $_arrData = $resultSet->toArray();
           
           $_arrDatos = $this->formatoArray($_arrData, $id, $data, $parent);
           return $_arrDatos;
       }
       
        protected function formatoArray($_arrData, $id, $dato, $parent) {
        $_arrRespuesta = array();       
        if(!isset($_arrData[0])){
            return $_arrRespuesta;
        }else{
            $_arrIndices = array_keys($_arrData[0]);
            foreach ($_arrData as $data) {
                if ($parent) {
                    $_arrRespuesta[$data[$id]] = array(
                        'id_parent' => $data[$parent],
                        'value' => $data[$dato]);
                } else {
                    if ($id && $dato) {
                        $_arrRespuesta[$data[$id]] = $data[$dato];
                    } else {
                        $_arrRespuesta[$data[$id]] = array(); 
                        foreach ($_arrIndices as $campo) {
                            $_arrRespuesta[$data[$id]][$campo] = $data[$campo];
                        }
                    }
                }
            }
        }
        return $_arrRespuesta;
    }
      }
