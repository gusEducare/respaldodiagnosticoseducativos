<?php
namespace Agenda\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Agenda\Model\AgendaModel;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\Session\Container;

class AgendaController extends AbstractActionController {

    protected $log;
    protected $datos_sesion;

    public function __construct() {
        $this->log = new Logger();
        $this->log->addWriter(new Stream('php://stderr'));
        $this->datos_sesion = new Container('user');
    }

    /**
     * Funcion que obtiene los datos de la agenda como lo son examenes, 
     * grupos y todos los eventos
     * @author mruiz.
     * @since 14/04/2015
     * @access public.
     * @return AgendaModel $response
     */
    public function agendaAction() {
        //$service_locator = $this->getServiceLocator();
        $_intIdUsuario = $this->datos_sesion->idUsuarioPerfil;

        $request = $this->getRequest();
        $_intId_examen = $request->getPost('idExamen');
        $_intId_grupo = $request->getPost('idGrupo');
        //$_consEstatus = $request->getPost('conEstatus');

        $_arr_Grupos = $this->getAgendaModel()->getGrupos($_intIdUsuario);
        $_arr_Examenes = $this->getAgendaModel()->getExamenes($_arr_Grupos);
        $_arr_Agenda = $this->getAgendaModel()->getEventos($_intIdUsuario, $_intId_examen, $_intId_grupo);
        if (isset($_intId_grupo)) {
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arr_Agenda));
            return $response;
        } else {
            $_arr_Json_Agenda = json_encode($_arr_Agenda);
            return new ViewModel(array(
                'grupos' => $_arr_Grupos,
                'agenda' => $_arr_Json_Agenda,
                'examenes' => $_arr_Examenes
            ));
        }
    }

    /**
     * Funcion que obtiene los datos del examen del evento ajax
     * @author mruiz.
     * @since 14/04/2015
     * @access public.
     * @return AgendaModel $response
     * @version 1.0
     * @package Agenda
     */
    public function obtenerexamenesajaxAction() {
        $_intIdUsuario = $this->datos_sesion->idUsuario;
        $request = $this->getRequest();
        $_intIdGrupo = $request->getPost('idGrupo');
        $_intExamenpost = $request->getPost('Examenes');
        $_intExamen = isset($_intExamenpost) ? $_intExamenpost : null;
        $_arr_Examenes = $this->getAgendaModel()->getExamenes($_intIdGrupo);
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($_arr_Examenes));
        return $response;
    }

    /**
     * Funcion que agrega e inserta el evento del ajax  
     * @author mruiz.
     * @since 10/04/2015
     * @access public.
     * @return AgendaModel $response
     * @version 1.0
     * @package Agenda
     */
    public function agregareventoajaxAction() {
        $request = $this->getRequest();
        $_intIdUsuario = $this->datos_sesion->idUsuarioPerfil;
        $_objcalEvent = $request->getPost('calEvent');
        $_dateInicio = ($request->getPost('dateStart')=="") ? null : $request->getPost('dateStart');
        $_dateFin = ($request->getPost('dateEnd')=="") ? null : $request->getPost('dateEnd');
        $_intId_examen = $request->getPost('idExamen');
        $_intId_grupo = $request->getPost('idGrupo');
        $_consulta = false;
        if($_objcalEvent['accion'] === 'guardar' ){
        $_consulta = $this->getAgendaModel()->obtenerExamenGrupo($_objcalEvent);
        }
        if($_consulta){
            if($_consulta['estatus'] != 'CANCELADO'){
                $_arr_Agenda = $_consulta;
            }else{
                   $_objcalEvent['accion'] = 'editar';
                   $_objcalEvent['id'] = $_consulta['id_agenda'];
                 $this->getAgendaModel()->insertaExamen($_objcalEvent, $_dateInicio, $_dateFin);
                 $_arr_Agenda = $this->getAgendaModel()->getEventos($_intIdUsuario,null,null);
                 //$_bolConsulta = false;
            }
        }else {
            //$_bolConsulta = false;
                    $this->getAgendaModel()->insertaExamen($_objcalEvent, $_dateInicio, $_dateFin);
                    $_arr_Agenda = $this->getAgendaModel()->getEventos($_intIdUsuario,null,null);
        }
        $response = $this->getResponse();
       // $_arr_Agenda = $this->getAgendaModel()->getEventos($_intIdUsuario, $_intId_examen, $_intId_grupo);
        $response->setContent(\Zend\Json\Json::encode($_arr_Agenda));
        return $response;
    }

    /**
     * Funcion que llama al service manager y trae la clase AgendaModel.
     * @author mruiz.
     * @since 07/04/2015
     * @access public.
     * @return AgendaModel $agenModel
     * @version 1.0
     * @package Agenda
     */
    public function getAgendaModel() {
        $sm = $this->getServiceLocator();
        $agenModel = $sm->get('Agenda\Model\AgendaModel');
        return $agenModel;
    }

}
