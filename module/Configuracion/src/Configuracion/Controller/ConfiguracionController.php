<?php
namespace Configuracion\Controller;

//importar espacios de nombres para la instancia del Logger.
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;

//importar espacios de nombres utilizados en la clase
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * 
 * @author oreyes
 * @since 05/11/2014
 * @version  configuracion-1.0
 *
 */
class ConfiguracionController extends AbstractActionController
{

	/**
	 * Para los mensajes log.
	 * @var Logger $log : instancia de la clase Logger
	 */
	protected $log;
	
	/**
	 * Para guardar el path del archivo a modificar.
	 * @var String $archivo : Path del archivo.
	 */
	protected $archivo;
	
	/**
	 * Para guardar el nombre del arreglo 
	 * donde se almacenan las constantes en el archivo global.php.
	 * @var String $nombreArray : Nombre del arreglo donde se almacenan las constantes.
	 */
	protected $nombreArray;
	
	/**
	 * Constructor de la clase.
	 * crea una instancia del objeto logger para mensajes en el log de apache.
	 * Agregar el path del archivo de configuracion que almacena las constantes.
	 * nombre del arreglo que contiene todas las constantes en el archivo global.php.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 31/10/2014
	 * 
	 */
	public function __construct()
	{
		$this->log = new Logger();
		$this->log->addWriter(new Stream('php://stderr'));
		
		$this->archivo 		= $_SERVER['DOCUMENT_ROOT']."/../config/autoload/global.php";
		$this->nombreArray  = 'constantes';
	}
	
	
	/**
	 * Mostrar todas las variables que se encuentran en global.php se muestran en una vista;
	 *
	 * @author oreyes@grupoeducare.com
	 * @since 18/09/2014
	 * @return ViewModel  verconfig.phtml;
	 */
	public function indexAction()
	{
		$config  = $this->getServiceLocator()->get("Config");
		return new ViewModel(array(
				'constants'				=> $config['constantes']
		));
	}
	
	/**
	 * Modificar las variables de configuración. y sobreescribir el archivo global.php
	 *
	 * @author oreyes@grupoeducare.com
	 * @since 23/09/2014
	 * @return Ambigous <\Zend\Http\Response, \Zend\Stdlib\ResponseInterface>
	 */
	public function modificarconfigAction()
	{
		$_response['msg'] 	  = '';
		$_response['success'] = true;
		
		$txtinicial = "";
		
		$request = $this->getRequest();
		$datos   = $request->getPost('datos');

		//exploramos el contenido del archivo.
		$contenido = @file($this->archivo) or $_response['success'] = false;
		if($_response['success']){
			foreach ($contenido as $linea){
				//iteramos las variables que se modificaron y llegan por peticion post.
				foreach ($datos as $modificar=>$dato){
					$modificar  = "'".$modificar."'";
					//comparamos con todas las lineas del contenido para saber cual es la linea modificada.
					if(preg_match("/".$modificar."/i",$linea)){
						$dato = $this->validarDato($dato);
						$linea = "\t\t".$modificar."=>". $dato .",\n";
					}
				}
				$txtinicial .= $linea;
			}
			
			$abrirArchivo = @fopen($this->archivo,"w") or $_response['success'] = false;
			 
			if($_response['success']){
				$_resp = fwrite($abrirArchivo, $txtinicial);
				if($_resp){
					fclose($abrirArchivo);
				}
			}else{
				$_response['success'] = false;
				$_response['msg'] 	  = 'No tienes permisos de escritura en el archivo.';
			}
		}else{
			$_response['success'] = false;
			$_response['msg'] 	  = 'El archivo no existe.';
		}
		
		$response = $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_response));
		return $response;
	}
	
	/**
	 * guardar una nueva variable para agregar al archivo global.php
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 05/11/2014
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	public function guardarVariableAction()
	{
		$_response['success'] = true;
		$_response['msg'] 	  = '';
		$txtinicial 		  = '';
		
		$_request = $this->getRequest();
		
		$_nameVar = strtoupper($_request->getPost('nameVar'));
		$_valuevar = $_request->getPost('valueVar');
		
		//exploramos el contenido del archivo.
		$contenido = @file($this->archivo) or $_response['success'] = false;
		if($_response['success']){
			foreach ($contenido as $linea){
				//comparamos con todas las lineas del contenido para saber cual es la linea modificada.
				if(preg_match("/".$this->nombreArray."/i",$linea)){
					$_nameVar  = "'".$_nameVar."'";
					$_valuevar = $this->validarDato($_valuevar);
					$linea = $linea."\n \t\t".$_nameVar."=>". $_valuevar .",\n";
				}
				$txtinicial .= $linea;
			}
				
			$abrirArchivo = @fopen($this->archivo,"w") or $_response['success'] = false;
		
			if($_response['success']){
				$_resp = fwrite($abrirArchivo, $txtinicial);
				if($_resp){
					fclose($abrirArchivo);
				}
			}else{
				$_response['success'] = false;
				$_response['msg'] 	  = 'No tienes permisos de escritura en el archivo.';
			}
		}else{
			$_response['success'] = false;
			$_response['msg'] 	  = 'El archivo no existe.';
		}

		$_resp = $this->getResponse();
		$_resp->setContent(\Zend\Json\Json::encode($_response));
		return $_resp;
	}
	
	/**
	 * Eliminar una variable seleccionada.
	 * 
	 * @author oreyes@grupoeducare.com
	 * @since 05/09/2014
	 * @return \Zend\Stdlib\ResponseInterface
	 */
	function eliminarvariableAction() 
	{
		$_response['success'] = true;
		$_response['msg'] 	  = '';
		$txtinicial 		  = '';
		$_request 			  = $this->getRequest();
		$_nameconst			  = $_request->getPost('nameconst');
	
		//exploramos el contenido del archivo.
		$contenido = @file($this->archivo) or $_response['success'] = false;
		if($_response['success']){
			foreach ($contenido as $linea)
			{
				//comparamos con todas las lineas del contenido para saber cual es la linea para eliminar.
				if(preg_match("/".$_nameconst."/i", $linea)){
					$linea = '';
				}
				$txtinicial .= $linea;
			}
				
			$abrirArchivo = @fopen($this->archivo,"w") or $_response['success'] = false;
		
			if($_response['success']){
				$_resp = fwrite($abrirArchivo, $txtinicial);
				if($_resp){
					fclose($abrirArchivo);
				}
			}else{
				$_response['success'] = false;
				$_response['msg'] 	  = 'No tienes permisos de escritura en el archivo.';
			}
		}else{
			$_response['success'] = false;
			$_response['msg'] 	  = 'El archivo no existe.';
		}
		
		$_resp = $this->getResponse();
		$_resp->setContent(\Zend\Json\Json::encode($_response));
		return $_resp;
	}
	
	/**
	 * Vadidar que tipo de dato es para concatenarle comillas en caso de que sea un dato de tipo
	 * cadena de texto.
	 *
	 * @author oreyes@grupoeducare.com
	 * @since 29/09/2014
	 * @param string o int $dato
	 * @return string o int $nuevodato : con el formato correcto segun sea su tipo.
	 */
	private function validarDato ( $dato )
	{
		$nuevodato = $dato;
	
		if(!preg_match('/^[[:digit:]]+$/', $dato)){
			$nuevodato = "'".$dato."'";
		}
		return $nuevodato;
	}
}