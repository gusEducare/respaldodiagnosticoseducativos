<?php
/**
 *
 * @author oreyes
 * @since 05/11/2014
 * @version  configuracion-1.0
 *
 */

return array(
	'controllers' => array(
		'invokables' => array(
			'Configuracion\Controller\Configuracion' => 'Configuracion\Controller\ConfiguracionController',
		),
	),
	
		
	'router' => array(
		'routes' => array(
			'configuracion' => array(
				'type' =>  'Segment',
				'options' => array(
					'route'    => '/configuracion[/[:action]]',
					'constraints' => array(
							'action'  =>  '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
							'controller' => 'Configuracion\Controller\Configuracion',
							'action'     => 'index',
					),
				),
			),
		),
	),
		
	'view_manager' => array(
			'display_not_found_reason' => true,
			'display_exceptions'       => true,
			'doctype'                  => 'HTML5',
			'not_found_template'       => 'error/404',
			'exception_template'       => 'error/index',
			'template_map' => array(
					'configuracion/configuracion/index' => __DIR__ . '/../view/configuracion/configuracion/verConfig.phtml',
			),
			'template_path_stack' => array(
					'configuracion' => __DIR__ . '/../view',
			),
	),
);