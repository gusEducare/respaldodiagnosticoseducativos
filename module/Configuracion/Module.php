<?php
namespace Configuracion;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Configuracion\Model\ConfiguracionModel;
use Zend\Db\Adapter\Adapter;

/**
 * 
 * @author oreyes
 * @since 05/11/2014
 * @version  configuracion-1.0
 *
 */
class Module
{	

	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
						__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}
	
	public function getConfig()
	{
		return include __dir__.'/config/module.config.php';
	}
	
	public function getServiceConfig()
	{
		return array();
	}
}