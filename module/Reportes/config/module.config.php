<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'reportes' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'         => '/reportes[/[:action]][/:id]',
                    'constraints'   => array(
                        'controller'=> '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'    =>  '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        =>  '[0-9]*',
                    ),
                    'defaults'          => array(
                        'controller'    => 'Reportes\Controller\Reportes',
                        'action'        => 'index',
                    ),
                ),
            ),
       	),
    ),
    'controllers' => array(
        'invokables' => array(
            	
            	'Reportes\Controller\Reportes' => 'Reportes\Controller\ReportesController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            

    	),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
