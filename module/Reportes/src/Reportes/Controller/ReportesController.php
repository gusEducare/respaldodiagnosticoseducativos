<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */


namespace Reportes\Controller;

use Zend\Session\Container;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;




class ReportesController extends AbstractActionController
{
	
	
	protected $sesion;
	
	protected $_objLogger;
	/**
	 * Se anexa el construct para utilizar las clase client y oauth....
	 */
	public function __construct(){
		$this->sesion = new Container('user');	
	}
	
	public function reporteAlumnoAction() {
		
		$sm = $this->getServiceLocator();
                $config = $sm->get('Config');
		$_modelReportes = $sm->get('Reportes\Model\Reportes');
                
		
		//$this->_objLogger->debug(print_r($this->sesion->getArrayCopy(),true));
		//obtner el id desde la la ruta 
		
		
		if( $this->sesion->perfil ==  $config['constantes']['ID_PERFIL_ALUMNO']){
                    $_intIdUSPC = $this->sesion->idUsuarioPerfil;
                    $_idExam = $this->params()->fromRoute('id');
                    
                    $_intIdUsuarioPerfil      = $this->sesion->idUsuarioPerfil;
                    
                    
		}
                if( $this->sesion->perfil ==  $config['constantes']['ID_PERFIL_PROFESOR']){
                    $_intIdUSPC = $this->params()->fromQuery('id_user');
                    $_idExam = $this->params()->fromQuery('id_exam');
                    $_idGrupo = $this->params()->fromQuery('id_group');
                    //verificar que el usuario pertenece al profesor
                    $_modelGrupo = $sm->get('Grupos\Model\Grupo');
                    $_intIdUsuarioPerfilProfesor = $this->sesion->idUsuarioPerfil;
                    $_arrDataUser = $_modelGrupo->getUserData($_intIdUSPC, $_intIdUsuarioPerfilProfesor, $_idGrupo,true );
                    $this->getServiceLocator()->get('Debug')->debug(print_r($_arrDataUser,true));
                    
                    $_intIdUsuarioPerfil      = $this->params()->fromQuery('id_user');
                    
                    
                    if(count($_arrDataUser) == 0){
                        //redireccionar a la vista error 
                        
                        $response = $this->getResponse();
                        $response->setStatusCode(403);
                        $viewModel = new ViewModel();
                        $viewModel->setTemplate('error/403');
                        return $viewModel;
                        die();
                    }
                    
		}
                
		$_arrDiagnostico = $_modelReportes->getReporteAlumno($_intIdUSPC, $_idExam);
                
                //verificar si existe POST realizado de este examen 
                $_idExamenAdjunto = null;
                $_boolIsPre = false;
                switch ($_idExam){
                    case $config['constantes']['ID_INTELLECTUS_KINDER']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_KINDER_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_1']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_2']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_3']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_4']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_5']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_6']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_KINDER_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_KINDER'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_1'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_2'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_3'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_4'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_5'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_6'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3'];
                        break;
                }

                if($_boolIsPre === true){
                    $_strTitleTableAlumnos =  'Examen Post';
                    $_strLabelExamenAdjunto = 'Comparar contra Examen Post';
                    $_strTooltipExamenAdjunto = 'Se compara este examen contra los resultados obtenidos en la evaluacion posterior(al finalizar el curso)';
                }
                else{
                    $_strTitleTableAlumnos =  'Examen Pre';
                    $_strLabelExamenAdjunto = 'Comparar contra Examen Pre';
                    $_strTooltipExamenAdjunto = 'Se compara este examen contra los resultados obtenidos en la evaluacion previa(al iniciar el curso)';
                }
                
                
                
                $_modelExamenes = $this->getServiceLocator()->get('/Examenes/Model/ExamenModel');
                $_arrGrupos = $_modelExamenes->getGruposActivos($_intIdUsuarioPerfil);
                $_arrRespExamenes   = $_modelExamenes->obtenerExamenesUsuario( $_intIdUsuarioPerfil, $_arrGrupos );

                //verificar si existe POST realizado de este examen 
                $_boolExisteAdjunto = false;
                if(count($_arrRespExamenes['FINALIZADAS']['examenes']) > 0){
                    foreach ($_arrRespExamenes['FINALIZADAS']['examenes'] as $_exam){
                        foreach ($_exam as $ex){
                            
                            if($ex['id'] == $_idExamenAdjunto){
                                $this->getServiceLocator()->get('Debug')->debug($_idExamenAdjunto.' ==  '.$ex['id']);
                                $_boolExisteAdjunto = true;
                            }
                            else{
                                $this->getServiceLocator()->get('Debug')->debug($_idExamenAdjunto.' != '.$ex['id']);
                            }
                        }
                        
                    }
                }
                
                
                
		$this->getServiceLocator()->get('Debug')->debug(print_r($_arrRespExamenes,true));
                
		$viewModel = new ViewModel(array(
                        "_arrDiagnostico"=> $_arrDiagnostico,
                        '_boolExisteAdjunto' => $_boolExisteAdjunto,
                        '_strTitleTableAlumnos' => $_strTitleTableAlumnos,
                        '_strLabelExamenAdjunto' => $_strLabelExamenAdjunto,
                        '_strTooltipExamenAdjunto' => $_strTooltipExamenAdjunto,
                        '_idExamenAdjunto' => $_idExamenAdjunto,
                        '_intIdUsuarioPerfil' => $_intIdUsuarioPerfil
                        
                ));
		$viewModel->setTerminal(true);
		return $viewModel;
	}
        
        
        public function reporteGrupoAction(){
            
            $_modelReportes = $this->getServiceLocator()->get('Reportes\Model\Reportes');
            $config = $this->getServiceLocator()->get('Config');
            $idGrupo = $this->getRequest()->getQuery('group');
            $idExamen = $this->getRequest()->getQuery('exam');
            $idUsuarioPerfilProfesor =  $this->sesion->idUsuarioPerfil;
            //verificar que el grupo pertenece al profesor
            $_modelGrupo = $this->getServiceLocator()->get('Grupos\Model\Grupo');
            $_arrDataGrupo = $_modelGrupo->getDataGrupo($idGrupo, $idUsuarioPerfilProfesor);
            if($_arrDataGrupo === false || count($_arrDataGrupo) == 0){
                //redireccionar a la vista error 
                $response = $this->getResponse();
                $response->setStatusCode(403);
                $viewModel = new ViewModel();
                $viewModel->setTemplate('error/403');
                return $viewModel;
                die();
            }
            $strUrlReporteAlumno = $this->url()->fromRoute('reportes', array('controller'=>'reportes', 'action'=>'reporteAlumno'));
            $_arrDiagnostico = $_modelReportes->getReporteGrupo($idGrupo, $idExamen, $strUrlReporteAlumno);
            $_arrDiagnostico['examenes'] = array();
            $_arrDiagnostico['examenes'] = $_modelReportes->getExamenesResueltos($idGrupo);
            
            
            //verificar si existe POST realizado de este examen 
            $_idExamenAdjunto = null;
            $_boolIsPre = false;
            switch ($idExamen){
                
                    case $config['constantes']['ID_INTELLECTUS_KINDER']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_KINDER_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_1']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_2']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_3']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_4']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_5']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_6']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST'];
                        $_boolIsPre = true;
                        break;
                    case $config['constantes']['ID_INTELLECTUS_KINDER_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_KINDER'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_1_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_1'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_2_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_2'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_3_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_3'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_4_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_4'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_5_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_5'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PRIMARIA_6_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PRIMARIA_6'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_1'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_2'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_SECUNDARIA_3'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_1'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_2'];
                        break;
                    case $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3_POST']:
                        $_idExamenAdjunto = $config['constantes']['ID_INTELLECTUS_PREPARATORIA_3'];
                        break;
            }
            
            if($_boolIsPre === true){
                $_strTitleTableAlumnos =  'Examen Post';
                $_strLabelExamenAdjunto = 'Comparar contra Examen Post';
                $_strTooltipExamenAdjunto = 'Se compara este examen contra los resultados obtenidos en la evaluacion posterior(al finalizar el curso)';
            }
            else{
                $_strTitleTableAlumnos =  'Examen Pre';
                $_strLabelExamenAdjunto = 'Comparar contra Examen Pre';
                $_strTooltipExamenAdjunto = 'Se compara este examen contra los resultados obtenidos en la evaluacion previa(al iniciar el curso)';
            }
            
            //verificar si existe POST realizado de este examen 
            $_boolExisteAdjunto = false;
            foreach ($_arrDiagnostico['examenes'] as $_exam){
                if($_exam['t12id_examen'] == $_idExamenAdjunto){
                    $_boolExisteAdjunto = true;
                }
            }
            
            
            
            
            $viewModel = new ViewModel(array(
                "_arrDiagnostico"=> $_arrDiagnostico,
                'idExamen' => $idExamen,
                'idGrupo'=> $idGrupo,
                '_arrDataTableAlumnos' => $_arrDiagnostico['alumnos_table'],
                '_arrLinksAlumnos' => $_arrDiagnostico['_arrLinksAlumnos'],
                '_boolExisteAdjunto' => $_boolExisteAdjunto,
                '_idExamenAdjunto' => $_idExamenAdjunto,
                '_strLabelExamenAdjunto' => $_strLabelExamenAdjunto,
                '_strTooltipExamenAdjunto' => $_strTooltipExamenAdjunto,
                '_strTitleTableAlumnos' => $_strTitleTableAlumnos
                ));
            
            $this->getServiceLocator()->get('Debug')->debug(print_r(array_keys($_arrDiagnostico),true));
            $this->getServiceLocator()->get('Debug')->debug(print_r($_arrDiagnostico['examenes'],true));
            $this->getServiceLocator()->get('Debug')->debug('examen adjunto --> '.$_idExamenAdjunto);
            
            
            $viewModel->setTerminal(true);
            return $viewModel;
            
        }
	
        public function dataUniversoAction(){
            
            $sm = $this->getServiceLocator();
            $_modelReportes = $sm->get('Reportes\Model\Reportes');
		$_idExamen = $this->getRequest()->getPost('_idExamen');
		$_arrDatos = $_modelReportes->getDataUniverso($_idExamen);
		$response = $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_arrDatos));
		return $response;
        }
        
        
        public function dataExamenAdjuntoAction(){
            /*
            $sm = $this->getServiceLocator();
            $_modelReportes = $sm->get('Reportes\Model\Reportes');
		$_idExamen = $this->getRequest()->getPost('_idExamen');
                
                $_arrDiagnostico = $_modelReportes->getReporteGrupo($idGrupo, $idExamen, $strUrlReporteAlumno);
		$_arrDatos = $_modelReportes->getDataUniverso($_idExamen);
		$response = $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_arrDatos));
		return $response;*/
                
                
                
            $_modelReportes = $this->getServiceLocator()->get('Reportes\Model\Reportes');
            $idGrupo = $this->getRequest()->getPost('idGrupo');
            $idExamen = $this->getRequest()->getPost('idExamenAdjunto');
            $idUsuarioPerfilProfesor =  $this->sesion->idUsuarioPerfil;
            //verificar que el grupo pertenece al profesor
            $_modelGrupo = $this->getServiceLocator()->get('Grupos\Model\Grupo');
            $_arrDataGrupo = $_modelGrupo->getDataGrupo($idGrupo, $idUsuarioPerfilProfesor);
            if($_arrDataGrupo === false || count($_arrDataGrupo) == 0){
                //redireccionar a la vista error 
                $response = $this->getResponse();
                $response->setStatusCode(403);
                $viewModel = new ViewModel();
                $viewModel->setTemplate('error/403');
                return $viewModel;
                die();
            }
            $strUrlReporteAlumno = $this->url()->fromRoute('reportes', array('controller'=>'reportes', 'action'=>'reporteAlumno'));
            $_arrDiagnostico = $_modelReportes->getReporteGrupo($idGrupo, $idExamen, $strUrlReporteAlumno);
            
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrDiagnostico));
            return $response;
        }
        
        
        public function dataExamenAdjuntoAlumnoAction(){
            /*
            $sm = $this->getServiceLocator();
            $_modelReportes = $sm->get('Reportes\Model\Reportes');
		$_idExamen = $this->getRequest()->getPost('_idExamen');
                
                $_arrDiagnostico = $_modelReportes->getReporteGrupo($idGrupo, $idExamen, $strUrlReporteAlumno);
		$_arrDatos = $_modelReportes->getDataUniverso($_idExamen);
		$response = $this->getResponse();
		$response->setContent(\Zend\Json\Json::encode($_arrDatos));
		return $response;*/
                
                
                
            $sm = $this->getServiceLocator();
                $config = $sm->get('Config');
		$_modelReportes = $sm->get('Reportes\Model\Reportes');
            
            $_intIdUsuarioPerfil = $this->getRequest()->getPost('_intIdUsuarioPerfil');
            $_idExam = $this->getRequest()->getPost('idExamenAdjunto');
            
            $_arrDiagnostico = $_modelReportes->getReporteAlumno($_intIdUsuarioPerfil, $_idExam);
            
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrDiagnostico));
            return $response;
        }
        
        
        public function reporteGrupoComparativoAction(){
            
            $_intIdUsuarioPerfilProfesor  = $this->sesion->idUsuarioPerfil;
            $_modelGrupos  = $this->getServiceLocator()->get('Grupos\Model\Grupo');
            $_modelReportes = $this->getServiceLocator()->get('Reportes\Model\Reportes');
            
            $_arrDataGrupos = $_modelGrupos->getGrupos($_intIdUsuarioPerfilProfesor);
            $_arrGrupos = array();
            foreach ($_arrDataGrupos as $grupo){
                $_arrExamenes = $_modelReportes->getExamenesResueltos($grupo['t05id_grupo']);
                $_arrGrupos[] = array('grupo' => $grupo, 'examenes' => $_arrExamenes);
            }
            
            $view = new ViewModel(array(
                '_arrGrupos'=> $_arrGrupos
            ));
            $view->setTerminal(true);
            return $view;
        }
        
        
        public function getDataVersusAction(){
            
            $_modelReportes = $this->getServiceLocator()->get('Reportes\Model\Reportes');
            
            $idGrupo = $this->getRequest()->getPost('idGrupo');
            $idExamen = $this->getRequest()->getPost('idExamen');
            $boolPrimeraPeticion = $this->getRequest()->getPost('boolPrimeraPeticion');
            
            if($boolPrimeraPeticion == 'true'){
                $idUsuarioPerfilProfesor =  $this->sesion->idUsuarioPerfil;
                $strUrlReporteAlumno = $this->url()->fromRoute('reportes', array('controller'=>'reportes', 'action'=>'reporteAlumno'));
                $_arrDiagnostico = $_modelReportes->getReporteGrupo($idGrupo, $idExamen, $idUsuarioPerfilProfesor, $strUrlReporteAlumno);
                
            }
            else{
                $idUsuarioPerfilProfesor =  $this->sesion->idUsuarioPerfil;
                $_arrDiagnostico = $_modelReportes->getDataVersus($idGrupo, $idExamen, $idUsuarioPerfilProfesor);
                
            }
            
            //$this->getServiceLocator()->get('Debug')->debug(print_r($_arrDiagnostico,true));
            
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($_arrDiagnostico));
            return $response;
        }
        
        
        
}