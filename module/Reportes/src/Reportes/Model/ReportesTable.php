<?php
/**
 * 
 * La clase Reportes se encargará de obtener los colegios, sus grupos,
 * examenes calificados
 * @since 17-01-2013
 * @author enajar@grupoeducare.com
 */

namespace Application\Model;

use Zend\Filter\Boolean;
use Zend\Filter\Int;
use Zend\XmlRpc\Value\Integer;
use Zend\XmlRpc\Value\String;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Delete;
use Zend\Validator\File\Count;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Spreadsheet_Excel_Reader;
use Zend\Db\Adapter\Adapter;
use XYChart;

class ReportesTable{
	
	/**
	 * para instanciar la clase SQL de Zend.
	 * @var $sql
	 */
	protected $sql;
	
	/**
	 * Para instanciar la clase adapater de Zend.
	 * @var $adapter
	 */
	protected $adapter;
	
	/**
	 * Constructor de la clase recibe un objeto adapter (dicha clase controla los accesos 
	 * a la base de datos) setea valores a la variable local adapter e inicia la variable
	 * $sql.
	 *
	 * @param Adapter $adapter
	 */
	public function __construct(Adapter $adapter){
		$this->adapter = $adapter;
		$this->sql = new Sql($adapter);
	}
	
	/**
	 * Funcion para obtener los grupos de un profesor 
	 * filtrando la busqueda por el id_usuario_servicio_perfil_colegio del profesor.
	 * 
	 * @param int $_intIdUsuarioServicioPerfil
	 * @return $_arrGrupos
	 */
	public function obtenerGruposReportes( $_intIdUsuarioServicioPerfil )
	{
		$_arrGrupos = array();
		$adapter = $this->adapter;
		$consulta = "SELECT  
						ge_t07.ge_t07id_profesor_grupo_grado_escolaridad as t36id_grupo_diag, 
						ge_t07.ge_t07descripcion as t36nombre_grupo, 
						ge_t07.ge_t07ciclo AS t36fecha_alta, 
						t37.t37id_examen, 
							 CASE t37.t37id_examen 
								when 35 THEN 'Kinder'
								When 23 THEN 'Primaria'
								When 24 THEN 'Primaria'
								When 25 THEN 'Primaria'
								When 26 THEN 'Primaria'
								When 27 THEN 'Primaria'
								When 28 THEN 'Primaria'
								When 29 THEN 'Secundaria'
								When 30 THEN 'Secundaria'
								When 31 THEN 'Secundaria'
								When 32 THEN 'Bachillerato'
								When 33 THEN 'Bachillerato'
								When 34 THEN 'Bachillerato' 
							ELSE 'SIN' END  as categoria 
					FROM 
						ge_t07profesor_grupo_grado_escolaridad as ge_t07 
					INNER JOIN t37respuesta as t37
						ON(t37.t36id_grupo_diag = ge_t07.ge_t07id_profesor_grupo_grado_escolaridad) 
					WHERE  
						ge_t07.ge_t03id_usuario_servicio_perfil_colegio =". $_intIdUsuarioServicioPerfil."  
					GROUP BY t37.t36id_grupo_diag 
					ORDER BY t37id_examen,t36nombre_grupo";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGrupos = $resultSet->toArray();
		
		return $_arrGrupos;
	}
	
	/**
	 * Funcion para obtener los grados de un profesor 
	 * filtrando la busqueda por el id_usuario_servicio_perfil_colegio del profesor.
	 * 
	 * @param int $_intIdUSPCProfesor : ge_t03Id_usuario_servicio_perfil_colegio campo de base de datos.
	 * @return $_arrGrados :arreglo de grados asociados a los grupos que existen para el profesor.
	 */
	public function obtenerGradosReportes($_intIdUSPCProfesor) {
		$_arrGrados = array();
		$adapter = $this->adapter;
		
		$consulta = "SELECT 
						'K' AS nivel,
						ge_t07.ge_t07id_profesor_grupo_grado_escolaridad as t36id_grupo_diag, 
						ge_t07.ge_t07descripcion as t36nombre_grupo, 
						ge_t07.ge_t07ciclo AS t36fecha_alta
					FROM 
						ge_t07profesor_grupo_grado_escolaridad as ge_t07 
					INNER JOIN t37respuesta as t37
						ON(t37.t36id_grupo_diag = ge_t07.ge_t07id_profesor_grupo_grado_escolaridad) 
					WHERE  
						ge_t07.ge_t03id_usuario_servicio_perfil_colegio = ".$_intIdUSPCProfesor." 
						and ge_t07.ge_t07id_profesor_grupo_grado_escolaridad IN (
													SELECT DISTINCT
														t36id_grupo_diag
													FROM
														t37respuesta
													WHERE
														t37id_examen IN (35))
					GROUP BY t37.t36id_grupo_diag
					UNION ALL SELECT 
						'P' AS nivel,
						ge_t07.ge_t07id_profesor_grupo_grado_escolaridad as t36id_grupo_diag, 
						ge_t07.ge_t07descripcion as t36nombre_grupo, 
						ge_t07.ge_t07ciclo AS t36fecha_alta
					FROM 
						ge_t07profesor_grupo_grado_escolaridad as ge_t07 
					INNER JOIN t37respuesta as t37
						ON(t37.t36id_grupo_diag = ge_t07.ge_t07id_profesor_grupo_grado_escolaridad) 
					WHERE  
						ge_t07.ge_t03id_usuario_servicio_perfil_colegio = ".$_intIdUSPCProfesor." 
						and ge_t07.ge_t07id_profesor_grupo_grado_escolaridad IN (
													SELECT DISTINCT
														t36id_grupo_diag
													FROM
														t37respuesta
													WHERE
														t37id_examen IN (23 , 24, 25, 26, 27, 28))
					GROUP BY t37.t36id_grupo_diag 
					UNION ALL SELECT 
						'S' AS nivel,
						ge_t07.ge_t07id_profesor_grupo_grado_escolaridad as t36id_grupo_diag, 
						ge_t07.ge_t07descripcion as t36nombre_grupo, 
						ge_t07.ge_t07ciclo AS t36fecha_alta
					FROM 
						ge_t07profesor_grupo_grado_escolaridad as ge_t07 
					INNER JOIN t37respuesta as t37
						ON(t37.t36id_grupo_diag = ge_t07.ge_t07id_profesor_grupo_grado_escolaridad) 
					WHERE  
						ge_t07.ge_t03id_usuario_servicio_perfil_colegio = ".$_intIdUSPCProfesor." 
						and ge_t07.ge_t07id_profesor_grupo_grado_escolaridad IN (
													SELECT DISTINCT
														t36id_grupo_diag
													FROM
														t37respuesta
													WHERE
														t37id_examen IN (29 , 30, 31))
					GROUP BY t37.t36id_grupo_diag 
					UNION ALL SELECT 
						'B' AS nivel,
						ge_t07.ge_t07id_profesor_grupo_grado_escolaridad as t36id_grupo_diag, 
						ge_t07.ge_t07descripcion as t36nombre_grupo, 
						ge_t07.ge_t07ciclo AS t36fecha_alta
					FROM 
						ge_t07profesor_grupo_grado_escolaridad as ge_t07 
					INNER JOIN t37respuesta as t37
						ON(t37.t36id_grupo_diag = ge_t07.ge_t07id_profesor_grupo_grado_escolaridad) 
					WHERE  
						ge_t07.ge_t03id_usuario_servicio_perfil_colegio = ".$_intIdUSPCProfesor."
						and ge_t07.ge_t07id_profesor_grupo_grado_escolaridad IN (
													SELECT DISTINCT
														t36id_grupo_diag
													FROM
														t37respuesta
													WHERE
														t37id_examen IN (32 , 33, 34))
					GROUP BY t37.t36id_grupo_diag;";
		
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGrados = $resultSet->toArray();
		
		return $_arrGrados;
	}
	
	/**
	 * Se obtienen diversas funciones estadisticas de un grupo para generar el reporte correspondiente.
	 * 
	 * @param int $_intIdGrupo
	 * @param boolean $_boolOptionGrp
	 * @return array : $_arrExms par de arreglos que se forma apartir de datos estadisticos funciones para minimo
	 * 				   maximo y promedio de respuestas acertadas por cada alumno.
	 */
	public function obtenerReporteGrupo( $_intIdGrupo , $_boolOptionGrp = true ){
		$_strOptionId = ($_boolOptionGrp == true) ? "t36id_grupo_diag" : "t37id_alumno" ;
		
		$_arrResExam1 = array();
		$_arrResExam2 = array();
		$adapter = $this->adapter;
		$consulta = "SELECT 
						t37nombre AS Nombre, 
						DATE_FORMAT(t37fecha_subido, '%d/%m/%Y') AS Fecha, 
						t37q1  AS 01Id, 
						t37q2   AS 02Di, 
						t37q3   AS 03Co, 
						t37q4   AS 04Cl, 
						t37q5   AS 05An, 
						t37q6   AS 06Si, 
						t37q7   AS 07PD, 
						t37q8   AS 08PA, 
						t37q9   AS 09PT, 
						t37q10   AS 10PL, 
						t37C1 AS 1RLV 
					FROM 
						t37respuesta 
					WHERE 
						".$_strOptionId." = " . $_intIdGrupo . " 
					AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) 
					UNION ALL SELECT 
								'', 
								'MIN', 
								MIN(t37q1),  
								MIN(t37q2),  
								MIN(t37q3),  
								MIN(t37q4),  
								MIN(t37q5),  
								MIN(t37q6),  
								MIN(t37q7),  
								MIN(t37q8),  
								MIN(t37q9),  
								MIN(t37q10), 
								MIN(t37C1) 
							FROM 
								t37respuesta 
							WHERE 
								".$_strOptionId." = " . $_intIdGrupo . " 
							AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) 
					UNION ALL SELECT 
								'', 
								'MAX', 
								Max(t37q1),  
								Max(t37q2),  
								Max(t37q3),  
								Max(t37q4),  
								Max(t37q5),  
								Max(t37q6),  
								Max(t37q7),  
								Max(t37q8),  
								Max(t37q9),  
								Max(t37q10), 
								Max(t37C1) 
							FROM 
								t37respuesta 
							WHERE 
								".$_strOptionId." = " . $_intIdGrupo . "  
							AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) 
					UNION ALL SELECT 
								'', 
								'AVEG', 
								CAST(AVG(CAST(t37q1  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q2  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q3  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q4  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q5  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q6  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q7  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q8  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q9  AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q10 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37C1 AS decimal)) AS decimal(5,3)) 
							FROM 
								t37respuesta 
							WHERE 
								".$_strOptionId." = " . $_intIdGrupo." 
							AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno)";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrResExam1 = $resultSet->toArray();
		
		$consulta = "SELECT 
						t37nombre AS Nombre, 
						DATE_FORMAT(t37fecha_subido, '%d/%m/%Y') AS Fecha, 
						t37q11 AS 11IdM, 
						t37q12 AS 12DiM, 
						t37q13 AS 13CoM, 
						t37q14 AS 14ClM, 
						t37q15 AS 15AnM, 
						t37q16 AS 16SiM, 
						t37q17 AS 17PDM, 
						t37q18 AS 18PAM, 
						t37q19 AS 19PTM, 
						t37q20 AS 20PLM, 
						t37C2 AS 2RLM 
					FROM 
						t37respuesta 
					WHERE 
						".$_strOptionId." = " . $_intIdGrupo . " 
					AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) 
					UNION ALL SELECT 
								'', 
								'MIN', 
								MIN(t37q11), 
								MIN(t37q12), 
								MIN(t37q13), 
								MIN(t37q14), 
								MIN(t37q15), 
								MIN(t37q16), 
								MIN(t37q17), 
								MIN(t37q18), 
								MIN(t37q19), 
								MIN(t37q20), 
								MIN(t37C2) 
							FROM 
								t37respuesta 
							WHERE 
								".$_strOptionId." = " . $_intIdGrupo . " 
							AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno)  
					UNION ALL SELECT 
								'', 
								'MAX', 
								Max(t37q11), 
								Max(t37q12), 
								Max(t37q13), 
								Max(t37q14), 
								Max(t37q15), 
								Max(t37q16), 
								Max(t37q17), 
								Max(t37q18), 
								Max(t37q19), 
								Max(t37q20), 
								Max(t37C2) 
							FROM 
								t37respuesta 
							WHERE 
								".$_strOptionId." = " . $_intIdGrupo . " 
							AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) 
					UNION ALL SELECT 
								'', 
								'AVEG', 
								CAST(AVG(CAST(t37q11 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q12 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q13 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q14 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q15 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q16 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q17 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q18 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q19 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37q20 AS decimal)) AS decimal(5,3)), 
								CAST(AVG(CAST(t37C2 AS decimal)) AS decimal(5,3)) 
							FROM 
								t37respuesta 
							WHERE 
								".$_strOptionId." = " . $_intIdGrupo."
							AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno)";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrResExam2 = $resultSet->toArray();
		$_arrExms = array(0=>$_arrResExam1, 1=>$_arrResExam2);
		return $_arrExms;
	}
	
	/**
	 * 
	 * @param int $_intIdGrupo
	 * @param boolean $_boolOptionGrp
	 * @return array : con las graficas correspondientes.
	 */
	public function generarGraficaG1( $_intIdGrupo, $_boolOptionGrp = true ){
		$_strOptionId = ($_boolOptionGrp == true) ? "t36id_grupo_diag" : "t37id_alumno" ;
		
		$_arrGra1 = array();
		$_arrGra2 = array();
		$adapter = $this->adapter;
		$consulta = "SELECT 
						t37q1,  
						t37q2,  
						t37q3,  
						t37q4,  
						t37q5,  
						t37q6,  
						t37q7,  
						t37q8,  
						t37q9,  
						t37q10  
					FROM 
						t37respuesta 
					WHERE 
					   ".$_strOptionId." = ". $_intIdGrupo."  
					 AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) ";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGra1 = $resultSet->toArray();
		$consulta = "SELECT 
						t37q11, 
						t37q12, 
						t37q13, 
						t37q14, 
						t37q15, 
						t37q16, 
						t37q17, 
						t37q18, 
						t37q19, 
						t37q20  
					FROM 
						t37respuesta 
					WHERE 
						".$_strOptionId." = ". $_intIdGrupo."  
					 AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) ";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGra2 = $resultSet->toArray();
		$_dblG1_1 = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		$_intq = 0;
		foreach($_arrGra1 as $dato) {
			for($i = 1; $i <= count($dato)+1; $i++) {
				if( isset($dato['t37q'.$i] ) ){
					if ($dato['t37q'.$i] == 1){
						$_intq ++;
					}
				}
			}
			$_intres = $_dblG1_1[$_intq];
			$_intres ++;
			$_dblG1_1[$_intq] = $_intres;
			$_intq = 0;
		}
		$_dblG1_2 = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		foreach($_arrGra2 as $dato) {
			for($i = 1; $i <= count($dato)+1; $i++) {
				if( isset($dato['t37q'.($i+9)] ) ){
					if ($dato['t37q'.($i+9)] == 1){
						$_intq ++;
					}
				}
			}
			$_intres = $_dblG1_2[$_intq];
			$_intres ++;
			$_dblG1_2[$_intq] = $_intres;
			$_intq = 0;
		}
		$labels1 = array( "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" );
		$colors = array( 0xb8bc9c, 0xa0bdc4, 0x999966, 0x333366, 0xc3c3e6 );
		$c1 = new XYChart(800, 450, 0xf8f8f8, 0xffffff);
		$c1->setPlotArea(50, 50, 730, 360);
		$c1->xAxis()->setLabels($labels1);
		$c1->xAxis()->setTitle("No. de aciertos", "Arial Bold", 10);
		$title_c1 = $c1->addTitle("Número de alumnos con aciertos por área", "Arial Bold", 10);
		$title_c1->setBackground(metalColor(0x9999ff), -1, 1);
		$legendBox_c1 = $c1->addLegend(55, 18, false, "Verdana", 10);
		$legendBox_c1->setBackground(Transparent);
		$c1->yAxis()->setTopMargin(20);
		$c1->yAxis()->setTitle("No. de Alumnos", "Arial Bold", 10);
		$layer1 = $c1->addBarLayer2(Side);
		$layer1->addDataSet($_dblG1_1, 0x80ff80, "Raz. Lingüístico Verbal");
		$layer1->addDataSet($_dblG1_2, 0x8080ff, "Raz. Lógico Matemático");
		$chart1URL = $c1->makeSession("chart1");
		$chart1ImageMap = $c1->getHTMLImageMap("", "", "title='{dataSetName} correctas {xLabel}: {value} alumnos'");
		return array(
					'url' => $chart1URL, 
					'map' => $chart1ImageMap);
	}
	
	/**
	 * 
	 * @param int $_intIdGrupo
	 * @param boolean $_boolOptionGrp
	 * @return multitype:unknown string
	 */
	public function generarGraficaG2( $_intIdGrupo ,$_boolOptionGrp = true){
		$_strOptionId = ($_boolOptionGrp == true) ? "t36id_grupo_diag" : "t37id_alumno" ;
		$_arrGra1 = array();
		$adapter = $this->adapter;
		$consulta = "SELECT 
						SUM(t37q1)  AS '0',  
						SUM(t37q2)  AS '1',  
						SUM(t37q3)  AS '2',  
						SUM(t37q4)  AS '3',  
						SUM(t37q5)  AS '4',  
						SUM(t37q6)  AS '5',  
						SUM(t37q7)  AS '6',  
						SUM(t37q8)  AS '7',  
						SUM(t37q9)  AS '8',  
						SUM(t37q10) AS '9'  
					FROM 
						t37respuesta 
					WHERE 
						".$_strOptionId." =" .$_intIdGrupo."  
					 AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) ";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGra1 = $resultSet->toArray();
		$_arrGra1 = $_arrGra1[0];
		$labrlv = array( "01Id", "02Di", "03Co", "04Cl", "05An", "06Si", "07PD", "08PA", "09PT", "10PL" );
		$c2 = new XYChart(800, 450, 0xf8f8f8, 0xffffff);
		$c2->setPlotArea(50, 50, 730, 340);
		$labels_c2 = $c2->xAxis()->setLabels($labrlv);
		$labels_c2->setFontAngle(-45);
		$c2->xAxis()->setTitle("Areas Raz. Lingüístico Verbal", "Arial Bold", 10);
		$title_c2 = $c2->addTitle("Número de alumnos con aciertos en Raz. Lingüístico Verbal", "Arial Bold", 10);
		$title_c2->setBackground(metalColor(0x9999ff), -1, 1);
		$legendBox_c2 = $c2->addLegend(55, 18, false, "Verdana", 10);
		$legendBox_c2->setBackground(Transparent);
		$c2->yAxis()->setTopMargin(20);
		$c2->yAxis()->setTitle("No. de Alumnos", "Arial Bold", 10);
		$layer2 = $c2->addBarLayer2(Side);
		$layer2->addDataSet($_arrGra1, 0x80ff80, "Raz. Lingüístico Verbal");
		$chart2URL = $c2->makeSession("chart2");
		$chart2ImageMap = $c2->getHTMLImageMap("", "", "title='{dataSetName} correctas {xLabel}: {value} alumnos'");
		return array('url2' => $chart2URL, 'map2' => $chart2ImageMap);
	}
	
	/**
	 * 
	 * @param int $_intIdGrupo
	 * @param boolean $_boolOptionGrp
	 * @return multitype:unknown string
	 */
	public function generarGraficaG3( $_intIdGrupo, $_boolOptionGrp = true ){
		$_strOptionId = ($_boolOptionGrp == true) ? "t36id_grupo_diag" : "t37id_alumno" ;
		$_arrGra1 = array();
		$adapter = $this->adapter;
		$consulta = "SELECT 
						SUM(t37q11) AS '0', 
						SUM(t37q12) AS '1', 
						SUM(t37q13) AS '2', 
						SUM(t37q14) AS '3', 
						SUM(t37q15) AS '4', 
						SUM(t37q16) AS '5', 
						SUM(t37q17) AS '6', 
						SUM(t37q18) AS '7', 
						SUM(t37q19) AS '8', 
						SUM(t37q20) AS '9' 
					FROM 
						t37respuesta 
					WHERE 
						".$_strOptionId." =" . $_intIdGrupo."  
					 AND t37id_calificacion IN (SELECT MAX(t37id_calificacion) AS t37id_calificacion FROM t37respuesta WHERE ".$_strOptionId." = ". $_intIdGrupo ." GROUP BY t37id_alumno) ";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGra1 = $resultSet->toArray();
		$_arrGra1 = $_arrGra1[0];
		$labrlm = array( "11IdM", "12DiM", "13CoM", "14ClM", "15AnM", "16SiM", "17PDM", "18PAM", "19PTM", "20PLM" );
		$c3 = new XYChart(800, 450, 0xf8f8f8, 0xffffff);
		$c3->setPlotArea(50, 50, 730, 340);
		$labels_c3 = $c3->xAxis()->setLabels($labrlm);
		$labels_c3->setFontAngle(-45);
		$c3->xAxis()->setTitle("Areas Raz. Lógico Matemático", "Arial Bold", 10);
		$title_c3 = $c3->addTitle("Número de alumnos con aciertos en Raz. Lógico Matemático", "Arial Bold", 10);
		$title_c3->setBackground(metalColor(0x9999ff), -1, 1);
		$legendBox_c3 = $c3->addLegend(55, 18, false, "Verdana", 10);
		$legendBox_c3->setBackground(Transparent);
		$c3->yAxis()->setTopMargin(20);
		$c3->yAxis()->setTitle("No. de Alumnos", "Arial Bold", 10);
		$layer3 = $c3->addBarLayer2(Side);
		$layer3->addDataSet($_arrGra1, 0x8080ff, "Raz. Lógico Matemático");
		$chart3URL = $c3->makeSession("chart3");
		$chart3ImageMap = $c3->getHTMLImageMap("", "", "title='{dataSetName} correctas {xLabel}: {value} alumnos'");
		return array('url3' => $chart3URL, 'map3' => $chart3ImageMap);
	}
	
	/**
	 * 
	 * @param int $_intGrupos
	 * @param int $_intIdCol
	 * @return multitype:multitype: Ambigous <multitype:, multitype:unknown NULL >
	 */
	public function obtenerReporteCompGrupo( $_intGrupos, $_intIdCol ){
		$session_id = session_id();
		$adapter = $this->adapter;
		$consulta = "CREATE TABLE 
						temp_col" . $_intIdCol . "_" . $session_id . " (
						Nombre varchar(150),  
						01Id decimal(5,3),  
						02Di decimal(5,3),  
						03Co decimal(5,3),  
						04Cl decimal(5,3),  
						05An decimal(5,3),  
						06Si decimal(5,3),  
						07PD decimal(5,3),  
						08PA decimal(5,3),  
						09PT decimal(5,3),  
						10PL decimal(5,3), 
						1RLV decimal(5,3))";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$consulta = "CREATE TABLE 
						temp_col" . $_intIdCol . "_2_" . $session_id . " (
						Nombre varchar(150), 
						11IdM decimal(5,3), 
						12DiM decimal(5,3), 
						13CoM decimal(5,3), 
						14ClM decimal(5,3), 
						15AnM decimal(5,3), 
						16SiM decimal(5,3), 
						17PDM decimal(5,3), 
						18PAM decimal(5,3), 
						19PTM decimal(5,3), 
						20PLM decimal(5,3), 
						2RLM decimal(5,3))";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$consulta = "INSERT INTO 
						temp_col" . $_intIdCol . "_" . $session_id . "  
					 SELECT 
						  (SELECT 
								CONCAT_WS(' - ',ge_t07.ge_t07descripcion, ge_t07.ge_t07ciclo) 
							FROM 
								ge_t07profesor_grupo_grado_escolaridad as ge_t07 
							WHERE
								ge_t07.ge_t03id_usuario_servicio_perfil_colegio = ".$_intIdCol." AND
								ge_t07.ge_t07id_profesor_grupo_grado_escolaridad = t37.t36id_grupo_diag) AS Nombre, 
							CAST(AVG(CAST(t37q1 AS decimal)) AS decimal(5,3))  AS 01Id, 
							CAST(AVG(CAST(t37q2 AS decimal)) AS decimal(5,3))  AS 02Di,  
							CAST(AVG(CAST(t37q3 AS decimal)) AS decimal(5,3))  AS 03Co,  
							CAST(AVG(CAST(t37q4 AS decimal)) AS decimal(5,3))  AS 04Cl,  
							CAST(AVG(CAST(t37q5 AS decimal))  AS decimal(5,3)) AS 05An,  
							CAST(AVG(CAST(t37q6 AS decimal)) AS decimal(5,3))  AS 06Si,  
							CAST(AVG(CAST(t37q7 AS decimal)) AS decimal(5,3))  AS 07PD,  
							CAST(AVG(CAST(t37q8 AS decimal)) AS decimal(5,3))  AS 08PA,  
							CAST(AVG(CAST(t37q9 AS decimal)) AS decimal(5,3))  AS 09PT,  
							CAST(AVG(CAST(t37q10 AS decimal)) AS decimal(5,3)) AS 10PL,  
							CAST(AVG(CAST(t37C1 AS decimal)) AS decimal(5,3)) AS 1RLV 
						FROM 
							t37respuesta t37 
						WHERE 
							t37.t36id_grupo_diag IN (" . $_intGrupos . ")  
						AND t37id_calificacion IN (
									SELECT MAX(t37id_calificacion) AS t37id_calificacion 
									FROM t37respuesta 
									WHERE t37.t36id_grupo_diag IN ( ". $_intGrupos ." ) 
									GROUP BY t37id_alumno) 
						GROUP BY t37.t36id_grupo_diag 
						ORDER BY Nombre;";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$consulta = "INSERT INTO 
						temp_col" . $_intIdCol . "_2_" . $session_id . " 
						SELECT 
							(SELECT 
								CONCAT_WS(' - ',ge_t07.ge_t07descripcion, ge_t07.ge_t07ciclo) 
							FROM 
								ge_t07profesor_grupo_grado_escolaridad as ge_t07 
							WHERE
								ge_t07.ge_t03id_usuario_servicio_perfil_colegio = ".$_intIdCol." AND
								ge_t07.ge_t07id_profesor_grupo_grado_escolaridad = t37.t36id_grupo_diag) AS Nombre, 
							CAST(AVG(CAST(t37q11 AS decimal)) AS decimal(5,3)) AS 11IdM, 
							CAST(AVG(CAST(t37q12 AS decimal)) AS decimal(5,3)) AS 12DiM, 
							CAST(AVG(CAST(t37q13 AS decimal)) AS decimal(5,3)) AS 13CoM, 
							CAST(AVG(CAST(t37q14 AS decimal)) AS decimal(5,3)) AS 14ClM, 
							CAST(AVG(CAST(t37q15 AS decimal)) AS decimal(5,3)) AS 15AnM, 
							CAST(AVG(CAST(t37q16 AS decimal)) AS decimal(5,3)) AS 16SiM, 
							CAST(AVG(CAST(t37q17 AS decimal)) AS decimal(5,3)) AS 17PDM, 
							CAST(AVG(CAST(t37q18 AS decimal)) AS decimal(5,3)) AS 18PAM, 
							CAST(AVG(CAST(t37q19 AS decimal)) AS decimal(5,3)) AS 19PTM, 
							CAST(AVG(CAST(t37q20 AS decimal)) AS decimal(5,3)) AS 20PLM, 
							CAST(AVG(CAST(t37C2 AS decimal)) AS decimal(5,3)) AS 2RLM 
						FROM 
							t37respuesta t37 
						WHERE 
							t37.t36id_grupo_diag IN (" . $_intGrupos . ") 
							AND t37id_calificacion IN (
									SELECT MAX(t37id_calificacion) AS t37id_calificacion 
									FROM t37respuesta 
									WHERE t37.t36id_grupo_diag IN ( ". $_intGrupos ." ) 
									GROUP BY t37id_alumno) 
						GROUP BY t37.t36id_grupo_diag 
						ORDER BY Nombre";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$consulta = "SELECT * 
					 FROM 
						temp_col" . $_intIdCol . "_" . $session_id . "   
					UNION ALL SELECT 
						'MIN', 
						MIN(01Id) , 
						MIN(02Di) , 
						MIN(03Co) , 
						MIN(04Cl) , 
						MIN(05An) , 
						MIN(06Si) , 
						MIN(07PD) , 
						MIN(08PA) , 
						MIN(09PT) , 
						MIN(10PL) , 
						MIN(1RLV) 
					FROM 
						temp_col" . $_intIdCol . "_" . $session_id . "   
				    UNION ALL SELECT 
						'MAX', 
						MAX(01Id) , 
						MAX(02Di) , 
						MAX(03Co) , 
						MAX(04Cl) , 
						MAX(05An) , 
						MAX(06Si) , 
						MAX(07PD) , 
						MAX(08PA) , 
						MAX(09PT) , 
						MAX(10PL) , 
						MAX(1RLV) 
					FROM 
						temp_col" . $_intIdCol . "_" . $session_id . "   
					UNION ALL SELECT 
						'AVEG', 
						CAST(AVG(01Id) AS decimal(5,3)) , 
						CAST(AVG(02Di) AS decimal(5,3)) , 
						CAST(AVG(03Co) AS decimal(5,3)) , 
						CAST(AVG(04Cl) AS decimal(5,3)) , 
						CAST(AVG(05An) AS decimal(5,3)) , 
						CAST(AVG(06Si) AS decimal(5,3)) , 
						CAST(AVG(07PD) AS decimal(5,3)) , 
						CAST(AVG(08PA) AS decimal(5,3)) , 
						CAST(AVG(09PT) AS decimal(5,3)) , 
						CAST(AVG(10PL) AS decimal(5,3)) , 
						CAST(AVG(1RLV) AS decimal(5,3)) 
					FROM 
						temp_col" . $_intIdCol . "_" . $session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrResExam1 = $resultSet->toArray();
		$consulta = "SELECT * 
					 FROM 
						temp_col" . $_intIdCol . "_2_" . $session_id . " 
					 UNION ALL SELECT 
							'MIN', 
							MIN(11IdM), 
							MIN(12DiM), 
							MIN(13CoM), 
							MIN(14ClM), 
							MIN(15AnM), 
							MIN(16SiM), 
							MIN(17PDM), 
							MIN(18PAM), 
							MIN(19PTM), 
							MIN(20PLM), 
							MIN(2RLM) 
						FROM 
							temp_col" . $_intIdCol . "_2_" . $session_id . " 
					UNION ALL SELECT 
							'MAX', 
							MAX(11IdM), 
							MAX(12DiM), 
							MAX(13CoM), 
							MAX(14ClM), 
							MAX(15AnM), 
							MAX(16SiM), 
							MAX(17PDM), 
							MAX(18PAM), 
							MAX(19PTM), 
							MAX(20PLM), 
							MAX(2RLM) 
						FROM 
							temp_col" . $_intIdCol . "_2_" . $session_id . " 
					UNION ALL SELECT 
							'AVEG', 
							CAST(AVG(11IdM) AS decimal(5,3)), 
							CAST(AVG(12DiM) AS decimal(5,3)), 
							CAST(AVG(13CoM) AS decimal(5,3)), 
							CAST(AVG(14ClM) AS decimal(5,3)), 
							CAST(AVG(15AnM) AS decimal(5,3)), 
							CAST(AVG(16SiM) AS decimal(5,3)), 
							CAST(AVG(17PDM) AS decimal(5,3)), 
							CAST(AVG(18PAM) AS decimal(5,3)), 
							CAST(AVG(19PTM) AS decimal(5,3)), 
							CAST(AVG(20PLM) AS decimal(5,3)), 
							CAST(AVG(2RLM) AS decimal(5,3)) 
						FROM 
							temp_col" . $_intIdCol . "_2_" .$session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrResExam2 = $resultSet->toArray();
		$_arrExms = array(0=>$_arrResExam1, 1=>$_arrResExam2);
		return $_arrExms;
	}
	
	/**
	 * 
	 * @param int $_intIdCol
	 * @return multitype:unknown string
	 */
	public function generarGraficaGrupo( $_intIdCol ){
		$session_id = session_id();
		$adapter = $this->adapter;
		$consulta = "SELECT 
						temp_col" . $_intIdCol . "_" . $session_id . ".Nombre, 
						AVG(01Id), AVG(02Di), AVG(03Co), AVG(04Cl), 
						AVG(05An), AVG(06Si), AVG(07PD), AVG(08PA), 
						AVG(09PT), AVG(10PL), AVG(1RLV), AVG(11IdM), 
						AVG(12DiM), AVG(13CoM), AVG(14ClM), AVG(15AnM), 
						AVG(16SiM), AVG(17PDM), AVG(18PAM), AVG(19PTM), 
						AVG(20PLM), AVG(2RLM) 
					from 
						temp_col" . $_intIdCol . "_" . $session_id . " 
					LEFT OUTER JOIN 
						temp_col" . $_intIdCol . "_2_" . $session_id . " 
						ON temp_col" . $_intIdCol . "_" . $session_id . ".Nombre = temp_col" . $_intIdCol . "_2_" . $session_id . ".Nombre 
					Group By temp_col" . $_intIdCol . "_" . $session_id . ".Nombre 
					Order By temp_col" . $_intIdCol . "_" . $session_id . ".Nombre";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrResExam = $resultSet->toArray();
		$consulta = "DROP TABLE temp_col" . $_intIdCol . "_" . $session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$consulta = "DROP TABLE temp_col" . $_intIdCol . "_2_" . $session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$labels = array( "01Id", "02Di", "03Co", "04Cl", "05An", "06Si", "07PD", "08PA", "09PT", "10PL", "1RLV", "11IdM", 
						"12DiM", "13CoM", "14ClM", "15AnM", "16SiM", "17PDM", "18PAM", "19PTM", "20PLM", "2RLM" );
		$c = new XYChart(1000, 550, 0xf8f8f8, 0xffffff);
		$c->setPlotArea(50, 70, 930, 420);
		$labels_c = $c->xAxis()->setLabels($labels);
		$labels_c->setFontAngle(-45);
		$c->xAxis()->setTitle("Área", "Arial Bold", 10);
		$title_c = $c->addTitle("Porcentaje de aciertos por sección y por área de razonamiento", "Arial Bold", 10);
		$title_c->setBackground(metalColor(0x9999ff), -1, 1);
		$legendBox_c = $c->addLegend(55, 18, false, "Verdana", 10);
		$legendBox_c->setBackground(Transparent);
		$c->yAxis()->setTopMargin(20);
		$c->yAxis()->setTitle("Porcentaje", "Arial Bold", 10);
		$layer = $c->addBarLayer2(Side);
		#
		foreach ($_arrResExam as $dato){
			$_dblG1 = array_values($dato);
			$_serie = $_dblG1[0];
			$_tmp = array_shift($_dblG1);
			$layer->addDataSet($_dblG1, -1, $_serie);
		}
		#
		$chart1URL = $c->makeSession("chart1");
		$chart1ImageMap = $c->getHTMLImageMap("", "", "title='{dataSetName} en {xLabel}: {value} %'");
		return array('url1' => $chart1URL, 'map1' => $chart1ImageMap);
	}
	
	/**
	 * 
	 * @param unknown $_intGrados
	 * @param unknown $_intIdCol
	 * @return Ambigous <multitype:, multitype:unknown NULL >
	 */
	public function obtenerReporteCompGrado( $_intGrados, $_intIdCol ){
		$session_id = session_id();
		$adapter = $this->adapter;
		$consulta = "
				CREATE TABLE 
					temp_col_gen" . $_intIdCol . "_" . $session_id . 
				" (Nombre varchar(150),
				 1RLV decimal(5,3), 
				 2RLM decimal(5,3))";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		
		$consulta = "INSERT INTO 
						temp_col_gen" . $_intIdCol . "_" . $session_id . " 
						SELECT (SELECT 
									CONCAT_WS(' - ',ge_t07.ge_t07descripcion, ge_t07.ge_t07ciclo) 
								FROM 
									ge_t07profesor_grupo_grado_escolaridad as ge_t07 
								WHERE
									ge_t07.ge_t03id_usuario_servicio_perfil_colegio = ".$_intIdCol." AND
									ge_t07.ge_t07id_profesor_grupo_grado_escolaridad = t37.t36id_grupo_diag) AS Nombre, 
								CAST(AVG(CAST(t37C1 AS decimal)) AS decimal(5,3)) AS 1RLV, 
								CAST(AVG(CAST(t37C2 AS decimal)) AS decimal(5,3)) AS 2RLM 
							FROM 
								t37respuesta t37 
							WHERE 
								t37.t36id_grupo_diag IN (" . $_intGrados . ") 
								AND t37id_calificacion IN (
									SELECT MAX(t37id_calificacion) AS t37id_calificacion 
									FROM t37respuesta 
									WHERE t37.t36id_grupo_diag IN ( ". $_intGrados ." )
									GROUP BY t37id_alumno) 
							GROUP BY t37.t36id_grupo_diag 
							ORDER BY Nombre";
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		
		$consulta = "SELECT * 
					 FROM  
					  	temp_col_gen" . $_intIdCol . "_" . $session_id . " 
					  UNION ALL SELECT 
					  			'MIN', 
					  			MIN(1RLV), 
					  			MIN(2RLM) 
					  	FROM temp_col_gen" . $_intIdCol . "_" . $session_id . " 
					  UNION ALL SELECT 
					  			'MAX', 
					  			MAX(1RLV), 
					  			MAX(2RLM) 
					  	FROM temp_col_gen" . $_intIdCol . "_" . $session_id . " 
					  UNION ALL SELECT 
					  			'AVEG', 
					  			CAST(AVG(1RLV) AS decimal(5,3)), 
					  			CAST(AVG(2RLM) AS decimal(5,3)) 
					  		FROM temp_col_gen" . $_intIdCol . "_" . $session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrResExam = $resultSet->toArray();
		
		return $_arrResExam;
	}
	
	/**
	 * 
	 * @param unknown $_intIdCol
	 * @return multitype:unknown string
	 */
	public function generarGraficaGrado( $_intIdCol ){
		$session_id = session_id();
		$adapter = $this->adapter;
		$consulta = "SELECT 
						Nombre, 
						1RLV 
					FROM 
						temp_col_gen" . $_intIdCol . "_" . $session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGra1 = $resultSet->toArray();
		$consulta = "SELECT 
						Nombre, 
						2RLM 
					FROM 
						temp_col_gen" . $_intIdCol . "_" . $session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$resultSet = new ResultSet;
		$resultSet->initialize($result);
		$_arrGra2 = $resultSet->toArray();
		$consulta = "DROP TABLE 
						temp_col_gen" . $_intIdCol . "_" . $session_id;
		$statement = $adapter->query($consulta);
		$result = $statement->execute();
		$c1 = new XYChart(800, 550, 0xf8f8f8, 0xffffff);
		$c1->setPlotArea(50, 50, 730, 330);
		$title_c1 = $c1->addTitle("Raz. Lingüístico Verbal", "Arial Bold", 10);
		$title_c1->setBackground(metalColor(0x9999ff), -1, 1);
		$legendBox_c1 = $c1->addLegend(55, 18, false, "Verdana", 10);
		$legendBox_c1->setBackground(Transparent);
		#
		$_intpos = 0;
		foreach ($_arrGra1 as $dato){
			$_dblG1 = array_values($dato);
			$labels1[$_intpos] = $_dblG1[0];
			$datat1[$_intpos] = $_dblG1[1];
			$_intpos ++;
		}
		#
		$c1->addBarLayer($datat1, 0x80ff80);
		$labels_c1 = $c1->xAxis()->setLabels($labels1);
		$labels_c1->setFontAngle(-75);
		$c2 = new XYChart(800, 550, 0xf8f8f8, 0xffffff);
		$c2->setPlotArea(50, 50, 730, 330);
		$title_c2 = $c2->addTitle("Raz. Lógico Matemático", "Arial Bold", 10);
		$title_c2->setBackground(metalColor(0x9999ff), -1, 1);
		$legendBox_c2 = $c2->addLegend(55, 18, false, "Verdana", 10);
		$legendBox_c2->setBackground(Transparent);
		#
		$_intpos = 0;
		foreach ($_arrGra2 as $dato){
			$_dblG2 = array_values($dato);
			$labels2[$_intpos] = $_dblG2[0];
			$datat2[$_intpos] = $_dblG2[1];
			$_intpos ++;
		}
		#
		$c2->addBarLayer($datat2, 0x8080ff);
		$labels_c2 = $c2->xAxis()->setLabels($labels2);
		$labels_c2->setFontAngle(-75);
		$chart1URL = $c1->makeSession("chart1");
		$chart1ImageMap = $c1->getHTMLImageMap("", "", "title='{xLabel}: {value}%'");
		$chart2URL = $c2->makeSession("chart2");
		$chart2ImageMap = $c2->getHTMLImageMap("", "", "title='{xLabel}: {value}%'");
		return array('url1' => $chart1URL, 'map1' => $chart1ImageMap, 'url2' => $chart2URL, 'map2' => $chart2ImageMap);
	}
}
?>