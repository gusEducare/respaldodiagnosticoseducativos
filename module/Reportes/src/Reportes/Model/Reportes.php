<?php
/**
 *
 * La clase Reportes se encargará de obtener los colegios, sus grupos,
 * examenes calificados
 * @since 16-09-2014
 * @author lfcelaya
 */

namespace Reportes\Model;


//use Zend\Db\Sql;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

use Grupos\Model\Debug;


class Reportes{
	
	
	
	/**
	 * Instancia de la clase  Zend\Db\Adapter\Adapter;
                 *  @var Adapter  $adapter
	 * 
	 */
	protected $adapter;
        
        /**
                *  Instancia de la clase  Grupos\Model\Debug
	 * @var Debug $_objLogger
	 *
	 */
	protected $_objLogger;
	
	/**
                *  Array de configuracion 
	 * @var Array $config
	 *
	 */
        public $config;
	
	public function __construct(Adapter $adapter, $config){
                $this->config = $config;
		$this->adapter = $adapter;
		$this->_objLogger = new Debug($config);
	}
	
	public function getReporteAlumno($_idUsuarioPerfil, $_idExamen){
		
		$_strQuestion = " SELECT 
                                        c11.c11id_razonamiento_intellectus,
                                        c11nombre,
                                        c10.c10id_proceso_pensamiento_intellectus,
                                        c10nombre,
                                        t10.t10respuesta_usuario,
                                        t10respuesta_correcta,
                                        t12.t12nombre,
                                        t04.t04fecha_fin
                                    FROM 
                                    t10respuesta_usuario_examen t10
                                    INNER JOIN t03licencia_usuario t03 ON (t03.t03id_licencia_usuario = t10.t03id_licencia_usuario)
                                    INNER JOIN t04examen_usuario t04 ON (t04.t03id_licencia_usuario = t03.t03id_licencia_usuario)
                                    INNER JOIN t12examen t12 ON (t12.t12id_examen = t10.t12id_examen)
                                    INNER JOIN t11pregunta t11 ON (t11.t11id_pregunta =  t10.t11id_pregunta)
                                    INNER JOIN t31pregunta_proceso_pensamiento_intellectus t31 ON (t31.t11id_pregunta = t11.t11id_pregunta)
                                    INNER JOIN c10proceso_pensamiento_intellectus c10 ON (c10.c10id_proceso_pensamiento_intellectus = t31.c10id_proceso_pensamiento_intellectus) 
                                    INNER JOIN t32pregunta_nivel_intellectus t32 ON (t32.t11id_pregunta = t11.t11id_pregunta)
                                    INNER JOIN c05nivel c05 ON (c05.c05id_nivel = t32.c05id_nivel)
                                    INNER JOIN t33pregunta_razonamiento_intellectus t33 ON (t33.t11id_pregunta = t11.t11id_pregunta)
                                    INNER JOIN c11razonamiento_intellectus c11 ON (c11.c11id_razonamiento_intellectus = t33.c11id_razonamiento_intellectus)

                                WHERE t12.t12id_examen = :idExamen
                                AND t03.t02id_usuario_perfil = :idUsuarioPerfil
                                AND t03.t03licencia = (
                                                        SELECT t03sub.t03licencia 
                                                        FROM 
                                                        t03licencia_usuario t03sub
                                                        INNER JOIN t04examen_usuario t04sub ON (t04sub.t03id_licencia_usuario = t03sub.t03id_licencia_usuario)      
                                                        WHERE t04sub.t12id_examen = :idExamen 
                                                        AND t03sub.t02id_usuario_perfil = :idUsuarioPerfil
                                                        AND t04sub.t04estatus = :estatusFinalizado
                                                        ORDER BY t03sub.t03fecha_activacion DESC
                                                        LIMIT 1
                                                    )
                                ORDER BY t10fecha_actualiza, t11.t11nombre_corto_clave ";
		
		$statement = $this->adapter->query($_strQuestion);
		$results = $statement->execute(array(
			":idExamen" => $_idExamen,
                        ":idUsuarioPerfil" => $_idUsuarioPerfil,
                        ':estatusFinalizado' => $this->config['constantes']['ESTATUS_EXAMEN_FINALIZADO']
		));
		$resultSet = new ResultSet;
		$resultSet->initialize($results);
		$_arrReactivos = $resultSet->toArray();
                foreach($_arrReactivos as $reactivo){
                    if(isset($_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']])){
                        
                        if(isset($_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento']['proceso_'.$reactivo['c10id_proceso_pensamiento_intellectus']])){
                            if(trim($reactivo['t10respuesta_usuario']) == trim($reactivo['t10respuesta_correcta'])){
                                $_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento']['proceso_'.$reactivo['c10id_proceso_pensamiento_intellectus']]['puntaje'] += 1;
                                $_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']]['puntos'] += 1;
                            }
                        }
                        else{
                            $_proceso = array(
                                'c10id_proceso_pensamiento_intellectus' => $reactivo['c10id_proceso_pensamiento_intellectus'],
                                'c10nombre' => $reactivo['c10nombre'],
                                'puntaje' => trim($reactivo['t10respuesta_usuario']) == trim($reactivo['t10respuesta_correcta']) ? 1 : 0
                            );
                            if(trim($reactivo['t10respuesta_usuario']) == trim($reactivo['t10respuesta_correcta'])){
                                $_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']]['puntos'] += 1;
                            }
                            $_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento']['proceso_'.$reactivo['c10id_proceso_pensamiento_intellectus']] =  $_proceso;
                        }
                        
                    }
                    else{
                        $_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']] = array(
                            'c11id_razonamiento_intellectus' => $reactivo['c11id_razonamiento_intellectus'],
                            'c11nombre'  => $reactivo['c11nombre'],
                            'puntos' => 0,
                            'procesos_pensamiento' => array()
                        );
                        $_proceso = array(
                            'c10id_proceso_pensamiento_intellectus' => $reactivo['c10id_proceso_pensamiento_intellectus'],
                            'c10nombre' => $reactivo['c10nombre'],
                            'puntaje' => trim($reactivo['t10respuesta_usuario']) == trim($reactivo['t10respuesta_correcta']) ? 1 : 0
                        );
                        if(trim($reactivo['t10respuesta_usuario']) == trim($reactivo['t10respuesta_correcta'])){
                            $_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']]['puntos'] += 1;
                        }
                        $_arrInterpretacion[$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento']['proceso_'.$reactivo['c10id_proceso_pensamiento_intellectus']] =  $_proceso;
                    }
                }
                
                // generate data GLOBAL
                $_arrxAxisGlobal = array();
                $_data_chartGlobal = array(
                    'name' => $_arrReactivos[0]['t12nombre']." (".$_arrReactivos[0]['t04fecha_fin'].")",
                    'data' => null
                );
                $_arrDataTemp = array();
                foreach ($_arrInterpretacion as $razonamiento){
                    array_push($_arrxAxisGlobal, $razonamiento['c11nombre']);
                    array_push($_arrDataTemp, $this->getNivelGlobal($razonamiento['puntos']));
                    
                }
                $_data_chartGlobal['data']= $_arrDataTemp;
                
                
                
                // generate data Verbal
                $_arrxAxisVerbal = array();
                $_data_chartVerbal = array(
                    'name' => $_arrReactivos[0]['t12nombre']." (".$_arrReactivos[0]['t04fecha_fin'].")",
                    'data' => null
                );
                $_arrDataTempVerbal = array();
                foreach($_arrInterpretacion[1]['procesos_pensamiento'] as $proceso){
                        array_push($_arrxAxisVerbal, $proceso['c10nombre']);
                        $_arrDataTempVerbal[] = $proceso['puntaje'] == 0 ? 1 : $proceso['puntaje'];
                }
                $_data_chartVerbal['data']= $_arrDataTempVerbal;

                
                $_arrReturn = array(
                    'examen' => $_arrReactivos[0]['t12nombre'],
                    'fecha' => $_arrReactivos[0]['t04fecha_fin'],
                    'global' => array(
                        'title' => $_arrReactivos[0]['t12nombre'],
                        'xAxis' => json_encode($_arrxAxisGlobal),
                        'data' => json_encode(array($_data_chartGlobal)),
                    ),
                    'verbal' => array(
                        'title' => 'Procesos de pensamiento Verbal',
                        'xAxis' => json_encode($_arrxAxisVerbal),
                        'data' => json_encode(array($_data_chartVerbal)),
                    ),
                    'data'=> $_arrInterpretacion
                    
                );
                
                
                // generate data Matematico
                $_arrxAxisMatematico = array();
                $_data_chartMatematico = array(
                    'name' => $_arrReactivos[0]['t12nombre']." (".$_arrReactivos[0]['t04fecha_fin'].")",
                    'data' => null
                );
                $_arrDataTempMatematico = array();
                foreach($_arrInterpretacion[2]['procesos_pensamiento'] as $proceso){
                        array_push($_arrxAxisMatematico, $proceso['c10nombre']);
                        $_arrDataTempMatematico[] = $proceso['puntaje'] == 0 ? 1 : $proceso['puntaje'];
                }
                $_data_chartMatematico['data']= $_arrDataTempMatematico;

                
                $_arrReturn = array(
                    'examen' => $_arrReactivos[0]['t12nombre'],
                    'fecha' => $_arrReactivos[0]['t04fecha_fin'],
                    'global' => array(
                        'title' => $_arrReactivos[0]['t12nombre'],
                        'xAxis' => json_encode($_arrxAxisGlobal),
                        'data' => json_encode(array($_data_chartGlobal)),
                    ),
                    'verbal' => array(
                        'title' => 'Procesos de pensamiento Verbal',
                        'xAxis' => json_encode($_arrxAxisVerbal),
                        'data' => json_encode(array($_data_chartVerbal)),
                    ),
                    'matematico' => array(
                        'title' => 'Procesos de pensamiento Matemático',
                        'xAxis' => json_encode($_arrxAxisMatematico),
                        'data' => json_encode(array($_data_chartMatematico)),
                    ),
                    'data'=> $_arrInterpretacion
                    
                );
                
		
		
                return $_arrReturn;
	}
        
        
        /**
	 *Obtener los resultados del grupo para generar reportes intellectus
	 * @access public
	 * @author LAFC lfcelaya@grupoeducare.com
	 * @param  int $idGrupo id del grupo
	 * @param  int $idExamen  id del Examen a consultar
                 * @param  int $idUsuarioPerfilProfesor  el idUsuarioPerfil del profesor, para validar que el grupo que se esta intentando consulatr pertenece al profesor 
                 * @param   string $strUrlReporteAlumno url del reporte individual
	 * @return array $arrReturn array formateado con los resultados para graficar 
	 */
        
        public function getReporteGrupo($idGrupo, $idExamen,  $strUrlReporteAlumno){
            
            $strQuery = 'SELECT 
                            c11.c11id_razonamiento_intellectus,
                            c11nombre,
                            c10.c10id_proceso_pensamiento_intellectus,
                            c10nombre,
                            t01.t01nombre,
                            t01apellidos,
                            t02.t02id_usuario_perfil,
                            t05.t05descripcion,
                            t10.t10respuesta_usuario,
                            t10respuesta_correcta,
                            t12.t12id_examen,
                            t12nombre,
                            t04.t04fecha_fin,
                            t04id_examen_usuario,
                            t05.t05id_grupo
                        FROM
			t10respuesta_usuario_examen t10 
			INNER JOIN t03licencia_usuario t03 ON (t03.t03id_licencia_usuario = t10.t03id_licencia_usuario)                                 
                        INNER JOIN t02usuario_perfil t02 ON (t02.t02id_usuario_perfil = t03.t02id_usuario_perfil)
                        INNER JOIN t01usuario t01 ON (t01.t01id_usuario = t02.t01id_usuario)
                        INNER JOIN t08administrador_grupo t08 ON (t08.t02id_usuario_perfil = t02.t02id_usuario_perfil) 
                        INNER JOIN t05grupo t05 ON (t08.t05id_grupo = t05.t05id_grupo) 
			INNER JOIN t04examen_usuario t04 ON (t04.t03id_licencia_usuario = t03.t03id_licencia_usuario)                                   
			INNER JOIN t12examen t12 ON (t12.t12id_examen = t10.t12id_examen)                                                               
			INNER JOIN t11pregunta t11 ON (t11.t11id_pregunta =  t10.t11id_pregunta)                                                        
			INNER JOIN t31pregunta_proceso_pensamiento_intellectus t31 ON (t31.t11id_pregunta = t11.t11id_pregunta) 
			INNER JOIN c10proceso_pensamiento_intellectus c10 ON (c10.c10id_proceso_pensamiento_intellectus = t31.c10id_proceso_pensamiento_intellectus)  
			INNER JOIN t32pregunta_nivel_intellectus t32 ON (t32.t11id_pregunta = t11.t11id_pregunta) 
			INNER JOIN c05nivel c05 ON (c05.c05id_nivel = t32.c05id_nivel) 
			INNER JOIN t33pregunta_razonamiento_intellectus t33 ON (t33.t11id_pregunta = t11.t11id_pregunta) 
			INNER JOIN c11razonamiento_intellectus c11 ON (c11.c11id_razonamiento_intellectus = t33.c11id_razonamiento_intellectus) 
                        WHERE 
                        t12.t12id_examen = :idExamen
                        AND t08.t05id_grupo = :idGrupo
                        AND  t08usuario_ligado =  :idStatusLigado
                        AND t04.t04estatus = :estatusFinalizado
                        ORDER BY t10fecha_actualiza, t11.t11nombre_corto_clave ';
            
            $statement = $this->adapter->query($strQuery);
            $results = $statement->execute(array(
                    ":idExamen" => $idExamen,
                    ":idGrupo" => $idGrupo,
                    ":idStatusLigado" => $this->config['constantes']['ESTATUS_LIGADO'],
                    ':estatusFinalizado' => $this->config['constantes']['ESTATUS_EXAMEN_FINALIZADO']
            ));
            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $_arrReactivos = $resultSet->toArray();
            
            
            
            
            
            //preparar array para highcharts 
            //variables global 
            $_arrXAxisGlobal = array();
            $_strTituloGlobal = 'Resultado global para: '.$_arrReactivos[0]['t12nombre'];
            $_strSubtituloGlobal = '';
            $_data_chartGlobal = array(
                    'name' => null,
                    'data' => null
               );
            $_arrDataTemp = array();
            //variables verbal
            $_arrXAxisVerbal = array();
            $_strTituloVerbal = 'Resultado de razonamiento verbal para: '.$_arrReactivos[0]['t12nombre'];
            $_strSubtituloVerbal = $_strSubtituloGlobal;
            $_arrDataChartVerbal = array(
                    'name' => null,
                    'data' => null
               );
            $_arrDataChartVerbalPuntaje = array(
                    'name' => null,
                    'data' => null
               );
            $_arrDataTempVerbal = array();
            //variables matematico
            $_arrXAxisMate = array();
            $_strTituloMate = 'Resultado de razonamiento matemático para: '.$_arrReactivos[0]['t12nombre'];
            $_strSubtituloMate = $_strSubtituloGlobal;
            $_arrDataChartMate = array(
                    'name' => null,
                    'data' => null
               );
            $_arrDataChartMatePuntaje = array(
                    'name' => null,
                    'data' => null
               );
            
            $_arrDataTempMate = array();
            
            $_aciertosGlobalVerbal = 0;
            $_aciertosGlobalMatematico = 0;
            
            $_arrAlumnos = array();
            
            foreach($_arrReactivos as $reactivo){
                $_isAcierto = trim($reactivo['t10respuesta_usuario']) === trim($reactivo['t10respuesta_correcta']);
                if(!isset($_data_chartGlobal['name'])){
                    $_data_chartGlobal['name'] =   $reactivo['t05descripcion'].' - '.$reactivo['t12nombre'];
                }
                
                if(!isset($_arrDataChartVerbal['name'])){
                    $_arrDataChartVerbal['name'] = $reactivo['t05descripcion'].' - '.$reactivo['t12nombre'];
                    $_arrDataChartVerbalPuntaje['name'] = $reactivo['t05descripcion'].' - '.$reactivo['t12nombre'];
                }
                
                if(!isset($_arrDataChartMate['name'])){
                    $_arrDataChartMate['name'] = $reactivo['t05descripcion'].' - '.$reactivo['t12nombre'];
                    $_arrDataChartMatePuntaje['name'] = $reactivo['t05descripcion'].' - '.$reactivo['t12nombre'];
                }
                
                //create array xAxis verbal y matematico
                switch ($reactivo['c11id_razonamiento_intellectus']){
                    case 1://Razonamiento verbal
                        $_intIndiceGlobal = 0;
                        $_aciertosGlobalVerbal += $_isAcierto ? 1 : 0;
                        if(!isset($_arrDataTempVerbal[$reactivo['c10id_proceso_pensamiento_intellectus']])){
                            $_arrDataTempVerbal[$reactivo['c10id_proceso_pensamiento_intellectus']] = array(
                                'nombre' => $reactivo['c10nombre'], 
                                'puntaje' => 0,
                                'id' => $reactivo['c10id_proceso_pensamiento_intellectus']
                            );
                        }
                        $_arrDataTempVerbal[$reactivo['c10id_proceso_pensamiento_intellectus']]['puntaje'] += $_isAcierto ? 1 : 0;
                        
                    break;
                    case 2://Razonamiento matematico
                        $_intIndiceGlobal = 1;
                        $_aciertosGlobalMatematico += $_isAcierto ? 1 : 0;
                        if(!isset($_arrDataTempMate[$reactivo['c10id_proceso_pensamiento_intellectus']])){
                            $_arrDataTempMate[$reactivo['c10id_proceso_pensamiento_intellectus']] = array(
                                'nombre' => $reactivo['c10nombre'], 
                                'puntaje' => 0,
                                'id' => $reactivo['c10id_proceso_pensamiento_intellectus']
                            );
                        }
                        $_arrDataTempMate[$reactivo['c10id_proceso_pensamiento_intellectus']]['puntaje'] += $_isAcierto ? 1 : 0;
                    break;
                }
                
                if(!isset($_arrXAxisGlobal[$_intIndiceGlobal])){
                    $_arrXAxisGlobal[$_intIndiceGlobal] =  $reactivo['c11nombre'];
                }
                
                //array de alumnos 
                if(!isset($_arrAlumnos[$reactivo['t04id_examen_usuario']])){
                    $_arrAlumnos[$reactivo['t04id_examen_usuario']] = array(
                        'id_usuario_perfil' => $reactivo['t02id_usuario_perfil'],
                        'id_grupo' => $reactivo['t05id_grupo'],
                        'nombre' =>  trim($reactivo['t01nombre'].' '.$reactivo['t01apellidos']),
                        'id_examen' => $reactivo['t12id_examen'],
                        'nombre_examen' => $reactivo['t12nombre'],
                        'razonamiento' => array()
                    );
                }
                
                
                if(!isset($_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']])){
                    $_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']] = array(
                        'id_razonamiento' => $reactivo['c11id_razonamiento_intellectus'],
                        'nombre' => $reactivo['c11nombre'],
                        'nombre_examen' => $reactivo['t12nombre'],
                        'contador_preguntas' => 0,
                        'puntaje' => 0,
                        'procesos_pensamiento' =>array()
                    );
                }
                $_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']]['contador_preguntas'] += 1;
                $_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']]['puntaje'] +=  $_isAcierto ? 1 : 0 ;
                
                if(!isset($_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento'][$reactivo['c10id_proceso_pensamiento_intellectus']])){
                    $_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento'][$reactivo['c10id_proceso_pensamiento_intellectus']]=array(
                        'nombre' => $reactivo['c10nombre'],
                        'nombre_examen' => $reactivo['t12nombre'],
                        'contador_preguntas' => 0,
                        'puntaje' => 0,
                    );
                }
                $_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento'][$reactivo['c10id_proceso_pensamiento_intellectus']]['contador_preguntas'] += 1;
                $_arrAlumnos[$reactivo['t04id_examen_usuario']]['razonamiento'][$reactivo['c11id_razonamiento_intellectus']]['procesos_pensamiento'][$reactivo['c10id_proceso_pensamiento_intellectus']]['puntaje'] +=  $_isAcierto ? 1 : 0 ;
                
                //agregar total de reactivos 
                
                
            }
            
            
            //agregar total de puntos por proceso de pensamiento  
            $arrTotalesVerbal = array();
            $arrTotalesMate = array();
            if(count($_arrAlumnos) > 0){
                $_arr1User =  array_shift(array_slice($_arrAlumnos, 0, 1));
                foreach ($_arr1User['razonamiento'] as $raz){
                    foreach ($raz['procesos_pensamiento'] as $proceso ){
                        
                        if($raz['id_razonamiento'] == 1){
                            if(!isset($arrTotalesVerbal[$raz['id_razonamiento']]['id_razonamiento'])){
                                $arrTotalesVerbal[$raz['id_razonamiento']] = array(
                                            'id_razonamiento' => $raz['id_razonamiento'],
                                            'contador_preguntas' => $raz['contador_preguntas'],
                                            'procesos_pensamiento' => array()
                                        );
                            }
                            $arrTotalesVerbal[$raz['id_razonamiento']]['procesos_pensamiento'][] = array(
                                'nombre' => $proceso['nombre'],
                                'contador_preguntas' => $proceso['contador_preguntas']
                            );
                        }
                        
                        if($raz['id_razonamiento'] == 2){
                            if(!isset($arrTotalesMate[$raz['id_razonamiento']]['id_razonamiento'])){
                                $arrTotalesMate[$raz['id_razonamiento']] = array(
                                            'id_razonamiento' => $raz['id_razonamiento'],
                                            'contador_preguntas' => $raz['contador_preguntas'],
                                            'procesos_pensamiento' => array()
                                        );
                            }
                            $arrTotalesMate[$raz['id_razonamiento']]['procesos_pensamiento'][] = array(
                                'nombre' => $proceso['nombre'],
                                'contador_preguntas' => $proceso['contador_preguntas']
                            );
                        }
                        
                            
                    }
                }
                
            }
            
            
            
            $this->_objLogger->debug(print_r($arrTotalesVerbal, true));
            $this->_objLogger->debug(print_r($arrTotalesMate, true));
            
            //promedio verbal 
            $_intTotalAlumnos = count($_arrAlumnos);
            $_data_chartGlobal['data'][0] = $this->getNivelGlobal($this->getPromedio($_aciertosGlobalVerbal, $_intTotalAlumnos));
            $_data_chartGlobal['data'][1] = $this->getNivelGlobal($this->getPromedio($_aciertosGlobalMatematico, $_intTotalAlumnos));
            $_dataVerbal = array();
            $_dataVerbalPuntaje = array();
            $_dataVerbalTotalPuntos = array();
            
            
            
            
            
            foreach ($_arrDataTempVerbal as $verbal){
                array_push($_arrXAxisVerbal, $verbal['nombre']);
                array_push($_dataVerbal, $this->getPromedio($verbal['puntaje'], $_intTotalAlumnos));
                array_push($_dataVerbalPuntaje, $verbal['puntaje']);
                
            }
             
            $_arrDataChartVerbal['data'] = $_dataVerbal;
            $_arrDataChartVerbalPuntaje['data'] = $_dataVerbalPuntaje;
            
            $_dataMate = array();
            $_dataMatePuntaje = array();
            foreach ($_arrDataTempMate as $mate){
                array_push($_arrXAxisMate, $mate['nombre']);
                array_push($_dataMate, $this->getPromedio($mate['puntaje'], $_intTotalAlumnos));
                array_push($_dataMatePuntaje, $mate['puntaje']);
                
            }
            $_arrDataChartMate['data'] = $_dataMate;
            $_arrDataChartMatePuntaje['data'] = $_dataMatePuntaje;
            
                         
             
             //prepare  tabla alumnos for highcharts 
            $_arrDataTableAlumnos = array();
            $_arrDataTableAlumnos['yAxis']['categories'] = array();
            $_arrDataTableAlumnos['xAxis']['categories_verbal'] = array();
            $_arrDataTableAlumnos['xAxis']['categories_matematico'] = array();
            $_arrDataTableAlumnos['verbal_data'] = array();
            $_arrDataTableAlumnos['matematico_data'] = array();
            $_cordenadaY = 4;
            $_arrLinksAlumnos = array();
            $_arrTotal = array();
            $arrayMin = array();
            $arrayMax = array();
            $arrPromedios= array();
            foreach ($_arrAlumnos as $user){
                array_push($_arrDataTableAlumnos['yAxis']['categories'], $user['nombre']);
                $_strLinkAlumno = $strUrlReporteAlumno.'?id_user='.$user['id_usuario_perfil'].'&id_exam='.$user['id_examen'].'&id_group='.$user['id_grupo'];
                $_arrLinksAlumnos[$user['nombre']] = $_strLinkAlumno;
                foreach ($user['razonamiento'] as $razonamiento){
                    switch ($razonamiento['id_razonamiento']){
                        case 1://razonamiento verbal
                            $_strIndiceCat = 'categories_verbal';
                            $_strIndiceData = 'verbal_data';
                        break;
                        case 2://razonamiento matematico
                            $_strIndiceCat = 'categories_matematico';
                            $_strIndiceData = 'matematico_data';
                        break;
                    
                    }
                    $_cordenadaX = 0;
                    $puntos = 0;
                    foreach ($razonamiento['procesos_pensamiento'] as $proceso){
                        array_push($_arrDataTableAlumnos['xAxis'][$_strIndiceCat], $proceso['nombre']);
                        $_arrCordenadasValor = array($_cordenadaX, $_cordenadaY, $proceso['puntaje']);
                        if(!isset($_arrTotal[$_strIndiceData])){
                            $_arrTotal[$_strIndiceData] = array(
                                'name' => $_strIndiceData,
                                'indiceCatName' => $_strIndiceCat,
                                'data' => array()
                            );
                            $arrayMin[$_strIndiceData] = array(
                                'name' => $_strIndiceData,
                                'indiceCatName' => $_strIndiceCat,
                                'data' => array()
                            );
                            $arrayMax[$_strIndiceData] = array(
                                'name' => $_strIndiceData,
                                'indiceCatName' => $_strIndiceCat,
                                'data' => array()
                            );
                        }
                        if(!isset($_arrTotal[$_strIndiceData]['data'][$_cordenadaX])){
                            $_arrTotal[$_strIndiceData]['data'][$_cordenadaX] = 0;
                        }
                        $_arrTotal[$_strIndiceData]['data'][$_cordenadaX] += $proceso['puntaje'];
                        
                        //crear el arreglo de puntos min y max
                        if(!isset($arrayMin[$_strIndiceData]['data'][$_cordenadaX])){
                            $arrayMin[$_strIndiceData]['data'][$_cordenadaX] = $proceso['puntaje'];
                            $arrayMax[$_strIndiceData]['data'][$_cordenadaX] = $proceso['puntaje'];
                        }
                        if($proceso['puntaje'] < $arrayMin[$_strIndiceData]['data'][$_cordenadaX] ){
                            $arrayMin[$_strIndiceData]['data'][$_cordenadaX]  = $proceso['puntaje'];
                        }
                        if($proceso['puntaje'] > $arrayMax[$_strIndiceData]['data'][$_cordenadaX] ){
                            $arrayMax[$_strIndiceData]['data'][$_cordenadaX]  = $proceso['puntaje'];
                        }
                        
                        
                        
                        $puntos += $proceso['puntaje'];
                        array_push($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
                        
                        $_cordenadaX += 1;
                    }
                    $_arrCordenadasValor = array($_cordenadaX, $_cordenadaY, $puntos);
                    array_push($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
                    array_push($_arrDataTableAlumnos['xAxis'][$_strIndiceCat], 'Total');
                    
                    
                    
                    
                }
                $_cordenadaY += 1;
            }
            
            
            
            
            
            foreach ($_arrTotal as $indice){
                $total_de_totales = 0;
                $total_promedios = 0;
                $_strIndiceData = $indice['name'];
                $_strIndiceCategories = $indice['indiceCatName'];
                $_cordenadaX = 0;
                foreach ($indice['data'] as $total){
                    $_arrCordenadasValor = array($_cordenadaX, 1, $total);
                    $total_de_totales += $total;
                    array_unshift($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
                    $promedio = 0 ;
                    if($total > 0){
                        $promedio = number_format($total / count($_arrDataTableAlumnos['yAxis']['categories']),3);
                        $total_promedios += $promedio;
                    }
                    $_arrCordenadasValor = array($_cordenadaX, 0, $promedio);
                    array_unshift($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
                    
                    $this->_objLogger->debug('min '.$_cordenadaX.' ---> '.print_r($arrayMin[$_strIndiceData]['data'][$_cordenadaX],true));
                    $min  = null;
                    if(isset($arrayMin[$_strIndiceData]['data'][$_cordenadaX])){
                        $min  = $arrayMin[$_strIndiceData]['data'][$_cordenadaX];
                    }
                    $_arrCordenadasValor = array($_cordenadaX, 3, $min);
                    array_unshift($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
                    
                    $max  = null;
                    if(isset($arrayMax[$_strIndiceData]['data'][$_cordenadaX])){
                        $max  = $arrayMax[$_strIndiceData]['data'][$_cordenadaX];
                    }
                    $_arrCordenadasValor = array($_cordenadaX, 2, $max);
                    array_unshift($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
                    
                    
                    $_cordenadaX += 1;
                }
                $promedio_de_promdeios  = 0;
                if($total_promedios > 0){
                    $promedio_de_promdeios = number_format($total_promedios / ($_cordenadaX),3);
                }
                
                //$this->_objLogger->debug(print_r('promedios = '.$total_promedios .' / '.count($_arrDataTableAlumnos['xAxis'][$_strIndiceCategories]) - 1 ),true);
                //$this->_objLogger->debug('total de promedios '.print_r($total_promedios,true));  
                
                //$this->_objLogger->debug('count--> '.print_r($_cordenadaX ,true));
                
                
                
                
                $_arrCordenadasValor = array($_cordenadaX, 1, $total_de_totales);
                array_unshift($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
                $_arrCordenadasValor = array($_cordenadaX, 0, $promedio_de_promdeios);
                array_unshift($_arrDataTableAlumnos[$_strIndiceData], $_arrCordenadasValor);
            }
            
             
            foreach ($_arrTotal as $indice){
                
            }
                  
            array_unshift($_arrDataTableAlumnos['yAxis']['categories'], 'MIN');
            array_unshift($_arrDataTableAlumnos['yAxis']['categories'], 'MAX');
            array_unshift($_arrDataTableAlumnos['yAxis']['categories'], 'Total');
            array_unshift($_arrDataTableAlumnos['yAxis']['categories'], 'Promedio');
            
            
            $this->_objLogger->debug('array min ---> '.print_r($arrayMin,true));
            $this->_objLogger->debug('array max ---> '.print_r($arrayMax,true));
           
            
             $arrReturn = array(
                'global' => array(
                    'xAxis' => $_arrXAxisGlobal,
                    'title' => $_strTituloGlobal,
                    'subtitle' => $_strSubtituloGlobal,
                    'data' => array($_data_chartGlobal)
                    
                ),
                'verbal' => array(
                    'xAxis' => $_arrXAxisVerbal,
                    'title' => $_strTituloVerbal,
                    'subtitle' => $_strSubtituloVerbal,
                    'data' => array($_arrDataChartVerbal),
                    'puntajes' => array($_arrDataChartVerbalPuntaje),
                    'arrTotalesVerbal' => $arrTotalesVerbal[1]
                    
                ),
                'matematico' => array(
                    'xAxis' => $_arrXAxisMate,
                    'title' => $_strTituloMate,
                    'subtitle' => $_strSubtituloMate,
                    'data' => array($_arrDataChartMate),
                    'puntajes' => array($_arrDataChartMatePuntaje),
                    'arrTotalesMate' => $arrTotalesMate[2]
                    
                ),
                'alumnos_table' => $_arrDataTableAlumnos,
                '_arrLinksAlumnos' => $_arrLinksAlumnos
            );
             //$this->_objLogger->debug('xAxis Verbal ---> '.print_r($arrReturn['verbal'],true));
             
             
             
            return $arrReturn;
        }
        
        private function getPromedio($_intPuntaje, $_intTotalAlumnos){
            $_promedio = intval($_intPuntaje) / intval($_intTotalAlumnos);
            $_promedio = round($_promedio);
            if($_promedio < 1){
                $_promedio = 1;
            }
            
            
            //$this->_objLogger->debug('puntos --> '.$_intPuntaje.' cantAlumnos ----> '.$_intTotalAlumnos.' result---> '. $_promedio);
            return $_promedio;
        }
    
        private function getNivelGlobal($aciertos){
                
                $aciertos = floatval($aciertos);
                if($aciertos <= 14){
                        $value = 1;
                }
                elseif($aciertos >= 15 && $aciertos <= 24 ){
                        $value = 2;
                }
                elseif(($aciertos >= 25 && $aciertos <= 30 ) || $aciertos > 30){
                        $value = 3;
                }
                else{
                        $value = NULL;
                }
                return $value;
        }
        
        /**
	 *Obtener la lista de  examnes que haya resulto el grupo , en caso de que el segundo parametro $count este habilitado(true)  se devuelve la  cantidad de examenes resueltos 
	 * @access public
	 * @author LAFC lfcelaya@grupoeducare.com
	 * @param  int $_intIdGrupo id del grupo
	 * @return mixed $mixReturn  array formateado con la lista de examenes o int con el numero(cantidad) de examnes resueltos 
	 */
        public function getExamenesResueltos($_intIdGrupo){
            
            
            $_strQuery = 'SELECT DISTINCT
                                t04.t12id_examen,
                                t12nombre_corto_clave,
                                t12.t12nombre
                            FROM 
                                t05grupo t05
                                INNER JOIN t08administrador_grupo AS t08 on t05.t05id_grupo=t08.t05id_grupo
                                INNER JOIN t02usuario_perfil AS t02 on  t02.t02id_usuario_perfil=t08.t02id_usuario_perfil
                                INNER JOIN t03licencia_usuario AS t03  on t02.t02id_usuario_perfil=t03.t02id_usuario_perfil
                                INNER JOIN t04examen_usuario AS t04 on t03.t03id_licencia_usuario=t04.t03id_licencia_usuario
                                INNER JOIN t12examen AS t12 on t04.t12id_examen=t12.t12id_examen
                                INNER JOIN t10respuesta_usuario_examen AS t10 on (t10.t03id_licencia_usuario = t03.t03id_licencia_usuario)
                                INNER JOIN t26examen_categoria t26 ON (t26.t12id_examen = t12.t12id_examen)
                            WHERE 
                                t05.t05id_grupo = :intIdGrupo 
                                AND (t26.c08id_categoria = :idCategoriaIntellectus OR t26.c08id_categoria = :idCategoriaIntellectusPost )
                                AND t08.t08usuario_ligado = :consEstatusLigado ';
            
            $statement = $this->adapter->query($_strQuery);
            $results = $statement->execute(array(
                    ':intIdGrupo' => $_intIdGrupo,
                    ':idCategoriaIntellectus' => $this->config['constantes']['ID_CATEGORIA_INTELLECTUS'],
                    ':idCategoriaIntellectusPost' => $this->config['constantes']['ID_CATEGORIA_INTELLECTUS_POST'],
                    ':consEstatusLigado' => $this->config['constantes']['ESTATUS_LIGADO']
            ));
            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $_arrExamenes = $resultSet->toArray();
            return $_arrExamenes;
            
        }
	 
        
    //Function obtener el promedio universal de un examen  
	
	public function getDataUniverso($_idExamen){
		
		$_strQuery = "
                        SELECT
                            
                            SUM(
                                    IF(t10.t10respuesta_usuario = t10.t10respuesta_correcta,1,0)
                            ) AS aciertos,

                            (
                                SELECT COUNT(*)
                                    FROM
                                    t03licencia_usuario t03sub2  
                                    INNER JOIN t04examen_usuario t04sub2 ON (t04sub2.t03id_licencia_usuario = t03sub2.t03id_licencia_usuario)
                                    INNER JOIN t02usuario_perfil t02sub2 ON (t02sub2.t02id_usuario_perfil = t03sub2.t02id_usuario_perfil)
                                    WHERE
                                     t04sub2.t12id_examen = :_idExamen
                                    AND t04sub2.t04estatus = :estatusFinalizado
                                    ORDER BY t03sub2.t03fecha_activacion DESC
                            ) AS total_alumnos,
                            c11.c11id_razonamiento_intellectus, 
                            c11nombre, 
                            c10.c10id_proceso_pensamiento_intellectus, 
                            c10nombre, 
                            t01.t01nombre, 
                            t01apellidos, 
                            t02.t02id_usuario_perfil, 
                            t05.t05descripcion, 
                            t10.t10respuesta_usuario, 
                            t10respuesta_correcta, 
                            t12.t12id_examen, 
                            t12nombre, 
                            t04.t04fecha_fin 
                        FROM    
                            t10respuesta_usuario_examen t10 
                            INNER JOIN t03licencia_usuario t03 ON (t03.t03id_licencia_usuario = t10.t03id_licencia_usuario)                                              
                            INNER JOIN t02usuario_perfil t02 ON (t02.t02id_usuario_perfil = t03.t02id_usuario_perfil) 
                            INNER JOIN t01usuario t01 ON (t01.t01id_usuario = t02.t01id_usuario) 
                            INNER JOIN t08administrador_grupo t08 ON (t08.t02id_usuario_perfil = t02.t02id_usuario_perfil)  
                            INNER JOIN t05grupo t05 ON (t08.t05id_grupo = t05.t05id_grupo) 
                            INNER JOIN t04examen_usuario t04 ON (t04.t03id_licencia_usuario = t03.t03id_licencia_usuario) 
                            INNER JOIN t12examen t12 ON (t12.t12id_examen = t10.t12id_examen) 
                            INNER JOIN t11pregunta t11 ON (t11.t11id_pregunta =  t10.t11id_pregunta) 
                            INNER JOIN t31pregunta_proceso_pensamiento_intellectus t31 ON (t31.t11id_pregunta = t11.t11id_pregunta) 
                            INNER JOIN c10proceso_pensamiento_intellectus c10 ON (c10.c10id_proceso_pensamiento_intellectus = t31.c10id_proceso_pensamiento_intellectus) 
                            INNER JOIN t32pregunta_nivel_intellectus t32 ON (t32.t11id_pregunta = t11.t11id_pregunta) 
                            INNER JOIN c05nivel c05 ON (c05.c05id_nivel = t32.c05id_nivel) 
                            INNER JOIN t33pregunta_razonamiento_intellectus t33 ON (t33.t11id_pregunta = t11.t11id_pregunta) 
                            INNER JOIN c11razonamiento_intellectus c11 ON (c11.c11id_razonamiento_intellectus = t33.c11id_razonamiento_intellectus) 
                        WHERE 
                            t12.t12id_examen = :_idExamen
                            AND t04.t04estatus = :estatusFinalizado
                        GROUP BY c11.c11id_razonamiento_intellectus, c10.c10id_proceso_pensamiento_intellectus  ";
		
		//$this->_objLogger->debug($_strQuery);
		
		
		$statementCalificaciones = $this->adapter->query($_strQuery);
		
		$resultCalificaciones = $statementCalificaciones->execute(array(
                            ":_idExamen" =>  $_idExamen,
                            ':estatusFinalizado' => $this->config['constantes']['ESTATUS_EXAMEN_FINALIZADO']
		));
		
		if($resultCalificaciones->count() < 1){
			return false;
		}
		$resultSet = new ResultSet;
		$resultSet->initialize($resultCalificaciones);
		$_arrResultados= $resultSet->toArray();
		
		//obtener promedios
                $_arrPuntosGlobal = array(
                    'verbal'=> 0,
                    'matematico'=> 0
                );
                
                $_arrDataVarbal = array();
                $_arrDataMate = array();
                $_intTotalAlumnos = null;
                foreach ($_arrResultados as $row){
                    
                    if(!isset($_intTotalAlumnos)){
                        $_intTotalAlumnos = $row['total_alumnos'];
                    }
                    if($row['c11id_razonamiento_intellectus'] == 1 ){//razonamiento verbal
                        $_strIndice = 'verbal';
                        $_promedio = $this->getPromedio($row['aciertos'], $_intTotalAlumnos);
                        array_push($_arrDataVarbal,$_promedio);
                        
                    }
                    else{
                        $_strIndice = 'matematico';
                        $_promedio = $this->getPromedio($row['aciertos'], $_intTotalAlumnos);
                        array_push($_arrDataMate,$_promedio);
                    }
                    //sumar los puntos obtenidos 
                    $_arrPuntosGlobal[$_strIndice] += $row['aciertos'] ;
                }
		
                //calcular promedio global
                $_promedioGlobalVerbal = $this->getNivelGlobal($this->getPromedio($_arrPuntosGlobal['verbal'], $_intTotalAlumnos));
                $_promedioGlobalMatematico = $this->getNivelGlobal($this->getPromedio($_arrPuntosGlobal['matematico'], $_intTotalAlumnos));
                
                $_arrInterpretacion = array(
                    'global' => array(
                                    'name' =>  'Promedio Universal',
                                    'data' => array($_promedioGlobalVerbal,$_promedioGlobalMatematico)
                                ),
                    'verbal' => array(
                                    'name' =>  'Promedio Universal',
                                    'data' => $_arrDataVarbal
                                ),
                    'matematico' => array(
                                    'name' =>  'Promedio Universal',
                                    'data' => $_arrDataMate
                                ),
                );
                $this->_objLogger->debug(print_r($_arrInterpretacion,true));
		return $_arrInterpretacion;
	}
        
        
        
        public function getDataVersus($idGrupo, $idExamen, $idUsuarioPerfilProfesor){
            
            
            
            $strQuery = 'SELECT 
                             SUM(
                                    IF(t10.t10respuesta_usuario = t10.t10respuesta_correcta,1,0)
                            ) AS aciertos,

                            (
                                SELECT COUNT(*)
                                    FROM
                                    t03licencia_usuario t03sub2  
                                    INNER JOIN t04examen_usuario t04sub2 ON (t04sub2.t03id_licencia_usuario = t03sub2.t03id_licencia_usuario)
                                    INNER JOIN t02usuario_perfil t02sub2 ON (t02sub2.t02id_usuario_perfil = t03sub2.t02id_usuario_perfil)
                                    INNER JOIN t08administrador_grupo t08sub2 ON (t08sub2.t02id_usuario_perfil = t02sub2.t02id_usuario_perfil)  
									INNER JOIN t05grupo t05sub2 ON (t08sub2.t05id_grupo = t05sub2.t05id_grupo)	
                                    WHERE 
                                        t04sub2.t12id_examen = :idExamen
                                        AND t08sub2.t05id_grupo = :idGrupo
                                        AND t08sub2.t08usuario_ligado = :idStatusLigado 
                                        AND t04sub2.t04estatus = :estatusFinalizado
                                    ORDER BY t03sub2.t03fecha_activacion DESC 
                            ) AS total_alumnos,
                            c11.c11id_razonamiento_intellectus, 
                            c11nombre, 
                            c10.c10id_proceso_pensamiento_intellectus, 
                            c10nombre, 
                            t01.t01nombre, 
                            t01apellidos, 
                            t02.t02id_usuario_perfil, 
                            t05.t05descripcion, 
                            t10.t10respuesta_usuario, 
                            t10respuesta_correcta, 
                            t12.t12id_examen, 
                            t12nombre, 
                            t04.t04fecha_fin 
                        FROM
			t10respuesta_usuario_examen t10 
			INNER JOIN t03licencia_usuario t03 ON (t03.t03id_licencia_usuario = t10.t03id_licencia_usuario)                                 
                        INNER JOIN t02usuario_perfil t02 ON (t02.t02id_usuario_perfil = t03.t02id_usuario_perfil)
                        INNER JOIN t01usuario t01 ON (t01.t01id_usuario = t02.t01id_usuario)
                        INNER JOIN t08administrador_grupo t08 ON (t08.t02id_usuario_perfil = t02.t02id_usuario_perfil) 
                        INNER JOIN t05grupo t05 ON (t08.t05id_grupo = t05.t05id_grupo) 
			INNER JOIN t04examen_usuario t04 ON (t04.t03id_licencia_usuario = t03.t03id_licencia_usuario)                                   
			INNER JOIN t12examen t12 ON (t12.t12id_examen = t10.t12id_examen)                                                               
			INNER JOIN t11pregunta t11 ON (t11.t11id_pregunta =  t10.t11id_pregunta)                                                        
			INNER JOIN t31pregunta_proceso_pensamiento_intellectus t31 ON (t31.t11id_pregunta = t11.t11id_pregunta) 
			INNER JOIN c10proceso_pensamiento_intellectus c10 ON (c10.c10id_proceso_pensamiento_intellectus = t31.c10id_proceso_pensamiento_intellectus)  
			INNER JOIN t32pregunta_nivel_intellectus t32 ON (t32.t11id_pregunta = t11.t11id_pregunta) 
			INNER JOIN c05nivel c05 ON (c05.c05id_nivel = t32.c05id_nivel) 
			INNER JOIN t33pregunta_razonamiento_intellectus t33 ON (t33.t11id_pregunta = t11.t11id_pregunta) 
			INNER JOIN c11razonamiento_intellectus c11 ON (c11.c11id_razonamiento_intellectus = t33.c11id_razonamiento_intellectus) 
                        WHERE 
                        t12.t12id_examen = :idExamen
                        AND t08.t05id_grupo = :idGrupo
                        AND  t08usuario_ligado =  :idStatusLigado
                        AND t04.t04estatus = :estatusFinalizado
                        GROUP BY c11.c11id_razonamiento_intellectus, c10.c10id_proceso_pensamiento_intellectus
                         ';
            
            $statement = $this->adapter->query($strQuery);
            $results = $statement->execute(array(
                    ":idExamen" => $idExamen,
                    ":idGrupo" => $idGrupo,
                    ":idStatusLigado" => $this->config['constantes']['ESTATUS_LIGADO'],
                    ':estatusFinalizado' => $this->config['constantes']['ESTATUS_EXAMEN_FINALIZADO']
            ));
            $resultSet = new ResultSet;
            $resultSet->initialize($results);
            $_arrResultados = $resultSet->toArray();
            
            
            

            //obtener promedios
            $_arrPuntosGlobal = array(
                'verbal'=> 0,
                'matematico'=> 0
            );

            $_arrDataVarbal = array();
            $_arrDataMate = array();
            $_intTotalAlumnos = null;
            foreach ($_arrResultados as $row){

                if(!isset($_intTotalAlumnos)){
                    $_intTotalAlumnos = $row['total_alumnos'];
                }
                if($row['c11id_razonamiento_intellectus'] == 1 ){//razonamiento verbal
                    $_strIndice = 'verbal';
                    $_promedio = $this->getPromedio($row['aciertos'], $_intTotalAlumnos);
                    array_push($_arrDataVarbal,$_promedio);

                }
                else{
                    $_strIndice = 'matematico';
                    $_promedio = $this->getPromedio($row['aciertos'], $_intTotalAlumnos);
                    array_push($_arrDataMate,$_promedio);
                }
                //sumar los puntos obtenidos 
                $_arrPuntosGlobal[$_strIndice] += $row['aciertos'] ;
            }

            //calcular promedio global
            $_promedioGlobalVerbal = $this->getNivelGlobal($this->getPromedio($_arrPuntosGlobal['verbal'], $_intTotalAlumnos));
            $_promedioGlobalMatematico = $this->getNivelGlobal($this->getPromedio($_arrPuntosGlobal['matematico'], $_intTotalAlumnos));

            $_arrInterpretacion = array(
                'global' => array(
                                'name' =>  $_arrResultados[0]['t05descripcion'].' - '.$_arrResultados[0]['t12nombre'],
                                'data' => array($_promedioGlobalVerbal,$_promedioGlobalMatematico)
                            ),
                'verbal' => array(
                                'name' =>  $_arrResultados[0]['t05descripcion'].' - '.$_arrResultados[0]['t12nombre'],
                                'data' => $_arrDataVarbal
                            ),
                'matematico' => array(
                                'name' =>  $_arrResultados[0]['t05descripcion'].' - '.$_arrResultados[0]['t12nombre'],
                                'data' => $_arrDataMate
                            ),
            );
            $this->_objLogger->debug(print_r($_arrInterpretacion,true));
            return $_arrInterpretacion;
        }
}
