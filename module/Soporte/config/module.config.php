<?php
/**
 * @author oreyes
 * @author jpgomez
 * @since 02/12/2014
 * @version  Examenes1.0
 *//*
return array(
    'controllers' => array(
        'invokables' => array(
           'Soporte\Controller\WebserviceSoportePortal'	=> 'Soporte\Controller\WebserviceSoportePortalController',
            //'webservice-soporte-portal'	=> 'Soporte\Controller\WebserviceSoportePortalController',
            ),
        ),
    'router' => array(
        'routes' => array(
            'soporte' => array(
                'type' =>  'Segment',
                'options' => array(
                    'route'    => '/soporte[/[:action]]',
                    'constraints' => array(
                        'action'  =>  '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                        'controller' => 'Soporte\Controller\WebserviceSoportePortal',
                        'action'     => 'index',
                        ),
                ),
                ),
            ),
        ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack' => array(
            'examenes' => __DIR__ . '/../view',
            ),
        ),
    );

*/
return array(
    'controllers' => array(
        'invokables' => array(
            'Soporte\Controller\WebserviceSoportePortal' => 'Soporte\Controller\WebserviceSoportePortalController',
            'Soporte\Controller\WebserviceTodo' => 'Soporte\Controller\WebserviceTodoController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'soporte' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/soporte[/[:action]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Soporte\Controller\WebserviceSoportePortal',
                        'action' => 'index',
                    ),
                ),
            ),
            'todo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/todo[/[:action]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Soporte\Controller\WebserviceTodo',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'email' => __DIR__ . '/../view',
        ),
    ),
);
