<?php
/**
 * Modelo de web service implementacion de la logica en las operaciones del WS Soporte PortalGE.
 * @author oreyes
 * @copyright Grupo Educare derechos reservados.
 */

namespace Soporte\Model;



use Zend\Validator\Explode;
use Zend\Db\Adapter\Driver\Sqlsrv\Exception\ErrorException;
use ZendTest\XmlRpc\Server\Exception;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;


/**
 * Clase webservice.
 * @author mateo
 *
 */
class WebserviceTodoModel {
	
	/**
	 * variable de la clase sql de zend-
	 * @var SQL $sql
	 */
	protected $sql;
	
	/**
	 * variable de la clase Adapter de zend.
	 * @var Adapter $adapter
	 */
	protected $adapter;
	
	/**
	 * Logger para registrar en un log errores o comentarios.
	 * @var array $config.
	 */
	private $config;
 	
	/**
	 * Arreglo de iPs de acceso al WS.
	 * @var array $_arrIPConsultoras
	 */
	private $_arrIPConsultoras	= array(
				    				'127.0.0.1',
				    				'205.186.133.250',
									'192.168.2.66'
				    		);
	/**
	 * Constructor de la clase inicializa variables.
	 * @param Adapter $adapter
	 * @param array $config
	 */
 	public function __construct(Adapter $adapter, $config){
 		
 		$this->adapter = $adapter;
 		$this->sql= new Sql($adapter);
 		$this->config = $config;
 		
 	}
 	
 	/**
 	 * Recibe array de datos y lo registra en el sitio con el perfíl de profesor
         * 
 	 * @param array  $arrDatosProfesor datos del registro exportado de la tienda etodo
 	 * @return array $_arrRespuesta
 	 */
 	
 	public function registraProfesor($arrDatosProfesor){
                
 		
 		//if($this->permitirAccesoIP()){
                    
                        $_strQueryBuscarPorUsuario= 'select * from t01usuario  t01
                                                    join t02usuario_perfil t02 on t01.t01id_usuario = t02.t01id_usuario
                                                    where c01id_perfil = 2
                                                    and t01correo = :correo';
	 		$statement=$this->adapter->query($_strQueryBuscarPorUsuario);
                        
//	 		$results=$statement->execute(array(
//                            ':correo' => 'juanpi@myself.com'
//                        ));
//	 		$resultSet = new ResultSet;
//	 		$resultSet->initialize($results);
//	 		$_arrDatosUsuario = $resultSet->toArray();
                        
                        $_arrDatosUsuario = array('id'=>1,'nombre'=>'Juan Perez', 'bool'=> true);
                        error_log(print_r($_arrDatosUsuario,true));
                        return $_arrDatosUsuario;
	 	//}
 		//die();
 	}
        
        /**
	 * Obtiene la IP origen de la peticion y la compara con el arreglo de IP's permitidas.
	 *
	 * @return boolean
	 * @access privado para no publicar como operacion en el WS.
	 */
	private function permitirAccesoIP(){
		
		$_bolRegresa = false;
		
		if($_SERVER['HTTP_X_FORWARDED_FOR'] != '' ){
			$_strIPCliente = ( !empty($_SERVER['REMOTE_ADDR']) ) ?	$_SERVER['REMOTE_ADDR']	: ( (!empty($_ENV['REMOTE_ADDR'])) ? $_ENV['REMOTE_ADDR']:"unknown" );
		
			// los proxys van añadiendo al final de esta cabecera
			// las direcciones ip que van "ocultando". Para localizar la ip real
			// del usuario se comienza a mirar por el principio hasta encontrar
			// una dirección ip que no sea del rango privado. En caso de no
			// encontrarse ninguna se toma como valor el REMOTE_ADDR
		
			$entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);
		
			reset($entries);
			while (list(, $entry) = each($entries)){
				$entry = trim($entry);
				if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) ){
					$_arrIPsPrivadas = array(
											'/^0\./',
							'/^127\.0\.0\.1/',
							'/^192\.168\..*/',
							'/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
							'/^10\..*/');
		
					$found_ip = preg_replace($_arrIPsPrivadas, $_strIPCliente, $ip_list[1]);
		
					if ($_strIPCliente != $found_ip){
						$_strIPCliente = $found_ip;
						break;
					}
				}
			}
		}
		else{
			$_strIPCliente = ( !empty($_SERVER['REMOTE_ADDR']) ) ?	$_SERVER['REMOTE_ADDR']	: ( (!empty($_ENV['REMOTE_ADDR']))?$_ENV['REMOTE_ADDR']:"unknown" );
		}
		
		foreach($this->_arrIPConsultoras as &$ip){
			if($ip == $_strIPCliente){
				$_bolRegresa = true;
				break;
			}
		}
		
		return $_bolRegresa;
	}
 	
}
?>