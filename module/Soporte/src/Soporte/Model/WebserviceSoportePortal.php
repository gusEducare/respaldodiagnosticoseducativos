<?php
/**
 * Modelo de web service implementacion de la logica en las operaciones del WS Soporte PortalGE.
 * @author oreyes
 * @copyright Grupo Educare derechos reservados.
 */

namespace Soporte\Model;



use Zend\Validator\Explode;
use Zend\Db\Adapter\Driver\Sqlsrv\Exception\ErrorException;
use ZendTest\XmlRpc\Server\Exception;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;


/**
 * Clase webservice.
 * @author mateo
 *
 */
class WebserviceSoportePortal {
	
	/**
	 * variable de la clase sql de zend-
	 * @var SQL $sql
	 */
	protected $sql;
	
	/**
	 * variable de la clase Adapter de zend.
	 * @var Adapter $adapter
	 */
	protected $adapter;
	
	/**
	 * Logger para registrar en un log errores o comentarios.
	 * @var array $config.
	 */
	private $config;
 	
	/**
	 * Arreglo de iPs de acceso al WS.
	 * @var array $_arrIPConsultoras
	 */
	private $_arrIPConsultoras	= array(
				    				'127.0.0.1',
				    				'205.186.133.250',
									'192.168.2.66'
				    		);
	/**
	 * Constructor de la clase inicializa variables.
	 * @param Adapter $adapter
	 * @param array $config
	 */
 	public function __construct(Adapter $adapter, $config){
 		
 		$this->adapter = $adapter;
 		$this->sql= new Sql($adapter);
 		$this->config = $config;
 		
 	}
 	
 	/**
 	 * Obtener los datos datos del usuario a traves del id
 	 * @param int  $_intIdUsuario id  ge_t02id_usuario_servicio correspondiente
 	 * @param bool  $_boolCertificaciones activar la busqueda para las licencias del servicio certificaciones
 	 * @return array $_arrDatosUsuario array con los resultados de la consulta, datos del usuario 
 	 */
 	public function getUserById($_intIdUsuario,$_boolCertificaciones = false){
 		
 		if($this->permitirAccesoIP()){
 			
 			$_strWhere =  ' WHERE ge_t02id_usuario_servicio = :_intIdUsuario ';
 			$_strJoin = '';
 			//si la bandera de certificaciones viene encendida se agrega un join y se modifica el where, ya que el id que se envia en el caso de certificaciones es el ge_t03id_usuario_servicio_perfil_colegio
 			if($_boolCertificaciones  === true){
 				$_strJoin = ' INNER JOIN ge_t03usuario_servicio_perfil_colegio t03 ON (t03.ge_t02id_usuario_servicio = t02.ge_t02id_usuario_servicio) ';
 				$_strWhere =  ' WHERE ge_t03id_usuario_servicio_perfil_colegio = :_intIdUsuario ';
 				
 			}
 			
	 			$_strQueryBuscarPorIdUsuario= 'SELECT 
												t01.ge_t01id_usuario as id,
												ge_t01nombre as nombre_usuario,
												ge_t01apellidos as apellidos ,
												ge_t01correo as usuario,
	 											ge_c17nombre as servicio
	 										FROM ge_t01usuario t01
	 											INNER JOIN ge_t02usuario_servicio t02 ON (t01.ge_t01id_usuario = t02.ge_t01id_usuario)
	 											'.$_strJoin.'
	 											INNER JOIN ge_c17servicio c17 ON (c17.ge_c17id_servicio = t02.ge_c17id_servicio) '. $_strWhere;
 			
	 		if(!isset($_intIdUsuario)){
	 			return false;
	 		}
	 		$statement=$this->adapter->query($_strQueryBuscarPorIdUsuario);
	 		$results=$statement->execute(array(
	 				":_intIdUsuario"=> $_intIdUsuario
	 		));
	 		$resultSet = new ResultSet;
	 		$resultSet->initialize($results);
	 		$_arrDatosUsuario = $resultSet->toArray();
	 		
	 		return $_arrDatosUsuario;
	 	}
 		die();
 	}
 	
 	/**
 	 * Obtener los datos datos del usuario a traves del UserName
 	 * @param string  $_strUserName nombre de usuario 
 	 * @return array $_arrDatosUsuario array con los resultados de la consulta, datos del usuario
 	 */
 	
 	public function getUserByUserName($_strUserName){
 		
 		if($this->permitirAccesoIP()){
	 		$_strQueryBuscarPorUsuario= 'SELECT 
                                                        t01.t01id_usuario  as id,
                                                        t01.t01nombre  as nombre_usuario,
                                                        t01.t01apellidos  as apellidos ,
                                                        t01.t01correo as usuario
                                                    FROM t01usuario t01
                                                    WHERE t01.t01correo like  "%'.$_strUserName.'%"';
	 		$statement=$this->adapter->query($_strQueryBuscarPorUsuario);
	 		$results=$statement->execute();
	 		$resultSet = new ResultSet;
	 		$resultSet->initialize($results);
	 		$_arrDatosUsuario = $resultSet->toArray();		 		
	 		return $_arrDatosUsuario;
	 	}
 		die();
 	}
 	
 	/**
 	 * Obtener el total de registros con coincidencia en un nombre de usuaruio.
 	 * @param string  $_strName nombre de pila del usuario
 	 * @return int $_intResponse cantidad de usuarios que coinciden 
 	 */
 	public function getCountUserByName($_strName){
 		
 		if($this->permitirAccesoIP()){
	 		$_strQuery = 'SELECT COUNT(*) AS total 
                                                        FROM t01usuario t01
                                                        WHERE CONCAT(t01.t01nombre," ",t01.t01apellidos ) LIKE :_strName ';
	 		$statement = $this->adapter->query($_strQuery);
	 		$results = $statement->execute(array(
	 				":_strName"=> '%'.$_strName.'%'
	 		));
	 		$resultSet = new ResultSet;
	 		$resultSet->initialize($results);
	 		$_arrDatosNombreUsuario = $resultSet->toArray();
	 		$_intResponse = $_arrDatosNombreUsuario[0]['total']; 
	 		return $_intResponse;
	 	}
 		die();
 	}
 	/**
 	 * Obtener los datos datos del usuario a traves del Nombre real
 	 * @param string  $_strName nombre de pila del usuario
 	 * @param string  $sidx campo pivote
 	 * @param string  $sord orden
 	 * @param int  $start inicio de la consulta
 	 * @param int  $limit limite de la consulta
 	 * @return array $_arrDatosUsuario array con los resultados de la consulat, datos del usuario
 	 */
 	public function getUserByName($_strName, $sidx = 'nombre_usuario', $sord = null, $start = null, $limit = null){
 		
 		$sidx = isset($sidx) ? $sidx : 'nombre_usuario';
 		$sord = isset($sord) ? $sord : 'ASC';
 		$start = isset($start) ? $start : 0;
 		$start = $start > 0 ? $start : 0;
 		$limit = isset($limit) ? $limit : 50;
	 	
	 	if($this->permitirAccesoIP()){
	 		$_strQuery = 'SELECT 
                                            t01.t01id_usuario as id,
                                            t01.t01nombre as nombre_usuario,
                                            t01.t01apellidos  as apellidos ,
                                            t01.t01correo  as usuario
                                    FROM t01usuario t01
                                    WHERE 
                                            CONCAT(t01.t01nombre," ",t01.t01apellidos ) LIKE  :_strName
                                    ORDER BY
                                            :_sidx :_sord
                                    LIMIT
                                            '.$start.', '.$limit.'
                                    ';

	 		$start = $start > 0 ? $start : 0;
	 		
	 		$statement = $this->adapter->query($_strQuery);
	 		
	 		$results = $statement->execute(array(
	 				':_strName' => '%'.$_strName.'%',
	 				':_sidx' => $sidx,
	 				':_sord' => $sord
	 		));
	 		
	 	
	 		$resultSet = new ResultSet;
	 		$resultSet->initialize($results);
	 		$_arrDatosNombreUsuario = $resultSet->toArray();
	 		return $_arrDatosNombreUsuario;
	 	}
 	die();
 }
 	
 	
 	
 	/**
 	 * Cambiar el password del usuario
 	 * @param string  $_strNewPass nuevo pass
 	 * @param int  $_intIdUser id del usuario
 	 * @return bool $_boolResult true en caso de ser ejecutado correctamente
 	 */
 	public function changePass($_strNewPass, $_intIdUser){
 		 
 	if($this->permitirAccesoIP()){
	 		$_strQueryCambiarDatos = "UPDATE t01usuario SET  t01contrasena = :_strNewPass WHERE t01id_usuario = :_intIdUser";
	 		$statement = $this->adapter->query($_strQueryCambiarDatos);
	 		try{
		 		$results = $statement->execute(array(
		 				":_strNewPass" => sha1($_strNewPass),
		 				":_intIdUser" => $_intIdUser
		 		));
		 		
		 		return  true;
	 		}
	 		catch (Exception $e){
	 			return false;
	 		}
	 	
	 }
 	 	die();
  
 	}
 	
 	/**
 	 * Cambiar el username
 	 * @param string  $_strNewUserName nuevo username
 	 * @param int  $_intIdUser id del usuario
 	 * @return bool $_boolResult true en caso de ser ejecutado correctamente
 	 */
 	public function changeUserName($_strNewUserName, $_intIdUser){
 		
 		if($this->permitirAccesoIP()){
	 		$_strQueryCambiarDatos = 'UPDATE t01usuario SET  t01correo = :_strNewUserName WHERE t01id_usuario = :_intIdUser';
	 		$statement = $this->adapter->query($_strQueryCambiarDatos);
	 		try {
	 			$results = $statement->execute(array(
	 							":_strNewUserName" => $_strNewUserName,
	 							":_intIdUser" => $_intIdUser
	 						));
	 		
	 			return true;
	 		}
			catch (Exception $e){
	 			return false;
	 		} 
	 	}
 	die();
 		        
   }

   /**
    * Verificar si el nombre el nombre usuario existe
    * @param string  $_strUserName el username a revisar
    * @param int  $_intIdUser id del usuario
    * @return bool $_boolResult true en caso de que el username exista 
    */
   public function userNameExists($_strUserName,$_intIdUser){
   	if($this->permitirAccesoIP()){
	    	$_strQuery = 'SELECT * FROM t01usuario WHERE t01correo = :_strUserName AND t01id_usuario <>  :_intIdUser';
	    	$statement = $this->adapter->query($_strQuery);
	    	
	    	$results = $statement->execute(array(
	    			':_strUserName' => $_strUserName,
	    			':_intIdUser' => $_intIdUser
	    	));
	    	if($results->count() > 0){
	    		return true;
	    	}
	    	else{
	    		return false;
	    	}
   	}
   	die();
    } 
    
    
	/**
	 * Obtiene la IP origen de la peticion y la compara con el arreglo de IP's permitidas.
	 *
	 * @return boolean
	 * @access privado para no publicar como operacion en el WS.
	 */
	private function permitirAccesoIP(){
		
		$_bolRegresa = false;
		
		if($_SERVER['HTTP_X_FORWARDED_FOR'] != '' ){
			$_strIPCliente = ( !empty($_SERVER['REMOTE_ADDR']) ) ?	$_SERVER['REMOTE_ADDR']	: ( (!empty($_ENV['REMOTE_ADDR'])) ? $_ENV['REMOTE_ADDR']:"unknown" );
		
			// los proxys van añadiendo al final de esta cabecera
			// las direcciones ip que van "ocultando". Para localizar la ip real
			// del usuario se comienza a mirar por el principio hasta encontrar
			// una dirección ip que no sea del rango privado. En caso de no
			// encontrarse ninguna se toma como valor el REMOTE_ADDR
		
			$entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);
		
			reset($entries);
			while (list(, $entry) = each($entries)){
				$entry = trim($entry);
				if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) ){
					$_arrIPsPrivadas = array(
											'/^0\./',
							'/^127\.0\.0\.1/',
							'/^192\.168\..*/',
							'/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
							'/^10\..*/');
		
					$found_ip = preg_replace($_arrIPsPrivadas, $_strIPCliente, $ip_list[1]);
		
					if ($_strIPCliente != $found_ip){
						$_strIPCliente = $found_ip;
						break;
					}
				}
			}
		}
		else{
			$_strIPCliente = ( !empty($_SERVER['REMOTE_ADDR']) ) ?	$_SERVER['REMOTE_ADDR']	: ( (!empty($_ENV['REMOTE_ADDR']))?$_ENV['REMOTE_ADDR']:"unknown" );
		}
		
		foreach($this->_arrIPConsultoras as &$ip){
			if($ip == $_strIPCliente){
				$_bolRegresa = true;
				break;
			}
		}
		
		return $_bolRegresa;
	}
}
?>