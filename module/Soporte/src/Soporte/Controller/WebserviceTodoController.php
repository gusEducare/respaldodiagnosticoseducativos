<?php

namespace Soporte\Controller;

use Zend\Console\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Server;

class WebserviceTodoController extends AbstractActionController {

    public function indexAction() {
        $sm = $this->getServiceLocator();
        $webservice = $sm->get('Soporte\Model\WebserviceTodoModel');
        $_strWsdlUrl = sprintf('http://%s/todo', $_SERVER['HTTP_HOST']);
        //error_log($_SERVER['HTTP_HOST']);

        // Si en la url esta presente la entrada ?wsdl
        if (strtolower($_SERVER['QUERY_STRING']) == "wsdl") {
            // Se incluye la clase AutoDiscover que es la encargada de generar en forma automática el WSDL
            $wsdl = new AutoDiscover();
            $wsdl->setClass('Soporte\Model\WebserviceTodoModel');
            $wsdl->setUri($_strWsdlUrl);
            //$wsdl->setServiceName('WebserviceTodo');
            
//            ob_clean();
//            ob_start();
//            $wsdl->handle();
//            $_respuestaXML = ob_get_clean();
//            $_respuestaXML = trim($_respuestaXML);
            
            $_respuestaXML = $wsdl->toXml();
            //$response = ob_get_clean();
           // $_respuestaXML = $wsdl->handle();
            error_log('_respuestaXML: ' . $_respuestaXML);
        } else {
            $server = new Server($_strWsdlUrl . "?wsdl");
            $server->setObject($webservice);
            $server->setReturnResponse(true);
            $_respuestaXML = $server->handle();
        }
        error_log("---------------------------------------------------------------respuesta XML " . $_respuestaXML);
        $response = $this->getResponse();
        // utf8_encode($_respuestaXML);
        //  $response->getHeaders()->addHeaderLine('Content-Type', 'application/xml');
        $response->setContent($_respuestaXML);

        return $response;
    }

}
