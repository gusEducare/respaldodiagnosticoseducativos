<?php

namespace Soporte\Controller;

use Zend\Console\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Server;


class WebserviceSoportePortalController extends AbstractActionController{
	
        public function indexAction(){
		$sm = $this->getServiceLocator();
		$webservice = $sm->get('Soporte\Model\WebserviceSoportePortal');
		$_strWsdlUrl = sprintf('http://%s/soporte', $_SERVER['HTTP_HOST']);
				
		// Si en la url esta presente la entrada ?wsdl
		if (strtolower($_SERVER['QUERY_STRING']) == "wsdl") {
			// Se incluye la clase AutoDiscover que es la encargada de generar en forma automática el WSDL
			$wsdl = new AutoDiscover();
			$wsdl->setClass('Soporte\Model\WebserviceSoportePortal')
                             ->setUri($_strWsdlUrl)
                             ->setServiceName('WebserviceSoporteDE');
                        //$_respuestaXML = ob_get_clean();
			$_respuestaXML =  strval($wsdl->toXml());
		}else {
			$server	= new Server($_strWsdlUrl."?wsdl");
			$server->setObject($webservice);
			$server->setReturnResponse(true);
			$_respuestaXML = $server->handle();
		}
		
		$response = $this->getResponse();
		$response->setContent($_respuestaXML);
                
                //error_log($_respuestaXML);
		return $response;
		
	}
}


