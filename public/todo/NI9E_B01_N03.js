json=[
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Explica con tus palabras cómo sucede un cambio químico en una disolución:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 4.2 ppm \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E R 3.5 ppm \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 4.0 ppm \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E4.0 ppm\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Una muestra de agua contiene 3.5 mg de iones calcio  en 825 mL de solución. Calcula las partes por millón del ion Calcio en la muestra:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Menciona una mezcla que separarías con filtración."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "¿Cuál es la única restricción del principio de Lavoisier?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 2.5 gramos \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 0.75 gramos \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 1.5 gramos \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 0.65 gramos\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuántos gramos de soluto se necesitan para preparar una solución al 25% con 30 g de CuSO<sub>4</sub>?"
        }
    }
]
