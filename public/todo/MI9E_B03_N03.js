json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 5.3 metros 9.3 metros y 12 metros \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 5.3 metros, 2.7 metros y 4 metros \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 2.7 metros. 4.3 metros y 5.7 metros \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Se quieren construir un tejado para un garaje y se deben colocar postes 4 postes, la distancia entre el primero y el segundo es de 4 m, entre el segundo y el tercero es de 3 m y entre el tercero y el cuarto 2 m. Si la viga que se desea colocar como travesaño mide 12 m a que distancia se deben colocar los postes en la viga midiendo desde el punto A?<div style='text-align:center;'><img src=''></div>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>1</span><span class='bottom'>3</span></span> <\/p></div> \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>1</span><span class='bottom'>3</span></span><\/p></div><div style='margin-top:-65px; margin-left:39px; font-size:16px;'>25</div> <br>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>3</span><span class='bottom'>25</span></span> <\/p></div> \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El alumno Memo  no estudio para el examen del bloque y lo contesto al azar. Sí el examen consta de 25 preguntas y cada una tiene 3 respuestas ¿Cuál es la probabilidad de que responda correctamente todas las preguntas?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 5 \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E  6 \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 8 \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuántos triángulos congruentes hay en la siguiente figura?<div style='text-align:center;'><img src='.png'></div>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E La velocidad de una motocicleta que acelera de forma violenta. \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E  Un objeto que se mueve a velocidad constante. \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E La velocidad con la que gira la Tierra sobre su propio eje. \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Un tren que frena.\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas":true,
            "t11pregunta": "Identifica y señala la situación que corresponde a la gráfica: <div style='text-align:center;'><img src='.png'></div>"
        }
    }
]