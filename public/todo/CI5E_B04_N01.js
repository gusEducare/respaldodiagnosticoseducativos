[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>democracia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>defensa<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>soberanía<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>derecho<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>gobernantes<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>todas<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>decisión<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>en un país implica la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> de la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> de los gobernados y su <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> a elegir y controlar a sus <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. En la práctica consiste en que <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> las personas participen de una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> para su bien común. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Reglamento",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Ley",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Constitución",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "norma",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Es la Carta magna de los Estados Unidos Mexicano que rige actualmente contiene el marco político y legal para la organización y relación del gobierno federal con los estados, los ciudadanos, y todas las personas que viven o visitan el país."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia tienen que ver con los intereses de los gobernantes.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la democracia se respetan todas las formas de pensar.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia buscar poner de acuerdo a personas o grupos con distintos puntos de vista.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la democracia algunas personas ganan y otras pierden.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Independientemente de la forma de pensar, actuar, de nuestras creencias y cultura, todos tenemos la misma importancia a la hora de votar.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Con un gobierno democrático",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Favoreciendo un gobierno oligárquico.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Garantizando los derechos de cada persona.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Siguiendo al pie de la letra las leyes y aplicándolas sin distinción alguna",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aplicando las leyes sólo a algunas personas.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala todas las maneras en que se pueden eliminar situaciones de corrupción desde el gobierno."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>transparencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>recaudación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>salud<\/p>",
                "t17correcta": "0"
            },
              {
                "t13respuesta": "<p>educación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>rendición<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>presupuesto<\/p>",
                "t17correcta": "0"
            },
              {
                "t13respuesta": "<p>hacienda<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ingresos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>servicios<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
             "t11medida": 13,
            "t11pregunta": "Delta sesion 12 a3"
        }
    }
]