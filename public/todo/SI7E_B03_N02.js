json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una exposición formal es un acto comunicativo formal.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lenguaje formal es el que utilizas cuando estás con tus amigos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el lenguaje formal es imperativo pronunciar las palabras de manera correcta; utilizar verbos en segunda persona y emplear oraciones cortas y con sentido.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una exposición oral no requieres investigación previa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El uso de tecnicismos es importante ya que denota tu conocimiento sobre el tema, por tanto, refiere al uso de Lenguaje formal.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una exposición oral y en caso de que el público no sea especializado; es necesario que como expositor, expliques de manera sencilla el significado de tecnicismos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El uso del lenguaje formal es una situación comunicativa distinta a cuando hablas con tu familia o amigos cercanos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es indispensable que evites las muletillas cuando presentes tu exposición oral.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Ruptura y distancias de los temas de la literatura tradicional.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Innovación en las formas expresivas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso indispensable de normas poéticas y gramaticales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se centra en temas de amor y desamor.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La poesía comprende una estructura tradicional.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Abandono de las normas poéticas y gramaticales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso del verso libre (ya no se emplean la rima, la estrofa ni la métrica).<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se toman tradiciones orientales (China y Japón).<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso de métrica, estrofa y rima.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se incorpora el aspecto visual al poema; se juega con la forma, las tipografías y las texturas.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "De entre las siguientes,  señala las más relevantes características de la Poesía de vanguardía del S XX. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>La belleza del cuerpo humano.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La contemplación de la naturaleza.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El amor filial.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los avances modernos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La observación de otras culturas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los animales en cautiverio.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La sensualidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La ciudad.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor los temas recurrentes en la poesía vanguardista.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Son temas recurrentes en la poesía vanguardista",
            "No son temas recurrentes en la poesía vanguardista"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Es breve y nombra tu tema elegido.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Te permite informar a la audiencia acerca del tema de tu  exposición.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aquí describes los contenidos del tema. Presentas toda la información que previamente investigaste.  En este momento se hace uso de los materiales de apoyo (mapas conceptuales, fotografías, ilustraciones, tablas, etcéteras).<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Cierre de tu exposición. Aquí presentaras los resultados de tu investigación o las ideas generales de tema.<\/p>",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra y ordena los títulos.  Considera los momentos para elaborar un guión de apoyo previo a la presentación oral de una investigación.<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["Titulo", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Introducción ", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Desarrollo", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Conclusión", "201,417", "cuadrado", "134, 73", "."]}
          
        ]
    },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EGramática\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFonética\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EProsodia\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Entonación y volumen, pausas, dicción y fluidez son recursos de la…"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Dicción",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Pausas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Entonacion",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Volumen",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Fluidez",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Apela a la correcta pronunciación de  las palabras."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Utilizar la fuerza de voz necesaria para sostener la atención del público."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Se hacen con toda conciencia para mantener la atención de la audiencia en una exposición. Pueden indicar un cambio de tema. Y se equilibran con el ritmo de la exposición."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Los cambios en la misma ayudan a dar énfasis en determinados momentos de la exposición y ayudan a transmitir emociones."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Se refiere evitar el uso de muletillas e interrupciones por parte del expositor."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona ambas columnas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "TimeLine y Ripiti",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Mindomo o iMapMind HD.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Stopmotion Maker  o Stop Motion Studio.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Pic Collage o Pixlr Express",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Google maps",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Audio y video"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Lineas de tiempo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mapas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mapas mentales"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Dibujos y fotografías"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>poesia concreta<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>dadaísmo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>vanguardismo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>subrealismo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>haikú<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>caligrama<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>creacionismo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>futurismo<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Poemas que utilizan el espacio gráfico de la página a su maxima expresión."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Desarrollo de lo ilógico y lo absurdo. Emplea en sus poemas sonidos onomatopéyicos sin ningún significado."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Movimientos literarios del siglo XX caracterizado por renovar la forma y contenido de los textos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Prevalece lo imaginario ante la razón. Destacando el mundo psíquico y de los sueños."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Son composiciones poeticas de origen japones muy breves, no tienen rima y no abordan sentimientos personales."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Poema visual. Sus palabras forman figuras que son mencionadas en su texto. Son característico de la vanguardia·"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se caracteriza por: Privilegiar el estilo del autor, evita anécdotas, suprime los signos de puntuación y el uso usar elementos tipográficos. "
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Su fuente de inspiración son las máquinas y sus principales atributos: fuerza y velocidad."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 8,
                "datoY" : 5
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 6
            },
            {
                "direccion" : 0,
                "datoX" : 7,
                "datoY" : 10
            },
            {
                "direccion" : 1,
                "datoX" : 17,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 15,
                "datoY" : 8
            },
            {
                "direccion" : 0,
                "datoX" : 8,
                "datoY" : 12
            },
            {
                "direccion" : 0,
                "datoX" : 4,
                "datoY" : 15
            },
            {
                "direccion" : 1,
                "datoX" : 5,
                "datoY" : 11
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 10,
            "t11pregunta": "Resuelve el Crucigrama y al final nómbralo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Ciudad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Directora<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Ambiente<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Delegación<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>CDMX<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>cordialmente<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>intervención<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>árboles<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>delegación<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>ecológica<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>salud<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>alarma<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>comercios<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>ordenamientos<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>autorización<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>derribar<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>propiedad<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>clandestina<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>cumplimiento<\/p>",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "<p>vital<\/p>",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "<p>salvaguardar<\/p>",
                "t17correcta": "21"
            },
            {
                "t13respuesta": "<p>propone<\/p>",
                "t17correcta": "22"
            },
            {
                "t13respuesta": "<p>irregularidades<\/p>",
                "t17correcta": "23"
            },
            {
                "t13respuesta": "<p>deterioro<\/p>",
                "t17correcta": "24"
            },
            {
                "t13respuesta": "<p>Atentamente<\/p>",
                "t17correcta": "25"
            },
            {
                "t13respuesta": "<p>Asociación<\/p>",
                "t17correcta": "26"
            },
            {
                "t13respuesta": "<p>Av.<\/p>",
                "t17correcta": "27"
            },
            
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br><br><br><br><br><p> <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de México, 10 de febrero de 2016. <br><br>Sra.  Ligia Butrin Madrigal&nbsp;<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;General de Medio&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>y<br>Desarrollo Sustentable en la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Tlalpan<br>Plaza de la Constitución No. 1<br>Colonia Centro de Tlalpan<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;14000 México<br><br>Sra. Butrin, le saludo&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br> Me permito solicitar su amable&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;a fin de evitar la tala desmedida de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;en nuestra&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br><br>Como es de su conocimiento la situación&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de la ciudad de México y en especial de nuestra delegación va en detrimento año con año. Lo anterior en perjuicio de la&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y calidad de vida de cada uno de los habitantes de nuestra delegación.<br><br>Nuestro grupo de vecinos ve con&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que añejos árboles desaparecen de la noche a la mañana tomando su lugar <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, edificios de oficinas y departamentos.<br><br>Es una prioridad seguir los&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;ecológicos que para tal fin existen, entendemos que  los particulares requieren de la&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;del titular de la Dirección de medio ambiente de la delegación; es decir, de usted para poder&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;árboles aun estando dentro de su&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;con el fin de evitar la tala&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y mayores daños al medio ambiente.<br><br>Sin embargo no se da cabal&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;al referido ordenamiento. Por lo que es de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;importancia para usted la tala desmedida y en la mayoría de los casos clandestina a fin de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;los recursos ecológicos de la demarcación.<br><br>Nuestro grupo le&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> que nos brinde un contacto directo con alguno de sus subalternos para mantenerle informado de las&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que observemos y de esta manera actúe usted en consecuencia y se evite el&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;ecológico.<br><br>Agradecemos su atención y esperamos su oportuna respuesta<br><br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br>Lorena Briseño del Bosque<br>Presidenta<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>de colonos de Tlalpan<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> de las gaviotas No. 55<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "ocultaPuntoFinal": true,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    }
]


