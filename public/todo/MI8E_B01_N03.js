json = [
        {  
      "respuestas":[
         {  
            "t13respuesta":"<p>243</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>–81</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>–243</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>81</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>256</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>-512</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>512</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>-128</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>-12</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>-24</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>-18</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>14</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["\u003Cp\u003E(-3)<sup>5</sup>\u003C\/p\u003E","\u003Cp\u003E(-2)<sup>8</sup>\u003C\/p\u003E","\u003Cp\u003E16 x (-15) ÷ 20\u003C\/p\u003E"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
        {  
      "respuestas":[
         {  
            "t13respuesta":"<p>7<sup>3</sup></p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>1</span><span class='bottom'>7<sup>3</sup></span></span>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>7<sup>5</sup></p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>7<sup>-3</sup></p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>12</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>12<sup>2</sup></p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>1</span><span class='bottom'>12</span></span>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>12<sup>-12</sup></p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["<span class='fraction'><span class='top'>7<sup>4</sup> x 7<sup>7</sup></span><span class='bottom'>(7<sup>4</sup>)<sup>2</sup></span></span>",
                         "<span class='fraction'><span class='top'>12<sup>3</sup> + 12<sup>5</sup></span><span class='bottom'>12<sup>5</sup>x 12<sup>2</sup></span></span> x (12<sup>2</sup>)<sup>4</sup>"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
  {
      "respuestas": [
          {
              "t13respuesta": "esp_kb1_s01_a01_02a.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "esp_kb1_s01_a01_02f.png",
              "t17correcta": "1,2,3",
              "columna":"0"
          },
          {
              "t13respuesta": "esp_kb1_s01_a01_02g.png",
              "t17correcta": "2,3,1",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "bloques":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,162", "cuadrado", "78, 81", "."]}
      ]
  },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Los lados deben medir 4 m, 5.5 m y 7 m.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los ángulos deben ser 88°, 45° y 47°.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los ángulos deben ser 88°, 45° y 47°.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dos lados deben medir 6 m y 4.5 m y el aángulo entre ellos 50°.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige las opciones que muestra los datos que no permiten construir un único triángulo."
        }
    },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>537.5 cm<sup>2</sup></p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>2 500 cm<sup>2</sup></p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>2 150 cm<sup>2</sup></p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>1 612.5 cm<sup>2</sup></p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué opción muestra el área de color vcerde de la siguiente figura?"
      }
   },
  {  
      "respuestas":[
         {  
            "t13respuesta":"<p>44.46 m<sup>2</sup></p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>57.96 m<sup>2</sup></p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>49.5 m<sup>2</sup></p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>54.8 m<sup>2</sup></p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>6 litros</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>5 litros</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>8 litros</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>7 litros</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["¿Qué superficie tiene que pintar Alfonso, sin considerar las puertas y ventana?",
                         "Si utiliza la pintura como indica la leyenda, ¿cuántos litros tiene que comprar para que le alcance y le sobre la menos posible?"],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>10%</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>18%</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>15%</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>25%</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Durante la temporada alta, una tienda de ropa deportiva incrementa 15% el precio de sus productos y al final de la misma, ofrece diferentes descuentos que van de 10% a 40% más un descuento adicional de 20%, sobre el precio ya rebajado.<br/>El precio de unos tenis antes del incremento de 15% era de $1 240 y, al final de la temporada, le aplicaron cierto descuento y sobre el precio resultante, 20% adicional.<br/>Si el precio final de los tenis es de $969.68, ¿cuál fue el primer porcentaje de descuento que aplicaron?"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>118 millones</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>121 millones</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>122 millones</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>119 millones</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"La población de México en 2010 era de aproximadamente 112.3 millones de habitantes y la tasa de crecimiento durante los siguientes años fue la que se muestra la tabla:<br/>\n\
                        <table style: width='100%'>\n\
                        <tr>\n\
                        <td>2011</td>\n\
                        <td>2012</td>\n\
                        <td>2013</td>\n\
                        <td>2014</td>\n\
                        <td>2015</td>\n\
                        <tr/>\n\
                        <tr>\n\
                        <td>1.1 %</td>\n\
                        <td>1.1 %</td>\n\
                        <td>1.1 %</td>\n\
                        <td>1.2 %</td>\n\
                        <td>1.4 %</td>\n\
                        <tr/>\n\
                        </table>\n\
                        </br>\n\
                        ¿Cuál era la población aproximada de México al final del 2015?"
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>más probable<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>igualmente probable<\/p>",
                "t17correcta": "2,3"
            },
            {
                "t13respuesta": "<p>igualmente probable<\/p>",
                "t17correcta": "3,2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que en la división el resultado sea un número decimal periódico que uno finito.<br/>Es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que el número decimal sea mayor que 0.5 que menor que 0.5.<br/>Es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que el cociente sea la unidad que un número decimal periódico.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
{  
      "respuestas":[
         {  
            "t13respuesta":"<p>Verdadero</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Falso</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Verdadero</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Falso</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Verdadero</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Falso</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Verdadero</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Falso</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["La tabla muestra el porcentaje de tiros libres acertados por los integrantes de un equipo de basquetbol durante la primera y segunda mitad de la temporada.\n\
                         <table style: width='100%'>\n\
                        <tr>\n\
                        <td>Jugador</td>\n\
                        <td>Joshua</td>\n\
                        <td>Matiás</td>\n\
                        <td>Santiago</td>\n\
                        <td>Didier</td>\n\
                        <td>Neri</td>\n\
                        <td>Daniel</td>\n\
                        <td>Josue</td>\n\
                        <td>Leonel</td>\n\
                        <tr/>\n\
                        <tr>\n\
                        <td>Primera mitad</td>\n\
                        <td>65 %</td>\n\
                        <td>85 %</td>\n\
                        <td>70 %</td>\n\
                        <td>80 %</td>\n\
                        <td>80 %</td>\n\
                        <td>80 %</td>\n\
                        <td>65 %</td>\n\
                        <td>85 %</td>\n\
                        <tr/>\n\
                        <td>Segunda mitad</td>\n\
                        <td>50 %</td>\n\
                        <td>80 %</td>\n\
                        <td>75 %</td>\n\
                        <td>80 %</td>\n\
                        <td>90 %</td>\n\
                        <td>95 %</td>\n\
                        <td>40 %</td>\n\
                        <td>85 %</td>\n\
                        <tr/>\n\
                        </table>\n\
                        </br>\n\
                        La media es un dato representativo del rendimiento del equipo en la primera mitad de la temporada.\n\
                            ",
                "La media no es un dato representantivo del rendimiento del equipo en la segunda mitad de la temporada.",
                "La mediana no es un valor que permita comparar el rendimiento del equipo en las dos mitades de la temporada.",
                "El rendimiento del equipo fue mejor en la primera mitad de la temporada que en la segunda."],
         "preguntasMultiples": true,
         "columnas":1
      }
   }
]

