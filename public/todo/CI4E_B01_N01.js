json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Psicológico<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Emociones<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cambios<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Físico<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Únicos<\/p>",
                "t17correcta": "0"
            },
                        {
                "t13respuesta": "<p>Transformar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Individuo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Personal<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cuerpo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pensar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Edad<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida": 16,
            "t11pregunta": "Identifica las palabras relacionadas con tu crecimiento."
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_01_01.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_01_02.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_01_03.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_01_04.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_01_05.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_01_06.png'><\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas":2,
            "t11pregunta": "Selecciona las personas con las que puedas compartir información personal como tu número telefónico y fecha de nacimiento."
        }
    },{
        "respuestas": [
            {
                "t13respuesta": "<p>Paz en el mundo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>cuidado del ambiente<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>la educación de los niños<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>servicio de salud digno<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>proteger a los animales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>escuchar música clásica<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>elegir cómo vestirnos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>equipo de fútbol<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>ser vegetarianos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>practicar determinada religión<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Clasifica las acciones en las que todas las personas debemos estar de acuerdo y las que pueden ser diversas de acuerdo a los gustos o preferencias de cada individuo.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Universal",
            "Particular"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>intrapersonal<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>interpersonal<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>kinestésica<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>musical<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>espacial<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>lógico<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>verbal<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Identifica las palabras relacionadas con la teoría de las inteligencias múltiples de Howard Gardner."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al colaborar con los demás en los trabajos que realizamos en equipo puedo imponer mi punto de vista, sin necesidad de aprovechar las habilidades que tienen mis otros compañeros.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando todos aportamos nuestras  ideas y habilidades se enriquece el trabajo, además compartimos la responsabilidad y aprendernos a de nuestros pares.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando trabajamos de forma colaborativa e interdisciplinaria, la cantidad de trabajo de cada persona disminuye, además la responsabilidad de la calificación no es sólo mía.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correspondiente:<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },{
        "respuestas": [
            {
                "t13respuesta": "Hacer objeto de trato deshonesto a una	persona de menor experiencia, fuerza o poder.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tratar mal a alguien de palabra u obra.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Utilizar abusivamente en provecho propio el trabajo o las cualidades de otra persona.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Abuso"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Maltrato"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Explotación"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los conceptos que correspondan."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_02_01.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_02_02.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_02_03.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_02_04.png'><\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona algunos ejemplos de los derechos que instituciones como el DIF o la UNICEF buscan proteger en la niñez de nuestro país y el mundo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_03_01.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_03_02.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_03_03.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_03_04.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='ci4e_b01_n01_03_05.png'><\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas":2,
            "t11pregunta": "Selecciona algunos de los profesionales que ayudan al cuidado de la salud de niños y niñas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>IMSS<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ISSSTE<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>salud<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>seguro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>popular<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>física<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>prevención<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>cuidado<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>enfermedad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>revision<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Encuentra las siguientes palabras:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p><img src='CI4E_B01_N01_04_01.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='CI4E_B01_N01_04_02.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='CI4E_B01_N01_04_03.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='CI4E_B01_N01_04_04.png'><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src='CI4E_B01_N01_04_05.png'><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src='CI4E_B01_N01_04_06.png'><\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas":2,
            "t11pregunta": "Selecciona las actividades que muestren convivencia familiar."
        }
    }
]
