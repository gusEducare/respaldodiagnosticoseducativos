json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Sólido<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Caliente<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Líquido<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Frio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Plasma<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tibio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Gaseoso<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Soleado<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Coloide<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas":2,
            "t11pregunta": "Señala todos los posibles estados físicos de la materia"
        }
    },
  {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Aumento de temperatura</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Disminución de temperatura</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Temperatura estable</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Aumento de temperatura</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Disminución de temperatura</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Temperatura estable</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["La evaporación de líquidos se produce por:","La condensación de líquido ocurre por"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
 {
        "respuestas": [
            {
                "t13respuesta": "<p>Color<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sabor<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Duración<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Consistencia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Vitaminas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Olor<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Minerales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Antojo<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige las propiedades de los alimentos que cambian por el calor."
        }
    },
 {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Temperatura</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Calor</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Temperatura</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Calor</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Temperatura</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Calor</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Temperatura</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Calor</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Te permite conocer que tan caliente o frio esta un objeto  determinado.","Cambia el estado físico de la materia.","Puede medirse con un termómetro.","Genera movimiento"],
         "preguntasMultiples": true,
         "columnas":2
      }
   }
]