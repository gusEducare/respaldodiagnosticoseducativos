json=  [
{
	"respuestas": [
	{
		"t13respuesta": "a<sup>2</sup>+3",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "a<sup>2</sup>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "2a",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "a<sup>2</sup> + 3",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "1",
		"dosColumnas": true,
		"t11pregunta": "<p style='margin-bottom: 5px;'>¿Qué expresión algebraica representa el área designada  para las butacas? <div  style='text-align:center;'><img style='height: 360px' src='mi9e_b01_n02_01.png'></div></p>"
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "6 m",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "5 m",
		"t17correcta": "0"
	}
	,
	{
		"t13respuesta": "4 m",
		"t17correcta": "0" 
	},
	{
		"t13respuesta": "7 m",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "1",
		"dosColumnas": true,
		"t11pregunta": "<p style='margin-bottom: 5px;'>Si el área destinada para las butacas es de 36m<sup>2</sup>, ¿cuánto mide X?<div  style='text-align:center;'><img style='height: 360px' src='MI9E_B01_N01_02.png'></div></p>"
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "MI9E_B01_N01_03_02.png",
		"t17correcta": "0,1,2",
		"columna":"0"
	},
	{
		"t13respuesta": "MI9E_B01_N01_03_03.png",
		"t17correcta": "1,0,2",
		"columna":"0"
	},
	{
		"t13respuesta": "MI9E_B01_N01_03_04.png",
		"t17correcta": "2,0,1",
		"columna":"1"
	},
	{
		"t13respuesta": "MI9E_B01_N01_03_05.png",
		"t17correcta": "3,4,5",
		"columna":"1"
	},
	{
		"t13respuesta": "MI9E_B01_N01_03_06.png",
		"t17correcta": "4,3,5",
		"columna":"0"
	},
	{
		"t13respuesta": "MI9E_B01_N01_03_07.png",
		"t17correcta": "5,4,3",
		"columna":"0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "<p>Arrastra del contenedor a la tabla, las figuras que por sus propiedades sean congruentes o semejantes a la foto<br/><\/p>",
		"tipo": "ordenar",
		"imagen": true,
		"url":"MI9E_B01_N01_03_08.png",
		"respuestaImagen":true, 
		"bloques":false,
		"tamanyoReal":true,
		"opcionesTexto":true,
		"borde":false

	},
	"contenedores": [
	{"Contenedor": ["", "75,93", "cuadrado", "95, 80", ".","transparent"]},
	{"Contenedor": ["", "185,92", "cuadrado", "95, 80", ".","transparent"]},
	{"Contenedor": ["", "283,90", "cuadrado", "95, 80", ".","transparent"]},
	{"Contenedor": ["", "75,370", "cuadrado", "95, 80", ".","transparent"]},
	{"Contenedor": ["", "185,370", "cuadrado", "95, 80", ".","transparent"]},
	{"Contenedor": ["", "283,370", "cuadrado", "95, 80", ".","transparent"]}
	]
},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Por sus características, podemos afirmar que los triángulos anaranjado y amarillo son semejantes.",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<br><div style='text-align: center'><img src='MI9E_B01_N01_04.png'></div><br><\/p>",
		"variante": "editable",
		"anchoColumnaPreguntas": 60,
		"evaluable"  : true
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "En un dulcero hay tres chocolates blancos, y cinco chocolates con leche, del mismo tamaño. Sacar un chocolate sin ver. Por las características del evento se puede decir que es un evento complementario.",
		"correcta"  : "1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
		"variante": "editable",
		"anchoColumnaPreguntas": 60,
		"evaluable"  : true
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"Los lados AC, AD, CB,DE y AB, AE son proporcionales respectivamente.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Los ángulos internos son congruentes.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Dos lados son proporcionales y los ángulos correspondientes son congruentes",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"dosColumnas": true,
		"t11pregunta":"<p style='margin-bottom: 5px;'>Señala cuál de los siguientes criterios verifica que los triángulos ABC y AED son semejantes:<div  style='text-align:center;'><img style='height: 360px' src='mi9e_b01_n01_05.png'></div></p>"
	}
},
{
	"respuestas":[  
	{  
		"t13respuesta":"10",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"20",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"30",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"40",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"dosColumnas": true,
		"t11pregunta":"<p style='margin-bottom: 5px;'>Completa la siguiente tabla identificando cuál es la relación de proporcionalidad.<div  style='text-align:center;'><table style='background-color:#3d85c6;'><tr><th style='color:white; font-size:15px;'>Distancia</th><th style='color:white; font-size:16px;'>tiempo</th><th style='color:white; font-size:16px;'>Velocidad=distancia/tiempo</th></tr><tr><td style='background-color: rgb(207, 226, 243);'>10</td><td style='background-color: rgb(207, 226, 243);'>1</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>20</td><td style='background-color: rgb(207, 226, 243);'>2</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>30</td><td style='background-color: rgb(207, 226, 243);'>3</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>40</td><td style='background-color: rgb(207, 226, 243);'>4</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>50</td><td style='background-color: rgb(207, 226, 243);'>5</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>60</td><td style='background-color: rgb(207, 226, 243);'>6</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>70</td><td style='background-color: rgb(207, 226, 243);'>7</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>80</td><td style='background-color: rgb(207, 226, 243);'>8</td><td></td></tr><tr><td style='background-color: rgb(207, 226, 243);'>90</td><td style='background-color: rgb(207, 226, 243);'>9</td><td></td></tr><tr><td>Relación de proporcionalidad</td><td></td></tr></table></div></p>"
	}
},
{
	"respuestas":[  
	{  
		"t13respuesta":"\u003Cp\u003EIncluir la pregunta: “¿te gustaría estudiar la carrera de física, química o matemáticas?”, en un examen.\u003C\/p\u003E",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"\u003Cp\u003EPreguntar a los orientadores si les gustaría ser científicos.\u003C\/p\u003E",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"\u003Cp\u003EPreguntar alumno por alumno si le gustan las matemáticas.\u003C\/p\u003E",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"La profesora de química desea saber quiénes de sus estudiantes desean ser profesionistas en el área de ciencias exactas. ¿Cuál de los siguientes medios es más eficiente para su propósito?"
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"\u003Cp\u003E<img src='mi9e_b01_n01_07_01.png'>\u003C\/p\u003E",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"\u003Cp\u003E<img src='mi9e_b01_n01_07_02.png'>\u003C\/p\u003E",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"\u003Cp\u003E<img src='mi9e_b01_n01_07_03.png'>\u003C\/p\u003E",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"columnas":2,
		"t11pregunta":"¿Cuál de los siguientes pares de figuras no son congruentes?"
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"\u003Cp\u003ESacar un rey en una baraja convencional.\u003C\/p\u003E",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"\u003Cp\u003ETirar un dado y que salga un tres.\u003C\/p\u003E",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"\u003Cp\u003ETirar un volado.\u003C\/p\u003E",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"\u003Cp\u003EJugar a la pirinola y que salga un “tome todo”.\u003C\/p\u003E",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Selecciona cuál de los siguientes eventos es un ejemplo de evento complementario y arrástralo al contenedor."
	}
}
]