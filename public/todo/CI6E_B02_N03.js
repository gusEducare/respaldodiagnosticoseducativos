json = [
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones son alteraciones del ánimo que se acompañan de cierta conmoción somática.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los sentimientos son mucho más cortos que las emociones, no pueden mantenerse por largos periodos de tiempo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones son expresadas y los estímulos que las generan, de forma inmediata producen una respuesta física.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones y los sentimientos deben expresarse de manera automática, permitiendo liberarlas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es necesario que los demás acepten nuestras emociones, sin importar la forma en las que las expresemos.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Contesta  falso o verdadero según corresponda. <\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
        }        
    },
    {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003ECompromiso\u003C\/p\u003E",
            "t17correcta":"0"
         },
          {
            "t13respuesta":"\u003Cp\u003EDepender emocionalmente\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003EAdaptación y flexibilidad\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003ENo tomar decisiones\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003EAutocontrol emocional\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003ENo hacer nada\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003EOrganizacional\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003ETratar de quedar bien con los demás\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003EAutomotivación\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EAutocomplacencia\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"5",
         "t11pregunta":"Arrastra a cada contenedor la respuesta correspondiente.",
         "tipo":"horizontal"
      },
        "contenedores":[ 
            "Favorece el logro de tus metas",
            "Dificulta el logro de tus metas"
            
        ]
   },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>principios<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>universales<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>derechos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>interioriza<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>familia<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>actuar<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;son una guía para el ser humano, aunque no todos los principios son <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, todos comienzan con base en los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;humanos y éstos, ayudan a desenvolvernos día a día. Son las asimilaciones que la persona <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y realiza con base en la información que le transmite su <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, la cual a su vez, está inmersa en una sociedad. Son las formas de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;correctas que nos hacen sentir bien.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente párrafo arrastrando las palabras a donde les corresponde."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p> Recibir el obsquio que pediste en navidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La muerte de tu mascota.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Saber que tu abuela enfermó.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Presentar un examen final para el que no estás preparado.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Tener que decidir entre dos cumpleaños de tus amigos que serán en el mismo día.<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Obtener el primer lugar en la competencia.<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Felicidad</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Tristeza</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Preocupación</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Neriviosismo</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Duda</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Orgullo</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona la emoción que te puede provocar cada circunstancia."
        }
    },
       {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Respeto</p>",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Equidad</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Tolerancia</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Paciencia</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Justicia</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Respeto</p>",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Equidad</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Tolerancia</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Paciencia</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Justicia</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Respeto a las ideas, creencias o prácticas de los demás cuando son diferentes o contrarias a las propias.",
                        "Principio moral que lleva a dar a cada uno lo que le corresponde."],
         "preguntasMultiples": true,
         "columnas":1
      }
   }
]