json = [
{
	"respuestas": [
	{
		"t13respuesta": "<p>adolescencia<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>infancia<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>físicos<\/p>",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "<p>psicológicos<\/p>",
		"t17correcta": "4"
	},
	{
		"t13respuesta": "<p>adaptación<\/p>",
		"t17correcta": "5"
	},
	{
		"t13respuesta": "<p>pertenecer<\/p>",
		"t17correcta": "6"
	},
	{
		"t13respuesta": "<p>aceptación<\/p>",
		"t17correcta": "7"
	},
	{
		"t13respuesta": "<p>emocionales<\/p>",
		"t17correcta": "8"
	},
	{
		"t13respuesta": "<p>humor<\/p>",
		"t17correcta": "9"
	},
	{
		"t13respuesta": "<p>exagerada<\/p>",
		"t17correcta": "10"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p><br>La <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; es la etapa de transición entre la <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; y la vida adulta. Además de los evidentes cambios <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;  por los que estás pasando, se presentan  importantes cambios <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>.Éstos se correlacionan con tu necesidad de <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; a la nueva apariencia física que tienes, así como a la necesidad de <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; a grupos  y la <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; social. En esta etapa los cambios <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; drásticos son normales: Notarás cambios de <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; y que eventualmente reaccionarás de manera <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; a algunas cosas que antes no te molestaban.<\/p>"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"pintaUltimaCaja":false,
		"t11pregunta": "Completa la siguiente información:"
	}
},

{
	"respuestas": [
	{
		"t13respuesta": "<p>madurez<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>planeación<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>futuro<\/p>",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "<p>persona<\/p>",
		"t17correcta": "4"
	},
	{
		"t13respuesta": "<p>trabajar<\/p>",
		"t17correcta": "5"
	},
	{
		"t13respuesta": "<p>amigos<\/p>",
		"t17correcta": "6"
	},
	{
		"t13respuesta": "<p>familia<\/p>",
		"t17correcta": "7"
	},
	{
		"t13respuesta": "<p>decisiones<\/p>",
		"t17correcta": "8"
	},
	{
		"t13respuesta": "<p>acciones<\/p>",
		"t17correcta": "9"
	},
	{
		"t13respuesta": "<p>cumplir<\/p>",
		"t17correcta": "10"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p><br>Una muestra de <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;, es la <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; de tu <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;. Imagina el tipo de <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p> &nbsp;que te gustaría ser, en qué te gustaría <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;, el tipo de relaciones y <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; que elegirás, cómo será tu <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; y el tipo de vida que deseas para tu adultez. Claro que además de imaginar debes hacer planes, tomar <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; y luego <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; que te llevarán a <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; tus sueños.<\/p>"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"pintaUltimaCaja":false,
		"t11pregunta": "Arrastra las palabras para completar la siguiente información."
	}
},
{
	"respuestas":[  
	{  
		"t13respuesta":"Contagio de Enfermedades de transmisión sexual.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Rechazo de la familia.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Embarazo no deseado.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Maternidad y paternidad temprana.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Burla de los amigos.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Contagio de virus de papiloma humano que favorece el cáncer.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Contagio de  virus de la inmunodeficiencia humana o VIH.",
		"t17correcta":"1"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"2",
		"t11pregunta":"Señala todos los riesgos de ejercer tu vida sexual a temprana edad."
	}
},
{
	"respuestas":[  
	{  
		"t13respuesta":"Conocer todas las posibles consecuencias de tus decisiones.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Estar seguro de qué es lo que tú deseas y no estás siendo presionado para hacerlo.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Buscar información y hablar con expertos de los métodos anticonceptivos y de protección que podrías llegar a utilizar. ",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Platicar con tus amigos sobre lo que se espera de ti.",
		"t17correcta":"0"
	},

	{  
		"t13respuesta":"Conocerte a ti mismo.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Contar con información suficiente para practicarlo de manera saludable.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"No tomar en cuenta la opinión de ningún adulto en relación al tema de la sexualidad.",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"2",
		"t11pregunta":"Algunos aspectos que debes tomar en cuenta antes de iniciar tu vida sexual de manera óptima y saludable son:"
	}
},
{
	"respuestas":[  
	{  
		"t13respuesta":"El VIH se transmite a través de picaduras de insectos.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Se transmite por contacto sexual no protegido con alguna persona infectada.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Se transmite por contacto casual o cotidiano como los abrazos, las caricias, los besos.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Se transmite por transfusiones de sangre infectada.",
		"t17correcta":"1"
	},

	{  
		"t13respuesta":"Se transmite por usar el baño de personas infectadas.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Se transmite por compartir agujas con personas infectadas.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Se transmite por uso de artículos personales de alguien infectado.",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"2",
		"t11pregunta":" Señala las opciones donde se explican las formas de contagio  del VIH."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>conocimiento<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>información<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>computadoras<\/p>",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "<p>medios<\/p>",
		"t17correcta": "4"
	},
	{
		"t13respuesta": "<p>profesionales<\/p>",
		"t17correcta": "5"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>Actualmente vivimos en la sociedad del <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>, esto quiere decir que la <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; está disponible en nuestros celulares, en nuestras <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; entre muchos otros <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; sumados a los que ya existían antes como: libros, revistas y periódicos que nos permitían conocer todo aquello que quisiéramos saber acerca de un tema en particular. Sin embargo, también podemos acudir a <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>,  a cursos, talleres y diferentes lugares en los que otras personas expertas en el tema nos pueden enseñar sobre sexualidad<\/p>"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "Arrastra las palabras para completar la siguiente información:"
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Los grandes descubrimientos del siglo XX",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Descubriendo nuestra historia",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "Los vídeos más divertidos del mundo",
		"t17correcta": "3"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Científico"
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Cultural"
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Recreativo"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Relaciona a qué tipo de programa televisivo crees que correspondan los siguientes nombres de programas:"
	}
},

{
	"respuestas": [
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Falso",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Todas las características que ofrecen los productos en los comerciales de radio y televisión son ciertos.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Mucha de la publicidad, busca manipularte para que adquieras el producto que están vendiendo.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "No toda la información que proviene de los medios masivos de comunicación es cierta.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "No necesitas verte como los personajes de las películas o los anuncios comerciales para que los demás te acepten.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Los medios de comunicación tienen una gran influencia en la sociedad, pero puedes contrarrestar la parte negativa, analizando el tipo de contenidos que te presentan.",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
		"descripcion":"Aspectos a valorar",
		"evaluable":false,
		"evidencio": false
	}
}
]