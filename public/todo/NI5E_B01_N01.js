json=[    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>dieta suficiente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>dieta completa<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>dieta equilibrada<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>dieta variada<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>dieta inocua<\/p>",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "La dieta debe aportar la cantidad de nutrimentos necesarias para cada persona de acuerdo a su edad y sus caracteristicas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es aquella que incluye todos los grupos de alimentos del plato del buen comer y considera beber según lo sugiere la jarra del buen beber."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es en la que los alimentos y los nutrientes que consumas deben ser estar en la proporcion adecuada. Por ejemplo, no debes comer muchos alimentos con grasas y debes comer una mayor cantidad de frutas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "La dieta debe incluir diferentes alimentos de los tres grupos del plato del buen comer o que por ejemplo, del grupo de alimentos de origen animal incluyas todos los que puedas de manera alternada."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Significa que no te haga daño, es decir, que los alimentos que consumas no te causen algún daño, te caigan mal, estén sucios, o en mal estado, tengan microbios que te puedan causar enfermedades."
            },
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 7,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 6,
                "datoY" : 2
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 13
            },
            {
                "direccion" : 1,
                "datoX" : 10,
                "datoY" : 4
            },
            {
                "direccion" : 1,
                "datoX" : 15,
                "datoY" : 9
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Completa el siguiente crucigrama que habla de cómo debe ser tu dieta.",
            "alto" : 21,
            "ancho" : 20
        }
    },     
       {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando hablamos de una cantidad y variedad de alimentos que una persona consume, nos referimos a la alimentación.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dentro de nuestra alimentación debemos de tomar en cuenta que tanto adultos como niños, pueden consumir el mismo tipo de alimentos, ya que todos son necesarios.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para hablar de nutrición, nos referimos a un proceso por el cual el cuerpo absorbe los nutrientes que están en los alimentos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los nutrientes aseguran que podamos estar sanos y promueven nuestros crecimiento.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "-Los alimentos son sustancias de las cuáles podemos obtener los nutrientes que nuestros cuerpo necesita.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elije “falso” o “verdadero” según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
  {
      "respuestas": [
          {
              "t13respuesta": "ni5e_b01_n01_04.png",
              "t17correcta": "2,1",
              "columna":"1"
          },
          {
              "t13respuesta": "ni5e_b01_n01_02.png",
              "t17correcta": "1,2",
              "columna":"0"
          },
          {
              "t13respuesta": "ni5e_b01_n01_03.png",
              "t17correcta": "0",
              "columna":"0"
          },
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>¿Cómo se calcula el índice de masa corporal? Arrastra los siguientes elementos para ordenar la fórmula.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"ni5e_b01_n01_01.png",
          "respuestaImagen":true,
          "anchoImagen": 60,
          "bloques":false,
          "tamanyoReal" : true
          
      },
      "contenedores": [
          {"Contenedor": ["", "201,225", "cuadrado", "280, 55", "."]},
          {"Contenedor": ["", "270,65", "cuadrado", "280, 55", "."]},
          {"Contenedor": ["", "270,368", "cuadrado", "280, 55", "."]}
      ]
  },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Autoestima<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Trastornos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Anorexia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Bullying<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Infartos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Obesidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Diabetes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Bulimia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Presión<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Peso<\/p>",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Resuelve la siguiente sopa de letras en relación a la obesidad."
        }
    },
       {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un embarazo es la etapa en la que se unen el óvulo con el espermatozoide.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La célula que se forma en la fecundación es el embrión.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El útero es el órgano en el que se implanta el embrión.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A través de la placenta y el cordón umbilical, la madre nutre al feto durante el embarazo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un parto es la etapa de la reproducción humana durante la cual el nuevo ser crece y se desarrolla dentro del vientre de la mujer.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un embrión es el nuevo ser en el que se están formando los órganos y los tejidos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Por medio de las contracciones es que el cuerpo de la mujer puede expulsar al feto durante el parto.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    }    
]