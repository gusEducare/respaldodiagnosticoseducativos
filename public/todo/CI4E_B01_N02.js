json = [
  {
        "respuestas": [
            {
                "t13respuesta": "<p>Conseguir un empleo y obtener un salario digno por el trabajo realizado.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Escuchar y decidir el tipo de música que prefiero.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Vivir solo, teniendo diferentes responsabilidades como mi propio sustento.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elegir el equipo deportivo que quiero apoyar.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ser responsable de mis cosas, lavar mis trastes o hacer mi cama.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Votar en las elecciones para eleguir al próximo presidente de mi país.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Selecciona 3 actividades que puede realizar un niño y otras 3 que pueda realizar un adulto de acuerdo a su desarrollo.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Niño",
            "Adulto"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Sentimiento",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Sensación",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Me siento muy contento porque mi cumpleaños se acerca.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Me cansé de estudiar toda la tarde.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Me duele todo el cuerpo después del entrenamiento de natación.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estoy emocionado por la competencia que tendré la próxima semana.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los participantes de los juegos olímpicos merecen mucho respeto por el gran esfuerzo que realizan. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Me da mucha hambre después de la escuela.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Relaciona los siguientes elementos:<\/p>", 
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Si me encuentro perdido y debo darle el número telefónico de mis padres a un policía para poder contactarlos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Platicar con desconocidos a través de internet.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Responder cuestionarios en la calle. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Brindar información sobre algún padecimiento que pueda tener al doctor o enfermera de la escuela.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Llenar formularios en internet donde me piden fecha de nacimiento, dirección y teléfono para descargar un juego. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Consultar a mis padres cuando debo llenar algún formulario por parte de la escuela. <\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las acciones que debes evitar para no correr riesgos al dar información sobre ti."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las personas que vivimos en México somos diferentes a las que viven en Estados Unidos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cada persona es diferente, por lo que debemos valorar las diferencias y respetarlas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando alguien no comparte mi opinión tengo derecho a cambiar su punto de vista, sin importar lo que los demás opinen.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si me siento enojado porque alguien  no opina como yo, no debo hablar con esa persona.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las personas son únicas e irrepetibles, por lo que deben pensar de la misma forma.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta que consideres correcta.<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>verbal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>lógico</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>espacial</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>musical</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>kinestésica</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>interpersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>intrapersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },{  
            "t13respuesta":"<p>verbal</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>lógico</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>espacial</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>musical</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>kinestésica</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>interpersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>intrapersonal</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Capacidad para identificar modelos, calcular, formular y verificar hipótesis, utilizar el método científico y los razonamientos inductivo y deductivo.","Capacidad para plantearse metas, evaluar habilidades y desventajas personales, así como controlar el pensamiento propio."],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {  
      "respuestas":[
         {  
            "t13respuesta":"<p>verbal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>lógico</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>espacial</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>musical</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>kinestésica</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>interpersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>intrapersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },{  
            "t13respuesta":"<p>verbal</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>lógico</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>espacial</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>musical</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>kinestésica</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>interpersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>intrapersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Capacidad para presentar ideas de manera visual, crear imágenes mentales, percibir detalles visuales, dibujar y confeccionar bocetos.","Capacidad para comprender el orden y significado de las palabras en la lectura, la escritura y, también, al hablar y escuchar."],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {  
      "respuestas":[
         {  
            "t13respuesta":"<p>verbal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>lógico</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>espacial</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>musical</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>kinestésica</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>interpersonal</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>intrapersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },{  
            "t13respuesta":"<p>verbal</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>lógico</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>espacial</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>musical</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>kinestésica</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>interpersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>intrapersonal</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Trabajar con gente, ayudar a las personas a identificar y superar problemas.","Capacidad para realizar actividades que requieren fuerza, rapidez, flexibilidad, coordinación ojo mano y equilibrio"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Carácter<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Género<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Origen étnico<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Forma de ser<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Situación socioeconómica<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Los niños son especialmente vulnerables en los casos de explotación y abuso debido a su : "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Cada persona aporta ideas y eso enriquece el trabajo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No hacemos mucho trabajo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las personas tenemos distintas habilidades, al trabajar de esta manera se fortalecen nuestras áreas de oportunidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Compartimos la responsabilidad y aprendernos a colaborar con otros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ya no soy responsable de la calificación.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala las opciones que consideras ventajas al realizar trabajo interdisciplinario."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Sus siglas identifican al Sistema Nacional para el Desarrollo Integral de la Familia. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se trata de un organismo nacional que ayuda a proteger y defender los derechos de los niños a través de políticas públicas relacionadas con la asistencia social.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es un organismo internacional que ayuda a los niños a satisfacer necesidades como la educación, alimentación, mejorar sus condiciones de vida, etc<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sus siglas en inglés significan Fondo Internacional de Emergencia de las Naciones Unidas para la Infancia.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Relaciona las características con los organismos que se encargan de proteger los derechos de la niñez.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "<img src='ci4e_b01_n02_01.png'>",
            "<img src='ci4e_b01_n02_02.png'>"
        ]
    }
    ,
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Ayudan a prevenir algunas enfermedades.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No podemos ver todas las enfermedades a simple vista.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es importante consumir medicinas constantemente<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La falta de revisión puede traer mayores problemas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sólo es importante acudir al médico si te sientes mal.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Realizar estudios médicos es una pérdida de tiempo y dinero<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "¿Por qué es importante acudir al doctor? Selecciona las opciones que respondan a esta pregunta."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando juego con mis compañeros debo ser respetuoso y escuchar a todos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A pesar de conocer las reglas del juego, puedo cambiarlas, si eso me permite ganar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando no obtengo el resultado que espero en algún juego o actividad debo dejar de participar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La UNICEF nos reitera el derecho que tienen todos los niños de jugar y divertirse sanamente.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La recreación es necesaria sólo para los niños, los adultos deben trabajar sin descanso.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona falso o verdadero.<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
]