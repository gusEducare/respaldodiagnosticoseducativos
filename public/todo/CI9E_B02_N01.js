json=[
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Habilidad</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Inteligencia</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>capacidad</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>desarrollo</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Habilidad</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Inteligencia</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>capacidad</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>desarrollo</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Conjunto de condiciones  consideradas como cualidades propias de un ser y que favorecen el desarrollo del mismo para cumplir una función u objetivo determinado.",
                         "Capacidad de una persona para desarrollar una actividad de manera óptima y con total facilidad a través del ejercicio de la misma."],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Responsable<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>evaluación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>objetivo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>información<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>aprendizaje<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ética<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>valores<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>acción<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>voluntad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>principios<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>social<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>personal<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ámbito<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Tolerancia</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Diálogo</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Responsabilidad</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Cooperación</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Euforia</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Asombro</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Desconocimiento</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Emoción</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Diálogo</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Hablar mucho más alto que los otros.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Invalidar las posturas contrarias.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>No manejar las emociones.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Respetar las opiniones de otros, aun cuando no se está de acuerdo con las mismas.",
                         "En caso de tener la responsabilidad ciudadana de tomar una decisión; un factor que puede generar duda es: ",
                         "Un elemento de un debate plural para una convivencia democrática puede ser: "],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Compromiso<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>suerte<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>constancia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>breve<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>esfuerzo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>flojera<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>planeación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>azar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>medios<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>acciones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>definir<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>lograr<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>rápido<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>corto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>largo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> mediano plazo<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "",
            "columnas":2
        }
    } ,{
        "respuestas": [
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=''><\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "",
            "columnas":2
        }
    }
];