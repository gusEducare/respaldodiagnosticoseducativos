json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>libertad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>derechos<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>considerar<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>obligaciones<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>sociedad<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>prudencia<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Si bien es cierto que la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es uno de nuestros <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; fundamentales, también hemos de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; que como lo expresó Jean Paul Sartre (1905-1980. Filósofo francés) &quot;<b>mi libertad termina donde empieza la de los demás</b>&quot;. Es decir, la libertad se relaciona con derechos pero también  <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; con nuestros pares y la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; en que nos desenvolvemos. Entonces hemos de ejercer nuestra libertad con responsabilidad y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;.<\/p>"
            }            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El enojo es un sentimiento que puede ser controlado si pensamos antes de actuar. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al perder el control, se puede herir o lastimar a la gente que queremos. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si respiramos profundo, tenemos la certeza de que no nos enojaremos y no diremos cosas que puedan lastimar al otro.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }, {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser conscientes de que el enojo es algo normal, nos puede ayudar a buscar y hacer ejercicios lentos para ayudar a calmarnos cuando estemos en una situación incómoda.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lo que nos produce enojo, es muy difícil de identificar, por lo que la única alternativa cuando estemos en una situación que lo produce, es gritar y tratar de desahogarnos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Cumplir con las leyes aún sin que nadie lo sepa o te vea.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Tomar la decisión de hacer lo correcto en cada oportunidad que tengas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Ofrecer dádivas, regalos o extorsiones a quienes son encargados de aplicar la ley para evitar una sanción.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Exigir el cumplimiento de tus derechos sin violentar los derechos de los otros.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Cuando cometes alguna falta acusar a otro para evitar la consecuencia.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Que acciones o actitudes debes evitar para favorecer la justicia y equidad."
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "Cada niño tiene derecho a expresar libremente sus opiniones sobre los asuntos que tengan que ver con su vida.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cada niño es libre de tener una opinión de acuerdo a sus emociones, capacidades reflexivas y conocimientos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Los niños tienen derecho a reunirse para ponerse de acuerdo y participar en temas que tienen que ver directamente con sus drechos y su bienestar",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "La libertad de expresión"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "La libertad de opinión"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "La libertad de asociación"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>personas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>natural<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>impulsos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>perder<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>dañarnos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>capacidad<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>desarrolla<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Todas las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; nos enojamos, eso es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y saludable. Sin embargo debemos de aprender a canalizar el enojo y controlar nuestros <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; para evitar <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; el control y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; a nosotros mismos o a otros. A esta <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;  de auto controlarnos se le llama y se autorregulación y se &nbsp;<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    }
];