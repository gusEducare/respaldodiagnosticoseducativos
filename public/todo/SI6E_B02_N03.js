json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Se utilizan para expresar una acción general, no se conjuga y usan las terminaciones:ir, er y ar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Señalan el orden en que suceden las acciones.(antes , después, primeramente, porteriormente, finalmente, entre otras).",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se usa para dar una orden, instrucción o expresar alguna voluntad, sólo puede decirse en tiempo presente.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Se refieren a la cantidad o intensidad que lleva la acción (bastante, menos, mucho, casi, nada, tanto).",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Indican relaciones numéricas, Pueden ser:<br>*Cardinales, que indican cantidad: una, dos, tres.<br>Múltiplos, indican el número de veces: doble, triple, cuádruple.<br>Partitivos; indican divisiones: medios, tercio.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Señalan como se realizan las acciones: bien, mal, regular, peor, fuertemente, rápidamente, etcétera.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Modo infinitivo"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Adverbios de orden"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Modo imperativo"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Adverbios de cantidad"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Adjetivos numerales"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Adverbios de modo"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona ambas columnas con información de los  de textos instructivos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Sus temas siempre son científicos o tecnológicos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Recolectan información de diversas fuentes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son textos informativos que desarrollan extensamente  un tema particular.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Su función es enteramente recreativa.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Su difusión puede ser a través de periódicos, revistas, radio, televisión o internet, entre otros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Jamás acude a entrevistas para su desarrollo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Usualmente contienen información recolectada a través de entrevistas o declaraciones.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Consideran una gran cantidad de temas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La información que recaban es siempre antigua. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pueden contener información tanto actual, como investigaciones previas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Su difusión es únicamente a través de medios impresos.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las opciones que señalan características de los reportajes. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Palabras que ayudan a catalogar, definir, clasificar y describir.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Figura literaria en la que se asignan cualidades propias de un elemento a otro sin estén relacionados ni que se mencionen explícitamente.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Comparación en la cual se emplea un nexo para relacionar dos elementos que tienen cierta similitud.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Adjetivos"
                
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Metáfora"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Símil"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona ambas columnas. "
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELo que a mí me parezca más importante es lo relevante.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELo irrelevante siempre es lo más aburrido.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELo relevante es la información que se repite en varias fuentes.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELo que responda a preguntas guía será información relevante (qué, cómo, dónde, cuándo, por qué).\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Señala la opción que te puede guiar a distinguir información relevante de la irrelevante en una investigación. "
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Evitar presentar toda la información de una sola vez.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Narrar los hechos en un orden distinto al que sucedieron o de manera desordenada.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Describir seres fantásticos y con valores universales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Incluir hechos sobrenaturales en el relato.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Describir con detalle aquellas situaciones que pongan en peligro el bienestar de las personas o que refieran dolor. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Incluir personajes fantásticos.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Identifica  y arrastra a cada contenedor los recursos que puedes emplear para crear atmósferas de terror y/o  misterio. ",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Misterio",
            "Terror"
        ]
    }
];