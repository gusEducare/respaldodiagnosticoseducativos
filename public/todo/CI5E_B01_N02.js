json=[   
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "La adolescencia conlleva exclusivamente cambios físicos entre los 10 y los 19 años.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Es común que las niñas inicien su desarrollo antes que los niños.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "No hay una edad precisa para el término de la adolescencia, pero puede decirse que es cuando se admite como adulto a la persona.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Todas las personas compartimos claros ejemplos, donde nuestro crecimiento es totalmente similar, por lo que crecemos de la misma forma y al mismo tiempo.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Los seres humanos somos únicos pero existen factores como el entorno que nos hacen iguales ante dichas circunstancias. ",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
		"descripcion": "Cuestiones",   
		"variante": "editable",
		"anchoColumnaPreguntas": 60,
		"evaluable"  : true
	}        
},
{  
	"respuestas": [
	{
		"t13respuesta":"<p><img src=\"ci5e_b01_n02_01.png\"></p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta":"<p><img src=\"ci5e_b01_n02_02.png\"></p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta":"<p><img src=\"ci5e_b01_n02_03.png\"></p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta":"<p><img src=\"ci5e_b01_n02_04.png\"></p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta":"<p><img src=\"ci5e_b01_n02_05.png\"></p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta":"<p><img src=\"ci5e_b01_n02_06.png\"></p>",
		"t17correcta": "1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "Observa la imagen y elige si representa ideas de diversidad o de intolerancia:",
		"tipo": "horizontal"
	},
	"contenedores":[ 
	"Diversidad",
	"Intolerancia"
	]
},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Los aspectos más visibles del crecimiento son el peso y la talla, sin embargo no son los únicos, todo nuestro cuerpo crece se desarrolla y permanece en cambio constante.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Todos los seres humanos deben crecer al mismo ritmo.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Existen varios factores, familiares genéticos y hasta sociales que modifican los momentos de desarrollo y crecimiento de cada persona.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Está perfectamente bien desarrollarse más rápido o más lento en ciertos momentos, cada uno de nosotros es único e irrepetible.",
		"correcta"  : "1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Elige falso o verdadero según consideres.<\/p>",
		"descripcion": "Cuestiones",   
		"variante": "editable",
		"anchoColumnaPreguntas": 60,
		"evaluable"  : true
	}        
}, 
{ 
	"respuestas": [
	{
		"t13respuesta": "<p>Cuando piensas que puedes fumar o tomar alcohol porque no pasa nada, sí lo haces.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Cuando estudias otro idioma porque te gusta y te ayudará en el futuro.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Cuando te informas sobre las consecuencias al tener relaciones sexuales sin protección.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Cuando no tomas los apuntes de las clases que te parecen aburridas o no haces las tareas.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Cuando comes todo lo que te gusta sin importar si es bueno o malo para tu salud.<\/p>",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "2",
		"t11pregunta": "Selecciona las opciones que muestran responsabilidad."
	}
},
{ 
	"respuestas": [
	{
		"t13respuesta": "Este elemento se forma con las experiencias del día a día, la cultura, el contexto, etc.",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Nacemos con ésto; es algo que está inscrito en nuestro código genético.",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "Conjunto de rasgos que hacen de un individu único, original, distinto e irrepetible.",
		"t17correcta": "3"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Carácter"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Temperamento"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Personalidad"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Une los conceptos con su definición."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>Usar bloqueador solar.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Conseguir muchos videojuegos.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Consumir agua natural todos los días.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Comer dulces tres o más veces al día.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Elegir una actitud positiva ante la vida.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Cepillar tus dientes.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Asistir al cine cada semana.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Rodearte de amigos con quienes sabes que cuentas.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Cuidar el planeta.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Tener la seguridad de que eres amado.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Apoyar a tu familia en sus metas.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Ayudar a otros siempre que te sea posible.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Tomar decisiones.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Bañarte diario.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Jugar con tus mascotas.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Alimentarte sanamente.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Hacer ejercicio.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Desarrollar tus capacidades y habilidades.<\/p>",
		"t17correcta": "1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "2",
		"t11pregunta": "Selecciona todas aquellas acciones que puedes realizar para autocuidarte. "
	}
},
{ 
	"respuestas":[ 
	{
		"t13respuesta": "Anorexia",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Bulimia",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Obesidad/Sobrepeso",
		"t17correcta": "2"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Enfermedad que puede llevar a la muerte ya que mantiene el peso por debajo de los límites soportados por el cuerpo.",
		"valores": [' ', ' ', ' '],
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Su causa es debido a un desequilibrio energético entre calorías consumidas y gastadas.",
		"valores": [' ', ' ', ' '],
		"correcta"  : "2"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "No hay una sola causa para esta enfermedad. Viene acompañada de la culpa por los excesos que se producen con los atracones.",
		"valores": [' ', ' ', ' '],
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Trastorno alimenticio que se caracteriza por una ingesta muy baja o limitada de alimentos.",
		"valores": [' ', ' ', ' '],
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Se tiene miedo de engordar y se deja de comer.",
		"valores": [' ', ' ', ' '],
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Acumulación anormal o excesiva de grasa que puede ser perjudicial para la salud.",
		"valores": [' ', ' ', ' '],
		"correcta"  : "2"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Relaciona los siguientes conceptos relacionados con los trastornos alimentarios.<\/p>",
		"descripcion": "Cuestiones",   
		"variante": "editable",
		"anchoColumnaPreguntas": 40,
		"evaluable"  : true
	}
},
{
	"respuestas":[ 
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Si consumes alcohol o alguna droga antes de los 18 años, es probable que continúes con su consumo cuando seas adulto.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "En algunos casos, ciertas drogas pueden ayudarte a relacionarte mejor con tus familiares.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Puedes desarrollar enfermedades cardiovasculares, al utilizar drogas o fumar tabaco.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "El consumo de tabaco puede desarrollar cáncer.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "El consumo de drogas o tabaco te ayuda a ser más creativo.",
		"correcta"  : "0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
		"descripcion": "Cuestiones",   
		"variante": "editable",
		"anchoColumnaPreguntas": 60,
		"evaluable"  : true
	}
},
{ 
	"respuestas": [
	{
		"t13respuesta": "<p>Haces ejercicio buscando aumentar tu musculatura con sustancias.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Consumir alcohol ocasionalmente aunque no seas mayor de edad.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Te alejas de personas que te presionan para fumar.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Buscas actividades que te permitan relajarte.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Consumes alimentos variados que te permitan una dieta balanceada.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Pruebas alguna droga que  no te parece peligrosa.<\/p>",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "2",
		"t11pregunta": "Selecciona las acciones que contribuyen a una vida saludable que pueda alejarte de las adicciones."
	}
},

{ 
	"respuestas":[  
	{  
		"t13respuesta":"Alejarte rápidamente de la situación; no sea que se aburran con ella y te molesten a ti.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Hacer sonar la alarma escolar; así se distraen y la dejan en paz.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Ir a la dirección en busca de un profesor.",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Levantar la voz y decir a ese grupo d eniños y niñas que lo que hace es incorrecto y que no tienen derecho a violentar a otra persona.",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Buscar otro grupo de compañeros para enfrentar a los violentos.",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Imagina que a la hora del recreo observas que un grupo de niños y niñas se burlan de una compañera por sus rasgos físicos. No ves ningún profesor o adulto cerca de la situación. Elige de entre las opciones la que favorezca el respeto a la compañera que está sufriendo este tipo de violencia."
	}
}
];