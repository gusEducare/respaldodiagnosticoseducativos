json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Doy mi opinión respecto al trabajo infantil.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Respetar su derecho al expresar su opinión en contra de la mía.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Donde inicia de la  libertad de otro.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ejerzo mi libertad cuando…"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Respeto la libertad de otros al …"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mi libertad termina…"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas completando la frase."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es necesario no  distinguir nuestras emociones y reaccionar rápidamente ante lo que nos hagan sentir.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando reprimes una emoción negativa, puede perjudicarte y prolongar el malestar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando modulas y expresas adecuadamente una emoción, podemos llamarlo autocontrol emocional.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },   
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Hacer alto en los semáforos en rojo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Agredir a los niños menores<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Patear un perro callejero<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ofender a los limpiaparabrisas <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Regar los árboles de la calle<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Evitar la violencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Burlarte de aquellos que se vean distintos a ti<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige ejemplos que puedan representar algo injusto."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada  contenedor las imágenes que corresponden.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Comercio Justo",
            "Comercio convencional"
        ]
    },
        {  
      "respuestas":[
         {  
            "t13respuesta":"<p>1955</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>1946</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>1948</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>1985</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Saber a quién de mis amigos les afectará.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Analizar hasta donde me sea posible las consecuencias.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Saber el costo que tiempo que invertiré en decidir.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Preguntarle a todas las personas que conozco.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Valor universal</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Valor espiritual</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Bondad</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Igualdad</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["¿En qué año dentro de la ONU se aprobó la Declaración Universal de los Derechos Humanos?",
                         "¿Qué debo hacer antes de tomar una decisión?",
                         "El respeto a la vida, es un ejemplo de"],
         "preguntasMultiples": true,
         "columnas":1
      }
   }
]
