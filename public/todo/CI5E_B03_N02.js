[
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala cuales de las siguientes imágenes refieren a una buena convivencia."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Bañarme solo.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Administrar los recursos del país y garantizar mis derechos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Preparar mis alimentos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Transportar mis alimentos desde el lugar donde proceden.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Enseñarme los valores que mi familia privilegia.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Limpiar la vía pública.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Comunicarme con mis amigos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Proporcionarme todo lo necesario para que asista al colegio.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Pagar por mis alimentos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Brindarme la seguridad de una casa.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Sembrar y cosechar todos los alimentos que he comido a lo largo de mi vida.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ayudarme a aprender académicamente.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas actividades que otros realizan para que tú puedas desarrollarte como un ser humano pleno."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Arrastra al contenedor las imágenes que muestren inequidad social"
        }
    },
        {
            "respuestas": [
                {
                    "t13respuesta": "<p>reciprocidad<\/p>",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "<p>correspondencia<\/p>",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "<p>mutua<\/p>",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "<p>compensa<\/p>",
                    "t17correcta": "4"
                },
                {
                    "t13respuesta": "<p>valores<\/p>",
                    "t17correcta": "5"
                },
                {
                    "t13respuesta": "<p>solidario<\/p>",
                    "t17correcta": "6"
                }
            ],
            "preguntas": [
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<p>La palabra <\/p>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<p>&nbsp; proviene del latín y significa <\/p>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<p>&nbsp;  <\/p>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<p>&nbsp; a una persona o cosa con otra. Cuando realizamos algo y se nos devuelve, <\/p>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<p>&nbsp; o restituye se da una relación de reciprocidad. Implícitamente va acompañada de <\/p>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<p>&nbsp; ya que una persona que realmente busca corresponder al otro; es decir, es <\/p>"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Completa el siguiente p&aacute;rrafo."
            }
        },
          {  
      "respuestas":[  
         {  
            "t13respuesta":"Condición de salud",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Creencias religiosas",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Uso de regionalismos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Edad",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Preferencias sexuales",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tipo de vehículo familiar",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Condición socioeconómica",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Discapacidad",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Riqueza",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Origen étnico",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Colegio",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Embarazo",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tipo de alimentación",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Genero",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Señalan todas las que situaciones que pueden dar origen a discriminación."
      }
   },
          {  
      "respuestas":[  
         
         {  
            "t13respuesta":"Cambio climático",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Fenómeno del niño",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Eutrofización",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Sobreexplotación",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Señalan todas las que situaciones que pueden dar origen a discriminación."
      }
   },
  {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser diferente no nos hace ni mejor ni peor, simplemente nos define como seres únicos",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser diferente es lo peor que te puede pasar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las diferencias culturales enriquecen nuestra sociedad.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El respeto sólo es para los que me caen bien",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos tenemos distintas opiniones y cada una de ellas es tan valiosa como las otras.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El aprendizaje y la tolerancia también se ejercitan a partir de las diferencias con otros.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Cerrando la llave del agua al lavarme los dientes.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Encendiendo todas las luces de casa al anochecer.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Reciclando, reusando y reduciendo mi consumo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Usando menos el automóvil.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Eligiendo alimentos producidos cerca de mi ciudad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comprando únicamente lo hecho en china",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Utilizando las aguas grises de la casa para regar plantas",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Elige todas las formas en que puedes colaborar en familia para crear un medio ambiente más sano y conservar los recursos naturales del país."
      }
   }
]