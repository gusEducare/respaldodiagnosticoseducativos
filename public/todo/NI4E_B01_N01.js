json = [
{
	"respuestas": [
	{
		"t13respuesta": "fibra",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "salud",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "frutas",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "energía",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "lácteos",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "cárnicos",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "verduras",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "leguminosas",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "desarrollo",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "crecimiento",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "minerales",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "proteínas",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "fortaleza",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "nutrición",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "vitaminas",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "16",
		"t11medida": 16,
		"t11pregunta": "Realiza la siguiente sopa de letras."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Insípida",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Inocua",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Incomible",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Inodora",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Inédita",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Incolora",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Intuba",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "2",
		"t11pregunta": "Las características del agua potable son:"
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "El agua no es un alimento",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "El agua sirve para mantener nuestra temperatura",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "El agua colabora en realizar la digestión de los alimentos",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Es seguro beber agua de las fuentes de agua natural sin antes potabilizarla",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Toda el agua clara es potable",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Los ríos, lagos, presas y depósitos subterráneos, tienen de manera natural microorganismos que pueden ocasionar enfermedades",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Selecciona la respuesta correcta.",
		"variante": "editable",
		"anchoColumnaPreguntas": 40,
		"evaluable"  : true
	}        
},


{  
	"respuestas":[  
	{  
		"t13respuesta":"Complejo B",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina C",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Vitamina A",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Zinc",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Calcio",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Hierro",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Es antibacteriana, por lo que inhibe el crecimiento de ciertas bacterias dañinas para el organismo. Tiene propiedades antihistamínicas. Ayuda a prevenir o mejorar afecciones de la piel como eccemas o soriasis."
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"Complejo B",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Vitamina C",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina A",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Zinc",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Calcio",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Hierro",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Aglutina todas las vitaminas del tipo B Es necesario para el correcto funcionamiento de casi todos los procesos en el cuerpo: Producción de energía, favorece un buen sistema nervioso saludable, provoca Buena digestión. Beneficios para la piel, pelo y uñas, Beneficios del complejo B para la Memoria."
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"Complejo B",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina C",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina A",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Zinc",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Calcio",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Hierro",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Mineral esencial para la formación del esqueleto del cuerpo, siendo muy importante que sus necesidades básicas estén cubiertas durante la infancia y adolescencia."
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"Complejo B",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina C",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina A",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Zinc",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Calcio",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Hierro",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Favorece la visión. Favorece el crecimiento y desarrollo de huesos, desarrollo celular y ayuda al sistema inmune."
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"Complejo B",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina C",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina A",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Zinc",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Calcio",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Hierro",
		"t17correcta":"1"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Interviene en el transporte de oxígeno y dióxido de carbono en sangre. Participa en la producción de elementos de la sangre como por ejemplo la hemoglobina."
	}
},
{  
	"respuestas":[  
	{  
		"t13respuesta":"Complejo B",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina C",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Vitamina A",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Zinc",
		"t17correcta":"1"
	},
	{  
		"t13respuesta":"Calcio",
		"t17correcta":"0"
	},
	{  
		"t13respuesta":"Hierro",
		"t17correcta":"0"
	}
	],
	"pregunta":{  
		"c03id_tipo_pregunta":"1",
		"t11pregunta":"Participa en múltiples reacciones químicas y en el sistema inmune (defensa del organismo), ya que favorece la producción de linfocitos. Ayuda en la cicatrización de heridas. Interviene en la síntesis de ADN y ARN. Produce la activación de ciertas hormonas.Colabora en el mantenimiento de la estructura de las células."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>Crecimiento de vello</p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Aparición de acné.</p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Aparición de acné.</p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Cambio en el timbre de voz.</p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Aumento de sudoración y olor corporal.</p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Inicio de menstruación</p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Crecimiento de vello púbico.</p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Crecimiento de vello púbico.</p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Crecimiento de testículos y pene.</p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Crecimiento de glándulas mamarias.</p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Acumulación de grasa en zonas determinadas.</p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Mayor desarrollo de musculatura.</p>",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "Arrastra los cambios que ocurren durante la pubertad a su contenedor correcto.",
		"tipo": "horizontal"
	},
	"contenedores":[ 
	"Mujeres",
	"Hombres"
	]
}
]