json = [            
	{//1
		"respuestas": [
		{
			"t13respuesta": "<p>cincuenta<\/p>",
			"t17correcta": "1"
		},
		{
			"t13respuesta": "<p>y<\/p>",
			"t17correcta": "2,7"
		},
		{
			"t13respuesta": "<p>seis<\/p>",
			"t17correcta": "3"
		},
		{
			"t13respuesta": "<p>mil<\/p>",
			"t17correcta": "4"
		},
		{
			"t13respuesta": "<p>ochocientos<\/p>",
			"t17correcta": "5"
		},
		{
			"t13respuesta": "<p>noventa<\/p>",
			"t17correcta": "6"
		},
		{
			"t13respuesta": "<p>y<\/p>",
			"t17correcta": "7,2"
		},
		{
			"t13respuesta": "<p>cuatro<\/p>",
			"t17correcta": "8"
		},
		{
			"t13respuesta": "<p>quinientos<\/p>",
			"t17correcta": "0"
		},
		{
			"t13respuesta": "<p>cinco<\/p>",
			"t17correcta": "0"
		},
		{
			"t13respuesta": "<p>nueve<\/p>",
			"t17correcta": "0"
		},
		{
			"t13respuesta": "<p>seiscientos<\/p>",
			"t17correcta": "0"
		}
		],
		"preguntas": [
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><\/p>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><\/p>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><\/p>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><\/p>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><\/p>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><br><\/p>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><\/p>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<p><\/p>"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"respuestasLargas":true,
			"soloTexto":true,
			"contieneDistractores":true,
			"anchoRespuestas":120,
			"t11pregunta": "Coloca las palabras que forman el número 56894."
		}
	},
	{//2
		"respuestas": [
		{
			"t13respuesta": "<p>59236451<\/p>",
			"t17correcta": "0"
		},
		{
			"t13respuesta": "<p>3460451<\/p>",
			"t17correcta": "1"
		},
		{
			"t13respuesta": "<p>3406451<\/p>",
			"t17correcta": "2"
		},
		{
			"t13respuesta": "<p>875321<\/p>",
			"t17correcta": "3"
		},
		{
			"t13respuesta": "<p>875123<\/p>",
			"t17correcta": "4"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p>Ordena los números de mayor a menor.<br><\/p>",
			"tipo": "ordenar"
		},
		"contenedores":[
		{"Contenedor": ["Mayor", "201,15", "cuadrado", "134, 73", "."]},
		{"Contenedor": ["", "201,149", "cuadrado", "134, 73", "."]},
		{"Contenedor": ["", "201,283", "cuadrado", "134, 73", "."]},
		{"Contenedor": ["", "201,417", "cuadrado", "134, 73", "."]},
		{"Contenedor": ["Menor", "201,551", "cuadrado", "134, 73", "."]}
		]
	},
	{
		"respuestas": [
		{
			"t13respuesta": "<p>1200</p>",
			"t17correcta": "0",
			"columna":"0"
		},
		{
			"t13respuesta": "<p>7500</p>",
			"t17correcta": "1",
			"columna":"0"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p>Coloca los números que corresponden en los espacios señalados.<br><\/p>",
			"tipo": "ordenar",
			"imagen": true,
			"url":"mi6e_b01_n02_01.png",
			"respuestaImagen":true, 
			"bloques":false,
			"borde":false,
			"tamanyoReal":true,
			"opcionesTexto":true
		},
		"contenedores": [
		{"Contenedor": ["", "150,56", "cuadrado", "72, 49", ".","transparent"]},
		{"Contenedor": ["", "249,561", "cuadrado", "72, 49", ".","transparent"]}
		]
	},
	   {//4
	   	"respuestas": [
	   	{
	   		"t13respuesta": "Opción 1",
	   		"t17correcta": "0"
	   	},
	   	{
	   		"t13respuesta": "Opción 2",
	   		"t17correcta": "1"
	   	}
	   	],
	   	"preguntas" : [
	   	{
	   		"c03id_tipo_pregunta": "13",
	   		"t11pregunta": "<img src=\"MI6E_B01_N02_02_01.png\"> | <img src=\"MI6E_B01_N02_02_02.png\">",
	   		"valores": ['&nbsp;', '&nbsp;', '&nbsp;'],
	   		"correcta"  : "0"
	   	},
	   	{
	   		"c03id_tipo_pregunta": "13",
	   		"t11pregunta": "<img src=\"MI6E_B01_N02_02_03.png\"> | <img src=\"MI6E_B01_N02_02_04.png\">",
	   		"valores": ['&nbsp;', '&nbsp;', '&nbsp;'],
	   		"correcta"  : "1"
	   	},
	   	{
	   		"c03id_tipo_pregunta": "13",
	   		"t11pregunta": "<img src=\"MI6E_B01_N02_02_05.png\"> | <img src=\"MI6E_B01_N02_02_06.png\">",
	   		"valores": ['&nbsp;', '&nbsp;', '&nbsp;'],
	   		"correcta"  : "1"
	   	},
	   	{
	   		"c03id_tipo_pregunta": "13",
	   		"t11pregunta": "<img src=\"MI6E_B01_N02_02_07.png\"> | <img src=\"MI6E_B01_N02_02_08.png\">",
	   		"valores": ['&nbsp;', '&nbsp;', '&nbsp;'],
	   		"correcta"  : "0"
	   	}
	   	],
	   	"pregunta": {
	   		"c03id_tipo_pregunta": "13",
	   		"t11pregunta": "<p>Selecciona la fracción más chica.<\/p>",
	   		"descripcion": "Opción 1 | Opción 2",   
	   		"variante": "editable",
	   		"anchoColumnaPreguntas": 30,
	   		"evaluable"  : true
	   	}        
	   },
	{//5
		"respuestas": [
		{
			"t13respuesta": "mi6e_b01_n02_03_01a.png",
			"t17correcta": "0",
			"columna":"0"
		},
		{
			"t13respuesta": "mi6e_b01_n02_03_01h.png",
			"t17correcta": "1",
			"columna":"0"
		},
		{
			"t13respuesta": "mi6e_b01_n02_03_02d.png",
			"t17correcta": "2",
			"columna":"0"
		},
		{
			"t13respuesta": "mi6e_b01_n02_03_02f.png",
			"t17correcta": "3",
			"columna":"0"
		},
		{
			"t13respuesta": "mi6e_b01_n02_03_03d.png",
			"t17correcta": "4",
			"columna":"0"
		},
		{
			"t13respuesta": "mi6e_b01_n02_03_03e.png",
			"t17correcta": "5",
			"columna":"0"
		},

		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p>Coloca las fracciones equivalentes que faciliten realizar la operación entre las fracciones.<br><\/p>",
			"tipo": "ordenar",
			"imagen": true,
			"url":"mi6e_b01_n02_03_01_02_03.png",
			"respuestaImagen":true, 
			"bloques":false,
			"contieneDistractores":true,
			"tamanyoReal":true,
			"opcionesTexto":true,
			"borde":true
		},
		"contenedores": [
		{"Contenedor": ["", "64,358", "cuadrado", "50,74", ".","transparent"]},
		{"Contenedor": ["", "64,462", "cuadrado", "50,74", ".","transparent"]},
		{"Contenedor": ["", "188,358", "cuadrado", "50,74", ".","transparent"]},
		{"Contenedor": ["", "188,462", "cuadrado", "50,74", ".","transparent"]},
		{"Contenedor": ["", "314,358", "cuadrado", "50,74", ".","transparent"]},
		{"Contenedor": ["", "314,462", "cuadrado", "50,74", ".","transparent"]}
		]
	},
  {//6
  	"respuestas": [
  	{
  		"t13respuesta": "mi6e_b01_n02_04_02.png",
  		"t17correcta": "0",
  		"columna":"0"
  	},
  	{
  		"t13respuesta": "mi6e_b01_n02_04_03.png",
  		"t17correcta": "1",
  		"columna":"0"
  	},
  	{
  		"t13respuesta": "mi6e_b01_n02_04_04.png",
  		"t17correcta": "2",
  		"columna":"0"
  	},
  	{
  		"t13respuesta": "mi6e_b01_n02_04_05.png",
  		"t17correcta": "3",
  		"columna":"0"
  	}
  	],
  	"pregunta": {
  		"c03id_tipo_pregunta": "5",
  		"t11pregunta": "<p>Arrastra las fracciones que resuelven el problema.<br><\/p>",
  		"tipo": "ordenar",
  		"imagen": true,
  		"url":"mi6e_b01_n02_04.png",
  		"respuestaImagen":true, 
  		"bloques":false,
  		"tamanyoReal":true,
  		"opcionesTexto":true,
  		"borde":true

  	},
  	"contenedores": [
  	{"Contenedor": ["", "110,450", "cuadrado", "54, 78", ".","transparent"]},
  	{"Contenedor": ["", "200,207", "cuadrado", "54, 78", ".","transparent"]},
  	{"Contenedor": ["", "495,205", "cuadrado", "54, 78", ".","transparent"]},
  	{"Contenedor": ["", "400,320", "cuadrado", "54, 78", ".","transparent"]}
  	]
  },
  {//7
  	"respuestas": [
  	{
  		"t13respuesta": "<p>28.01<\/p>",
  		"t17correcta": "1"
  	},
  	{
  		"t13respuesta": "<p>21.99<\/p>",
  		"t17correcta": "2"
  	}
  	],
  	"preguntas": [
  	{
  		"c03id_tipo_pregunta": "8",
  		"t11pregunta": "<p>Vidal puso 5 litros de gasolina a su automóvil, cada litro le rinde 10 km. Vidal ha recorrido 14.82 km al visitar a su hermana y 13.19 km al visitar a su tío. ¿Qué distancia puede recorrer el auto de Vidal con la gasolina que aún tiene? <br> • En total ha recorrido:&nbsp;<\/p>"
  	},
  	{
  		"c03id_tipo_pregunta": "8",
  		"t11pregunta": "<p>&nbsp;km <br>• Por lo que aún puede recorrer:&nbsp;<\/p>"
  	},
  	{
  		"c03id_tipo_pregunta": "8",
  		"t11pregunta": "<p>&nbsp;km<\/p>"
  	}
  	],
  	"pregunta": {
  		"c03id_tipo_pregunta": "8",
  		"pintaUltimaCaja": false,
  		"t11pregunta": "Completa el siguiente p&aacute;rrafo."
  	}
  },
	{//8
		"respuestas": [
		{
			"t13respuesta": "<p>1200</p>",
			"t17correcta": "0",
			"columna":"0"
		},
		{
			"t13respuesta": "<p>1350</p>",
			"t17correcta": "1",
			"columna":"0"
		},
		{
			"t13respuesta": "<p>1600</p>",
			"t17correcta": "2",
			"columna":"1"
		},
		{
			"t13respuesta": "<p>950</p>",
			"t17correcta": "3",
			"columna":"1"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p><br><\/p>",
			"tipo": "ordenar",
			"imagen": true,
			"url":"mi6e_b01_n02_05.png",
			"respuestaImagen":true, 
			"bloques":false,
			"tamanyoReal":true,
			"opcionesTexto":true,
			"borde":false
		},
		"contenedores": [
		{"Contenedor": ["", "163,470", "cuadrado", "108, 40", ".","transparent"]},
		{"Contenedor": ["", "236,467", "cuadrado", "108, 40", ".","transparent"]},
		{"Contenedor": ["", "309,467", "cuadrado", "108, 40", ".","transparent"]},
		{"Contenedor": ["", "382,467", "cuadrado", "108, 40", ".","transparent"]}
		]
	},
  {//9
  	"respuestas": [
  	{
  		"t13respuesta": "<p>540</p>",
  		"t17correcta": "0",
  		"columna":"0"
  	},
  	{
  		"t13respuesta": "<p>810</p>",
  		"t17correcta": "1",
  		"columna":"0"
  	},
  	{
  		"t13respuesta": "<p>1116</p>",
  		"t17correcta": "2",
  		"columna":"1"
  	},
  	{
  		"t13respuesta": "<p>324</p>",
  		"t17correcta": "3",
  		"columna":"1"
  	}
  	],
  	"pregunta": {
  		"c03id_tipo_pregunta": "5",
  		"t11pregunta": "<p>Ximena utiliza el riego por goteo para mantener la humedad de sus hortalizas. Con esta técnica ocupa aproximadamente 360 litros por hora en 100 m. Arrastra la cantidad de agua que consume en 100 m.<br><\/p>",
  		"tipo": "ordenar",
  		"imagen": true,
  		"url":"MI6E_B01_N02_06_01.png",
  		"respuestaImagen":true, 
  		"bloques":false,
  		"opcionesTexto": true,	
  		"tamanyoReal": true,
  		"borde":false

  	},
  	"contenedores": [
  	{"Contenedor": ["", "49,303", "cuadrado", "150, 40", ".","transparent"]},
  	{"Contenedor": ["", "91,303", "cuadrado", "150, 40", ".","transparent"]},
  	{"Contenedor": ["", "133,303", "cuadrado", "150, 40", ".","transparent"]},
  	{"Contenedor": ["", "176,303", "cuadrado", "150, 40", ".","transparent"]}
  	]
  },
  {//10
  	"respuestas": [
  	{
  		"t13respuesta": "<p>60</p",
  		"t17correcta": "0",
  		"columna":"0"
  	},
  	{
  		"t13respuesta": "<p>12</p",
  		"t17correcta": "1",
  		"columna":"0"
  	},
  	{
  		"t13respuesta": "<p>50</p",
  		"t17correcta": "2",
  		"columna":"1"
  	},
  	{
  		"t13respuesta": "<p>55</p",
  		"t17correcta": "3",
  		"columna":"1"
  	}
  	],
  	"pregunta": {
  		"c03id_tipo_pregunta": "5",
  		"t11pregunta": "<p><br><\/p>",
  		"tipo": "ordenar",
  		"imagen": true,
  		"url":"MI6E_B01_N02_07_01.png",
  		"respuestaImagen":true, 
  		"bloques":false,
  		"opcionesTexto": true,	
  		"tamanyoReal": true,
  		"borde":false

  	},
  	"contenedores": [
  	{"Contenedor": ["", "237,497", "cuadrado", "78, 40", ".","transparent"]},
  	{"Contenedor": ["", "288,497", "cuadrado", "78, 40", ".","transparent"]},
  	{"Contenedor": ["", "338,497", "cuadrado", "78, 40", ".","transparent"]},
  	{"Contenedor": ["", "388,497", "cuadrado", "78, 40", ".","transparent"]}
  	]
  },
  {//11
  	"respuestas": [
  	{
  		"t13respuesta": "1 eje de simetría",
  		"t17correcta": "1"
  	},
  	{
  		"t13respuesta": "2 ejes de simetría",
  		"t17correcta": "2"
  	},
  	{
  		"t13respuesta": "3 ejes de simetría",
  		"t17correcta": "3"
  	},
  	{
  		"t13respuesta": "4 ejes de simetría",
  		"t17correcta": "4"
  	},
  	{
  		"t13respuesta": "5 ejes de simetría",
  		"t17correcta": "5"
  	}
  	],
  	"preguntas": [
  	{
  		"c03id_tipo_pregunta": "12",
  		"t11pregunta": "<img src='mi6e_b01_n02_08_a.png'>"
  	},
  	{
  		"c03id_tipo_pregunta": "12",
  		"t11pregunta": "<img src='mi6e_b01_n02_08_b.png'>"
  	},
  	{
  		"c03id_tipo_pregunta": "18",
  		"t11pregunta": "<img src='mi6e_b01_n02_08_c.png'>"
  	},
  	{
  		"c03id_tipo_pregunta": "12",
  		"t11pregunta": "<img src='mi6e_b01_n02_08_d.png'>"
  	},
  	{
  		"c03id_tipo_pregunta": "12",
  		"t11pregunta": "<img src='mi6e_b01_n02_08_e.png'>"
  	},
  	],
  	"pregunta": {
  		"c03id_tipo_pregunta": "18",
  		"t11pregunta": "Relaciona cada figura con la cantidad de ejes de simetría que tiene."
  	}
  },
	{//12
		"respuestas": [
		{
			"t13respuesta": "<p>C3<\/p>",
			"t17correcta": "1"
		},
		{
			"t13respuesta": "<p>H11<\/p>",
			"t17correcta": "2"
		},
		{
			"t13respuesta": "<p>H4<\/p>",
			"t17correcta": "3"
		},
		{
			"t13respuesta": "<p>L8<\/p>",
			"t17correcta": "4"
		},
		{
			"t13respuesta": "<p>L4<\/p>",
			"t17correcta": "5"
		}
		],
		"preguntas": [
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "<center><table style='background-color:#3D6F9C;'>\n\
			<tr>\n\
			<td width='120px' style='color:white; background-color:#3d85c6; text-align:center;  font-size:22px;'><b>Personas</b></td>\n\
			<td width='200px' style='color:white; background-color:#3d85c6; text-align:center;  font-size:22px;'><b>Asientos</b></td>\n\
			<td width='200px' style='color:white; background-color:#3d85c6; text-align:center;  font-size:22px;'><b>Pantalla</b></td>\n\
			</tr>\n\
			\n\
			<tr>\n\
			<td style='background-color:#cfe2f3; font-size:16px;'><img src=\"mi6e_b01_n02_09_01.png\"> Rogelio</td>\n\
			<td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>F10</td>\n\
			<td width='400px' rowspan='9' style='background-color:white; vertical-align:center; text-align:center;'>\n\
			<img style='width: 300px;' src=\"mi6e_b01_n02_09.png\">\n\
			</td>\n\
			</tr>\n\
			<tr>\n\
			<td style='background-color:white; font-size:16px;'><img src=\"mi6e_b01_n02_09_02.png\"> Beatriz</td>\n\
			<td style='background-color:white; text-align:center; font-size:16px;'>"
		},           
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "</td>\n\
			</tr>\n\
			<tr>\n\
			<td style='background-color:#cfe2f3; font-size:16px;'><img src=\"mi6e_b01_n02_09_03.png\"> Diana</td>\n\
			<td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "</td>\n\
			</tr>\n\
			<tr>\n\
			<td style='background-color:white; font-size:16px;'><img src=\"mi6e_b01_n02_09_04.png\"> Manuel</td>\n\
			<td style='background-color:white; text-align:center; font-size:16px;'>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "</td>\n\
			</tr>\n\
			<tr>\n\
			<td style='background-color:#cfe2f3; font-size:16px;'><img src=\"mi6e_b01_n02_09_05.png\"> Pedro</td>\n\
			<td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "</td>\n\
			</tr>\n\
			<tr>\n\
			<td style='background-color:white; font-size:16px;'><img src=\"mi6e_b01_n02_09_06.png\"> Rita</td>\n\
			<td style='background-color:white; text-align:center; font-size:16px;'>"
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "</td>\n\
			</tr>\n\
			</table></center>"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"pintaUltimaCaja":false,
			"respuestasLargas":true,  
			"anchoRespuestas":150,
			"soloTexto":true,
			"ocultaPuntoFinal":true,
			"t11pregunta": "Observa la imagen y determina qué asientos ocuparon en el cine las personas que se indican."
		}
	},
  {  //13
  	"respuestas":[
  	{  
  		"t13respuesta":"<p>300 m</p>",
  		"t17correcta":"1",
  		"numeroPregunta":"0"
  	},
  	{  
  		"t13respuesta":"<p>2500 m</p>",
  		"t17correcta":"0",
  		"numeroPregunta":"0"
  	},
  	{  
  		"t13respuesta":"<p>1100 m</p>",
  		"t17correcta":"0",
  		"numeroPregunta":"0"
  	}, 
  	{  
  		"t13respuesta":"<p>300 m</p>",
  		"t17correcta":"0",
  		"numeroPregunta":"1"
  	},
  	{  
  		"t13respuesta":"<p>2500 m</p>",
  		"t17correcta":"0",
  		"numeroPregunta":"1"
  	},
  	{  
  		"t13respuesta":"<p>1100 m</p>",
  		"t17correcta":"1",
  		"numeroPregunta":"1"
  	},
  	{  
  		"t13respuesta":"<p>300 m</p>",
  		"t17correcta":"0",
  		"numeroPregunta":"2"
  	},
  	{  
  		"t13respuesta":"<p>2500 m</p>",
  		"t17correcta":"0",
  		"numeroPregunta":"2"
  	},
  	{  
  		"t13respuesta":"<p>1100 m</p>",
  		"t17correcta":"1",
  		"numeroPregunta":"2"
  	}
  	],
  	"pregunta":{  
  		"c03id_tipo_pregunta":"2",
  		"t11pregunta": ["<img style='width: 430px;' src=\"mi6e_b01_n02_10.png\"><br>¿Cuál es la distancia aproximada entre la Biblioteca Central y la Facultad de Arquitectura?","¿Cuál es la distancia aproximada entre el Estadio Olímpico y el Universum?","¿Cuál es la distancia aproximada entre la Facultad de ciencias políticas y la estación del metro Universidad?"],
  		"preguntasMultiples": true,
  		"columnas":2
  	}
  },
   {  //14
   	"respuestas":[
   	{  
   		"t13respuesta":"<p>$110</p>",
   		"t17correcta":"1",
   		"numeroPregunta":"0"
   	},
   	{  
   		"t13respuesta":"<p>$50</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"0"
   	},
   	{  
   		"t13respuesta":"<p>$55</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"0"
   	}, 
   	{  
   		"t13respuesta":"<p>8 g</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"1"
   	},
   	{  
   		"t13respuesta":"<p>80 g</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"1"
   	},
   	{  
   		"t13respuesta":"<p>800 g</p>",
   		"t17correcta":"1",
   		"numeroPregunta":"1"
   	},
   	{  
   		"t13respuesta":"<p>75 ml</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"2"
   	},
   	{  
   		"t13respuesta":"<p>750 ml</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"2"
   	},
   	{  
   		"t13respuesta":"<p>7500 ml</p>",
   		"t17correcta":"1",
   		"numeroPregunta":"2"
   	},
   	{  
   		"t13respuesta":"<p>15 minutos</p>",
   		"t17correcta":"1",
   		"numeroPregunta":"3"
   	},
   	{  
   		"t13respuesta":"<p>25 minutos</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"3"
   	},
   	{  
   		"t13respuesta":"<p>30 minutos</p>",
   		"t17correcta":"0",
   		"numeroPregunta":"3"
   	}
   	],
   	"pregunta":{  
   		"c03id_tipo_pregunta":"2",
   		"t11pregunta": ["El 50% de 220","El 10% de 8 kg","El 75% de 10 L","El 25% de 1 h"],
   		"preguntasMultiples": true,
   		"columnas":2
   	}
   },
   {
   	"respuestas": [
   	{
   		"t13respuesta": "<p>360<\/p>",
   		"t17correcta": "1"
   	},
   	{
   		"t13respuesta": "<p>264<\/p>",
   		"t17correcta": "2"
   	},
   	{
   		"t13respuesta": "<p>300<\/p>",
   		"t17correcta": "3"
   	},
   	{
   		"t13respuesta": "<p>420<\/p>",
   		"t17correcta": "4"
   	}
   	],
   	"preguntas": [
   	{
   		"c03id_tipo_pregunta": "8",
   		"t11pregunta": "<p>150% = <\/p>"
   	},
   	{
   		"c03id_tipo_pregunta": "8",
   		"t11pregunta": "<p>&nbspm<br>110% = <\/p>"
   	},
   	{
   		"c03id_tipo_pregunta": "8",
   		"t11pregunta": "<p>&nbspm<br>125% = <\/p>"
   	},
   	{
   		"c03id_tipo_pregunta": "8",
   		"t11pregunta": "<p>&nbspm<br>175% = <\/p>"
   	},
   	{
   		"c03id_tipo_pregunta": "8",
   		"t11pregunta": "<p>&nbspm<\/p>"
   	}
   	],
   	"pregunta": {
   		"c03id_tipo_pregunta": "8",
   		"pintaUltimaCaja":false,
   		"respuestasLargas":true,
   		"anchoRespuestas":80,
   		"ocultaPuntoFinal":true,
   		"t11pregunta": "Escribe los siguientes porcentajes de 240 m."
   	}
   }
   ]