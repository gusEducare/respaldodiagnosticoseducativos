json = [
  {
    "respuestas": [
        {
            "t13respuesta": "Puedes mejorar tus modales en la mesa.",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Te permite desarrollar facultades como la inteligencia, la voluntad, la libertad y la individualidad.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "A partir de la convivencia, puedes aprender y conocer formas de pensar que son diferentes a la tuya.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "La diversidad social, sólo puedes observarla dentro del ámbito escolar.",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "La Constitución Política de México hace referencia a la educación como un derecho ciudadano que debe tener un costo para ser apreciada.",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "La educación preescolar, primaria y secundaria, conforman la educación básica y es obligatoria.",
            "t17correcta": "1"
        }
    ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las oraciones que hablen de la importancia de asistir a la escuela."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con la formación cívica y ética, aprenderás los valores que regirán tu vida, llevándolos a la práctica.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los preceptos que se enseñan dentro de la Formación Cívica y Ética, son elementos que podrán ayudarte a conocer qué tipo de obligaciones y derechos tienes como ciudadano.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las decisiones que tomas pueden estar influenciadas de manera positiva, por los preceptos que se presentan en materias como Formación Cívica y Ética, pero también en todas tus materias.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los estándares éticos te permiten identificar acciones que deben prevalecer y también las que deben evitarse como la violencia, la corrupción o cualquier tipo de fraude.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según sea el caso.",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nuestra personalidad se ve desarrollada a partir de que somos adultos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una formación integral comprende la parte conceptual, procedimental  y actitudinal.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ser y el convivir, me permiten tener mejores relaciones humanas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El objetivo de aprender se centra en que puedas memorizar conceptos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando nos conocemos a nosotros mismos podemos mejorar y alcanzar nuestros objetivos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según sea el caso.",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Diversidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Dignidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Identidad",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Potencial",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Autoestima",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Valor",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Diferencia o distinción entre las personas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Cualidad que permite reconocer el valor de una persona y que merece respeto."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Conjunto de rasgos propios de un individuo o de una colectividad que los caracterizan frente a los demás. "
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Fuerza o poder del que se dispone para lograr un fin."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Valoración generalmente positiva de sí mismo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Cualidad que caracteriza al ser humano y le brinda un desarrollo pleno."
            },
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 0,
                "datoY" : 0
            },
            {
                "direccion" : 0,
                "datoX" : 0,
                "datoY" : 7
            },
            {
                "direccion" : 1,
                "datoX" : 7,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 4,
                "datoY" : 3
            },
            {
                "direccion" : 0,
                "datoX" : 7,
                "datoY" : 8
            },
            {
                "direccion" : 1,
                "datoX" : 16,
                "datoY" : 7
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "ancho" : 17,
            "alto" : 13,
            "t11pregunta": "Realiza el siguiente crucigrama."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La adolescencia es una etapa de desarrollo que abarca de los 9 a los 20 años.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La adolescencia toma su nombre del verbo “adolece” que significa sufrir a causa de algo. Se retoma ese término porque los cambios que implica pueden llegar a ser difíciles. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uno de los cambios que se dan en la adolescencia, es el establecimiento de lazos afectivos con otras personas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La edad en la que los niños ingresan a la pubertad dependerá sólo de los cambios físicos que se presenten.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La adolescencia es un periodo intermedio entre  la pubertad y la edad madura.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los estereotipos que a veces tienen connotaciones negativas, es necesario llevarlos a cabo durante la adolescencia.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la respuesta que consideres correcta.",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "elecciones",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "Decidir",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "razonar",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Actuar",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "proceder",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Pensar implica planificar "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "implica "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "expectativas.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "implica "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "congruentemente."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra el complemento de las siguientes ideas en relación a la toma de decisiones para poder ser congruentes entre nuestras acciones y pensamientos. "
        }
    }  ,
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Suposiciones</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Imaginación</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Lenguaje</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Personas que afectan al contexto</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Situaciones que afectan el contexto</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Completa el siguiente cuadro sobre “Toma de decisiones”.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Factores Internos",
            "Factores Externos"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tus padres verifican el coche para no tener multas.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ayudas a una persona mayor a cruzar la calle.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Te detienes cuando el semáforo peatonal está en rojo, aún sin autos.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No copias por temor a que te reprueben.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Levantas las heces de tu perro aunque no haya gente observando.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Conduces a la velocidad permitida, por miedo a una infracción.</p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Completa el siguiente cuadro sobre “Tipos de moral”.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Moral heterónoma",
            "Moral autónoma"
        ]
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Dar estructura a tus actividades.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Tener un orden mental.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Tener conflictos aunque no sean tu culpa.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Tener miedo, pero seguirlas por miedo a una sanción.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Cambiar tu forma de pensar sin reflexionar en ella.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Hacer un uso de la libertad que tienes.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Tener un comportamiento adecuado.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Actuar basándote en tus impulsos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Son personas que nos ayudan con su experiencia y forma de pensar.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Son exclusivamente adultos que tienen ese rol, sólo por su edad.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Son personas que adquieren su rol a través de la coherencia que demuestran.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Son impuestas por la autoridad, como lo es el gobierno.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>La flexibilidad y aplicabilidad que permiten su vigencia.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>La facilidad y rapidez con las que pueden cambiar incluso sin necesitar de la opinión de los demás.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>El hecho de que  para poder  modificarlas, es necesario tener una opinión consensuada.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>El hecho de no tener que seguirlas si no te parecen adecuadas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Las normas no pueden modificarse, porque de lo contrario no tendríamos orden.</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Las figuras de autoridad son personas que son congruentes y son ejemplo.</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Para que existan acuerdos, es necesario firmar documentos que los sustenten.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Los acuerdos son decisiones en donde las partes involucradas aceptan una propuesta en común.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Debes obedecer las reglas sin preguntar, ni cuestionarlas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Para poder cumplir mejor con reglas y normas, es necesario que participes en su elaboración.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Las reglas y normas que nos rigen te ayudan a:","La ética te ayuda a:","Las figuras de autoridad:","Dentro de los aspectos importantes que tienen las normas están:","Dentro de los aspectos importantes que tienen las normas están:"],
         "preguntasMultiples": true,
         "columnas":2
      }
    }
];