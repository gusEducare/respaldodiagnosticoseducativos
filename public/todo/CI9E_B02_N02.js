json = [
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Definición<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Planeación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Acción<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Alcanzar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Proceso<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Logro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Medio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Corto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Largo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Plazo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cumplimiento<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Capacidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Habilidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Desarrollo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Conducir<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Alimentarme de manera saludable, cuidando las porciones y nutrientes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elegir siempre bebidas procesadas en lugar de agua natural.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Realizar actividad física de forma constante y de ser posible desarrollar mis habilidades en un deporte específico.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Procurar hacer ejercicio únicamente cuando me veo en la obligación de hacerlo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Participar socialmente y colaborar con otros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tomar acciones claras para conocerme y respetarme a mí mismo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Equilibrar actividades cotidianas y de descanso acorde a las necesidades del cuerpo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Responsabilizarme del cuidado de mi salud física y emocional.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Depender de mis amigos y familia para ser feliz.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Desarrollarse socialmente, respetando valores y virtudes propias y ajenas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aprender a canalizar el enojo y los momentos de tristeza.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No tomar en cuenta las necesidades de otros.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Evaluar las consecuencias de mis decisiones de forma anticipada.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Buscar que se respeten mis derechos personales sin tomar en cuenta los derechos de los otros.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos tenemos capacidades inherentes al ser humano",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al ejercitar nuestras capacidades desarrollamos habilidades específicas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las capacidades humanas son estáticas",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Capacidad y habilidad son innatas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nuestra voluntad de lograr nos permite desarrollar habilidades.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunas capacidades son adquiridas por experiencia ",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
 {
        "respuestas": [
            {
                "t13respuesta": "Dañar accidentalmente la propiedad de otro y dejar una nota para con tus datos para que te pueda contactar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Conversar con los representantes de distintos partidos políticos para solucionar una situación específica.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Tomar en cuenta la opinión y llegar a un acuerdo con los habitantes de la zona para construir un nuevo centro comercial.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Respetar la opinión de tu compañero de grupo aun cuando sea completamente distinta a la tuya.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Responsabilidad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Diálogo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Consenso"
            },
            {
                "c03id_tipo_pregunta": "1",
                "t11pregunta": "Tolerancia"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
  {
        "respuestas": [
            {
                "t13respuesta": "<p>desarrollo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>practica<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>habilidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>objetivo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>capacidad<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Significa crecimiento, preceso, evolución. De momento hce referencia a una persona o ser vivo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "EL ejercicio o realización de una actividad de forma continua. Con ella se contribuye al desarrollo de habilidades especificas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Capacidad de una persona para desarrollar una actividad de manera óptima y con total facilidad a través del ejercicio de la misma."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Fin al que se desea llegar, impúlsa al individuo a tomar desiciones y cumplir metas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Conjunto de condiciones como cualidades propias de un ser y que favorecen al desarrollo del mismo para cumplir una funcion u obejtivo determinado."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 12,
                "datoY" : 1
            },
            {
                "direccion" : 1,
                "datoX" :9,
                "datoY" : 3
            },
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 5
            },
            {
                "direccion" : 0,
                "datoX" : 5,
                "datoY" : 7
            },
            {
                "direccion" : 0,
                "datoX" : 3,
                "datoY" : 12
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3",
             "alto":18
        }
    },
 {
        "respuestas": [
            {
                "t13respuesta": "<p>Desconocimiento<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Fatiga<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Indiferencia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Temor<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Valor excesivo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Baja autoestima<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hartazgo<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "En el caso de tener la responsabilidad ciudadana de tomar una decisión que afectará al colectivo. ¿Cuáles son los factores que pueden hacerte dudar respecto al tema; es decir, permanecer indeciso?  Señala todas las opciones posibles. "
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>democracia<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>participar<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>social<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>económico<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>cultural<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>votación<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>gobernantes<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>informado<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>congruente<\/p>",
                "t17correcta": "9"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>México es un país con una incipiente&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, &nbsp;como ciudadanos es nuestra obligación &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y fortalecer nuestra vida democrática. <br>El papel de cada ciudadano implica la responsabilidad de participar activamente en la solución de problemas relacionados con el desarrollo &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;político&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp; y &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; en nuestras comunidades y en la nación. Dicha participación va mucho más allá de los momentos de &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;para elegir a nuestros &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Un ciudadano comprometido con el desarrollo integral del país  ha de reunir características como ser: responsable, participativo, estar  &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, ser crítico, analítico  y  &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Diálogo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hablar mucho más alto que los otros.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Argumentar postura personal.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Escuchar activamente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Invalidar las posturas contrarias.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Manejo de emociones.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso de argumentos de fuerte impacto, aunque no se relacionen con el tema.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Seguir un orden que permita la interacción de los debatientes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Robar la voz a otros participantes.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Es indiscutible su influencia en tu proyecto de vida. Tiene que ver con: Expectativas, apoyos más cercanos además de costumbres y cultura",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Condiciones sociales, politicas y economicas del entorno. Se relaciona con las facilidades y oportunidades para el cumplimiento de tu proyecto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Al desarrollar tu proyecto de vida se toman en cuenta las capacidades, habilidades, aspiraciones y motivación intrínseca.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Factores familiares"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Factores sociales"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Factores personales"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },{
        "respuestas": [
            {
                "t13respuesta": "<p>Prioriza el bienestar social sobre el personal.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso de las leyes para ganancias personales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Respeto a la dignidad humana de cada persona.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Búsqueda del bienestar individual sobre el social.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aplicación de la ley sin distinción.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respeto a los derechos humanos de cada persona sin afectar el bien social.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Intolerancia ante las diferencias del ser humano.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    }
]