json=[  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E<img src=''>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<img src=''>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<img src=''>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"La fuerza gravitacional se comporta de forma radial hacia el centro de objetos esféricos. Esto se puede representar con el siguiente diagrama: "
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EMovimiento\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFuerza\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Emasa\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Epeso\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Se define como el cambio de posición de un objeto?"
      }
   },
   {  
      "respuestas":[  
          {  
            "t13respuesta":"\u003Cp\u003EMovimiento\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFuerza\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Emasa\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Epeso\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Vigor, robustez y capacidad para mover algo o a alguien que tenga peso o haga resistencia."
      }
   },
   {  
      "respuestas":[  
          {  
            "t13respuesta":"\u003Cp\u003EMovimiento\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eaceleración\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Einercia\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Efricción\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Magnitud que expresa la variación de la velocidad en la unidad de tiempo."
      }
   },
   {  
      "respuestas":[  
          {  
            "t13respuesta":"\u003Cp\u003Efuerza\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Epeso\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Einercia\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Emovimiento\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Se representa mediante la ecuación: masa * aceleración de la gravedad"
      }
   },
   {  
      "respuestas":[  
          {  
            "t13respuesta":"\u003Cp\u003Emoléculas\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Epeso\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Einercia\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Emasa\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Unidad mínima de una sustancia que conserva sus propiedades químicas y puede estar formado por átomos iguales y diferentes."
      }
   }
   ,
   {  
      "respuestas":[  
          {  
            "t13respuesta":"\u003Cp\u003Evelocidad\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eposición\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Einercia\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eaceleración\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Magnitud física que expresa el espacio recorrido por un móvil en la unidad de tiempo y cuya unidad en el sistema internacional es el metro por segundo."
      }
   },
   {  
      "respuestas":[  
          {  
            "t13respuesta":"\u003Cp\u003E0 Km/h2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E100 Km/h2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E275 Km/h2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E375 Km/h2\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Calcula  la aceleración de un auto que parte del reposo y su velocidad final es de 60 Km/h, viajando en 10 min."
      }
   },
   {  
      "respuestas":[  
          {  
            "t13respuesta":"\u003Cp\u003E0 kg* Km/h\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E150 kg* Km/h\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E250kg* Km/h\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E350 kg* Km/h\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Calcula  la cantidad de movimiento de una persona cuya masa es de 50 kg y viaja a 5Km/h."
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>En. Potencial<\/p>",
                "t17correcta": "1,5,3"
            },
           {
                "t13respuesta": "<p>En. Cinética<\/p>",
                "t17correcta": "2,4"
            },
            {
                "t13respuesta": "<p>En. Potencial<\/p>",
                "t17correcta": "3,1,5"
            },
            {
                "t13respuesta": "<p>En. Cinética<\/p>",
                "t17correcta": "4,2"
            },
            {
                "t13respuesta": "<p>En. Potencial<\/p>",
                "t17correcta": "5,1,3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>a) Una pelota inmóvil sobre una mesa &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>b) Un ciclista andando&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>c) Un ciclista sentado en el camino&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>d) Un papalote volando con el viento&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>e) El agua contenida en una presa&nbsp;<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": " Observa las opciones mostradas e indica si las acciones implican una mayor energía potencial o mayor energía cinética."
        }
    }
];