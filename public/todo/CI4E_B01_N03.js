json = [
{
	"respuestas": [
	{
		"t13respuesta": "<p><img src='CI4E_B01_N03_01_01.png'><\/p>",
		"t17correcta": "0,0"
	},
	{
		"t13respuesta": "<p>Se presentan cambios drásticos en los estados de humor que pueden llevar a conflictos, desafío a la autoridad o incluso sentimientos encontrados.<\/p>",
		"t17correcta": "1,0"
	},
	{
		"t13respuesta": "<p><img src='CI4E_B01_N03_01_02.png'><\/p>",
		"t17correcta": "0,1"
	},
	{
		"t13respuesta": "<p>Aumenta la necesidad de pertenencia y de pasar más tiempo con los amigos, en lugar de la familia.<\/p>",
		"t17correcta": "1,1"
	},
	{
		"t13respuesta": "<p><img src='CI4E_B01_N03_01_03.png'><\/p>",
		"t17correcta": "0,2"
	},
	{
		"t13respuesta": "<p>Aumento en la estatura y cambios en la forma del cuerpo.<\/p>",
		"t17correcta": "1,2"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "Relaciona los aspectos que se relacionan con los cambios que se producen cuando un ser humano crece"
	},
	"contenedores":[ 
	"",
	""

	],
	"contenedoresFilas":[ 
	"Físico",
	"Psicológico",
	"Emocional"

	]
},
{
	"respuestas": [
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Falso",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "A pesar de crecer, la manera de pensar no cambia.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Nuestros gustos y preferencias se ven influenciados por el medio en el que nos desarrollamos.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Conforme crecemos, nuestra libertad y responsabilidades también aumentan.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Los sentimientos se pueden indentificar de manera clara y precisa en todas las personas.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Nuestra forma de comportarnos, movernos o incluso, algunos de nuestros gestos pueden ayudarnos a identificar los sentimientos que estamos experimentando.",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
		"descripcion": "",   
		"variante": "editable",
		"anchoColumnaPreguntas": 70,
		"evaluable"  : true
	}        
},
{
	"respuestas": [
	{
		"t13respuesta": "Si",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "No",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p><img src='ci4e_b01_n03_02_01.png'></p>",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<img src='ci4e_b01_n03_02_02.png'>",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<img src='ci4e_b01_n03_02_03.png'>",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona las actividades que te gusta realizar  y escribe un ejemplo en el que  consideras que te desempeñas mejor.<\/p>",
		"descripcion":"Aspectos a valorar",
		"evaluable":false,
		"evidencio": false
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Si",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "No",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<img src='ci4e_b01_n03_02_04.png'>",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<img src='ci4e_b01_n03_02_05.png'>",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<img src='ci4e_b01_n03_02_06.png'>",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona las actividades que te gusta realizar  y escribe un ejemplo en el que  consideras que te desempeñas mejor.<\/p>",
		"descripcion":"Aspectos a valorar",
		"evaluable":false,
		"evidencio": false
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>habilidad<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>capacidad<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>tareas<\/p>",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "<p>natural<\/p>",
		"t17correcta": "4"
	},
	{
		"t13respuesta": "<p>práctica<\/p>",
		"t17correcta": "5"
	},
	{
		"t13respuesta": "<p>inteligencias<\/p>",
		"t17correcta": "6"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>La &nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;se define como un talento, &nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;destreza o competencia que tienen las personas para realizar ciertas &nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;o actividades. A pesar de ser un talento &nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; la habilidad se puede mejorar mediante la  &nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;y el entusiasmo. Las habilidades, como las  &nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;, se desarrollan de diferente forma en cada persona.<\/p>"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "Arrastra y ordena cada palabra donde corresponda para completar el texto."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Coordinación",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Comunicación",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "Compromiso",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "Confianza",
		"t17correcta": "4"
	},
	{
		"t13respuesta": "Complementariedad",
		"t17correcta": "5"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Trabajar juntos y al mismo tiempo, cada uno una parte del estudio o trabajo para obtener nuevos y mejores resultados."
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Que cada individuo sea responsable de su aportación al trabajo compartido y se asegure de llevarlo a cabo en tiempo y forma."
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Que la conducta de cada individuo sea simple, familiar y funcional. Saber que cada quien hará la parte que le corresponde del proyecto."
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Compartir ideas y conocimientos de forma clara y directa."
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Acciones compartidas que dan forma a las relaciones sociales y aportan mejores resultados a los estudios, trabajos y procesos."
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Arrastra algunas de las claves que te permiten hacer un trabajo en equipo más eficiente con su definición."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>Unicef<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>internacional<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>ONU<\/p>",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "<p>condiciones<\/p>",
		"t17correcta": "4"
	},
	{
		"t13respuesta": "<p>DIF<\/p>",
		"t17correcta": "5"
	},
	{
		"t13respuesta": "<p>México<\/p>",
		"t17correcta": "6"
	},
	{
		"t13respuesta": "<p>políticas<\/p>",
		"t17correcta": "7"
	},
	{
		"t13respuesta": "<p>integración<\/p>",
		"t17correcta": "8"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p><\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;es la sigla en inglés del Fondo Internacional de Emergencia de las Naciones Unidas para la Infancia. Es un organismo&nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;dependiente de la&nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;que se encarga de promover la protección de los derechos de los niños ayudándolos a satisfacer sus necesidades más importantes dándoles la oportunidad de mejorar sus&nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;de vida.&nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;es la sigla que identifica al Sistema Nacional para el Desarrollo Integral de la Familia en&nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; Es una institución del gobierno que ayuda a proteger y en su caso defender los derechos de las familias mediante la conducción de&nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp; públicas en materia de asistencia social que promuevan la de&nbsp;<\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;familiar<\/p>"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "Arrastra y ordena cada palabra donde corresponda para completar el texto."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Las instituciones encargadas de cuidar de nuestra salud pueden obligarnos a comer los alimentos que necesitamos.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Para el cuidado de nuestra salud se requiere de profesionales como abogados, nutriólogos, ingenieros, etc.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Los buenos hábitos alimenticios deben ser una prioridad en nuestras vidas y existen programas del gobierno que nos ayudan a llevarlos a cabo.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Gracias a las instituciones del gobierno, se crean programas para controlar las enfermedades y evitar contagios masivos como son las epidemias. ",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Existen fenómenos naturales  como los terremotos o huracanes que pueden ser contrarrestados por los programas de instituciones gubernamentales que cuidan de nuestra salud.",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
		"variante": "editable",
		"anchoColumnaPreguntas": 70,
		"evaluable"  : true
	}        
},
{
	"respuestas": [
	{
		"t13respuesta": "IMSS",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "ISSSTE",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "Seguro Popular",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "Secretaría de Salud",
		"t17correcta": "4"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Instituto Mexicano del Seguro. Cuenta con clínicas pero también con centros de salud y deporte."
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Instituto de Seguridad y Servicios Sociales de los Trabajadores del Estado. Brinda servicios de salud a las personas que trabajan en el gobierno mexicano."
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Seguro médico para todos los mexicanos que no cuenten con otro tipo de seguro médico."
	},
	{
		"c03id_tipo_pregunta": "12",
		"t11pregunta": "Se encarga de las políticas públicas relacionadas con la Salud."
	}

	],
	"pregunta": {
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Arrastra la institución con el recuadro que le corresponda."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Sólo las personas enfermas deben hacer uso de los servicios de salud que ofrece el gobierno.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Debemos fomentar las campañas de prevención de enfermedades para evitar problemas con nuestra salud.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "La salud de una persona se puede ver afectada si no cuenta con alternativas para tener una vida saludable.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Sólo los médicos, enfermeras y personal especializado debe encargarse del cuidado de mi salud.",
		"correcta"  : "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",  
		"variante": "editable",
		"anchoColumnaPreguntas": 70,
		"evaluable"  : true
	}        
},
{
	"respuestas": [

	{
		"t13respuesta": "<p>recreacion<\/p>",
		"t17correcta": "1"
	},{
		"t13respuesta": "<p>salud<\/p>",
		"t17correcta": "1"
	},{
		"t13respuesta": "<p>institucion<\/p>",
		"t17correcta": "1"
	},{
		"t13respuesta": "<p>derechos<\/p>",
		"t17correcta": "1"
	},{
		"t13respuesta": "<p>diversion<\/p>",
		"t17correcta": "1"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Diversión o descanso durante el tiempo libre."
	},{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Estado en el que no se tiene ninguna enfermedad."
	},{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Organismo creado para desempeñar una labor."
	},{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Conjunto de normas que regulan la convivencia social."
	},{
		"c03id_tipo_pregunta": "15",
		"t11pregunta":"Actividad que hace el tiempo de manera agradable."
	}
	],
	"pocisiones": [
	{
		"direccion" : 0,
		"datoX" : 2,
		"datoY" : 3
	},{
		"direccion" : 1,
		"datoX" : 7,
		"datoY" : 2
	},{
		"direccion" : 1,
		"datoX" : 11,
		"datoY" : 2
	},{
		"direccion" : 1,
		"datoX" : 3,
		"datoY" : 2
	},{
		"direccion" : 1,
		"datoX" : 9,
		"datoY" : 2
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "15",
		"alto":13,
		"t11pregunta": "Realiza el siguiente crucigrama:"
	}
}
]