json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Por sí mismo</p>",
                "t17correcta": "0,0"
            },
            {
                "t13respuesta": "<p>Otro, diferente</p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p>Alimentación, alimento</p>",
                "t17correcta": "0,2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>¿Cuál es la clave dentro de las siguientes palabras que nos dice las características por las que los seres vivos obtienen energía y nutrientes? Analiza la palabra para entenderla mejor y arrastra su significado.<\/p>"
        },
        "contenedores": [
            "Significado"
        ],
        "contenedoresFilas": [
            "Auto",
            "Hetero",
            "Trofos"
        ],
        "css": {
            "alturaRespuestas": "100px"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>equilibrio<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>ecosistema<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>bióticos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>abióticos<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>a) El <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;ambiental es el estado constante y dinámico de armonía que existe en un ecosistema. <br><p>b) Un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;es un sistema biológico constituido por una comunidad de seres vivos y el medio natural en que viven. <br><p>c) Los factores <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;son todos los tipos de seres vivos que habitan en el ecosistema.<br><p> d) Los factores <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; son los elementos que no tienen vida.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra el texto que corresponde a la frase."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>La teoría de Darwin surgió a partir de la expedición que realizó por el hemisferio sur del planeta de 1831 a 1836.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Trabajó en su teoría por 20 años y en 1856 trató de explicar la evolución de las especies.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La teoría de Darwin surgió a partir de la expedición que realizó por el hemisferio sur del planeta de 1831 a 1836.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>En 'El origen de las especies por medio de la selección natural', establece que la explicación de la diversidad se debe a las modificaciones acumuladas por la evolución.<\/p>",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena los bloques como corresponden.<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            {"Contenedor": ["1", "201,15", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["2", "201,149", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["3", "201,283", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["4", "201,417", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Brazo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Base<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Revólver<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Ocular<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Platina<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Enfoque<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Es una pieza metálica de forma curvada que puede girar; sostiene por su extremo superior al Tubo Óptico y en el inferior lleva varias piezas importantes.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Sujeción de todo el microscopio.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Contiene los sistemas de lentes objetivos. Permite, al girar, cambiar los objetivos. La esfera se suele llamar cabezal, y contiene los sistemas de lentes oculares (monoculares o binoculares (2 lentes)).<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Lente situada cerca del ojo del observador (por donde mira). Su misión es ampliar la imagen del objetivo. Suelen tener dos oculares, por eso se llaman binoculares, si solo tiene uno se llama monocular.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Lugar donde se deposita la preparación que se quiere observar. Tiene en su centro una abertura circular por la que pasará la luz del sistema de iluminación.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Macrométrico que aproxima el enfoque y micrométrico que consigue el enfoque correcto.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la palabra que corresponda para completar las siguientes oraciones."
        }
    },      
    {
        "respuestas": [
            {
                "t13respuesta": "<p>microbiana<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Koch<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Snow<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Miasma<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Pasteur<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Teoría que proponía que las enfermedades eran ocasionadas por gérmenes."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Aisló la bacteria causante del cólera."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Proponía que el cólera era transmitido a través de alimentos y fuentes de agua contaminada."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Nuves de vapores venenosos a las que se les atribuia la aparicion de enfermedades."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Desarrolló las primeras vacunas para prevenir enfermedades."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 4
            },
            {
                "direccion" : 1,
                "datoX" : 3,
                "datoY" : 2
            },
            {
                "direccion" : 1,
                "datoX" : 5,
                "datoY" : 2
            },
            {
                "direccion" : 1,
                "datoX" : 1,
                "datoY" : 4
            },
            {
                "direccion" : 1,
                "datoX" : 10,
                "datoY" : 3
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto":9,
            "t11pregunta": "Realiza el crucigrama"
        }
    }      
    
]