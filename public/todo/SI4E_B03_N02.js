json=[
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Experto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Presentacion<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>reporte<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>psicologica<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>informativa<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>cierre<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>preguntas<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>cuerpo<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>entrevistar<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Persona con relevantes conocimientos respecto a un tema. Algunos realizan investigaciones en el área."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Parte inicial de la entrevista donde se presenta al entrevistado y se hace una breve semblanza de él, del tema o ambos."
            }, 
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Labor posterior a la entrevista. El entrevistador relata los puntos más destacados de su conversación."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Tipo de entrevista que busca información respecto a la personalidad del entrevistado."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Tipo de entrevista a expertos respecto a uno o varios temas en específico."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Parte final de la entrevista donde se agradece la participación del entrevistado y se concluye."
            },
            { 
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "A partir de ellas obtenemos y ampliamos los conocimientos que tenemos sobre un tema."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Parte de la entrevista en donde se realizan preguntas, respuestas y se obtiene información."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Acto de comunicación oral entre un entrevistador y uno o más entrevistados."
            }
        ],
        "pocisiones": [
            { 
                "direccion" : 0,
                "datoX" : 16,
                "datoY" : 8
            },
            {
                "direccion" : 0,
                "datoX" : 5,
                "datoY" : 11
            },
            {
                "direccion" : 0,
                "datoX" : 3,
                "datoY" : 15
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 17
            },
            {
                "direccion" : 1,
                "datoX" : 12,
                "datoY" : 1
            },
            {
                "direccion" : 1,
                "datoX" : 7,
                "datoY" : 6
            },
            {
                "direccion" : 1,
                "datoX" : 16,
                "datoY" : 6
            },
            {
                "direccion" : 1,
                "datoX" : 19,
                "datoY" : 6
            },
            {
                "direccion" : 1,
                "datoX" : 9,
                "datoY" : 11
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "ancho": 23,
            "alto": 23,
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los poetas utilizan recursos literarios",
                "valores": ["", ""],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los poemas son obras literarias generalmente escritas en verso",
                "valores": ["", ""],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los poemas jamás expresan emociones",
                "valores": ["", ""],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Comparación y metáfora son recursos literarios",
                "valores": ["", ""],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La intención del autor es irrelevante para comprender un poema",
                "valores": ["", ""],
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Relaciona dos cosas reales o imaginarias que tienen algún elemento en común.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Generalmente emplean palabras conectoras (como, cuan, tan, semejante a, igual que, tal como).<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Fuerte como un roble<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es bondadosa semejante a una madre<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pasó el tiempo tan lento como pasa el agua por el dique<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Comparan y asocia elementos entre los que existe alguna semejanza. Uno en sentido literal otro figurado.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Usualmente sin palabras conectoras.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Su piel es blanca se diría nívea<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sol gentil y clima amable<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Soy un burro<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Comparación",
            "Metáfora"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>rima<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>verso<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>poema<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>poetas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>variados<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>leerlos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>subjetivos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>sentimientos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>escucharlos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>musicalidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>entonacion<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>como<\/p>",
                "t17correcta": "1,7"
            },
            {
                "t13respuesta": "<p>igual<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>que<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>semejante<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>cual<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>tal<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>como<\/p>",
                "t17correcta": "7,1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Es dócil "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbspuna paloma.<br>Mario guapo y listo "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbspsu padre.<br>Pelirroja, su cabello es rojo obscuro "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbspa las brasas.<br>Siempre con buen ánimo "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbspamanecer en primavera<br>Callado "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbspun búho por la mañana<br>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "respuestasLargas":true,
            "ocultaPuntoFinal": true,
            "anchoRespuestas": 150,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se logra identificar qué se vende",
                "valores": ["", ""],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La imagen está relacionada con el texto",
                "valores": ["", ""],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Contiene publicidad engañosa o difícil de verificar",
                "valores": ["", ""],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sabes con certeza a quién va dirigido",
                "valores": ["", ""],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Explica dónde puedes comprar el producto",
                "valores": ["", ""],
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "¿Qué se anuncia?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿A quién va dirigido el anuncio?",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "¿Cuál es su eslogan?",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "¿Cuál es la finalidad del anuncio?",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "¿La imagen es adecuada al mensaje?",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Caldo en cubitos."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "A personas que cocinan"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "No tiene"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Vender caldo en cubitos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Sí"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Mensajes publicitarios<\/p>",
                "etiqueta": "a",
                "t17correcta": "0,2,3,4"
            },
            {
                "t13respuesta": "<p>Eslogan<\/p>",
                "etiqueta": "b",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Mensajes publicitarios<\/p>",
                "etiqueta": "c",
                "t17correcta": "2,3,4,0"
            },
            {
                "t13respuesta": "<p>Mensajes publicitarios<\/p>",
                "etiqueta": "d",
                "t17correcta": "3,4,0,2"
            },
            {
                "t13respuesta": "<p>Mensajes publicitarios<\/p>",
                "etiqueta": "e",
                "t17correcta": "4,0,2,3"
            },
            {
                "t13respuesta": "<p>Frases publicitarias descriptivas<\/p>",
                "etiqueta": "f",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Frases publicitarias emocionales<\/p>",
                "etiqueta": "g",
                "t17correcta": "6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "conservaColores": true,
            "t11pregunta": "<script>$(document).ready(function(){$('#columnaContenedores').prepend('<div></div>') $('#columnaContenedores>div:nth-child(1)').append('<p>Tipo de comunicación que tiene como objetivo que las personas compren algún producto o servicio.</p>') $('#columnaContenedores>div:nth-child(1)').append('<p>Frase que repiten constantemente para identificar al producto.</p>') $('#columnaContenedores>div:nth-child(1)').append('<p>En sus diseños utilizan colores llamativos.</p>') $('#columnaContenedores>div:nth-child(1)').append('<p>Contienen texto e imágenes.</p>') $('#columnaContenedores>div:nth-child(1)').append('<p>Se dirigen a todo tipo de público.</p>') $('#columnaContenedores>div:nth-child(1)').append('<p>Explican las características o funciones del producto.</p>') $('#columnaContenedores>div:nth-child(1)').append('<p>Intentan relacionar un sentimiento con el producto.</p>')});</script><style>.contenedorRespuesta{border: solid 1px silver !important; box-shadow: inset 0 4px 8px rgba(0,0,0, 0.15);} #columnaContenedores>div:nth-child(1){width: 88%; position: relative; left: 10%;} #columnaContenedores>div:nth-child(1)>p{position: relative; width: 100%; font-size: 18px; font-weight: bold; text-align: left;} #columnaContenedores>div:nth-child(1)>p:nth-child(1){top: 10px;} #columnaContenedores>div:nth-child(1)>p:nth-child(2){top: 27px;} #columnaContenedores>div:nth-child(1)>p:nth-child(3){top: 57px;} #columnaContenedores>div:nth-child(1)>p:nth-child(4){top: 85px;} #columnaContenedores>div:nth-child(1)>p:nth-child(5){top: 115px;} #columnaContenedores>div:nth-child(1)>p:nth-child(6){top: 144px;} #columnaContenedores>div:nth-child(1)>p:nth-child(7){top: 178px;}</style>",
            "tipo": "ordenar",
            "imagen": true,
            "url":""
            
        },
        "contenedores": [
            {"Contenedor": ["", "15,0", "", "180, 50","","whitesmoke"]},
            {"Contenedor": ["", "95,0", "", "180, 50","","whitesmoke"]},
            {"Contenedor": ["", "175,0", "", "180, 50","","whitesmoke"]},
            {"Contenedor": ["", "250,0", "", "180, 50","","whitesmoke"]},
            {"Contenedor": ["", "330,0", "", "180, 50","","whitesmoke"]},
            {"Contenedor": ["", "410,0", "", "180, 50","","whitesmoke"]},
            {"Contenedor": ["", "490,0", "", "180, 50","","whitesmoke"]}
        ],css:{"espacioRespuestas":25}
    }
]