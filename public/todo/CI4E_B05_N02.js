[  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las imágenes que representen una convivencia pacífica."
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Guerra<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mundo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Política<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Paz<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Social<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Bélico<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Situación o estado en que no hay guerra ni luchas entre dos o más partes enfrentadas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Acuerdo para poner fin a una guerra.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Estado de tranquilidad y buena relación entre los miembros de un grupo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Estado anímico de tranquilidad y sosiego que experimenta la persona no turbada por molestias, preocupaciones, etc.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Actitud de la persona que no respeta las opiniones, ideas o actitudes de los demás si no coinciden con las propias.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Falta de justicia, de bien común y de equilibrio dentro de diversos grupos sociales.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las oraciones que hagan referencia a definiciones del concepto de paz."
      }
   },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Abuso de alguna autoridad frente un grupo vulnerable",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Conflictos religiosos por la falta de tolerancia",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Falta de oportunidades para un pueblo o comunidad",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Equidad e igualdad en oportunidades para los miembros de un país",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Injusticias sociales como la pobreza",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Cumplimiento de los derechos humanos universales",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona los elementos que pueden ser causas para la falta de paz o que son detonantes para la guerra."
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los conflictos pueden favorecer la violencia si las partes afectadas no se tratan con respeto.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La violencia debe combatirse con violencia.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Han existido líderes en el mundo que han podido combatir la violencia con demostraciones de paz.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen expresiones de violencia que se dan en nuestro país como los secuestros, los homicidios o la tortura.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos humanos pueden ser válidos dependiendo del país o la región.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Petición o solicitud de algo especialmente cuando se trata de una exigencia que se considera un derecho.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cuestión discutible que hay que resolver.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cosa elemental para existir, realizar una actividad o estar en correcto estado o funcionamiento.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Gusto hacia algo en concreto cuando existen varias opciones.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Demandas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Problemas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Necesidades"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Preferencias"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las imágenes que representen ejemplos de servidores públicos."
      }
   },
      {
        "respuestas": [
            {
                "t13respuesta": "<p>6°<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>federal<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>transparencia<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>información<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>raza<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En el artículo <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de la Constitución Política de los Estados Unidos Mexicano está establecido un reglamento que establece como ley <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y ley estatal, la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y acceso a la información pública gubernamental. Este derecho garantiza a toda persona que puede conocer la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; sin justificar su finalidad. Así mismo como se trata de un derecho universal se puede ejercer sin distinción de sexo, creencia, religión, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, nacionalidad o edad.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p style='font-size:90%'><br>Están vinculados formalmente a la entidad correspondiente; es decir, cumplen funciones que están establecidas con anterioridad<br><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:90%'><br>La vinculación de estos empleados se hace mediante contratos de trabajo, contratos que pueden rescindirse de acuerdo con el desempeño mostrado por el trabajador.<br><\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p style='font-size:90%'><br>Son los emplados que han de servir a la comunidad y que son elegidos por medio de voto popular(tal es el caso de los congresistas, los ediles, los comuneros, etc.<br><\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Empleados públicos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Trabajadores oficiales"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Miembors de corporaciones de elección popular"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Empresas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sociedad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Gobierno<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Lucrativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Civil<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    }
]