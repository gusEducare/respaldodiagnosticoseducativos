json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Son diferencias entre la energía cinética y la energía potencial.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Menciona la similitud entre la energía cinética y la energía potencial.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se define como la masa por su velocidad",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Ley que relaciona la fuerza, masa y aceleración",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Expresa que a toda acción corresponde una reacción",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "La primera depende de la velocidad o la segunda depende de la altura y la gravedad"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Ambas dependen de la masa del cuerpo en estudio."
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Cantidad de movimiento"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "segunda ley de Newton"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Tercera ley de Newton"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003Eh = E/ mg\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eh = g/ Me\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eh = Em/g\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eh = mg/E\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Encuentra la expresión para la altura en términos de la Energía potencial y  la masa."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEc=900 kg*(m/s)2 Ep= 1962 kg*(m/s)2\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEc=1962 kg*(m/s)2 Ep=  900 kg*(m/s)2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEc=1962 kg*(m/s)3 Ep=  900 kg*(m/s)3\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEc=900 kg*(m/s)3 Ep= 1962 kg*(m/s)3\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Calcula la energía cinética y la energía potencial de una pelota con masa de 2 kg, que se deja caer de un edificio de 100m de altura y cuya velocidad es 30m/s."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E40 Kg\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1000 Kg\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E250 Kg\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E100 Kg\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Calcula la masa de una caja que es empujada por un pasillo sin fricción desde el reposo. Se le aplica una fuerza de 100N durante 5s y alcanza una velocidad de 2 m/s. "
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>F1 < F2<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>F1 = F2<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>F1 > F2<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>a) El blosque se mueve hacia la derecha  &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>b) El Bloque no se mueve  &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>c) El Bloque se mueve hacia la Izquierda  &nbsp;<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E1280 N\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E500 N\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E130 N\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E4095 N\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Una roca grande pesa  800 N en la Tierra ¿Cuánto pesa en la Luna? La gravedad es 1.6m/s2. Nota: Los resultados se presentan redondeados."
      }
   }
];

