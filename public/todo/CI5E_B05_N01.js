json=[  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Que gane quien sea más poderoso.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que gane el más débil.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que ambos ganen.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Cuál es la forma ideal y ética de resolver un conflicto."
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Ponerse en contacto con la comisión de los derechos humanos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Proporcionar nuestros datos explicar bien lo sucedido<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Presentar nuestra queja de manera formal por escrito o por teléfono si es urgente.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Seguir los pasos y procedimientos que nos indiquen<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Dar seguimiento al debido proceso<\/p>",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."]}
        ]
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Derechos administrativos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Derechos civiles",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Derechos humanos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Derechos económicos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Derechos políticos",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Se les nombra así a los derechos con que todo individuo cuenta por el hecho mismo de ser persona."
      }
   },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Ley de amparo",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Ley general de desarrollo forestal sustentable",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Código civil federal",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Ley General de las Personas con Discapacidad",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Constitución política de los estados unidos mexicanos",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿En qué documento rector de la nación se garantizan los derechos de los mexicanos?"
      }
    }
];