json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>biodiversidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>clima<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>identidad<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>México<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>535<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>tercer<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>segundo<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>cien mil<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>cinco<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>El término <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;se refiere a toda la variedad de formas en las que se manifiesta la vida en la Tierra. La biodiversidad describe la variedad de plantas, animales, hongos y microorganismos, así como las diferencias genéticas entre los organismos de una misma especie y las diversas relaciones entre las especies y el ambiente que se dan en un ecosistema.<br/>\n\
                                    La biodiversidad es muy importante pues nos brindan diferentes recursos como alimentos, medicinas, materiales para elaborar vestimentas, construir viviendas, favorece la fertilidad del suelo para el cultivo, brinda oxígeno y atrapa el dióxido de carbono con lo cual ayuda a regular el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, además de contribuir a la formación de la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;cultural a través de los ingredientes culinarios, entre otros elementos.<br/> <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;es uno de los cuatro países que alberga entre el 60 y el 70% de las especies conocidas en el planeta, por ello es considerado como un país megadiverso. México cuenta con 535 <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;especies de mamíferos, por ello ocupa el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;lugar en diversidad de este grupo, además de ser el país con el mayor número especies de mamíferos marinos. En cuanto a los reptiles, México cuenta con 804 especies, por lo que ocupa el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;lugar mundial en diversidad de este grupo. Se han descrito 2184 especies de peces, esta diversidad de especies solo es superada por la región del Pacífico asiático conformada por Indonesia, Filipinas, Australia y parte de Papua, Nueva Guinea. En lo que se refiere a Insectos, en México se han descrito 47853 especies, pero se estima que existen cerca<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. En cuanto a las plantas, se han descrito un poco más de 25000 especies, lo que coloca a México entre los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;países con mayor número de plantas vasculares.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras que te ayuden a completar el siguiente párrafo que nos habla sobre la biodiversidad."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Reino</p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Categoría</p>",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Mamíferos</p>",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "<p>Especie</p>",
                "t17correcta": "3",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Completa el siguiente cuadro con las siguientes posibles respuestas:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "esp_kb1_s01_a01_01.png",
            "respuestaImagen": true,
            "bloques": false

        },
        "contenedores": [
            { "Contenedor": ["", "0,0", "cuadrado", "100, 150", "."] },
            { "Contenedor": ["", "0,150", "cuadrado", "100, 150", "."] },
            { "Contenedor": ["", "0,350", "cuadrado", "100, 150", "."] },
            { "Contenedor": ["", "0,550", "cuadrado", "100, 150", "."] }
        ]
    },


    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los humanos pueden clasificarse por la especie Homo erectus.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El nombre de la especie de un ser vivo, es conocido como nombre científico.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se denomina un nombre científico o nombre de la especie a los seres vivos para que podamos nombrarlos con mucha mayor facilidad.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El nombre científico de un pato común es Anasplatyhynchos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los perros, sin importar su raza, pertenecen a la especie de los Canisfamiliaris.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El nombre científico del hombre es Homo sapiens",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tienen una distribución restringida.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pueden vivir en prácticamente cualquier parte del mundo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Representan una forma de vida única.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son especies que se han adaptado en todo ecosistema.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Dentro del contexto de la biodiversidad, endémico se refiere a una especie que: "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Deforestación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Contaminación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Reproducción en cautiverio.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cacería controlada.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Algunos elementos que han hecho que las especies endémicas estén en peligro son:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>20,000 especies de plantas y 1,000 de vertebrados.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>15,000 especies de vertebrados y 1,000 de plantas. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>15,000 especies de plantas y 1,000 de vertebrados.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>1,000 especies de plantas y 15,000 de vertebrados.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "En México existen alrededor de:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>65%<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>56%<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La mitad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Un tercio<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Dentro de los anfibios que existen en nuestro país, el porcentaje de ellos que son especies endémicos son:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Rana de garras africana<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Coyote<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cactus<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ajolote<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Un ejemplo de especie endémica en nuestros país es:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Seres vivos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Insectos y animales<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elementos qie no poseen vida<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Agua, luz solar y temperatura<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Plantas y hongos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El suelo, minerales y el relieve<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Factores bióticos"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Seres vivos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Insectos y animales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elementos qie no poseen vida<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Agua, luz solar y temperatura<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Plantas y hongos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El suelo, minerales y el relieve<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Factores abióticos"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ESelva húmeda\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMatorral\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ESelva seca\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDunas\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Se localizan en la mayor parte del Altiplano,  Península de Baja California, extremo norte de las planicies del Pacífico y Golfo de México."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ESelva húmeda\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EMatorral\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ESelva seca\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDunas\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Su vegetación son los árboles de gran altura como el palo mulato, higueras matapalo y maderas preciosas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ESelva húmeda\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMatorral\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ESelva seca\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDunas\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Extensiones de arenas que tienen como vegetación plantas que viven en el suelo con alto contenido de sales de herbáceas y matorrales arbustivos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ESelva húmeda\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMatorral\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ESelva seca\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EDunas\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Se distribuye desde Sinaloa hasta Chiapas y parte de la Península de Yucatán, con vegetación como mezquites, guaje, bonete y herbaceas trepadoras."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ESelva húmeda\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMatorral\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ESelva seca\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDunas\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Ocupan el 40% de la superficie del país."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>permanente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>corriente<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>dulce<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>6 metros<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>humedales<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>manglares<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>presas<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los ecosistemas acuáticos son porciones de terreno inundados que mantienen el agua de forma <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;o temporal, ya sea de forma estancada o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Forman parte de este tipo de ecosistemas los cuerpos de agua <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y salada, incluyendo zonas de playa con profundidades menores de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;A los ecosistemas acuáticos se les denomina en forma genérica <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, existiendo una gran variedad como los cenotes, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, ríos, arroyos, lagos, lagunas, y artificiales como <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>,  bordos, estanques.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E1\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E2\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E3\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E4\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E5\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El impacto mayor del ser humano sobre los ecosistemas, se da a partir de que se hace sedentario merced a la domesticación de alguna plantas y animales. Así pues, desarrolló herramientas para despojar al suelo de su vegetación natural\n\
                        para crear áreas donde cultivar y criar animales, aprovechó otros recursos para la construcción de casas, vestido, que llevó a mejores condiciones de vida con un consecuente incremento de su población."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E1\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E2\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E3\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E4\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E5\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Las primeras sociedades humanas tenían un modo de subsistencia basado en la recolección de recursos silvestres. El hombre tomaba lo que la naturaleza tenía disponible en cada época del año, por lo que\n\
                        se mantenía en constante movimiento o nomadismo, manteniendo una relacion no invasiva con los ecosistemas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E1\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E2\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E3\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E4\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E5\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "La necesidad de alimentar a una mayor población, condujo a la modificación de grnades extensiones de ecosistemas transformándolos en campos agrícolas y la devastación de zonas boscosas para obtención de combustibles y material para la construcción de casas y herramientas.\n\
                        Con el paso del tiempo, la observación, el conocimiento del entorno, el desarrollo cientifico y la fabricación de herramientas más sofisticadas, le permitió al hombre explotar otro tipo de recursos naturales contenidos en los ecosistemas establecidos una relación destructiva."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E1\u003C\/p\u003E",
                "t17correcta": "o"
            },
            {
                "t13respuesta": "\u003Cp\u003E2\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E3\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E4\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E5\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Desde la revolución industrial y hasta la actualidad, el desarrollo cientifico y tecnológico ha brindado la oportunidad de explotar y aprovechar de un sinfín de recursos naturales, no solo para cubrir las necesidades básicas de los seres humanos sino para proveer de satisfactores\n\
                         que forman parte de estilos de vida impuestos por la sociedad consumista y de los cuales no depende la existencia."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E1\u003C\/p\u003E",
                "t17correcta": "o"
            },
            {
                "t13respuesta": "\u003Cp\u003E2\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E3\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E4\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E5\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "En forma de uso de los recursos naturales, ha llevado a su agotamiento, reducción, extinción y a la pérdidad de bienes y servicios ecológicos, incluyendo la producción de alimentos, a escala local, regional y mundial. Si el ser humano como especie quiere prevalecer en el planeta, es \n\
                        urgente cambiar los estilos de vida, las formas de consumo y adoptar formas de aprovechamiento de los recursos naturales racionales."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Pérdida de hábitats",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sobreexplotación de recursos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Contaminación",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Especies exóticas invasoras",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Cambio climático",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Se refiere a que el lugar donde viven plantas o animales o toda una comunidad de seres vivos desaparece o se modifica drásticamente. Es la principal amenaza de la pérdida de la biodiversidad."
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Se refiere al uso indiscriminado que hace el ser humano de los recursos naturales, y por lo tanto son más probables de desaparecer."
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Es una de las amenazas más graves de la biodiversidad. Se refiere a la transmisión de sustancias perjudiciales o tóxicas al ambiente, al agua, aire o suelo."
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Son especie que vivian en otro sitio y que poblaron de manera repentina y abundante otro sitio. por ejemplo, las plagas."
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Se describe como la modificación de l clima en un área geográfica en un periodo de 20 años. Se ha visto que las temperaturas anuales en muchos lugares se han incremnetado producto de este cambio."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Cambio Climático</p>",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Contaminación</p>",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Especies exóticas invasoras</p>",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Cambio climático</p>",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Sobreexplotación de recursos</p>",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Pérdida de hábitats</p>",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Contaminación</p>",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Sobreexplotación de resursos</p>",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Cambio climático</p>",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Contaminación</p>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Sobreexplotación de resursos</p>",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Pérdida de hábitats</p>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Contaminación</p>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<p>Sobreexplotación de resursos</p>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<p>Pérdida de hábitats</p>",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Son especie que vivian en otro sitio y que poblaron de manera repentina y abundante otro sitio. por ejemplo, las plagas.",
                "Se describe como la modificación de l clima en un área geográfica en un periodo de 20 años. Se ha visto que las temperaturas anuales en muchos lugares se han incremnetado producto de este cambio.",
                "Es una de las amenazas más graves de la biodiversidad. Se refiere a la transmisión de sustancias perjudiciales o tóxicas al ambiente, al agua, aire o suelo.",
                "Se refiere al uso indiscriminado que hace el ser humano de los recursos naturales, y por lo tanto son más probables de desaparecer.",
                "Se refiere a que el lugar donde viven plantas o animales o toda una comunidad de seres vivos desaparece o se modifica drásticamente. Es la principal amenaza de la pérdida de la biodiversidad."],
            "preguntasMultiples": true,
            "columnas": 1
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Riega tu jardín en las mañanas o en la noches</p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Coloca una cubeta mientra esperas el agua caliente en la regadera</p>",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Reduce el tiempo en el que te bañas</p>",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "<p>Lava tu automóvil con una cubeta y no con manguera</p>",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "<p>Utiliza la lavadora con cargas completas</p>",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Pide a tus padre que revisen periódicamente las tuberías</p>",
                "t17correcta": "5",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "esp_kb1_s01_a01_01.png",
            "respuestaImagen": true,
            "bloques": false

        },
        "contenedores": [
            { "Contenedor": ["", "201,0", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "201,81", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "201,162", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "201,243", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "201,324", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "201,405", "cuadrado", "78, 81", "."] }
        ]
    }
]