json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Enojo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Pegaso",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Médico",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Gazapo",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Salud",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Sentimiento que generalmente causa malestar"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Animal mitológico"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Profesionista que coadyuva a conservar la salud"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cría de conejo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Estado de óptimo de un organismo"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el sustantivo con la construcción nominal que lo puede sustituir."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Panza",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Pancha",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "planchando",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "pantalones",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Pancho "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;compra y plancha, pantalones para "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", y le dice Pancha Panza: ¿qué estás "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Pancho? y Pancho Panza responde: compro y plancho "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;para Pancha."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLarsgas": true,
            "t11pregunta": "Arrastra las palabras más adecuadas para completar el trabalenguas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "¿Quién?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿Cuándo?",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "¿Cómo?",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "¿Qué?",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "¿Dónde?",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "¿Cuánto?",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Los comuneros de las zonas y el ejército mexicano lograron controlar el peor de ellos."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Durante 2015."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "El más grave de los incendios fue ocasionado por una fogata sin vigilancia."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Se solicita la colaboración de la población para cuidar el patrimonio forestal del país."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "El estado con mayor registro de incendios fue Michoacán"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "El total de fue de 41 hectáreas de bosque destruidas por el fuego."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona la respuesta que corresponda a cada pregunta."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "tu",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "yo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "que",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "les",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "mio",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "esta",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "suyo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "cual",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "tuyo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "esos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "ellos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "pocas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "cuanto",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "algunas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "alguien",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "aquello",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Señala 16 pronombres (personales, demostrativos, numerales, posesivos, relativos e indefinidos)"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe un párrafo adecuado al título “Aprendí a ser valiente”"
        }
    }
];
