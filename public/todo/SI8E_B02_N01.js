json = [
	{
		"respuestas": [
			{
				"t13respuesta": "\u003Cp\u003ESiempre comparan información.\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003ESiempre sintetizan información compleja.\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003ESiempre presenta información en ejes vertical y horizontal.\u003C\/p\u003E",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "\u003Cp\u003ESiempre incluyen información cuantitativa y cualitativa.\u003C\/p\u003E",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Señala una característica inherente a la construcción de cuadros comparativos"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "\u003Cp\u003EExposición\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003EMesa redonda\u003C\/p\u003E",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "\u003Cp\u003EDebate\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003EConferencia\u003C\/p\u003E",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Las características presentadas corresponden a"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Verdadero",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Falso",
				"t17correcta": "1"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Los hechos corresponden a información objetiva, real y comprobable, en muchos casos se sustentan en investigaciones.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Los hechos corresponden a experiencias personales y la interpretacion subjetiva de los mismos.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Las opiniones son apreciaciones subjetivas que reflejan la forma en que una persona valora e interpreta algo.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "En mesas redondas los expositores deben sustentar sus argumentos en hechos.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Las opiniones siempre estan basadas en estadisticas y estudios del tema en cuestion.",
				"correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "13",
			"t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
			"descripcion": "",
			"variante": "editable",
			"anchoColumnaPreguntas": 60,
			"evaluable": true
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "\u003Cp\u003ENovela de ciencia ficción\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003EPoema\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003ECuento\u003C\/p\u003E",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "\u003Cp\u003ENovela policiaca\u003C\/p\u003E",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Elige la respuesta que define el texto.:"
		}
	}
]
