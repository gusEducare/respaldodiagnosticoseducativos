json=[
   {
      "respuestas": [
          {
              "t13respuesta": "HI6E_B01_N03_01_04.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "HI6E_B01_N03_01_06.png",
              "t17correcta": "2",
              "columna":"0"
          },
          {
              "t13respuesta": "HI6E_B01_N03_01_05.png",
              "t17correcta": "1",
              "columna":"1"
          },
          {
              "t13respuesta": "HI6E_B01_N03_01_03.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "HI6E_B01_N03_01_02.png",
              "t17correcta": "4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Relaciona la imagen con su respectiva etapa:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"HI6E_B01_N03_01_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "borde":false, 
          "tamanyoReal": true
          
      },
      "contenedores": [
          {"Contenedor": ["", "125,75", "cuadrado", "200, 46", ".","","true"]},
          {"Contenedor": ["", "125,369", "cuadrado", "200, 46", ".","","true"]},
          {"Contenedor": ["", "294,75", "cuadrado", "200, 46", ".","","true"]},
          {"Contenedor": ["", "294,369", "cuadrado", "200, 46", ".","","true"]},
          {"Contenedor": ["", "463,222", "cuadrado", "200, 46", ".","","true"]}
      ]
  },
  {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Elaboraba algunos utensilios muy rudimentarios con piedras.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>La especie más parecida a los monos.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Su marcha era totalmente erguida.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Sólo recolectaba plantas o frutos para alimentarse.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Tenía una mandíbula más redondeada sin colmillos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Emigró de bosques lluviosos a la sabana.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Podía crear e idear objetos tallados con piedras.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Comían sobrantes de animales.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Es el más parecido al ser humano actual.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Tenía el cerebro poco desarrollado.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Descubrió el fuego.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Se comunicaba a través de símbolos.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta":"<p>Contaba con el cerebro más desarrollado.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Sus restos sólo se han encontrado en Europa.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Desarrollo utensilios muy sencillos con piedras.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Comenzó a enterrar muertos.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["El Australopithecus:","El Homo Habilis:","El Homo Erectus:","El Homo Sapiens:"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
    {
        "preguntas": [
            {
                "t11pregunta": "<img src='\hi6e_b01_n03_02_01.png'>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta":"<img src='\hi6e_b01_n03_02_02.png'>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta":"<img src='\hi6e_b01_n03_02_04.png'>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta":"<img src='\hi6e_b01_n03_02_03.png'>",
                "c03id_tipo_pregunta": "12"
            }
        ],
        "respuestas": [
            {
                "t17correcta": "1",
                "t13respuesta": "Australopithecus<br>(3-2 millnes de años)"
            },
            {
                "t17correcta": "2",
                "t13respuesta": "Homo erectus<br>(750,000 años)"
            },
            {
                "t17correcta": "3",
                "t13respuesta": "Homo (sapiens) sapiens<br>(40,000 años hasta hoy)"
            },
            {
                "t17correcta": "4",
                "t13respuesta": "Homo neanderthalensis<br>(100,000 a 40,000 años)"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el cráneo en las imágenes con el Homo correspondiente:",
            "t11pregunta":"text",
            "anchoImagen": 84,
            "altoImagen": 84,
            "iconos": true
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lucy, el Australopithecus hallado, fue descubierto por Donald Johanson.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La edad del Australopithecus Lucy era de 40 años, aproximadamente. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es el primer homínido cuyo nombre proviene de un libro.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los restos de Lucy están resguardados en Estados Unidos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lucy es el descubrimiento que captó interés mundial por conocer información del origen del hombre.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la opción correcta:<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
{  
      "respuestas":[
         {  
            "t13respuesta":"<p>Una necesidad exclusivamente cultural.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>La necesidad de llevar un registro de lo que se comerciaba.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Querer fomentar el intercambio cultural entre los pueblos. </p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>La obligación de dejar un registro de los acontecimientos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Un dibujo que representa una historia.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Un signo que representa a una etnia.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Un signo que representa una palabra.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>El antecedente de la escritura.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Egipto</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Israel</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Mesopotamia</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>China</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta":"<p>La evolución mental de los seres humanos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>La creación de sociedades complejas que se transformaron en grandes civilizaciones.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>La posibilidad de aprender a leer y escribir.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>La difusión de las religiones.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Se pudieron escribir todas las palabras con 25 letras.</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Hay un mayor conocimiento, por lo tanto, el mundo evoluciona.</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Se pudo trabajar de una forma más eficiente en el campo.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Permitió una convivencia más adecuada entre las personas. </p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["El desarrollo de la escritura se debió a:","Un logograma es: ",
                         "Los primeros descubrimientos de la evolución de la escritura se encuentra en:","Algunas de las consecuencias de la escritura son:",
                         "Con el surgimiento del alfabeto:"],
         "preguntasMultiples": true,
         "columnas":1
      }
   }
 ]