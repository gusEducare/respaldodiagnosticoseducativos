json = [
      {
        "respuestas": [
            {
                "t13respuesta": "Voluntad",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "Inteligencia",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Individualidad",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Libertad",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ""
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Es el movimiento interno-personal que te lleva a conseguir un objetivo por deseo propio.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Es la capacidad que tenemos para razonar a diferencia de otros seres vivos.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Cada ser posee características que son únicas e irrepetibles.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Es la capacidad de decidir y ejecutar acciones individuales."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras a sus conceptos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Seleccionar el tipo de ropa que usarás en una fiesta.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Decidir qué carrera te gustaría estudiar.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aceptar o no si serás novio(a) de la personas que te lo pidió.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Estudiar o no para un examen final.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Decidir el tipo de película que verás el próximo fin de semana.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Seleccionar el tipo de corte de cabello que te harás.</p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Elige las decisiones comunes que podrías tomar en el día a día y  las que consideres mucho más importantes y trascendentes.",
            "tipo": "horizontal"
        },
        "contenedores":[
            "Decisiones comunes",
            "Decisiones trascendentes"
        ]
    },
{
        "respuestas": [
            {
                "t13respuesta": "personal",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "integral",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "actitudes",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "críticas",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "ser",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "individual",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "integración",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "El desarrollo "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;es parte fundamental de la formación "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", por lo que nuestra educación contempla la adquisición de conocimientos, "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;y valores para generar estructuras de pensamiento "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;y autónomas al momento de resolver cualquier problema.<br/>Cuando aprendemos a "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;y convivir, logramos entablar mejores relaciones humanas, pues el reconocimiento "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;mejora la interacción e "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;entre los individuos."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras para completar la siguiente información."
        }
    },
    {
            "respuestas": [
                {
                    "t13respuesta": "valor",
                    "t17correcta": "1"
                },
               {
                    "t13respuesta": "cualidad",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "pleno",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "calidad",
                    "t17correcta": "4"
                },
                {
                    "t13respuesta": "prudencia",
                    "t17correcta": "5"
                },
                {
                    "t13respuesta": "cívico",
                    "t17correcta": "6"
                }
            ],
            "preguntas": [
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "Un "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "&nbsp;indica que es virtud o "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "&nbsp;que caracteriza al ser humano como tal y cuya práctica constante brinda un desarrollo "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "&nbsp;al individuo.<br/>Los valores humanos, entonces, nos dan pautas de "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "&nbsp;y crecimiento personal. Es posible clasificar a los valores humanos en distintos tipos. Un ejemplo de valor moral es la "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "; mientras que la tolerancia podemos clasificarla como un valor "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "."
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Arrastra y ordena cada palabra donde corresponda para completar el texto."
            }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "obligacion",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "congruencia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "repercusion",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "conviccion",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "deseo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "necesidad",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Satisfacción fisiológica, afectiva, intelectual, económica, etcétera."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Exigencia establecida por la moral para asumir ciertas conductas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "El gusto por lograr un objetivo o meta"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Seguridad que cada individuo tiene sobre sus propias ideas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Tener coherencia entre dos elementos o cosas, ser lógico u oportuno."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Influencia de determinada cosa en un asunto o efecto que causa en él."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 15,
                "datoY" : 0
            },
            {
                "direccion" : 0,
                "datoX" : 7,
                "datoY" : 6
            },
            {
                "direccion" : 1,
                "datoX" : 13,
                "datoY" : 3
            },
            {
                "direccion" : 0,
                "datoX" : 5,
                "datoY" : 12
            },
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 8
            },
            {
                "direccion" : 0,
                "datoX" : 0,
                "datoY" : 8
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto" : 14,
            "ancho" :20,
            "t11pregunta": "Completa el siguiente crucigrama utilizando las definiciones que se presentan. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Propone reglas de conducta que rigen la vida del hombre entre las que destacan los valores.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Serie de conceptos encaminados a demostrar algo o a persuadir o mover a oyentes o lectores.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Conjunto de conocimientos que tiene una persona y que comparte con la comunidad, que le permiten juzgar acontecimientos de una forma razonable.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Hace la reflexión para definir las causas por las que tal o cual acción es correcta o no. ",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Moral"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Razonamiento"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Sentido común"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ética"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las siguientes definiciones con la vida moral de la vida humana."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La libertad es un derecho universal pero no se necesita para la toma de decisiones.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A pesar del razonamiento, el ser humano suele actuar por instinto.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La razón es una condición que compartes con los animales, ya que se producen consecuencias que repercuten sobre el resto de la sociedad y los individuos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando actúas instintivamente, dejas un poco al lado a la razón, para sólo reaccionar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando evalúas situaciones y reaccionas tomando en cuenta las consecuencias utilizas tu razonamiento.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según sea el caso.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "La tecnología y los avances científicos están al alcance de todos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lo que hoy es nuevo, en unos meses sigue siendo vigente, dentro del ámbito tecnológico.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Tienes acceso a gran cantidad de información por medios como Internet.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No necesitas comprar instrumentos tecnológicos constantemente porque sirven para toda la vida.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Te puedes comunicar de manera instantánea, prácticamente a cualquier parte del mundo. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Encontrar un trabajo, se vuelve complicado y debes prepararte más, ya que compites con gente de todo el mundo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El crecimiento de un país está ligado al nivel de acceso de la tecnología que tiene.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las oraciones que hablen de los cambios a los que te enfrentas en la sociedad contemporánea."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La formación ética es el tipo de decisiones que puedes tomar en todas las situaciones de tu vida.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La formación ética radica en la reflexión de la práctica de todos nuestros deseos e ilusiones.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La práctica de los principios y valores universales implica la práctica de la formación ética.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al construir la autonomía en tus decisiones, te vuelves más responsable e independiente.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando actúas éticamente, reflexionas sobre las cosas que son adecuadas o no.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un juicio individual y autónomo está relacionado con buscar satisfacer tus necesidades sin pensar en consecuencias o en las necesidades de los demás.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según sea el caso.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "acuerdos",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "cívicos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "éticos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "validez",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "voluntad",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Los seres humanos, por nuestra capacidad de razonamiento, logramos participar en la definición de "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;mismos que requieren de elementos "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;y principios "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ". Estos acuerdos pueden ser verbales o escritos, ambos tienen la misma "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", pues en ellos imperan el respeto, la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;y las resoluciones objetivas."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra y ordena cada palabra donde corresponda para completar el texto."
        }
    }
];
