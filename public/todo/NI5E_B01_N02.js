json=[
    {
        "respuestas": [
            {
                "t13respuesta": "pieza",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "unidad",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "plato",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "rebanada",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "trozo",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "taza",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "porción",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;El plato del buen comer es una guía de alimentación para la población mexicana. Si sigues las recomendaciones del plato del buen comer, tu alimentación será sana. Una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;es una medida de la cantidad de alimentos que debes ingerir de cada uno de los grupos. Pero, ¿cómo la mido? Es muy sencillo, las puedes medir por: una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;(una manzana, una naranja), una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;(de melón o sandía que quepa en tu mano) una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;o de (de leche, de jugo natural), un "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;(de carme del tamaño de tu mano), un "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;(de fruta, de lentejas), una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;(de pan, una tortilla)"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo con las caraterísticas del plato del buen comer."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Manzanas, naranjas, papayas, lechuga</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Fruta y Verdura</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Carbohidratos</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Harina, papa, arroz</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Leguminosas y alimentos de origen animal</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Frijoles, huevos, leche, queso, pollo, bistec</p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra las cantidades de alimentos que debes de comer, de acuerdo con el plato del buen comer.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Muchas",
            "Suficientes",
            "Pocos"
        ]
    },
       {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Refrescos y aguas de sabor.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Jugos de fruta, leche entera, bebidas deportivas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Café y té sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Leche descremada, bebidas de soya sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Agua potable</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Refrescos y aguas de sabor.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Jugos de fruta, leche entera, bebidas deportivas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Café y té sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Leche descremada, bebidas de soya sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Agua potable</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Refrescos y aguas de sabor.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Jugos de fruta, leche entera, bebidas deportivas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Café y té sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta":"<p>Leche descremada, bebidas de soya sin azúcar.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Agua potable</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Refrescos y aguas de sabor.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Jugos de fruta, leche entera, bebidas deportivas.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Café y té sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta":"<p>Leche descremada, bebidas de soya sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Agua potable</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Refrescos y aguas de sabor.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Jugos de fruta, leche entera, bebidas deportivas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Café y té sin azúcar.</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         }, 
         {  
            "t13respuesta":"<p>Leche descremada, bebidas de soya sin azúcar.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Agua potable</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Elije de los siguientes alimentos, aquellos que correspondan.<br/>No se recomienda su consumo.","Es la más importante y deben de consumirse de 6 a 8 vasos al día.",
                         "Se recomienda consumir de 0 a 2 vasos al día.","Se deben de evitar al máximo o sólo beber de 0 a medio vaso al día.",
                         "Se recomienda beber de 0-4 vasos al día, si se trata de bebidas no calóricas con edulcorantes artificiales, es preferible evitarlas o sólo beber 2 vasos al día."],
         "preguntasMultiples": true,
         "columnas":1
      }
   }, 
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Menos de 14.1</p>",
                "t17correcta": "0,0"
            },
            {
                "t13respuesta": "<p>Menos de 14.3</p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p>14.1 a 17.5</p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p>14.3 a 17.2</p>",
                "t17correcta": "1,1"
            },
            {
                "t13respuesta": "<p>17.5 a 20.7</p>",
                "t17correcta": "2,0"
            },
            {
                "t13respuesta": "<p>17.2 a 20.6</p>",
                "t17correcta": "2,1"
            },
            {
                "t13respuesta": "<p>20.7 a 22.6</p>",
                "t17correcta": "3,0"
            },
            {
                "t13respuesta": "<p>20.6 a 21.9</p>",
                "t17correcta": "3,1"
            },
            {
                "t13respuesta": "<p>24 a 27.8</p>",
                "t17correcta": "4,0"
            },
            {
                "t13respuesta": "<p>25.4 a 27.8</p>",
                "t17correcta": "4,1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Ordena dentro de los siguientes índices de masa corporal correspondientes a niños de tu edad (11 a 13 años) lo correspondiente con su condición corporal.",
            //"tipo": "matrizHorizontal"
        },
        "contenedores": [
            "Desnutrición",
            "Peso bajo",
            "Peso normal",
            "Sobrepeso",
            "Obesidad"
        ],
        "contenedoresFilas": [
            "Niñas",
            "Niños"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "primer",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "UNICEF",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "malos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "70%",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "azúcares",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "grasas",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "elevado",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "obesidad",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;La obesidad es uno de los principales problemas de salud en el país. México ocupa el "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;lugar de obesidad a nivel mundial, según datos de la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br>La principal causa de la obesidad son los "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;hábitos alimenticios. El alto consumo de comida chatarra ha provocado que en México el "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;de los niños obesos seguirá siéndolo cuando sean adultos. Al preferir alimentos ricos en carbohidratos ("
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ") y grasas, se ingieren grandes cantidades de energía que se almacena en el cuerpo como "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".&nbsp;Aunado a la falta de ejercicio, gastar esas grandes cantidades de energía que comiste, es una tarea muy difícil para el cuerpo, por lo que éste va acumulando toda esa energía en forma de grasa que se refleja en un peso corporal "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br> El exceso de golosinas, alimentos fritos, panes, comida rápida, refrescos, bebidas endulzadas y jugos industrializados ha aumentado de una forma alarmante la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "en México."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras que te ayuden a completar el siguiente párrafo que habla sobre la obesidad."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para tener una buena alimentación debemos de preferir alimentos con alto contenido calórico y comer comida sin restricciones.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Adquirir el hábito y disciplina de comer alimentos con bajo contenido calórico, puede ser una gran opción para mantenerte saludable.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los horarios de comida no son importantes para tener una buena alimentación, sólo es necesario comer saludablemente.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las comidas son importantes, pero la que se le debe de dar más importancia es al desayuno.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ejercicio debe ser una actividad que realicemos pero puede ser esporádico y no es necesario practicarlo diariamente.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la respuesta correcta.",
            "evaluable": true,
            "anchoColumnaPreguntas": 60,
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Es una enfermedad física psicoemocional de dependencia de una sustancia o una actividad.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Consumo mínimo, en pequeñas cantidades, no ocasiona graves transtornos en el comportamiento ni en la salud.</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Consumo problematico con consecuencias negativas relacionadas con un deterioro orgánico o psicosocial.</p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>El consumo forma parte del estilo de vida de la presona y lo necesita para desarrollar sus actividades cotidianas.</p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Adicción"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Uso"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Abuso"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Dependencias"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Arrastra los siguientes conceptos con la definición que corresponda."
        }
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>La nicotina es la sustancia activa. se producen más de 4,000 sustancias químicas tóxicas al quemar la hoja del tabaco.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Causa padecimientos de corazón aumento en la presión arterial que puede afectar órganos como el corazón cerebro y riñones.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Sustancia adictiva que provoca daños severos premanentes en el órganos como el hígado, riñones, corazón, etc. Se disminuye la capacidad del corazón para bombear sangre.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Causa padecimientos de corazón aumento en la presión arterial que puede afectar órganos como el corazón cerebro y riñones.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Sustancia adictiva que provoca daños severos premanentes en el órganos como el hígado, riñones, corazón, etc. Se disminuye la capacidad del corazón para bombear sangre.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Provoca desinhibición, dificulta el control de los movimientos, provoca pérdida de reflejos y en mujeres embarazadas, puede provocar malformaciones fisicas, retraso físico y mental.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Sustancia adictiva que provoca daños severos premanentes en el órganos como el hígado, riñones, corazón, etc. Se disminuye la capacidad del corazón para bombear sangre.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Provoca desinhibición, dificulta el control de los movimientos, provoca pérdida de reflejos y en mujeres embarazadas, puede provocar malformaciones fisicas, retraso físico y mental.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Sustancias que modifican una o varias funciones en el organismo, pueden causar efectos estimulantes, depresivos o alucinógenos.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las características de las siguientes sustancias adictivas.<br/>Tabaco <br/><img src='ni5e_b01_n02_01.png'>","Alcohol<br/><img src='ni5e_b01_n02_02.png'>","Drogas<br/><img src='ni5e_b01_n02_03.png'>"],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
       {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Primera fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Segunda fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Tercera fase</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Cuarta fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Primera fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Segunda fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Tercera fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Cuarta fase</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Primera fase</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Segunda fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Tercera fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta":"<p>Cuarta fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Primera fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Segunda fase</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Tercera fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta":"<p>Cuarta fase</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": [
"Seleciona las fases del ciclo menstrual.<br/>En esta fase el endometrio sigue engrosándose y el óvulo maduro sale del ovario para tema trasladarse hasta el útero. La ovulación ocurre entre los días 11 y 16 antes del siguiente ciclo menstrual. Esta fase también se conoce como el periodo fértil pues son los únicos días en los que el óvulo puede encontrarse con un espermatozoide y engendrar un nuevo ser mediante un embarazo.",
"En esta fase, en el ovario se producen estrógenos y progesterona, que son hormonas que mantendrán al endometrio para alojar al nuevo ser en caso de un embarazo. En caso de no haber embarazo, la producción de estrógenos y progesterona disminuirá al cabo de dos semanas, favoreciendo el desprendimiento del endometrio con lo cual comenzará nuevamente otro ciclo menstrual.",
"Esta fase se caracteriza por un sangrado vaginal que dura de 3 a 5 días. Este sangrado se origina por el desprendimiento del endometrio que es la capa interna del útero. La sangre menstrual fluye desde el útero hasta el orificio vaginal. La cantidad de sangre que se pierde en cada ciclo es alrededor de 35ml. El primer día del ciclo menstrual es el primer día de sangrado vaginal.",
"En esta fase los ovarios producen estrógenos que participan en el engrosamiento del endometrio que se prepara para un posible embarazo. Así mismo el óvulo comienza a madurar por la acción de las hormonas folículo estimulante y leuteinizante. Esta fase se da entre los días 6 y 13 del ciclo menstrual."],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
  {
        "respuestas": [
            {
                "t13respuesta": "<p>reproducción<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>vagina<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>útero<\/p>",
                "t17correcta": "3"
            }, 
            {
                "t13respuesta": "<p>Falopio<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>fecundación<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>cigoto<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>40<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>El proceso de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;humana comienza con una relación sexual, en la cual los millones de espermatozoides del varón son depositados en la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de la mujer. Una vez en la vagina, la mayoría de los espermatozoides mueren y algunos otros entrarán al <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;para realizar un viaje que dura aproximadamente dos días y en el que solo los más fuertes llegarán a las trompas de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, en donde tendrá lugar la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, que es la unión de un óvulo y un espermatozoide. La unión del óvulo y el espermatozoide dará lugar a la formación de una célula llamada huevo o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, a partir de la cual se generará un nuevo ser. El desarrollo de este nuevo ser dentro del vientre materno se denomina embarazo y dura aproximadamente <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>semanas hasta su nacimiento.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras que te ayuden a completar el siguiente párrafo que nos habla sobre las etapas del proceso de reproducción humana."
        }
    }
]