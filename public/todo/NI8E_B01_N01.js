json=[  
	{
		"respuestas": [
		{
			"t13respuesta": "<p>15 m<\/p>",
			"t17correcta": "1"
		},
		{
			"t13respuesta": "<p>20 s<\/p>",
			"t17correcta": "2"
		},
		{
			"t13respuesta": "<p>30 s<\/p>",
			"t17correcta": "3"
		},
		{
			"t13respuesta": "<p>45 m<\/p>",
			"t17correcta": "4"
		},
		{
			"t13respuesta": "<p>40 s<\/p>",
			"t17correcta": "5"
		},
		{
			"t13respuesta": "<p>60 m<\/p>",
			"t17correcta": "6"
		}
		],
		"preguntas": [
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": ""
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": ""
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": ""
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": ""
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": ""
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": ""
		},
		{
			"c03id_tipo_pregunta": "8",
			"t11pregunta": ""
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"ocultaPuntoFinal":true,
			"respuestasLargas":true,
			"anchoRespuestas": 80,
			"soloTexto":true,
			"pintaUltimaCaja":false,
			"t11pregunta": "Realiza la actividad."
		}
	},
	{  
		"respuestas":[  
		{  
			"t13respuesta":"Una bicicleta tirada en el patio al lado de la ventana.",
			"t17correcta":"0"
		},
		{  
			"t13respuesta":"El Sol rojo elevándose durante el amanecer en la playa.",
			"t17correcta":"0"
		},
		{  
			"t13respuesta":"Una patineta moviéndose a cinco kilómetros por hora en promedio.",
			"t17correcta":"0"
		},
		{  
			"t13respuesta":"Un barco que viaja a treinta nudos hacia el Sur.",
			"t17correcta":"1"
		}
		],
		"pregunta":{  
			"c03id_tipo_pregunta":"1",
			"t11pregunta":"Selecciona la opción que refiere a una velocidad. Sugerencia: Recuerda que debe cumplir con las características de un vector."
		}
	},
	{  
		"respuestas":[  
		{  
			"t13respuesta":"Una bicicleta tirada en el patio al lado de la ventana.",
			"t17correcta":"0"
		},
		{  
			"t13respuesta":"El Sol rojo elevándose durante el amanecer en la playa",
			"t17correcta":"0"
		},
		{  
			"t13respuesta":"Una patineta moviéndose a cinco kilómetros por hora en promedio.",
			"t17correcta":"1"
		},
		{  
			"t13respuesta":"Un barco que viaja a treinta nudos hacia el Sur.",
			"t17correcta":"0"
		}
		],
		"pregunta":{  
			"c03id_tipo_pregunta":"1",
			"t11pregunta":"De las opciones anteriores, ¿Cuál corresponde a rapidez?"
		}
	},
	{
		"respuestas": [
		{
			"t13respuesta": "ni8e_b01_n01_01_02.png",
			"t17correcta": "0,1,2,3",
			"columna":"0"
		},
		{
			"t13respuesta": "ni8e_b01_n01_01_02.png",
			"t17correcta": "1,0,2,3",
			"columna":"0"
		},
		{
			"t13respuesta": "ni8e_b01_n01_01_02.png",
			"t17correcta": "2,0,1,3",
			"columna":"1"
		},
		{
			"t13respuesta": "ni8e_b01_n01_01_02.png",
			"t17correcta": "3,0,1,2",
			"columna":"1"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p>Coloca los puntos en las coordenadas que corresponden.<\/p>",
			"tipo": "ordenar",
			"imagen": true,
			"url":"ni8e_b01_n01_01_01.png",
			"respuestaImagen":true, 
			"bloques":false,
			"opcionesTexto": true,	
			"tamanyoReal": true,
			"borde":false

		},
		"contenedores": [
		{"Contenedor": ["", "365,67", "cuadrado", "45, 45", ".","transparent",true]},
		{"Contenedor": ["", "317,163", "cuadrado", "45, 45", ".","transparent",true]},
		{"Contenedor": ["", "271,258", "cuadrado", "45, 45", ".","transparent",true]},
		{"Contenedor": ["", "222,353", "cuadrado", "45, 45", ".","transparent",true]}
		]
	},
	{
		"respuestas": [
		{
			"t13respuesta": "EJEMPLO.png",
			"t17correcta": "0,1,2,3",
			"columna":"0"
		},
		{
			"t13respuesta": "EJEMPLO.png",
			"t17correcta": "1,0,2,3",
			"columna":"0"
		},
		{
			"t13respuesta": "EJEMPLO.png",
			"t17correcta": "2,0,1,3",
			"columna":"1"
		},
		{
			"t13respuesta": "EJEMPLO.png",
			"t17correcta": "3,0,1,2",
			"columna":"1"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p><\/p>",
			"tipo": "ordenar",
			"imagen": true,
			"url":"EJEMPLO.png",
			"respuestaImagen":true, 
			"bloques":false,
			"opcionesTexto": true,	
			"tamanyoReal": true,
			"borde":false

		},
		"contenedores": [
		{"Contenedor": ["", "365,67", "cuadrado", "45, 45", ".","transparent",true]},
		{"Contenedor": ["", "317,163", "cuadrado", "45, 45", ".","transparent",true]},
		{"Contenedor": ["", "271,258", "cuadrado", "45, 45", ".","transparent",true]},
		{"Contenedor": ["", "222,353", "cuadrado", "45, 45", ".","transparent",true]}
		]
	}, 
	{
		"respuestas": [
		{
			"t13respuesta": "NI8E_B01_N01_03_02.png",
			"t17correcta": "0",
			"columna":"0"
		},
		{
			"t13respuesta": "NI8E_B01_N01_03_05.png",
			"t17correcta": "1",
			"columna":"0"
		},
		{
			"t13respuesta": "NI8E_B01_N01_03_04.png",
			"t17correcta": "2",
			"columna":"1"
		},
		{
			"t13respuesta": "NI8E_B01_N01_03_06.png",
			"t17correcta": "3",
			"columna":"1"
		},
		{
			"t13respuesta": "NI8E_B01_N01_03_03.png",
			"t17correcta": "4",
			"columna":"0"
		}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
                            "t11pregunta": "<p>Coloca la palabra correspondiente a cada parte de la onda.<\/p>",
			"tipo": "ordenar",
			"imagen": true,
			"url":"NI8E_B01_N01_03_01.png",
			"respuestaImagen":true, 
			"bloques":false,
			"opcionesTexto": true,	
			"tamanyoReal": true,
			"borde":false

		},
		"contenedores": [
		{"Contenedor": ["", "122,16", "cuadrado", "233, 45", ".","transparent"]},
		{"Contenedor": ["", "23,289", "cuadrado", "233, 45", ".","transparent"]},
		{"Contenedor": ["", "117,395", "cuadrado", "233, 45", ".","transparent"]},
		{"Contenedor": ["", "383,156", "cuadrado", "232, 45", ".","transparent"]},
		{"Contenedor": ["", "457,28", "cuadrado", "232, 45", ".","transparent"]}
		]
	},
]