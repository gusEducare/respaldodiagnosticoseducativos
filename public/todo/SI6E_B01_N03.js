json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Identificación del programa (nombre, horario, nombre de los locutores y estación en la que se transmite).",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presenta el tema a tratar así como a los locutores e invitados.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Son las partes musicales que sirven para diferenciar las secciones dentro del programa.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Corresponden a los bloques temáticos del programa.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Indican los cortes comerciales o de promoción.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "El locutor se despide y menciona los créditos del programa.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Encabezado"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Entradilla"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cortinilla"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Secciones"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cuñas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cierre"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Recuerda lo que sabes de los guiones radiofónicos y relaciona ambas columnas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Exámenes orales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Exámenes de desarrollo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Exámenes escritos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Exámenes de opciones múltiples",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No profundiza en el tema.",
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El alumno debe desarrollar las respuestas para demostrar su aprendizaje.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El alumno debe seleccionar; de entre varias, la respuesta correcta.",
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Queda evidencia en papel.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El alumno recurre a la argumentación verbal.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se puede permitir al alumno que expresen con sus propias palabras lo que aprendió.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se dividen en varios tipos: cuestionario, desarrollo, opción múltiple, resolución de problemas, entre otros.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se exige al estudiante responder las preguntas con argumentos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Suele ser corto y rápido.",
                "correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona el tipo de examen que le corresponde a cada característica<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 35,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "guion de radio",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "micrófono",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "radio escucha",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "productor",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "locutor",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Sirve para que el locutor se guié y para programar lo que va a pasar en el programa."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Aparato que se usa para transformar las ondas sonoras en energía eléctrica y viceversa."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Público que sigue la transmisión de un programa de radio."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Persona responsable de organizar los recursos humanos y técnicos necesarios para realizar la emisión de radio"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Persona calificada para hablar a través de la radio, como narrador."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 8,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 1,
                "datoY": 5
            },
            {
                "direccion": 0,
                "datoX": 5,
                "datoY": 13
            },
            {
                "direccion": 1,
                "datoX": 15,
                "datoY": 8
            },
            {
                "direccion": 0,
                "datoX": 10,
                "datoY": 10
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Recuerda lo que aprendiste de los programas radiofónicos y resuelve el crucigrama.",
            "alto": 15,
            "ancho": 20
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "ambiguo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "entenderse",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "varias",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "reactivos",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "genera",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "confusión",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "respuesta",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "estrategias",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "detenidamente",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "contexto",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "puntuación",
                "t17correcta": "11"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br>Se dice que algo es "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;cuando puede "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;de "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;maneras.  En ocasiones ocurre que en un examen los "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;son ambiguos y esto "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;dudas y "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;sobre cuál es la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;correcta puesto que no se sabe con precisión qué se está preguntando.<br/>Algunas "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;que puedes utilizar para resolver este tipo de preguntas son: Leer "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;la pregunta,  tratar de comprender el significado de la misma por el "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", o corregir la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;del texto."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Da congruencia al texto ordenando las palabras propuestas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Los hechos son contados por alguien que no es de quien se habla.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Su voz narrativa es en tercera persona</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los verbos están conjugados en primera persona.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se recurre a diversas fuentes para recopilar información sobre la vida y obra de la persona de quien se escribe.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Su voz narrativa es en primera persona.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La fuente de información es la propia persona que escribe el texto.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los verbos están conjugados en tercera persona.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los hechos son contados por la persona que los vivió.</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor lo que identifica a cada tipo de texto.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Biografía",
            "Autobiografía"
        ]
    }
];