json=[
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPara que se note nuestra cultura\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara evitar la repetición\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara evitar aburrir al lector\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara que el texto sea más interesante\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Menciona el objetivo de utilizar  sinónimos y pronombres al realizar una biografía."
      }
   },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ERecursos discursivos\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ERecursos didácticos\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ERecursos emocionales\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Ironía, persuasión y carga emotiva son ejemplos de"
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>género<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>biografía<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>tercera<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>destacado<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>contexto<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>nacimiento<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>momentos<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>influir<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>muerte<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>función<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>proporcionar<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>detallar<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>personaje<\/p>",
                "t17correcta": "13"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><br/>Otro <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;literario es la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, en esta el autor relata en <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;persona, la vida y obra de algún personaje <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;en diversas áreas de conocimiento.<br/>Toma en cuenta el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>y comienzan desde el momento de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Además toma revela <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;personales y profesionales pudieron <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;en sus obra o logros. El relato concluye con la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;del personaje.<br/>La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;principal de las biografías es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;información y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;aspectos desconocidos del <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ECopretérito \u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPretérito\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Cuál de las opciones es un tiempo verbal que expresa una acción que sucedió en el pasado pero que no necesariamente ha culminado?"
      }
   },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EResumen\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EReporte\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENoticia\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EArticulo\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnsayo\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Elige el concepto que refleje todas las caracteristicas descritas. "
      }
   }
]