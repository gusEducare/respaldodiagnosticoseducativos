[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Regla establecida por una autoridad superior para regular, de acuerdo con la justicia, algun aspecto de las relaciones sociales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"></p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"></p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Une con líneas la definición y los ejemplos de lo que es una ley y una norma.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ley",
            "Norma"
        ]
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Propiciar que los demás hablen<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ser honesto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Interrumpir a los demás mientras hablan<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Promover discusiones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Solucionar tods los problemas de los demás<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Promover",
            "Evitar"
        ]
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>No jugar con la comida<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Respetar a las personas mayores<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Contar con el derecho a trabajar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tener derecho a estudiar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Contar con el derecho a votar con 18 años cumplidos<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Normas implícitas",
            "Normas explícitas"
        ]
    },
     {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una sociedad organizada puede sobrevivir sin que se apliquen leyes o normas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las sanciones son necesarias para que las personas cumplan con las normas y leyes.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La injusticia surge cuando un organismo encargado de vigilar y cumplir las leyes, no lo hace.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos con los que contamos están plasmados en la Constitución.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "-La equidad implica que todos podamos tener los mismos salarios y poder comprar las mismas cosas, sin importar el trabajo que realicemos.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    }
]