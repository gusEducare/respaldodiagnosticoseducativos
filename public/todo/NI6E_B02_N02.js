json = [
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Aprovechamiento sustentable de recursos naturales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sobre explotación de recursos naturales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Beneficios económicos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Erosión y pérdida de ecosistemas y recursos naturales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Beneficios sociales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La generación excesiva de residuos y desechos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Contaminación por manejo inadecuado de los residuos.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Impactos positivos",
            "Impactos negativos"
        ]
    },
    {
    "respuestas": [
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Falso",
            "t17correcta": "1"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Se nombra evolución a los cambios graduales que sufren los organismos a lo largo de miles o millones de años.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La evolución de los organismos se realiza en pocos años.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La Tierra ha sufrido cambios graduales a lo largo del tiempo.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La tierra no se ha modificado desde que surgió.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El origen de las primeras formas de vida en la Tierra es de 3,800 millones de años atrás.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El origen del hombre en la tierra fue en la Era Paleozoica.",
            "correcta"  : "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p><\/p>",
        "descripcion": "",   
        "variante": "editable",
        "anchoColumnaPreguntas": 60,
        "evaluable"  : true
    }        
},
     {
        "respuestas": [
            {
                "t13respuesta": "<p>La lluvia ácida<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elevación de precios<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Destrucción de flora y fauna<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Adicción a sustancias toxicas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La destrucción de la capa de ozono<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Deserción escolar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cambio climático<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Daños a la salud<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Embarazo temprano<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El calentamiento global<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala los que reconozcas como efectos de la contaminación."
        }
    },    
        {
        "respuestas": [
        {
        "t13respuesta": "<p>Extinción<\/p>",
                "t17correcta": "1"
        },
        {
        "t13respuesta": "<p>Estrato<\/p>",
                "t17correcta": "0"
        },
        {
        "t13respuesta": "<p>Petróleo<\/p>",
                "t17correcta": "0"
        },
        {
        "t13respuesta": "<p>Fósiles<\/p>",
                "t17correcta": "0"
        }
        ],
                "preguntas": [
                {
                "c03id_tipo_pregunta": "15",
                        "t11pregunta": "Se refiere a la desaparición de todos los individuos que forman úna especie.”"
                },
                {
                "c03id_tipo_pregunta": "15",
                        "t11pregunta": "Así se nombra a la capa de sedimiento en la Tierra que permiten datar los fósiles."
                },
                {
                "c03id_tipo_pregunta": "15",
                        "t11pregunta": "Recurso fósil que se transforma en combustible y es un motor de la economía mundial."
                },
                {
                "c03id_tipo_pregunta": "15",
                        "t11pregunta": "Son vestigios de seres vivos que vivieron hace muchos millones de años."
                }
                ],
                "pocisiones": [
                {
                "direccion" : 1,
                        "datoX" : 3,
                        "datoY" : 1
                },
                {
                "direccion" : 0,
                        "datoX" : 3,
                        "datoY" : 1
                },
                {
                "direccion" : 0,
                        "datoX" : 1,
                        "datoY" : 3
                },
                {
                "direccion" : 0,
                        "datoX" : 2,
                        "datoY" : 8
                }
                ],
                "pregunta": {
                "c03id_tipo_pregunta": "15",
                "alto" : 10,
                        "t11pregunta": "Delta sesion 12 a3"
                }
        },
            {
        "respuestas": [
            {
                "t13respuesta": "<p>Adquirir productos locales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Comprar solo productos orgánicos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Concientizarse del impacto social y ambiental de los productos que se compran<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Comprar solo lo necesario.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al comprar elegir productos sin empaque.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sembrar en casa tus alimentos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Preferir productos locales sobre los extranjeros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Comprar productos que provengan de empresas amigables con el ambiente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Programar tus compras y evitar el impulso.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Comprar en las baratas por cambio de temporada.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Reducir el consumo.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala todas las acciones que favorecen el consumo sustentable."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Se desarrollan los reptiles, y aparecen los dinosaurios.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se forman los primeros organismos unicelulares.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Aparecen los primeros seres vivos unicelulares, como los caracoles, los trilobites, los primeros peces, reptiles.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Se forman las primeras sustancias orgánicas.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Aparece el hombre.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Al final de esta era se forma el súper continente llamado Pangea.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mesozoica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Precámbrico"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Paleozoica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Era Azoica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cenozoica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Paleozoica"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "Protege a los seres vivos de la radiación ultravioleta que envía el Sol. Está siendo destruida por los cloroflurocabonos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es una consecuencia del calentamiento global",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Es ocasionada por los gases producidos durante la quema de combustibles fósiles principalmente en la industria y en el transporte.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Se refiere al aumento de la temperatura atmosférica por la acumulación de gases.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "La capa de ozono"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cambio climático"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "La lluvia ácida"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Calentamiento global"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Nadar en altamar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Utilizar transporte que usa combustibles fósiles.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Fumar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Incendios forestales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ganadería.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Siembra.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Quema de basura.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Industria textil.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Metalurgia.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Utilización de aerosoles.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala todas las actividades que provocan mayor contaminación del aire."
        }
    },

{
        "respuestas": [
            {
                "t13respuesta": "Tiene que ver con las industrias, los sistemas de producción, transporte y comercialización de bienes y servicios.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se relaciona con los usos y costumbres, la religión y las tradiciones.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Comprende a las formas de gobernanza así como a la promulgación de leyes y/o normas dirigidas a tener orden, una convivencia sana, armónica, pacífica, respetuosa y sustentable.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Son elementos inertes que son fundamentales para propiciar la vida en el planeta. Entre ello: agua, aire, suelo, minerales y luz solar.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Corresponden a los seres vivos que habitan un lugar determinado.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Componente económico"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Componente cultural"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Componente político"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Factores abióticos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Factores bióticos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "<p>Biológicas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Masivas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Modificación del habitad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sobre explotación de recursos naturales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cacería.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cambios climáticos.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las que sean causas de la extinción de los seres vivos del planeta."
        }
    }
]