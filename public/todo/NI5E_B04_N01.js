json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El cuerpo sigue une parábola durante su movimiento, por ejemplo el lanzamiento de una bala, una pelota, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es el movimiento que realiza un objeto de un lado a otro, colgado de una base fija mediante un hilo o una varilla.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El cuerpo se mueve en oscilaciones o vibraciones en torno a un punto de equilibrio, por ejemplo: un resorte, un péndulo, un sismo, la fuente vibratoria del sonido de un tambor, una ola, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es la línea que describe el sentido del movimiento marcada del punto inicial al punto final y se expresa de acuerdo a los cuatro puntos cardinales.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El cuerpo se desplaza dibujando una circunferencia en su movimiento, por ejemplo, un reloj, la rueda de la fortuna, una licuadora, un ventilador, etc.</p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Dirección"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El cuerpo sigue une parábola durante su movimiento, por ejemplo el lanzamiento de una bala, una pelota, etc.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es el movimiento que realiza un objeto de un lado a otro, colgado de una base fija mediante un hilo o una varilla.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El cuerpo se mueve en oscilaciones o vibraciones en torno a un punto de equilibrio, por ejemplo: un resorte, un péndulo, un sismo, la fuente vibratoria del sonido de un tambor, una ola, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es la línea que describe el sentido del movimiento marcada del punto inicial al punto final y se expresa de acuerdo a los cuatro puntos cardinales.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El cuerpo se desplaza dibujando una circunferencia en su movimiento, por ejemplo, un reloj, la rueda de la fortuna, una licuadora, un ventilador, etc.</p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Trayectoria parabólica"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El cuerpo sigue une parábola durante su movimiento, por ejemplo el lanzamiento de una bala, una pelota, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es el movimiento que realiza un objeto de un lado a otro, colgado de una base fija mediante un hilo o una varilla.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El cuerpo se mueve en oscilaciones o vibraciones en torno a un punto de equilibrio, por ejemplo: un resorte, un péndulo, un sismo, la fuente vibratoria del sonido de un tambor, una ola, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es la línea que describe el sentido del movimiento marcada del punto inicial al punto final y se expresa de acuerdo a los cuatro puntos cardinales.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El cuerpo se desplaza dibujando una circunferencia en su movimiento, por ejemplo, un reloj, la rueda de la fortuna, una licuadora, un ventilador, etc.</p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Trayectoria pendular"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El cuerpo sigue une parábola durante su movimiento, por ejemplo el lanzamiento de una bala, una pelota, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es el movimiento que realiza un objeto de un lado a otro, colgado de una base fija mediante un hilo o una varilla.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El cuerpo se mueve en oscilaciones o vibraciones en torno a un punto de equilibrio, por ejemplo: un resorte, un péndulo, un sismo, la fuente vibratoria del sonido de un tambor, una ola, etc.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es la línea que describe el sentido del movimiento marcada del punto inicial al punto final y se expresa de acuerdo a los cuatro puntos cardinales.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El cuerpo se desplaza dibujando una circunferencia en su movimiento, por ejemplo, un reloj, la rueda de la fortuna, una licuadora, un ventilador, etc.</p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Trayectoria oscilatorio o vibratorio."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El cuerpo sigue une parábola durante su movimiento, por ejemplo el lanzamiento de una bala, una pelota, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es el movimiento que realiza un objeto de un lado a otro, colgado de una base fija mediante un hilo o una varilla.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El cuerpo se mueve en oscilaciones o vibraciones en torno a un punto de equilibrio, por ejemplo: un resorte, un péndulo, un sismo, la fuente vibratoria del sonido de un tambor, una ola, etc.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es la línea que describe el sentido del movimiento marcada del punto inicial al punto final y se expresa de acuerdo a los cuatro puntos cardinales.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El cuerpo se desplaza dibujando una circunferencia en su movimiento, por ejemplo, un reloj, la rueda de la fortuna, una licuadora, un ventilador, etc.</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Trayectoria circular"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Más rápido<p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Más lento<p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Más lento<p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Más lento<p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Más rápido<p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>Más lento<p>",
                "t17correcta": "0",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "esp_kb1_s01_a01_01.png",
            "respuestaImagen": true,
            "bloques": false

        },
        "contenedores": [
            { "Contenedor": ["", "201,0", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "201,81", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "201,162", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "401,0", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "401,81", "cuadrado", "78, 81", "."] },
            { "Contenedor": ["", "401,162", "cuadrado", "78, 81", "."] },
        ]
    },




    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La corriente eléctrica es el flujo de protones que viajan en una dirección a través de un material no conductor.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los electrones necesitan de una fuente de energía como una batería para que puedan moverse.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La batería tiene tres polos: uno negativo, uno positivo y uno neutro.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La corriente eléctrica se genera porque los electrones viajan del polo negativa al de polo positivo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La ley de Coulomb establece que cuando dos cuerpos tienen la misma carga eléctrica se rechazan; cuando tienen cargas distintas se atraen y crean corriente eléctrica.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si colocamos pilas en un aparato eléctrico del mismo lado, enciende:",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La corriente eléctrica se mide en wats y existen de dos tipos: la continua y la alterna.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>interruptor:<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>conductor:<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>fusible:<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>receptor:<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>generador:<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Elemento de mando que permite al usuario cortar o dirigir la corriente eléctrica a voluntad como apagadores."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es el material por donde se  mueve la corrient eléctronica, normalmente son metale como cobre o aluminio o cualquier otro metal."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Elementos de protección que resguardna a los elementos del circuito cuando hay alteraciones en la corriente eléctronica."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Objetos que transforman la energía elétrica en otro tipo de energía o fuerza para realizar un trabajo: licuadoras, planchas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Artefactos que producen la corriente eléctronica, ya sea corriente continua o correinet alterna. Tipos: pilas, baterías."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 3,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 8,
                "datoY": 5
            },
            {
                "direccion": 1,
                "datoX": 6,
                "datoY": 3
            },
            {
                "direccion": 0,
                "datoX": 2,
                "datoY": 3
            },
            {
                "direccion": 1,
                "datoX": 10,
                "datoY": 1
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Sonora<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Térmica<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Mecánica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mecánica<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Luminosa<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Sonora<\/p>",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_a.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Completa las oraciones con el adjetivo interrogativo que corresponda. Arrastra las palabras al lugar que les corresponde. Atiende los acentos en cada caso para responder correctamente la actividad."
        }
    }
]