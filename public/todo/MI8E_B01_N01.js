json = [
    {
        "respuestas": [
            {
                "t13respuesta": "126",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "128",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "-128",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "-126",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "16 x (-8)"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "(-16)(-7)"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "(-9) x 14"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "(-7) x 18"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona cada operación con el resultado que el corresponde."
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E3<sup>4+6</sup>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E3<sup>4-6</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E3<sup>3+6</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E3<sup>12</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"3<sup>4</sup> x 3<sup>6</sup>"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E5<sup>5-7</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E5<sup>5+7</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E5<sup>5 &times; 7</sup>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E5<sup>12</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"(5<sup>5</sup>)<sup>7</sup>"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E7<sup>4</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E7<sup>9+2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E7<sup>16</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E7<sup>9-2</sup>\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"7<sup>8</sup> &divide; 7<sup>2</sup>"
      }
   },
  {  
      "respuestas":[  
         {  
            "t13respuesta": "< a y < g",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"< b y < d",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"< h y < d",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"< c y < g",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Analiza la relación entre los ángulos del siguiente trazo y determina cuál de las siguienetes parajas no miden los mismo.<div  style='text-align:center;'><img style='height: 360px' src='.png'></div></p>"
      }
   },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EInfinidad de triángulos, acomodando los segmentos de diferente forma.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENinguno, porque las medidas no cumple con los criterios.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EUn único triángulo, porque dados tres segmentos solo se puede contruir un triángulo.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETres triángulo, porque son tres lados y tres madidas diferentes.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Cuántos triángulos diferentes se pueden construir cuyas medidas sean igual a los segmentos que se muestran?<br><br><table style='background-color: #f2f2f2;   margin-left: 102px;'><tr><td><u style='font-size: 27px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td><td><u style='font-size: 27px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td><td><u style='font-size: 27px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td></tr><tr><td style='text-align: center; background-color: #f2f2f2;'>12 cm</td><td style='text-align: center; background-color: #f2f2f2;'>8 cm</td><td style='text-align: center; background-color: #f2f2f2;'>4 cm</td></tr></table>"
      }
   },
{
      "respuestas": [
          {
              "t13respuesta": "esp_kb1_s01_a01_02a.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "esp_kb1_s01_a01_02f.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "esp_kb1_s01_a01_02g.png",
              "t17correcta": "2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "bloques":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,162", "cuadrado", "78, 81", "."]}
      ]
  },
   {  
      "respuestas":[
         {  
            "t13respuesta":"\u003Cp\u003E230.4 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E284.92 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E245.76 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"\u003Cp\u003E262.82 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E230.4 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E284.92 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E245.76 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"\u003Cp\u003E262.82 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona la cantidad de cartón que se utilizó para hacerlo.<br/><img>",
                         "Selecciona la cantidad de cartón que se utilizó para hacerlo.<br/><img>"],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
{
        "respuestas": [
            {
                "t13respuesta": "<p>$3 560<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>2 654<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>$2 820<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>$3 840<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>$2 414<\/p>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
           {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div><center><table style='background-color: #f2f2f2;'><tr><th style='text-align: center;'>Producto</th><th style='text-align: center;'>Precio original</th><th style='text-align: center;'>Porcentaja de descuento</th><th style='text-align: center;'>Precio final</th></tr><tr><td style='font-size: 16px; text-align: center;'><b>Refrigerador<b></td><td style='font-size: 16px; text-align: center;'><b>$4 450<b></td><td style='font-size: 16px; text-align: center;'><b>20%<b></td><td style='font-size: 16px; text-align: center;'>"
            },{
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td style='font-size: 16px; text-align: center;'><b>Horno de microondas<b></td><td style='font-size: 16px; text-align: center;'><b>$2 840<b></td><td style='font-size: 16px; text-align: center;'><b>15%<b></td><td style='font-size: 16px; text-align: center;'><b><b>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td style='font-size: 16px; text-align: center;'><b>Teatro en casa<b></td><td style='font-size: 16px; text-align: center;'><b>$3 760<b></td><td style='font-size: 16px; text-align: center;'><b>25%<b></td><td style='font-size: 16px; text-align: center;'><b><b>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><p></table></center></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "respuestasLargas":true,
            "anchoRespuestas":150,
            "pintaUltimaCaja": false,
            //"largoRespuestas":150,
            // "soloTexto":true,
            "ocultaPuntoFinal":true,
            "contieneDistractores": true
        }
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"<table ><tr><th>Mes<td><th>Deuda al inicio del periodo<td><th>Deuda al final del periodo<td></tr><tr><td>1<td><td>$1 500<td><td>$1 560<td></tr><tr><td>2<td><td>$1 560<td><td>$1 622.4<td></tr><tr><td>3<td><td>$1 622.4<td><td>$1 687.30<td></tr><td>4<td><td>$1 687.30<td><td>$1 754.80<td></tr></table>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<table ><tr>\<th>Mes<td><th>Deuda al inicio del periodo<td><th>Deuda al final del periodo<td></tr><tr><td>1<td><td>$1 500<td><td>$1 575<td></tr><tr><td>2<td><td>$1 575<td><td>$1 650<td></tr><tr><td>3<td><td>$1 650<td><td>$1 725<td></tr><td>4<td><td>$1 725<td><td>$1 800<td></tr></table>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "columnas": 2,
         "t11pregunta":"Un banco le ofreció a Juan un crédito con dos opciones de pago. Analiza las tablas y selecciona la que representa la que representa un crédito con interés compuetos."
      }
   },
{
        "respuestas": [
            {
                "t13respuesta": "<p>más probable</p>",
                "t17correcta": "1,3"
            },
           {
                "t13respuesta": "<p>Igualmente probable</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>más probable</p>",
                "t17correcta": "3,1"
            },
            {
                "t13respuesta": "<p>menos probable</p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Al lanzar dos dados, sacar 8 es </p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; que sacar 3.<br>Al lanzar dos dados es </p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; que salga 11 a que salga 3.<br>Al lanzar dos dados es </p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; obtener 7 que sacar 6.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "ocultaPuntoFinal": true
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>La media del conjunto de datos es 8.8 km.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La mediana del conjunto de datos es 9 km.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La media es un dato representativo del entrenamiento de Manuel.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La mejor representación del entrenamiento de Manuel es la mediana.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La media se ve afectada por los valores pequeños del conjunto.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "La siguiente lista muestra el número de kilómetros que corrió Manuel en su entrenamiento durante 10 días consecutivos: <br><br>12 km, 9 km, 10 km, 3 km, 10 km, 4 km, 9 km, 12 km, 9 km, 10 km"
        }
    }
]