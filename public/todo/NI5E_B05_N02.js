json=[
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Fungi<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Monera<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Protista<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Animal<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Vegetal<\/p>",
                "t17correcta": "5"
            }
        ],
       "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_a.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_c.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,        
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Completa las oraciones con el adjetivo interrogativo que corresponda. Arrastra las palabras al lugar que les corresponde. Atiende los acentos en cada caso para responder correctamente la actividad."
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Causa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Consecuencia",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p><img></p>",
                //"valores": ['$70', '$60', '$45'],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p><img></p>",
                //"valores": ['$90', '$85', '$80'],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p><img></p>",
                //"valores": ['$100', '$110', '$120'],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p><img></p>",
                //"valores": ['$85', '$125', '$150'],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p><img></p>",
                //"valores": ['$85', '$125', '$150'],
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Producción de alimentos que requiere de la polinización.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Produce oxígeno y regula la temperatura local.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Producción de energía hidroeléctrica.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Reciclamiento de nutrientes",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Bosques"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Cuencas de ríos"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Abejas"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Océanos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>Construcción sostenible<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Impulso a la energía solar<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Ahorro energético<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Menos agua, ríos más limpios<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Consumo responsable<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Contra la especulación del suelo<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Menos basura<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Compostaje<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>Participación ecologista<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>Transporte<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><br/><br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:El diseño de los barrios debe tener en cuenta el entorno. Debe potenciarse la utilización de materiales en cuya extracción no se haya producido un deterioro del medio ambiente, como la madera certificada FSC.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:Las azoteas de nuestras ciudades son excelentes lugares para la ubicación a gran escala de centrales de energía solar fotovoltaica para producir electricidad y captadores solares térmicos para producir agua caliente. Deben generalizarse las ordenanzas solares que hagan obligatorios estos dispositivos.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:La utilización eficiente y el ahorro energético son el recurso energético con mayor potencial en las ciudades. Con adecuado aislamiento y criterios bioclimáticos en el diseño de edificios y en el planeamiento urbanístico se podría evitar el uso de aires acondicionados. La demanda de energía para climatización en edificios existentes se puede reducir en un 30-50% y en edificios nuevos en un 90-95%.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:El problema no es la sequía, es el aumento sin límites del consumo de agua. Por ello debe detenerse la promoción de actividades muy intensivas en el uso del agua y promoverse un uso mucho más racional de este recurso basándose en el ahorro, la eficiencia y la reutilización.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:La mayor parte de la producción de alimentos termina en las ciudades. Los habitantes de la mismas pueden influir en las formas de producción agraria y pesquera rechazando los productos transgénicos, el pescado que proviene de artes de pesca destructivas o demandando alimentos y productos (limpieza, juguetes, textiles,...) sin sustancias químicas tóxicas y utilizando papel y productos de madera respetuosos con los bosques.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:La financiación de las ciudades no puede seguir dependiendo de la expansión constante y sin freno de la especulación urbana. Debe analizarse en profundidad la insostenibilidad del actual modelo, para ponerle freno.<br/>Participación ecologista: La presencia activa de ciudadanos ecologistas en las ciudades es un beneficio para ellas. Por ello es importante que las administraciones municipales impulsen la participación de la sociedad en la defensa del medio ambiente.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:Casi tres cuartas partes de lo consumido por la sociedad industrial tarda menos de un año en convertirse directamente en residuo. Hay que impulsar definitivamente la recuperación de los materiales que hoy se convierten en basura a través de medidas que impulsen las tres erres: reducción, reutilización y reciclaje.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:La materia orgánica debe volver a la tierra para evitar su progresivo empobrecimiento y el uso de abonos artificiales. Por ello la parte orgánica de nuestras basuras puede recuperarse a través de un impulso al compostaje.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:La presencia activa de ciudadanos ecologistas en las ciudades es un beneficio para ellas. Por ello es importante que las administraciones municipales impulsen la participación de la sociedad en la defensa del medio ambiente.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;:Greenpeace propone una limitación del uso del coche en las ciudades y que se promueva de manera preferente al peatón, el uso de la bicicleta y el transporte público. En la actualidad la mitad de los desplazamientos en coche se realizan a menos de 3 kms. de distancia, y un 10% son para trayectos de menos de 500 metros.<br/><\/p>"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "textosLargos": "si",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>reunión<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>ideas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>intercambio<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>moderador<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>reglas<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>preguntas<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Un foro es una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;donde varias personas conversan o exponen <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;sobre un tema en común. Puede utilizarse un escenario para el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de ideas. Debe existir un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, es decir una persona que presente a los participantes y les dé la palabra, señala las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;del foro, e indica cuándo se deben hacer las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;por parte de los demás miembros del foro.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras para completar la siguiente información relacionada con la organización de un foro:"
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>La observación de los fenómenos y el planteamiento del problema a resolver.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Formulación de hipótesis que expliquen el problema.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Experimentación para validar o rechazar la hipótesis planteada.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Obtención de los resultados del experimento.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Interpretación y discusión de los resultados.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Conclusiones, aceptación o rechazo de la hipótesis planteada.<\/p>",
                "t17correcta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["Etapa 1", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Etapa 2", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Etapa 3", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Etapa 4", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Etapa 5", "201,551", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Etapa 6", "201,600", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Una página web donde todo mundo puede subir información, fotografías y platicar con otras personas. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es una página web en al que se publican artículos cortos con contenido actualizado sobre temas específicos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Una página para conocer amigos en todo el mundo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Una página interactiva para consultar información.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Un blog es:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Publicaciones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Comentarios<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aportaciones<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Chats<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Los artículos que se publican en un blog se conocen como “Post” y en español como:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Publicar sólo bajo la supervisión de un editor.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Publicar artículos que están bajo la censura de las autoridades.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Puedes publicar sin necesidad de una formación especial.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Publicar sólo información y contenidos verdaderos.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Una de las ventajas de tener acceso a Internet, es que puedes:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Reflexionar sobre sus ideas y compartirlas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Utilizar el blog para subir información, sin importar su procedencia o fuente.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Colaborar con muchas personas para mejorar los contenidos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Decir lo que piensa sin importar las consecuencias de lo que escribe.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Una persona que publica en un blog puede:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "1",
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "",                        
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Los se aspira conocer.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Lo que no se conoce.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Por qué se desea conocer.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Base para obtener el nuevo conocimiento.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Cómo se obtendrá el conocimiento.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Cuándo y con qué recursos se llevan a cabo la investigación.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Objetivo de investigación"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Problema de investigación"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Justificación"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Marco teórico"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Metodología"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Recursos y tiempo"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
  {
      "respuestas": [
          {
              "t13respuesta": "<p>Definición de metas y objetivos</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Formación de equipos</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Comunicación y respeto</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Programación y revisión periódica</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Uso de metodologías y actividades diversas</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Orientación y guía</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Autoevaluación y coevaluación</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Presentación del trabajo y fomento de la creatividad</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Debate y constrantes de ideas</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Presentación del trabajo y fomento de la creatividad</p>",
              "t17correcta": "0",
              "columna":"0"
          }
          
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "tamanyoReal" : true,
          "opcionesTexto": true
          
      },
      "contenedores": [
          {"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,162", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,243", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,324", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,405", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,486", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "201,567", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "406,0", "cuadrado", "78, 81", "."]},
          {"Contenedor": ["", "406,81", "cuadrado", "78, 81", "."]}
      ]
  }
]