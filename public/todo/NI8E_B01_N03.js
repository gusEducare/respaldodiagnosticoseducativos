json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Un carro pequeño  recorriendo una calle a 50 Km/h hacia el centro de la ciudad ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Un avión de pasajeros volando a 50 Km/h hacia el Sur",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Una persona en motocicleta hacia el atardecer con una velocidad de 50 Km/h",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Un camión de carga moviéndose a 50 Km/h hacia el NE",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Observa las situaciones mostradas e indica cuál es la que tiene mayor momento. Sugerencia: Recuerda que el momento es el producto de la masa por la velocidad. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Un avión de pasajeros volando a 50 Km/h hacia el Sur<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Un camión de carga moviéndose a 50 Km/h hacia el NE<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Un carro pequeño  recorriendo una calle a 50 Km/h hacia el centro de la ciudad<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Una persona en motocicleta hacia el atardecer con una velocidad de 50 Km/h<\/p>",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena las opciones del ejercicio anterior colocando primero la de mayor momento y al final la de menor momento.<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            { "Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."] }
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Es más difícil frenar un transporte grande porque poseé mayor masa.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "<p>Analizando las situaciones mostradas en el ejercicio anterior, responde la siguiente pregunta:  Si todos viajan a la misma velocidad, ¿Cuál de los aparatos de transporte tardará más tiempo en frenar? Explica tu respuesta.<\/p>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_01_01.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_01_02.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_01_03.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_01_04.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 2,
            "t11pregunta": "Indica cuál de las siguientes gráficas representa un movimiento ondulatorio:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_02_01.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_02_02.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_02_03.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img style='width: 200px;' src=\"ni8e_b01_n03_02_04.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 2,
            "t11pregunta": " Indica cuál de las siguientes gráficas representa un movimiento lineal con velocidad constante:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"ni8e_b01_n03_03_01.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"ni8e_b01_n03_03_02.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"ni8e_b01_n03_03_03.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"ni8e_b01_n03_03_04.png\">",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Observa los vectores dados en el siguiente sistema cartesiano. Indica cuál de las figuras mostradas muestra incorrectamente la suma de vectores por el método gráfico de polígono."
        }
    }
]