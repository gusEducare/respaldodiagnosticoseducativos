json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1821<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>1822<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>provincias<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Honduras<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>California<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Estados Unidos<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Para <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, México fue el país más grande de todo el continente Americano. En <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; se incorporaron lo que se conocía como las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;centroamericanas, lo que actualmente son Guatemala, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; , El Salvador, Nicaragua y Costa Rica. Con lo anterior, México creció aún más, y con ello, los problemas para mantener la seguridad y presencia del gobierno en todo el territorio. Más tarde, del lado norte, los territorios de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; , Nuevo México y Texas, pasarían a formar parte de los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;; y  del lado sur, las provincias centroamericanas se separarían de México<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que te permitan completar la siguiente información."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>imperio<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>constitución<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>democracia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>monarquia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>voto<\/p>",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Forma de gobierno en la que una sola persona toma las decisiones de un país."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Máxima ley que puedo tener un país. Se delimitan los derechos y obligaciones de los ciudadanos y establece los poderes que guian a una nación."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Forma de gobierno en la que el pueblo puede elegir a sus gobernantes."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Forma de gobierno en la que la jefatura del Estado reside en una persona, un rey o una reina, cargo habitualmente vitalicio al que se accede por derecho y de forma hereditaria."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Mina de cobre más grande de México."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 6
            },
            {
                "direccion" : 1,
                "datoX" : 9,
                "datoY" : 2
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 11
            },
            {
                "direccion" : 1,
                "datoX" : 2,
                "datoY" : 10
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto":20,
            "ancho":20,
            "t11pregunta": "Realiza el siguiente crucigrama:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al establecerse la Independencia en México, lo primero que se hizo fue, elaborar una nueva bandera y un gobierno monárquico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "México estableció, provisionalmente, una forma de organización del gobierno, a partir del Plan de Iguala. Después, crea la Junta Provisional Gubernativa.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tanto “La Regencia” como “La Junta Provisional Gubernativa” estaban integradas por Iturbide, diputados, miembros de la Iglesia para crear un gobierno definitivo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El “Congreso Constituyente” buscaría la representación de grupos muy específicos para elegir un gobierno. Esta representación no tomaría en cuenta a todos los sectores del país.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las fuerzas políticas que se crearon a partir del “Congreso Constituyente” fueron Republicanos, Borbonistas e Iturbidistas. ",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona las respuestas correctas.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Iturbide<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Antonio López de Santa Anna<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>republicano<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Congreso<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>1823<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>provincias<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>La República<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Federalista<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br><p>El mal gobierno de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;provoca la inconformidad de los otros grupos, los cuales deciden unirse. Además, el General&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;quien era gobernador de Veracruz, se levanta en armas contra el imperio de Iturbide con el apoyo de otros generales y buena parte del ejército. El levantamiento busca derrocar a Iturbide y se declaran a favor de establecer un gobierno&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;; en un intento por calmar los ánimos, Santa Anna decide restablecer el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, sin embargo éste declara inconstitucional el imperio y nombra traidor a Iturbide, por lo que Iturbide se ve forzado a dejar su cargo de emperador en mayo de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y sale del país huyendo hacia Europa, el reinado de Iturbide duró apenas once meses<br>Tras la salida de Iturbide, el Congreso nombra un gobierno provisional, pero los problemas continuaron. Para el mes de julio de 1823, tan solo cuatro meses después, las&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, entre ellas, las centroamericanas se declaran independientes de México, declarándose libres y soberanas. Finalmente, el Congreso convoca a elecciones y este nuevo Congreso declara como forma de gobierno a “&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;”.<br>Una vez establecida la República surgen dos corrientes que discuten si debe ser una República <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;o una República Centralista.<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "t11pregunta": "Arrastra las palabras que te permitan completar la siguiente información."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Cada territorio tendría su propio gobierno.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los estados se sujetarían a la autoridad general del país.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La clase media propone la forma de gobierno.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Predomina la mentalidad abierta.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Busca unir provincias o estados con autoridades propias.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El poder y las funciones de gobierno en un solo órgano.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Gobierno conformado por la clase alta.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Gobierno de terratenientes, comerciantes, militares, iglesia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Proponen gobiernos de 8 años. Las libertades son limitadas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Unión de provincias o estados con autoridades propias.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": " Arrastra al contenedor  las características que correspondan al Centralismo y Federalismo.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Federalismo",
            "Centralismo"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Inglaterra<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Intervencionismo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Francia<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>España<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Estados Unidos<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Reconoció a México como país independiente. Vio en los recursos naturales del país, una forma de expandir su economía. Fue el socio comercial de México en 1846.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ": Países extranjeros que, pese a tener tratados comerciales, abusaban de su poder aprovechándose de la conformación de una nueva nación.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ": Firmó dos tratados con México, pero ninguno fue aprobado por el Congreso, por no reconocer plenamente a México como país independiente y porque no había libertad de culto.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ": Este país conspiró para derrocar al gobierno mexicano. México emitió una ley en 1827 para expulsar a todos los ciudadanos del País. Al hacerlo, se afectó la economía interna ya que se llevaron sus riquezas.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ": Invadió constantemente a México, quitándole el 50% de su territorio. Era una potencia que consolidaba su independencia y contaba con un ejército mucho más articulado y con más recursos.<br><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Coloca el país que correspondan a las siguientes descripciones."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>La gente emigra para buscar una mejor calidad de vida.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sin cambios significativo a pesar de la Independencia.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El sustento provenía de los cultivos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los terratenientes  se beneficiaban de la producción.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al emigrar, muchas veces terminaban en la miseria.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La clase media comenzó a prepararse académicamente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Había fiesta religiosa y taurina, teatro, paseo dominical.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Académicos mostraban lo que pasaban dentro y fuera del país.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra los conceptos correspondientes a la vida en el campo y en la ciudad respectivamente.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Vida en el campo",
            "Vida en la ciudad"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>intrusiones<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>La Guerra de los Pasteles<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>conflictos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>independiente<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>negocios<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>sublevaciones<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><p>México enfrentó las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de varios países y sus efectos; sin embargo, hubo un peculiar conflicto al que el común del pueblo mexicano llamó “<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>”. México sufrió los abusos de países que buscaban aprovecharse de la situación por la que atravesaba el país, los malos gobiernos, los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;internos, la inocencia de un país recién creado y la urgencia de que otros países lo reconocieran como una nación&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>; estos fueron factores que ayudaron a que el país no creciera ni desarrollara su infraestructura y economía.<br>Tras la independencia de México, varios países extranjeros vieron las condiciones favorables para invertir y crear&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, sin embargo la situación del país todavía era inestable, pues había constantes&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;contra los malos gobiernos y sus malas decisiones. Fue precisamente, que durante las revueltas, varios negocios se vieran afectados, a causa de daños materiales que sufrieron por parte de los inconformes<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "t11pregunta": "Coloca la palabra en el párrafo donde correspondan. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "“La Guerra de los Pasteles” surge por un conflicto entre el gobierno mexicano y un comerciante oportunista.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un pastelero contactó al gobierno Inglés y exigió el pago correspondiente al tumulto que se desarrolló por los conflictos internos de México y con esto, comenzó “La Guerra de los Pasteles”.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Francia envió muchas embarcaciones para exigir el pago al ciudadano francés como reparación de los daños.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Francia bombardea San Juan de Ulúa y Veracruz. Con esta situación se declara la guerra contra Francia.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se llega a un tratado de paz, donde Francia se compromete a pagar la cantidad que exigía el pastelero.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correspondiente.<\/p>",
            "descripcion": "Aspectos a valorar",
            "anchoColumnaPreguntas": 70,
            "evaluable": true

        }
    }
]