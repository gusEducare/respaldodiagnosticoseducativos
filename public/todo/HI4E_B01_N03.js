json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Utensilios, herramientas y armas de caza.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Tipos de vivienda desde las cuevas hasta tiendas de acampar.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Conocimiento y aprendizaje de su entorno.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cazar, pescar y recolectar.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Establecer viviendas donde se desarrollaba la agricultura.<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Almacenamiento y conservación de alimentos.<\/p>",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Coloca en orden cronológico los eventos ocurridos durante la evolución de la humanidad.",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["1", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["2", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["3", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["4", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["5", "201,551", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["6", "201,685", "cuadrado", "134, 73", "."]}
        ]
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPorque en la época prehispánica, el maíz era el único alimento que podían consumir.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque los prehispánicos, basaban su alimentación en un producto con muchos beneficios para la salud y para sus condiciones de vida.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque, por las peculiaridades de la región, se reproducía con mayor facilidad, eso garantizaba que siempre tuvieran alimentos.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Elige la opción que más corresponda a la pregunta ¿por qué crees que la palabra maíz significa “Lo que sustenta la vida”?"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAsí quedaban protegidos de la fuerza de la naturaleza. \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELo hacían por simple gusto. \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara poder comunicar mejor alguna idea a otros miembros de su grupo.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETodas las opciones.\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona qué teoría es la más adecuada con  relación al propósito que tenían las pinturas rupestres."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPorque la humanidad está ligada íntimamente a las artes desde sus orígenes. \u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque no había manera de conocer el entorno si no era a través de sus expresiones gráficas.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque no había nada más que hacer.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la opción que más se acerque a la pregunta ¿Por qué crees que el desarrollo de la humanidad tiene una relación directa con el arte?"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Baja California Sur<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>San Luis Potosí<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Guanajuato<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Querétaro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tamaulipas<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona los estados de nuestro país en los que se han encontrado pinturas rupestres:"
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Producción de alimentos que permitían que un pueblo pudiera ser sedentario.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cosechas a partir de las estaciones del año.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Comunidades con mejor situación familiar.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Perfeccionamiento en el cálculo del tiempo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Producción de diferentes tipos de maíz.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las opciones que muestren los avances que trajo consigo la agricultura:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Capacidad de pelear contra los pueblos por la comida para no morir de hambre adaptando con el tiempo, desde sus estrategias de guerra, hasta la forma de alimentarse.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Capacidad de adaptarse al clima utilizando las pieles de animales, esta capacidad les ayudó a soportar el cambio climático e inventar nuevas formas de supervivencia en climas adversos. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Superar los retos que se presentan en el ambiente aprendiendo a adaptarse a las circunstancias y desarrollando o descubriendo nuevas cosas de acuerdo a sus necesidades fundamentales.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": " Selecciona el concepto de supervivencia que te parezca más adecuado:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los awas son una tribu sedentaria y basa su alimentación en la recolección de frutos y la caza de animales pequeños.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los masáis son seminómadas  ya que se mueven a otros sitios para dejar crecer el pasto en los lugares donde alimentan a sus animales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las tribus sedentarias se establecían en lugares fijos ya que contaban con técnicas de agricultura que les permitían tener suficiente alimento.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hadzas son una tribu que se encuentra en Asia y que es la única que se comunica con chasquidos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los Anasazi y los Hohokam, dependieron mucho de la cacería, la recolección y la pesca.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona si la aseveración es falsa o verdadera:",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Por el desarrollo de la agricultura, eligen los mejores lugares para vivir<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Invento de utensilios, herramientas y armas de caza.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Cuentan con viviendas para establecerse y poder alimentarse de lo que haya en el lugar.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Conocimiento y aprendizaje de su entorno.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Almacenamiento y conservación de alimentos.<\/p>",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "<p>Cazar, pescar y recolectar.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca en orden cronológico, la evolución que tuvo el ser humano para alimentarse.<\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["1", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["2", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["3", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["4", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["5", "201,551", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["6", "201,685", "cuadrado", "134, 73", "."]}
        ]
    }
]