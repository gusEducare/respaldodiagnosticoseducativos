json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Biografía",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Entrevista",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src=si6e_b01_n01_01.png>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src=si6e_b01_n01_02.png>",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Lee ambos textos y luego señala a qué tipo corresponde cada uno.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
{  
  "respuestas":[  
     {  
        "t13respuesta":"Evaluar la distancia entre lo que sabe el docente y lo que saben los alumnos.",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Comprobar  el aprendizaje de los alumnos.",
        "t17correcta":"1"
     },
     {  
        "t13respuesta":"Poner nerviosos a los alumnos.",
        "t17correcta":"0"
     }
  ],
  "pregunta":{  
     "c03id_tipo_pregunta":"1",
     "t11pregunta":"¿Cuál es el objetivo principal de los exámenes?"
  }
},
    {
        "respuestas": [
            {
                "t13respuesta": "Esta frente al micrófono y presenta entrevistas, información, llamadas telefónicas entre otros.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Persona que se encarga de manejar el área de sonido, ya sea, música o efectos sonoros.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Persona que se encargan de preparar el programa antes de que suceda; debe soluciona durante la transmisión, todos aquellos problemas que pudieran ocurrir en el programa de radio.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Locutor"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Operador"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Productor"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las funciones de cada uno."
        }
    },
{  
  "respuestas":[  
     {  
        "t13respuesta":"Responde rápidamente y sin mayor reflexión.",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Lee detenidamente el planteamiento.",
        "t17correcta":"1"
     },
     {  
        "t13respuesta":"Trata de inferir el significado de la pregunta por el contexto.",
        "t17correcta":"1"
     },
     {  
        "t13respuesta":"En realidad no es importante.",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"De ser necesario, corrige la puntuación.",
        "t17correcta":"1"
     }
  ],
  "pregunta":{  
     "c03id_tipo_pregunta":"2",
     "t11pregunta":"Señala las estrategias que puedes utilizar para resolver preguntas ambiguas"
  }
},
{  
  "respuestas":[  
     {  
        "t13respuesta":"Texto narrativo",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Guion teatral",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Ensayo técnico",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Guion de radio",
        "t17correcta":"1"
     }
  ],
  "pregunta":{  
     "c03id_tipo_pregunta":"1",
     "t11pregunta":"Texto que estructura las partes del programa radiofónico; prevé todo lo necesario y presenta el contenido incluyendo datos técnicos, también constituye un texto guía clave que permite el entendimiento entre los locutores y los operadores técnicos."
  }
}
];