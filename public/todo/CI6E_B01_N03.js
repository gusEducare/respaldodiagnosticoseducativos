json = [  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Disfruta socializar con personas del sexo opuesto.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Se preocupa por su imagen corporal.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Alopecia, experimenta caída de cabello.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Hasta esta edad puede practicar un deporte.",
            "t17correcta":"0"
         },
         
         {  
            "t13respuesta":"Convive más con sus amigos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Necesita de mayor independencia.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Ignora a la familia.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Cambia de humor.",
            "t17correcta":"1"
         }
         ,
         {  
            "t13respuesta":"Necesita de mayor descanso.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Busca pertenecer a determinados grupos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Consume dulces y golosinas.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":2,
         "t11pregunta":"Señala los cambios sociales y psicológicos que caracterizan a la adolescencia. "
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando no está de acuerdo  con sus mayores, se controla y no muestra sus discrepancias.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen cambios físicos que pueden indicar con precisión cuándo ocurrirá la primera menstruación o la primera eyaculación.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La madurez, permite visualizar el futuro y tomar decisiones acertadas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al crecer, se enfrenta a la toma de decisiones. Puede solicitar y escuchar los consejos de las personas que considere, puedan ayudarle.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para tener una vida sana, hace uso de sustancias que le ayudan a verse o sentirse mejor, aunque no sean legales.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige la opción que corresponde a las características del adolescente<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    }
    
    ,
       {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La sexualidad es una parte importante en la vida del ser humano, pero no es necesario que conozcamos todos los aspectos que implica.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos sexuales y la sexualidad comienzan a ser importantes sólo en la adolescencia. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno debe proporcionar de manera gratuita y libre información sobre los derechos sexuales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los embarazos no deseados pueden ser una consecuencia de no conocer a fondo todos los aspectos de una sexualidad responsable.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tener información sobre los derechos sexuales, nos ayuda a tomar decisiones que pueden afectar nuestro futuro.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso o verdadero según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Comprender el concepto de persona humana.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comprender que cada ser humano es un individuo con alma y cuerpo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comprender la biología humana.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Comprender a cada persona como integral un ser biológico, psíquico y social.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comprender la genética.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Comprender que aun cuando tenemos valores universales cada persona tiene su escala de valores.",
            "t17correcta":"1"
         },
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":2,
         "t11pregunta":"Señala las perspectivas humanistas que favorecen entender la sexualidad humana. "
      }
   },
    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Hacer mis tareas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mi aprendizaje.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Llevar una vida saludable.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Organizar una venta de cosas que ya no utilizan.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cumplir mi proyecto de vida.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Reparar algunos juguetes y venderlos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mi felicidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cuidar de mí mismo.<\/p>",
                "t17correcta": "0"
            },
            
            
            {
                "t13respuesta": "<p>Que mis maestros lleguen a clases.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> La desigualdad social.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El matrimonio de mis padres.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La felicidad de mis amigos.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Relaciona las siguientes oraciones  con el número que corresponda.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Son mi responsabilidad",
            "No son mi responsabilidad"
        ]
    },
     {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Conforme creces, adquieres más libertad, pero también, mayores responsabilidades.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tu autoestima, terminará de formarse cuando seas adulto.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuidarme e informarme, cuando decida sobre mi vida sexual, es parte de mi responsabilidad al crecer.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Puedes encontrar la información necesaria en internet sobre todas tus dudas sobre sexualidad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La sexualidad es una acción que realizan ambos sexos y necesitas información sobre ella. ",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
    
       {
        "respuestas": [
            {
                "t13respuesta": "<p>humana<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>unidad<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>cuerpo<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>integral<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>valores<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>identidad<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Algunas de las perspectivas que nos permitirán entender la sexualidad humana son: Entender qué es la persona <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br>Comprender el hombre como una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; muy particular: <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y alma El hombre como un ser <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; bio - psico – social.<br>Persona con <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br>Ser humano con <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y complementariedad<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "t11pregunta": "Arrastra las palabras para completar la siguiente información:"
        }
    },
    
       {  
      "respuestas":[          
         
         {  
            "t13respuesta":"Cuando un compañero te presta los apuntes que no tienes y te ayuda a estudiar.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Cuando un compañero te insiste en fumar porque es lo que todo mundo hace.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Cuando una persona en internet que no conoces te pide que se reúnan en un parque.",
            "t17correcta":"1"
         },
           {  
            "t13respuesta":"Cuando alguien te obliga a hacer algo que te hace sentir incómodo respecto a tu cuerpo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Cuando un amigo te invita a su casa y sus familias se conocen.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Cuando alguien te toca inapropiadamente.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":2,
         "t11pregunta":" Selecciona las situaciones que te ponen en riesgo y por lo que consideras que debes alejarte de alguna persona"
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Las mujeres deben dedicarse al hogar.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los hombres no pueden usar color rosa.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Todos los musulmanes son terroristas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El hombre es más inteligente que la mujer.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor correspondiente:",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Prejuicio",
            "Estereotipo"
        ]
    },
    
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No puedes embarazarte al tener relaciones sexuales por primera vez.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No hay ninguna complicación médica si te embarazas siendo adolescente.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un embarazo precoz puede afectar tu futuro.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La única consecuencia si tienes relaciones sexuales sin protección es la posibilidad de un embarazo no deseado.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es mejor que no hables temas de sexualidad para evitar que la gente se incomode.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    }
]