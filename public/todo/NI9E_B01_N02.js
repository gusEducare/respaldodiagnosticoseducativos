json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Cambio Físico",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Cambio Químico",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hielo derritiéndose",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Leña encendida en chimenea",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tira de papel cortada a la mitad",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Alivio tras aplicación de ungüento en la piel después de una quemadura  ",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona si el cambio que se menciona hace referencia a un cambio físico o a un cambio químico.<\/p><br>",
            "descripcion": "Sentencia",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "<p>Describe brevemente la diferencia entre un cambio químico y un cambio físico</p>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E la tierra\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E la materia \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E la mezcla \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E el cuepo humano\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El objeto de estudio de la química son los cambios estructurales en:<br>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Las propiedades intensivas miden propiedades internas de la materia mientras que las extensivas miden propiedades exteriores. \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Las propiedades intensivas son independientes de la cantidad de masa, mientras que  las extensivas si dependen.\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E Las unidades y los instrumentos con que se miden.\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál es la diferencia entre una propiedad extensiva y una intensiva?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ETemperatura\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EPunto de ebullición\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ESolubilidad\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EMasa\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EVolumen\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Clasifica las siguientes propiedades de la materia en extensiva o intensiva:",
            "tipo": "vertical"
        },
        "contenedores": [
            "Extensiva",
            "Intensiva"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Describe con tus palabras el cambio de estado de agregación"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Explica con tus palabras el proceso de destilación"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cantidad de fases observadas en una mezcla, nos ayuda a determinar si una mezcla es homogénea o heterogénea.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero o falso<\/p>",
            "descripcion": "Sentencia",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 38% \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 45% \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 76% \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 57% \u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Sí se tiene una solución de 300 mL de alcohol etílico, la cual se preparó con 135 mL de etanol, y 175 mL de agua ¿Cuál será su concentración en volumen?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Concentración \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E Soluto \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Solvente \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Partes por millón  \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "La proporción en la cual una sustancia, se encuentra dentro de una mezcla:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lavoisier desarrolló el principio de conservación de la masa, responsable de la primera revolución de la química",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero o falso<\/p>",
            "descripcion": "Sentencia",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "¿Qué diferencias de propiedades de la materia se aprovechan para un proceso de decantación?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Define ductilidad. ¿Es intensiva o extensiva? "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 24% \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 45% \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 29% \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 10% \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Determina el porcentaje en masa de 172 g de NaOH en 526 g de agua. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 210 g \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 165 g \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 225 g \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 235 g \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuántos gramos de soluto se necesitan para preparar una solución al 25% de CuSO<sub>4</sub>, con 900 g de agua?"
        }
    }
]
