json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Exponer las ideas según el grado de simpatía que provoquen.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Presentar y desarrollar los contenidos de acuerdo a su orden de importancia o cronología.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Seleccionar el vocabulario más jocoso que conoces.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Presentar las ideas según su relación de causa y efecto.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Exponer las ideas mediante ejemplos, citas o referencias que despierten interés.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Repetir las frases, para que el texto sea más claro.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Presentar las ideas según van surgiendo en tu pensamiento.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Diferenciar las ideas principales de las complementarias.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Seleccionar un vocabulario claro y variado.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evitar las repeticiones y las redundancias.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Presentar las ideas secundarias antes de las principales.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todos los criterios que puedes utilizar para ordenar las ideas en un texto."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": " Tiene el título completo del libro, el nombre completo del autor o autores, el lugar y el año en que se imprimió, el nombre de la editorial y el número de la colección o volumen.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "También se le conoce como anteportada. Tiene escrito el título de la obra,  el nombre del autor y el sello editorial.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Es una lista que contiene separados cada uno de los capítulos del contenido del libro y su correspondiente numeración. Sirve para localizar una parte específica del libro, sin necesidad de leerlo completamente.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "También se le llama “prefacio” y es un escrito que es realizado por el mismo autor o por algún especialista en el tema. Es una primera impresión acerca de lo que se conseguirá más adelante en la lectura.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Portada"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Portadilla"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Índice"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Introducción"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los elementos convencionales de los libros con su descripción."
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Desarrollar un relato de ciencia ficción.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Expone los sentimientos más profundos de una familia.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Informar sobre un tema de manera objetiva.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Relatar las leyendas de la comunidad.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Realizar una paráfrasis de una investigación científica.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Cuál es el propósito de un texto expositivo?"
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>leyenda<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>narración<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>oral<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>escrita<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>naturales<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>sobrenaturales<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>héroes<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>mitológicos<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>suceso<\/p>",
                "t17correcta": "9"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, transmitida de forma <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de generación en generación, de hechos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; o mezclados. Sus temas abarcan <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de la historia patria , seres <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; o religiosos, apariciones o algún <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; importante.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal":true,
            "t11pregunta": "Coloca cada palabra en el lugar correspondiente para completar el párrafo."
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "Son las imágenes, dibujos o gráficos que se representan en el cuerpo del texto.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Son palabras u oraciones breves que permiten predecir el contenido del texto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Corresponden a subtemas o aspectos específicos del tema general.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Contienen extractos exactos de información recopilada de los textos consultados y siempre se da crédito al autor.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Guían para encontrar respuestas específicas sobre la información relevante del texto.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Listado que incluye todos los contenidos y los números de página donde encontrarás cada tema o subtema en el texto.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ilustraciones"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Título"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Subtítulos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Citas bibliográficas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Preguntas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Índice"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los elementos convencionales de los libros con su descripción."
        }
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Fichas de trabajo",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Fichas bibliográficas",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Citas bibliográficas",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Citas médicas",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Contienen información que extraemos desde otras investigaciones para dar seriedad y formalidad al trabajo, así como respaldar nuestras opiniones o contrastarlas. Refieren a información dicha por un tercero y podemos incluirlas en distintos tipos de texto, ya sean expositivos, de investigación o ensayos; y siempre se transcriben de manera exacta, entre comillas y se cita al autor para darle crédito por su obra."
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Adversativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Comparativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Disyuntivo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tiempo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Coordinante<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Finales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Modo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Consecutivo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Copulativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Casual<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Condicional<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Lugar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ilativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Concesivo<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Localiza en la sopa de letras los tipos de nexos que debes tomar en cuenta al redactar textos."
        }
    },
          {
        "respuestas": [
            {
                "t13respuesta": "Informaciones recientes sobre la temática central de la entrevista.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Texto expositivo sobre la biografía de dos monarcas de Egipto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Ensayo sobre algún tema relevante de interés para la comunidad lectora, en este caso los restos óseos del Museo del Cairo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Incluyen reseñas de páginas web que ofrecen información sobre Egipto antiguo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Muestras de exposiciones sobre el Egipto antiguo.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": " Trivias y sopas de letras.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Noticias"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Historia"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Análisis"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Páginas egiptológicas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Museos y colecciones"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Pasatiempos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona cada sección del boletín informativo  con su descripción."
        }
    },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>familia léxica<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>palabras<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>primitiva<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>raíz<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>significado<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>derivadas<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La familia de palabras o “<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>” es el conjunto de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que surgen de una  misma palabra llamada “palabra <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>” o “ <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>“. Todas las palabras de la familia tienen elementos en común: se relacionan por su <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y dan posibilidad a que surjan otras “palabras <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>”. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal":true,
            "t11pregunta": "Coloca cada palabra en el lugar correspondiente para completar el párrafo."
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "Nombran seres, objetos e ideas abstractas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Indican las acciones realizadas por los sujetos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Expresan cualidades o características del nombre.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Sustantivos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Verbos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Adjetivos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los elementos convencionales de los libros con su descripción."
        }
    },
]