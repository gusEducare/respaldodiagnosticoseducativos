[
   {
        "respuestas": [
            {
                "t13respuesta": "<p>humano<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>social<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>imposible<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Necesitamos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>crecer<\/p>",
                "t17correcta": "5"
            },
             {
                "t13respuesta": "<p>desarrollarnos<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>actividades<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>brindan<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>ayuda<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>plena<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>El ser <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es un ser <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; por naturaleza, tratar de vivir sólo resulta <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de los demás para <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como personas, con nuestras habilidades y nuestras capacidades. Los otros desempeñan <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; diversas y nos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; necesaria para tener una vida <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Cambio climático",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sobreexplotación",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Eutrofización",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<img src=\"EJEMPLO.png\">"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<img src=\"EJEMPLO.png\">"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<img src=\"EJEMPLO.png\">"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Respetar a los indígenas",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Burlarme de las personas distintas a mí",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Solidarizarme con aquellos que son discriminados y abogar por ellos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Poner apodos o hacer chistes ofensivos de aquellos que piensan distinto.",
            "t17correcta":"0"
         },
               {  
            "t13respuesta":"Reconocer que el tono de piel no es relevante para valorar a una persona",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Etiquetar a otros con frases o palabras que impliquen una situación de discapacidad. Por ejemplo “pareces autista”",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Llevar a la práctica en mi vida diaria valores universales",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"No ejercer ni permitir violencia contra aquellos que parecen distintos a lo que conozco.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Respetar a todo ser humano por sí mismo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Reconocer el profundo valor que tienen las culturas antiguas de México",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Elige todo lo que tú puedes hacer para evitar la discriminación."
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Biodiversidad",
            "Diversidad cultural"
        ]
    },
       {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La equidad es darle a cada uno lo que requiere; aun cuando no sea lo mismo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Equidad es que a mi hermana y a mí nos compren el mismo balón.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para ser equitativos también hay que reconocer las diferencias y las necesidades de cada persona.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todo ser humano merece un trato justo y equitativo por parte de las autoridades.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La equidad es algo natural que ya ocurre en cada lugar de la república mexicana.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La equidad fomenta la igualdad de oportunidades, de acceso a los recursos que nos brindan bienestar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aún no existe la equidad social ni en México ni en otros países.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La equidad rompe con la dignidad de la persona",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    }
]