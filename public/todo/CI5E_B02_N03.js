json = [
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es necesario apresurarnos sin tener que distinguir nuestras emociones y reaccionar ante lo que nos hagan sentir.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando reprimes una emoción, puede perjudicarte.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones pueden ser dominadas y darles una forma de expresión adecuada.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando reprimes una emoción, podemos llamarlo autocontrol emocional.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Saber reconocer una emoción es un primer paso para poder controlarlas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Poder Legislativo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Poder Judicial",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Poder Ejecutivo",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "En México ¿Cuál de los Poderes de gobierno se encarga de administrar la justicia y hacer cumplir las leyes?"
        }
    },
 {
        "respuestas": [
            {
                "t13respuesta": "Puedo decidir conducir el auto",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Puedo decidir comida saludable y balanceada",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Puedo decidir hacer deporte",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Puedo decidir si pagar los recibos o no",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Puedo decidir ser positivo ante la vida.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Puedo decidir respetar mi cuerpo y auto cuidarme",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Puedo decidir usar la ropa que yo quiera para ir a la escuela",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Puedo decidir no ir a la escuela",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Puedo decidir evitar el uso de drogas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Puedo decidir respetar los derechos de los otros",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las situaciones en las que puedes tomar decisiones de acuerdo a tu edad."
        }
    },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"Respetar  la ley",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tomar en cuenta tus valores y ética al actuar",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Conseguir lo que quiero sin importar las consecuencias",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Defender lo que consideras adecuado",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Respetar las diferencias",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Buscar mi interés personal sin importar las consecuencias",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Siempre tener en cuenta las consecuencias de mis actos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Respetar a animales y la naturaleza en general",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Proteger a los menos favorecidos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Utilizar la información o confianza de los demás en mi beneficio",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"No permitir situaciones de violencia",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Evitar las injusticas, aunque no te afecten",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Actuar y hablar con congruencia y verdad",
            "t17correcta":"1"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todo lo que consideres te puede hacer una persona justa."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Protección contra la esclavitud",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Protección contra la tortura",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Igual protección ante la ley",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Estar libre de detención arbitraria y el derecho a un juicio justo",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Libertad de pensamiento, de opinión, de religión y de expresión",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Derecho a la educación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Derecho a un nivel de vida adecuado, así como a la salud, vivienda y alimentación suficiente",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Derecho al trabajo y fundar y afiliarse a sindicatos",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"De los siguientes derechos humanos elige aquellos que consideras aún no son una realidad en nuestro país."
      }
   }
]
