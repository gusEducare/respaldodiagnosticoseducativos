json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 844.2 \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 803.1 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 40.2 \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Para llegar a la escuela Ricargo camina al salir de su casa 40 metros hacia el este, después camina 50 metros hacia el norte y después camina 800 metros al este. ¿Cuál es la distancia en línea recta desde su casa hasta la escuela? "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Error \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='display:inline-block; vertical-align:top; font-size:24px; font-weight:bold;'>&#8730;<\/div><div style='display:inline-block;'><hr style='padding:0; margin:0; background-color:black; height:3px;'>(x + 2<sup>2</sup>) + (x + 2<sup>2</sup>)<\/div>\u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='display:inline-block; vertical-align:top; font-size:24px; font-weigth:bold;'>&#8730;</div><div style='display:inline-block;'><hr style='padding:0; margin:0; background-color:black; height:3px;'>(x + 2<sup>2</sup>)<\/div>\u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál de las siguientes expresiones representa la dimensión de la diagonal del cuadrado cuyo lado mide x + 2?<br><div style='text-align:center'><img src='.png'></div>"
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 6 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 2 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 8 \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "La suma del cuadrado de un número con el doble del mismo es igual a 80. ¿De qué número se trata? "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E x<sup>2</sup> + 4x = 0 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E x<sup>2</sup> = 4x \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 2x - 4x<sup>2</sup> = 0 \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál de las siguientes expresiones permite calcular la medida del lado de un cuadrado en el que la suma de sus 4 lados es igaul al cuadrado de cada uno de sus lados"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'>\n\
                                                    <span class='fraction'><span class='top'>18</span><span class='bottom'>36</span></span><sup><sup>+</sup></sup> \n\
                                                    <span class='fraction'><span class='top'>3</span><span class='bottom'>36</span></span> <sup><sup> - </sup></sup> \n\
                                                    <span class='fraction'><span class='top'>1</span><span class='bottom'>36</span></span> <sup><sup>=</sup></sup> \n\
                                                    <span class='fraction'><span class='top'>20</span><span class='bottom'>36</span></span>\n\
                                                </div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'>\n\
                                                    <span class='fraction'><span class='top'>18</span><span class='bottom'>36</span></span><span><sup><sup>+</sup></sup>\n\
                                                    <span class='fraction'><span class='top'>18</span><span class='bottom'>36</span></span><span><sup><sup>=</sup></sup>\n\
                                                     <span class='fraction'><span class='top'>30</span><span class='bottom'>36</span></span>\n\
                                                </div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'>\n\
                                                    <span class='fraction'><span class='top'>18</span><span class='bottom'>36</span></span><span><sup><sup>+</sup></sup>\n\
                                                    <span class='fraction'><span class='top'>12</span><span class='bottom'>36</span></span><span><sup><sup> - </sup></sup>\n\
                                                    <span class='fraction'><span class='top'>6</span><span class='bottom'>36</span></span><span><sup><sup>=</sup></sup>\n\
                                                    <span class='fraction'><span class='top'>20</span><span class='bottom'>36</span></span>\n\
                                                </div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'>\n\
                                                    <span class='fraction'><span class='top'>18</span><span class='bottom'>36</span></span><span><sup><sup>+</sup></sup>\n\
                                                    <span class='fraction'><span class='top'>12</span><span class='bottom'>36</span></span><span><sup><sup>+</sup></sup>\n\
                                                    <span class='fraction'><span class='top'>6</span><span class='bottom'>36</span></span><span><sup><sup>=</sup></sup>\n\
                                                    <span class='fraction'><span class='top'>36</span><span class='bottom'>36</span></span>\n\
                                                </div> \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Rocío y David juegan a lanzar dos dados, la probabilidad de obtener un número non o un múltiplo de tres, al lanzar dos datos se obtiene: "
        }
    }
]

