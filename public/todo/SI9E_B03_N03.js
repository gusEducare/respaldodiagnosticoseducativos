json = [
	{
		"respuestas": [
			{
				"t13respuesta": "<p>grafica<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>investigar<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>tabla<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>esquema<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>diagrama<\/p>",
				"t17correcta": "0"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Que se representa por figuras o signos."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Hacer las acciones necesarias para descubrir algo"
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Conjunto de datos o informaciones representadas gráficamente, generalmente en forma de columna, y dispuestis según determinado orden o clasificación."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Representación mental o simbólica de una cosa material o inmaterial o de un proceso en la que aparecen relacionadas de forma lógica sus lineas o rasgos esenciales."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Representación gráfica de las variaciones de un fenómeno o delas relaciones que tienen los elementos o las partes de un conjunto."
			}
		],
		"pocisiones": [
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 1
			},
			{
				"direccion": 1,
				"datoX": 5,
				"datoY": 1
			},
			{
				"direccion": 1,
				"datoX": 11,
				"datoY": 3
			},
			{
				"direccion": 0,
				"datoX": 5,
				"datoY": 4
			},
			{
				"direccion": 0,
				"datoX": 3,
				"datoY": 9
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "15",
			"t11pregunta": "Delta sesion 12 a3",
			"alto": 11
		}
	},
	{
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El verbo de la oración principal condiciona al segundo: el tiempo y el modo en el que esté escrito el primero, se expresará con el mismo tiempo y modo en el segundo. Ejemplo: Los exámenes se aplicaron sin problema y se obtuvieron los resultados deseados.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El verbo de la oración principal y la o las oraciones subordinadas deben concordar con el mismo sujeto en persona y número. Ejemplo: Los ejemplos que se da no es adecuado.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si el verbo de la oración principal está en modo indicativo, el verbo de la oración subordinada también deberá estarlo. Ejemplo: Los resultados modficarán las decisiones que se tomarán.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando el verbo principal expresa alguna voluntad o deseo del pasado o del futuro, el verbo de la oración subordinada puede ir en modo subjuntivo. Ejemplo: Si se decidiera esa opción, el resultado puede ser peligroso.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
	{
		"respuestas": [
			{
				"t13respuesta": "<p>antropocentrismo<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>teocentrismo<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>burgueses<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>humanismo<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>mecenas<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>grecolatino<\/p>",
				"t17correcta": "0"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Concepción filosófica que considera al ser humano como centro de todas las cosas y el fin absoluto de la creación."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Doctrina que cosidera a Dios o a la divinidad centro de la realidad y de todo el pensamiento y actividad humana."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Es un término de origen francés (bourguoisie), utilizado en la economía política, y también extensivamente en folosofía política, sociología e historia. Designa  a la clase media acomodada."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Movimiento  intelectual pretendía descubrir al hombre y dar una sentido racional a la vida tomando como maestros a los clásicos griegos y latinos, cuyas obras redescubrío y estudío."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Persona o fundación rica y poderosa que protege a los artistas y adquiere o promueve sus obras."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Relativo a los antiguos griegos y latinos y su civilización común."
			}
		],
		"pocisiones": [
			{
				"direccion": 1,
				"datoX": 11,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 9,
				"datoY": 5
			},
			{
				"direccion": 0,
				"datoX": 4,
				"datoY": 9
			},
			{
				"direccion": 1,
				"datoX": 7,
				"datoY": 13
			},
			{
				"direccion": 1,
				"datoX": 4,
				"datoY": 14
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 16
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "15",
			"t11pregunta": "Delta sesion 12 a3",
			"alto": 22
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Identifica quién es el público deseado para que escuche la información; de esta manera sabrás qué lenguaje emplear, qué palabras pudieran ser confusas o no, entre otros.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Ubica cuáles son las noticias o el contenido más reciente. Recuerda que en la radio la comunicación es inmediata, así que debes aprovechar esta característica y atraer la atención del publico siendo uno de los primeros medios  en mencionar algo.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>La audiencia tiene muchos referentes visuales, por lo que puedes hacer referencia a ellos para que los imaginen aunque no sea muy amplia tu descripción.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Desarrolla guiones con información muy amplia para brindar la información mas completa a la audiencia, ya que cuentas con mucho más tiempo que en un medio audiovisual.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Recolecta información que sea de interés para el target del programa de radio, ya que se pretende invitar a la conversación o el diálogo con el receptor. Se deben cominicar mensajes con significado o aporte para la comunidad.<\/p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Difusión de la información"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Se deben evitar las mulettilas y se debe tratar en lo posible de presentar la información sin rodeos ni concisos que desvíen la atención principal.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "El usuario u oyente no tiene acceso a repetir la información que ya salió al aire, el locutor debe informar cada cierto tiempo acerca de lo se está conversando. Puede tornarse repetitivo pero es necesario para captar la atención de nuevos oyentes.",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "Se debe nutir no solo de la palabra del(la) locutor(a). También debe incluir elemnetos que le den dinamismo al programa.",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "Esta puede ayudar al oyente a trasladarse a un lugar, a revivir una emoción o puede servirle de distracción.",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "Sirven para recrear ambientes o situacioes de las que se pueden estar hablando. De esta manera se puede comentar algo que haya sucedido en el pasad pero que se sienta o escuhe como si estuviera sucediendo en el momento.",
				"t17correcta": "5"
			},
			{
				"t13respuesta": "Este es un recurso que debe emplearse cuando se quiere transmitir alguna reflexión sobre lo dicho. Puede servir de espacio para que los oyentes capten la seriedad acerca de algo que se esté mencionando.",
				"t17correcta": "6"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Claro y conciso"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Repetitivo"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Dinámico"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Emplea música"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Efectos de sonido"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Silencios"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "18",
			"t11pregunta": "s1a2"
		}
	}
]