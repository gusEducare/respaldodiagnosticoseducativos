json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Su elaboración te permite organizar la información presentarás al público.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Con ellos obtendrás una mejor calificación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los puedes utilizar como apoyo para recordar la información.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Deben tener una estructura que incluye: título, introducción, desarrollo y conclusiones.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Te sirven para presentar buenos exámenes escritos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Te ayudan a reconocer los momentos en que presentarás ejemplos, materiales de apoyo o harás preguntas a la audiencia.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige las opciones relacionadas con la utilidad de elaborar guiones de apoyo para tus presentaciones orales."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Surrealismo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Impresionismo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Futurismo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dadaísmo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Abandonismo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Creacionismo<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Arrastra el contenedor los que identifiques como movimientos surgidos en la Poesía de vanguardia del S XX"
        }
    }

]
