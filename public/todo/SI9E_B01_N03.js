json = [
	{
		"respuestas": [
			{
				"t13respuesta": "Argumento",
				"t17correcta": "1",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Dato",
				"t17correcta": "0",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Opinión",
				"t17correcta": "0",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Argumento",
				"t17correcta": "0",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Dato",
				"t17correcta": "1",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Opinión",
				"t17correcta": "0",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Argumento",
				"t17correcta": "0",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Dato",
				"t17correcta": "1",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Opinión",
				"t17correcta": "0",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Argumento",
				"t17correcta": "1",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Dato",
				"t17correcta": "0",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Opinión",
				"t17correcta": "0",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Argumento",
				"t17correcta": "0",
				"numeroPregunta": "4"
			},
			{
				"t13respuesta": "Dato",
				"t17correcta": "0",
				"numeroPregunta": "4"
			},
			{
				"t13respuesta": "Opinión",
				"t17correcta": "1",
				"numeroPregunta": "4"
			},
			{
				"t13respuesta": "Argumento",
				"t17correcta": "0",
				"numeroPregunta": "5"
			},
			{
				"t13respuesta": "Dato",
				"t17correcta": "0",
				"numeroPregunta": "5"
			},
			{
				"t13respuesta": "Opinión",
				"t17correcta": "1",
				"numeroPregunta": "5"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": ["Nuestro país es el resultado de la mezcla de varias culturas donde podemos encontrar diferentes formas de pensar.",
				"Para 1821 México fue el país más grande de todo el continente Americano y un año más tarde se incorporaron lo que se conocía como las provincias centroamericanas.",
				"Las provincias centroamericanas son lo  que actualmente conocemos como Guatemala, Honduras, El Salvador, Nicaragua y Costa Rica.",
				"México creció aún más y con ello los problemas para mantener la seguridad y presencia del gobierno en todo el territorio.",
				"Los gobernantes mexicanos no estaban preparados para asumir los retos que trajo consigo la independencia del país.",
				"Si hubiéramos sido conquistado por los ingleses, nuestra economía sería mucho más fuerte."],
			"preguntasMultiples": true,
			"columnas": 1
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Progresión temática, es decir, el lector debe reconocer con facilidad el tema del que se está hablando en el ensayo.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Uso indistinto de los signos de puntuación, es decir, que no son fundamentales, si se entiende la idea.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Evitar repetir palabras. Hacer use de sinónimos o palabras del mismo tiempo semántico.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Se puede utilizar un lenguaje formal e informal dentro del mismo texto, siempre y cuando sea entendible.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Empleo adecuado de tipografías, negritas, viñetas o elementos gráficos que ayuden para la comprenpresión del texto.",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Elige las sugerencias para lograr la coherencia y cohesión en un ensayo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "época",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "poeta",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "contexto",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "modernismo",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "industrializada",
				"t17correcta": "5"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "Cada poema registra una visión de mundo que se correlaciona con la&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;en la que se desenvuelve el&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ".<br>Es por esto que resulta pertinente conocer el&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;social, político y moral en el que surgen las obras literarias para así poder comprenderlas y a su vez entender qué nos dicen éstas de la realidad a la que hacen referencia, ya sea que la apoyen o la rechacen. En el caso del&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ", el sentimiento fue de huida y escape a una realidad&nbsp;"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"respuestasLargas": true,
			"ocultaPuntoFinal": false,
			"t11pregunta": "Recorta el tipo de nexo y arrástralo a la definición que corresponda."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Una población muestra.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Todos los individuos que forman parte de una comunidad.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Sectores específicos de la población.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Todas las personas que quieran participar en ella.",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Una encuesta es una técnica de recopilación de datos a través de preguntas que se realizan a ________________________________ para conocer su opinión."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Antropología.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Política.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Economía.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Religión.",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Principalmente, las encuestas se realizan en estudios de:"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Presentar las preguntas.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Tener un objetivo de estudio.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Tener una población muestra.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Captar la atención de la audiencia.",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Dentro de las características de las encuestas, está <sub>&#8213;&#8213;&#8213;&#8213;&#8213;&#8213;</sub> en el que se específica el tema y el propósito de realizar la encuesta."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Abiertas",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Cerradas",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Múltiples",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Concretas",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Las preguntas <sub>&#8213;&#8213;&#8213;&#8213;&#8213;&#8213;</sub> en una encuesta son aquellas que proporcionan respuestas extensas por parte del encuestado."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Encuesta",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Cuestionario",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Población general",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Población muestra",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": " <sub>&#8213;&#8213;&#8213;&#8213;&#8213;&#8213;</sub> es el grupo de individuos de quienes se quiere obtener información y son el objeto de estudio."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Título: incluye el tema, el objeto y la población muestra.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Introducción: en la que se plantean los objetivos de la encuesta y se describe a la población muestra.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Desarrollo: se incluye el cuestionario de preguntas y se explica cómo estás están formuladas en base a lo que se quiere medir. Asimismo, se presenta los resultados obtenidos en organizadores gráficos y su análisis.</p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Conclusiones: se presentan las reflexiones obtenidas a partir del análisis de los resultados.</p>",
				"t17correcta": "3"
			},
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br>",
			"tipo": "ordenar"
		},
		"contenedores": [
			{ "Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."] }
		]
	}
]
