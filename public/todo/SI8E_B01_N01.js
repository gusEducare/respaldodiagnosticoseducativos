json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ERevista de Ocio\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ERevista temática\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ERevista científica\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Elige de entre las opciones una que identifique el tipo de revista que refieren el texto.<br><br>Este tipo de revistas  son dirigidas a un tipo de público con interés en cierta disciplina específica. Se publican de manera  periódica y contienen artículos especializados en un tema en particular identificando a sus autores expertos en el tema. Además están compuestas por una cantidad de artículos, escritos breves sobre un tema de interés, que guardan relación estrecha con el criterio editorial y temático de la revista y que pueden ser de dos tipos; expositivos o de opinión (argumentativos). En ambos casos los artículos incluyen un lenguaje especializado, ya que las revistas van dirigidas a un público en particular que está familiarizado con el tema. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ECitas\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EEjemplos\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EChistes\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ERepeticiones\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EPoemas\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EExplicaciones\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EComentarios\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ERefritos\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 2,
            "t11pregunta": "Señala todos los recursos que puedes emplear para desarrollar tus ideas en un texto escrito."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ESu tamaño\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EExpositivos informan y argumentativos intentan persuadir con una postura específica\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EExpositivos divierten y argumentativos informan.\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál es la diferencia esencial entre los textos expositivos y los argumentativos?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EArgumentos racionales.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EArgumentos de hecho.\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EArgumentos de ejemplificación.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EArgumentos que apelan a los sentimientos.\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Elige la opción que nombra el tipo de argumento que utilizan hechos reales y comprobables para defender una idea."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "si8e_b01_n01_04.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "si8e_b01_n01_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "si8e_b01_n01_06.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "si8e_b01_n01_05.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "si8e_b01_n01_03.png",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena y arrastra los datos para elaborar una ficha bibliográfica.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "SI8E_B01_N01_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "borde":false,
            "tamanyoReal":true

        },
        "contenedores": [
            {"Contenedor": ["", "77,60", "cuadrado", "230,74", ".", "transparent",true]},
            {"Contenedor": ["", "77,352", "cuadrado", "230,74", ".", "transparent",true]},
            {"Contenedor": ["", "201,60", "cuadrado", "230,74", ".", "transparent",true]},
            {"Contenedor": ["", "201,354", "cuadrado", "230,74", ".", "transparent",true]},
            {"Contenedor": ["", "328,60", "cuadrado", "230,74", ".", "transparent",true]}

        ]
    }
];