json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1830 México sufrió la invasión de países como Inglaterra y Alemania.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La guerra de Independencia comenzó en 1910.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La consumación de la independencia de México se da en 1820.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1830, México se encontraba en una mala situación, eso retrasó el progreso y desarrollo de infraestructura en el país.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El siglo XIX es conocido como el Siglo de las Luces en nuestro país.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el siglo XIX  se inicia el periodo del México Independiente.",
                "correcta": "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Constitución de 1824 trajo consigo el nombre oficial de México.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1824, se establece una nueva Constitución donde la República estaba conformada por 31 estados y un Distrito Federal.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para La República la religión Católica era la única religión válida. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A partir de la Constitución de 1824, se establecieron tres poderes: ejecutivo, legislativo y judicial.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El poder ejecutivo, se conformaba, a partir de la Constitución de 1824, por un presidente y un vicepresidente que durarían cuatro años en el cargo. ",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la respuesta correcta.",
            "descripcion": "Aspectos a valorar",
            "anchoColumnaPreguntas": 70,
            "evaluable": true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>gobernantes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>universal<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>gobierno<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>elección<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>secreto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>libre<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>voto<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida":16,
            "t11pregunta": "Selecciona las palabras de la siguiente sopa de letras."
        }
    },
    
    {
        "respuestas": [
            {
                "t13respuesta": "El pueblo podía elegir a sus gobernantes, quienes estarían en el poder por un tiempo específico.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Buscaban que hubiera un emperador en México.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Proponían a Fernando VII para que que gobernara al país.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Republicanos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Iturbidistas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Borbonistas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Une la fuerza política a la que correspondan las siguientes descripciones."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El ejército trigarante en el fin de la guerra de Independencia.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Con la expedición de Isidro Barradas, España intenta reconquistar México.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Francia y Estados Unidos eran aliados de México.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>México tuvo distintas formas de gobierno.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hubo mucha estabilidad social y económica.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las opciones que correspondan a los primeros años de vida independiente de México. "
        }
    },
     {  
      "respuestas":[
         {  
            "t13respuesta":"<p>1820</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>1810</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>1830</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>El Porfiriato</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>La Independencia de México</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Independencia de Texas</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>El páramo</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>La Mesilla</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>La Provincia</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Guerra de los Pasteles entre Francia y México.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Proclamación de la independencia de Texas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Promulgación de la primera constitución de México como país independiente.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Gobierno de la ley</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Gobierno de uno</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>El poder ejecutivo.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>criollos</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>clérigos</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>caudillos</p>",
            "t17correcta":"1",
            "numeroPregunta":"5"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Es la década donde se llevó a cabo la consumación de la Independencia de México","Acontecimiento que ocurrió entre 1821 y 1862","Acuerdo entre Estados Unidos y México para la venta de un territorio.","Elige el acontecimiento que sucedió primero.","Monarquía significa…","Personajes que querían ser dueños de los territorios, en las batallas  que libraron."],
         "preguntasMultiples": true,
         "columnas":1
      }
   }
]