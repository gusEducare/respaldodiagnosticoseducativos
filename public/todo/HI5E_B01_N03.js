json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Se quiere crear una nueva nación separada de España.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Inseguridad por todo el territorio a partir de la lucha.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se consolida la idea de independencia de España.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mueren 600,000 personas, principalmente, clase trabajadora.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Invasiones de países: Estados Unidos, Francia e Inglaterra.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Lucha en armas para conseguir la independencia de España.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Nace un nuevo país. Conflicto interno por disputa del poder.<\/p>",
                "t17correcta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra acontecimientos con las décadas correspondientes.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Primera Década",
            "Segunda Década",
            "Tercera Década"
        ]
    },
    {
      "respuestas":[
         {  
            "t13respuesta":"<p>La Guerra contra países como Estados Unidos, Francia e Inglaterra. </p>",
            "t17correcta":"'0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>La abolición de la esclavitud.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>La Independencia de México.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>La Declaración Universal de los Derechos Humanos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Es libre y secreto.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Es universal, libre, directo y secreto.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Es público y necesario.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Obligatorio a partir de los 18 años.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>La declaración de Independencia en 1822.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>La conformación de un gobierno independiente en 1824.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>La proclamación de la Constitución de 1824.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>La separación de Iglesia-Estado en nuestro país. </p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Las mujeres, analfabetas y pobres no podían votar.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Las elecciones se llevaban a cabo en plazas públicas.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Los criollos no podían votar por ser extranjeros.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Que hubo candidatas mujeres para puestos públicos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Del siglo XX.</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Que la Iglesia Católica lo permitió.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Registrarse ante los organizadores de elecciones.</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las respuestas correspondientes a las siguientes oraciones:<br/>El voto es un proceso de elección de los gobernantes en nuestro país. Se logró a partir de:","Las características del Voto, son: ","El proceso de votación en nuestro país cambió a partir de:","Antes de los cambios en el proceso de votación:","La mujer tuvo derecho a votar a partir de:"],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Baja la producción minera en un 75%.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Surgen declaraciones de igualdad entre las personas.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Los criollos querían una forma de gobierno similar a  E.U.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Había falta de mano de obra.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los indígenas eran explotados por los criollos.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Conflicto entre militares, civiles, clérigos y burócratas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El gobierno se queda sin el pago del tributo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Empeoran las condiciones de peones y artesanos.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p> Los caudillos querían adueñarse de territorios.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El país en bancarrota, pide préstamos a otros países.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p> Hay declaraciones de igualdad para acabar con injusticias.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Los terrenos de los indígenas son tomados por latifundios.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Los criollos no tenían experiencia para gobernar.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La clase media crece por mezcla de razas.<\/p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra la situación con los elementos a los que pertenezcan.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Situación económica",
            "Situación política",
            "Situación social"
        ],
        "css": {
            "altoContenedorRespuesta": "100px",
            "maxAltoContenedorRespuesta": "100px"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>E.U. busca comprar territorio como California y Nvo. México.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Su población era de más estadounidense que mexicanos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se busca comenzar guerra contra el expansionismo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Texas se anexa a Estados Unidos hasta 1845.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Santa Anna establece ”Las Siete Leyes”.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>México rompe relación diplomática con E.U.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Santa Anna deja sin efecto la Constitución de 1824.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los pobladores hablaban inglés y  tenían religión diferente.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los conflictos internos de México causaron su derrota.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>En 1833, se reconoce Texas como estado separado de Coahuila.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra la etapa con el acontecimiento que le corresponde.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ideas para obtener dinero",
            "Ideas para administrar el dinero"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para evitar la guerra, el presidente Herrera manda un enviado para exigir el reconocimiento del río Bravo como límite de los países. La negociación no prosperó.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Santa Anna comienza una guerra con Texas en 1899.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El presidente en turno, José Joaquín Herrera, reafirma la independencia de Texas con el “Tratado de Velasco”.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1848, el ejército de Estados Unidos llega a la Ciudad de México y toma Palacio Nacional.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El “Tratado de Guadalupe Hidalgo”, reconoce la independencia de Texas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estados Unidos pagó 15 millones de indemnización por ceder Nuevo México y California.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estados Unidos declaró la guerra a México en 1846. ",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>delinquir<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>desigualdad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>proliferar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>anarquia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>precario<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>tribu<\/p>",
                "t17correcta": "0"
            }, 
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Cometer un delito."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Condición o circunstancia de no tener una misma naturaleza, cantidad, valor o forma que otro, o de diferenciarse de él en uno o más aspectos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Multiplicarse [algo] con rapidez."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Ausencia total de estructura gubernamental en un Estado."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Que es poco estable, poco seguro o poco duradero."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Agrupación o asociación social y política propia de pueblos primitivos o integrada por un conjunto de personas que comparten un origen, una lengua, unas costumbres ny unas creencias y que obedecen a un ..."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 10,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 5,
                "datoY" : 7
            },
            {
                "direccion" : 1,
                "datoX" : 8,
                "datoY" : 3
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 11
            },
            {
                "direccion" : 1,
                "datoX" : 3,
                "datoY" : 7
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 13
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto":20,
            "ancho":20,
            "t11pregunta": "Completa el siguiente crucigrama."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>tribus<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>apaches<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>precaria<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>delinquir<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>“La Guerra de Castas”<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En el norte varias "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;de indígenas propias de la región como los yaquis, mayos y "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;era quienes,  por la desigualdad social, la situación "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;y la falta de recursos económicas de México, se dedicaban a "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", complicando la vida de los habitantes de toda esta zona. En el otro extremo, del lado sur, se vivía una situación similar; en Yucatán, los indígenas mayas eran víctimas de brutales abusos por parte de las personas de color blanco, lo anterior da inicio a "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;en 1848; durante tres años en guerra, los indígenas se dedicaron a vengarse, por lo que mataron, robaron e incluso quemaron todo a su paso<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "t11pregunta": "Coloca la palabra en el párrafo donde corresponda."
        }
    }
]