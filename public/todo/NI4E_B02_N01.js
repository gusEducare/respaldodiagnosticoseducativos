json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Mariposa<\/p>",
                "t17correcta": "1,2,3,4,5,6"
            },
           {
                "t13respuesta": "<p>Abejorro<\/p>",
                "t17correcta": "2,1,3,4,5,6"
            },
            {
                "t13respuesta": "<p>Colibrí<\/p>",
                "t17correcta": "3,2,1,4,5,6"
            },
            {
                "t13respuesta": "<p>Murciélago<\/p>",
                "t17correcta": "4,5,6,1,2,3"
            },
            {
                "t13respuesta": "<p>Avispa<\/p>",
                "t17correcta": "5,6,1,2,3,4"
            },
            {
                "t13respuesta": "<p>Abeja<\/p>",
                "t17correcta": "6,1,2,3,4,5"
            },
            {
                "t13respuesta": "<p>Koala<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Ardilla<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Saltamontes<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Araña<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Lagartija<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Gusano de seda<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<table style='position: relative; top: 101px; left: 140px;'><tr><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><th>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</th></tr><table>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "ocultaPuntoFinal":true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Colibrí<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pulpo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Salmon<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Avestruz<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tortuga<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cocodrilo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Conejo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>León<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Lobo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Chimpancé<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ballena<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Rinoceronte<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ovíparos",
            "Vivíparos"
        ]
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hongos y las bacterias son muy importantes para  los ecosistemas ya quecontribuyen a mantenerlos  limpios",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son una fuente de contaminación para los ecosistemas",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunos hongos y bacterias se alimentan de restos de animales y plantasmuertos, descomponiéndolos reintegrando nutrientes al suelo",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Únicamente se reproducen en el cuerpo humano",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
           {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre los microorganismos se encuentran algunos hongos y bacterias.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los microorganismos tienen efectos negativos en la vida de otros seresvivos",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se deben tomar medidas de higiene evitar enfermedades de algunosmicroorganismos",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
]


