json = [
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_01_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_06.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_04.png",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el nombre del continente al lugar que le corresponde en el mapa.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n01_01_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "ocultaPuntoFinal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "316,26", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "58,264", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "396,232", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "76,493", "cuadrado", "133, 63", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>África y Europa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>África y Asia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Europa y Asia<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div>¿Entre qué continentes se encuentra el Mar Mediterráneo?<br><img src=\"gi5e_b01_n01_02_01.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div>¿Entre qué continentes se encuentra el canal de Suez en Egipto?<br><img src=\"gi5e_b01_n01_02_02.png\" style='width:200px'\/><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la ubicación que corresponda a la imagen.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n01_03_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n01_03_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n01_03_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n01_03_05.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n01_03_06.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n01_03_07.png",
                "t17correcta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena los continentes de acuerdo a su extensión territorial. Coloca en primer lugar el continente más extenso, y en último lugar, el menos extenso.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n01_03_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": false,
            "borde": false
        },
        "css":{
            "tamanoFondoColumnaContenedores":"inherit",
            "anchoRespuestas":"241"
            
        },
        "contenedores": [
            {"Contenedor": ["", "377,76", "cuadrado", "247, 47", ".", "transparent"]},
            {"Contenedor": ["", "424,76", "cuadrado", "247, 47", ".", "transparent"]},
            {"Contenedor": ["", "471,76", "cuadrado", "247, 47", ".", "transparent"]},
            {"Contenedor": ["", "377,370", "cuadrado", "247, 47", ".", "transparent"]},
            {"Contenedor": ["", "424,370", "cuadrado", "247, 47", ".", "transparent"]},
            {"Contenedor": ["", "471,370", "cuadrado", "247, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n01_04_04.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n01_04_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n01_04_02.png",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el nombre del continente a la imagen con la que se identifique.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n01_04_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "205,74", "cuadrado", "200, 44", ".", "transparent"]},
            {"Contenedor": ["", "417,221", "cuadrado", "200, 44", ".", "transparent"]},
            {"Contenedor": ["", "205,369", "cuadrado", "200, 44", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>fronteras<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>artificiales<\/p>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los países están separados por líneas imaginarias llamadas <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; las cuales pueden ser naturales como océanos o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como un muro.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa la siguiente oración."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_05_05.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_06.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_03.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_02.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_07.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_08.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_09.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_10.png",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_11.png",
                "t17correcta": "9"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ubica al país al lugar que le corresponde.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n02_05_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "contieneDistractores": true,
            "opcionesTexto": true,
            "borde": false,
            "anchoImagen": 70
        },
        "css":{
            "tamanoFondoColumnaContenedores":"contain"
            
        },
        "contenedores": [
            {"Contenedor": ["", "334,42", "cuadrado", "130, 60", ".", "transparent"]},
            {"Contenedor": ["", "57,213", "cuadrado", "130, 60", ".", "transparent"]},
            {"Contenedor": ["", "373,303", "cuadrado", "130, 60", ".", "transparent"]},
            {"Contenedor": ["", "232,448", "cuadrado", "130, 60", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n01_06_03.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_02.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_05.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_09.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_06.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_08.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_07.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_11.png",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_10.png",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_12.png",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "gi5e_b01_n01_06_13.png",
                "t17correcta": "11"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<style>#columnaContenedores{height:2000px !important;}\n\
                                    #tarjetas li{height:38px !important;}</style><p>Observa los siguientes mapas y arrastra el país al lugar que le corresponde.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n01_06_01.png",
            "respuestaImagen": true,
            "bloques": true,
            "contieneDistractores": true,
            "opcionesTexto": false,
            "borde": false
        },
        "css":{
            "tamanoFondoColumnaContenedores":"100% 100"
            
        },
        "contenedores": [
            {"Contenedor": ["", "159,446", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "235,366", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "532,459", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "800,166", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "973,488", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "1019,66", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "1062,215", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "1217,21", "cuadrado","130, 32", ".", "transparent"]},//1358 222
            {"Contenedor": ["", "1358,222", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "1433,75", "cuadrado", "130, 32", ".", "transparent"]},
            {"Contenedor": ["", "1508,106", "cuadrado", "130, 32", ".", "transparent"]},//1508 108
            {"Contenedor": ["", "1510, 406", "cuadrado", "130, 32", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Estados Unidos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Canadá",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "México",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Brasil",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Inglaterra",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Alemania",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "España",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "Rusia",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "China",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "Japón",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "Egipto",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "Sudáfrica",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "Australia",
                "t17correcta": "13"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Washington D.C"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ottawa"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ciudad de México"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Brasilia"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Londres"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Berlín"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Madrid"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Moscú"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Pekín"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Tokio"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "El Cairo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Pretoria"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Camberra"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "<style>#content{height:1680px !important; width: 102% !important;}</style>Relaciona el país con su capital."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n01_07_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n01_07_03.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n01_07_05.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n01_07_04.png",
                "t17correcta": "3",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca en su lugar el nombre del tipo de línea o punto imaginario.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n01_07_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false

        },
        "contenedores": [
            {"Contenedor": ["", "12,77", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "461,76", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "84,453", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "376,400", "cuadrado", "152, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Natural<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cultural<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Económico<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Político<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Social<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div><img src=\"gi5e_b01_n03_08_02.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><img src=\"gi5e_b01_n03_08_01.png\" style='width:200px'\/><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Selecciona el componente que se relaciona con la imagen.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Natural",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Cultural",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Que componente podemos observar principalmente en el siguiente paisaje",
                "valores": ['', ''],
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Que componente podemos observar principalmente en el siguiente paisaje",
                "valores": ['', ''],
                "correcta": "1"

            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<style> .labelCheck{margin-top:30px !important;}</style><p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Japón es más grande que Mexico",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En Mexico hace más calor que en la Antártida",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En Canadá hace más calor que en Mexico",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una cultura es más parecida a otra que está en el mismo continente que a otra en un continente diferente",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Se refiere a los indicadores sociales y culturales de una región.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Son las diferencias físicas y biológicas de las personas de cada lugar.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Son las diferencias políticas, naturales, económicas, culturales y sociales que existen entre los componentes de cada lugar.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué es la diversidad?"
        }
    }
];