json = [
     {
         "respuestas": [
             {
                 "t13respuesta": "<p>0.675<\/p>",
                 "t17correcta": "1"
             },
            {
                 "t13respuesta": "<p><img src='MI7E_B01_N02_01.png'><\/p>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<p>0.75<\/p>",
                 "t17correcta": "3"
             },
             {
                 "t13respuesta": "<p><img src='MI7E_B01_N02_02.png'><\/p>",
                 "t17correcta": "4"
             }
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<p> <\/p>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<p> < <\/p>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<p> < <\/p>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<p> < <\/p>"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "8",
             "t11pregunta": "Ordena de menor a mayor los números que se muestran, arrastra cada uno al lugar que le corresponde. ",
             "ocultaPuntoFinal":true,
             "respuestasLargas":true,
             "anchoRespuesta":60
         }
     },
     {  
       "respuestas":[  
          {  
             "t13respuesta":"<img src='MI7E_B01_N02_01_02.png'>",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"<img src='MI7E_B01_N02_01_03.png'>",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"<img src='MI7E_B01_N02_01_04.png'>",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"<img src='MI7E_B01_N02_01_05.png'>",
             "t17correcta":"0"
          }
       ],
       "pregunta":{  
          "c03id_tipo_pregunta":"1",
          "t11pregunta":"¿Qué fracción corresponde al punto señalado con rojo en la recta numérica?<br>\n\
                         <div style='text-align:center;'><img src='MI7E_B01_N02_01_01.png'></div>"
       }
    },
//     {
//         "respuestas": [
//             {
//                 "t13respuesta": "mi7e_b01_n02_02_02.png",
//                 "t17correcta": "0"
//             },
//             {
//                 "t13respuesta": "mi7e_b01_n02_02_03.png",
//                 "t17correcta": "1"
//             },
//             {
//                 "t13respuesta": "mi7e_b01_n02_02_04.png",
//                 "t17correcta": "2"
//             },
//             {
//                 "t13respuesta": "mi7e_b01_n02_02_05.png",
//                 "t17correcta": "3"
//             }
//         ],
//         "pregunta": {
//             "c03id_tipo_pregunta": "5",
//             "t11pregunta": "<p>Arrastra al lugar que le corresponde la fracción que completa correctamente la operación.<\/p>",
//             "tipo": "ordenar",
//             "imagen": true,
//             "url": "mi7e_b01_n02_02_01.png",
//             "respuestaImagen": true,
//             "bloques": false,
//             "tamanyoReal": true
// 
//         },
//         "contenedores": [
//             {"Contenedor": ["", "201,0", "cuadrado", "54, 76", ".","gray"]}
//         ]
//     },
     {  
       "respuestas":[  
          {  
             "t13respuesta":"<img src='mi7e_b01_n02_02_02.png'>",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"<img src='mi7e_b01_n02_02_03.png'>",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"<img src='mi7e_b01_n02_02_04.png'>",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"<img src='mi7e_b01_n02_02_05.png'>",
             "t17correcta":"0"
          }
       ],
       "pregunta":{  
          "c03id_tipo_pregunta":"1",
          "t11pregunta":"Selecciona la fracción que completa correctamente la operación:<br>\n\
                         <div style='text-align:center;'><img src='mi7e_b01_n02_02_01.png'></div>"
       }
    },
     {
         "respuestas": [
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_03_02.png'>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_03_03.png'>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_03_04.png'>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_03_05.png'>",
                 "t17correcta": "0"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "1",
             "t11pregunta":"<img src='MI7E_B01_N02_03_01.png'>"
         }
     },
    {  
       "respuestas":[  
          {  
             "t13respuesta":"<p>29</p>",
             "t17correcta":"0",
             "numeroPregunta":0
          },
          {  
             "t13respuesta":"<p>38</p>",
             "t17correcta":"1",
             "numeroPregunta":0
          },
          {  
             "t13respuesta":"<p>49</p>",
             "t17correcta":"0",
             "numeroPregunta":0
          },
          {  
             "t13respuesta":"<p>65</p>",
             "t17correcta":"0",
             "numeroPregunta":0
          },
          {  
             "t13respuesta":"<p>76</p>",
             "t17correcta":"1",
             "numeroPregunta":0
          },
          {  
             "t13respuesta":"<p>0.00256</p>",
             "t17correcta":"1",
             "numeroPregunta":1
          },
          {  
             "t13respuesta":"<p>0.00256</p>",
             "t17correcta":"0",
             "numeroPregunta":1
          },
          {  
             "t13respuesta":"<p>0.004096</p>",
             "t17correcta":"0",
             "numeroPregunta":1
          },
          {  
             "t13respuesta":"<p>0.00016384</p>",
             "t17correcta":"1",
             "numeroPregunta":1
          }
       ],
       "pregunta":{  
          "c03id_tipo_pregunta":"2",
          "preguntasMultiples":true,
          "t11pregunta":["Elige el o los números que no pertenecen a cada sucesión:<br><br>\n\
                         a) 5,9,13,17,21,...","b) 1, 0.4, 0.16, 0.064,..."]
       }
    },
     {
         "respuestas": [
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_04_05.png'>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_04_02.png'>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_04_03.png'>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N02_04_04.png'>",
                 "t17correcta": "0"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "2",
             "columnas": 2,
             "t11pregunta": "Selecciona las figuras cuyo perímetro es igual a 4b + 4a:\n\
                     <div style='text-align:center;'><img src='MI7E_B01_N02_04_01.png'></div>"
         }
     },
    {
         "respuestas": [
             {
                 "t13respuesta": "Diagonales del mismo tamaño que se cortan en su punto medio y son perpendiculares.",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "Diagonales del mismo tamaño que no se cortan en su punto medio y no son perpendiculares.",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "Diagonales del mismo tamaño que se cortan en su punto medio, pero no son perpendiculares.",
                 "t17correcta": "3"
             }
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "<img src='mi7e_b01_n02_05_01.png'>"
             },
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "<img src='mi7e_b01_n02_05_02.png'>"
             },
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "<img src='mi7e_b01_n02_05_03.png'>"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "18",
             "t11pregunta": "Relaciona cada cuadrilátero con la opción que describe a sus diagonales"
         }
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "<img src='mi7e_b01_n02_06_a.png'>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src='mi7e_b01_n02_06_b.png'>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<img src='mi7e_b01_n02_06_c.png'>",
                 "t17correcta": "3"
             },
             {
                 "t13respuesta": "<img src='mi7e_b01_n02_06_d.png'>",
                 "t17correcta": "4"
             }
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "Mediana: Perpendicular a la base."
             },
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "Altura: Perpendicular a la base y pasa por su punto medio."
             },
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "Bisectriz: Divide al ángulo superior en dos ángulos iguales."
             },
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "Mediatriz: Pasa por el punto medio de la base y llega al vértice superior."
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "18",
             "t11pregunta": "Relaciona el nombre de la recta notable que está trazada en cada triángulo"
         }
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "<p>$650</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 1
             },
             {
                 "t13respuesta": "<p>$750</p>",
                 "t17correcta": "1",
                 "numeroPregunta": 1
             },
             {
                 "t13respuesta": "<p>$1500</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 1
             },
             {
                 "t13respuesta": "<p>$500</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 2
             },
             {
                 "t13respuesta": "<p>$650</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 2
             },
             {
                 "t13respuesta": "<p>$1250</p>",
                 "t17correcta": "1",
                 "numeroPregunta": 2
             },
             {
                 "t13respuesta": "<p>$650</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 3
             },
             {
                 "t13respuesta": "<p>$500</p>",
                 "t17correcta": "1",
                 "numeroPregunta": 3
             },
             {
                 "t13respuesta": "<p>$1250</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 3
             },
             {
                 "t13respuesta": "<p>20</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 5
             },
             {
                 "t13respuesta": "<p>45</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 5
             },
             {
                 "t13respuesta": "<p>30</p>",
                 "t17correcta": "1",
                 "numeroPregunta": 5
             },
             {
                 "t13respuesta": "<p>40</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 6
             },
             {
                 "t13respuesta": "<p>50</p>",
                 "t17correcta": "1",
                 "numeroPregunta": 6
             },
             {
                 "t13respuesta": "<p>20</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 6
             },
             {
                 "t13respuesta": "<p>50</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 7
             },
             {
                 "t13respuesta": "<p>60</p>",
                 "t17correcta": "0",
                 "numeroPregunta": 7
             },
             {
                 "t13respuesta": "<p>20</p>",
                 "t17correcta": "1",
                 "numeroPregunta": 7
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "2",
             "preguntasMultiples": true,
             "t11pregunta": ["<b><div style='position:relative; left:24px; top:-26px; font-size:20px;'>Elige la respuesta correcta en cada caso y arrástrala al lugar que le corresponde.<br/>Yuridia, Mariana y Elena, cooperaron para comprar una caja de 100 carretes de hilo de costura en $2 500. \n\
                             Yuridia aportó <img src='mi7e_b01_n02_07_01.png' style='height:38px;'>del dinero, Mariana la mitad y Elena el resto.<br>\
                             a)¿Cuánto dinero aportó cada una?</div><br>", "Yiridia:", "Mariana:", "Elena:",
                             "<b><div style='position:relative; left:24px; top:-26px; font-size:20px;'>b) Si reparten los hilos de acuerdo con lo que aportó cada una, ¿Cuántos hilos les corresponden?</div></b>",
                             "Yiridia:", "Mariana:", "Elena:"]
         }
     },
    {
        "respuestas": [
            {
                "t13respuesta": "11",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "7",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "5",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "9",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br/>Si se lanzan 2 dados al mismo tiempo, ¿cuál de los siguientes números tiene más posibilidades de salir, es decir, cuál ocurre con la mayor cantidad de combinaciones?"
        }
    }
]