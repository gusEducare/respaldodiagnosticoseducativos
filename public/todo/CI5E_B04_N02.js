[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>México<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>derecho<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>garantías<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Constitución<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>económica<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>raza<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>religión<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>políticas<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>no<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>motivo<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>discriminación<\/p>",
                "t17correcta": "11"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>En <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, todo individuo tiene <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; a gozar y disfrutar por igual de las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; previstas en la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Por ello, la posición <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; o social, la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, el color, el origen étnico, nacional o familiar, la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, las ideas <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, el idioma o el sexo, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; deben ser <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; o ventaja para las personas.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "ocultaPuntoFinal": true,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Normas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Constitución",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Derechos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Reglamentos",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Elige la palabra que nombre el concepto del párrafo.<br>Constituye el orden normativo e institucional que regula la conducta humana en sociedad, así como postula la justicia para la misma. Esencialmente garantizan el bienestar y convivencia en sociedad"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>bienestar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>acuerdo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>armonía<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>todos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>garantia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>consenso<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>derecho<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>opinión<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>libertad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>votos<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Acuerdo o conformidad en algo de todas las personas que pertenecen a una colectividad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Desacuerdo entre dos o más personas acerca de una algo.<\/p>",
                "t17correcta": "1"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Consenso",
            "Disenso"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Conviviendo armónicamente sin ser parte de las decisiones",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Participando activamente en la toma de decisiones",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Garantizando que voten únicamente todos pensemos de la misma manera.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿De qué manera podemos garantizar el éxito de la democracia?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "De donativos que hacen los ciudadanos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "De la recaudación de impuestos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "De la venta de productos diversos",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El gobierno de México se encarga de administrar los bienes del país; sin embargo, el gobierno no genera riqueza. ¿A través de que mecanismo obtiene recursos para pagar escuelas, maestros, policías, servicios públicos, hospitales gubernamentales y demás gastos que se generan en la administración?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p style='font-size:85%'>Democracia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p style='font-size:85%'>Oligarquía<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:85%'>Monarquía<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p style='font-size:85%'>Tiranía<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p style='font-size:85%'>Oclocracia<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p style='font-size:85%'>Aristocracia<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Forma de gobierno en la que la jefatura del Estado reside en una persona, un rey o una reina, cargo habitualmente vitalicio al que se accede por derecho y de forma hereditaria.",
                "valores": ['', '', '', '', '', ''],
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Refiere a aquellas personas en cuyo nombre recae el poder político y económico de un país, transmutado por derecho hereditario",
                "valores": ['', '', '', '', '', ''],
                "correcta": "5"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Forma de gobierno en la que el gobernante tiene un poder total o absoluto, no limitado por unas leyes, especialmente cuando lo obtiene por medios ilícitos. y abusa de él.",
                "valores": ['', '', '', '', '', ''],
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Forma de gobierno que defiende la soberanía del pueblo y el derecho del pueblo a elegir y controlar a sus gobernantes.",
                "valores": ['', '', '', '', '', ''],
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sistema de gobierno en la que el poder esta en manos de unas pocas personas pertenecientes a una clase social privilegiada.",
                "valores": ['', '', '', '', '', ''],
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gobierno de la muchedumbre o de la plebe. Aristóteles la consideraba como el gobierno de los demagogos en nombre de la muchedumbre y, por tanto, una degradación de la democracia.",
                "valores": ['', '', '', '', '', ''],
                "correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "juventud",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "cooperación",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "justicia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "tolerancia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "respeto",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "resolución",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "solidaridad",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "ganancia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "conveniencia,",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "amor",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "diálogo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "discriminación",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "libertad",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "violencia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "tolerancia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "intolerancia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "paz",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "obligación",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 3,
            "t11pregunta": "Selecciona todos los valores en los que debemos basar la democracia."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdarero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno tiene que garantizar las condiciones y recursos necesarios para que sus ciudadanos vivan en bienestar.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno debe crear empresas exitosas para generar recursos en el país.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los recursos económicos con los que cuenta el país deben ser dirigidos a procurar el bienestar de la mayoría de la población; por ejemplo: Construcción de escuelas, hospitales, servicios de policía y bomberos, entre otros.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno es el encargado de administrar, organizar y distribuir los bienes de los que el país dispone.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno jamás debe rendir cuentas a la ciudadanía del dinero que gasta en beneficio social.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    }
]
