[
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mi familia y yo podemos ayudar a reducir el problema de basura en Mëxico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno es el único que puede controlar el problema de la basura.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La contaminación generada por la excesiva cantidad de basura es un problema que ya afecta a la vida de animales y humanos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Elegir productos que contaminan  menos y son amigables con el ambiente es una acción para controlar el problema.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si generas menos basura consumes la m isma cantidad recursos de la naturaleza.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Romana",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Olmeca",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Zapoteca",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Maya",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Griega",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Teotihuacana",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Rusa",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Ixtlán del Río",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tolteca",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Huasteca",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Chavina",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Mixteca",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Totonaca",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Nazca",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Purépecha",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Mexica",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Paraca",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":3,
         "t11pregunta":"Señala todas las culturas originarias de México."
      }
   },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Elote",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tejocote",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tule",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Azul",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Quetzal",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Baguette",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Almohadón",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Jarrón,",
            "t17correcta":"0"
         },         
         {  
            "t13respuesta":"Nopal",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Bondad",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Tlacuache",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tejocote",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Teporingo",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Zapato",
            "t17correcta":"0"
         },         
         {  
            "t13respuesta":"Zopilote",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Vecindad",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":3,
         "t11pregunta":"Señala todas las culturas originarias de México."
      }
   },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Los hombres no deben hacer labores domésticas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las mujeres están para servir a los hombres.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Un hombre puede golpear a una mujer<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las mujeres no son buenas para las matemáticas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los sueldos de los hombres deben ser mejores que los de las mujeres.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Una esposa debe obedecer a su marido<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Una mujer siempre es débil.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Una mujer solo es respetada si tiene un hombre que proteja.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los niños no pueden jugar con muñecas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hombres y mujeres pueden llevar a cabo labores domésticas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los hombres pueden mostrar sus sentimientos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Las decisiones familiares se toman en pareja.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Todos los miembros del hogar colaboran con las tareas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ambos, hombres y mujeres pueden hacerse cargo de los gastos del hogar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>En un matrimonio las opiniones de ambos son igual de importantes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ninguna persona tiene derecho de violentar a su pareja.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada contenedor lo que corresponda.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ideas sociales erróneas",
            "Ideas sociales acertadas"
        ]
    }   
]