json = [
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Diferencias<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Premisas<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Semejanzas<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Comparación<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Persuación<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Metáfora<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Argumentos<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Ironia<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Desarrollo<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Hipérbole<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Tesis<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Ensayo<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Discursivos<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Paralelismo<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Literarios<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Recursos<\/p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "16",
			"t11pregunta": "Delta sesion 12 a3"
		}
	},



	{
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función primordial de una biografía es brindar información de los logros y estudios académicos de la persona.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se nombra biografía al género literario en el que se narra, en tercera persona, la vida y obra de otra persona.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se nombra biografía al género literario narrado en primera persona donde el autor relata su vida y obra comenzando desde su nacimiento.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la biografía se anotan detalles sobre la educación, contexto histórico, logros, premios, aportes y todo hecho relevante de la vida de la persona.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función principal de una biografía es brindar información y revelar aspectos inéditos del personaje.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las biografias son sobre personajes relevantes por su contribución a la humanidad en ciencia, política, música, literatura, arte, entre otros.",
                "correcta": "0"
            },
			{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las biografías no tratan sobre el contexto o esfuerzos realizados por el personaje antes de ser relevante.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Colaboraría<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Gritaba<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Trabajaban<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Comía<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Carecía<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Solían<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Decían<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Entendíamos<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Usaba<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Participaría<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Sabía<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Escribían<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Llegaban<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Nació<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Dirigió<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Participaron<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Saludé<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Fui<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Salieron<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Era<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Tenías<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Recibió<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Participó<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Escribió<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Colaboró<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Enfatizó<\/p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Zeta sesion 9 a1",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Tiempo copretérito",
			"Tiempo pretérito"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Introducción<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Conclusión<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Desarrollo<br/>- Premisas<br/>- Tesis<br/>- Argumentos<\/p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
			"tipo": "ordenar"
		},
		"contenedores": [
			{ "Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."] }
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Información que se obtienen de los protagonistas o testigos de los hechos.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Entrevistas, videos, audio y video grabaciones, diarios personales, obras literarias del autores.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Biografías, documentales, diccionarios, enciclopedias, ensayos, monografías.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Información que no es directa de la fuente original ya fue compilada por terceras personas.<\/p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Zeta sesion 9 a1",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Fuentes primarias",
			"Fuentes secundarias"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Huichol<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Cancún<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Oro<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Michoacán<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cananea<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Huipil<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cultural<\/p>",
				"t17correcta": "0"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Grupo indígena mexicano conocido por sus hermosas artesanías con coloridas chaquiras, en el 2010 presentaron en el Museo de Arte Popular el “Volchol”"
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Famosa playa mexicana ubicada en la península de Yucatán cuya principal actividad económica es el turismo, nacional e internacional."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Sonora es la entidad federativa que mayor cantidad produce de este metal precioso."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Entidad federativa que produce más aguacate en México."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Mina de cobre más grande de México."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Las mujeres triquis de Oaxaca son reconocidas por su típico…"
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Tipo de diversidad que se refiere a las costumbres y tradiciones que un grupo de personas comparten."
			}
		],
		"pocisiones": [
			{
				"direccion": 1,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 8,
				"datoY": 5
			},
			{
				"direccion": 1,
				"datoX": 6,
				"datoY": 3
			},
			{
				"direccion": 0,
				"datoX": 2,
				"datoY": 3
			},
			{
				"direccion": 1,
				"datoX": 10,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "15",
			"t11pregunta": "Delta sesion 12 a3"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>1922<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>1975<\/p>",
				"t17correcta": "2,10"
			},
			{
				"t13respuesta": "<p>1974<\/p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>1998<\/p>",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "<p>2010<\/p>",
				"t17correcta": "5"
			},
			{
				"t13respuesta": "<p>1947<\/p>",
				"t17correcta": "6"
			},
			{
				"t13respuesta": "<p>1966<\/p>",
				"t17correcta": "7"
			},
			{
				"t13respuesta": "<p>1970<\/p>",
				"t17correcta": "8"
			},
			{
				"t13respuesta": "<p>1981<\/p>",
				"t17correcta": "9"
			},
			{
				"t13respuesta": "<p>1975<\/p>",
				"t17correcta": "10,2"
			},
			{
				"t13respuesta": "<p>1976<\/p>",
				"t17correcta": "11"
			},
			{
				"t13respuesta": "<p>1978<\/p>",
				"t17correcta": "12"
			},
			{
				"t13respuesta": "<p>1980<\/p>",
				"t17correcta": "12"
			},
			{
				"t13respuesta": "<p>1981<\/p>",
				"t17correcta": "13"
			},
			{
				"t13respuesta": "<p>1984<\/p>",
				"t17correcta": "14"
			},
			{
				"t13respuesta": "<p>1986<\/p>",
				"t17correcta": "15"
			},
			{
				"t13respuesta": "<p>1989<\/p>",
				"t17correcta": "16"
			},
			{
				"t13respuesta": "<p>1993<\/p>",
				"t17correcta": "17"
			},
			{
				"t13respuesta": "<p>1991<\/p>",
				"t17correcta": "18"
			},
			{
				"t13respuesta": "<p>1995<\/p>",
				"t17correcta": "19"
			},
			{
				"t13respuesta": "<p>1997<\/p>",
				"t17correcta": "20,21"
			},
			{
				"t13respuesta": "<p>1997<\/p>",
				"t17correcta": "21,20"
			},
			{
				"t13respuesta": "<p>2000<\/p>",
				"t17correcta": "22"
			},
			{
				"t13respuesta": "<p>2002<\/p>",
				"t17correcta": "23"
			},
			{
				"t13respuesta": "<p>2004<\/p>",
				"t17correcta": "24"
			},
			{
				"t13respuesta": "<p>2005<\/p>",
				"t17correcta": "25"
			},
			{
				"t13respuesta": "<p>2006<\/p>",
				"t17correcta": "26"
			},
			{
				"t13respuesta": "<p>2008<\/p>",
				"t17correcta": "27"
			},
			{
				"t13respuesta": "<p>2009<\/p>",
				"t17correcta": "28"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p><br/><br/><br/><br/><br/><br/><br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Nacimiento<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Codirector del Diario de Noticias<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Se sumó a la Revolución de los Claveles.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Premio Nobel de Literatura.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Fallecimiento<br/><b>Obras Publicadas</b><br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Tierra de pecado<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Los poemas posibles<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Probablemente alegría<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Viaje a Portugal<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;El año 1993<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Manual de pintura y caligrafía<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Casi un objeto<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Alzado del Suelo<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;En Memorial del convento<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;El año de la muerte de Ricardo Reis<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;La balsa de piedra<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Historia del cerco de Lisboa<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;In nomine Dei<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;El Evangelio según Jesucristo<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Ensayo sobre la ceguera<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Cuadernos de Lanzarote<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Todos los nombres<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;La caverna<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;El hombre duplicado<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Ensayo sobre la lucidez<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Las intermitencias de la muerte<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Las pequeñas memorias<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;El viaje del elefante<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Caín<br/><\/p>"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Completa el siguiente p&aacute;rrafo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Onomatopeyas</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Hipérbole</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cartela</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Caricatura</p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Retrato exagerado que presenta las caracteristicas de una persona o situación social. Busca generar crítica social a partir de la burla"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Mensaje implícito</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cartón o viñeta</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Ironía</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Onomatopeyas</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Area rectangular en la que se dibujan a los personajes o escenarios."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Globos de dialogos</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Elementos alegóricos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Mensaje implícito</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cartón o viñetas</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Espacio en donde se presenta lo que dicen o piensan los personajes."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Onomatopeyas</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Hipérbole</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cartela</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Caricatura</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Espacio superior de la viñeta en donde se incluye un enunciado que no pertenece a ningún personaje."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Globos de dialogos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Elementos alegóricos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Mensaje implícito</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Cartón o viñetas</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "No se presentan de manera evidente por lo que le lector debe realizar una interpretación."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Globos de dialogos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Elementos alegóricos</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Mensaje implícito</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cartón o viñetas</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Son simbolos #@¿! o animales con connotación negativa para representar sentimientos negativos."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Onomatopeyas</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Hipérbole</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cartela</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Caricatura</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Las onomatopeyas son palabras que imitan el sonido de una acción. En las caricaturas su función es recrear en la mente del lector."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Onomatopeyas</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Hipérbole</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Cartela</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Caricatura</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Pretenden dar a entender la idea contraria al mensaje que se está planteando."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Onomatopeyas</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Hipérbole</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Cartela</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Caricatura</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Recurso propio de las caricaturas. Aumentan o disminuyen caracterisiticas para logar un fin humorístico, burla o evidenciar situaciones de desigualdad."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Persuación",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Burla",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "Hipérbole",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "Ensayo",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "Comparación",
				"t17correcta": "5"
			},
			{
				"t13respuesta": "Paralelismo",
				"t17correcta": "6"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Se pretende convencer sobre un punto, idea u opinión a otros."
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Burla que se usa para dar a entender lo contrario a lo que se dijo"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Exageración de cualidades o características de personas u objetos"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Texto argumentativo narrado en primera persona, plantean comentarios y puntos de vista personalessobre determinado tema"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Establecer semejanzas y diferencias relacionado una cosa con otra"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Relacionar partes del texto similares pero que no comparan una con otra sino que tienen estructuras o incluso palabras iguales o sinónimas"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "18",
			"t11pregunta": "s1a2"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Adverbios<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Histórico<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Aposiciones<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Adjetivos<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Participios<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Copretérito<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Habitual<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Pretérito<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Atemporal<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Pronombres<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Sinónimos<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Nexos<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Presente<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Tiempo<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Indicativo<\/p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "16",
			"t11pregunta": "Delta sesion 12 a3"
		}
	}
]
