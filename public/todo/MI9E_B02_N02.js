json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>(a + 2b)(a + 2b)</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>(a + 5)(a + 5)</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>(a - 3b)(a + 3b)</p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>(a + 1)(a + 8)</p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "a<sup>2</sup> + 4ab + 4ab<sup>2</sup>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "(a + 5)<sup>2</sup>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "a<sup>2</sup> - 9b<sup>2</sup>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "a<sup>2</sup> + 9b + 8"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las siguientes ecuaciones con el resultado correspondiente:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Error \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>2</span><span class='bottom'>15</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>8</span><span class='bottom'>12</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>8</span><span class='bottom'>15</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El  profesor de química prepara la feria de ciencias y rifa las actividades a desarrollar por sus alumnos. De una caja los alumnos que participarán sacan un papel con el nombre de la actividad que les corresponderá. 4 papelitos dicen juegos, 3 papelitos dicen experimentos en vivo y cinco papelitos dicen guía de la exposición de trabajos. <br>Calcula la probabilidad de:<br>\n\
                            Sacar un papelito que indique hacer experimento en vivo o ser guía de la exposición de trabajos"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>1</span><span class='bottom'>6</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>7</span><span class='bottom'>12</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>2</span><span class='bottom'>7</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>7</span><span class='bottom'>15</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "No sacar un papel que indique ser guía de la exposición de trabajos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 4x<sup2</sup> + 12x + 9 = (2x + 3)(2x - 3) = 81 <br>\n\
                                                (2x + 3)(2x - 3) = 27(3)<br>\n\
                                                2x + 3 = 27 <br>\n\
                                                2x = 27 - 3 = 24 <br>\n\
                                                x= 24/12 = 12<br>\n\
                                                y <br>\n\
                                                2x - 3 = 3 <br>\n\
                                                2x = 3 + 3 = 6<br>\n\
                                                x= 6/2 = 3<br>\n\
                                                \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 4x<sup2</sup> + 12x + 9 = (2x + 3)(2x - 3) = 81 <br>\n\
                                                entonces: <br>\n\
                                                (4x -3) = 81 <br>\n\
                                                4x = 81 + 3 = 84 <br>\n\
                                                Error<br>\n\
                                                y <br>\n\
                                                (x - 3) = 1 <br>\n\
                                                x = 1 + 3 = 4 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 4x<sup2</sup> + 12x + 9 = 81 <br>\n\
                                                4x<sup2</sup> + 12x + 9= (2x + 3)<sup>2</sup> = 81<br>\n\
                                                <div style='display:inline-block; vertical-align:top; font-size:24px; font-weight:bold;'>&#8730;\n\
                                                    <\/div><div style='display:inline-block;'>\n\
                                                        <hr style='padding:0; margin:0; background-color:black; height:3px;'>\n\
                                                            (2x + 3)<sup>2</sup>\n\
                                                    <\/div>\n\
                                                    = \n\
                                                <div style='display:inline-block; font-size:24px; font-weight:bold;'>&#8730;\n\
                                                    <\/div><div style='display:inline-block;'>\n\
                                                        <hr style='padding:0; margin:0; background-color:black; height:3px;'>\n\
                                                             81\n\
                                                    <\/div><br>\n\
                                                2x + 3 = &#177;9 entonces<br>\n\
                                                2x = 9 - 3 = 6 <br>\n\
                                                x = 6/2 = 3<br>\n\
                                                y <br>\n\
                                                2x = -9 -3 =-12 <br>\n\
                                                x = -12/2 = -6\u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Elige el inciso que muestra el procedimiento correcto para resolver la ecuación: 4x<sup>2</sup> + 12x + 9 = 81"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E x + 2 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E x - 3 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E x + 3 \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El área de un cuadrado es x<sup>2</sup> + 6x + 9 ¿En términos de x, cuánto mide su lado?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 257 \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 287 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 223 \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "En un triángulo rectángulo el valor de los lados es 32 y 255 ¿Cuál es el valor de la hipotenusa?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 38 m. \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 32 m. \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 24 m. \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Se sabe que se utilizó un cable de 25 m para anclar una torre de comunicación. El cable se fijó desde la punta de la torre al piso a 7 m de distancia de la base de la torre. ¿Cuál es la altura de la torre?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>7</span><span class='bottom'>30</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>11</span><span class='bottom'>30</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>4</span><span class='bottom'>30</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Observa el tablero del juego de canicas y responde las preguntas: <br>\n\
                             1. ¿Cuál es la probabilidad de  acertar  en un número par mayor que 4?<br><br>\n\
                                <center><img src='mat_pb2_s02_a06_02_b.png'></center>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>14</span><span class='bottom'>30</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>14</span><span class='bottom'>29</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>13</span><span class='bottom'>30</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Si el primer tiro la canica cae en un número par ¿Cuál es la probabilidad de que en el segundo tiro la canica caiga en un número impar?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_a.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_b.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_c.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_d.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": "2",
            "t11pregunta": "Elige ¿Cuál de las siguientes figuras se construyó utilizando simetría axial y traslación de figuras?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Base= -6, altura=-24  \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Base= 24, altura=6 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Base= 6, altura=24 \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E Base= 9, altura=16 \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El área de un triángulo rectángulo es 72 cm<sup>2</sup>. Si la altura del triángulo es cuatro veces la medida de la base, eligiendo como base y altura los catetos, ¿Cuánto mide de base y cuánto mide de altura?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_a.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_b.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_c.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb2_s02_a06_03_d.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": "2",
            "t11pregunta": "¿Qué cuadro corresponder a una rotación de 180° en sentido anti horario del texto que se muestra?<div align='center'><img src='mat_pb2_s02_a06_03_d.png'></div>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E  <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>7</span><span class='bottom'>12</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>5</span><span class='bottom'>12</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <div style='margin-left:4px; margin-top:-42px; display:'inline-block'><span class='fraction'><span class='top'>4</span><span class='bottom'>12</span></span></div> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "En un bolso hay tres canicas verdes, cuatro blancas y cinco azules.¿Cuál es la probabilidad de sacar una canica que no sea azul?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png' width='100'> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png' width='100'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png' width='100'> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas": true,
            "t11pregunta": "Indica que figura corresponde a una traslación horizontal del avión (usa la nube como referencia)<div style='text-align:center'><img src='.png'></div>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 11 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 12 \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 13 \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas": true,
            "t11pregunta": "¿Cuánto debe medir el lado <i>a</i> para que la suma de las áreas verdes sea igual al área azul si el área b<sup>2</sup>= 25 y c<sup>2</sup> = 169?<br><br><div style='text-align:center'><img src='.png'></div>"
        }
    }
]