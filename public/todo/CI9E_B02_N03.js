json=[
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las capacidades de una persona son elementos con los que nacemos y sólo pueden desarrollarse, si se nacen con ellas. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El éxito o fracaso de un proyecto depende en gran medida de los medios puestos para alcanzarlo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El desarrollo integral de una persona, se alcanza cuando puede ver realizados sus objetivos en cada uno de los ámbitos en los que se ve inmerso. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aunque se trate de una tarea difícil, debe ser necesario poder superar las expectativas de nuestras familias. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para la toma de decisiones eficiente, es necesario poder realizar acciones de forma reaccionaría sin pensar demasiado en las consecuencias. ",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>Capacidad<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>confundirse<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>habilidad<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>voluntad<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>capaces<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>hábiles<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>desempeñarlas<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp; y habilidad son dos conceptos que pueden &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;  o  parecer iguales, su diferencia radica en que la  &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;es un ejercicio de la capacidad se desarrolla con la &nbsp; <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;. Si bien todas las personas somos &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;de realizar determinadas tareas o actividades no todas somos &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;para &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;<br>Con el conocimiento y desarrollo de las &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;se favorece el logro de tus &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;de vida<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>Elegir la misma profesión de mis padres o algún familiar.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Evaluar mis capacidades y habilidades para relacionarlas con un área específica de conocimiento.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Analizar las posibles consecuencias de mi elección de carrera en todos los planos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evaluar el impacto social de las profesiones de mi interés.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Preguntar qué tan rico seré al terminar desarrollar mi profesión.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Estar seguro de los días de vacaciones y festivos que puedo tener en mi profesión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Recabar información suficiente de las profesiones de mi interés. Como:  alternativas de acción, posibilidad de crecimiento, oportunidad laboral fuera de tu ciudad o del país, etc<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
   
 {
        "respuestas": [
            {
                "t13respuesta": "Puedes ser originado por el fracaso en el logro de metas u objetivos planteados.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Permite a los individuos identifican fortalezas y áreas de mejora en la vida y reconocer que el logro de los objetivos también depende de la relación y colaboraacion con otros.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Reconocer una satisfacción por los logros alcanzados.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Lugar donde el ser humano se identifica y desarrolla, ofrece retos.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "situacion que se espera ocurra en el futuro puede ser o no realista. También se puede dar de forma personal, familiar y hasta social.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "El primero corresponde a la identificacion personal a partir del reconocimiento de las caracteristicas personales y el segundo es el reconocimiento de las mismas caracteristicas pero en relacion con otras personas y su entorno.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Frustrccion"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Competencia"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Entorno"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Realización personal"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Expectativa"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Principios básicos de unidad y dualidad"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
 {
        "respuestas": [
            {
                "t13respuesta": "Hacerse cargo de las consecuencias de sus actos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Respeto a las opiniones de otros, aun cuando no estés de acuerdo con las mismas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Platica con otras personas en la búsqueda de lograr acuerdos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Persona que habita determinado país y se encuentra sujeto a las leyes del mismo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Obligación de las instituciones gubernamentales a brindar rendimiento de cuentas a sus gobernados.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Tomar en cuenta todas las opiniones de los involucrados para llegar a un acuerdo",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Cooperación, unión entre personas o instituciones para lograr metas en común.",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Responsabilidad"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Tolerancia"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Diálogo"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Ciudadano"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Transparencia"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Consenso"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Cooperación"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    }
]