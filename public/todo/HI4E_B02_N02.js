json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Hidalgo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Yucatan",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Tabasco",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Distrito Federal",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Toltecas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mayas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Olmecas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Aztecas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "ocultaPuntoFinal": true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Relaciona el estado de la República Mexicana con la cultura que se desarrolló en dicho lugar."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Veracruz<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Olmecas<\/p>",
                "t17correcta": "2"
            },{
                "t13respuesta": "<p>talentos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>mercancías<\/p>",
                "t17correcta": "4"
            },{
                "t13respuesta": "<p>La Venta<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>maíz<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>comunidad<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; se dedicaban al cultivo de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, frijol y calabaza.  Sus dirigentes religiosos eran llamados “chamanes”. Su sociedad  se dividía  de acuerdo a sus <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. <br>Sus centros ceremoniales eran usados como centro de vida en <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y el intercambio de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Los más conocidos son:<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, en Tabasco; San Lorenzo en <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras en los espacios que faltan para completar las frases.",
            "pintaUltimaCaja":false
            
        }
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Olmecas</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Zapotecas</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Mixteca</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Purépecha</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Teotihuacana</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Tolteca</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Mexica</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Maya</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Teotihucana</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Tolteca</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Mexica</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Maya</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Esta cultura es considerada la madre de las culturas mesoamericanas utilizaron materiales de diferentes lugares como Guerrero o Puebla, pero se localizaron principalmente en Veracruz y Tabasco.",
                         "Es una de las culturas más importantes y prolíficas por los avances en astronomía, matemáticas y arquitectura. Se estableció en México y Centroamérica. ",
                         "Esta cultura se localizó principalmente en la zona del Valle de México. Los sacerdotes eran quienes gobernaban. Presentaron grandes avances en arquitectura, astronomía, matemáticas, etc. No se sabe el motivo de su colapso."],
         "preguntasMultiples": true,
         "pintaUltimaCaja":false,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Conjunto de creencias, valores y sistemas de conocimiento que articulan la  vida social de los grupos indígenas. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Inscripción o imagen tallada en una laja de piedra, para transmitir un mensaje o acontecimiento histórico.",
                "t17correcta": "2"
            },
             {
                "t13respuesta": "Bloque de piedra del que tallaban los costados y el frente.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Que trae desgracias y causa tristeza, dolor o sufrimiento moral.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Altar"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cosmovisión"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Infausto"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Estela"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "ocultaPuntoFinal": true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Relaciona los siguientes elementos con sus respectivas definiciones."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Oaxaca<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>500 a.C.  al 1200 d.C.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Punto y barra<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Monte Albán<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>Chiapas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>300 a.C a 1400 d.C.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Pictografía<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Palenque<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>Veracruz<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>1400 a.C. a 400 a.C.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Símbolos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Chichén Itzá<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<style>#texto{margin-top:80px;}</style><center><table style='background-color:#3d85c6;'>\n\
                                    <tr>\n\
                                        <td width='250px' style='color:white; font-size:22px; text-align:center;' colspan='2'><b>Cultura Zapoteca</b></td>\n\
                                       </tr>\n\
                                        \n\
                                    <tr>\n\
                                        <td style='background-color:#cfe2f3; font-size:16px;'>Ubicación</td>\n\
                                        <td style='background-color:#cfe2f3; font-size:16px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='background-color:#cfe2f3; font-size:16px;'>Periodo</td>\n\
                                        <td style='background-color:#cfe2f3; font-size:16px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='background-color:white; font-size:16px;'>Sistema de numeración</td>\n\
                                        <td style='background-color:white; font-size:16px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='background-color:#cfe2f3; font-size:16px;'>Zona arqueológica</td>\n\
                                        <td style='background-color:#cfe2f3; font-size:16px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td>\n\
                                    </tr>\n\
                                </table></center>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "respuestasLargas":true,  
            "anchoRespuestas":150,
            "soloTexto":true,
            "contieneDistractores":true,
            "ocultaPuntoFinal":true,
            "t11pregunta": "Completa el siguiente cuadro que habla sobre los aspectos más importantes de la cultura Zapoteca."
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Calendario más exacto del mundo en su época.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso de la guerra para la obtención de tributos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p> Astronomía avanzada que calculaba eclipses solares.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sistema numérico considerado el más importante de Mesoamérica.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Algunas de sus ciudades más importantes son: Tzintzuntzan, Tula y Monte Albán.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona 3 aportaciones de la Cultura Maya."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No se sabe a ciencia cierta cuál fue el motivo para el declive de la civilización maya, \n\
                                aunque algunas de las teorías se basan en el desarrollo de culturas como la Olmeca, \n\
                                que obstruyeron el comercio y poco a poco algunas ciudades colapsaron.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Responde si la siguiente afirmación es verdadera o falsa.<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Calendario organizado en 18 meses de 20 días. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Desarrollo de sistema numérico con el uso del cero.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Estelas  altares entre sus esculturas más famosas.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
                        {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Aztecas"
            },{
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Mayas"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Olmecas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las aportaciones de cada una de las culturas revisadas en el bloque."
        }
    }    
];