json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>justicia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>armonía<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>utilidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tradiciones<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tolerancia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>cambio<\/p>",
                "t17correcta": "0"
            }
            
            ,
            {
                "t13respuesta": "<p>libertad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>arte<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>belleza<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>orden<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    
       {
        "respuestas": [
            {
                "t13respuesta": "Situaciones donde las acciones se realizan por una creencia personal y la aceptación con actos autónomos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Conciencia centrada en la obediencia y alentada por la aceptación o rechazo de alguien que tiene una autoridad sobre nosotros.",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Convicción"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Obligación o condicionamiento"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
]