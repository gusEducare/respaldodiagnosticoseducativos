json=[
        {
        "respuestas": [
            {
                "t13respuesta": "<p>rapidez<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>velocidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>trayectoria<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>tiempo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>movimiento<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>distancia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>direccion<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Da informacion sobre la variación temporal, es la distancia que recorre un objeto en un determinado periodo de tiempo"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Concepto que hace alusión a la rapidez, dirección y trayectoria de los objetos en movimiento."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Línea descrita o recorrido que sigue alguien o algo al desṕlazarse de un punto a otro."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Magnitud física con la que medimos la duración o separación de acontecimientos, sujetos a cambio."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Tiene una serie de características que ocurren de manera sumultánea, dos de las cuales son la rapidez y la velocidad."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Espacio, considerado desde una perspectiva lineal, entre una persona o cosa y otra."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Indicación de la orientación o destino de un cuerpo en movimiento."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 3,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 8,
                "datoY" : 5
            },
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 3
            },
            {
                "direccion" : 0,
                "datoX" : 2,
                "datoY" : 3
            },
            {
                "direccion" : 1,
                "datoX" : 10,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 3,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 7
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3",
            "ancho":15,
            "alto":15
        }
    },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EOído externo\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído medio\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído interno\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Está formado por una membrana llamada tímpano, la cavidad timpánica y por tres huesecillos llamados martillo, yunque y estribo."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EOído externo\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído medio\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído interno\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Está conformado por la oreja y el canal auditivo, donde las orejas funcionan como antenas parabólicas diseñadas para captar los sonidos y están formadas de cartílago para captar mejor las ondas sonoras."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EOído externo\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído medio\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído interno\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es una cámara llena de aire, y parar trasmitir el sonido de una manera más eficiente, la trompa de Eustaquio ayuda a igualar la presión del aire."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EOído externo\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído medio\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído interno\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Está formado por un órgano llamado caracol o cóclea. Este órgano transforma las vibraciones sonoras y éstas se conducen a través del nervio auditivo que manda la información al cerebro."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EOído externo\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído medio\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOído interno\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El sonido se concentra en un canal principal de la oreja llamado pabellón y de ahí se dirigirse al canal auditivo para pasar a la siguiente parte del oído."
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "1",
                // "coordenadas": "150,150"  // top, left
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "0",
                // "coordenadas": "150,350"
            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "1",
                // "coordenadas": "150,550"
            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "1",
                // "coordenadas": "350,150"
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "1",
                // "coordenadas": "350,350"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "0",
                // "coordenadas": "350,550"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las opciones que correspondan a ruidos y tiempos que pueden ser dañinos para la salud, así como efectos negativos",                        
            // "secuencia": true,
            // "ordenados": true,
            // "url":"rtp_k_s03_a01.png"
        }
    },
      {
      "respuestas": [
          {
              "t13respuesta": "<p>Electrón (Carga negativa)</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Núcleo (Carga positiva(protones)</p>",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "Orbitales",
              "t17correcta": "2",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra los elementos de la estructura de un átomo.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "bloques":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "200,250", "cuadrado", "300, 100", "."]},
          {"Contenedor": ["", "350,250", "cuadrado", "300, 100", "."]},
          {"Contenedor": ["", "500,250", "cuadrado", "300, 100", "."]}
      ]
  },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Saturno<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sol<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mercurio<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Luna<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Venus<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Tierra<\/p>",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Es un planeta gaseoso formado por hidrógeno y hielo. Único planeta con sistema de anillos visibles desde la Tierra, los cuáles están compuestos de polvo, rocas y agua congelada. Cuenta con 18 satélites. Su periodo de rotación es de 9 horas y 55 minutos y el de translación de 11 años y 314 días.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Único astro del Sistema Solar que produce energía que emana en forma de rayos ultravioletas, gama, luz y calor. Tiene varias capas. Se calcula una edad aproximada de cien mil millones de años. La luz, tarda ocho minutos en llegar a la Tierra.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Planeta más pequeño, no tiene atmósfera ni satélites. Está formado principalmente por un 70% de elementos metálicos y un 30% de silicatos. Su periodo de translación es de 88 días y el de rotación de 58.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Su periodo de rotación y translación es de 28 días. No tiene atmósfera y su superficie está formada de oxígeno, aluminio y silicio. Sus fases dependen de su posición con respecto al Sol. Tiene un diámetro de 3480 kilómetros.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Es un planeta similar a la Tierra pero no tiene océanos , pero si llanuras atravesadas por ríos de lava y montañas. No tiene satélites. Su atmósfera está compuesta por dióxido de carbono extremadamente caliente. Su período de traslación es de 225 días.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Planeta que tiene un sólo satélite. Su periodo de rotación es de 24 horas y el de traslación de 365 días y 6 horas.  Es un planeta rocoso y tiene un diámetro de 12756 km. Se encuentra a una distancia de 1 UA.<br/><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la respuesta correcta utilizando las palabras del recuadro"
        }
    }
]