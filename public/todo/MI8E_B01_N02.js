json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>+1 152<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>–192<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>–12<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>+24<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>a x c =&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> b x c =&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>&nbsp;d x c =&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;a &divide; b =&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Anota el resultado de cada operación, toca el recuadro y elige el signo y los números que corresponde.<br>a = –144 b = 24 c = –8 d = –96"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "9<sup>14</sup>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "9<sup>16</sup>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "9<sup>-8</sup>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "9<sup>8</sup> X 9<sup>6</sup>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "(9<sup>4</sup>)<sup>4</sup>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<span class='fraction'><span class='top'>9<sup>4</sup></span><span class='bottom'>9<sup>12</sup></span></span>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ángulo i mide 126° por ser alterno interno del ángulo e, que a su vez es correspondiente con el ángulo de 54°.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ángulo g mide 126° ya que es correspondiente con <i>f</i> que a su vez es colateral del ángulo que mide 54°.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Por la relación entre sus medidas, los ángulos <i>a, b, i</i> y <i>d</i>, forman un paralelogramo.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>12 cm, 9 cm, 7 cm<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>14 cm, 6 cm, 8 cm<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>18 cm, 15 cm, 21 cm<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>4 cm, 7 cm, 5 cm<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "En el grupo de Hugo propusieron las siguientes triadas para construir un triángulo. Elige las opciones con las que sí se pude construir un triángulo."
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E76 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E68 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E72 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E86 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Cuál es la superficie que ocupa la parte amarilla en la figura de la derecha?"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E112 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E140 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E124 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E136 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Cuál es el área de la figura original?"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E738 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1 260 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E369 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E848 cm<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Elige la opción que representa el área total del siguiente cuerpo"
      }
  },
 {
        "respuestas": [
            {
                "t13respuesta": "28 %",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "35 %",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "70 de 280"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "161 de 460"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
{  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E$28 584.75\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E$28 375\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E$29 596.25\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E$27 855\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Paola quiere empezar a ahorrar y metió al banco $25 000 a un plazo fijo de 9 meses y el banco le ofreció un plan con interés compuesto de 1.5% mensual.<br><br>¿Cuánto dinero tendrá Paola al final de los nueve meses?"
      }
   },
  {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si se saca un pelota rosa, es más probable que la segunda pelota sea verde que rosa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si primero se saca una pelota verde, es más probable que la segunda pelota sea verde que rosa.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El evento que salgan dos pelotas del mismo color es menos probable que salgan dos pelotas de diferente color.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>mediana<\/p>",
                "t17correcta": "1,2,4"
            },
            {
                "t13respuesta": "<p>mediana<\/p>",
                "t17correcta": "2,1,4"
            },
            {
                "t13respuesta": "<p>media<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>mediana<\/p>",
                "t17correcta": "4,2,1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><table style='margin-left: 140px;  background-color:#3d85c6; '><tr><td style='text-align: center; font-size: 22px; color:white;'>Papeleria</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>1</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>2</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>3</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>4</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>5</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>6</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>7</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>8</td><td style='text-align: center; font-size: 20px; background-color:#cfe2f3;'>9</td><tr><tr><td style='text-align: center; font-size: 20px; color:white;'>Cuaderno</td><td style='text-align: center; font-size: 20px; background-color:white;'>$22</td><td style='text-align: center; font-size: 20px; background-color:white;'>$21.5</td><td style='text-align: center; font-size: 20px; background-color:white;'>$20.5</td><td style='text-align: center; font-size: 20px; background-color:white;'>$32</td><td style='text-align: center; font-size: 20px; background-color:white;'>$19.5</td><td style='text-align: center; font-size: 20px; background-color:white;'>$20</td><td style='text-align: center; font-size: 20px; background-color:white;'>$27</td><td style='text-align: center; font-size: 20px; background-color:white;'>$23.5</td><td style='text-align: center; font-size: 20px; background-color:white;'>$21</td></tr></table><br> La&nbsp;"
            },{
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "&nbsp;del precio del cuaderno es $21.<br>La&nbsp;"
            },{
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "&nbsp;es un dato representativo del precio de los cuadernos.<br>La&nbsp;"
            },{
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "&nbsp;se ve afectada por el precio más alto, por lo que no puede representar al conjunto de datos.<br>Si se agrega el precio de una décima papelería la&nbsp;"
            },{
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "&nbsp;no se modifica mucho independientemente del costo del producto."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "pintaUltimaCaja": false
        }
    }
]