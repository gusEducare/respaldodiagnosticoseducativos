json=[  
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Ayudan a mantener el cuerpo saludable y en buen funcionamiento.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Los podemos encontrar en carnes rojas, pollo, pescado y huevos.</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Son la principal fuente de energía del organismo.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Los podemos encontrar en cereales y tubérculos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Ayudan a mantener el cuerpo saludable y en buen funcionamiento.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Los podemos encontrar en carnes rojas, pollo, pescado y huevos.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Son la principal fuente de energía del organismo.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Los podemos encontrar en cereales y tubérculos.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Podemos encontrarlos en aceites de cocina, mantequillas, margarinas, mantecas y semillas.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Podemos encontrarlos en frutas y verduras.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Aportan energía pero es recomendable consumirlos en pocas cantidades.</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Ayudan a la formación y reparación de tejidos como huesos y músculos del cuerpo.</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "<p>Podemos encontrarlos en frutas y verduras.</p>",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Podemos encontrarlos en aceites de cocina, mantequillas, margarinas, mantecas y semillas.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Aportan energía pero es recomendable consumirlos en pocas cantidades.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Ayudan a la formación y reparación de tejidos como huesos y músculos del cuerpo.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las opciones que correspondan con los nutrientes presentados.<br>Minerales y Vitaminas:","Carbohidratos:","Proteínas:","Grasas:"],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"IMC= Tu estatura por (x)el número de kilogramos, entre (÷)el número de kilogramos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"IMC= Número de kilogramos, entre (÷)tu estatura, por (x)tu estatura",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"IMC= Tu estatura, entre (÷)el número de kilogramos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"IMC= El número de kilogramos entre (÷)tu estatura",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta que corresponde. Si quisieras representar la fórmula para calcular el Índice de Masa Corporal (IMC) la respuesta correcta sería:"
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tomar alcohol y manejar sin tomar.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Utilización de algún método anticonceptivo si no se quiere embarazarse.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Probar una droga que me han regalado para saber qué se siente.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Comenzar a fumar.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Preguntar e informarme sobre las consecuencias del consumo de alcohol.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Rechazar una invitación a una fiesta en donde no conozco a nadie y no he pedido permiso a mis padres.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No prever las consecuencias al desobedecer a mis padres.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Platicar con mis padres o maestros sobre una decisión importante que debo tomar.</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Las posibles respuestas son: situación de riesgo y medida de prevención.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Situación de riesgo",
            "Medida de prevención"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "5",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "2",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "1",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "4",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "3",
                "t17correcta": "5 "
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ""
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Una vez pasadas las 40 semanas de embarazo o gestación, inicia el proceso de nacimiento o parto que dura aproximadamente 12 horas. El trabajo de parto comienza con las contracciones del útero que desplazarán al feto en sentido descendente hacia la vagina. Con cada contracción el feto es empujado por los músculos del útero y el abdomen para que pueda salir. Una vez que ha nacido el feto, la placenta también es expulsada por contracciones del útero.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Una vez implantado el embrión en el útero, se comenzará a desarrollar la placenta y el cordón umbilical por medio de los cuales la madre alimentará a su hijo y recogerá los desechos de éste a través de la sangre. En el embrión implantado se comenzarán a formar tejidos y órganos.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Aproximadamente 24 horas después que se ha formado el cigoto, se comienza a formar el embrión, el cual se implantará en el endometrio del útero en el séptimo día del desarrollo.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Durante las siguientes 32 semanas el feto crecerá y se desarrollarán diversos órganos, por ejemplo para la semana 12 será posible conocer el sexo del feto, mientras que para la semana 16 el corazón del feto puede ser escuchado con un estetoscopio, para las últimas semanas del embarazo el feto crece rápidamente alcanzando un peso promedio de 3 kilos y una talla promedio de 56 centímetros.<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Para la octava semana del embarazo todos los órganos del nuevo ser están formados pero aún no están desarrollados, a partir de este momento el embrión comienza a denominarse feto.<br/>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Ordena los procesos en que se lleva a cabo el proceso de fecundación y embarazo."
        }
    },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"Se adqieren responsabilidades compartidas al tener relaciones sexuales, ya que puede existir el riesgo de enfermedades de transmisión sexual o un embarazo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Un embarazo a temprana edad tiene grandes beneficios y ningún riesgo para la salud de la mujer.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La decisión de tener un hijo debe ser compartida y trae consigo muchas responsabilidades que no sólo acaban cuando nace el bebé.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tener relaciones sexuales no sólo debe basarse en la atracción física por otra persona.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"El vínculo que se desarrolla a partir de tener relaciones sexuales no necesariamente conlleva responsabilidad y comunicación, puede ser algo pasajero.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las ideas que te ayuden a valorar los vínculos afectivos y la responsabilidad que conlleva un embarazo y el nacimiento de un nuevo ser. "
      }
   }
]