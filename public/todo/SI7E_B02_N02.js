json=[
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Con lo que intuyes sobre el tema es suficiente para su elaboración.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El tema debe ser específico y riguroso.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Debe relatar hechos fantásticos con personajes de tu creación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>En su desarrollar debe contener aspectos relevantes sobre el tema.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es posible que cuente con varias imprecisiones.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La redacción debe estar hecha con un lenguaje claro.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Entre menos se comprenda es mejor.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Debe ser objetiva, es decir, el investigador no pude emitir opiniones sobre el tema.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es adecuado para que el investigador sea subjetivo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La investigación debe sustentarse en fuentes bibliográficas confiables.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>nexos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>orden<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>causa<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>aunque<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>además<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>después<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>entonces<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>incluso<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>adición<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ejemplo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>palabras<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>finalmente<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>conectoras<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ilustración<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>oposición<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida":16,
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una investigación objetiva toma como fuentes investigaciones previas para sustentar sus conceptos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una investigación objetiva no toma en cuenta investigaciones previas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Toda cita, paráfrasis o resumen de un texto consultado debe citarse.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con citar algunas fuentes de consulta es suficiente para una investigación seria.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para citar una referencia usando las notas a pie de página se escribe en el texto un número consecutivo.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Anota Falso (F) o Verdadero(V)",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Vida artificial<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hadas y Gnomos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mundos paralelos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Selvas y su conservación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Viajes en el tiempo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Recetas para diabéticos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Clonación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Instalación de mamparas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Manipulación genética<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cultivo de hierbas medicinales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Extraterrestres<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Mary Shelley<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ciencia ficción<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Julio Verne<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Herbert Wells<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Isaac Asimov<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Su obra Frankenstein (1818) es considerada el primer relato de ciencia ficción."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Género narrativo cuya lectura permite imaginar situaciones basadas en avances de ciencia y tecnología; entre sus personajes puedes estar; extraterrestres, máquinas o personas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Su primer libro de ciencia ficción fue la máquina del tiempo(1895). Se hizo famoso cuando su obra la guerra de los mundos(1898) causó histeria entre la población con su transmisión radiofónica."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Escritor estadounidense con más de quinientos títulos publicados donde a través de la divulgación científica crea obras de ciencia ficción entre ellas Yo, Robot(1950)."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Escritor Francés, Anticipó inventos que hoy son una realidad, entre sus obras están Viaje al centro de la Tierra(1864). De la Tierra a la Luna(1865) y 20,000 leguas de viaje submarino(1869-1870)."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 1,
                "datoY" : 3
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 8
            },
            {
                "direccion" : 1,
                "datoX" : 1,
                "datoY" : 8
            },
            {
                "direccion" : 0,
                "datoX" : 4,
                "datoY" : 11
            },
            {
                "direccion" : 1,
                "datoX" : 15,
                "datoY" : 5
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto":15,
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Al día le sigue la noche<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hoy tomaré agua de limón<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Todos tenemos padres.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>De momento, no es oportuno.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Dos más dos son cuatro.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Frida Kahlo fue una gran artista.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La tierra gira sobre su propio eje<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Las ballenas son mamíferos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Las estrellas reflejan la luz del sol.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Útil para elaborar monografías.<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p>Deben citarse las fuentes consultadas.<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p>Recopila las ideas principales de un texto sin modificar  ni propósito ni intención del autor estudiado.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Útil para elaborar monografías.<\/p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p>Deben citarse las fuentes consultadas.<\/p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p>Presenta las ideas principales de un texto  sin modificar la intensión o propósito del autor pero en nuestras propias palabras.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "",
            "tipo": "horizontal",
            "respuestaDuplicada": true,
        },
        "contenedores":[ 
            "Resumen",
            "Paráfrasis"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Narrador testigo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Narrador versátil<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Narrador omnisciente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Narrador inverosímil<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Narrador protagonista<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Plantear el tema a discutir con nuestra opinión al respecto.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tener la plena seguridad de lo que se dice y no estar dispuesto a modificar nuestra opinión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Desarrollar con hechos verídicos el punto de vista que se tiene.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Demostrar que nuestra subjetividad es correcta<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>De ser posible basarse en cifras, estadísticas y situaciones equivalentes que hayan ocurrido con antes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Modificar nuestro discurso continuamente, confiando en la intuición.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Estructura de nota<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cuerpo de la noticia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hecho<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Opinión<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>titular<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>entrada<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>noticia<\/p>",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se desarrolla desde lo más general a lo más específico. Se desarrolla desde lo más general a lo más específico."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Desarrolla la noticia en orden decreciente de importancia."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Describe los sucesos exactamente como ocurrieron. Presentan datos objetivos y comprobables que se sustentan de manera objetiva sin emitir ninguna opinión al respecto."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Presentan los comentarios de diversas personas que expresan su postura ante los hechos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Debe ser breve y resaltar lo más importante para captar el interés del lector. En ocasiones se acompaña de subtítulos que anticipan el contenido de la noticia."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Su primer párrafo debe responder las preguntas ¿qué?, ¿cuándo?, ¿por qué?, ¿cómo?, ¿dónde? y ¿quién?"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Texto informativo que presenta información reciente y de interés de un grupo social determinado."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 11,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 1
            },
            {
                "direccion" : 1,
                "datoX" : 3,
                "datoY" : 3
            },
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 6
            },
            {
                "direccion" : 0,
                "datoX" : 8,
                "datoY" : 17
            },
            {
                "direccion" : 1,
                "datoX" : 15,
                "datoY" : 8
            },
            {
                "direccion" : 0,
                "datoX" : 17,
                "datoY" : 6
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto":18,
            "t11pregunta": "Completa el crucigrama y al final nómbralo"
        }
    }
]


