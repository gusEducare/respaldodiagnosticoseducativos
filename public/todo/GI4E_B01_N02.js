json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "0",
                "coordenadas": "94,223"
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "0",
                "coordenadas": "188,223"

            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "1",
                "coordenadas": "250,220"

            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "0",
                "coordenadas": "347,350"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona en el mapa el territorio que corresponde a México.",
            "secuencia": true,
            "ordenados": false,
            "url": "gi4a_b01_n02_01_01.png",
            "borde": true

        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_02_03.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_06.png",
                "t17correcta": "1",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_02.png",
                "t17correcta": "2",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_05.png",
                "t17correcta": "3",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra al siguiente mapa las islas y penínsulas.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_02_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "borde": false,
            "tamanyoReal": true

        },
        "contenedores": [
            {"Contenedor": ["", "48,12", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "346,219", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "259,532", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "331,549", "cuadrado", "90, 30", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Guatemala<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Brasil<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Estados Unidos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Canadá<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Belice<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "¿Qué países son vecinos de México?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n02_03_04.png",
                "t17correcta": "0",
                "etiqueta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n02_03_03.png",
                "t17correcta": "1",
                "etiqueta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n02_03_02.png",
                "t17correcta": "2",
                "etiqueta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el océano o golfos al lugar que les corresponde.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_03_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "tamanyoReal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "301,50", "", "132, 62", "", "transparent", "true"]},
            {"Contenedor": ["", "417,167", "", "132, 62", "", "transparent", "true"]},
            {"Contenedor": ["", "180,477", "", "132, 62", "", "transparent", "true"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E México se divide en 40 estados \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E México se divide en 31 estados y dos distritos federales \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E  México se divide en 32 estados \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E México se divide en 4 estados \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n02_04_03.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_02.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_10.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_11.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_09.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_14.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_05.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_07.png",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_06.png",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_08.png",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_12.png",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_13.png",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_16.png",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "gi4a_b01_n02_04_15.png",
                "t17correcta": "14"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Acomoda el Nombre del Estado al lugar que corresponda en el mapa de la república mexicana.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_04_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true,
        },
        "contenedores": [
            {"Contenedor": ["", "9,48", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "157,19", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "217,48", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "269,125", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "323,113", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "376,149", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "432,272", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "36,177", "cuadrado", "113, 40", ".", "transparent", true]},
            {"Contenedor": ["", "87,299", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "149,384", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "208,360", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "262,376", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "315,398", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "377,509", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "432,408", "cuadrado", "114, 40", ".", "transparent", true]},
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n02_05_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_05_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n02_05_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n02_05_06.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi4a_b01_n02_05_05.png",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena los estados de acuerdo a su tamaño. Coloca el de mayor tamaño hasta arriba y el de menor tamaño, hasta abajo.</p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_05_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
         "css":{
            "tamanoFondoColumnaContenedores":"contain"
        },
        "contenedores": [
            {"Contenedor": ["", "64,448", "cuadrado", "178, 42", ".", "transparent"]},
            {"Contenedor": ["", "107,448", "cuadrado", "178, 43", ".", "transparent"]},
            {"Contenedor": ["", "151,448", "cuadrado", "178, 44", ".", "transparent"]},
            {"Contenedor": ["", "196,448", "cuadrado", "178, 43", ".", "transparent"]},
            {"Contenedor": ["", "241,448", "cuadrado", "178, 42", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_06_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_06_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_06_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n03_06_05.png",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca los Estados de acuerdo a su posición en el mapa tomando en cuenta los puntos cardinales.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_06_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
         "css":{
            "tamanoFondoColumnaContenedores":"contain"
        },       
        "contenedores": [
            {"Contenedor": ["", "64,448", "cuadrado", "179, 42", ".", "transparent"]},
            {"Contenedor": ["", "107,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "152,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "196,448", "cuadrado", "179, 43", ".", "transparent"]}

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Jalisco<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Nuevo León<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Oaxaca<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Guadalajara"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Monterrey"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Oaxaca"
            }
        ],
        "pregunta": {
            "t11pregunta": "Une cada estado con su capital.",
            "c03id_tipo_pregunta": "18"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n02_07_02.png",
                "t17correcta": "0,2",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_06.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_07.png",
                "t17correcta": "2,0",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_05.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_03.png",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_04.png",
                "t17correcta": "5",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Identifica los lugares representativos de México que están señalados y coloca en el lugar que le corresponde.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_07_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true,
            "tamanyoReal": true

        },
        "contenedores": [
            {"Contenedor": ["", "60,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "125,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "191,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "256,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "323,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "388,387", "cuadrado", "202, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n02_08_02.png",
                "t17correcta": "0,2",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_08_03.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_08_04.png",
                "t17correcta": "2,0",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el componente que corresponda a la imagen.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_08_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true,
            "tamanyoReal": true

        },
        "contenedores": [
            {"Contenedor": ["", "49,336", "cuadrado", "240, 100", ".", "transparent"]},
            {"Contenedor": ["", "150,336", "cuadrado", "240, 100", ".", "transparent"]},
            {"Contenedor": ["", "253,336", "cuadrado", "240, 100", ".", "transparent"]}
        ]
    }
];