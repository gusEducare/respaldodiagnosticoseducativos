json = [
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Mundial<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Nacional<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Local<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div><img src=\"GI7E_B01_N03_01.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><img src=\"GI7E_B01_N03_02.png\" style='width:200px'\/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><img src=\"GI7E_B01_N03_03.png\" style='width:200px'\/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Relaciona cada imagen correspondiente a escalas.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El país Ecuador toma su nombre de la línea ecuatorial de la Tierra que significa zona de calor.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El meridiano de Greenwich, toma su nombre a partir del nombre de su descubridor en Londres.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Podemos encontrar la mitad del mundo en la ciudad de Quito.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La diferencia horaria entre el lugar más avanzado cronológicamente (el primer en entrar en cada nuevo día) y el lugar más rezagado es de 26 horas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En total son 39 zonas horarias oficiales diferentes.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {//3
        "respuestas": [
            {
                "t13respuesta": "<p>altimetro<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>latitud<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>altitud<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>polos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>longitud<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Instrumento para medir la altitud de un lugar."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "es la distancia angular (norte o sur) que tiene un lugar respecto al ecuador (0°) hacia los polos de (90°)"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "es una distancia de un punto terrestre medida desde el nivel del mar (0 metros)"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Puntos extremos de la tierra y en ellos se unen todos los meridianos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "se refiere a la distancia (este u oeste) que tiene un lugar respecto al meridiano de Greenwich (0°) y el antimeridiano (180°)"
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 5
            },
            {
                "direccion" : 0,
                "datoX" : 4,
                "datoY" : 7
            },
            {
                "direccion" : 1,
                "datoX" : 10,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 8,
                "datoY" : 2
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 11
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 13,
            "t11pregunta": "Realiza el siguiente crucigrama."
        }
    },    
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Proyección cónica<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Proyección acimutal<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Proyección de Mercator<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Proyección cilíndrica<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Proyección de Peters<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Proyección de Robinson <\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br> <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Sirven para representar un hemisferio, \n\
                colocando un cono en uno de los hemisferios de la Tierra. \n\
                Los paralelos se dibujan como arcos concéntricos y los meridianos \n\
                son radios que se originan en el polo.<br> <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Su característica más representativa es que los\n\
                 meridianos y paralelos son líneas rectas que se cortan de manera perpendicular.\n\
                 Conserva las formas de las masas de tierra pero no las distancias.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Esta proyección se logra si se proyecta el globo terráqueo sobre una superficie plana haciendo que ambos se toquen en un punto. Los paralelos son círculos concéntricos y los meridianos con líneas rectas que convergen en los polos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "respuestasLargas":true,
            "textosLargos": "si",
            "t11pregunta": "Arrastra la palabra que corresponde en cada caso."
        }
    },   
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Toma de desiciones a partir de información rápida y precisa.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Construcción de edificaciones tomando en cuenta el impacto económico.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Beneficio en industrias como la pesquera.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ayuda para llegar a lugares que no conocemos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Verificación de la calidad del aire en las ciudades.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Conocimiento de número de habitantes de una población.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Información para poder reforestar algún territorio.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Identificación de zonas para la extracción de petróleo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Toma de decisiones para la evaluación en el impacto de un territorio, relacionado con las ectividades que realiza el ser humano.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las oraciones que hablen de las ventajas al contar con sistemas de información obtenida de los satélites en el área de la Geografía."
        }
    }];


