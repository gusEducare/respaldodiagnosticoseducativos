json=[
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E4 m/s\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E2 m/s\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E40 m/s\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E20 m/s\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Si un autobús se mueve en una línea ascendente de 80 metros de longitud en un tiempo de 40 segundos su velocidad será:"
      }
   },  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E10 m/s\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1 m/s\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E20 m/s\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E2 m/s\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Si un automóvil se mueve en una línea ascendente de 60 metros de longitud en un tiempo de 30 segundos su velocidad será:"
      }
   },  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez media.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez instantánea.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez constante.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa velocidad más el tiempo.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"La rapidez que no varía a lo largo de un trayecto y tiempo determinado es:"
      }
   },  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez media.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez instantánea.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez constante.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa velocidad más el tiempo.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es el promedio de todos los valores de rapidez que se realizaron a lo largo de un trayecto:"
      }
   },  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez media.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez constante.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez del trayecto.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa rapidez instantánea.\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la magnitud que adquiere un objeto en un lapso de tiempo muy pequeño."
      }
 },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EMovimiento\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEspacio\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETiempo\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETrayectoria\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la distancia que existe entre el punto inicial y el punto final."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EMovimiento\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEspacio\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETiempo\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETrayectoria\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es el periodo de tiempo que tardar el objeto en recorrer la distancia entre el punto inicial y el punto final."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EMovimiento\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEspacio\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETiempo\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETrayectoria\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Fenómeno físico que implica el cambio de posición de un objeto en relación a un punto inicial o al punto de referencia durante un espacio de tiempo."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EMovimiento\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEspacio\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETiempo\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETrayectoria\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la línea que describe el objeto en movimiento en el transcurso del tiempo en el que ocurre."
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>sonido<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>vibraciones<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>onda<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>crestas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>longitud<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>amplitud<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>frecuencia<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>El <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;es el conjunto de vibraciones que se propagan en un medio elástico. Las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;son movimientos rápidos (de vaivén) en forma de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que se propagan por el aire y se producen cuando se mueve un objeto o cuando hacemos chocar o golpear dos objetos o superficies. Una onda está formada por <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y valles.<br/>Las ondas sonoras tienen una longitud, una amplitud y una frecuencia. La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de la onda es la distancia entre crestas. La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;es el tamaño en largo que alcanza la onda. La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;se refiere al número de ondas producidas en una unidad de tiempo. El sonido varía de acuerdo con la longitud, la amplitud y la frecuencia de las ondas sonoras.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EDuración\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EIntensidad\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETimbre\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETono\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la cualidad que permite asignar un sonido a una escala musical, lo que permite distinguir sonidos graves de los agudos."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EDuración\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EIntensidad\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETimbre\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETono\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es el tiempo en el cual se mantiene un sonido. Tiene que ver con el tiempo en el que se mantiene la ejecución de una vibración."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EDuración\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EIntensidad\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETimbre\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETono\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la cualidad que nos permite distinguir sonidos de diferentes fuentes, por ejemplo, si es una voz, un piano, o la música de la radio."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EDuración\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EIntensidad\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETimbre\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETono\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la propiedad que permite diferenciar un sonido fuerte de uno débil. Es decir, qué tan fuerte es el sonido percibido y tiene que ver con la fuerza con la que se produjeron las vibraciones y el medio en el que estas se desplazan. Se mide en decibeles."
      }
   },
    {
       "respuestas": [
            {
                "t13respuesta": "<p>Número de decibeles máximos tolerables para el oído humano.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Número de decibeles que puede producir el sonido de una sirena de ambulancia o patrulla.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Número de decibeles que produce un avión al despegar.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Número de decibeles que puede producir el sonido de una moto o los reproductores de música.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Número de decibeles que puede producir una charla normal.<\/p>",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["70 db", "201,0", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["90 db", "201,100", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["130 db", "201,200", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["110 db", "201,300", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["60 db", "201,400", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>átomos<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>núcleo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>orbitales<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>protones<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>electrón<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>positiva<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>negativa<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>eléctrica<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;son entidades microscópicas, invisibles al ojo humano, que están formados de tres partículas más pequeñas, llamadas partículas subatómicas. La estructura de un átomo se conforma de un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de energía.  El núcleo está formado por dos de las tres partículas subatómicas, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y neutrones. Alrededor del núcleo están los orbitales de energía y en ellos se localiza la tercer partícula subatómica llamada <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Los protones tienen carga eléctrica <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, los neutrones no poseen carga y los electrones tienen carga eléctrica <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La existencia de estas partículas subatómicas es lo que hace que la materia posea energía o que sea<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Corriente alterna</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Corriente continua</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Corriente eléctrica en la que los electrones se mueven un mismo sentido y en la misma cantidad. Esta condición hace que la polaridad no cambie y que la corriente tenga una amplitud constante."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Corriente alterna</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Corriente continua</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"<p><img></p>"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Corriente alterna</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Corriente continua</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Corriente eléctrica que se caracteriza porque circula por un tiempo en un sentido y luego se invierte al sentido opuesto, repitiéndose un sin número de veces durante el tiempo que está encendido el interruptor. El cambiode dirección ocurre porque la polaridad se invierte periódicamente."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Corriente alterna</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Corriente continua</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"<p><img></p>"
      }
   },
  {
      "respuestas": [
          {
              "t13respuesta": "<p>Conductor</p>",
              "t17correcta": "0,1,3,5",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Conductor</p>",
              "t17correcta": "0,1,3,5",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Aislante</p>",
              "t17correcta": "2,4",
              "columna":"1"
          },
          {
              "t13respuesta": "<p>Conductor</p>",
              "t17correcta": "0,1,3,5",
              "columna":"1"
          },
          {
              "t13respuesta": "<p>Aislante</p>",
              "t17correcta": "2,4",
              "columna":"0"
         },
          {
              "t13respuesta": "<p>Conductor</p>",
              "t17correcta": "0,1,3,5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "bloques":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "201,0", "cuadrado", "90, 100", "."]},
          {"Contenedor": ["", "201,120", "cuadrado", "90,100", "."]},
          {"Contenedor": ["", "201,230", "cuadrado", "90, 100", "."]},
          {"Contenedor": ["", "401,0", "cuadrado", "90, 100", "."]},
          {"Contenedor": ["", "401,120", "cuadrado", "90, 100", "."]},
          {"Contenedor": ["", "401,230", "cuadrado", "90, 100", "."]}
      ]
  },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>La Vía Láctea, una galaxia de las millones que se encuentran en el Universo.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad especial de medida para las distancias entre los astros que conforman el Sistema Solar  y equivale a 150 millones de kilómetros.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Conjuntos de millones de estrellas, nubes de gas y polvo cósmico unidas gravitatoriamente, esto es, unidas por la atracción que ejerce la masa de un cuerpo sobre otro.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>La nube de Oort, de donde proceden los cometas.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad de medida de la distancia que tarda la luz en recorrer en un año.</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"La Unidad Astronómica  (UA) es:"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>La Vía Láctea, una galaxia de las millones que se encuentran en el Universo.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Unidad especial de medida para las distancias entre los astros que conforman el Sistema Solar  y equivale a 150 millones de kilómetros.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Conjuntos de millones de estrellas, nubes de gas y polvo cósmico unidas gravitatoriamente, esto es, unidas por la atracción que ejerce la masa de un cuerpo sobre otro.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>La nube de Oort, de donde proceden los cometas.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad de medida de la distancia que tarda la luz en recorrer en un año.</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El Sistema Solar está ubicado en:"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>La Vía Láctea, una galaxia de las millones que se encuentran en el Universo.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad especial de medida para las distancias entre los astros que conforman el Sistema Solar  y equivale a 150 millones de kilómetros.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Conjuntos de millones de estrellas, nubes de gas y polvo cósmico unidas gravitatoriamente, esto es, unidas por la atracción que ejerce la masa de un cuerpo sobre otro.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>La nube de Oort, de donde proceden los cometas.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad de medida de la distancia que tarda la luz en recorrer en un año.</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Año luz:"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>La Vía Láctea, una galaxia de las millones que se encuentran en el Universo.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad especial de medida para las distancias entre los astros que conforman el Sistema Solar  y equivale a 150 millones de kilómetros.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Conjuntos de millones de estrellas, nubes de gas y polvo cósmico unidas gravitatoriamente, esto es, unidas por la atracción que ejerce la masa de un cuerpo sobre otro.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>La nube de Oort, de donde proceden los cometas.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad de medida de la distancia que tarda la luz en recorrer en un año.</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Las galaxias son:"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>La Vía Láctea, una galaxia de las millones que se encuentran en el Universo.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Unidad especial de medida para las distancias entre los astros que conforman el Sistema Solar  y equivale a 150 millones de kilómetros.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Conjuntos de millones de estrellas, nubes de gas y polvo cósmico unidas gravitatoriamente, esto es, unidas por la atracción que ejerce la masa de un cuerpo sobre otro.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>La nube de Oort, de donde proceden los cometas.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Unidad de medida de la distancia que tarda la luz en recorrer en un año.</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"La zona más alejada del sistema solar es:"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Geocéntrico</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Heliocéntrico</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Modelo que  propone que el Sol es el centro del Universo y que los planetas, incluida la Tierra, y otros astros giran alrededor de él."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Geocéntrico</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Heliocéntrico</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Modelo propone que la Tierra es el centro del Universo y que los astros, como el Sol y los planetas, giran alrededor de ella. Este modelo fue propuesto por varias civilizaciones, sin embargo, el mundo occidental conoció este modelo por medio de Claudio Ptolomeo."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Geocéntrico</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Heliocéntrico</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Fue propuesto por Nicolás Copérnico en 1543 y logró explicar con mayor facilidad los movimientos de los astros en la bóveda celeste colocando al Sol en el centro y los planetas en el orden que actualmente los conocemos girando alrededor de  él."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Geocéntrico</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Heliocéntrico</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El orden de los astros que propone el modelo a partir de la Tierra es: Luna, Mercurio, Venus, Sol, Marte, Júpiter, Saturno y estrellas fijas. Este modelo explicó los fenómenos astronómicos desde el  siglo II hasta  el siglo XVI."
      }
   }
]