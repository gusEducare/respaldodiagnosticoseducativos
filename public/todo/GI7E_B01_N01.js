json = [
    {
        "respuestas": [
            {
                "t13respuesta": "GI7E_B01_N01_01_02.png",
                "t17correcta": "0,1",
                "columna": "0"
            },
            {
                "t13respuesta": "GI7E_B01_N01_01_02.png",
                "t17correcta": "1,0",
                "columna": "0"
            },
            {
                "t13respuesta": "GI7E_B01_N01_01_03.png",
                "t17correcta": "2,3",
                "columna": "1"
            },
            {
                "t13respuesta": "GI7E_B01_N01_01_03.png",
                "t17correcta": "3,2",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra las palabras que te ayuden a identificar qué tipo de lugares son los que se muestran en las siguientes imágenes.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "GI7E_B01_N01_01_01.png",
            "respuestaImagen": true,
            "opcionesTexto": true,
            "tamanyoReal": false,
            "bloques": true,
            "borde":false

        },
        "css":{
            "tamanoFondoColumnaContenedores":"contain"
        },
        "contenedores": [
            {"Contenedor": ["", "12,361", "cuadrado", "193, 127", ".", "transparent"]},
            {"Contenedor": ["", "152,361", "cuadrado", "193, 127", ".", "transparent"]},
            {"Contenedor": ["", "293,361", "cuadrado", "193, 127", ".", "transparent"]},
            {"Contenedor": ["", "434,361", "cuadrado", "193, 127", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Expresa mediante una fracción que indica cantidad de veces que ha sido transparentucida la superficie representada en el mapa",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Herramienta visual y según su escala, nos permite establecer una relación exacta entre la realidad y lo representado.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Número de veces que se ha transparentucido o aumentado una superficie en un mapa.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Es el objeto de estudio de la Geografía. Comprende toda la superficie que habitamos los seres humanos.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Línea dividida en segmentos, cada uno representa 1 centímetro y expresa los kilómetros de la superficie representada.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Ciencia que estudia los mapas.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Escala numérica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mapa"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Escala"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Espacio geográfico"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Escala geográfica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cartografía"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el término con su definición"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "GI7E_B01_N01_02_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "GI7E_B01_N01_02_06.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "GI7E_B01_N01_02_04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "GI7E_B01_N01_02_05.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "GI7E_B01_N01_02_03.png",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra las palabras del recuadro para completar el siguiente esquema.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "GI7E_B01_N01_02_01.png",
            "respuestaImagen": true,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "bloques": true,
            "borde": false

        },
        "contenedores": [
            {"Contenedor": ["", "74,121", "cuadrado", "118, 49", ".", "transparent"]},
            {"Contenedor": ["", "165,20", "cuadrado", "140, 70", ".", "transparent"]},
            {"Contenedor": ["", "325,69", "cuadrado", "118, 49", ".", "transparent"]},
            {"Contenedor": ["", "50,440", "cuadrado", "140, 70", ".", "transparent"]},
            {"Contenedor": ["", "155,508", "cuadrado", "119, 50", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Los meridianos son ligeramente curvos. Los polos se estiran convirtiéndose en una línea. Representa a toda la superficie terrestre y simula la forma de la Tierra. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Dicha proyección conserva con exactitud las dimensiones de todos los territorios, pero no conserva la forma y distorsiona la región ecuatorial. Sirve para comparar el tamaño de dos países o territorios",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Utiliza el cilindro como figura de proyección. El ecuador es el punto tangente, los meridianos son rectas paralelas equidistantes y los paralelos están representados por rectas perpendiculares.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Proyección de Robinson"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Proyección de Peters"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Proyección cilíndrica"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el término con su definición"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>satélite<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>sensor<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>artificial<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>relieve<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>altitud<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La tecnología en el siglo XX ha beneficiado a la Geografía en el análisis del espacio geográfico. Las imágenes de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; constituyen una representación visual de imágenes de la superficie terrestre; éstas se capturan mediante un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;  ubicado en un satélite<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, fuera de la atmósfera. Los datos recopilados son enviados a una estación terrena en donde se procesan y se convierten en imagen con mucha rapidez, casi en tiempo real. Estas imágenes permiten observar el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, distinguir tipos de vegetaciones, conocer la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, del terreno, entre muchos otros datos de interés.<\/p>"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "ocultaPuntoFinal": true,
            "pintaUltimaCaja": false,
            "t11pregunta": "Arrastra las palabras del recuadro para completar la siguiente información."
        }
    }
];



