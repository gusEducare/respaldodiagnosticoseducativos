json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las revistas temáticas incluyen, además de la portada, página legal, índice de contenido señalando los nombres de los artículos y autores.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las revistas temáticas contienen artículos de diversos temas y usan un lenguaje coloquial para llegar a sus lectores.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las revistas temáticas tienen una identidad gráfica que las diferencia de otras por ejemplo utilizan siempre la misma paleta de colores y tipografías específicas.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Anota falso (F) o verdadero (V) según corresponda a cada aseveración:<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las revistas temáticas los artículos incluyen un lenguaje especializado, ya que van dirigidas a un público en particular que está familiarizado con el tema en cuestión.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las revisas temáticas son publicaciones periódicas",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las revistas temáticas tienen publicaciones esporádicas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las revistas temáticas presentan artículos especializados de un tema en partícular",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Anota falso (F) o verdadero (V) según corresponda a cada aseveración.<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p style='font-size:80%;'>El autor informa y explica determinado tema de manera objetiva, sin emitir juicios de opinión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p style='font-size:80%;'>El autor pretende probar una idea, refutar la de alguien más o persuadir al lector sobre determinado punto de vista.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Título<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Introducción<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Desarrollo<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Apoyos gráficos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Conclusión o cierre<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Introducción<\/p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Desarrollo<\/p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Premisas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Tesis<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Argumentos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:90%;'>Conclusión o cierre<\/p>",
                "t17correcta": "1,0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada contenedor lo que corresponda según el tipo de texto.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Textos expositivos",
            "Textos argumentativos"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Argumentos que apelan a los sentimientos</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Argumentos racionales</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Argumentos de ejemplificación</p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Argumentos de hecho</p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Utilizan experiencias compatidas como sociedad o el uso de emociones en común para lograr empatía con el lector.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Utilizan ideas y creencias aceptadas como verdaderas por la sociedad.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Establecen ejemplos o declaraciones que confirman lo que se quiere demostrar.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Apelan a hechos reales y comprobables para defender una idea.</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el tipo de argumento con su concepto:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Sin embargo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>pero<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Aun<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>El delegado explicó que “Los espacios públicos; jardines y centros de desarrollo comunitario, están siendo monitoreados por oficiales de policía de manera constante.&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, los vecinos aún no perciben un cambio real en su seguridad.<br>Lo pasamos genial en casa de tía Susana&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;el centro del corazón familiar es la casa de los abuelos.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;cuando la facultad de ingeniería de Tampico no está dentro de las 10 mejores del país, los alumnos han tenido éxito en varios concursos internacionales.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra y ordena los nexos y expresiones que den lógica a los textos.",
            "pintaUltimaCaja": false,
            "respuestasLargas": true,
            "ocultaPuntoFinal": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>entre otros<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>aunque<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>a pesar de<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los delitos de violencia de género se encuentran regulados y tipificados en la ley mexicana, código civil del estado de México, ley orgánica, código penal,&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;. <br>El juez intentó lograr un acuerdo de voluntades&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;una de las partes se resistió al mismo.<br>Día a día las mujeres sufren de violencia de género,&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;las acciones tomadas por gobiernos federal, estatal y municipal.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra y ordena los nexos y expresiones que den lógica a los textos.",
            "pintaUltimaCaja": false,
            "respuestasLargas": true,
            "ocultaPuntoFinal": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Se denominan así cuando nuestra lengua española incorpora palabras que son propias de otras lenguas. Muchas de ellas con el tiempo comienzan a formar parte del habla cotidiana.<\/p>",
                "t17correcta": "0,0"
            },
            {
                "t13respuesta": "<p>miss, surf, clip, Hobby, comfort, penalty, sandwich, almohada, gourmet, chat.<\/p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p>Palabras de origen indígena provenientes de lenguas nativas. Usualmente de centro américa.<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p>alpaca, jacal, cacao, cóndor, caucho, aguacate, chicle, chile, jitomate, caimán.<\/p>",
                "t17correcta": "1,1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Completa el cuadro comparativo arrastrando el concepto y ejemplo según corresponda"
        },
        "contenedores": [
            "Concepto",
            "Ejemplos"

        ],
        "contenedoresFilas": [
            "Extranjerismos",
            "Indigenismos"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>explicaciones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>repeticiones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>comentarios<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>ejemplos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>citas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>recursos<\/p>",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Exposición acerca de un tema o hecho con palabras comprensibles a la audiencia favorecen la claridad y entendimiento del tema."
            }, {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Te permiten enfatizar una idea. Consisten en decir algo que se había practicado o dicho anteriormente."
            }, {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Son críticas, juicios o expresiones acerca de algo que se ha dicho o hecho."
            }, {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Hechos o expresiones que se utilizan para ilustrar o comprobar algo que se ha dicho."
            }, {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Hacer referencia a un autor experto o texto para otorgar autoridad y justificar lo que se dice o explica."
            }, {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Los puedes emplear para desarrollar las ideas en los párrafos de los artículos que escribirás."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 8,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 7,
                "datoY": 12
            },
            {
                "direccion": 0,
                "datoX": 0,
                "datoY": 5
            },
            {
                "direccion": 1,
                "datoX": 17,
                "datoY": 12
            },
            {
                "direccion": 1,
                "datoX": 10,
                "datoY": 1
            },
            {
                "direccion": 1,
                "datoX": 13,
                "datoY": 10
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 19,
            "ancho": 19,
            "t11pregunta": "Completa el crucigrama:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las referencias bibliográficas deben incluir toda la información de las fuentes consultadas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es opcional utilizar referencias bibliográficas en una investigación seria.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sirven para informarle al lector dónde consultar con mayor profundidad la información presentada en el texto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las fichas bibliográficas no tienen una estructura determinada. Puedes anotar lo que recuerdas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sin excepción; las fichas bibliograficas deben contener : Apellido e inicial del nombre del autor o autores, año de publicación, Título del libro, Ciudad de la publicación y nombre de la editorial.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Indica Verdadero o Falso de acuerdo con las características y funciones de las referencias bibliográficas:<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Derechos Individuales",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Derechos de Conciencia",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Derechos Ciudadanos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Derechos culturales, sociales y económicos.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Derechos inherentes y básicos del ser humano en los que se descatan el derecho a la vida, libertad, igualdad y seguridad.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Garantizan tu libertad de credo, pensamiento y de expresión.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Esta clase de derechos pueden diferir entre pasises. Ejemplos son: Derecho de elegir gobernantes y de postularse a un cargo público, particupar en movimientos políticos e ideológicos sin ser perseguido por tus ideas politicas.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Entre ellos se encuentran: derecho a trabajo digno, a la comunicación en diversas lenguas, acceso a la educación, a tener un propiedades, a servicios médicos, entre otros.</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Une los tipos de derechos con su descripción:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>deber<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>letras<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>viñetas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>gráficos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>imperativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>indicativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>números<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>recursos<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Localiza en la sopa de letras todas las palabras que indiquen lo relacionado con la redacción de documentos que establecen derechos y obligaciones"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>poder<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>modos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>conjugar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>arábigos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>romanos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tecnicimos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>subjuntivo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>infinitivo<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Localiza en la sopa de letras todas las palabras que indiquen lo relacionado con la redacción de documentos que establecen derechos y obligaciones"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EOpción\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EPetición\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDerecho\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EObligacion\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EPrerrogativa\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Identifica la palabra que conceptualiza el texto.<br><br>Facultad de exigir y de hacer todo aquello que esté en su favor. Usualmente se vincula con una obligación tratandose de nuestra relación hacia otra persona. Entre sus fines esta dotar los miembros de la sociedad de garantías de seguridad, certeza, igualdad, libertad y justicia."
        }
    }
]