json = [
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Respetar a toda forma de vida<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cuidar en mi casa a los animales en peligro de extinción<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Evito contaminar.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Adopto a los perros y gatos callejeros.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Utilizo los recursos naturales con responsabilidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Me hago responsable de la atención y cuidado de mi mascota.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evito la caza clandestina<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Mantengo limpia mi habitación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cuido mi entorno.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No compro animales en peligro de extinción.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todas las acciones que puedes realizar para cuidar a los seres vivos."
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Boro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Metano<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Plomo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Litio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Monóxido de nitrógeno<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dióxido de azufre<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cloroflurocarbonos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hollín<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cloro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Dióxido de carbono<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Monóxido de carbono<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ozono<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Radio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cadmio<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Arrastra al contenedor las sustancias que provocan mayor contaminación del aire."
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>Aparecen los primeros mamíferos primitivos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aparecen muchas de las plantas actuales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se le conoce como la era de los dinosaurios.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al final de esta era se extinguen los dinosaurios.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La Tierra comienza dividirse en continentes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Inicia la aparición de muchos mamíferos actuales.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Se le considera la era de los mamíferos.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Aparece el hombre.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Se extinguen grandes mamífeors como el mamut o tigre dientes de sable.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Surgen las primeras civilizaciones humanas, la agricultura, ganadería y la cacería.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Se formaron las primeras montañas del planeta.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aparecen los primeros seres vivos unicelulares, como los caracoles.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aparecen las primeras plantas vasculares.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al final de esta era se forma el supercontinente llamado Pangea.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se entinguen los trilobites, y los primeros reptiles y peces.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Paleozoica",
            "Mesozoica",
            "Cenozoica"
        ]
    },
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre mayor es nuestro consumo de bienes y servicios, se tiene un mayor impacto negativo sobre el medio ambiente.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La huella ecológica es una medida de la cantidad de superficie de tierra que cada persona necesita para obtener todos los recursos que consume diariamente así como el espacio requerido para disponer los residuos que genera.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los seres vivos que habitamos la Tierra mantenemos una interdependencia entre nosotros y con también con nuestro ambiente.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Separar la basura que generamos a diario no tiene impacto en la calidad de vida ni en el ambiente.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al separar los residuos, reutilizarlos y reciclar contribuimos con un estilo de vida sustentable.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un estilo de vida consumista es lo mismo que un estilo de vida sustentable.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los residuos orgánicos se definen como los desechos que están hechos a partir de compuestos de plantas o animales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se consideran residuos orgánicos vidrio, metal, plástico y cartón, entre otros.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
  {
      "respuestas": [
          {
              "t13respuesta": "<p>Ambiente</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Elementos sociales</p>",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Elementos naturales</p>",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "<p>Componentes Económicos</p>",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "<p>Compenentes Culturales</p>",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Componentes Políticos</p>",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Factores Bióticos</p>",
              "t17correcta": "6",
              "columna":"1"
          },
          {
              "t13respuesta": "<p>Factores Abióticos</p>",
              "t17correcta": "7",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Completa el esquema arrastrando a cada sitio la palabra que corresponda.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "tamanyoReal": true
          
      },
      "contenedores": [
          {"Contenedor": ["", "10,200", "cuadrado", "150, 90", "."]},
          {"Contenedor": ["", "150,100", "cuadrado", "150, 90", "."]},
          {"Contenedor": ["", "150,300", "cuadrado", "150, 90", "."]},
          {"Contenedor": ["", "250,120", "cuadrado", "150, 90", "."]},
          {"Contenedor": ["", "250,320", "cuadrado", "150, 90", "."]},
          {"Contenedor": ["", "350,120", "cuadrado", "150, 90", "."]},
          {"Contenedor": ["", "350,320", "cuadrado", "150, 90", "."]},
          {"Contenedor": ["", "450,120", "cuadrado", "150, 90", "."]}
      ]
  }
]

