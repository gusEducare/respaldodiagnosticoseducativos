[
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Violación al derecho de contar con casa y alimento"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Derecho a no ser discriminado"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Violación al derecho a la educación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Violación al derecho a la libertad de expresión"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Todos los seres humanos nacen libres e iguales en dignidad y derechos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Toda persona tiene todos los derechos y libertades, sin distinción alguna de raza, color, sexo, idioma, religión, opinión política o de cualquier otra índole, origen nacional o social, posición económica, nacimiento o cualquier otra condición.", 
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Todo individuo tiene derecho a la vida, a la libertad y a la seguridad de su persona.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Nadie estará sometido a esclavitud ni a servidumbre.",
            "t17correcta":"1"
         },
         {  
             "t13respuesta":"Nadie será sometido a torturas ni a penas o tratos crueles, inhumanos o degradantes.",
            "t17correcta":"1"
         },
         {  
             "t13respuesta":"Todo ser humano tiene derecho, en todas partes, al reconocimiento de su personalidad jurídica.",
            "t17correcta":"0"
         },         
         {  
            "t13respuesta":"Todos son iguales ante la ley y tienen, sin distinción, derecho a igual protección de la ley.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Toda persona tiene derecho a circular libremente y a elegir su residencia en el territorio de un Estado.", 
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Toda persona tiene derecho a la propiedad, individual y colectivamente.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Toda persona tiene derecho a la libertad de pensamiento, de conciencia y de religión.",
            "t17correcta":"0"
         },
         {  
             "t13respuesta":"Todo individuo tiene derecho a la libertad de opinión y de expresión; este derecho incluye el de no ser molestado a causa de sus opiniones.",
            "t17correcta":"1"
         },
         {  
             "t13respuesta":"Toda persona tiene derecho a la libertad de reunión y de asociación pacíficas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Toda persona tiene derecho al trabajo, a la libre elección de su trabajo, a condiciones equitativas y satisfactorias de trabajo y a la protección contra el desempleo.", 
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Toda persona tiene derecho al descanso, al disfrute del tiempo libre, a una limitación razonable de la duración del trabajo y a vacaciones periódicas pagadas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Toda persona tiene derecho a un nivel de vida adecuado que le asegure, así como a su familia, la salud y el bienestar, y en especial la alimentación, el vestido, la vivienda, la asistencia médica y los servicios sociales necesarios.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Lo que leerás son fragmentos de 15 derechos humanos de los 30 que forman parte de losderechos humanos universales. Identifica y señala aquellos que no son una realidad y aún debemos trabajar en México."
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "Mediador de ONU reclama respetar la tregua en Yemen",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Disputa por intestado del Sr. Domínguez",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Desaparición de los 43 normalistas de Ayotzinapa.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Wirikuta: las dos caras de un conflicto legal",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Internacional"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Familiar"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Nacional"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Comunitario"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Pequeño grupo de personas; que representa a grupos más grandes, y están facultados para tomar decisiones en común.<\/p>",
                "t17correcta": "0,0"
            },
            {
                "t13respuesta": "<p>Negociación con la dirección del colegio para realizar un baile en el gimnasio<\/p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p>Ejercicio democrático donde la mayoría expresa su opinión<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p>Elección del jefe de grupo<\/p>",
                "t17correcta": "1,1"
            },
            {
                "t13respuesta": "<p>Decisión tomada de conformidad entre las partes para solucionar alguna situación<\/p>",
                "t17correcta": "0,2"
            },
            {
                "t13respuesta": "<p>Redacción colectiva de la regla del aula<\/p>",
                "t17correcta": "1,2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Delta sesion 4 a1"
        },
        "contenedores":[ 
            "Descripción",
            "Ejemplo"
            
        ],
        "contenedoresFilas":[ 
            "Asamblea",
            "Votación",
            "Acuerdo"
            
            
        ]
    }
]
