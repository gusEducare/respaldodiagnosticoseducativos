json = [
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Presenta una opinión aunque no tenga sustento.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Presenta una idea que se defenderá mediante el uso de argumentos.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Presenta una idea en común por más de un participante.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Utilizan los científicos y estudiosos.</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Un texto argumentativo es el que"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Científicos</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Filosóficos</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Políticos</p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "La argumentación se presenta en textos"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Presentación de una tesis, sólo para comprobarla.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Que se encuentra acompañada de una idea presentada por un experto.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>La utilización de tecnicismos y lenguaje formal.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>No hay un orden en la presentación o estructura.</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Algunas característica de los textos argumentativos son"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Se da a conocer cuál es el origen del tema.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Se presenta la idea u opinión que sustenta el escrito.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Si se refuta o se comprueba la idea base del mismo.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Se muestran las conclusiones que se desarrollarán.</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Dentro de la introducción de un texto argumentativo"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Inicio</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Desarrollo</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Conclusión</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Contenido</p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Al presentar en el texto argumentativo si estamos a favor o en contra de lo planteado, hablamos del"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Se defienden las ideas planteadas.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Se rechazan las ideas planteadas. </p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Se utilizan datos, comentarios y opiniones de los expertos.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Se cierra el texto pero sin hablar de algún punto concluyente. </p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "En las conclusiones de un texto argumentativo"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Argumentos racionales",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Argumentos de ejemplificación",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "Argumentos que apelan a los sentimientos o experiencias compartidas",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "Argumentos de hechos",
				"t17correcta": "4"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Se basan en ideas y creencias aceptadas como verdaderas por la sociedad."
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Utilizan ejemplos o declaraciones que confirman lo que se quieres demostrar."
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "El uso de referencias a experiencias compartidas como sociedad o el uso de sentimientos en común logar una empatía con el lector y logra una posible aceptación del argumento."
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Utilizan hechos reales y comprobables para defender una idea. Estos incluyen citas, datos y resultados de investigaciones."
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "18",
			"t11pregunta": "s1a2"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Idea personal que se tiene sobre un asunto.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Son apreciaciones subjetivas que reflejan la forma en que un persona valora e interpreta algo.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Información o datos objetivos reales y comprobables.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Ejemplos de esto pueden ser las noticias, resultados de investigaciones.<\/p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Zeta sesion 9 a1",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Hechos",
			"Opiniones"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Establecer contacto directo con los asistentes: demostrarles que tanto ellos como nosotros tenemos cosas en común, Ya sea alguna situación que nos afecta o beneficia, un lufar de estudio, las mismas metas o ideales a seguir.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Mantener la atención: una vez que hemos logrado ininiar un establecimiento con nuestra propuesta, es importante mantener su interés para poder hacerles llegar nuestro mensaje. Esto se logra presentando información o datos que involucren o sean parte de los invitados.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Presentar con seguridad la idea principal y los argumentos que la avalan: es importante demostrar que estamos de acuerdo o en desacuerdo con alguna idea presentada en el panel con seguridad, apoyándonos con todos los puntos que tengamos para dar a entender por qué ellos deberían seguirnos.<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>concluir con una frase o énfasis: para terminar de convencer o persuadir a una audiencia, se puede aplicar la estrategia de culminar la presentación cion una frase contundente de algún experto en el área en el cual se vea reflejado lo que estamos defendiendo, apoyando o estableciendo.<\/p>",
				"t17correcta": "3"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
			"tipo": "ordenar"
		},
		"contenedores": [
			{ "Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."] },
			{ "Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."] }
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Verdadero",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Falso",
				"t17correcta": "1"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Un prólogo es necesariamente escrito por el autor para introducir cualquiera de sus obras.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "El prólogo se encuentra principalmente en las páginas previas de las obras literarias.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Un prólogo puede servirnos para presentar un síntesis sobre algún tema a resaltar dentro de una obra literaria.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Dentro del prólogo se habla de las conclusiones a las que el autor ha llegado.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "El prólogo puede ayudarnos a adelantar una reflexión, idea o conclusión de las ideas planteadas en la obra.",
				"correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "13",
			"t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
			"descripcion": "",
			"variante": "editable",
			"anchoColumnaPreguntas": 60,
			"evaluable": true
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Se seleccionan materiales distinguidos, según un criterio específico para crearlas y hacer las recopilacinoes necesarias.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Pueden ser de temas variados, pero que sean relevantes para el lector.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Es realizado por un experto en la temática y pertenecen a un género específico.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>El género al que deben pertener, es indistinto ya que se hablan de varias temáticas dentro de las antologías.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Se utilizan varios enfoques en los que se abordan una misma temática o movimiento literario.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Es información recolectada de varios artistas y de varios movimientos y temas en general.<\/p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Selecciona las características que nos hablan de las características y funciones de las antologías"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Nos ayudan a indicar el comienzo de un nuevo planteamiento, ya que finaliza una idea y comienza con otro en un parrafo diferente.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Nos ayuda a distinguir una pausa mucho mayor que la coma, pero no tan grande como el punto.",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "Ayuda a distiguir el fin de una idea y distinguir entre diferentes oraciones pero que éstan relacionadas.",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "Ayudan a indicar una pausa en el ritmo de la lectura y puede usarse también para hacer enumeraciones.",
				"t17correcta": "4"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Punto y aparte"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Punto y coma"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Punto y seguido"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Coma"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "18",
			"t11pregunta": "s1a2"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>objetivo<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>impersonales<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>verbos<\/p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>tiempos<\/p>",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "<p>modo<\/p>",
				"t17correcta": "5"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>Los formularios se caracterizan por tener un lenguaje <\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;y utilizar oraciones <\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;(aquellas que no tienen sujeto). Además, se utilizan <\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;en distintos modos, <\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;y voz para dar instrucciones y solicitar la información que se necesita.<br/>El <\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;de un verbo indica la actitud del hablante.<\/p>"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Completa el siguiente p&aacute;rrafo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Modo imperativo<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Modo infinitivo<\/p>",
				"t17correcta": "2,5"
			},
			{
				"t13respuesta": "<p>Modo indicativo<\/p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>Modo subjuntivo<\/p>",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "<p>Modo infinitivo</p>",
				"t17correcta": "5,2"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p><br/><br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Sólo puede conjugarse en tiempo presente.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;No se conjuga, no tiene persona gramatical ni número.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Utiliza la forma en la que hablamos cotidianamente con los verbos en presente, pretérito y futuro.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Usualmente se utilizan en los formularios donde denotan la relación semántica entre el sujeto, el verbo y el objeto para expresar sus propósitos.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Se utiliza para expresar una acción general como: leer.<\/p>"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Completa el siguiente p&aacute;rrafo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Verdadero",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Falso",
				"t17correcta": "1"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Dentro de los formularios puede utilizarse el modo infinitivo, ya que el verbo no se conjuga, no tienen persona gramatical ni número.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Cuando se presente una duda sobre el modo imperativo, sólo basta con buscarlo en internet.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Dentro del modo subjuntivo, se puede usar la forma activa, como pasiva.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "La voz activa indica que la acción es realizada por el sujeto que es consciente de dicha acción.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "No se puede conjugar la segunda persona del singular, cuando hablamos en modo imperativo.",
				"correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "13",
			"t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
			"descripcion": "",
			"variante": "editable",
			"anchoColumnaPreguntas": 60,
			"evaluable": true
		}
	}
]
