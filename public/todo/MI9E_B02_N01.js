json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E r(r + r) \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E r(r + 1) \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E r(r<sup>2</sup> + 1) \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál es el resultado de factorizar la ecuación r<sup>2</sup> + r ?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb1_s02_a05_05.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb1_s02_a05_05.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='mat_pb1_s02_a05_05.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Elige la figura que se ha rotado:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Verdadero \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E Falso \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "3",
            "t11pregunta": "Para calcular la probabilidad de que al tirar un dado caiga un número par o un múltiplo de cinco el procedimiento correcto es: Calcular la probabilidad de que caiga un número par, calcular la probabilidad de que caiga un múltiplo de cinco y sumar ambas probabilidades.",
            "forma":"circulo",
            "texto":false,
            "icono":true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 3 cm. \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 6 cm. \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 5 cm. \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas":false,
            "t11pregunta": "La base de un triángulo rectángulo es de 3 cm. y su altura es de 4 cm.¿Cuánto mide su hipotenusa?<br><br><img src='mat_pb1_s02_a05_05.png'>"
        }
    }
];