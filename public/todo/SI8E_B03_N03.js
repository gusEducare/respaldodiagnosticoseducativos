json = [
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Metáfora<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Paralelismo<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Comparación<\/p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>Hipérbole<\/p>",
				"t17correcta": "4"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;“Su nombre es Dulcinea... sus cabellos son oro, su frente campos elíseos, sus cejas arcos de cielo, sus ojos soles, sus mejillas rosas, sus labios corales, perlas sus dientes, alabastro su cuello, mármol su pecho, marfil sus manos”...  Miguel de Cervantes, Don Quijote<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;“La tierra más verde de huertos la tierra más rubia de mies la tierra más roja de viñas”. Gabriela Mistral<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;“¡Oh, soledad sonora! Mi corazón sereno se abre, como un tesoro, al soplo de tu brisa” Juan Ramón Jiménez<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;“Por tu amor me duele el aire, el corazón y el sombrero” Federico García Lorca<\/p>"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Completa el siguiente p&aacute;rrafo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>participios<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>complemento circunstancial<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>pte indicativo habitual<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>pte indicativo histórico<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>adverbios temporales<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>expresiones sinónimas<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>tiempo copretérito<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>pronombres<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>tiempo pretérito<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>adjetivos<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>aposiciones<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>pte indicativo atemporal<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>nexos<\/p>",
				"t17correcta": "0"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Forma no personal del verbo que puede cumplir función de adjetivo. sus palabras termniana en ado, ido, to, so, cho."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Función sintáctica que expresa o informa acerca de alguna circuntancia en la que sucede una acción del verbo, ya sea lugar, tiempo, modo, cantidad, entre otros."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Refiere a acciones que se realizan en la cotidianidad, y así como pueden estar sucediendo en el presente, también pueden ocurrir en el pasado y en el futuro."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Indica una acción que sucedió en el pasado y se menciona en el presente."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Indican en qué momento sucede una acción."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Se utilizan para describir a un mismo objeto, hecho, persona, animal, entre otros ya que tienen significado parecidos."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Acción que sucedió en el pasado pero que no necesariamente ha culminado."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Palabras que representan o sustituyen a los nombres de personas, obejtos o hechos que fueron mencionados previamente en el texto."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Expresa que una acción ya sucedió y terminó."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Califican a los sustantivos y expresan sus cualidades."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Casos en que un sustantivo o frase sustantiva se añade a otro sustantivo o frase sustantiva para explicar o describir algo sobre el primero."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Refiere una acción que no está reflejada en un tiempo determinado."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Se utilizan para crear enlaces entre palabras u oraciones."
			}
		],
		"pocisiones": [
			{
				"direccion": 1,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 8,
				"datoY": 5
			},
			{
				"direccion": 1,
				"datoX": 6,
				"datoY": 3
			},
			{
				"direccion": 0,
				"datoX": 2,
				"datoY": 3
			},
			{
				"direccion": 1,
				"datoX": 10,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "15",
			"t11pregunta": "Delta sesion 12 a3"
		}
	},




	{
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tiempo verbal presente de indicativo expresa simultaneidad entre la acción del verbo y el momento en el que se menciona o se lee.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tiempo verbal presente indicativo únicamente se usa en relatos históricos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tiempo verbal presente indicativo expresa que  la acción está sucediendo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El presente indicativo debe ir siempre acompañado de adverbios temporales.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El  presente indicativo puede emplearse para referirse a acciones que ocurren frecuentemente.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tiempo verbal presente indicativo se puede usar para expresar órdenes.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
	{
		"respuestas": [
			{
				"t13respuesta": "\u003Cp\u003EMalala Yousafzai\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003E12 de julio de 1997 en Pakistán\u003C\/p\u003E",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "\u003Cp\u003EHija de Ziauddin Yousafzai, tiene dos hermanos.\u003C\/p\u003E",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "\u003Cp\u003ECuando cuenta 13 años, se hizo célebre gracias a un blog que escribía bajo el pseudónimo Gul Makai para la BBC en el que narraba su vida bajo el régimen del Tehrik e Taliban Pakistan.\u003C\/p\u003E",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "\u003Cp\u003EActivista a favor de los derechos civiles, especialmente de los derechos de la mujer.\u003C\/p\u003E",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "\u003Cp\u003E13\u003C\/p\u003E",
				"t17correcta": "5"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Ingresos",
			"tipo": "vertical"
		},
		"contenedores": [
			"¿Nombre del personaje?",
			"¿Cuándo y dónde nació?",
			"¿Qué se expresa de su familia?",
			"¿De qué manera comienza a involucrase en la vida política de su país?",
			"¿Por qué es un personaje relevante?",
			"¿Cuántos premios ha recibido?"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>metáfora<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>hipérbole<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>carga emotiva<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Michoacán<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>paralelismo<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>ironía<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>ensayo<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>persuación<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>comparación<\/p>",
				"t17correcta": "0"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Comparación que consiste en la identificación de un término real con otro imaginario entre los que existe una relación de semejanza."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Exageracion de cualidades o características de personas u objetos."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Se utiliza para obtener empatía, apela al uso de sentimento sin presentar datos e información objetiva."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Relacionar partes del texto similares pero que no comparan una con otra sino que tienen estrucuturas o incluso palabras iguales o sinónimas."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Burla que se usa para dar a entender lo contrario a lo que se dijo."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Texto argumentativo narrado en primera persona, plantean comentarios y puntos de vista personales sobre determinado tema."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Tipo de diversidad que se refiere a las costumbres y tradiciones que un grupo de personas comparten."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Recurso de argumentación son el que se pretende convencer sobre un punto, idea u opinión a otros."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Establecer semejanzas y diferencias relacionando una cosa con otra."
			}
		],
		"pocisiones": [
			{
				"direccion": 1,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 8,
				"datoY": 5
			},
			{
				"direccion": 1,
				"datoX": 6,
				"datoY": 3
			},
			{
				"direccion": 0,
				"datoX": 2,
				"datoY": 3
			},
			{
				"direccion": 1,
				"datoX": 10,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			},
			{
				"direccion": 0,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 1,
				"datoY": 7
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "15",
			"t11pregunta": "Delta sesion 12 a3"
		}
	}
]