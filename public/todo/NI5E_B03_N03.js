json=[
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EFiltración\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETamizado\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDecantación\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es un método de separación de mezclas en el cual se hace pasar dicha mezcla por un tamiz, es decir, por una malla, colador o por un cernidor que separará los dos componentes."
      }
   },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EFiltración\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETamizado\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDecantación\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Separación de mezclas en las cuales las partículas sólidas no se disuelven en el líquido; si son dos líquidos, se dejan en reposo y al aparecer una división entre ambos, uno puede extraerse."
      }
   },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EFiltración\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETamizado\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDecantación\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Cuando uno de los componentes de la mezcla no se hace soluble en el otro, los componentes pueden separarse haciendo pasar la mezcla en un filtro (que puede ser un papel, tela o algún material similar)."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E20 %\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E99 %\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E0.93 %\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El gas con mayor proporción es el nitrógeno, seguido del oxígeno, juntos forman el _______ de los gases de la atmósfera:"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E78 %\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E99 %\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E0.0035 %\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El porcentaje de nitrógeno en el aire es: "
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E99 %\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E20 %\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E0.93 %\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El porcentaje de oxígeno en el aire es: "
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E78 %\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E20 %\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E0.93 %\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El porcentaje de Argón en el aire es: "
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E0.0035 %\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E20 %\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E0.93 %\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El porcentaje de dióxido de carbono en el aire es: "
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEnergía luminosa del sol\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía nuclear\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía eólica\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía hídrica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía calorifica\u003C\/p\u003E",
            "t17correcta":""
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la energía obtenida a partir del viento, es decir, la energía cinética generada por efecto de las corrientes de aire, y que es convertida en otras formas útiles de energía para las actividades humana"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEnergía luminosa del sol\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía nuclear\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía eólica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía hídrica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía calorifica\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es  la energía que refiere a la luz del sol y la que en ella se transporta."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEnergía luminosa del sol\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía nuclear\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía eólica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía hídrica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía calorifica\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la manifestación de la energía en forma de calor."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEnergía luminosa del sol\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía nuclear\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía eólica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía hídrica\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía calorifica\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es aquella que se obtiene del aprovechamiento de las energías cinética y potencial de la corriente del agua, saltos de agua o mareas."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEnergía luminosa del sol\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía nuclear\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía eólica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía hídrica\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnergía calorifica\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es la energía que se libera espontánea o artificialmente en las reacciones nucleares."
      }
   }, 
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Convencción<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Conducción<\/p>",
                "t17correcta": "2,4"
            },
            {
                "t13respuesta": "<p>Radiación<\/p>",
                "t17correcta": "3,5"
            },
            {
                "t13respuesta": "<p>Conducción<\/p>",
                "t17correcta": "4,2"
            },
            {
                "t13respuesta": "<p>Radiación<\/p>",
                "t17correcta": "5,3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_a.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_c.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <\/p>"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,        
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Completa las oraciones con el adjetivo interrogativo que corresponda. Arrastra las palabras al lugar que les corresponde. Atiende los acentos en cada caso para responder correctamente la actividad."
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EMovimiento y velocidad muy lenta.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EMovimiento y velocidad alto.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EForma que permite una estructura sólida.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPropención a solidificarse.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Las partículas de los gases tienen:"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAceleran su movimiento y se expanden.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EReducen su movimiento y se contraen.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EMantienen cercanas unas de otras.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOcupan un volumen menor.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Cuando un gas se calienta, sus particulas:"
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Se hace más ligero<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Disminuye su densidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se hace más pesado.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aumenta su densidad.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Cuando un gas se enfría:"
        }
    },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Cambio climático.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Transferencia de calor.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Por el movimiento de los gases de la atmósfera al calentarse.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Por la disminución de calor.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Las corrientes de aire se generan en los océanos por:"
        }
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEl polo norte\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eel polo sur\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EMeridiano\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEcuador\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El origen de las correintes de viento ocurre en:"
      }
   }
]
