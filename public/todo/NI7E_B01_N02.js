json = [
    {
        "respuestas": [
            {
                "t13respuesta": "NI7E_B01_N02_05.png",
                "t17correcta": "0,1,2"
            },
            {
                "t13respuesta": "NI7E_B01_N02_03.png",
                "t17correcta": "1,2,0"
            },
            {
                "t13respuesta": "NI7E_B01_N02_04.png",
                "t17correcta": "2,0,1"
            },
            {
                "t13respuesta": "NI7E_B01_N02_02.png",
                "t17correcta": "3,4"
            },
            {
                "t13respuesta": "NI7E_B01_N02_06.png",
                "t17correcta": "4,3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Selecciona los ejemplos de seres vivos y arrástralos donde corresponda de acuerdo a la forma en la que obtienen sus nutrientes.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "NI7E_B01_N02_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "tamanyoReal":true,
            "borde":false

        },
        "contenedores": [
            {"Contenedor": ["", "89,144", "cuadrado", "145, 90", ".","transparent"]},
            {"Contenedor": ["", "242,145", "cuadrado", "145, 90", ".","transparent"]},
            {"Contenedor": ["", "392,145", "cuadrado", "145, 90", ".","transparent"]},
            {"Contenedor": ["", "91,353", "cuadrado", "145, 90", ".","transparent"]},
            {"Contenedor": ["", "242,353", "cuadrado", "145, 90", ".","transparent"]}
        ],
        "css":{
            "displayImgLineas":"none"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La respiración también forma parte de las características del metabolismo, donde los seres vivos pueden reproducirse.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hay dos procesos importantes dentro de la respiración: el intercambio de gases entre ambiente y seres vivos y la respiración celular ya que se obtiene energía en el interior de las células.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reproducción sólo puede ser de un tipo: sexual.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La respiración en los seres vivos puede ser de tipo: aerobio o anaerobio.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los clones sólo pueden darse en laboratorios para crear nuevos organismos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero o falso según sea el caso.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },     
    {
        "respuestas": [
            {
                "t13respuesta": "<p>células<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>unicelulares<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>célula<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>crecimiento<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>desarrollo<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>a) En los seres vivos, la unidad estructural que los forma son las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>b) Entre los seres vivos existen algunos de tamaño microscópico formados tan solo por una célula a los que se les denomina <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como bacterias, protozoarios y algunos hongos.<br>\n\
                                    c) Los organismos pluricelulares están formados por más de una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; incluso por miles o millones.<br>d) El <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es el proceso por el cual los seres vivos incrementan de tamaño por aumento en el número y tamaño de sus células.<br>\n\
                                e) El <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; implica cambios en la estructura y funcionamiento del cuerpo de un ser vivo a largo de su ciclo de vida.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>celula<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>celulas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>evolucion<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>respiracion<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>desarrollo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>crecimiento<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>homeostasis<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>reproduccion<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida":16,
            "t11pregunta": "Encuentra las siguientes palabras en la sopa de letras."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>ecosistemas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>acuáticas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>bióticos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>abióticos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>rocas<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>distribución<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; son áreas terrestres o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, dominadas por algún tipo de vegetación o por la ausencia de ella. Están formados por dos componentes interrelacionados: los factores<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y los factores <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.&nbsp; Los factores bióticos son todos los tipos de seres vivos que habitan en el ecosistema (plantas, hongos, animales, bacterias, protozoarios), y los factores abióticos son los elementos que no tienen vida como las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;o el suelo; estos actúan sobre los seres vivos y determinan su <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;e incluso su sobrevivencia.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>desintegradores<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>terciario<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>secundario<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>productores<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>primario<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Son el quinto nivel trófico, lo forman los seres heterótrofos, la mayoria son bacterias y hongos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es el consumidor de cuarto nivel trófico y corresponde a carnivoros que se alimentan de otros carnivoros."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es el consumidor de tercer nivel trófico y está representado por carnivoros depradores de herbívoros."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Ocupan el primer nivel trófico y son representados por los seres autótrofos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Son los consumidores que ocupan el segundo nivel trófico, corresponde a los herbívoros."
            }
        ],
        "pocisiones": [
            {
                "direccion": 0,
                "datoX": 2,
                "datoY": 4
            },
            {
                "direccion": 1,
                "datoX": 7,
                "datoY": 4
            },
            {
                "direccion": 1,
                "datoX": 3,
                "datoY": 3
            },
            {
                "direccion": 0,
                "datoX": 5,
                "datoY": 12
            },
            {
                "direccion": 1,
                "datoX": 14,
                "datoY": 3
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 23,
            "ancho": 23,
            "t11pregunta": "Completa el crucigrama"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Un ejemplo son los delfines.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tienen mayor posibilidad de sobrevivir.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los mapaches y osos pueden ser ejemplos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La extinción de estos animales puede ayudar al equilibrio de un ecosistema.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Son organismos que pueden consumir a otros organismos de varios niveles tróficos.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las características que puede llegar a presentar un animal omnívoro."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El flujo de la materia en los ecosistemas, permite que existan fenómenos como la erosión.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El transporte de los elementos que conforman un ecosistema permite que haya un intercambio y puedan reincorporarse al medio ambiente.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ciclos gaseosos, implican un depósito de elementos químicos en la atmósfera.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ciclos biogeoquímicos están conformados por los procesos correspondientes a los elementos como el carbón o el nitrógeno.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Selecciona la respuesta correcta",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>XIX<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>creacionista<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>1800<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>1859<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Charles R. Darwin<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La idea de que el ser humano y los seres vivos fueron creados, tal y como los conocemos, por un ser supremo al principio de los tiempos, prevaleció como una idea absoluta e irrefutable hasta finales del siglo <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Esta idea sobre el origen de la vida en el planeta Tierra, nombrada teoría <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, sin embargo, comenzó a desvanecerse con la publicación de ensayos por parte de filósofos y naturalista en las décadas de 1700 y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, en los que exponían la posibilidad de que los seres vivos cambian a través del tiempo. Fue hasta <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, con la publicación del libro “Sobre el origen de las especies por medio de la selección natural” escrito por naturalista inglés <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp(1809-1882), que se aceptó la idea de evolución en los seres vivos<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas": true,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Inventó el primer microscopio",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Refutó la teoría de que los organismos se crean a partir de alimentos en descomposición",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Microscopio cuyo funcionamiento es con base a electrones",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Diseñó su propio microscopio para estudiar el cielo",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Zacharias Janssen"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Louis Pasteur"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Microscopio Electrónico"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Galileo Galilei"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas."
        }
    }
]