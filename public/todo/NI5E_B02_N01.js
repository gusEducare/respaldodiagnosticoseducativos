json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Desperdicio y uso de recursos para satisfacer las necesidades del hombre.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Campañas para plantar árboles y plantas para tratar el agua sucia.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Contaminación ambiental, ya que el smog y la basura perurban el hábitat de dichas especies.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Campañas de esterilización y control natal de perros y gatos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pérdida de biodiversidad, al introducir especies exóticas que compiten con los locales.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona algunas de las causas de la pérdida de especies en nuestro país."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>ecosistema<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>geográfica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>físico<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>vegetación<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>terrestres<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>bosques<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>acuáticos<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>adaptación<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;se define como un área <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;donde habitan en conjunto de seres vivos que interactúan entre sí y con el medio físico que lo integra. Los ecosistemas están delimitados por el medio <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;donde ocurren y por el tipo de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;predominante que poseen. Así por ejemplo, existen ecosistema <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;como los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y selvas, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;como lo son lagos y lagunas, y marinos como los mares y oceános. La presencia de los seres vivos que integran cada ecosistema es resultado de un largo proceso evolutivo de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;a las condiciones ambientales propias de cada ecosistema, por lo que es difícil para cualquier ser vivo sobrevivir en un ecosistema distinto, o bien, en caso de hacerlo, podría ocasionar alteraciones severas en el ecosistema al que llega.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras que te ayuden a completar el siguiente párrafo que nos habla de los sistemas naturales de México."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Agua<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Riegos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Potable<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Consumo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Agrícola<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Prevención<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ecosistemas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Infecciones<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Abastecimiento<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Contaminación<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una medida para cuidar el agua es guardar en un recipiente o frasco, evitando que se tiren en el desagüe.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El uso de manguera y cubeta para lavar el automóvil es una medida eficiente para no desperdiciar el agua.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Podemos tardarnos el tiempo que querramos al bañarnos si usamos agua recolectada de la lluvia.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La basura en los ríos se degrada y no afecta a los ecosistemas acuáticos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando visito el mar o algún ecosistema acuático debo ser cuidadoso y no tirar basura.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para cuidar el agua, puedo proponer en mi escuela que utilicemos un sistema de recolección de agua de lluvia para lavar los baños, salones o espacios comunes.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    }
]