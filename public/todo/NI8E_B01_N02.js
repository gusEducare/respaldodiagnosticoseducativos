json = [
    {
        "respuestas": [
            {
                "t13respuesta": "34 m/s hacia el Norte",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "34 m/s hacia el Sur",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "34 m/s hacia el Este",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "34 m/s hacia el Oeste",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Si una bicicleta recorre 30m en 40s hacia el Norte, ¿Cuál es su velocidad?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "40 km",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "50 km",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "30 km",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "20 km",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué distancia recorre un avión que viaja hacia Tokio a 80km/h en un tiempo de 0.5 h?"
        }
    }
    ,
    {
        "respuestas": [
            {
                "t13respuesta": "3 m/s",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<sup>2</sup>&frasl;<sub>3</sub> m/s",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "2 m/s",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<sup>3</sup>&frasl;<sub>2</sub> m/s",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 2,
            "t11pregunta": "De acuerdo con el ejercicio anterior, ¿Cuál es la rapidez del auto?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "ni8e_b01_n02_01_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "ni8e_b01_n02_01_03.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "ni8e_b01_n02_01_04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "ni8e_b01_n02_01_05.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "ni8e_b01_n02_01_06.png",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arma el siguiente cuadro sinóptico del movimiento según Aristóteles usando las palabras y frases dadas.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "ni8e_b01_n02_01_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": true

        },
        "contenedores": [
            { "Contenedor": ["", "144,97", "cuadrado", "143, 80", ".", "transparent"] },
            { "Contenedor": ["", "144,407", "cuadrado", "143, 80", ".", "transparent"] },
            { "Contenedor": ["", "424,97", "cuadrado", "143, 80", ".", "transparent"] },
            { "Contenedor": ["", "424,329", "cuadrado", "143, 80", ".", "transparent"] },
            { "Contenedor": ["", "424,482", "cuadrado", "143, 80", ".", "transparent"] }
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Desplazamiento 12.8 Km",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Desplazamiento 13.2 Km",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Desplazamiento 12.8 Km",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Desplazamiento 13.2 Km",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Un camión de transporte sale a entregar mercancías por una carretera recta que va de Norte a Sur. En el Km 4.5 un agente de tránsito registra el paso del camión a las 14:45. Más tarde, en el Km 17.3, otro agente de tránsito registra el paso a las 15:17. Calcule el desplazamiento y la velocidad, en Km/min, del camión de transporte"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Velocidad 0.4 Km/min hacia el Sur",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Velocidad 0.4 Km/min hacia el Sur",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Velocidad 0.46 Km/min hacia el Sur",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Velocidad 0.46 Km/min hacia el Sur",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Usa el ejercicio anterior para calcular la velocidad del camión de transporte."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_02_02.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_02_03.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_02_04.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_02_05.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 2,
            "t11pregunta": "Observa el diagrama mostrado y determina cuál de las fuerzas anularía el efecto de la fuerza que ya actúa sobre la caja <br> <img style='width: 300px;' src=\"NI8E_B01_N02_02_01.png\">"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Las dos pelotas caen con la misma rapidez porque están sometidas a la misma aceleración (gravedad)",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cae primero la de béisbol porque es más pesada",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Cae primero la de papel por ser más ligera",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Se preguntó a 10 personas sobre cuál de dos objetos caería más rápido: una pelota de papel o una pelota de béisbol.  De las respuestas, ocho dijeron que caería más rápido la de béisbol y dos dijeron que caerían al mismo tiempo.  ¿Qué les diría Galileo Galilei?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"ni8e_b01_n02_03_01.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"ni8e_b01_n02_03_02.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"ni8e_b01_n02_03_03.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"ni8e_b01_n02_03_04.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 2,
            "t11pregunta": "¿Cuál de las siguientes gráficas representa al objeto que se mueve más rápido? Sugencia: identifica la gráfica que representa un cambio grande en el desplazamiento durante un tiempo muy pequeño"
        }
    },
    // {
    //     "respuestas": [
    //         {
    //             "t13respuesta": "<p>Es la ubicación de un objeto o persona con respecto a un observador.</p>",
    //             "t17correcta": "0"
    //         },
    //         {
    //             "t13respuesta": "<p>Es el cambio de posición. No se considera el tiempo que tarda en desplazarse el objeto.<br>D = d <span style='font-size: 12px'>final</span> – d <span style='font-size: 12px'>inicial</span></p>",
    //             "t17correcta": "1"
    //         },
    //         {
    //             "t13respuesta": "<p>Es la ruta que sigue el objeto desde el principio hasta el fin del movimiento. Suele ser más larga que el desplazamiento ya que no sigue una línea recta.</p>",
    //             "t17correcta": "2"
    //         },
    //         {
    //             "t13respuesta": "<p>Es el cambio de la posición considerando el cambio en el tiempo.</p>",
    //             "t17correcta": "3"
    //         },
    //         {
    //             "t13respuesta": "<p>Es el cambio de la velocidad dividido por el cambio en el tiempo.<br>a = V <span style='font-size: 12px'>final</span> - V <span style='font-size: 12px'>inicial</span> / Tiempo</p>",
    //             "t17correcta": "4"
    //         }
    //     ],
    //     "pregunta": {
    //         "c03id_tipo_pregunta": "5",
    //         "t11pregunta": "Completa el cuadro arrastrando las respuestas dadas y colocándolas en la posición correcta.",
    //         "tipo": "vertical"
    //     },
    //     "contenedores": [
    //         "Posición",
    //         "Desplazamiento",
    //         "Trayectoria",
    //         "Velocidad",
    //         "Aceleración"
    //     ]
    // },
    {
        "respuestas": [
            {
                "t13respuesta": "<p style='font-size:80%'>Posición<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p style='font-size:80%'>Desplazamiento<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:80%'>Trayectoria<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p style='font-size:80%'>Velocidad<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p style='font-size:80%'>Aceleración<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src=\"ni8e_b01_n02_04_02.png\">",
                "valores": ['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;'],
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src=\"ni8e_b01_n02_04_03.png\">",
                "valores": ['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;'],
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src=\"ni8e_b01_n02_04_04.png\">",
                "valores": ['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;'],
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src=\"ni8e_b01_n02_04_05.png\">",
                "valores": ['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;'],
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src=\"ni8e_b01_n02_04_06.png\">",
                "valores": ['&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;', '&nbsp;'],
                "correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta<\/p>",
            "descripcion": "Descripción",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "NI8E_B01_N02_05_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "NI8E_B01_N02_05_04.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "NI8E_B01_N02_05_03.png",
                "t17correcta": "2",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca los nombres de las características más importantes de un vector.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "NI8E_B01_N02_05_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["", "162,167", "cuadrado", "178, 44", ".", "transparent"] },
            { "Contenedor": ["", "268,409", "cuadrado", "178, 44", ".", "transparent"] },
            { "Contenedor": ["", "333,307", "cuadrado", "178, 44", ".", "transparent"] }
        ]
    },


    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_06_02.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_06_03.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_06_04.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"NI8E_B01_N02_06_05.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 2,
            "t11pregunta": "Observa el diagrama mostrado y determina cuál de las fuerzas anularía el efecto de la fuerza que ya actúa sobre la caja.<br><img style='width: 300px;' src=\"NI8E_B01_N02_06_01.png\"> "
        }
    },


    {
        "respuestas": [
            {
                "t13respuesta": "<p><img style='width: 87px;' src=\"ni8e_b01_n02_07_01.gif\"></p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img style='width: 87px;' src=\"ni8e_b01_n02_07_02.gif\"></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img style='width: 87px;' src=\"NI8E_B01_N02_07_03.gif\"></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img style='width: 87px;' src=\"NI8E_B01_N02_07_04.gif\"></p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Organiza los ejemplos mostrados de acuerdo al tipo de movimiento.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Lineal",
            "Ondulatorio"
        ]
    },



    {
        "respuestas": [
            {
                "t13respuesta": "<img style='width: 250px;' src=\"ni8e_b01_n02_08_01.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img style='width: 250px;' src=\"ni8e_b01_n02_08_02.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img style='width: 250px;' src=\"ni8e_b01_n02_08_03.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img style='width: 250px;' src=\"ni8e_b01_n02_08_04.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 2,
            "t11pregunta": "Selecciona la gráfica que muestra ondas con la misma longitud de onda pero con diferentes amplitudes."
        }
    }
]
