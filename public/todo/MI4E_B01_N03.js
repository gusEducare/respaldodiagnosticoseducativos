json =[
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_04.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 3,
 	  		"valor":1000
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_03.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":500
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_02.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":200
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_01.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":100
 	  	},
 	  	{
 	  		"t13respuesta": "cincuenta.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":50
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la cantidad mostrada.<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url": "MI4E_B01_N01_01_06.png",
 	  		"respuestaImagen": true,
 	  		"anchoImagen": 60,
 	  		"bloques": false,
 	  		"posicionamiento": "descendente",
 	  		"respuestaUnicaMultiple": true,
 	  		"filas": 1,
 	  		"borde":false,
 	  		"renglones": 1,
 	  		"contenedorContador":true
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "180,159", "cuadrado", "235, 164", "."]}
 	  	], 
 	  	"css":{
 	  		"posicionIndicadorValor":{top:107, left:26},
 	  		"anchoRespuestas":200
 	  	}
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_04.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":1000
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_03.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 5,
 	  		"valor":500
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_02.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":200
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_01.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":100
 	  	},
 	  	{
 	  		"t13respuesta": "cincuenta.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":50
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la cantidad mostrada.<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url": "MI4E_B01_N01_01_06.png",
 	  		"respuestaImagen": true,
 	  		"anchoImagen": 60,
 	  		"bloques": false,
 	  		"posicionamiento": "descendente",
 	  		"respuestaUnicaMultiple": true,
 	  		"filas": 1,
 	  		"borde":false,
 	  		"renglones": 1,
 	  		"contenedorContador":true
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "180,159", "cuadrado", "235, 164", "."]}
 	  	], 
 	  	"css":{
 	  		"posicionIndicadorValor":{top:107, left:26},
 	  		"anchoRespuestas":200
 	  	}
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_04.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":1000
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_03.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":500
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_02.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 9,
 	  		"valor":200
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_01.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":100
 	  	},
 	  	{
 	  		"t13respuesta": "cincuenta.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":50
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la cantidad mostrada.<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url": "MI4E_B01_N01_01_06.png",
 	  		"respuestaImagen": true,
 	  		"anchoImagen": 60,
 	  		"bloques": false,
 	  		"posicionamiento": "descendente",
 	  		"respuestaUnicaMultiple": true,
 	  		"filas": 1,
 	  		"borde":false,
 	  		"renglones": 1,
 	  		"contenedorContador":true
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "180,159", "cuadrado", "235, 164", "."]}
 	  	], 
 	  	"css":{
 	  		"posicionIndicadorValor":{top:107, left:26},
 	  		"anchoRespuestas":200
 	  	}
 	  },
 	  {  
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"Sobre A",
 	  		"t17correcta":"1"
 	  	},
 	  	{  
 	  		"t13respuesta":"Sobre B",
 	  		"t17correcta":"0"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"<img src=\"pef_l_s19_a1_01.png\">",
 	  		"forma":"cuadrado",
 	  		"texto":false,
 	  		"icono":false
 	  	}
 	  },
 	  {  
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"Sobre A",
 	  		"t17correcta":"1"
 	  	},
 	  	{  
 	  		"t13respuesta":"Sobre B",
 	  		"t17correcta":"0"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"<img src=\"pef_l_s19_a1_01.png\">",
 	  		"forma":"cuadrado",
 	  		"texto":false,
 	  		"icono":false
 	  	}
 	  },
 	  {  
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"Sobre A",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":"Sobre B",
 	  		"t17correcta":"1"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"<img src=\"pef_l_s19_a1_01.png\">",
 	  		"forma":"cuadrado",
 	  		"texto":false,
 	  		"icono":false
 	  	}
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "<p><sup>1</sup>&frasl;<sub>2</sub><\/p>",
 	  		"t17correcta": "1"
 	  	},
 	  	{
 	  		"t13respuesta": "<p><sup>1</sup>&frasl;<sub>6</sub><\/p>",
 	  		"t17correcta": "2"
 	  	},
 	  	{
 	  		"t13respuesta": "<p><sup>2</sup>&frasl;<sub>12</sub><\/p>",
 	  		"t17correcta": "3"
 	  	}
 	  	],
 	  	"preguntas": [
 	  	{
 	  		"c03id_tipo_pregunta": "8",
 	  		"t11pregunta": "Juan había comprado dos pasteles, pero al pasar a la casa de sus papás dejó la mitad. Ya en su casa recibió la visita de su primo y antes de que se fuera le dio la mitad de lo que tenía de pastel. En la noche dividió en tres partes lo que le quedaba de pastel, una para la cena, otra para el desayuno y la última para un compañero del trabajo. <br><table><tr><td>¿Qué fracción de un pastel le convidó a su primo?</td><td>"
 	  	},
 	  	{
 	  		"c03id_tipo_pregunta": "8",
 	  		"t11pregunta": "</td></tr><tr><td>¿Qué fracción de un pastel le convidó a su compañero de trabajo?</td><td>"
 	  	},
 	  	{
 	  		"c03id_tipo_pregunta": "8",
 	  		"t11pregunta": "</td></tr><tr><td>¿Qué fracción de la cantidad total que había comprado Juan, se comió él?</td><td>"
 	  	},
 	  	{
 	  		"c03id_tipo_pregunta": "8",
 	  		"t11pregunta": "</td></tr></table>"
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "8",
 	  		"t11pregunta": "Completa el siguiente p&aacute;rrafo."
 	  	}
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "6.80 + 6.80",
 	  		"t17correcta": "1"
 	  	},
 	  	{
 	  		"t13respuesta": "7.10 + 7.20",
 	  		"t17correcta": "2"
 	  	},
 	  	{
 	  		"t13respuesta": "8.90 + 7.90",
 	  		"t17correcta": "3"
 	  	},
 	  	{
 	  		"t13respuesta": "15.00 + 12.10",
 	  		"t17correcta": "4"
 	  	},
 	  	{
 	  		"t13respuesta": "10.10 + 3.30",
 	  		"t17correcta": "5"
 	  	}
 	  	],
 	  	"preguntas": [
 	  	{
 	  		"c03id_tipo_pregunta": "12",
 	  		"t11pregunta": "6.60 + 7.00"
 	  	},
 	  	{
 	  		"c03id_tipo_pregunta": "12",
 	  		"t11pregunta": "9.40 + 4.90"
 	  	},
 	  	{
 	  		"c03id_tipo_pregunta": "12",
 	  		"t11pregunta": "11.20 + 5.60"
 	  	},
 	  	{
 	  		"c03id_tipo_pregunta": "12",
 	  		"t11pregunta": "14.80 + 12.30"
 	  	},
 	  	{
 	  		"c03id_tipo_pregunta": "12",
 	  		"t11pregunta": "8.10 + 5.30"
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "12",
 	  		"t11pregunta": "s1a2"
 	  	}
 	  },
 	  {  
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"20",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":"24",
 	  		"t17correcta":"1"
 	  	},
 	  	{  
 	  		"t13respuesta":"28",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":"32",
 	  		"t17correcta":"0"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"1",
 	  		"t11pregunta":"Para elegir una contraseña para su correo, Leonel va a utilizar las vocales a, e y las cifras 1, 2. ¿Cuántas contraseñas distintas puede formar utilizando los cuatro caracteres?"
 	  	}
 	  },
	/*** Actividad 6 ***/
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "<p>1<\/p>",
 	  		"t17correcta": "1",
 	  		"coordenadas": "420,112"
 	  	},
 	  	{
 	  		"t13respuesta": "<p>2<\/p>",
 	  		"t17correcta": "1",
 	  		"coordenadas": "450,215"
 	  	},
 	  	{
 	  		"t13respuesta": "<p>3<\/p>",
 	  		"t17correcta": "1",
 	  		"coordenadas": "335,315"
 	  	},
 	  	{
 	  		"t13respuesta": "<p>4<\/p>",
 	  		"t17correcta": "1",
 	  		"coordenadas": "210,160"
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "2",
 	  		"t11pregunta": "",                        
 	  		"secuencia": true,
 	  		"ordenados": false,
 	  		"url":"rtp_k_s03_a01.png"
 			
 	  	}
 	  },
	  {
	  	"respuestas": [
	  	{
	  		"t13respuesta": "<p>Nombre del proyecto<\/p>",
	  		"etiqueta": "1",
	  		"t17correcta": "0"
	  	},
	  	{
	  		"t13respuesta": "<p>Comandos del Birdy<\/p>",
	  		"etiqueta": "2",
	  		"t17correcta": "1"
	  	},
	  	{
	  		"t13respuesta": "<p>Paleta de comandos<\/p>",
	  		"etiqueta": "3",
	  		"t17correcta": "2"
	  	},
	  	{
	  		"t13respuesta": "<p>Botones de acción<\/p>",
	  		"etiqueta": "4",
	  		"t17correcta": "3"
	  	},
	  	{
	  		"t13respuesta": "<p>Espacio de programación<\/p>",
	  		"etiqueta": "5",
	  		"t17correcta": "4"
	  	}
	  	],
	  	"pregunta": {
	  		"c03id_tipo_pregunta": "5",
	  		"t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
	  		"tipo": "ordenar",
	  		"imagen": true,
	  		"url":"rtp_d_s03_a01_a.png"
			
	  	},
	  	"contenedores": [
	  	{"Contenedor": ["", "10,210", "", "180, 50"]},
	  	{"Contenedor": ["", "90,100", "", "180, 50"]},
	  	{"Contenedor": ["", "246,45", "", "180, 50"]},
	  	{"Contenedor": ["", "2,490", "derecha", "180, 50"]},
	  	{"Contenedor": ["", "330,200", "", "180, 50"]}
	  	]
	  }
]