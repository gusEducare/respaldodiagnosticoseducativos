json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>adolescencia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>identidad<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>niñez<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>intereses<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>hormonas<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>testosterona<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>estrógenos<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En la &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;se desarrollan habilidades, capacidades y gustos; y con ello, se consolida la&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; personal. En la &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; la mayor parte de las decisiones están influidas por los padres o personas mayores, mientras que en la adolescencia se tiene capacidad de reflexionar, sentir y actuar según sus&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y necesidades. El tema de la sexualidad se hace presente y se manifiesta física y psicológicamente, ya que el cuerpo comienza con la producción de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; que en el caso de los hombres es la &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;  y en el caso de las mujeres son&nbsp;<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "Ventaja",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Desventaja",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Permiten un acercamiento con las personas que se encuentran lejos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Agilizan procesos y permiten hacer varias cosas a la vez. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pueden llegar a entorpecer la comunicación con la gente a nuestro alrededor.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pueden existir abusos o engaños que pueden publicarse a través de las redes sociales. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nos permite tener acceso a información, prácticamente de todo el mundo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cualquier información que sea subida a internet puede ser vista, prácticamente por cualquiera si no tenemos cuidado.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
     , {
        "respuestas": [
            {
                "t13respuesta": "<p>prioridad<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>a la vida<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>identidad<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>salud<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>descanso<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Derecho de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En caso de necesidad como puede ser desastre natural o situaciones de riesgo, deben ser \n\
                                   tomados en cuenta en primera instancia, así como diseñar políticas que mejoren sus \n\
                                   condiciones ante tal situación. <br>Derecho&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Garantizar su supervivencia, desarrollo y crecimiento en el ambiente. <br>Derecho a la&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Registro nacional, a través de un acta de nacimiento que indique su nacionalidad que lo identifique como parte de una cultura.<br>Derecho a la&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Cuidado de la alimentación, vacunación, atención médica, así como prevención de situaciones de riesgo.<br>Derecho al&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y juego: Promoción de la diversión y el descanso como parte del desarrollo integral.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tener varias parejas sexuales, pero conocidas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Usar preservativo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Si se trata de una primera relación, no hay consecuencias.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Acudir al médico una vez que se ha iniciado una vida sexualmente activa.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evitar compartir implementos personales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evitar el contacto con personas que parezcan enfermas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mantener una higiene del cuerpo y en especial de tus genitales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Utilizar la pastilla anticonceptiva, evita el contagio de enfermedades como el VIH.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "<p>experiencias<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>principios<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>integrantes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>personal<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>grupos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>pertenencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>afecciones<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>valores<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>vinculación<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
       {
        "respuestas": [
            {
                "t13respuesta": "<p>afectivas<\/p>",
                "t17correcta": "2"
            },
           {
                "t13respuesta": "<p>hormonal<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>atracción<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>vínculos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>noviazgo<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En los adolescentes las relaciones<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>y sentimentales son fundamentales y a su vez son inevitables, pues por la evolución<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>presente, la<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>sexual se manifiesta y comienzan los lazos afectivos y<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>amorosos que se reflejan a través de un<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "Violencia que se manifiesta con golpes, bofetadas, pellizcos en alguna o varias partes del cuerpo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Violencia que se manifiesta con gritos, insultos, palabras ofensivas, apodos o ridiculización de la persona. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Violencia que se manifiesta con insultos, manipulación, amenazas o humillaciones. No es visible a simple vista.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Violencia que se manifiesta con hostigamiento y abusos para lograr prácticas sexuales sin ejercicio de la libertad. ",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Física"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Verbal"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Psicológica"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Sexual"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
 {
        "respuestas": [
            {
                "t13respuesta": "<p>adicción<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>anorexia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>bulimia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>compulsion<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>dieta<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Habito negativo que desencadena consecuencias negativas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Enfermedad especifica caracterizada por una pérdida autoinducida de peso acompañada por una distorsion de la imagen corporal."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Trastorno de la alimentación de origen neurótico que se caracteriza por periodos en que se come compulsivamente, seguidos de otros de culpabilidad y malestar, con provocación  del vómito."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Impulso o deseo intenso de hacer algo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Brinda al organismo de forma equilibrada alimentos variados."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 4
            },
            {
                "direccion" : 1,
                "datoX" : 8,
                "datoY" : 3
            },
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 1
            },
            {
                "direccion" : 1,
                "datoX" : 4,
                "datoY" : 4
            },
            {
                "direccion" : 1,
                "datoX" : 2,
                "datoY" : 4
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3",
             "alto":18
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>VPH<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Gonorrea<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>VIH<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Clamidia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sifilis<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mentir puede estar justificado, si con esto evitamos un problema mayor. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Poner límites en una situación, aumenta nuestra seguridad  y nos hace mucho más responsables.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Anteponer mis convicciones no siempre es fácil, así que a veces, será necesaria la violencia.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La argumentación, el diálogo y la negociación deben ser siempre una constante en nuestra forma de relacionarnos con los demás. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al cumplir con mis obligaciones en tiempo y forma, lo hago por evitar una sanción y no por convicción. ",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
]