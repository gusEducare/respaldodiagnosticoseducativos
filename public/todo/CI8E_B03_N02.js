json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>escala<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>historia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>valores<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>criterios<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>utilidad<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Cada persona emplea una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; distinta de medición, misma que está motivada por su <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; personal, anhelos y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; sin embargo se utilizan <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; comunes como es el gusto, la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; el esfuerzo y el deseo para todo aquello que debe identificarse como importante en nuestra vida.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "Pagarle dinero a un policía para que no se lleve el automóvil detenido por haber cometido una infracción.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cuando decido no compartir mi refrigerio con algún compañero que no trae.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Una señora por la calle se tropieza y nadie se detiene a ayudarla.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Dos personas trabajan el mismo número de horas, haciendo el mismo tipo de trabajo pero a una por ser mujer, le pagan menos.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Corrupción"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Egoísmo"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Indiferencia"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Desigualdad"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Corte de cabello<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elección del jefe de grupo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elección del jefe de grupo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elección de un presidente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>tener novio(a)<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elección de tu equipo de fútbol favorito<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Individuales",
            "Colectivas"
        ]
    },
    
        {
        "respuestas": [
            {
                "t13respuesta": "<p>identidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>rasgos<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>físicas<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>necesidades<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>valores<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>responsabilidad<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En la construcción de la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; intervienen el desarrollo de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; personales, capacidades <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; e intelectuales, así como gustos y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; sin embargo la identificación con otros hace reconocer los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; personales y a su vez identificar aquellos aspectos que son compartidos y que implican una <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La declaración de los derechos del hombre y de la mujer se publicaron en 1889, durante la Revolución Francesa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sólo las mujeres pueden desempeñar ciertas labores como la crianza de los hijos, la cocina o la limpieza de una casa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es mucho más común observar enfermeras que enfermeros o secretarias que secretarios.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La equidad de género es necesario ya que debemos continuar con estereotipos que nos ayuden a conservar los valores.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En cualquier situación, es mejor evitar los extremos o las etiquetas que puedan hacer sentir mal a alguna persona.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
    
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al relacionarnos con otras personas debemos de tratar de imponer nuestro punto de vista, reforzar nuestras posiciones aunque nuestros argumentos no sean sólidos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "-Para poder llegar a un consenso cuando permitimos que las personas planteen sus puntos de vista, aunque sean diferentes a los míos y los tomo en cuenta.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando discrimino a alguien, limito la libertad de alguien más.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando excluyo a alguien, tengo el derecho de hacerlo si no coincide con lo que pienso.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "-La divergencia de pensamiento es necesaria para poder encontrar otros puntos de vista.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPensar individualmente en ideas para resolver un conflicto.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EProponer muchas ideas entre todos para crear algo nuevo.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETener ideas sobre c\u0026oacute;mo desempe\u0026ntilde;arse en el sal\u0026oacute;n de clases.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EImaginar y dibujar propuestas de ideas para crear una empresa.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Una lluvia de ideas es una t\u0026eacute;cnica grupal que consiste en:"
      }
   }
    
]