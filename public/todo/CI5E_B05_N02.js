json=[  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Cuidando el agua y los recursos naturales del país",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Haciendo ejercicio cada día",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Aprovechando mis oportunidades de educación para desarrollar mi máximo potencial",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Alimentándome sanamente",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Respetando a cada persona.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Evitando la discriminación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Alimentando a los perros callejeros",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Haciendo labores de voluntario",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Limpiando mi habitación",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Fomentando culturas de paz y legalidad",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Participando activamente en política",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Señala todas las maneras en las que puedes contribuir al bienestar social en México."
      }
   },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Bienestar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Participación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Conflicto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Emprender<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sociedad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Dialogo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Derechos<\/p>",
                "t17correcta": "0"
            },
            
             {
                "t13respuesta": "<p>Meditación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Iniciativa<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Social<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Autoridad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Paz<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Humanos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ciudadano<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
             "t11medida": 13,
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Dialogo",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Huir del conflicto",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Negociación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Mediación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Intimidación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Provocación",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué estrategias pacíficas son óptimas para resolver conflictos?"
      }
   },
   
       {
        "respuestas": [
            {
                "t13respuesta": "<p>derechos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>inherentes<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>humanos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>distinción<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>mismos<\/p>",
                "t17correcta": "5"
            },
             {
                "t13respuesta": "<p>discriminación<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>universales<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>contemplados<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>obligaciones<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>gobiernos<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>medidas<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>abstenerse<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>proteger<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>libertades<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>individuos<\/p>",
                "t17correcta": "15"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br>Los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; humanos son derechos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; a todos los seres <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, sin <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; alguna de nacionalidad, lugar de residencia, sexo, origen nacional o étnico, color, religión, lengua, o cualquier otra condición. Todos tenemos los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; derechos humanos, sin <\/p>"
            }, 
            
            
            
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; alguna. Estos derechos son interrelacionados, interdependientes e indivisibles.<br>Los derechos humanos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; están a menudo <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; en la ley y garantizados por ella, a través de los tratados,el derecho internacional, los principios generales y otras fuentes del derecho internacional. El derecho internacional de los derechos humanos establece las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; que tienen los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de tomar <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; en determinadas situaciones, o de <\/p>"
            },
            
            
            
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de actuar de determinada forma en otras, a fin de promover y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; los derechos humanos y las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; fundamentales de los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;o grupos.<\/p>"
            } 
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"TÍTULO PRIMERO, CAPÍTULO I.De las Garantías Individuales",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"TÍTULO PRIMERO, CAPÍTULO II.De los mexicanos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"TÍTULO PRIMERO, CAPÍTULO III.De los extranjeros",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"TÍTULO PRIMERO, CAPÍTULO IV.De los Ciudadanos Mexicanos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"TÍTULO SEGUNDO, CAPÍTULO I.De la Soberanía Nacional y de la Forma de Gobierno",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"TÍTULO SEGUNDO, CAPÍTULO II.De las Partes Integrantes De la Federación y El Territorio Nacional",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"TÍTULO TERCERO, CAPÍTULO I.De la División de Poderes",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"TÍTULO TERCERO, CAPÍTULO II.Del Poder Legislativo",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"TÍTULO TERCERO, CAPÍTULO III.Del Poder Ejecutivo",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué capítulo de nuestra Constitución política se relaciona directamente con derechos humanos?"
      }
   },
      {
        "respuestas": [
            {
                "t13respuesta": "Personal, Familiar, Comunitario, Local, Nacional, Internacional",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cuando afectan ala estructura de distribución de poder. Cuando afectan acuerdos políticos o de intereses entre actores sociales.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se observan fácilmente Están encubiertos Incluyen agresión y hostilidad",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Por el ámbito"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Por su impacto en la sociedad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Por los síntomas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Felicidad",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Simulación",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Bienestar social",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Patriotismo",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Identifica el concepto que se utiliza para describir cuando la población tiene sentido de satisfacción y tranquilidad, se relaciona con la calidad de vida."
      }
   },
   
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Derecho a la educación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Derecho a comer golosinas",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Derecho a casa y alimento",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Derecho a visitar a tus amigos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Derecho a la salud",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"¿Cuáles de estos son derechos humanos que vives en el día a día?"
      }
   },
   
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Comité internacional de coordinación de las instituciones nacionales de derechos humanos / CUCIOC",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comisión Nacional de Agua /CONAGUA",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Comisiones Estatales de Derechos Humanos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Centro por la Justicia y el Derecho Internacional /CEJIL",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comisión Nacional Bancaria y de valores /CNBV",
            "t17correcta":"0"
         }
         
         ,
         {  
            "t13respuesta":"Organización de las naciones unidas / ONU",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comisión Nacional de Bioética / CNB",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La Comisión Nacional de los Derechos Humanos / CNDH",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Señala las instituciones dedicadas a a la protección y defensa de los derechos humanos a nivel nacional internacional."
      }
   }
];