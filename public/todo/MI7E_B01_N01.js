json=[
     {
         "respuestas": [
             {
                 "t13respuesta": "<img src='MI7E_B01_N01_01_02.png'>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N01_01_03.png'>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N01_01_04.png'>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<img src='MI7E_B01_N01_01_05.png'>",
                 "t17correcta": "0"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "1",
             "t11pregunta": "<img src='MI7E_B01_N01_01_01.png'>"
         }
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "mi7e_b01_n01_02_03.png",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "mi7e_b01_n01_02_02.png",
                 "t17correcta": "1"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "5",
             "t11pregunta": "Arrastra la fracción y el número decimal al punto que les corresponde sobre la recta numérica.",
             "tipo": "ordenar",
             "imagen": true,
             "url": "mi7e_b01_n01_02_01.png",
             "respuestaImagen": true,
             "bloques": false,
             "tamanyoReal":true,
             "borde":false
 
         },
         "contenedores": [
             {"Contenedor": ["", "98,278", "cuadrado", "50, 75", ".","transparent"]},
             {"Contenedor": ["", "352,350", "cuadrado", "50, 75", ".","transparent"]}
         ]
     },
     {  
       "respuestas":[  
          {  
             "t13respuesta":"<img src='MI7E_B01_N01_03_02.png'>",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"<img src='MI7E_B01_N01_03_03.png'>",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"<img src='MI7E_B01_N01_03_04.png'>",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"<img src='MI7E_B01_N01_03_05.png'>",
             "t17correcta":"0"
          }
       ],
       "pregunta":{  
          "c03id_tipo_pregunta":"1",
          "t11pregunta":"<img src='MI7E_B01_N01_03_01.png'>"            
       }
    },
     {
         "respuestas": [
             {
                 "t13respuesta": "<p style='color:black;'>7</p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p style='color:black;'>8</p>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<p style='color:black;'>2</p>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<p style='color:black;'>3</p>",
                 "t17correcta": "3"
             },
             {
                 "t13respuesta": "<p style='color:black;'>4</p>",
                 "t17correcta": "4"
             },
             {
                 "t13respuesta": "<p style='color:black;'>5</p>",
                 "t17correcta": "5"
             },
             {
                 "t13respuesta": "<p style='color:black;'>6</p>",
                 "t17correcta": "6"
             },
             {
                 "t13respuesta": "<p style='color:black;'>9</p>",
                 "t17correcta": "7"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "5",
             "t11pregunta": "",
             "tipo": "ordenar",
             "imagen": true,
             "url": "MI7E_B01_N01_04_01.png",
             "respuestaImagen": true,
             "bloques": false,
             "contieneDistractores": true,
             "tamanyoReal": true,
             "borde": false,
             "opcionesTexto":true
         },
         "contenedores": [
             {"Contenedor": ["", "176,456", "cuadrado", "50, 34", ".", "transparent"]},
             {"Contenedor": ["", "212,456", "cuadrado", "50, 34", ".", "transparent"]}
         ]
     },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>8<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>29<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>50<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>27<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>729<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>28<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>52<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>7<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>749<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>24<\/p>",
                "t17correcta": "12"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/><br> a) "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, 15, 22, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, 36, 43, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, 57, 64,…<br><br>b) 1, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, 9 <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, 81, 243, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, 2 187,… <\/p>"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Analiza la regularidad de las siguientes sucesiones y coloca en el lugar que corresponde los términos que faltan.",
            "contieneDistractores": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true,
            "respuestasLargas":true,
            "anchoRespuestas":50
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "2a + 2b",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "(ab)(2b)",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "2(ab)",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "ab",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las expresiones algebraicas permiten obtener el perímetro y área de la siguiente figura:<br>\n\
                            <div style='text-align:center;'><img src='MI7E_B01_N01_05.png' width:220px;>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Rombo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Rectángulo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Romboide",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Cuadrado",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "La siguiente imagen muestra la diagonal de un cuadrilátero y uno de sus lados. ¿A qué cuadrilátero corresponde el trazo? \n\
                    <div style='text-align:center;'><img src='mi7e_b01_n01_06.png' width:220px;>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='mi7e_b01_n01_07_01.png'>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src='mi7e_b01_n01_07_02.png'>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src='mi7e_b01_n01_07_03.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='mi7e_b01_n01_07_04.png'>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige la opción que muestra los trazos para construir un triángulo isósceles."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Armando $1 200 y Pedro $1 800",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Armando $1 000 y Pedro $2 000",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Armando $1 500 y Pedro $1 500",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Armando $1 300 y Pedro $1 700",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee el problema y elige la respuesta correcta.<br/>Armando y Pedro compraron un billete de lotería y obtuvieron un premio de $3 000. Armando puso $40 para comprar el billete y Pedro puso $60. Si reparten de manera proporcional el premio, ¿cuánto dinero le corresponde a cada uno? "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Todos los jugadores tienen la mismo oportunidad de ganar.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El cinco es el número con más posibilidades de ganar porque salió menos veces en la partida anterior.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El dos tiene más posibilidades de ganar porque salió más veces en la partida pasada.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Elegir el número que ganó la partida pasada no garantiza que ganarás la siguiente.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Los resultados del último juego no permiten anticipar quien va a ganar.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee el problema y elige la respuesta correcta.<br/>Pamela juega con sus amigos en el siguiente tablero. Cada uno elige un número, lanzan un dado y, según el número que salga el jugador correspondiente avanza una casilla. El primero que llega a la meta gana.<br>El tablero muestra los resultados de la última partida.\n\
                    <br> <div style='text-align:center;'><img src='mi7e_b01_n01_08.png'></div> \n\
                   Toca las afirmaciones que no son ciertas."
        }
    }
];