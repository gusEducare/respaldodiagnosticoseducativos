[
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Salir al cine con los amigos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Vivir en compañía de otros de manera pacífica y armoniosa.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Habitar en la misma casa.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Elige la opción que define convivencia."
      }
   },
        {  
      "respuestas":[  
         {  
            "t13respuesta":"Sobrecalentamiento",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Sobreexplotación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Sobrepoblación",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Cómo se le llama a la explotación de un recurso natural de manera abusiva o que excede a lo necesario."
      }
   },
        {  
      "respuestas":[  
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"De las siguientes imágenes lleva al contenedor aquellas donde se muestren actividades que aunque tú no las realizas, son indispensables para tu desarrollo, vida y sana convivencia."
      }
   },
        {  
      "respuestas":[  
         {  
            "t13respuesta":"Justicia",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Reciprocidad",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Relaciones",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Sociedad",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"De entre las siguientes elige la palabra relacionada con la descripción. Se dice que es la base para un ambiente justo y armónico ya ayuda al generar entendimiento a partir de la empatía."
      }
   },
        {  
      "respuestas":[  
         {  
            "t13respuesta":"A Pedro le llamamos “el indio” por sus raíces toltecas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Mariana siempre lleva fruta para el almuerzo.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Cecilia es una niña con Down, cuando estamos solos en el salón todos le lanzamos bolas de papel y la ofendemos hasta que llora.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Diego es excelente para las matemáticas y pésimo en español.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las situaciones que ejemplifican discriminación en el colegio."
      }
   }  
]