json = [
       {
        "respuestas": [
            {
                "t13respuesta": "socializar",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "individual",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "institución",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "voluntad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "diversidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "aprendizaje",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "libertad",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Identifica las palabras en la sopa de letras."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Se mantendrá por completo ajena a cualquier doctrina religiosa.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "La educación desarrollará todas las facultades del ser humano, fomentando el amor a la Patria",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "El Estado brindará educación que no tendrá costo para los estudiantes.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Todos los niños y adolescentes deben acudir a la escuela.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Laica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Armónica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Gratuita"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Obligatoria"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las características que tiene la educación en México, según nuestra constitución."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Desarrollas habilidades para trabajar en equipo, tomando en cuenta la información y propuestas que todos tus compañeros brindan para hacer una exposición.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Conoces todos los pasos para realizar un trabajo de investigación, tomando en cuenta que no sólo con búsquedas rápidas de internet se puede elaborar un trabajo de calidad.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Escuchas a las personas, tomando en cuenta sus puntos de vista, aunque no estés de acuerdo del todo con los mismos.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Eres tolerante y estás abierto a la diversidad en cuanto a la forma de ser, pensar o actuar de los demás.</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra cada situacion al ambito que pertenece.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Académico",
            "Personal"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "humano",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "derechos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "dignidad",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "libertad",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "A pesar de las diferencias en relación a nuestras características propias, todos pertenecemos a un grupo “"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "” el cual posee "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "igualitarios que velan por la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;y "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;de las personas."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Aumento de vello corporal</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Primeros noviazgos</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Reto a la autoridad</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra los ejemplos a los tipos de cambios que pueden darse en la adolescencia.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Físico",
            "Psicológico"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Artículo 1",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Artículo 2",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Artículo 3",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Artículo 4",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Artículo 5",
                "t17correcta": "4"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nadie será sometido a torturas ni a penas o tratos crueles, inhumanos o degradantes.",
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todo individuo tiene derecho a la vida, a la libertad y a la seguridad de su persona.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nadie estará sometido a esclavitud ni a servidumbre, la esclavitud y la trata de esclavos están prohibidas en todas sus formas.",
                "correcta"  : "3"
            },            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los seres humanos nacen libres e iguales en dignidad y derechos y, dotados como están de razón y conciencia, deben comportarse fraternalmente los unos con los otros.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Toda persona tiene todos los derechos y libertades proclamados en esta Declaración, sin distinción alguna de raza, color, sexo, idioma, religión, opinión política o de cualquier otra índole, origen nacional o social, posición económica, nacimiento o cualquier otra condición.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona el artículo al que pertenece cada enunciado.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },
    {  
          "respuestas":[
             {  
                "t13respuesta":"<p>Autónoma</p>",
                "t17correcta":"0",
                "numeroPregunta":"0"
             },
             {  
                "t13respuesta":"<p>Heterónoma</p>",
                "t17correcta":"1",
                "numeroPregunta":"0"
             },
              {  
                "t13respuesta":"<p>Autónoma</p>",
                "t17correcta":"1",
                "numeroPregunta":"1"
             },
             {  
                "t13respuesta":"<p>Heterónoma</p>",
                "t17correcta":"0",
                "numeroPregunta":"1"
             }
          ],
          "pregunta":{  
             "c03id_tipo_pregunta":"2",
             "t11pregunta": ["Se manifiesta en obligaciones u órdenes que se siguen por imposición o imitación. Esto implica un temor a la consecuencia al hacer o no determinada acción.","Se manifiesta desde el interior de cada persona al cuestionar las normas, incluso fuera de nuestras propias perspectivas. Implicando el reconocimiento sobre las diferencias personales y de quienes nos rodean."],
             "preguntasMultiples": true,
             "columnas":2
          }
       },    
        {
            "respuestas": [
                {
                    "t13respuesta": "<p>Tienes problemas de comunicación con tus padres porque no piensan como tú.</p>",
                    "t17correcta": "0"
                },
                {
                    "t13respuesta": "<p>Puedes tener problemas si brindas información personal en redes sociales.</p>",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "<p>Te sientes poco entendido o valorado por tus maestros u otros adultos.</p>",
                    "t17correcta": "0"
                },
                {
                    "t13respuesta": "<p>No todos tienen acceso a las nuevas tecnologías o conexión a internet.</p>",
                    "t17correcta": "1"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "5",
                "t11pregunta": "Arrastra los problemas, identificando si pueden ser situaciones que vives en la actualidad o que siempre han existido en relación a la adolescencia.",
                "tipo": "horizontal"
            },
            "contenedores":[ 
                "Adolescentes en todas las épocas",
                "Adolescentes en la actualidad"
            ]
        },
        {
            "respuestas": [
                {
                    "t13respuesta": "razonar",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "éticos",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "verbales",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "respeto",
                    "t17correcta": "4"
                },
                {
                    "t13respuesta": "objetivas",
                    "t17correcta": "5"
                }
            ],
            "preguntas": [
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "Los seres humanos por nuestra capacidad de "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": ", logramos participar en la definición de acuerdos mismos que requieren de elementos cívicos y principios "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": ". Estos acuerdos pueden ser "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "&nbsp; o escritos, sin embargo ambos tienen la misma validez, pues impera "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": ", voluntad y resoluciones "
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "."
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Completa el siguiente p&aacute;rrafo."
            }
        },
        {
            "respuestas": [
                {
                    "t13respuesta": "Verdadero",
                    "t17correcta": "0"
                },
                {
                    "t13respuesta": "Falso",
                    "t17correcta": "1"
                }
            ],
            "preguntas" : [
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Las normas y reglas son elementos que no debes cuestionar y sólo debes seguirlas.",
                    "correcta"  : "1"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Las normas y reglas te ayudan a desarrollar hábitos que favorecen tu perspectiva personal.",
                    "correcta"  : "0"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Sólo debes seguir una regla, cuando sea para tu propio beneficio.",
                    "correcta"  : "1"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Puedes considerarte una persona congruente, cuando decides seguir las reglas porque consideras que está bien y no porque te de miedo el castigo.",
                    "correcta"  : "0"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Todas las personas mayores se convierten en figuras de autoridad.",
                    "correcta"  : "1"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Selecciona verdadero o falso según sea el caso.<\/p>",
                "descripcion": "",   
                "variante": "editable",
                "anchoColumnaPreguntas": 60,
                "evaluable"  : true
            }        
        }
    ]