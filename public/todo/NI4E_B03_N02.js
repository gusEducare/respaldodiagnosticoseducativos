json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Hielo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Diamante<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Madera<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Bolas de plasma<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pantallas de plasma<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Jugo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Petróleo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Agua<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>oxigenada<\/p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Sólido",
            "Plasma",
            "Liquido"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Alcohol en gel<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Gelatina<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mostaza<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Humo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Vapor de agua<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Oxigeno<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Coloide",
            "Gaseoso"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=''>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=''>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src=''>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src=''>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Solidificación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Fusión"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Condensación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Evaporación"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
  {
        "respuestas": [
            {
                "t13respuesta": "<p>asar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>vapor<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>freir<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>guisar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>hervir<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>hornear<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>bañomaria<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>microondas<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Endurece los alimentos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elimina microorganismos nocivos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Les quita los nutrientes.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Favorece la absorción de nutrientes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Retarda la descomposición.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La cocción facilita la digestión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Facilita la digestión.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El calor es una forma de energía.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La transferencia de energía pasa de un cuerpo con temperatura elevado a otro frio.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Calor y fuego son indispensables para la vida humana por sus muchas utilidades.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El sol es una importante fuentes de claor para nuestro planeta.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>A la energía que se transfiere por diferencia de temperaturas se le llama calor.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Existen varias formas de general calor entre ellas fricción, combstiojn y electricidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los objetos se sienten fríos cuando los tocamos ya que nuetran temperatura corporal es mayor.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Con el desarrollo tecnológico, el fuego ya no es necesario para la vida humana.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El calor proviene únicamente deñ sol.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los seres humanos podemos sobrevivir sin energía calórica.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los objetos se sienten fríos por no recibir el calor solar.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ideas para obtener dinero",
            "Ideas para administrar el dinero"
        ]
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>La congelación es una técnica que acaba con todos los nutrientes de los alimentos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La deshidratación favorece la conservación de alimentos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La congelación y baja temperatura pueden eliminar microorganismos nocivos para el ser humano.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los microorganismos ayudan a conservar los alimentos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Todos los microorganismos viven y se desarrollan en los alimentos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Lavar los alimentos elimina siempre y en todos los casos los microorganismos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La cocción ayuda a eliminar microorganismos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dejar los alimentos a temperatura ambiente nunca los descompone.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La pasteurización también elimina microorganismos<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas":2,
            "t11pregunta": ""
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Pollo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Salmon<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Carne de res<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Calabazas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Salmon<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Zanahoria<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Espinaca<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Huevo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Carne de res<\/p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Asado",
            "Al vapor",
            "Frito"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Carne de res<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Zanahoria<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Lentejas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pollo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Calabazas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Huevo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Atole<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Carne de res<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Zanahoria<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Espinaca<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Horneado",
            "Hervido"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El agua dulce es inagotable en el planeta.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el planeta tenemos la misma cantidad de agua dulce y saladas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas el agua dulce proviene de ríos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En general el agua que utilizamos en las regaderas se reutiliza.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }   
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La contaminación provoca que la posible agua para consumo de seres vivos disminuya.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aproximadamente el 3% del total de agua del planeta es dulce.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El 2% del agua dulce del planeta se encuntra en forma de hielo.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }   
    }
]

