json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Homo Habilis pobló el continente asiático. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Homo erectus  descubrió el fuego. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Homo Sapiens Neardenthalensis  fue el primero en desarrollar rituales para enterrar a los muertos",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El hombre actual proviene del Homo Sapiens Neardenthalensis.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las primeras organizaciones sociales con cierta complejidad se dieron con el Homo Sapiens Sapiens.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p> Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>agricultura<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>sedentarios<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>neolítico<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>población<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>extinción<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>cerámica<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>nómadas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>piedra<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>fuego<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": " Selecciona las palabras correspondientes."
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>mesopotamia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>egipcios<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>civilizacion<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>sumerios<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>uruk<\/p>",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Albergó a los sumerios, acadios, babilonios, asirios y persas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Establecidos a orillas del río Nilo. Desarrollo de jeroglificos. Tumbas para sus gobernantes. Uso del aire como forma de energía."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Conjunto de costumbres, ideas, creencias, cultura y conocimientos cientificos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Surgen en Mesopotamia. Crean la rueda y escritura. Desarrollan conceptos de astronomía y matematicas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Ciudad más grande su época. Arquitectura para contar con un centro de reunión. Edificios para el gobierno y religión."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 1,
                "datoY" : 0
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 1
            },
            {
                "direccion" : 1,
                "datoX" : 3,
                "datoY" : 0
            },
            {
                "direccion" : 1,
                "datoX" : 8,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 7,
                "datoY" : 5
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "ancho":16,
            "alto":16,
            "t11pregunta": " Completa el siguiente crucigrama."
        }
    },
  {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lucy pertenece a la especie de los Homo erectus.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los restos de Lucy permitieron conocer más sobre el origen de la evolución del hombre.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su descubrimiento fue realizado por el paleontoantropólogo Donald Johanson en 1874.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dentro de las características más importantes que demostraron los restos de Lucy, fue que caminaba en dos extremidades para poder recolectar alimentos con dos manos. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los restos de Lucy fueron encontrados en el continente asiático. ",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
]