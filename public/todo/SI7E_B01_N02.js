json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Lo mejor para una investigación seria es elegir información únicamente de internet.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Al investigar es válido obtener información de una sola fuente.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Para investigar de manera confiable es necesario consultar cuando menos tres fuentes de información.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Al investigar es importante tomar en cuenta fuentes de investigación seria y expertas en el tema.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todos los enunciados verdaderos"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "De causa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Consecuencia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Oposición",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Igualdad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Explicación",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Temporalidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Ejemplificación",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 2,
            "t11pregunta": "Identifica el tipo de nexo utilizado en la frase. <br> Sin explicación alguna Julián olvidó sus nombres; <ins>entonces</ins>, a Hortensia le llamó Karla y a Mireya Mariana. Estaba realmente apenado."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Apellido, Inicial del nombre. (Año). <i>Título.</i> Recuperado de <u>http://www.xxxxxx.xxx</u>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Apellido, Inicial del nombre. (año, mes y día). Título del artículo. <i>Nombre del periódico.</i> Recuperado de <u>http://www.xxxxxx.xxx</u>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Apellido, Inicial del nombre. (Año). <i>Título del libro.</i> Ciudad de la publicación. Nombre de la editorial.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Apellido, inicial del nombre. (año).<i>Título de la página o lugar.</i> Recuperado el (Fecha) de <u>http://www.xxxxxx.xxx</u>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Elige la opción que indique la forma de citar libros de acuerdo con APA:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<style='font-size:98%;'>Los textos se organizan de esta manera, cada uno de ellos tiene una idea principal y otras que son secundarias que ofrecen detalles o refuerzan la idea principal.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<style='font-size:98%;'>Representan niveles inferiores de organización se colocan dentro del cuerpo del texto y sirve para introducir y anticipar contenido de apoyo y temas derivados del tema principal.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<style='font-size:98%;'>Elemento del texto que atrae la atención del lector, visualmente permiten ubicar información de manera rápida.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<style='font-size:98%;'>Frase que anuncia y encabeza el texto exponiendo de manera resumida el tema a tratar. Siempre son generales y breves.<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Párrafos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Subtítulos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Apoyos gráficos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Título"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas"
        }
    },
     {
         "respuestas": [
             {
                 "t13respuesta": "<p>letras<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>infinitivo<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>imperativo<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>tipografias<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>convivencia<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>numerales<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>permitido<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>autoridad<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>derechos<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>sanciones<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>normas<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>deberes<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>limites<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>ordenes<\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p>procedimientos<\/p>",
                 "t17correcta": "0"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "16",
             "t11medida":14,
             "t11pregunta": "Localiza las palabras y al concluir pon un título a la sopa de letras."
         }
     },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Mito<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Leyenda<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Cuento<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Ensayo<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p style='font-size:70%'> En el principio, Dios pronunció Su Nombre, y el Manred (la primera sustancia del Universo) fue formado. El Manred era un conglomerado de diminutas partículas indivisibles, cada una de las cuales eran Dios y a la vez parte de Dios. La vida surgió de Annwn (la nada). Fue Partholan el primer ser en llegar a Irlanda. Llegó con su Reina Dalny y un grupo de compañeros. Vinieron del Oeste, de la tierra de los muertos. Poco tiempo después de haberse instalado en esta tierra, tuvieron que luchar contra la temible raza de los Fomorianos: seres crueles, violentos, deformes y malignos. Los vencieron después de largas luchas. Los Partholeanos desaparecerían tiempo después, a causa de la gran peste. Los Fomorianos retomaron el poder en Irlanda y bajo el mando de sus dos reyes: Morc y Connan, tenían totalmente tiranizada la tierra de Partholan. Fue entonces que llegaron los nemedios, parientes de la raza de Partholan. Estos dieron una fuerte lucha, pero al final salieron derrotados por los fomorianos. Solo treinta nemedios sobrevivieron a la cruenta guerra. Se dice que de estos treinta había una familia que se llamaba Britan, y se debe a ésta el nombre actual de Gran Bretaña. Tiempo después apareció el gran pueblo de Dana. Ellos vinieron del cielo, pues su origen era divino. Dana era hija del jefe de los dioses Dagda. Los danaanos se esparcieron por cuatro grandes ciudades: Falias, Gorias, Finias y Murias. En cada ciudad adquirieron conocimientos propios de cada región. De Falias trajeron la Piedra del Destino, la cual se ponían los reyes al ser coronados. De Gorias se trajeron la Espada Invencible de Lugh. De Finias trajeron una lanza mágica y de Murias el Caldero de los Dagda, el cual tenía la propiedad de poder alimentar a todo un ejército y no quedar nunca vacío. Fue con todas estas posesiones que llegaron a Irlanda. Al llegar se encontraron con los Firbolgs (seres mortales). Estos no aceptaron ningún tipo de tratado sobre división de tierras, así que declararon la guerra a los danaanos. Se enfrentaron en Moytura. Al mando de los danaanos estaba Nuada, el de la mano de plata, quien no podía ser rey debido a su defecto de la mano. La victoria fue de los danaanos gracias, entre otras cosas, a sus artes mágicas. Sucedió entonces que el pueblo quería a Nuada como rey a pesar de su defecto. El monarca actual: Bres, tuvo que ceder su corona. Poco después Bres se enteró que era pariente directo de la corte de los fomorianos (enemigos de los danaanos). Así que traicionando a su gente, buscó el apoyo de Balor, rey de los fomorianos, para conquistar al pueblo de Dana. Balor era conocido como el Ojo Diabólico, pues tenía un solo ojo y con la sola mirada de éste podía matar a quien quisiera. Pero, por cuestiones de vejez, no podía mantener el ojo abierto mucho tiempo. El pueblo de Dana cayó entonces bajo el yugo de los fomorianos por un largo tiempo. Los danaanos esperaban con ansia la llegada de un Salvador que los libertara de la tiranía en que vivían. Este Salvador llegó por fin con el nombre de Lugh, hijo de Kian y nieto de Balor. Fue gracias a Lugh que los danaanos se enfrentaron a los fomorianos en una gran batalla y terminaron derrotándolos. En esta batalla perdieron la vida Nuada, el de la Mano de Plata y Balor, el del Ojo Diabólico. Para matar a Balor, Lugh tuvo que esperar a que el gran ojo se cerrara para lanzarle una piedra que se incrustó en su cerebro. http://mitosyleyendascr.com/mitologia-celta/<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p style='font-size:70%'><br>Los Aluxe Nos encontrábamos en el campo yermo donde iba a hacerse una siembra. Era un terreno que abarcaba unos montículos de ruinas tal vez ignoradas. Caía la noche y con ella el canto de la soledad. Nos guarecimos en una cueva de piedra, y para bajar utilizamos una soga y un palo grueso que estaba hincado en el piso de la cueva. La comida que llevamos nos la repartimos. ¿Qué hacía allá?, puede pensar el lector. Trataba de cerciorarme de lo que veían miles de ojos hechizados por la fantasía. Trataba de ver a esos seres fantásticos que según la leyenda habitaban en los cuyo (montículos de ruinas) y sementeras: Los ALUXES. Me acompañaba un ancianito agricultor de apellido May. La noche avanzaba…De pronto May tomó la Palabra y me dijo: -Puede que logre esta milpa que voy a sembrar. -¿Por qué no ha de lograrla?, pregunté. -Porque estos terrenos son de los aluxes. Siempre se les ve por aquí. ¿Está seguro que esta noche vendrán? Seguro, me respondió. -¡Cuántos deseos tengo de ver a esos seres maravillosos que tanta influencia ejercen sobre ustedes! Y dígame, señor may ¿usted  les ha visto? -Explíqueme, cómo son, qué hacen. El ancianito, asumiendo un aire de importancia, me dijo: Por las noches, cuanto todos duermen, ellos dejan sus escondites y recorren los campos; son seres de estatura baja, niños, pequeños, pequeñitos, que suben, bajan, tiran piedras, hacen maldades, se roban el  fuego y molestan con sus pisadas y juegos. Cuando el humano despierta y trata de salir, ellos se alejan, unas veces por pares, otras en tropel. Pero cuando el fuego es vivo y chispea, ellos le forman rueda y bailan en su derredor; un pequeño ruido les hace huir y esconderse, para salir luego y alborotar más. No son seres malos. Si se les trata bien, corresponden. -¿Qué beneficio hacen? -Alejan los malos vientos y persiguen las plagas. Si se les trata mal, tratan mal, y la milpa no da nada, pues por las noches roban la semilla que se esparce de día, o bailan sobre las matitas que comienzan a salir. Nosotros les queremos bien y le regalamos con comida y cigarrillos. Pero hagamos silencio para ver si usted logra verlos. El anciano salió, asiéndose a la soga, y yo tras él, entonces vi que avivaba el fuego y colocaba una jicarita de miel, pozole cigarrillos, etc., y volvió a la cueva. Yo me acurruqué en el fondo cómodamente. La noche era espléndida, noche plenilunar. Transcurridas unas horas, cuando empezaba a llegarme el sueño, oí un ruido que me sobresaltó. Era el rumor de unos pasitos sobre la tierra de la cueva: Luego, ruido de pedradas, carreras, saltos, que en el silencio de la noche se hacían más claros.  http://mitosyleyendascr.com/mexico/mexico42/ <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "textosLargos": "si",
            "t11pregunta": "Lee los escritos y arrastra el tipo de texto según corresponda."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Sancionar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sancionó",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Sancionaría",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Interrumpir",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Interrumpido",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Interruptor",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Escupió",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Escupir",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Escupitajo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Portará",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Portería",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Notificaremos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Notificar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Noticia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Obligado",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Oblicuo",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 3,
            "t11pregunta": "Observa los modos y tiempos verbales y selecciona las palabras que son adecuadas para elaborar un reglamento:"
        }
    },
   {
       "respuestas": [
           {
               "t13respuesta": "Mapa Conceptual",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Mapa Mental",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Esquema de llaves",
               "t17correcta": "1"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "1",
           "t11pregunta": "<p>Selecciona el título correspondiente a la imagen \n\
                               <div style='text-align:center;'>\n\
                                   <img src='SI7E_B01_N02_01.png'></div></p>"
       }
   },
   {
       "respuestas": [
           {
               "t13respuesta": "Esquema de llaves",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Mapa Conceptual",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "Mapa Mental",
               "t17correcta": "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "1",
           "t11pregunta": "<p>Selecciona el título correspondiente a la imagen \n\
                               <div style='text-align:center;'>\n\
                                   <img src='SI7E_B01_N02_02.png'></div></p>"
       }
   },
   {
       "respuestas": [
           {
               "t13respuesta": "Esquema de llaves",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Mapa Conceptual",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Mapa Mental",
               "t17correcta": "1"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "1",
           "t11pregunta": "<p>Selecciona el título correspondiente a la imagen \n\
                               <div style='text-align:center;'>\n\
                                   <img src='SI7E_B01_N02_03.png'></div></p>"
       }
   },
   {
       "respuestas": [
           {
               "t13respuesta": "Para escribir mí biografía",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Recopilar información sintetizada",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "Para presentarlas en lugar de un examen",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Apoyo en exposiciones orales",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "Para que los profesores reconozcan mi esfuerzo",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Como herramienta al estudiar para los exámenes",
               "t17correcta": "1"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "2",
           "t11pregunta": "Elige todas las opciones que indique ¿Qué uso les puedes dar a las fichas de trabajo?"
       }
   },
   {
       "respuestas": [
           {
               "t13respuesta": "<p>Henderson, N., Milstein, M.<\/p>",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "<p>(2005).<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p><i>Resiliencia en la escuela</i><\/p>",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "<p>Argentina:<\/p>",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "<p>Editorial Paidós<\/p>",
               "t17correcta": "5"
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p><center><br>Ordena los elementos para citar de acuerdo a APA.<br><\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p><br><\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p><br><\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p><br><\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p><br><\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p><br><\/p></center>"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "respuestasLargas": true,
           "ocultaPuntoFinal": true,
           "t11pregunta": "Ordena los elementos para citar de acuerdo a APA:"
       }
   }
]