json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Se emplea en situaciones específicas como cuando nos comunicamos con alguine extraño o que no es cercano a nosotros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se puede utilizar en situaciones casuales donde hablamos o escribimos a personas que conocemos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Lo utilizamos con familiares y amigos donde nos comunicamos algo que no necesita de un protocolo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al dirigirnos a una autoridad, en lo académico o cuando realizamos una presentación para expresarnos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se utiliza un mayor vocabulario, con pronunciación adecuada y sin muletillas o modismos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Si nos encontramos en una conversación con nuestros compañeros o amigos, lo utilizamos, sin preocuparnos del todo en lo que decimos.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las características que nos hablan de un lenguaje formal"
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>Primera persona<\/p>",
                "t17correcta": "1,4"
            },
           {
                "t13respuesta": "<p>Tercera persona<\/p>",
                "t17correcta": "2,3,5"
            },
            {
                "t13respuesta": "<p>Tercera persona<\/p>",
                "t17correcta": "3,2,5"
            },
            {
                "t13respuesta": "<p>Primera persona<\/p>",
                "t17correcta": "4,1"
            },
            {
                "t13respuesta": "<p>Tercera persona<\/p>",
                "t17correcta": "5,2,3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Puede ser singular o plural, donde se demuestra un compromiso por parte del autor hacia el lector, utilizando palabras propias o directas.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Sucede cuando el autor cita la información de otra persona o bien menciona alguna frase o dato que no extrajo directamente sino que tomó de otro autor.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Demuestra poco compromiso con lo que se dice, ya que se le está atribuyendo lo dicho a otra persona o grupo de personas.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Sucede cuando la persona dice “yo” (en singular) o “nosotros” (en plural), o habla desde una perspectiva personal.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;El autor, en este caso, no asume las consecuencias de lo mencionado sino que lo encarga a quien identifica.<br/><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>Uso de distintas familias tipográficas<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Numerales<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Subtítulos o apartados<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Letras<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Viñetas<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: emplear distintos tipos de letras y tamaños ayuda a diferenciar segmentos y apartados de texto. Asimismo, se emplean las negritas y subrayados para enfatizar algún verbo u enunciado que tenga mucha importancia.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: (1, 2, 3…) se utilizan para organizar los apartados o capítulos de un formulario. Sirven para marcar orden y coherencia.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: Organizan las normas, deberes, derechos o sanciones según el tipo de actividad, procedimiento; funciones de miembros involucrados; etcétera. Los subtítulos o apartados permiten que el lector ubique rápidamente una sección que sea de su interés sin leer todo el reglamento.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: (a, b, c) se emplean para numerar o enlistar requisitos o procedimientos.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: clasifican grupos o aspectos de una misma categoría.<br/><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "respuestasLargas" : true
        }
    },
 {
        "respuestas": [
            {
                "t13respuesta": "<p>a. de C<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Av.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>blvd.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>diag.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>dcho.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>dcha.<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>d. de C.<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>dpto.<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Distrito Federal<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>Distrito Federal<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>etc.<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>izdo.<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>izda.<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>long.<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>pág.<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>p. ej.<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>plza.<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>ppal.<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>temp.<\/p>",
                "t17correcta": "18"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["Antes de Cristo", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Avenida", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Bulevar", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Diagonal", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Derecho", "201,551", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Derecha", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Después de Cristo", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Departamento", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["D. F.", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Etcétera", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Izquierdo", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Izquierda", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Longitud", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Página", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Por ejemplo", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Plaza", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Principal", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Temperatura", "201,417", "cuadrado", "134, 73", "."]},
        ]
 },
    {
        "respuestas": [
            {
                "t13respuesta": "Completar<br/>Escribir<br/>Enviar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Mencione<br/>No utilice<br/>Envia",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Recibirá<br/>Escribió<br/>Firme",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Diseñe<br/>Modifique<br/>Será notificado",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Modo infinitivo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Modo imperativo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Modo indicativo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Modo subjuntivo"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    }
]
