[  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Conquistar",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Tolerancia",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Convivir",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Abusar",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Comunicar",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Respeto",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Humillar",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Compartir",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Subestimar",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Mostrar empatía",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Valorar las diferencias",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Disposición a aprender",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":2,
         "t11pregunta":"Señala las actitudes y acciones necesarias para que diversas culturas coexistan en armonía."
      }
   },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Riqueza",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Cultura",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Diversidad",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Pluricultural",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"En nuestro país conviven diferentes tradiciones culturales, desarrolladas por varias etnias o grupos poblacionales. ¿Cuál es el término que define lo anterior?"
      }
   },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Identifica las imágenes que presentan distintas manifestaciones culturales de origen mexicano."
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hombres no cocinan.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las mujeres son tan buenas para manejar como los hombres.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hombres son incapaces de cuidar a los hijos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una mujer sin esposo no puede ser feliz.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hombre y mujeres tienen los mismos derechos y obligaciones.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Papá puede hacerse cargo de la casa y mamá salir a trabajar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hombres no lloran.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"La falta de conocimiento",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"La falta de equidad",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La intolerancia",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Las diferencias económicas",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Las fronteras",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Considerarse superior a los demás",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"La pobreza",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columas":2,
         "t11pregunta":"Una lluvia de ideas es una t\u0026eacute;cnica grupal que consiste en"
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>respeto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>solidaridad<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>cuidarse<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>frente<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>relación<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>dignidad<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>valor<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>persona<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>todos<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>colaboras<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>sociedad<\/p>",
                "t17correcta": "11"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>El buen trato aparece vinculado al <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y es la capacidades que tienen las personas, para <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; entre ellas y hacer <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; a las necesidades propias y ajenas, manteniendo siempre una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; afectiva y de amor. La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de cada <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y que <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; poseemos. Al ser respetuoso y solidario <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; para que nuestra <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; tenga bienestar y calidad de vida.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>cacao<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>atole<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>chipotle<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>chicle<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>México<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Zopilote<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Epazote<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Coyote<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Zacate<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mecate<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Finlandés",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Hebreo",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Lenguaje de señas mexicano",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Lituano",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Español",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Vasco",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Las 68 lenguas indígenas originarias de México",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Gales",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Gaélico irlandés",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Elige todas las lenguas que se hablan en México."
      }
   },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Nuestra cultura",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"La labor policiaca",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Los símbolos patrios",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"El himno nacional",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Poder comprar lo que quiero",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Nuestras tradiciones",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"La inseguridad",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"El tránsito en viernes",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Nuestros políticos",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Elige todo aquello que te hace sentir orgulloso de ser mexicano. El gobierno"
      }
   }
]