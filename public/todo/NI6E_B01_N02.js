json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Uso de condón<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Abstinencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Postergar el inicio de la actividad sexual<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Evaluar los pros y contras antes de iniciar una vida sexual activa.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tomar elecciones responsables y asumir las consecuencias<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tener múltiples parejas sexuales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dejarte llevar el impulso sexual.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sexo sin condón.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Inicio temprano de la actividad sexual.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No tomar tiempo para evaluar las consecuencias de iniciar tu vida sexual.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada contenedor según corresponda.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Conductas sexuales responsables",
            "Conductas sexuales riesgosas"
        ]
    },
  {
      "respuestas": [
          {
              "t13respuesta": "ni6e_b01_n02_02.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "ni6e_b01_n02_03.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "ni6e_b01_n02_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "ni6e_b01_n02_05.png",
              "t17correcta": "1",
              "columna":"1"
          },
          {
              "t13respuesta": "ni6e_b01_n02_06.png",
              "t17correcta": "4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra a la imagen y ordena los elementos que integran el cuerpo humano.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"ni6e_b01_n02_01.png",
          "respuestaImagen":true, 
          "bloques":false, 
          "tamanyoReal" : true
          
      },
      "contenedores": [
          {"Contenedor": ["", "205,400", "cuadrado", "155, 47", "."]},
          {"Contenedor": ["", "265,335", "cuadrado", "155, 47", "."]},
          {"Contenedor": ["", "325,250", "cuadrado", "155, 47", "."]},
          {"Contenedor": ["", "385,150", "cuadrado", "155, 47", "."]},
          {"Contenedor": ["", "445,75", "cuadrado", "155, 47", "."]}
      ]
  },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Padre y madre aportan a las células de su hijo la misma cantidad de información genética.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los hijos heredan las características genéticas del padre o madre con un carácter más fuerte.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cada padre aporta toda la información sobre cada una de las características que tendrá su hijo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Existen genes que dominan sobre otros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Las características físicas de los hijos tienen que ver con la alimentación de la madre durante la gestación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>En algunos casos las características de los padres se mezclar dan resultados distintos ya que contienen la información genética de ambos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Todas las características genéticas son heredadas de uno solo de los padres.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La dominancia de los genes tiene que ver con la fuerza de los mismos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al formarse el cigoto ya contiene toda la información genética de los padres.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala todo lo que tenga que ver con la trasmisión de información genética."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Refrescos y líquidos procesados con alta concentración de azúcar",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Agua natural",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Compone nuestro cerebro en un 75%.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ácido fosfórico que contienen inhibe la absorción del calcio.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Colabora para llevar nutrientes y oxígeno a las células.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Contienen un ph ácido que mancha y daña el esmalte de los dientes.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Regula la temperatura corporal.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Contienen muchas calorías.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Confirma el 83 % de nuestra sangre.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ayuda a eliminar desechos.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No se recomienda su consumo o hacerlo ocasionalmente y en cantidades muy pequeñas.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Humedece el oxígeno que respiramos.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Favorece el sobrepeso y obesidad.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Colabora para amortiguar las articulaciones.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Marca en cada columna la opción que corresponda.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Contagio de virus de papiloma humano<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Súbita subida de peso<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Contagio de virus de inmunodeficiencia humana<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Burlas de los compañeros<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Contagio de diversas enfermedades de trasmisión sexual<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Embarazo no deseado<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona cuáles son los posibles riesgos al tener relaciones sexuales sin protección."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Es una campaña que recomienda elegir las bebidas saludables para consumir diariamente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Su escala es de 6 niveles y propone una categorización de las bebidas que debemos consumir para mantener la salud.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son una serie de recetas que explican la elaboración de refrescos en casa.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>En su nivel 1 Recomienda beber de 6 a 8 vasos de agua potable al día.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>En su nivel 1 recomienda la ingesta diaria de bebidas con azúcar añadida y bajo contenido de nutrimentos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>En su nivel 6 explica que no se recomienda el consumo de bebidas con azúcar añadido y escaso contenido de nutrientes.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las oraciones relacionadas con la jarra del buen beber."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Sedentarismo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Practicar ejercicio al menos 30 minutos diariamente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Consumo de alcohol y tabaco.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Alimentación rica en carbohidratos, grasas y baja en fibra.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Llevar una alimentación balanceada y alta en fibra.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Antecedentes de un familiar con cáncer de mama.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Disminuir el consumo de azúcares y grasas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Usar anticonceptivos hormonales por más de 5 años.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Realizar auto-exploraciones mamas cada mes.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Relaciona ambas columnas según corresponda.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Factores de riesgo para cáncer de mama",
            "Medidas preventivas al cáncer de mama."
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>responsable<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>prevención<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>sexualidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>enfermedad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>infección<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>papiloma<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>contagio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>sistemas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>celulas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>organos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tejidos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>condón<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>cancer<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>salud<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>mama<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Localiza en la sopa de letras 16 palabras relacionadas con tus aprendizajes en este bloque."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Eligiendo comida rápida y con alta concentración de grasas y azucares.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Eligiendo comer saludablemente y de acuerdo a las actividades que realizas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Peleando con tus compañeros y familia diariamente.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Evitando los alimentos de alto contenido calórico, altos en azúcares y grasas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Eligiendo beber agua natural todos los días.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evitar el consumo de golosinas o comida chatarra.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Practicando ejercicio, por lo menos 30 minutos diarios.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Descuidando tus dientes y evitando acudir al dentista.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Descansando adecuadamente; es decir, dormir cada noche por lo menos 8 horas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cuidando tu limpieza y ejerciendo tu derecho a la salud.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pasando cada fin de semana en la cama practicando en tus videojuegos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Decidiendo ser feliz.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las oraciones que hablen de qué manera puedes acceder a un estilo de vida saludable."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sólo las células delos sistemas reproductivos tienen genes.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ADN contiene el material genético.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En nuestro cuerpo existen 5 tipos de células distintas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nuestro cuerpo está formado por millones de células.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nuestras células realizan diferentes funciones.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las células de nuestro cuerpo realizan las mismas funciones.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ADN es una sustancia que se encuentra en el interior de la célula.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las células de un ser humano tienen los mismos genes.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La información genética se trasmite de padres a hijos por el ovulo y espermatozoide.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona las respuestas correctas.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }      
    }
]