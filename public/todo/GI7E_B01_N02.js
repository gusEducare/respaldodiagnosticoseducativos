json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>espacio geográfico<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>economicos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>políticos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>sociales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>naturales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>culturales<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Objeto de estudio de la Geografía; es toda la superficie que habilitamos los seres humanos integrado por diferentes componentes."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se refieren a la producción y distribución de productos , bienes y servicios, la agricultura, ganadería, explotación petrolera."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Sistemas de gobierno de cada país y sus diferentes partidos político, participación ciudadana, leyes, división territorial."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Integrado por los grupos humanos que habitan el mundo y sus diferentes formas de organizacion y distribuirse. Distintas razas. "
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Lo integran las dinámica ambientales de la Tierra: huracanes, terremotos, erupciones, montañas, lagos, mares, etc."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se refieren a las tradiciones y costumbres, formas de vida, creencias religiosas, lenguas de cada grupo social o etnia."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 9,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 7,
                "datoY": 7
            },
            {
                "direccion": 1,
                "datoX": 13,
                "datoY": 2
            },
            {
                "direccion": 0,
                "datoX": 12,
                "datoY": 3
            },
            {
                "direccion": 0,
                "datoX": 2,
                "datoY": 9
            },
            {
                "direccion": 0,
                "datoX": 4,
                "datoY": 13
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 20,
            "ancho": 20,
            "t11pregunta": "Completa el siguiente crucigrama: "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p style='font-size:75%'>Es el espacio geográfico que nos rodea.<\/p>",
                "t17correcta": "0,0"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Genera un sentido de pertenencia e identidad con sus habitantes.<\/p>",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Puede ser rural o urbano, según sus elementos y tipo de interacción con el medio.<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Los  grupos humanos interactúan con componentes naturales para su desarrollo económico.<\/p>",
                "t17correcta": "1,1"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Relación del relieve, clima, suelo, flora, fauna y grupos humanos. <\/p>",
                "t17correcta": "0,2"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Es natural o cultura. Predominan elementos naturales poco modificados por el ser humano.<\/p>",
                "t17correcta": "1,2"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Espacio homogéneo identificable y único en relación a otros espacios.<\/p>",
                "t17correcta": "0,3"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>En él interactúan varios componentes que lo hacen único.<\/p>",
                "t17correcta": "1,3"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Suelo, subsuelo, espacio aéreo, mar territorial y patrimonial, y recursos delimitados por frontera.<\/p>",
                "t17correcta": "0,4"
            },
            {
                "t13respuesta": "<p style='font-size:75%'>Tiene extensión variada, delimitada por la organización política y administrativa.<\/p>",
                "t17correcta": "1,4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Coloca el tipo de espacio y característica relacionados con las categorías de análisis espacial."
        },
        "contenedores": [
            "Tipo de espacio",
            "Característica"

        ],
        "contenedoresFilas": [
            "Lugar",
            "Medio",
            "Paisaje",
            "Región",
            "Territorio"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>mapas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>local<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>nacional<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>mundial<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>escala<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>El espacio geográfico se representa en&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que pueden estar en una escala&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, nacional o mundial. Por ejemplo, la escala local nos permite obtener detalles de las características topográficas de un lugar y una&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;nos permitiría apreciar por ejemplo las actividades económicas de nuestro país y, si por ejemplo, quisiéramos analizar el recorrido de un huracán por el océano necesitaríamos utilizar una escala&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;señala el número de veces que se ha reducido o aumentado una superficie; estas son consideradas pequeñas ya que de ser mayores resultaría impráctico y difícil representarlas en un pliego de papel.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente párrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Uno es a mil<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uno es a diez mil<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Uno es a cien mil<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Uno es a un millón<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><table style='background-color:#3D6F9C;'>\n\
                                    <tr>\n\
                                        <td width='150px' style='color:white; background-color:#3d85c6; font-size:18px;'><b>Representación numérica</b></td>\n\
                                        <td width='200px' style='color:white; background-color:#3d85c6; font-size:18px;'><b>Se Lee</b></td>\n\
                                        <td width='200px' style='color:white; background-color:#3d85c6; font-size:18px;'><b>Se Lee</b></td>\n\
                                        <td width='200px' style='color:white; background-color:#3d85c6; font-size:18px;'><b>Se Lee</b></td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='text-align: center; style=background-color:#cfe2f3; font-size:16px;'>1:1 000</td>\n\
                                        <td style='text-align: center'> "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td style='background-color:white; text-align:center; font-size:16px;'>Uno es igual a mil</td>\n\
                                        <td style='background-color:white; text-align:center; font-size:16px;'>Mil corresponde a uno</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>1: 10 000</td>\n\
                                        <td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>Diez mil corresponde a uno</td>\n\
                                        <td style='background-color:#cfe2f3; style=text-align: center'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>Uno es igual a mil</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='background-color:white; text-align:center; font-size:16px;'>1: 100 000</td>\n\
                                        <td WIDTH=30 style='text-align: center'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "\
                                        </td>\n\
                                        <td WIDTH=50 style='text-align: center; font-size:16px;'>Uno es igual a mil</td>\n\
                                        <td WIDTH=50 style='text-align: center; font-size:16px;'>Cien mil corresponde a uno</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>1:1 000 000</td>\n\
                                        <td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "\
                                        </td>\n\
                                        <td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>Un millón corresponde a uno</td>\n\
                                        <td style='background-color:#cfe2f3; text-align:center; font-size:16px;'>Uno es igual a 1</td>\n\
                                    </tr>\n\
                                </table>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona la respuesta que corresponda de la forma como se leen las siguientes escalas.",
            "pintaUltimaCaja": false,
            "respuestasLargas": true,
            "ocultaPuntoFinal": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>numérica<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>gráfica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>tónica<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>cromática<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>térmica<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La escala <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; se expresa mediante una fracción que indica la cantidad de veces que ha sido reducida la superficie representada en el mapa.<br>La escala <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es una línea dividida en segmentos, cada uno representa 1 centímetro y expresan los kilómetros de la superficie representada en el mapa.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "contieneDistractores": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Opcion 1",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opcion 2",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Opcion 3",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Opcion 4",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p style='font-size:90%'>En una escala  1:2000; 1 centímetro representa:<\/p>",
                "valores": ['50m', '20km', '20m', '2000km'],
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p style='font-size:90%'>En una escala 1:3 000 000; 1 centímetro representa:<\/p>",
                "valores": ['20km', '20m', '2000m', '20m'],
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p style='font-size:90%'>Para poder apreciar el recorrido que podría llegar a tener un huracán, debes utilizar un mapa con la escala:<\/p>",
                "valores": ['1:1000000', '1:1000', '1: 100', '1:10'],
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p style='font-size:90%'>La distancia real en kilómetros que separa a dos países que están separados por 30 centímetros en una escala de 1:5000 000 es:<\/p>",
                "valores": ['1.5km', '15 km', '150km', '1,500km'],
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p style='font-size:90%'>Los kilómetros que corresponden 20 centímetros en un mapa a escala: 1:20 000 son:<\/p>",
                "valores": ['2km', '4km', '6km', '10km'],
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Medición",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Planisferio<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Antípoda<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Zona Horaria<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Equidistante<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Mapamundi<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>&nbsp;</div><img src=\"GI7E_B01_N02_01_01.png\" style='width:200px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>&nbsp;</div><img src=\"GI7E_B01_N02_01_02.png\" style='width:200px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>&nbsp;</div><img src=\"GI7E_B01_N02_01_03.png\" style='width:250px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>&nbsp;</div><img src=\"GI7E_B01_N02_01_04.png\" style='width:200px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>&nbsp;</div><img src=\"GI7E_B01_N02_01_05.png\" style='width:200px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Une las imágenes con las siguientes palabras."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Línea imaginaria que divide a la Tierra en dos hemisferios.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es el paralelo 0º, de origen y su círculo mide 40 075 km.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Greenwich es el punto de referencia para marcar la distancia del Este u Oeste.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Van de norte a sur y convergen en los polos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Determina el cambio de día y fecha.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Conocido como Línea Internacional del Tiempo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dividen a la tierra en hemisferio oriental y occidental<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son representativos: trópico de Cáncer y de Capricornio.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Establecen límites en zonas frías y templadas de la Tierra<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Disminuyen de tamaño al alejarse del ecuador<\/p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a los contenedores, los datos que correspondan con los siguientes conceptos.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Ecuador",
            "Meridiano",
            "Paralelo"
        ]},
    {
        "respuestas": [
            {
                "t13respuesta": "<p>altitud<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>(msnm)<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>debajo<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>positiva<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>negativa<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>grados<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>(”)<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>terrestre<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;es una distancia de un punto terrestre medida desde el nivel del mar (0 metros); esta nos permite saber a cuántos metros sobre el nivel del mar&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;el nivel del mar encuentra un lugar (mbnm).  Si un punto se encuentra por encima del nivel del mar se dice que la altitud es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, en cambio si se encuentra debajo del nivel del mar la altitud será&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br> La longitud y la altitud se miden en&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> (°), minutos (') y segundos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Con estas tres coordenadas podremos ubicar un lugar en el mapa&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras para completar la siguiente información."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lo que ocasiona que existan el día y la noche es el movimiento de traslación de la Tierra.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los meridianos son el punto de partida para establecer las distintas zonas horarias.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las 24 horas del día se dividieron en 360º dando como resultado que una hora fuera igual a 360º/24 horas= 15º.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los 15º son la separación entre cada paralelo y cada grado representan cuatro minutos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Línea Internacional del cambio del tiempo se encuentra en el océano atlántico y tiene un trazo regular para impedir que en un mismo lugar tenga fechas distintas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen países, que por su extensión o distribución geográfica, tienen varios husos horarios.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona falso o verdadero según sea el caso.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi7e_b01_n02_02_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi7e_b01_n02_02_03.png",
                "t17correcta": "1",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra la imagen al contenedor que le corresponde.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi7e_b01_n02_02_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": true

        },
        "contenedores": [
            {"Contenedor": ["", "152,73", "cuadrado", "150, 150", ".", "transparent"]},
            {"Contenedor": ["", "152,374", "cuadrado", "150, 150", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Proporciona información sobre posicionamiento, navegación, cronometría, latitud, longitud y altitud.</center></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Software para la visualización de datos en un mapa y que son empleados en la planificación y gestión.</center></p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra la definición al contenedor que le corresponde.",
            "tipo": "vertical"
        },
        "contenedores": [
            "GPS",
            "SIG"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las imágenes satelitales nos brindan información inmediata.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con los SIG podemos obtener información para planear y tomar decisiones relacionadas con la infraestructura.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los GPS nos permiten tener información sobre la calidad del aire dentro de una ciudad.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen muchas aplicaciones que usan las imágenes satelitales como Google Maps y Waze.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los GPS pueden calcular distancias entre dos puntos, delimitar fronteras y brindarnos imágenes precisas de un lugar.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los SIG permiten delimitar las fronteras que existen entre los países del mundo.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero o falso según sea el caso<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true
        }
    }
];