json = [
       {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EProceso que permite la reproducción asexual de las plantas donde un solo ser vivo a partir de un trozo de sí mismo puede generar uno idéntico.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EProceso que permite la reproducción sexual de las plantas en que intervienen algunos animales, viento y agua, mediante el cual el polen llega al pistilo para llevar a cabo la fecundación.\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué es la polinización?"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Tulipán<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ajo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>jengibre<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>papa<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>cebolla<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>fresa<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Guayaba<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>jitomate<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>aguacate<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>rosa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>mandarina<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>manzana<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Reproducción asexual",
            "Reproducción sexual"
        ]
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>sexual<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>macho<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>hembra<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>procrear<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>nacimiento<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Ovíparos<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>huevo<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Toda reproducción animal es de tipo &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;; es decir necesariamente participan &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;y  &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;de la especie para &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;un organismo con genes de ambos.<br><br>Por su tipo de &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;se les clasifica en: <br><br>&nbsp;&nbsp;&nbsp;- <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;siendo aquellos que luego de la fecundación se desarrollan dentro &nbsp;&nbsp;&nbsp;de un&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;hasta estar listos para nacer.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true, 
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>anfibios<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>aves<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>insectos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>cocodrilo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>abeja<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Vivíparos<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>dentro<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>madre<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>caballo<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>delfín<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>mamíferos<\/p>",
                "t17correcta": "11"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br>&nbsp;&nbsp;Entre ellos se encuentran &nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;, reptiles,&nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;,peces e &nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;por ejemplo: avestruz, &nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;, víbora, &nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;, rana, trucha.&nbsp;<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;son quienes desde la fecundación se desarrollan&nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;de la&nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;, algunos ejemplos son: perro, &nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;, oveja &nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;, y león. Todos estos animales se cuentan entre los &nbsp;"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",	
       	    "respuestasLargas":true, 
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Hojas verdes que protegen a la flor durante su desarrollo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<br>Conjunto de sépalos.<br><br>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Hojas de color que atraen a los insectos para la polinización.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "corola"
            },
            { 
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "pistilo"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "pétalos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<br>Conjunto de sépalos.<br><br><br>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<br>Órganos masculinos de la flor que producen el polen.<br><br>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Órgano femenino de la flor en el que se encuentran los óvulos para que se lleve a cabo la fecundación que permitirá la producción de frutos y semillas.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Sépalos"
            },
            { 
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Estambres"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "caliz"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tienen raíz, tallos, hojas, flores.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Solo visibles con microscopio.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se forman de hifas y micelio.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Se reproducen por semillas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Están por todos lados.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No producen su propio alimento.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Producen su propio alimento.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se reproducen por división de sus cuerpos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No son plantas.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Absorbe nutrientes del medio.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Producen esporas.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Pueden beneficiar o perjudicar al hombre.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se hacen de nutrientes mediante absorción.<\/p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Plantas",
            "Bacterias",
            "Hongos"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>habitantes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tipo de suelo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>plantas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>relieve<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>clima<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>temperatura<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>insectos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>belleza del paisaje<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Son los factores que integran el medio físico de los ecosistemas. Elige todas las respuestas correctas"
        }
    },{
         "respuestas": [
            {
                "t13respuesta": "<p>temperatura<\/p>",
                "t17correcta": "0"
            },{
                "t13respuesta": "<p>ecosistema<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>nutrimentos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>equilibrio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>terrestres<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>acuáticos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>bosque<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>aire<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida": 16,
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>interacción<\/p>",
                "t17correcta": "0"
            },{
                "t13respuesta": "<p>desierto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>luzsolar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>suelo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>selva<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>agua<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>mar<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida": 16,
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "Productores",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Consumidores primarios o herbívoros",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Consumidores secundarios",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Consumidores terciarios",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Desintegradores",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<img src=''>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<img src=''>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<img src=''>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<img src=''>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<img src=''>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Introducir especies de otros ecosistemas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hacer santuarios para animales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Contaminación.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evitar la pérdida de especies.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Caza desmedida.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Deforestación.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cuidar los osos polares.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todas las acciones que rompen el equilibrio de los ecosistemas."
        }
    }
]

