json = [
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_04.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 3,
 	  		"valor":1000
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_03.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 1,
 	  		"valor":500
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_02.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 2,
 	  		"valor":200
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_01.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":100
 	  	},
 	  	{
 	  		"t13respuesta": "cincuenta.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":50
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la cantidad mostrada.<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url": "MI4E_B01_N01_01_06.png",
 	  		"respuestaImagen": true,
 	  		"anchoImagen": 60,
 	  		"bloques": false,
 	  		"posicionamiento": "descendente",
 	  		"respuestaUnicaMultiple": true,
 	  		"filas": 1,
 	  		"borde":false,
 	  		"renglones": 1,
 	  		"contenedorContador":true
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "180,159", "cuadrado", "235, 164", "."]}
 	  	], 
 	  	"css":{
 	  		"posicionIndicadorValor":{top:107, left:26},
 	  		"anchoRespuestas":200
 	  	}
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_04.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 2,
 	  		"valor":1000
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_03.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":500
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_02.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 2,
 	  		"valor":200
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_01.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":100
 	  	},
 	  	{
 	  		"t13respuesta": "cincuenta.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 1,
 	  		"valor":50
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la cantidad mostrada.<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url": "MI4E_B01_N01_01_06.png",
 	  		"respuestaImagen": true,
 	  		"anchoImagen": 60,
 	  		"bloques": false,
 	  		"posicionamiento": "descendente",
 	  		"respuestaUnicaMultiple": true,
 	  		"filas": 1,
 	  		"borde":false,
 	  		"renglones": 1,
 	  		"contenedorContador":true
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "180,159", "cuadrado", "235, 164", "."]}
 	  	], 
 	  	"css":{
 	  		"posicionIndicadorValor":{top:107, left:26},
 	  		"anchoRespuestas":200
 	  	}
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_04.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 4,
 	  		"valor":1000
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_03.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 1,
 	  		"valor":500
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_02.png",
 	  		"t17correcta": "1",
 	  		"clones": 9,
 	  		"numeroCorrectas": 0,
 	  		"valor":200
 	  	},
 	  	{
 	  		"t13respuesta": "MI4E_B01_N01_01_01.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 1,
 	  		"valor":100
 	  	},
 	  	{
 	  		"t13respuesta": "cincuenta.png",
 	  		"t17correcta": "0",
 	  		"clones": 9,
 	  		"numeroCorrectas": 1,
 	  		"valor":50
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la cantidad mostrada.<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url": "MI4E_B01_N01_01_06.png",
 	  		"respuestaImagen": true,
 	  		"anchoImagen": 60,
 	  		"bloques": false,
 	  		"posicionamiento": "descendente",
 	  		"respuestaUnicaMultiple": true,
 	  		"filas": 1,
 	  		"borde":false,
 	  		"renglones": 1,
 	  		"contenedorContador":true
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "180,159", "cuadrado", "235, 164", "."]}
 	  	], 
 	  	"css":{
 	  		"posicionIndicadorValor":{top:107, left:26},
 	  		"anchoRespuestas":200
 	  	}
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02a.png",
 	  		"t17correcta": "0",
 	  		"columna":"0"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02f.png",
 	  		"t17correcta": "1,2,3",
 	  		"columna":"0"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02g.png",
 	  		"t17correcta": "2,3,1",
 	  		"columna":"1"
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url":"esp_kb1_s01_a01_01.png",
 	  		"respuestaImagen":true, 
 	  		"bloques":false
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,162", "cuadrado", "78, 81", "."]}
 	  	]
 	  },
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02a.png",
 	  		"t17correcta": "0",
 	  		"columna":"0"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02f.png",
 	  		"t17correcta": "1,2,3",
 	  		"columna":"0"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02g.png",
 	  		"t17correcta": "2,3,1",
 	  		"columna":"1"
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url":"esp_kb1_s01_a01_01.png",
 	  		"respuestaImagen":true, 
 	  		"bloques":false
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,162", "cuadrado", "78, 81", "."]}
 	  	]
 	  },
 
 	{
 		"respuestas": [
 			{
 				"t13respuesta": "<p><sup>1</sup>&frasl;<sub>3</sub><\/p>",
 				"t17correcta": "1"
 			},
 			{
 				"t13respuesta": "<p><sup>1</sup>&frasl;<sub>6</sub></p>",
 				"t17correcta": "2"
 			},
 			{
 				"t13respuesta": "<p><sup>4</sup>&frasl;<sub>6</sub></p>",
 				"t17correcta": "3"
 			}
 		],
 		"preguntas": [
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "<p>a) Dos pasteles se dividieron a la mitad. Tres personas tomaron una mitad cada uno.</p><br><img src=''><br><table></tr><tr><td>¿Qué fracción de la última mitad le corresponde a cada persona?</td><td>"
 			},
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "</td></tr><tr><td>¿Qué fracción de un pastel le tocó a cada uno en la segunda repartición?</td><td>"
 			},
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "</td></tr><tr><td>¿Qué fracción de un pastel le tocó a cada uno en total?</td><td>"
 			},
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "</td></tr></table>"
 			}
 		],
 		"pregunta": {
 			"c03id_tipo_pregunta": "8",
 			"t11pregunta": "Resuelve los siguientes problemas."
 		}
 	},
 	{
 		"respuestas": [
 			{
 				"t13respuesta": "<p><sup>1</sup>&frasl;<sub>2</sub><\/p>",
 				"t17correcta": "1"
 			},
 			{
 				"t13respuesta": "<p><sup>1</sup>&frasl;<sub>6</sub></p>",
 				"t17correcta": "2"
 			},
 			{
 				"t13respuesta": "<p><sup>3</sup>&frasl;<sub>6</sub></p>",
 				"t17correcta": "3"
 			}
 		],
 		"preguntas": [
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "<p>b) Un pastel se dividió en tres partes. Dos personas tomaron un pedazo cada quien.</p><br><img src=''><br><table></tr><tr><td>¿Qué fracción de la parte que sobró le corresponde a cada persona?</td><td>"
 			},
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "</td></tr><tr><td>¿Qué fracción de un pastel le tocó a cada uno en la segunda repartición?</td><td>"
 			},
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "</td></tr><tr><td>¿Qué fracción de un pastel le tocó a cada uno en total?</td><td>"
 			},
 			{
 				"c03id_tipo_pregunta": "8",
 				"t11pregunta": "</td></tr></table>"
 			}
 		],
 		"pregunta": {
 			"c03id_tipo_pregunta": "8",
 			"t11pregunta": "Resuelve los siguientes problemas."
 		}
 	},
 	  {  
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EVerdadero\u003C\/p\u003E",
 	  		"t17correcta":"1"
 	  	},
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EFalso\u003C\/p\u003E",
 	  		"t17correcta":"0"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"13.50 + 6.40 = 19.90"
 	  	}
 	  },
 	  {  
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EVerdadero\u003C\/p\u003E",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EFalso\u003C\/p\u003E",
 	  		"t17correcta":"1"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"22.60 + 4.40 = 26.00"
 	  	}
 	  },
 	  {
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EVerdadero\u003C\/p\u003E",
 	  		"t17correcta":"1"
 	  	},
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EFalso\u003C\/p\u003E",
 	  		"t17correcta":"0"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"8.20 + 2.80 = 11.00"
 	  	}
 	  },
 	  {
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EVerdadero\u003C\/p\u003E",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EFalso\u003C\/p\u003E",
 	  		"t17correcta":"1"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"9.70 + 6.80 = 15.00"
 	  	}
 	  },
 	  {
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EVerdadero\u003C\/p\u003E",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":"\u003Cp\u003EFalso\u003C\/p\u003E",
 	  		"t17correcta":"1"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"3",
 	  		"t11pregunta":"15.30 + 10.80 = 15.10"
 	  	}
 	  },
 	  {  
 	  	"respuestas":[  
 	  	{  
 	  		"t13respuesta":     "11 formas diferentes.",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":     "12 formas diferentes.",
 	  		"t17correcta":"1"
 	  	},
 	  	{  
 	  		"t13respuesta":     "14 formas diferentes.",
 	  		"t17correcta":"0"
 	  	},
 	  	{  
 	  		"t13respuesta":     "15 formas diferentes.",
 	  		"t17correcta":"0"
 	  	}
 	  	],
 	  	"pregunta":{  
 	  		"c03id_tipo_pregunta":"1",
 	  		"dosColumnas": true,
 	  		"t11pregunta":"<p style='margin-bottom: 5px;'>¿De cuántas maneras diferentes se puede elegir los dos primeros caminos del laberinto? <div  style='text-align:center;'><img style='height: 360px' src='S03_A01_01.png'></div></p>"
 	  	}
 	  },
	/*** Actividad 5 ***/
 	  {
 	  	"respuestas": [
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02a.png",
 	  		"t17correcta": "0",
 	  		"columna":"0"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02f.png",
 	  		"t17correcta": "1,2,3",
 	  		"columna":"0"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02g.png",
 	  		"t17correcta": "2,3,1",
 	  		"columna":"1"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02h.png",
 	  		"t17correcta": "3,1,2",
 	  		"columna":"1"
 	  	},
 	  	{
 	  		"t13respuesta": "esp_kb1_s01_a01_02i.png",
 	  		"t17correcta": "4,5,6",
 	  		"columna":"0"
 	  	}
 	  	],
 	  	"pregunta": {
 	  		"c03id_tipo_pregunta": "5",
 	  		"t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
 	  		"tipo": "ordenar",
 	  		"imagen": true,
 	  		"url":"esp_kb1_s01_a01_01.png",
 	  		"respuestaImagen":true, 
 	  		"bloques":false
 
 	  	},
 	  	"contenedores": [
 	  	{"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,162", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,243", "cuadrado", "78, 81", "."]},
 	  	{"Contenedor": ["", "201,324", "cuadrado", "78, 81", "."]}
 	  	]
 	  },
	/*** Actividad 6 ***/
	  {
	  	"respuestas": [
	  	{
	  		"t13respuesta": "<p>Equilátero<\/p>",
	  		"t17correcta": "1"
	  	},
	  	{
	  		"t13respuesta": "<p>Rectángulo<\/p>",
	  		"t17correcta": "2"
	  	},
	  	{
	  		"t13respuesta": "<p>Obtusángulo<\/p>",
	  		"t17correcta": "3"
	  	},
	  	{
	  		"t13respuesta": "<p>Escaleno<\/p>",
	  		"t17correcta": "4"
	  	},
	  	{
	  		"t13respuesta": "<p>Isóceles<\/p>",
	  		"t17correcta": "5"
	  	},
	  	{
	  		"t13respuesta": "<p>Acutángulo<\/p>",
	  		"t17correcta": "6"
	  	}
	  	],
	  	"preguntas": [
	  	{
	  		"c03id_tipo_pregunta": "8",
	  		"t11pregunta": "<p><br><img src=\"rtp_z_s01_a01_a.png\" style='width:300px'><div></div><\/p>"
	  	},
	  	{
	  		"c03id_tipo_pregunta": "8",
	  		"t11pregunta": "<p><br><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'><div></div><\/p>"
	  	},
	  	{
	  		"c03id_tipo_pregunta": "8",
	  		"t11pregunta": "<p><br><img src=\"rtp_z_s01_a01_c.png\" style='width:300px'><div></div><\/p>"
	  	},
	  	{
	  		"c03id_tipo_pregunta": "8",
	  		"t11pregunta": "<p> <\/p>"
	  	},

	  	],
	  	"pregunta": {
	  		"c03id_tipo_pregunta": "8",
	  		"pintaUltimaCaja": false,        
	  		"textosLargos": "si",
	  		"contieneDistractores": true,
	  		"t11pregunta": "Completa las oraciones con el adjetivo interrogativo que corresponda. Arrastra las palabras al lugar que les corresponde. Atiende los acentos en cada caso para responder correctamente la actividad."
	  	}
	  }

]