json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Automóvil \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Caminar \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Bicicleta \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál de los medios de transporte es el que consume menos combustible? "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E a = 3; b = -4; c = 5 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E a = -3; b = 4; c = -5  \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E a = 3; b = -4; c = 5  \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "De acuerdo con la forma de la ecuación cuadrática ax<sup>2</sup> + bx + c = 0; identifica los coeficientes a, b y c en la ecuación: 3x<sup>2</sup> = 4x - 5;"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Error \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'>  \u003C\/p\u003E",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Señala cuál es la expresión correcta de la fórmula general para resolver una expresión cuadrática:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E  paralelos \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E proporcionales \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E perpendiculares\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas": true,
            "t11pregunta": "Los triángulos del triángulo A'B'C' son: ________ a los lados del triángulo ABC, lo que se traduce en la fórmula:<br><br>\n\
                                        <div style='text-align:center;'>\n\
                                            <span class='fraction'><span class='top'>A'B'</span><span class='bottom'>AB</span></span><sup><sup>=</sup></sup>\n\
                                            <span class='fraction'><span class='top'>B'C'</span><span class='bottom'>BC</span></span><sup><sup>=</sup></sup>\n\
                                            <span class='fraction'><span class='top'>A'C'</span><span class='bottom'>AC</span></span>\n\
                                            <br><br><img src='.png'>\n\
                                        </div>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E  <img src=''> \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src=''> \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src=''> \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Identifica cuál de las siguientes gráficas corresponde a la gráfica de una función cuadrática:"
        }
    }
]