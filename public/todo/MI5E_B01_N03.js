json = [      
      {
        "respuestas": [
            {
                "t13respuesta": "<img src='mi5_b01_03_01_01.png'>",                
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='mi5_b01_03_01_03.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='mi5_b01_03_01_05.png'>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='mi5_b01_03_01_07.png'>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<img src='mi5_b01_03_01_02.png'>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<img src='mi5_b01_03_01_04.png'>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<img src='mi5_b01_03_01_06.png'>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<img src='mi5_b01_03_01_08.png'>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona cada operación con su resultado."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>304<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>302<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>306<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<div style='display:inline'><table class='customTableDiv'><tr><td></td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td></td><td></td></tr><tr><td>8</td><td class='tdDiv' colspan='3'rowspan='2'>2 432</td></tr></table></div><div style='display:inline'><table class='customTableDiv'><tr><td></td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td></td><td></td></tr><tr><td>1</td><td class='tdDiv' colspan='3'rowspan='2'>3 624</td></tr><tr><td>2</td></tr></table><div style='display:inline'><table class='customTableDiv'><tr><td></td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td></td><td></td></tr><tr><td>8</td><td class='tdDiv' colspan='3'rowspan='2'>2 448</td></tr></table></div><script> $('.customTableDiv').css({border: '0', background:'transparent', 'padding-left':'50px', display:'inline'}); $('.customTableDiv td').css({width:'20px', height:'40px', background:'#f2f2f2', 'font-size':'24px'}); $('.customTableDiv .tdDiv').css({'border-top':'1px solid black','border-left':'1px solid black', 'vertical-align':'top'});</script>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal":true,
            "soloTexto":true,
            "respuestasLargas":true,
            "anchoRespuestas":80,
            "t11pregunta": "Sin resolver las divisiones, coloca los cocientes en el lugar que les corresponde."
        }
    },   
 {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dos rectas secantes son rectas perpendiculares cuando los ángulos que forman son rectos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dada una recta y un punto de ella, la forma de trazar 180 rectas diferentes que resulten secantes a la primera y pasen por el punto es con ayuda de un transportador, trazando dos rectas por cada grado.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Responde “falso” o “verdadero” a las siguientes preguntas.<br/><br/><img src='MI5E_B01_N03_01_01.png'><br/><br/>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
   {  
      "respuestas":[ 
         {  
            "t13respuesta":"Norte",
            "numeroPregunta":"1",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Sur",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Este",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Oeste",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Norte",
            "numeroPregunta":"2",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Sur",
            "numeroPregunta":"2",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Este",
            "numeroPregunta":"2",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Oeste",
            "numeroPregunta":"2",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Noreste",
            "numeroPregunta":"3",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Sureste",
            "numeroPregunta":"3",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Noroeste",
            "numeroPregunta":"3",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Suroeste",
            "numeroPregunta":"3",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Noroeste",
            "numeroPregunta":"4",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Suroeste",
            "numeroPregunta":"4",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Noreste",
            "numeroPregunta":"4",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Sureste",
            "numeroPregunta":"4",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":1,
         "preguntasMultiples":true,
         "t11pregunta":["Observa el mapa y determina en qué dirección, respecto al Palacio de Bellas Artes, se encuentran los siguientes lugares.<br/><img src='mi5e_b01_n03_02.png'>","Teatro blanquita","Catedral metropolitana","Plaza de Santo Domingo","Club de banqueros"]
      }
   },
  {
        "respuestas": [
            {
                "t13respuesta": "MI5E_B01_N03_03_01.png",
                "t17correcta": "0",
                "clones": 6,
                "numeroCorrectas": 5
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca en el contenedor los elementos necesarios para obtener la cantidad indicada.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "MI5E_B01_N03_03_04.png",
            "respuestaImagen": true,
            //"anchoImagen": 60, // porcentaje del div principal
            "bloques": false,
            "posicionamiento": "descendente",
            "respuestaUnicaMultiple": true,
            "filas": 3,
            "borde": false,
            "renglones": 2

        },
        "contenedores": [
            {"Contenedor": ["", "180,150", "cuadrado", "340,300", "."]}
        ],
    },
  {
        "respuestas": [
            {
                "t13respuesta": "MI5E_B01_N03_03_02.png",
                "t17correcta": "0",
                "clones": 4,
                "numeroCorrectas": 3
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca en el contenedor los elementos necesarios para obtener la cantidad indicada.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "MI5E_B01_N03_03_03.png",
            "respuestaImagen": true,
            //"anchoImagen": 60, // porcentaje del div principal
            "bloques": false,
            "posicionamiento": "descendente",
            "respuestaUnicaMultiple": true,
            "filas": 3,
            "borde":false,
            "renglones": 1

        },
        "contenedores": [
            {"Contenedor": ["", "180,150", "cuadrado", "340,300", "."]}
        ],
    }
]
