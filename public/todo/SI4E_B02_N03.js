json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tema debe ser delimitado y preciso.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Relata hechos fantásticos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es un texto anónimo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debe desarrollar aspectos importantes y relevantes acerca del tema.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Trata temas de la vida cotidiana.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En general es extensa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La redacción debe estar hecha con un lenguaje claro.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debe ser objetiva, es decir, el investigador no puede emitir opiniones sobre el tema.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La investigación debe respaldarse con fuentes bibliográficas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona falso o verdadero de acuerdo a las características que debe tener una monografía.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "Aquel que tiene un poco de conocimiento parecerá sabio si está rodeado de ignorantes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Evitar dar opiniones si no eres parte del asunto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Los grupos sociales a los que pertenecemos tienen una influencia en nosotros.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "En tierra de ciegos, el tuerto es rey."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Dime con quién andas y te diré quién eres."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Los mirones son de palo."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona cada refrán con su mensaje correspondiente."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Primero licúa por cinco minutos todos los ingredientes.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Coloca el sartén en la estufa a fuego medio.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Con el sartén caliente, pon un poco de mezcla en el centro.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mueve el sartén en círculos para expandir la mezcla.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Cocina a fuego lento hasta que la mezcla tenga burbujas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Dale la vuelta al hotcake para que se cocine del otro lado.<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Espera que se cocine al gusto.<\/p>",
                "t17correcta": "6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p><h4>Arrastra las frases y ordena de manera lógica los pasos para preparar hotcakes.</h4></p><p><b>Instrucciones para preparar hotcakes, 3 raciones.</b></p><p>Ingredientes:</p><p><ol><li>Una taza de harina de trigo.</li><li>1/2 taza de avena.</li><li>Una taza de leche.</li><li>Una cucharada de vainilla.</li><li>Un huevo</li><li>3 cucharadas de mantequilla derretida.</li></ol></p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 6", "201,617", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 7", "201,751", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mapa conceptual se construye sólo con imágenes.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se utilizan figuras geométricas para encerrar cada idea.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un mapa conceptual es realmente extenso.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Utilizan palabras que funcionan como enlaces.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Presenta las ideas principales de un tema y las relaciones que se establecen entre ellos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Invariablemente utiliza flechas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Identifica si el enunciado es falso o verdadero.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Este<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Esos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Esa<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aquello<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Rojo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ansioso<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Corto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pequeñito<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Primero<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Cuádruple<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Diez<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Último<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mío<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Sus<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Nuestros<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Suyos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Nunca<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Poco<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Sobre<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Siempre<\/p>",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Coloca cada palabra en el cuadro que corresponda.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Adjetivos demostrativos",
            "Adjetivos calificativos",
            "Adjetivos numerales",
            "Adjetivos posesivos",
            "Adverbios"
        ]
    },
];