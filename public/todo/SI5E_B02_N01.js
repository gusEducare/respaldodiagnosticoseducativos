json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Relata hechos naturales, sobrenaturales o mezclados.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Siempre desarrollan elementos fantásticos y sobrenaturales, sin ninguna base histórica.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Entre sus temas pueden estar héroes de la historia, seres mitológicos o algún suceso importante.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Siempre  se desarrollan en lugares y tiempos imaginarios.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hacen referencia al lugar y tiempo a la que pertenecen y suelen tener referencias históricas concretas. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Su función es la divulgación científica.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sus personajes se basan en personas reales aunque llevadas a la ficción, modificando sus características físicas y psicológicas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Forman parte del folclore de una cultura.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aun cuando su origen sea muy antiguo, jamás se modifican.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Su función se limita a contar relatos; sus temas son variados; es por esto que tienen un carácter didáctico y de moraleja.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Carecen de toda veracidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se han modificado a través del tiempo, debido a que en su origen fueron transmitidas de forma oral de generación en generación.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Iniciaron con el desarrollo de la imprenta.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aun cuando tiene una base histórica desarrolla elementos fantásticos y sobrenaturales.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala los enunciados que muestran las características y funciones de las leyendas."
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Introducción<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Desarrollo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Conclusión<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Reflexión, Opinión, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, Resumen, fichas bibliográficas.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal":true,
            "t11pregunta": "Arrastra y ordena en el contenedor los elementos que debe tener un texto expositivo."
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Leer el texto para conocer su contenido.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Extraer citas del autor para incorporarlas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Leer brevemente y sin análisis la información. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Extraer información que sirva para elaborar tu propio texto.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Identificar las ideas principales de cada párrafo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No profundizar demasiado en la búsqueda de información.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Identificar los aspectos que abarca dicho texto.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Utilizar varias fuentes de información y compararlas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Realizar tu propio texto de manera que sea completo, innovador y coherente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Reproducir en tu texto lo que dice el autor aun cuando no le des crédito.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala todo lo que deberás tomar en cuenta para obtener información relevante  en un texto."
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Al inicio de cada escrito.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Después de un punto y seguido o un punto y aparte.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>En todos los nombres propios.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Después de una coma.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al escribir nombres religiosos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al escribir un sobrenombre.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al escribir el título de una obra, libro o  película.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al escribir todos los verbos en infinitivo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al escribir títulos de autoridades.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al escribir números romanos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cuando los artículos son de una sílaba.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al escribir nombres de Instituciones.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al escribir los nombres de las distintas ciencias.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las situaciones donde debes usar mayúsculas."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>de lugar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>de tiempo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>de modo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>ilativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>comparativo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>consecutivo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>copulativo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>causal<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>adversario<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>condicional<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>final<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>disyuntivo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>concesivo<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Identifica y arrastra el tipo de nexo que corresponde a cada grupo.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Nexos coordinantes",
            "Nexos subordinantes"
        ]
    }
]