json=[
    {
        "respuestas": [
            {
                "t13respuesta": "a<sup>2</sup>+3",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "a<sup>2</sup>+9",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "4a+6",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "a<sup>2</sup>+3a+2",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>En una escuela se piensa construir un auditorio con la distribución que se muestra en la figura. ¿Qué expresión algebraica representa el área del piso del auditorio completo?<div  style='text-align:center;'><img style='height: 360px' src='mi9e_b01_n02_01.png'></div></p>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "De largo 12 m y de ancho 11m.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "De largo 1 m y de ancho 21m.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "De largo 13 m y de ancho 12m.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "De largo 10 m y de ancho 20m.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Si el área del cuadrado verde es de 132 m2 ¿cuánto mide de ancho y cuánto mide de largo el rectángulo completo?<div  style='text-align:center;'><img style='height: 360px' src='MI9E_B01_N02_02.png'></div></p>"
        }
     },
     {
      "respuestas": [
          {
              "t13respuesta": "mi9e_b01_n02_03_02.png",
              "t17correcta": "0,2",
              "columna":"0"
          },
          {
              "t13respuesta": "mi9e_b01_n02_03_03.png",
              "t17correcta": "1,3",
              "columna":"0"
          },
          {
              "t13respuesta": "mi9e_b01_n02_03_04.png",
              "t17correcta": "2,0",
              "columna":"1"
          },
          {
              "t13respuesta": "mi9e_b01_n02_03_05.png",
              "t17correcta": "3,1",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"mi9e_b01_n02_03_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "tamanyoReal":true,
          "opcionesTexto":true,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "141,115", "cuadrado", "181,111", ".","transparent"]},
          {"Contenedor": ["", "141,349", "cuadrado", "181,111", ".","transparent"]},
          {"Contenedor": ["", "310,115", "cuadrado", "181,111", ".","transparent"]},
          {"Contenedor": ["", "310,349", "cuadrado", "181,111", ".","transparent"]}
      ]
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E5 m\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E30 m\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E13 m\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E10 m\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Cuánto mide el lado de un cuadrado que tiene por área 169 m<sup>2</sup>?"
      }
    },
    {
      "respuestas": [
          {
              "t13respuesta": "mi9e_b01_n02_04_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "mi9e_b01_n02_04_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "mi9e_b01_n02_04_04.png",
              "t17correcta": "2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"mi9e_b01_n02_04_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "tamanyoReal":true,
          "opcionesTexto":true,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["true", "337,323", "cuadrado", "255,45", ".","transparent"]},
          {"Contenedor": ["", "384,323", "cuadrado", "255,45", ".","transparent"]},
          {"Contenedor": ["", "431,323", "cuadrado", "255,45", ".","transparent"]}
      ]
  },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E<img src='MI9E_B01_N02_05_02.png'>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<img src='MI9E_B01_N02_05_03.png'>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<img src='MI9E_B01_N02_05_04.png'>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa las siguientes figuras y selecciona la figura que contenga triángulos que no son semejantes:"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"$30",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"$50",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"$40",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"$60",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>La siguiente gráfica representa el ingreso por venta de cuadernos en una papelería conforme se aproxima el inicio del curso escolar. ¿Cuál es el costo de cada cuaderno? <div  style='text-align:center;'><img style='height: 360px' src='mi9e_b01_n02_06.png'></div></p>"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>6</span><span class='bottom'>4</span></span>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>3</span><span class='bottom'>10</span></span>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>1</span><span class='bottom'>10</span></span>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>2</span><span class='bottom'>10</span></span>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "columnas":2,
         "t11pregunta":"Para no despertar a su hermana por las mañanas, Aidé se viste sin prender la luz. En su cajón tiene 3 pares de calcetas blancas, 4 azules y 3 negras. ¿Cuál es la probabilidad de sacar un par de calcetas negras?"
      }
   }, {  
      "respuestas":[  
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>4</span><span class='bottom'>100</span></span>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>9</span><span class='bottom'>100</span></span>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>3</span><span class='bottom'>100</span></span>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<span class='fraction'><span class='top'>2</span><span class='bottom'>100</span></span>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "columnas":2,
         "t11pregunta":"Arturo, Bruno y Héctor son los integrantes del equipo 1 y Carlos, Luis y Karla son los integrantes del equipo 2. Para la rifa de una tableta, Karla compró 3 boletos, Luis 1, Héctor 2 , Carlos 1, Bruno 2 y Arturo ninguno. ¿Cuál es la probabilidad de que la tableta quede en el equipo 1 si en total se vendieron 100 boletos?"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Los representantes de grupo de cada clase.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Un alumno de cada grado.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Cuatro alumnos y cuatro alumnas de cada grupo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Los padres de familia.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Los padres de familia.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas":2,
         "t11pregunta":"Laura y sus amigos están formando una planilla para representar a todos los alumnos de la escuela secundaria. Entre las propuestas que quieren presentar está: organizar un convivio para fin de año; pero existe desacuerdo dentro de la planilla: algunos opinan que se realice una kermés temática, otros proponen que sea una posada y otros más quieren una kermés tradicional. Para que la propuesta de la planilla de Laura sea la más atractiva, ella decide realizar una encuesta. Si desea tener información confiable, la muestra la pueden formar:"
      }
   }
]