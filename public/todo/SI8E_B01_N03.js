json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E¿Qué es la marihuana medicinal?\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E10 motivos por los que no es 'cool' fumar marihuana\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Lee los textos e identifica su tipo de texto arrastra al contenedor el título de cada uno donde corresponda.",
            "tipo": "vertical"
        },
        "contenedores": [
            "Textos expositivos",
            "Textos argumentativos"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Pero</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sin embargo</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Entre otros</p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Aunque</p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Aun</p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>A pesar de</p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Se utiliza para enlazar dos oraciones o frases que se contraponen una a otra.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Puede interpretarse como 'sin sirva de impedimento'. Es decir, cuando se presenta un problema o barrera pero que no va a impedir que se reañice la acción.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Se usa para cerrar enumeraciones incompletas. Puede sustituir o sustituirse por etcétera.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Expresa dos acciones opuestas una a la otra. Presenta un impedimento y una acción a tomar en cuenta.</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Es una palabra que puede sustituir o sustituirse por 'hasta', 'incluso' y/o 'tambien.'</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Se utiliza para decir que se logró o logrará algo aunque exista oposición o resistencia.</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Racionales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "De hecho",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ejemplo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Emocionales",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>El 2014 fue el año con la mayor incidencia de cáncer de mama en las mexicanas al registrar 28.75 nuevos casos por cada 100 mil mujeres de 20 años y más, reveló el Instituto Nacional de Estadística y Geografía (Inegi).</p>",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>El cáncer de mama es una de las enfermedades que no hacen distinción entre la población de países desarrollados y en desarrollo y es el que tiene mayor presencia en las mujeres a nivel mundial.</p>",
                "correcta": "0"
            },
             {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Según la dependencia, se ha detectado una serie de factores que contribuyen al riesgo de padecerlo. Entre ellos destacan: el tabaquismo, una dieta rica en grasas animales y ácidos grasos trans, así como niveles elevados de estrógeno en la sangre.</p>",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>¡Mujer, ámate y auto explórate! Que el cáncer no te aleje de tus hijos.</p>",
                "correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Señala el tipo de argumento al que se recurre:<\/p>",
            "descripcion": "Argumento",
            "variante": "editable",
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "si8e_b01_n03_06.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "si8e_b01_n03_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "si8e_b01_n03_04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "si8e_b01_n03_03.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "si8e_b01_n03_05.png",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Analiza las imágenes y arrastra los datos donde corresponden para crear la ficha bibliográfica.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "SI8E_B01_N03_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "borde": false,
            "tamanyoReal": true

        },
        "contenedores": [
            {"Contenedor": ["", "77,60", "cuadrado", "230,74", ".", "transparent", true]},
            {"Contenedor": ["", "77,352", "cuadrado", "230,74", ".", "transparent", true]},
            {"Contenedor": ["", "201,60", "cuadrado", "230,74", ".", "transparent", true]},
            {"Contenedor": ["", "201,354", "cuadrado", "230,74", ".", "transparent", true]},
            {"Contenedor": ["", "328,60", "cuadrado", "230,74", ".", "transparent", true]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Título del relato leído.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Breve biografía del autor del relato.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Síntesis del texto.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evaluación cuantitativa del relato.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Características de personajes, ambiente y lenguaje utilizado.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Opinión personales sobre el relato leído.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La narración de los motivos del autor para escribir el relato.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Toca todos los elementos de un comentario literario."
        }
    }
]