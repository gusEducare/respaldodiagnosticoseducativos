json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nuestros abuelos y padres son las generaciones  que están más relacionadas con la hiperconectividad. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La hiperconectividad es un estado de vida que refleja cómo el ser humano está conectado constantemente a la tecnología y los dispositivos móviles.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Internet es un medio exclusivo para los jóvenes que desean comunicarse y acortar distancias. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No necesariamente, los jóvenes utilizan los medios tecnológicos para su beneficio, ya que pueden correr riesgos si no son cuidadosos con la información que proporcionan en herramientas como las que brinda Internet. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con herramientas como el chat o servicios de mensajería instantánea, es muy común que nos alejemos de la gente que está cerca de nosotros y nos acerque con las personas que están lejos. ",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false,
            "anchoColumnaPreguntas": 70
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Permiten un acercamiento con las personas que se encuentran lejos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Agilizan procesos y permiten hacer varias cosas a la vez.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pueden llegar a entorpecer la comunicación con la gente a nuestro alrededor.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pueden existir abusos o engaños que pueden publicarse a través de las redes sociales. ",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las relaciones afectivas y sentimentales son básicas para el desarrollo del ser humano. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La atracción sexual da desde los primeros años de vida del hombre. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El noviazgo debe ser una etapa grata que fortalezca el conocimiento de ti mismo y de tus gustos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Como parte de la adolescencia, las amistades que tienes, se convierten en relaciones de noviazgo. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para poder resolver conflictos en una relación de pareja, es necesario tomar en cuenta los intereses mutuos y las responsabilidades que conllevan este tipo de relaciones.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Falta de autoestima",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Figuras corporales no naturales",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comunicación con los padres",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Consumo excesivo de productos light",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Búsqueda de pérdida rápido de peso",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las causas que pueden influir para que se den los trastornos alimenticios en los adolescentes"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>psicológica<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Física<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Verbal<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Sexual<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Violencia&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Se manifiesta con insultos, manipulación, amenazas o humillaciones. No se observa a simple vista pues no deja rastros de agresión, sin embargo deja huellas profundas en la autoestima. <br>Violencia&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;Se manifiesta con golpes, bofetadas, pellizcos en alguna o varias partes del cuerpo.<br>Violencia&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Se manifiesta con insultos, palabras ofensivas, apodos o ridiculización de la persona. Puede estar presente o combinarse con otro tipo de violencia. <br>Violencia&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Se manifiesta con hostigamiento y abusos para lograr prácticas sexuales sin ejercicio de la libertad. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra el tipo de violencia que corresponde a la siguiente información."
        }
    }
]