json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Relatan algo verdadero (situación,  lugar o persona que existió).<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Suelen incluir personajes fantásticos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pueden incluir rasgos característicos y únicos de una cultura, como una costumbre o tradición.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Contienen exageraciones en las que les atribuyen poderes o características irreales a los personajes de los que se habla.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se ubican en un contexto histórico, es decir, parten de un hecho o época específica real.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Incluyen elementos sobrenaturales que exagerar la historia.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Identifica las características que denotan ficción o veracidad en las leyendas y  arrastralas al contenedor que correspondan.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Veracidad",
            "Ficción"
        ]
    },
       {
        "respuestas": [
            {
                "t13respuesta": "Presenta el contenido y desarrolla cada aspecto   del tema a tratar, aquí se detallan procesos, ejemplifican    situaciones y explican conceptos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Plantea el tema y el objetivo del texto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se elabora una síntesis de lo más relevante del tema.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Desarrollo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Introducción"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Conclusión"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona cada elemento del texto expositivo con su contenido."
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "Se desarrolla la historia o evento noticioso. Incluye datos que aportan mayor información al lector.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es una palabra o frase corta que está por encima del título en letra más pequeña. Sirve par identificar acerca de qué trata la nota.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Palabra o enunciado que resume la nota periodística.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Es el primer párrafo de la nota y contiene la parte más relevante. En él se responde: qué ocurrió, quién fue o estuvo ahí, cuándo, dónde, cómo y por qué.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Oración que va debajo del título y que aporta información extra. Su función es llamar la atención del lector para que continúe leyendo la nota.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cuerpo de la nota"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Epigrafe o antetítulo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Título"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Introducción o lead"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Subtítulo"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los elementos de una nota periodística con su descripción."
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Permiten enriquecer la información de nuestra investigación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Refieren a información dicha por un tercero.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dan crédito al autor o autores de los que se extrajo la información.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se copia de manera exacta cada palabra leída.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aportan seriedad y formalidad a nuestros trabajos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Siempre debemos colocarlas entre comillas: “ ”.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respaldan nuestras opiniones.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Deben incluir el nombre y apellido del autor además de los datos del texto o publicación de la que fueron  extraídas.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada contenedor la información sobre las citas bibliográficas, según corresponda.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Funciones",
            "Características"
        ]
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Hiperbole<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Metafora<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Coherencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Comparación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Muletilla<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es un recurso literario que consiste en exagerar las cualidades de una persona o cosa."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Consiste en la sustitución de un elemento por otro con el cual tiene similitud."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Presenta los hechos o la información siguiendo una estructura lógica: inicio, desarrollo y cierre."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se utiliza para mostrar relaciones de semejanza entre dos o más elementos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Palabra o frase que se repite mucho por hábito, en ocasiones llega al extremo de ya no poder decir frase alguna sin ella."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 5,
                "datoY" : 0
            },
            {
                "direccion" : 0,
                "datoX" : 0,
                "datoY" : 6
            },
            {
                "direccion" : 0,
                "datoX" : 2,
                "datoY" : 8
            },
            {
                "direccion" : 1,
                "datoX" : 11,
                "datoY" : 4
            },
            {
                "direccion" : 0,
                "datoX" : 6,
                "datoY" : 12
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 17,
            "t11pregunta": "Resuelve el crucigrama."
        }
    }
];