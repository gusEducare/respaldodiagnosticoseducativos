json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Marco teórico<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Materiales<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hipótesis<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Objetivos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Bibliografía<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Resultados<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cuadros, tablas y gráficas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Conclusión<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Procedimiento o metodología<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Introducción",
            "Desarrollo",
            "Cierre"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Subordinación<\/p>",
                "t17correcta": "1,2"
            },
            {
                "t13respuesta": "<p>Yuxtaposición<\/p>",
                "t17correcta": "2,5"
            },
            {
                "t13respuesta": "<p>Subordinación<\/p>",
                "t17correcta": "3,1"
            },
            {
                "t13respuesta": "<p>Coordinación<\/p>",
                "t17correcta": "4,6"
            },
            {
                "t13respuesta": "<p>Yuxtaposición<\/p>",
                "t17correcta": "5,2"
            },
            {
                "t13respuesta": "<p>Coordinación<\/p>",
                "t17correcta": "6,4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><br/><br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Oraciones formadas por otras oraciones simples pero que dependen una de la otra para tener un sentido completo.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Oraciones que se unen con signos de puntuación.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Son oraciones que no tienen un fin completo si no están acompañadas de otras.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Son dos oraciones simples que no dependen entre ellas para tener un significado completo.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Oraciones que cobran un sentido distinto al que tendrían si se omitieran los signos de puntuación.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Oraciones que contienen dos oraciones simples unidas por un mismo sentido pero que pueden funcionar una sin la otra.<br/><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "respuestasLargas": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Se identifican por nexos como: así que, por lo tanto, luego, por consiguiente, con que, tan, tanto, tal, etc.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Expresan la causa de porqué sucedió algo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se identifican por nexos como: porque, pues, a causa de que, puesto que, ya que, etc.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Expresan la consecuencia de haber realizado alguna acción principal.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Expresan una condición necesaria para que cumpla o suceda algo.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Los nexos que utilizan son: si, como, cuando, a menos que, siempre que, con tal de que, etc.<\/p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Oraciones compuestas causales",
            "Oraciones compuestas consecutivas",
            "Oraciones compuestas condicionales"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El investigador entregó los resultados en tiempo y forma.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pueden ser de temas variados, pero que sean relevantes para el lector.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los investigadores realizadas no han dado los resultados esperados.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>En las muestras tomadas no hubo el número de participantes necesarios.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>¡Está listo!<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se desarrolló sin problema alguno.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Forma impersonal"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>La investigación fue realizada por los expertos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los resultados fueron muy claros.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Muchas de las investigaciones son muy buenas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las gráficas son presentadas en el informe con mucha precisión.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La muestra no fue representativa para el estudio.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La presentación fue ofrecida ante una gran grupo de investigadores.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Voz pasiva"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Renacimiento fue un movimiento que permeó a toda Europa, pero que se generó en Florencia.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Renacimiento es conocido como una época fundamental y de transición entre la Edad Media y la Revolución Industrial.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En este movimiento se busca dejar a un lado la idea que planteaba el teocentrismo donde Dios era el centro de la realidad y de todo el pensamiento y actividad humana.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cultura que se retomó fue la clásica grecolatina porque realzaba ideales como la belleza y la armonía.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La literatura renacentista se acerca al adoctrinamiento medieval, donde el eje didáctico y la guía de la cultura es la Iglesia.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Italianismos<\/p>",
                "t17correcta": "1,5"
            },
            {
                "t13respuesta": "<p>Arabismos<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Galicismos<\/p>",
                "t17correcta": "3,6"
            },
            {
                "t13respuesta": "<p>Latinismos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Italianismos<\/p>",
                "t17correcta": "5,1"
            },
            {
                "t13respuesta": "<p>Galicismos<\/p>",
                "t17correcta": "6,3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Novela, soneto, monseñor, embajada, parco.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Lechuga, almohada, manada, físico.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Amarrar, jardín, chambrana, cable.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Ofuscar, volumen, silvestre, terrible.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Carnaval, camposanto, banco, cambio.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;Corchete, alijar, brida, forjar.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>diversidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>inclusión<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>derechos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>cultura<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>intercambiar<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>tolerantes<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>apertura<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;cultural tiene como eje principal el respeto y la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de todas las culturas del mundo garantizando los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;humanos de cada persona independientemente de la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;a la que pertenezca. Si todos fuéramos iguales no habría manera de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;conocimiento ni de enriquecer experiencias. Conocer culturas diferentes a la nuestra nos enseña a ser <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y a comprender el entorno bajo múltiples enfoques.<br/>Además del respeto y la tolerancia, la diversidad cultural implica una apertura a la preservación, promoción  y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;a cada cultura existente.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Por tratarse de un medio donde permite que muchas personas puedan acceder a él,  no es necesario saber leer o escribir para recibir la información.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se puede apoyar en aspectos visuales para que el oyente no pierda el interés en la transmisión, ya que la mayoría de la gente es principalmente visual.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es prácticamente igual de rápido que el internet y su transmisión es inmediata, por lo que es un medio muy eficaz para compartir.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tipo de contenidos, no puede ser clasificado ya que todo mundo tiene acceso a la radio.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al ser un medio tan popular, cualquier persona puede ser locutor de radio, sin importar cómo es la forma en la que habla.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Informar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Entretener<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Educar<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Hablar sobre las noticias del día, o de alguna situación que se esté presentando en el momento o que vaya a suceder en el futuro.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Muchas personas acceden a la radio para divertirse o distraerse de la vida cotidiana. Así que es común que en las horas de mayor tráfico, los programas de radio presenten contenido de entretenimiento, para descansar después de un día de trabajo o estudio.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Algunos programas dedican secciones de contenido novedoso, poco conocido o de mucho interés para que los oyentes tengan acceso a aprender a través de lo que escuchan.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    }
]