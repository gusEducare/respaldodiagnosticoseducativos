json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Carbohidratos</p>",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Proteínas</p>",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Lípidos</p>",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Vitaminas</p>",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Minerales.</p>",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "<p>Carbohidratos</p>",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Proteínas</p>",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Lípidos</p>",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Vitaminas</p>",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Minerales.</p>",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "<p>Carbohidratos</p>",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Proteínas</p>",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Lípidos</p>",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Vitaminas</p>",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Minerales.</p>",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<p>Carbohidratos</p>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Proteínas</p>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Lípidos</p>",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Vitaminas</p>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Minerales.</p>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<p>Carbohidratos</p>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<p>Proteínas</p>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<p>Lípidos</p>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<p>Vitaminas</p>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<p>Minerales.</p>",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Son necesarios para el crecimiento, reparación de tejidos, función inmunológica, y los encontramos en alimentos como la carne, leche, pollo, etc.",
                "Son la principal fuente de energía para el funcionamiento del cuerpo y lo encontramos en leguminosas frutas, papas, pasta, etc.",
                "Son necesarias para obtener energía de los carbohidratos y para el fortalecimiento del sistema inmunológico, y las encontramos en frutas y verduras.",
                "Son necesarios para la estructura de las células, para la regulación de la temperatura corporal y la producción de hormonas. Los encontramos en la mantequilla, carne, aguacate, pescado, etc.",
                "Son necesarios para el funcionamiento de los músculos, para fortalecer los huesos y para el transporte de oxígeno en la sangre. Los encontramos en leguminosas leche y verduras."],
            "preguntasMultiples": true,
            "columnas": 2
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Vegetales</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Frutas</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cereales</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Leguminosas</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Alimentos de origen animal</p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra los alimentos y agrupa de acuerdo a la cantidad que puedes consumir diariamente.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Abundantes",
            "Moderados",
            "Pocos"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Lo encontramos en la guayaba, kiwi, mango, piña, cítricos y aumenta la producción de interferón. Es necesaria para formar colágeno.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se encuentra en el aceite de germen de trigo, soja, cereales, aceite de oliva y aumenta la respuesta inmunológica.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "La encontramos en el hígado, mantequilla, huevo y lácteos completos. Representa un papel esencial en las infecciones y en el mantenimiento de la superficie de las mucosas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Lo encontramos en el hígado, carne, pescado, huevo y su déficit es frecuente, afectando a jóvenes y embarazadas, disminuye la respuesta inmunológica.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Vitamina C"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Vitamina E"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Vitamina A"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Hierro"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "No hace daño",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No tiene sabor",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "No tiene color",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "No tiene olor",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "inocua"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "insípida"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Incolora"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Inodora"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Estrógenos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Insulina",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Adrenalina",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Testosterona",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Hormona del crecimiento",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Gonadorfinas",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Controla la cantidad de azúcar en la sangre.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Regular el ciclo menstrual.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ayudan para tener un crecimiento y desarrollo saludables.",
                "correcta": "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En situaciones de estrés promueve la producción de mayor energía.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Regula el crecimiento de los huesos y aumento de estatura.",
                "correcta": "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Provoca crecimiento de gónadas.",
                "correcta": "5"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Beneficia el crecimiento de barba en varones.",
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Favorece el crecimiento de senos en mujeres.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la opción correcta",
            "anchoColumnaPreguntas": 20,
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    }
]