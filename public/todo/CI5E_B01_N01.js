json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ESe ensanchan las caderas.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EBuscas pertenecer a algún grupo e identificarte con sus miembro.\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ESe produce la primera menstruación.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EComienza a aparecerte vello púbico y en las axilas.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EPuedes presentar problemas en la toma de decisiones por no saber medir las consecuencias de tus actos.\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Relaciona los cambios que correspondan a la parte física y psicológica que puedes observar en la pubertad.",
            "tipo": "vertical"
        },
        "contenedores": [
            "Físicos",
            "Psicológicos"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la adolescencia te sientes mucho más confiado con la imagen que proyectas a los demás.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cambios de humor son parte del proceso que atraviesas dentro de la adolescencia.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es un mito que el cambio hormonal te provoque cambios de humor.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tu personalidad se comienza a conformar durante la adolescencia, por lo que los cambios que tienes tendrás muchos factores externos e internos. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tu imagen personal dice mucho de lo que eres, lo que te gusta y lo que piensas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Cuestiones",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Lavarme los dientes.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Jugar videojuegos sin medida.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Comer todos los productos que puedan vender en las pequeñas tiendas de la comunidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Exponerme al sol sin protección.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Estudiar en altas horas de la noche.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Comer adecuadamente sin excederme.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Escuchar los consejos que me dan para mejorar.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona el tipo de acciones que ayudan a que tengas un cuidado apropiado de ti mismo."
        }

    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EAfición\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EPreferencia\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EAdicción\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDeseo\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Enfermedad que afecta a la persona física y emocionalmente. Crea dependencia hacia una sustancia o actividad que inicialmente genera satisfacción."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EAutocuidado\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EAutorrealización\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EAutoconocimiento\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EAutoestima\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Parte fundamental de la persona que implica cuidar de ti mismo y tener presente que nadie te conoce mejor que tú mismo. Implica la parte física, intelectual y emocional."
        }
},
    {
        "respuestas": [
            {
                "t13respuesta": "<p><img src=\"ci5e_b01_n03_01_01.png\"></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"ci5e_b01_n03_01_02.png\"></p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"ci5e_b01_n03_01_03.png\"></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"ci5e_b01_n03_01_04.png\"></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"ci5e_b01_n03_01_05.png\"></p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"ci5e_b01_n03_01_06.png\"></p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor todas las imágenes que correspondan a la descripción",
            "tipo": "vertical"
        },
        "contenedores": [
            "Vida saludable",
            "Adicción"

        ]
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La anorexia y bulimia, sólo se presentan en mujeres.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La principal causa de los trastornos alimenticios son los medios de comunicación. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen diversas causas que pueden provocar un desorden alimenticio o una adicción como los problemas emocionales, familiares o la presión social. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen modelos o imágenes que te presentan los medios de comunicación que pueden distorsionar lo que implica estar saludable.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si llegaras a tener algún trastorno alimenticio o adicción, sería muy importante contar con el apoyo de tu familia y con el deseo de mejorar.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Cuestiones",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>adicción<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tabaco<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>anorexia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>bulimia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>prevención<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>salud<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>hábito<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>comunidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>muerte<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>jóvenes<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Realiza el siguiente crucigrama:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>comunicación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>intolerancia<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>sociedad<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>necesidades<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>estereotipos<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En la actualidad, los medios de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> tienen mucha influencia sobre la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> en su forma de pensar, elegir, actuar e incluso sobre sus <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Durante la niñez, son muchos los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> que existen, por ejemplo: que los niños vistan de color azul y las niñas de color rosa. Sin embargo, los estereotipos van más allá de un color, incluso en sus juegos, de la forma física; de pensar y de actuar. Estos estereotipos pueden llegar a causar <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> o, incluso, acoso cuando no cumples con ellos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras para completar la siguiente información.",
            "pintaUltimaCaja": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>cordialidad<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>armónico<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>comunidad<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>respeto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>intereses<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>El <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> es un valor muy importante que debemos cultivar día a día en nuestro hogar, escuela, familia, amigos y todas las personas que están a tu alrededor. De esta manera, es posible tener un espacio <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> para compartir libremente. Es la esencia de la vida en <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Para beneficiarte de un ambiente de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> y seguridad es fundamental valorar los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> y necesidades de los demás y no sólo los propios.<\/p>"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras para completar la siguiente información.",
            "pintaUltimaCaja": false
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El respeto debe ganarse a través de la fuerza física para imponer tus ideas, ante los que no están de acuerdo contigo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La convivencia, implica respeto a las opiniones de personas que pueden pensar diferente a ti.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gran parte de las problemáticas que se viven en el mundo, es por la falta de respeto a la diversidad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando observo que alguien se burla o molesta a un compañero, debo defenderlo e imponerme con violencia.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de hacer una broma o buscar pelea, debo pensar si hay algo que podría cambiar para evitar la violencia o herir los sentimientos de alguien.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Cuestiones",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    }
];