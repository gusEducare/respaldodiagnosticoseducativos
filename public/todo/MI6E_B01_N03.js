 json =[
    {//1
        "respuestas": [
            {
                "t13respuesta": "<p>0.0099<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>0.0867<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>0.1703<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>0.3456<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>0.37<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>0.5<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>0.5004<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>0.504<\/p>",
                "t17correcta": "8"
            }
            
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "soloTexto":true,
            "contieneDistractores":true,
            "anchoRespuestas":120,
            "t11pregunta": "Ordena los números de menor a mayor."
        }
    },
    {//2
        "respuestas": [
            {
                "t13respuesta": "<p>27.87<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>26.07<\/p>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Dulce hizo un papalote pero ahora necesita hilo con qué sujetarlo y poder echarlo a volar. Encontró varios pedazos que ahora une para tener un solo tramo.  <br> &nbsp;&nbsp;a)¿Qué longitud puede alcanzar el papalote si los trozos que unió Dulce son: 12.34 m, 8.5&nbsp;&nbsp; m y 7.03 m?&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;m <br>&nbsp;&nbsp;b) Dulce decidió reforzar las esquinas de su papalote por lo que utilizó 1.8 m del hilo que tenía, ¿cuánto hilo le queda disponible para volar su papalote?&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;m<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {//3
      "respuestas": [
          {
              "t13respuesta": "MI6E_B01_N03_01_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B01_N03_01_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B01_N03_01_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B01_N03_01_05.png",
              "t17correcta": "3",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"MI6E_B01_N03_01_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "opcionesTexto": true,	
          "tamanyoReal": true,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "232,236", "cuadrado", "78, 43", ".","transparent",true]},
          {"Contenedor": ["", "277,236", "cuadrado", "78, 43", ".","transparent",true]},
          {"Contenedor": ["", "322,236", "cuadrado", "78, 43", ".","transparent",true]},
          {"Contenedor": ["", "367,236", "cuadrado", "78, 43", ".","transparent",true]}
      ]
  },
  {  //4
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E<img style='width: 50px;' src='MI6E_B01_N03_02_02.png'>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<img style='width: 50px;' src='MI6E_B01_N03_02_03.png'>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<img style='width: 50px;' src='MI6E_B01_N03_02_04.png'>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<img style='width: 50px;' src='MI6E_B01_N03_02_05.png'>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "columnas":4,
         "t11pregunta":"Coloca la figura que hace un eje de simetría.<br><img style='width: 300px;' src='MI6E_B01_N03_02_01.png'>"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>19<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>12<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>3.6<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>160<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>a) Dado que el 10% de $380 es $38, entonces el 5% de $380 es: $ &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br>b) Dado que el 10% de una hora es 6 minutos, entonces el 20% de una hora es:&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;minutos.<br>c) Ya que el 10% de un día son 2.4 horas, entonces el 15% de un día es:&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;horas.<br>d) Ya que el 10% de asistentes a una fiesta es de 16 personas, entonces el total de asistentes es de: &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;personas.<br><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "contieneDistractores": false,
           "textosLargos": "si",
            "t11pregunta": "Arrastra la palabra que corresponde en cada caso."
        }
    }
]