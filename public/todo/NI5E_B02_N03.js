json=[
    //         {
    //     "preguntas": [
    //         {
    //             "t11pregunta": "<img src=\"esp_db1_i_32_a_01.png\" style=\"height: 84px;\" style=\"height: 84px;\" alt=\"\" \/>",
    //             "c03id_tipo_pregunta": "12"
    //         },
    //         {
    //             "t11pregunta": "<img src=\"esp_db1_i_32_a_02.png\" style=\"height: 84px;\" alt=\"\" \/>",
    //             "c03id_tipo_pregunta": "12"
    //         },
    //         {
    //             "t11pregunta": "<img src=\"esp_db1_i_32_a_03.png\" style=\"height: 84px;\" alt=\"\" \/>",
    //             "c03id_tipo_pregunta": "12"
    //         },
    //         {
    //             "t11pregunta": "<img src=\"esp_db1_i_32_a_04.png\" style=\"height: 84px;\" alt=\"\" \/>",
    //             "c03id_tipo_pregunta": "12"
    //         }
    //     ],
    //     "respuestas": [
    //         {
    //             "t17correcta": "1",
    //             "t13respuesta": "Reino: Vegetal"
    //         },
    //         {
    //             "t17correcta": "2",
    //             "t13respuesta": "Reino: Fungi"
    //         },
    //         {
    //             "t17correcta": "3",
    //             "t13respuesta": "Reino: Animal"
    //         },
    //         {
    //             "t17correcta": "4",
    //             "t13respuesta": "Reino: Monera"
    //         },
    //         {
    //             "t17correcta": "4",
    //             "t13respuesta": "Reino: Protista"
    //         }
    //     ],
    //     "pregunta": {
    //         "c03id_tipo_pregunta": "12",
    //         "t13respuesta": "s1a2",
    //         "anchoImagen": 84,
    //         "altoImagen": 84,
    //         "iconos": true
    //     }
    // },
{
        "respuestas": [
            {
                "t13respuesta": "<p>Ecosistema artificial acuático<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ecosistema artificial terrestre<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Ecosistema natural acuático<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Ecosistema natural terretre<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>Ecosistema artificial acuático</div><img src=\"rtp_z_s01_a01_a.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>Ecosistema artificial terrestre</div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>Ecosistema natural acuático</div><img src=\"rtp_z_s01_a01_c.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div>Ecosistema natural terretre</div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <\/p>"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,        
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Escribe a qué tipo de ecosistema pertenecen las siguientes imágenes. Las posibles respuestas son: Ecosistema natural terrestre, Ecosistema natural acuático, Ecosistema artificial terrestre y Ecosistema artificial acuático ."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Están constituidas por grandes ríos y lagos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tienen una gran cantidad de vegetación.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Albergan un número importante de fauna única.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sólo cuentan con una gran cantidad de extensión.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Un ecosistema terrestre son extensiones de tierra que"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Temperatura.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cantidad de precipitación.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tipo de suelo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Extensión geográfica.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Los tres factores que determinan un ecosistema terrestre son:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Dos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tres<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Cuatro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Nueve<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "En México ¿cuántos tipos de ecosistemas existen?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Frías<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cálidas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Picos de montañas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>De la planicies costeras<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Los ecosistemas de tundra podemos encontrarlos en zonas:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El Pico de Orizaba.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La región del altiplano.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La Sierra Madre de Chiapas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El Golfo de México.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Los pastizales en nuestro país se localizan en:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Veracruz<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>San Luis Potosí<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Puebla<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Oaxaca<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "El bosque de niebla es un ecosistema muy importante y lo localizamos en:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Cedro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mayores a 20 metros<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Oyamel<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Líquenes<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "El bosque boreal se conforma de árboles:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tundra<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pastizal<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Manglar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Matorral<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "El ecosistema que tiene la mayor distribución en México con una extensión del 40% de la superficie de nuestro país es:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tundra, pastizal, bosque templado.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Bosque boreal, bosque de niebla, Selva húmeda.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Manglares, selva seca, cenotes.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Selva seca, matorral y campos agrícolas.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Los nueve ecosistemas terrestres en nuestro país son:"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Fuente de alimento y hábitat para una alta diversidad de flora y fauna silvestre.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Proveen de alimento al ser humano.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "son centros importantes de recarga de los mantos acuiferos de donde se extrae el agua que llega a nuestras casas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Son barreras importantes contra tromentas, huracanes e inundaciones.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Regulan el clima local.",
            "t17correcta":"0"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona el beneficio que nos dan los ecosistemas acuáticos de acuerdo a las imágenes presentadas a continuación:<div  style='text-align:center;'><img style='height: 360px' src='S03_A01_01.png'></div></p>"
      }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Fuente de alimento y hábitat para una alta diversidad de flora y fauna silvestre.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Proveen de alimento al ser humano.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "son centros importantes de recarga de los mantos acuiferos de donde se extrae el agua que llega a nuestras casas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Son barreras importantes contra tromentas, huracanes e inundaciones.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Regulan el clima local.",
            "t17correcta":"0"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona el beneficio que nos dan los ecosistemas acuáticos de acuerdo a las imágenes presentadas a continuación:<div  style='text-align:center;'><img style='height: 360px' src='S03_A01_01.png'></div></p>"
      }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Fuente de alimento y hábitat para una alta diversidad de flora y fauna silvestre.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Proveen de alimento al ser humano.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "son centros importantes de recarga de los mantos acuiferos de donde se extrae el agua que llega a nuestras casas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Son barreras importantes contra tromentas, huracanes e inundaciones.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Regulan el clima local.",
            "t17correcta":"0"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona el beneficio que nos dan los ecosistemas acuáticos de acuerdo a las imágenes presentadas a continuación:<div  style='text-align:center;'><img style='height: 360px' src='S03_A01_01.png'></div></p>"
      }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Fuente de alimento y hábitat para una alta diversidad de flora y fauna silvestre.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Proveen de alimento al ser humano.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "son centros importantes de recarga de los mantos acuiferos de donde se extrae el agua que llega a nuestras casas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Son barreras importantes contra tromentas, huracanes e inundaciones.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Regulan el clima local.",
            "t17correcta":"0"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona el beneficio que nos dan los ecosistemas acuáticos de acuerdo a las imágenes presentadas a continuación:<div  style='text-align:center;'><img style='height: 360px' src='S03_A01_01.png'></div></p>"
      }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Fuente de alimento y hábitat para una alta diversidad de flora y fauna silvestre.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Proveen de alimento al ser humano.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "son centros importantes de recarga de los mantos acuiferos de donde se extrae el agua que llega a nuestras casas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Son barreras importantes contra tromentas, huracanes e inundaciones.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Regulan el clima local.",
            "t17correcta":"1"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona el beneficio que nos dan los ecosistemas acuáticos de acuerdo a las imágenes presentadas a continuación:<div  style='text-align:center;'><img style='height: 360px' src='S03_A01_01.png'></div></p>"
      }
    },
 {
        "respuestas": [
            {
                "t13respuesta": "<p>7<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>50<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>1,400<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>70<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>1,385<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Millones de personas que mueren por enfermedades relacionadas con el agua: <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Porcentaje de ríos contaminados en México y el mundo: <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Millones de personas que viven sin agua potable para consumo doméstico: <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Porcentaje de agua dulce congelada en los glaciares: <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/>Millones de Km3 de agua en la Tierra: <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las cifras o porcentajes correspondientes al problema del agua."
        }
    }
]