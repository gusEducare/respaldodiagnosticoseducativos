json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>¿En México, es posible vivir dignamente de la creación literaria?<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>¿En qué momento decidió ser escritor?<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>¿Sus padres apoyaron su carrera literaria?<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>¿Cuántos premios ha ganado?<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>¿Cuál fue su primer escrito publicado?<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Pregunta abierta",
            "Pregunta cerrada"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Alimentaci&oacute;n",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Vivienda",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Despensa"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Servicio de agua"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
  {
      "respuestas": [
          {
              "t13respuesta": "<p>Que la vida es bella y se debe vivir en plenitud.</p>",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Que no importan lo difícil de la situación, siempre se verá recompensada.</p>",
              "t17correcta": "0",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<script>$(document).ready(function(){$('#columnaContenedores').prepend('<div>¿Qué importan para ti las horas malas, si cada hora en tus nacientes alas pone una pluma bella más? Ya verás al cóndor en plena altura, ya verás concluida la escultura, ya verás alma, ya verás…<p><br>Deidad (fragmento)<br>Amado Nervo</p>  <br><br><br><br><br>  Quiero ser inmortal, con sed intensa, porque es maravilloso el panorama con que nos brinda la creación inmensa; porque cada lucero me reclama, diciéndome, al brillar: Aquí se piensa, también aquí se lucha, aquí se ama.<p>Éxtasis (fragmento)<br>Amado Nervo</div></p>');});</script><style> #columnaContenedores{width: 50% !important;} #columnaRespuestas{width: 49% !important;} #columnaRespuestas li{width: 100% !important;} #columnaContenedores>div:nth-child(1){position: absolute; top:0; left: 0; width: 100%; height: 100%; text-align: justify; font-weight: bold; font-size: 18px;} #columnaContenedores>div:nth-child(1)>p{text-align: right; font-weight: bold;} .subContenedor{width: 100% !important;} .subContenedor>div{border-color: DarkGray !important;}</style>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"",
          "respuestaImagen":true, 
          "bloques":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "180,0", "cuadrado", "78, 81", ".", "whitesmoke"]},
          {"Contenedor": ["", "490,0", "cuadrado", "78, 81", ".", "whitesmoke"]}
      ]
  },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>sencilla</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>orilla</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>lugar</p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>mar</p>",
                "t17correcta": "4,8,12"
            },
            {
                "t13respuesta": "<p>sencillas</p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>orillas</p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>besar</p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>mar</p>",
                "t17correcta": "8,4,12"
            },
            {
                "t13respuesta": "<p>muerto</p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>desierto</p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>penar</p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>mar</p>",
                "t17correcta": "12,4,8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/><div style='width: 350px; display: inline-block; vertical-align: top;'><b>La orilla del mar</b><br><br>No es agua ni arena<br>la orilla del mar.<br><br>El agua sonora<br>de espuma "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ",<br>el agua no puede<br>formarse la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br><br>Y porque descanse<br>en muelle "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ",<br>no es agua ni arena<br>la orilla del "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br><br>Las cosas discretas,<br>amables, "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ";<br>las cosas se juntan<br>como las "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".</div><div style='width: 350px; display: inline-block; vertical-align: top;'>Los mismo los labios,<br>si quieren "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br>No es agua ni arena<br>la orilla del "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br><br>Yo sólo me miro<br>por cosa de "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ";<br>solo, desolado,<br>como en un "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br><br>A mí venga el lloro,<br>pues debo "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br>No es agua ni arena<br>la orilla del "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".</div>"}
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "respuestasLargas":true,
            "anchoRespuestas": 130,
            "ocultaPuntoFinal":true,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    }
]