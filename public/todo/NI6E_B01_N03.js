json = [
{
    "respuestas": [
        {
            "t13respuesta": "Ovulo",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Ambos",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Espermatozoide",
            "t17correcta": "2"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Siempre tienen un cromosoma X",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Se produce en los testículos",
            "correcta"  : "2"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Cuenta con 23 cromosomas",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "A partir de su unión se forma un nuevo ser humano",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Célula con función reproductiva.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Pueden llevar un cromosoma X o un cromosoma Y",
            "correcta"  : "2"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Luego de su unión con la contraparte se forma una nueva célula de 46 cromosomas",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Se produce en los ovarios",
            "correcta"  : "0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p>Completa la siguiente tabla, tocando en cada columna la opción que corresponda.<\/p>",
        "descripcion": "",   
        "variante": "editable",
        "anchoColumnaPreguntas": 45,
        "evaluable"  : true
    }        
},
     {
        "respuestas": [
            {
                "t13respuesta": "<p>infecciones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>sexual<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>transmiten<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>sexuales<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>papiloma<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>VPH<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>VIH<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>manifestarse<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>infectada<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>riesgo<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>contagiará<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>relaciones<\/p>",
                "t17correcta": "12"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/><br/>Las "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;de transmisión "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;son un conjunto de enfermedades que se "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;frecuentemente a través de las relaciones "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ". Dos de las infecciones de transmisión sexual más frecuentes y peligrosas son el virus del "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; humano ("
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ") y el "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;(virus de inmunodeficiencia adquirida), pues ambos virus pueden llevar a la muerte.<br/>En algunas ocasiones estas enfermedades tardan mucho en "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", así que la persona "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; no se da cuenta y pone en "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;su vida y las otras personas; es decir, "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;a todas las personas con las que tenga "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;sexuales."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "t11pregunta": "Completa el texto ordenando las palabras que hacen falta."
        }
    },   
{
        "respuestas": [
            {
                "t13respuesta": "<p>NERVIOSO<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>NEURONAS<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>RESPIRATORIO<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>ADIPOSITAS<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>CIGOTO<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>DIGESTIVO<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Sistemas que controla todas las funciones del cuerpo humano"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Célula del sistema nervioso que conduce información del ambiente al cerebro y viceversa"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Sistema en donde nuestro cuerpo intercambia oxígeno por bióxido de carbono"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Células que acumulan grasa en el cuerpo"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se forma al momento de la fecundación entre un ovulo y un espermatozoide"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Sistema que procesa los alimentos y los convierte en nutrimentos para nuestro cuerpo"
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 15,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 6,
                "datoY" : 4
            },
            {
                "direccion" : 1,
                "datoX" : 9,
                "datoY" : 4
            },
            {
                "direccion" : 0,
                "datoX" : 6,
                "datoY" : 7
            },
            {
                "direccion" : 0,
                "datoX" : 4,
                "datoY" : 12
            },
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 15
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto":15,
            "ancho":15,
            "t11pregunta": "Completa el crucigrama."
        }
    },  
    {
      "respuestas":[
         {
            "t13respuesta":"<p>Se recomienda tomar de 6 a 8 vasos al día.</p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Agua Potable.</p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Se recomienda tomar de 0 a 4 vasos al día.</p>",
            "t17correcta":"2"
         },
         {
            "t13respuesta":"<p>Refrescos, aguas endulzadas.</p>",
            "t17correcta":"5"
         },
         {
            "t13respuesta":"<p>No se recomienda su consumo en preescolares y escolares.</p>",
            "t17correcta":"3"
         },
         {
            "t13respuesta":"<p>Leche semidescremada y descremada, bebidas de soya sin azúcar adicionada.</p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Se recomienda tomar de 0 a 2 vasos al día.</p>",
            "t17correcta":"3"
         },
         {
            "t13respuesta":"<p>Se recomienda tomar de 0 a 2 vasos al día.</p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Café y té sin azúcar añadida.</p>",
            "t17correcta":"2"
         },
         {
            "t13respuesta":"<p>Jugos 100% fruta, leche entera, bebidas deportivas.</p>",
            "t17correcta":"4"
         },
         {
            "t13respuesta":"<p>Refrescos de dieta, aguas con vitaminas, bebidas energizantes u otras bebidas dietéticas con endulzantes no calóricos.</p>",
            "t17correcta":"3"
         },
         {
            "t13respuesta":"<p>Se recomienda tomar de 0 a 1/2 vaso al día.</p>",
            "t17correcta":"4"
         },
         {
            "t13respuesta":"<p>No se recomienda su consumo.</p>",
            "t17correcta":"5"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"5",
         "t11pregunta":"Arrastra a cada contenedor lo que corresponda de acuerdo a las recomendaciones de la jarra del buen beber.",
         "tipo":"vertical"
      },
        "contenedores":[ 
            "Nivel 1",
            "Nivel 2",
            "Nivel 3",
            "Nivel 4",
            "Nivel 5",
            "Nivel 6",
            
        ],
        "css":{
            "altoContenedorRespuesta":80,
            "alturaRespuestas":80
        }
   },   
{
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los seres humanos tenemos aproximadamente 25, 000 genes.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los varones tienen dos cromosomas sexuales X.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los humanos; como especie, compartimos el 99.9% de los genes.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Únicamente el 0.1% de genes que compartimos como humanos es lo que nos distintos entre nosotros.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los genes se organizan en unas estructuras llamadas cromosomas.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El número de cromosomas es variable dependiendo del momento de la fecundación.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cada célula del cuerpo de un ser humano cuenta con 46 cromosomas.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las mujeres tiene dos cromosomas sexuales X.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hombres un cromosoma sexual X y otro Y.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las mujeres poseen un cromosoma sexual X y otro Y.",
                "valores": ["&nbsp", "&nbsp"],
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elije falso (F) o verdadero (V) en cada enunciado.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
]