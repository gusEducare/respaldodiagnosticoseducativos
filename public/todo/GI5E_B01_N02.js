json = [
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_01_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_06.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_04.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_05.png",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el nombre del continente al lugar correspondiente en el mapa.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n02_01_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "ocultaPuntoFinal": true,
            "borde": false
        },
         "css":{
            "tamanoFondoColumnaContenedores":"contain"
            
        },
        "contenedores": [
            {"Contenedor": ["", "312,26", "cuadrado", "130, 61", ".", "transparent"]},
            {"Contenedor": ["", "58,260", "cuadrado", "130, 61", ".", "transparent"]},
            {"Contenedor": ["", "390,228", "cuadrado", "130, 61", ".", "transparent"]},
            {"Contenedor": ["", "75,486", "cuadrado", "130, 61", ".", "transparent"]},
        {"Contenedor": ["", "218,493", "cuadrado", "130, 61", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>África y Europa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>África y Asia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Europa y Asia<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div>¿Cuáles son los continentes de la imagen que se encuentran separados por el Mar Mediterráneo?<br><img src=\"gi5e_b01_n02_02_01.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div>¿Entre qué continentes es la frontera que se ve en la imagen, separados por el canal de Suez en Egipto?<br><img src=\"gi5e_b01_n02_02_02.png\" style='width:200px'\/><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Arrastra la ubicación que corresponda a la imagen.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_03_03.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_04.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_06.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_07.png",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Los continentes deben ser ordenados de acuerdo a su extensión territorial. \n\
                               El Continente Asiático es el de mayor extensión. Coloca los continentes que faltan, en el orden que corresponde.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n02_03_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "422,195", "cuadrado", "131, 47", ".", "transparent", true]},
            {"Contenedor": ["", "470,195", "cuadrado", "131, 47", ".", "transparent", true]},
            {"Contenedor": ["", "422,372", "cuadrado", "131, 47", ".", "transparent", true]},
            {"Contenedor": ["", "470,372", "cuadrado", "131, 47", ".", "transparent", true]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n02_04_05.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n02_04_04.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n02_04_02.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n02_04_03.png",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el nombre del continente a la imagen con la que se identifique.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n02_04_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "205,74", "cuadrado", "200, 44", ".", "transparent"]},
            {"Contenedor": ["", "417,74", "cuadrado", "200, 44", ".", "transparent"]},
            {"Contenedor": ["", "205,369", "cuadrado", "200, 44", ".", "transparent"]},
            {"Contenedor": ["", "417,369", "cuadrado", "200, 44", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>naturales<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>artificiales<\/p>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los países están separados por líneas imaginarias llamadas fronteras, las cuales pueden ser <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como océanos o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como un muro.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa la siguiente oración"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_05_05.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_06.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_03.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_02.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_07.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_08.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_09.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_10.png",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "gi5e_b01_n03_05_11.png",
                "t17correcta": "9"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el nombre del país a la ubicación que le corresponda.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n02_05_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "contieneDistractores": true,
            "opcionesTexto": true,
            "borde": false,
            "anchoImagen": 70
        },
        "css":{
            "tamanoFondoColumnaContenedores":"contain"
            
        },
        "contenedores": [
            {"Contenedor": ["", "335,42", "cuadrado", "130,60", ".", "transparent"]},
            {"Contenedor": ["", "58,213", "cuadrado", "130,60", ".", "transparent"]},
            {"Contenedor": ["", "373,304", "cuadrado", "130,60", ".", "transparent"]},
            {"Contenedor": ["", "232,449", "cuadrado", "130,60", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_06_03.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_04.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_02.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_05.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_06.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_07.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_09.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_08.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_10.png",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_11.png",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_13.png",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_16.png",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_15.png",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_12.png",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_14.png",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_20.png",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_19.png",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_17.png",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_18.png",
                "t17correcta": "18"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<style>#columnaContenedores{height:2000px !important;}\n\
                            #tarjetas li{height:38px !important;}</style>\n\
                                    <p>Arrastra el nombre del país a la ubicación que le corresponda.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n03_06_01.png",
            "respuestaImagen": true,
            "bloques": true,
            "contieneDistractores": true,
            "opcionesTexto": false,
            "borde": false
        }, 
        "css":{
            "tamanoFondoColumnaContenedores":"100% 100%"
            
        },
        "contenedores": [
            {"Contenedor": ["", "144,488", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "249,215", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "198,66", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "329,23", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "431,22", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "426,190", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "543,322", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "626,459", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "941,165", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1098,223", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1186,75", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1273,106", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1367,198", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1277,406", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1437,384", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1687,445", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1776,366", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1819,125", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "1926,215", "cuadrado", "130, 38", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_07_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_05.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_03.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_04.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_07.png",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_06.png",
                "t17correcta": "5",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca en su lugar el nombre del tipo de línea o punto imaginario.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n03_07_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false

        },
        "contenedores": [
            {"Contenedor": ["", "12,77", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "202,124", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "309,46", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "461,76", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "84,453", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "376,400", "cuadrado", "152, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Se basa en los meridianos y nos dice que tan al este o al oeste a partir del meridiano de Greenwich es esta el punto a localizar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Está basada en los paralelos y nos dice que tan al norte o al sur a partir del ecuador está el punto que queremos localizar.",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Longitud"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Latitud"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "ocultaPuntoFinal": true,
            "t11pregunta": "Relaciona el componente de las coordenadas geográficas con su significado."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Natural<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cultural<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Económico<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Político<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Social<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div><img src=\"gi5e_b01_n03_08_02.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><img src=\"gi5e_b01_n03_08_01.png\" style='width:200px'\/><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Selecciona el componente que se relaciona con la imagen.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Japón es más grande que Rusia",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En Australia hace más calor que en la Antártida",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En Canadá hace más calor que en Colombia",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una cultura es más parecida a otra que está en el mismo continente que a otra en un continente diferente.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Responde si la siguiente afirmación es verdadera o falsa.",
            "descripcion": "Aspectos a valorar",
            "evaluable": false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Naturales",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Demográficos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Geopolíticos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Sociales",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Culturales",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Macroeconómicos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Económicos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Políticos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Biológicos",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las opciones que correspondan a elementos de la diversidad."
        }
    }
];

