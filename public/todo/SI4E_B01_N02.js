json = [
    {
        "respuestas": [
            {
                "t13respuesta": "¿Qué?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿Quién?",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "¿Cómo?",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "¿Cuándo?",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "¿Dónde?",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "¿Cuánto?",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Para saber sobre conceptos, temas utiliza "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/>Si quieres conocer respecto a personas o personajes usas "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;<br/>Utilizas "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;para saber sobre procedimientos, descripciones, instrucciones o pasos "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Te permite saber sobre tiempo o una secuencia. <br/> Para saber lugares o ubicaciones, usas "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;Tendrás información sobre cantidades o proporciones."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "respuestasLarsgas": true,
            "ocultaPuntoFinal": true,
            "t11pregunta": "Arrastra la palabra correcta."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Raíz</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Agradar</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Solución</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tonalá</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Natural</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Trébol</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Abuelo</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dátil</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elefante</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>López</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hígado</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Imágenes</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Cáscara</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Máquina</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Eléctrico</p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "De acuerdo a su acentuación, identifica y coloca en cada bloque las palabras que correspondan.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Agudas",
            "Graves",
            "Esdrújulas"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son palabras agudas: acción, bebé, café, abarcar y borrar",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las palabras agudas se tienen tilde",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las palabras esdrújulas siempre se escriben con tilde",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sábanas, médula, y pájaro son ejemplos de palabras esdrújulas",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las palabras graves nunca se escriben con tilde",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Página es una palabra grave",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "César, banco y dócil son ejemplos de palabras graves",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona si el enunciado es falso o verdadero",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Alegría",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Fallecer",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Limitar",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Higiénico",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Acertado",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Inicio",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Belicoso",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "tristeza"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "vivir"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "permitir"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "insalubre"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "fallido"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "final"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "pacífico"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los antónimos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "depto",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "trayecto",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "siglas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "rosa de los vientos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "av",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "simbolos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "onu",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "m",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "croquis",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "carr",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "unam",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "km",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Abreviatura de departamento"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Camino o espacio que se recorre para ir de un lugar a otro"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se escriben con las letras o sílabas iniciales de las palabras que la componen"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Símbolo que representa la circunferencia del horizonte mediante el uso de 32 rombos unidos por sus extremos"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "abreviatura de avenida"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Son un tipo de abreviación que se usa para indicar medidas y denominaciones científicas"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Asamblea General de las Naciones Unidas"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Símbolo de metro"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Dibujo que sirve para indicar un trayecto con mayor precisión"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Abreviatura de carretera"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Universidad Nacional Autónoma de México"
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Símbolo de Kilómetro"
            }
        ],
        "pocisiones": [
            {
                "direccion": 0,
                "datoX": 1,
                "datoY": 1
            },
            {
                "direccion": 1,
                "datoX": 4,
                "datoY": 1
            },
            {
                "direccion": 1,
                "datoX": 13,
                "datoY": 8
            },
            {
                "direccion": 0,
                "datoX": 3,
                "datoY": 8
            },
            {
                "direccion": 1,
                "datoX": 6,
                "datoY": 8
            },
            {
                "direccion": 1,
                "datoX": 13,
                "datoY": 8
            },
            {
                "direccion": 1,
                "datoX": 20,
                "datoY": 8
            },
            {
                "direccion": 0,
                "datoX": 13,
                "datoY": 10
            },
            {
                "direccion": 0,
                "datoX": 7,
                "datoY": 15
            },
            {
                "direccion": 1,
                "datoX": 7,
                "datoY": 15
            },
            {
                "direccion": 1,
                "datoX": 11,
                "datoY": 15
            },
            {
                "direccion": 0,
                "datoX": 10,
                "datoY": 18
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "ancho": 21,
            "alto": 20,
            "t11medida": 16,
            "t11pregunta": "Completa el crucigrama. Recuerda lo que aprendiste sobre Croquis."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Ella",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "este",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "todos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "él",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Ellos",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "a) Eumelia corría en el juego y cayó al agua. "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;se lastimó. <br>b) El auto sobrecalentó y al final el "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;ya no se movió. <br>c) Juan, Miguel, Fernando y Yo tenemos 11 años, "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;vamos en cuarto grado.<br>d) Alejandro, es un joven extraordinario."
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;salvó al cachorro del incendio, entrando por la ventana para rescatarlo. <br>e) La familia completa asistió al teatro. "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;no me invitaron."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Elige de entre los pronombres propuestos, aquel que te permita hacer paráfrasis y arrástralo hasta el enunciado correspondiente."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Opción 1",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opción 2",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Opción 3",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Triunfar",
                "valores": ["Fortalecer", "Vencer", "Trozar"],
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Extrovertido",
                "valores": ["Chistoso", "Comunicativo", "Llamativo"],
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuaderno",
                "valores": ["Álbum", "Libreta", "Carta"],
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dolor",
                "valores": ["Malestar", "Moretón", "Maldad"],
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige el sinónimo que corresponda.</p>",
            "descripcion": "Elige el sinónimo que corresponda.",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "<div>Escribe un título de cuando menos tres palabras a cada imagen propuesta. Recuerda ser congruente y lógico.</div><div style='text-align: center;'><img src='si4e_b01_n02_01.png'></div><br><br>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "<div>Escribe un título de cuando menos tres palabras a cada imagen propuesta. Recuerda ser congruente y lógico.</div><div style='text-align: center;'><img src='si4e_b01_n02_02.png'></div><br><br>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "<div>Escribe un título de cuando menos tres palabras a cada imagen propuesta. Recuerda ser congruente y lógico.</div><div style='text-align: center;'><img src='si4e_b01_n02_03.png'></div><br><br>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "sortija",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "desensortija",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "risa",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "sonrisa",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Ensortijada "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;tu risa <br>desensortija tu "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br>Ensortijada "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ""
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra y ordena las palabras faltantes hasta construir el trabalenguas"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "humor",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "favor",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "paz",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "más",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "caracol",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "sol",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "dictar",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gritar",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "también",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "bien",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "nada",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "emplumada",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "preocupado",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "picado",
                "t17correcta": "14"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/><br/>El monarca Moctezuma<br>hoy está de mal "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br>Se han cansado de pedirle<br>que sonría, por "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".-- ¡No sonrío! ¡Hoy no quiero<br>¡Por favor, déjenme en "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "!<br>¡Que se quede mi ayudante!<br>¡Que no entre nadie "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "!-- ¡Pero si hay mucho trabajo!<br>¿Qué no ha oído el "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "?<br>Ya sonó para la junta,<br>allá, en el templo del "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".Y recuerde que hoy le toca<br>cuatro códices "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "...<br>Muy furioso Moctezuma<br>le termina por "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":¡A volar con los ministros!<br>¡Con los códices "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "!<br>¡Ya no quiero ver a nadie<br>porque no me siento "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "!-- ¡Está bueno! ¡Ya me iba!<br>¡No se puede hablar de "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ",<br>porque está usted de peor genio<br>que la serpiente "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "!En la casa del monarca<br>todo el mundo "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;<br>se pregunta en los pasillos<br>-- ¿Qué mosquito le ha "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ""
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas": true,
            "anchoRespuestas": 120,
            "pintaUltimaCaja": false,
            "soloTexto": true,
            "ocultaPuntoFinal": true,
            "t11pregunta": "El siguiente es un extracto del poema El berrinche de Moctezuma de Nuria Gómez Benet.  Arrastra al recuadro las palabras del riman."
        }
    }
];
