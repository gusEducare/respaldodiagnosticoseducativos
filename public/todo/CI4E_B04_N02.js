[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>No cumplir con una tarea<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Insultar a un compañero<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Robar en un supermercado<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No pagar impuestos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Usar tu teléfono en la sala de cine<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Norma",
            "Ley"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la Constitución de nuestro país, están descritos, sólo los derechos de los niños.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos de los niños deben ser conocidos para crear conciencia y que sean respetados.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nuestra constitución se creó el 5 de febrero de 1910 en el gobernó de Venustiano Carranza.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la Constitución se fija la organización política de un estado, estableciendo derechos y obligaciones.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen derechos universales contemplados en la Constitución como el derecho a vivir en familia y a no ser discriminado.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>legitimar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>país<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>poder<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>bienestar<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>leyes<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Autoridad significa <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; a alguien, tener el poder de tomar decisiones en nombre de un grupo de personas como puede ser un municipio, una empresa o un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.Este prestigio se debe a la capacidad de la persona ante ese puesto y su preparación. Sin embargo tener el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; no representa la máxima autoridad ya que éstas deben fomentar el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y deben respetar los derechos y las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; que rigen a las personas por eso, cuando una persona tiene un puesto de autoridad tiene la responsabilidad de ejercerlo y no de alardear de un puesto.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Articulo tercero Derecho a la educación",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ley federal de trabajo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Es el medio que nos dice las sanciones si cometemos alguna infracción",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Alzar la mano para participar en el salón",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Constitución"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ley"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Reglamento"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Norma"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Constitución Política sólo nos habla de los derechos con los que contamos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Contamos con el derecho, gracias a la Constitución, de conocer cómo trabajar las autoridades.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los servidores públicos deben trabajar en su propio beneficio, para luego ver por los demás.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen instituciones específicas que se encargan de supervisar a las autoridades.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Constitución nos muestra las sanciones a funcionarios que no cumplan con sus obligaciones.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>opiniones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>diferencias<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>creencias<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>prejuicios<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>diferente<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>A veces las personas tenemos diferentes <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y no siempre estamos de acuerdo con todo, sin embargo, ser consciente de estas <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; nos puede ayudar a comprender el punto de vista de otra persona, sabiendo que no va en nuestra contra sino que por sus condiciones de vida, cultura o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; piensa de forma diferente. Una buena herramienta es ponerse en el lugar de la otra persona y observar los puntos buenos de su idea, sin generar <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; ni juicios acerca de si está bien o está mal, simplemente es <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Al cometer una infracción de tránsito, pagarle al policía para que te deje ir.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>En la cooperativa de la escuela, la persona que te atiende te da de más en el cambio y no lo regresas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Un policía comienza a pegarle a una persona mayor por manifestarse contra algo injusto.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Olvidar la verificación del auto y pagar la multa correspondiente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pagar los impuestos en las fechas que lo establece el gobierno.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Escribe si las siguientes situaciones hablan de una respuesta de forma legal o ilegal.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Ilegal",
            "Legal"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las normas pueden o no, ser cumplidas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las normas pueden ser modificadas para adaptarse a los cambios que vive una sociedad.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las normas pueden ayudar a crear o modificar leyes.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las normas son obligatorias.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La buena convivencia sólo puede darse si cada quien tiene sus propias normas, aunque no haya un consenso.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    }
]