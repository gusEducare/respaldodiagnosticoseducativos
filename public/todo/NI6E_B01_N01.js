json = [
        {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Sistema nervioso</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Sistema inmunológico</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Sistema respiratorio</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Sistema nervioso</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Sistema inmunológico</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Sistema respiratorio</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Sistema circulatorio</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Sistema excretor</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Sistema locomotor</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Sistema digestivo</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Sistema circulatorio</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Sistema excretor</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Sistema circulatorio</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Sistema respiratorio</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Sistema digestivo</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Sistema circulatorio</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Sistema excretor</p>",
            "t17correcta":"1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Sistema digestivo</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Sistema respiratorio</p>",
            "t17correcta":"1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta":"<p>Sistema excretor</p>",
            "t17correcta":"0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta":"<p>Sistema digestivo</p>",
            "t17correcta":"0",
            "numeroPregunta":"6"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las opciones correspondientes a los sistemas que componen nuestro cuerpo.<br/>Forma anticuerpos. Utiliza las vacunas.",
                         "A través de él, recibimos y percibimos los estímulos.",
                         "Está encargado de función del movimiento del cuerpo.",
                         "Utiliza la sangre para transportar nutrientes y oxígeno al cuerpo, así como desprenderse de desechos.",
                         "Obtienen los nutrientes cuando rompre los alimentos.",
                         "Su función es eliminar las sustancias de desecho a través de la orina.",
                         "A través de procesos se utiliza el oxígeno para producir energía."],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Falta de actividad física.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Desarrollo integral.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Consumo excesivo de calorías por líquidos procesados y azucarados.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Alimentación deficiente y con elevado aporte calórico.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Consumo diario de frutas y verduras.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona tres hábitos que favorecen el sobrepeso y obesidad en niños y adolescentes."
        }
    },
  {
      "respuestas": [
          {
              "t13respuesta": "ni6e_b01_n01_02.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "ni6e_b01_n01_03.png",
              "t17correcta": "",
              "columna":"0"
          },
          {
              "t13respuesta": "ni6e_b01_n01_04.png",
              "t17correcta": "2,0,1,3,4",
              "columna":"1"
          },
          {
              "t13respuesta": "ni6e_b01_n01_05.png",
              "t17correcta": "",
              "columna":"1"
          },
          {
              "t13respuesta": "ni6e_b01_n01_06.png",
              "t17correcta": "4,0,1,2,3",
              "columna":"0"
          },
          {
              "t13respuesta": "ni6e_b01_n01_07.png",
              "t17correcta": "1,0,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "ni6e_b01_n01_08.png",
              "t17correcta": "3,0,1,2,4",
              "columna":"1"
          },
          {
              "t13respuesta": "ni6e_b01_n01_09.png",
              "t17correcta": "",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra al contenedor las imágenes que reflejen una vida saludable.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"ni6e_b01_n01_01.png",
          "respuestaImagen":true, 
          "bloques":false, "tamanyoReal": true
          
      },
      "contenedores": [
          {"Contenedor": ["", "45,1", "cuadrado", "110,110", "."]},
          {"Contenedor": ["", "45,113", "cuadrado", "110,110", "."]},
          {"Contenedor": ["", "45,225", "cuadrado", "110, 110", "."]},
          {"Contenedor": ["", "45,335", "cuadrado", "110, 110", "."]},
          {"Contenedor": ["", "45,448", "cuadrado", "110, 110", "."]}
      ]
  },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Color de piel<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Color de ojos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Preferencias de alimentos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Complexión física<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tipo de cabello<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Gustos en películas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Color de cabello<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tipo de piel<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todas las opciones que muestren características heredadas de padres a hijos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Más de 200 tipos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Más de 50 tipos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Más de 500 tipos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Más de 2000 tipos.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "¿Cuántos tipos de células integran el cuerpo humano?"
        }
    },
 {
        "respuestas": [
            {
                "t13respuesta": "<p>Alimentarse con frutas y verduras.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Antecedentes de familiares con cáncer de mama.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tener 40 años o más.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso de tintes capilares.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tener la primera menstruación antes de los 12 años.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Usar anticonceptivos hormonales por más de 5 años.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tener el primer hijo después de los 30 años.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cocinar en microondas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No haber tenido hijos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tener la última menstruación después de los 52 años.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Exponerse a eclipses lunares.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Obesidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Alimentación rica en carbohidratos y baja en fibra.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dieta alta en grasas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elegir un estilo de vida saludable.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sedentarismo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Alto consumo de alcohol.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tabaquismo.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todos los factores de riesgo para padecer cáncer de mama."
        }
    }
]