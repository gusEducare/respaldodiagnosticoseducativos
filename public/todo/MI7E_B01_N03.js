json = [
     {
         "respuestas": [
             {
                 "t13respuesta": "<p><img src='MI7E_B01_N03_01_04.png'><\/p>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<p><img src='MI7E_B01_N03_01_03.png'><\/p>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<p><img src='MI7E_B01_N03_01_02.png'><\/p>",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<p><img src='MI7E_B01_N03_01_01.png'><\/p>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<p><img src='MI7E_B01_N03_01_05.png'><\/p>",
                 "t17correcta": "0"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "5",
             "t11pregunta": "Arrastra cada fracción al lugar que le corresponde cuando se convierte a número decimal. ",
             "tipo": "horizontal"
         },
         "contenedores": [
             "Decimal finito",
             "Periódico puro",
             "Periódico mixto"
         ]
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "<img src='mi7e_b01_n03_02_04.png'>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src='mi7e_b01_n03_02_06.png'>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": " <img src='mi7e_b01_n03_02_07.png'>",
                 "t17correcta": "3"
             }
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "<img src='mi7e_b01_n03_02_01.png'>"
             },
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "<img src='mi7e_b01_n03_02_02.png'>"
             },
             {
                 "c03id_tipo_pregunta": "18",
                 "t11pregunta": "<img src='mi7e_b01_n03_02_03.png'>"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "18",
             "t11pregunta": "Relaciona cada pareja de números con el número decimal o fracción que se encuentra entre ellos.",
             "anchoImagen": 130,
             "altoImagen": 90
         }
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "Manzanas",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "Peras",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "Guayabas",
                 "t17correcta": "0"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "1",  
             "t11pregunta": "Selecciona la respuesta correcta.<br/>María compró en el mercado 1 ¼ kg de manzanas, 3/2 de peras y 7/8 kg de guayabas y todo lo metió en una misma bolsa. Camino a su casa le dio a su sobrina la mitad de lo que compró de una de las frutas. Si ahora la bolsa pesa 2 7/8 kg, ¿qué frutas le regaló María a su sobrina?"
         }
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "<img src=\"EJEMPLO.png\">",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src=\"EJEMPLO.png\">",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<img src=\"EJEMPLO.png\">",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "<img src=\"EJEMPLO.png\">",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<img src=\"EJEMPLO.png\">",
                 "t17correcta": "1"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "2",
             "t11pregunta": "Selecciona la respuesta correcta.<br/>Ulises guardó en una caja tres bolsas de limones cuyo peso suma 5 13/24 kg.<br>Arrastra a la caja las bolsas que guardó Ulices en ella. "
         }
     },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>17<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>21<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>25<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>8<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>16<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>32<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p>a) El quinto término de una sucesión con progresión aritmética es 29. Si cada término se obtiene multiplicarlo por cierta cantidad y al resultado sumarle 9, ¿cuáles son los primeros cuatro términos de la sucesión? <br> <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>,29,… <br><br>b) El quinto término de una sucesión con progresión geométrica es 64. Si cada término se obtiene multiplicando por 2 el anterior, elige los primeros cuatro términos de la sucesión.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>,64,... <\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "contieneDistractores": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true,
            "respuestasLargas": true,
            "anchoRespuestas": 50,
            "t11pregunta": "Relacciona cada recuadro y anota los primeros términos de cada sucesión de acuerdo con la regla dada."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "P = 2<i>a</i> + 4<i>a</i>;&nbsp;&nbsp;A = 4a<sup>2</sup>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "P = 2(2<i>a</i>) + 2(4<i>a</i>); &nbsp;&nbsp;A = 8a<sup>2</sup>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "P = 2<i>a</i> + 8<i>a</i>; &nbsp;&nbsp;A = 4a<sup>2</sup>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "P = <i>a</i> + <i>a</i> + 4<i>a</i>;&nbsp;&nbsp; A =a(4<i>a</i>)",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br/>En un terreno rectangular la medida del largo es cuatro veces mayor que el ancho. Si el ancho del terreno mide a, ¿qué expresiones algebraicas representan su perimetro y su área?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": " Si las diagonales de un cuadrilátero se cortan en su punto medio, éste es un parelogramo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las diagonales de un rombo y de un cuadrado cortan a los ángulos por la mediatriz de sus ángulos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": " Las diagonales de un rectángulo dividen a sus ángulos en dos ángulos iguales.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso o verdadero, según corresponda a cada afirmación.<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Punto donde se cortan las mediatrices de un triángulo.<\/p>",
                "t17correcta": "0,0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1,0"
            },
            {
                "t13respuesta": "<p>Punto donde se intersecan las alturas de un triángulo.<\/p>",
                "t17correcta": "0,1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1,1"
            },
            {
                "t13respuesta": "<p>Punto en el que se intersecan las bisectrices del triángulo.<\/p>",
                "t17correcta": "0,2"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1,2"
            },
            {
                "t13respuesta": "<p>Punto de intersección de las medianas de un triángulo.<\/p>",
                "t17correcta": "0,3"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1,3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Relaciona cada concepto con su definición y con la figura que muestra el punto que lo representa. "
        },
        "contenedores": [
            "Definición",
            "Representación"

        ],
        "contenedoresFilas": [
            "Circuncentro",
            "Ortocentro",
            "Incentro",
            "Baricentro"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>$3,000</p>",
                "t17correcta": "0",
                "numeroPregunta": 1
            },
            {
                "t13respuesta": "<p>$9,600</p>",
                "t17correcta": "0",
                "numeroPregunta": 1
            },
            {
                "t13respuesta": "<p>$4,800</p>",
                "t17correcta": "1",
                "numeroPregunta": 1
            },
            {
                "t13respuesta": "<p>$3,000</p>",
                "t17correcta": "1",
                "numeroPregunta": 2
            },
            {
                "t13respuesta": "<p>$2,625</p>",
                "t17correcta": "0",
                "numeroPregunta": 2
            },
            {
                "t13respuesta": "<p>$4,800</p>",
                "t17correcta": "0",
                "numeroPregunta": 2
            },
            {
                "t13respuesta": "<p>$5,200</p>",
                "t17correcta": "0",
                "numeroPregunta": 3
            },
            {
                "t13respuesta": "<p>$3,000</p>",
                "t17correcta": "0",
                "numeroPregunta": 3
            },
            {
                "t13respuesta": "<p>$9,600</p>",
                "t17correcta": "1",
                "numeroPregunta": 3
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "preguntasMultiples": true,
            "t11pregunta": ["<div style='position:relative; left:31px; top:-25px; font-size:120%'><b>Analiza la información y calcula lo que se pide, arrastra la respuesta correcta al lugar que le corresponde.<br/>Cuatro personas se asociaron para poner un negocio. La aportación inicial fue la siguiente: \n\
                            <br>- Socio 1: aportó 20% del capital. \n\
                            <br>- Socio 2: aportó $7 875. \n\
                            <br>- Socio 3: aportó 1/8 del capital\n\
                            <br>- Socio 4: aportó $18 000. \n\
                            <br>Al final de primer mes se repartieron 40% de las ganacias de manera proporcional a la aportación de cada uno. Al socio 2 le tocaron $6 600. ¿Cuánto dinero le tocó a los otros socios?</b>",
                "Socio 1:", "Socio 3:", "Socio 4:"]
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si se quiere sacar una pelota roja, la caja con mayor probabilidad de obtenerla es la 3, porque es la caja con más pelotas de ese color.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caja donde es menos probable sacar una pelota azul es la 1.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si se saca una pelota 20 veces de la caja 2, es muy probable que salga más veces una pelota roja, que una azul.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso o verdadero en cada afirmación, de acuerdo con la información que se da.<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    }
]
