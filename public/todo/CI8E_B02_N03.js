json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Período de la vida de la persona que constituye la primera fase de la adolescencia y el paso de la infancia a la edad adulta.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Período de adaptación en la que se consolida la madurez sexual. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Diferencia o distinción entre personas, animales o cosas, es decir,  varieda",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "Consideración de que alguien o algo, tiene un valor por sí mismo y se establece como reciprocidad, es decir, reconocimiento mutuo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Sentimiento de unidad basado en metas o intereses comunes, así como la capacidad para entregar bienes y ayuda a otros individuos.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Cualidad que consiste en dar a cada uno lo que se merece en función de sus méritos o condiciones.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "pubertad"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "adolescencia"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Diversidad"
            },{
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "pubertad"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Solidaridad"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Equidad"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la actualidad los adolescentes como tú, están expuestos a un fenómeno llamado hiperconectividad, donde pareces estar todo el tiempo conectado.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La música es un elemento que sirve como medio de expresión para los adolescentes y con ello puedes expresar lo que sientes o piensas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Como adolescente no se tienen contemplados derechos específicos que se planteen en organismos internacionales como la ONU.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La información sobre sexualidad debe ser presentada sólo cuando tengas la mayoría de edad. ",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los temas como la sexualidad o enfermedades de transmisión sexual, sólo provocan que exista morbo por los temas.",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
    ,
    {
        "respuestas": [
            {
                "t13respuesta": "Evitar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Realizar",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Brindar información personal en redes sociales que pueda comprometer tu seguridad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tener contacto con personas que no conocemos en redes sociales aunque te agraden. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Conocer los aspectos que envuelven a la sexualidad y ejercerla responsablemente. ",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Crear lazos de confianza y lealtad con amigos que puedan ser duraderos. ",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Buscar y  brindar situaciones de respeto que nos permitan crecer en una relación de pareja",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Situaciones que pongan en riesgo tu integridad, ya sea de forma física o psicológica en una relación de pareja.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Formación de grupos con intereses en común.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Depresión que puede llevar a pensar en el suicidio.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Indecisión en las relaciones de pareja.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aislamiento.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Baja autoestima.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Búsqueda de identidad  a través de la música.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Fracaso escolar o deficiente rendimiento laboral.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Relaciones que permiten el crecimiento personal.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Trastornos en la alimentación.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Trastorno en el sueño.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Adicción<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Derecho al pleno respeto a la integridad del cuerpo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Decidir si se quiere o no tener relaciones sexuales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Embarazos no deseados.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Infecciones de transmisión sexual.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Agresiones físicas que pueden atentar contra tú salud física y emocional.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ""
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>anorexia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>bulimia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>obesidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>fobia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>atracon<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Miedo real a engordar y tienen una imagen distorsianada de las dimensiones y la forma de su cuerpo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se dan grandes atracones de comida y tratan de compensarlo con el vómito inducido o el ejercicio físico excesivo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "El desaquilibrio entre la ingesta de calorías y su uso en actividades físicas produce la acumulación de grasa en las células."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Algunas alimentos pueden ser tentadores y al verlos generan un rechazo por temor a engordar."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Implican patrones de alimentación no saludables que comienzan de manera gradual y llegan al punto de no control."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 0,
                "datoX" : 1,
                "datoY" : 5
            },
            {
                "direccion" : 1,
                "datoX" : 7,
                "datoY" : 2
            },
            {
                "direccion" : 1,
                "datoX" : 5,
                "datoY" : 3
            },
            {
                "direccion" : 1,
                "datoX" : 3,
                "datoY" : 4
            },
            {
                "direccion" : 1,
                "datoX" : 1,
                "datoY" : 5
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3",
            "alto":12
            
        }
    }
];