json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>hadzas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tanzania<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>baobabs<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>chasquidos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>200<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>hombres<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;son una tribu de personas que se ubican en &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, África. Son de los pocos grupos nómadas que sobreviven actualmente. Su supervivencia se basa en la caza y recolección de frutos, semillas, bayas, frutos de los &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y a la recolección de hierbas o pasto seco; recogen la miel que se encuentra en grandes árboles. Hablan una lengua de &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como ninguna en el planeta. Es un grupo de aporximadamente 1000 integrantes aunque solamente &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, de ellos viven bajo las condiciones descritas anteriormente.<br>Los&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; son los que principalmente cazan dedicando entre 4 y 6 horas esta actividad y el resto del día al ocio.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras del recuadro y  completar la siguiente información."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la cultura de los hadza, los hombres y mujeres tienen diferencias notablemente marcadas en relación a la igualdad.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la tribu hadza, sus integrantes dedican la mayor parte de su tiempo a la caza y la recolección, dejando poco tiempo para actividades recreativas. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hadzas viven en comunidades poco numerosas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La tribu hadza es nómada, constantemente cambia de lugar en busca de alimento. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La forma en la que viven los hadzas es un referente para saber cómo es que vivían los seres humanos hace miles de años. ",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Sentencia",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Es la etapa del paleolítico y Homo Erectus, abarca hasta el año 3,500 a.C.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Etapa donde vivió el Homo Sapiens y se pobló Europa, Asia y Oceanía.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "En esta etapa surgen las primeras ciudades como Mesopotamia y Egipto.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Prehistoria"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Edad de Piedra"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Edad de los metales"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los siguientes conceptos:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>metalurgia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>evolución<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Chales Darwin<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>homo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>recolección<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>sedentarios<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>domesticación<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Durante la Prehistoria hubo varias etapas distribuidas en diversas partes del mundo. El ritmo y crecimiento de las poblaciones en la prehistoria, dieron distintos avances; por ejemplo, la agricultura y la &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;surgieron primero en Asia y Europa y después en América.<br>Es durante la prehistoria que se explica la teoría de la &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;del hombre, la cual fue creada por&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y modificada de acuerdo con descubrimientos arqueológicos más recientes. La teoría de Darwin plantea que el hombre proviene de una especie llamada &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, quienes en un principio fueron nómadas dedicadas a la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y caza, para luego establecerse en un lugar y ser &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, dedicándose al cultivo y a la &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de algunos animales.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras del recuadro para completar la siguiente información.",
            "respuestasLargas": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la etapa del Paleolítico se inventa el fuego.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El arte rupestre es una manifestación que surge con los pobladores de la etapa del Paleolítico.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las primeras manifestaciones del hombre como un ser seminómada, surgen en la etapa del Paleolítico.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El hombre de  la etapa Mesolítica se establece por periodos cortos de tiempo cerca de ríos y lagos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el período Neolítico el hombre emigra a diferentes lugares en busca de comida.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La domesticación de animales y la agricultura son características de la etapa Neolítica.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Primero se pobló el continente africano, y después, parte de Europa y Asia. Poco a poco, se fueron trasladando a otros lugares como Oceanía, la mayor parte de Europa y América.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se caracteriza por el dominio del fuego. Quienes habitaron esta etapa, exploraron nuevos territorios. Su alimentación era a base de recolección y caza. Sus viviendas eran al aire libre o en cuevas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hay migraciones a América por el estrecho de Bering. La forma de vida es por medio de la caza y la recolección.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Aparece el Homo Sapiens Sapiens y se consolida como la especie dominante.<\/p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena las fases en que se fue poblando nuestro planeta.<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EHicieron sus herramientas adaptándolas a la mano.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ELos conocemos como los humanos actuales.\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ESe extinguieron sin saber las causas.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ERealizaban pinturas rupestres.\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Selecciona las características correspondientes a los dos grupos de Homo Sapiens:",
            "tipo": "vertical"
        },
        "contenedores": [
            "Homo Sapiens Neanderthalensis",
            "Homo Sapiens Sapiens"

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>homo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Etiopía<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>habilis<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>1,3<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>australophitecus<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los restos fósiles más antiguos que tienen propiamente características humanas, los &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, fueron encontrados hace 2,3 millones de años en Tanzania y &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Aunque los antropólogos consideran que esta especie pudo haber aparecido hace 4 millones hasta 1,3 millones de años. Entre los <i>homo</i> la especia más antiguas es el <i>homo</i>&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;, que vivió hace&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;millones de años, sus restos fósiles fueron encontrados en Tanzania al igual que los del&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;afarensis.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el párrafo arrastrando las palabras al lugar que le corresponda.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Utilización del fuego.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Surgimiento de los Homos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Desarrollo de la caza y recolección para subsistir.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Economía basada en la recolección.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Desarrollo de utensilios creados con piedras.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra los siguientes eventos, ordenándolos:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>México<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Columbia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>10<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>americano<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Alaska<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los mamuts existieron en &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;hasta el año 10 000 mil a.C. se han encontrado fósiles, primordialmente, en la zona del valle de México, ya que era una zona lacustre. De acuerdo con la CONABIO, en México existió la especie mamut de&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, como podrás ver se parece más a un elefante y carece de pelo. Medía hasta 4 metros de altura y podía pesar hasta &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;toneladas. Desapareció hace menos de 8000 años. Fueron cazados hasta el momento de su extinción. Existió otro gran mamífero que es más parecido al mamut lanudo y fue el mastodonte&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;, vivió desde&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;hasta el Salvador.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    }
]