json = [
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_01_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_06.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_04.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_05.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n03_01_07.png",
                "t17correcta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<style>#columnaContenedores{width:72% !important;}\n\
                                   #columnaRespuestas{width:20% !important; margin-top: 50px !important;}</style><p>Arrastra el nombre del continente al lugar que le corresponde en el mapa.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n03_01_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "ocultaPuntoFinal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "316,26", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "58,264", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "396,232", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "76,493", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "220,500", "cuadrado", "133, 63", ".", "transparent"]},
            {"Contenedor": ["", "381,387", "cuadrado", "133, 63", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>África y Europa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>África y Asia<\/p>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div>¿Qué continentes visualizas en la imagen?<br><img src=\"gi5e_b01_n03_02_01.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div>¿Entre que continentes ves en la imagen?<br><img src=\"gi5e_b01_n03_02_02.png\" style='width:200px'\/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Arrastra la ubicación que corresponda a la imagen.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_03_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_05.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_06.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n03_03_07.png",
                "t17correcta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena los continentes de acuerdo a su extensión territorial. Coloca en primer lugar el continente más extenso, y en último lugar, el menos extenso.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n03_03_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "374,195", "cuadrado", "131, 47", ".", "transparent"]},
            {"Contenedor": ["", "422,195", "cuadrado", "131, 47", ".", "transparent"]},
            {"Contenedor": ["", "470,195", "cuadrado", "131, 47", ".", "transparent"]},
            {"Contenedor": ["", "374,372", "cuadrado", "131, 47", ".", "transparent"]},
            {"Contenedor": ["", "422,372", "cuadrado", "131, 47", ".", "transparent"]},
            {"Contenedor": ["", "470,372", "cuadrado", "131, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_04_06.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_04_05.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_04_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_04_03.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_04_02.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n03_04_07.png",
                "t17correcta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el nombre del continente a la imagen con la que se identifique.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n03_04_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false

        },
        "contenedores": [
            {"Contenedor": ["", "125,101", "cuadrado", "149, 44", ".", "transparent"]},
            {"Contenedor": ["", "293,101", "cuadrado", "149, 44", ".", "transparent"]},
            {"Contenedor": ["", "461,101", "cuadrado", "149, 44", ".", "transparent"]},
            {"Contenedor": ["", "125,394", "cuadrado", "149, 44", ".", "transparent"]},
            {"Contenedor": ["", "293,394", "cuadrado", "149, 44", ".", "transparent"]},
            {"Contenedor": ["", "461,394", "cuadrado", "149, 44", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>fronteras<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>naturales<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>artificiales<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los países están separados por líneas imaginarias llamadas <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; las cuales pueden ser <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como océanos o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; como un muro<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras a los espacios que faltan para completar la oración."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_06_03.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_04.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_02.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_05.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_06.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_07.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_09.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_08.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_10.png",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_11.png",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_13.png",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_16.png",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_15.png",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_12.png",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_14.png",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_20.png",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_19.png",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_17.png",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "gi5e_b01_n03_06_18.png",
                "t17correcta": "18"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<style>#columnaContenedores{height:2000px !important;}\n\
                                   #tarjetas li{height:38px !important;}</style>\n\
                            <p>Ubica al país al lugar que le corresponde.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n03_06_01.png",
            "respuestaImagen": true,
            "bloques": true,
            "contieneDistractores": true,
            "opcionesTexto": false,
            "borde": false
        },
        "css":{
            "tamanoFondoColumnaContenedores":"100% 100%"
            
        },
        "contenedores": [
            {"Contenedor": ["", "198,66", "cuadrado", "130, 38", ".", "transparent"]},
            {"Contenedor": ["", "329,24", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "249,215", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "426,191", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "144,488", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "431,21", "cuadrado", "130,38", ".", "transparent"]},
            
            {"Contenedor": ["", "543,322", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "626,459", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "941,165", "cuadrado", "130,38", ".", "transparent"]},
            
            {"Contenedor": ["", "1098,223", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1185,75", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1273,106", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1367,198", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1776,366", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1277,406", "cuadrado", "130,38", ".", "transparent"]},
            
            {"Contenedor": ["", "1437,383", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1687,446", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1819,125", "cuadrado", "130,38", ".", "transparent"]},
            {"Contenedor": ["", "1926,215", "cuadrado", "130,38", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Japón",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "China",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Rusia",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Canadá",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Sudáfrica",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "España",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "México",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "Chile",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "Inglaterra",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "Australia",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "India",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "Alemania",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "Egipto",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "Argentina",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "Estados Unidos",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "Brasil",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "Francia",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "Italia",
                "t17correcta": "18"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Tokio"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Pekín"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Moscú"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ottawa"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Pretoria"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Madrid"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Ciudad de México"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Santiago"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Londres"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Camberra"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Nueva Delhi"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Berlín"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "El Cairo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Buenos Aires"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Washington D.C."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Brasilia"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "París"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Berlín"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "<style>#content{height:2330px !important; width: 102% !important;}</style>Relaciona el país con su capital."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi5e_b01_n03_07_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_05.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_03.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_04.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_07.png",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "gi5e_b01_n03_07_06.png",
                "t17correcta": "5",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca en su lugar el nombre del tipo de línea o punto imaginario.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi5e_b01_n03_07_01.png",
            "respuestaImagen": true,
            "bloques": true,
            "opcionesTexto": true,
            "tamanyoReal": true,
            "borde": false

        },
        "contenedores": [
            {"Contenedor": ["", "12,77", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "202,124", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "309,46", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "461,76", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "84,453", "cuadrado", "152, 47", ".", "transparent"]},
            {"Contenedor": ["", "376,400", "cuadrado", "152, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Indica que tan arriba o abajo está el punto a partir del nivel del mar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se basa en los meridianos y nos dice que tan al este o al oeste a partir del meridiano de Greenwich es esta el punto a localizar.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Está basada en los paralelos y nos dice que tan al norte o al sur a partir del ecuador está el punto que queremos localizar.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Altitud"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Longitud"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Latitud"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el componente de las coordenadas geográficas con su significado.",
            "ocultaPuntoFinal": true
            
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Natural<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Cultural<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Económico<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Político<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Social<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div><img src=\"gi5e_b01_n03_08_02.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><img src=\"gi5e_b01_n03_08_01.png\" style='width:200px'\/><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Selecciona el componente que se relaciona con la imagen.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "México es más grande que Rusia",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En Australia hace más calor que en la Antártida porque está más cerca del ecuador",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En Canadá hace más calor que en Colombia por que está más lejos del ecuador",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una cultura es más parecida a otra que está en el mismo continente que a otra en un continente diferente",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Responde si la siguiente afirmación es verdadera o falsa.<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Las corrientes de viento alcanzan velocidades que superan los 320 km/h. Es el continente con más vientos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "La población cuenta con servicios de salud muy eficientes.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "El día de muertos, es una tradición mestiza.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "En el Estado, se han establecido 10 zonas industriales.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "El río Bravo es una frontera natural entre México y Estados Unidos.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Natural"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Social"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Cultural"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Económico"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Político"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el elemento de diversidad que corresponda a la descripción. "
        }
    }
];