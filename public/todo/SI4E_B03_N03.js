json=[
    {
        "respuestas": [
            {
                "t13respuesta": "El entrevistador termina la entrevista y agradece al entrevistado. En ocasiones elabora algunas conclusiones.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se explica quién será el entrevistado y se hacer una breve semblanza del mismo.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se realizan las preguntas y respuestas. De este momento se obtiene toda la información.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cierre de entrevista"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Presentación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cuerpo de entrevista"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Amor</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Gratitud</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Inquietud</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Enojo</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Apego</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el poema e identifica el sentimiento general que el autor expresó"
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"<p>Universos</p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Perversos</p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Adversos</p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Consuela</p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Cazuela</p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Espuela</p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Abril</p>",
            "t17correcta":"2"
         },
         {
            "t13respuesta":"<p>Sutil</p>",
            "t17correcta":"2"
         },
         {
            "t13respuesta":"<p>gentil</p>",
            "t17correcta":"2"
         },
         {
            "t13respuesta":"<p>Tina</p>",
            "t17correcta":"3"
         },
         {
            "t13respuesta":"<p>Ruina</p>",
            "t17correcta":"3"
         },
         {
            "t13respuesta":"<p>Cortina</p>",
            "t17correcta":"3"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"5",
         "t11pregunta":"",
         "tipo":"vertical"
      },
        "contenedores":[ 
            "Versos",
            "Escuela",
            "Infantil",
            "imagina"
            
        ],css:{"alturaRespuestas": 50, "altoContenedorRespuesta": 50}
   },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pablo es ágil y certero al decidir.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es tan blanca igual a la nieve.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todo el día en el agua cual delfines.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Inquieto como colibrí.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fluyó rápidamente.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>lee<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>reduce<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>consumo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>cuidate<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>reutiliza<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>verifica<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>informate<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>desconfia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>etiquetas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>reflexiona<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>responsable<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida": 16,
            "t11pregunta": "Delta sesion 12 a3"
        }
    }
]