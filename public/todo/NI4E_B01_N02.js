json = [
	{
		"respuestas": [
			{
				"t13respuesta": "Beber agua potable.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Hacer ejercicio regularmente.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "No comer nunca dulces.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Comer las porciones recomendadas en el plato del bien comer.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Prevenir enfermedades.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Evitar los cereales y verduras.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Incluir alimentos de todos los grupos en cada comida.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Evitar a toda costa la chatarra.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Seguir las medidas básicas de higiene antes de comer y después de ir al baño.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Completar tu esquema de vacunación.",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Selecciona las acciones que te conducen a una vida saludable.",
			"columnas": 2
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Fortalecen el sistema inmunológico.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Colaboran en la producción hormonas regulan la temperatura corporal y pueden ser una reserva energética.",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "Transportan oxígeno a la sangre. Tienen acciones estructurales y contribuyen a metabolizar nutrientes.",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "Fuente principal de energía y regulan el tránsito intestinal.",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "Reparan tejidos y permiten el crecimiento de los mismos.",
				"t17correcta": "5"
			},
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Vitaminas"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Lípidos"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Minerales"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Carbohidratos"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Proteínas"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "18",
			"t11pregunta": "Relaciona las columnas"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Pollo",
				"t17correcta": "1",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Lentejas",
				"t17correcta": "0",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Res",
				"t17correcta": "1",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Mantequilla",
				"t17correcta": "0",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Huevo",
				"t17correcta": "1",
				"numeroPregunta": "0"
			},
			{
				"t13respuesta": "Pollo",
				"t17correcta": "0",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Lentejas",
				"t17correcta": "1",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Sardinas",
				"t17correcta": "1",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Arroz",
				"t17correcta": "0",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Espinacas",
				"t17correcta": "1",
				"numeroPregunta": "1"
			},
			{
				"t13respuesta": "Fresa",
				"t17correcta": "1",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Naranja",
				"t17correcta": "1",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Papa",
				"t17correcta": "0",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Kiwi",
				"t17correcta": "1",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Aguacate",
				"t17correcta": "0",
				"numeroPregunta": "2"
			},
			{
				"t13respuesta": "Fresa",
				"t17correcta": "0",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Pasta",
				"t17correcta": "1",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Papa",
				"t17correcta": "1",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Kiwi",
				"t17correcta": "0",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Arroz",
				"t17correcta": "1",
				"numeroPregunta": "3"
			},
			{
				"t13respuesta": "Fresa",
				"t17correcta": "0",
				"numeroPregunta": "4"
			},
			{
				"t13respuesta": "Pasta",
				"t17correcta": "0",
				"numeroPregunta": "4"
			},
			{
				"t13respuesta": "Aguacate",
				"t17correcta": "1",
				"numeroPregunta": "4"
			},
			{
				"t13respuesta": "Mantequilla",
				"t17correcta": "1",
				"numeroPregunta": "4"
			},
			{
				"t13respuesta": "Nueces",
				"t17correcta": "1",
				"numeroPregunta": "4"
			},
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": ["Proteínas", "Minerales", "Vitaminas", "Carbohidratos", "Lípidos"],
			"preguntasMultiples": true,
			"columnas": 2
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Filtración",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Tracción",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Unción",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Ebullición",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Cloración",
				"t17correcta": "1"
			}

		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Marca todos los métodos posibles para potabilizar el agua potable."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Jamón Serrano",
				"t17correcta": "1,2"
			},
			{
				"t13respuesta": "Jamón Serrano",
				"t17correcta": "2,1"
			},
			{
				"t13respuesta": "Jamón de York",
				"t17correcta": "3,4"
			},
			{
				"t13respuesta": "Jamón de York",
				"t17correcta": "4,3"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "¿Qué tipo de Jamón tiene menos sodio?&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<br>¿Cuál aporta más proteínas?&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<br>¿Cuál contiene menos colesterol?&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<br>¿Cuál tiene más vitaminas?&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ""
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Completa el siguiente p&aacute;rrafo.",
			"ocultaPuntoFinal": true,
			"respuestasLargas": true,
            "ocultaPuntoFinal": true
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Completo mi esquema de vacunación</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Consumo alimentos contaminados</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Me alimento equilibrando los distintos grupos de alimentos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Evito moverme</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Bebo agua potable</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Mi alimentación se basa sólo en carbohidratos</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Hago ejercicio</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Bebo agua sin potabilizar</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Cuido no sobrepasar las cantidades de alimentos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Evito las vacunas</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Como alimentos inocuos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Bebo jugos y refrescos comprados</p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Elige las acciones y arrastra al contenedor correspondiente.",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Cuido de mi salud cuando…",
			"Pongo en riesgo mi salud si…"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Hervir",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Lavar",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "lavar y desinfectar",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "cocinar al 100%",
				"t17correcta": "3"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Carne",
				"correcta": "3"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Nopales",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Leche",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Fresas",
				"correcta": "2"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Pollo",
				"correcta": "3"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Lechuga",
				"correcta": "2"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Naranja",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Cilantro",
				"correcta": "2"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Limón",
				"correcta": "1"
			}

		],
		"pregunta": {
			"c03id_tipo_pregunta": "13",
			"t11pregunta": "Selecciona la opción que corresponda para comer y beber sin riesgo de contaminación.",
			"variante": "editable",
			"anchoColumnaPreguntas": 19,
			"evaluable": true
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Botella de agua",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Río",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Manantial",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Pozo",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Lago",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Océano",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Agua de Lluvia",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Rio subterraneo",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Mar",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Elige todas las fuentes de agua dulce que se han de potabilizar para consumo inocuo",
			"columnas": 2

		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Guayaba</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Mandarina</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Kiwi</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Hígado</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Salmon</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Levadura de cerveza</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Leche</p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Almendras</p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>queso</p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Zanahoria</p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>Camote</p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>Melón</p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>Espinaca</p>",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "<p>Lentejas</p>",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "<p>Cordero</p>",
				"t17correcta": "5"
			},
			{
				"t13respuesta": "<p>Cacahuate</p>",
				"t17correcta": "5"
			},
			{
				"t13respuesta": "<p>Germen de trigo</p>",
				"t17correcta": "5"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Arrastra al contenedor los alimentos según correspondan.",
			"tipo": "vertical"
		},
		"contenedores": [
			"Vitamina C",
			"Complejo B",
			"Calcio",
			"Vitamina A",
			"Hierro",
			"Zinc"

		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Tosferina</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Tétanos</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Poliomielitis</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Hepatitis B</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Infección por rotavirus</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Infección por virus de la Influenza</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Sarampión</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Rubéola</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Tuberculosis</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Poliomielitis</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Virus del papiloma humano y cáncer</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Infecciones estomacales</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Tifoidea</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Obesidad</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Diabetes</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Gripa</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Hipertensión</p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Arrastra las acciones al contenedor que corresponde.",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Enfermedades prevenibles por vacunación",
			"Enfermedades prevenibles con buenos hábitos (alimenticios, higiénicos y ejercicio)"
		]
	}
]