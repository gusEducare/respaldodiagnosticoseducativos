json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "0",
                "coordenadas": "150,223"
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "0",
                "coordenadas": "300,223"

            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "1",
                "coordenadas": "400,220"

            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "0",
                "coordenadas": "470,300"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona en el mapa que se muestra a continuación la región que corresponde a México.<br>",
            "secuencia": true,
            "ordenados": false,
            "url": "gi4a_b01_n01_01_01.png",
            "borde": true,
            "ocultaPuntoFinal": true
            

        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Isla<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Península<\/p>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div><img src=\"gi4a_b01_n01_02_04.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><img src=\"gi4a_b01_n01_02_03.png\" style='width:200px'\/><div></div><\/p>"
            },
            {
                 "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p></p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Relaciona cada imagen con su nombre",
            //"textosLargos": "si",
            "ocultaPuntoFinal": true

        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Guatemala<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Estados Unidos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>España<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Belice<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>China<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona los países que colindan con México."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n01_03_03.png",
                "t17correcta": "0",
                "etiqueta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n01_03_02.png",
                "t17correcta": "1",
                "etiqueta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra los nombres al mapa de México en el lugar correspondiente.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n01_03_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "tamanyoReal": true,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "313,55", "", "132, 62", "", "transparent", "true"]},
            {"Contenedor": ["", "179,477", "", "132, 62", "", "transparent", "true"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='gi4a_b01_n01_04_01.png' style='width:160px;'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='gi4a_b01_n01_04_02.png' style='width:160px;'>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src='gi4a_b01_n01_04_03.png' style='width:160px;'>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src='gi4a_b01_n01_04_04.png' style='width:160px;'>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué imagen representa la división política de México?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n01_05_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_08.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_07.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_04.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_05.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_06.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_09.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi4a_b01_n01_05_10.png",
                "t17correcta": "8"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Acomoda el nombre del estado al lugar que corresponda en el mapa de la república mexicana.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n01_05_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true
        },
        "contenedores": [
            {"Contenedor": ["", "174,29", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "254,64", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "313,96", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "377,216", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "41,198", "cuadrado", "113, 40", ".", "transparent", true]},
            {"Contenedor": ["", "109,295", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "182,359", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "249,376", "cuadrado", "114, 40", ".", "transparent", true]},
            {"Contenedor": ["", "306,391", "cuadrado", "114, 40", ".", "transparent", true]},
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n01_06_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n01_06_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n01_06_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n01_06_06.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi4a_b01_n01_06_05.png",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<style>div{background-size:contain;}<style/><p>Ordena los estados de acuerdo a su tamaño. Coloca el de mayor tamaño hasta arriba y el de menor tamaño, hasta abajo.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n01_06_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false
            

        },
        "css":{
            "tamanoFondoColumnaContenedores":"contain"
        },
        "contenedores": [
            {"Contenedor": ["", "64,448", "cuadrado", "179, 42", ".", "transparent"]},
            {"Contenedor": ["", "108,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "152,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "196,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "240,448", "cuadrado", "179, 43", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n01_07_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n01_07_04.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n01_07_03.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n01_07_05.png",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Coloca los puntos cardinales en la rosa de los vientos.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n01_07_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true

        },
        "contenedores": [
            {"Contenedor": ["", "64,272", "cuadrado", "100, 47", ".", "transparent"]},
            {"Contenedor": ["", "239,473", "cuadrado", "100, 50", ".", "transparent"]},
            {"Contenedor": ["", "413,272", "cuadrado", "100, 47", ".", "transparent"]},
            {"Contenedor": ["", "239,71", "cuadrado", "100, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Hermosillo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Mérida<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Xalapa<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>a) La capital de Sonora es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br><br>b) La capital de Yucatán es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br><br>c) La capital de Veracruz es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<\/p>"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa las oraciones.",
            "ocultaPuntoFinal": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n01_09_04.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n01_09_02.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n01_09_03.png",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra la ciudad o lugar turístico a la ubicación que corresponda.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n01_09_01.png",
            "respuestaImagen": true,
            "tamanyoReal": false,
            "bloques": false,
            "borde": false,
            "ocultaPuntoFinal": true,
            "opcionesTexto": true,
            "anchoImagen":80
        },
        "css":{
            "tamanoFondoColumnaContenedores":"contain"            
        },
        "contenedores": [
            {"Contenedor": ["", "173,437", "cuadrado", "127, 45", ".", "transparent"]},
            {"Contenedor": ["", "300,437", "cuadrado", "127, 45", ".", "transparent"]},
            {"Contenedor": ["", "467,264", "cuadrado", "127, 46", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Cultural<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Desierto<\/p>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div><img src=\"gi4a_b01_n01_08_01.png\" style='width:200px' \/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><img src=\"gi4a_b01_n01_08_02.png\" style='width:200px'\/><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><br><div></div><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Relaciona cada imagen con el nombre que le corresponde.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
        
    }
];

