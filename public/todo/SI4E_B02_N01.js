json=[
      {
        "respuestas": [
            {
                "t13respuesta": "<p>El tema deber ser bien delimitado y preciso.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tiene como propósito dejar una vivencia.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Es anónima.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Debe desarrollar aspectos importantes y relevantes acerca del tema.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Trata temas de la vida cotidiana.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es muy extensa.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La redacción debe estar hecha con un lenguaje claro.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Debe ser objetiva, es decir, el investigador no puede emitir opiniones sobre el tema.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La investigación debe respaldarse con fuentes bibliográficas.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las oraciones que hablan de las características que debe tener una monografía."
        }
    },
    {
             "respuestas": [
            {
                "t13respuesta": "<p>¿Cuál es el colmo de un semáforo? Decir “No me mires que me estoy cambiando”.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Era un hombre tan pequeño, pero tan pequeño, que infringía las leyes del tráfico para que los policías le gritaran <i>alto</i>.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><i>La amabilidad es como una almohadilla, que aunque no tenga nada por dentro, por lo menos amortigua los embates de la vida.</i> -Arthur Schopenhauer<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Jarrito nuevo, ¿dónde te pondré?; jarrito viejo ¿dónde te botaré?<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Árbol que nace torcido, jamás su tronco endereza.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "<p>Selecciona las oraciones que sean <i>refranes</i></p>"
        }   
        
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Construcción de un yoyo</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Los yoyos del futuro</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Instructivo <i>cómo jugar al yoyo</i></p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>¿Qué hacer luego del colegio?</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>¡Mi abuela jugaba así!</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["<b>Elige el título más adecuado para el texto.</b><p>Requerimientos:</p><p>Un yoyo</p><p>Instrucciones:</p><ol><li>Pasa el aro del yoyo por tu dedo.</li><li>Impulsa el brazo hacia abajo, soltando el yoyo y abriendo la mano.</li><li>Da un tirón veloz y preciso cuando el hilo del yoyo esté completamente extendido para hacer que regrese.</li><li>Repite el movimiento tantas veces como quieras.</li></li></ol>"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Un instructivo utiliza lenguaje complejo y científico para que puedas llevar a cabo lo que se describe.</p>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Dentro de un instructivo, los procesos están numerados o contienen palabras específicas sobre los pasos a seguir.</p>",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Los verbos que se utilizan en un instructivo, están en modo imperativo e infinitivo.</p>",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>centímetro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>kilómetro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>metros<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>kilogramo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>gramo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>miligramo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>mililitro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>litro<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>metro<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Encuentra las medidas de longitud, peso y  volumen."
        }
    }
];