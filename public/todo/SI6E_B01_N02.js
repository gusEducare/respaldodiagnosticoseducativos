json=[
{
    "respuestas": [
        {
            "t13respuesta": "Como, debido a, gracias a, porque, ya que.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Es decir, o sea, por ejemplo.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "En un principio, mientra tanto; inmediatamente, a la vez.",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "A consecuencia de, por esa razón, por eso, por lo tanto, entonces.",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "A pesar de, al contrario, en cambo, a diferencia de, al igual que, aunque; sin embargo.",
            "t17correcta": "5"
        },
        {
            "t13respuesta": "Para comenzar, en primer lugar, pues entonces; para finalizar, para concluir, finalmente, en resumen, después, en seguida.",
            "t17correcta": "6"
        },
        {
            "t13respuesta": "Incluso, además, y, también, por lo demás.",
            "t17correcta": "7"
        }
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Causa"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Ilustración"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Temporalidad"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Consecuencia"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Oposición"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Orden"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Adición"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "18",
        "t11pregunta": "Trae a tu memoria los tipos de nexos y relaciona ambas columnas."
    }
},   
{
    "respuestas": [
        {
            "t13respuesta": "Preguntas abiertas",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Preguntas cerradas",
            "t17correcta": "1"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El alumno puede hacer uso de diversos argumentos.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Tienen una única respuesta correcta.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Permite que el alumno demuestre con mayor detalle su conocimiento. ",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "No requieren información extra.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Forma rápida de evaluar.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Tienen más de una respuesta correcta.",
            "correcta"  : "0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p>Relaciona cada frase con el tipo de pregunta a la que se refiere.<\/p>",
        "descripcion": "",   
        "variante": "editable",
        "anchoColumnaPreguntas": 60,
        "evaluable"  : true
    }        
},
{
    "respuestas": [
        {
            "t13respuesta": "autobiográfia",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "biográfia",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "entrevista",
            "t17correcta": "0"
        }
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Narración escrita por el protagonista, en ella detalla sus motivaciones y situación de vida."
        },
        {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Narración sobre otra persona, contando desde su nacimiento hasta su muerte y señalando lo que hizo en su vida, sus logros, fracasos, inventos o actividades especiales."
        },
        {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Diálogo entre dos o más personas, en el cual se puede recabar información a traves de preguntas."
        }
    ],
    "pocisiones": [
        {
            "direccion" : 1,
            "datoX" : 7,
            "datoY" : 1
        },
        {
            "direccion" : 0,
            "datoX" : 6,
            "datoY" : 6
        },
        {
            "direccion" : 0,
            "datoX" : 1,
            "datoY" : 12
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "15",
        "t11pregunta": "Resuelve el crucigrama.",
        "alto" : 12
    }
},
{  
  "respuestas":[  
     {  
        "t13respuesta":"Biografías y Monografías",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Autobiografías y novelas históricas",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Biografías y poemas",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Entrevistas y Ensayos",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Biografías y  Autobiografías.",
        "t17correcta":"1"
     }
  ],
  "pregunta":{  
     "c03id_tipo_pregunta":"1",
     "t11pregunta":"Señala los textos que proporcionan información sobre la vida de una persona. Incluyendo datos como lugar en donde nació y vivió, principales experiencias de vida, estudios,  relaciones interpersonales y en general ahondan en detalles de su vida. "
  }
},
{  
  "respuestas":[  
     {  
        "t13respuesta":"Comillas",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Paréntesis",
        "t17correcta":"1"
     },
     {  
        "t13respuesta":"Puntos suspensivos",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Signos de interrogación",
        "t17correcta":"1"
     },
     {  
        "t13respuesta":"Punto y coma",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Signos de admiración",
        "t17correcta":"1"
     },
     {  
        "t13respuesta":"Puntos suspensivos",
        "t17correcta":"0"
     },
     {  
        "t13respuesta":"Dos puntos",
        "t17correcta":"1"
     }
  ],
  "pregunta":{  
     "c03id_tipo_pregunta":"2",
     "t11pregunta":"Señala los signos de puntuación más utilizados en la escritura de guiones de radio"
  }
},
{
    "respuestas": [
        {
            "t13respuesta": "Su función es comprobar que la persona realmente aprendió lo que se enseñó en clases y prentende desarrollar la argumentación del estudiante.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Su función es identificar el aprendizaje que obtuvo cada estudiante para luego medirlos resultados de toda el aula.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "Su función es conocer el nivel de aprendizaje que obtuvo cada persona por separado del resto de los estudiantes ademas de permitirles que expresen con sus propias palabras lo que aprendieron.",
            "t17correcta": "3"
        }
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Exámenes de desarrollo"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Exámenes escritos"
        },
        {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Exámenes orales"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "18",
        "t11pregunta": "Relaciona el tipo de examen con su función."
    }
},
{
    "respuestas": [
        {
            "t13respuesta": "musicalización",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "comprensible",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "entonación",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "productor",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "lenguaje",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "microfono",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "operador",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "locutor",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "técnico",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "volumen",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "sonido",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "básico",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "radio",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "guion",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "aire",
            "t17correcta": "0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "16",
        "t11medida": 16,
        "t11pregunta": "Localiza en la sopa de letras 16 palabras relacionadas con un programa radiofónico. "
    }
},
{
    "respuestas": [
        {
            "t13respuesta": "Lee todo el examen",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Trata de estudiar hasta el último minuto antes del examen.",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Ubica las preguntas más sencillas y resuélvelas primero.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Lee con atención las indicaciones e instrucciones.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Busca la oportunidad de comparar tus respuestas con las de tus compañeros.",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "En los  reactivos de opción múltiple, descarta primero las respuestas incorrectas.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Para responder las preguntas abiertas; organiza primero, incluso puedes escribirlas aparte, las ideas principales que vas a desarrollar; asegúrate de que estas dan respuesta a lo que se solicita.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Respira rápidamente y muéstrate muy nervioso.",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Una vez concluido el examen, haz una lectura final y modifica lo que consideres necesario. Revisa también si respondiste todas las preguntas.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Intenta utilizar tus apuntes.",
            "t17correcta": "0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "2",
        "t11pregunta": "Señala todo lo que debes considerar para  responder un examen."
    }
},
{
    "respuestas": [
        {
            "t13respuesta": "<p>Encabezado</p>",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "<p>Introducción</p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>Entradilla</p>",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "<p>Resumen</p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>Cortinillas</p>",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "<p>Índice</p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>Secciones</p>",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "<p>Conclusión</p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>Cuñas</p>",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "<p>Cierre</p>",
            "t17correcta": "0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "5",
        "t11pregunta": "Arrastra los elementos al contenedor que les corresponde.",
        "tipo": "horizontal"
    },
    "contenedores":[ 
        "Elementos que integran un guion radiofonico",
        "Elementos que no integran un guion radiofonico"
    ]
},
  {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los reactivos nunca tienen relación con los aprendizajes.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un reactivo es una instrucción, pregunta, problema, ejercicio o problema  que se presentan en un examen.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se nombra reactivo a una composición literaria que data del S XVII.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uno de los propósitos de los reactivos es evaluar los conocimientos o destrezas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los reactivos se realizan con base en la información de mayor relevancia sobre un tema.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre los tipos de reactivos están: preguntas abiertas, preguntas cerradas, jerarquización, falso y verdadero, Relación, respuesta breve y Opción múltiple.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los reactivos de los exámenes son elaborados en la Secretaría de Educación Pública.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona falso o verdadero según corresponda.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    }
];