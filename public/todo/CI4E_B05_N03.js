[
    {
        "respuestas": [
            {
                "t13respuesta": "Abuso de alguna autoridad frente a un grupo vulnerable",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Conflictos religiosos por la falta de tolerancia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Falta de oportunidades para un pueblo o comunidad",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Equidad e igualdad en oportunidades para los miembros de un país",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Injusticias sociales como la pobreza",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cumplimiento de los derechos humanos universales",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona los elementos que pueden ser causas para la falta de paz o que son detonantes para la guerra."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los conflictos pueden favorecer la violencia si las partes afectadas no se tratan con respeto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La violencia debe combatirse con violencia.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Han existido líderes en el mundo que han podido combatir la violencia con demostraciones de paz.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen expresiones de violencia que se dan en nuestro país como los secuestros, los homicidios o la tortura.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos humanos pueden ser válidos dependiendo del país o la región.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Tratar un asunto para llegar a un acuerdo o solución.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Poner de acuerdo sobre algo a dos o más personas entre las que hay desacuerdo, enfrentamiento o lucha.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Discusión sobre un asunto o sobre un problema con la intención de llegar a un acuerdo o de encontrar una solución.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Negociación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Conciliación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Diálogo"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno subsidia a las personas morales para que puedan crear sindicatos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las asociaciones religiosas pueden ser empresas lucrativas que dependen del gobierno.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una nación está constituida por un gobierno y la sociedad civil.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando la sociedad civil se organiza puede obligar al gobierno a atender alguna demanda apoyando a alguna fundación o institución no lucrativa.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las iglesias son asociaciones con fines de lucro que pueden llegar a ser autosuficientes, es decir, pueden existir sin el patrocinio del gobierno.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Aspectos a valorar",
            "evaluable": true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Sector lucrativo",
            "Sector no lucrativo"
        ]
    }
]