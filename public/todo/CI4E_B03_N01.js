[
       {
        "respuestas": [
            {
                "t13respuesta": "<p>Trato idéntico que un organismo, estado, grupo o individuo brinda a las personas sin que medie ningún tipo de discriminación por raza, sexo, clase social u otra circunstancia.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se relaciona con la justicia social, ya que defiende las mismas condiciones y oportunidades para todas las personas, sin distinción, pero adaptándose en los casos particulares; es decir, se trata de dar a cada uno lo que requiere, aunque sea distinto a lo de otro.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor la palabra que define el concepto.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Equidad",
            "Igualdad"
        ]
    },
           {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor la palabra que define el concepto.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Imagenes que identificas México",
            "Imagenes que no Identificas México"
        ]
    },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Opiniones",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Perjuicios",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Discusiones",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Juzgar las cosas sin tener conocimiento fundamentado, provoca opiniones sin justificación y origina..."
      }
   },
     {
        "respuestas": [
            {
                "t13respuesta": "Las opciones de otros de otros son tan importantes como las propias.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Todos los tenemos sin distición alguna de lugar de procedencia, economía, sexo, origen, etnia, color, religión o lengua",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se violeta cuando los niños de familias de escasos recursos que viven en pueblos rurales mueren por destrucción y otra derivadas de la pobreza",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Todos los tatuados son delicuentes",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Respeto"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Derecho"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Dignidad humana"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Prejuicio"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    }, 
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Olmeca<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Maya<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Totonaca<\/p>",
                "t17correcta": "0"
            },
              {
                "t13respuesta": "<p>Tolteca<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Zapoteca<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Huasteca<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mexica<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    }    
]