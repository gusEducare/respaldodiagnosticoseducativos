json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Lenguaje claro y con palabras adecuadas al tipo de público.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No es necesario que hagas un trabajo previo de investigación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Parafrasear los tecnicismos del tema para garantizar la comprensión del mismo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Una vez que eliges fuentes de consulta expertas y actualizadas debes separar la información principal de la secundaría.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Desarrolla tu exposición con palabras muy técnicas y de difícil comprensión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Realizar una investigación profunda con varias fuentes de consulta serias.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tu exposición se puede basar en información irrelevante; en tanto, seas simpático.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Seleccionar la información relevante del tema de acuerdo al enfoque de tu exposición.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Con dos horas de exposición es suficiente.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No hacer una exposición de más de 20 minutos.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Arrastra al contenedor lo que debes tomar en cuenta para una exposición oral."
        }
    },
    {
      "respuestas": [
          {
              "t13respuesta": "<p>Haikú</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Poesía concreta</p>",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Caligrama</p>",
              "t17correcta": "2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "opcionesTexto": true,
          "tamanyoReal": true,
          "bloques":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "20,10", "cuadrado", "180, 40", ".","transparent"]},
          {"Contenedor": ["", "220,10", "cuadrado", "180, 40", ".","transparent"]},
          {"Contenedor": ["", "420,10", "cuadrado", "180, 40", ".","transparent"]}
      ]
  },
  {
        "respuestas": [
            {
                "t13respuesta": "<p>Ofrece argumentos para sustentar solicitudes, demandas o aclaraciones.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Relatos de tu forma de vida.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Uso de lenguaje formal y expresiones de cortesía.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Empleo de lenguaje informal.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se redactan en segunda persona para crear una distancia formal e impersonal en la comunicación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Uso de lenguaje coloquial.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Especificación en el destinatario.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Incluir antecedentes, explicación de motivos, planteamiento de la situación y posibles propuestas.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor las caracteristicas de una carta formal",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Son caracteristicas de una carta formal",
            "No son caracteristicas de una carta formal"
        ]
    }
]


