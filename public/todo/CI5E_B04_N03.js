[
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Toma en cuenta la opinión de todos los involucrados.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Cada persona involucrada expresa por escrito su opinión.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Aplican para todos sin distinción alguna.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Toma en cuenta la opinión de los personajes muy relevantes en la sociedad.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Debe actualizarse constantemente para garantizar que los acuerdos tomados son lo que la mayoría requiere.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Toma en cuenta las exigencias de los empresarios.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"De entre las siguientes identifica las que son características de los acuerdos democráticos."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Principio de soberanía popular.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Principio de alimentación para todos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Principio de representación política.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Principio de la mayoría y defensa de los derechos de las minorías.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Principio de equidad en educación.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona principios democráticos."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Participando activamente en los procesos democráticos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Fundamentar tu decisión después de conocer las opciones posibles.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Votar justo como te late en ese momento.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Contribuir con la resolución aunque cuando no sea lo que tú esperabas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Realizar acciones encaminadas a la participación de todos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Si la resolución no te favorece puedes evadir hacer lo que se recomienda.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Evitando la discriminación y violencia a los que opinan distinto a ti.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Señala todo lo que debes hacer para favorecer la democracia en los ámbitos en que te desarrollas."
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Aristocracia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Democracia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>IMonarquía<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Oligarquía<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Oclocracia o Demagogia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tiranía<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Forma de gobierno aceptada",
            "Forma de gobierno no aceptada"
        ]
    },
      {  
      "respuestas":[  
         {  
            "t13respuesta":"Ley general de sociedades mercantiles.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Declaración Universal de Derechos Humanos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Reglamento interno de la secretaria de Gobernación.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Constitución política de los estados unidos mexicanos.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Cómo se le llama al documento rector de nuestra república? En él se expresan las leyes supremas del País; Se establece su organización, funcionamiento y estructura política, además de fundar los derechos y garantías de todos los habitantes de México."      
     }
   }
]