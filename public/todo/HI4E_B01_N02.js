json=[
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EProceso en el que los humanos aprendieron a cultivar la tierra.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPeriodo en el cual hubo grandes formaciones de masas de hielo.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EProceso que permitió unir dos continentes a lo largo de muchos años.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETiempo en el que los nómadas cruzaron de un continente a otro.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EProceso natural de deshielo en el planeta.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la opción que habla sobre lo que es una glaciación."
      }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPorque se han encontrado pinturas de los primeros hombres en ese lugar.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque se han encontrado esqueletos y objetos en esa región.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque lo dicen libros muy antiguos.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque ahí existían más animales que se cazaban para comer.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":" ¿Por qué se cree que el ser humano surgió en el continente Africano?"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "365 días",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "5 años",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "10 años",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "100 años",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "1000 años",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Año"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Lustro"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Década"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Siglo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Milenio"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona la medida de tiempo con su valor:"
        }
    },
    {
      "respuestas": [
          {
              "t13respuesta": "hi4e_b01_n02_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "hi4e_b01_n02_03.png",
              "t17correcta": "1,2,3",
              "columna":"0"
          },
          {
              "t13respuesta": "hi4e_b01_n02_04.png",
              "t17correcta": "2,3,1",
              "columna":"1"
          },
          {
              "t13respuesta": "hi4e_b01_n02_05.png",
              "t17correcta": "3,1,2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra el evento a la etapa que le corresponda en la línea del tiempo:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"hi4e_b01_n02_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "322,7", "cuadrado", "150, 54", ".","transparent"]},
          {"Contenedor": ["", "151,168", "cuadrado", "150, 54", ".","transparent"]},
          {"Contenedor": ["", "322,330", "cuadrado", "150, 54", ".","transparent"]},
          {"Contenedor": ["", "146,488", "cuadrado", "150, 54", ".","transparent"]}
      ]
  },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPorque en ese momento, ese istmo estaba congelado y pudieron pasar de un continente al otro.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque la vida surgió muy cerca del Estrecho de Bering y se facilitó su traslado.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque se encontraron cuevas con pinturas rupestres que representaban el paso por el Estrecho de Bering.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Por qué se cree que las primeras migraciones ocurrieron a través del Estrecho de Bering?"
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Aridoamérica<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Oasisamérica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mesoamérica<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>a) En el territorio de "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", el clima es cálido y seco que no permitía la agricultura. Su población era básicamente de nómadas, tenían un gran conocimiento de la flora y fauna desértica. Se ubica en Baja California, Sonora, Durango, etc.<br>b)Territorio caluroso pero con algunas lluvias, por lo que la agricultura era de “temporal”. En <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, las culturas que se desarrollaron fueron: Anasazi, Pataya, Hohokam y Mogollón.<br>c)<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, es un territorio con abundantes ríos, lagos y vegetación que ayudaron a que los pobladores fueran sedentarios, desarrollaron la agricultura, específicamente del maíz.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona el área cultural del México antiguo que corresponde con su definición:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Incas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Trinchera<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Chichimecas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Esquimales<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Apaches<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona 3 culturas que hayan formado parte de los primeros pobladores de nuestro país."
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EProvenían de Bavaria.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEran pueblos que peleaban entre ellos por la comida.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EUsaban barbas y cabello muy largo.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ESe ubicaron en una zona llamada de esa forma.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Los bárbaros eran llamados así porque…"
      }
   }
]