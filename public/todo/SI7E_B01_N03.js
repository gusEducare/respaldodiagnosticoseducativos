json = [
      {  
      "respuestas":[  
         {  
            "t13respuesta":"¿Qué hortalizas son más nutritivas?",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"¿Por qué se usan pesticidas en sembradíos de hortaliza?",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"¿Cuántas toneladas de espinaca y calabaza se cosechan por temporada?",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"¿Qué son los pesticidas?",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Elige dos preguntas guía que te permitan comenzar una investigación respecto al uso de pesticidas en sembradíos de hortalizas"
      }
   },
    {"respuestas": [
            {
                "t13respuesta": "<p>comparacion<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>parafrasis<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>fichas de trabajo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>citar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>analogia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>resumen<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>nexos<\/p>",
                "t17correcta": "0"
            }

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Rescursos literario que pretenden relacionar dos ideas por su semejanza o por su oposición."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "La realizamos cuando repetimos con nuestras propias palabras la información que hemos leido o escuchado.Para realizarla debemos ubicar las ideas principales del texto."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Textos que presentan información relevante, organizada y seleccionada sobre un tema en particular, puede ser de parafrasis, resumen o de cita textual."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Copiar de manera exacta lo que se ha leido o escuchado, no debemos cambiar no una palabra dicha por el autor origina y siempre se escribe entre comillas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Recursos literario que establece relaciones de semejanza entre elementos diferentes."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Selección de las ideas principales de un texto, siempre se deben escribir con objetividad. Ayuda a recordar y a organizar la información sin tomar en cuenta detalles poco relevantes."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se utilizan para unir oraciones y establecer una relación entre las mismas. Pueden ser de varios tipos: de causa, consecuencia, ejemplificación, oposición, igualdad, explicación y temporalidad."
            }
        ], "pocisiones": [
            {
                "direccion": 1,
                "datoX": 13,
                "datoY": 0
            },
            {
                "direccion": 1,
                "datoX": 5,
                "datoY": 3
            },
            {
                "direccion": 0,
                "datoX": 1,
                "datoY": 4
            },
            {
                "direccion": 0,
                "datoX": 13,
                "datoY": 7
            },
            {
                "direccion": 0,
                "datoX": 3,
                "datoY": 9
            },
            {
                "direccion": 0,
                "datoX": 3,
                "datoY": 12
            },
            {
                "direccion": 1,
                "datoX": 9,
                "datoY": 12
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 16,
            "ancho":17,
            "t11pregunta": "Responde el siguiente crucigrama."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <hr style='padding:0px; margin:0px; background-color:black; height:3px;'>\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
    
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "¿Qué es un esquema?"
        }
    },
     {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Referencia de página de Internet</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo de periódico digital</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo en periódico impreso</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Referencia de página de Internet</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo de periódico digital</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo en periódico impreso</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Referencia de página de Internet</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo de periódico digital</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo en periódico impreso</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Referencia de página de Internet</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Referencia de libro en versión electrónica</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo en periódico impreso</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Referencia de libro con dos o tres autores</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Referencia de libro en versión electrónica</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Referencia de artículo en periódico impreso</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Referencia de libro con dos o tres autores</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Referencia de libro en versión electrónica</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Referencia de libro</p>",
            "t17correcta":"1",
            "numeroPregunta":"5"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona la cita según corresponda con el material consultado.<br/><img src='si7e_b01_n03_07.png'>",
             "<img src='si7e_b01_n03_08.png'>",
             "<img src='si7e_b01_n03_09.png'>",
             "<img src='si7e_b01_n03_10.png'>",
             "<img src='si7e_b01_n03_11.png'>",
             "<img src='si7e_b01_n03_12.png'>",
         ],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El espacio y tiempo de estos relatos no está en nuestra historia.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No tienen ninguna explicación científica.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tienen vínculos religiosos y no se cuestiona su veracidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Intentaron explicar todo un entorno.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se divulgaron de forma oral, de generación en generación.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Suelen tener referencias históricas concretas haciendo referencia al lugar y tiempo al que pertenecen<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sus personajes se basan en personas reales aunque llevadas a la ficción.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son parte del folclore de una cultura.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se limita a contar relatos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tienen carácter didáctico y de moraleja.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Trasmitidas de forma oral de generación en generación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aun teniendo una base histórica desarrolla elementos fantásticos y sobrenaturales.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada contenedor las frases según corresponda",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Mito",
            "Leyenda"
        ]
    }
]