json =[
{
	"respuestas": [
	{
		"t13respuesta": "Trescientos cuarenta y cinco mil seiscientos diecisiete",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Doscientos treinta y cuatro millones quinientos cuarenta y seis mil ochenta y dos.",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "Tres millones cuatrocientos cincuenta y seis mil ciento siete",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "Doscientos treinta y cuatro millones quinientos cuarenta y seis mil ochocientos dos.",
		"t17correcta": "4"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "345617"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "234546082"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "3456107"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "234546802"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Relaciona cada número con su nombre."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<img src='mi6e_b01_n01_02_01.png'>",
		"t17correcta": "1,2"
	},
	{
		"t13respuesta": "<img src='mi6e_b01_n01_02_01.png'>",
		"t17correcta": "2,1"
	},
	{
		"t13respuesta": "<img src='mi6e_b01_n01_02_02.png'>",
		"t17correcta": "3"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p> En &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de un terreno se sembró jitomate y en &nbsp;&nbsp;&nbsp;&nbsp; se sembró tomate, ¿qué parte del terreno queda disponible para sembrar lechugas?</p>\n\
		<sup class='fraction' style='position: relative; display: inline-block; top: -42px; left:-120px; font-size:23px; line-height: 20px;' >\n\
		<sup class='top'  style='display:block;border-bottom: 2px solid black'>2</sup><sup class='bottom' style='width:1.3%;'>3</sup></sup> \n\
		<sup class='fraction'  style='position: relative; display: inline-block; top: -40px; left:195px; font-size:23px; line-height: 20px;' >\n\
		<sup class='top' style='border-bottom: 2px solid black; display:block;'>1</sup>\n\
		<sup class='bottom' style=' display: inline-block; width:1.3%;'>4</sup></sup> <p>Terreno ocupado: &nbsp;&nbsp;&nbsp; + </p> \n\
		<sup class='fraction' style='position: relative; display: inline-block; top: 27px; left:-35px; font-size:23px; line-height: 20px;'>\n\
		<sup class='top' style='border-bottom: 2px solid black; display:block;'>2</sup>\n\
		<sup style='display: inline-block;'>3</sup></sup><sup class='fraction' style='position: relative; display: inline-block; top: 27px; left:-10px; font-size:23px; line-height: 20px;'>\n\
		<sup class='top'style='border-bottom: 2px solid black; display:block'>1</sup>\n\
		<sup>4</sup></sup>= &nbsp;&nbsp;"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "&nbsp; <br><br><br> * Terreno disponible: 1 - "
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "&nbsp;= &nbsp;"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "&nbsp;"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "Completa el siguiente p&aacute;rrafo."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>63<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>475<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>1750<\/p>",
		"t17correcta": "3"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "a) Una piña cuesta $18, ¿cuánto se debe pagar por 3 \n\
		<sup class='fraction' style='position: relative; display: inline-block; top: 25px; font-size:23px; line-height: 20px;' >\n\
		<sup class='top' style='display:block;border-bottom: 2px solid black'>1</sup><sup style=''>2</sup></sup> \n\
		&nbsp;piñas? $&nbsp;"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "&nbsp;<br> b) Un tinaco puede almacenar 100 L de agua, ¿qué cantidad de agua hay si se llenaron 4 tinacos y &nbsp; \n\
		<sup class='fraction' style='position: relative; display: inline-block; left:-5px; top: 27px; font-size:23px; line-height: 20px;' >\n\
		<sup class='top' style='display:block;border-bottom: 2px solid black'>3</sup><sup>4</sup></sup> partes de otro?&nbsp;"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "&nbsp;<br>c) Una pieza de queso pesa 750 g, ¿cuánto pesan 2 \n\
		<sup class='fraction' style='position: relative; display: inline-block; top: 27px; font-size:23px; line-height: 20px;' >\n\
		<sup class='top' style='display:block;border-bottom: 2px solid black'>1</sup><sup>3</sup></sup> \n\
		&nbsp;piezas de queso?&nbsp;"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "&nbsp;"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "Completa el siguiente p&aacute;rrafo."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "MI6E_B01_N01_03_01.png",
		"t17correcta": "0,1",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_03_02.png",
		"t17correcta": "1,0",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_03_03.png",
		"t17correcta": "",
		"columna":"1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "<p>Coloca en el contenedor la figura sobre la cual está trazado un eje de simetría<\/p>",
		"tipo": "ordenar",
		"imagen": true,
		"url":"MI6E_B01_N01_03_04.png",
		"respuestaImagen":true, 
		"bloques":false,
		"tamanyoReal":true,
		"opcionesTexto":true,
		"borde":false,
		"contieneDistractores":true
		
	},
	"contenedores": [
	{"Contenedor": ["true", "113,182", "cuadrado", "130, 140", ".","transparent","true"]},
	{"Contenedor": ["", "112,363", "cuadrado", "130, 140", ".","transparent","true"]}
	]
},
{
	"respuestas": [
	{
		"t13respuesta": "MI6E_B01_N01_07.png",
		"t17correcta": "0",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_08.png",
		"t17correcta": "1",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_09.png",
		"t17correcta": "2",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_10.png",
		"t17correcta": "3",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_11.png",
		"t17correcta": "4",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_12.png",
		"t17correcta": "5",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_13.png",
		"t17correcta": "14",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_14.png",
		"t17correcta": "13",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_15.png",
		"t17correcta": "12",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_16.png",
		"t17correcta": "11",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_17.png",
		"t17correcta": "10",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_18.png",
		"t17correcta": "9",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_19.png",
		"t17correcta": "8",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_20.png",
		"t17correcta": "7",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_21.png",
		"t17correcta": "6",
		"columna":"1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "<p>Etiqueta las filas y las columnas de manera que el cuadrado señalado corresponda al lugar indicado. Puedes etiquetar de izquierda a derecha, arriba abajo, o viceversa.<\/p>",
		"tipo": "ordenar",
		"imagen": true,
		"url":"MI6E_B01_N01_01.png",
		"respuestaImagen":true, 
		"bloques":false,
		"tamanyoReal":true,
		"opcionesTexto":true,
		"borde":false
	},
	"contenedores": [
	{"Contenedor": ["", "9,358", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,402", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,446", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,490", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,534", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,578", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "52,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "96,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "139,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "183,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "226,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "270,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "314,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "357,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "401,315", "cuadrado", "40, 40", ".","transparent"]}
	]
},
{
	"respuestas": [
	{
		"t13respuesta": "MI6E_B01_N01_07.png",
		"t17correcta": "5",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_08.png",
		"t17correcta": "4",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_09.png",
		"t17correcta": "3",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_10.png",
		"t17correcta": "2",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_11.png",
		"t17correcta": "1",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_12.png",
		"t17correcta": "0",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_13.png",
		"t17correcta": "6",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_14.png",
		"t17correcta": "7",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_15.png",
		"t17correcta": "8",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_16.png",
		"t17correcta": "9",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_17.png",
		"t17correcta": "10",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_18.png",
		"t17correcta": "11",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_19.png",
		"t17correcta": "12",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_20.png",
		"t17correcta": "13",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_21.png",
		"t17correcta": "14",
		"columna":"1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "<p>Etiqueta las filas y las columnas de manera que el cuadrado señalado corresponda al lugar indicado. Puedes etiquetar de izquierda a derecha, arriba abajo, o viceversa.<\/p>",
		"tipo": "ordenar",
		"imagen": true,
		"url":"MI6E_B01_N01_02.png",
		"respuestaImagen":true, 
		"bloques":false,
		"tamanyoReal":true,
		"opcionesTexto":true,
		"borde":false
	},
	"contenedores": [
	{"Contenedor": ["", "9,358", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,402", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,446", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,490", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,534", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,578", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "52,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "96,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "139,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "183,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "226,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "270,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "314,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "357,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "401,315", "cuadrado", "40, 40", ".","transparent"]}
	]
},
{
	"respuestas": [
	{
		"t13respuesta": "MI6E_B01_N01_07.png",
		"t17correcta": "5",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_08.png",
		"t17correcta": "4",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_09.png",
		"t17correcta": "3",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_10.png",
		"t17correcta": "2",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_11.png",
		"t17correcta": "1",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_12.png",
		"t17correcta": "0",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_13.png",
		"t17correcta": "14",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_14.png",
		"t17correcta": "13",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_15.png",
		"t17correcta": "12",
		"columna":"0"
	},
	{
		"t13respuesta": "MI6E_B01_N01_16.png",
		"t17correcta": "11",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_17.png",
		"t17correcta": "10",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_18.png",
		"t17correcta": "9",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_19.png",
		"t17correcta": "8",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_20.png",
		"t17correcta": "7",
		"columna":"1"
	},
	{
		"t13respuesta": "MI6E_B01_N01_21.png",
		"t17correcta": "6",
		"columna":"1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "<p>Etiqueta las filas y las columnas de manera que el cuadrado señalado corresponda al lugar indicado. Puedes etiquetar de izquierda a derecha, arriba abajo, o viceversa.<\/p>",
		"tipo": "ordenar",
		"imagen": true,
		"url":"MI6E_B01_N01_03.png",
		"respuestaImagen":true, 
		"bloques":false,
		"tamanyoReal":true,
		"opcionesTexto":true,
		"borde":false
	},
	"contenedores": [
	{"Contenedor": ["", "9,358", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,402", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,446", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,490", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,534", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "9,578", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "52,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "96,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "139,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "183,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "226,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "270,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "314,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "357,315", "cuadrado", "40, 40", ".","transparent"]},
	{"Contenedor": ["", "401,315", "cuadrado", "40, 40", ".","transparent"]}
	]
}
];