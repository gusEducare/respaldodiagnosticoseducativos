json = [  
     {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la reproducción sexual de las plantas participan dos sexos: uno masculino y otro femenino",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para la reproducción sexual de las plantas se requiere de polinización",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La floración es fundamental en la reproducción sexual de las plantas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La única forma de reproducción en las plantas se relaciona con polinización.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
             
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la reproducción asexual de las plantas un ser vivo por sí mismo puede generar otro idéntico a él.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la reproducción asexual un fragamento de los tallos, hojas y raíces se separa de la planta madre y generan otro ser igual a su progenitor.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
             
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>La flor se abre ofreciendo su polen y néctar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Intervienen los polinizadores y transportan el polen hacia otras flores.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Algunos animales se alimentan de dichos frutos y favorecen el esparcimiento de semillas.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Fecundado el ovulo se forma los frutos y las semillas.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>La planta genera flores.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Las semillas producen nuevas plantas.<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>EL polen es llevado al pistilo para fecundar al óvulo.<\/p>",
                "t17correcta": "6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 6", "201,551", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 7", "201,551", "cuadrado", "134, 73", "."]}
        ]
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAyudan al crecimiento de diversos organismos.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFertilizan alimentos para consumo animal.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECausan enfermedades en distintos organismos.\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Los hongos parásitos:"
      }
   },{  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ESe crecimiento es nocturno y sólo se reproducen en luna nueva.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDescomponen a las plantas y animales que mueren.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECausan enfermedades.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Hongos saprófitos:"
      }
   },{  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ESe asocian con otros organismos y se benefician mutuamente.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ESon aquellos con forma de estrella.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ESólo viven en calor.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Los hongos simbióticos:"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ecosistemas se forman por medio físico y seres vivos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ser humano no pertenece a ningún ecosistema",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los habitantes de un ecosistema se determinan por el medio físico del ecosistema",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El medio físico de un ecosistema sólo se relaciona con el tipo de suelo",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
             
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },{
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El medio físico de un ecosistema depende de factores como clima, temperatura, tipo de suelo y el relieve",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los ecosistemas cuentan con los mismos seres vivos",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
   
]