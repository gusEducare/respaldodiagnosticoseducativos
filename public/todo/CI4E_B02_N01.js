json=[  
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El enojo es un sentimiento que puede ser controlado si pensamos antes de actuar. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al perder el control, se puede herir  o lastimar a la gente que queremos. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al respirar profundo o decir una palabra tranquilizadora,se tiene la certeza de que no nos vamos a enojar o diremos cosas que puedan lastimar a otro.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Libertad individual",
            "Libertad colectiva"
        ]
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Comisión Nacional de derechos humanos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Declaración de la Haya",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Constitución política de los Estados Unidos Mexicanos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Carta de los derechos de niños, niñas y adolescentes.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"En México ¿Cuál es el máximo documento que garantiza nuestros derechos a libertades individuales y colectivas?"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Tirando la basura en su lugar.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Respetando las diferentes opiniones, aun cuando sean contrarias a la tuya.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Ordenando el material para trabajar.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Solucionar conflictos a partir del diálogo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Denunciar las  injusticias.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Ser empático con otros y escuchar sus razones.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Señala todas las formas en que puedes contribuir con la justicia en tu escuela."
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En caso de que no cumplas una de tus metas no lograrás ninguna otra.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lo mejor para llegar a cumplir tu meta es dividirla en objetivos más pequeños, así podrás ver el avance y darás pasos firmes para lograr tu meta principal.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En ocasiones las metas no se cumplen por llevar una planeación inadecuada.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tus metas pueden ser en diversas áreas, físicas, emocionales, académicas; entre otras.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las metas únicamente las cumplen las personas más listas",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false,
            "anchoColumnaPreguntas": 70
        }
    }
];