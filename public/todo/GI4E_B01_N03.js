json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "0",
                "coordenadas": "94,143"
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "0",
                "coordenadas": "188,143"

            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "1",
                "coordenadas": "250,140"

            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "0",
                "coordenadas": "347,270"
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "0",
                "coordenadas": "100,646"

            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "0",
                "coordenadas": "178,420"

            },
            {
                "t13respuesta": "<p>7<\/p>",
                "t17correcta": "0",
                "coordenadas": "210,691"

            },
            {
                "t13respuesta": "<p>8<\/p>",
                "t17correcta": "0",
                "coordenadas": "395,785"

            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona en el mapa que se muestra a continuación la región que corresponde a México.",
            "secuencia": true,
            "ordenados": false,
            "url": "gi4a_b01_n03_01_01b.png",
            "borde": true

        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_02_03.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_08.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_09.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_07.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_10.png",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_04.png",
                "t17correcta": "5",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_06.png",
                "t17correcta": "6",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_02.png",
                "t17correcta": "7",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_02_05.png",
                "t17correcta": "8",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra las islas y penínsulas al lugar que le corresponde. <\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n03_02_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "borde": false,
            "tamanyoReal": true

        },
        "contenedores": [
            {"Contenedor": ["", "48,12", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "99,9", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "197,9", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "372,94", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "129,109", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "172,143", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "346,219", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "259,532", "cuadrado", "90, 30", ".", "transparent"]},
            {"Contenedor": ["", "331,549", "cuadrado", "90, 30", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Guatemala<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Honduras<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Estados Unidos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Canadá<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Belice<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El Salvador<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Panamá<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "¿Qué países son vecinos de México?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_03_05.png",
                "t17correcta": "0",
                "etiqueta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_03_04.png",
                "t17correcta": "1",
                "etiqueta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n03_03_03.png",
                "t17correcta": "2",
                "etiqueta": "3"
            },
            {
                "t13respuesta": "gi4a_b01_n03_03_02.png",
                "t17correcta": "3",
                "etiqueta": "4"
            },
            {
                "t13respuesta": "gi4a_b01_n03_03_06.png",
                "t17correcta": "4",
                "etiqueta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra los golfos, mares y océanos de México al lugar correspondiente.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n03_03_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "tamanyoReal": true,
            "borde": false


        },
        "contenedores": [
            {"Contenedor": ["", "315,20", "", "132, 62", "", "transparent"]},
            {"Contenedor": ["", "408,102", "", "132, 62", "", "transparent"]},
            {"Contenedor": ["", "121,319", "", "132, 62", "", "transparent"]},
            {"Contenedor": ["", "200,494", "", "132, 62", "", "transparent"]},
            {"Contenedor": ["", "442,463", "", "132, 62", "", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E México se divide en 32 estados \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E México se divide en 24 estados y un distrito federal \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E  México se divide en 31 estados y un distrito federal \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E México se divide en 24 estados \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_04_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_05.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_06.png",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_07.png",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_08.png",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_09.png",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_10.png",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_11.png",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_12.png",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_13.png",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_14.png",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_15.png",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_16.png",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_17.png",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_18.png",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_19.png",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_20.png",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_21.png",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_22.png",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_23.png",
                "t17correcta": "21"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_24.png",
                "t17correcta": "22"
            },
            {
                "t13respuesta": "gi4a_b01_n03_04_25.png",
                "t17correcta": "23"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arma el rompecabezas de México. <\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n03_04_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "anchoImagen": 64,
            "tamanyoReal": false,
            "borde": false
        },
        "contenedores": [
            {"Contenedor": ["", "17,15", "cuadrado", "141, 82", ".", "transparent"]},
            {"Contenedor": ["", "17,155", "cuadrado", "141, 82", ".", "transparent"]},
            {"Contenedor": ["", "17,295", "cuadrado", "141, 82", ".", "transparent"]},
            {"Contenedor": ["", "17,435", "cuadrado", "141, 82", ".", "transparent"]},
            {"Contenedor": ["", "99,15", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "99,155", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "99,295", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "99,435", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "181,15", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "181,155", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "181,295", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "181,435", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "263,15", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "263,155", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "263,295", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "263,435", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "345,15", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "345,155", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "345,295", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "345,435", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "427,15", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "427,155", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "427,295", "cuadrado", "141, 83", ".", "transparent"]},
            {"Contenedor": ["", "427,435", "cuadrado", "141, 83", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_05_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_05_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_05_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n03_05_05.png",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "gi4a_b01_n03_05_06.png",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena los Estados de acuerdo a su tamaño. Coloca el de mayor tamaño hasta arriba y el de menor tamaño, hasta abajo.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n03_05_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": true

        },
        "contenedores": [
            {"Contenedor": ["", "38,476", "cuadrado", "122, 40", ".", "transparent"]},
            {"Contenedor": ["", "79,476", "cuadrado", "122, 40", ".", "transparent"]},
            {"Contenedor": ["", "121,476", "cuadrado", "122, 40", ".", "transparent"]},
            {"Contenedor": ["", "163,476", "cuadrado", "122, 40", ".", "transparent"]},
            {"Contenedor": ["", "206,476", "cuadrado", "122, 40", ".", "transparent"]}

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_06_02.png",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_06_03.png",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_06_04.png",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gi4a_b01_n03_06_05.png",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra los Estados que se encuentran en los 4 puntos cardinales del Estado de Zacatecas.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n03_06_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "css":{
            "tamanoFondoColumnaContenedores":"contain"
        },
        "contenedores": [
            {"Contenedor": ["", "64,448", "cuadrado", "179, 42", ".", "transparent"]},
            {"Contenedor": ["", "108,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "152,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "196,448", "cuadrado", "179, 43", ".", "transparent"]},
            {"Contenedor": ["", "240,448", "cuadrado", "179, 43", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Jalapa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Toluca<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Hermosillo<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Mérida<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Veracruz"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Estado de México"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Sonora"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Yucatán"
            }
        ],
        "pregunta": {
            "t11pregunta": "Une cada estado con su capital.",
            "c03id_tipo_pregunta": "18"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n02_07_02.png",
                "t17correcta": "0,2",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_06.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_07.png",
                "t17correcta": "2,0",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_05.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_03.png",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n02_07_04.png",
                "t17correcta": "5",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Identifica los lugares representativos de México que están señalados y arrastralos al lugar que pertenecen.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n02_07_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true,
            "tamanyoReal": true

        },
        "contenedores": [
            {"Contenedor": ["", "60,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "125,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "191,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "256,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "323,387", "cuadrado", "202, 47", ".", "transparent"]},
            {"Contenedor": ["", "388,387", "cuadrado", "202, 47", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi4a_b01_n03_08_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_08_03.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_08_04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "gi4a_b01_n03_08_05.png",
                "t17correcta": "3",
                "columna": "0"
            },
            {
                "t13respuesta": "gi4a_b01_n03_08_06.png",
                "t17correcta": "4",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra el componente que corresponda a la imagen.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi4a_b01_n03_08_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true,
            "tamanyoReal": true

        },
        "contenedores": [
            {"Contenedor": ["", "49,336", "cuadrado", "240, 100", ".", "transparent"]},
            {"Contenedor": ["", "150,336", "cuadrado", "240, 100", ".", "transparent"]},
            {"Contenedor": ["", "253,336", "cuadrado", "240, 100", ".", "transparent"]},            
            {"Contenedor": ["", "355,336", "cuadrado", "240, 100", ".", "transparent"]},
            {"Contenedor": ["", "457,336", "cuadrado", "240, 100", ".", "transparent"]}
        ]
    }
];