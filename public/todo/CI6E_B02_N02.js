json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Para que no conozcan quién soy.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Para no dañarte a ti mismo ni a los demás.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Para tener tiempo de serenarte y no arrepentirte de un arrebato.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Para no hacer el ridículo.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Para que no te castiguen.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Para tomar mejores decisiones una vez que estés tranquilo<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige de entre las opciones todas aquellas que expresan motivos por los que debemos aprender a expresar nuestras emociones y controlar nuestros impulsos."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Ordenar nuestras ideas antes de decir lo que sentimos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aceptar nuestros sentimientos y emociones.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hacer uso de gestos o escritos. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Esperar a que se de el momento adecuado.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tener paciencia, ser amable y tolerante antes las reacciones de los demás.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Poner excusas a lo que sentimos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Guardarlos para nosotros y no decirlos de ninguna forma.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Decirlo cuando surge la emoción.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Juzgar las reacciones de los demás.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ser agresivos con los que no entienden lo que sentimos.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra las acciones de acuerdo a si nos ayudan o nos dificultan expresar nuestros sentimientos y emociones.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ayudan a expresar nuestros sentimientos y  emociones",
            "Dificultan expresar nuestros sentimientos y emociones"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Actuar honestamente y en favor de la mayoría.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Bajar el precio de los productos básicos.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Evitar el mal uso de los recursos económicos.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Garantizar el servicio de recolección de basura.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Rendición de cuentas.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Brindar a los ciudadanos acceso a la información.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Construir centros deportivos gratuitos.</p>",
                "t17correcta": "0"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Algunas de las obligaciones de gobierno en materia de transparencia son:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>justicia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>injusto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>vulnerable<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tolerancia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>empatia<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Principio moral que lleva a dar a cada uno lo que le corresponde."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Falta a la justicia y equidad."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Por la situación en que se encuentra es más fácil que una persona sea herida física, psicológica o emocionalmente."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Respeto a las ideas, creencias o prácticas de los demás cuando son diferentes o contrarias a las propias."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Capacidad de identificarse con otros y compartir sus sentimientos."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 10,
                "datoY" : 2
            },
            {
                "direccion" : 1,
                "datoX" : 2,
                "datoY" : 3
            },
            {
                "direccion" : 1,
                "datoX" : 6,
                "datoY" : 3
            },
            {
                "direccion" : 0,
                "datoX" : 2,
                "datoY" : 8
            },
            {
                "direccion" : 0,
                "datoX" : 6,
                "datoY" : 12
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 13,
            "t11pregunta": "Completa el crucigrama"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Trabaja activamente por la equidad social<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ofrece dádivas para no ser multado por loa agentes de tránsito.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Denuncia ante las instancias correspondientes si se comete una injusticia.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es solidario con las personas en situación de vulnerabilidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hace trampa para no pagar los impuestos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Decide ser honesto.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es tolerante y respetuoso con las personas que opinan distinto a él.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Evita cumplir con sus obligaciones.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No permite abusos de la autoridad, aunque no te afecten personalmente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Participa socialmente.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "¿Cuál es el comportamiento de un ciudadano socialmente responsable?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ECongruente\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EIncongruente \u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Imagina que tu nueva vecina se dice ecologista, a ti te interesa conocer sus opiniones y tomar acciones concretas que ayuden al planeta. Sin embargo, despiertas el domingo temprano y al ver por tu ventana observas a siguiente imagen: <br><div style='text-align:center;'><img src='.png'></div>¿De qué manera puedes calificar a la persona que se ejemplifica en la situación?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Al relacionarse socialmente con otros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los hereda.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Conociendo y respetando a sus amigos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Con lo que vio, escucho y vivió en su familia.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al estudiar ética y moral.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Con lo que aprende en los exámenes.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Por los regaños de sus padres.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al decidir lo que para él es correcto y o que no.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todas las opciones que explican las formas en que se construyen los principios guían a cada ser humano."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Equidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>reciba<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>requiere<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>necesidades<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>específicas<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>considerar<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>individuo<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>autoridades<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>proceso<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>carecen<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>no<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>básicas<\/p>",
                "t17correcta": "12"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es que cada ser humano <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp; lo que <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>,&nbsp; acorde a sus <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y circunstancias <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>,&nbsp; podemos pensar que el término implica dar a todos lo mismo; sin embargo lo correcto es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; al <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Imagina por ejemplo que las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de la SEP deciden que cada niño de sexto grado inscrito en una escuela oficial reciba una computadora... algunos podrán utilizarla en su <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de aprendizaje, pero para los niños que <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de energía eléctrica <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; sería útil, antes de una computadora deben cubrir sus necesidades <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra al texto las palabras que lo completan."
        }
    },
    {
      "respuestas":[
          {
              "t13respuesta":"<p>honestidad<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>ejercitar<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>correcto<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>respetar<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>aprender<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>organizar<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>diversion<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>compartir<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>convivencia<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>tolerar<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>normas<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>social<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>reglas<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>juego<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>justo<\/p>",
              "t17correcta":"0"
          },
          {
              "t13respuesta":"<p>sano<\/p>",
              "t17correcta":"0"
          },
      ],
      "pregunta":{
          "c03id_tipo_pregunta":"16",
          "t11medida":16,
          "t11pregunta":"Localiza en la sopa letras 16 palabras relacionadas con el bloque."
      }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Equidad es dar a cada uno lo que requiere, no lo mismo.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El trabajo infantil no ocurre en nuestro país.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las familias de madres solteras son más propensas a vivir en pobreza.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En México existe un 75% de personas que no cubren sus necesidades básicas diariamente.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nadie muere por hambre en México.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cada persona debe contribuir con la justicia social.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las personas en situación de discapacidad requieren más apoyos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los discapacitados adultos son contratados para trabajar.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Niños y mujeres son las personas que sufren más violencia intrafamiliar.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En cada poblado de México los ciudadanos cuentan con servicios básicos como agua o energía eléctrica.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Contesta  falso (F) o verdadero (V) según corresponda.<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
        }
    }
]