json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Implica una apreciación subjetiva del objeto en juicio.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cualidades propias del objeto que está en juicio.",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Valorar"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Valor"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "s1a2"
        }
    }
]