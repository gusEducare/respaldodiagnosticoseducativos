json = [
    //  {
    //     "respuestas": [
    //         {
    //             "t13respuesta": "<p>volumen:<\/p>",
    //             "t17correcta": "1"
    //         },
    //         {
    //             "t13respuesta": "<p>Kilo:<\/p>",
    //             "t17correcta": "2"
    //         },
    //         {
    //             "t13respuesta": "<p>masa:<\/p>",
    //             "t17correcta": "3"
    //         },
    //         {
    //             "t13respuesta": "<p>libra:<\/p>",
    //             "t17correcta": "4"
    //         },
    //         {
    //             "t13respuesta": "<p>litro:<\/p>",
    //             "t17correcta": "5"
    //         }
    //     ],
    //     "preguntas": [
    //         {
    //             "c03id_tipo_pregunta": "15",
    //             "t11pregunta": "Se refiere al espacio que ocupa un cuerpo, su unidad de medidad es el metro cúbico."
    //         },
    //         {
    //             "c03id_tipo_pregunta": "15",
    //             "t11pregunta": "Unidad de masa del Sistema Internacional de simbolo kg, que equivale a la masa del prototipo de platino iridiano que se encuentra en la Oficina Internacional de pesas y medidas de París."
    //         },
    //         {
    //             "c03id_tipo_pregunta": "15",
    //             "t11pregunta": "Cantidad de materia que posee un cuerpo y se mide en kilogramos o sus fracciones."
    //         },
    //         {
    //             "c03id_tipo_pregunta": "15",
    //             "t11pregunta": "Unidad de masa usada desde la Antigua roma. Significa 'escala o balanza', todavia es el nombre de la principal unidad de masa usada en Estados Unidos."
    //         },
    //         {
    //             "c03id_tipo_pregunta": "15",
    //             "t11pregunta": "Unidad de volumen del Sistema Internacional, de simbolo l o L, equivale a 1 decimetro cúbico."
    //         }
    //     ],
    //     "pocisiones": [
    //         {
    //             "direccion" : 1,
    //             "datoX" : 6,
    //             "datoY" : 1
    //         },
    //         {
    //             "direccion" : 1,
    //             "datoX" : 10,
    //             "datoY" : 1
    //         },
    //         {
    //             "direccion" : 0,
    //             "datoX" : 6,
    //             "datoY" : 5
    //         },
    //         {
    //             "direccion" : 0,
    //             "datoX" : 4,
    //             "datoY" : 5
    //         },
    //         {
    //             "direccion" : 1,
    //             "datoX" : 1,
    //             "datoY" : 8
    //         }
    //     ],
    //     "pregunta": {
    //         "c03id_tipo_pregunta": "15",
    //         "alto": 11,
    //         "t11pregunta": "Delta sesion 12 a3"
    //     }
    // },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Gaseoso<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Líquido<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Sólido<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Sólido<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Líquido<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Gaseoso<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_a.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_c.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div></div><\/p>"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": true,
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Completa las oraciones con el adjetivo interrogativo que corresponda. Arrastra las palabras al lugar que les corresponde. Atiende los acentos en cada caso para responder correctamente la actividad."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Mezcla homogénea"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p></div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'><div><\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Mezcla heterogénea"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor es la energía transferida de un objeto de mayor temperatura a un objeto metálico.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Usamos comunmente la palabra calor para describir las condiciones del tiempo o la temperatura de los cuerpos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La temperatura y el calor son dos conceptos diferente, aunque los usamos como sinónimos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al transferir energía de un objeto con mayor temperatura a uno con menor, es cuando hablamos de calor.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La temperatura es el grado o nivel térmico de un cuerpo o de la atmósfera.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Usa algún material aislante como la tela o guantes de caucho o carnaza para tomar objetos o materiales calientes."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "No realices ejercicio en las horas de mayor calor en tu comunidad, te puede ocasionar un glope de calor y poner tu vida en riesgo por deshidrantación, llevando a un estado de coma y hasta la muerte."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "No tocar o asir focos encendidos, el vidrio que recubre los filamentos que generan luz se calienta por radiación y puedes ocasionar quemaduras."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "No apagar con los dedos un cerillo o una vela en forma vertical, eso te causaría quemaduras, pues recuerda que transfiere calor hacia arriba."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E<img>\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "No tocar la parte inferior de la plancha cuando este conectada a la corriente eléctrica."
        }
    }
]