json = [
      {
        "respuestas": [
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "2,4,6"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "2,4,6"
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "1,3"
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "3,1"
            },
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "2,4,6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Renato pintó ayer una sexta parte de su casa y hoy pintó dos terceras partes más. ¿Qué parte de la casa le falta por pintar? Como \n\
                                <div style='position: relative; padding: 7px;  display: inline-block; text-align: center; vertical-align:middle;'>\n\
                                <div style='padding: 0px 10px 10px 10px;'>1</div>\n\
                                <div style='padding: 10px; border-top: 1px solid #000; display: block;'>6</div></div> + \n\
                                <div style='position: relative; padding: 7px;  display: inline-block; text-align: center; vertical-align:middle;'>\n\
                                <div style='padding: 0px 10px 10px 10px;'>2</div>\n\
                                <div style='padding: 10px; border-top: 1px solid #000; display: block;'>3</div></div> = \n\
                                <div style='position: relative; padding: 7px;  display: inline-block; text-align: center; vertical-align:middle;'>\n\
                                <div style='padding: 0px 10px 10px 10px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</div><div style='padding: 10px; border-top: 1px solid #000; display: block;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</div></div>, entonces le falta por pintar 1 -<div style='position: relative; padding: 7px;  display: inline-block; text-align: center; vertical-align:middle;'><div classstyle='padding: 0px 10px 10px 10px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</div><div style='padding: 10px; border-top: 1px solid #000; display: block;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</div></div> = <div style='position: relative; padding: 7px;  display: inline-block; text-align: center; vertical-align:middle;'><div style='padding: 0px 10px 10px 10px;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</div><div style='padding: 10px; border-top: 1px solid #000; display: block;'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</div></div></p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal":true,
            "soloTexto":true,
            "respuestasLargas":true,
            "anchoRespuestas":15,
            "t11pregunta": "Coloca los números en el lugar que les corresponde."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>8<\/p>",
                "t17correcta": "2,5,6"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>7<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>8<\/p>",
                "t17correcta": "2,5,6"
            },
            {
                "t13respuesta": "<p>8<\/p>",
                "t17correcta": "2,5,6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<div style='display:inline'><table class='customTableDiv'><tr><td></td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td></td><td></td></tr><tr><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td class='tdDiv' colspan='3'>30</td></tr><tr><td></td><td>6</td><td></td><td></td></tr></table></div><div style='display:inline'><table class='customTableDiv'><tr><td></td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td></td><td></td></tr><tr><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td class='tdDiv' colspan='3'>48</td></tr><tr><td></td><td>6</td><td></td><td></td></tr></table></div><div style='display:inline'><table class='customTableDiv'><tr><td></td><td> "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td></td><td></td></tr><tr><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td class='tdDiv' colspan='3'>69</td></tr><tr><td></td><td>5</td><td></td><td></td></tr></table></div><script>$('.customTableDiv').css({border: '0', background:'transparent', 'padding-left':'50px', display:'inline'}); $('.customTableDiv td').css({width:'40px', height:'40px', background:'#f2f2f2','font-size':'24px'});$('.customTableDiv .tdDiv').css({'border-top':'1px solid black','border-left':'1px solid black'});</script>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal":true,
            "soloTexto":true,
            "respuestasLargas":true,
            "anchoRespuestas":20,
            "t11pregunta": "Coloca los números en el lugar que les corresponde."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>A3<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>A1<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>recto<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>secantes<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<img src='mi5e_b01_n01_01.png'><br/><p>Ángulo agudo: <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br><p>Ángulo obtuso: <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br><p>Si las rectas r1 y r2 son perpendiculares entre sí, <br>¿qué tipo de ángulo es A2? <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br><p>¿Cómo son entre sí las rectas r2 y r3? <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":true,
            "respuestasLargas":true,
            "ocultaPuntoFinal":true,
            "t11pregunta": "Observa la imagen y responde las preguntas."
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La dirección de la marcha es de oeste a este.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La marcha inicia en el zócalo y termina en la calzada Chivatito.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La avenida más afectada por la marcha es Av. Chapultepec.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "De las alternativas viales, las de un solo sentido de circulación son: Antonio Caso, Eje 1 Nte. Mosqueta, Eje 1 Ote. Anillo de Circunvalación, Av. José Ma. Izazága y Dr. Río de la Loza. Verdadero",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>El mapa muestra la ruta de una marcha. Observa la imagen y responde “falso” y “verdadero” según corresponda.<br/><img src='mi5e_b01_n01_02.png'></p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
      {
      "respuestas": [
          {
              "t13respuesta": "mi5e_b01_n01_03_03.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "mi5e_b01_n01_03_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
                 "t13respuesta": "mi5e_b01_n01_03_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
                 "t13respuesta": "mi5e_b01_n01_03_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
                 "t13respuesta": "mi5e_b01_n01_03_06.png",
              "t17correcta": "4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Coloca la unidad más adecuada para medir cada uno de los objetos siguientes.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"mi5e_b01_n01_03_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "tamanyoReal" : true
          
      },
      "contenedores": [
          {"Contenedor": ["", "180,32", "cuadrado", "155, 55", "."]},
          {"Contenedor": ["", "180,240", "cuadrado", "160, 55", "."]},
          {"Contenedor": ["", "180,460", "cuadrado", "155, 55", "."]},
          {"Contenedor": ["", "402,140", "cuadrado", "160, 55", "."]},
          {"Contenedor": ["", "402,350", "cuadrado", "160, 55", "."]}
      ]
  }
]