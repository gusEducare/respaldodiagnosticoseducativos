json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>solidos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>particulas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>materia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>liquidos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>gases<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Se formam por partículas que no tiene movimiento y están muy cerca unas de otras, por tanto tienen forma definida y carecen de fluidez."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Componen la materia y su movimiento determina el estado de la misma."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Todo aquello que tiene masa y ocupa un lugar en el espacio."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Los forman particulas que tiene movimiento intermedio y se mantienen separadas, por lo que tienen fluidez y forma indefinida, entonces adquieren la formadel recipiente que los contiene."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Sus partículas se mueven libremente y se mantienen dispersas lo que hace que presenten fluidez y su forma depende del recipiente donde estén contenidos."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 13,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 4,
                "datoY" : 7
            },
            {
                "direccion" : 1,
                "datoX" : 5,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 7,
                "datoY" : 2
            },
            {
                "direccion" : 0,
                "datoX" : 2,
                "datoY" : 4
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPara que polo sur y polo norte no se descongelen.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara reciclar y purificar el agua\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara modificar el estado físico del agua\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara abastecer los mares\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara potabilizar el agua\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El ciclo del agua es fundamental para la vida en el planeta."
      }
   },
   {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Evapora</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Condensa</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Solidifica</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Fusiona</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Evapora</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Condensa</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Solidifica</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Fusiona</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Cuando el calor modifica el estado del agua se","Cuando la baja temperatura modifica el estado del agua se"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Evapora</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Condensa</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Solidifica</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Fusiona</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Evapora</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Condensa</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Solidifica</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Fusiona</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Cuando el vapor se transforma en gotas entonces se","Cuando el calor provoca que el agua derrita se"],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor es un tipo de energía",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor no modifica de ninguna manera las partículas",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor es capaz de eliminar microorganismos nocivos",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor no tiene ninguna relación con la energía",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Pares de calcetines",   
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
      },
      {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor provoca que las partículas se muevan e incrementen su temperatura.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor es necesario para la supervivencia del ser humano.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    }
]
