json = [
	{
		"respuestas": [
			{
				"t13respuesta": "personal",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "primera",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "reflexiones",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "hechos",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "persuadir",
				"t17correcta": "5"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<br>El ensayo: es un texto argumentativo de estilo&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ", narrado en&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;persona, de larga extensión en el que se plantean comentarios y puntos de vista. Su función principal es hacer&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;sobre determinado asunto , respaldando las opiniones con&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;o fundamentos teóricos. Un ensayo puede estar dirigido a un público especializado o general y no pretende informar sino "
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;y convencer al lector."
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Selecciona las palabras que te ayuden a completar el siguiente párrafo con las características de un ensayo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Verdadero",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Falso",
				"t17correcta": "1"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Son preguntas para investigar: ¿Qué? ¿Cuándo? ¿Cómo?",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "En un ensayo, un apersona debe de plantear su punto de vista.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Una opinión que no está sustentada en citas, no puede formar parte de un ensayo.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "La introducción en un ensayo es la parte más importante del mismo.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Dentro de un ensayo se pueden refutar o demostrar las tesis que se plantean.",
				"correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "13",
			"t11pregunta": "Selecciona la respuesta correcta.",
			"descripcion": "",
			"variante": "editable",
			//"anchoColumnaPreguntas": 30,
			"evaluable": true,
			"evidencio": false
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Se trata de una recreación de algún autor o experto en nuestras propias palabras.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Se tratan de evitar términos técnicos para que pueda ser entendida la informacion.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Se evitar opiniones personales y para realizarla se identifican las ideas principales y secundarias para explicarlas después con nuestras palabras.</p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Repetición exacta de lo que haya dicho algún autor o experto sobre un tema.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>No se pueden cambiar las palabras, se usan de forma textual, de acuerdo a lo dicho por un autor o experto.</p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Se pone entre comillas lo que se está retomando para hacer referencia directa a quien haya dicho lo utilizado.</p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Une con líneas las características relacionadas con la paráfrasis y las correspondientes a una cita textual.",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Paráfrasis",
			"Cita Textual"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "relevante",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "monografía",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "estudiar",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "paráfrasis,",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "resumen",
				"t17correcta": "5"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<br>Las fichas de trabajo son textos que presentan información "
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ", organizada y seleccionada sobre un tema en particular. Sus usos son múltiples; se utilizan para realizar un ensayo o "
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ", como soporte para una exposición oral, para "
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;para los exámenes, entre otros fines. Las fichas de trabajo pueden ser de "
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ", &nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "&nbsp;o de cita textual."
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"pintaUltimaCaja": false,
			"t11pregunta": "Completa el siguiente párrafo con la información relacionada a una ficha de trabajo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "nota a pie de pagina",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "fichas de trabajo",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "argumentar",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "explicar",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "notas",
				"t17correcta": "0"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Son llamados o marcas que se hacen dentro del texto y que remiten a la parte inferior de la página y ofrecen información adicional. Pueden ser un comentario u opinión del escrito o para una referencia."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Textos que presentan información relevante, organizada sobre un tema en particular."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Es compartir información pero avalandola por alguien o por algún dato u opinión. Requiere justificar lo que se dice."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Es dar a conocer algo: su definición, cómo o dónde sucede y todos los datos que sepamos para hacer entender a alguien sobre algo en específico."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Apuntes cortos y concisos sobre alguna información amplia. Se basan en incluir palabras clave. Son de uso personal."
			}
		],
		"pocisiones": [
			{
				"direccion": 1,
				"datoX": 12,
				"datoY": 0
			},
			{
				"direccion": 0,
				"datoX": 0,
				"datoY": 3
			},
			{
				"direccion": 0,
				"datoX": 12,
				"datoY": 5
			},
			{
				"direccion": 0,
				"datoX": 8,
				"datoY": 17
			},
			{
				"direccion": 0,
				"datoX": 9,
				"datoY": 19
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "15",
			"alto": 20,
			"ancho": 22,
			"t11pregunta": "Realiza el siguiente crucigrama."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Nexos concesivos",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "Nexos casuales",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Nexos condicionales",
				"t17correcta": "2"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<style>#texto1, #texto2, #texto3 {padding-top: 10px !important;}</style>Expresan la causa o el motivo por el que se realiza una acción, algunos de estos nexos son: como, debido a, gracias a, porque, ya que...&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<br>Expresan que debe producirse cierta condición para generar determinada acción, se identifica con palabras como: en caso que, siempre que, siempre y cuando, si...&nbsp;"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<br>Se refieren a un impedimento que se origina como consecuencia de la acción que indica el verbo mencionado previamente: a pesar de, al contrario, en cambio, a diferencia de, al igual que, aunque; sin embargo...&nbsp;"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"respuestasLargas": true,
			"ocultaPuntoFinal": true,
			"t11pregunta": "Recorta el tipo de nexo y arrástralo a la definición que corresponda."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Es la demostración de la semejanza que puede tener algo con otra cosa y/o persona. Suele emplear palabras como ”se asemeja a”.”aue”.”como”v”cual”",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Es la descripción de algo mediante la semejanza contraria que este podría tener. Es decir, la metáfora define algo buscando la relación que puede tener con algo que no sea idéntico ni parecido al mismo.",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "Es el uso de repetición. Se repiten palabras o frases exactamente iguales en distintos puntos para así conseguir un efecto rítmico-secuencial.",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "Es el aumento o disminución de cualidades. Se reconoce por el empleo de la exageración para referirse a algo.",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "Es un tipo de metáfora que busca atribuirle características humanas a otros seres como animales, plantas u objetos.",
				"t17correcta": "5"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Símil o comparación"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Metáfora"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Hipérbole"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Paralelismo"
			},
			{
				"c03id_tipo_pregunta": "18",
				"t11pregunta": "Personificación"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "17",
			"t11pregunta": "Une con una línea la figura retórica que corresponda."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Encabezado",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Fotografía",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "Logotipo",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>Llamado a la acción</p>",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "Eslogan",
				"t17correcta": "5"
			},
			{
				"t13respuesta": "Contacto",
				"t17correcta": "6"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<style>span{padding-top:10px !important;}</style><br>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ": contiene un titular que presenta una idea, necesidad o pregunta. Es un mensaje corto que pretende atraer la atención del espectador.<br/><br/>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ": relacionada con la marca y con lo que se vende, suele ser lo que capta aún más rápido la atención del espectador. Es recomendable que contenga alguna figura humana para atraer más vistas.<br/><br/>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ": el concepto de imagen que representa a la marca. Símbolos, imágenes y letras que conforman el nombre de la empresa. Suele ser un dibujo sencillo y fácil de recordar.<br/><br/>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ": es el mensaje que incita directamente al espectador a realizar la compra, llamada u a aprovechar alguna oferta, descuento o promoción.<br/><br/>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ": es la frase que identifica a la marca o a la campaña publicitaria. Es un mensaje corto, conciso, fácil de recordar y que pretende generar empatía entre la empresa y el consumidor.<br/><br/>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": ": teléfono, correo electrónico, página web o redes sociales de la compañía para invitar al espectador a solicitar información del producto y a enterarse de nuevas promociones o lanzamientos.<br>"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"pintaUltimaCaja": false,
			"respuestasLargas": true,
			"t11pregunta": "Recorta la palabra que corresponda a las características que un anuncio publicitario contiene:"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Verdadero",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Falso",
				"t17correcta": "1"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "En las encuestas sólo deben de incluirse respuestas de opción múltiple.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Para determinar qué queremos saber en una encuesta, hay que definir el objetivo de la misma.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Al realizar una encuesta, debes de tomar en cuenta sólo las opiniones que favorezcan tu estudio o investigación.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Antes de realizar la encuesta debe determinar dónde se encuentra tu población muestra y qué tiempo dispones para realizarla.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "La información que se obtiene con preguntas cerradas es objetiva y pero no cuantificable.",
				"correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "13",
			"t11pregunta": "Selecciona la respuesta correcta.",
			"descripcion": "",
			"variante": "editable",
			"anchoColumnaPreguntas": 60,
			"evaluable": true
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Uso de graficas con las que los datos pueden mostrar claramente la información.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Redacción amplia del análisis de cada respuesta por parte de los encuestados.",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Presentación de tablas o cuadros de doble entrada para organizar la infromación, regisatrarla y compararla.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Verificar si los objetivos de la investigación se cumplieron con la información obtenida de las encuestas.",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "Presentación de las gráficas y encuestas sin análisis o sin retomar dicha información para redactar las conclusiones.",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Ordena las ideas que te ayudan a interpretar la información obtenida de encuestas."
		}
	}
]
