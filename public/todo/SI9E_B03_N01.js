json=[
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Conclusión<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Objetivos<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Marco teórico<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Hipótesis<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Procedimiento o metodología<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Cuadros, tablas y gráficas<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Materiales<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Bibliografía<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>Resultados<\/p>",
                "t17correcta": "9"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><br/><br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: se desarrolla sobre todos aquellos resultados que se obtuvieron y se analiza qué se puede concluir o a qué aprendizaje se generó luego de haber realizado el experimento científico.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: Se explica cuál es el propósito de realizar el experimento; qué es lo que se pretende hacer u obtener.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: ofrece los conceptos y explicaciones necesarias para contextualizar el problema e hipótesis a tratar.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: se describe brevemente la situación previa a realizar el experimento y se incluye la idea de qué pasaría si se realizara un experimento científico.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: se describe paso a paso los procedimientos para realizar la investigación.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: constituyen recursos de apoyo para comprender el procedimiento realizado.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: se enumeran los materiales e instrumentos que se utilizaron en la investigación.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: se incluyen las fuentes consultadas así como las recomendaciones teóricas.<br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;: se describen objetivamente los resultados obtenidos en los procedimientos<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "respuestasLargas" : true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>impersonales<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>acción<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>pasiva<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>verbo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>complemento<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Las oraciones <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: son aquellas que no tienen sujeto, es decir, la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;no se le atribuye a una persona, animal o cosa.<br/>El uso de la voz <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;se refiere a cuando el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>que se expresa en una oración no es ejecutado por el sujeto (agente) sino por un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Renacimiento<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>XV y XVI<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Edad Media<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>astronomía<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Leonardo Da Vinci<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>arte<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>El <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;es un período de la historia que abarcó los siglos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>; constituyó una ruptura o distancia con la filosofía de la época anterior, la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Los viajes de exploración que dieron como resultado el descubrimiento de América, los avances en la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y la navegación, el surgimiento de genios como <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;entre muchos otros hechos marcaron una nueva forma de vivir. La literatura y el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;en general, siempre constituyen un espejo en el que se reflejan los valores, preocupaciones y costumbres de una época determinada, ya que el hombre  recurre a ella para manifestarse.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Fue un movimiento que surgío en Italia en el siglo XIV y que se progagó por Europa. Se caracterizó por romper con la visión del mundo que se tenía en la Edad Media.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La imprenta que apareció en 1540, permitió el acceso a la información y difusión de nuevas ideas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los principales escritores renacentistas fueron Willian Shakespeare (1564-1616) quién fue un dramaturgo y poeta inglés y padre de la lengua inglesa.<br/>Miguel de Cervante Saavedra (1547-1616) fue un novelista y dramaturgo español su obra mas reconocida fue una parodia a las novelas de cabellería Don Quijote de la Mancha.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Las obras literarias tienen una relación poco directa con la época y el contexto en el que fueron escritas. La poesía o la narrativa tienen mensajes universales que no se ven influnciados por el contexto histórico que las ven nacer.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Coincide con las conquistas de ultramar; esto marca el comienzo del intercambio intercultural. Al mismo tiempo se afianza el absolutismo como de forma de gobierno; de esta manera el descubrimiento de América convierte a los reyes de España en los más poderosos del mundo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El renacimiento puso al hombre en el centor de todo dejando a un lado el teocentrismo medieval, el hombre comienza a sentirse capaz de crear y de hacer y en ese sentido se considera un dios.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Renacimiento"
        }
    },
  {
        "respuestas": [
            {
                "t13respuesta": "<p>programación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>radioescucha<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>medio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>unisensorial<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>radiofónico<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>comunicación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>diversidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>secciones<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>difusión<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>programa<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    }
]