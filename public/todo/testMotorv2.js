json = [
    {
    "respuestas": [
        {
            "t13respuesta": "<p>5750+3200<\/p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>21000<\/p>",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "<p>15625<\/p>",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "<p>15682+6853<\/p>",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "<p>16000<\/p>",
            "t17correcta": "5"
        },
        {
            "t13respuesta": "<p>6582+3517<\/p>",
            "t17correcta": "6"
        }
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<br><style>\n\
                                    .table img{height: 90px !important; width: auto !important; }\n\
                                </style><table class='table' style='margin-top:-40px;'>\n\
                                <tr>\n\
                                    <td>a)</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    <td>=</td>\n\
                                    <td>8950</td>\n\
                                </tr><tr>\n\
                                    <td>b)</td>\n\
                                    <td>9000+12000</td>\n\
                                    <td>=</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                </tr><tr>\n\
                                    <td>c)</td>\n\
                                    <td>7895+7730</td>\n\
                                    <td>=</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                </tr><tr>\n\
                                    <td>d)</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    <td>=</td>\n\
                                    <td>22535</td>\n\
                                </tr></table>"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
       "contieneDistractores":true,
        "anchoRespuestas": 70,
        "soloTexto": true,
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "ocultaPuntoFinal": true
    }
}]