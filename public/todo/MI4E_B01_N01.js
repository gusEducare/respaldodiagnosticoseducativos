json = [ 
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_04.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 1,
 	   		"valor":1000
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_03.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 1,
 	   		"valor":500
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_02.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 1,
 	   		"valor":200
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_01.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 1,
 	   		"valor":100
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a05_extra.png",
 	   		"t17correcta": "1",
 	   		"clones": 9,
 	   		"numeroCorrectas": 0,
 	   		"valor":50
 	   	}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "5",
 	   		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la siguiente cantidad.<br>a) $1 800<\/p>",
 	   		"tipo": "ordenar",
 	   		"imagen": true,
 	   		"url": "MI4E_B01_N01_01_06.png",
 	   		"respuestaImagen": true,
 	   		"anchoImagen": 60, 
 	   		"bloques": false,
 	   		"posicionamiento": "descendente",
 	   		"respuestaUnicaMultiple": true,
 	   		"filas": 1,
 	   		"borde":false,
 	   		"renglones": 1,
 	   		"contenedorContador":true
 
 	   	},
 	   	"contenedores": [
 	   	{"Contenedor": ["", "80,80", "cuadrado", "235, 164", "."]}
 	   	], 
 	   	"css":{
 	   		"posicionIndicadorValor":{top:107, left:26},
 	   		"anchoRespuestas":200
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_04.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 1,
 	   		"valor":1000
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_03.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 3,
 	   		"valor":500
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_02.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 2,
 	   		"valor":200
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_01.png",
 	   		"t17correcta": "1",
 	   		"clones": 9,
 	   		"numeroCorrectas": 0,
 	   		"valor":100
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a05_extra.png",
 	   		"t17correcta": "1",
 	   		"clones": 9,
 	   		"numeroCorrectas": 0,
 	   		"valor":50
 	   	}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "5",
 	   		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la siguiente cantidad.<br> b) $2 900<\/p>",
 	   		"tipo": "ordenar",
 	   		"imagen": true,
 	   		"url": "MI4E_B01_N01_01_06.png",
 	   		"respuestaImagen": true,
 	   		"anchoImagen": 60, 
 	   		"bloques": false,
 	   		"posicionamiento": "descendente",
 	   		"respuestaUnicaMultiple": true,
 	   		"filas": 1,
 	   		"borde":false,
 	   		"renglones": 1,
 	   		"contenedorContador":true
 
 	   	},
 	   	"contenedores": [
 	   	{"Contenedor": ["", "80,80", "cuadrado", "235, 164", "."]}
 	   	], 
 	   	"css":{
 	   		"posicionIndicadorValor":{top:107, left:26},
 	   		"anchoRespuestas":200
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_04.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 2,
 	   		"valor":1000
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_03.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 2,
 	   		"valor":500
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_02.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 3,
 	   		"valor":200
 	   	},
 	   	{
 	   		"t13respuesta": "mi4e_b01_n01_01_01.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 1,
 	   		"valor":100
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a05_extra.png",
 	   		"t17correcta": "0",
 	   		"clones": 9,
 	   		"numeroCorrectas": 1,
 	   		"valor":50
 	   	}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "5",
 	   		"t11pregunta": "<p>Coloca en el contenedor los billetes necesarios para obtener la siguiente cantidad.<br> c) $3 750<\/p>",
 	   		"tipo": "ordenar",
 	   		"imagen": true,
 	   		"url": "MI4E_B01_N01_01_06.png",
 	   		"respuestaImagen": true,
 	   		"anchoImagen": 60, 
 	   		"bloques": false,
 	   		"posicionamiento": "descendente",
 	   		"respuestaUnicaMultiple": true,
 	   		"filas": 1,
 	   		"borde":false,
 	   		"renglones": 1,
 	   		"contenedorContador":true
 
 	   	},
 	   	"contenedores": [
 	   	{"Contenedor": ["", "80,80", "cuadrado", "235, 164", "."]}
 	   	], 
 	   	"css":{
 	   		"posicionIndicadorValor":{top:107, left:26},
 	   		"anchoRespuestas":200
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "<p>$2050<\/p>",
 	   		"t17correcta": "1"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$4000<\/p>",
 	   		"t17correcta": "2"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$5100<\/p>",
 	   		"t17correcta": "3"
 	   	}
 	   	],
 	   	"preguntas": [
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p><br><img src='mi4e_b01_n01_02_01.png' style='width:400px'><div></div></p>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p><br><img src='mi4e_b01_n01_02_02.png' style='width:400px'><div></div></p>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p><br><img src='mi4e_b01_n01_02_03.png' style='width:400px'><div></div></p>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p> </p>"
 	   	},
 
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "8",
 	   		"pintaUltimaCaja": false,        
 	   		"textosLargos": "si",
 	   		"contieneDistractores": false,
 	   		"t11pregunta": "Arrastra la cantidad de dinero que se obtiene en cada caso."
 	   	}
 	   },
 	   {  
 	   	"respuestas":[  
 	   	{  
 	   		"t13respuesta":     "Avanzo hacia al Sur y después hacia el Oeste.",
 	   		"t17correcta":"0"
 	   	},
 	   	{  
 	   		"t13respuesta":     "Avanzo hacia el Oeste y después hacia el Norte.",
 	   		"t17correcta":"0"
 	   	},
 	   	{  
 	   		"t13respuesta":     "Avanzo hacia el Norte.",
 	   		"t17correcta":"1"
 	   	},
 	   	{  
 	   		"t13respuesta":     "Avanzo hacia el Sur.",
 	   		"t17correcta":"0"
 	   	}
 	   	],
 	   	"pregunta":{  
 	   		"c03id_tipo_pregunta":"1",
 	   		"dosColumnas": true,
 	   		"t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona la cantidad de dinero que se obtiene en cada caso. <div  style='text-align:center;'><img style='width: 1200px;' src='mi4e_b01_n01_02_01.png'></div></p>"
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "esp_kb1_s01_a01_02a.png",
 	   		"t17correcta": "0",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "esp_kb1_s01_a01_02f.png",
 	   		"t17correcta": "1",
 	   		"columna":"0"
 	   	},
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "5",
 	   		"t11pregunta": "<p>Resuelve los siguientes problemas.<br><\/p>",
 	   		"tipo": "ordenar",
 	   		"imagen": true,
 	   		"url":"esp_kb1_s01_a01_01.png",
 	   		"respuestaImagen":true, 
 	   		"bloques":false
 
 	   	},
 	   	"contenedores": [
 	   	{"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
 	   	{"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]},
 	   	]
 	   },
 	   {
 	   	"respuestas": [
 	   		{
 	   			"t13respuesta": "<p>3<\/p>",
 	   			"t17correcta": "1"
 	   		},
 	   		{
 	   			"t13respuesta": "<p><sup>1</sup>&frasl;<sub>6</sub></p>",
 	   			"t17correcta": "2"
 	   		}
 	   	],
 	   	"preguntas": [
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "<p>a) Dos pizzas habían sido divididas a la mitad, pero ahora hay 12 invitados.</p><br><img src=''><br><table></tr><tr><td>¿En cuántas partes se debe dividir cada porción?</td><td>"
 	   		},
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "</td></tr><tr><td>¿Qué fracción de una pizza le corresponde a cada invitado?</td><td>"
 	   		},
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "</td></tr></table>"
 	   		}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "Resuelve los siguientes problemas."
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   		{
 	   			"t13respuesta": "<p>3<\/p>",
 	   			"t17correcta": "1"
 	   		},
 	   		{
 	   			"t13respuesta": "<p><sup>1</sup>&frasl;<sub>9</sub></p>",
 	   			"t17correcta": "2"
 	   		}
 	   	],
 	   	"preguntas": [
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "<p>b) Una pizza se había dividido en tres partes, ahora hay 9 invitados.</p><br><img src=''><br><table></tr><tr><td>¿En cuántas partes se debe dividir cada porción?</td><td>"
 	   		},
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "</td></tr><tr><td>¿Qué fracción de una pizza le corresponde a cada invitado?</td><td>"
 	   		},
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "</td></tr></table>"
 	   		}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "Resuelve los siguientes problemas."
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   		{
 	   			"t13respuesta": "<p>3<\/p>",
 	   			"t17correcta": "2"
 	   		},
 	   		{
 	   			"t13respuesta": "<p><sup>2</sup>&frasl;<sub>6</sub></p>",
 	   			"t17correcta": "1"
 	   		}
 	   	],
 	   	"preguntas": [
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "<p>c) Dos pizzas se dividieron en seis partes cada una, pero sólo hay seis invitados.</p><br><img src=''><br><table></tr><tr><td>¿Qué fracción le corresponde a cada invitado?</td><td>"
 	   		},
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "</td></tr><tr><td>De haber sabido que sólo habría seis invitados, ¿en cuántas partes se hubiera partido cada pizza?</td><td>"
 	   		},
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "</td></tr></table>"
 	   		}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "Resuelve los siguientes problemas."
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   		{
 	   			"t13respuesta": "<p>1<\/p>",
 	   			"t17correcta": "2"
 	   		},
 	   		{
 	   			"t13respuesta": "2",
 	   			"t17correcta": "1"
 	   		},
 	   		{
 	   			"t13respuesta": "8",
 	   			"t17correcta": "5"
 	   		}
 	   	],
 	   	"preguntas": [
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "<tables style=''><tr><th>Billete</th><th>Ejemplares</th></tr><tr><td><img src=''></td><td>"
 	   		},
 	   		{
 	   			"c03id_tipo_pregunta": "8",
 	   			"t11pregunta": "</td></tr></table>"
 	   		}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "Completa el siguiente p&aacute;rrafo."
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   		{
 	   			"t13respuesta": "esp_kb1_s01_a01_02a.png",
 	   			"t17correcta": "0",
 	   			"columna":"0"
 	   		},
 	   		{
 	   			"t13respuesta": "esp_kb1_s01_a01_02f.png",
 	   			"t17correcta": "1,2,3",
 	   			"columna":"0"
 	   		}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "5",
 	   		"t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
 	   		"tipo": "ordenar",
 	   		"imagen": true,
 	   		"url":"esp_kb1_s01_a01_01.png",
 	   		"respuestaImagen":true, 
 	   		"bloques":false
 
 	   	},
 	   	"contenedores": [
 	   		{"Contenedor": ["", "201,0", "cuadrado", "78, 81", "."]},
 	   		{"Contenedor": ["", "201,81", "cuadrado", "78, 81", "."]}
 	   	]
 	   },
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_1.png",
 	   		"t17correcta": "0",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_1.png",
 	   		"t17correcta": "0",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_2.png",
 	   		"t17correcta": "0",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_8.png",
 	   		"t17correcta": "0",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_6.png",
 	   		"t17correcta": "1",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_3.png",
 	   		"t17correcta": "1",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_4.png",
 	   		"t17correcta": "1",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_7.png",
 	   		"t17correcta": "1",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_5.png",
 	   		"t17correcta": "1",
 	   		"columna":"0"
 	   	},
 	   	{
 	   		"t13respuesta": "mat_db1_s01_a01_7.png",
 	   		"t17correcta": "1",
 	   		"columna":"0"
 	   	}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "5",
 	   		"t11pregunta": "<p>contenedor]Coloca en el contenedor las piezas que necesitas agregar a la figura para que complete correctamente la sucesión.<br><\/p>",
 	   		"tipo": "ordenar",
 	   		"imagen": true,
 	   		"url":"balanza-resultado_1.png",
 	   		"respuestaImagen":true, 
 	   	  "anchoImagen": 60, 
 	   	  "bloques":false,
 	   	  "posicionamiento": "descendente"
 		  
 	   	},
 	   	"contenedores": [
 	   	{"Contenedor": ["", "118,102", "cuadrado", "150, 316", "."]}
 	   	]
 	   },
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "<p>$8.10<\/p>",
 	   		"t17correcta": "1"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$7.10<\/p>",
 	   		"t17correcta": "2"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$16.00<\/p>",
 	   		"t17correcta": "3"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$15.00<\/p>",
 	   		"t17correcta": "4"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$10.00<\/p>",
 	   		"t17correcta": "5"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$14.00<\/p>",
 	   		"t17correcta": "6"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>$9.00<\/p>",
 	   		"t17correcta": "7"
 	   	}
 	   	],
 	   	"preguntas": [
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<table style='margin-left: auto; margin-right:auto;'><tr><th style='text-align: center;'>Operación</th><th style='text-align: center;'>Resultado</th></tr><tr><td style='font-size: 20px;'>$4.50 + $3.60</td><td>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "</td></tr><tr><td style='font-size: 20px;'>$2.10 + $5.00</td><td>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "</td></tr><tr><td style='font-size: 20px;'>$9.30 + $6.70</td><td>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "</td></tr><tr><td style='font-size: 20px;'>$10.50 + $4.50</td><td>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "</td></tr><tr><td style='font-size: 20px;'>$4.80 + $5.20</td><td>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "</td></tr></table>"
 	   	}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "8",
 	   		"pintaUltimaCaja": false,
 	   		"contieneDistractores": true,
 	   		"t11pregunta": "Coloca las cantidades en el lugar que corresponde."
 	   	}
 	   },
 	   {
 	   	"respuestas": [
 	   	{
 	   		"t13respuesta": "<p>155<\/p>",
 	   		"t17correcta": "1"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>168<\/p>",
 	   		"t17correcta": "2"
 	   	},
 	   	{
 	   		"t13respuesta": "<p>300<\/p>",
 	   		"t17correcta": "3"
 	   	}
 	   	],
 	   	"preguntas": [
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p><br>a) Para mejorar su inglés, Montserrat planea aprenderse cinco verbos diarios durante un mes (31 días) ¿Cuántos verbos habrá aprendido al final?<\/p>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p><br>b) Fernanda había colocado diez filas con diez sillas en cada una, después tuvo que colocar cuatro sillas más en cada fila, y finalmente dos filas más. ¿Cuántas sillas hay disponibles en total?<\/p>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p><br>c) Leonardo vendió su colección de muñecos en $15 cada uno; si en total vendió 20 muñecos, ¿cuánto dinero recaudó por la venta? $<\/p>"
 	   	},
 	   	{
 	   		"c03id_tipo_pregunta": "8",
 	   		"t11pregunta": "<p> <\/p>"
 	   	}
 	   	],
 	   	"pregunta": {
 	   		"c03id_tipo_pregunta": "8",
 	   		"pintaUltimaCaja": false,
 	   		"contieneDistractores": false,
 	   		"textosLargos": "si",
 	   		"t11pregunta": "Arrastra la palabra que corresponde en cada caso."
 	   	}
 	   },
	   {
	   	"preguntas": [
	   	{
	   		"t11pregunta": "<img src=\"esp_db1_i_32_a_01.png\" style=\"height: 84px;\" style=\"height: 84px;\" alt=\"\" \/>",
	   		"c03id_tipo_pregunta": "12"
	   	},
	   	{
	   		"t11pregunta": "<img src=\"esp_db1_i_32_a_02.png\" style=\"height: 84px;\" alt=\"\" \/>",
	   		"c03id_tipo_pregunta": "12"
	   	},
	   	{
	   		"t11pregunta": "<img src=\"esp_db1_i_32_a_03.png\" style=\"height: 84px;\" alt=\"\" \/>",
	   		"c03id_tipo_pregunta": "12"
	   	}
	   	],
	   	"respuestas": [
	   	{
	   		"t17correcta": "1",
	   		"t13respuesta": "Vista superior"
	   	},
	   	{
	   		"t17correcta": "2",
	   		"t13respuesta": "Vista frontal"
	   	},
	   	{
	   		"t17correcta": "3",
	   		"t13respuesta": "Vista inferior"
	   	}
	   	],
	   	"pregunta": {
	   		"c03id_tipo_pregunta": "12",
	   		"t13respuesta": "s1a2",
	   		"anchoImagen": 84,
	   		"altoImagen": 84,
	   		"iconos": true
	   	}
	   },
	   {
	   	"respuestas": [
	   	{
	   		"t13respuesta": "Tiene un ángulo de 90°.",
	   		"t17correcta": "1"
	   	},
	   	{
	   		"t13respuesta": "Sus ángulos internos son menores de 90°.",
	   		"t17correcta": "2"
	   	},
	   	{
	   		"t13respuesta": "Sus tres lados son de igual longitud.",
	   		"t17correcta": "3"
	   	},
	   	{
	   		"t13respuesta": "Dos de sus lados son iguales.",
	   		"t17correcta": "4"
	   	},
	   	{
	   		"t13respuesta": "Tiene un ángulo mayor a 90°.",
	   		"t17correcta": "5"
	   	}
	   	],
	   	"preguntas": [
	   	{
	   		"c03id_tipo_pregunta": "12",
	   		"t11pregunta": "Triángulo rectángulo"
	   	},
	   	{
	   		"c03id_tipo_pregunta": "12",
	   		"t11pregunta": "Triángulo acutángulo"
	   	},
	   	{
	   		"c03id_tipo_pregunta": "12",
	   		"t11pregunta": "Triángulo equilátero"
	   	},
	   	{
	   		"c03id_tipo_pregunta": "12",
	   		"t11pregunta": "Triángulo isósceles"
	   	},
	   	{
	   		"c03id_tipo_pregunta": "12",
	   		"t11pregunta": "Triángulo obtusángulo"
	   	}
	   	],
	   	"pregunta": {
	   		"c03id_tipo_pregunta": "12",
	   		"t11pregunta": "s1a2"
	   	}
	   },
];