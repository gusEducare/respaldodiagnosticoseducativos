json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Hacer alto en los semáforos en rojo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Agredir a los niños menos sociables",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Patear un perro callejero",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ofender a los limpiaparabrisas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Regar los arboles de la banqueta",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Evitar la violencia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Tratar mal a los indígenas que venden artesanía en la calle",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Burlarte de aquellos que se vean distintos a ti.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Favorece la violencia física",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todos los ejemplos de injusticia."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>autorregularnos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>arrastrar<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>emociones<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>control<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>aprendernos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>contralar<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>identificar<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Es muy importante reconocer la necesidad de aprender a <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, sobre todo para no dejarnos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; por nuestras <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Cada uno de nosotros es el dueño de las mismas y así debemos mostrarnos; con el poder de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; sobre lo que sentimos.<br>En la primera infancia es complejo identificar las emociones y más aún la forma de mostrarlas; ¿recuerdas niños haciendo berrinches en el supermercado llorando a mares por lo que consideramos algo sin importancia? Bueno pues con el tiempo <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; a <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y encausar de la mejor manera nuestras emociones.<br>Al inicio nuestros padres y familia nos ayudan a <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y regular nuestras reacciones emocionales, después a medida que los niños se hacen mayores, ejercitan más y mejor la autorregulación.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras para darle congruencia a los párrafos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Reconocer qué tipo de emoción es.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Clasificar si la emoción es positiva o negativa.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Al reconocer una emoción negativa, debes resolver la causa o el problema que la produce.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Expresar  la emoción de forma adecuada para ti y los demás.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena los pasos que pueden ayudarte al autocontrol emocional.<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores":[
          {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
          {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "preguntas": [
            {
                "t11pregunta": "<img src=\".png\" style=\"height: 84px;\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\".png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\".png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\".png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\".png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\".png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            }
        ],
        "respuestas": [
            {
                "t17correcta": "1",
                "t13respuesta": "Decepción"
            },
            {
                "t17correcta": "2",
                "t13respuesta": "Felicidad"
            },
            {
                "t17correcta": "3",
                "t13respuesta": "Alegría"
            },
            {
                "t17correcta": "4",
                "t13respuesta": "Enojo"
            },
            {
                "t17correcta": "5",
                "t13respuesta": "Furia"
            },
            {
                "t17correcta": "6",
                "t13respuesta": "Indecisión"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Arrastra  el nombre de la emoción al lugar que le corresponde."
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Los sitios donde producen son lejanos a los sitios de consumo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No cumplen con estándares de limpieza.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No tienen una marca que genere confianza en el comprador.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Tienen menor calidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No cuentan con un sistema óptimo para transportar.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No tienen la infraestructura para llegar al comprador.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala algunos de los motivos por los que pequeños  productores venden a intermediarios y no a consumidores finales."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las decisiones que tomó en el día a día pueden impactar mi futuro. Por ejemplo: Salir a verme con un desconocido.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mis decisiones personales únicamente me incumben y afectan a mí.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mis decisiones de consumo afectan los recursos del planeta.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mientras soy niños mis decisiones no son trascendentales.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En tanto más responsable y analítico sea podré tomar mejores decisiones.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Anota falso (F) o verdadero (V) según corresponda.<\/p>",
            "evaluable": true,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img src=\"EJEMPLO.png\"><\/p>",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor las imágenes de  los que son productos artesanales.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Productos Artesanales",
            "Productos No Artesanales"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Mis padres",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Mis padres y hermanos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Yo mismo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Mis amigos y los profesores",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El gobierno",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Señala a todos los responsables de tus emociones positivas o negativas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Busca ganancias pero también el desarrollo integral de las personas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Refiere al equilibrio entre desarrollo sustentable y responsabilidad social.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es el que realizan las grandes cadenas de autoservicio internacionales.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Implica el uso de comerciales veraces para vender productos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Contribuye al desarrollo sostenible ofreciendo mejores condiciones comerciales y asegurando los derechos de los pequeños productores.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Asegura los derechos de los pequeños productores.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Resalta la necesidad de un cambio en las reglas y prácticas del comercio convencional y muestra cómo un negocio exitoso puede también dar prioridad a la gente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Muestra cómo un negocio exitoso puede también dar prioridad a la gente.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todo lo que define al comercio justo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Virtudes espirituales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Ejemplos de bondad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Valores universales",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cómo se conceptualiza a un grupo de cualidades del ser humano consideradas socialmente positivas sin distinguir países ni culturas? Algunos ejemplos son: Respeto a la vida, libertad, bondad, justicia, igualdad, amor, responsabilidad, solidaridad, verdad, honor y la paz."
        }
    }
]
