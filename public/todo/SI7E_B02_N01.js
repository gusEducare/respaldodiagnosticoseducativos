json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>monografía<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Portada<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Índice<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Introducción<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Cuerpo del trabajo<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Conclusiones<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Fuentes de consulta<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Se nombra <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;a un informe escrito cuya finalidad es informar sobre un tema en específico dentro del ámbito académico. En su estructura se cuenta:<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Con  datos de la institución, título, nombre y  lugar con fecha.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Comprende los capítulos de la investigación y las paginas donde se encuentra cada uno.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Explica el motivo de la elección del tema; los objetivos y propósitos de la investigación y la estructura del trabajo.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: El desarrollo de la monografía.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Presenta una síntesis del trabajo.<br><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: Lista de libros; revistas; enciclopedias y fuentes digitales de donde se obtuvo la información.<\/p>"
            },
              
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "respuestasLargas":true,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Escape de Neptuno<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El maravilloso mundo de los insectos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Recetas Mixtecas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mi Visita al imperio romano<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta":"Elige todos los títulos que utilizarías para un cuento de ciencia ficción",
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Llegó a México el Papa Francisco<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El papa no observará la realidad Méxicana.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor correspondiente",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Hecho",
            "Opinión"
        ]
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Querella",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Plenaria",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Debate",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Moderar",
                "t17correcta": "3"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Discutir sobre determinado tema; usualmente se realiza con opiniones distintas entre los participantes. Comprende una serie de momentos en los que se intercambian posturas y se llegan acuerdos.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para participar se debe elaborar; con anticipación, un guión donde se registra la información y nuestra postura respecto al tema.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se realiza con una estructura específica respetando los turnos para hablar.",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    }
]


