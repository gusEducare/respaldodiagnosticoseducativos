json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Una caja que presenta fricción con el piso al moverla. Más rápido se mueve, mayor la fricción generada.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Jalar un mantel de la mesa sin que se muevan los trastes.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cohetes de un transbordador espacial elevando la nave.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "3ª Ley	"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "1ª Ley	"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "2ª Ley	"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E194.6 kg* m/s<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E195kg* m/s<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E196.2kg* m/s<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E197.5 kg* m/s<sup>2</sup>\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Calcula el peso de una persona cuya masa es de 20 Kg"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003Em= 0.50 kg p= 0.5 kg* Km/h\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Em= 0.1 kg p= 0.1 kg* Km/h\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E m= 0.50 kg p= 1.0kg* Km/h\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Em= 1.0 kg p= 0.5 kg* Km/h\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Calcula la masa de un perrito que pesa 5 kg, después calcula la cantidad de movimiento si corre un Km / h. "
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E0.456 X 10-10 N\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E456 X 10-10 N\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E4.56X10-10 N\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E45.6 X 10-10 N\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Calcula la fuerza de atracción entre dos personas cuyas masas son M=10.8 kg y m=5.7 kg, además la distancia a la que se encuentran es de 3m, recuerda que el valor de la constante gravitacional es  6.6726 x 10-11N*m2kg2"
      }
   }
   ,
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003Ev = 2E/m \u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Ev = (2E/m)1/2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Ev = (2E/m)1/4\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E(v)1/2= 2E/m\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Encuentra la expresión para la velocidad en términos de la Energía cinética y la masa."
      }
   } ,
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E3000 Kg * (m/s)2\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E4000 Kg * (m/s)2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E5000 Kg * (m/s)2u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E6000 Kg * (m/s)2\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Encuentra la energía cinética de una persona cuya masa es de 60kg y su velocidad es de 10 m/s."
      }
   } ,
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E3000 Kg * (m/s)2 \u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1500 Kg * (m/s)2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E750 Kg* (m/s)2\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E375 Kg * (m/s)2\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Si la persona del ejercicio anterior disminuye su velocidad a la mitad, ¿Cuál es su energía cinética?"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Cuerda y viento",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     " Peso y cuerda",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     " Viento y peso",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Peso",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Analiza las fuerzas mostradas en la imagen y selecciona las que son necesariamente mayores para mantener en vuelo a un papalote. <div  style='text-align:center;'><img style='height: 360px' src='.png'></div></p>"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "<img src=''>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     " <img src=''>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     " <img src=''>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "<img src=''>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Un automóvil puede ser levantado por una grúa, jalado con una cuerda o empujado. Recuerda que la gravedad estaría actuando sobre el automóvil hacia abajo.  Selecciona el movimiento que requiere de mayor fuerza, arrastrando al contenedor la flecha que lo representa en la imagen mostrada: <div  style='text-align:center;'><img style='height: 360px' src='.png'></div></p>"
      }
   }
];


