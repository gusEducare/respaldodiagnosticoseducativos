[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>estado<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>enfrentadas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>guerras<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>meta<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>diálogo<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La paz es una situación o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; que nos corresponde a toda la humanidad dado que para lograrla hay que eliminar toda aquella situación en la que dos o más partes están <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y por lo tanto, los seres humanos somos quienes podemos contribuir a eliminar las <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; o las injusticias. Cada pequeño esfuerzo suma, erradicar la guerra en el mundo debe ser el objetivo y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de cada uno de los individuos. La sana convivencia se da a través del <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y de buscar metas que contribuyan al beneficio de todos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
       {  
      "respuestas":[  
         {  
            "t13respuesta":"Contribución",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Diálogo",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Confrontación",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Comunicación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Conflictos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Anarquía",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las características que nos hablan de lo que implica la participación ciudadana."
      }
   },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Participar en votaciones convocadas por los gobiernos para elecciones.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Manifestarse fuera de las oficinas de gobierno para ser escuchados, sin importar si es que se realizan disturbios.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Organizar marchas o manifestaciones donde se ponga en riesgo el orden público.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Participar en plebiscitos para llegar a acuerdos que beneficien a la comunidad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Solicitar información de la transparencia con la que deben estar actuando las autoridades.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las oraciones que hagan referencia a las acciones que los ciudadanos pueden hacer para comunicarse con las autoridades."
      }
   },
       {
        "respuestas": [
            {
                "t13respuesta": "<p style='font-size:90%'>Cumpliendo con mis obligaciones para lograr los resultados que se esperan, mediante la correcta utilización de los recursos que me son asignados.<br><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p style='font-size:90%'><br>Para que mi actuar dentro de la Administración pública, se apegue a la Ley y demás disposiciones jurídicas y administrativas que rigen<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p style='font-size:90%'><br>En mi diario desempeño sin preferencia o prevención anticipada a favor de persona alguna<br><br><\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p style='font-size:90%'><br>En mi conducta como servidor público ajusándome a los principios morales fundamentales de esta sociedad, y con ello evitar una afectación al interés y a la hacienda pública.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p style='font-size:90%'>Al prestar mis servicios de tal forma que el cumplimiento de mis obligaciones este siempre por encima de mis intereses personales, asi como que los recursos que se me asignen, sean utulizados exclusivamente para el fin correspondiente.<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Legalidad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Honradez"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Lealtad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Imparcialidad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Eficacia"
                
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Escribe los incisos de las definiciones con los siguientes conceptos sobre las obligaciones de los servidores públicos."         }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El gobierno subsidia a las personas morales para que puedan crear sindicatos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las asociaciones religiosas pueden ser empresas lucrativas que dependen del gobierno.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una nación está constituida por un gobierno y la sociedad civil.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando la sociedad civil se organiza puede obligar al gobierno a atender alguna demanda apoyando a alguna fundación o institución no lucrativa.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las iglesias son asociaciones con fines de lucro que pueden llegar a ser autosuficientes, es decir, pueden existir sin el patrocinio del gobierno.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    }
]