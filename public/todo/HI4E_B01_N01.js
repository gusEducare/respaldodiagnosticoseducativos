json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "¿Un milenio abarca 500 millones de años?",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tu nacimiento fue a.C.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hace 100,000 años a.C. los hombres comenzaron a cultivar la tierra y domesticar animales.",
                "correcta"  : "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la opción correcta:<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Asia<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>África<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Europa<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>México<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Australia<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los primeros pobladores del continente americano provienen de&nbsp;<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa la oración:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Olmeca<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>Zapoteca<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mixteca<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Teotihuacana<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>La cultura&nbsp;"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;es considerada, además de la Maya,  como la madre de las civilizaciones<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "t11pregunta": "Completa la oración:"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ETrigo\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EMaíz\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EArroz\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona qué semilla es originaria de Europa."
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003EOrganizados en pequeñas comunidades de forma permanente.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EOrganizaron pequeños sembradíos y domesticaron a ciertos animales.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EDedicados a cazar animales y recolectar plantas y frutas.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003ESus viviendas no eran muy resistentes, por lo que eran temporales.\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"5",
         "t11pregunta":"Arrastra las características que correspondan a los grupos que se presentan a continuación:",
         "tipo":"vertical"
      },
        "contenedores":[ 
            "Sedentarios",
            "Nómadas"
            
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Maíz",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Trigo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Arroz",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "América"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Europa"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Asia"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona la región del mundo con la semilla base de su alimentación:"
        }
    }
];


