json = [
       {
         "respuestas": [
             {
                 "t13respuesta": "<p>1<\/p>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<p>8<\/p>",
                 "t17correcta": "2"
             }
         ],
         "preguntas": [
                     {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<p font-size:18px;>David pasó <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>1</div><div class='bottom'>4</div></div> parte de su vida estudiando, <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>1</div><div class='bottom'>2</div></div>\n\
                                 trabajando,<div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>1</div><div class='bottom'>8</div></div >viajajando.\n\
                                 </br>¿Qué fracción de su vida dedicó a otras cosas?<div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top' style='font-size:18px;'>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "</div><div class='bottom'>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "</div></div>"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "8",
             "pintaUltimaCaja":false,
             "ocultaPuntoFinal":true,
             "soloTexto":true,
             "respuestasLargas":true,
             "anchoRespuestas":25,
             "t11pregunta": "Coloca los números en el lugar que les corresponde."
         }
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "<center><table style='padding:none;; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>14</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>6</td></tr></table></center>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<center><table style='padding:none;; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>13</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>4</td></tr></table></center>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<center><table style='padding:none;; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>32</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>8</td></tr></table></center>",
                 "t17correcta": "3"
             },
             {
                 "t13respuesta": "<center><table style='padding:none; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>16</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>10</td></tr></table></center>",
                 "t17correcta": "4"
             },
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<br><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>5</div><div class='bottom'>3</div></div> + <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>4</div><div class='bottom'>6</div></div> = "
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<hr width='200px' style='display:inline-block; border-color:transparent;'><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>3</div><div class='bottom'>2</div></div> + <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>7</div><div class='bottom'>4</div></div> = "
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<hr width='200px' style='border-color:transparent; height:50px;'><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>4</div><div class='bottom'>8</div></div> + <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>7</div><div class='bottom'>2</div></div> = "
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<hr width='200px' style='display:inline-block; border-color:transparent;'><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>4</div><div class='bottom'>10</div></div> + <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>6</div><div class='bottom'>5</div></div> = <script>var heightSpan= $('.respuestasArrastra').height();var widthtSpan= $('.respuestasArrastra').width();$('#texto span').css({position: 'relative', top:'-32px'});</script>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": ""
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "8",
             "pintaUltimaCaja":false,
             "ocultaPuntoFinal":true,
             "respuestasLargas":true,
             "soloTexto":true,
             "anchoRespuestas":35,
             "altoRespuestas":80,
             "t11pregunta": "Completa el siguiente p&aacute;rrafo."
         }
     },
     {
         "respuestas": [
             {
                 "t13respuesta": "<center><table style='padding:none;; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>2</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>15</td></tr></table></center>",
                 "t17correcta": "1,3"
             },
             {
                 "t13respuesta": "<center><table style='padding:none;; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>1</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>9</td></tr></table></center>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<center><table style='padding:none;; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>2</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>15</td></tr></table></center>",
                 "t17correcta": "3,1"
             },
             {
                 "t13respuesta": "<center><table style='padding:none; background:none; border:0; margin-top:10px;'><tr><td style='padding-bottom:10px; font-size:21px;'>5</td></tr><tr><td style='padding:0; height:1px; background:black;'></td></tr><tr><td style='padding-top:10px; font-size:21px;'>6</td></tr></table></center>",
                 "t17correcta": "4"
             },
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<br><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>8</div><div class='bottom'>15</div></div> - <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>2</div><div class='bottom'>5</div></div> = "
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<hr width='200px' style='display:inline-block; border-color:transparent;'><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>13</div><div class='bottom'>9</div></div> - <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>4</div><div class='bottom'>2</div></div> = "
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<hr width='200px' style='border-color:transparent; height:50px;'><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>8</div><div class='bottom'>15</div></div> - <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>2</div><div class='bottom'>5</div></div> = "
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<hr width='200px' style='display:inline-block; border-color:transparent;'><div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>5</div><div class='bottom'>2</div></div> - <div class='fraction' style='position: relative; top:20px; margin-top:-35px;'><div class='top'>10</div><div class='bottom'>6</div></div> = <script>var heightSpan= $('.respuestasArrastra').height();var widthtSpan= $('.respuestasArrastra').width();$('#texto span').css({position: 'relative', top:'-32px'});</script>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": ""
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "8",
             "pintaUltimaCaja":false,
             "ocultaPuntoFinal":true,
             "respuestasLargas":true,
             "soloTexto":true,
             "anchoRespuestas":20,
             "altoRespuestas":80,
             "t11pregunta": "Completa el siguiente p&aacute;rrafo."
         }
     },
    {  
       "respuestas":[  
          {  
             "t13respuesta":"50",
             "numeroPregunta": "0",
             "t17correcta":"1"
          }, 
          {  
             "t13respuesta":"100",
             "numeroPregunta": "0",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"150 ",
             "numeroPregunta": "0",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"60",
             "numeroPregunta": "1",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"70",
             "numeroPregunta": "1",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"80 ",
             "numeroPregunta": "1",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"71",
             "numeroPregunta": "2",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"72",
             "numeroPregunta": "2",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"73",
             "numeroPregunta": "2",
             "t17correcta":"0"
          }
       ],
       "pregunta":{  
          "c03id_tipo_pregunta":"2",
          "preguntasMultiples": true,
          "columnas": 1,
          "t11pregunta":["Sin hacer la división, elige la mejor aproximación.<br/>286÷4","286÷4","286÷4"]
       }
    },
    {  
       "respuestas":[  
          {  
             "t13respuesta":"10",
             "numeroPregunta": "0",
             "t17correcta":"0"
          }, 
          {  
             "t13respuesta":"20",
             "numeroPregunta": "0",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"22 ",
             "numeroPregunta": "0",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"70",
             "numeroPregunta": "1",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"700",
             "numeroPregunta": "1",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"7 000 ",
             "numeroPregunta": "1",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"321",
             "numeroPregunta": "2",
             "t17correcta":"1"
          },
          {  
             "t13respuesta":"3 021",
             "numeroPregunta": "2",
             "t17correcta":"0"
          },
          {  
             "t13respuesta":"3 201",
             "numeroPregunta": "2",
             "t17correcta":"0"
          }
       ],
       "pregunta":{  
          "c03id_tipo_pregunta":"2",
          "preguntasMultiples":true,
          "columnas":1,
          "t11pregunta":["Sin hacer las divisiones elige la opción que representa el cociente exacto.<br/>220÷11","3 500÷50","9 630÷30"]
       }
    },    
     {
         "respuestas": [
             {
                 "t13respuesta": "Falso",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "Verdadero",
                 "t17correcta": "1"
             }
         ],
         "preguntas" : [
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Todo par de rectas perpendiculares es un par de rectas secantes. ",
                 "correcta"  : "1"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Todo par de rectas secantes es un par de rectas perpendiculares.",
                 "correcta"  : "0"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Dos rectas paralelas forman un ángulo recto entre sí.",
                 "correcta"  : "0"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Cualesquiera dos rectas, siempre forman cuatro ángulos.",
                 "correcta"  : "0"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "13",
             "t11pregunta": "<p>Califica las siguientes afirmaciones.<\/p>",
             "descripcion": "",   
             "variante": "editable",
             "anchoColumnaPreguntas": 60,
             "evaluable"  : true
         }        
     },   
     {
         "respuestas": [
             {
                 "t13respuesta": "<p>8<\/p>",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "<p>7<\/p>",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "<p>5<\/p>",
                 "t17correcta": "3"
             }
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<div style='display:inline'><table class='customTableDiv'><tr><td></td><td>1 2</td><td></td><td></td></tr><tr><td>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "</td><td class='tdDiv' colspan='3'>30</td></tr><tr><td></td><td>6</td><td></td><td></td></tr></table></div><div style='display:inline'><table class='customTableDiv'><tr><td></td><td>1 1</td><td></td><td></td></tr><tr><td>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "</td><td class='tdDiv' colspan='3'>48</td></tr><tr><td></td><td>6</td><td></td><td></td></tr></table></div><div style='display:inline'><table class='customTableDiv'><tr><td></td><td>1 5</td><td></td><td></td></tr><tr><td>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "</td><td class='tdDiv' colspan='3'>69</td></tr><tr><td></td><td>4</td><td></td><td></td></tr></table> </div><script> $('.customTableDiv').css({border: '0', background:'transparent', 'padding-left':'50px', display:'inline'}); $('.customTableDiv td').css({width:'50px', height:'40px', background:'#f2f2f2', 'font-size':'24px'}); $('.customTableDiv .tdDiv').css({'border-top':'1px solid black','border-left':'1px solid black'});</script>"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "8",
             "pintaUltimaCaja":false,
             "ocultaPuntoFinal":true,
             "soloTexto":true,
             "respuestasLargas":true,
             "anchoRespuestas":20,
             "t11pregunta": "Arrastra los números que son viables en cada división."
         }
     },
    {  
      "respuestas":[
         {  
            "t13respuesta":"94 = 14 × 10 + 6",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"94 = 10 × 6 + 14",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"94 = 14 × 6 + 10",
            "numeroPregunta":"1",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Restar 6 a 14",
            "numeroPregunta":"2",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Restar 10 a 94",
            "numeroPregunta":"2",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Restar 14 a 94",
            "numeroPregunta":"2",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"El cociente se duplica",
            "numeroPregunta":"3",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"El cociente reduce a la mitad",
            "numeroPregunta":"3",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"El residuo se duplica",
            "numeroPregunta":"3",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"El cociente se duplica",
            "numeroPregunta":"4",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"El residuo se duplica",
            "numeroPregunta":"4",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"El cociente y el residuo se duplican",
            "numeroPregunta":"4",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "preguntasMultiples":true,
         "t11pregunta":["Observa la división y elige la opción que corresponde a cada enunciado.<br/><table class='customTableDiv'><tr><td></td><td>6</td><td></td></tr><tr><td>14</td><td class='tdDiv' colspan='2'>94</td></tr><tr><td></td><td>10</td><td></td></tr></table><script> $('.customTableDiv').css({border: '0', background:'transparent', 'padding-left':'50px', display:'inline'}); $('.customTableDiv td').css({width:'50px', height:'40px', background:'#f2f2f2', 'font-size':'24px'}); $('.customTableDiv .tdDiv').css({'border-top':'1px solid black','border-left':'1px solid black'});</script><style>.pregunta .circulo-opcMultiple{display: none;}</style>","De acuerdo con la división anterior, ¿cuál de las siguientes expresiones es correcta?","Para que la división anterior sea exacta debemos…","Si intercambiamos 14 por 28, entonces …","Si intercambiamos 94 por 188, entonces …"]
      }
   },     
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mapa corresponde a una parte de la Ciudad de México.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La intención del mapa es mostrar las alternativas viales a usar durante un embotellamiento cualquiera.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La manifestación inicia en el Hemiciclo a Juárez y termina en la Cámara de senadores.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para ir del “hemiciclo a Juárez” rumbo a la “Cámara de senadores”, el trayecto menos adecuado es la ruta marcada por Hidalgo, Alvarado e Insurgentes.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa el mapa y responde las preguntas</p><br/><br/><img src='mi5e_b01_n02_02.png'><br/><br/>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Años",
            "numeroPregunta":"0",
            "t17correcta":"0"
         }, 
         {  
            "t13respuesta":"Días",
            "numeroPregunta":"0",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Minutos ",
            "numeroPregunta":"0",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Años",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Días",
            "numeroPregunta":"1",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Horas ",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Años",
            "numeroPregunta":"2",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Meses",
            "numeroPregunta":"2",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Semanas",
            "numeroPregunta":"2",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "preguntasMultiples":true,
         "t11pregunta":["Elige la unidad más adecuada para medir el tiempo indicado.<br/>Tiempo en que hierve un litro de agua al calor del fuego.","Tiempo para ir caminando de Morelia a Guanajuato. ","Duración de una estación del año"]
      }
   }, 
   {  
      "respuestas":[  
         {  
            "t13respuesta":"10 g",
            "numeroPregunta":"0",
            "t17correcta":"0"
         }, 
         {  
            "t13respuesta":"100 g",
            "numeroPregunta":"0",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"1 000 g",
            "numeroPregunta":"0",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"1/2 kg",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"5 kg",
            "numeroPregunta":"1",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"50 kg ",
            "numeroPregunta":"1",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"10 ml",
            "numeroPregunta":"2",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"100 ml",
            "numeroPregunta":"2",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"1 000 ml",
            "numeroPregunta":"2",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "columnas": 3,
         "preguntasMultiples":true,
         "t11pregunta":["Elige la opción equivalente a la cantidad indicada.<br/>1 kg","5 000 g","0.1 L"]
      }
   },    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>30<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>12<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>50<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>1/2  hora = <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbspminutos <br>1/2 día = <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsphoras <br>1/2  año = <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><p>&nbspmeses <br>1/2 siglo = <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><p>&nbspaños<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "anchoRespuestas":80,
            "t11pregunta": "Arrastra los números que completan las equivalencias."
        }
    },      
      {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Siglo XV</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Siglo XIX</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Siglo XX</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Siglo XV</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Siglo XIX</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Siglo XX</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Siglo XV</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Siglo XIX</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Siglo XX</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona los siglos en los que vivió cada personaje.<br/>Netzahualcóyotl, nació en 1402 y murió en 1472.<br/><img src='MI5E_B01_N02_03_01.png'>",
                         "David Alfaro Siqueiros, nació en 1896 y murió en 1974.<br/><img src='MI5E_B01_N02_03_02.png'>",
                         "Sor Juana Inés de la Cruz, nació en 1651 y murió en 1695.<br/><img src='MI5E_B01_N02_03_03.png'>"],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>36<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "4,5"
            },
            {
                "t13respuesta": "<p>6<\/p>",
                "t17correcta": "5,4"
            },
            {
                "t13respuesta": "<p>30<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><table><tr><td id='td0'>Mochilas</td><td id='td0'>Cuadernos</td><td id='td0'>Colores</td><td id='td0'>Plumas</td><td id='tdImg' rowspan='4'><img src='MI5E_B01_N02_04_02.png'></td></tr><tr><td id='td1'>1</td><td id='td1'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td id='td1'>12</td><td id='td1'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td id='td2'>3</td><td id='td2'>15</td><td id='td2'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td id='td2'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td id='td1'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td id='td1'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td id='td1'>72</td><td id='td1'>12</td></tr></table></center><style> table{background:#3d85c6;} table #td0{width:160px; color:white; font-size:22px;} table #td1{background:#9fc5e8; font-size:18px; text-align:center;} table #td2{background:white; font-size:18px; text-align:center;} table #tdImg{width: 250px; background: white;}</style>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "anchoRespuestas":80,
            "ocultaPuntoFinal":true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Coloca los números en el lugar que les corresponde dentro de la tabla."
        }
    },    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>90<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>180<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>360<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><table><tr><td id='td0'>Cantidad</td><td id='td0'>Costo</td><td id='tdImg' rowspan='5'><img src='MI5E_B01_N02_05_02.png'></td></tr><tr><td id='td1'>1</td><td id='td1'>$45</td></tr><tr><td id='td2'>2</td><td id='td2'>$"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td id='td1'>4</td><td id='td1'>$"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td id='td2'>8</td><td id='td2'>$"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr></table></center><style> table{background:#dd7e6b;} table #td0{width:160px; color:white; font-size:22px;} table #td1{background:#f9cb9c; font-size:18px; text-align:center;} table #td2{background:white; font-size:18px; text-align:center;} table #tdImg{width: 250px; background:white;}</style>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "anchoRespuestas":80,
            "ocultaPuntoFinal":true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Escribe el costo de las diferentes cantidades de piñatas."
        }
    },  
    {
        "respuestas": [
            {
                "t13respuesta": "<p>36<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>27<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>45<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><table><tr><td id='td0'>Cantidad</td><td id='td0'>Costo</td><td id='tdImg' rowspan='6'><img src='mi5e_b01_n02_06.png'></td></tr><tr><td id='td1'>4</td><td id='td1'>$18</td></tr><tr><td id='td2'>8</td><td id='td2'>$"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td id='td1'>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td><td id='td1'>$9</td></tr><tr><td id='td2'>6</td><td id='td2'>$"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr><tr><td id='td1'>10</td><td id='td1'>$"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr></table></center><style>table{background:#dd7e6b;} table #td0{width:160px; color:white; font-size:22px;} table #td1{background:#f9cb9c; font-size:18px; text-align:center;} table #td2{background:white; font-size:18px; text-align:center;} table #tdImg{width: 300px; }</style>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "anchoRespuestas":80,
            "ocultaPuntoFinal":true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Completa la tabla de los diferentes costos de pulseras."
        }
    }
]
