json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Lo más relevante es desarrollar la dimensión personal sin tomar en cuenta la social.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Algunas de las elecciones en la dimensión personal, aun siendo actos privados, se convierten en públicos en el momento que repercuten en la situación y desarrollo de otros individuos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Al tomar cada elección personal siempre considerar la dimensión social.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lo más relevante es desarrollar la dimensión personal sin tomar en cuenta la social.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "En tanto mi dimensión social sea gratificante, es irrelevante la personal",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lo realmente relevante de entre ambas dimensiones es mi desarrollo.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Elige la opción que explique la relación entre las dimensiones personal y social del ser humano."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Afiliación",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Autorrealización",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Fisiológicas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Reconocimiento",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Seguridad",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Señala una de las necesidades básicas del ser humano cuyo origen va a la par del nacimiento."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Carácter privado<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elección y ejercicio de tu sexualidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Eventualmente mis decisiones en esta área afectan la otra dimensión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Carácter publico<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Convivencia adecuada<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respeto y tolerancia a las diferencias<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra las características de acuerdo a su dimensión, al contenedor que corresponda.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Dimensión personal",
            "Dimensión social"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Contagio de Enfermedades de transmisión sexual",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Falta de autoestima",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Embarazo no deseado",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aumento de popularidad entre mis amigos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Maternidad y paternidad temprana",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Contagio de virus de papiloma humano que favorece el cáncer.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Contagio de virus de la inmunodeficiencia humana o VIH",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala todos los posibles efectos de ejercer tu sexualidad sin protección."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Elije la opción sin consecuencias negativas o la que menos tenga y te permita lograr el objetivo inicial<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Identifica las opciones posibles. Toma en cuenta todas las alternativas para llegar a tu objetivo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Encuentra posibilidades. Evalúa cada resultado o consecuencia planteada, no importa si es positiva o negativa.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Proyecta las consecuencias. Debes considerar cada una de las alternativas.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Define el objetivo. Con la definición de los resultados que pretender lograr y la importancia de los mismos<\/p>",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Ordena los pasos para una efectiva toma de decisiones",
            "tipo": "ordenar"
        },
        "contenedores": [
            {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>retribucion<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>oficio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>profesion<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>justicia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>desigualdad<\/p>",
                "t17correcta": "0"
            }

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Pago para cada acción laboral realizada por una persona."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Trabajo que se realiza manualmente. Se aprende empíricamente o con la guía de un experto."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Requiere de conocimientos especializados. Se enseña en universidades o institutos. Cuenta con un respaldo académico para realizar un trabajo específico."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Igualdad de trato y apego a los derechos de cada individuo. "
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Condición o circunstancia de no tener una misma naturaleza, cantidad, calidad, valor o forma que otro."
            }
        ], "pocisiones": [
            {
                "direccion": 0,
                "datoX": 2,
                "datoY": 2
            },
            {
                "direccion": 1,
                "datoX": 11,
                "datoY": 2
            },
            {
                "direccion": 0,
                "datoX": 4,
                "datoY": 7
            },
            {
                "direccion": 1,
                "datoX": 9,
                "datoY": 5
            },
            {
                "direccion": 0,
                "datoX": 3,
                "datoY": 12
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 12,
            "t11pregunta": "Realiza el siguiente crucigrama."
        }
    },
    {
         "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El crecimiento de una sociedad depende del desarrollo personal de cada uno de sus integrantes y el impacto que éste tiene sobre el entorno.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La producción de un país está ligada al desempeño mundial y no al de los trabajadores. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La estructura  de una país se compone de aspectos económicos, organizacionales y de bienestar.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El modelo de producción de un país no garantiza que toda la población tenga acceso a todos los satisfactores que necesita, o bienestar para todas las personas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Promover la polarización de un grupo social, permite que pueda acceder a los derechos que le corresponden.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Distribución inadecuada de la riqueza. No se cumplen con las necesidades básicas de la población.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Medio para atender el bienestar físico de los individuos, así como prevenir enfermedades mortales.\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Falta de oportunidades laborales. Trae consigo consecuencias como trabajos mal remunerados, subempleo o autoempleo.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Uso ilegal de recursos para un entiquecimiento o beneficio personal.<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Pobreza"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Salud"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Desempleo"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Corrupción"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los siguientes conceptos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>madurez<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>conocimiento<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>reproducción<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>sano<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>empatía<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>impulsos<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>La <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; sexual de un individuo implica el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; personal respecto a la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, así como los cuidados del cuerpo. Se considera que un individuo es  <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; sexualmente cuando asume una actitud de responsabilidad personal, pero también frente a su pareja. Esta <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y vinculación entre ambos individuos genera relaciones sanas y encamina los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;sexuales, invitando a cada individuo a prevenir las consecuencias de sus actos. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras correspondientes.",
            "pintaUltimaCaja":false
        }
    },
    {
        "respuestas": [
            
            {
                "t13respuesta": "CI9E_B01_N03_03.png",
                "t17correcta": "0",
                "columna": "1"
            },
            {
                "t13respuesta": "CI9E_B01_N03_02.png",
                "t17correcta": "1",
                "columna": "1"
            },
            {
                "t13respuesta": "CI9E_B01_N03_04.png",
                "t17correcta": "2",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra las imagénes para completar la Pirámide de Maslow<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "CI9E_B01_N03_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            {"Contenedor": ["", "234,344", "cuadrado", "190, 48", ".", "transparent"]},
            {"Contenedor": ["", "302,380", "cuadrado", "190, 48", ".", "transparent"]},
            {"Contenedor": ["", "372,421", "cuadrado", "192, 48", ".", "transparent"]}
        ]
    }
];