json=[
{
	"respuestas": [
	{
		"t13respuesta": "<p>Aparece acné<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Desarrollo del cerebro y todas las habilidades cognitivas<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Crecimiento de vello en genitales y axilas<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Aumento del tejido muscular<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>Desarrollo de habilidades y capacidades físicas e intelectuales<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Mayor independencia<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Mayor socialización<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Ensanchamiento de caderas<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Aumento del tejido adiposo<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Mayor importancia a las relaciones de amistad; la familia ya no es el centro<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Cambio de voz hasta hacerse más grave<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>Desarrollo de senos<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Crecimiento de barba y bigote<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>Crecimiento de huesos y órganos en todo el cuerpo<\/p>",
		"t17correcta": "1"
	},    
	],
	"pregunta": {
		"c03id_tipo_pregunta": "5",
		"t11pregunta": "Arrastra a cada contenedor las características  del desarrollo según correspondan a  niñas, niños o ambos. ",
		"tipo": "horizontal"
	},
	"contenedores":[ 
	"Niñas",
	"Ambos",
	"Niños"
	]
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>Reconoces que hay diferencias en cómo nos vemos, pero no por eso te burlas o haces sentir mal a alguien.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Si alguno de tus compañeros es mucho más alto que la mayoría, es gracioso ponerle sobrenombre que haga referencia a ello.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Cuando alguno de tus amigos hace alguna broma relacionada con el peso de otro amigo, te opones y evitas dichos comentarios.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Trato de comprar la ropa que usan mis artistas favoritos y constantemente estoy a dieta para poder lucir como ellos.<\/p>",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "2",
		"t11pregunta": "Selecciona las opciones que hagan referencia al respeto que debemos tener ante la diversidad."
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>anorexia<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>prevención<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>obesidad<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>bulimia<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>autocuidado<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>adicción<\/p>",
		"t17correcta": "0"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Trastorno de alimentación caracterizado por la baja o nula ingesta de alimentos causada por el miedo a subir de peso existe una distorsión de la propia imagen corporal."
	},
	{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Anticiparse con acciones claras para evitar que suceda alguna situación o evento desagradable."
	},
	{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Acumulación excesiva de grasa en el cuerpo que genera importantes problemas de salud. Es causado por un desequilibrio energético entre calorías consumidas y gastadas."
	},
	{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Es una enfermedad en la cual una persona tiene episodios regulares de comer una gran cantidad de alimentos para luego vomitar o consumir laxantes y así evitar el aumento de peso."
	},
	{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Conjunto de rasgos que hacen de un individuo un ser único, original y distinto a todos los demás."
	},
	{
		"c03id_tipo_pregunta": "15",
		"t11pregunta": "Enfermedad de causas emocionales que causa dependencia afectando a la persona y su familia física y emocionalmente."
	}
	],
	"pocisiones": [
	{
		"direccion" : 1,
		"datoX" : 12,
		"datoY" : 1
	},
	{
		"direccion" : 0,
		"datoX" : 4,
		"datoY" : 3
	},
	{
		"direccion" : 0,
		"datoX" : 6,
		"datoY" : 8
	},
	{
		"direccion" : 1,
		"datoX" : 10,
		"datoY" : 5
	},
	{
		"direccion" : 1,
		"datoX" : 6,
		"datoY" : 5
	},
	{
		"direccion" : 0,
		"datoX" : 1,
		"datoY" : 11
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "15",
		"alto": 20,
		"ancho": 20,
		"t11pregunta": "Completa el siguiente crucigrama"
	}

},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Las adicciones únicamente son a sustancias como alcohol o drogas.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Existen adicciones a sustancias, cosas o situaciones.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Si bien la adicción es considerada una enfermedad, una autoestima bien construida puede evitar caer en ellas.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Usualmente las personas adictas, presentan algunos problemas emocionales que pueden haber detonado dicha situación.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Las adicciones no sólo destruyen a quien las padece, también dañan a su familia, amigos y a la sociedad.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Una vida sana y las decisiones positivas que tomas en tu vida pueden evitar que caigas en situaciones de adicción.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Todos los adictos pueden dejarlo cuando quieran. ",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Las adicciones dañan permanentemente el cuerpo y emociones de los adictos.",
		"correcta"  : "1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Indica falso o verdadero para cada afirmación.<\/p>",
		"descripcion": "Cuestiones",   
		"variante": "editable",
		"anchoColumnaPreguntas": 60,
		"evaluable"  : true
	}        
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>Aumento en la probabilidad de continuar consumiendo drogas o alcohol en la adultez.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Aumento en la talla y altura debido al consumo de sustancias adictivas.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Predispocición a enfermedades respiratorias y trastorno gástricos.<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Disminución de riesgos para contraer enfermedades cardiovasculares.<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Situaciones relacionadas con accidentes automovilísticos relacionados con el consumo de alcohol y drogas.<\/p>",
		"t17correcta": "1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "2",
		"t11pregunta": "Selecciona algunas de las consecuencias que puede tener el consumo de drogas o alcohol en menores:"
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "Falso",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "Verdadero",
		"t17correcta": "1"
	}
	],
	"preguntas" : [
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Las adicciones están relacionadas con los jóvenes.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Todas las personas pueden dejar una adicción si tienen un tratamiento.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Es muy fácil que desarrolles una adicción si tienes problemas para comunicarte.",
		"correcta"  : "1"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "El riesgo de caer en una adicción sólo lo tienen las personas que no tienen dinero.",
		"correcta"  : "0"
	},
	{
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "Puedes caer en una adicción por tratar de pertenecer  un grupo.",
		"correcta"  : "1"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "13",
		"t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
		"descripcion": "Cuestiones",   
		"variante": "editable",
		"anchoColumnaPreguntas": 60,
		"evaluable"  : true
	}        
},
{
	"respuestas": [
	{
		"t13respuesta": "Estrechamiento de los pequeños vasos sanguíneos que suministran sangre y oxígeno al corazón.",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "Presión arterial alta.",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "Falta o suspensión de respiración",
		"t17correcta": "3"
	},
	{
		"t13respuesta": "Estado en el que se tiene sensación de cansancio, pesadez, sueño, embotamiento de los sentidos y torpeza en los movimientos.",
		"t17correcta": "4"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Cardiopatía coronaria"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Hipertensión"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Apnea"
	},
	{
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Somnolencia"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "18",
		"t11pregunta": "Une los riesgos a los que te expones cuando tienes alguna adicción o trastorno alimentario:"
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>Ataques cardiacos<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Mejor autoestima<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Pérdida de memoria<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Diabates<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Infecciones de transmisión sexual<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Madurez<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Osteoporosis<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Falta de concentración<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>Fortalece el sistema inmunológico<\/p>",
		"t17correcta": "0"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "2",
		"t11pregunta": "Selecciona todas aquellas acciones que pueden ser un riesgo por padecer algún trastorno alimenticio o adicción:"
	}
},
{
	"respuestas": [
	{
		"t13respuesta": "<p>Rasgos<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Fenotipo<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Genotipo<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Entorno<\/p>",
		"t17correcta": "0"
	},
	{
		"t13respuesta": "<p>Linaje<\/p>",
		"t17correcta": "0"
	}        
	],
	"pregunta": {
		"c03id_tipo_pregunta": "16",
		"t11pregunta": "Realiza el siguiente crucigrama:"
	}
},
{

	"respuestas": [
	{
		"t13respuesta": "<p>padres<\/p>",
		"t17correcta": "1"
	},
	{
		"t13respuesta": "<p>dominante<\/p>",
		"t17correcta": "5"
	},
	{
		"t13respuesta": "<p>genético<\/p>",
		"t17correcta": "3"
	},


	{
		"t13respuesta": "<p>rasgos<\/p>",
		"t17correcta": "4"
	},
	{
		"t13respuesta": "<p>apariencia<\/p>",
		"t17correcta": "2"
	},
	{
		"t13respuesta": "<p>amigos<\/p>",
		"t17correcta": "4"
	}
	],
	"preguntas": [
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>Nuestros <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;contribuyen en la determinacióń de nuestra <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p> transmitiendo material <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>. <br>La mayoría de los <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>&nbsp;que podemos ver e identificar en otra persona, por ejemplo: el color de cabello o de ojos, la altura, el peso, el color de piel, las huellas digitales, la forma del rostro y las facciones, se promedian de los rasgos de tus padres; del grupo <\/p>"
	},
	{
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "<p>, ya sea el de tu padre o tu madre.<\/p>"
	}
	],
	"pregunta": {
		"c03id_tipo_pregunta": "8",
		"t11pregunta": "Arrastra las palabras para completar la siguiente información.",
		"pintaUltimaCaja": false,
	}
}
];