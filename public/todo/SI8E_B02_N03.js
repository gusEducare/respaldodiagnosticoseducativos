json=[
 {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Definir un tema de interés actual para el colectivo.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Definir varios temas de tu interés.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Estudiar a profundidad el tema o problemática.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>No es necesario que investigues respecto al tema, con lo que conoces basta.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Conocer el rol activo  que desempeñaras ya sea como moderador,  expositor o audiencia.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Elegir una postura respecto al tema o problemática.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Conocer el rol activo  que desempeñaras ya sea como moderador,  expositor o audiencia.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Cualquiera puede tomar uno de los roles ya estando en la mesa.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Prepararte para ofrecer al público o audiencia una discusión en el que se intercambien puntos de vista sobre el tema elegido.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Ofrecer a la audiencia información importante y respaldar los argumentos de manera objetiva.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Presentar a la audiencia información relevante y respaldar tus argumentos de manera objetiva.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Dar tus puntos de vista aun cuando no los respaldes con investigación previa.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Prepararte para las posibles preguntas que puede hacer al final la audiencia.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>No permitir preguntas de la audiencia.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Precisar el orden de las intervenciones y la duración de cada una.</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"De entre las opciones arrastra al contenedor todo lo que debes prever a tu participación en una Mesa redonda."
      }
   },
 {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Presentar información estadística</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Presentar semejanzas y diferencias entre dos hechos.</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Resumir las consecuencias actuales de los hechos.</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Ilustrar cuantitativamente las consecuencias de los hechos</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa el siguiente cuadro comparativo y determina si es utilizado para: <br/><img>"
      }
   },   
 {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Raza del hablante</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Lugar de procedencia del hablante</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Edad del hablante</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Recursos económicos del hablante</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Nivel educativo del hablante</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Sexo del hablante</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Cuando nos referimos a variantes léxicas nos referimos a las distintas maneras de nombrar y expresar una mismo significante. Las mismas pueden ser originadas por:"
      }
   },   
 {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Dialogos</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Desenlace</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Nudo</p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Es esta parte del relato se presenta la resolución de la situación planteada."
      }
   },   
 {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Secuencia de acción</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Desenlace</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El autor plantea el ambiente, personajes y lugar donde se desarrollará la historia."
      }
   },   
    {  
         "respuestas":[  
            {  
               "t13respuesta":"<p>Secuencia de acción</p>",
               "t17correcta":"0"
            },
            {  
               "t13respuesta":"<p>Nudo</p>",
               "t17correcta":"1"
            },
            {  
               "t13respuesta":"<p>Introducción</p>",
               "t17correcta":"0"
            }
         ],
         "pregunta":{  
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Indica la situación que los personajes deben enfrentar y solucionar."
         }
      },   
    {  
         "respuestas":[  
            {  
               "t13respuesta":"<p>Secuencia de acción</p>",
               "t17correcta":"0"
            },
            {  
               "t13respuesta":"<p>Dialogos</p>",
               "t17correcta":"0"
            },
            {  
               "t13respuesta":"<p>Narrador</p>",
               "t17correcta":"1"
            }
         ],
         "pregunta":{  
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Aquel que lleva la voz de la historia, todo realto es contado por uno de ellos."
         }
      },   
    {  
         "respuestas":[  
            {  
               "t13respuesta":"<p>Dialogos</p>",
               "t17correcta":"1"
            },
            {  
               "t13respuesta":"<p>Secuencia de acción</p>",
               "t17correcta":"0"
            },
            {  
               "t13respuesta":"<p>Narrador</p>",
               "t17correcta":"0"
            }
         ],
         "pregunta":{  
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Las palabras que emplean los personajes para comunicarse."
         }
      },   
    {  
         "respuestas":[  
            {  
               "t13respuesta":"<p>Dialogos</p>",
               "t17correcta":"0"
            },
            {  
               "t13respuesta":"<p>Secuencia de acción</p>",
               "t17correcta":"1"
            },
            {  
               "t13respuesta":"<p>Narrador</p>",
               "t17correcta":"0"
            }
         ],
         "pregunta":{  
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Orden en el que se mencionan los hechos para que el lector conozca lo sucedió en el pasado, lo que está sucediendo en el presente y lo que podría seceder en el futuro."
         }
      },   
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Urbano Gómez<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Don Urbano<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Dimas “el Abuelo“<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Fidencio Gómez<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La Arremangada<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Lucio Chico<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>“la Berenjena“<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Natalia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Nachito Rivero<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Inés<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Don Refugio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tío Fidencio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Fulano<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Padre cura<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No lo explica exactamente el autor, sin embargo se asume que es un pueblo.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Personajes",
            "Ambiente en que se desarrolla"
        ]
    }
]