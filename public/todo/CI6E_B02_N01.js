json = [
        {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones son alteraciones del ánimo que se acompañan de cierta conmoción somática.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones son mucho más cortas que los sentimientos y no pueden mantenerse por largos periodos de tiempo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nuestros sentimientos y emociones pueden ser modificadas por el ambiente en el que nos desarrollamos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>emociones<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>fisicas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>feliz<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>sonríes<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>enojado<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>respiración<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>rápida<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>calor<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Sabes que aún cuando intentes controlar todas tus <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; existen respuestas <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; instantáneas a ellas, por ejemplo cuando estás <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y sin pensarlo <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; todo el tiempo o cuando estás muy <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; y tu <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; se hace más <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; además de tener una sensación de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='.png'\/>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='.png'\/>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='.png'\/>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='.png'\/>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<img src='.png'\/>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Enojo</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Ternura</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Envidia</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Tristeza</p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Gratitud</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona con una línea el  hecho y emoción que puede provocar."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ETodas las personas sienten lo mismo en determinadas situaciones.\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ECada persona los vive de determinada forma y en momentos diferentes.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ELas personas cambian drásticamente con el tiempo.\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta que completa la oración.(Opción múltiple)<br/>Las emociones y sentimientos son personales por qué:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Ayudar en tareas de la casa a cambio de dinero.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ahorrar parte del dinero que obtengan.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Invertir sus ahorros con su pap&aacute;.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Organizar una venta de cosas que ya no utilizan.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No comprar todo lo que quieren y no necesitan.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Reparar algunos juguetes y venderlos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>No pedir dinero prestado a sus compa&ntilde;eros o amigos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No gastar m&aacute;s de lo que tienen.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ofrecer al vecino lavar su auto.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hacer mandados a su t&iacute;a.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada contenedor las imágenes que presentan situaciones  justas o injustas.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Situaciones justas",
            "Situaciones injustas"
        ]
    }
]