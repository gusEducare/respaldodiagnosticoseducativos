json=[
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Es anónimo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Lo redacta un escritor reconocido.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Explica el movimiento estelar.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Trata temas de la vida cotidiana.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es un texto muy breve.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>En general es muy extenso.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tiene como propósito dejar una enseñanza.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Su objetivo es divertir.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todas las opciones que describan el concepto de refrán."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una monografía es un escrito fantástico.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La monografía se respalda con fuentes bibliográficas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es una monografía la redacción y uso del lenguaje es irrelevante.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la monografía se desarrolla y analiza un tema en particular.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tema de una monografía ha de ser preciso y delimitado.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige si el enunciado es falso o verdadero.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Mensaje explícito",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Mensaje implícito",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aunque sea del mismo barro, no es lo mismo bacín que jarro.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Bonito es ver llover y no mojarse.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Rápido que lleva bala.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En esta vida todo se paga.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las apariencias engañan.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con la vara que midas, serás medido.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "De acuerdo al tipo de mensaje, selecciona la respuesta correcta.",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Son palabras invariables que pueden modificar tanto a los verbos como a los adjetivos y no tienen género ni número.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Son las palabras que nos indican a quién le pertenece un objeto, idea o característica.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son las palabras que expresan cantidad y números.</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Son las palabras que sirven para indicar si algo está lejos o cerca.</p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Adjetivos posesivos</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Adjetivos demostrativos</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Adjetivos numerales</p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Adverbios</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona  los conceptos según corresponda:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Fábulas</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Instructivos</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Diccionarios</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Historietas</p>",
                "t17correcta": "3"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Los modos imperativo e infinitivo se aplican para redactar:</p>",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Copulativas</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Disyuntivas</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Copulativas</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Historietas</p>",
                "t17correcta": "3"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Las conjunciones <i>o</i> y <i>u</i> que indican opción o elección son:</p>",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Las conjunciones <i>y</i>, <i>e</i> y <i>ni</i> que indican unión, está última de dos términos negativos son:</p>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Las conjunciones como <i>porque</i> y <i>ya que</i> que señalan el motivo de lo sucedido son:</p>",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Las conjunciones <i>siempre que</i> y <i>si</i> son.</p>",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>,<\/p>",
                "t17correcta": "0"
            },
           {
                "t13respuesta": "<p>,<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>,<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>,<\/p>",
                "t17correcta": "3"
            },
             
            {
                "t13respuesta": "<p>;<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>,<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>;<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>,<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Ricardo gustaba de escribir cuentos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> chistes <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> fábulas <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> su biografía y hasta relatos fantásticos para su hermana <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> leer biografías <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> monografías con temas sociales <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> y sus preferidas... Novelas de ciencia ficción <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> y de hornear lasaña. Es decir <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> no era un chico común. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Coloca “coma” o “punto y coma” en el párrafo, según corresponda."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>El mapa conceptual se construye sólo con imágenes</p>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Se utilizan figuras geométricas para encerrar cada idea.</p>",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Un mapa conceptual es realmente extenso.</p>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Utilizan palabras que funcionan como enlaces.</p>",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Identifica si los enunciados son falsos o verdaderos.",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Este<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Esos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Esa<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Aquello<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Rojo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ansioso<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Corto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pequeñito<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Primero<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Cuádruple<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Diez<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Último<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mío<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Sus<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Nuestros<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Suyos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Nunca<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Poco<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Sobre<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Siempre<\/p>",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Coloca cada palabra en el cuadro que corresponda.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Adjetivos demostrativos",
            "Adjetivos calificativos",
            "Adjetivos numerales",
            "Adjetivos posesivos",
            "Adverbios"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Escribe con letra de molde<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Separa y reorganiza el material<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Decora tu dibujo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Colocar los pies sobre cada cuadro<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Escribe con letra de molde<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Separa y reorganiza el material.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Decora tu dibujo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Refrigerar por 3 horas.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las indicaciones que están en modo imperativo."
        }
    }
];