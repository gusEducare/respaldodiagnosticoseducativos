json=[
    {  
      "respuestas":[  
         {  
            "t13respuesta":"X<sup>2</sup>+2x+x",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"2X<sup>2</sup>+3x+x",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"3X<sup>2</sup>+4",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "dosColumnas": true,
         "t11pregunta":"<p style='margin-bottom: 5px;'>¿Cuál de las siguientes expresiones te permite calcular el área del triángulo?<div  style='text-align:center;'><img style='height: 360px' src='mi9e_b01_n03_01.png'></div></p>"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EVerdadero\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFalso\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"3",
         "t11pregunta":"Un block de dibujo tiene una área de 90 cm<sup>2</sup>. Si su forma es de un cuadrado la ecuación que sirve para calcular la medida de sus lados es L<sup>2</sup>-90=0.<img src='mi9e_b01_n03_02.png'>"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EVerdadero\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFalso\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"3",
         "t11pregunta":"En la imagen siguiente imagen AC= 5, CD =10 y CB=16/3 ¿Cuál es el valor de DE es 8/5.<br><img src=\"mi9e_b01_n03_02.png\">"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"48cm",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"12cm",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"15cm",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"-El triángulo XYZ es semejante al triángulo TSU. Si los lados de los triángulos TSU miden 12 cm, 16 cm y 20 cm, y el lado correspondiente al de 12 cm en XYZ mide 3 cm, ¿cuánto mide el perímetro del triángulo XYZ?"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"10<sup>7</sup>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"1/10<sup>7</sup>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"7",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El átomo de hidrógeno emite  espontáneamente energía cada 107 años ¿Cuál es la probabilidad de que un átomo emita en   2x1014 años"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"de que salga un número par múltiplo de tres.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"de que salga un número primo que sea impar.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"sacar un número par en ambos dados.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Elige el enunciado que complete el siguiente problema para que se trate de un evento mutuamente excluyente e independiente: Roció lanza dos dados, calcula la probabilidad:"
      }
   },
   {
      "respuestas": [
          {
              "t13respuesta": "mi9e_b01_n03_03_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "mi9e_b01_n03_03_03.png",
              "t17correcta": "1,2,3",
              "columna":"0"
          },
          {
              "t13respuesta": "mi9e_b01_n03_03_04.png",
              "t17correcta": "2,3,1",
              "columna":"1"
          },
          {
              "t13respuesta": "mi9e_b01_n03_03_05.png",
              "t17correcta": "3,1,2",
              "columna":"1"
          },
          {
              "t13respuesta": "mi9e_b01_n03_03_06.png",
              "t17correcta": "4,5,6",
              "columna":"0"
          },
          {
              "t13respuesta": "mi9e_b01_n03_03_07.png",
              "t17correcta": "5,6,4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"mi9e_b01_n03_03_01.png",
          "respuestaImagen":true, 
          "bloques":false,
          "tamanyoReal":true,
          "opcionesTexto":true
          
          
      },
      "contenedores": [
          {"Contenedor": ["", "84,529", "cuadrado", "70, 43", ".","transparent"]},
          {"Contenedor": ["", "128,529", "cuadrado", "70, 43", ".","transparent"]},
          {"Contenedor": ["", "172,529", "cuadrado", "70, 43", ".","transparent"]},
          {"Contenedor": ["", "216,529", "cuadrado", "70, 43", ".","transparent"]},
          {"Contenedor": ["", "261,529", "cuadrado", "70, 43", ".","transparent"]},
          {"Contenedor": ["", "305,529", "cuadrado", "70, 43", ".","transparent"]}
      ]
  },
  {
        "respuestas": [
            {
                "t13respuesta": "<p>d=vt<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>v<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>recta<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>E=(mv<sup>2</sup>)/2<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>a<sub>c</sub>=v<sup>2</sup>/r<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>curva<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>v<sup>2</sup><\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>t<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>La ecuación &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;con variación en la velocidad&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> &nbsp;se puede representar gráficamente con una <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra del contenedor, las opciones que completen la afirmación que a continuación se presenta."
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EGrafica de barras\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EGráfica circular\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EHistograma\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"En el INEGI realizan un estudio sobre el porcentaje de la población de jóvenes entre 15 y 17 años que no asiste a la escuela, y cómo cambio dicho porcentaje del año 2006 al 2011. Si se desea realizar una gráfica comparativa de cómo cambió ese porcentaje en ese periodo, que además muestre el cambio que hubo de dicho porcentaje dependiendo el sexo, ¿qué tipo de gráficca es mejor para representar dicha comparación?"
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EObtener un número par.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EObtener un número non, menor que cinco.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EObtener un par mayor que seis.\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Al jugar con un dado, un ejemplo de un evento complementario mutuamente excluyente es:"
      }
   }
]