json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Aceptación de hechos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Alimentación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Descanso<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Moralidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sexo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respiración<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "De acuerdo a lo propuesto en la pirámide de Maslow selecciona la información relacionada con <i>Fisiología</i>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>De empleo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Amor<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Seguridad física<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Descanso<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Resolución de problemas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Propiedad privada<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>De salud<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "De acuerdo a lo propuesto en la pirámide de Maslow selecciona la información relacionada con <i>Seguridad</i>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Alimentación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Afecto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sexo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Amor<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respeto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Amistad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pertenencia<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "De acuerdo a lo propuesto en la pirámide de Maslow selecciona la información relacionada con <i>Afiliación</i>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Moralidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Amor<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Confianza<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Amor<\/p>",
                "t17correcta": ""
            },
            {
                "t13respuesta": "<p>Respeto<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Amistad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Éxito<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "De acuerdo a lo propuesto en la pirámide de Maslow selecciona la información relacionada con <i>Reconocimiento</i>"
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Éxito<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sexo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Moralidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Amor<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Resolución de problemas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Aceptación de hechos<\/p>",
                "t17correcta": "1"
            },
            
            {
                "t13respuesta": "<p>Amistad<\/p>",
                "t17correcta": "0"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "De acuerdo a lo propuesto en la pirámide de Maslow selecciona la información relacionada con <i>Autorealización</i>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tomar un baño una vez al día.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elegir sin presión el momento de iniciar mi sexualidad.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elección de mi carrera profesional.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Usar o no, pañuelos desechables para limpiar mi nariz.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elegir de entre los asientos disponibles en una sala de cine.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Respetar los bienes ajenos.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Levantar de la vía publica las heces de mi mascota.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No dejar mi basura en la vía pública.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respetar los espacios de estacionamiento de personas en situación de discapacidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Estornudar en mi codo en un centro comercial.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respetar la fila en las cajas del supermercado.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra alcontenedor que corresponda lo relacionado acada dimensión.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Dimensión personal",
            "Dimensión social"
        ],
        "css": {
            "altoContenedorRespuesta": "100px",
            "maxAltoContenedorRespuesta": "100px"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "El VIH se trasmite por el contacto casual o cotidiano como los abrazos, las caricias, los besos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El VIH se trasmite por contacto sexual no protegido (pene-ano, pene-vagina pene-boca) con una persona con VIH.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El VIH se trasmite por compartir utensilios para comer o de baño.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El VIH se trasmite Por transfusiones de sangre o sus derivados (plasma, plaquetas) que tienen virus.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El VIH se transmite a través de contacto con animales, y por picaduras de insectos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El VIH se trasmite por compartir agujas/jeringas en personas usuarias de drogas inyectables.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El VIH se trasmitedurante el parto, cuando el bebé pasa a través del canal vaginal.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Marca todas las opciones donde se expliquen las formas de contagio del VIH."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>SEP<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>IMSS<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>ISSSTE<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>FAMILIA<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>CONASIDA<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>SEDUVI<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p>Institución pública que regula la aplicabilidad de la Ley general de Educación. <\/p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p><br>Institución pública que brinda seguridad socialy atención médica a los empleados de empresas privadas. <\/p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p><br>Institución pública que brinda seguridad social y atención médica a los empleados federales. <\/p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p><br>Institución base de la sociedad miembros con relación de parentesco. <\/p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p><br>Institución pública que corrdina esfuerzos de sectores público, social y privado para promover y apoyar las acciones de prevención y control del virus de la inmunodeficiencia humana, y del síndrome de la inmunodeficiencia humana. <\/p>"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "<p><br>Institución pública cuya finalidad es formular y conducir las políticas de asentamientos humanos, urbanismo y vivienda en el estado de México. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona ambas columnas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Expulsión del colegio",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Embarazo temprano",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Rezago académico",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Riesgo de contagio de virus del papiloma humano",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Riesgo de contagio de sífilis y otras enfermedades de trasmisión sexual",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Rechazo familiar",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Maternidad y paternidad temprana",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Marginación social",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Matrimonio forzado",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Abortos",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 2,
            "t11pregunta": "Señala todas las consecuencias negativas de ejercer tu sexualidad antes de contar con información verídica sobre sexo seguro."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Remedios caseros",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Píldora del día siguiente",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Infusiones abortivas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Los consejos de amistades",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lavado vaginal",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 2,
            "t11pregunta": "Qué opción tienes para evitar un embarazo no deseado luego de tener relaciones sexuales sin protección."
        }
    },
{
        "respuestas": [
            {
                "t13respuesta": "<p>responsable<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>prevencion<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>violencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>adolescencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>noviazgo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>autoestima<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>libertad<\/p>",
                "t17correcta": "1"
            }

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Que es consciente de sus obligaciones y actúa conforme a ellas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Medida o disposición que se toma de manera anticipada para evitar que suceda una cosa considerada negativa."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Uso de la fuerza para conseguir un fin, especialmente para dominar a alguien o imponer algo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Período de la vida de la persona comprendido entre la aparición de la pubertad, que marca el final de la infancia, y el inicio de la edad adulta, momento en que se ha completado el desarrollo del organismo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Relación que existe entre dos personas que se van a casar."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Aprecio o consideración que uno tiene de sí mismo."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": " Facultad y derecho de las personas para elegir de manera responsable su propia forma de actuar dentro de una sociedad."
            }
        ], "pocisiones": [
            {
                "direccion": 1,
                "datoX": 9,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 7,
                "datoY": 2
            },
            {
                "direccion": 0,
                "datoX": 1,
                "datoY": 8
            },
            {
                "direccion": 0,
                "datoX": 2,
                "datoY": 11
            },
            {
                "direccion": 1,
                "datoX": 16,
                "datoY": 2
            },
            {
                "direccion": 0,
                "datoX": 13,
                "datoY": 9
            },
            {
                "direccion": 1,
                "datoX": 22,
                "datoY": 3
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 25,
            "ancho": 25,
            "t11pregunta": "Completa el crucigrama."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Pastillas anticonceptivas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Práctica de sexo seguro, es decir, sin penetración (besos, caricias, abrazos autoerotismo o masturbación).",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Uso de dispositivo intrauterino",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Aborto",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Pastilla del día siguiente",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Uso de Condón masculino o femenino",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Abstinencia",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 2,
            "t11pregunta": "Cuáles de los siguientes son métodos para evitar enfermedades de trasmisión sexual."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Socialismo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Capitalismo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Liberalismo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Positivismo",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas": 1,
            "t11pregunta": "Elige la palabra que define el párrafo.<br>Brindar a la sociedad cierta estabilidad en la producción y acceso a bienes materiales específicos y requeridos para la vida de los seres humanos; sin embargo, este efecto como todos en su desmesura produce desigualdades y diferencias entre clases sociales."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Igualdad de trato en apego a los derechos de cada individuo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Consideración invididual no importando sexo, raza, religión, edad o condición social.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Aportación y participación no solo en recursos sino a través de un involucramiento pleno.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Elección y decisión de cada persona. En tanto no afecte derechos de otros.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Trato igualitario sin distinción alguna, recibiendo los beneficios y reconocimiento de los demas.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Integración social de acuerdo a los principios de igualdad y justicia.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Apoyo desisteresado a personas en situación de desventaja.",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Justicia"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Igualdad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cooperación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Libertad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Equidad"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Inclusión"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Solidaridad"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las columnas."
        }
    }
]