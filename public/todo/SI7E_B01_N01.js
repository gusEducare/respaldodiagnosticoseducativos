json=[
    {
        "respuestas": [
            {
                "t13respuesta": "¿qué?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¡que!",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "¿quién o quiénes?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿cuándo?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿cómo?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿dónde?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿por qué?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "porqué",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "donde",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 3,
            "t11pregunta": "Selecciona todas las preguntas guía que te permitirán recabar información respecto a un tema específico y luego generar nuevas preguntas"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "si7e_b01_n01_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "si7e_b01_n01_03.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "si7e_b01_n01_04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "si7e_b01_n01_05.png",
                "t17correcta": "3",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra y ordena para identificar la estructura del texto expositivo<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "si7e_b01_n01_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "tamanyoReal":true,
            "borde":false

        },
        "contenedores": [
            {"Contenedor": ["", "141,454", "cuadrado", "156, 35", ".","transparent"]},
            {"Contenedor": ["", "182,454", "cuadrado", "156, 35", ".","transparent"]},
            {"Contenedor": ["", "277,454", "cuadrado", "156, 35", ".","transparent"]},
            {"Contenedor": ["", "398,454", "cuadrado", "156, 35", ".","transparent"]}
        ],
        "css":{
            "displayImgLineas":"none"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"si7e_b01_n01_02_01.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"si7e_b01_n01_02_02.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"si7e_b01_n01_02_03.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "columnas":3,
            "t11pregunta": "¿Cuál de la imágenes corresponde a un mapa conceptual? "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Padres padrísimos, S.A. de Jaime Alfonso Sandoval, Editorial progreso S.A. de C.V. en México, 2005",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Jaime Alfonso Sandoval, Editorial Progreso, S.A. de C.V. Libro: Padres Padrísimos, S.A. México 2005.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Sandoval, J. A. (2005). <i>Padres Padrísimos,</i> S.A. México: Editorial Progreso, S.A. de C.V.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "2005 México, Padres Padrísimos, S.A.,  Editorial Progreso, S.A. de C.V., Sandoval J.A.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la forma correcta de citar un libro de acuerdo a APA:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El origen de los mitos fue para preservar hechos históricos",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En los mitos el espacio y tiempo del relato no está en nuestra historia.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las leyendas jamás tienen carácter didáctico y de moraleja.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Leyendas y mitos se divulgaron de forma oral de generación en generación",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los mitos hacen relatos de personas reales aunque llevadas a la ficción",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige Falso o Verdadero<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":true,
            "evidencio": false
        }
    }  
]
