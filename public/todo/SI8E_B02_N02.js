json = [
	{
		"respuestas": [
			{
				"t13respuesta": "<p>1<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>2<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>3<\/p>",
				"t17correcta": "3"
			},
			{
				"t13respuesta": "<p>4<\/p>",
				"t17correcta": "4"
			},
			{
				"t13respuesta": "<p>5<\/p>",
				"t17correcta": "5"
			},
			{
				"t13respuesta": "<p>6<\/p>",
				"t17correcta": "6"
			},
			{
				"t13respuesta": "<p>7<\/p>",
				"t17correcta": "7"
			},
			{
				"t13respuesta": "<p>8<\/p>",
				"t17correcta": "8"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Elige con cuál opinión o punto de vista estás de acuerdo y será el que defenderás con tu postura.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Realiza un guion con introducción, desarrollo (argumentos y posibles contraargumentos) y conclusiones.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Desarrollo. Presenta tus argumentos sobre por qué tu postura respecto al tema.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Ya en la mesa realiza una introducción: describe a la audiencia lo que has investigado acerca del tema y resalta cuál es tu preocupación o punto de vista.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Investiga seria y profundamente del tema que se discutirá en la mesa redonda. Recuerda que es mejor si conoces los diversos puntos de vista que existen sobre el tema.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Conclusión: finaliza con un comentario personal planteando una posible solución o tus razones de por qué los demás deberían apoyarte.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Argumenta. Menciona los datos o cifras que hayas investigado que apoyen tu posición frente al tema.<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Contraargumentos. Comenta sobre la opinión contraria a la tuya o el contraargumento que podrían utilizar en tu contra.<br/><\/p>"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Completa el siguiente p&aacute;rrafo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Opinión<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Hecho real<\/p>",
				"t17correcta": "2"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;El problema es de muchas personas que no se meten en la cabeza que las drogas en principio son controladas por ellos, pero llega un momento en el que la adicción va venciendo a la persona, y en caso de drogas muy adictivas como cocaína, heroína es necesaria una desintoxicación en centros de rehabilitación.Y es que las drogas comienza en la gente despacio, en ocasiones los cigarros… o el cannabis, otras veces es el alcohol el primer contacto a muy corta edad, y desde luego unas pocas dosis no suelen ser graves, pero el aumento de la espiral va metiendo al individuo en más drogas y provocando que cada vez las consuma en mayores cantidades De: http://www.dedrogas.com/2005/12/01/opinion-sobre-las-drogas/<br/><\/p>"
			},
			{
				"c03id_tipo_pregunta": "8",
				"t11pregunta": "<p>&nbsp;Viena, 26 de junio 2015 - La prevalencia del consumo de drogas sigue siendo estable en todo el mundo, según el Informe Mundial sobre las Drogas 2015 de la Oficina de las Naciones Unidas contra la Droga y el Delito -UNODC-. Se estima que un total de 246 millones de personas - un poco más de 5 por ciento de los mayores de 15 a 64 años en todo el mundo - consumieron una droga ilícita en 2013. Unos 27 millones de personas son consumidores problemáticos de drogas, casi la mitad de los cuales son personas que se inyectan drogas (   PWID- People Who Inject Drugs ). Se estima que 1,65 millones de personas que se inyectan drogas vivían con el VIH en 2013. Los hombres son tres veces más propensos que las mujeres a consumir  cannabis, cocaína y anfetaminas, mientras que las mujeres son más propensas a abusar de los opioides con prescripción médica y de los tranquilizantes.  Fragmento. De: https://www.unodc.org/mexicoandcentralamerica/es/webstories/2015/informe-mundial-sobre-las-drogas-2015.html<br/><\/p>"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "8",
			"t11pregunta": "Completa el siguiente p&aacute;rrafo."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Picarones<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Batatas<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Chancaca<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Donas o rosquillas<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Camote<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Piloncillo<\/p>",
				"t17correcta": "1"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Zeta sesion 9 a1",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Variantes lexicas",
			"Significado probable de acuerdo al contexto."
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "\u003Cp\u003EUn campo semántico\u003C\/p\u003E",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "\u003Cp\u003EPalabras relacionadas\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003ERaíces etimológicas\u003C\/p\u003E",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "De que manera puedes conceptualizar lo referido en el texto."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "\u003Cp\u003ENarrador y  Ángel\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003E…el hombre comenzó a estrangularme de modo decisivo…\u003C\/p\u003E",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "\u003Cp\u003EÁngel: fuerte, maduro y repulsivo, con bata de boxeador. La descripción del narrador no es clara\u003C\/p\u003E",
				"t17correcta": "2"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Ingresos",
			"tipo": "vertical"
		},
		"contenedores": [
			"Personajes",
			"Nudo",
			"Descripción de personajes"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "Verdadero",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "Falso",
				"t17correcta": "1"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Las tablas comparativas son organizadores gráficos que permiten contrastar información encontrando semejanzas y diferencias respecto a un tema en común.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Las tablas comparativas únicamente se usan para anotar información proveniente de datos estadísticos.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Además de permitir que se contraste información, las tablas comparativas también son útiles para sintetizar información compleja.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "La información que contienen las tablas comparativas es únicamente cualitativa.",
				"correcta": "1"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "Las tablas comparativas siempre presentar su información en dos ejes lo que permite comparar claramente los datos registrados.",
				"correcta": "0"
			},
			{
				"c03id_tipo_pregunta": "13",
				"t11pregunta": "La información incluida en las tablas comparativas puede ser cuantitativa o cualitativa.",
				"correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "13",
			"t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
			"descripcion": "",
			"variante": "editable",
			"anchoColumnaPreguntas": 60,
			"evaluable": true
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>roles<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>orden y duración<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>expositores<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>audiencia<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>moderador<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>tema<\/p>",
				"t17correcta": "1"
			}
		],
		"preguntas": [
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Se asignan con anticipación y usualmente son tres: moderador, expositores y la audiencia"
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Ambos aspectos son relevantes para las intervenciones en una mesa redonda, se establecen de manera anticipada y se informan a la audiencia al iniciar la misma"
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Presentan información del tema a tratar, exponer el sutema asignado y argumentan su punto de vista, ademas emplear lenguaje formal y respaldar sus ideas con fundamentos objetivos"
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Asistentes a la mesa, al concluir la misma pueden formular preguntas y hacer comentarios"
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Coordina las interveciones y tiempo de los participantes. Brinda voz al público al finaliza la mesa redonda y ofrece las conclusiones del tema abordado."
			},
			{
				"c03id_tipo_pregunta": "15",
				"t11pregunta": "Se plantea con antelación y informa a los expositores para su preparación. Debe ser de ínteres general y se argumentará por cada expositor."
			}
		],
		"pocisiones": [
			{
				"direccion": 0,
				"datoX": 3,
				"datoY": 1
			},
			{
				"direccion": 0,
				"datoX": 8,
				"datoY": 5
			},
			{
				"direccion": 1,
				"datoX": 6,
				"datoY": 3
			},
			{
				"direccion": 1,
				"datoX": 2,
				"datoY": 3
			},
			{
				"direccion": 1,
				"datoX": 10,
				"datoY": 1
			},
			{
				"direccion": 1,
				"datoX": 3,
				"datoY": 1
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "15",
			"t11pregunta": "Delta sesion 12 a3"
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Narración breve<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Narración extensas con un gran número de personajes<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Su origen puede ser  literario o popular<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Desarrollan una trama<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Tienen en común ser relatos fantásticos ubicados en lugar y momentos distintos a la realidad.<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Los hechos que narra pueden ser reales o imaginarios.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Usan la prosa.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Sus temas son variados, tantos como la creatividad permita.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>En su escritura pueden incluir diálogos o solo narración.<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Usan el verso<\/p>",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "2",
			"t11pregunta": "Elige todas las que sean características de un cuento."
		}
	},
	{
		"respuestas": [
			{
				"t13respuesta": "<p>Jurisprudencia<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Articulos<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Ley<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Justicia<\/p>",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "<p>Interfase<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Página web<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Maleware<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Skype<\/p>",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "<p>Dependencia<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Enfermedad<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Sustancias<\/p>",
				"t17correcta": "2"
			},
			{
				"t13respuesta": "<p>Crónico<\/p>",
				"t17correcta": "2"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "5",
			"t11pregunta": "Zeta sesion 9 a1",
			"tipo": "horizontal"
		},
		"contenedores": [
			"Derechos",
			"Tecnologia",
			"Adicciones"
		]
	},
	{
		"respuestas": [
			{
				"t13respuesta": "\u003Cp\u003ENarrador en primera persona o protagonista\u003C\/p\u003E",
				"t17correcta": "0"
			},
			{
				"t13respuesta": "\u003Cp\u003ENarrador en segunda persona o testigo\u003C\/p\u003E",
				"t17correcta": "1"
			},
			{
				"t13respuesta": "\u003Cp\u003ENarrador en tercera persona u omnisciente\u003C\/p\u003E",
				"t17correcta": "0"
			}
		],
		"pregunta": {
			"c03id_tipo_pregunta": "1",
			"t11pregunta": "Elige la opción que nombra al Personaje de la historia que presencia los hechos y narra la historia de personaje principal. Se expresa en primera o tercera persona."
		}
	}
]