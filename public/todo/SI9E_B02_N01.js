json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Argumentar<\/p>",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "<p>idea<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>convencer<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>opinión<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>datos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>comprobables<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>discutir<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>tesis<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br/><br/><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;es defender una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;o punto de vista. Es la forma en la que podemos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;a otra persona de aceptar nuestra <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;como verdadera. La argumentación no solo se basa en opiniones sino también en <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y hechos <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que respalden una idea en particular, de esta manera se convence más fácilmente a una audiencia o lector ya que el argumento no se verá como una opinión sin fundamento o meramente caprichosa.<br/>Es importante tener claro que argumentar no significa <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, se trata de presentar de manera respetuosa y fundamentada hechos, ejemplos, citas y datos  que defiendan la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;que  se propone.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>dedicatoria<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>advertencia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>introducción<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>presentación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>prólogo<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es un texto muy breve en el autor escribe a quién le dedica u ofrecer su obra. Puede ser un familiar, un amigo o inclusive a un motivo. Puede incluir el por qué se lo dedica."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es una información que el autor presenta al lector para que se tome en cuenta antes de leer el material y asi pueda comprenderlo mejor. Puede estar relacionado con la temática."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es una explicación breve, escrita por el autor u otra persona, acerca de lo que tratará la obra. Su propósito es el de captar la atención del lector."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es un escrito del autor en el que se encuentra información pertinente sobre la obra: sus motivos y sucesos que le dieron origen entre apreaciones que el autor o presentador considere pertinente."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es el texto que antecede directamente a la lectura y pretende guiar al lector acerca de lo que encontrará a continuación. También puede reflejar alguna reflexión."
            }
        ],
        "pocisiones": [
            {
                "direccion" : 1,
                "datoX" : 7,
                "datoY" : 1
            },
            {
                "direccion" : 1,
                "datoX" : 2,
                "datoY" : 2
            },
            {
                "direccion" : 1,
                "datoX" : 12,
                "datoY" : 6
            },
            {
                "direccion" : 0,
                "datoX" : 7,
                "datoY" : 1
            },
            {
                "direccion" : 0,
                "datoX" : 11,
                "datoY" : 9
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3",
            "alto" : 13
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>prólogos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>lector<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>indirecta<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>primera<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;son el primer texto que consigue el lector en un libro antes de iniciar una lectura, y es este encuentro el que suele animar al receptor a leer el material por algún motivo u otro, a aplicar énfasis en una temática de fondo, o leer una reflexión temprana.<br/>Sin embargo, a pesar de que todos los autores del prólogo escriben para un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, no siempre se dirigen directamente al mismo. Existen dos maneras de dirigirse a los lectores en los prólogos: de manera directa e indirecta.<br/><b>La forma </b><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>: se refiere directamente a la lectura o a su propósito.<br/><b>La forma directa </b>: se refiere al lector en <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;persona, puede tutearlo o nombrarlo directamente como “querido lector”, “estimado lector” u otro.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Identidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Formulario<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Impreso<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Requerimiento<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Asociación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Documento<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Requisito<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Planilla<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Digital<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Formato<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Modo indicativo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Modo imperativo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Modo infinitivo",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Modo subjuntivo",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Se refiere a sucesos reales y es el que más usamos con tiempos: presente, pretérito y futuro."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Utilizado cuando se quieres dar una orden, instrucción o alguna voluntad."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Indica acciones concretas y el verbo es impersonal."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Se utiliza en formularios para indicar posibilidad, una acción hipotética o un deseo. Es una acción que se desea sea realizada en el futuro."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    }
]
