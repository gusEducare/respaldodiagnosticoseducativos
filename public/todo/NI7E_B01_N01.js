json = [
   {  
     "respuestas":[  
        {  
           "t13respuesta":"<p>Con el metabolismo y el proceso por el cuál obtenemos nutrientes.</p>",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"<p>Con estudios genéticos.</p>",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"<p>Con la simple observación.</p>",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"<p>Con una radiografía.</p>",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"1",
        "t11pregunta":"¿Qué característica que nos permite identificar que tenemos similitudes con las formas de vida existentes en el planeta?"
     }
  },
      {
       "respuestas": [
           {
               "t13respuesta": "<p>carbono<\/p>",
               "t17correcta": "1,2,3,4"
           },
           {
               "t13respuesta": "<p>nitrógeno<\/p>",
               "t17correcta": "1,2,3,4"
           },
           {
               "t13respuesta": "<p>fósforo<\/p>",
               "t17correcta": "1,2,3,4"
           },
           {
               "t13respuesta": "<p>azufre<\/p>",
               "t17correcta": "1,2,3,4"
           },
           {
               "t13respuesta": "<p>aire<\/p>",
               "t17correcta": "5"
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p>El flujo de materia en los ecosistemas, consiste en el movimiento de elementos químicos como <\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p>, <\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p>, <\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p>, <\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p>&nbspentre los elementos físicos de los ecosistemas (suelo, agua y <\/p>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<p>) y sus elementos biológicos (flora y fauna), para quienes tales elementos químicos son nutrientes esenciales para su desarrollo.<\/p>"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "respuestasLargas":true,
           "anchoRespuestas":140,
           "t11pregunta": "Completa el siguiente p&aacute;rrafo."
       }
   },
   {
       "respuestas": [
           {
               "t13respuesta": "<p>Se refiere a los distintos tipos de seres vivos –incluido el ser humano- que habitan en un lugar particular.</p>",
               "t17correcta": "1",
               "numeroPregunta":"0"
           },
           {
               "t13respuesta": "<p>Ciclo de los biogeoquímicos, consumidores y productores donde forman parte del componente.</p>",
               "t17correcta": "0",
               "numeroPregunta":"0"
           },
           {
               "t13respuesta": "<p>Nombre que recibe el ciclo que mantiene constante la cantidad del agua del planeta.</p>",
               "t17correcta": "0",
               "numeroPregunta":"0"
           },
           {
               "t13respuesta": "<p>Proceso biológico que provoca que el carbono regrese a la atmósfera.</p>",
               "t17correcta": "0",
               "numeroPregunta":"0"
           },
           {
               "t13respuesta": "<p>Se encuentra en todos los países del mundo.</p>",
               "t17correcta": "0",
               "numeroPregunta":"1"
           },
           {
               "t13respuesta": "<p>El 70% se encuentra en América.</p>",
               "t17correcta": "0",
               "numeroPregunta":"1"
           },
           {
               "t13respuesta": "<p>17 países albergan el 70% del porcentaje mundial.</p>",
               "t17correcta": "1",
               "numeroPregunta":"1"
           },
           {
               "t13respuesta": "<p>México tiene una biodiversidad muy pobre por la contaminación.</p>",
               "t17correcta": "0",
               "numeroPregunta":"1"
           },
                       {
               "t13respuesta": "<p>Mamíferos</p>",
               "t17correcta": "0",
               "numeroPregunta":"2"
           },
           {
               "t13respuesta": "<p>Reptiles</p>",
               "t17correcta": "1",
               "numeroPregunta":"2"
           },
           {
               "t13respuesta": "<p>Anfibios</p>",
               "t17correcta": "0",
               "numeroPregunta":"2"
           },
           {
               "t13respuesta": "<p>Plantas</p>",
               "t17correcta": "0",
               "numeroPregunta":"2"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "2",
           "t11pregunta": ["La biodiversidad...","La biodiversidad mundial:","México tienen el primer lugar en biodiversidad de:"],
           "preguntasMultiples":true
       }
   },
   {
       "respuestas": [
           {
               "t13respuesta": "Falso",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Verdadero",
               "t17correcta": "1"
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las medicinas actuales son el fruto de la investigación de la herbolaría.",
               "correcta": "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En la antigüedad para poder curar enfermedades se utilizaban seres vivos y plantas.",
               "correcta": "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las plantas y hongos son los elementos que utilizan para hacer todas las medicinas en la actualidad.",
               "correcta": "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Como parte de nuestra cultura, la herbolaría mexicana es una de las más reconocidas a nivel mundial.",
               "correcta": "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Al usar tantas medicinas procesadas, la herbolaría en la actualidad ya no cumple con sus funciones.",
               "correcta": "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
           "descripcion": "",
           "variante": "editable",
           "anchoColumnaPreguntas": 70,
           "evaluable": true
       }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>humanidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>biológicas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>microscopio<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>célula<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>funcional<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En la historia de la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; han ocurrido sucesos icónicos determinantes para el desarrollo de la ciencia y el bienestar humano. En la ciencias <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>,&nbsp; el acontecimiento más significativo fue la invención del <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, que abrió la puerta del conocimiento a un universo inmenso de seres vivos imperceptibles para el ojo humano y que tienen un papel crítico en la salud, en la fabricación de productos y en la dinámica del entorno.<br>Asimismo, el microscopio dio la oportunidad de comprender la naturaleza de los seres vivos, permitiendo llegar a conocer las estructuras básicas que constituyen a los seres vivos e identificar a la<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbspcomo la unidad estructural y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbspde los seres vivos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas": true,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    }
]