json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Etapa Colonial hace referencia al período más temprano de la historia antigua de México.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cultura Olmeca es considerada la madre de las culturas mesoamericanas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cultura Mexica se localizó principalmente al norte de Veracruz y no se sabe con claridad por qué desapareció. ",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la opción correcta:<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Representaban a seres humanos, animales, plantas en pinturas. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tenían un gran manejo del oro, plata y cobre.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Elaboraron hachas y leznas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Difundian noticias entre la población.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Tenían la costumbre de pintarse la cara y el cuerpo.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona tres aportaciones hechas por parte de la cultura Purépecha"            
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los Mixtecos fueron considerados una cultura pacífica que destacó por  tener una sociedad sin divisiones, ni clases sociales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los  Olmecas se dedicaban al cultivo de maíz, frijol y calabaza.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Quetzalcóatl fue un dios que la cultura mixteca reconoció como el salvador de su época.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la opción correcta:<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "ocultaPuntoFinal": true,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Los mexicas fueron quienes nombraron a Teotihuacán.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se sabe con certeza quiénes fueron los constructores y habitantes de Teotihuacán.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los nahuas o totonacos fueron los posibles pobladores de Teotihuacán.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La pirámide del sol y la luna fueron nombradas por los españoles.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "<p>Selecciona las oraciones con la información correcta.</p><br><br>\n\
                            <p>Dentro de las teorías sobre la Cultura Teotihuacana, se cree que:</p>",
            "ocultaPuntoFinal": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Mayas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mexicas<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Toltecas<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Totonacas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Olmecas<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<center><p><br><div></div>Arrastra la cultura que está representando la siguiente imagen.<br><img src=\".png\" alt=\"Mexicas\" style='width:200px' \/><div></div><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Arrastra la ubicación que corresponda a la imagen.",
            "ocultaPuntoFinal": true,
            "contieneDistractores": true
        }
    }    
];