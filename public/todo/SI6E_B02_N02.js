json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Recurso literario para generar emociones positivas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Recurso literario para crear tensión.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Forman parte del género literario del cuento o narraciones breves. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Basado en hechos reales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El autor  recrea un ambiente sobrenatural que genera sentimientos de misterio o terror.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Prevalecen elementos como sombras, aullidos, espacios cerrados. Se hace mucha referencia a lo oscuro. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Narración de gran extensión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se destacan temas lúgubres como la muerte. Los personajes principales siempre sufren algún temor o peligro.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Todos los relatos ocurren en lugares fantásticos.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los personajes o los hechos tienen características irracionales, inhumanas o sobrenaturales.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hay pérdida de la integridad física y psicológica.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Se presenta una polarización entre el bien y el mal. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los personajes son siempre racionales  y poco emotivos.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Arrastra al contenedor las características de los cuentos de misterio y terror. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los textos instructivos son aquellos que describen relatan leyendas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los instructivos pueden estar acompañados de imágenes para que el lector comprenda mejor las instrucciones, procedimientos y materiales a utilizar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sus procesos están enumerados o se emplean palabras que indican orden temporal.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los textos instructivos detallan hechos fantásticos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Utilizan verbos en modo imperativo e infinitivo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lenguaje técnico y especializado. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función de estos textos es describirnos los procedimientos y reglas para realizar alguna actividad de forma detallada, clara y precisa.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",  
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "¿Qué y a quién le sucede?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿Qué problema se plantea?",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "¿Qué  sucede para que la historia termine?",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Introducción"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Nudo"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Desenlace"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona ambas columnas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Narrador que presencia los hechos. Conoemos sus emociones pero no las del resto de los pesonajes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Son quienes realizan acciones y viven numerosas aventuras. Se describen sus características físicas y emocionales.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Narra en primera persona y lector sólo conoce lo que éste le cuenta. ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Lugares en que se desarrollan los hechos y en donde los personajes realizan las acciones",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Narra desde afuera del relato. No participa activamente en la historia y conoce todo lo que pasa.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Se opone al personaje principal creando dificultades e impide sus propoósitos. ",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Personaje principal  en torno al quien suceden los hechos. ",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Testigo"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Personajes"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Narrador"
            },
              {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Ambiente"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Omnisciente"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "antagonista"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Protagonista"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Selecciona las respuestas que correspondan. "
        }
    }
    ,
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los reportajes  son  textos informativos que desarrollan extensamente  un tema particular. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los reportajes abordan solamente  temas culturales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pueden hacer una recopilación de información actual e investigaciones previas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": " Su difusión es  siempre a través de revistas especializadas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En su investigación recopilan información de distintas fuentes.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta relaciona con los reportajes. <\/p>",  
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Reportaje",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Noticia",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Texto extenso.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Texto corto.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su información puede mantenerse vigente a través del tiempo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se puede basar en numerosos sucesos noticiosos que tengan alguna relación entre sí o se agrupen en un solo tema.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Eventualmente pierde vigencia.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es objetiva y no incluye opiniones.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se basa en un solo suceso.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Suele incluir opiniones de expertos y entrevistas a otras personas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable"  : true
        }        
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Sirven para comprender los miedos, pensamientos, sentimientos y motivaciones de los personajes.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las mismas determinará las acciones de los personajes en el relato.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Pueden ayudar a recrear la escena; por ejemplo si se describe un personaje como un monstruo o  espectro, entre otros.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Señala la clase de características de los personajes a las que se refiere cada enunciado.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Psicológicas",
            "Físicas"
        ]
    },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>objetivo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>irrelevante<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>búsqueda<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>comunicar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>preguntas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>fuentes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>entrevista<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>diversas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>información<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>notas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>hecho<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>difusión<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>tema<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>reportaje<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>relevante<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Localiza 15 palabras relacionadas con el concepto de reportaje. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Acto comunicativo entre una persona que cuestiona y otra u otras que responden. Su finalidad es obtener información sobre un tema en que el entrevistado es experto.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Estas son las preguntas que más nos permiten ampliar información ya que ofrecen la oportunidad de dar respuestas extensas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Limitan la extensión de la respueta, son más específicas y concretas.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Entrevista"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Preguntas abiertas"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Preguntas cerradas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona ambas columnas. "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Son una representación gráfica del pensamiento.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Presentan información de una manera ordenada, clara y precisa.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Permiten enlazar relaciones entre algunos elementos con otros.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hacer una narración extensa de un procedimiento.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ayudan a detectar posibles problemas a futuro.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ayudan a entender todo el proceso completo por el que debe pasar el trabajo o actividad que se está realizando.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Resume los pasos más irrelevantes de un proceso.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala las características de los diagramas de flujo."
        }
    }
];
