json=[
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Portada</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Índice</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Conclusiones</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Cuerpo del trabajo</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Fuentes consultadas</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Portada</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Índice</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Conclusiones</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Cuerpo del trabajo</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Fuentes consultadas</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Portada</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Índice</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta":"<p>Conclusiones</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Cuerpo del trabajo</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Fuentes consultadas</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Portada</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Índice</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta":"<p>Conclusiones</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Cuerpo del trabajo</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Fuentes consultadas</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         
         {  
            "t13respuesta":"<p>Portada</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Índice</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }, 
         {  
            "t13respuesta":"<p>Conclusiones</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Cuerpo del trabajo</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Fuentes consultadas</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Portada</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Índice</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         }, 
         {  
            "t13respuesta":"<p>Conclusiones</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Cuerpo del trabajo</p>",
            "t17correcta":"1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Fuentes consultadas</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": [
                        "Contiene  los datos de la institución; título del trabajo; nombre del investigador y el lugar y la fecha del informe.",
                        "Presenta el nombre de  los capítulos, enumeraciones y apartados de la investigación.",
                        "En ella se leen los motivos de la elección del tema; objetivos y propósitos de la investigación y la estructura del trabajo.",
                        "Comprende un listado de todas las revistas, libros, enciclopedias y fuentes digitales de las cuales se obtuvo información.",
                        "Contiene la síntesis del trabajo presentado y las recomendaciones a los lectores que quieran realizar investigaciones a futuro sobre ese tema.",
                        "El desarrollo completo de la monografía. En esta se incluyen los apartados del tema y en algunos casos elementos gráficos que acompañan al texto."],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Cumple",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "No cumple",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sí",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uso de presente atemporal",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uso del verbo ser y otros verbos copulativos para establecer comparaciones o analogías al describir",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uso de la tercera persona, el impersonal y la voz pasiva",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nexos para introducir ideas",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Organización del texto en párrafos",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Referencias bibliográficas incluidas en el cuerpo del texto y en el apartado de la bibliografía",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "¿Consideras que el fragmento es parte de una monografía?",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>¿Por qué son útiles los modelos animales en psiquiatría? Contribuciones, ventajas y limitaciones Las enfermedades psiquiátricas, y particularmente las adicciones, constituyen un problema de salud y social de primer orden. Dada la compleja naturaleza de estas enfermedades, ningún modelo animal puede representar el completo espectro fenotípico de la esquizofrenia, la depresión o la adicción, por poner algunos ejemplos. Sin embargo, pueden utilizarse componentes fenotípicos específicos para diseñar modelos animales adecuados que contribuyan a arrojar luz sobre los mecanismos neurobiológicos subyacentes a determinados signos o síntomas, lo que puede resultar muy útil para la mejora de intervenciones terapéuticas y estrategias de prevención (1-8).<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
    {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003E“¡Levanté la vista y ahí estaba! Justo a la altura de mis ojos. Era él… frente a mí… el robot que dibujé hace años, había cobrado vida y me llamaba por mi nombre”\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003E“Cada una de las máquinas utilizó 3 minutos para explicar los motivos de la rebelión”.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003E“Todo extraterrestre; sin excepción, deberá ser registrado en el censo del próximo 28 de febrero. En caso de negarse deberá ser remitido a la fortaleza espacial”.\u003C\/p\u003E",
            "t17correcta":"2"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"5",
         "t11pregunta":"Ingresos",
         "tipo":"vertical"
      },
        "contenedores":[ 
            "Narrador protagonista",
            "Narrador testigo",
            "Narrador omnisciente"
        ]
   },
   {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003ESe describen exactamente como ocurrieron. Presentan sucesos o datos objetivos y comprobables que se sustentan de manera objetiva.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EPresentan los comentarios de diversas personas que expresan su postura ante lo sucedido. Pueden aprobar o no el suceso.\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"5",
         "t11pregunta":"Arrastra al contenedor correspondiente",
         "tipo":"vertical"
      },
        "contenedores":[ 
            "Hechos",
            "Opiniones"
        ]
   }
]
