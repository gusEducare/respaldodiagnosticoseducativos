json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Estado de México, Guanajuato y Querétaro.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Hidalgo, Puebla y Michoacán.</p>",
                "t17correcta": "0"
            },
            //Agrupador 2
            {
                "t13respuesta": "<p>Chihuahua, Durango y Nueva León.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Sonora, Tamaulipas, Zacatecas.</p>",
                "t17correcta": "1"
            },
            //Agrupador 3
            {
                "t13respuesta": "<p>Arizona, Chihuahua y Utah.</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Sonora, Colorado y Nevada.</p>",
                "t17correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "¿A qué región corresponden los siguientes estados? Arrastra la respuesta correcta.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Mesoamérica",
            "Aridoamérica",
             "Oasisamérica"
        ]
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Olmecas</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Zapotecas</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Mixteca</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Purépecha</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Olmecas</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Zapotecas</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Mixteca</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Purépecha</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Olmecas</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Zapotecas</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Mixteca</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta":"<p>Purépecha</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Teotihuacana</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Tolteca</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Mexica</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta":"<p>Maya</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Olmecas</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Zapotecas</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Mixteca</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }, 
         {  
            "t13respuesta":"<p>Purépecha</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Teotihuacana</p>",
            "t17correcta":"1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Tolteca</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Mexica</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         }, 
         {  
            "t13respuesta":"<p>Maya</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Cultura que poseía una escritura jeroglífica que se localizó principalmente en Oaxaca.",
                         "Esta cultura es considerada la madre de las culturas mesoamericanas utilizando materiales de diferentes lugares como Guerrero o Puebla, pero se localizaron principalmente en Veracruz y Tabasco.",
                         "Se establecieron desde las montañas de Oaxaca hasta Guerrero y Puebla, fundando ciudades como Mitla y Teozacoalco. Se agruparon en señoríos y eran principalmente guerreros.",
                         "Fue una de las culturas más importantes, estableciendo en gran cantidad de estados en México y Centroamérica. Los avances que presentaron en astronomía, matemáticas y arquitectura, posicionan a dicha cultura como una de las más prolíficas.",
                         "De esta cultura no se conocen ni su origen o la procedencia de su lengua. Su organización política se basaba en un rey, como máxima autoridad. Utilizaron metales con técnicas muy avanzadas para la época.",
                         "Esta cultura se localizó principalmente en la zona del Valle de México. Los sacerdotes eran quienes gobernaban. Presentaron grandes avances en arquitectura, astronomía, matemáticas, etc. No se sabe el motivo de su colapso."],
         "preguntasMultiples": true,
         "pintaUltimaCaja":false,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunas de las aportaciones de las culturas mesoamericanas pueden observarse en calendarios, medios de transporte, numeración arábiga, etc.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gracias a las ruinas y vestigios que se tienen de las culturas mesoamericanas podemos observar que poseían grandes conocimientos sobre elementos astronómicos y matemáticos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cultura maya utilizó cuentas que combinaban números con glifos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los sistemas de numeración mexica, azteca y maya denotan cantidades por medio de símbolos pictográficos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Elementos que representan la sustentabilidad con el medio que los rodeaba, es una de las grandes contribuciones de las culturas mesoamericanas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta:<\/p>",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {
        "respuestas": [
            {
                "t13respuesta": "El Popol Vuh es el libro donde dicha cultura habla de los orígenes divinos y humanos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Existió un dios que quiso vivir como los hombres, les dio el maíz y su cultivo: Quetzalcóatl. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Ometéotl es el único Dios, fuerza femenina y masculina del universo.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mayas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Teotihuacanos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Toltecas"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "ocultaPuntoFinal": true,
            "pintaUltimaCaja":false,
            "t11pregunta": " Relaciona las culturas con su creencias acerca de la creación del hombre."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Desarrollo de sistema numérico con el uso del cero. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Calendario organizado en 18 meses de 20 días. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Monte Albán como ejemplo del desarrollo de esta cultura. ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Producción y comercio de cacao, algodón y obsidiana.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Desarrollo de un calendario de 360 días.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Mayas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Aztecas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Zapotecas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Teotihuacanos"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Olmecas"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "ocultaPuntoFinal": true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Arrastra la aportación a la cultura que corresponde."
        }
    }
];