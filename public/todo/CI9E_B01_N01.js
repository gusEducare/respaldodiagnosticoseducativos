json = [
    {
        "respuestas": [
            {
                "t13respuesta": "CI9E_B01_N03_02.png",
                "t17correcta": "1",
                "columna": "1"
            },
            {
                "t13respuesta": "CI9E_B01_N03_03.png",
                "t17correcta": "0",
                "columna": "1"
            },
            {
                "t13respuesta": "CI9E_B01_N03_04.png",
                "t17correcta": "2",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra  y ordena las necesidades del ser humano de acuerdo a la pirámide de Maslow.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "CI9E_B01_N03_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            {"Contenedor": ["", "234,344", "cuadrado", "190, 48", ".", "transparent"]},
            {"Contenedor": ["", "302,380", "cuadrado", "190, 48", ".", "transparent"]},
            {"Contenedor": ["", "372,421", "cuadrado", "192, 48", ".", "transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Carácter privado<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Elección y ejercicio de mi sexualidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Eventualmente mis decisiones en esta área afectan la otra dimensión.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Carácter publico<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Convivencia adecuada<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Respeto y tolerancia a las diferencias<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor adecuado las siguientes oraciones.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Dimensión personal",
            "Dimensión social"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Amigos que ya ejercen son sexualmente activos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Profesionales de la salud",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tutoriales de youtube",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Los primos de mi edad",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál de las siguientes es la fuente óptima para infórmate de las opciones de métodos de planificación adecuados a tu edad y evitar embarazos no deseados?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>prevención<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>candidiasis<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>abstinencia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>condilomas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>sifilis<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>clamidia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>gonorrea<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ladillas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>condon<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>sarna<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>herpes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>VIH<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida":11,
            "t11pregunta": " Encuentra en la sopa de letras las siguientes palabras relacionadas con enfermedades de transmisión sexual."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Familia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Fisiológicas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Vivienda<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Independencia<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Alimentación<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Acorde a las investigaciones de Maslow. Jerarquiza las siguientes necesidades.<\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."]}
        ]
    }
]