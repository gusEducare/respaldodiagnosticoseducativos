json = [
    {
        "respuestas": [
            {
                "t13respuesta": "que",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "como",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "quien",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "donde",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "cuando",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "cuanto",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Identifica las seis palabras clave con las que formas preguntas básicas para obtener información."
        }
    },
   {
      "respuestas":[
         {
            "t13respuesta":"Día de campo",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"Incendio forestal",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"El cerro colorado",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Elige el título que corresponda a la imagen.<br/><img src='si4e_b01_n01_01.png' style='margin : 10px 10px 10px 100px'>"
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "cien",
                "t17correcta": "1,2,4"
            },
            {
                "t13respuesta": "cien",
                "t17correcta": "1,2,4"
            },
            {
                "t13respuesta": "sien",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "cien",
                "t17correcta": "1,2,4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Si "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;problemas tú ves, en "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;problemas te encuentras, <br/>exprímete bien la "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ", hasta que "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;problemas resuelvas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la palabra adecuada para completar el trabalenguas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Sabroso",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ganador",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Hospedar",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Acertado",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Delicioso"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Vencedor"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Albergar"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Fallido"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los antónimos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "donde",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Qué",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Como",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "cuándo",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "cuanto",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Cómo",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "que",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "Cuánto",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "quien",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "quién",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/><br/>a) ¡Por fin! recordé "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;estaba guardado.<br>b) ¿"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; es lo que haces?<br>c) "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;puedes ver es mi primera clase de ballet.<br>d) Me gustaría saber "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;nos veremos.<br>e) No gracias, prefiero salir en "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;termine.<br>f) ¿"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;dices que se llama la calle?<br>g) Ella es inglesa y el italiano, "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;situación tan interesante.<br>h) ¿"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;es lo que se debe?<br>i) Fue Carlos "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;ganó el primer lugar<br>j) ¿A "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp;le gusta el pay de queso con zarzamora?"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas":true,
            "anchoRespuestas":120,
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal":true,
            "t11pregunta": "Arrastra la palabra adecuada a cada oración."
        }
    }
];
