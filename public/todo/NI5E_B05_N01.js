json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>biodiversidad</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>especie</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>conciencia</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>reino</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ecosistema</p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Término por el que se hace referencia a la variedad de seres vivos en la Tierra y los patrones naturales que la conforman."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es la unidad básica de la clasificación biológica."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Conocimiento que un ser tiene de sí mismo y de su entorno."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Representa cada una de las grandes subdivisiones en que se consideran distribuidos los seres vivos, con sus caracteres comunes."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es una unidad compuesta de organismos interdependientes que comparten el mismo hábitat."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 10,
                "datoY": 4
            },
            {
                "direccion": 1,
                "datoX": 6,
                "datoY": 0
            },
            {
                "direccion": 0,
                "datoX": 6,
                "datoY": 5
            },
            {
                "direccion": 0,
                "datoX": 10,
                "datoY": 11
            },
            {
                "direccion": 0,
                "datoX": 0,
                "datoY": 15
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Delta sesion 12 a3",
            "alto": 17,
            "ancho": 17
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Ecosistema acuático<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Factores abióticos<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Ecosistema terrestre<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Factores bióticos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Adaptación<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_a.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_b.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_c.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div><img src=\"rtp_z_s01_a01_d.png\" style='width:300px'></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <\/p>"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Arrastra las siguientes palabras con la imagen que mejor las represente:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "0",
                "coordenadas": "100,300"
            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "1",
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "",
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Preparación del proyecto, materiales, fechas, recursos, objetivos, etc.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se trata de la valoración y la pertinencia de los elementos realizados en el proyecto.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Mediante el consenso con los miembros del grupo, se seleccionará la temática que será trabajada durante el proyecto.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Es llevar a la práctica cada una de las actividades propuestas para la elaboración del proyecto.<\/p>",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            { "Contenedor": ["Diseño", "201,15", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Evaluación", "201,149", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Propósito o selección del tema", "201,283", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Ejecución", "201,417", "cuadrado", "134, 73", "."] },
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para poder realizar un proyecto, es necesario tener un equipo.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando trabajo en equipo, puedo conocer diferentes puntos de vista que enriquezcan la visión que tengo sobre algún tema.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No es necesario llevar un orden para realizar un proyecto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debo escuchar y respetar la opinión de los demás cuando realizo un trabajo en equipo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al realizar una investigación, debo consultar más de una fuente para poder tener diferentes puntos de vista, argumentos e información.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando trabajamos en equipo, todos los integrantes deben participar activamente, pero sólo uno de ellos debe exponer el trabajo.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    }
]