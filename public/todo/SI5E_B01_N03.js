json=[
    {
        "respuestas": [
            {
                "t13respuesta": "efectos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "acontecimientos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "causa",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "reacción",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "consecuencia",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "La relación antecedente-consecuente se refiere a los "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; que tiene un hecho sobre otro. La historia se compone de una serie de "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": " &nbsp; &nbsp;  que se presentan uno tras otros, pero entre ellos se da una relación de "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; y efecto que se repite indefinidamente; es decir una acción determinada origina (causa) una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; o consecuencia que deriva en otro acontecimiento("
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ")"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras que te ayuden a completar el siguiente párrafo con las características del plato del buen comer."
        }
    },
        {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Orden</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>De causa</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Consecuencia</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Orden</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Acción</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Ilustración</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Orden</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Temporalidad</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Ilustración</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>De causa</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Acción</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Oposición</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>De causa</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Consecuencia</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Oposición</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Acción</p>",
            "t17correcta":"1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Orden</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Temporalidad</p>",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":"<p>Acción</p>",
            "t17correcta":"0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta":"<p>Orden</p>",
            "t17correcta":"0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta":"<p>Temporalidad</p>",
            "t17correcta":"1",
            "numeroPregunta":"6"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["La independencia de México fue un suceso de suma importancia, debido a todos los cambios que trajo al país.",
             "Para comenzar, la lucha tuvo que ser un proceso en el que se involucraran todos los habitantes.",
             "Cuando se dan injusticias en un país, por ejemplo, en la época de la Independencia, surgen luchas para pelear en contra de esto.",
             "La lucha de independencia trajo muchos cambios, sin embargo, no fueron suficientes.",
             "Como consecuencia de la lucha, muchas personas murieron.",
             "No todas las personas estaban de acuerdo con los levantamientos armados, incluso, se oponían a ellos porque perderían sus privilegios.",
             "En un principio, las personas que querían revelarse no contaban con mucha gente para continuar la lucha."],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Las fábulas y los refranes suelen tener personajes animales u objetos con características humanas.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las fábulas y los refranes suelen estar escritas en algún texto pero también son contadas de manera oral a través del tiempo.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las fábulas y los refranes surgen de la experiencia de otras personas que han pasado por algunas situaciones muy parecidas.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Las fábulas cuentan historias que suelen ser una extensión considerable, en cambio, los refranes son frases cortas.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Las fábulas pueden tener autor; en cambio, los refranes son dichos populares anónimos.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Las fábulas suelen contener un mensaje en el que se expresa la enseñanza. Y los refranes mencionan palabras que no contienen la moraleja expresada literalmente.</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor las oraciones que hablen de las similitudes o de diferencias entre fábulas y refranes.",
            "tipo": "horizontal",
            "respuestaDuplicada": true
        },
        "contenedores": [
            "Similitudes",
            "Diferencias"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Comercial",
                "t17correcta": "1,3"
            },
            {
                "t13respuesta": "Educativa",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Comercial",
                "t17correcta": "3,1"
            },
            {
                "t13respuesta": "Política",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Social",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><div></div><img src=si5e_b01_n03_01_01.png><div></div>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><div></div><img src=si5e_b01_n03_01_05.png><div></div>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><div></div><img src=si5e_b01_n03_01_02.png><div></div>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><div></div><img src=si5e_b01_n03_01_03.png><div></div>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><div></div><img src=si5e_b01_n03_01_04.png><div></div>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ""
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Arrastra el propósito al anuncio que le corresponda.",
            "textosLargos": "si",
            "contieneDistractores": true
        }
    },
    {
        "preguntas": [
            {
                "t11pregunta": "<img src=\"si5e_b01_n03_02_01.png\"  \/>",
                "c03id_tipo_pregunta": "18"
            },
            {
                "t11pregunta": "<img src=\"si5e_b01_n03_02_04.png\" \/>",
                "c03id_tipo_pregunta": "18"
            },
            {
                "t11pregunta": "<img src=\"si5e_b01_n03_02_03.png\" \/>",
                "c03id_tipo_pregunta": "18"
            },
            {
                "t11pregunta": "<img src=\"si5e_b01_n03_02_02.png\" \/>",
                "c03id_tipo_pregunta": "18"
            }
        ],
        "respuestas": [
            {
                "t17correcta": "1",
                "t13respuesta": "Breve"
            },
            {
                "t17correcta": "2",
                "t13respuesta": "Rimas"
            },
            {
                "t17correcta": "3",
                "t13respuesta": "Uso de analogías"
            },
            {
                "t17correcta": "4",
                "t13respuesta": "Comparaciones"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona las imágenes que correspondan con algunas características que debe tener un anuncio publicitario."
        }
    }
]
