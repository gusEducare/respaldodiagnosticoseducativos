json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Introducción",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Desarrollo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cierre o conclución",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Contiene una exposición en la que el autor intente captar la atención del autor y le presenta el tema a tratar."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Contiene las premisas donde se mensionan los antecedentes o hechos referentes al tema a argumentar, las tesis que son la opinión del autor sobre lo que apoya o rechaza el autor y los armentos que son el contenido que presentará el autor para funcionar su tesis."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Constituye un texto breve en el que se refuerza la tesis u opinión del autor junto con el principal argumento."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Une las características de un ensayo con el momento en que se dividen."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Introducción",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Fichas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Párafrasis",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Texto",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Conclusiones",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Resumen",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Desarrollo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Ensayo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Notas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Citas",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Ubica las siguientes palabras en la sopa de letras."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Resúmen",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "Nota",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ""
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ": es una técnica de estudio que pretende reducir el contenido de algún texto haciendo hincapié en resaltar las ideas principales. Para resumir una información debemos: leerla, buscar el significado de las palabras que no conocemos, subrayar las ideas principales y secundarias y luego escribir con nuestras propias palabras lo que captamos de esas ideas.<br><br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ": apuntes cortos y concisos sobre alguna información amplia; se basan en incluir palabras clave que sirvan para recordar lo leído. Son de uso personal."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "t11pregunta": "Escribe si la información se trata de una nota o un resumen."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Coherencia",
                "t17correcta": "1"
            },
           {
                "t13respuesta": "Cohesión",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Texto en donde las partes que lo integran son armónicas y brindan un sentido a la totalidad de texto."
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br>Concordancia sintáctica y gramatical que deben de haber entre las palabras, las oraciones y posteriormente los párrafos."
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ""
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja":false,
            "ocultaPuntoFinal": true,
            "t11pregunta": "Responde si el texto define cohesión o coherencia."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Interrrogatorio",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Publicidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Propaganda",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Eslogan",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Promesa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Encuesta",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Logotipo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Consumidor",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Emisor",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Destinatario",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Ubica las siguientes palabras en la sopa de letras."
        }
    }
]

