json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Derechos Humanos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Constitución<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mexicanos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Convención<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Ley<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>titulares<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Los &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de niñas, niños y adolescentes están previstos en la &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Política de los Estados Unidos &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, en los tratados internacionales y en las demás leyes aplicables, esencialmente en la&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;sobre los Derechos del Niño y en la  &nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; General de los Derechos de Niñas, Niños y Adolescentes (publicada el 4 de diciembre de 2014), la cual reconoce a niñas, niños y adolescentes como&nbsp;<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;  de derechos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "respuestasLargas": true,
            "pintaUltimaCaja":false,
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Impulso</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Autorregulación</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Enojo</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Emoción</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Sentimiento</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         
         {  
            "t13respuesta":"<p>Impulso</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Dolor</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Enojo</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Incomodidad</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Sentimiento</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         
         
         {  
            "t13respuesta":"<p>Impulso</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Libertad</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Autorregulación</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Respeto</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Sentimiento</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }, 
         
         
         {  
            "t13respuesta":"<p>Impulso</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Libertad</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Autorregulación</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Enojo</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Injusticia</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         }, 
         
         
         {  
            "t13respuesta":"<p>Moral</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Justicia</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Normas</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Órdenes</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Es la capacidad de autocontrol que aprendemos y desarrollamos con el tiempo y ayuda de nuestra familia y amigos.",
                         "Emoción que ocasiona molestia o disgusto.",
                         "Facultad de obrar según su voluntad y deseo, respetando la ley y el derecho ajeno.",
                         "Ausencia  de bien común, equidad y equilibrio dentro de diversos grupos sociales.",
                         "Reglas sociales para relacionarse con otros, que fomentan la sana convivencia. En general las dicta la sociedad a partir de la educación y las costumbres. "],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Libertad de asociación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Libertad de reunión pacifica<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Libertad sindical<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Derecho a la manifestación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Libertad de opinión<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Libertad de expresión<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Libertad de circulación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Libertad de pensamiento<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Libertad de conciencia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Libertad de religión<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Derecho a la vida privada<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra al contenedor las libertades según corresponda.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Libertad colectiva",
            "Libertad individual"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Respirar profundamente cuando te encuentres alterado",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Morderte las uñas para no gritar",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Decir una palabra que te recuerde tranquilizarte",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Gritar palabras altisonantes",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Imaginar situaciones graciosas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Patear y lanzar lo que tengas cerca",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Golpear a aquel que tengas más cerca.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Conocer las causa de tus enojos y anticiparte a que ocurran.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige de entre todo lo que te pueda ayudar a controlar impulsos."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Recuerda y no pierdas de vista la meta para la que ahorras",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cada moneda cuenta...",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Diviérte sin gastar",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Explica a tu familia y amigos que estas ahorrado y cuéntales tu meta"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Gasta sabiamente..."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Persevera..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "s1a2"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Un padre no se permite a su hijo dar su opinión respecto al deporte que quiere practicar, sólo por ser un niño.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "La madre explica a su hija que por ahora no puede comprar el pantalón que ella desea, hará lo posible para el mes siguiente.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Mariana y Pedro trabajan en una fábrica de galletas, ambos realizan la misma actividad, sin embargo a Mariana le pagan menos que a Pedro.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Un oficial de policía ayuda para a los autos que tienen derecho de paso para permitir a un grupo de niños cruzar la calle para ir a la escuela.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "En la feria itinerante el puesto de pesca ofrece un peluche gigante a quien gane 50 puntos. Alejandro los ganó pero no le dieron el peluche.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala todas las situaciones injustas que identifiques."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>respeto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>salud<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>protección<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>obligaciones<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>equidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>justicia<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ambiente<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>leyes<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },{
        "respuestas": [
            {
                "t13respuesta": "<p>igualdad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>injusto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>educación<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>certeza<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>derechos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>ley<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>responsables<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>legalidad<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Define claramente tu meta<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Escribe el plan que tienes para llegar a la meta<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Describe a detalle cada uno de los pasos de tu plan anotando el tiempo en que lo lograrás<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Actúa y desarrolla tus planes. ¡pon todo tu esfuerzo, capacidad y habilidades para lograr tu meta!<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Evalúa lo que hiciste y de ser necesario replantea tu meta<\/p>",
                "t17correcta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            {"Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 4", "201,417", "cuadrado", "134, 73", "."]},
            {"Contenedor": ["Paso 5", "201,551", "cuadrado", "134, 73", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Lavar mis dientes con un vaso de agua.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Conectar sólo 10 minutos los cargadores de celular.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Sacar de una sola vez todo lo que se necesita del refrigerador",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Dejar de tomar agua de frutas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Apagar todas las luces que no son indispensables",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Reutilizar y reciclar",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todas posibilidades que tienes para colaborar con el ahorro en tu casa."
        }
    },
    {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Moral</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Justicia</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Normas</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Impulso</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Autorregulación</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Emoción</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Impulso</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Enojo</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Tristeza</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta":"<p>Justicia</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Libertad</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Autorregulación</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta":"<p>Autorregulación</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Enojo</p>",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"<p>Injusticia</p>",
            "t17correcta":"1",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Son las reglas sociales que debes seguir al relacionarte con otros, para fomentar la sana convivencia. En general las dicta la sociedad a partir de la educación y las costumbres."
                         ,"Se llama así, a la capacidad de autocontrol que aprendemos, desarrollamos con el tiempo, y ayuda de nuestra familia y amigos.",
                          "Emoción que ocasiona molestia o disgusto. ",
                          "Facultad que posee cada persona de obrar según su voluntad y deseo, respetando la ley y el derecho ajeno.",
                          "Falta de bien común, equidad  y equilibrio dentro de diversos grupos sociales."],
         "preguntasMultiples": true,
         "columnas":2
      }
   }
];