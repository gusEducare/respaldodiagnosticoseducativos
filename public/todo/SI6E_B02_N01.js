json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Se forma un círculo con los niños que deseen jugar.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se nombra un director que irá cantando las consignas   que los demás participantes deberán de imitar.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Al momento de iniciar  el juego  empiezan a palmear las manos y así sucesivamente con cada  consigna que imiten.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Se forman todos los niños participantes en una hilera, poniendo sus manos en los hombros del compañero que tienen al frente.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Previamente se eligen dos niños que serán  los que formarán  un arco con sus  brazos,  por donde pasarán todos los niños, a uno se le denomina melón y al otro sandía.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ya que están listos comienzan a avanzar cantando esta canción.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra a cada contenedor la palabra que nombra a cada ejemplo del recurso gráfico.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Números arábicos",
            "Viñetas"
        ]
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ENarración en tercera persona.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEscritura en prosa y lenguas romances.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDespertar sentimientos de tensión o angustia en el lector.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENunca pueden intervenir animales.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETodos los personajes deben sufrir alucinaciones.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Señala la condición básica para que un cuento sea con considerado de terror o misterio. "
      }
   },
   {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Entrevista</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Análisis</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Reportaje</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Enciclopedia</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta":"<p>Son una representación gráfica del pensamiento.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Presentan información de una manera ordenada, clara y precisa.</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Hacer una narración extensa de un procedimiento.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Utilizan citas para expresar la información.</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Noticia</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Entrevista</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Análisis</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Reportaje</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["¿Cuáles con los textos informativos que desarrollan extensamente un tema particular, recolectando información de diversas fuentes? ",
                         "Señala una característica de los diagramas de flujo. ", 
                         "Se puede basar en numerosos sucesos noticiosos que tengan alguna relación entre sí o se agrupen en un solo tema."],
         "preguntasMultiples": true,
         "columnas":2
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Utilizar letra legible.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>No utilices comillas para citas textuales.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Utilizar un lenguaje propio y sencillo.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Utilizar abreviaturas.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Usa el bolígrafo más cómodo que tengas.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Registra únicamente información relevante.<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Señala lo que debes tomar en cuenta para realizar notas personales."
        }
    }
    ,
   {  
      "respuestas":[
         {  
            "t13respuesta":"<p>Uso de pronombres</p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Uso de sustantivos </p>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Uso de adjetivos</p>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<p>Desenlace</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta":"<p>Nudo</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>Introducción</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["¿Qué elementos son necesarios para realizar una descripción detallada en los cuentos de misterio o terror?  ",
                         "¿Cuál es el momento de un cuento en que los personajes se enfrentan a un problema que deben solucionar o luchar por algún objetivo? "
                         ],
         "preguntasMultiples": true,
         "columnas":2
      }
   }
];