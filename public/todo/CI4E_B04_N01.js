[ 
        {
        "respuestas": [
            {
                "t13respuesta": "<p>Leyes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Sanción<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Legalidad<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Normas<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
        {
        "respuestas": [
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<img src=\"EJEMPLO.png\">",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ley",
            "No Ley"
        ]
    },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src=\"EJEMPLO.png\">",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona la imágenes que representen algunos de los pasos para aprender a convivir con los demás."
      }
   },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>localidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>ciudad<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>pertenencia<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>normas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>convivencia<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Una <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; es la referencia para indicar un pueblo o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>,respecto a donde tu vives, en la cual compartes ese sentido de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; con tus vecinos y personas que como tu, viven ahí. Dentro de una localidad pueden existir infinidad de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; o pueden basarse en las normas básicas que regulan la buena <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>País<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Norma<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Ley<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Orden<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Actos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Respecto<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
       {
        "respuestas": [
           
            {
                "t13respuesta": "<p>Secretaría de Hacienda y Crédito Público<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Dirección del colegio<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Policía<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Padres de familia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Juez de la Suprema Corte<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Ley",
            "Norma"
        ]
    }
]