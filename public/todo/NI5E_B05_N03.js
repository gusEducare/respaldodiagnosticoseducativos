json=[    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Todas las formas de vidas existentes en el universo. <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>A la variedad de plantas, animales, hongos y microorganismos en un ecosistema.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>La relación entre las diferencias genéticas entre los organismos de una misma especie. <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los diferentes atributos que tienen los animales.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "La biodiversidad se refiere:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El agua y la energía solar.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>La infraestructura para construcciones.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Alimentos, medicinas, materiales para construcción  y vestimenta.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los elementos para investigaciones en la conservación de energía.<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Hay muchos recursos que nos brinda la biodiversidad como son:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>60% y 70%<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>50%<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>100%<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>70% y 80%<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "El porcentaje de especies conocidas en el planeta que alberga México es del:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>535 de mamíferos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>804 de reptiles<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>2184 de insectos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>47853 de peces<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "México alberga en especies:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Formación de la identidad cultural.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Gran producción de tecnología.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Influencia en las formas de organización social.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Producción de artículos provenientes de animales, plantas, etc. <\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Dentro de los beneficios que tiene México y cualquier país con una gran biodiversidad, están: "
        }
    },
    {
        "preguntas": [
            {
                "t11pregunta": "<img src=\"esp_db1_i_32_a_01.png\" style=\"height: 84px;\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\"esp_db1_i_32_a_02.png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\"esp_db1_i_32_a_03.png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\"esp_db1_i_32_a_04.png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            },
            {
                "t11pregunta": "<img src=\"esp_db1_i_32_a_04.png\" style=\"height: 84px;\" alt=\"\" \/>",
                "c03id_tipo_pregunta": "12"
            }
        ],
        "respuestas": [
            {
                "t17correcta": "1",
                "t13respuesta": "Delphinus delphis"
            },
            {
                "t17correcta": "2",
                "t13respuesta": "Homo sapiens"
            },
            {
                "t17correcta": "3",
                "t13respuesta": "Canis familiaris"
            },
            {
                "t17correcta": "4",
                "t13respuesta": "Mus musculus"
            },
            {
                "t17correcta": "5",
                "t13respuesta": "Octupus vulgaris"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t13respuesta": "s1a2",
            "anchoImagen": 84,
            "altoImagen": 84,
            "iconos": true
        }
    },
     {
      "respuestas": [
          {
              "t13respuesta": "<p>PET (Tereftalato de polietileno) Reciclado 23%</p>",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>Bolsas para compras, botellas de detergentes, leche y jugos.</p>",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "<p>PVC (Policloruro de vinilo) Reciclado 1%</p>",
              "t17correcta": "2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"esp_kb1_s01_a01_01.png",
          "respuestaImagen":true, 
          "bloques":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "0,0", "cuadrado", "200, 100", "."]},
          {"Contenedor": ["", "250,0", "cuadrado", "200, 100", "."]},
          {"Contenedor": ["", "250,250", "cuadrado", "200, 100", "."]}
      ]
  },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Observación y preguntas</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Investigación</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Hipótesis</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Experimentación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Análisis y conclusión</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Comunicación de resultados</p>",
            "t17correcta":"0"
         },
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Ya teniendo el tema que investigarás, realiza búsquedas en libros, Internet y entrevista a gente especializada para recabar la mayor cantidad de información. Revisa y depura la información para validar las fuentes."
      }
   },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Observación y preguntas</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Investigación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Hipótesis</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Experimentación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Análisis y conclusión</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Comunicación de resultados</p>",
            "t17correcta":"0"
         },
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Una vez que tienes la información realiza una conjetura, que generalmente puede empezar como 'Si yo... (hago esto) entonces... (pasa esto)'. Dicha conjetura se indica de tal forma que los resultados se puedan mediar y se refiere a la pregunta original"
      }
   },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Observación y preguntas</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Investigación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Hipótesis</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Experimentación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Análisis y conclusión</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Comunicación de resultados</p>",
            "t17correcta":"0"
         },
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Se analiza la información o los datos obtenidos de la experimentación para concluir si la hipótesis es falsa o verdadera. Si la hipótesis es falsa, se puede investigar con información adicional, formulando una nueva hipótesis y un nuevo proceso. Si es verdadera, se experimenta de nuevo para comprobar los resultados."
      }
   },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Observación y preguntas</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Investigación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Hipótesis</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Experimentación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Análisis y conclusión</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Comunicación de resultados</p>",
            "t17correcta":"0"
         },
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Realiza observaciones sobre cosas ordinarias para examinarlas en nuevas formas. Realiza planteamientos y preguntas para seleccionar la pregunta que sea la base de tu investigación cientifíca."
      }
   },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Observación y preguntas</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Investigación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Hipótesis</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Experimentación</p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Análisis y conclusión</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Comunicación de resultados</p>",
            "t17correcta":"0"
         },
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Ahora se pone a prueba la hipótesis con un experimenyt, con el objetivo de descubrir si la hipótesis es verdadera o falsa."
      }
   },
  {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Observación y preguntas</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Investigación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Hipótesis</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Experimentación</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Análisis y conclusión</p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Comunicación de resultados</p>",
            "t17correcta":"1"
         },
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Sera inportante presentar las conclusiones a través de informes, demostraciones o exhibiciones. Se pueden utilizar medios electrónicos para su publicación."
      }
   }
]