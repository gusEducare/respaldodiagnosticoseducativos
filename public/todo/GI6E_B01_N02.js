json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>plana<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Eratóstenes<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>redonda<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Copérnico<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Cuando se empiezan a desarrollar las primeras civilizaciones se creía que la tierra era <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, no fue hasta que <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; comprobó en el siglo tres antes de cristo que la tierra es <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Algún tiempo después, Ptolomeo crea un modelo que dice que la tierra es el centro del universo y el sol, los planetas y las estrellas giran alrededor de ella. Esta teoría fue valida por más de 1600 años, hasta que <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; crea un modelo en el que el sol es el centro del universo y la tierra y todos los planetas giran en torno a él.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras a los espacios que faltan para completar el párrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Medición de distancias",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Saber llegar a otros lugares",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Medir el tiempo de vida del planeta",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Delimitación de fronteras",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Comprobar que la tierra es redonda",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Disminuir la contaminación",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona cuales de las siguientes son razones de la importancia de que se hayan inventado los mapas."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "gi6e_b01_n03_01_02.png",
                "t17correcta": "3",
                "columna": "0"
            },
            {
                "t13respuesta": "gi6e_b01_n03_01_03.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "gi6e_b01_n03_01_04.png",
                "t17correcta": "1",
                "columna": "1"
            },
            {
                "t13respuesta": "gi6e_b01_n03_01_05.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "gi6e_b01_n03_01_06.png",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
    "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "gi6e_b01_n03_01_01.png",
            "respuestaImagen": true,
            "bloques": true,
            "tamanyoReal": true
        },
        "contenedores": [
            {"Contenedor": ["", "28,231", "cuadrado", "165,55", "."]},
            {"Contenedor": ["", "27,455", "cuadrado", "165,55", "."]},
            {"Contenedor": ["", "155,20", "cuadrado", "165,55", "."]},
            {"Contenedor": ["", "225,430", "cuadrado", "165,55", "."]},
            {"Contenedor": ["", "360,250", "cuadrado", "165,55", "."]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Mundial<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Nacional<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Estatal<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"gi6e_b01_n03_02_01.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"gi6e_b01_n03_02_02.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br><div></div><img src=\"gi6e_b01_n03_02_03.png\" style='width:300px'><div></div><\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "textosLargos": "si",
            "contieneDistractores": false,
            "t11pregunta": "Arrastra la escala al tipo de mapa que le corresponde."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Brinda información de lo que contiene el mapa en símbolos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sirve para localizar puntos en un mapa",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Nos indica la posición del norte",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Relación del tamaño real con el tamaño del mapa",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Simbología"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Coordenadas"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Orientación"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Escala"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona el tipo de elemento de los mapas con su definición."
        }
    },
   {  
      "respuestas":[
         {  
            "t13respuesta":"<p>5</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>10</p>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>15</p>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<p>5</p>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>2</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>6</p>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<p>Área peatonal, playa municipal, palacio municipal.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Playa municipal, La palapa, palacio municipal.</p>",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"<p>Playa municipal, palacio municipal, plaza y área peatonal.</p>",
            "t17correcta":"1",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Observa el siguiente plano y responde las preguntas.<br/><img src='gi6e_b01_n03_03.png'>","¿Cuántos hoteles hay en el plano?","¿Cuántos restaurantes hay?","¿Qué sitios de interés podemos encontrar?"],
         "preguntasMultiples": true,
         "columnas":1
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "Lázaro Cárdenas, Obrera, Algarín, Eje Central",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Parque Delta, Mural, Panteón Francés, Universidad Insurgentes, Mercado Hidalgo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Dr. Bolaños, Dr. García Diego, Dr. Márquez, Dr. Jiménez, Dr. Neva",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuáles de los siguientes son sitios de interés en el plano?<br/><img src='gi6e_b01_n03_04.png'>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Sistema de posicionamiento global",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Imágenes tomadas desde aviones",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Integra varias tecnologías geográficas",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Imágenes tomadas desde satélites",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "GPS"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Fotografía aérea"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Sistema de información geográfica"
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Imágenes de satélite"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona la tecnología de manejo de información geográfica con la frase que le corresponda"
        }
    }
]