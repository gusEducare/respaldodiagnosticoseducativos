json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<p>materia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>sólido<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>gaseoso<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>físicas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>volumen<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>kilos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>litro<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Todos los cuerpos que conocemos, como las libretas, los lápices, computadoras, teléfonos, por mencionar algunos, están formados de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, que es lo que forma cada cuerpo. La materia se presenta en estado<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, líquido o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, cada estado se caracteriza por un conjunto de propiedades <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y químicas que son susceptibles de sufrir cambios. Las propiedades físicas más conocidas de la materia son la masa y el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. Frecuentemente la cantidad de materia de los cuerpos sólidos se mide en <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, para los líquidos se usa el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y para los gases es muy variable pues ello depende del recipiente que los contenga, algunas veces se mide en volumen y en otras la masa.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ser humano tiene una necesidad constante de medir para poder tener relaciones justas y entender el mundo en el que vive.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la ciencia se mide para poder tener certezas, datos  y forma de comprobar las cualidades de un objeto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las unidades más comunes para medir son el galón y la onza.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La masa y el volumen son dos propiedades con las que cuentan todos los cuerpos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para poder medir un cuerpo en estado gaseoso, necesariamente tenemos que cambiarlo a estado líquido.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando un cuerpo está en estado sólido es mucho más fácil medir la masa.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EVolumen\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMasa\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EPeso\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El concepto que corresponde a la cantidad de materia que tiene un cuerpo es: "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EVolumen\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EDensidad\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMasa\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El concepto que corresponde al espacio que ocupa un cuerpo es:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EVolumen\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDensidad\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EMasa\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El concepto que determina la flotabilidad de una sustancia sobre otro es:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EMayor\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMenor\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EPeso\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Para que un cuerpo deba flotar en el agua debe tener:________________ masa"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EVolumen\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EPeso\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMasa\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El concepto de _____________ corresponde a la acción de la gravedad sobre la materia de un cuerpo es: "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Propiedades fisicas<\/p>",
                "t17correcta": "1"
            },
            {

                "t13respuesta": "<p>Disolver<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Mezcla homogenea<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Mezcla heterogénea<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Mezcla<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Son las propiedades que cambian su materia, pero no su composición."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Deshacer hasta que todas las partícula se incorporan al otro componente de la mezcla."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Mezcla donde los componentes no se distinguen uno de otro."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Mezcla donde distinguimos cada uno de los componentes."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Combinación de dos o más sustancias."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 9,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 6,
                "datoY": 3
            },
            {
                "direccion": 0,
                "datoX": 4,
                "datoY": 8
            },
            {
                "direccion": 0,
                "datoX": 1,
                "datoY": 10
            },
            {
                "direccion": 0,
                "datoX": 4,
                "datoY": 18
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "alto": 20,
            "t11pregunta": "Delta sesion 12 a3"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Tamizado",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Decantación",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Filtración",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img>",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img>",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img>",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img>",
                //"valores": ['$85', '$125', '$150'],
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img>",
                //"valores": ['$200', '$180', '$130'],
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img>",
                //"valores": ['$200', '$180', '$130'],
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Pregunta Opci&oacute;n Matriz<\/p>",
            "descripcion": "Métodos de separación de mezclas",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Falta de aire<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Asma<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Baja oxigenación de los glóbulos rojos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Mayor riesgo de formación de coágulos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Opresión en el pecho<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Muerte y eliminación de las células que revisten las vías respiratorias.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Estrechamiento de las vías respiratorias.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Flemas<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Fatiga inusual<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Enfisema<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Efectos respiratorios",
            "Efectos cardiovasculares"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ERadiación\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EConvección\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EConducción\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El calor se transmite de un objeto caliente a otros objetos a distancia por medio de ondas electromagéticas. Los rayos solares son un ejemplo, pues los rayos son luz que viajan en forma de ondas a través de la atmósfera del planeta y al incidir en los objetos los calienta."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EConvección\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ERadiación\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EConducción\u003C\/p\u003E",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Es la transferencia de calor que courre por contacto directo de objetos o sustancias expuestas directamente a una fuente de calor. Un ejemplo común es el calor que transfiere el metal de una sartén caliente cuando se toma por la parte metálica."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EConvección\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ERadiación\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EConducción\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Transferencia de calor acontecida por el movimiento ascendente de moléculas de gases o líquidos calientes de un lugar a otro. Este tipo de transferencia de calor se aprecia claramente cuando calentamos nuestras manos en una fogata. Lo que nos calienta no es fuego, sino el aire que ha sido calentado por la llama y que comienza ascender en la atmósfera porque el calor lo hace más ligero. Los líquidos y gases son las sustancias con la mejor capacidad para transferir calor por convección."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>transferencia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>climáticas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>ciclo de agua<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>atmósfera<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>ganancia<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Los procesos de <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;de calor ocurren de manera permanente en el planeta y son el origen de fenómenos naturales que mantienen las condiciones <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y ambientales que permiten sostener la vida.<br/>Dos fenómenos naturales donde se manifiestan los procesos de transferencia del calor son el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;y las corrientes de aire. Como sabes, el ciclo del agua es la serie de etapas que tiene el agua en su paso de los océanos a la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, la tierra y nuevamente a los océanos. Esta serie de etapas ocurre por el cambio de estado (líquido, gas, sólido) que sufre el agua por la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp;o pérdida de calor.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras que te ayuden a completar el siguiente párrafo:"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El ciclo comienza con la evaporación del agua líquida contenida en los océanos, lagos, ríos y por la transpiración de plantas y animales por efecto de la energía del Sol.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El vapor asciende, se enfría y se vuelve a transformar en líquida formando las nubes.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Luego el agua de las nubes cae nuevamente a la Tierra en forma líquida, de granizo y nieve.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Después viene la transferencia por convección, pues el vapor de agua que está caliente disminuye su densidad y se hace mas ligero, provocando que ascienda por la atmósfera y que masas de \n\
                                    aire frío ocupen su lugar, donde también se calentar, ascenderá y el cielo se repite.<\/p>",
                "t17correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en&nbsp;esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar"
        },
        "contenedores": [
            { "Contenedor": ["Paso 1", "201,15", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Paso 2", "201,149", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Paso 3", "201,283", "cuadrado", "134, 73", "."] },
            { "Contenedor": ["Paso 4", "201,517", "cuadrado", "134, 73", "."] }
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Son materiales aislantes y son porosos o con aire en su interior, de modo que no conducen el calor.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img /><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><img /><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Son aquellos que tienen una composión compacta que permite un rápido intercambio de energía y conduce el calor de un lugar a otro.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img /><\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p><img /><\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Zeta sesion 9 a1",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Materiales conductores",
            "Materiales aislantes"
        ]
    }
]