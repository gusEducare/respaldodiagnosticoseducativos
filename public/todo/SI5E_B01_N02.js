json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Todo el día estaré con sueño y no podré hacer mis actividades normales.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es muy probable que tenga sobrepreso o que no tenga buena salud.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Es muy probable que pueda reprobar la materia.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Practico un deporte y me divierto haciéndolo.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "No duermo bien y me desvelo constantemente."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Sólo como comida chatarra, no tomo agua y no realizo ejercicio."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "No cumplo con mis tareas y tampoco estudio para un examen."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Practico constantemente un deporte y me preparo."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": " Relaciona los sucesos que ocurran con sus respectivas consecuencias."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Podemos retomar la información de un texto, aunque no citemos cuando escribimos de forma textual lo que dice un autor.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Es importante identificar el propósito de un texto, leyendo con atención y extrayendo la información que necesitamos, citando adecuadamente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Una estrategia para analizar la información de un texto es resaltar las ideas parafrasear los elementos que consideremos los más importantes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se puede relatar un hecho histórico sin tener precisión o dando nuestra opinión no sea del todo objetiva.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Para realizar una investigación adecuada, es necesario, consultar más de una fuente para poder tener datos suficientes y no quedarnos con una idea parcial.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": "1",
            "t11pregunta": "Elige las oraciones que hablen de la forma de recuperar información sin perder el significado original de un texto."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>El presidente de la República de México comienza la ceremonia.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>con grandes fugos artificiales y comida típica.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>las palabras que dijo el Padre Miguel Hidalgo</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Él hace sonar las campanas y repite</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El presidente ondea la bandera de nuestro pais</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>celebrando nuestra independencia.</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": " Arrastra las oraciones que presenten una idea central y las oraciones que son de apoyo al contenedor correcto.",
            "tipo": "horizontal"
        },
        "contenedores":[ 
            "Las oraciones que presenten una idea central",
            "Las oraciones que son de apoyo"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "después, luego, finalmente, posteriormente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "antes,anteriormente,previamente.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Mientras tanto, al mismo tiempo, en la misma fecha, para ese entonces, entonces, etcétera.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Para exponer hechos que ocurren después de un acontecimiento puntual."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Para exponer hechos que sucedieron antes de uno en particular."
            },
            {
                "c03id_tipo_pregunta": "18",
                "t11pregunta": "Cuando queremos hablar de sucesos que ocurren al mismo tiempo usando conectores temporales de simultaneidad."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "18",
            "t11pregunta": "Relaciona los conectores con su respectiva definición."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Estas expresiones no tienen autor, si no que son frases que se han repetido de generación en generación.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Es una frase o dicho que escuchamos día a día y se tratan de consejos basados en la experiencia.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Los personajes suelen ser animales u objetos inanimados que cobran características humanas.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Narraciones cortas, escritas en prosa o verso que cuentan alguna historia que finaliza con una reflexión o enseñanza.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Los personajes viven situaciones que alguna vez nos pudieran suceder.</p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra las características de las fábulas y refranes al sitio que les corresponde.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Fábulas",
            "Refranes"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Son atemporales, no se ubican en una época específica.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Pueden tener autor.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Están escritas en prosa o verso.</p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Son frases cortas.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son fáciles de recordar y repetir.</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Son anónimos.</p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra las características que pertenecen a las fábulas o a los refranes.",
            "tipo": "horizontal"
        },
        "contenedores": [
            "Fábulas",
            "Refranes"
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "agua lleva.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "échate a dormir.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "no mires a quien.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "más le viene.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "se lo lleva la corriente.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br/>Cuando el rio suena, "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br/>Cría fama y "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br/>Haz el bien y "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br/>A quien mucho tiene, "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ".<br/><br/>Camarón que se duerme "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa los siguientes refranes."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "hipérbole o exageracion",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "recursos literarios",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "metáfora",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "expresiones coloquiales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "personificación",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es cuando se exageran las características de algo para hacerlo más llamativo o impresionante."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Nos ayudan a embellecer un texto, ser más expresivos y dar a entender el mensaje."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es cuando se menciona algo que se parece a otra cosa, es decir, tienen algo que los asemeja."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Son frases o palabras informales y espontáneas que utiliza le gente en su forma de hablar cotidianamente."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es cuando se le otorga características humanas a animales o seres inanimados."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 12,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 2,
                "datoY": 2
            },
            {
                "direccion": 0,
                "datoX": 6,
                "datoY": 5
            },
            {
                "direccion": 0,
                "datoX": 1,
                "datoY": 12
            },
            {
                "direccion": 1,
                "datoX": 8,
                "datoY": 8
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Realiza el siguiente crucigrama:",
            "alto": 25,
            "ancho": 25
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Corto y sencillo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Invita a comprar o realizar alguna acción.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tiene un función educativa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Es pegajoso y se recuerda facilmente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Resalta defectos o aspectos negativos.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las oraciones que nos hablan de las características del 'eslogan':"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "persuacion indirecta",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "apariencia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "publicidad informativa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "persuadir",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "testimonial",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "similitud",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Cuando se relaciona el producto o servicio que se está vendiendo con una característica que no tiene nada que ver para atraer a las personas y lograr que ellos se creen una conexión entre las dos cosas."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Usada en imágenes para atraer la atención del comprador. Debe ser atractiva y llamativa."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Cuando se presenta información real al público sobre los resultados de una investigación, se le presentan datos que demuestren que el producto o servicio que se está ofreciendo es bueno por una razón."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Es la capacidad de convencer a un público sobre algo según unas características o argumentos."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Colocar a personas famosas o a gente común hablando o recomendando un producto o servicio."
            },
            {
                "c03id_tipo_pregunta": "15",
                "t11pregunta": "Técnica muy usada es la de mostrarle al público que el producto o servicio que están vendiendo se parece a quien quieren que lo compre, ya sea por su físico, personalidad o gustos."
            }
        ],
        "pocisiones": [
            {
                "direccion": 1,
                "datoX": 7,
                "datoY": 1
            },
            {
                "direccion": 0,
                "datoX": 4,
                "datoY": 3
            },
            {
                "direccion": 0,
                "datoX": 1,
                "datoY": 12
            },
            {
                "direccion": 1,
                "datoX": 22,
                "datoY": 7
            },
            {
                "direccion": 0,
                "datoX": 7,
                "datoY": 19
            },
            {
                "direccion": 1,
                "datoX": 11,
                "datoY": 14
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "15",
            "t11pregunta": "Realiza el siguiente crucigrama:",
            "alto": 25,
            "ancho" : 25
        }
    },
        {  
      "respuestas":[
         {  
            "t13respuesta": "Brevedad",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Uso de adjetivos",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Uso de analogías",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Metáforas",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Rimas",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Uso de adjetivos",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Uso de analogías",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Metáforas",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Metáforas",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Rimas",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Juegos de palabras",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Comparaciones",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Metáforas",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Uso de adjetivos",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Juegos de palabras",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Comparaciones",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Uso de adjetivos",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Comparaciones",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Metáforas",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Uso de analogías",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Comparaciones",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Juego de palabras",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Metáforas",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Metáforas",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Comparaciones",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Uso de analogías",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Juego de palabras",
            "t17correcta": "1",
            "numeroPregunta":"6"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Es importante condensar los textos y hacerlos lo más breve posibles, porque de lo contrario es probable que las personas no recuerden el mensaje que transmitía la publicidad\"; se refiere a:",
                         "Para que las personas se sientas más seguras al momento de comprar un producto, se suelen emplear comparaciones o relaciones entre lo que se ofrece y algún otro producto o servicio que ya existe, como:",
                         "Es el uso de la repetición de un fonema o sonido de palabras para que las personas lo recuerden más facilmente:",
                         "Se refiere cuando se sustituye una palabra por una imagen o por otra palabra que tenga otro significado pero que tenga relación con el sonido:",
                         "Para hacer más llamativos los productos o servicios que se ofrecen, las publicidades contienen muchas características positivos que definan a la marca como algo bueno que todos deberían tener, se utilizan:",
                         "Es la definición de algo con las características de otra cosa, resaltando las semejanzas o elementos en común:",
                         "Se emplean para resaltar en qué se diferencia o asemeja lo que se está ofreciendo a otra cosa, producto, sentimiento o sensación:"],
         "preguntasMultiples": true,
         "columnas":2
      }
   }
]
