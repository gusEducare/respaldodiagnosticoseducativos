json =[
    {
        "respuestas": [
            {
                "t13respuesta": "ci6e_b01_n01_02.png",
                "t17correcta": "0,1,2,3",
                "columna": "1"
            },
            {
                "t13respuesta": "ci6e_b01_n01_03.png",
                "t17correcta": "1,0,2,3",
                "columna": "0"
            },
            {
                "t13respuesta": "ci6e_b01_n01_04.png",
                "t17correcta": "2,0,3,1",
                "columna": "1"
            },
            {
                "t13respuesta": "ci6e_b01_n01_05.png",
                "t17correcta": "3,0,1,2",
                "columna": "1"
            },
            {
                "t13respuesta": "ci6e_b01_n01_06.png",
                "t17correcta": "4,5,6,7",
                "columna": "0"
            },
            {
                "t13respuesta": "ci6e_b01_n01_07.png",
                "t17correcta": "5,4,6,7",
                "columna": "0"
            },
            {
                "t13respuesta": "ci6e_b01_n01_08.png",
                "t17correcta": "6,4,5,7",
                "columna": "1"
            },
            {
                "t13respuesta": "ci6e_b01_n01_09.png",
                "t17correcta": "7,4,5,6",
                "columna": "0"
            },
            {
                "t13respuesta": "ci6e_b01_n01_10.png",
                "t17correcta": "8,9,10,11,12",
                "columna": "0"
            },
            {
                "t13respuesta": "ci6e_b01_n01_11.png",
                "t17correcta": "9,10,11,8,12",
                "columna": "1"
            },
            {
                "t13respuesta": "ci6e_b01_n01_12.png",
                "t17correcta": "10,11,8,9,12",
                "columna": "1"
            },
            {
                "t13respuesta": "ci6e_b01_n01_13.png",
                "t17correcta": "11,8,9,10,12",
                "columna": "1"
            },
            {
                "t13respuesta": "ci6e_b01_n01_14.png",
                "t17correcta": "12,8,9,10,11",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Lleva a cada contenedor todo lo que corresponda al desarrollo adolescente de acuerdo al sexo.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "ci6e_b01_n01_01.png",
            "respuestaImagen": true,
            "bloques": false,
            "opcionesTexto": true,	
            "tamanyoReal": true,
            "borde":false,
            "anchoImagen":"67"

        },
        "contenedores": [
            {"Contenedor": ["", "136,35", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "205,35", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "274,35", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "343,35", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "136,235", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "205,235", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "274,235", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "343,235", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "136,437", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "205,437", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "274,437", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "343,437", "cuadrado", "130, 62", ".","transparent"]},
            {"Contenedor": ["", "412,437", "cuadrado", "130, 62", ".","transparent"]}
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>bienestar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>ausencia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>sexual<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>enfoque<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>experiencias<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>seguras<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>derechos<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>“Un estado de completo <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; físico, emocional, mental y social en relación con la sexualidad, no es solamente la <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; de enfermedad, disfunción o malestar. La salud <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; requiere un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; positivo y respetuoso hacia la sexualidad y las relaciones sexuales, así como la posibilidad de tener <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> sexuales placenteras y <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, libres de coerción, discriminación y violencia. Para que la salud sexual se logre y se mantenga, los <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, sexuales de todas las personas deben ser respetados, protegidos y cumplidos.” <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": false,
            "t11pregunta": "Arrastra las palabras para completar la definición de salud sexual según la WAS y la OMS."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Realizar un lavado vaginal.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Usar la píldora del día siguiente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Probar algún té que recomienden. ",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Luego de tener relaciones sexuales sin protección ¿Qué opción tienes para evitar un embarazo no deseado?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Uso de Condón",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ejercer mi sexualidad con una sola persona.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Abstinencia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tomar pastillas anticonceptivas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Práctica de sexo seguro: besos, caricias, abrazos y autoerotismo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Pastilla del día siguiente",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Elige todos los métodos para evitar enfermedades de transmisión sexual."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "casualidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "obligaciones",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "causas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "inoportunas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "jovialidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "decisiones",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "suerte",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "crecimiento",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "consecuencias",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "compromiso",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "madurez",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "disminución",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "columnas": 4,
            "t11pregunta": "Elige todas las palabras que tengan relación con el concepto de responsabilidad."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Información<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Adultos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Fuentes<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Indagar<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Internet<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Expertos<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Libros<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Veraces<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Certeza<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Análisis<\/p>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11medida": 13,
            "t11pregunta": "Identifica las palabras relacionadas con la obtención de información."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Cuando platicas con tus padres.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cuando conoces a una persona por internet.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Con un extraño en la calle.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Con un psicólogo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Con un doctor.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las situaciones en las que consideras que es apropiado compartir tu información o dudas"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>manipulación<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>prejuicios<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>inter&eacute;s<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>prestatario<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>plazo<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>Cuando se realiza un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, una persona conocida como<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> le entrega una cantidad de dinero a otra, conocida como <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. <br>El dinero debe ser devuelto en el <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p> acordado y se debe pagar, adicionalmente, un <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, es decir un porcentaje extra por haber usado el dinero <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras para completar el texto:"
        }
    }
]