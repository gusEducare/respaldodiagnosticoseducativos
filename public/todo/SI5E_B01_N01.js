json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Consecuencia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Consecuente",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Antecendente",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Relaciones",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Conectores",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Reaccion",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Origen",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Efecto",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Causa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Raiz",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "16",
            "t11pregunta": "Realiza la siguiente sopa de letras."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "moralejas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "fábula",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "lección",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "aconsejarles",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "equivocarse",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br>Las "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; son aquellas enseñanzas que quedan después de leer o escuchar una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": " &nbsp; o refrán. Es el mensaje que pretende transmitir una "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; sobre nuestra vida a través de las situaciones que viven otros personajes.<br>\n\
                                                        Las moralejas suelen estar presentes en libros de literatura infantil. Sin embargo, \n\
                                                            las personas suelen comentarlas a otros cuando quieren "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; sobre cómo hacer las cosas o cómo evitar "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que te ayuden a completar el siguiente párrafo relacionado con el significado de las moralejas.",

        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Contacto",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Logotipo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Informar",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Llamado a la acción",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Encabezado",
                "t17correcta": "5"
            },
                   {
                "t13respuesta": "Fotografía",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Recordar",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "Persuadir",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "Eslogan",
                "t17correcta": "9"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; el teléfono, correo electrónico, página web o redes sociales de la marca para invitar a las personas a continuar el contacto.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; símbolos, imágenes y letras que conforman el nombre de la empresa. Suele ser un dibujo sencillo y fácil de recordar.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; decirle al público qué es lo que se está ofreciendo, si es un producto o servicio, y el por qué es único entre las distintas marcas que existen.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; el mensaje que invita al público a hacer la compra, realizar una provechar una oferta o promoción.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; es un título o mensaje corto que busca llamar la atención del espectador.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; imagen que capte la atención del público y que esté relacionada con la marca y con lo que se vende. Es recomendable que tenga la foto de una persona para que los demás se sientan identificados.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; lograr que las personas recuerden una marca o producto en particular y no otra aunque sea parecida o cubra las mismas necesidades.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; convencer al público de que el producto o servicio que se está ofreciendo es el mejor y es el que necesita.<br>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ":&nbsp; la frase que identifica a la marca o a la empresa. Es un mensaje corto, sencillo, fácil de recordar y que busca que las personas lo tengan en mente."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los conceptos a sus definiciones."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "sugestiva",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "sugerir",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "compren",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "emociones",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "público",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "publicitario",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><br>La función "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; quiere decir que se pretende "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; a las personas que "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; o inviertan su dinero en lo que se está ofreciendo.<br>Incluye "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; o sentimientos para atraer aún más al "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "&nbsp; y lograr que después de ver o escuchar el anuncio "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": ",&nbsp;compre"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que te ayuden a completar el siguiente párrafo relacionado con la función sugestiva de la publicidad.",
            "pintaUltimaCaja": false
        }
    }
]
