json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 5.19 cm \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 10.38 cm \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 5.19 cm \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "El plano muestra las dimensiones de  la casa , si se requiere que  en el plano la dimensión correspondiente al lado de 8,88 m mida 44.4 cm ¿Cuánto debe medir el lado homologo a 10,38 m?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 3000 cm<sup>2</sup> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 2271.83 cm<sup>2</sup> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 2240.79 cm<sup>2</sup> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué superficie de papel se requiere para elaborar el plano con las dimensiones anteriores"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 05/06/2015 \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 01/07/2015 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 17/06/2015 \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "De acuerdo  con la gráfica, ¿En que día del mes de junio el dólar alcanzo su valor más alto?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 11/06/2015 \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 17/06/2016 \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 25/06/2016 \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "De acuerdo  con la gráfica, ¿Qué día el dólar alcanzó su valor mínimo?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>4</p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>-1</p>",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>2</p>",
                "t17correcta": "2",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>-1</p>",
                "t17correcta": "3",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "mat_7b1_s05_a06_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true

        },
        "contenedores": [
            {"Contenedor": ["", "114,292", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "114,417", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,211", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,318", "cuadrado", "64, 64", ".", "blue"]}
        ],
        "css": {
            "displayImgLineas": "none"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>10</p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>-11</p>",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>12</p>",
                "t17correcta": "2",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>-13</p>",
                "t17correcta": "3",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>-14</p>",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "mat_7b1_s05_a06_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true

        },
        "contenedores": [
            {"Contenedor": ["", "114,292", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "114,417", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,211", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,318", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,400", "cuadrado", "64, 64", ".", "blue"]}
        ],
        "css": {
            "displayImgLineas": "none"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>5</p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>2</p>",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>1</p>",
                "t17correcta": "2",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>2</p>",
                "t17correcta": "3",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "mat_7b1_s05_a06_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true

        },
        "contenedores": [
            {"Contenedor": ["", "114,292", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "114,417", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "114,500", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,211", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,318", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "271,400", "cuadrado", "64, 64", ".", "blue"]}
        ],
        "css": {
            "displayImgLineas": "none"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>B</p>",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>C</p>",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "<p>A</p>",
                "t17correcta": "2",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "mat_7b1_s05_a06_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "opcionesTexto": true

        },
        "contenedores": [
            {"Contenedor": ["", "114,292", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "114,417", "cuadrado", "64, 64", ".", "blue"]},
            {"Contenedor": ["", "114,500", "cuadrado", "64, 64", ".", "blue"]}
        ],
        "css": {
            "displayImgLineas": "none"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Identifica cual curva representa  gráficamente la función  y=  3 x + 5 "
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál de las  siguientes gráficas representa  a un caracol que sube una pared a velocidad constante por la noche y desciende a velocidad constante por la mañana?"
        }
    },
    {
        "respuestas": [
            
            {
                "t13respuesta": "\u003Cp\u003E Error \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Observa la siguiente ecuación  x<sup>2</sup> = -4x-4 . ¿Cuál expresión representa el uso correcto de la formula general para resolver la ecuación?"
        }
    },
    { 
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E 25 metros \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E 2.5 metros \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E 250 metros \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué longitud tendrá una rampa en la que su altura máxima es de 7 m y su longitud horizontal (sobre el piso) es 24 m?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas":false,
            "t11pregunta": "Utilizando los criterios de semejanza de triángulos arrastra del contenedor los triángulos frente al triángulo que sea congruente<img src='.png'>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas":false,
            "t11pregunta": "<img src='.png'>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E <img src='.png'> \u003C\/p\u003E ",
                "t17correcta": "0"
            }
        ], "pregunta": {
            "c03id_tipo_pregunta": "1",
            "dosColumnas":false,
            "t11pregunta": "<img src='.png'>"
        }
    }
]