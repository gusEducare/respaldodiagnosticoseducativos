json=[
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "<p>Menciona 3 ramas de la química analítica, estequiometría, bioquímica.</p>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>separa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>puros<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>comprime<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>concentrados<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>limpios<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p><br>Una mezcla se <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>&nbsp; para obtener compuestos <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<p>Completa la oración</p>"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Homogéneas y heterogéneas \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E Heterogéneas y sintéticas \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Homogéneas y variables \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Sintéticas y variables \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué tipo de mezclas existen?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E Separar las mezclas \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E Conocer las propiedades de un material\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E Experimentar con materiales\u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Para qué sirven los instrumentos de medición en la química?"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "<p>Menciona 3 propiedades del agua</p>"
        }
    }
];
