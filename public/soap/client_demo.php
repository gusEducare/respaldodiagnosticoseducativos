<?php

include('../../config/autoload/global.php');
require_once('lib/nusoap.php');

// Connect to SOAP server
$soap_url = 'http://evaluaciones.local.com/soap/serverEvaluaciones.php';
$soap_client = new nusoap_client($soap_url);
$soap_client->soap_defencoding = 'UTF-8';
$error = $soap_client->getError();
error_log($error);


//$result = $soap_client->call('consultaUsuario',array('correo'=>'constructor'));
$result = $soap_client->call('getIdUser',array('correo'=>'gtest'));

if ($soap_client->fault) error($result);
$error = $soap_client->getError();
if ($error) error($error);

// Print result
print '<h2>Result</h2><pre>';
print_r($result);
print '</pre>';


/***** HELPER METHODS *****/


function error($message) {
	print '<h2>Error</h2><pre>';
	print_r($message);
	print '</pre>';
	exit;
}
