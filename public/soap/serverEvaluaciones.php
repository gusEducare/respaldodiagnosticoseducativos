<?php

$arrDB= include(__DIR__.'/../../config/autoload/database.local.php');
require_once('lib/nusoap.php');

$dbConexion = db_connect($arrDB);


/**
     * Recibe array de datos y lo registra en el sitio con el perfíl de profesor
     * 
     * @param array  $arrDatosProfesor datos del registro exportado de la tienda etodo
     * @return mixed $_arrRespuesta
     */
    function registraProfesor($arrDatosProfesor) {
        
        global $dbConexion;
        
        //if($this->permitirAccesoIP()){

        $_strQueryInsertarUsuario = 'INSERT INTO t01usuario(
                        t01nombre,t01apellidos,t01correo,t01contrasena,t01bloqueada,
                        t01tiempo_bloqueo,t01fecha_registro,t01fecha_actualiza,t01demo,
                        t01acepto_condiciones,t01provider_id,t01provider,t01estatus)
                        VALUES(:t01nombre,:t01apellidos,:t01correo,:t01contrasena,:t01bloqueada,:t01tiempo_bloqueo,
                        now(),now(),:t01demo,:t01acepto_condiciones,:t01provider_id,
                        :t01provider,"Activo");';
        
            $instertaProfesor = $dbConexion->prepare($_strQueryInsertarUsuario);
        try{
            $dbConexion->beginTransaction();
            
            $instertaProfesor->bindParam(':t01nombre', $arrDatosProfesor['t33nombre'], PDO::PARAM_STR);
            $instertaProfesor->bindParam(':t01apellidos', $arrDatosProfesor['t33apellido'], PDO::PARAM_STR);
            $instertaProfesor->bindParam(':t01correo', $arrDatosProfesor['t33correo'], PDO::PARAM_STR);
            $instertaProfesor->bindParam(':t01contrasena',$arrDatosProfesor['t33contrasena'], PDO::PARAM_STR);
            $instertaProfesor->bindParam(':t01bloqueada', 0, PDO::PARAM_INT);
            $instertaProfesor->bindParam(':t01tiempo_bloqueo', '0000-00-00 00:00:00', PDO::PARAM_STR);
            $instertaProfesor->bindParam(':t01demo', 0, PDO::PARAM_INT);
            $instertaProfesor->bindParam(':t01acepto_condiciones', 0, PDO::PARAM_INT);
            $instertaProfesor->bindParam(':t01provider_id', '', PDO::PARAM_STR);
            $instertaProfesor->bindParam(':t01provider', 'TODO', PDO::PARAM_STR);

            $instertaProfesor->execute();
                
            $idProfesor = $dbConexion->lastInsertId();
        
            
            
        } catch (Exception $e) {
            error_log('Catch exception');
            error_log(print_r($e->getMessage(), true));
            $dbConexion->rollBack();
            return array('boolean' => false, 'msj' => $e->getMessage());
        }
        
        //Insertar usuario perfil
        $idUsuarioPerfil = $this->insertarUsuarioPerfil($idProfesor , $this->config['constantes']['general']['PERFIL_PROFESOR_CERT']);
        if(!$idUsuarioPerfil){
            $dbConexion->rollBack();        
        }
        $_arrDatosUsuario = array('boolean' => true,'idUsuarioPerfilCert' => $idUsuarioPerfil);
        return $_arrDatosUsuario;
//        $dbConexion->rollBack();
        $dbConexion->commit();
        //$this->conection->commit();
        //}
        //die();
    }
    
    /**
     * Recibe array de datos de un alumno, grupo al que va a ser registrado y código de examenes.
     * 
     * @param array  $arrDatosProfesor datos del registro exportado de la tienda etodo
     * @param int  $idPerfil id de perfil de alumno
     * @param array  $_arrIdExamen array de exámenes que serán activados
     * @param string  $llave llave que se activó
     * @param int  $_intDias días diración licencia
     * @return array $_arrRespuesta
     */
    function updateAlumno($arrDatosAlumno,$idPerfil,$arrExamen,$intDias,$idColegioPortal,$idGradoEscolaridad){
        global $dbConexion;
//        
        $idUser = getIdUser($arrDatosAlumno['usr']);
        
        if($idUser==0){
            
            $respuesta = registrarAlumno($arrDatosAlumno, $idPerfil, $arrExamen, $intDias,$idColegioPortal,$idGradoEscolaridad);
            
        return $respuesta;
            
        }else{
            
            $_strQuery = "UPDATE t01usuario set t01nombre=:nombre , t01apellidos=:apellidos, t01contrasena=:contrasenia where t01correo=:correo;";
            
            $contrasena = sha1($arrDatosAlumno['passwordDesencriptado']);

            $update = $dbConexion->prepare($_strQuery);
            $update->bindParam(':nombre', $arrDatosAlumno['nombre'], PDO::PARAM_STR);
            $update->bindParam(':apellidos', $arrDatosAlumno['apellido'], PDO::PARAM_STR);
            $update->bindParam(':contrasenia', $contrasena, PDO::PARAM_STR);
            $update->bindParam(':correo', $arrDatosAlumno['usr'], PDO::PARAM_STR);
            
            $update->execute();
            
            $idUserProfile = getIdUsrPerfil($idUser);
            
            //Registra examenes
            foreach ($arrExamen as $indice => $examen){
                $idHijoEcommerce = $arrDatosAlumno['id'];
                $llave = generarLicencia($idHijoEcommerce);
                $idExamenUsuario = insertarExamenesUsuario($idUserProfile, $examen['ID'], $llave, $intDias);
            }
                        
             $queryExam = 'SELECT t12id_examen,t12nombre FROM t12examen
                            WHERE t36id_todo_grado_escolaridad = :idGradoEscolaridad;';
        
                $queryUsrExam = $dbConexion->prepare($queryExam);
                $queryUsrExam->bindParam(":idGradoEscolaridad", $idGradoEscolaridad, PDO::PARAM_INT);

                $queryUsrExam->execute();

                $arrExamenTODO = array();
                while ($row = $queryUsrExam->fetch(PDO::FETCH_ASSOC)) {
                    $arrExamenTODO[] = $row;
                }
           
             //Registra examenes
            foreach ($arrExamenTODO as $indice => $examen){
                $idHijoEcommerce = $arrDatosAlumno['id'];
                $llave = generarLicencia($idHijoEcommerce);
                $idExamenUsuario = insertarExamenesUsuario($idUserProfile, $examen['t12id_examen'], $llave, $intDias);
            }
        $_arrDatosUsuarioA = array('boolean' => true,'idExamenUsuario' => $idExamenUsuario,'actualizado'=>'Usuario Actualizado','idUsr'=>$idHijoEcommerce,'idUsrPer'=>$idUserProfile);
        return $_arrDatosUsuarioA;
        }
        
    }
    
    function registrarAlumno($arrDatosAlumno,$idPerfil,$arrExamen,$intDias,$idColegioPortal,$idGradoEscolaridad){
        global $dbConexion;
        
        $_strQueryInsertarUsuario = 'INSERT INTO t01usuario(
                        t01nombre,t01apellidos,t01correo,t01contrasena,t01bloqueada,
                        t01tiempo_bloqueo,t01fecha_registro,t01fecha_actualiza,t01demo,
                        t01acepto_condiciones,t01provider_id,t01provider,t01estatus)
                        VALUES(:t01nombre,:t01apellidos,:t01correo,:t01contrasena,:t01bloqueada,:t01tiempo_bloqueo,
                        now(),now(),:t01demo,:t01acepto_condiciones,:t01provider_id,
                        :t01provider,"Activo");';
       
        $insertarAlumno = $dbConexion->prepare($_strQueryInsertarUsuario);
        $bloqueada = 0;
        $tiempoBloqueo = '0000-00-00 00:00:00';
        $t01demo = 0;
        $idProveedor = '';
        $proveedor = 'TODO';
        $aceptoCondiciones = 0;
        $contrasena = sha1($arrDatosAlumno['passwordDesencriptado']);
        try{
            $dbConexion->beginTransaction();
            $insertarAlumno->bindParam(':t01nombre', $arrDatosAlumno['nombre'], PDO::PARAM_STR);
            $insertarAlumno->bindParam(':t01apellidos', $arrDatosAlumno['apellido'], PDO::PARAM_STR);
            $insertarAlumno->bindParam(':t01correo', $arrDatosAlumno['usr'], PDO::PARAM_STR);
            $insertarAlumno->bindParam(':t01contrasena', $contrasena, PDO::PARAM_STR);
            $insertarAlumno->bindParam(':t01bloqueada', $bloqueada, PDO::PARAM_INT);
            $insertarAlumno->bindParam(':t01tiempo_bloqueo',$tiempoBloqueo, PDO::PARAM_STR);
            $insertarAlumno->bindParam(':t01demo',$t01demo, PDO::PARAM_INT);
            $insertarAlumno->bindParam(':t01acepto_condiciones', $aceptoCondiciones, PDO::PARAM_STR);
            $insertarAlumno->bindParam(':t01provider_id', $idProveedor, PDO::PARAM_STR);
            $insertarAlumno->bindParam(':t01provider', $proveedor, PDO::PARAM_STR);
            
            $insertarAlumno->execute();
            
        } catch (Exception $ex) {
            error_log('ERROR:serverNusoap->registrarAlumno');
            error_log($e->getMessage());
            $dbConexion->rollBack();
//            return array('boolean' => false, 'msj' => $e->getMessage());
            $_arrError = array('boolean' => false,'FALLO EN' => 'registrarAlumno','idUsr'=>$arrDatosAlumno['usr']);
            return $_arrError;
        }
        
        $idAlumno = $dbConexion->lastInsertId();
        
//        $idColPortal= $arrDatosAlumno['idColegioPortal'];
        
        //Insertar usuario perfil
        $idUsuarioPerfil = insertarUsuarioPerfil($idAlumno, $idPerfil,$idColegioPortal);
        if(!$idUsuarioPerfil){
            $dbConexion->rollBack();
            $_arrError = array('boolean' => false,'FALLO EN' => 'Insertar Usr Perfil','idUsr'=>$idAlumno);
            return $_arrError;
            //return false;
        }
        //Registra examenes
        foreach ($arrExamen as $indice => $examen){
            $idHijoEcommerce = $arrDatosAlumno['id'];
            $llave = generarLicencia($idHijoEcommerce);
            
            $idExamenUsuario = insertarExamenesUsuario($idUsuarioPerfil, $examen['ID'], $llave, $intDias);
        }
        
            
             $queryExam = 'SELECT t12id_examen,t12nombre FROM t12examen
                            WHERE t36id_todo_grado_escolaridad = :idGradoEscolaridad;';
        
                $queryUsrExam = $dbConexion->prepare($queryExam);
                $queryUsrExam->bindParam(":idGradoEscolaridad", $idGradoEscolaridad, PDO::PARAM_INT);

                $queryUsrExam->execute();

                $arrExamenTODO = array();
                while ($row = $queryUsrExam->fetch(PDO::FETCH_ASSOC)) {
                    $arrExamenTODO[] = $row;
                }
             //Registra examenes
            foreach ($arrExamenTODO as $exam){
                $idHijoEcommerce = $arrDatosAlumno['id'];
                $llave = generarLicencia($idHijoEcommerce);
                $idExamenUsuario = insertarExamenesUsuario($idUsuarioPerfil, $exam['t12id_examen'], $llave, $intDias);
            }
           
        $dbConexion->commit();
        
        $_arrDatosUsuario = array('boolean' => true,'idExamenUsuario' => $idExamenUsuario,'idUsr'=>$idHijoEcommerce,'idUsrPer'=>$idUsuarioPerfil,'funcion'=>'Reg Alumno','nuevo'=>'Usuario nuevo');        
            return $_arrDatosUsuario;
    }
    
     /**
     * Si el usuario ya existe, sólo registra los exámenes correspondientes
     * @param int $idUsuario
     * @param array $_arrIdExamen
     * @param string $llave
     * @param int $_intDias
     * @return boolean $bool
     */
    
    function registrarExamenes($idUsuario,$_arrIdExamen,$llave,$_intDias) {
        global $dbConexion;
        
        $bool = true;
        
        //Consulta usuario perfil
        $_strQuery = 'select t02id_usuario_perfil from t01usuario  t01
                        join evaluaciones.t02usuario_perfil t02 on t01.t01id_usuario = t02.t01id_usuario
                        where t01.t01id_usuario = :idUsuario';
        
        $queryUsrPerfil = $dbConexion->prepare($_strQuery);
        $queryUsrPerfil->bindParam(":idUsuario", $idUsuario, PDO::PARAM_INT);
        
        $queryUsrPerfil->execute();
        
        $arrUsrPerfil = array();
	while ($row = $queryUsrPerfil->fetch(PDO::FETCH_ASSOC)) {
            $arrUsrPerfil[] = $row;
        }
        
        //Consulta si el examen ya fue activado
        $_strQuery2 = 'SELECT * FROM t03licencia_usuario 
                        where t03licencia = :llave
                        and t02id_usuario_perfil = :idUsuarioPerfil';
        
        $consultaExActivo = $dbConexion->prepare($_strQuery2);
        $consultaExActivo->bindParam(":llave", $llave, PDO::PARAM_STR);
        $consultaExActivo->bindParam(":idUsuarioPerfil", $arrUsrPerfil[0]['t02id_usuario_perfil'], PDO::PARAM_INT);
        
        $consultaExActivo->execute();
        
        $arrLic = array();
        while ($row = $consultaExActivo->fetch(PDO::FETCH_ASSOC)){
            $arrLic[]=$row;
        }
        
        if(!$arrLic){
            //Registra exámenes
            foreach ($_arrIdExamen as $indice => $intIdExamen){
                $idExamenUsuario = insertarExamenesUsuario($arrUsrPerfil[0]['t02id_usuario_perfil'], $indice, $llave, $_intDias);
            }
            if(!$idExamenUsuario){
                $bool = false;
            }
        }else{
            $bool= false;
        }
        return $bool;
    }


    function insertarUsuarioPerfil($idUsuario, $idPerfil,$idColegioPortal) {
         //Insertar usuario perfil
        
        global $dbConexion;
        
        $_strQueryInsertarUsuario = 'INSERT INTO t02usuario_perfil (c01id_perfil, t01id_usuario, t02id_colegio) 
                                     VALUES (:idUsuarioPerfil, :idProfesor, :idColegio);';
        
        $statement = $dbConexion->prepare($_strQueryInsertarUsuario);

        try {
            $statement->bindParam(':idUsuarioPerfil', $idPerfil, PDO::PARAM_INT);
            $statement->bindParam(':idProfesor',$idUsuario, PDO::PARAM_INT);
            $statement->bindParam(':idColegio',$idColegioPortal, PDO::PARAM_INT);
            $statement->execute();

        } catch (Exception $e) {
            error_log('ERROR:serverNusoap->insertarUsuarioPerfil');
            error_log($e->getMessage());
            $dbConexion->rollBack();
            return array('boolean' => false, 'msj' => $e->getMessage(), 'FATAL'=>'No se pudo insertar en t02usuarioperfil');
        }

        $idUsuarioPerfil = $dbConexion->lastInsertId();
        
        return $idUsuarioPerfil;
    }
    
    
    
    /**
    * Funcion que se encarga de insertar los registros necesarios para que 
    * el usuario pueda entrar a contestar examenes, guarda la información 
    * de la licencia, además de los examenes ligados al paquete correspondiente 
    * a la licencia 
    * 
    * @param int $idUsuarioPerfil
    * @param int $_intIdExamen
    * @param string $_strLlave
    * @param int $_intDias
    * @return mixed
    */
    function insertarExamenesUsuario($idUsuarioPerfil, $intIdExamen, $_strLlave, $_intDias) {
        global $dbConexion;
        
        //--Inserta Licencia Usuario
       $_strInsertLic = "insert into t03licencia_usuario 
                (t21id_paquete, t02id_usuario_perfil, t03licencia, t03fecha_activacion, t03dias_duracion, t03estatus)
                values(:idPaquete,:idUsuarioPerfil,:licencia,:fechaActivacion, :diasDuracion,:estatus)";
       
       $insertLicencia =$dbConexion->prepare($_strInsertLic);
       $idPaquete = 1;
       $estatus = 1;
       $fechaActivacion = date("Y-m-d H:i:s");
    
       try{
            $insertLicencia->bindParam(':idPaquete', $idPaquete, PDO::PARAM_INT);
            $insertLicencia->bindParam(':idUsuarioPerfil', $idUsuarioPerfil, PDO::PARAM_INT);
            $insertLicencia->bindParam(':licencia', $_strLlave, PDO::PARAM_STR);
            $insertLicencia->bindParam(':fechaActivacion', $fechaActivacion, PDO::PARAM_STR);
            $insertLicencia->bindParam(':diasDuracion', $_intDias, PDO::PARAM_INT);
            $insertLicencia->bindParam(':estatus', $estatus, PDO::PARAM_INT);
            
            $insertLicencia->execute();
            
       } catch (Exception $ex) {
            error_log('ERROR:serverNusoap->insertarExamenesUsuario');
            error_log($e->getMessage());
            return false;
       }
       
       $idLicenciaUsuario = $dbConexion->lastInsertId();
       
       //Inserta examen usuario 
       $_strInsertExamen = "insert into t04examen_usuario 
                (t12id_examen,t03id_licencia_usuario,t04estatus)
                values(:idexamen,:idLicenciaUsuario,:estatus)";
       
       global $dbConexion;
       
       $insertExamenUsuario = $dbConexion->prepare($_strInsertExamen);
       
       $estatusExamen = 'NOPRESENTADO';
       
       try{
           
           $insertExamenUsuario->bindParam(':idexamen', $intIdExamen, PDO::PARAM_INT);
           $insertExamenUsuario->bindParam(':idLicenciaUsuario', $idLicenciaUsuario, PDO::PARAM_INT);
           $insertExamenUsuario->bindParam(':estatus', $estatusExamen, PDO::PARAM_STR);
           
           $insertExamenUsuario->execute();
           
       } catch (Exception $ex) {
            error_log('ERROR:serverNusoap->insertarExamenesUsuario');
            error_log($e->getMessage());
            return false;
       }
       
       $idExamenUsuario = $dbConexion->lastInsertId();
       
       return $idExamenUsuario;
        
    }
    
    /**
     * Consulta si el usuario ya existe
     * @param string $strUsuario
     * @return array
     */
    function consultaUsuario($strUsuario) {
        global $dbConexion;
        
        $_strQuery = 'SELECT * FROM t01usuario where t01correo = :usuario';
        
        $query = $dbConexion->prepare($_strQuery);
        $query ->bindParam(":usuario" , $strUsuario, PDO::PARAM_STR);
        $query->execute();
        
        $arrUsrServ = array();
        
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $arrUsrServ[]= $row;
        }
        
        if (count($arrUsrServ)>0){
            return $arrUsrServ[0];
        }else{
            return false;
        }
    }
    
    function getIdUser($usrAlumno) {
        global $dbConexion;
        
        $_strQuery = 'SELECT t01id_usuario FROM t01usuario where t01correo = :usuario;';
        
        $query = $dbConexion->prepare($_strQuery);
        $query ->bindParam(":usuario" , $usrAlumno, PDO::PARAM_STR);
        $query->execute();
        
        $idUser = false;
        
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $idUser= $row['t01id_usuario'];
        }
        
        return $idUser;
    }

    /**
     * Obtiene los datos del usuario depende del parametro.
     * ESTE SERVICIO ES USADO PARA PORTAL
     * @global type $dbConexion
     * @param string $strCorreo
     * @param string $strParametro
     * @return array $arrUsrServ;
     */
    function obtieneUsuario($strDatoABuscar, $strParametro){
        global $dbConexion;
        $arrUsrServ = array();

        switch ($strParametro) {
            case 'name':
                    $strQuery = "SELECT  t01.t01provider, t01.t01id_usuario, concat(t01.t01nombre,' ',t01.t01apellidos) as nombre, t01.t01correo, t03.t03licencia
                         FROM t03licencia_usuario t03
                         LEFT JOIN t04examen_usuario t04 on t03.t03id_licencia_usuario = t04.t03id_licencia_usuario
                         LEFT JOIN t02usuario_perfil t02 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil
                         LEFT JOIN t01usuario t01  on t01.t01id_usuario = t02.t01id_usuario
                         LEFT JOIN t12examen t12  on t04.t12id_examen = t12.t12id_examen
                         WHERE concat(t01.t01nombre,' ',t01.t01apellidos) LIKE :strDatoABuscar AND t01.t01estatus = 'Activo' ORDER BY t03.t03licencia ASC;";
            break;

            case 'email': 
                    $strQuery = "SELECT  t01.t01provider, t01.t01id_usuario, concat(t01.t01nombre,' ',t01.t01apellidos) as nombre, t01.t01correo, t03.t03licencia
                                 FROM t03licencia_usuario t03
                                 LEFT JOIN t04examen_usuario t04 on t03.t03id_licencia_usuario = t04.t03id_licencia_usuario
                                 LEFT JOIN t02usuario_perfil t02 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil
                                 LEFT JOIN t01usuario t01  on t01.t01id_usuario = t02.t01id_usuario
                                 LEFT JOIN t12examen t12  on t04.t12id_examen = t12.t12id_examen
                                 WHERE t01.t01correo LIKE :strDatoABuscar AND t01.t01estatus = 'Activo' ORDER BY t03.t03licencia ASC;";
            break;

            case 'licencia': 
                    $strQuery = "SELECT  t01.t01provider, t01.t01id_usuario, concat(t01.t01nombre,' ',t01.t01apellidos) as nombre, t01.t01correo, t03.t03licencia
                                 FROM t03licencia_usuario t03
                                 LEFT JOIN t04examen_usuario t04 on t03.t03id_licencia_usuario = t04.t03id_licencia_usuario
                                 LEFT JOIN t02usuario_perfil t02 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil
                                 LEFT JOIN t01usuario t01  on t01.t01id_usuario = t02.t01id_usuario
                                 LEFT JOIN t12examen t12  on t04.t12id_examen = t12.t12id_examen
                                 WHERE t03.t03licencia LIKE :strDatoABuscar AND t01.t01estatus = 'Activo' ORDER BY t03.t03licencia ASC;";
            break;
        
            default:
            break;
        }

        try{
            $strDatoABuscar = '%'.$strDatoABuscar.'%';    
            $query = $dbConexion->prepare($strQuery);
            $query->bindParam(":strDatoABuscar", $strDatoABuscar, PDO::PARAM_STR);
            $query->execute();
        }catch (PDOException $e){
            return $arrUsrServ;
        }
        
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $arrUsrServ[]= $row;
        }
        
        return $arrUsrServ;
    }

    
    /**
     * Obtiene los datos del usuario por el id.
     * ESTE SERVICIO ES USADO PARA PORTAL
     * @param int $intIdUsuario
     * @return array $arrUsrServ;
     */
    function obtieneUsuarioId($intIdUsuario){
        global $dbConexion;
        $arrUsrServ = array();
        
        $strQuery = "SELECT  t01.t01provider, t01.t01id_usuario, concat(t01.t01nombre,' ',t01.t01apellidos) as nombre, 
                             t01.t01correo, t03.t03licencia , t03.t03fecha_activacion,
                         DATE_ADD(t03.t03fecha_activacion, INTERVAL t03.t03dias_duracion DAY) as vigencia, t02.c01id_perfil,
                         t12.t12nombre
                    FROM t03licencia_usuario t03
                    LEFT JOIN t04examen_usuario t04 on t03.t03id_licencia_usuario = t04.t03id_licencia_usuario
                    LEFT JOIN t02usuario_perfil t02 on t03.t02id_usuario_perfil = t02.t02id_usuario_perfil
                    LEFT JOIN t01usuario t01  on t01.t01id_usuario = t02.t01id_usuario
                    LEFT JOIN t12examen t12  on t04.t12id_examen = t12.t12id_examen
                    WHERE t01.t01id_usuario = :intIdUsuario;";
      
        try{
            $query = $dbConexion->prepare($strQuery);
            $query->bindParam(":intIdUsuario", $intIdUsuario, PDO::PARAM_INT);
            $query->execute();
        }catch (PDOException $e){
            return $arrUsrServ;
        }
        
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $arrUsrServ[]= $row;
        }
        
        return $arrUsrServ;
    }

    /**
     * Función para modificar la contraseña del usuario.
     * ESTE SERVICIO ES USADO PARA PORTAL
     * @param int $intIdUsuario
     * @param string $strNewPass 
     * @return int 1 exitoso 0 fallido
     */
    function cambiarContrasena($intIdUsuario,$strNewPass){
        global $dbConexion;
        
        $strContraseña = sha1($strNewPass);
        $_strQuery = "UPDATE t01usuario SET t01contrasena = :contrasena WHERE t01id_usuario = :intIdUsuario;";
    
        try{
            $update = $dbConexion->prepare($_strQuery);
            $update->bindParam(':contrasena', $strContraseña, PDO::PARAM_STR);
            $update->bindParam(':intIdUsuario', $intIdUsuario, PDO::PARAM_INT);

            $update->execute();            

            return 1;
        }catch(PDOException $e) {
            return 0;
        }
    }

    /**
     *  Funcion para verificar si existe o no el usuario.
     * ESTE SERVICIO ES USADO PARA PORTAL
     * @param string $strNombreUsuario
     * @return int 1 exitoso 0 fallido  
     */
    function existeUsuario($strNombreUsuario){
        global $dbConexion;
        
        $arrCorreo = array();

        $strQuery = "SELECT t01correo FROM t01usuario WHERE t01correo = :strCorreo;";
      
        try{
            $query = $dbConexion->prepare($strQuery);
            $query->bindParam(":strCorreo", $strNombreUsuario, PDO::PARAM_STR);
            $query->execute();
        }catch (PDOException $e){
            return false;
        }
        
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $arrCorreo[]= $row;
        }
        
        if (count($arrCorreo) > 0) {
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * Funcion para cambiar el nombre de usuario / correo.
     * ESTE SERVICIO ES USADO PARA PORTAL 
     * @param string $strNombreUsuario
     * @param int $intIdUsuario
     * @return int 1 exitoso 0 fallido
     */
    function cambiarNombreUsuario($strNombreUsuario,$intIdUsuario){
        global $dbConexion;
        
        $_strQuery = "UPDATE t01usuario SET t01correo = :strCorreo WHERE t01id_usuario = :intIdUsuario;";
        
        try{
            $update = $dbConexion->prepare($_strQuery);
            $update->bindParam(':strCorreo', $strNombreUsuario, PDO::PARAM_STR);
            $update->bindParam(':intIdUsuario', $intIdUsuario, PDO::PARAM_INT);
            
            $update->execute();            
            
            return 1;
        }catch(PDOException $e) {
            return 0;
        }
    }

    /**
     * Funcion para cambiar el nombre y apellidos del usuario(HIJO).
     * ESTE SERVICIO ES USADO PARA PORTAL 
     * @param string $strNombreUsuario
     * @param int $intIdUsuario
     * @return int 1 exitoso 0 fallido
     */
    function cambiarNombre($strNombre,$strApellido,$intIdUsuario){
        global $dbConexion;
        
        $_strQuery = "UPDATE t01usuario SET t01nombre = :strNombre, t01apellidos = :strApellidos WHERE t01id_usuario = :intIdUsuario;";
        
        try{
            $update = $dbConexion->prepare($_strQuery);
            $update->bindParam(':strNombre', $strNombre, PDO::PARAM_STR);
            $update->bindParam(':strApellidos', $strApellido, PDO::PARAM_STR);
            $update->bindParam(':intIdUsuario', $intIdUsuario, PDO::PARAM_INT);
            
            $update->execute();            
            
            return 1;
        }catch(PDOException $e) {
            return 0;
        }
    }

    /**
     * Función para cambiar datos del hijo dependiendo del parametro recibido.
     * @param string $strParametro NAME / EMAIL / PASSWORD
     * @param string $strCorreo nombre del usuario para poder cambiarlo
     * @param string $strDatoACambiar 
     * @param string $strDatoACambiarDos recibe el apellido solo cuando se cambia nombre.
     * @return int 1 exitoso 0 fallido 10 usuario no encontrado 14 email registrado en la bd 11 contrasena es menor a 6 caracteres
     */
    function cambiarDatosHijo($strParametro,$strCorreo,$strDatoACambiar,$strDatoACambiarDos){
        $intIdHijo = getIdUser($strCorreo);

        if (is_null($intIdHijo) || $intIdHijo == "") return 10;
        switch ($strParametro) {
            case 'NAME':
                $intUpdate = cambiarNombre($strDatoACambiar,$strDatoACambiarDos,$intIdHijo);
                break;
            case 'EMAIL':
                if(existeUsuario($strDatoACambiar) == 1) return 14;
                $intUpdate = cambiarNombreUsuario($strDatoACambiar,$intIdHijo);
                break;
            case 'PASSWORD':
                if(validaContrasena($strDatoACambiar) == false) return 11;
                $intUpdate = cambiarContrasena($intIdHijo,$strDatoACambiar);
                break;
            default:
                break;
        }
        return $intUpdate;
    }

    $server = new soap_server();
    $server->register('registraProfesor');
    $server->register('registrarAlumno');
    $server->register('registrarExamenes');
    $server->register('insertarUsuarioPerfil');
    $server->register('insertarExamenesUsuario');
    $server->register('consultaUsuario');
    $server->register('getIdUser');
    $server->register('updateAlumno');
    $server->register('obtieneUsuario');
    $server->register('obtieneUsuarioId');
    $server->register('cambiarContrasena');
    $server->register('existeUsuario');
    $server->register('cambiarNombreUsuario');
    $server->register('cambiarDatosHijo');
    $server->register('cambiarNombre');
    $server->service(file_get_contents("php://input"));
    
    /*Conexión a la base de datos*/
function db_connect($arrDB){
    
    $DB_HOST = $arrDB['db']['hostname'];
    $DB_SCHEMA = $arrDB['db']['database'];
    $DB_USER = $arrDB['db']['username'];
    $DB_PASSWORD = $arrDB['db']['password'];
    
    try{
        $dbh = new PDO('mysql:host='.$DB_HOST.';dbname='.$DB_SCHEMA,$DB_USER,$DB_PASSWORD);
        return $dbh;
        
    } catch (Exception $ex) {
        echo 'ERROR: ' . $ex->getMessage();
    }
}
    
    function generarLicencia($idHijoEcommerce){
        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
        $texto =$idHijoEcommerce.'-';
        $numero = strlen($texto);
        $resta= 10-$numero;
        $numerodeletras=$resta; //numero de letras para generar el texto
        $cadena = ""; //variable para almacenar la cadena generada
        for($i=0;$i<$numerodeletras;$i++)
        {
            $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); /*Extraemos 1 caracter de los caracteres 
        entre el rango 0 a Numero de letras que tiene la cadena */
        }
        $llave = $texto.$cadena;
        return $llave;
    }
    
    function getIdUsrPerfil($idUser){
        global $dbConexion;
        
        $_strQuery = 'SELECT t02id_usuario_perfil FROM t02usuario_perfil where t01id_usuario = :idUser';
        
        $query = $dbConexion->prepare($_strQuery);
        $query ->bindParam(":idUser" , $idUser, PDO::PARAM_STR);
        $query->execute();
        
        $idUserProfile = false;
        
        while($row = $query->fetch(PDO::FETCH_ASSOC)){
            $idUserProfile= $row['t02id_usuario_perfil'];
        }
        
        return $idUserProfile;
    }

    /**
     * Valida si el correo es mayor a 6 caracteres.
     * @param string $strContrasena
     * @return boolean
     */
    function validaContrasena($strContrasena) {
        $intMinLength = 6;
        return (strlen($strContrasena) >= $intMinLength);
    }
    
?>