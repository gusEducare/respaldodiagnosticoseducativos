<?php 

    echo 'date.timezone: ' . ini_get('date.timezone');
    echo '<br/><br/><br/>';
    echo "Today is " . date("Y/m/d") . "<br>";
    echo "Today is " . date("Y.m.d") . "<br>";
    echo "Today is " . date("Y-m-d") . "<br>";
    echo "Today is " . date("l");
    echo '<br/><br/><br/>';
    echo "The time is " . date("h:i:sa");

?>