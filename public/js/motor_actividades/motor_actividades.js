var calificacion = "0";
// var cronometroExamen = "0";

$(document).ready(function(){	
    $('#bg_header').hide();
    //fuynciones adiocionales --reacomoda elementos
    $("#header2").append("<nav class='menu-nav'></nav>");//contenedor para los menus
    $("#time-left").appendTo("#header2");//timer
    $("#ifo-examen").appendTo("nav[tag='info-nav']");//titulo examen
    $("nav[tag='info-nav'] li>a").appendTo("nav.menu-nav");//menu datos/examenes
    $(".logout").appendTo("nav.menu-nav");//menu salir
    $("#button-finalizar").appendTo("nav[tag='info-nav']");//boton finalizar
    $(".contenedor-menu-nav.temp>*").appendTo("nav[tag='info-nav']");//iconos info/instrucciones
    $("a:has(span:contains('Menu'))").remove();
    $("#bar-remove, #menu-item").remove();
    $("#header2 p:contains('Bienvenido: ')").remove();
    $(".contenedor-menu-nav.temp, .spacer60.row").remove();
    $("#infoPregunta").remove();//quita el boton de info (innecesario para esta vista)

    var frame = $("#frameEvaluacion");
    getPMessage();
    frame.on("load", function(){
        frame[0].contentWindow.postMessage( pMessage ,"*");
    });
    frame[0].contentWindow.postMessage( pMessage ,"*");

    $("*").on("finalizarExamen", function(){        
        $(".calificacion_numero").html(parseInt(calificacion));
        $('#ir-examenes').foundation('reveal', 'open');
        localStorage.clear();
        return false;
    });
    
//    $("#infoPregunta").on("click", function(){//el contenido aparece en un modal dentro del iframe
//        $(".instrucciones_pregunta").html(json[preguntaActual].pregunta.t11instruccion);
//    });
    $('.btn_cancelar').on('click', function(){
        $('#confirma-finalizar').foundation('reveal', 'close');
    });
    
    $('.btn_evaluar').on('click', function(){
        $('#segundocandado').foundation('reveal', 'open');
    }); 

    $('#btnAceptar').on('click', function(){
        finalizarExamen(calificacion); 
    });    
    
});



function getPMessage(){
    $.ajax({
        type: 'POST',
        async: false,
        dataType: 'json',
        url: $('#urlGetPMessage').val(),
        data: {
            'idLicenciaUsr': $('#idLicenciaUsr').val()
        },
        beforeSend: function () {

        },
        success: function (pMessage) {
//            console.log(pMessage);
            window.pMessage = pMessage;
        },
        error: function (json) {
            console.log("Hubo un error al procesar los datos: " + json);
        }
    });   
}

function setPMessage(objMeta){
//    console.log($('#idLicenciaUsr').val())
    objMeta.calificacion = calificacion;
    $.ajax({
        type: 'POST',
        async: false,
        dataType: 'json',
        url: $('#urlSetPMessage').val(),
        data: {
            'idLicenciaUsr': $('#idLicenciaUsr').val(),
            'objMeta': objMeta,
        },
        beforeSend: function () {

        },
        success: function (pMessage) {
            if(pMessage){
//                console.log(pMessage);
                return pMessage;
            }else{
                return false;
            }
        },
        error: function (json) {
            console.log("Hubo un error al procesar los datos: " + json);
        }
    });   
}

function finalizarExamen(metaCal) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        async: false,
        url: $('#urlFinalizaExamen').val(),
        data: {
            'idLicenciaUsr': $('#idLicenciaUsr').val(),
            'metaCal': metaCal,
        },
        beforeSend: function () {
//            console.log(metaCal);
        },
        success: function (json) {
            if (json.bool) {
//                console.log(json);
                $('.calificacion_numero').text(json.calif); 
                $('#calificacionReveal').foundation('reveal','open'); 
            } else {
            }
        },
        error: function (json) {
            console.log("Hubo un error al procesar los datos: " + json);
        }
    });
}
//esta funcion se manda llamar desde el iframe de la evaluacion (funciones eval)
//actualiza la barra de progreso, si esta se llena se habilita el boton finalizar
function updateProgress(val, max, range){
    if(val === max){
        $("#button-finalizar").removeClass("progress");
    }else{
        $("#button-finalizar").addClass("progress");
        var currentProgress = val * range;
        $("#button-finalizar>hr").css("width", currentProgress+"%");
    }
}