var boolPrimeraPeticion = true;
var chartGlobal = null; 
var chartVerbal = null;
var chartMatematico = null;

$(document).ready(function(){
	
	$(document).foundation();
	
	$(".chkExamen").change(function(){
            
            if($(this).is(":checked")){
                var idGrupo = $(this).prop("id");
                idGrupo = idGrupo.split("-");
		getData(idGrupo[1], $(this).val());
            }
            else{
                //remove data 
                var idGrupo = $(this).prop("id");
                idGrupo = idGrupo.split("-");
                var _seriesGobalName = $('#globalDataJson-'+idGrupo[1]+'-'+$(this).val()).val();
                $('#globalDataJson-'+idGrupo[1]+'-'+$(this).val()).remove();
                var _seriesVerbalName = $('#verbalDataJson-'+idGrupo[1]+'-'+$(this).val()).val();
                $('#verbalDataJson-'+idGrupo[1]+'-'+$(this).val()).remove();
                var _seriesMatematicoName = $('#matematicoDataJson-'+idGrupo[1]+'-'+$(this).val()).val();
                $('#matematicoDataJson-'+idGrupo[1]+'-'+$(this).val()).remove();
                
                var seriesLength = chartGlobal.series.length;
                //console.log(_seriesGobalName);
                for(var i = seriesLength - 1; i > -1; i--){
                    //console.log(chartGlobal.series[i].name);
                    if(chartGlobal.series[i].name == _seriesGobalName){
                        chartGlobal.series[i].remove();
                    }
                }
                
                seriesLength = chartVerbal.series.length;
                for(var i = seriesLength - 1; i > -1; i--){
                    if(chartVerbal.series[i].name == _seriesVerbalName){
                        chartVerbal.series[i].remove();
                    }
                }
                
                seriesLength = chartMatematico.series.length;
                for(var i = seriesLength - 1; i > -1; i--){
                    if(chartMatematico.series[i].name == _seriesMatematicoName){
                        chartMatematico.series[i].remove();
                    }
                }
            }
		
	});
});



function getData(idGrupo, idExamen){
	
	var _arrDataGrupo = new Array();
	$(".chkExamen").each(function(){
		if($(this).is(":checked")){
			
		}
	});
	
	//console.log(_arrData1);
	
	//obtener datos json
	if(idGrupo.length > 0 && idExamen.length > 0) {
	
            $.ajax({
                    type:'POST',
                    dataType:'json',
                    url:  $("#urlGetData").val(),
                    data:{ 
                        "idGrupo" : idGrupo,
                        "idExamen" : idExamen,
                        'boolPrimeraPeticion' : boolPrimeraPeticion
                    },
                    beforeSend: function() {
                            
                            $('#lsitExamenesContainer').block({
                                message: '<h5 ><i class="fa fa-spinner fa-spin fa-2x"></i>Procesando...</h5>'
                            });
                    },
                    success:function(json){
                            //$("#graficaGlobal").html("Success   "+ json);
                            if(boolPrimeraPeticion === true){
                                generateChart(json);
                                
                                $('<input>').attr({
                                    type: 'hidden',
                                    id: 'globalDataJson-'+idGrupo+'-'+idExamen,
                                    name: 'globalDataJson-'+idGrupo+'-'+idExamen,
                                    value: json['global']['data'][0]['name'],
                                    class: 'dataGlobal'
                                }).appendTo('#dataInputs');
                                $('<input>').attr({
                                    type: 'hidden',
                                    id: 'verbalDataJson-'+idGrupo+'-'+idExamen,
                                    name: 'verbalDataJson-'+idGrupo+'-'+idExamen,
                                    value: json['verbal']['data'][0]['name'],
                                    class: 'dataVerbal'
                                }).appendTo('#dataInputs');
                                $('<input>').attr({
                                    type: 'hidden',
                                    id: 'matematicoDataJson-'+idGrupo+'-'+idExamen,
                                    name: 'matematicoDataJson-'+idGrupo+'-'+idExamen,
                                    value: json['matematico']['data'][0]['name'],
                                    class: 'dataMatematico'
                                }).appendTo('#dataInputs');
                            }
                            else{
                                var result = json;
                                $('<input>').attr({
                                    type: 'hidden',
                                    id: 'globalDataJson-'+idGrupo+'-'+idExamen,
                                    name: 'globalDataJson-'+idGrupo+'-'+idExamen,
                                    value: result.global.name,
                                    class: 'dataGlobal'
                                }).appendTo('#dataInputs');
                                
    				chartGlobal.addSeries(result.global);
    				chartGlobal.redraw();
                                $('<input>').attr({
                                    type: 'hidden',
                                    id: 'verbalDataJson-'+idGrupo+'-'+idExamen,
                                    name: 'verbalDataJson-'+idGrupo+'-'+idExamen,
                                    value: result.verbal.name,
                                    class: 'dataVerbal'
                                }).appendTo('#dataInputs');
    				chartVerbal.addSeries(result.verbal);
    				chartVerbal.redraw();
                                $('<input>').attr({
                                    type: 'hidden',
                                    id: 'matematicoDataJson-'+idGrupo+'-'+idExamen,
                                    name: 'matematicoDataJson-'+idGrupo+'-'+idExamen,
                                    value: result.matematico.name,
                                    class: 'dataMatematico'
                                }).appendTo('#dataInputs');
    				chartMatematico.addSeries(result.matematico);
    				chartMatematico.redraw();
                            }
                            boolPrimeraPeticion = false;
                            
                            $('#lsitExamenesContainer').unblock();

                    },
                    error: function(json){
                        $('#lsitExamenesContainer').unblock();
                        alert('Ocurrió un error al procesar tu solicitud, por favor intenta nuevamente.');
                    }
            });
	}
	else{
            alert("Selecciona una opción para generar las graficas.");
            
	}
	
	
	
}
function generateChart(json){
    
    
            
	var arrNiveles = ["", "Básico", "Intermedio", "Avanzado","Avanzado"];
	
        
        chartGlobal = new Highcharts.Chart({

        //data: json['global']['data'],
	chart: {
            type: 'column',
            renderTo: 'graficaGlobal'
        },
        credits: {
            enabled: false
        },
        title: {
            text:'Resultado global' //json['global']['title']
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: json['global']['xAxis']
        },
        yAxis: { 
        	title: {
                text: 'Nivel obtenido'
            },
        	plotBands: [{ 
                from: 0,
                to: 1,
                color: 'rgba(253, 253, 150, 0.2)'
            },
            { 
                from: 1,
                to: 2,
                color: 'rgba(119, 221, 119, 0.2)'
            },
            { 
                from: 2,
                to: 3,
                color: 'rgba(119, 158, 203, 0.2)'
            }], 
            
            labels: {
                formatter: function() {
                    return arrNiveles[this.value];
                }
            }
            
        },
        tooltip: {
        	
            useHTML: true,
        	formatter: function () {
                
                var message  = "";
                //arrNiveles[this.y];
                
                message = '<span style="font-size:10px">'+this.key+'</span>'+
                '<div style="color:'+this.point.series.color+';padding:0">'+this.point.series.name+' Nivel Alcanzado: ' +
                '<b>&nbsp;'+arrNiveles[this.y]+' </b></div>';
                return message;
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:  json['global']['data']
    });
	
	
        chartVerbal = new Highcharts.Chart({

        //data: json['global']['data'],
        chart: {
            type: 'column',
            renderTo: 'graficaVerbal'
            
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Resultado razonamiento verbal' //json['verbal']['title']
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: json['verbal']['xAxis']
        },
        yAxis: { 
        	title: {
                text: 'Nivel obtenido'
            },
        	plotBands: [{ 
                from: 0,
                to: 1,
                color: 'rgba(253, 253, 150, 0.2)'
            },
            { 
                from: 1,
                to: 2,
                color: 'rgba(119, 221, 119, 0.2)'
            },
            { 
                from: 2,
                to: 4,
                color: 'rgba(119, 158, 203, 0.2)'
            }
            ], 
            
            labels: {
                formatter: function() {
                    return arrNiveles[this.value];
                }
            }
            
        },
        tooltip: {
        	
            useHTML: true,
        	formatter: function () {
                
                var message  = "";
                //arrNiveles[this.y];
                
                message = '<span style="font-size:10px">'+this.key+'</span>'+
                '<div style="color:'+this.point.series.color+';padding:0">'+this.point.series.name+' Nivel Alcanzado: ' +
                '<b>&nbsp;'+arrNiveles[this.y]+' </b></div>';
                return message;
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:  json['verbal']['data']
    });
	
	chartMatematico = new Highcharts.Chart({

        //data: json['global']['data'],
        chart: {
            type: 'column',
            renderTo: 'graficaMatematico'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Resultado razonamiento matemático'//json['matematico']['title']
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: json['matematico']['xAxis']
        },
        yAxis: { 
        	title: {
                text: 'Nivel obtenido'
            },
        	plotBands: [{ 
                from: 0,
                to: 1,
                color: 'rgba(253, 253, 150, 0.2)'
            },
            { 
                from: 1,
                to: 2,
                color: 'rgba(119, 221, 119, 0.2)'
            },
            { 
                from: 2,
                to: 4,
                color: 'rgba(119, 158, 203, 0.2)'
            }
            ], 
            
            labels: {
                formatter: function() {
                    return arrNiveles[this.value];
                }
            }
            
        },
        tooltip: {
        	
            useHTML: true,
        	formatter: function () {
                
                var message  = "";
                //arrNiveles[this.y];
                
                message = '<span style="font-size:10px">'+this.key+'</span>'+
                '<div style="color:'+this.point.series.color+';padding:0">'+this.point.series.name+' Nivel Alcanzado: ' +
                '<b>&nbsp;'+arrNiveles[this.y]+' </b></div>';
                return message;
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series:  json['matematico']['data']
    });
}


