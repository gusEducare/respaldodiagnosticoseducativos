var chartGlobal = null; 
var chartVerbal = null;
var chartMatematico = null;
var chartVerbalPuntos = null;
var chartMatematicoPuntos = null;

$(document).ready(function () {
		$(document).foundation();
		
		createChartGlobal();
		createChartVerbal();
		createChartMatematico();
                
                createChartPuntajeVerbal();
                createChartPuntajeMate();
        
        
        
        
        $("#sltExamen").change(function() {
        	//alert($("#urlReporteGrupo").val());
        	window.location = $("#urlReporteGrupo").val() + "&exam="+$(this).val();
	     });   

        
        
	var arrChartsObjects = new Array();
        var arrChartsObjectsPost = new Array();
	
	var yAxisTabla = JSON.parse($("#yAxisTabla").val());
	
	var xAxisTablaVerbal = JSON.parse($("#xAxisTablaVerbal").val());
	var xAxisTablaMatematico = JSON.parse($("#xAxisTablaMatematico").val());
	var verbal_data_pre = JSON.parse($("#verbal_data_pre").val());
	//var verbal_data_post = JSON.parse($("#verbal_data_post").val());
	var matematico_data_pre = JSON.parse($("#matematico_data_pre").val());
	//var matematico_data_post = JSON.parse($("#matematico_data_post").val());
	
	//console.log(xAxisTablaMatematico);
	
	
	//console.log(verbal_data_post);
	//console.log(matematico_data_post);

        var options1 = generateChartOptions("tableAlumnosVerbalPre","Puntaje obtenido por proceso de pensamiento - Razonamiento Verbal",yAxisTabla,xAxisTablaVerbal,verbal_data_pre);
        var chart1 = new Highcharts.Chart(options1);
        arrChartsObjects.push(chart1);


        var options2 = generateChartOptions("tableAlumnosMatematicoPre","Puntaje obtenido por proceso de pensamiento - Razonamiento Matemático",yAxisTabla,xAxisTablaMatematico,matematico_data_pre);
        var chart2 = new Highcharts.Chart(options2);
        arrChartsObjects.push(chart2);
	
	
	
    function generateChartOptions(divId,title,yAxis,xAxis,data){
    	
    	var max_point = 0;
        max_point = $('#contador_preguntas_RM').val();
        if (title.indexOf("Verbal") !=-1) {
            max_point = $('#contador_preguntas_RV').val();
        }
        
    	var  _arrLinksAlumnos = JSON.parse($("#_arrLinksAlumnos").val());
    	
    	var arrNiveles = [ "Básico", "Básico", "Intermedio", "Avanzado", "Avanzado"];
    	
    	 var options ={
 	    		
 				//renderTo: div,
 	    		chart: {
 	    			renderTo: divId,
 		            type: 'heatmap',
 		            
 		        },
 		       credits: {
 		            enabled: false
 		        },

 		        title: {
 		        	text: title
 		        	
 		            
 		        },
 		       subtitle: {
 		    	  text: 'Tabla de alumnos'
 		    	},
 		        xAxis: {
 		           categories: xAxis,
 		          
 		            
 		        },

 		        yAxis: {
 		            categories: yAxis,
 		            title: null,
                            labels: {
		                formatter: function() {
                                        if(this.value.length > 15){
                                            return '<a target="_blank" href="' + _arrLinksAlumnos[this.value] + '">' +
                                            this.value.substring(0, 15)+ '...</a>';
                                        }
                                        else{
                                            return '<a target="_blank" href="' + _arrLinksAlumnos[this.value] + '">' +
                                            this.value.substring(0, 15)+ '</a>';
                                        }
		                	
		                },
		               useHTML: true
		            }

 		        },

 		        legend: {
 		        	enabled: false
 		        },
 		        colorAxis: {
                            /*
 		             stops: [
 		                     [0, '#FDFD96'],
 		                     [0.7, '#77DD77'],
 		                     [0.9, '#779ECB']
 		                 ],
 		                 min: 0, 
 		                 max: 3*/
                            dataClasses: [{
                                   from: 0,
                                   to: 2,
                                   color: '#FDFD96'
                               },{
                                   from:2,
                                   to:3,
                                   color:'#77DD77'
                               },{
                                   from:3,
                                   to:4,
                                   color:'#779ECB'
                               },{
                                   from:4,
                                   to:50,
                                   color:'#FFFFFF'


                               }]
 		            
 		        },

 		        
 		        

 		        tooltip: {
 		        	
 		            formatter: function () {
                                var legend = '';
                                
                                if(this.series.xAxis.categories[this.point.x] == 'Total'){
                                    var raz = title.split('-');
                                    if(this.series.yAxis.categories[this.point.y] == 'Total'){
                                        legend = 'Este es el <b>total</b> de aciertos obtenidos por <b>todos los alumnos del grupo</b> en <b>todos los procesos de pensamiento</b>';
                                    }
                                    else if(this.series.yAxis.categories[this.point.y] == 'Promedio'){
                                        legend = 'Este es el <b>promedio</b> del grupo en <b>'+raz[1]+'</b>';
                                    }
                                    else{
                                        
                                        legend = 'Este es el total de aciertos obtenidos por  <b>' + this.series.yAxis.categories[this.point.y] +'</b> en <b>'+raz[1]+'</b>'
                                        +'<br/><br/> Puntación máxima: <b>'+max_point +' puntos.</b>';
                                    }
                                    
                                    
                                }
                                else if(this.series.yAxis.categories[this.point.y] == 'Total'){
                                    legend = 'Este es el <b>total</b> de puntos obtenidos por los alumnos en <b>'+ this.series.xAxis.categories[this.point.x]+'</b>';
                                }
                                else if(this.series.yAxis.categories[this.point.y] == 'Promedio'){
                                    legend = 'Este es el <b>promedio</b> de puntos obtenidos por los alumnos en <b>'+ this.series.xAxis.categories[this.point.x]+'</b>';
                                }
                                else if(this.series.yAxis.categories[this.point.y] == 'MIN'){
                                    legend = 'Este es el <b>puntaje mínimo</b> de aciertos obtenidos por <b>todos los alumnos del grupo</b> en <b>'+this.series.xAxis.categories[this.point.x]+'</b>';
                                }
                                else if(this.series.yAxis.categories[this.point.y] == 'MAX'){
                                    legend = 'Este es el <b>puntaje máximo</b> de aciertos obtenidos por <b>todos los alumnos del grupo</b> en <b>'+this.series.xAxis.categories[this.point.x]+'</b>';
                                }
                                else{
                                    legend =  '<b>' + this.series.yAxis.categories[this.point.y] + '</b> obtuvo <br><b>' +
 		                    this.point.value + '</b> puntos en <b>' + this.series.xAxis.categories[this.point.x] + '</b>, alcanzando el nivel: <b>'+ arrNiveles[this.point.value] +'</b>' ;
                                }
 		                return legend;
 		            }
 		        },

 		        series: [{
 		            name: 'Aciertos Obtenidos',
 		            borderWidth: 1,
 		            data: data,
 		            dataLabels: {
 		                
 		                enabled: true,
 		                color: 'black',
 		                style: {
 		                    textShadow: 'none',
 		                    HcTextStroke: null
 		                }
 		            }
 		        }],
 		       exporting: {
 		           scale: 1,
 		           enabled: false
 		       }
 		    
 		    
 		};
    	
    	return options;
    	
    }
    
    
    $('#chkUniverso').change(function() {
    	
        if($(this).is(":checked")) {
        	$.ajax({
    			url: $('#urlDataUniverso').val(),
    			dataType: 'json',
    			type: 'POST',
    			
    			data: {
    				_idExamen: $('#sltExamen').val(),
    				
    			},
    			beforeSend: function() {
    					//mostrar imagen procesando 
    				 $('#rowOptions').block({
                                    message: '<h5 ><i class="fa fa-spinner fa-spin fa-2x"></i>Procesando...</h5>'
                                });
    			},
    			success: function(data) {
                            console.log(data.global);
                            var result = data;
                            chartGlobal.addSeries(result.global);
                            chartGlobal.redraw();
                            chartVerbal.addSeries(result.verbal);
                            chartVerbal.redraw();
                            chartMatematico.addSeries(result.matematico);
                            chartMatematico.redraw();
                            $('#rowOptions').unblock();
    			},
    			error: function(data) {
                            $('#rowOptions').unblock();
    			}
    		});	
        }
        else{
        	//redraw charts originales   
        	
        	createChartGlobal();
			createChartVerbal();
			createChartMatematico();
        }
    });
    
    
    
    $('#chkGetPrePost').change(function() {
    	
        
        if($(this).is(":checked")) {
            
        	$.ajax({
    			url: $('#urlDataAdjunto').val(),
    			dataType: 'json',
    			type: 'POST',
    			
    			data: {
    				idExamenAdjunto : $('#idExamenAdjunto').val(),
                                idGrupo : $('#idGrupo').val()
    				
    			},
    			beforeSend: function() {
    					//mostrar imagen procesando 
    				 $('#rowOptions').block({
                                    message: '<h5 ><i class="fa fa-spinner fa-spin fa-2x"></i>Procesando...</h5>'
                                });
    			},
    			success: function(data) {
                          
                           var result = data;
                           chartGlobal.addSeries(result.global.data[0]);
                           chartVerbal.addSeries(result.verbal.data[0]);
                           chartVerbalPuntos.addSeries(result.verbal.puntajes[0]);
                           chartMatematico.addSeries(result.matematico.data[0]);
                           chartMatematicoPuntos.addSeries(result.matematico.puntajes[0]);
                           //create table alumnos
                           console.log(result.alumnos_table.yAxis.categories);
                            var heightForTables = $('#heightTables').val();
                                $('#tableAlumnosVerbalPost').height(heightForTables);
                           var options1 = generateChartOptions("tableAlumnosVerbalPost",
                                                                "Puntaje obtenido por proceso de pensamiento - Razonamiento Verbal - "+$('#strExamenPrePost').val(),
                                                                 result.alumnos_table.yAxis.categories,
                                                                 result.alumnos_table.xAxis.categories_verbal,
                                                                 result.alumnos_table.verbal_data);
                            var chart1 = new Highcharts.Chart(options1);
                            arrChartsObjectsPost.push(chart1);

                                $('#tableAlumnosMatematicoPost').height(heightForTables);
                            var options2 = generateChartOptions("tableAlumnosMatematicoPost","Puntaje obtenido por proceso de pensamiento - Razonamiento Matemático - "+$('#strExamenPrePost').val(),
                             result.alumnos_table.yAxis.categories,
                             result.alumnos_table.xAxis.categories_matematico,
                             result.alumnos_table.matematico_data);
                            var chart2 = new Highcharts.Chart(options2);
                            arrChartsObjectsPost.push(chart2);
                           
                           
                           $('#rowOptions').unblock();
    			},
    			error: function(data) {
                            $('#rowOptions').unblock();
    			}
    		});	
        }
        else{
        	window.location.reload();
        }
    });
        
    
	


		function createChartGlobal(){
			
			//var arrNiveles = ["", "Básico", "Intermedio", "Avanzado", "Avanzado"];
			var xAxisGlobal = JSON.parse($("#xAxisGlobal").val());
			var data_chartGlobal = JSON.parse($("#data_chartGlobal").val());
			var title_global = $("#title_global").val();
			var subtitle_global = $("#sub_title_global").val();
			var options = generateChartOptionsBar("chart_global", title_global,subtitle_global,xAxisGlobal,data_chartGlobal);
			chartGlobal = new Highcharts.Chart(options);
		}
		
		function createChartVerbal(){
			
			var titleRV = $("#titleRV").val();
			var subtitle_RV = $("#sub_title_RV").val();
			var xAxisRV = JSON.parse($("#xAxisRV").val());
			var data_chartRV = JSON.parse($("#data_chartRV").val());
			
			var options = generateChartOptionsBar("chartRV", titleRV,subtitle_RV,xAxisRV,data_chartRV);
			
			chartVerbal = new Highcharts.Chart(options);
		}
		
		function createChartMatematico(){
			
			var xAxisRM = JSON.parse($("#xAxisRM").val());
			var data_chartRM = JSON.parse($("#data_chartRM").val());
			var titleRM = $("#titleRM").val();
			var subtitle_RM = $("#sub_title_RM").val();
			var options = generateChartOptionsBar("chartRM", titleRM,subtitle_RM,xAxisRM,data_chartRM);
			chartMatematico = new Highcharts.Chart(options);
		}
                
                
                
               function createChartPuntajeVerbal(){
			
			var titleRV = $("#titleRV").val();
			var subtitle_RV = $("#sub_title_RV").val();
                        
                        var arrPuntajes = JSON.parse($("#puntajes_RV").val());
                        
                        console.log(arrPuntajes);
                        
			var xAxisRV =  JSON.parse($("#xAxisRV").val());
			var data_chartRV = JSON.parse($("#puntajes_RV").val());
			
			var options = generateChartOptionsBarPuntos("chartPuntajeRV", titleRV,subtitle_RV,xAxisRV,data_chartRV);
			
			chartVerbalPuntos = new Highcharts.Chart(options);
		}
                
                function createChartPuntajeMate(){
			
			var titleRV = $("#titleRM").val();
                        
			var subtitle_RV = $("#sub_title_RM").val();
                        
                        var arrPuntajes = JSON.parse($("#puntajes_RM").val());
                        
                        console.log(arrPuntajes);
                        
			var xAxisRV =  JSON.parse($("#xAxisRM").val());
			var data_chartRV = JSON.parse($("#puntajes_RM").val());
			
			var options = generateChartOptionsBarPuntos("chartPuntajeRM", titleRV,subtitle_RV,xAxisRV,data_chartRV);
			
			chartMatematicoPuntos = new Highcharts.Chart(options);
		}
		
		
		function generateChartOptionsBar(divId,title,subtitle,xAxis,data){
			
			//generateChartOptionsBar("chartRM", titleRM,subtitle_RM,xAxisRM,data_chartRM)
			var arrNiveles = ["", "Básico", "Intermedio", "Avanzado", "Avanzado"];
			
			var options = {
		            chart: {
		                type: 'column',
		                renderTo: divId
		            },
		            credits: {
		                enabled: false
		            },
		            title: {
		                text: title
		            },
		            subtitle: {
		                text: subtitle
		            },
		            xAxis: {
		                categories: xAxis
		            },
		            yAxis: { 
		            	title: {
		                    text: 'Nivel obtenido'
		                },
		            	plotBands: [{ 
		                    from: 0,
		                    to: 1,
		                    color: 'rgba(253, 253, 150, 0.2)'
		                },
		                { 
		                    from: 1,
		                    to: 2,
		                    color: 'rgba(119, 221, 119, 0.2)'
		                },
		                { 
		                    from: 2,
		                    to: 3,
		                    color: 'rgba(119, 158, 203, 0.2)'
		                }
		                ], 
		                
		                labels: {
		                    formatter: function() {
		                        return arrNiveles[this.value];
		                    }
		                }
		                
		            },
		            tooltip: {
		            	
		                useHTML: true,
		            	formatter: function () {
		                    
		                    var message  = "";
		                    //arrNiveles[this.y];
		                    
		                    message = '<span style="font-size:10px">'+this.key+'</span><table>'+
		                    '<tr><td style="color:'+this.point.series.color+';padding:0">'+this.point.series.name+' Nivel Alcanzado: </td>' +
		                    '<td style="padding:0"><b>&nbsp;'+arrNiveles[this.y]+' </b></td></tr></table>';
		                    return message;
		                }
		            },
		            plotOptions: {
		                column: {
		                    pointPadding: 0.2,
		                    borderWidth: 0
		                }
		            },
		            series:  data
		            
		        };
			
			return options	
		}
                
                function generateChartOptionsBarPuntos(divId,title,subtitle,xAxis,data){
                        
                        var newTitle = title.replace("Resultado de", "Total de puntos obtenidos en");
			
			//generateChartOptionsBar("chartRM", titleRM,subtitle_RM,xAxisRM,data_chartRM)
			var arrNiveles = ["", "Básico", "Intermedio", "Avanzado", "Avanzado"];
			
			var options = {
		            chart: {
		                type: 'column',
		                renderTo: divId
		            },
		            credits: {
		                enabled: false
		            },
		            title: {
		                text: newTitle
		            },
		            subtitle: {
		                text: subtitle
		            },
		            xAxis: {
		                categories: xAxis
		            },
		            yAxis: { 
		            	  min: 0,
                                    title: {
                                        text: 'Puntos obtenidos por todos los alumnos'
                                    },
                                    plotBands: [{ 
                                        from: 0,
                                        to: 1000,
                                        color: '#f1f1f1'
                                    }
                                    ], 
		                
		            },
		            tooltip: {
		            	
		                useHTML: true,
		            	formatter: function () {
		                    
		                    var message  = "";
		                    //arrNiveles[this.y];
		                    
		                    message = '<span style="font-size:17px">El grupo obtuvo  <b>'+this.y+ '</b> puntos en <b>' +this.key+'</b> </span>';
		                    return message;
		                }
		            },
		            plotOptions: {
		                column: {
		                    pointPadding: 0.2,
		                    borderWidth: 0
		                }
		            },
		            series:  data
		            
		        };
			
			return options	
		}
});