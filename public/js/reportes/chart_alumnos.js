var chartGlobal = null; 
var chartVerbal = null;
var chartMatematico = null;


$(document).ready(function () {
		
        $(document).foundation();

        var arrNiveles = ["", "Básico", "Intermedio", "Avanzado"];
        var xAxisGlobal = JSON.parse($("#xAxisGlobal").val());

        var data_chartGlobal = JSON.parse($("#data_chartGlobal").val());

        var title_global = $("#title_global").val();
        var options = generateChartOptionsBar('chart_global',title_global,'',xAxisGlobal,data_chartGlobal);
        chartGlobal = new Highcharts.Chart(options);
        
        
        
        var xAxisRV = JSON.parse($("#xAxisRV").val());
		
		var data_chartRV = JSON.parse($("#data_chartRV").val());
		
		var titleRV = $("#titleRV").val();
        var optionsVerbal = generateChartOptionsBar('chartRV',titleRV,'',xAxisRV,data_chartRV);
        chartVerbal = new Highcharts.Chart(optionsVerbal);
        
        
        
        var xAxisRM = JSON.parse($("#xAxisRM").val());
		
		var data_chartRM = JSON.parse($("#data_chartRM").val());
		
		var titleRM = $("#titleRM").val();
                
        var optionsMat = generateChartOptionsBar('chartRM',titleRM,'',xAxisRM,data_chartRM);
        chartMatematico = new Highcharts.Chart(optionsMat);
        
        
        
        
        $('#chkGetPrePost').change(function() {
    	
        
            if($(this).is(":checked")) {

                    $.ajax({
                            url: $('#urlDataAdjunto').val(),
                            dataType: 'json',
                            type: 'POST',

                            data: {
                                    idExamenAdjunto : $('#idExamenAdjunto').val(),
                                    _intIdUsuarioPerfil : $('#_intIdUsuarioPerfil').val()

                            },
                            beforeSend: function() {
                                            //mostrar imagen procesando 
                                     $('#rowOptions').block({
                                        message: '<h5 ><i class="fa fa-spinner fa-spin fa-2x"></i>Procesando...</h5>'
                                    });
                            },
                            success: function(data) {
                                
                                var newDataGlobal = JSON.parse(data.global.data);
                                var newDataVerbal = JSON.parse(data.verbal.data);
                                var newDataMat = JSON.parse(data.matematico.data);
                               chartGlobal.addSeries(newDataGlobal[0]);
                               chartVerbal.addSeries(newDataVerbal[0]);
                               chartMatematico.addSeries(newDataMat[0]);
                               $('#rowOptions').unblock();
                            },
                            error: function(data) {
                                $('#rowOptions').unblock();
                            }
                    });	
            }
            else{
                    //redraw charts originales   
                    window.location.reload();
                   
            }
        });
        
        
        
        function generateChartOptionsBar(divId,title,subtitle,xAxis,data){
			
			//generateChartOptionsBar("chartRM", titleRM,subtitle_RM,xAxisRM,data_chartRM)
			var arrNiveles = ["", "Básico", "Intermedio", "Avanzado", "Avanzado"];
			
			var options = {
		            chart: {
		                type: 'column',
		                renderTo: divId
		            },
		            credits: {
		                enabled: false
		            },
		            title: {
		                text: title
		            },
		            subtitle: {
		                text: subtitle
		            },
		            xAxis: {
		                categories: xAxis
		            },
		            yAxis: { 
		            	title: {
		                    text: 'Nivel obtenido'
		                },
		            	plotBands: [{ 
		                    from: 0,
		                    to: 1,
		                    color: 'rgba(253, 253, 150, 0.2)'
		                },
		                { 
		                    from: 1,
		                    to: 2,
		                    color: 'rgba(119, 221, 119, 0.2)'
		                },
		                { 
		                    from: 2,
		                    to: 3,
		                    color: 'rgba(119, 158, 203, 0.2)'
		                }
		                ], 
		                
		                labels: {
		                    formatter: function() {
		                        return arrNiveles[this.value];
		                    }
		                }
		                
		            },
		            tooltip: {
		            	
		                useHTML: true,
		            	formatter: function () {
		                    
		                    var message  = "";
		                    //arrNiveles[this.y];
		                    
		                    message = '<span style="font-size:10px">'+this.key+'</span><table>'+
		                    '<tr><td style="color:'+this.point.series.color+';padding:0">'+this.point.series.name+' Nivel Alcanzado: </td>' +
		                    '<td style="padding:0"><b>&nbsp;'+arrNiveles[this.y]+' </b></td></tr></table>';
		                    return message;
		                }
		            },
		            plotOptions: {
		                column: {
		                    pointPadding: 0.2,
		                    borderWidth: 0
		                }
		            },
		            series:  data
		            
		        };
			
			return options	
		}
        
        
    });
    
    
    