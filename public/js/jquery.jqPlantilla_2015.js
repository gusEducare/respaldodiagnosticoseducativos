(function( $ ){

	var methods = {};
	//var dataTemp=null;
//	const HUEVOKIDS = 5;
//	const TECNOP = 6;
//	const SWO = 7;
//	const IC4 = 8;
//	const SEDESU = 9;
//	const DCT = 10;
	var urlSitioGE = 'http://www.grupoeducare.com/web/';
	var urlTienda = 'http://hubbleged.educaredigital.com/tienda/';
	//var urlSitioGE = 'localhost'; 

	//const urlSitio = 'http://pruebas.com';
	var urlSitioPortal = 'http://portalge.grupoeducare.com';
	var urlSitio = 'http://certificacionestics.com';
//	var urlPeticion = urlSitio + '/suscripcionajax';
	var urlTerminos = urlSitioPortal + '/terminos.phtml';
	var urlPrivacidad = urlSitioPortal + '/privacidad.phtml';
        var urlCreditos = urlSitio + 'creditos.html';
	var urlContacto = 'http://soporte.grupoeducare.com/main/';
	var urlColabora = 'http://grupoeducare.com/colabora/';
        var urlFAQ= 'http://soporte.grupoeducare.com/main/?page_id=\n\
    7';
      
	methods = {
		init : function( opt ) { 
			
			options = opt;
			
			settings = $.extend( {
					'type'		: 'POST',
					'dataType'	: 'jsonp',
					'numRows'	: 10,
					'page'		: 1
				}, options);
			if( typeof options.sitio === 'undefined' ){
				//$.error('El sitio es obligatorio');
				// Seteamos un servicio por default
				//settings.sitio = HUEVOKIDS;
			}else{
				//methods.cargaAjax( dataTemp ) ;
				//settings.sitio = methods.obtenerSitio( options.sitio );
			}
			// Se setean los valores default en caso de no especifcarlos en el sitio
			settings.header 		= typeof options.header === 'undefined' ? true : options.header;
			settings.footer 		= typeof options.footer === 'undefined' ? true : options.footer;
			settings.nombreSitio	= typeof options.nombreSitio === 'undefined' ? 
										'Grupo Educare' : 
											options.nombreSitio;
			settings.urlFacebook 	= typeof options.urlFacebook === 'undefined' ? 
										'http://www.facebook.com/GrupoEducare' : 
											options.urlFacebook;
			settings.urlTwitter 	= typeof options.urlTwitter === 'undefined' ? 
										'https://twitter.com/GrupoEducare' : 
											options.urlTwitter;
			settings.urlYoutube 	= typeof options.urlYoutube === 'undefined' ? 
										'http://www.youtube.com/user/GrupoEducare' : 
											options.urlYoutube;
                        settings.urlInstagram   = typeof options.urlInstagram === 'undefined' ? 
										'https://instagram.com/grupoeducare/' : 
											options.urlInstagram;                                                                  
			settings.headColor		= typeof options.headColor === 'undefined' ? 
										'#466BB2' : 
											options.headColor;
			settings.footColor		= typeof options.footColor === 'undefined' ? 
										'#466BB2' : 
											options.footColor;
			settings.colorBotones		= typeof options.colorBotones === 'undefined' ? 
										'azul' : 
											options.colorBotones;
			settings.colorBotonesSec	= typeof options.colorBotonesSec === 'undefined' ? 
			
                    'naranja' : 
											options.colorBotonesSec;
                                                                                
                                                                                
			
				
			//dataTemp = $.extend(options.postData, {idSitio: settings.sitio}, {header: settings.header}, {footer: settings.footer} );
			//methods.cargaAjax( dataTemp );
			if(settings.header){
				methods.insertaHeader();
			}
			if(settings.footer){
				methods.insertaFooter();
			} 
			
			
		},
		/*
		 * Se comenta esta funcion ya que no se necesitan datos de la base, todos serán fijos
		 */
		/*
		cargaAjax : function ( dt ) {				
			$.ajax({
				type	: settings.type,
				dataType: settings.dataType,
				url		: urlPeticion, //options.url,
				data	: dt,
				beforeSend: function() {
				},
				success:function(json){
					if(dt.header){
						methods.insertaHeader( json['COLOR_HEADER'] );
					}
					if(dt.footer){
						methods.insertaFooter( json['URL_TERMINOS'], json['URL_POLITICAS'], json['COLOR_FOOTER'] );
					}
				},
				error: function(json){
				}
			});
		},
		*/
		insertaHeader : function(){
			var _strHtmlHeader = '';
                        
                         _strHtmlHeader += 
//                                '<div id="divMenuTop"  class="row ">'+
//                                    '<div class="small-6 medium-4 large-5 columns margen-logo margen-arriba-logo">'+
//                                        '<img src="'+settings.urlLogo+'"> '+ 
//                                    '</div>'+
//                                    '<div class="hide-for-small medium-5 large-4 columns text-right ">'+
//                                        '<ul class="button-group margin-top-36 top-36">'+
//                                            '<li><a href="#" id="abrirModalLogin" class="button btn-sesion btn-'+settings.colorBotonesSec+'">Ingresa</a></li>'+
//                                            '<li><a href="#" id="abrirModalValidacion" class="button btn-registro btn-'+settings.colorBotones+'">Regístrate</a></li>'+
//                                        '</ul>'+
//                                    '</div>'+
//                                    '<div class="small-6 medium-3 large-3 columns margin-top-36 text-right top-36">'+
//                                        '<div class="circulo btn-'+settings.colorBotones+'"><a href="http://grupoeducare.com/colabora/" target="_blank"><img src="' + urlSitio + '/imgs/colaboraNuevo.png" alt="" /></div></a>'+
//                                        '<div class="circulo btn-'+settings.colorBotones+'"><a href="http://soporte.grupoeducare.com/main/" target="_blank"><img src="' + urlSitio + '/imgs/contactoNuevo.png" alt="" /></div></a>'+
//                                        '<div class="circulo btn-'+settings.colorBotones+'"><a href="http://hubbleged.educaredigital.com/tienda/" target="_blank"><img src="' + urlSitio + '/imgs/tiendaNuevo.png" alt="" /></div></a>'+
//                                    '</div>'+
//                                    '<div class=" small-12 columns show-for-small spacer-20">'+
//                                    '</div>'+
//                                '</div>';
                                

//			_strHtmlHeader1 +=
//				'<div id="ss_header" class="ca_banner" style="background-color:'+settings.headColor+';"> '+
//						
//							'<div id="ca"> '+
//								'<div class="cu"> '+
//									'<a class="tooltipss" target="_blank" href=" ' + urlSitioGE + ' "> '+
//										'<img src="'+settings.urlLogo+'"> '+ 
//									'</a> '+
//
//								'</div> '+
//						        '<div class="ce"> '+
//						        	'<a class="tooltipss" target="_blank" href=" ' + urlColabora + ' "> '+
//						        		'<img class="circulo" src="' + urlSitio + '/imgs/colaboraNuevo.png"> '+
//						        		'<span>'+
//										'<b></b>Colabora con nosotros '+
//										'</span>'+
//						        	'</a> '+
//						        	'<a class="tooltipss" target="_blank" href=" ' + urlContacto + ' "> '+
//						        		'<img class="circulo" src="' + urlSitio + '/imgs/contactoNuevo.png"> '+
//						        		'<span>'+
//                                                                            '<b></b>Soporte y Contacto  '+
//									'</span>'+
//						        	'</a> '+
//						        	'<a class="tooltipss" target="_blank" href=" ' + urlTienda + ' "> '+
//						        		'<img class="circulo" src="' + urlSitio + '/imgs/tiendaNuevo.png"> '+
//						        		'<span>'+
//                                                                            '<b></b>Tienda en línea'+
//									'</span>'+
//						        	'</a> '+
//						        '</div> '+ 
//						    '</div> '+
//						
//				'</div>';
			/*finjqueryheader*/
				
			   
			$('body').prepend( _strHtmlHeader );
		},
                /*
//		insertaFooter : function(){
//			var _strHtmlFooter = '';
//			_strHtmlFooter +=	
//			    '<div class="row datos_footer" style="background-color:'+settings.footColor+';"> '+
//			    '	<div class="container datos_footer"  style="text-align:center"> '+
//			    '	<a href="'+settings.urlFacebook+'" class="" target="_blank"><img src="' + urlSitio + '/imgs/icon_fb.png"/></a> '+
//			    '	<a href="'+settings.urlYoutube+'" class="" target="_blank"><img src="' + urlSitio + '/imgs/icon_yt.png"/></a> '+
//			    '	<a href="'+settings.urlTwitter+'" class="" target="_blank"><img src="' + urlSitio + '/imgs/icon_tw.png"/></a> '+
//			    '	</div> '+
////			    '	<div class="spacer-subscripcion"></div> '+
//			    '	<div class="container datos_footer"> '+
//			    '	<p class="datos_footer">'+settings.nombreSitio+' | 2015 Grupo Educare S. A de C.V. Todos los derechos reservados. | (442) 222-5444 con 4 líneas</p> '+
//			    '	 <a class="datos_footer" href="' + urlTerminos + '" target="_blank">T&eacute;rminos y condiciones </a> '+
//			    '  |  '+
//			    ' 	 <a class="datos_footer" href="' + urlPrivacidad + '" target="_blank">Aviso de Privacidad</a> '+
////			    '   <div id="toTop" title="Ir al inicio de la p&aacute;gina">Arriba</div> '+
//			    '   </div> '+
//			    ' </div> ';
//			$('body').append( _strHtmlFooter );
//			$("a.datos_footer").css('font-family',$("a.datos_footer").prev().css("font-family")); // hack para PDP
//			$('#ss_header').css('max-width', 'none'); // Hack para los sitios que tienen  foundation
//			$('.datos_footer').css('max-width', 'none'); // Hack para los sitios que tienen  foundation
//			if(settings.nombreSitio=='Edutype'){ // Hack para solucionar problemas de vista en Edutype
//				$('#fondo').css('top', 32);
//				$('.row.datos_footer').css('bottom', 0).css('position', 'fixed').css('width', '100%');
//			}
//		},	
                */
                
                insertaFooter : function(){
                    
                    
//			var _strHtmlFooter = '';
//			_strHtmlFooter +=	
//			   ' <div class="row footer"> '+
//                                '<div class="large-12 medium-6 small-6 columns text-left">'+
//                                   '<img style="padding-bottom: 20px;" src="' + urlSitio + '/imgs/logo_ge_foot.png" alt="" />'+
//                                '</div>'+
//                                '<div class="large-3 medium-6 small-6 columns text-left">'+
//                                   ' <div class="text-foot">S&iacute;guenos</div><div class="spacer"></div>'+
//                                   '<a href="'+settings.urlFacebook+'" class="" target="_blank"><img style="padding-bottom: 20px;" src="' + urlSitio + '/imgs/icon_face.png" alt="" /></a>'+
//                                   '<a href="'+settings.urlTwitter+'" class="" target="_blank"> <img style="padding-bottom: 20px;" src="' + urlSitio + '/imgs/icon_tweet.png" alt="" /></a>'+
//                                   '<a href="'+settings.urlYoutube+'" class="" target="_blank"> <img style="padding-bottom: 20px;" src="' + urlSitio + '/imgs/icon_youtube.png" alt="" /></a>'+
//                                   '<a href="'+settings.urlInstagram+'" class="" target="_blank"><img style="padding-bottom: 20px;" src="' + urlSitio + '/imgs/icon_phone.png" alt="" /></a>'+
//                               ' </div>'+
//                                '<div class="large-2 medium-3 small-4 columns text-left">'+
//                                   ' <div class="text-foot"><a class="a" href="' + urlTerminos + '" target="_blank">T&eacute;rminos y condiciones</a></div>'+
//                                    '<div class="text-foot"><a class="a" href="' + urlPrivacidad + '" target="_blank">Aviso de privacidad</a></div>'+
//                                   
//                               ' </div>'+
//                                '<div class="large-1 medium-3 small-4 columns text-left">'+
//                                   ' <div class="text-foot"><a class="a" href="' + urlCreditos + '" target="_blank">Cr&eacute;ditos</a></div>'+
//                                   '<div class="text-foot"><a class="a" href="' + urlFAQ + '" target="_blank">FAQ</a></div>'+
//                                   
//                                '</div>'+
//                               '<div class="large-2 medium-3 small-4 columns text-left">'+
//                                   '<div class="text-foot"><a class="a" href="' + urlContacto + '" target="_blank">Soporte y contacto</a></div>'+
//                                    '<div class="text-foot"><a class="a" href="' + urlTienda + '" target="_blank">Tienda en l&iacute;nea</a></div>'+
//                                    '<div class="text-foot"><a class="a" href="' + urlColabora + '" target="_blank">Colabora con nosotros</a></div>'+
//                                '</div>'+
//                               '<div class="large-4 medium-3 columns hide-for-small text-right">'+
//                                    '<div class="text-foot">Diagn&oacute;sticos Educativos | 2015 </div>'+
//                                     '<div class="text-foot">Grupo Educare S. A de C.V.</div>'+
//                                   '<div class="text-foot">Todos los derechos reservados. Tel. (442) 2 22 54 44</div>'+
//                                '</div>'+
//                                '<div class="show-for-small">'+
//                                '<br/>'+
//                                    '<div class="text-foot text-center">Diagn&oacute;sticos Educativos | 2015 Grupo Educare S. A de C.V. Todos los derechos reservados. Tel. (442) 2 22 54 44</div>'+
//                                '</div>'+
//
//                            '</div>';
//			$('body').append( _strHtmlFooter );
                
			$("a.datos_footer").css('font-family',$("a.datos_footer").prev().css("font-family")); // hack para PDP
			$('#ss_header').css('max-width', 'none'); // Hack para los sitios que tienen  foundation
			$('.datos_footer').css('max-width', 'none'); // Hack para los sitios que tienen  foundation
			if(settings.nombreSitio=='Edutype'){ // Hack para solucionar problemas de vista en Edutype
				$('#fondo').css('top', 32);
				$('.row.datos_footer').css('bottom', 0).css('position', 'fixed').css('width', '100%');
			}
		},
                
               
		/*
		obtenerSitio : function( strSitio ){
			var intIdSitio;
			switch( strSitio ){
				case 'huevokids': case 'hk':
					intIdSitio = HUEVOKIDS;
				break;
				case 'tid': case 'tecno': case 'tecnop':
					intIdSitio = TECNOP;
				break;
				case 'swo': case 'software':
					intIdSitio = SWO;
				break;
				case 'cert': case 'ic4':
					intIdSitio = IC4;
				break;
				case 'sedesu':
					intIdSitio = SEDESU;
				break;
				case 'dct': case 'DCT':
					intIdSitio = DCT;
				break;
				default:
					intIdSitio = HUEVOKIDS;			
			}
			return intIdSitio;
		
		*/
	};
	
	  
	$.fn.jqPlantilla = function( method ) {
		$t = this;
		// Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {			
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'El metodo ' +  method + ' no existe en jqPlantilla' );
		}   

	};
	$.plantilla=$.fn.jqPlantilla;   
})( jQuery );