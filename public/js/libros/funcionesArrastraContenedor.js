/*_______                         __                       ______               __                        __             
 |   _   |.----.----.---.-.-----.|  |_.----.---.-.        |      |.-----.-----.|  |_.-----.-----.-----.--|  |.-----.----.
 |       ||   _|   _|  _  |__ --||   _|   _|  _  |        |   ---||  _  |     ||   _|  -__|     |  -__|  _  ||  _  |   _|
 |___|___||__| |__| |___._|_____||____|__| |___._|        |______||_____|__|__||____|_____|__|__|_____|_____||_____|__| 
 @author:           Grupo Educare S.A. de C.V.
 @desarrolladores   Greg: gregorios@grupoeducare.com
 Juan Pablo: jpgomez@grupoeducare.com
 Juan José: jlopez@grupoeducare.com
 @estilos css:      Claudia:  cgochoa@grupoeducare.com        
 *********************************************************/
var posPilaY;
var posPilaX;
var posColocada;
var tocando = false;
var dragElement;
var dragColor;
var respImgWidth;
var espacioRespuestas;
var alturaRespuestas, respuestasRandom;
var numRespuestasAux, totalContenedoresRespUnicos = 0;
var contadorValor;
var varianteAC;
function estructuraHTMLarrastraContenedor(estructuraBotones) {
    varianteAC = determinaVariante();
    respuestasRandom = false;
    /*Sección de variables compartidas*/
    contadorValor = 0;
    var estructuraColumna2 = "";
    var alturaFrame = 525, anchoFrame = 950;
    var heightContResp = 100;

    var liRandom = respuestasRandom ? shuffleRespuestas(estructuraBotones) : estructuraBotones;
    if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
        estructuraColumna2 = estructuraBotones.split("<*_*>")[0];
        estructuraBotones = estructuraBotones.split("<*_*>")[1];
        estructuraColumna2 = '<ul id="tarjetas2" class="tarjetas ui-helper-reset ui-helper-clearfix">' + estructuraColumna2 + '</ul>';
        estructuraBotones = '<ul id="tarjetas" class="tarjetas ui-helper-reset ui-helper-clearfix">' + estructuraBotones + '</ul>';
    } else {
        estructuraBotones = '<ul id="tarjetas" class="tarjetas ui-helper-reset ui-helper-clearfix">' + liRandom + '</ul>';

    }


    var varianteTipo = json[idPreguntaGlobal].pregunta.tipo;
    var topImg;
    var $coloresTarjeta = ["51c3af", "f8b559", "accb69", "61c2d6", "3e82c1", "9a9a9a", "e16fa6",
        "fecb2f", "e65c73", "ce92bc", "b6cd4f", "b664a4", "eb7c8f", "639bcd", "ed7d21"];
    var margenBottomContenedor = 8;
    var margenTopContenedorResp = 2;
    var espacioTitulosOcupado, espaciosTopContResp, separacionContenedores, espacioDisponible;
    var anchoContResp;
//    $(".pregunta").html(json[idPreguntaGlobal].pregunta.t11pregunta);
//    $(".pregunta").css({margin: "15px 0 15px 0", width:"100%", height:"auto"});
    $("#respuestasRadialGroup").css({width:"100%"});
    /*-------Variables compartidas-----*/

    /*Estilos Compartidos*/
    /*Termmina estilos Compartidos*/

    /*Sección de variantes
     * Para agrega una variante nueva o modificar alguna existe,
     * edita la siguiente estructura.
     * */
    //varianteTipo = varianteTipo === undefined ? 'vertical' : varianteTipo;
    if (varianteTipo == "horizontal") {
        var alturaBarraAvance = 0;
        elementosHtmlCompartidos();
        if (json.length > 1) {
            alturaBarraAvance = 75;
            heightContResp = 135;
            //$("barraAvance").css()
        }
        var numeroRespuestasIndividual = [];
        var contRespuestas, maximoRespuestas;
        $.each(json[idPreguntaGlobal].contenedores, function (indexContenedores) {
            contRespuestas = 0;
            $.each(json[idPreguntaGlobal].respuestas, function (indexRespuestas) {
                if (parseInt(json[idPreguntaGlobal].respuestas[indexRespuestas].t17correcta) === indexContenedores) {
                    contRespuestas++;
                }
            });
            numeroRespuestasIndividual[indexContenedores] = contRespuestas;
        });
        maximoRespuestas = numeroRespuestasIndividual.sort(function (a, b) {
            return b - a
        })[0];
        alturaRespuestas = ($(window).height() - 120 - (maximoRespuestas * 2) - 50 - alturaBarraAvance) / (maximoRespuestas);
        if (alturaRespuestas > 110) {
            alturaRespuestas = 110;
        }
        var anchoElementos = $("html").width;
        $("#respuestasRadialGroup").css({width: anchoElementos, margin: "0px"});
        var anchoRespuestas = (907 - (5 * (json[idPreguntaGlobal].contenedores.length - 1))) / json[idPreguntaGlobal].contenedores.length;
        $("#columnaRespuestas").css({width: "100%", height: "110px", textAlign: "center"});
        $("#principal").css({float: "none", width: "100%", margin: "0 auto", height: "auto"});
        $("#columnaRespuestas").css({width: "100%", height: "110px", textAlign: "center"});
        $(".subContenedor").css({width: anchoRespuestas - 4, display: "inline-block", background: "black",
            marginRight: "5px", padding: "0 10px 10px 10px", verticalAlign: "top"});
        $(".tituloSubcontenedor").css({width: anchoRespuestas - 4, display: "table-cell",
            background: "inherit", border: "none", height: "50px", fontSize: "20px"});
        $("#tarjetas li").css({width: anchoRespuestas - 24, verticalAlign: "top", maxWidth: maxWidth,
            marginRight: "10px", boxShadow: "none"});
        $("#tarjetas").css({float: "right", width: "100%"});
        $("#tarjetas li").css({display: "inline-table", margin: "8px", height: alturaRespuestas, maxHeihgt: "110px"});
        $("#columnaContenedores").css({width: "100%"});
        $(".subContenedor").last().css({margin: "0px"});
        coloresRandom('.subContenedor');
        $(".contenedorRespuesta").css({height: alturaRespuestas, maxHeight: "110px"})
        topImg = (alturaRespuestas - 30) / 2;
        coloresRandom(".tarjetas li");

    } else if (varianteTipo === "ordenar") {
        cadenaRespustaUsuario = '0,1,2,3,4'
        espacioOpcionesArrastrable();
        elementosHtmlCompartidos();
        $(".subContenedor").css({marginBottom: espacioRespuestas});
        $(".contenedorRespuesta").css({display: "block"});
        $(".tituloSubcontenedor").css("display", "none");
        $(".contenedorRespuesta").each(function (index, value) {
            if (json[idPreguntaGlobal].contenedores.length > 1) {
                $(this).append('<div class="textoContenedor"id="texto' + index + '"><div class="textoPaso">' + json[idPreguntaGlobal].contenedores[index].Contenedor[0] + '</div></div>');
            } else {
                //$(this).append('<div class="textoContenedor"id="texto' + index + '"><div class="textoPaso">' + json[idPreguntaGlobal].contenedores[index].Contenedor[0] + '</div></div>');
            }
        });
        $(".textoContenedor").css({color: "#a4cd6d", fontWeight: "bold", fontSize: "18px",
            width: "100%", textAlign: "center", verticalAlign: "middle", height: "inherit", display: "table"});
        $(".textoPaso").css({display: "table-cell", verticalAlign: "middle"});
        $(".contenedorRespuesta").height(alturaRespuestas - 2);
        $(".tarjetas li").height(alturaRespuestas);
        $(".tarjetas li").css({marginBottom: "8px"});
        $(".contenedorRespuesta").last().css({margin: "0px"});
        $(".contenedorRespuesta").first().css({margin: "0px"});
        if (!json[idPreguntaGlobal].pregunta.agrupaContenedores) {
            coloresRandom(".tarjetas li");
        }
        topImg = (alturaRespuestas - 30) / 2;
        if (json[idPreguntaGlobal].pregunta.imagen) {
            var widthCont, heightCont;
            //var alturaRespuestasTemporal=alturaRespuestas;
            alturaRespuestas = 45;
            var alturaExpacioDisponible = json.length === 1 ? 0 : 75;
            espacioRespuestas = (($(window).height() - alturaExpacioDisponible - (alturaRespuestas * (json[idPreguntaGlobal].respuestas.length))) / (json[idPreguntaGlobal].respuestas.length - 1)) - 1;
            topImg = (alturaRespuestas - 30) / 2;
            var top, left, coordenadas;
            $(".contenedorRespuesta").css({display: "inline-table"});
            if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
                respImgWidth = 120;
                $("#columnaRespuestas").prepend('<div id="columnasPilas"><div id="columnaResp1" class="textoColRes">' + json[idPreguntaGlobal].contenedoresRespuesta[0] + '</div><div id="columnaResp2" class="textoColRes">' + json[idPreguntaGlobal].contenedoresRespuesta[1] + '</div></div>')
                var idElement;
                $("#columnasPilas").css({margin: "0 auto", textAlign: "center"});
                $(".textoColRes").css({width: respImgWidth, color: "#000", height: "20px", display: "inline-table", margin: "0 32px 0 32px"});
                var idExtra1 = json[idPreguntaGlobal].contenedores.length + 1, idExtra2 = json[idPreguntaGlobal].contenedores.length + 2;
                $("#tarjetas").append('<li id="li' + idExtra1 + '" class="ui-widget-content ui-draggable ui-draggable-handle respuestaExtra"><div class="imgLineas"></div><div id="respuesta' + idExtra1 + '_btn" class="resp_arra_cont"><p>' + json[idPreguntaGlobal].OpcionesExtra[0] + '</p></div></li>');
                $("#tarjetas2").append('<li id="li' + idExtra2 + '" class="ui-widget-content ui-draggable ui-draggable-handle respuestaExtra"><div class="imgLineas"></div><div id="respuesta' + idExtra2 + '_btn" class="resp_arra_cont"><p>' + json[idPreguntaGlobal].OpcionesExtra[1] + '</p></div></li>');
                espacioRespuestas = 10;
                var alturaAreaImagen = $(window).height() - 100;
                $("#columnaRespuestas").css({width: "100%", position: "absolute", top: "0px", textAlign: "center"});
                $("#columnaContenedores").css({position: "absolute", top: "100px", height: alturaAreaImagen, width: "95%"});
                $("#tarjetas, #tarjetas2").css({display: "inline-table"});
                $("#tarjetas li").css({display: "inline-table", backgroundColor: "#eb7c8f"});
                $("#tarjetas li, #tarjetas2 li").css({display: "none"});
                $("#tarjetas li").first().css({display: "inline-table"});
                $("#tarjetas2 li").first().css({display: "inline-table"});
                $("#tarjetas").css({margin: "0 auto"});
                $(".contenedorRespuesta").css({border: "none"});
                $(".contenedorRespuesta").each(function (index) {
                    widthCont = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[3].split(",")[0]);
                    heightCont = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[3].split(",")[1]);
                    $(this).css({width: widthCont, height: heightCont});
                });
                $(".textoContenedor").css({display: "none"});
                $("ul").css({width: 200, display: "inline-block", verticalAlign: "top"});

            } else {
                if (json[idPreguntaGlobal].pregunta.orientacion == "horizontal") {
                    $("#columnaRespuestas").css({width: "100%"});
                    $("#columnaContenedores").css({position: "relative", height: $(window).height() - 100, backgroundSize: "100% 100%", width: "100%"});
                    $("#tarjetas").css({height: "80px"});
                    $("#tarjetas li").css({display: "inline-table"});
                    $(".textoContenedor").css({display: "none"});
                    respImgWidth = 140;
                    $(".textoContenedor").css({display: "none"});
                } else {
                    respImgWidth = "98%";
                    var anchoImagen = json[idPreguntaGlobal].pregunta.anchoImagen !== undefined ? json[idPreguntaGlobal].pregunta.anchoImagen : 70;
                    var anchoOpciones = json[idPreguntaGlobal].pregunta.anchoImagen !== undefined ? 100 - json[idPreguntaGlobal].pregunta.anchoImagen : 30;
                    var alturaImagen = json.length > 1 ? "85%" : "100%";
                    $("#columnaRespuestas").css({width: anchoOpciones + "%", paddingLeft: "20px"});
                    if (json[idPreguntaGlobal].pregunta.tamanyoReal !== true) {
                        $("#columnaContenedores").css({width: anchoImagen + "%", position: "relative", height: $(window).height(), backgroundSize: "100% " + alturaImagen});
                    } else {
                        $("#columnaContenedores").css({width: anchoImagen + "%", position: "relative", height: $(window).height() - 15, backgroundSize: 'initial'});
                    }
                    $("#tarjetas").css({marginLeft: "3px"});
                    $(".textoContenedor").css({display: "none"});
                    if (json[idPreguntaGlobal].pregunta.respuestaImagen) {
                        respImgWidth = "40%";
                        var widthCont = parseInt(json[idPreguntaGlobal].contenedores[0].Contenedor[3].split(",")[0]);
                        var heightCont = parseInt(json[idPreguntaGlobal].contenedores[0].Contenedor[3].split(",")[1]);
                        alturaRespuestas = json[idPreguntaGlobal].pregunta.tamanyoReal !== undefined ? heightCont : 80;
                        anchoRespuestas = json[idPreguntaGlobal].pregunta.tamanyoReal !== undefined ? widthCont : 100;
                        espacioRespuestas = 10;
                        if (json[idPreguntaGlobal].pregunta.bloques) {
                            $("#tarjetas li").css({display: "table"});
                        } else {
                            $("#tarjetas li").css({display: "inline-table"});
                        }
                        $("#tarjetas li").css({marginLeft: '10px'});
                    }

                }
                if (json[idPreguntaGlobal].pregunta.forma == "circulo") {
                    respImgWidth = alturaRespuestas;
                    $("#tarjetas li").css({heigh: alturaRespuestas, borderRadius: "50%"});
                    $(".contenedorRespuesta").css({borderRadius: "50%"});
                }
                if (json[idPreguntaGlobal].pregunta.imagen == true) {
                    //respImgWidth=alturaRespuestas;                  
                    $("#tarjetas li").css({heigh: alturaRespuestas, verticalAlign: "top", width: "80%"});
                    $(".imgLineas").css({backgroundImage: "none"});

                }
                if (!json[idPreguntaGlobal].pregunta.respuestaImagen) {
                    var liIndex, bgIndex = 98;
                    $("#tarjetas").css({width: "100%"});
                    respImgWidth = "100%";
                    $("#tarjetas").css({width: "100%", display: "inline-block"});
                    var textoEtiqueta;
                    //$("#columnaRespuestas").append('<div id="contenedorEtiquetas"></div>');
                    var colorFondoTarjetas = json[idPreguntaGlobal].pregunta.conservaColores ? $(this).find('.etiquetasBackground').parent().parent().css('background-color') : "#51C3AF";
                    $(".imgLineas").each(function (index, element) {
                        textoEtiqueta = json[idPreguntaGlobal].respuestas[index].etiqueta !== undefined ? json[idPreguntaGlobal].respuestas[index].etiqueta : liIndex;
                        //colorFondoTarjetas = json[idPreguntaGlobal].pregunta.conservaColores ? $(this).find('.etiquetasBackground').parent().parent().css('background-color') : "#51C3AF";
                        liIndex = index + 1;
                        $(this).parent().find(".imgLineas").append('<div class="contenedorEtiquetas" value="' + $(this).next().attr("value") + '"><p>' + textoEtiqueta + '</p></div><div class="etiquetasBackground"><p>' + textoEtiqueta + '</p></div>');
                        $(this).find('.etiquetasBackground').css({zIndex: bgIndex, backgroundColor: colorFondoTarjetas});
                        bgIndex++;
                        $("#li" + liIndex).css({zIndex: bgIndex});
                        bgIndex++;
                        $(this).css({zIndex: bgIndex});
                        bgIndex++;
                        $(this).find('.contenedorEtiquetas').css({zIndex: bgIndex});
                        bgIndex++;
                        if (json[idPreguntaGlobal].pregunta.conservaColores) {
                            if (json[idPreguntaGlobal].contenedores[index].Contenedor[6]) {
                                $(this).find('.etiquetasBackground').css({backgroundColor: json[idPreguntaGlobal].contenedores[index].Contenedor[5]});
                                $(this).find('.contenedorEtiquetas').css({backgroundColor: json[idPreguntaGlobal].contenedores[index].Contenedor[5]});
                            } else {
                                $(this).find('.etiquetasBackground').css({backgroundColor: $(this).find('.etiquetasBackground').parent().parent().css('background-color')});
                                $(this).find('.contenedorEtiquetas').css({backgroundColor: $(this).find('.etiquetasBackground').parent().parent().css('background-color')});
                            }
                        }
                    });
//                    $('#tarjetas li').each(function(){
//                        
//                    });
                    $("#contenedorEtiquetas").css({display: "inline-block", verticalAlign: "top"});
                    $("#columnaRespuestas").css({display: "inline-block", width: "30%"});
                    $(".etiquetasRespuesta").css({height: "45px", backgroundColor: "#39D887", marginBottom: espacioRespuestas});
                    $(".etiquetasRespuesta").last().css({marginBottom: "0px"});
                    $(".contenedorRespuesta").css({width: "45px", height: "45px"});
                    $(".resp_arra_cont").css({paddingLeft: "10px"});
                    $("#tarjetas li").css({padding: "0px", backgroundColor: colorFondoTarjetas});
                    anchoRespuestas = 10;
                } else {
                    respImgWidth = "100px";
                    $("#tarjetas").css({width: "99%"});
                }


            }
            $("#tarjetas li").css({marginBottom: espacioRespuestas});
            $("#tarjetas li").last().css({marginBottom: "0"});


            if (strAmbiente === 'DEV') {
                $("#columnaContenedores").css({backgroundImage: 'url("../../../../' + strRutaLibro + json[idPreguntaGlobal].pregunta.url + '")',
                    backgroundRepeat: "no-repeat"});
            } else {
                $("#columnaContenedores").css({backgroundImage: 'url("' + json[idPreguntaGlobal].pregunta.url + '")',
                    backgroundRepeat: "no-repeat"});
            }

            if (json[idPreguntaGlobal].contenedores.length === 1) {
                $("#columnaContenedores").css({backgroundSize: "auto", backgroundPosition: 'center'});
            }
            borde = json[idPreguntaGlobal].pregunta.borde === undefined ? true : json[idPreguntaGlobal].pregunta.borde;
            borde = borde ? "1px solid #000" : "none";
            $(".contenedorRespuesta").css({border: borde});
            if (json[idPreguntaGlobal].contenedores.length !== 1) {
                $(".contenedorRespuesta").css({position: "absolute"});
                $(".contenedorRespuesta").css({background: "#6587de", margin: "0px"});
                if (json[idPreguntaGlobal].contenedoresRespuesta === undefined && json[idPreguntaGlobal].pregunta.respuestaImagen !== true) {
                    anchoRespuestas = '100%';
                    if (!json[idPreguntaGlobal].pregunta.respuestaImagen) {
                        if (!json[idPreguntaGlobal].opcionesTexto) {
                            if (/iPad|iPod/i.test(navigator.userAgent)) {
                                $('body').css({width: '83%'});

                            }
                        }
                        var settingsCss = json[idPreguntaGlobal].css;
                        if (settingsCss !== undefined) {
                            if (settingsCss.anchoRespuestas !== undefined) {
                                anchoRespuestas = settingsCss.anchoRespuestas;
                            }
                            if (settingsCss.espacioRespuestas !== undefined) {
                                $("#tarjetas li").css({marginBottom: settingsCss.espacioRespuestas});
                                $("#tarjetas li").last().css({marginBottom: "0"});
                            }

                        }
                    }

                }
                $("#tarjetas li, #tarjetas2 li").css({width: anchoRespuestas}); //
                $("#tarjetas li, #tarjetas2 li").css({height: alturaRespuestas});
            } else {
                $(".contenedorRespuesta").css({display: "block", background: 'transparent',
                    margin: '0px', height: alturaRespuestas});
                var displayMode = json[idPreguntaGlobal].pregunta.renglones === undefined && json[idPreguntaGlobal].pregunta.filas === undefined ? 'block' : 'inline-table';
                var numFilas = json[idPreguntaGlobal].pregunta.filas === undefined ? 1 : json[idPreguntaGlobal].pregunta.filas;
                var anchoFila = 100 / numFilas;
                $('.contenedorRespuesta').css({width: anchoFila + '%', display: displayMode, verticalAlign: 'top'});
                $('#tarjetas li').css({boxShadow: 'none', height: alturaRespuestas});
                if (json[idPreguntaGlobal].pregunta.tamanyoReal !== true) {
                    $('#tarjetas li').css({width: '40%'});
                }
                if (json[idPreguntaGlobal].pregunta.respuestaUnicaMultiple === true) {
                    var positionTarjetasLista, topLista, defase, posicionesPilas = [], zIndex;
                    $("#tarjetas").css({position: "relative"});
                    $("#tarjetas li").css({display: "block", width: "120px"});

                    topLista = 0;
                    for (var i = 1; i <= json[idPreguntaGlobal].respuestas.length; i++) {
                        defase = 0;
                        zIndex = 50;
                        $(".listaTarjetas_" + i).each(function (index) {
                            $(this).css({top: topLista, position: "absolute", left: defase, zIndex: zIndex});
                            defase = 10 * (index + 1);
                            zIndex--;
                        });

                        topLista = (alturaRespuestas + espacioRespuestas) * i;
                    }
                    if (json[idPreguntaGlobal].pregunta.contenedorContador) {
                        $("#respuestasRadialGroup").append('<div id="indicadorValor"><p></p></div>');
                        $("#indicadorValor p").html("$0.00");
                        var settingsCss = json[idPreguntaGlobal].css;
                        var top = 0, left = 0;
                        if (settingsCss !== undefined) {
                            if (settingsCss.posicionIndicadorValor) {
                                top = settingsCss.posicionIndicadorValor.top === undefined ? top : parseInt(settingsCss.posicionIndicadorValor.top);
                                left = settingsCss.posicionIndicadorValor.left === undefined ? left : parseInt(settingsCss.posicionIndicadorValor.left);
                            }
                            if (settingsCss.anchoRespuestas !== undefined) {
                                $("#tarjetas li").css({width: settingsCss.anchoRespuestas});
                            }


                        }
                        $("#indicadorValor").css({position: "absolute", top: top, left: left, width: "130px", height: "80px",
                            border: "5px solid #616161", display: "table", textAlign: "center", borderRadius: "8px"});
                        $("#indicadorValor p").css({display: "table-cell", verticalAlign: "middle", fontSize: "20px"});
                        $(".contenedorRespuesta").css({height: "100%"});
                    }
                }
            }
            $(".pila2").css({background: "#639BCD"});
            var colorContenedor, liIndex;
            $(".contenedorRespuesta").css({verticalAlign: "top"});
            $(".subContenedor").each(function (index) {
                if (json[idPreguntaGlobal].pregunta.imagen && json[idPreguntaGlobal].contenedoresRespuesta === undefined && json[idPreguntaGlobal].pregunta.respuestaImagen === undefined) {
                    if (!json[idPreguntaGlobal].pregunta.conservaColores) {
                        if (json[idPreguntaGlobal].contenedores[index].Contenedor[2] === undefined) {
                            $(this).prepend('<div class="indicadorContenedorIzquierda flecha"></div>');
                        } else {
                            switch (json[idPreguntaGlobal].contenedores[index].Contenedor[2]) {
                                case 'derecha':
                                    $(this).append('<div class="indicadorContenedorDerecha flecha"></div>');
                                    $(this).find('.indicadorContenedorDerecha').prev().css({position: 'relative', display: 'inline-table'});
                                    //$(this).find('.indicadorContenedorDerecha').css({position: 'absolute', left: $(this).find('.contenedorRespuesta').width()});
                                    break;
                                case 'arriba':
                                    $(this).prepend('<div class="indicadorContenedorArriba flecha"></div>');
                                    break;
                                case 'abajo':
                                    $(this).append('<div class="indicadorContenedorAbajo flecha"></div>');
                                    $(this).find('.indicadorContenedorAbajo').prev().css({position: 'relative'});
                                    break;
                                default:
                                    $(this).prepend('<div class="indicadorContenedorIzquierda flecha"></div>');

                            }
                        }

                    } else {
                        //$('.contenedorRespuesta').css({border: 'none'});
                    }

                    $(this).css({width: 'initial'});
                }
                colorContenedor = json[idPreguntaGlobal].contenedores[index].Contenedor[5] === undefined ? "#6587DE" : json[idPreguntaGlobal].contenedores[index].Contenedor[5];
                coordenadas = json[idPreguntaGlobal].contenedores[index].Contenedor[1].split(",");
                top = parseInt(coordenadas[0]);
                left = parseInt(coordenadas[1]);
                $(this).children().children().text(json[idPreguntaGlobal].contenedores[index].Contenedor[0]);
                $(this).css({top: top, left: left, position: "absolute"});
                if (json[idPreguntaGlobal].contenedores[index].Contenedor[2] === "circular") {
                    //$(this).css({borderRadius: "10px"});
                }
                widthCont = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[3].split(",")[0]);
                heightCont = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[3].split(",")[1]);

                if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined || json[idPreguntaGlobal].pregunta.respuestaImagen == true) {
                    if (json[idPreguntaGlobal].contenedores.length !== 1) {
                        $(this).css({backgroundColor: colorContenedor});
                    }
                    $(this).css({width: widthCont, height: heightCont});
                    if (json[idPreguntaGlobal].contenedores.length !== 1) {
                        $(this).find('.contenedorRespuesta').css({height: heightCont});
                    }

                }
                if (json[idPreguntaGlobal].contenedores.length !== 1) {
                    $(this).find('.contenedorRespuesta').css({backgroundColor: colorContenedor});

                }
                if (json[idPreguntaGlobal].contenedores[index].Contenedor[6]) {
                    liIndex = index + 1;
                    $("#li" + liIndex).css({backgroundColor: colorContenedor});
                }
                if (json[idPreguntaGlobal].contenedores[index].Contenedor[7]) {
                    $("#li" + liIndex).css({backgroundColor: json[idPreguntaGlobal].contenedores[index].Contenedor[7]});
                }
            });
            $('#tarjetas').css({marginLeft: 0});
            $("#tarjetas2 li").css({background: "#639BCD"});
            if (json[idPreguntaGlobal].pregunta.conservaColores) {
                $('.contenedorRespuesta').css({border: 'none'});
                if (json[idPreguntaGlobal].pregunta.respuestaImagen) {
                    $('.ui-widget-content').each(function () {
                        $(this).find('.imgLineas').remove();
                    });
                }
            }
        }
        if (json[idPreguntaGlobal].pregunta.opcionesTexto === true) {
            borde = json[idPreguntaGlobal].pregunta.borde === undefined ? true : json[idPreguntaGlobal].pregunta.borde;
            borde = borde ? "1px solid #000" : "none";
            $('.imgLineas').css({display: 'none'});
            $('.contenedorRespuesta').css({background: 'transparent', border: borde});
            $('.tarjetas li').css({padding: '0px'});
            if (json[idPreguntaGlobal].pregunta.tamanyoReal !== true) {
                $('#columnaContenedores').css({backgroundSize: '100% 100%'});
            }
        }
    } else if (varianteTipo === "matrizHorizontal") {
        console.log("Variante Matriz Horizontal");
        elementosHtmlCompartidos();
        espacioOpcionesArrastrable();
        var liMostrar;
        var subContMostrar;
        var contRespMostrar;
        var coordenadasMostrar;
        var alturaRespuestas;
        var tituloSubWidth = 180;
        var principalWidth = 814;
        var margenTema = 13;
        margen = 9;
        $('.contenedorRespuesta').addClass('contenedorMatriz');
        var numeroContenedores = json[idPreguntaGlobal].respuestas.length / $(".subContenedor").length;
        var widthRespuestas = json[idPreguntaGlobal].pregunta.proporcion ? (anchoFrame - 26) / 2 : (anchoFrame - tituloSubWidth - 26) / numeroContenedores;
        var widthTitulo = json[idPreguntaGlobal].pregunta.proporcion ? ((anchoFrame - 26) / 2) + "px" : "165px";
        $(".tFila").css({marginRight: margen, marginBottom: margen});
//            $("#leyendaLineas").css({width: "100%", height: "60px", marginBottom: "2px"});
//            alturaRespuestas = (($(window).height()) - ($('#leyendaLineas').height() + 
//                (margen * $('.subContenedor').size()) + alturaOpciones)) /
//                ($('.subContenedor').size());
        $("#leyendaLineas").append('<span id="tituloVacio"></span>');
        $.each(json[idPreguntaGlobal].contenedoresFilas, function (index) {
            $("#leyendaLineas").append('<div class="tFila"><div id="filaTexto' + index + '" class="filaTexto">' + json[idPreguntaGlobal].contenedoresFilas[index] + '</div></div>');
            $('#filaTexto' + index).css({height: alturaRespuestas + "px"})
        });
        $("#leyendaLineas").css({width: "100%", height: "55px", verticalAlign: "top", marginBottom: "6px"});
        $("#tituloVacio").css({display: "inline-table", width: widthTitulo, height: "inherit",
            verticalAlign: "auto", marginRight: margenTema, float: "left"});
        $(".tFila").css({width: widthRespuestas, display: "inline-table",
            height: "inherit", verticalAlign: "top", float: "left", marginRight: "9px"});
        $(".filaTexto").css({width: "100%", display: "table-cell", height: "inherit",
            verticalAlign: "middle", background: "#816dbb"});
        $("#respuestasRadialGroup").css({width: anchoElementos, margin: "0px"});
        var tituloMatriz = json[idPreguntaGlobal].pregunta.descripcion ? json[idPreguntaGlobal].pregunta.descripcion : "Tema";
        $("#tituloVacio").append('<div class="tituloEstatico">' + tituloMatriz + '</div>');
        $(".tituloEstatico").css({color: "#FFF", display: "table-cell", verticalAlign: "middle",
            background: "#816dbb", textAlign: "center"});
        var anchoRespuestas = (907 - (5 * (json[idPreguntaGlobal].contenedores.length - 1))) / json[idPreguntaGlobal].contenedores.length;
        $("#columnaRespuestas").css({width: "100%", height: "auto", textAlign: "center"});
        $("#principal").css({float: "none", width: "100%", margin: "0 auto", height: "auto"});
        $(".subContenedor").css({width: "100%", display: "inline-block", background: "black", height: "auto",
            margin: "0 0 6px", padding: "5px 0 5px", verticalAlign: "top", maxWidth: "none", marginBotoom: "10px"});
        $(".contenedorRespuesta").css({width: widthRespuestas, display: "inline-block",
            border: "0px", margin: "0 10px 0 0", verticalAlign: "top", height: alturaRespuestas});
        $(".tituloSubcontenedor").css({background: "inherit", border: "none", height: "57px",
            color: "#FFF", width: tituloSubWidth, display: "inline-block", verticalAlign: "top",
            marginTop: "3px"});
        $("#tarjetas li").css({width: widthRespuestas, verticalAlign: "top",
            marginRight: "10px", boxShadow: "none", maxWidth: "none"});
        $("#tarjetas").css({float: "none", width: "100%", margin: "0px"});
        $("#tarjetas li").css({display: "inline-table", margin: "0px",
            color: "#000", background: "#FFF", height: alturaRespuestas});
        $("#columnaContenedores").css({width: "100%"});
        $(".subContenedor").last().css({margin: "0px"});
        topImg = (alturaRespuestas - 30) / 2;
        contadorRespMostrar = 0;
        for (var i = 0; i < json[idPreguntaGlobal].respuestas.length; i++) {
            if (json[idPreguntaGlobal].respuestas[i].mostrar === true) {
                coordenadasMostrar = json[idPreguntaGlobal].respuestas[i].t17correcta.split(",");
                liMostrar = "li" + (i + 1);

                subContMostrar = "subcontenedor_" + coordenadasMostrar[0];
                contRespMostrar = "cont" + i;
                //$("#"+liMostrar).css({display:"none"});
                $("#" + liMostrar).addClass("mostrarRespuesta");
                //$("#"+liMostrar).remove();
                $('#' + subContMostrar + ' .' + contRespMostrar).css({background: "#FFF", color: "#47c09e",
                    textAlign: "left"});
                $('#' + subContMostrar + ' .' + contRespMostrar).append(json[idPreguntaGlobal].respuestas[i].t13respuesta);
                $('#' + subContMostrar + ' .' + contRespMostrar).css({display: "inline-table"});
                $('#' + subContMostrar + ' .' + contRespMostrar + " p").css({lineHeight: "20px", verticalAlign: "middle", display: "table-cell", paddingLeft: "15px"});
                $('#' + subContMostrar + ' .' + contRespMostrar).addClass("mostrar");
                $('#' + subContMostrar + ' .' + contRespMostrar).removeClass("contenedorRespuesta");
                contadorRespMostrar++;
            }
        }
        $('.tituloSubcontenedor').each(function (index) {
            $(this).parent().prepend($('<div class="contenedorTituloFila" id="contenedor_' + index + '"></div>'));
            $(this).appendTo("#contenedor_" + index);
            $(this).css({width: "165px", display: "table-cell", verticalAlign: "middle"});
            $(this).parent().css({height: alturaRespuestas});
        });
        $(".contenedorTituloFila").css({display: "inline-table", marginRight: "13px"});
        if (json[idPreguntaGlobal].pregunta.proporcion) {
            $(".contenedorTituloFila").css({width: widthTitulo});
        }
        $('.subContenedor').each(function () {
            $(this).find('div').last().css({margin: '0 -12px 0 0 '});
        });

        coloresRandom(".subContenedor");
    } else if (varianteTipo === "vertical") {
        cadenaRespustaUsuario = '';
        elementosHtmlCompartidos();
        $("#principal").css({height: parseInt($(window).height())});
        $(".tarjetas li").css({height: alturaRespuestas});
        $("#tarjetas li").css({marginBottom: espacioRespuestas});
        coloresRandom(".tarjetas li");
        $(".subContenedor").css({marginBottom: margenBottomContenedor});
        $(".subContenedor").last().css({marginBottom: "0px"});
        $("#tarjetas li").last().css({marginBottom: "0px"});
        coloresRandom(".tarjetas li");
        $(".subContenedor").css({marginBottom: margenBottomContenedor});
        $(".subContenedor").last().css({marginBottom: "0px"});
        $("#tarjetas").css({maxHeight: alturaFrame});
        $("#respuestasRadialGroup").css({margin: "0px"});
        if (json[idPreguntaGlobal].contenedores.length < 2) {
            $("#tarjetas").css({width: "100%"});
            $("#columnaRespuestas").css({width: "30%"});
            $(".contenedorRespuesta").css({width: "33%", display: "inline-block", marginTop: "0px"});
            $(".tituloSubcontenedor").css({display: "none"});
            $("#columnaContenedores").css({width: "70%"});
            $("#principal").css({width: "100%", margin: "0"});
            for (var i = 0; i < json[idPreguntaGlobal].contenedores.length; i++) {
                if (index === i) {
                    $('<div/>').attr({class: 'contenedorRespuesta cont' + i + ' r_' + json[idPreguntaGlobal].contenedores[i].Contenedor[0] + '', id: "contenedor" + i}).appendTo("#subcontenedor_" + index);
                    $(".contenedorRespuesta").css({height: alturaRespuestas + "px"});
                }
            }
            console.log("Altura: " + parseInt($(window).height() / alturaRespuestas));
        }
    } else if (json[idPreguntaGlobal].contenedoresFilas !== undefined) {//Varitante Arrastra Matriz
        console.log("Variante arrastra matriz");
        elementosHtmlCompartidos();
        espacioOpcionesArrastrable();
        var maxWidth = 290;
        var maxHeight = 140;
        var margen = 6;
        var contContenedores = 0;
        var maxAltura = 0;

        if (json[idPreguntaGlobal].contenedores.length < 2) {
            $(".subContenedor").css({maxWidth: "400px"});
            $("#tarjetas li").css({maxWidth: "400px"});
            $("#leyendaLineas").css({width: "270px"});
            alturaRespuestas = alturaRespuestas > 100 ? 100 : alturaRespuestas;
        }
        if (json[idPreguntaGlobal].contenedoresFilas.length < 3) {
            alturaRespuestas = alturaRespuestas > maxHeight ? maxHeight : alturaRespuestas;
            $(".contenedorRespuesta").css({maxHeight: maxHeight});
            $(".subContenedor").css({maxHeight: maxHeight});
            $("#tarjetas li").css({maxHeight: maxHeight});
        }
        $("#columnaRespuestas").css({width: "100%", height: "auto", margin: "0 auto"});
        $("#columnaContenedores").css({width: "100%", height: "auto", margin: "0px"});
        $("#principal").css({width: "100%", margin: "0 auto", display: "inline-block", textAlign: "center"});
        $(".subContenedor").css({margin: "0 auto", display: "inline-block", marginLeft: margen, verticalAlign: "top"});
        $(".tituloSubcontenedor").css({display: "table-cell", verticalAlign: "middle", width: anchoRespuestas});
        $(".tituloSubcontenedor").each(function () {
            if ($(this).height() > maxAltura) {
                maxAltura = $(this).height();
            }
        });
        $(".tituloSubcontenedor").height(maxAltura);
        $("#tarjetas li").css({width: anchoRespuestas, verticalAlign: "top", marginRight: "10px", boxShadow: "none", height: alturaRespuestas});
        $("#tarjetas").css({float: "right", width: "100%"});
        $("#tarjetas li").css({display: "inline-table"});
        $("#leyendaLineas").append('<span id="tituloVacio"></span>');
        $("#tituloVacio").height(maxAltura - 4);
        $.each(json[idPreguntaGlobal].contenedoresFilas, function (index) {
            $("#leyendaLineas").append('<div class="tFila"><div id="filaTexto' + index + '" class="filaTexto">' + json[idPreguntaGlobal].contenedoresFilas[index] + '</div></div>');
            $('#filaTexto' + index).css({height: alturaRespuestas + "px"});
        });
        $(".filaTexto").css({background: "#f8b559", border: "none"});
        $(".tFila").css({marginRight: margen, marginBottom: margen});
        $("#leyendaLineas span").css({background: "inherit", border: "none"});
        $(".contenedorRespuesta").css({borderColor: "#c9c9c9", marginBottom: margen, height: alturaRespuestas, display: 'table'});
        $(".subContenedor").last().css("margin-right", "0");
        $(".subContenedor").width(anchoRespuestas);
        $(".tarjetas span").css({verticalAlign: "middle"});
        coloresRandom(".tarjetas li");
    }

    function espacioOpcionesArrastrable(variante) {
        var heightTitulos;
        var widthTextosLineas = 250;
        var margenLeftMatrizLocal = 8, opcionesArrastrablesTotales = json[idPreguntaGlobal].respuestas.length;
        var espacioTarjetasApiladas = 0;
        var valorCompensacion = 0;
        var valorCompensacionAlto = 0;
        var barraCreada = 0, espacioBarra = 0;
        if (json.length > 1) {
            barraCreada = 75;
        }
        if (varianteTipo === "vertical")
            valorCompensacion = 2;
        if (varianteTipo === "vertical" || json[idPreguntaGlobal].contenedoresFilas !== undefined) {
            heightTitulos = 40;
            valorCompensacionAlto = json[idPreguntaGlobal].contenedoresFilas !== undefined && json[idPreguntaGlobal].contenedoresFilas.length === 3 ? 15 : 0;
            espacioTitulosOcupado = (json[idPreguntaGlobal].contenedores.length * heightTitulos);
            if (json[idPreguntaGlobal].contenedoresFilas !== undefined) {
                opcionesArrastrablesTotales = json[idPreguntaGlobal].contenedoresFilas.length;
                espacioTarjetasApiladas = 105;
                espacioTitulosOcupado = heightTitulos;
                if (varianteTipo === "matrizHorizontal") {
                    opcionesArrastrablesTotales = json[idPreguntaGlobal].contenedores.length;
                    heightTitulos = 60 / json[idPreguntaGlobal].contenedores.length;
                    margenTopContenedorResp = 10;

                }
            }

        } else if (varianteTipo === "ordenar" || varianteTipo === "titulosCirculos") {
            heightTitulos = 0;
            margenTopContenedorResp = 0;
            var arrDimensiones = json[idPreguntaGlobal].contenedores[0].Contenedor[3].split(',');
            var ancho = arrDimensiones[0];
            var alto = arrDimensiones[1];
            var espacioBarra = 0;
            alturaFrame = json[idPreguntaGlobal].contenedores.length !== 1 ? 523 : alto;
            if (json[idPreguntaGlobal].contenedores.length === 1) {
                opcionesArrastrablesTotales = cuentaOpcionesCorrectasContenedorUnico(idPreguntaGlobal);
            }
            espacioTitulosOcupado = (json[idPreguntaGlobal].contenedores.length * heightTitulos);
        }

        //var espaciosSubcontenedor= ((obj.contenedores.length - 1) * margenBottomContenedor) / contenedoresArr.length;
        espaciosTopContResp = margenTopContenedorResp * opcionesArrastrablesTotales;
        separacionContenedores = margenBottomContenedor * (json[idPreguntaGlobal].contenedores.length - 1);
        if (json.length > 1) {
            espacioBarra = 95;
        }
        espacioDisponible = alturaFrame - (espaciosTopContResp + separacionContenedores + espacioTitulosOcupado + espacioTarjetasApiladas + barraCreada + valorCompensacionAlto);
        alturaRespuestas = 120;
        espacioRespuestas = (alturaFrame - espacioDisponible - espacioBarra) / (opcionesArrastrablesTotales - 1) - valorCompensacion;
        var espacioDisponibleAncho = anchoFrame - (widthTextosLineas + json[idPreguntaGlobal].contenedores.length * margenLeftMatrizLocal);
        anchoRespuestas = espacioDisponibleAncho / json[idPreguntaGlobal].contenedores.length;
    }
    function elementosHtmlCompartidos() {
        totalContenedoresRespUnicos = 0;
        $("#respuestasRadialGroup").append('<div id="columnaContenedores"></div><div id="columnaRespuestas"></div>');
        $('#columnaRespuestas').append(estructuraBotones + estructuraColumna2);
        $('#columnaContenedores').append('<div id="principal"></div>');
        for (var i = 0; i < json[idPreguntaGlobal].respuestas.length; i++) {
            if (json[idPreguntaGlobal].respuestas[i].t17correcta == "0") {
                totalContenedoresRespUnicos += json[idPreguntaGlobal].respuestas[i].numeroCorrectas;
            }
        }
        if (json[idPreguntaGlobal].pregunta.contenedorContador === true) {
            totalContenedoresRespUnicos = 1;
        }
        $.each(json[idPreguntaGlobal].contenedores, function (index, elementA) {
            $('<div/>').attr({class: "subContenedor", id: "subcontenedor_" + index}).appendTo("#principal");
            $("#subcontenedor_" + index).append('<span class="tituloSubcontenedor">' + elementA + '</span>');
            if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
                for (var i = 0; i < json[idPreguntaGlobal].contenedores.length; i++) {
                    if (index === i) {
                        $('<div/>').attr({class: 'contenedorRespuesta cont' + i + ' r_' + json[idPreguntaGlobal].contenedores[i].Contenedor[0] + '', id: "contenedor" + i}).appendTo("#subcontenedor_" + index);
                        $(".contenedorRespuesta").css({height: alturaRespuestas + "px"});
                    }
                }
            } else {
                if (json[idPreguntaGlobal].pregunta.respuestaUnicaMultiple === true || json[idPreguntaGlobal].contenedores.length === 1) {
                    var numCorrectas = cuentaOpcionesCorrectasContenedorUnico(idPreguntaGlobal);
                    var filas = json[idPreguntaGlobal].pregunta.filas !== undefined ? json[idPreguntaGlobal].pregunta.filas : 1;
                    var renglones = json[idPreguntaGlobal].pregunta.renglones !== undefined ? json[idPreguntaGlobal].pregunta.renglones : numCorrectas;
                    totalContenedoresRespUnicos = filas === 1 && renglones === 1 ? totalContenedoresRespUnicos : filas * renglones;
                    for (var i = 0; i < totalContenedoresRespUnicos; i++) {
                        $('<div/>').attr({class: 'contenedorRespuesta cont0', id: "contenedor" + i}).appendTo("#subcontenedor_0");
                        $(".contenedorRespuesta").css({height: alturaRespuestas + "px"});
                    }
//                    $.each(contenedoresArr, function (indexContenedoresArr, elementB) {
//                        
//                    });
                } else {
                    $.each(contenedoresArr, function (indexContenedoresArr, elementB) {
                        if (index === parseInt(elementB)) {
                            $('<div/>').attr({class: 'contenedorRespuesta cont' + indexContenedoresArr + '', id: "contenedor" + indexContenedoresArr}).appendTo("#subcontenedor_" + index);
                            $(".contenedorRespuesta").css({height: alturaRespuestas + "px"});
                        }
                    });

                }
            }
        });
        $(".tituloSubcontenedor").css({width: $('#principal').width(), display: "table-cell"});
        if (json[idPreguntaGlobal].contenedoresFilas !== undefined) {
            $("#principal").prepend('<div id="leyendaLineas"></div>');

            $(".contenedorRespuesta").each(function (index) {
                if (index % json[idPreguntaGlobal].contenedoresFilas.length === 0);
                    contContenedores = 0;
                $(this).attr("id", "contenedor" + index + "_" + contContenedores);
                contContenedores++;
            });
        }
        numRespuestasAux = json[idPreguntaGlobal].respuestas.length;
    }
    $('.respuesta').css({margin: '0px'});
    cadenaRespustaUsuario = '0,1,2,3,,4,5';
    arrastraContenedor();
//    respondeActividad(cadenaRespustaUsuario);
    determinaVariante();
    var heightRespuestas = determinaAltoRespuestas(varianteAC);
    asignaAlturaRespuestas(heightRespuestas, varianteAC);
    $("#tarjetas li").last().css({marginBottom: "0"});
    if (json[idPreguntaGlobal].contenedoresFilas !== undefined) {
        $("#tarjetas li").css({margin: "0 8px 8px 0"});
    } else if (json[idPreguntaGlobal].pregunta.tipo === "horizontal") {
        $("#tarjetas li").css({margin: "8px"});
    }else if (json[idPreguntaGlobal].pregunta.tipo === "vertical") {
        $("#tarjetas li").css({marginBottom: "15px"});
        $("#tarjetas li").last().css({marginBottom: "0"});
           
    }else if(varianteAC === "ordenar"){
        $("#tarjetas li").css({marginBottom: "3px"});
        $("#tarjetas li").last().css({marginBottom: "0"});
    }else if(varianteAC === "etiquetas"){
        $("#tarjetas li").css({marginBottom: "10px"});
        $("#tarjetas li").last().css({marginBottom: "0"});
    }
}

function arrastraContenedor() {
    $('#contenedor').css('min-height', '67.26666px');
    $('#tarjetas').css('min-height', '20px');

    var $tarjetas = $("#tarjetas, #tarjetas2");
    var $tarjetasElementos;
    if (json[idPreguntaGlobal].pregunta.imagen && json[idPreguntaGlobal].contenedoresRespuesta === undefined && !json[idPreguntaGlobal].pregunta.respuestaImagen) {
        $tarjetasElementos = $(".contenedorEtiquetas", $tarjetas);
    } else {

        $tarjetasElementos = $("li", $tarjetas)
    }

    var $contenedor = $(".contenedorRespuesta");

    //Para hacer dragueables los elementos que se llevan al contenedor
    $tarjetasElementos.draggable({revert: true, revertDuration: 1000, containment: "document", cursor: "move", zIndex: 100,
        start: function (event, ui) {
            dragElement = $(this);
            nextElement = getNextVisible(dragElement);
            if (json[idPreguntaGlobal].contenedoresFilas !== undefined || json[idPreguntaGlobal].pregunta.tipo === "horizontal") {
                dragElement.css({margin: "0px"});
            }
            if (json[idPreguntaGlobal].pregunta.imagen) {

                dragColor = dragElement.css("background-color");
            }

        },
        stop: function (event, ui) {
//                $("#tarjetas li").css({marginBottom: espacioRespuestas});
            $("#tarjetas li").last().css({marginBottom: "0"});
            if (json[idPreguntaGlobal].contenedoresFilas !== undefined) {
                $("#tarjetas li").css({margin: "0 8px 8px 0"});
            } else if (json[idPreguntaGlobal].pregunta.tipo === "horizontal") {
                $("#tarjetas li").css({margin: "8px"});
            }
            if (json[idPreguntaGlobal].pregunta.respuestaDuplicada === true && json[idPreguntaGlobal].pregunta.tipo === 'horizontal') {

                desbloquearContenedorRepetidos();
            }
        },
        scroll: true,
        drag: function (event, ui) {
            if (json[idPreguntaGlobal].pregunta.respuestaDuplicada === true && json[idPreguntaGlobal].pregunta.tipo === 'horizontal') {

                var contenedoresRepetidos = mismoElementoDroppeado($(this));
                bloquearContenedorRepetidos(contenedoresRepetidos);
            }
        }
    }); //Para hacer dragueables los elementos que se llevan al contenedor                    
    var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    var start = mobile ? "touchstart" : "mousedown";
    var fin = mobile ? "touchend" : "mouseup";
    $contenedor.droppable({//Para que el contenedor sea dropable y acepte las tarjetas
        //activeClass: "ui-state-highlight",
        //tolerance: 'pointer',
        over: function (event, ui) {
            $(this).addClass("ui-state-highlight");
        },
        drop: function (event, ui) {
            idContenedor = ($(this).attr('id'));
            pintaRespuestaEnContenedor($("#" + idContenedor), dragElement);
            var elementoArrastra;
            if (json[idPreguntaGlobal].pregunta.imagen && json[idPreguntaGlobal].contenedoresRespuesta === undefined && !json[idPreguntaGlobal].pregunta.respuestaImagen) {
                elementoArrastra = ui.draggable.attr("value");
            } else {
                elementoArrastra = ui.draggable.children().not(".imgLineas").attr("value");

            }
            var elementoContenedor = $(this).parent().attr("id").split("_");
            siguientePregunta(c03id_tipo_pregunta, elementoArrastra, parseInt(elementoContenedor[1]));
            //$(this).droppable("disable");
            //ui.draggable.draggable("disable");

            $(this).removeClass("ui-state-highlight");
            if (json[idPreguntaGlobal].contenedoresFilas !== undefined) {
                var numeroContenedor = $(this).attr("id").split("_");
                contenedorDroppeado = parseInt(numeroContenedor[1]);
                console.log("drop de matriz: " + contenedorDroppeado);
                $("#principal ul li p").css({padding: "0px"})
            }
            $(this).find(".textoContenedor").addClass("textoOcutlo");
            $(this).find(".textoContenedor").css({display: "none"});
            if (json[idPreguntaGlobal].pregunta.imagen) {
                var widthCont = parseInt(json[idPreguntaGlobal].contenedores[0].Contenedor[3].split(",")[0]);
                var heightCont = parseInt(json[idPreguntaGlobal].contenedores[0].Contenedor[3].split(",")[1])
                //dragElement.css({height: heightCont, width: widthCont});
                if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
                    if (dragElement.hasClass("pila1")) {
                        $(".pila2").css({position: "absolute", left: "465px", top: "25px"});
                    } else {
                        $(".pila1").css({position: "absolute", left: "345px", top: "25px"});
                    }
                }
            }
            if (dragElement.parent().find('.dragRespuesta')[0] !== undefined) {

                dragElement.parent().droppable("enable");
//                    dragElement.parent().css({backgroundColor:"initial"});
                dragElement.remove();
            }
        },
        out: function (event, ui) {
            $(this).removeClass("ui-state-highlight");
        }
    });
    $tarjetas.droppable({//Permite que las tarjetas sean dropables aceptando elementos del contenedor
        accept: ".contenedorRespuesta li",
        activeClass: "custom-state-active",
        drop: function (event, ui) {
            creaImagenEnContenedor(ui.draggable);
        }
    });



    $("ul.tarjetas > li").click(function (event) {//Resuelve los iconos
        var $elemento = $(this),
                $area = $(event.area);
        if ($area.is("a.ui-icon-contenedor")) {
            borrarTarjeta($elemento);
        }
        return false;
    });
}



function coloresRandom(elementoHtml) {
    var $coloresTarjeta = ["51c3af", "f8b559", "accb69", "61c2d6", "3e82c1", "9a9a9a", "e16fa6",
        "fecb2f", "e65c73", "ce92bc", "b6cd4f", "b664a4", "eb7c8f", "639bcd", "ed7d21", "51c3af",
        "f8b559", "accb69", "61c2d6", "3e82c1", "9a9a9a", "e16fa6", "fecb2f", "e65c73"];
    $(elementoHtml).each(function (index) {
        var tamanoColoresTarjeta = $coloresTarjeta.length - 1;
        var indexRandom = Math.floor((Math.random() * tamanoColoresTarjeta));
        $(this).css({background: "#" + $coloresTarjeta[indexRandom]});
        delete $coloresTarjeta[indexRandom];
        $coloresTarjeta.splice(indexRandom, 1);
    });
}
function shuffleRespuestas(respuestasString) {
    var estructuraBotonesShuffle = [], indicesEB = [], indexRandom,
            arrayMostrar = [], indiceAM = 0;
    for (var i = 0; i < json[idPreguntaGlobal].respuestas.length; i++) {
        if (json[idPreguntaGlobal].respuestas[i].mostrar === true) {
            console.log("MostrarIndice: " + i);
            arrayMostrar[indiceAM] = i;
            indiceAM++;
        }
    }
    var estructuraBotonesArray = respuestasString.split("</li>");
    delete estructuraBotonesArray[estructuraBotonesArray.length - 1];
    estructuraBotonesArray.splice(estructuraBotonesArray.length - 1, 1);
    for (var i = 0; i < (estructuraBotonesArray.length); i++) {
        indicesEB[i] = i;
        estructuraBotonesArray[i] = estructuraBotonesArray[i] + "</li>"
    }
    $.each(estructuraBotonesArray, function (index, element) {
        indexRandom = Math.floor((Math.random() * (indicesEB.length)));
        estructuraBotonesShuffle[index] = estructuraBotonesArray[indicesEB[indexRandom]];
        delete indicesEB[indexRandom];
        indicesEB.splice(indexRandom, 1);

    });
    //    if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
//    } else {
//        estructuraBotones = '<ul id="tarjetas" class="tarjetas ui-helper-reset ui-helper-clearfix">' + estructuraBotonesShuffle.join("") + '</ul>';
//    }
    return estructuraBotonesShuffle.join("");
}

function pintaRespuestaEnContenedor(contenedor, respuesta) {
    var id = respuesta.hasClass('dragRespuesta') ? respuesta.attr('value') : respuesta.find('.combos').attr('value');
    if (json[idPreguntaGlobal].pregunta.posicionamiento !== undefined) {
        var contenedorRespuesta;
        switch (json[idPreguntaGlobal].pregunta.posicionamiento) {
            case 'descendente':
                contenedorRespuesta = $('.contenedorRespuesta');
                contenedorRespuesta.each(function () {
                    if (!$(this).hasClass('ui-droppable-disabled')) {
                        contenedor = $(this);
                        return false;
                    }
                });
                break;
            case 'ascendente':
                contenedorRespuesta = $('.contenedorRespuesta').get().reverse();
                $.each(contenedorRespuesta, function () {
                    if (!$(this).hasClass('ui-droppable-disabled')) {
                        contenedor = $(this);
                        return false;
                    }
                });
                break;
            default:

        }
    }

    //respuesta.css({display: "none"});
    respuesta.addClass("liAcierto");
    if (!json[idPreguntaGlobal].pregunta.contenedorContador) {
        contenedor.removeClass("ui-state-highlight");
        contenedor.find('.textoContenedor').css({display: 'none'});
        if (dragElement.attr("grupo") !== undefined && json[idPreguntaGlobal].pregunta.agrupaContenedores) {//los contenedores pueden seguir droppeando elementos
            var contador = 0;
            $.each(json[idPreguntaGlobal].respuestas, function (index, element) {
                if (element.grupoContenedor + "" === "" + dragElement.attr("grupo").split("g")[1]) {
                    contador++;
                }
            });
            if ($("#" + contenedor.attr("id") + " div[grupo]").length + 1 === contador) {
                contenedor.droppable("disable");                   //Solo si se trata de la bandera "agrupaContenedores"                                
            }
        } else {
            contenedor.droppable("disable");
        }
        //respuesta.css({display: "none"});

    } else {
        var pilaUnica = parseInt(respuesta.find(".resp_arra_cont").attr("pila")) - 1;
        contadorValor += json[idPreguntaGlobal].respuestas[pilaUnica].valor;
        $("#indicadorValor p").html("$" + contadorValor.toFixed(2));
        $("#indicadorValor").animate({borderColor: "#FFF"}, {duration: 200,
            complete: function () {
                $(this).css({borderColor: "#616161"})
            }
        });
        console.log("Dinero en el alcancia: " + contadorValor);

    }

    var esArrastraAImagen = json[idPreguntaGlobal].pregunta.imagen;

    if (esArrastraAImagen) {
        if (json[idPreguntaGlobal].pregunta.respuestaImagen && respuesta.css("background-image") !== "none") {
            contenedor.append('<div class="dragRespuesta" value=' + id + '><a href="#" class="revertir">X</a></div>');
            contenedor.find('.dragRespuesta').css({backgroundImage: respuesta.css("background")});
            $('.dragRespuesta').css({height: 'inherit', padding: 0, margin: 0, position: 'relative'});

        } else {
            contenedor.append('<div class="dragRespuesta"><div class="textoCorrecta"><p>' + respuesta.find("p").html() + '</p></div></div>');
            $(".textoCorrecta p").css({color: "#000"});
            //$(".textoCorrecta p").css(respuesta.find("p").css());
            $(".textoCorrecta").css({width: "inherit", textAlign: "center"});
            var index = parseInt(contenedor.attr('id').split('contenedor')[1]);
            if (json[idPreguntaGlobal].pregunta.opcionesTexto !== true) {
                if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined || json[idPreguntaGlobal].pregunta.conservaColores) {
                    $("#" + idContenedor).css({borderColor: '#000', background: "transparent"});
                    $("#" + idContenedor + ' .textoCorrecta p').css({textAlign: "center"});
                    if (json[idPreguntaGlobal].contenedores[index].Contenedor[7]) {
                        contenedor.css({background: json[idPreguntaGlobal].contenedores[index].Contenedor[7]});
                        var topNE = 0;
                        var leftNE = 0;
                        var anchoNE = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[3].split(",")[0]);
                        var altoNE = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[3].split(",")[1]);
                        if (json[idPreguntaGlobal].contenedores[index].Contenedor[8] !== undefined) {
                            topNE = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[8].split(",")[0]);
                            leftNE = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[8].split(",")[1]);
                            anchoNE = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[8].split(",")[2]);
                            altoNE = parseInt(json[idPreguntaGlobal].contenedores[index].Contenedor[8].split(",")[3]);
                        }
                        contenedor.css({top: topNE, left: leftNE, width: anchoNE, height: altoNE});
                    } else {
                        contenedor.css({background: "transparent"});
                    }
                } else {
                    contenedor.css({background: "#FFF"});
                }
            } else {
                if (json[idPreguntaGlobal].contenedores[index].Contenedor[7]) {
                    contenedor.css({background: json[idPreguntaGlobal].contenedores[index].Contenedor[7]});
                } else {
                    contenedor.css({background: "transparent"});
                }
            }
        }

    } else {
        contenedor.append('<div class="dragRespuesta" value=' + id + '><div class="imgLineas"></div><div class="textoCorrecta"><p>' + respuesta.find("p").html() + '</p></div><a href="#" class="revertir">X</a></div>');
        $(".textoCorrecta").css({width: ($("#" + idContenedor).width() - $(".imgLineas").width()) - 1});
        $(".textoCorrecta p").css({color: respuesta.find("p").css("color"), texAlign: 'left'});
        $(".textoCorrecta p").css('font-size', respuesta.find("p").css('font-size'));
        $('.dragRespuesta').css({height: '100%', padding: 0, margin: 0, position: 'relative'});
    }

    if (json[idPreguntaGlobal].contenedores.length !== 1) {
        if (json[idPreguntaGlobal].pregunta.agrupaContenedores !== true) {//se valida si se esta usando la bandera agrupaContenedores
            contenedor.find('.dragRespuesta').css({background: respuesta.css('background'), color: respuesta.css('color')});//si no se usa la bandera, se pinta de manera normal la respuesta correcta
        } else {
            $("#" + contenedor.attr("id") + ".textoContenedor").css({display: "none"});
            contenedor.find('.dragRespuesta').append("<div style='position: absolute; top: 0; left: 0; width: 100%; height: " + contenedor.height() + "px; background:" + dragElement.css("background") + ";' grupo='" + dragElement.attr("grupo") + "'></div>");
        }
    } else {
        if (!json[idPreguntaGlobal].pregunta.contenedorContador) {
            contenedor.find('.dragRespuesta').css({background: respuesta.css('background'), color: respuesta.css('color'),
                backgroundSize: 'auto 102%', backgroundPosition: '50% 50%'});
        }
    }
    respuesta.addClass("dropped").css({display: "none"});
    $(".textoCorrecta").css({height: "inherit", display: "inline-table", verticalAlign: "top"});

    contenedor.css({textAlign: "left"});
    if (json[idPreguntaGlobal].pregunta.imagen) {
        if (json[idPreguntaGlobal].contenedoresRespuesta === undefined) {
            //respuesta.next().css({marginTop:espacioRespuestas+45})
            contenedor.css({textAlign: "center"});
            contenedor.find("p").css({textAlign: "center"});
        }
    }

    $('.dragRespuesta').draggable({revert: true, revertDuration: 1000, containment: "document", cursor: "move", zIndex: 100,
        start: function (event, ui) {
            dragElement = $(this);
            if (obj.pregunta.imagen) {

                dragColor = dragElement.css("background-color");
            }
        },
        scroll: false
    });

    if ($(".dragRespuesta").length === $("#tarjetas li").length) {
        console.log('todos');
    }
    settingsCssRespuestas();

    eventoRevertirClick();
}
function muestraOpcionSiguientePila(status) {
    if (status = "correcta") {

        var droppedElements = $(".dropped").length;
        var li = dragElement.index() + 1;//9
        var liInicio = dragElement.index() + 2;//10
        var droppedDespuesCont = 0;
        if (json[idPreguntaGlobal].pregunta.tipo === "matrizHorizontal") {
            var contadorMostradas = 0;
            for (var i = li; i <= $("#tarjetas li").size(); i++) {
                if ($("#li" + i).hasClass("mostrarRespuesta")) {
                    contadorMostradas++;
                }
            }
            if (li + contadorMostradas === $("#tarjetas li").size())
                liInicio = 1;
        }
        for (var i = liInicio; i <= $("#tarjetas li").size(); i++) {
            if ($("#li" + i).hasClass("dropped")) {
                droppedDespuesCont++;
            }
        }
        console.log(droppedDespuesCont + " dropped despues");
        if (li === json[idPreguntaGlobal].respuestas.length || (li + droppedDespuesCont) === json[idPreguntaGlobal].respuestas.length || (li + droppedDespuesCont + contadorMostradas === json[idPreguntaGlobal].respuestas.length)) {
            liInicio = 1;
        }
        for (var i = liInicio; i <= $("#tarjetas li").size(); i++) {
            if ($("#li" + i).hasClass("dropped") !== true && $("#li" + i).hasClass("mostrarRespuesta") !== true) {
                $("#li" + i).css({display: "inline-table"});
                console.log("nextLi: " + $("#li" + i).find("p").text());
                return false;
            }
        }
    } else {

    }
}

function getNextVisible(dragElement) {
    //var listElements = dragElement.parent();
    //var li = parseInt(dragElement.children().not(".imgLineas").attr("value"));//9
    var liInicio = parseInt(dragElement.children().not(".imgLineas").attr("value")) + 1;//10    
    for (var i = liInicio; i <= $("#tarjetas li").size(); i++) {
        var nextElement;
        if ($("#li" + i).is(':visible')) {
            nextElement = $("#li" + i);
            return nextElement;
        }
    }
    return false;
}
function cuantosElementosDosColumnas() {
    var contPorColumna = 0;
    var propiedad = json[idPreguntaGlobal].respuestas;
    for (var i = 0; i < propiedad.length; i++) {
        if (json[idPreguntaGlobal].respuestas[i].columna === "0") {
            contPorColumna++;
        }
    }
    return contPorColumna;
}
function cuentaOpcionesCorrectasContenedorUnico(idPregunta) {
    var opcionesArrastrablesTotales = json[idPregunta].respuestas.length;
    if (json[idPregunta].contenedores.length === 1 && json[idPregunta].contenedoresFilas === undefined) {
        for (var i = 0, r = 0; i < opcionesArrastrablesTotales; i++) {
            if (json[idPregunta].respuestas[i].t17correcta === '0') {
                if (json[idPregunta].respuestas[i].numeroCorrectas !== undefined) {
                    r += json[idPregunta].respuestas[i].numeroCorrectas;
                } else {
                    r++;
                }
            }
        }
    } else {
        if (json[idPregunta].pregunta.tipo === 'ordenar') {
            if (json[idPregunta].pregunta.agrupaContenedores === true) {
                r = json[idPregunta].respuestas.length;
            } else {
                r = json[idPregunta].contenedores.length;
            }
        } else {
            r = opcionesArrastrablesTotales;
        }
    }
    return r;
}

function mismoElementoDroppeado(elementoArrastrado) {
    var valorElementoArrastrado = elementoArrastrado.find('p').html();
    var contadorRepeticiones = 0, contenedorElementoRepetido = [];
    $('.subContenedor').each(function (index, element) {
        $(this).find('p').each(function () {
            if (valorElementoArrastrado === $(this).html()) {
                contenedorElementoRepetido[contadorRepeticiones] = element;
                contadorRepeticiones++;
//                console.log(valorElementoArrastrado+' aparece en '+ index);
            }
        });
    });
    return contenedorElementoRepetido;
}

function bloquearContenedorRepetidos(contenedores) {
    if (contenedores.length > 0) {
        $.each(contenedores, function (index, element) {
            $(this).find('.contenedorRespuesta').each(function (indexC, lementC) {
                $(this).droppable("disable");
            })
        });
    }
}
function desbloquearContenedorRepetidos() {
    $('.contenedorRespuesta').each(function (index, lement) {
        if ($(this).find('.textoCorrecta').length === 0) {

            $(this).droppable("enable");
        }
    });

}

function modificaEstilosCss(elemento) {
    var settingsCss = json[idPreguntaGlobal].css;
    if (settingsCss !== undefined) {
        if (settingsCss.anchoRespuestas !== undefined) {
//            anchoRespuestas = settingsCss.anchoRespuestas;
            $("#tarjetas li").css({width: settingsCss.anchoRespuestas});
        }
        if (settingsCss.tamanoFondoRespuesta !== undefined) {
            $("#tarjetas li").css({backgroundSize: settingsCss.tamanoFondoRespuesta});
        }
        if (settingsCss.alturaRespuestas !== undefined) {
            $("#tarjetas li").css({height: settingsCss.alturaRespuestas});
        }
        if (settingsCss.espacioRespuestas !== undefined) {
            $("#tarjetas li").css({marginBottom: settingsCss.espacioRespuestas});
        }
        if (settingsCss.altoContenedorRespuesta !== undefined) {
            $(".contenedorRespuesta").css({height: settingsCss.altoContenedorRespuesta});
        }
        if (settingsCss.altoColumnaContenedores !== undefined) {
            $("#columnaContenedores").css({height: settingsCss.altoColumnaContenedores});
        }
        if (settingsCss.tamanoFondoColumnaContenedores !== undefined) {
            $("#columnaContenedores").css({backgroundSize: settingsCss.tamanoFondoColumnaContenedores});
        }
        if (settingsCss.anchoColumnaContenedores !== undefined) {
            $("#columnaContenedores").css({width: settingsCss.anchoColumnaContenedores});
        }
        if (settingsCss.anchoColumnaRespuestas !== undefined) {
            $("#columnaRespuestas").css({width: settingsCss.anchoColumnaRespuestas});
        }
        if (settingsCss.bordeRespuestas !== undefined) {
            $("#tarjetas li").css({border: settingsCss.bordeRespuestas});
        }
        if (settingsCss.tamanoFuenteRespuestas !== undefined) {
            $("#tarjetas li p").css({fontSize: settingsCss.tamanoFuenteRespuestas})
        }
        if (settingsCss.colorFuenteRespuestas !== undefined) {
            $("#tarjetas li").css({color: settingsCss.colorFuenteRespuestas});
        }
        if (settingsCss.displayImgLineas !== undefined) {
            $(".imgLineas").css({display: settingsCss.displayImgLineas});
            $(".resp_arra_cont p").css("padding", 0);
        }
        if (settingsCss.bordeContenedorRespuesta !== undefined) {
            $(".contenedorRespuesta").css({border: settingsCss.bordeContenedorRespuesta});
        }
        if (settingsCss.bordeSubContenedor !== undefined) {
            $(".subContenedor").css({border: settingsCss.bordeSubContenedor});
        }
        if (settingsCss.displayFlechaContenedor !== undefined) {
            $('.flecha').css({display: settingsCss.displayFlechaContenedor});
        }
        if (settingsCss.maxAltoContenedorRespuesta !== undefined) {
            $('.contenedorRespuesta').css({maxHeight: settingsCss.maxAltoContenedorRespuesta});
        }

    }
}
function settingsCssRespuestas() {
    var settingsCss = json[idPreguntaGlobal].css;
    if (settingsCss !== undefined) {
        if (settingsCss.tamanoFondoRespuestaContenedor !== undefined) {
            $('.contenedorRespuesta').css({backgroundSize: settingsCss.tamanoFondoRespuestaContenedor});
        }
    }
//    if(settingsCss.tamañoFondoRespuestaContenedor !== undefined){
//       $(".contenedorRespuesta") 
//    }
}

function determinaAltoRespuestas(variante) {
    var tamaño = 0;
    switch (variante) {
        case "vertical":
        case "horizontal":
        case "arrastraMatriz":
        case "matrizHorizontal":
        case "ordenar":
            tamaño = 120;
            break;
        default :
            tamaño = alturaRespuestas;
    }
    return tamaño;
}

function determinaVariante() {
    var etiquetas = json[idPreguntaGlobal].pregunta.tipo === 'ordenar' && json[idPreguntaGlobal].pregunta.imagen === true && json[idPreguntaGlobal].respuestas[0].etiqueta !== undefined ? true : false; //etiquetas
    var ordenaFondoImagenTexto = json[idPreguntaGlobal].pregunta.tipo === 'ordenar' && json[idPreguntaGlobal].pregunta.imagen === true && json[idPreguntaGlobal].respuestas[0].etiqueta === undefined ? true : false;
    var arrastraImagen = json[idPreguntaGlobal].pregunta.tipo === 'ordenar' && json[idPreguntaGlobal].pregunta.imagen === true && json[idPreguntaGlobal].respuestas[0].etiqueta === undefined ? true : false;
    var vertical = json[idPreguntaGlobal].pregunta.tipo === "vertical" ? true : false;
    var horizontal = json[idPreguntaGlobal].pregunta.tipo === "horizontal" ? true : false;
    var arrastraMatriz = json[idPreguntaGlobal].contenedoresFilas !== undefined && json[idPreguntaGlobal].pregunta.tipo !== 'matrizHorizontal';
    var matrizHorizontal = json[idPreguntaGlobal].pregunta.tipo === 'matrizHorizontal';
    var strVariante = "No existe variante";
    if (etiquetas) {
        strVariante = "etiquetas";
    } else if (ordenaFondoImagenTexto) {
        strVariante = "ordenaTextoEnImagen";
    } else if (arrastraImagen) {
        strVariante = "arrastraImagenEnImagen";
    } else if (vertical) {
        strVariante = "vertical";
    } else if (horizontal) {
        strVariante = "horizotal"
    } else if (arrastraMatriz) {
        strVariante = "arrastraMatriz";
    } else if (matrizHorizontal) {
        strVariante = "matrizHorizontal";
    } else {
        strVariante = "ordenar";
    }
    return strVariante;
}
function eventoRevertirClick() {
    $('.revertir').on('click', function () {
        console.log('XX');
        var li = $(this).parent().attr('value');
        $('#li' + li).css({display: 'inline-table'});
        $(this).parent().parent().droppable('enable');
        $(this).parent().remove();
    });
}

function asignaAlturaRespuestas(altura, variante) {
    if (variante !== "etiquetas" && variante !== "arrastraImagenEnImagen" && variante !== "ordenaTextoEnImagen") {
        $('#tarjetas li').css({height: altura});
        $('.contenedorRespuesta').css({height: altura});
    }
}