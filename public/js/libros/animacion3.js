var anchoFrase;
var posicionFrase;
var ajusteMovimiento = 10;

$(document).ready(function() {
    creaEstructuraAnimacion2();               
});
function creaEstructuraAnimacion2 (idDiv){
    $( 'body' ).append(''+
    '<div id="principal_div"   style="position: absolute; clip: rect(0px,1024px,400px,0px);"   >' +
        '<div id="animacion0_div" class="alturaDivs"></div>' +
        '<div id="animacion1_div" class="divElementos_De_Animacion1 alturaDivs"></div>' +
        '<div id="animacion2_div" class="  alturaDivs"></div>' +        
    '</div>' +
    '<div id="divFondo" class="div_fondo"></div>');
    crearAnimacion('Leccion<br/>1', "Es lo mismo pero<br/>se ve diferente", "", '#E07000', '#FFFFFF', '220px', '230px');//# de sesión , Nombre de sesión parte 1, Nombre de sesión parte 2, Color del banner, Color de letra del # de la sesión, ancho de la imagen central, alto de la imagen central
}
function crearAnimacion (numeroSesion , frase1, frase2, colorFondo, colorNumSesion, anchoImagen, altoImagen){
    var tiempoDeEspera = 350;
    var duracionAnimacionMedia = 1000;
    var duracionAnimacionCorta = 300;

    $('#divFondo').css('background', colorFondo);        

    $('#animacion0_div').append('<div id="textoCentral_div"><div id="textoCentralInterior_div" class="nombre_sesion">'+frase1+'</div></div>');                
    $('#textoCentralInterior_div').css('white-space', 'normal');
    $('#animacion0_div').css('width', '1024px');

    $('#animacion1_div').append('<div id="idTriangulo">'+
        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="750" height="360" x="0">' +                
            '<polygon points="0,0 690,0 372,361 0,360" fill="#7FCCDF"/>' +
        '</svg>'+
    '</div>');
    $('#animacion2_div').append('<div id="idTriangulo">'+
        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1100" height="360" x="0">' +
            '<polygon points="1033,0 689,0 373,360 1036,360" fill="#D65F07"/>' +
        '</svg>'+
    '</div>');
    
    $('#animacion1_div').append('<div id="idNumeroSesion" class="numero_Sesion" >'+numeroSesion+'</div>');
    $('#idNumeroSesion').css('color', colorNumSesion);
    $('#idNumeroSesion').css('font-size', '33px');
    $('#idNumeroSesion').css('white-space', 'nowrap');
    $('#idNumeroSesion').css('left', '200px');
    $('#idNumeroSesion').css('top', '54px');
    $('#idNumeroSesion').css('text-align', 'center');
    $('#idNumeroSesion').css('line-height', '1');
    $('#idNumeroSesion').css('opacity', '0');
    $('#idNumeroSesion').delay(tiempoDeEspera).show().animate({ left: 460, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'easeOutQuart', complete: function(){
        $('#animacion1_div').delay(tiempoDeEspera).show().animate({ left: -450, opacity: '1' }, {duration: duracionAnimacionMedia, easing: 'easeOutBounce', complete: function(){                
        }});
        $('#animacion2_div').delay(tiempoDeEspera).show().animate({ left: 420, opacity: '1' }, {duration: duracionAnimacionMedia, easing: 'easeOutBounce', complete: function(){                    
        }});
    }});
}
function obtieneCentrado (idDiv){
    var dato = 512 - $('#'+idDiv).width() / 2;
    return dato;
}