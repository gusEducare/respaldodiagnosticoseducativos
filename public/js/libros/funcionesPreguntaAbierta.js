 /*  _____                            _                     _     _           _        
    |  __ \                          | |              /\   | |   (_)         | |       
    | |__) | __ ___  __ _ _   _ _ __ | |_ __ _       /  \  | |__  _  ___ _ __| |_ __ _ 
    |  ___/ '__/ _ \/ _` | | | | '_ \| __/ _` |     / /\ \ | '_ \| |/ _ \ '__| __/ _` |
    | |   | | |  __/ (_| | |_| | | | | || (_| |    / ____ \| |_) | |  __/ |  | || (_| |
    |_|   |_|  \___|\__, |\__,_|_| |_|\__\__,_|   /_/    \_\_.__/|_|\___|_|   \__\__,_|
                     __/ |                                                             
                    |___/ 
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com          
*********************************************************/
var primeraVez = true;

/***********************************************************
  F U N C I O N E S     P R E G U N T A    A B I E R T A
***********************************************************/
function estructuraHTMLpreguntaAbierta (estructuraBotones ){    
    $("#respuestasRadialGroup").html(estructuraBotones);
    aplicarEstilos();
}
function aplicarEstilos(){
    $('#respuesta_txt').addClass('clasePreguntaAbierta');
    $('.pregunta').html('<b>' + eliminarParrafos(json[idPreguntaGlobal].pregunta.t11pregunta) + '</b>');
//    $('#txtCrecer').html('Escribe la respuesta correcta...');
    $('#txtCrecer').css('color', '#000000');

    $('#txtCrecer').addClass('clasePreguntaAbierta_respuesta');        
}
function limpiarCaja(element) {
    if(primeraVez){
        element.value = '';
        $('#txtCrecer').css('color', '#000000');
        primeraVez = false;
    }    
}
function shuffle( myArr ){
  //return myArr.sort( function() Math.random() - 0.5 );
}