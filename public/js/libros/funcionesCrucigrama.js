/*______                   __                                  
 |      |.----.--.--.----.|__|.-----.----.---.-.--------.---.-.
 |   ---||   _|  |  |  __||  ||  _  |   _|  _  |        |  _  |
 |______||__| |_____|____||__||___  |__| |___._|__|__|__|___._|
                             |_____| 
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com     
*********************************************************/
var cuantasCeldasHorizontales = 13;//Tamaño de la cuadrícula
var cuantasCeldasVerticales = 7;
var informacion_arr = new Array();
var crucigramaMain;
var eGloblal;
var codigoDeCaracterDown;
var codigoDeCaracterPress;
var dispositivoMovil = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

function crucigramaPistas( len, pista, respuesta, firma, dir, xpos, ypos ){//longitud de respuesta, texto de pregunta, texto de respuesta, id, 0:vert 1:hor, posX, posY
    this.len = len;
    this.pista = pista;
    this.respuesta = respuesta;
    this.dir = dir;
    this.xpos = xpos;
    this.ypos = ypos;
    this.revelado = false;
    this.relacionado = false;
}
crucigramaPistas.prototype.completado = function(){
    return this.relacionado || this.revelado;
}
function listaDePistasCrucigrama(cruci, nombre, pistas, ns){//Lista de pistas horizontales y verticales
    this.cruci = cruci;
    this.name = nombre;
    this.pistas = pistas;
    this.ns = ns;
    this.IdSeleccionado = -1;
}
listaDePistasCrucigrama.prototype.renderear = function(){
    var cadena = "<div class='tituloOrientacion'>" + this.name + "</div>";
    for (var i=0; i < this.pistas.length; i++){
        cadena += "<div class='listaCrucigramaNormal' id='" + this.ns + i + "'><b>" + (i + 1) + ".</b> " + this.pistas[i].pista + "</div>";
    }
    return cadena;
}
listaDePistasCrucigrama.prototype.bind = function(){
    for (var i=0; i < this.pistas.length; i++){
        var elem = document.getElementById(this.ns + i);
        this.bindearElemento(elem, i);
    }
}
listaDePistasCrucigrama.prototype.unbind = function(){
    for (var i=0; i < this.pistas.length; i++){
        var elem = document.getElementById(this.ns + i);
        elem.onclick = null;
    }
}
listaDePistasCrucigrama.prototype.bindearElemento = function(elem, idx){
    var esteObjeto = this;
    elem.onclick = function(){
        esteObjeto.clickItem(idx);
    };
}
listaDePistasCrucigrama.prototype.clickItem = function(idx){
    this.itemSeleccionado(idx);
    this.cruci.focoEnCeldaAnterior();
    this.cruci.focoEnNuevaCelda(this.pistas[idx].xpos, this.pistas[idx].ypos, true, this.pistas[idx]);
}
listaDePistasCrucigrama.prototype.itemSeleccionado = function(idx){//Selecciona pregunta al dar clic en la celda
    if (this.IdSeleccionado != - 1){
        document.getElementById(this.ns + this.IdSeleccionado).className = "listaCrucigramaNormal";
    }
    if (idx != -1){
        document.getElementById(this.ns + idx).className = "listaSeleccionada";
    }
    this.IdSeleccionado = idx;
}
listaDePistasCrucigrama.prototype.obtenerIndiceParaPunto = function(x, y){
    for (var i=0; i < this.pistas.length; i++){
        if (this.pistas[i].dir == 0){
            if (y == this.pistas[i].ypos){
                if (x >= this.pistas[i].xpos && x < this.pistas[i].xpos + this.pistas[i].len){
                    return i;
                }
            }
        } else {
            if (x == this.pistas[i].xpos){
                if (y >= this.pistas[i].ypos && y < this.pistas[i].ypos + this.pistas[i].len){
                    return i;
                }
            }
        }
    }	
    return -1;
}
function menuDelCrucigrama(cruci){
    this.cruci = cruci;	
    this.listaH = cruci.listaH;
    this.listaV = cruci.listaV;
    this.footer = cruci.footer;	
    this.puedeRevelar = cruci.puedeRevelar;
    this.puedeChecar = cruci.puedeChecar;			
    this.pistas = cruci.pistas;
    this.menuActual = null;
    this.over = null;	
    this.cache = new Array();//estado_var de las celdas
    for (var i=0; i < this.cruci.h; i++){	
        for (var j=0; j < this.cruci.w; j++){	  
            var key = j + "_" + i; 
            this.cache[key] = -1; 	// -1 - vacía, 0 - llena, 1 - adivinada, 2 - revelada
        }  
    }	
    this.checados = 0;    
    this.deducidos = 0;	
    this.relacionados = 0;
    this.record = 0;	
    this.rango = -1;	
    this.xpos = cruci.xpos; 
    this.ypos = cruci.ypos;		
} 
menuDelCrucigrama.prototype.seteaEstadoCelda = function(x, y, valor){    
    this.cache[x + "_" + y] = valor;
}  
menuDelCrucigrama.prototype.obtenerEstadoDeCelda = function(x, y){
    return this.cache[x + "_" + y];
}
menuDelCrucigrama.prototype.bind = function(){
    this.inputCache = this.cruci.inputCache; 
    this.iniciaOn = new Date();	
}
menuDelCrucigrama.prototype.unbind = function(){
    this.inputCache = null;
}
menuDelCrucigrama.prototype.focoEnNuevaCelda = function(x, y){
    this.xpos = x; 
    this.ypos = y;
}
menuDelCrucigrama.prototype.crearMenu = function(){
    this.menuActual = this.crearMenu;
    var target = document.getElementById("pieDePagina_id");
    target.innerHTML = "";
    var esteObjeto = this;		
    var despliegaNombre = escape(this.name);
    despliegaNombre = despliegaNombre.replace(/%20/g, " ");	
    this.agregarAccion( 
        target, "Inicio", "", " ",
        function(){				 
            esteObjeto.cruci.bind();	            
            esteObjeto.crearMenuContextual();            
        }
    );	  
}
var esteObjetoGlobal;
menuDelCrucigrama.prototype.crearMenuContextual = function(){
    this.menuActual = this.crearMenuContextual;
    var target = document.getElementById("pieDePagina_id");
    target.innerHTML = "";
    var indiceH = this.listaH.obtenerIndiceParaPunto(this.xpos, this.ypos);
    var indiceV = this.listaV.obtenerIndiceParaPunto(this.xpos, this.ypos);
    if (!this.puedeChecar){
    } else {
        var esteObjeto = this;
        esteObjetoGlobal = esteObjeto;       
        var esteObjeto = this;
    }
    var existeSiguiente = false;//Revisa fin del juego
    for (var i=0; i< this.pistas.length; i++){
        if (!this.pistas[i].completado()){
            existeSiguiente = true;
            break;
        }
    }
    if (!existeSiguiente){
        this.over = true;
    }
    if (this.over){
        this.over = true;
        this.cruci.unbind();
    }
} 
menuDelCrucigrama.prototype.agregarAccion = function(target, etiqueta, area, dato, lambda){
    etiqueta = etiqueta.replace(" ", "&nbsp;");
    var elem = document.createElement("A");
    elem.innerHTML = etiqueta;
    elem.href = "";		    
    if (!lambda){        		
    } else {        
        elem.className = "accionesMenu"; 		
        var esteObjeto = this;
    }	
    target.appendChild(elem);
    esteObjeto.footer.estadoOcupado(area);
    lambda(); 
    return false;                
}
menuDelCrucigrama.prototype.obtieneValorActual = function(x, y){    
    var valor = this.inputCache.obtenerElemento(x, y).value;    
    if (valor == ""){//Aquí se revisaba si estaba llena o con espacio, el espacio ok
        valor = null;
    }	
    return valor;
}
menuDelCrucigrama.prototype.obtienePosicionCeldaLista = function(pista, left, top){
    var todo_arr = new Array();
    for (var i=0; i < pista.len; i++){
        todo_arr.push(this.caracterPorPosicion(pista, i));
    }
    return todo_arr;
}
menuDelCrucigrama.prototype.caracterPorPosicion = function(pista, offset){
    var pos = new function (){}	
    if (pista.dir == 0){	
        pos.x = pista.xpos + offset;
        pos.y = pista.ypos;
    } else {
        pos.x = pista.xpos; 
        pos.y = pista.ypos + offset;
    } 
    return pos;
}
menuDelCrucigrama.prototype.mostrarRespuesta = function(pista, codigoDeEstado){//Marca de otro color las celdas de las palabras encontradas
    for (var i=0; i < pista.len; i++){
        var pos = this.caracterPorPosicion(pista, i);	
        var input = this.inputCache.obtenerElemento(pos.x, pos.y);		
        if (!input.readOnly){			
            //input.readOnly = true;
            input.value = pista.respuesta.charAt(i).toUpperCase();
            this.seteaEstadoCelda(pos.x, pos.y, codigoDeEstado);
            var cell = document.getElementById("celdaCreada" + pos.x + "_" + pos.y);
            switch(codigoDeEstado){
                case 1:
                    cell.className = "enCeldasAdivinadas";
                    break;
                case 2:
                    cell.className = "enCeldasReveladas";
                    break;
                default:
                    //Error en código
            } 		 	
        }  
    } 	  
    this.cruci.invalidar();
}
menuDelCrucigrama.prototype.pintarPalabraErronea = function(pista, codigoDeEstado){//Marca de otro color las celdas de las palabras encontradas
    for (var i=0; i < pista.len; i++){
        var pos = this.caracterPorPosicion(pista, i);         
        document.getElementById( "elementoEntrada" + pos.x + "_" + pos.y ).style.color = "#CC0000";
    }
}
menuDelCrucigrama.prototype.despintarPalabraErronea = function(pista, codigoDeEstado){//Marca de otro color las celdas de las palabras encontradas    
    for (var i=0; i < pista.len; i++){
        var pos = this.caracterPorPosicion(pista, i);         
        document.getElementById( "elementoEntrada" + pos.x + "_" + pos.y ).style.color = "#000000";
    }
}
menuDelCrucigrama.prototype.revisarEstadoDePalabra = function(pista, id){//Verifica el estado_var del avance
    var datoEstado = new function (){};
    datoEstado.equivocado = 0;
    datoEstado.esCompletado = true;
    datoEstado.cadena = "";	
    datoEstado.id = id;	
    for (var i=0; i < pista.len; i++){
        var value;
        if (pista.dir == 0){
            value = this.obtieneValorActual(pista.xpos + i, pista.ypos);
        } else {
            value = this.obtieneValorActual(pista.xpos, pista.ypos + i);
        }
        if (value == null){
            datoEstado.esCompletado = false;
            datoEstado.cadena += ".";
        } else {
            datoEstado.cadena += value;
        }
        if (value != pista.respuesta.charAt(i).toUpperCase()){
            datoEstado.equivocado++;
        }
    }
    return datoEstado;
}
menuDelCrucigrama.prototype.obtieneRecordParaRelacion = function(pista){
    return pista.len; 
}
menuDelCrucigrama.prototype.obtenerDeduccionParaChecar = function(pista){
    var CHECAR_FRAGMENTO = 3;
    var deduction = (pista.len - pista.len % CHECAR_FRAGMENTO) / CHECAR_FRAGMENTO;
    if (deduction < 1){
            deduction = 1;
    }	
    return deduction;
}
menuDelCrucigrama.prototype.checarTodo = function(){
    var esChecado = 0;
    var esCorrecto = 0;
    for (var i=0; i < this.pistas.length; i++){
        try{
            if (this.pistas[i].completado()){    continue;    }
        } catch (e){
            console.log(e.message);
        }
        var datoEstado = this.revisarEstadoDePalabra(this.pistas[i], i);
        if (datoEstado.esCompletado){
            esChecado++;
            this.checados++;
            this.deducidos += this.obtenerDeduccionParaChecar(this.pistas[i]);                
            if (datoEstado.equivocado === 0){
                this.mostrarRespuesta(this.pistas[i], 1);
                this.record += this.obtieneRecordParaRelacion(this.pistas[i]);
                this.pistas[i].relacionado = true;
                this.pistas[i].revelado = false;
                esCorrecto++;
                this.relacionados++;                
                //console.log("Haaaas obtenido "+this.relacionados+" de "+this.pistas.length+" respuestas.");
            }else{                
                this.pintarPalabraErronea(this.pistas[i], 1);
                var tiempoPintado = setInterval(myTimer, 1000, this, i);
                function myTimer(this_, i) {                    
                    this_.despintarPalabraErronea(this_.pistas[i], 1);
                    clearInterval(tiempoPintado);                    
                }                
            }            
            siguientePregunta( c03id_tipo_pregunta, (i+1), datoEstado.cadena );
	}
    }        
};
function CrucigramaComponente(pistas, w, h ){//pistas: pistas, ancho del area de juego de las celdas, alto del área de juegos de las celdas    
    var ruta = "./";
    this.appInicial = ruta;
    this.w = w;				
    this.h = h;
    this.pistas = pistas;	
    this.xpos = 0;
    this.ypos = 0;	
    this.enfocado = null;	
    this.dir = 0;//Dirección del movimiento del foco cuando se escribe	        
    this.menu = null;	
    this.puedeRevelar = true;
    this.puedeChecar = true;	
    this.reordenarPistas();
    this.iniciado = false;
}
CrucigramaComponente.prototype.reordenarPistas = function(){
    var pistasViejas = [].concat(this.pistas);
    var sobreponer = [];
    for (var i=0; i < this.pistas.length; i++){
        for (var j=0; j < this.pistas.length; j++){
            if (this.pistas[i] == null || this.pistas[j] == null || i == j){
                continue;
            }
            if (this.pistas[i].xpos == this.pistas[j].xpos &&  this.pistas[i].ypos == this.pistas[j].ypos ){
                sobreponer.push(this.pistas[i]);
                sobreponer.push(this.pistas[j]);
                this.pistas[i] = null;
                this.pistas[j] = null;
            }
        }
    }
    this.pistasH = [];
    this.pistasV = [];
    for (var i=0; i < sobreponer.length; i++){
        if (sobreponer[i].dir == 0){
            this.pistasH.push(sobreponer[i]);
        } else {
            this.pistasV.push(sobreponer[i]);
        }
    }
    for (var i=0; i < this.pistas.length; i++){
        if (this.pistas[i] == null){
            continue;
        }
        if (this.pistas[i].dir == 0){
            this.pistasH.push(this.pistas[i]);
        } else {
            this.pistasV.push(this.pistas[i]);
        }
    } 	
    this.pistas = pistasViejas;
}
CrucigramaComponente.prototype.iniciar = function(){ 
    this.listaH = new listaDePistasCrucigrama(this, "Horizontal", this.pistasH, "pistaHorizontal", "H");
    this.listaV = new listaDePistasCrucigrama(this, "Vertical", this.pistasV, "pistaVertical", "V");    
    this.footer = new creaPiePagCrucigrama(this);
    this.menu = new menuDelCrucigrama(this);
}
CrucigramaComponente.prototype.renderear = function(){    
    var cadena = "";
    cadena += "<table border='0' cellspacing='0' cellpadding='0' style='border-collapse: collapse;'>";
    for (var i=0; i < this.h; i++){	
        var filaNueva = "<tr>";
        for (var j=0; j < this.w; j++){
            filaNueva += "<td class='enCeldasVacias' id='celdaCreada" + j + "_" + i + "'></td>";
        }
        cadena += filaNueva + "</tr>";
    }
    cadena += "</table>";
    var TAMANOCELDASH = 25;
    var MIN_ANCHO_DIV = 400;
    var anchoDelDiv = this.w * TAMANOCELDASH;
    if (anchoDelDiv < MIN_ANCHO_DIV){
        anchoDelDiv = MIN_ANCHO_DIV;
    }
    anchoDelDiv += "px";
    var target = document.getElementById("pieDePagina_id");
    var target = document.getElementById("divEstado");
    var target = document.getElementById("miCrucigrama_id");
    target.innerHTML = cadena;
    for (var i=0; i < this.listaH.pistas.length; i++){
            this.rendereoHorizontal(this.listaH.pistas[i]);
    }
    for (var i=0; i < this.listaV.pistas.length; i++){
            this.rendereoVertical(this.listaV.pistas[i]);
    }
    var target = document.getElementById("listaHorizontal_div");
    target.innerHTML = "";
    target.className = "panelOculto_div";
	
    var target = document.getElementById("listaVertical_div");
    target.innerHTML = "";
    target.className = "panelOculto_div";		
}
CrucigramaComponente.prototype.rendereoVertical = function(pista){    
    for (var i=0; i < pista.len; i++){	
        var key = "celdaCreada" + pista.xpos + "_" + (pista.ypos + i);
        var cell = document.getElementById(key);
        cell.className = "enCeldasLlenas";
    } 
}
CrucigramaComponente.prototype.rendereoHorizontal = function(pista){
    for (var i=0; i < pista.len; i++){	
        var key = "celdaCreada" + (pista.xpos + i) + "_" + pista.ypos
        var cell = document.getElementById(key);
        if(i===0){
            cell.className = "enCeldasLlenas pal_" + i;            
        }else{
            cell.className = "enCeldasLlenas";
        }
    }
}
CrucigramaComponente.prototype.llenadoVertical = function(pista, idx){
    for (var i=0; i < pista.len; i++){
        var key = "celdaCreada" + pista.xpos + "_" + (pista.ypos + i);
        var cell = document.getElementById(key);
        this.llenadoEn(cell, pista.xpos, pista.ypos + i, i, idx, 1);
        this.menu.seteaEstadoCelda(pista.xpos, pista.ypos + i, 0);
    }
}
CrucigramaComponente.prototype.llenadoHorizontal = function(pista, idx){
    for (var i=0; i < pista.len; i++){
        var key = "celdaCreada" + (pista.xpos + i) + "_" + pista.ypos;
        var cell = document.getElementById(key);
        this.llenadoEn(cell, pista.xpos + i, pista.ypos, i, idx, 0);
        this.menu.seteaEstadoCelda(pista.xpos + i, pista.ypos, 0);
    }
}
CrucigramaComponente.prototype.llenadoEn = function(cell, x, y, i, idx, dir){
    if (i == 0){
        cell.style.backgroundImage = "url(\"" + this.appInicial + "/images/" + (idx + 1) + ".png\")";
    }
    cell.innerHTML = "<input id='elementoEntrada" + x + "_" + y + "' class='celdasDeEntrada' autocomplete='off' type='text' size='1' maxlength='1' value=''>";
}
CrucigramaComponente.prototype.bind = function(){		
    var target = document.getElementById("listaHorizontal_div");
    target.className = "panel_div";
    target.innerHTML = this.listaH.renderear();			
    var target = document.getElementById("listaVertical_div");
    target.className = "panel_div";		
    target.innerHTML = this.listaV.renderear();    
    var contadorH = 0;//Llena en la tabla con caracteres de entrada
    for (var i=0; i < this.listaH.pistas.length; i++){	
        this.llenadoHorizontal(this.listaH.pistas[i], contadorH);
        contadorH++;
    }	  
    var contadorV = 0;
    for (var i=0; i < this.listaV.pistas.length; i++){	
        this.llenadoVertical(this.listaV.pistas[i], contadorV);
        contadorV++;
    } 	
    this.inputCache = new elementoGridCache(this.w, this.h, "elementoEntrada");    
    for (var i=0; i < this.h; i++){
        for (var j=0; j < this.w; j++){
            this.bindearElemento(j, i);
        }
    }
    this.menu.bind();
    this.listaH.bind();
    this.listaV.bind();
    this.iniciado = true;
}
CrucigramaComponente.prototype.unbind = function(){	
    for (var i=0; i < this.h; i++){	
        for (var j=0; j < this.w; j++){	 
            var target =  this.inputCache.obtenerElemento(j, i);
            if (target != null){
                target.onclick = null;
                target.onkeydown = null;
                target.onchange = null;
            }
        }
    }
    this.listaH.unbind();
    this.listaV.unbind();	
    this.footer.unbind();
    this.menu.unbind(); 
}
CrucigramaComponente.prototype.bindearElemento = function(x, y){	
    var target =  this.inputCache.obtenerElemento(x, y);
    if (target != null){
        var esteObjeto = this;	
        
        dispositivoMovil = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
        dispositivoMovil = esMobile();
        /*eventoPress = dispositivoMovil ? "touchstart" : "mousedown";
        eventoRelease = dispositivoMovil ? "touchend" : "mouseup";
        eventoMove = dispositivoMovil ? "touchmove" : "mousemove";*/
                
        target.ontouchstart = function(){
            //console.log('ontouchstart');
        }
        target.onclick  = function(){//console.log('onclick');
            esteObjeto.focoEnCeldaAnterior();
            esteObjeto.focoEnNuevaCelda(x, y, false);
        }  
        target.onkeydown = function(e){//console.log('onkeydown value'+this.value);
            var caracter = this.value;
            codigoDeCaracterDown = caracter.charCodeAt(0);            
            return esteObjeto.eventoTeclaAbajo(x, y, e);//No aplica en móviles teclas arriba, abajo, izquierda, derecha
        }
        if(dispositivoMovil){//Si es un dispositivo móvil
            target.onkeyup = function(e){//console.log('onkeyup');
                var caracter = this.value;
                codigoDeCaracterPress = caracter.charCodeAt(0);   
                return esteObjeto.eventoPresionarTecla(x, y, e);            
            }
        }else{//Sino es un dispositivo móvil
            target.onkeypress = function(e){//console.log('onkeypress ');
                var caracter = this.value;
                codigoDeCaracterPress = caracter.charCodeAt(0);            
                return esteObjeto.eventoPresionarTecla(x, y, e);
            }
        }                
    }
}
CrucigramaComponente.prototype.focoLista = function(x, y){
    var indiceH = this.listaH.obtenerIndiceParaPunto(x, y);
    this.listaH.itemSeleccionado(indiceH);
    var indiceV = this.listaV.obtenerIndiceParaPunto(x, y);
    this.listaV.itemSeleccionado(indiceV);
}
CrucigramaComponente.prototype.focoCeldaLista = function(todo_arr, enfocado){
    for (var i=0; i < todo_arr.length; i++){
        var cell = document.getElementById("celdaCreada" + todo_arr[i].x + "_" + todo_arr[i].y);			
        var codigoDeEstado = this.menu.obtenerEstadoDeCelda(todo_arr[i].x, todo_arr[i].y);
        if (cell != null){
            if (enfocado && codigoDeEstado == 0){                
            } else {
                this.restaurarEstadoDeCelda(cell, todo_arr[i].x, todo_arr[i].y);
            }
        }
    } 		
}
CrucigramaComponente.prototype.focoPalabraAnterior = function(){ 
    if (this.enfocado != null){
        this.focoCeldaLista(
        this.menu.obtienePosicionCeldaLista(this.enfocado), false );	  
        this.enfocado = null;
    } 
}
CrucigramaComponente.prototype.focoNuevaPalabra = function(x, y){
    var indiceH = this.listaH.obtenerIndiceParaPunto(x, y);
    var indiceV = this.listaV.obtenerIndiceParaPunto(x, y);
    var pista = null;
    if (indiceH != -1 && indiceV != -1){
        if (this.dir == 0){
            pista = this.listaH.pistas[indiceH];
        } else {
            pista = this.listaV.pistas[indiceV];
        }
    } else {
        if (indiceH != -1){
            pista = this.listaH.pistas[indiceH];
        }
        if (indiceV != -1){
            pista = this.listaV.pistas[indiceV];
        }
    }
    return pista;
}  
CrucigramaComponente.prototype.focoEnNuevaCelda = function(x, y, focus, pista){
    if (pista != null){
        this.enfocado = pista;
    } else {
        this.enfocado = this.focoNuevaPalabra(x, y);
    }
    if (this.enfocado != null){
        this.dir = this.enfocado.dir;
        this.focoCeldaLista( this.menu.obtienePosicionCeldaLista(this.enfocado), true );
    }
    var target = document.getElementById("celdaCreada" + x + "_" + y);			
    if (target != null){             
            this.focoLista(x, y);
            if (focus){
                var target = this.inputCache.obtenerElemento(x, y);		
                if(dispositivoMovil){//Si es un dispositivo móvil
                    $( "#"+target.id ).focus();
                }else{//Sino es un dispositivo móvil
                    target.focus();
                }
            }
    }
    this.xpos = x;
    this.ypos = y;
    this.menu.focoEnNuevaCelda(x, y);
}
CrucigramaComponente.prototype.focoEnCeldaAnterior = function(){ 
    var target = document.getElementById("celdaCreada" + this.xpos + "_" + this.ypos);			
    if (target != null){  
        this.restaurarEstadoDeCelda(target, this.xpos, this.ypos);
    }  
    this.focoPalabraAnterior();
} 
CrucigramaComponente.prototype.invalidar = function(){
    this.focoEnCeldaAnterior();  
    this.focoEnNuevaCelda(this.xpos, this.ypos, true);
}
CrucigramaComponente.prototype.restaurarEstadoDeCelda = function(target, x, y){
    var codigoDeEstado = this.menu.obtenerEstadoDeCelda(x, y);
    switch(codigoDeEstado){
        case -1:
            target.className = "enCeldasVacias";    break;
        case 0:
            target.className = "enCeldasLlenas";    break;
        case 1:
            target.className = "enCeldasAdivinadas";break;
        case 2:
            target.className = "enCeldasReveladas"; break;
        default:
            //Error en código
    }
}
CrucigramaComponente.prototype.esCaracterValido = function (c){
    return (c >= "A" && c <= "Z") || c == " ";
}
CrucigramaComponente.prototype.moveraCeldaPrevia = function(x, y){       
    var ultimaX = x;
    var ultimaY = y;
    var tiempoAparecerLetra;
    var idPosicionado = 'celdaCreada'+x+'_'+y;    
    var targetSinCodigo = this.inputCache.obtenerElemento(x, y);
    
    if (this.dir == 0) {    x--;     } else {    y--;    } 
    var codigoDeEstado = this.menu.obtenerEstadoDeCelda(x, y);    
    if (codigoDeEstado != -1){
        var target = this.inputCache.obtenerElemento(x, y);
        var targetTemp = this.inputCache.obtenerElemento(ultimaX, ultimaY);        
        var valorTemporal = target.value;        
        var claseContenedora;
        var idModificado = target.id;
        idModificado = idModificado.replace('elementoEntrada', 'celdaCreada');        
        if (target != null){
            if(targetTemp.value != ''){//Si está llena la caja bórrala primero
                claseContenedora = document.getElementById( idPosicionado ).className;                
                if(claseContenedora === 'enCeldasAdivinadas'){//Si es una caja verde (resuelta)                    
                    if(dispositivoMovil){//Si es un dispositivo móvil
                        $( "#"+target.id ).focus();
                    }else{//Sino es un dispositivo móvil
                        target.focus();
                    }
                    tiempoAparecerLetra = setInterval(tiempoLetra, 10, target, valorTemporal);
                }else{//No es una celda verde (resuelta)
                    //Borra el contenido de la caja.
                }
            }else{
                this.focoEnCeldaAnterior();
                this.focoEnNuevaCelda(x, y, false);
                if(dispositivoMovil){//Si es un dispositivo móvil
                    $( "#"+target.id ).focus();
                    console.log('mueve');
                }else{//Sino es un dispositivo móvil
                    target.focus();
                }
                console.log('FOCO');
                claseContenedora = document.getElementById( idModificado ).className;
                if(claseContenedora === 'enCeldasAdivinadas'){//Si es una caja verde (resuelta)
                    tiempoAparecerLetra = setInterval(tiempoLetra, 10, target, valorTemporal);
                }
            }
        }
    }else{
        claseContenedora = document.getElementById( idPosicionado ).className;        
        if(claseContenedora === 'enCeldasAdivinadas'){//si no hay código y es una celda verde (resuelta)
            tiempoAparecerLetra = setInterval(tiempoLetra, 10, targetSinCodigo, targetSinCodigo.value);
        }
    }
    function tiempoLetra(target, valorTemporal) {
        target.value = valorTemporal;
        clearInterval(tiempoAparecerLetra);        
    }
}
CrucigramaComponente.prototype.moveraCeldaProxima = function(x, y){    
    if (this.dir == 0) {    x++;     } else {    y++; 	}
    var codigoDeEstado = this.menu.obtenerEstadoDeCelda(x, y);
    if (codigoDeEstado != -1){
        var target = this.inputCache.obtenerElemento(x, y);        
        if (target != null){ 
            this.focoEnCeldaAnterior(); 
            this.focoEnNuevaCelda(x, y, false);            
            if(dispositivoMovil){//Si es un dispositivo móvil
                $( "#"+target.id ).focus();
            }else{//Sino es un dispositivo móvil
                target.focus();
            }
            //$( "#"+target.id ).focus();
            //target.focus();//ya no
        }
    }
}
CrucigramaComponente.prototype.eventoPresionarTecla = function(x, y, e){// espaciadora: 32    
    dispositivoMovil = esMobile();    
    var idPosicionado = 'celdaCreada'+x+'_'+y;
    var keyCode;
    if (!e) {    e = window.event;    }    
    if(dispositivoMovil){//Si es un dispositivo móvil
        keyCode = codigoDeCaracterPress;
    }else{//Sino es un dispositivo móvil
        keyCode = (e.which) ? e.which : e.keyCode;
    }    
    
    var c = String.fromCharCode(keyCode).toUpperCase();		
    var palabraCompletada;
    if (keyCode == 32){
        c = " ";
    } 			    
    if (this.esCaracterValido(c)){         
        var target = this.inputCache.obtenerElemento(x, y);	 			
        if (!target.readOnly){             
            var claseContenedora = document.getElementById( idPosicionado ).className;                
            if(claseContenedora === 'enCeldasAdivinadas'){
                //es celda verde
            }else{
                target.value = c.toUpperCase();
            }
        }
        palabraCompletada = seCompletoPalabra(target);
        
        if(palabraCompletada){            
            esteObjetoGlobal.checarTodo();
        }                                
        this.moveraCeldaProxima(x, y);
    }				
    return false;
}
CrucigramaComponente.prototype.eventoTeclaAbajo = function(x, y, e){// izquierda: 37, arriba: 38, derecha: 39, abajo: 40, backspace: 8    
    if (!e) {
        e = window.event;
    }
    var keyCode = (e.which) ? e.which : e.keyCode;
    
    var dir = (keyCode >= 37 && keyCode <= 40) || keyCode ==8;
    if (dir) {
        var target = null;
        switch(keyCode){
            case 8://case 8 backspace
                this.moveraCeldaPrevia(x, y);
                break;
            case 37:
                while(true){
                    if (x > 0){
                        x = x - 1;
                        target = this.inputCache.obtenerElemento(x, y);
                        if (target != null){
                            break;
                        }
                    } else {
                        break;
                    }
                }
            break;
            case 39:
                while(true){
                    if (x < this.w - 1){
                        x = x + 1;
                        target = this.inputCache.obtenerElemento(x, y);
                        if (target != null){
                            break;
                        }
                    } else {
                        break;
                    }
                }
            break;
            case 38:
                while(true){
                    if (y > 0){
                        y = y - 1;
                        target = this.inputCache.obtenerElemento(x, y);
                        if (target != null){
                                break;
                        }
                    } else {
                        break;
                    }
                }
            break;
            case 40:
                while(true){
                    if (y < this.h - 1){
                        y = y + 1;
                        target = this.inputCache.obtenerElemento(x, y);
                        if (target != null){
                            break;
                        }
                    } else {
                        break;
                    }
                }
            break;
        } 
        if (target != null){            
            this.focoEnCeldaAnterior();
            this.focoEnNuevaCelda(x, y, false);
            if(dispositivoMovil){//Si es un dispositivo móvil
                $( "#"+target.id ).focus();
            }else{//Sino es un dispositivo móvil
                target.focus();
            }
        }	
    }	
    return true; 
} 
function creaPiePagCrucigrama(cruci){
    this.cruci = cruci;	
}
creaPiePagCrucigrama.prototype.unbind = function(){
    clearInterval(this.tiempoDeReloj);
    this.tiempoDeReloj = null;
}
creaPiePagCrucigrama.prototype.estadoOcupado = function(text){
    var target = document.getElementById("divEstado");
    target.innerHTML = "" + text + "";
    target.className = "enEstadoOcupado";
}
creaPiePagCrucigrama.prototype.estadoConError = function(text){
    var target = document.getElementById("divEstado");
    target.innerHTML = "" + text + "";
    target.className = "enEstadoErroneo";
}
function elementoGridCache(w, h, ns){
    this.ns = ns;
    this.cache = new Array();
    for (var i=0; i < h; i++){
        for (var j=0; j < w; j++){
            var key;
            key = this.ns + j + "_" + i;
            this.cache[key] = document.getElementById(key);
        } 
    }
}
elementoGridCache.prototype.obtenerElemento = function(x, y){
    return this.cache[this.ns + x + "_" + y];
}
function seCompletoPalabra(target){
    var elementosPalabra_arr = new Array();
    var idTarget = target.id;
    var arrPos = idTarget.split('elementoEntrada');
    arrPos = arrPos[1].split('_');
    var posX = parseInt(arrPos[0]);
    var posY = parseInt(arrPos[1]);
    var r = 0;
    var numeroColumnas = $( '.miCrucigrama table tr' ).first().find('td').length;
    var numeroRenglones = $( '.miCrucigrama table tr' ).length;
    var completadaX = true;
    var completadaY = true;

    for(var t=posX;t>=0;t--){
        if( $( '#elementoEntrada'+t+'_'+posY ).size() ){
            elementosPalabra_arr[r] = '#elementoEntrada'+t+'_'+posY;
            r++;                 
        }else{
            break;
        }
    }
    for(var t=posX+1;t<=numeroColumnas;t++){
        if( $( '#elementoEntrada'+t+'_'+posY ).size() ){
            elementosPalabra_arr[r] = '#elementoEntrada'+t+'_'+posY;           
            r++;                 
        }else{
            break;
        }
    }
    for(var u=0;u<elementosPalabra_arr.length;u++){        
        if( $( elementosPalabra_arr[u] ).val() === ""){
            completadaX = false;
        }
    }
    
    completadaX = elementosPalabra_arr.length > 1 ? completadaX : false;
    if(completadaX){
        return completadaX;
    }
    r = 0;
    elementosPalabra_arr = new Array();
    for(var t=posY;t>=0;t--){
        if( $( '#elementoEntrada'+posX+'_'+t ).size() ){
            elementosPalabra_arr[r] = '#elementoEntrada'+posX+'_'+t;            
            r++;                 
        }else{
            break;
        }
    }
    for(var t=posY+1;t<=numeroRenglones;t++){
        if( $( '#elementoEntrada'+posX+'_'+t ).size() ){
            elementosPalabra_arr[r] = '#elementoEntrada'+posX+'_'+t;            
            r++;                 
        }else{
            break;
        }
    }
    for(var u=0;u<elementosPalabra_arr.length;u++){        
        if( $( elementosPalabra_arr[u] ).val() === ""){
            completadaY = false;
        }
    }
    
    completadaY = elementosPalabra_arr.length > 1 ? completadaY : false;    
    return completadaY;
}
function eliminaEspacioEnBlanco(texto){
    if(typeof texto !== 'undefined'){
        texto = texto.replace('&nbsp;', ' ');
    }
    return texto;
}
function eliminaBr(texto){
    if(typeof texto !== 'undefined'){
        texto = texto.replace('<br />', '');        
    }
    return texto;
}
function funcionesCrucigrama(cruci){
    cruci.iniciar();
    cruci.renderear();
    cruci.menu.crearMenu();
}
function estructuraHTMLcrucigrama(estructuraBotones){
    $("#respuestasRadialGroup").html(estructuraBotones);
    llenarDatos();
    funcionesCrucigrama(crucigramaMain);
}
function llenarDatos(){
    var respuesta;
    var pregunta;
    var longitud;
    var posicionamiento_arr = posicionesCrucigrama_arr;
    informacion_arr = new Array();
        
    for(var i=0;i<preguntasArr.length;i++){
        respuesta = respuestasArr[i];
        pregunta = preguntasArr[i];
        respuesta = eliminaAcentos(respuesta);
        respuesta = eliminarParrafos(respuesta);
        longitud = respuesta.length;
        //pregunta = eliminaAcentos(pregunta);
        pregunta = eliminaEspacioEnBlanco(pregunta);
        pregunta = eliminaBr(pregunta);
        informacion_arr[i] = new crucigramaPistas(longitud, pregunta, respuesta, "", posicionamiento_arr[i].direccion, posicionamiento_arr[i].datoX, posicionamiento_arr[i].datoY );
    }
    
    if( typeof json[idPreguntaGlobal].pregunta.ancho === 'undefined' ){        
    }else{
        cuantasCeldasHorizontales = json[idPreguntaGlobal].pregunta.ancho;
    }
    if( typeof json[idPreguntaGlobal].pregunta.alto === 'undefined' ){        
    }else{
        cuantasCeldasVerticales = json[idPreguntaGlobal].pregunta.alto;
    }
    cuantasCeldasHorizontales = cuantasCeldasHorizontales + 2;
    cuantasCeldasVerticales = cuantasCeldasVerticales + 2;
    
    crucigramaMain = new CrucigramaComponente( informacion_arr, cuantasCeldasHorizontales, cuantasCeldasVerticales);
}