/*_______                         __                       ______              __               
 |   _   |.----.----.---.-.-----.|  |_.----.---.-.        |      |.-----.----.|  |_.---.-.-----.
 |       ||   _|   _|  _  |__ --||   _|   _|  _  |        |   ---||  _  |   _||   _|  _  |__ --|
 |___|___||__| |__| |___._|_____||____|__| |___._|        |______||_____|__|  |____|___._|_____| 
 @author:           Grupo Educare S.A. de C.V.
 @desarrolladores   Greg: gregorios@grupoeducare.com
 Juan Pablo: jpgomez@grupoeducare.com
 Juan José: jlopez@grupoeducare.com
 @estilos css:      Claudia:  cgochoa@grupoeducare.com      
 *********************************************************/
var cardNumberGlobal;
function estructuraHTMLarrastraCortas(estructuraBotones) {
    $("#respuestasRadialGroup").html(estructuraBotones);
}
function creaArrastraCortas() {
    var finalContieneCaja = false;//true
    var palabras = new Array();
    var dato = '';
    var texto;
    var sonidosArr;
    var etiquetaSonido = '';
    var dimensiones = new Object();

    if (pizarron) {
        finalContieneCaja = typeof respuestasArr[respuestasArr.length - 1] !== 'undefined' ? true : false;
        $("#pilaDeCartas").css('height', '220px');
        $("#pilaDeCartasExtra").css('height', '220px');
        if (tipoMultimedia === "audio") {
            imagenesPizarron = ["5audio.png", "4audio.png", "1audio.png", "3audio.png", "6audio.png"];
            if (idioma === "es") {
                sonidosArr = ["5.mp3", "4.mp3", "1.mp3", "3.mp3", "6.mp3"];
            }
            if (idioma === "en") {
                sonidosArr = ["5en.mp3", "4en.mp3", "1en.mp3", "3en.mp3", "6en.mp3"];
            }
            for (var i = 0; i < sonidosArr.length; i++) {
                etiquetaSonido = etiquetaSonido + '<audio id="audio' + i + '"><source src="http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/' + sonidosArr[i] + '"></source></audio>';
            }
        }
        if (tipoMultimedia === "imagen") {
            imagenesPizarron = ["5.png", "4.png", "1.png", "3.png", "6.png"];
        }
    } else {
        tipoMultimedia = "texto";//siempre texto
        finalContieneCaja = typeof respuestasArr[respuestasArr.length - 1] !== 'undefined' ? true : false;
    }

    $('#pilaDeCartas').html('');
    $('#pilaDeCartasExtra').html('');
    $('#espaciosDeCartas').html('');

    // Crea la pila de cartas revueltas      
    if (!finalContieneCaja) {
        respuestasArr.pop();//Monitorear elimina el último elemento del arreglo  
    }
    if (respuestasArr.length <= 4) { // 4 es el tamaño de elementos que caben en un renglon
        $('#espaciosDeCartas').css('top', '90px');
    } else {
        $('#espaciosDeCartas').css('top', '96px');
    }
    for (var i = 0; i < respuestasArr.length; i++) {
        if (pizarron) {
            if (tipoMultimedia === "imagen") {
                dato = respuestasArr[i];
                texto = '';
            }
            if (tipoMultimedia === "audio") {
                dato = '<img id="' + i + '" src="http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/' + imagenesPizarron[i] + '"/>' + etiquetaSonido;
                texto = '';
            }
        } else {
            if (tipoMultimedia === "texto") {
                dato = '';
                texto = respuestasArr[i];
            }
        }

        //Áreas vacías        
        $('<div><span></span></div>').prepend(dato).attr('id', 'vacio' + i)
                .appendTo('#pilaDeCartasExtra').draggable({
            containment: '#content',
            stack: '#pilaDeCartasExtra div'
        });

        $('<div><span style="text-align: left;">' + texto + '</span></div>').data('text', texto)
                .data('number', i).prepend(dato).attr({id: 'card' + i, class: 'respuestasArrastra'})
                .appendTo('#pilaDeCartas').draggable({
            containment: '#content',
            stack: '#pilaDeCartas div',
            cursor: 'move',
            revert: true,
            drop: handleCardReturn
        });
        $('#vacio' + i).css({width: $('#card' + i).width()});
        $('#vacio' + i).css({height: $('#card' + i).height()});
        var alturaRespuesta = $(".respuestasArrastra").height();
        var topImg = (alturaRespuesta - 30) / 2;
        $('<style>.respuestasArrastra:before {margin-top: ' + topImg + 'px;}</style>').appendTo('.respuestasArrastra');
        //$('#respuestasRadialGroup').css({width:"96%"}); // Revisar el comportamiento de este estilo JPGA
        if (i === respuestasArr.length - 1) {
            $('#card' + i).css('clear', 'left');//Al inicio aplica un clear a la última caja superior verde para evitar que se desacomoden las demás
        }

        $('#card' + i).mouseup(function () {
            var idActual = this;
            var idActual2 = $(this).attr('id');
            arrTmpInit = idActual2.split('card');
            sueltaEnDrop = false;
            $(idActual).draggable('option', 'revert', false);

            myInterval = setInterval(function () {
                clearInterval(myInterval);//Elimina el timer

//            if(sueltaEnDrop){
                //No hace nada - envía a revisión //console.log('Soltó correctamente.');                
//                siguientePregunta(c03id_tipo_pregunta, parseInt(arrTmpInit[1]) + 1);
//            }else{//Si soltaste la caja fuera del drop regresa a su posición                                                                
                if ($(idActual).hasClass('dropped-text')) {//Si es una caja dropeada  
                } else {
                    if ($(idActual2).hasClass('dropped-text')) {
                        animarCajaVerde(idActual);
                    } else {
                        //No hace nada
                    }
                    animarCajaVerde(idActual);
                }
//            }                        
            }, 100);//Tiempo de espera para iniciar la animación
        });

        if (tipoMultimedia === "audio") {
            $('#card' + i).click(function (event) {//Resuelve los iconos
                var audio = document.getElementById("audio" + event.target.id);
                audio.play();
            });
        }
        if (pizarron) {
            $('#card' + i).css('height', 'auto');
            $('#card' + i).css('background-color', 'transparent');
            $('#card' + i).css('border', 'transparent');
        }
    }

    for (var i = 1; i <= respuestasArr.length; i++) {
        palabras[i - 1] = ''; //Crea las cajas inferiores
    }
    var saltoLinea;
    for (var i = 1; i <= preguntasArr.length; i++) {
        // mismo caso de las preguntas de tipo corta, hay que agregar este elemento:
        // <div class="texto-respuesta-spacer"></div> el detalle vino que cuando
        // se agrega, también afecta al funcionamiento del tipo de pregunta, se
        // tendría que revisar los estilos para corregir todas las fallas                

        if (typeof obj.pregunta.pintaUltimaCaja === 'undefined') {
            //Sino la trae no altera el valor de pintarUltimaCaja
        } else {
            if (obj.pregunta.pintaUltimaCaja === false) {
                pintarUltimaCaja = false;
            } else {
                pintarUltimaCaja = true;
            }
        }

        if (i === preguntasArr.length && !pintarUltimaCaja) {
            dato += preguntasArr[i - 1];//+'.'; //Agrega un punto final en el último enunciado
            dato = dato.replace(/<p>/g, "");
            dato = dato.replace(/<\/p>/g, ""); // Sanitizo último campo
        } else {
            var pregArr = preguntasArr[i - 1];
            if (i === 1) {
                saltoLinea = '';//Se agrega un salto de línea sólo antes de la primera caja inferior punteada
            } else {
                saltoLinea = '';//No se agregan saltos de línea en las cajas inferiores punteadas
            }
            palabras[i - 1] = '';//Se setean puntos suspensivos sólo para que no se quede vacio y se solucione el conflicto de las cajas punteadas cortadas.                                
            dato += saltoLinea + pregArr.replace(/<\/p>/, "") + '<span id="texto' + i + '" style="line-height:45px;"  class="texto-respuesta-sub">' + palabras[i - 1] + '</span>';
            dato = dato.replace(/<p>/g, "");//Se eliminan los párrafos (abrir párrafo)
            dato = dato.replace(/<\/p>/g, "");//Se eliminan los párrafos (cerrar párrafo)                         
            //console.log('dato '+dato);
        }

        if (pizarron) {
            $('#cajaPunteada' + i).css('font-size', '40px');
            $('#espaciosDeCartas').css('margin', '150px 0px 0px 40px');

            if (tipoMultimedia === "texto") {
                $('#cajaPunteada' + i).css('height', 'auto');
            }
            if (tipoMultimedia === "imagen" || tipoMultimedia === "audio") {
                $('#cajaPunteada' + i).css('height', '160px');
                $('.texto-respuesta-sub').css('height', '62px').css('width', '60px');
            }
        } else {
            //$('#espaciosDeCartas').css('margin', '20px 0px 0px 0px');
        }
        $('.ui-droppable').data('elemDentro', 0);

        $('.texto' + i).droppable('disable');
        $('#texto' + i).droppable('disable');

        $('#texto' + i).removeClass('ui-droppable');
        $('#texto' + i).removeClass('ui-droppable-disabled');
        $('#texto' + i).removeClass('ui-state-disabled');
    }
    var ultimoCaracter = dato.substring(dato.length - 1);
    if (ultimoCaracter !== '.') {
        if (!obj.pregunta.ocultaPuntoFinal) {
            dato += '.';
        }
    }


    if (typeof obj.pregunta.textosLargos === 'undefined' || obj.pregunta.textosLargos === 'no') {
        medida = 15;
    } else {//Si si son textos largos        
        var cuantosBR = calculaRepetidos(dato, "<br>");
        var datoTemp = dato;
        var estilosFijos = 'top: 90px; position: absolute; width: 100%';
        for (var t = 0; t < cuantosBR; t++) {
            if (t == 0) {
                datoTemp = datoTemp.replace('<br>', 'brInicial');//<div><br>
            } else {
                datoTemp = datoTemp.replace('<br>', 'br' + t);//</div><div><br>
            }
        }
        for (var t = 0; t < cuantosBR; t++) {
            if (t == 0) {
                datoTemp = datoTemp.replace('brInicial', '<div id="br' + t + '" style="' + estilosFijos + '"><br>');
            } else {
                datoTemp = datoTemp.replace('br' + t, '</div><div id="br' + t + '" style="' + estilosFijos + '"><br>');
            }
        }
        datoTemp = datoTemp + '</div>';
        dato = datoTemp;
    }

    $('<div id="texto" style="border-style: none; ">' + dato + '</div>').appendTo('#espaciosDeCartas');//Textos de los incisos    
//    $('.texto-respuesta-sub').css('padding-right', $('#card0').width() - $('#texto1').width());   
    for (var t = 0; t < cuantosBR; t++) {//Oculta todos los divs, menos el primero
        if (t >= 1) {
            $('#br' + t).addClass('elementosOcultos');
        }
    }

    $('.texto-respuesta-sub').droppable({
        accept: '#pilaDeCartas div',
        hoverClass: 'hovered',
        out: handleCardReturn,
        drop: handleCardDrop
    });
    if (pizarron) {
        dimensiones = obtenerDimensionesImagen("http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/pizarron17.png", 992);
        $('#content').width('875px');
    } else {
//        var alturaContent = 800;
        dimensiones.height = $('#pilaDeCartas').height() + $('#texto').height();
        $('#content').height("100%");
        $('#content').css({padding: '0'});
        $("#espaciosDeCartas").css({margin: '0', padding: '0'});

        $('#texto img').each(function () {
            $(this).load(function () {
                imagenesCargadas++;
                if (imagenesCargadas === numImagenes) {//Si se cargaron todas las imágenes 
                    /*
                     console.log("Entro el console " );
                     dimensiones.height = $('#pilaDeCartas').height() + $('#espaciosDeCartas').height();//dimensiones.height = $('#respuestasRadialGroup').height();
                     $('#content').height( dimensiones.height + 120 );
                     $('#respuestasRadialGroup').height( dimensiones.height + 120 );   // Ajusto tamaño del div de respuestas
                     $('#respuestasRadialGroup').prev().css('margin-top', ($('#respuestasRadialGroup').height() / 2) - 80);
                     */
                }
            });
        });
    }
    var imagenesCargadas = 0;
    var numImagenes = $('#texto img').length;

    // Hack para aumentar el tamaño del div content
    seteaFloatLeft();
    posicionaCajasArrastradas();
    redimensionaAlturaDivPrincipal();//Se emplea para volver a ajustar la altura del contenedor principal (respuestasRadialGroup)
    //DETECTAR ESTA PARTE: una vez cargadas todas las imágenes del rectivo
//    $("#fondoPregunta").css({background:"#f2f2f2"});
    aplicaCambiosImagen();    
}
function calculaRepetidos(cadena, buscar) {
    var i = 0;
    var cuenta = 0;
    while (i != -1) {
        var i = cadena.indexOf(buscar, i);
        if (i != -1) {
            i++;
            cuenta++;
        }
    }
    return cuenta;
}
$(window).resize(function () {
    seteaFloatLeft();
    redimensionaAlturaDivPrincipal();//Se emplea para volver a ajustar la altura del contenedor principal (respuestasRadialGroup) una vez cargas todas las imágenes del rectivo
    //posicionaCajasArrastradas();
});
function seteaFloatLeft() {//Función para solucionar el reacomodo de cajas superiores verdes
    for (var i = 0; i < respuestasArr.length; i++) {
        $('#card' + i).css('float', 'left');//Aplica un clear a la última caja superior verde para evitar que se desacomoden las demás             
        if (i === respuestasArr.length - 1) {
            $('#card' + i).css('clear', '');//Al inicio aplica un clear a la última caja superior verde para evitar que se desacomoden las demás
        }
    }
}
function handleCardDrop(event, ui) {
    sueltaEnDrop = true;
    carta = $(this).first().attr("id");
    if ($(this).hasClass('hasCard')) {
        //console.log('No se puede colocar un elemento dentro de un contenedor ocupado - handleCardDrop');
    } else {
        var elem = $(this);
        var cardNumber = ui.draggable.data('text');
        var numeroDeEspacio = elem.data('number');
        var pocisionDeEspacio = ui.draggable.data('number') + 1;
        elem.data('elemDentro', elem.data('elemDentro') + 1);
        var elemDentro = elem.data('elemDentro');
        //Si la carta fue soltada en lugar correcto cambia su color, posición directamente en la parte superior y evitar que sea dragueada de nuevo.
        ui.draggable.addClass('dropped');
        //ui.draggable.css('float', '');        
        ui.draggable.addClass('dropped-text');

        elem.data('elemDentro');
        var idElem = ui.draggable.attr('id');
        $(this).attr('value', pocisionDeEspacio);
        $(this).attr('id', numeroDeEspacio);
        cardNumberGlobal = idElem;

        /*t11usuario[pocisionDeEspacio-1] = pocisionDeEspacio;
         pocisionDeEspacioGlobal = pocisionDeEspacio;
         reacomodaDivsCajasFlotantes();      */

        ui.draggable.position({of: $(this), my: 'left top', at: 'left top'});
        ui.draggable.draggable('option', 'revert', false);
        elem.attr('class', 'ui-droppable hasCard ' + idElem);
        ui.draggable.draggable("disable");
        $("#" + idElem).draggable("enable");
    }
}
function animarCajaVerde(idActual) {
    $(idActual).animate({//Regresala a la parte superior
        left: "0px",
        top: "0px"
    }, 500, function () {//Tiempo de la animación
        //animación completada
        sueltaEnDrop = false;
        $(this).draggable("enable");
    });
    var idCard = $(idActual).attr("id");
}
function handleCardReturn(event, ui) {
    if ($(this).hasClass('hasCard') && !$(this).hasClass(ui.draggable.attr('id'))) {
        //console.log('No se puede colocar un elemento dentro de un contenedor ocupado - handleCardReturn');
    } else {
        /*ui.draggable.removeClass('dropped');
         $(this).removeAttr('id');
         $(this).removeAttr('value');
         $(this).removeClass('dropped');
         //$(this).addClass('ui-droppable');
         $(this).attr('class', 'ui-droppable');*/
        //console.log($(this));
        //console.log(ui);
        //.log('Regresa a su estado normal');
    }
}
function reacomodaDivsCajasFlotantes() {
    var arregloTemporal = new Array();
    var colocados_arr = new Array();
    var reordenado_arr = new Array();
    var ultimoElemento;
    var primerElemento;

    for (i = 0; i < t11usuario.length; i++) {
        arregloTemporal[i] = i + 1;
    }
    for (i = 0; i < respuestasArr.length; i++) {
        arregloTemporal[i] = i + 1;
    }
    for (i = 0, j = 0; i < t11usuario.length; i++) {
        if (t11usuario[i]) {
            colocados_arr[j] = t11usuario[i];
            j++;
        }
    }
    for (i = 0; i < colocados_arr.length; i++) {
        for (j = 0; j < arregloTemporal.length; j++) {
            if (parseInt(arregloTemporal[j]) === parseInt(colocados_arr[i])) {
                delete arregloTemporal[j];
            }
        }
    }
    arregloTemporal = $.grep(arregloTemporal, function (n) {//Función para eliminar elementos vacíos de un array
        return(n);
    });
    for (j = 0; j < arregloTemporal.length; j++) {
        arregloTemporal[j] = parseInt(arregloTemporal[j]) - 1;
    }
    for (j = 0; j < arregloTemporal.length; j++) {
        primerElemento = arregloTemporal[0];
        ultimoElemento = arregloTemporal[arregloTemporal.length - 1];
        arregloTemporal.unshift(arregloTemporal[arregloTemporal.length - 1]);
        arregloTemporal.pop();
        $('#card' + ultimoElemento).insertBefore('#card' + primerElemento);

        if (parseInt(arregloTemporal[0]) !== 0 && ceroInicialUtilizado === 1) {
            $('#card7').insertBefore('#card0');
        }
    }
    ceroInicialUtilizado++;
}
function posicionaCajasArrastradas() {
    var posTextTop, posTextLeft, posCardTop, posCardLeft, posFinalTop, posFinalLeft, idCard;
    //reacomodaDivsCajasFlotantes();

    for (j = 1, i = 0; j <= t11usuario.length; j++, i++) {
        if (t11usuario[i]) {
            idCard = parseInt(t11usuario[i]) - 1;
            // Revisar                                    
            var cadena = "Es es una cadena de ejemplo";
            var numero = cadena.indexOf("cadena");

            if (typeof $('#texto' + j).attr('id') === 'undefined') {
                posTextTop = $('#' + j).offset().top;
                posTextLeft = $('#' + j).offset().left;
            } else {
                posTextTop = $('#texto' + j).offset().top;
                posTextLeft = $('#texto' + j).offset().left;
            }

            posCardTop = $('#card' + idCard).offset().top;
            posCardLeft = $('#card' + idCard).offset().left;
            posFinalTop = posTextTop - posCardTop;
            posFinalLeft = posTextLeft - posCardLeft;

            $('#card' + idCard).addClass('dropped').css('top', posFinalTop).css('left', posFinalLeft).css('z-index', 1000);
            $('#texto' + j).addClass('hasCard').addClass('card' + idCard).attr('value', t11usuario[i]).attr('id', j);
            // Hack para las preguntas con imagenes

            if (pizarron) {
                $('.dropped').addClass('dropped-image');
                $('.dropped').removeClass('dropped-text');
            } else {
                $('.dropped').removeClass('dropped-image');
                $('.dropped').addClass('dropped-text');
            }
        }
    }
}
function verificaUltimaCaja() {
    if (preguntasArr.length > respuestasArr.length) {
        pintarUltimaCaja = false;
    } else {
        pintarUltimaCaja = true;
    }
}
function aplicaCambiosImagen() {
    var hayImagen = contieneImagen();
    if (hayImagen) {
        var elementoMostrado = $('#br' + numeroInciso);
        elementoMostrado.find('br').next().css({marginTop: '-140px'});
        elementoMostrado.css({textAlign: 'center'});
        elementoMostrado.find('img').next('div').css({height: '10px'});
    }
}

function ajustarAnchoCajas() {
    var anchoMaximoCajas = 0, altoMaximoCajas = 0, tmpAnchoCajas, tmpAltCajas;
    if (!obj.pregunta.anchoRespuestas) {
        $('.respuestasArrastra').each(function () {
            tmpAnchoCajas = $(this).width();
            if (tmpAnchoCajas > anchoMaximoCajas) {
                anchoMaximoCajas = tmpAnchoCajas;
            }
            tmpAltCajas = $(this).height();
            if (tmpAltCajas > altoMaximoCajas) {
                altoMaximoCajas = tmpAltCajas;
            }
        });
    } else {
        anchoMaximoCajas = obj.pregunta.anchoRespuestas;
    }
    // se ajustan para que queden del tamaño correcto
    $('.respuestasArrastra').width(anchoMaximoCajas + 11);
    $('.texto-respuesta-sub').css({padding: (altoMaximoCajas - 35) + 'px ' + (anchoMaximoCajas + 15) + 'px 0 0'});
    //$('.respuestasArrastra').height(altoMaximoCajas-24); 

    var topImg = ($('.respuestasArrastra').height() - 30) / 2;
    $('<style>.respuestasArrastra:before {margin-top: ' + topImg + 'px !important;}</style>').appendTo('head');
    var soloTexto = obj.pregunta.soloTexto;
    if (soloTexto !== undefined && soloTexto === true) {
        $('.respuestasArrastra').addClass('hiddenBefore');
        $('.respuestasArrastra p').css({width: '300px'});
    }

}

$(document).ready(function () {
    var totalContenedores = $("#espaciosDeCartas span").length;

    $(".respuestasArrastra").draggable({
        drag: function () {
            for (var i = 0; i <= totalContenedores; i++) {
                if ($("#texto" + i).hasClass($(this).attr("id"))) {
                    $("#texto" + i).removeClass("hasCard");
                    $("#texto" + i).removeClass($(this).attr("id"));
                }
            }
            $(this).removeClass("dropped");
            $(this).removeClass("dropped-text");
        }
    });
});


function revierteCorta(){
    var totalContenedores = $("#espaciosDeCartas span").length;

    $(".respuestasArrastra").draggable({
        drag: function () {
            for (var i = 0; i <= totalContenedores; i++) {
                if ($("#texto" + i).hasClass($(this).attr("id"))) {
                    $("#texto" + i).removeClass("hasCard");
                    $("#texto" + i).removeClass($(this).attr("id"));
                }
            }
            $(this).removeClass("dropped");
            $(this).removeClass("dropped-text");
        }
    });
}