/*______         __              __                                  ______         __                                   
 |   __ \.-----.|  |.---.-.----.|__|.-----.-----.---.-.----.        |      |.-----.|  |.--.--.--------.-----.---.-.-----.
 |      <|  -__||  ||  _  |  __||  ||  _  |     |  _  |   _|        |   ---||  _  ||  ||  |  |        |     |  _  |__ --|
 |___|__||_____||__||___._|____||__||_____|__|__|___._|__|          |______||_____||__||_____|__|__|__|__|__|___._|_____|       
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com         
****************************************************************/
var cuantosPuntos = 6;
var radio = 20;
var espaciadoImagen = 70;
var area;
var tname;
var posXClic1 = 0;
var posYClic1 = 0;
var posXClic2 = 0;
var posYClic2 = 0;
var incremento = 0;
var lineaCreada;
var circuloCreadoIzquierda;
var circuloCreadoDerecha;
var textoPreguntaCreado;
var lineaSVGeliminar;
var objetoActual;
var valorXTextoPregunta = 10;
var color = "black";
var anchoStroke = "6";
var tipoPregunta;
var congelaBotonesDerechos = true;
var idBoton1;
var idLinea;
var arrRespuestasLineas = new Object();
var posicionXmouse = 0;
var posicionYmouse = 0;
var areaScrollDerecha = 0;
var areaScrollSuperior = 0;
var direccionTrazado = "";
var desfaseIzquierda;
var desfaseSuperior;
var centroDeCirculo = 0;//Obtiene la posición centro de los puntos           
var arrTmpInit, arrTempFin;
var congelaSonidoIncorrecto = false;
var alturaCaja = "84px";//56px: Fija una altura específica  Nada: Toma la altura automática
var anchoImagen;//Es el ancho en la que se desea ver la imagen
var alturaImagen;//Es la altura en la que se desea ver la imagen
var ajusteImagenes = 10;
var tieneImagenesIzquierda = false;
var tieneImagenesDerecha = false;
var colorSeleccion = '#0066FF';//Color de selección
var colorBien = '#A4CD6D';//Color bien
var colorMal = '#F04B57';//Color mal
var presionadoGlobal;
var contornoCirculo = '#FFB55D';
var rellenoCirculo = '#FFFFFF';
var colorCajaIzquierda = '#A4CD6D';
var colorCajaDerecha = '#2F83C0';
var alineacionContenido = 'center';
var claseAlineacion = 'h1-centrarTexto-RL';
var espacioHaciaIzquierda = 310;
var espacioNegativoIzquierda = 8;
var anchoCajaIzquierda = '300px';
var thisIdGlobal;
var idBoton1Global;
var idLineaGlobal;
var evitarPintadoCirculo = false;
var anchoCentro;
var eventoPress;
var eventoRelease;
var eventoMove;
var id_temp = "null";
var evitarTriggerIzquierda = false;
var evitarTriggerDerecha = false;
var dispositivoMovil;
var espaciadoGlobal = 0;
var margen = 6;
var posicionesYcirculosIzquierda_arr = new Array();
var posicionesYcirculosDerecha_arr = new Array();
var posicionesYrectangulosIzquierda_arr = new Array();
var posicionesYrectangulosDerecha_arr = new Array();
var posicionesYcajasIzquierda_arr = new Array();
var posicionesYcajasDerecha_arr = new Array();
var posicionXcirculoIzquierda;
var posicionXcirculoDerecha;
var posicionXrectangulosIzquierda;
var posicionXrectanguloDerecha;
var posicionXcajaIzquierda;
var posicionYcajaDerecha;   
var longitudesDeTextos = new Array();
var longitudesDeTextosDerecha = new Array();
var posLongitudes = 0;
var numeroMayor;
var numeroMayorDerecha;
var separacionEntrePuntos = 400;//Separación en X entre los puntos izquierda y derecha, sólo cuando hay imágenes en la derecha
var ajusteSeparacionRectCirculo = 20;
var posicionesYcirculosIzquierdaTemp_arr = new Array();

function resetearVariables(){
    cuantosPuntos = 6;
    radio = 20;
    espaciadoImagen = 70;
    posXClic1 = 0;
    posYClic1 = 0;
    posXClic2 = 0;
    posYClic2 = 0;
    incremento = 0;
    valorXTextoPregunta = 10;
    arrRespuestasLineas = new Object();
    posicionXmouse = 0;
    posicionYmouse = 0;
    areaScrollDerecha = 0;
    areaScrollSuperior = 0;
    direccionTrazado = "";
    centroDeCirculo = 0;//Obtiene la posición centro de los puntos           
    arrTmpInit, arrTempFin;
    congelaSonidoIncorrecto = false;
    alturaCaja = "84px";//56px: Fija una altura específica  Nada: Toma la altura automática
    ajusteImagenes = 10;
    tieneImagenesIzquierda = false;
    tieneImagenesDerecha = false;
    presionadoGlobal;   
    espacioHaciaIzquierda = 310;
    espacioNegativoIzquierda = 8;
    anchoCajaIzquierda = '300px';
    id_temp = "null";
    evitarTriggerIzquierda = false;
    evitarTriggerDerecha = false;    
    espaciadoGlobal = 0;
    margen = 6;
    posicionesYcirculosIzquierda_arr = new Array();
    posicionesYcirculosDerecha_arr = new Array();
    posicionesYrectangulosIzquierda_arr = new Array();
    posicionesYrectangulosDerecha_arr = new Array();
    posicionesYcajasIzquierda_arr = new Array();
    posicionesYcajasDerecha_arr = new Array(); 
    longitudesDeTextos = new Array();
    longitudesDeTextosDerecha = new Array();
    posLongitudes = 0;
    separacionEntrePuntos = 400;//Separación en X entre los puntos izquierda y derecha, sólo cuando hay imágenes en la derecha
    ajusteSeparacionRectCirculo = 20;
    posicionesYcirculosIzquierdaTemp_arr = new Array();
    numeroDeLineasVerdes = 0;
}
function estructuraHTMLrelacionarColumnas(estructuraBotones, estructuraBotones2){
    var _strIzquierda   = '<div id="izquierda" onmousedown="return false" class="large-5 medium-5 small-5 columns"></div>';
    var _strCentro      = '<div id="centro" class="large-2 medium-2 small-2 columns"></div>';
    var _strDerecha     = '<div id="derecha" onmousedown="return false" class="large-5 medium-5 small-5 columns"></div>';
    var _strHtml        = _strIzquierda + _strCentro + _strDerecha;
    var estructuraBotones3 = "";
    if(numEstilo === "10"){//Relacionar Líneas            
        estructuraBotones3 = estructuraBotones3 + '<svg id="elementosSVG" height="100%" xmlns=svgNS xmlns:xlink=svgNSLink></svg>';
    }
    $("#respuestasRadialGroup").html(_strHtml);
    $("#izquierda").html(estructuraBotones);
    $("#derecha").html(estructuraBotones2);
    $("#centro").html(estructuraBotones3);  
}
function preguntaRelacionarColumnas(){
    arrTmpInit = new Array();
    arrTempFin = new Array();

    if(longitudesDeTextos.length >= 1){        
        longitudesDeTextos.length=0;
        longitudesDeTextosDerecha.length=0;
        posLongitudes = 0;
        resetearVariables();
    }
    
    var alturaSVG;
    var arrRespuesta = new Array();
    var arrRespuestaDer = new Array();
    var textoRespuestaIzquierda;
    var textoRespuestaDerecha;
    var imagenRespuesta = 'null';
    var imagenRespuestaDer = 'null';
    var espaciado = 28;//10
    var espaciadoDer = 28;//10
    var valorXIzquierda = 14;//0
    var valorXDerecha = 0;//0
    var margen = 14;
    var areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    var imagenesCargadas = 0;
    centroDeCirculo = 0;
    terminoTrazo = false;
                
    cuantasRelaciona++;    
    anchoCentro = $('#centro').width();
    
    var heightFrame=$( window ).height()-1;
    $('#respuestasRadialGroup').append('<div id="capaSuperior" style="display:none"></div>');
    $("#capaSuperior").height(heightFrame);
    
    if(typeof obj.pregunta.iconos !== 'undefined'){
//        if( obj.pregunta.iconos === true){
            //colorCajaIzquierda = 'transparent';
            alineacionContenido = 'right';
//            claseAlineacion = '';
//            espacioHaciaIzquierda = 10;
//            espacioNegativoIzquierda = 108;
//            anchoCajaIzquierda = '200px';
//        }
    }
    
    if( typeof obj.pregunta.anchoImagen === 'undefined'){//Sino la trae el json setea el valor por default        
        anchoImagen = 84;//Es el ancho en la que se desea ver la imagen
        alturaImagen = 84;//Es la altura en la que se desea ver la imagen        
    }else{
        anchoImagen = obj.pregunta.anchoImagen;//Es el ancho en la que se desea ver la imagen
        alturaImagen = obj.pregunta.altoImagen;//Es la altura en la que se desea ver la imagen        
    }  

    textoRespuestaIzquierdaTemp = htmlDecode(preguntasArr[0]);//Paso por el filtro decodificador
    textoRespuestaDerechaTemp = htmlDecode(respuestasArr[0]);//Paso por el filtro decodificador  
    var encuentraImgIzquierda = textoRespuestaIzquierdaTemp.indexOf('<img');
    var encuentraImgDerecha = textoRespuestaDerechaTemp.indexOf('<img');    
    if(encuentraImgIzquierda >= 0){//Si hay imágenes en el lado izquierdo                 
       colorCajaIzquierda = 'transparent';
       //anchoImagen = obj.pregunta.anchoImagen;//Es el ancho en la que se desea ver la imagen
       //alturaImagen = obj.pregunta.altoImagen;//Es la altura en la que se desea ver la imagen          
    }else{//Sino hay imágenes en el lado izquierdo            
    }
    
    if(encuentraImgDerecha >= 0){//Si hay imágenes en el lado derecho
       colorCajaDerecha = 'transparent';
       //anchoImagen = obj.pregunta.anchoImagen;//Es el ancho en la que se desea ver la imagen
       //alturaImagen = obj.pregunta.altoImagen;//Es la altura en la que se desea ver la imagen          
    }else{//Sino hay imagen en el lado izquierdo            
    }                            
    
    var caracteresNum_arr = new Array();    
    for(var c=0,h=1;c<obj.respuestas.length;c++,h++){        
        textoRespuestaIzquierda = htmlDecode(preguntasArr[h-1]);// Paso por el filtro decodificador
        textoRespuestaDerecha = htmlDecode(respuestasArr[h-1]);// Paso por el filtro decodificador
                
        var textoRespuestaIzquierdaTemp = eliminaAcentos(textoRespuestaIzquierda);   
        textoRespuestaIzquierdaTemp = eliminarParrafos(textoRespuestaIzquierdaTemp);
        //textoRespuestaIzquierdaTemp = textoRespuestaIzquierdaTemp.split(" ");  var numeroPalabras = textoRespuestaIzquierdaTemp.length;  longitudesDeTextos[posLongitudes] = numeroPalabras;
        longitudesDeTextos[posLongitudes] = textoRespuestaIzquierdaTemp.length;                        
                        
        var textoRespuestaDerechaTemp = eliminaAcentos(textoRespuestaDerecha);
        textoRespuestaDerechaTemp = eliminarParrafos(textoRespuestaDerechaTemp);
        //textoRespuestaDerechaTemp = textoRespuestaDerechaTemp.split(" ");  var numeroPalabras = textoRespuestaDerechaTemp.length;  longitudesDeTextos[posLongitudes] = numeroPalabras;
        longitudesDeTextosDerecha[posLongitudes] = textoRespuestaDerechaTemp.length;        
        posLongitudes++;
    }
    
    numeroMayor=Math.max.apply(null, longitudesDeTextos);//Obtiene el número de palabras menor        
    numeroMayorDerecha=Math.max.apply(null, longitudesDeTextosDerecha);//Obtiene el número de palabras menor        
    
    var mayorAmbos = obtenerRenglonesMaximo(numeroMayor, numeroMayorDerecha);
    //console.log('mayorAmbos '+mayorAmbos);
    
    /*var datosCaja_arr = obtenerDatosCaja(mayorAmbos);
    alturaCaja = datosCaja_arr[0];
    espaciado = datosCaja_arr[1];*/
    cuantosPuntos = preguntasArr.length;
    for ( j=1, i=0; j<=cuantosPuntos; j++, i++ ) {        
        //CREA TEXTOS DE LAS PREGUNTAS        
        arrRespuesta = separaUrls(preguntasArr[j-1]);
        arrRespuestaDer = separaUrls(respuestasArr[j-1]);
        
        textoRespuestaIzquierda = htmlDecode(preguntasArr[j-1]); // Paso por el filtro decodificador
        textoRespuestaDerecha = htmlDecode(respuestasArr[j-1]); // Paso por el filtro decodificador                
        
        var encuentraImgIzq = textoRespuestaIzquierda.indexOf('<img');
        if(encuentraImgIzq >= 0){//Si hay imagen en el lado izquierdo
           tieneImagenesIzquierda = true;           
        }else{//Sino hay imagen en el lado izquierdo            
        }
        
        var encuentraImgDer = textoRespuestaDerecha.indexOf('<img');            
        if(encuentraImgDer >= 0){//Si hay imagen en el lado izquierdo           
           tieneImagenesDerecha = true;           
        }else{//Sino hay imagen en el lado izquierdo            
        }      
        if(tieneImagenesIzquierda === false && tieneImagenesDerecha === false){//Si en ambos lados hay textos se hace el cálculo de la altura de las cajas dependiendo del número máximo de renglones            
            console.log('aplica');
            var datosCaja_arr = obtenerDatosCaja(mayorAmbos);
            alturaCaja = datosCaja_arr[0];
            espaciado = datosCaja_arr[1];
            
        }
    }
    
        
    for(var c=0,h=1;c<respuestasArr.length;c++,h++){            
        $( '#izquierda_'+c ).css('background-color', colorCajaIzquierda);
        $( '#izquierda_'+c ).css('width', anchoCajaIzquierda);
                
        /*if(numeroMayor >= 1 && numeroMayor <= 80){//Si la línea tiene entre 1 y 80 caracteres
            $( '#izquierda_'+c ).css('height', '60px');                 
            espaciado = 18;
        }else{
            if(alturaCaja !== ""){  $( '#izquierda_'+c ).css('height', alturaCaja);  }           
        }*/  //Ya no aplica este cálculo, aw hace con el código de la linea 40        
        $( '#izquierda_'+c ).css('height', alturaCaja);
        
        $( '#izquierda_'+c ).css('color', 'white');
        $( '#izquierda_'+c ).removeClass('text-right');
        $( '#izquierda_'+c ).css('text-align', alineacionContenido);
        $( '#izquierda_'+c ).css('float', 'right');        
        $( '#izquierda_'+c ).css('padding-left', '6px');
        $( '#izquierda_'+c ).css('padding-right', '6px');        
                
        $( '#derecha_'+c ).css('background-color', colorCajaDerecha);
        $( '#derecha_'+c ).css('width', '412px');   
        espaciadoDer = 18;
        /*if(numeroMayor >= 1 && numeroMayor <= 80){//Si la línea tiene entre 1 y 80 caracteres
            $( '#derecha_'+c ).css('height', '60px');
            espaciadoDer = 18;
        }else{
            if(alturaCaja !== ""){  $( '#derecha_'+c ).css('height', alturaCaja);  }        
        }*/  //Ya no aplica este cálculo, aw hace con el código de la linea 40        
        $( '#derecha_'+c ).css('height', alturaCaja);
         
        $( '#derecha_'+c ).css('color', 'white');          
        //$( '#derecha_'+c ).css('padding-right', '6px'); 
    }    
    $( '#derecha' ).css('float', 'left');        
    $( '#derecha' ).css('z-index', '2');  
    $( '#centro' ).css('z-index', '3');
    $( '#izquierda' ).css('width', '315px');
    $( '#izquierda' ).css('z-index', '1');
        
    deshabilitaSeleccionDeTexto();
    
    
    for ( j=1, i=0; j<=cuantosPuntos; j++, i++ ) {        
        // Lleno el array de respuestas usuario desde el inicio
        if(typeof arrRespuestasLineas[i] === 'undefined'){
            arrRespuestasLineas[i] = new Object();
        }
        if(typeof arrRespuestasLineas[i].id === 'undefined'){
            arrRespuestasLineas[i].id = 0;
        }
        if(typeof arrRespuestasLineas[i].selected === 'undefined'){
            arrRespuestasLineas[i].selected = 0;
        }
          
        //CREA TEXTOS DE LAS PREGUNTAS        
        arrRespuesta = separaUrls(preguntasArr[j-1]);
        arrRespuestaDer = separaUrls(respuestasArr[j-1]);
        
        textoRespuestaIzquierda = htmlDecode(preguntasArr[j-1]); // Paso por el filtro decodificador
        textoRespuestaDerecha = htmlDecode(respuestasArr[j-1]); // Paso por el filtro decodificador                
        imagenRespuesta = arrRespuesta[1];
        imagenRespuestaDer = arrRespuestaDer[1];
        
        var encuentraImgIzq = textoRespuestaIzquierda.indexOf('<img');
        if(encuentraImgIzq >= 0){//Si hay imagen en el lado izquierdo
           //textoRespuestaIzquierda = textoRespuestaIzquierda.replace('84px', alturaImagen+'px');
           tieneImagenesIzquierda = true;           
        
           /*var cierreImg = textoRespuestaIzquierda.indexOf('>', 0);
           var cierreTextoImg = textoRespuestaIzquierda.length;
           var distancia = cierreTextoImg - cierreImg;
           
           if(distancia >1){
               console.log('HAY IMAGEN EN LA IZQUIERDA y TIENE TEXTO: '+'#izquierda_'+i);
               $( 'h1').css('color', '#CAE');    //color: #CAE;
           }else{          
               console.log('HAY IMAGEN EN LA IZQUIERDA y NO TIENE TEXTO');
           }     */
        }else{//Sino hay imagen en el lado izquierdo            
        }
        
        var encuentraImgDer = textoRespuestaDerecha.indexOf('<img');            
        if(encuentraImgDer >= 0){//Si hay imagen en el lado izquierdo           
           //textoRespuestaDerecha = textoRespuestaDerecha.replace('84px', alturaImagen+'px');
           tieneImagenesDerecha = true;           
        }else{//Sino hay imagen en el lado izquierdo            
        }      
    
        /*if( imagenRespuesta ){//Si el reactivo contiene imágenes
//            var imagen = obtenerDimensionesImagen(imagenRespuesta[0], 50);
//            createSVGImage(imagenRespuesta[0], valorXIzquierda-254, (espaciadoImagen*j)-20, imagen.width, imagen.height);
//            valorXIzq = valorXIzquierda-100;
            textoRespuestaIzquierda = '<img id="'+(j-1)+'" src="'+imagenRespuesta+'">';                                    
        }                     
        if( imagenRespuestaDer ){//Si el reactivo contiene imágenes
            var imagen = obtenerDimensionesImagen(imagenRespuesta[0], 50);
            createSVGImage(imagenRespuesta[0], valorXIzquierda-254, (espaciadoImagen*j)-20, imagen.width, imagen.height);
            valorXIzq = valorXIzquierda-100;
            textoRespuestaDerecha = '<img id="'+(j-1)+'" src="'+imagenRespuestaDer+'">';                                    
        }    */                 
        
        //textoPreguntaCreado = createSVGtext(textoRespuestaIzquierda, valorXIzq-90, (espaciado*j), font_size, font_color, "start", font_text);
        //document.getElementById(nombreSVG).appendChild(textoPreguntaCreado);

        //CREA TEXTOS DE LAS RESPUESTAS
        //textoPreguntaCreado = createSVGtext(textoRespuestaDerecha, valorXDerecha, (espaciado*j), font_size, font_color, "start", font_text);
        //document.getElementById(nombreSVG).appendChild(textoPreguntaCreado);        
        if(alturaCaja === ""){//Sino se especifica la altura de la caja
            $('#izquierda_' + (j-1)).html(textoRespuestaIzquierda);                  
            $('#derecha_' + (j-1)).html(textoRespuestaDerecha);                        
        }else{
            $('#izquierda_' + (j-1)).html('<h1 class="h1-RL '+claseAlineacion+'">' + textoRespuestaIzquierda+'</h1>');
            $('#derecha_' + (j-1)).html('<h1 class="h1-RL h1-sangriaTexto-RL">'+textoRespuestaDerecha+'</h1>');
            congelaTextos( '#izquierda_' + (j-1) + ' h1' );
            congelaTextos( '#derecha_' + (j-1) + ' h1' );
        }                

        valorXDerecha = anchoCentro + 300;
        espaciado += $('#izquierda_' + (j-2)).height();
        
        imagenRespuestaa = $('#imagen'+(j-1)).attr('src');        
        $('#imagen'+(j-1)).load(function (e) {
            imagenesCargadas++;            
            if(imagenesCargadas === cuantosPuntos){//Si se cargaron todas las imágenes
                espaciadoGlobal = 10;
                for(var j=1;j<=cuantosPuntos;j++){
                    //ajustaAlturasRelacionaLineas(j-1);                    
                }                
                reacomodaPuntosNegros();
            }
        }).error(function () {
            console.log('Error al cargar la imagen');
        }).attr('src', imagenRespuestaa);
        
        var posicionX_ = valorXIzquierda+espacioHaciaIzquierda;
        posicionXcirculoIzquierda = posicionX_;
        var posicionY_ = (espaciado + ( margen * j ));           
                       
        //CREA RECTÁNGULOS DE LA IZQUIERDA
        var anchoCajas;
        var altoCajas;
        var xCajas;
        var yCajas;
        
        if(tieneImagenesIzquierda === false){//Si no tiene imágenes la izquierda
            anchoCajas = $('#izquierda_0').width();
            anchoCajas = anchoCajas + 3;
            
            if(tieneImagenesDerecha){
                if(tieneImagenesIzquierda === false){
                    posicionY_ = posicionY_;
                    altoCajas = alturaImagen;                               
                }else{
                    posicionY_ = posicionY_ - 11;
                    altoCajas = alturaImagen + 10;
                }                
            }else{
                altoCajas = $('#izquierda_0').height();
            }                             
            xCajas = posicionX_ - 319;            
            yCajas = posicionY_ - 31;
        }else{//Si tiene imágenes en la izquierda
            anchoCajas = anchoImagen + 'px';
            altoCajas = alturaImagen + 'px';
            xCajas = 23;
            yCajas = posicionY_ - 28;            
            var datoAncho = parseInt(anchoImagen)+ajusteImagenes+'px';
            var datoAlto = parseInt(alturaImagen)+ajusteImagenes+'px';
                        
            $( '#izquierda' ).css('width', datoAncho);
            $( '#izquierda' ).css('height', datoAlto);            
            $( '#izquierda' ).css('left', '36px');
            $( '#izquierda' ).css('position', 'absolute');            
            $( '#izquierda_'+ (j-1) ).css('width', datoAncho);     
            $( '#izquierda_'+ (j-1) ).css('height', datoAlto);
        }                           
        posicionXrectangulosIzquierda = xCajas;
        posicionesYrectangulosIzquierda_arr.push(yCajas);
        
        //rellenoCirculo = 'pink';
        var rectanguloCreadoIzquierda = document.createElementNS(svgNS,"rect");//Se declara la instancia de círculo
        rectanguloCreadoIzquierda.setAttributeNS(null,"id","ic_"+j);
        rectanguloCreadoIzquierda.setAttributeNS(null,"x", xCajas);
        rectanguloCreadoIzquierda.setAttributeNS(null,"y", yCajas); // Se aumenta el margen que se le agrego a cada opcion        
        rectanguloCreadoIzquierda.setAttributeNS(null,"width",anchoCajas);
        rectanguloCreadoIzquierda.setAttributeNS(null,"height",altoCajas);        
        rectanguloCreadoIzquierda.setAttributeNS(null, 'style', 'fill: '+rellenoCirculo+'; ' );
        rectanguloCreadoIzquierda.setAttributeNS(null,"cursor","pointer");
        rectanguloCreadoIzquierda.setAttributeNS(null,"opacity","0");
        
        //CREA RECTÁNGULOS DE LA DERECHA                                                                             
        if(tieneImagenesDerecha === false){//Si no tiene imágenes en la derecha
            anchoCajas = $('#derecha_0').width();
            if(tieneImagenesIzquierda){
                altoCajas = alturaImagen + 10;
                posicionY_ = posicionY_ - 11;  
            }  
            xCajas = posicionX_ + 180;   
            if(tieneImagenesIzquierda === false && tieneImagenesDerecha === false){
                yCajas = posicionY_ - 31;
            }else{
                yCajas = posicionY_ - 21; 
            } 
                           
            var aumentaAncho = parseInt(anchoCajas);                                
            aumentaAncho = aumentaAncho + 6;
            aumentaAncho = aumentaAncho + 'px';                                
            anchoCajas = aumentaAncho;
        }else{//Si tiene imágenes en la derecha           
            if(tieneImagenesIzquierda === false){//Sino tiene imágenes en la izquierda                 
                posicionY_ = posicionY_ + 12;
            }                
            anchoCajas = anchoImagen;
            anchoCajas = anchoCajas + 3;
            altoCajas = alturaImagen + 'px';                   
            xCajas = 506;
            if(tieneImagenesIzquierda && tieneImagenesDerecha){
                yCajas = posicionY_ - 28;
            }else{
                if(tieneImagenesDerecha){
                    yCajas = posicionY_ - 43;
                }else{
                    yCajas = posicionY_ - 38;
                }                  
            }                
            
            var datoAncho = parseInt(anchoImagen)+ajusteImagenes+'px';
            var datoAlto = parseInt(alturaImagen)+ajusteImagenes+'px';
            $( '#derecha' ).css('width', datoAncho);            
            $( '#derecha' ).css('left', '131px');            
            $( '#derecha_'+ (j-1) ).css('width', datoAncho);
            $( '#derecha_'+ (j-1) ).css('height', datoAlto);            
        }        
        posicionXrectanguloDerecha = xCajas;
        posicionesYrectangulosDerecha_arr.push(yCajas);   
        
        var rectanguloCreadoDerecha = document.createElementNS(svgNS,"rect");//Se declara la instancia de círculo
        rectanguloCreadoDerecha.setAttributeNS(null,"id","dc_"+j);
        rectanguloCreadoDerecha.setAttributeNS(null,"x", xCajas);
        rectanguloCreadoDerecha.setAttributeNS(null,"y", yCajas); // Se aumenta el margen que se le agrego a cada opcion
        rectanguloCreadoDerecha.setAttributeNS(null,"width",anchoCajas);
        rectanguloCreadoDerecha.setAttributeNS(null,"height",altoCajas);        
        rectanguloCreadoDerecha.setAttributeNS(null, 'style', 'fill: '+rellenoCirculo+'; ' );
        rectanguloCreadoDerecha.setAttributeNS(null,"cursor","pointer");
        rectanguloCreadoDerecha.setAttributeNS(null,"opacity","0");                
                           
        //CREA CIRCULOS DE LA IZQUIERDA
        circuloCreadoIzquierda = document.createElementNS(svgNS,"circle");//Se declara la instancia de círculo
        circuloCreadoIzquierda.setAttributeNS(null,"id","i_"+j);
        circuloCreadoIzquierda.setAttributeNS(null,"cx",0);
        circuloCreadoIzquierda.setAttributeNS(null,"cy",posicionY_); // Se aumenta el margen que se le agrego a cada opcion
        circuloCreadoIzquierda.setAttributeNS(null,"r",radio);
        circuloCreadoIzquierda.setAttributeNS(null, 'style', 'fill: '+rellenoCirculo+'; stroke: '+contornoCirculo+'; stroke-width: 4px;' );
        circuloCreadoIzquierda.setAttributeNS(null,"stroke","1");
        circuloCreadoIzquierda.setAttributeNS(null,"cursor","pointer");
        
        dispositivoMovil = esMobile();
        eventoPress = dispositivoMovil ? "touchstart" : "mousedown";
        eventoRelease = dispositivoMovil ? "touchend" : "mouseup";
        eventoMove = dispositivoMovil ? "touchmove" : "mousemove";
        
        circuloCreadoIzquierda.onmouseover=function(e){//Clic sobre cualquier circulo de la derecha
            if(direccionTrazado !== "" && direccionTrazado === "derecha-izquierda"){
                colocaRecuadrosIzquierda(e);
            }
        };
        circuloCreadoIzquierda.onmouseout=function(e){//Clic sobre cualquier circulo de la derecha
            if(direccionTrazado === "derecha-izquierda"){
                eliminarRecuadrosIzquierda(e);
            }
        };
        
        document.getElementById(nombreSVG).appendChild(circuloCreadoIzquierda);
        document.getElementById(nombreSVG).appendChild(rectanguloCreadoIzquierda);
        document.getElementById(nombreSVG).appendChild(rectanguloCreadoDerecha);                        
                        
        $( '#i_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha          
            e.preventDefault();
            evitarTriggerIzquierda = true;
            evitarTriggerDerecha = false;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'izquierda-derecha';
            }
            if(direccionTrazado === 'izquierda-derecha'){                  
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{
                //no hace nada
                removerEventos();//Verificar si se aplicará
            }
        });
        $( '#ic_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha
            e.preventDefault();
            evitarTriggerIzquierda = true;
            evitarTriggerDerecha = false;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'izquierda-derecha';
            }
            if(direccionTrazado === 'izquierda-derecha'){
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{
                //no hace nada
                removerEventos();//Verificar si se aplicará
            }
        });
                 
        $( '#i_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha
            e.preventDefault();
            if(evitarTriggerIzquierda === true){//Si es dispositivo móvil
                $('#'+id_temp).trigger(eventoRelease);
            }else{//Sino es dispositivo móvil
                if(direccionTrazado === ""){
                    direccionTrazado = 'izquierda-derecha';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    finTrazado(e,this.id);
                }else{
                    //no hace nada
                }
            }
        });
        
        $( '#ic_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha
            e.preventDefault();
            if(evitarTriggerIzquierda === true){//Si es dispositivo móvil
                $('#'+id_temp).trigger(eventoRelease);
            }else{//Sino es dispositivo móvil
                if(direccionTrazado === ""){
                    direccionTrazado = 'izquierda-derecha';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    finTrazado(e,this.id);
                }else{
                    //no hace nada
                }
            }
        });
        
        espaciadoDer += $('#derecha_' + (j-2)).height();                
        posicionX_ = valorXDerecha;
        posicionXcirculoDerecha = posicionX_;
        posicionY_ = (espaciadoDer + ( margen * j ))                        
        
        if(tieneImagenesIzquierda === false){//Si no tiene imágenes en la izquierda    
             posicionesYcirculosIzquierda_arr.push(posicionY_);
        }else{//Si tiene imágenes en la izquierda
            var elemento = document.querySelector( '#ic_'+ j );
            var posXrectanguloIzquierda = elemento.getAttribute('x') ;            
            var widthRectanguloIzquierda = elemento.getAttribute('width') ;
            widthRectanguloIzquierda = widthRectanguloIzquierda.replace('px', '');                        
            var widthCirculoIzquierda = radio;             
            posicionX_ = parseInt(posXrectanguloIzquierda) + parseInt(widthRectanguloIzquierda);
            posicionX_ = posicionX_ + widthCirculoIzquierda;            
            posicionX_ = posicionX_ + ajusteSeparacionRectCirculo;
            posicionXcirculoIzquierda = posicionX_;
            
            //Calcula la posicion Y de los puntos de la izquierda
            var posicionYPuntosConImagen = $( '#ic_'+j).attr( 'height' );
            posicionYPuntosConImagen = posicionYPuntosConImagen.replace('px', '');
            posicionYPuntosConImagen = parseInt($( '#ic_'+j).attr( 'y' )) + ( posicionYPuntosConImagen / 2 );
            posicionY_ = posicionYPuntosConImagen;
            posicionesYcirculosIzquierda_arr.push(posicionY_);
        } 
        
        //CREA RECTÁNGULOS DE LA DERECHA                                                             
        if(tieneImagenesDerecha === false){//Si no tiene imágenes en la derecha
            posicionesYcirculosDerecha_arr.push(posicionY_);
        }else{//Si tiene imágenes en la derecha                        
            //Calcula la posicion Y de los puntos de la derecha
            var posicionYPuntosConImagen = $( '#dc_'+j).attr( 'height' );
            posicionYPuntosConImagen = posicionYPuntosConImagen.replace('px', '');
            posicionYPuntosConImagen = parseInt($( '#dc_'+j).attr( 'y' )) + ( posicionYPuntosConImagen / 2 );
            posicionY_ = posicionYPuntosConImagen;
            posicionesYcirculosDerecha_arr.push(posicionY_); 
        }
        
        //CREA CIRCULOS DE LA DERECHA
        circuloCreadoDerecha = document.createElementNS(svgNS,"circle");//Se declara la intancia de círculo
        circuloCreadoDerecha.setAttributeNS(null,"id","d_"+j);
        circuloCreadoDerecha.setAttributeNS(null,"cx",posicionX_);
        circuloCreadoDerecha.setAttributeNS(null,"cy", posicionY_); // Se aumenta el margen que se le agrego a cada opcion
        circuloCreadoDerecha.setAttributeNS(null,"r",radio);
        circuloCreadoDerecha.setAttributeNS(null,"fill","black");        
        circuloCreadoDerecha.setAttributeNS(null,"stroke","none");
        circuloCreadoDerecha.setAttributeNS(null, 'style', 'fill: '+rellenoCirculo+'; stroke: '+contornoCirculo+'; stroke-width: 4px;' );        
        circuloCreadoDerecha.setAttributeNS(null,"cursor","pointer");        
         
        circuloCreadoDerecha.onmouseover=function(e){//Clic sobre cualquier circulo de la derecha                        
            if(direccionTrazado !== "" && direccionTrazado === "izquierda-derecha"){
                colocaRecuadrosDerecha(e);
            }  
        };
        circuloCreadoDerecha.onmouseout=function(e){//Clic sobre cualquier circulo de la derecha                                    
            if(direccionTrazado === "izquierda-derecha"){
                 eliminarRecuadrosDerecha(e);
            }
        };
                       
        document.getElementById(nombreSVG).appendChild(circuloCreadoDerecha);
                        
        $( '#d_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown cualquier circulo de la derecha
            e.preventDefault();
            evitarTriggerIzquierda = false;
            evitarTriggerDerecha = true;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'derecha-izquierda';
            }
            if(direccionTrazado === 'derecha-izquierda'){
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{                        
                removerEventos();                                 
            }             
        });
        $( '#dc_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown cualquier circulo de la derecha
            e.preventDefault();
            evitarTriggerIzquierda = false;
            evitarTriggerDerecha = true;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'derecha-izquierda';
            }
            if(direccionTrazado === 'derecha-izquierda'){
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{                        
                removerEventos();                                 
            }             
        });
                
        $( '#d_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha       
            e.preventDefault();
            if(evitarTriggerDerecha === true){//Si es dispositivo móvil                
                $('#'+id_temp).trigger(eventoRelease); 
            }else{
                if(direccionTrazado === ""){
                    direccionTrazado = 'derecha-izquierda';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    //no hace nada
                }else{
                    finTrazado(event,this.id);
                }
            }            
        });           
        
        $( '#dc_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha       
            e.preventDefault();
            if(evitarTriggerDerecha === true){//Si es dispositivo móvil                
                $('#'+id_temp).trigger(eventoRelease); 
            }else{
                if(direccionTrazado === ""){
                    direccionTrazado = 'derecha-izquierda';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    //no hace nada
                }else{                    
                    finTrazado(event,this.id);
                }
            }            
        }); 
        
        if(cuantosPuntos === j){
            alturaSVG = circuloCreadoDerecha.cy.baseVal.value + 80;
        }

        ajustaAlturasRelacionaLineas(j-1);
                
        if( j === cuantosPuntos && imagenRespuestaDer === null && imagenRespuesta === null){//Si es la última vuelta y no existen imágenes.
            ajustaAltura();            
            pintaLineasResueltas();
        }            
                
        if(c03id_tipo_pregunta === "12"){//Si es Relaciona Líneas
            if(cuantasRelaciona >= 2){//Solo entra con animación el ejercicio 1
                if(cuantosPuntos === j){                    
                    $('#respuestasRadialGroup').css('left', '800px');                      
                    $('#respuestasRadialGroup').animate( {                        
                        left: "-=800px"
                    }, tiempoAnimacionBarrido, function() {//1500 Tiempo de la animación
                        //fin de la animación
                    });
                }
            }
        }
        
        if(tieneImagenesIzquierda === false){//Si no tiene imágenes en la izquierda                           
            if(tieneImagenesDerecha){              
                if(tieneImagenesIzquierda === false){                                        
                    $( '#izquierda_'+ (j-1) ).css('height', alturaImagen);    
                    $( '#derecha_'+ (j-1) ).css('height', alturaImagen);                    
                    var posYCirDer = $( '#d_' + j ).attr('cy');                    
                    posicionesYcirculosIzquierda_arr = new Array();
                    posicionesYcirculosIzquierdaTemp_arr.push(posYCirDer);                                        
                }
            }
        }
    }
    var alturaSVG = obj.preguntasTotal.length > 1 ? '450px' : '525px';
    
    $( '#elementosSVG' ).css('width', '920px');
    $( '#elementosSVG' ).css('height', alturaSVG );
    $( '#elementosSVG' ).css('left', '6px');
    $( '#centro' ).css('width', '260px');    
    for ( i=0; i<cuantosPuntos; i++ ) { 
        textoRespuestaIzquierda = htmlDecode(preguntasArr[i]);// Paso por el filtro decodificador
        var encuentraImgIzq = textoRespuestaIzquierda.indexOf('<img');
        
        if(encuentraImgIzq >= 0){//Si hay imagen en el lado izquierdo
           var cierreImg = textoRespuestaIzquierda.indexOf('>', 0);
           var cierreTextoImg = textoRespuestaIzquierda.length;
           var distancia = cierreTextoImg - cierreImg;
           if(distancia >1){//Hay imagen en la izqueirda y tiene texto
               $( 'h1').css('color', '#000');
           }else{//ay imagen en la izqueirda y no tiene texto               
           }     
        }else{//Sino hay imagen en el lado izquierdo            
        }
    }
    if(tieneImagenesIzquierda === false){//Si no tiene imágenes en la izquierda                           
        if(tieneImagenesDerecha){              
            if(tieneImagenesIzquierda === false){
                posicionesYcirculosIzquierda_arr = posicionesYcirculosIzquierdaTemp_arr;
            }
        }
    }
        
    /*var tmpAlturaDiv=0, alturaDiv=0;
    $('.preguntaRelaciona').each(function(){
        alturaDiv = $(this).height();
        if(tmpAlturaDiv < alturaDiv){
            //$(this).height(alturaDiv);
            tmpAlturaDiv = alturaDiv;
        }
    });
    $('.preguntaRelaciona').height(tmpAlturaDiv);*/
    posicionaElementosAleatoriamente();
    if(vuelta === 0){//Sólo crea la barra la primera vez     
        barraAvance = creaBarraAvance(obj.preguntasTotal.length);
        $( '#fondoPregunta' ).append(barraAvance);
    }
    vuelta++;
    
}
function congelaTextos (elemento){
    $(elemento).attr('unselectable', 'on');
    $(elemento).css('user-select', 'none');
    $(elemento).on('selectstart', false);
}
function posicionaElementosAleatoriamente (){
    var posiciones1_arr = new Array();
    var posiciones2_arr = new Array();
    
    for(var c=0;c<preguntasArr.length;c++){                
        posicionesYcajasIzquierda_arr[c] = $( '#izquierda_'+c ).position().top;
        posicionesYcajasDerecha_arr[c] = $( '#derecha_'+c ).position().top;
        posicionXcajaIzquierda = $( '#izquierda_'+c ).position().left;
        posicionXcajaDerecha = $( '#derecha_'+c ).position().left;
        posiciones1_arr[c] = c;
        posiciones2_arr[c] = c;                        
    }
    posiciones1_arr = posiciones1_arr.sort(function() {return Math.random() - 0.5});
    posiciones2_arr = posiciones2_arr.sort(function() {return Math.random() - 0.5});
    
    posicionXcajaIzquierda = posicionXcajaIzquierda - espacioNegativoIzquierda;
    posicionXcajaDerecha = posicionXcajaDerecha + 216;    
    posicionXcirculoDerecha = posicionXcirculoDerecha + 54;
    
    if(tieneImagenesDerecha === false){//Si no tiene imágenes en la derecha
    }else{//Si tiene imágenes en la derecha           
        var elemento = document.querySelector( '#i_1' );            
        var posXcircIzquierda = elemento.getAttribute('cx');            
        posicionX_ = parseInt(posXcircIzquierda) + 600;
        posicionXcirculoDerecha = posicionX_;
        posXcircDerecha = posicionX_;
        posicionXrectanguloDerecha = ( parseInt(posXcircDerecha) + radio ) + ajusteSeparacionRectCirculo;
    }
        
    for(var c=0;c<preguntasArr.length;c++){
        $('#izquierda_'+c).css('position', 'absolute');
        $('#izquierda_'+c).css('left', posicionXcajaIzquierda);
        $('#izquierda_'+c).css('top', posicionesYcajasIzquierda_arr[posiciones1_arr[c]]);
        
        document.getElementById('i_'+(c+1)).setAttributeNS(null,"cx",posicionXcirculoIzquierda);//Posiciona en X los circulos SVG de la izquierda
        document.getElementById('i_'+(c+1)).setAttributeNS(null,"cy", posicionesYcirculosIzquierda_arr[posiciones1_arr[c]]);//Posiciona en Y los circulos SVG de la izquierda                
        document.getElementById('ic_'+(c+1)).setAttributeNS(null,"x",posicionXrectangulosIzquierda);//Posiciona en X los rectángulos SVG de la izquierda
        document.getElementById('ic_'+(c+1)).setAttributeNS(null,"y", posicionesYrectangulosIzquierda_arr[posiciones1_arr[c]]);//Posiciona en Y los rectángulos SVG de la izquierda
                        
        $('#derecha_'+c).css('position', 'absolute');
        $('#derecha_'+c).css('left', posicionXcajaDerecha);
        $('#derecha_'+c).css('top', posicionesYcajasDerecha_arr[posiciones2_arr[c]]);
        $('#derecha_'+c).css('left', '240px');
        
        $( '#centro').css('left', '-20px');
        
        document.getElementById('d_'+(c+1)).setAttributeNS(null,"cx",posicionXcirculoDerecha);//Posiciona en X los circulos SVG de la derecha
        document.getElementById('d_'+(c+1)).setAttributeNS(null,"cy", posicionesYcirculosDerecha_arr[posiciones2_arr[c]]);//Posiciona en Y los circulos SVG de la derecha        
        document.getElementById('dc_'+(c+1)).setAttributeNS(null,"x",posicionXrectanguloDerecha);//Posiciona en X los rectángulos SVG de la derecha
        document.getElementById('dc_'+(c+1)).setAttributeNS(null,"y", posicionesYrectangulosDerecha_arr[posiciones2_arr[c]]);//Posiciona en Y los rectángulos SVG de la derecha
    }         
}
function moveSection(idStr, xOffset, yOffset) {
    var domElemnt = document.getElementById(idStr);
    if (domElemnt) {
        var transformAttr = ' translate(' + xOffset + ',' + yOffset + ')';
        domElemnt.setAttribute('transform', transformAttr);
    }
}
function ajustaAltura (){  
     //espaciadoDer += $('#derecha_' + (j-2)).height();
    var _strTextoDivMayor = '';
    if($('#derecha').height() > $('#izquierda').height()){
        //$('#izquierda_' + (j-1)).height($('#derecha_' + (j-1)).height());
        _strTextoDivMayor = 'derecha';
    }else{
        //$('#derecha_' + (j-1)).height($('#izquierda_' + (j-1)).height());
        _strTextoDivMayor = 'izquierda';
    }
    $('#centro').height($('#' + _strTextoDivMayor).height() + 10);
    $('#elementosSVG').width($('#centro').width());                   
    $('#respuestasRadialGroup').height( $('html').height() - 2);//$('#respuestasRadialGroup').height( alturaSVG  +  'px'); 
}
function ajustaAlturasRelacionaLineas (id){        
    if($('#derecha_' + (id)).height() > $('#izquierda_' + (id)).height()){
        $('#izquierda_' + (id)).height($('#derecha_' + (id)).height()  );        
    }else{
        $('#derecha_' + (id)).height($('#izquierda_' + (id)).height()  );        
    }
}
function reacomodaPuntosNegros (){
    var cir;
    /*for(var id=0;id<cuantosPuntos;id++){
        if(id === 0){
             espaciadoGlobal = 40;             
        }else{
            espaciadoGlobal += $('#izquierda_' + (id-1)).height() + margen ;
            //setea '+"i_"+(id+1)  + 'con ' + espaciadoGlobal);                        
        }
        cir = document.getElementById( "i_"+(id+1) );
        cir.setAttributeNS(null, "cy", espaciadoGlobal );
        cir = document.getElementById( "d_"+(id+1) );
        cir.setAttributeNS(null, "cy", espaciadoGlobal );
    }*/
    
    for(var id=0;id<cuantosPuntos;id++){                
        var elemento = $('#izquierda_' + (id));
        var posicion = elemento.position();        
        
        cir = document.getElementById( "i_"+(id+1) );
        cir.setAttributeNS(null, "cy", posicion.top + 22 );
        cir = document.getElementById( "d_"+(id+1) );
        cir.setAttributeNS(null, "cy", posicion.top + 22 );
    }
    ajustaAltura();
    pintaLineasResueltas();
}
function inicioTrazado (e,thisId){
    evitarPintadoCirculo = false;
    congelaSonidoIncorrecto = false;
    terminoTrazo = false;
    permiteOver = true;
    incremento = incremento + 1;//Checar si aplica se agregó para que funcione hacer varias lineas ya montado en el proyecto
    congelaBotonesDerechos = false;                                
    objetoActual = document.getElementById(e.target.id);
    
    $ ( '#'+e.target.id ).css('fill', colorSeleccion);
    $ ( '#'+e.target.id ).css('stroke', colorSeleccion);

    arrTmpInit = objetoActual.getAttribute('id').split('_');    
    //arrRespuestasLineas[arrTmpInit[1] - 1] = arrTmpInit[1];
    //objetoActual.getAttribute('id');

    posXClic1 = obtenerDimensiones(objetoActual).x;
    posYClic1 = obtenerDimensiones(objetoActual).y;
    
    desfaseIzquierda = $("#elementosSVG").offset().left;
    desfaseSuperior = $("#elementosSVG").offset().top;
    areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    
    if(arrTmpInit[0] === 'ic' || arrTmpInit[0] === 'dc'){//Se da clic en los rectángulos  
        centroDeCirculo = 0;                        
        if(dispositivoMovil){//Si es un dispositivo móvil
            posXClic1 = e.originalEvent.touches[0].pageX;
            posYClic1 = e.originalEvent.touches[0].pageY;
        }else{//Sino es un dispositivo móvil
            posXClic1 = e.pageX;
            posYClic1 = e.pageY;
        }
    }else{//Si da click en los círculos        
        centroDeCirculo = parseInt(e.target.getAttribute("r"));//Obtiene la posición centro de los puntos
        posXClic1 = (posXClic1 - desfaseIzquierda) + centroDeCirculo;
        posYClic1 = (posYClic1 - desfaseSuperior) + centroDeCirculo;
        posYClic1 = posYClic1 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic        
    }            

    idBoton1 = thisId;
        
    if(direccionTrazado === 'izquierda-derecha'){
        colocaRecuadrosIzquierda(e);
    }else{
        colocaRecuadrosDerecha(e);
    }    
}
function finTrazado (e,thisId){
    terminoTrazo = true;
    evitarPintadoCirculo = true;
        
    if(congelaBotonesDerechos == false){
        congelaSonidoIncorrecto = true;
        
        //objetoActual = document.getElementById(e.target.id);
        objetoActual = document.getElementById(thisId);
        arrTmpFin = objetoActual.getAttribute('id').split('_');
        
        //arrRespuestasLineas[arrTmpInit[1] - 1]  = {'id': arrTmpInit[1], 'selected': arrTmpFin[1]};
        //.select = arrTmpFin[1]; 
        if(direccionTrazado === 'izquierda-derecha'){            
            arrRespuestasLineas[arrTmpInit[1] - 1]  = {'id': arrTmpInit[1], 'selected': arrTmpFin[1]};
            actualizaArrayRespuesta(arrTmpInit[1] - 1, arrTmpFin[1]);
        }else{            
            arrRespuestasLineas[arrTmpFin[1] - 1]  = {'id': arrTmpFin[1], 'selected': arrTmpInit[1]};
            actualizaArrayRespuesta(arrTmpFin[1]- 1, arrTmpInit[1] );
        }
        
        posXClic2 = obtenerDimensiones(objetoActual).x;
        posYClic2 = obtenerDimensiones(objetoActual).y;//Para crear la linea al liberar el mouse sobre circulo de la derecha                                
        var desfaseIzquierda = $("#elementosSVG").offset().left;
        var desfaseSuperior = $("#elementosSVG").offset().top;

        areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
        centroDeCirculo = parseInt(e.target.getAttribute("r"));//Obtiene la posición centro de los puntos
        
        if(arrTmpInit[0] === 'ic' || arrTmpInit[0] === 'dc'){//Se da clic en los rectángulos       
            var cadena = thisId;
            cadena = cadena.substring(0,2);            
            centroDeCirculo = 0;
            centroDeCirculo2 = 15;
            posXClic2 = e.pageX;
            posYClic2 = e.pageY;          
                 
            var thisIdGlobalTemp = idBoton1;
            if(arrTmpInit[0] === 'ic'){                                                              
                thisIdGlobalTemp = thisIdGlobalTemp.replace('ic', 'i');
            }
            if(arrTmpInit[0] === 'dc'){                                                
                thisIdGlobalTemp = thisIdGlobalTemp.replace('dc', 'd');
            }
            posXClic1 = $( '#'+thisIdGlobalTemp ).position().left;
            posYClic1 = $( '#'+thisIdGlobalTemp ).position().top;

            posXClic1 = (posXClic1-desfaseIzquierda) + centroDeCirculo;
            posYClic1 = (posYClic1-desfaseSuperior) + centroDeCirculo2;
            posYClic1 = posYClic1 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic                        
        }else{//Si da click en los círculos     
            var cadena = thisId;
            cadena = cadena.substring(0,2);
            if(cadena === 'dc'){                        
                if(tieneImagenesDerecha){              
                    if(tieneImagenesIzquierda === false){                                
                        obtienePosicionesCirculos(thisId);
                    }else{
                        posXClic2 = posXClic2-desfaseIzquierda;//+ centroDeCirculo
                        posYClic2 = posYClic2-desfaseSuperior;//+ centroDeCirculo
                    }
                }
                if(tieneImagenesIzquierda && tieneImagenesDerecha){                    
                    obtienePosicionesCirculos(thisId);
                }
                if(tieneImagenesIzquierda){              
                    if(tieneImagenesDerecha === false){                                
                        obtienePosicionesCirculos(thisId);
                    }else{
                        posXClic2 = posXClic2-desfaseIzquierda;//+ centroDeCirculo
                        posYClic2 = posYClic2-desfaseSuperior;//+ centroDeCirculo
                    }
                }
            }                                    
            posYClic2 = posYClic2 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
        }  

        //crearLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke);  
        thisIdGlobal = thisId;
        idBoton1Global = idBoton1;
        
        crearLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, colorBien, anchoStroke);
        idLineaGlobal = idLinea;  
        
        eliminarLineaAnterior("lineaCreada");
        incremento = incremento + 1;//Checar si se suma 1 o 2
        congelaBotonesDerechos = true;

        //document.onmouseup=detenerEventos;//Elimina el evento onmouseup
        //document.onmousemove=detenerEventos;//Elimina el evento onmousemove
        // Comento la parte de deshabilitar los circulos        
        /*cambiaColorObjeto(thisId);
        cambiaColorObjeto(idBoton1);
        cambiaColorObjeto(idLinea);*/                
        
              
        
        posColores = posColores + 1;
        if(posColores == colores_arr.length){
            posColores = 0;
        }
               
        if(direccionTrazado === 'izquierda-derecha'){        
            eliminarLineasAnteriores(posXClic1, posYClic1, posXClic2,posYClic2,  arrTmpInit[1]);
        }else{    
            eliminarLineasAnteriores(posXClic1, posYClic1, posXClic2,posYClic2, arrTmpInit[1]);
        }
        congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
        direccionTrazado = "";//Resetea el status de trazado
        eliminarRecuadros();
        permiteOver = false;
        siguientePregunta( c03id_tipo_pregunta, arrTmpInit[1] );                         
    }
}
function obtienePosicionesCirculos (thisId){  
    var cadena2;
    var data2;
    var data3;
            
    cadena2 = thisId;
    cadena2 = cadena2.replace('c', '');                            
    data2 = $( '#'+cadena2 ).attr('cx');
    data3 = $( '#'+cadena2 ).attr('cy');                            
    posXClic2 = parseInt(data2) + radio;
    
    if(tieneImagenesIzquierda && tieneImagenesDerecha){
        posXClic2 = posXClic2 - 3;
        posYClic2 = parseInt(data3) - ((radio*2) -4);
    }else{
        if(tieneImagenesIzquierda && tieneImagenesDerecha === false){              
            posYClic2 = (parseInt(data3) - radio*2) + 4;    
        }else{
            if(tieneImagenesIzquierda === false && tieneImagenesDerecha){              
                posYClic2 = (parseInt(data3) - radio*2) + 4;    
            }else{
                posYClic2 = parseInt(data3) - radio;
            }
            
        }        
    }    
}

function congelarBotones (){        
    if(thisIdGlobal.indexOf('dc', 0) === '-1'){//Si existe la caja derecha        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        thisIdGlobal = thisIdGlobal.replace('dc', 'd');
        thisIdGlobal = thisIdGlobal.replace('ic', 'i');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
    }else{        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        thisIdGlobal = thisIdGlobal.replace('d', 'dc');
        thisIdGlobal = thisIdGlobal.replace('i', 'ic');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        
        thisIdGlobal = thisIdGlobal.replace('dcc', 'd');
        thisIdGlobal = thisIdGlobal.replace('icc', 'i');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        
        thisIdGlobal = thisIdGlobal.replace('i', 'dc');
        thisIdGlobal = thisIdGlobal.replace('d', 'ic');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        
        thisIdGlobal = thisIdGlobal.replace('icc', 'ic');
        thisIdGlobal = thisIdGlobal.replace('dcc', 'dc');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
                
        thisIdGlobal = thisIdGlobal.replace('icc', 'dc');
        thisIdGlobal = thisIdGlobal.replace('dcc', 'ic');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        
        thisIdGlobal = thisIdGlobal.replace('dc', 'ic');
        thisIdGlobal = thisIdGlobal.replace('ic', 'dc');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        
        thisIdGlobal = thisIdGlobal.replace('dc', 'i');
        thisIdGlobal = thisIdGlobal.replace('ic', 'd');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
        
        thisIdGlobal = thisIdGlobal.replace('d', 'i');
        thisIdGlobal = thisIdGlobal.replace('i', 'd');        
        desactivaBotones(thisIdGlobal, idBoton1Global);
    }            
}
function desactivaBotones (thisIdGlobal, idBoton1Global){
    $( '#'+thisIdGlobal ).unbind(eventoPress);//Congela botones i y c
    $( '#'+thisIdGlobal ).unbind(eventoRelease);    
    $( '#'+idBoton1Global ).unbind(eventoPress);
    $( '#'+idBoton1Global ).unbind(eventoRelease);        
    
    $( '#'+thisIdGlobal ).css({cursor:"default"});
    $( '#'+idBoton1Global ).css({cursor:"default"});
}   
function cambiaColoresCorrectos (){
    cambiaColorCirculo(thisIdGlobal, colorBien);
    cambiaColorCirculo(idBoton1Global, colorBien);
    $("#capaSuperior").css("display", "none");
    //cambiaColorLinea(idLineaGlobal);
}
function cambiaColoresInCorrectos (){    
    cambiaColorCirculo(thisIdGlobal, colorMal);
    cambiaColorCirculo(idBoton1Global, colorMal);
    cambiaColorLinea(idLineaGlobal, colorMal);
}
function cambiaColorCirculo(idRecibido, color){
    idRecibido = idRecibido.replace('dc', 'd');
    idRecibido = idRecibido.replace('ic', 'i');
    $ ( '#'+idRecibido ).css('stroke', color);
    $ ( '#'+idRecibido ).css('fill', color);
}
function cambiaColorLinea(idRecibido, color){
    $ ( '#'+idRecibido ).css('stroke', color);
}
function regresaColoresNormales (){
    if(thisIdGlobal === 'undefined'){
        thisIdGlobal = "aa";
        idBoton1Global = "aaa";
    }
    thisIdGlobalTemp = thisIdGlobal.replace('dc', 'd');
    thisIdGlobalTemp = thisIdGlobal.replace('ic', 'i');    
    idBoton1GlobalTemp = idBoton1Global.replace('dc', 'd');
    idBoton1GlobalTemp = idBoton1Global.replace('ic', 'i');
    
    reseteaColorCirculo(thisIdGlobalTemp, contornoCirculo, rellenoCirculo);
    reseteaColorCirculo(idBoton1GlobalTemp, contornoCirculo, rellenoCirculo);    
}
function reseteaColorCirculo(idRecibido, colorContorno, colorRelleno){    
    idRecibido = idRecibido.replace('dc', 'd');
    idRecibido = idRecibido.replace('ic', 'i');    
    $ ( '#'+idRecibido ).css('stroke', colorContorno);
    $ ( '#'+idRecibido ).css('fill', colorRelleno);
}
function regresaColoresIniciales (){
    cambiaColorCirculo(thisIdGlobal, colorBien);
    cambiaColorCirculo(idBoton1Global, colorBien);    
}
function tiempoEspera (){    
    setTimeout(function(){          
        desvanece();
    }, 800);
}

function desvanece (){
    $( '#'+idLineaGlobal ).animate({//Regresala a la parte superior
        opacity: "-="+1
    }, 400, function() {//Tiempo de la animación //animación completada            
        regresaColoresNormales();
        $("#capaSuperior").css("display", "none");
    });
}

function eventosDePuntos (id){
    presionadoGlobal = id;
    
    //$(document).mousemove(function(event) {    
    $( 'body' ).bind(eventoMove,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha        
        obtienePositionMouse(e, this);
    });
    $(window).scroll(function(event) {        
        if(areaScrollDerecha != $(document).scrollLeft()){
            posicionXmouse -= areaScrollDerecha;
            areaScrollDerecha = $(document).scrollLeft();
            posicionXmouse += areaScrollDerecha;
        }
        if(areaScrollSuperior != $(document).scrollTop()){
            posicionYmouse -= areaScrollSuperior;
            areaScrollSuperior = $(document).scrollTop();
            posicionYmouse += areaScrollSuperior;
        }                    
        procesaTrazado(desfaseIzquierda, desfaseSuperior, centroDeCirculo);
    });
    function obtienePositionMouse(e, _this){           
        if( dispositivoMovil ){//Si es un dispositivo Móvil            
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];                        
            var elm = $(_this).offset();
            var x = touch.pageX - elm.left;
            var y = touch.pageY - elm.top;

            if(x < $(_this).width() && x > 0){
                if(y < $(_this).height() && y > 0){
                    posicionXmouse = touch.pageX;
                    posicionYmouse = touch.pageY;
                }
            }
        }else{//Sino es un dispositivo Móvil
            if (!e) { var e = window.event; }
            if (e.target) {    area = e.target;
            } else if (e.srcElement) {
                area = e.srcElement;
            }
            if (document.layers){
                posicionXmouse = e.x;            posicionYmouse = e.y;
            }else if (document.all){
                posicionXmouse = event.clientX;  posicionYmouse = event.clientY;
            }else if (document.getElementById){
                posicionXmouse = e.clientX;      posicionYmouse = e.clientY;
            }
        }
        
        obtieneSegundoElemento(posicionXmouse, posicionYmouse);
        procesaTrazado(desfaseIzquierda, desfaseSuperior, centroDeCirculo);
    }
                
    $(document).on(eventoRelease,function(event){//document.onmouseup=function(e){
        eliminarLineaActual("lineaCreada");
        $(document).unbind('mousemove');//Elimina el evento onmousemove
        $( 'body' ).unbind(eventoMove);
        $(window).unbind('scroll');
                        
        congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
        direccionTrazado = "";//Resetea el status de trazado
        eliminarRecuadros();
        
        if(evitarPintadoCirculo === false){
            $ ( '#'+presionadoGlobal ).css('stroke', contornoCirculo);
            $ ( '#'+presionadoGlobal ).css('fill', rellenoCirculo);
        }
        
        if(congelaSonidoIncorrecto === false){            
            sndIncorrecto();
            $(document).unbind(eventoRelease);
        }
    });
}
function obtieneSegundoElemento (){
    if( dispositivoMovil ){
        var lado;
        var endTarget = document.elementFromPoint(
            posicionXmouse,
            posicionYmouse
        );
        id_temp = $(endTarget).attr('id');
        if(id_temp !== 'undefined'){
            if(direccionTrazado === 'izquierda-derecha'){
                lado = 'derecha';
            }else{
                lado = 'izquierda';
            }
            if(id_temp.indexOf('_') !== -1 ){
                var num = parseInt( id_temp.substring( id_temp.indexOf('_', 0)+1, id_temp.length  ) );
                num = num - 1;                
                //$('#'+lado+'_'+num).addClass('pintaCuadro');                
            }else{
                for(var d=0;d<preguntasArr.length;d++){
                   // $('#'+lado+'_'+d).removeClass('pintaCuadro');                    
                }
            }
        }
    }
}
function removerEventos (){    
    //circuloCreadoIzquierda.onmousedown = detenerEventos;
    document.onmousemove = detenerEventos;
    $(document).unbind('mousemove');//Elimina el evento onmousemove
    $(window).unbind('scroll');
    document.onmouseup = detenerEventos;
    $(document).unbind(eventoRelease);
    
    congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
    direccionTrazado = "";//Resetea el status de trazado
    eliminarRecuadros();
}
function procesaTrazado (desfaseIzquierda, desfaseSuperior, centroDeCirculo){
    posXClic2 = posicionXmouse;
    posXClic2 = (posXClic2-desfaseIzquierda) - centroDeCirculo/4;
        
    if(direccionTrazado === 'izquierda-derecha'){
        posXClic2 = posXClic2 - 1;//Se resta un pixel para que el puntero pueda tocar el botón derecho y no obstruya la línea trazada
    }else{        
        posXClic2 = posXClic2 + 1;//Se resta un pixel para que el puntero pueda tocar el botón derecho y no obstruya la línea trazada
    }
            
    posYClic2 = posicionYmouse;
    posYClic2 = (posYClic2-desfaseSuperior) - centroDeCirculo/4;
    
    if(navegador === 'Firefox'){
        if(versionInt <= 31){
            posXClic2 = posXClic2 + 2;
            posYClic2 = posYClic2 + 15;
        }
    }
    
    //crearLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke);
    crearLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, '#0066FF', anchoStroke);
    eliminarLineaAnterior("lineaCreada");
}
function colocaRecuadrosIzquierda (e){    
    /*var circuloActual = document.getElementById(e.target.id);
    var datosObtenidos_arr = circuloActual.getAttribute('id').split('_');
    var idObtenidoInt = ( parseInt(datosObtenidos_arr[1]) ) - 1;
    $('#izquierda_'+idObtenidoInt).addClass('pintaCuadro');*/
}
function colocaRecuadrosDerecha (e){
    /*var circuloActual = document.getElementById(e.target.id);
    var datosObtenidos_arr = circuloActual.getAttribute('id').split('_');
    var idObtenidoInt = ( parseInt(datosObtenidos_arr[1]) ) - 1; 
    //console.log($('#derecha_'+idObtenidoInt).prop('id'));
    $('#derecha_'+idObtenidoInt).addClass('pintaCuadro');*/
}
function eliminarRecuadros (){    
    for(var g=0;g<cuantosPuntos;g++){       
       $('#izquierda_'+g).removeClass('pintaCuadro');
       $('#derecha_'+g).removeClass('pintaCuadro');       
    }
}
function eliminarRecuadrosIzquierda (){    
    for(var g=0;g<cuantosPuntos;g++){
        $('#izquierda_'+g).removeClass('pintaCuadro');
    }
}
function eliminarRecuadrosDerecha (){    
    for(var g=0;g<cuantosPuntos;g++){
       $('#derecha_'+g).removeClass('pintaCuadro');
    }
}
function eliminarLineaActual(identificador){
    var d = document.getElementById(nombreSVG);
    lineaSVGeliminar = document.getElementById(identificador+(incremento));//Asigna la linea actual para ser eliminada
    if(lineaSVGeliminar !== null){
        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
    }
}
function eliminarLineaAnterior(identificador){
    var d = document.getElementById(nombreSVG);
    lineaSVGeliminar = document.getElementById(identificador+(incremento-1));//Asigna la linea anterior para ser eliminada
    
    try {
        //console.log('lineaSVGeliminar '+lineaSVGeliminar.getAttribute('id'));
        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior        
    }
    catch(err) {
        //console.log('ENTRA AL CATCH');
        //document.getElementById("demo").innerHTML = err.message;
    }
    
        
    /*if(lineaSVGeliminar == null){        
        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
    }*/
}

function generaEstilosImagenFondo() {
    var $divImagen = $("#centro");
    var arrPreguntas = obj.preguntas;
    var arrRespuestas = obj.respuestas;
    var numPreguntas = arrPreguntas.length;
    if (strAmbiente === 'DEV') {
        $divImagen.css({backgroundImage: 'url("../../../../' + strRutaLibro + obj.pregunta.url + '")',
            backgroundRepeat: "no-repeat"});
    } else {
        $divImagen.css({backgroundImage: 'url("' + obj.pregunta.url + '")',
            backgroundRepeat: "no-repeat"});
    }
    $('svg rect').remove(); // Se eliminan todos los rectangulos del SVG
    $('.preguntaRelaciona').remove(); // Se eliminan todos los rectangulos de las respuestas
    $divImagen.css({textAlign: "center", width: "100%", height: "100%", backgroundSize: "100% 100%"});
    $('svg');
    
    for(var i=1, j=0; i<=numPreguntas; i++, j++){
        $('#i_'+i).attr('cx', arrPreguntas[j].cx);
        $('#i_'+i).attr('cy', arrPreguntas[j].cy);
        $('#d_'+i).attr('cx', arrRespuestas[j].cx);
        $('#d_'+i).attr('cy', arrRespuestas[j].cy);
    }
}

/**
 * Funcion que elimina las lineas anteriores que existian en el mismo punto
 * @param {type} identificador
 * @param {type} posXClic1
 * @param {type} posYClic1
 * @returns {undefined}
 */
function eliminarLineasAnteriores(posXClic1, posYClic1, posXClic2,posYClic2, respPerm){     // Revisar a detalle    
    var d = document.getElementById(nombreSVG);
    var $lineaActual = $('line').last();
    
    $('line').each(function(){
        var limite = 10; // margen de error que le doy a las lineas
        if(direccionTrazado === 'izquierda-derecha'){
            if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea
                ((posXClic1 > parseInt($(this).attr('x1')) - limite && posXClic1 < parseInt($(this).attr('x1')) + limite)  &&
                (posYClic1 > parseInt($(this).attr('y1')) - limite && posYClic1 < parseInt($(this).attr('y1')) + limite) ||
                (posXClic2 > parseInt($(this).attr('x1')) - limite && posXClic2 < parseInt($(this).attr('x1')) + limite)  &&
                (posYClic2 > parseInt($(this).attr('y1')) - limite && posYClic2 < parseInt($(this).attr('y1')) + limite))
                     && $(this).attr('id') !== $lineaActual.attr('id') ){
                 $(this).remove();
             }
             if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea
                ((posXClic2 > parseInt($(this).attr('x2')) - limite && posXClic2 < parseInt($(this).attr('x2'))+ limite)  &&
                (posYClic2 > parseInt($(this).attr('y2')) - limite && posYClic2 < parseInt($(this).attr('y2')) + limite) ||
                (posXClic1 > parseInt($(this).attr('x2')) - limite && posXClic1 < parseInt($(this).attr('x2'))+ limite)  &&
                (posYClic1 > parseInt($(this).attr('y2')) - limite && posYClic1 < parseInt($(this).attr('y2')) + limite))
                     && $(this).attr('id') !== $lineaActual.attr('id') ){
                 $(this).remove();
             }
             //Elimino las lineas que se trazan en la misma columna
//             if(parseInt($(this).attr('x1')) > parseInt($(this).attr('x2')) - 5 && parseInt($(this).attr('x1')) > parseInt($(this).attr('x2')) + 5 ){
//                 $(this).remove();
//             }
        }else{
            if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea
                ((posXClic1 > parseInt($(this).attr('x2')) - limite && posXClic1 < parseInt($(this).attr('x2')) + limite)  && 
                (posYClic1 > parseInt($(this).attr('y2')) - limite && posYClic1 < parseInt($(this).attr('y2')) + limite) ||
                ((posXClic2 > parseInt($(this).attr('x2')) - limite && posXClic2 < parseInt($(this).attr('x2')) + limite)  && 
                (posYClic2 > parseInt($(this).attr('y2')) - limite && posYClic2 < parseInt($(this).attr('y2')) + limite)))  
                 && $(this).attr('id') !== $lineaActual.attr('id') ){            
                 $(this).remove();
             }
             if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea
                ((posXClic2 > parseInt($(this).attr('x1')) - limite && posXClic2 < parseInt($(this).attr('x1'))+ limite)  && 
                (posYClic2 > parseInt($(this).attr('y1')) - limite && posYClic2 < parseInt($(this).attr('y1')) + limite) ||
                (posXClic1 > parseInt($(this).attr('x1')) - limite && posXClic1 < parseInt($(this).attr('x1'))+ limite)  && 
                (posYClic1 > parseInt($(this).attr('y1')) - limite && posYClic1 < parseInt($(this).attr('y1')) + limite))
                 && $(this).attr('id') !== $lineaActual.attr('id') ){           
                 $(this).remove();
             }
             //Elimino las lineas que se trazan en la misma columna
//             if(parseInt($(this).attr('y1')) > parseInt($(this).attr('y2')) - 5 && parseInt($(this).attr('y1')) > parseInt($(this).attr('y2')) + 5 ){
//                 $(this).remove();
//             }
        }
                                        
        //var arrPosLinea[]
    });
    //lineaSVGeliminar = document.getElementById(identificador+(incremento-1));//Asigna la linea anterior para ser eliminada
//    if(lineaSVGeliminar !== null){
//        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
//    }
}

/**
 * Funcion que actuliza la informacion del array de arrRespuestasLineas,
 * esto se hace porque hay ocasiones donde se borran dos lineas y solo se 
 * actualiza una linea en el array
 * @param {type} indice
 * @param {type} posicionColocada
 * @returns {undefined}
 */
function actualizaArrayRespuesta(indice, posicionColocada){
    var arrResp = Object.keys(arrRespuestasLineas);
    var longResp = arrResp.length;
    for(var i=0; i<longResp; i++){
        if(parseInt(arrRespuestasLineas[i].selected) === parseInt(posicionColocada)
        && indice !== i){
            arrRespuestasLineas[i] = {'id': 0, 'selected': 0};
        }
    }
}
function crearLinea(identificador, posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke){       
    incremento++;
    lineaCreada = document.createElementNS(svgNS,"line");//Se declara la instancia de linea
    lineaCreada.setAttributeNS(null,"id",identificador+incremento);
    lineaCreada.setAttributeNS(null,"x1",posXClic1+4);
    lineaCreada.setAttributeNS(null,"y1",posYClic1+4);
        
    var centroDeCirculo2;    
    if(terminoTrazo){
        $("#capaSuperior").css("display", "block");
        if(arrTmpInit[0] === 'ic' || arrTmpInit[0] === 'dc'){//Se da clic en los rectángulos
            centroDeCirculo = 0;                         
            centroDeCirculo2 = 15;            
            var thisIdGlobalTemp = thisIdGlobal;
            if(arrTmpInit[0] === 'ic'){//Click en rectángulo izquierdo y libera en círculo o rectángulo derecho
                thisIdGlobalTemp = thisIdGlobalTemp.replace('i', 'd');
                thisIdGlobalTemp = thisIdGlobalTemp.replace('dc', 'd');
            }
            if(arrTmpInit[0] === 'dc'){//Click en rectángulo derecho y libera en círculo o rectángulo izquierdo
                thisIdGlobalTemp = thisIdGlobalTemp.replace('d', 'i');
                thisIdGlobalTemp = thisIdGlobalTemp.replace('ic', 'i');
            }                        
                        
            posXClic2 = $( '#'+thisIdGlobalTemp ).position().left;
            posYClic2 = $( '#'+thisIdGlobalTemp ).position().top;
            
            posXClic2 = (posXClic2-desfaseIzquierda) + centroDeCirculo;
            posYClic2 = (posYClic2-desfaseSuperior) + centroDeCirculo2;
            posYClic2 = posYClic2 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
            
        }else{//Si da click en los círculos
            var idAlterno = thisIdGlobal.substring(0,2);
            if(idAlterno === 'dc'){//Click en punto izquierdo y libera en rectángulo derecho
                centroDeCirculo = -26;
                centroDeCirculo2 = 30;                
                posXClic2 = (posXClic2-desfaseIzquierda) + centroDeCirculo;
                posYClic2 = (posYClic2-desfaseSuperior) + centroDeCirculo2;
                posYClic2 = posYClic2 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
            }else{                
                if(idAlterno === 'ic'){//Click en punto derecho y libera en rectángulo izquierdo
                    centroDeCirculo = 0;
                    centroDeCirculo2 = 18;  
                    thisIdGlobalTemp = thisIdGlobal;
                    thisIdGlobalTemp = thisIdGlobalTemp.replace('dc', 'd');
                    thisIdGlobalTemp = thisIdGlobalTemp.replace('ic', 'i');
                    
                    posXClic2 = $( '#'+thisIdGlobalTemp ).position().left;
                    posYClic2 = $( '#'+thisIdGlobalTemp ).position().top;

                    posXClic2 = (posXClic2-desfaseIzquierda) + centroDeCirculo;
                    posYClic2 = (posYClic2-desfaseSuperior) + centroDeCirculo2;
                    posYClic2 = posYClic2 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
                }else{
                    posXClic2 = (posXClic2-desfaseIzquierda) + centroDeCirculo;
                    posYClic2 = (posYClic2-desfaseSuperior) + centroDeCirculo;
                    posYClic2 = posYClic2 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
                }
                
            }
        }
        
       lineaCreada.setAttributeNS(null,"x2",posXClic2+5);//Posición final del trazado (posicion xy del mouse)
       lineaCreada.setAttributeNS(null,"y2",posYClic2+5);
    }else{
      if(direccionTrazado === 'izquierda-derecha'){            
          lineaCreada.setAttributeNS(null,"x2",posXClic2);//Posición final del trazado (posicion xy del mouse)
          lineaCreada.setAttributeNS(null,"y2",posYClic2);
      }else{
          lineaCreada.setAttributeNS(null,"x2",posXClic2+5);//Posición final del trazado (posicion xy del mouse)
          lineaCreada.setAttributeNS(null,"y2",posYClic2+5);    
      }
    }
    //lineaCreada.setAttributeNS(null,"x2",posXClic2+5);//Posición final del trazado (posicion xy del mouse)
    //lineaCreada.setAttributeNS(null,"y2",posYClic2+5);
    lineaCreada.setAttributeNS(null,"stroke",color);
    lineaCreada.setAttributeNS(null,"stroke-width",anchoStroke);
    lineaCreada.setAttributeNS(null,"class", "linea_relacionar");
    idLinea = identificador+incremento;    
    document.getElementById(nombreSVG).appendChild(lineaCreada);//Crea una linea nueva
}
function createSVGtext(etiqueta, x, y,tamano,relleno, alineacion, fuente) {
    var textoPreguntaCreado = document.createElementNS(svgNS, 'text');
    var maximoCaracteresPorLinea = 35;
    var altoLinea = 20;
    var palabras = etiqueta.split(" ");
    var linea = "";

    textoPreguntaCreado.setAttributeNS(null, 'x', x);
    textoPreguntaCreado.setAttributeNS(null, 'y', y);
    textoPreguntaCreado.setAttributeNS(null, 'font-size', tamano);
    textoPreguntaCreado.setAttributeNS(null, 'fill', relleno);
    textoPreguntaCreado.setAttributeNS(null, 'text-anchor', alineacion);
    textoPreguntaCreado.setAttributeNS(null, 'font-family', fuente);
    textoPreguntaCreado.setAttributeNS(null, 'contentEditable', "true");
    
    for (var n = 0; n < palabras.length; n++) {
        var lineaPrueba = linea + palabras[n] + " ";
        if (lineaPrueba.length > maximoCaracteresPorLinea){
            var spanSVG = document.createElementNS(svgNS, 'tspan');
            spanSVG.setAttributeNS(null, 'x', x);
            spanSVG.setAttributeNS(null, 'y', y);
            var nodoTextoSpan = document.createTextNode(linea);
            spanSVG.appendChild(nodoTextoSpan);
            textoPreguntaCreado.appendChild(spanSVG);
            linea = palabras[n] + " ";
            y += altoLinea;
        }else {
            linea = lineaPrueba;
        }
    }    
    var spanSVG = document.createElementNS(svgNS, 'tspan');
    spanSVG.setAttributeNS(null, 'x', x);
    spanSVG.setAttributeNS(null, 'y', y);
    var nodoTextoSpan = document.createTextNode(linea);
    spanSVG.appendChild(nodoTextoSpan);
    textoPreguntaCreado.appendChild(spanSVG);
    return textoPreguntaCreado;
}
function createSVGImage(image, posX, posY, anchoImg, altoImg ){
    var img = document.createElementNS(svgNS,'image');
    img.setAttributeNS(null,'height',altoImg);
    img.setAttributeNS(null,'width',anchoImg);
    img.setAttributeNS('http://www.w3.org/1999/xlink','href', image); // 'http://grupoeducare.com/web/images/inicio/ldca.png'
    img.setAttributeNS(null,'x',posX);
    img.setAttributeNS(null,'y',posY);
    img.setAttributeNS(null, 'visibility', 'visible');
    document.getElementById(nombreSVG).appendChild(img);
}
function separaUrls(cuerpo){ 
   var searchText = cuerpo;
   var strTexto = strip_tags(cuerpo);   
   var arrRespuesta = new Array();
    // urls will be an array of URL matches
    var urls = searchText.match(/\b(http|https|recursos\/imagenes)+(:\/\/)?(\S*)\.(\w{2,4})\b/ig);

    // you can then iterate through urls
//    if(urls !== null){
//        for (var i = 0, il = urls.length; i < il; i++) {
//            // do whatever with urls[i]
//            console.log(urls[i]);
//        }
//    }
    arrRespuesta[0] = sustraeTextoHTML(strTexto);
    if(urls !== null){
        arrRespuesta[1] = urls[0];
    }else{
        arrRespuesta[1] = null;
    }
    
    return arrRespuesta;
}
function strip_tags(input, allowed) {
  //  discuss at: http://phpjs.org/functions/strip_tags/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Luke Godfrey
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //    input by: Pul
  //    input by: Alex
  //    input by: Marc Palau
  //    input by: Brett Zamir (http://brett-zamir.me)
  //    input by: Bobby Drake
  //    input by: Evertjan Garretsen
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Eric Nagel
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Tomasz Wesolowski
  //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
  //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
  //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
  //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
  //   returns 2: '<p>Kevin van Zonneveld</p>'
  //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
  //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
  //   example 4: strip_tags('1 < 5 5 > 1');
  //   returns 4: '1 < 5 5 > 1'
  //   example 5: strip_tags('1 <br/> 1');
  //   returns 5: '1  1'
  //   example 6: strip_tags('1 <br/> 1', '<br>');
  //   returns 6: '1 <br/> 1'
  //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
  //   returns 7: '1 <br/> 1'

  allowed = (((allowed || '') + '')
    .toLowerCase()
    .match(/<[a-z][a-z0-9]*>/g) || [])
    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return input.replace(commentsAndPhpTags, '')
    .replace(tags, function($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}

function obtenerDimensionesImagen(url, ancho){
    var img = new Image();
    var nuevasDimensiones = new Object();
    var anchoDefault = ancho;
    img.src = url;
    var resolucion = img.width / anchoDefault;
    //img.width; //Ancho;
    //img.height; //Alto;
    nuevasDimensiones.width = anchoDefault;
    nuevasDimensiones.height = img.height / resolucion;
    return nuevasDimensiones;
}
function sustraeTextoHTML(strTexto){
    return strTexto.replace("&nbsp", " ");
}
function pintaLineasResueltas(){    
    var posInicioX, posInicioY, posFinX, posFinY;
    var color = "black";
    var anchoStroke = "2";      
    var areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    var centroDeCirculo = 0//Obtiene la posición centro de los puntos
    posColores = 0;//Se resetea la variable de posición de los colores
    incremento = 0;//Se resetea la variable de incremento de los lineas
    
    if(t11usuario.length >= 1){//Si hay datos por pintar
        for(j=1, i=0, g=1; j<=t11usuario.length; j++, i++){              
            if(t11usuario[i]){                                
                 var desfaseIzquierda = $("#elementosSVG").offset().left;
                 var desfaseSuperior = $("#elementosSVG").offset().top;

                 areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
                 centroDeCirculo = parseInt($('#i_'+j).attr("r"));//Obtiene la posición centro de los puntos                

                 posInicioX =  obtenerDimensiones($('#i_'+j)[0]).x;
                 posInicioY = obtenerDimensiones($('#i_'+j)[0]).y;

                 posInicioX = (posInicioX - desfaseIzquierda) + centroDeCirculo;
                 posInicioY = (posInicioY - desfaseSuperior) + centroDeCirculo;
                 posInicioY = posInicioY + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic

                 posFinX = obtenerDimensiones($('#d_'+t11usuario[i])[0]).x;
                 posFinY = obtenerDimensiones($('#d_'+t11usuario[i])[0]).y;                                       

                 posFinX = (posFinX - desfaseIzquierda) + centroDeCirculo;
                 posFinY = (posFinY - desfaseSuperior) + centroDeCirculo;    
                 posFinY = posFinY + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
                 posFinY = posFinY + 2;//Ajuste en Y para pintado correcto de lineas marcadas

                 crearLinea("lineaCreada", posInicioX, posInicioY,posFinX,posFinY, color, anchoStroke);

                 //Este fragmento de código pinta los circulos y las lineas                                    
                 cambiaColorSVG($('#i_'+j)[0]);                
                 cambiaColorSVG($('#d_'+t11usuario[i])[0]);                
                 cambiaColorObjeto( 'lineaCreada'+g );//Cambia el color a la linea                
                 g++;

                 posColores = posColores + 1;//Incrementa la siguiente posición del array colores.
                 if(posColores == colores_arr.length){
                     posColores = 0;
                 }                
                 arrRespuestasLineas[i] = {'id': j, 'selected': t11usuario[i]};                               
             }else{
                 arrRespuestasLineas[i] = {'id': j, 'selected': 0};
             }          
         }
    }            
}
function cambiaColorSVG(idRecibido){
    var objetoSVG = idRecibido;    
    objetoSVG.setAttributeNS(null,"stroke",colores_arr[posColores]);
    objetoSVG.setAttributeNS(null,"fill",colores_arr[posColores]);
}
function cambiaColorObjeto(idRecibido){
    var objetoSVG;
    objetoSVG = document.getElementById(idRecibido);
    objetoSVG.setAttributeNS(null,"stroke",colores_arr[posColores]);
    objetoSVG.setAttributeNS(null,"fill",colores_arr[posColores]);
}
function obtenerDimensiones (oElement) {
    var x, y, w, h;
    x = y = w = h = 0;        
    navegador=get_browser();
    navegador_version=get_browser_version();
    versionInt = parseInt(navegador_version);
    
    if (document.getBoxObjectFor) {//Mozilla
        var oBox = document.getBoxObjectFor(oElement);
        x = oBox.x-1;
        w = oBox.width;
        y = oBox.y-1;
        h = oBox.height;
    }  else if ( typeof oElement !== 'undefined'){
        if(oElement.getBoundingClientRect) {//IE
            var oRect = oElement.getBoundingClientRect();
            x = oRect.left-2;
            w = oElement.clientWidth;
            y = oRect.top-2;
            h = oElement.clientHeight;
            if(navegador === 'Firefox'){
                if(versionInt <= 32){
                    x = x + 2;
                    y = y + 15;
                }
            }
        }
    }    
    return {x: x, y: y, w: w, h: h};
}
function deshabilitaSeleccionDeTexto (){
    //document.onselectstart = function() {return false;};//Deshabilitar la selección de texto
    //document.onmousedown = function() {return false;};//Deshabilitar la selección de texto
}
function detenerEventos(e) {
    if (!e) e = window.event;
    if (e.stopPropagation) {
        e.stopPropagation();
    } else {
        e.cancelBubble = true;
    }
}
function obtenerRenglonesMaximo (numeroMayor, numeroMayorDerecha){
    var numRenglonesIzquierda = 0;
    var numRenglonesDerecha = 0;
    if(numeroMayor >= 1 && numeroMayor <= 26){//Si es de un renglón
        numRenglonesIzquierda = 1;
    }else{
        if(numeroMayor >= 27 && numeroMayor <= 52){//Si es de dos renglones
            numRenglonesIzquierda = 2;
        }else{
           if(numeroMayor >= 53 && numeroMayor <= 78){//Si es de tres renglones
               numRenglonesIzquierda = 3;
           }else{
                if(numeroMayor >= 79 && numeroMayor <= 104){//Si es de cuatro renglones
                   numRenglonesIzquierda = 4;
                }
            } 
        }
    }
    //console.log('numRenglonesIzquierda '+numRenglonesIzquierda);
        
    if(numeroMayorDerecha >= 1 && numeroMayorDerecha <= 36){//Si es de un renglón
        numRenglonesDerecha = 1;
    }else{
        if(numeroMayorDerecha >= 37 && numeroMayorDerecha <= 72){//Si es de dos renglones
            numRenglonesDerecha = 2;
        }else{
           if(numeroMayorDerecha >= 73 && numeroMayorDerecha <= 108){//Si es de tres renglones
               numRenglonesDerecha = 3;
           }else{
                if(numeroMayorDerecha >= 109 && numeroMayorDerecha <= 144){//Si es de cuatro renglones
                   numRenglonesDerecha = 4;
                }
            } 
        }
    }
    //console.log('numRenglonesDerecha '+numRenglonesDerecha);
    var ambosNumeros_Arr = new Array(numRenglonesIzquierda, numRenglonesDerecha);    
    return Math.max.apply(null, ambosNumeros_Arr);
}
function obtenerDatosCaja (mayorAmbos){
    var datos_arr = new Array();
    if(mayorAmbos === 1){
        espaciado = 18;
        alturaCaja = "60px";        
    }else{
        if(mayorAmbos === 2){
            espaciado = 18;
            alturaCaja = "62px";
        }else{
            if(mayorAmbos === 3){
                espaciado = 18;
                alturaCaja = "84px";
            }else{
                if(mayorAmbos === 4){
                    espaciado = 18;
                    alturaCaja = "108px";
                }
            }
        }
    }
    datos_arr[0] = alturaCaja;
    datos_arr[1] = espaciado;
    return datos_arr;
}