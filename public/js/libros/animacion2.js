var anchoFrase;
var posicionFrase;
var ajusteMovimiento = 10;

$(document).ready(function() {
    creaEstructuraAnimacion2();
});
function creaEstructuraAnimacion2 (idDiv){
    $( 'body' ).append(''+
    '<div id="principal_div">' +
        '<div id="animacion1_div" class="divElementos_De_Animacion1 alturaDivs"></div>' +
        '<div id="animacion2_div" class="divElementos_De_Animacion2 mascaraCentral alturaDivs"></div>' +
            '<div id="animacion3_div" class="divElementos_De_Animacion3 mascaraCentral2 alturaDivs"></div>' +
    '</div>' +
    '<div id="divFondo" class="div_fondo"></div>');

    crearAnimacion('Sesion 3', "A separar", "el dinero", '#E07000', '#503070', '250px', '260px');//# de sesión , Nombre de sesión parte 1, Nombre de sesión parte 2, Color del banner, Color de letra del # de la sesión, ancho de la imagen central, alto de la imagen central
}
function crearAnimacion (numeroSesion , frase1, frase2, colorFondo, colorNumSesion, anchoImagen, altoImagen){
    var tiempoDeEspera = 350;
    var duracionAnimacionMedia = 1000;
    var duracionAnimacionCorta = 300;

    $('#divFondo').css('background', colorFondo);
    $('#animacion1_div').append('<div id="idNumeroSesion" class="numero_Sesion" >'+numeroSesion+'</div>');
    $('#idNumeroSesion').css('color', colorNumSesion);

    var posicionCentro = obtieneCentrado('idNumeroSesion');
    $('#idNumeroSesion').css('left',posicionCentro+'px');

    $('#animacion1_div').delay(tiempoDeEspera).show().animate({ top: 130, opacity: '1' }, {duration: duracionAnimacionMedia, easing: 'easeOutElastic', complete: function(){//Animación para bajar Sesión # al centro del banner
        $('#animacion1_div').css('height', '52px');
        $('#animacion1_div').delay(tiempoDeEspera).show().animate({ top: 315, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'swing', complete: function(){
        }});//Animación para bajar Sesión # a la parte inferior del banner
        $('#idNumeroSesion').delay(tiempoDeEspera).show().animate({ top: 0, left: posicionCentro + 120, 'font-size': '35', opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'swing', complete: function(){//Animación para bajar Sesión # a la parte inferior del banner
            $('#animacion2_div').append('<div id="idNombreSesion" class="nombre_sesion" >'+frase1+'</div></div>');//Se agrega al stage la frase 1 del nombre de la sesión
            anchoFrase = $('#animacion2_div').css('width');
            posicionFrase = 0 - ( anchoFrase.replace('px', '') );
            posicionFrase = posicionFrase - ajusteMovimiento;

            $('#idNombreSesion').css('position', 'absolute');
            $('#idNombreSesion').delay(tiempoDeEspera).show().animate({ left: posicionFrase, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'swing', complete: function(){
                $('#animacion3_div').append('<div id="idNombreSesion2" class="nombre_sesion" >'+frase2+'</div></div>');
                anchoFrase = $('#animacion2_div').css('width');
                posicionFrase = $('#animacion2_div').position().left - ( anchoFrase.replace('px', '') );
                posicionFrase = posicionFrase - 160;

                $('#idNombreSesion2').css('position', 'absolute');
                $('#idNombreSesion2').delay(tiempoDeEspera).show().animate({ left: posicionFrase, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'swing', complete: function(){
                    $('#principal_div').append('<div id="animacion4_div"><div id="idCenterDiv" class="center"><div id="idDivInterno"><img class="imagenCentral" id="imagenBanner" src="images/sesion03.png"></div></div></div>');
                    $('#idCenterDiv').css('width', anchoImagen);
                    $('#idCenterDiv').css('height', altoImagen);
                    $('#idDivInterno').css('width', anchoImagen);
                    $('#idDivInterno').css('height', altoImagen);

                    $('#imagenBanner').css('width', anchoImagen);
                    $('#imagenBanner').css('height', altoImagen);
                    $('#imagenBanner').css('opacity', '0');
                    $('#imagenBanner').delay(tiempoDeEspera).show().animate({ top: -130, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'easeOutBounce', complete: function(){
                    }});//Animación para aparecer en fadeIn la imagen central
                }});
            }});
        }});
    }});
}
function obtieneCentrado (idDiv){
    var dato = 512 - $('#'+idDiv).width() / 2;
    return dato;
}