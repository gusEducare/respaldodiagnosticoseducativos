/*______         __              __                             ______                  __          
 |   __ \.-----.|  |.---.-.----.|__|.-----.-----.---.-.        |      |.-----.--------.|  |--.-----.
 |      <|  -__||  ||  _  |  __||  ||  _  |     |  _  |        |   ---||  _  |        ||  _  |  _  |
 |___|__||_____||__||___._|____||__||_____|__|__|___._|        |______||_____|__|__|__||_____|_____|                                                                                    
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com          
*********************************************************/
function elementoSeleccionado1(value){
    var valor=$("#combo1").val();
    var texto=$("#combo1 option:selected").text();
    alert("Esta seleccionado el valor "+valor+": "+texto);
}
function elementoSeleccionado2(value){
    var valor=$("#combo2").val();
    var texto=$("#combo2 option:selected").text();
    alert("Esta seleccionado el valor "+valor+": "+texto);
}
function accionesRelacionaCombo(){
    for(d=0;d<preguntasArr.length;d++){
        arrayValores[d]= new Array(preguntasArr[d]);
        arrayValores2[d]= new Array(respuestasArr[d]);
    }
    var list = $('#combo1')[0];
    $.each(arrayValores, function(index, text) {
        list.options[list.options.length] = new Option(arrayValores[index], index, text);
    });
    list = $('#combo2')[0];
    $.each(arrayValores2, function(index, text) {
        list.options[list.options.length] = new Option(arrayValores2[index], index, text);
    });
}
