 /*_______                                __                 _____          __                    
  |     __|.-----.-----.---.-.        .--|  |.-----.        |     |_.-----.|  |_.----.---.-.-----.
  |__     ||  _  |  _  |  _  |        |  _  ||  -__|        |       |  -__||   _|   _|  _  |__ --|
  |_______||_____|   __|___._|        |_____||_____|        |_______|_____||____|__| |___._|_____|
                 |__|                                                                                    
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com          
*********************************************************/
var jsons_arr = new Array();
var palabrasContestadas_arr = new Array();
var letrasContestadas_arr = new Array();
var fuente = 1;//1. Ninguna (se genera Aleatorio sólo la primera vez)    2.- Base de datos
var incremento = 0;
var terminaPintadoInterrumpido = false;
var ajusteSopa = 11;
var coloresHexadecimalesIDs = new Array();//= coloresHexadecimales[ rand(0, coloresHexadecimales.length - 1) ];//Para obtener un random
var cuantosColores = 83;
var posicionID = 0;
var band = false;
var reactivosLargos = false;
var palabrasResueltas = new Array();
var preguntasLargas = new Array('Es el valor que debes tener para responder por tus acciones.','Es algo que debemos hacer cuando nos comprometemos.','Cualidad para hacer las cosas en el tiempo acordado.','Es la caracteristica de que crean en ti.','Así se llama cuando te compromes con algo o alguien.');
var medidaGridGlobal = 50;
var medidaExtendida;
var medida;
var medidaMaxima;
var errorAlFormarSopa = false;
var coordenadasCorrecta=[];
var cadenaSopa="";

//variables de prueba
var sopaTest="P,B,P,C,D,C,N,O,Q,I,Z,S,E,E,H,B,O,L,D,D,D,U,E,K,X,E,R,L,E,M,H,E,V,N,O,C,D,Y,G,S,S,U,N,Y,T,X,Y,P,O,B,G,M,O,T,M,I,J,C,T,A,Q,M,R,T,I,N,M,L,M,L,C,L,S,U,K,D,G,I,A,X,O,T,U,D,Z,Y,A,C,E,J,B,L,I,U,D,L,L,D,Z,N,Q,H,N,T,I,U,D,F,Q,B,T,N,J,Y,U,Q,G,D,U,W,O,O,Y,C,S,D,G,U,I,K,A,O,S,H,U,M,O,R,P,K,F,J,Y,D";
sopaTest=sopaTest.split(",");
var CadenaTest=["1-1", "2-2", "3-3", "4-4", "5-5", "6-6", "7-7", "8-8", "9-9", "10-10", "11-11", "12-12", ";", "1-12", "2-11", "3-10", "4-9", "5-8", "6-7", "7-6", "8-5", "9-4", "10-3", "11-2", "12-1", ";"];
var colocateAllWords=[]; var gridSize;
var posIn=[]; var posEnd=[]; var isClick=false; var dragInterval;
listaPalabras=[];
/***********************************************************
  F U N C I O N E S     S O P A     D E     L E T R A S
***********************************************************/
function estructuraHTMLsopaDeLetras (estructuraBotones ){
    $("#respuestasRadialGroup").html(estructuraBotones);
    $("#gridSopadeletras").css('visibility', 'hidden');
    listaPalabras=[];
}
function shuffle( myArr ){
  //return myArr.sort( function() Math.random() - 0.5 );
}
function llenaArregloIDs(estructuraSopa){
    for(var f=0;f<=cuantosColores;f++){
        coloresHexadecimalesIDs[f] = f;
    }
    //coloresHexadecimalesIDs = shuffle( coloresHexadecimalesIDs );
}
function muestraSiguientePregunta(palabra_strIndex){
   ocultaPalabras();
   for(var i=0;i<palabrasResueltas.length;i++){       
       //$('#lista'+inc2).css('display','none');
       if(palabrasResueltas[i] === 0){
            if( typeof obj.pregunta.t11ayudas === 'undefined' || obj.pregunta.t11ayudas === 'no' ){
                //no aplica initial
            }else{ 
                $('#lista'+i).css('display','inherit');//initial
            }                           
        i = 1000;//Para salir del for
       }       
   }
}
function ocultaPalabras(){
   /*for(var i=0;i<palabrasResueltas.length;i++){
       $('#lista'+i).css('display','none');
   }*/
}
function leeInformacionBds(){
    if(fuente === 1){
    }else{
        jsons_arr = ['T','F','E','K','G','G','F','A','I','G','X','J',
                     'J','I','S','A','T','U','R','N','O','L','F','U',
                     'U','P','E','Z','M','N','G','W','T','R','H','B',
                     'P','K','L','R','V','E','E','H','V','H','Y','L',
                     'I','R','Z','U','R','M','R','P','J','W','Q','M',
                     'T','Q','H','R','T','A','H','C','T','P','M','V',
                     'E','U','Z','N','K','O','Q','U','U','U','P','Z',
                     'R','Y','I','O','E','X','N','R','M','R','N','I',
                     'Q','K','V','E','N','U','S','A','K','H','I','O',
                     'M','A','R','T','E','U','Q','N','U','U','C','O',
                     'F','A','B','J','F','T','B','O','E','X','F','R',
                     'L','R','E','M','L','F','D','F','J','H','Q','X'];
        palabrasContestadas_arr = [0,3];//Indica las palabras respondidas al momento
        letrasContestadas_arr = ['28-41-54-67-80-93-106-119','108-109-110-111-112'];//Posiciones de las celdas respondidas al momento
    }
    //var palabras_arr = "mercurio,venus,tierra,marte,jupiter,saturno,urano,neptuno,pluton";
    //var palabras_arr = "uno,dos,tres";
    //var palabras_str = "uno,dos";
        
    //var medidaMaxima = obj.preguntasTotal.length > 1 ? 10 : 11;
    for(var t=0;t<respuestasArr.length;t++){        
        elementoMasGrande = obtenerPalabraLarga(respuestasArr);            
//        if( json[idPreguntaGlobal].pregunta.t11medida === undefined ){
            if(elementoMasGrande.length <= 11){
               medida = 11;
            }else{
               medida = elementoMasGrande.length;
            }        
//        }else{
//            //medida = json[idPreguntaGlobal].pregunta.t11medida > medidaMaxima ? medidaMaxima : json[idPreguntaGlobal].pregunta.t11medida;
//            medida = json[idPreguntaGlobal].pregunta.t11medida;
//        }        
        var longitudMayor = elementoMasGrande.length;

        if(longitudMayor > medida){//Si la medida del texto es mayor a la medida indicada en el json
            medida = longitudMayor;
        }
        if(medida >= 17){//Si la longitud de la palabra es igual o mayor a 17 caracteres
            remove(respuestasArr, elementoMasGrande);
            console.log('LA PALABRA "'+elementoMasGrande+'" NO CABE EN LA SOPA DE LETRAS, POR LO QUE FUÉ ELIMINADA');
            remove2( json[0].respuestas, elementoMasGrande );
        }
    }
    
    for(var f=0, g=0;f<respuestasArr.length;f++){
        var tmpRespuesta = eliminarParrafos(respuestasArr[f]);
        var tmpRespuesta =  obtenerTextoParentesis(tmpRespuesta);
        var longitudRespuesta = longitudCadena(tmpRespuesta);
        if(longitudRespuesta <= medida){
            respuestasArr[g] = tmpRespuesta;
            g++;
        }
    }
    var palabras_str = respuestasArr.join();
    palabras_arr = palabras_str;
    
    elementoMasGrande = obtenerPalabraLarga(respuestasArr);
//    if( json[idPreguntaGlobal].pregunta.t11medida === undefined ){
        if(elementoMasGrande.length <= 11){
           medida = 11;
        }else{
           medida = elementoMasGrande.length<17 ? elementoMasGrande.length : 17;
            medidaGridGlobal = medida === 12 ? -20 : medidaGridGlobal;
            medidaGridGlobal = medida === 13 ? 20 : medidaGridGlobal;
        }        
//    }//else{
        //medida = json[idPreguntaGlobal].pregunta.t11medida > medidaMaxima ? medidaMaxima : json[idPreguntaGlobal].pregunta.t11medida;
//        medida = json[idPreguntaGlobal].pregunta.t11medida;
//    }
//    
//    if(medida === 12){
//        medidaGridGlobal = -20;
//    }
//    if(medida === 13){
//        medidaGridGlobal = 20;
//    }
//    if(medida >= 12){//Si la medida de la sopa es mayor a 11
//        medidaExtendida = true;
//    }
    
    llenaArregloIDs();
    $("#gridSopadeletras").sopa({"listadepalabras" : palabras_arr,"medidaGrid" : medida});
    $('table').css('margin-bottom','0.25rem');
}
function remove(arr, item) {
    for(var i = arr.length; i--;) {
        if(arr[i] === item) {
            arr.splice(i, 1);
        }
    }
}
function remove2(arr, item) {
    for(var i = arr.length; i--;) {
        var respuestaEnArray = arr[i].t13respuesta;
        respuestaEnArray = eliminarParrafos(respuestaEnArray);

        if(respuestaEnArray === item) {
            arr.splice(i, 1);
        }
    }
}
function obtenerPalabraLarga(respuestasTempArr){
    for(var g=0;g<respuestasTempArr.length;g++){
        respuestasTempArr[g] = eliminarParrafos( respuestasTempArr[g] );
    }
    
    var elementoMasGrande;
    var tamanos_arr = new Array();
    for(var g=0;g<respuestasTempArr.length;g++){
        tamanos_arr[g] = respuestasTempArr[g].length;
    }
    var elementoMaximo = Math.max.apply( Math, tamanos_arr );
    var posicionSeleccionada = 0;    
    for(var g=0;g<respuestasTempArr.length;g++){
        if(respuestasTempArr[g].length === elementoMaximo){
            posicionSeleccionada = g;
        }        
    }
    elementoMasGrande = respuestasTempArr[posicionSeleccionada];    
    return elementoMasGrande;
}
function obtenerTextoParentesis(tmpRespuesta){
    var matches = tmpRespuesta.match(/\([a-zA-Z]+\)*/);
    if(matches !== null){
        tmpRespuesta = matches[0].substring(1, matches[0].length - 1)
    }
   return tmpRespuesta;
}

function guardarSopaEnBDs(estructuraSopa){
//    estructuraSopa = eliminaElementosDuplicados(estructuraSopa);
    //console.log("GUARDA estructuraSopa: "+estructuraSopa);
}
function guardaPalabrasContestadas(indicePalabra){
    palabrasContestadas_arr.push(indicePalabra);
    palabrasContestadas_arr = eliminaElementosDuplicados(palabrasContestadas_arr);
    //console.log("GUARDA palabrasContestadas_arr: "+palabrasContestadas_arr);
}
function guardaLetrasContestadas(indicePalabra){
    letrasContestadas_arr.push(indicePalabra);
    letrasContestadas_arr = eliminaElementosDuplicados(letrasContestadas_arr);
    //console.log("GUARDA letrasContestadas_arr: "+letrasContestadas_arr);
}
function eliminaElementosDuplicados(arr) {
    var i,
        len=arr.length,
        out=[],
        obj={};

    for (i=0;i<len;i++) {
       obj[arr[i]]=0;
    }
    for (i in obj) {
       out.push(i);
    }
    return out;
}
function rand(min, max) {
  var offset = min;
  var range = (max - min) + 1;

  var randomNumber = Math.floor( Math.random() * range) + offset;
  return randomNumber;
}
function longitudCadena(respuesta){
    var intCaracteres = respuesta.length;
    return intCaracteres;
}
var incrementoSopa = 0;
function funcionesSopa (){
    (function( $, undefined ) {
        $.widget("claseSopa.sopa", $.ui.mouse, {
            options : {
                listadepalabras : null,
                medidaGrid : medidaGridGlobal},
            _mapearEventoACelda: function(event) {                
                var columnaActual = Math.ceil((event.pageX - this._celdaX) / this._celdaAncho);               
                var filaActual = Math.ceil((event.pageY - this._cellY) / this._celdaAlto);
                var el = $('#tablaGrid tr:nth-child('+filaActual+') td:nth-child('+columnaActual+')');                
                return el;
            },
            _create : function () {
                 gridSize=this.options.medidaGrid;
                while(gridSize<21){
                    if(colocateAllWords[0] !== undefined){
                        var contador=0;
                        $.each(colocateAllWords, function(index, element){
                            if(!element) contador++;
                        });
                        colocateAllWords=[];
                        if(gridSize===20 && contador!==0) console.log("no se logro acomodar la palabra");
                        if(contador===0) break;
                    }
                    $("#gridSopadeletras").sopa({"listadepalabras" : palabras_arr,"medidaGrid" : gridSize});
                    this.modelo = ayudanteJuego.preparaGrid(this.options.listadepalabras);
                    gridSize++;
                }
                this.inicio  = new Principal();
                this.zonaprincipal    = new areaPrincipal();
                this.trazos       = new Trazos();				                
                ayudanteJuego.renderearJuego(this.element[0],this.modelo);
                this.options.distance=0;//Setea la propiedades del mouse
                this._mouseInit();
                var cell = $('#tablaGrid tr:first td:first');
                this._celdaAncho = cell.outerWidth();
                //console.log('this._celdaAncho '+this._celdaAncho);
                this._celdaAlto = cell.outerHeight();
                this._celdaX = cell.offset().left;
                this._cellY = cell.offset().top;                                
            },            
            destroy : function () {                
                this.zonaprincipal.limpiar();
                this.trazos.limpiar();
                this.inicio.limpiar();
                this._mouseDestroy();
                return this;
            },
            _mouseStart: function(event) {//Eventos del mouse
                clearInterval(dragInterval);
                posIn[0]=event.pageX; posIn[1]=event.pageY;
                posEnd[0]=event.pageX; posEnd[1]=event.pageY;
                var pos = document.elementFromPoint(event.pageX, event.pageY); 
                    if ($(pos).is("td")) {
                        var cell=($(pos).attr("coord")+"").split("-");
                        firstPosition=[cell[1]*1, cell[0]*1];//X, Y
                        lastPosition=[cell[1]*1, cell[0]*1];//X, Y
                        $("#openWordSelecetion").css({opacity:"1", transform:"scale(01, 1)", top: $(pos).position().top, left:$(pos).position().left});
                    }    
                                        arrSeleccionado = [];//Para imprimir las palabras que seleccione el usuario
                   $(".rf-glowing").each(function (index, value) {
                       arrSeleccionado.push($(this).html());
                   });
                   arrPalabrasSeleccionadas.push(arrSeleccionado.join(""));
                    dragInterval = setInterval(function(){
                        var pos = document.elementFromPoint(posEnd[0], posEnd[1]);
                        if($(pos).is("td")){
                            var cell=($(pos).attr("coord")+"").split("-");
                            if(lastPosition[0]!==cell[1]*1 || lastPosition[1]!==cell[0]*1){
                                $("#closeWordSelecetion").css({opacity:"1", transform:"scale(1, 1)", top: $(pos).position().top, left:$(pos).position().left});
                                lastPosition=[cell[1]*1, cell[0]*1];//X, Y
                                calcularTrayectoria();
                            }
                        }                        
                    }, 75);         
            },
            _mouseDrag : function(event) {
                posEnd[0]=event.pageX; posEnd[1]=event.pageY;
            },
            _mouseStop : function (event) {
                clearInterval(dragInterval);
                posEnd[0]=event.pageX; posEnd[1]=event.pageY;
                var pos = document.elementFromPoint(posEnd[0], posEnd[1]);
                
                if($(pos).is("td")){
                    var cell=($(pos).attr("coord")+"").split("-");
                    if(lastPosition[0]!==cell[1]*1 || lastPosition[1]!==cell[0]*1){
                        $("#closeWordSelecetion").css({top: $(pos).position().top, left:$(pos).position().left});
                        lastPosition=[cell[1]*1, cell[0]*1];//X, Y
                        calcularTrayectoria();
                    }
                }
                var margenError=($("td").first().width())*2;
//                    var palabraseleccionada = '';//Obtiene palabra
////                    var letrasSeleccionadas = '';//Obtiene las letras seleccionadas para guardalas
//                    //$('.rf-glowing, .iluminado', this.element[0]).each(function() {//Se cambia esta condicion para que permita seguir seleccionando
//                    var coordenadas="";
//                    $('.rf-glowing', this.element[0]).each(function() {
//                        var u = $.data(this,"cell");
//                        palabraseleccionada += u.value;
//                        coordenadas=coordenadas+$(this).attr("coord")+",";
//                    });
                if(palabraFormada.length>2){
//                    $(".rf-glowing").css({background:"#dee594", boxShadow: "inset 0 2px 6px rgba(0,0,0,0.3)",transition:"box-shadow 0.35S, background 0.5s"});
                    var invert=false;//si es necesario invertir la palabra
//                    //direcciones en las que el usuario arrastro el maouse y pueden generar una palabra invertida
//                    
                        var margenErrorSup = posIn[1]-posEnd[1];
                        var margenErrorDer = posIn[0]-posEnd[0];
                        margenErrorSup>(-1*margenError) && margenErrorSup<margenError && margenErrorDer>margenError ? invert=true : invert=false;//se invierte al desplazar hacia la izquierda
                        if(!invert){
                            margenErrorSup>margenError ? invert=true : invert=false;//se invierte al desplazar hacia arriba y en diagonal superior
                        }
//                    
//                    
                    if(invert){
                        palabraFormada=palabraFormada.split("").reverse().join("");
                    }
                    var existe=false;
                    if(listaPalabras.length>0){
                        $.each(listaPalabras, function(index, element){
                            if(palabraFormada === element){
                                existe = true;
                                return false;
                            }
                        });
                    }
                    if(!existe && listaPalabras.length<json[idPreguntaGlobal].respuestas.length){
                        listaPalabras.push(palabraFormada);
                        pintaPalabraSeleccionada();
                    }
                }
                
                $(".onSelection").removeClass("onSelection");
                $("#openWordSelecetion, #closeWordSelecetion").css({opacity:"0", transform:"scale(0.1, 0.1)"});
//                 $(".rf-glowing").removeClass("rf-glowing");             
                 
                 /////////////////////////////////////////////////////////////////
//                if($( event.target).is("td")){}
//                var palabra_strIndex = this.modelo.listaPalabras.esUnaPalabraPresente(palabraseleccionada);
//                if (palabra_strIndex!=-1) {
//                    var inc = 0;                                                                                
//                    //$('.rf-glowing, .iluminado', this.element[0]).each(function() {                    
//                      $('.rf-glowing', this.element[0]).each(function() {
//                        Visualizador.select(this);
//                        var a = this;
//                        Object.keys(a).forEach(function (key) {
//                            inc++;                            
//                            var akey_string;
//                            var indicePalabra_int = parseInt(a[key]);
//                            indicePalabra_int = indicePalabra_int - ajusteSopa;
//                            akey_string = indicePalabra_int.toString();
//                            
//                            if(inc == 1){
//                                letrasSeleccionadas = akey_string;
//                            }else{
//                                letrasSeleccionadas = letrasSeleccionadas + '-' + akey_string;
//                            }
//                        });
//                        $.data(this,"selected", "true");
//                    });
//                    guardaLetrasContestadas(letrasSeleccionadas);
//                    ayudanteJuego.preparaMarca(palabra_strIndex);
//                    //console.log('letrasSeleccionadas '+letrasSeleccionadas);
//                    //console.log('palabra_strIndex '+palabra_strIndex);
//                    palabrasResueltas[palabra_strIndex] = 1;//Marca la palabra como resuelta
//                    
//                    if( typeof obj.pregunta.t11ayudas === 'undefined' || obj.pregunta.t11ayudas === 'no' ){
//                        //no aplica initial
//                    }else{
//                        $('#lista'+palabra_strIndex).css('display','inherit');
//                    }
//                    
//                    siguientePregunta( c03id_tipo_pregunta, palabra_strIndex+1 );
//                    //muestraSiguientePregunta(palabra_strIndex);
//                }else{                    
//                    sndIncorrecto();    
//                    siguientePregunta( c03id_tipo_pregunta, 0 );
//                    //console.log('suena incorrecto');
//                }
//                this.zonaprincipal.regresaANormal();//Regresa las celdas a estados normales
//                this.inicio.regresaANormal();
//                this.trazos.regresaANormal();      
//                
//                
//                if(band === true){   posicionID++;    }                
            }            
        }//Fin del widget
    );

    function Trazos() {//Trazos
        this.trazos = null;    
        this.deduceTrazado = function (root, idx) {
            this.regresaANormal(); //Limpia trazos antiguos
            var ix = $(root).parent().children().index(root);        
            this.trazos = new Array();//Crea los nuevos trazos        
            switch (idx) {
                case 0: //izquierda - horizontal
                    this.trazos = $(root).prevAll();    break;
                case 1: //derecha horizontal
                    this.trazos = $(root).nextAll();    break;
                case 2: //arriba vertical
                    var $n = this.trazos;
                    $(root).parent().prevAll().each( function() {
                        $n.push($(this).children().get(ix));
                    });    break;
                case 3: //abajo vertical
                    var $o = this.trazos;
                    $(root).parent().nextAll().each( function() {
                        $o.push($(this).children().get(ix));
                    }); break;
                case 4: //diagonal derecha arriba
                    var $p = this.trazos;                
                    var currix = ix;
                    $(root).parent().prevAll().each( function () {
                        $p.push($(this).children().get(++currix));
                    }); break;
                case 5: //derecha diagonal arriba
                    var $q = this.trazos;                
                    var currixq = ix;
                    $(root).parent().prevAll().each( function () {
                        $q.push($(this).children().get(--currixq));
                    });break;
                case 6 : //izquierda diagonal abajo
                    var $r = this.trazos;                
                    var currixr = ix;
                    $(root).parent().nextAll().each( function () {
                        $r.push($(this).children().get(++currixr));
                    });break;
                case 7: //derecha diagonal abajo
                    var $s = this.trazos;                
                    var currixs = ix;
                    $(root).parent().nextAll().each( function () {
                        $s.push($(this).children().get(--currixs));
                    });break;
            }
            for (var x=1;x<this.trazos.length;x++) {
                Visualizador.arm(this.trazos[x]);
            }
        };
        this.glowTo = function (upto) {//Encender las celdas de arriba 
            var to = $(this.trazos).index(upto);            
            for (var x=1;x<this.trazos.length;x++) {            
                if (x<=to) {
                    Visualizador.glow(this.trazos[x]);
                }
                else {
                    Visualizador.arm(this.trazos[x]);                                   
                }
            }            
        }	
        this.regresaANormal = function () {//Limpia los trazos
            if (!this.trazos) return;
            for (var t=1;t<this.trazos.length;t++) { //No limpia zona principal
                Visualizador.restaura(this.trazos[t]);                                             
            }     
        }        
        this.limpiar = function() {
            $(this.trazos).each(function () {
               Visualizador.limpiar(this); 
            });
        }
    }

    function areaPrincipal() {    
        this.elems = null;            
        this.creaZona = function (root) {//Selecciona las zonas cercanas
        this.elems = new Array();         
        var $tgt = $(root);
        var ix = $tgt.parent().children().index($tgt);
        var above = $tgt.parent().prev().children().get(ix);
        var below = $tgt.parent().next().children().get(ix);        
        this.elems.push($tgt.prev()[0],$tgt.next()[0]);//horizontal
        this.elems.push( above, below, 
            $(above).next()[0],$(above).prev()[0], //diagonal
            $(below).next()[0],$(below).prev()[0] //diagonal
        );
        $(this.elems).each( function () {
            if ($(this)!=null) {
                Visualizador.arm(this);
            }
        });       
    }    

    this.index = function (elm) {//Dar a la zona principal algo de inteligencia
        return $(this.elems).index(elm);
    }

    this.seteaResto = function (eligeUno) {
        for (var x=0;x<this.elems.length;x++) {
            Visualizador.arm(this.elems[x]);            
        }
        if (eligeUno != -1) {            
            Visualizador.glow(this.elems[eligeUno]);
        }
    }

    this.regresaANormal = function () {        
        for (var t=0;t<this.elems.length;t++) {            
            Visualizador.restaura(this.elems[t]);            
        }        
    }

    this.limpiar = function() {
            $(this.elems).each(function () {
               Visualizador.limpiar(this); 
            });
        }    
    }

    function Principal() {//Primera celda clickeada
        this.root = null;    
        this.seteaPrincipal = function (root) {
            this.root = root;                        
            Visualizador.glow(this.root);
        }    
        this.regresaANormal = function () {
            Visualizador.restaura(this.root);
        }
        this.esMismaCelda = function (t) {
            return $(this.root).is($(t));
        }    
        this.limpiar = function () {
            Visualizador.limpiar(this.root);
        }
    }

    var Visualizador = {//Objeto para manipular la celda desplegada basada en métodos llamados
        glow : function (c) {
            $(c).removeClass("trazado").removeClass("palabraSeleccionada").addClass("rf-glowing").removeClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]).addClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);            
        },    
        arm : function (c) {          
            $(c).removeClass("rf-glowing").addClass("trazado").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);            
        },   
        restaura : function (c) {            
            $(c).removeClass("trazado").removeClass("rf-glowing").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);
            if ( c!=null && $.data(c,"selected") == "true" ) {
                $(c).addClass("palabraSeleccionada");                
            }            
        },    
        select : function (c) {           
            $(c).removeClass("trazado").removeClass("rf-glowing").removeClass("iluminado").addClass("palabraSeleccionada").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]).removeClass("iluminado"+coloresHexadecimalesIDs[posicionID]).addClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]);            
        },
        resetea : function (c){
           $(c).addClass("palabraSeleccionada").removeClass("iluminado").addClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]).removeClass("iluminado"+coloresHexadecimalesIDs[posicionID]);                       
       },
        iluminar : function (c) {            
            //$(c).removeClass("trazado").removeClass("palabraSeleccionada").addClass("iluminado").removeClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]).addClass("iluminado"+coloresHexadecimalesIDs[posicionID]);            
            $(c).removeClass("trazado").removeClass("palabraSeleccionada").addClass("iluminado");            
        },
        marcaPalabraEncontrada : function (w,idx) {//Marcar con linea atravezada y cambio de color la parabra encontrada
            $(w).css("background",'transparent');
            $(w).addClass('palabraEncontrada');
            $('#lista'+idx).addClass('palabraEncontrada');
            band = true;
            //clickCorrecto();
            sndCorrecto();            
        },
        limpiar : function (c) {            
            $(c).removeClass("trazado").removeClass("rf-glowing").removeClass("palabraSeleccionada").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]).removeClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]);
            $.removeData($(c),"selected");            
        }
    }

    //Objetos que representan celdas individuaes del grid
    function Cell() {
        this.DEFAULT = "-";
        this.isHighlighted = false;
        this.value = this.DEFAULT;
        this.parentGrid = null;
        this.isUnwritten = function () {
            return (this.value == this.DEFAULT);
        };
        this.isSelected = false;
        this.isSelecting = true;
        this.td = null;   
    }

    function Grid() {
        this.cells = null;
        this.directions = [
            "LeftDiagonal",
            "Horizontal",
            "RightDiagonal",
            "Vertical"
        ];    
        this.inicializaGrid= function(size) {            
            this.cells = new Array(size);    
            for (var i=0;i<size;i++) {
                this.cells[i] = new Array(size);                
                for (var j=0;j<size;j++) {
                    var c = new Cell();                                           
                    c.parentgrid = this;
                    this.cells[i][j] = c;                    
                }
            }

        }        
        this.getCell = function(row,col) {           
            return this.cells[row][col];
        }    
        this.createHotZone = function(uic) {
            var $tgt = uic;
            var hzCells = new Array(); 
            var ix = $tgt.parent().children().index($tgt);
        }    
        this.size = function() {
            return this.cells.length;
        }
        this.put = function(row, col, palabra_str) {//Coloca palabra en el grid en la ubicación sugerida 
            var populator = eval("new "+ eval("this.directions["+Math.floor(Math.random()*4)+"]") +"Populator(row,col,palabra_str, this)");
            var esColocado= populator.populate();                
            if (!esColocado) {//Sino consigue ubicarla forza si es posible
                for (var x=0;x<this.directions.length;x++) {
                    var populator2 = eval("new "+ eval("this.directions["+x+"]") +"Populator(row,col,palabra_str, this)");
                    var esColocado2= populator2.populate();
                    esColocado=esColocado2;
                    if (esColocado){
                        break;
                    }               
                }
            }
            colocateAllWords.push(esColocado);
//            var populator = eval("new "+ eval("this.directions["+Math.floor(Math.random()*4)+"]") +"Populator(row,col,palabra_str, this)");
        }
        
        this.llenarGrid = function() {           
            for (var i=0;i<this.size();i++) {
                for (var j=0;j<this.size();j++) {                
                    if (this.cells[i][j].isUnwritten()) {
                        this.cells[i][j].value = String.fromCharCode(Math.floor(65+Math.random()*26));//Llena de letras aleatorias la sopa (letras de relleno)                        
                    }
                }
            }        
        }
    }//Fin de la función Grid
    function HorizontalPopulator(row, col, palabra_str, grid) {//Crea los trazos horizontales    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {                        
            if (this.willWordFit()) {//Trata de ubicar palabra en la fila, checa si la fila contigua está bloqueda
                this.writeWord();
            }
            else {            
                for (var i=0;i<this.size;i++) {//Recorrre todas las filas empezando en la actual
                    var xRow = (this.row+i)%this.size;                
                    var startingPoint = this.findContigousSpace(xRow, palabra_str);//Trata de iniciar con algun trazo
                    if (startingPoint == -1) {//Si no trata de ver si podemos cruza la palabra solo si existe
                        var overlapPoint = this.isWordOverlapPossible(xRow, palabra_str);
                        if (overlapPoint == -1) {//Sino se repite el proceso                        
                            continue;
                        }
                        else {
                            this.row = xRow;
                            this.col = overlapPoint;
                            this.writeWord();
                            break;
                        }
                    }
                    else {
                        this.row = xRow;
                        this.col = startingPoint;
                        this.writeWord();
                        break;
                    }
                }
            }
            // Si no lo logra regresa false, sino logra posicionarlo necesitaremos intentarlo en otra dirección
            return (palabra_str.esColocado);                    
        }

        //Escribe una palabra en el grid dando una ubicación, recuerda cada celda usada para desplegar la palabra
        this.writeWord = function () {
            var chars = palabra_str.chars;        
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i].concat("-");
                this.cells[this.row][this.col+i] = c;                
                palabra_str.containedIn(c);
                palabra_str.esColocado = true;
            }
        }

        //Checa si la palabra puede ser cambiada o intercambiar celdas con el mismo contenido
        this.isWordOverlapPossible = function (row, palabra_str) {
            return -1;
        }

        //Checa si la palabra se encontrará en la misma locación
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            for (var i=col;i<this.size;i++) {
                if (this.cells[row][i].isUnwritten() || this.cells[row][i] == chars[i] ) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
            }
            return isFree;
        }

        //Checa si hay un espacio contiguo en algún lugar de la línea
        this.findContigousSpace = function (row, palabra_str) {
            var freeLocation = -1;
            var freeCounter=0;
            var chars = palabra_str.chars;
            for (var i=0;i<this.size;i++) {
                if (this.cells[row][i].isUnwritten() || this.cells[row][i] == chars[i]) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = (i - (palabra_str.size-1));
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
            }
            return freeLocation;
        }
    }//HorizontalPopulator

    //Crea los trazos verticales
    function VerticalPopulator(row, col, palabra_str, grid) {    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {           
            if (this.willWordFit()) {
                this.writeWord();
            }
            else {           
                for (var i=0;i<this.size;i++) {
                    var xCol = (this.col+i)%this.size; // loop through all rows starting at current;                
                    var startingPoint = this.findContigousSpace(xCol, palabra_str);
                    if (startingPoint == -1) {                    
                        var overlapPoint = this.isWordOverlapPossible(xCol, palabra_str);
                        if (overlapPoint == -1) {                        
                            continue;
                        }
                        else {
                            this.row = overlapPoint;
                            this.col = xCol;
                            this.writeWord();
                            break;
                        }
                    }
                    else {
                        this.row = startingPoint;
                        this.col = xCol;
                        this.writeWord();
                        break;
                    }
                }
            }        
            return (palabra_str.esColocado);                    
        }       
        this.writeWord = function () {
            var chars = palabra_str.chars;
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i].concat("-");
                this.cells[this.row+i][this.col] = c;
                palabra_str.containedIn(c);
                palabra_str.esColocado = true;
            }        
        }    
        this.isWordOverlapPossible = function (col, palabra_str) {
            return -1;
        }
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            for (var i=row;i<this.size;i++) {
                if (this.cells[i][col].isUnwritten() || chars[i] == this.cells[i][col].value) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
            }
            return isFree;
        }        
        this.findContigousSpace = function (col, palabra_str) {
            var freeLocation = -1;
            var freeCounter=0;
            var chars = palabra_str.chars;
            for (var i=0;i<this.size;i++) {
                if (this.cells[i][col].isUnwritten() || chars[i] == this.cells[i][col].value) { 
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = (i - (palabra_str.size-1));
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
            }
            return freeLocation;        
        }
    }

    //Crea estrategia para diagonal derecha
    function LeftDiagonalPopulator(row, col, palabra_str, grid) {    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {           
            if (this.willWordFit()) {
                this.writeWord();
            }
            else {
                var output = this.findContigousSpace(this.row,this.col, palabra_str);
                if (output[0] != true) {                                
                    OUTER:for (var col=0, row=(this.size-palabra_str.size); row>=0; row--) {
                        for (var j=0;j<2;j++) {
                            var op = this.findContigousSpace( (j==0)?row:col, (j==0)?col:row, palabra_str);
                            if (op[0] == true) {
                                this.row = op[1];
                                this.col = op[2];
                                this.writeWord();
                                break OUTER;
                            }
                        }                    
                    }
               }
                else {
                    this.row = output[1];
                    this.col = output[2];
                    this.writeWord();
                }
            }        
            return (palabra_str.esColocado);                    
        }           
        this.writeWord = function () {
            var chars = palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i].concat("-");
                this.cells[lrow++][lcol++] = c;
                palabra_str.containedIn(c);
                palabra_str.esColocado=true;
            }
        }    
        this.isWordOverlapPossible = function (row, palabra_str) {
            return -1;
        }   
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            var i=0;
            while (lcol < this.grid.size() && lrow < this.grid.size()) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++] ) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
                lrow++;
                lcol++;            
            }
            return isFree;
        }    
        this.findContigousSpace = function (xrow, xcol,palabra_str) {
            var freeLocation = false;
            var freeCounter=0;
            var chars = palabra_str.chars;
            var lrow = xrow;
            var lcol = xcol;

            while (lrow > 0 && lcol > 0) {
                lrow--;
                lcol--;
            }
            var i=0;
            while (true) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++]) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = true;
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
                lcol++;
                lrow++;
                if (lcol >= this.size || lrow >= this.size) {
                    break;
                }
            }
            if (freeLocation) {
                lrow = lrow - palabra_str.size+1;
                lcol = lcol - palabra_str.size+1;
            }
            return [freeLocation,lrow,lcol];        
        }
    }

    function RightDiagonalPopulator(row, col, palabra_str, grid) {    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {           
            var rr=0;
            if (this.willWordFit()) {
                this.writeWord();
            }
            else {
                var output = this.findContigousSpace(this.row,this.col, palabra_str);
                if (output[0] != true) {                                
                    OUTER:for (var col=this.size-1, row=(this.size-palabra_str.size); row>=0; row--) {
                        for (var j=0;j<2;j++) {
                            var op = this.findContigousSpace( (j==0)?row:(this.size-1-col), (j==0)?col:(this.size-1-row), palabra_str);
                            if (op[0] == true) {
                                this.row = op[1];
                                this.col = op[2];
                                this.writeWord();
                                break OUTER;
                            }
                        }                    
                    }
               }
               else {
                    this.row = output[1];
                    this.col = output[2];
                    this.writeWord();
                }
            }        
            return (palabra_str.esColocado);                    
        }        
        this.writeWord = function () {
            var chars = palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i].concat("-");
                this.cells[lrow++][lcol--] = c;
                palabra_str.containedIn(c);
                palabra_str.esColocado = true;
            }
        }    
        this.isWordOverlapPossible = function (row, palabra_str) {
            return -1;
        }
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            var i=0;
            while (lcol >= 0 && lrow < this.grid.size()) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++] ) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
                lrow++;
                lcol--;

            }
            return isFree;
        }    
        this.findContigousSpace = function (xrow, xcol,palabra_str) {
            var freeLocation = false;
            var freeCounter=0;
            var chars = palabra_str.chars;
            var lrow = xrow;
            var lcol = xcol;

            while (lrow > 0 && lcol < this.size-1) {
                lrow--;
                lcol++;
            }
            var i=0;
            while (lcol >= 0 && lrow < this.grid.size()) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++]) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = true;
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
                lrow++;
                lcol--;
            }
            if (freeLocation) {
                lrow = lrow - palabra_str.size+1;
                lcol = lcol + palabra_str.size-1;
            }
            return [freeLocation,lrow,lcol];        
        }
    }

    function Model() {
        this.grid= null;
        this.listaPalabras= null;
        this.init = function(grid,list) {
            this.grid = grid;
            this.listaPalabras = list;  
            for (var i=0;i<this.listaPalabras.size();i++) {
                grid.put( Utilidad.random(this.grid.size()), Utilidad.random(this.grid.size()), this.listaPalabras.get(i) );
            }
        }    
    }
    function Word(val) {
        this.value = val.toUpperCase();
        this.valorOriginal = this.value;
        this.isFound= false;
        this.cellsUsed = new Array();
        this.esColocado = false;
        this.row = -1;
        this.col = -1;
        this.size = -1;
        this.chars = null;

        this.init = function () {
            this.chars = this.value.split("");
            this.size = this.chars.length;
        }
        this.init();

        this.containedIn = function (cell) {                
            this.cellsUsed.push(cell);        
        }		    
        this.checarSiEsSimilar = function (w) {
            if (this.valorOriginal == w || this.value == w) {
                this.isFound = true;
                return true;
            }
            return false;
        }    
    }

    function ListaPalabras() {
        this.palabras = new Array();    
        this.cargaPalabras = function (csvpalabras) {
            var $n = this.palabras;
            $(csvpalabras.split(",")).each(function () {
                $n.push(new Word(this));
            });        
        }    
        this.add = function(palabra_str) {     
            if (Math.random()*10 >5) {
                var s="";
                for (var i=palabra_str.size-1;i>=0;i--) {
                    s = s+ palabra_str.value.charAt(i);
                }
                palabra_str.value = s;            
                palabra_str.init();
            }
            this.palabras[this.palabras.length] = palabra_str;
        }    
        this.size = function() {
            return this.palabras.length;
        }    
        this.get = function(index) {        
            return this.palabras[index];
        }    
        this.esUnaPalabraPresente = function(checarPalabra2) {
            for (var x=0;x<this.palabras.length;x++) {
                if (this.palabras[x].checarSiEsSimilar(checarPalabra2)) return x;
            }
            return -1;
        }
    }

    var Utilidad = {
        random : function(max) {    return Math.floor(Math.random()*max);
        },    
        log : function (msg) {
            $("#logger").append(msg);
        }
    } 

    var ayudanteJuego = {
        preparaGrid : function (palabras) {
            var grid = new Grid();
            grid.inicializaGrid(gridSize);
            var listaPalabras = new ListaPalabras();
            listaPalabras.cargaPalabras(palabras);
            var modelo = new Model();
            modelo.init(grid, listaPalabras);
            grid.llenarGrid();
            return modelo;
        },	
        renderearJuego : function(contenedor, modelo) {            
            var grid = modelo.grid;
            var cells = grid.cells;
            medida = cells.length > 16 ? 17 : cells.length;
            medidaExtendida = medida < 12 ? false : true;
            var contador=0;$
            var estructura = "<div id='contenedorJuego'><table id='tablaGrid' cellspacing=0 cellpadding=0 class='estiloTabla'>";
            var sopaParaGuardar = new Array();
            incremento = 0;
            for (var i=0;i<grid.size();i++) {
                estructura += "<tr>";   
                if(sopaTest!==""){//se reemplaza la sopa de letras generada por una ya existente
                    var currentString=0;
                    
                }
                var currentString="";
                for (var j=0;j<grid.size();j++) {
//                    cells[i][j].value=sopaTest[contador]; contador++;//se reemplaza el modelo de la sopa ed letras por uno ya creado
                    if(jsons_arr[0] === undefined){//Si no es un examen interrumpido se prepara el array para guardarlo en la BDs
                        sopaParaGuardar[incremento] = cells[i][j].value;                        
                    }else{
                        cells[i][j].value = jsons_arr[incremento];//Para que tome los valores (letras)de la base de datos                                      
                    }
                    incremento++;
                    
                        currentString=cells[i][j].value;
                        if(currentString.length>1){//se obtinen la coordenadas de letras que pertenecen a palabra correcta
                            coordenadasCorrecta.push((i+1)+"-"+(j+1));
                            currentString=currentString.slice(0, -1);
                            cadenaSopa=cadenaSopa+currentString+",";
                            cells[i][j].value=currentString;
                        }
                    estructura += "<td coord='"+(i+1)+"-"+(j+1)+"' class='estiloGrid'>"+currentString+"</td>";                                                                               
                }
                estructura += "</tr>";
            }

            cadenaSopa=cadenaSopa.slice(0, -1);
            guardarSopaEnBDs(cadenaSopa);            

            estructura += "</table><div id='openWordSelecetion'></div><div id='closeWordSelecetion'></div></div>";
            $(contenedor).append(estructura);
            //se calculan las dimenciones de las celdas del grid
            var tamañoCeldas=(((($("#contenedorJuego").width() - (cells.length*3)) / cells.length)+"").split(".")[0])*1;
            $("#openWordSelecetion, #closeWordSelecetion").css({width:tamañoCeldas, height:tamañoCeldas});
            $("tr .estiloGrid").css({width:tamañoCeldas+"px", height:tamañoCeldas+"px"});
            var x=0;
            var y=0;
            $("tr","#tablaGrid").each(function () {
                $("td", this).each(function (col){
                    var c = cells[x][y++];
                    $.data(this,"cell",c);
                    c.td = this;
                });
                y=0;
                x++;
            });
            var palabras = "<div id='contenedorPalabra'><ul>"
            var inc = 0;
            var inc2 = 0;
            var valorOriginalGlobal;
            var esColocadoGlobal;            
            
            var cuantasPalabras = modelo.listaPalabras.palabras.length;                        
            
            $(modelo.listaPalabras.palabras).each(function (index) {
                var txtRespuesta = eliminarParrafos(json[idPreguntaGlobal].respuestas[index].t13respuesta);
                if(cuantasPalabras > 13){//Si hay más de 13 palabas en la lista
                    //Reduce el interlineado de la lista de palabras a encontrar.
                    palabras += '<li id="lista'+inc+'" class="colocada'+this.esColocado+' listaLarga'+cuantasPalabras+' quitarPadding" >'+txtRespuesta +'</li>';
                }else{//Si hay 13 palabas en la lista ó menos
                    palabras += '<li id="lista'+inc+'" class=colocada'+this.esColocado+'>'+txtRespuesta +'</li>';
                }
                if(this.esColocado === false){
                    errorAlFormarSopa = true;
                }
                inc++;
            });
            
            if(errorAlFormarSopa){//Si ocurrió un error al formar la sopa y no se incluyeron todas las palabras
                $(".pregunta").html("No se pudo generar la sopa de letras <br>:(");
//                $('#iFrame',window.parent.document).attr('src',$('#iFrame',window.parent.document).attr('src'));//Se vuelve a recargar la sopa                                           
//                funcionesSopa(gridSize, newGridSize);                        
            }else{
                $("#gridSopadeletras").css('visibility', 'visible');
            }
            /*if(reactivosLargos === false){
                $(modelo.listaPalabras.palabras).each(function () {
                    palabras += '<li style="display: inline-block; padding: 0px 30px; font-size: 21px;" id="lista'+inc+'" class=colocada'+this.esColocado+'>'+this.valorOriginal+'</li>';
                    inc++;
                });
            }else{                
                valorOriginalGlobal = $(modelo.listaPalabras.palabras)[0].valorOriginal;
                esColocadoGlobal = $(modelo.listaPalabras.palabras)[1].esColocado;
                console.log('valorOriginalGlobal: '+valorOriginalGlobal );
                palabras += '<li style="display: inline-block; padding: 0px 30px; font-size: 21px;" id="lista'+inc+'" class=colocada'+esColocadoGlobal+'>'+valorOriginalGlobal+'</li>';
            }*/
            
            /*$(modelo.listaPalabras.palabras).each(function () {
                //preguntasLargas
                //this.valorOriginal
                palabras += '<li style="display: inline-block; padding: 0px 30px; font-size: 21px;" id="lista'+inc+'" class=colocada'+this.esColocado+'>'+preguntasLargas[inc]+'</li>';
                palabrasResueltas[inc] = 0;
                inc++;
            });*/
                
            palabras += "</ul></div>";
            $(contenedor).prepend(palabras); //Código para mostrar el menú de palabras
     
            $( "#contenedorPalabra" ).insertBefore( "#contenedorJuego" );
            $( "#respuestasRadialGroup" ).append( "<div id='palabrasRespondidas'></div>" );
            $("#palabrasRespondidas").css({top:($("#gridSopadeletras").height()+20), height:"calc(100% - 110px)", overflowY:"auto", overflowX:"hidden"});
            $("#respuestasRadialGroup").css({height:"85%"});
            if(medidaExtendida){                
                $( '#contenedorPalabra' ).css('width', '25%');//Reducir el ancho de la caja de la lista de palabras para que quepa en el espacio de 920
                $( 'tr td, table tr td' ).css('line-height', '11px');
            }
                        
            $(modelo.listaPalabras.palabras).each(function () {
                
                if( typeof obj.pregunta.t11ayudas === 'undefined' || obj.pregunta.t11ayudas === 'no' ){
                    //No oculta las ayudas
                }else{
                    $('#lista'+inc2).css('display','none');
                }
                inc2++;
            });
            
            $('#preguntaTexto').css({height:"0px", margin:"0px"});
            if(jsons_arr[0] === undefined){
                terminaPintadoInterrumpido = true;
            }else{
                var palabraContestada;
                var cadena;
                for(var idx=0;idx<palabrasContestadas_arr.length;idx++){
                    palabraContestada = modelo.listaPalabras.get(idx);
                    cadena = letrasContestadas_arr[idx];
                    var celdasExtraidas = cadena.split("-");

                    for(var f=0;f<celdasExtraidas.length;f++){
                        Visualizador.iluminar($("#tablaGrid").find("td").eq(celdasExtraidas[f]));                                                                            
                        $("#tablaGrid").find("td").eq(celdasExtraidas[f]).addClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);
                        //Visualizador.resetea($("#tablaGrid").find("td").eq(celdasExtraidas[f]));//Importante: NO APLICA sino se borra lo seleccionado                                                                      
                    }
                    posicionID++;                    
                    ayudanteJuego.preparaMarca(palabrasContestadas_arr[idx]);
                }
                terminaPintadoInterrumpido = true;
            }             
//            responderSopa();           //CI5A_S01_A04
        },
        preparaMarca : function(idx) {
            var w = $("li").get(idx);
            Visualizador.marcaPalabraEncontrada(w,idx);//Marca la palabra encontrada en la lista de palabras
            if(terminaPintadoInterrumpido){
                guardaPalabrasContestadas(idx);
            }    
        }
    }
    })(jQuery);
}

function pintaPalabraSeleccionada(){
    $(".onSelection").addClass("wordSelected");
    $("#palabrasRespondidas").prepend("<p class='answer' style='opacity: 0; height: 0px;' wordCoordinates='"+coordenadas+""+"'>&nbsp&nbsp"+palabraFormada+"</p>");
    setTimeout(function(){$("#palabrasRespondidas p").first().css({opacity:"1", height:"40px"});}, 10);
            var coordinates;
        $("#palabrasRespondidas p").before().on("click", function(){
            $(".answer").addClass("answerInDelete").removeClass("answer");
            coordinates=$(this).attr("wordCoordinates");
            $(this).addClass("toRemove");
            limpiarRespuesta(coordinates, $(this).index(), ($(this).text()).replace(/\s/g, ''));
            setTimeout(function(){
                $(".answerInDelete").addClass("answer").removeClass("answerInDelete");
                $(".toRemove").remove();
            }, 500);
        });
}

function limpiarRespuesta(coordinates, indexRemove, word){
    var palabras = [];
    $.each(listaPalabras, function (index, element) {
        if (word !== element) {
            palabras.push(element);
        }
    });
    listaPalabras=palabras;
    var listaResueltos="";
    coordinates = coordinates.split("_");
    $.each($("p[wordCoordinates]"), function(){
        listaResueltos += $(this).attr("wordCoordinates")+"_";
    });
    listaResueltos=listaResueltos.slice(0, -1).split("_");

    $.each(coordinates, function(index, element){
        var contadorRepetios=0;
        listaResueltos.find(function(ce){
            contadorRepetios= ce===element ? contadorRepetios+1 : contadorRepetios;
        });
        if(contadorRepetios<2){
            $("td[coord='"+element+"']").removeClass("wordSelected");
        }
    });
}


//function responderSopa(){
//    var filaLetra; var currentCoord=""; var palabra="";
//    $.each(CadenaTest, function(index, element){
//        if(element!==";"){
//            currentCoord=currentCoord+element+"_";
//            filaLetra=element.split("-")[0];
//            palabra=palabra+$("#tablaGrid tr:nth-child("+filaLetra+")").find("td[coord='"+element+"']").text();
//            $("#tablaGrid tr:nth-child("+filaLetra+")").find("td[coord='"+element+"']").addClass("wordSelected");
//            $("#tablaGrid tr:nth-child("+filaLetra+")").find("td[coord='"+element+"']").css({background:"#dee594", boxShadow: "inset 0 2px 6px rgba(0,0,0,0.3)",transition:"box-shadow 0.35S, background 0.5s"});
//        }else{
//            currentCoord=currentCoord.slice(0, -1);
//            $("#palabrasRespondidas").append("<p class='answer' style='opacity: 0;' wordCoordinates='"+currentCoord+"'>&nbsp&nbsp"+palabra+"</p>");
//            setTimeout(function(){$("#palabrasRespondidas p").css({opacity:"1"});}, 10);            
//            currentCoord=""; palabra="";
//                $("#palabrasRespondidas p").before().on("click", function(){
//                    cadenaCoord=$(this).attr("wordCoordinates");
//                    $(this).css({opacity:"0"});
//                    $(this).addClass("toRemove");
//                    limpiarRespuesta(cadenaCoord);
//                    setTimeout(function(){
//                        $(".toRemove").remove();
//                    }, 500);
//                });            
//        }
//    });
//}


var firstPosition=[], lastPosition=[], palabraFormada="", coordenadas="";
function calcularTrayectoria(){
    var x1=firstPosition[0], x2=lastPosition[0], y1=firstPosition[1], y2=lastPosition[1];
    var despX = x2 < x1-1 || x2 > x1+1 ? true : false;
    var despY = y2 < y1-1 || y2 > y1+1 ? true : false;
    if(despX && despY){//trayectoria diagonal
        var i, j, final, coords=[];
        var dx = posIn[0] < posEnd[0] ? posEnd[0] - posIn[0] :  posIn[0] - posEnd[0];//delta x
        var dy = posIn[1] < posEnd[1] ? posEnd[1] - posIn[1] :  posIn[1] - posEnd[1];//delta y
        i = dx > dy ? x1 : y1;//se define la diagonal para tomar a x o y como base para trazar la linea
        j = dx > dy ? y1 : x1;
        final = dx > dy ? x2 : y2;
        while(i !== final){//se itera para obtener todas las coordenadas de la linea trazada
            if(dx > dy){
                coords.push(j+"-"+i);
                j = y1 < y2 ? j+1 :  j-1;
                i = x1 < x2 ? i+1 :  i-1;
            }else{
                coords.push(i+"-"+j);
                j = x1 < x2 ? j+1 :  j-1;
                i = y1 < y2 ? i+1 :  i-1;
            }
        }
        dx > dy ? coords.push(j+"-"+i) : coords.push(i+"-"+j);
        pintaTrayectoria(coords);
        
    }else if(despY && !despX){//trayectoria recta en y
        var i=y1, coords=[];
        while(i!==y2){
            coords.push(i+"-"+x1);
            i = y1 < y2 ? i+1 :  i-1;
        }
        coords.push(i+"-"+x1);
        pintaTrayectoria(coords);
        
    }else if(despX && !despY){//trayectoria recta en X
        var i=x1, coords=[];
        while(i!==x2){
            coords.push(y1+"-"+i);
            i = x1 < x2 ? i+1 :  i-1;
        }
        coords.push(y1+"-"+i);
        pintaTrayectoria(coords);
    }else{
        var coords=[];
        coords.push(y1+"-"+x1);
        coords.push(y2+"-"+x2);
        pintaTrayectoria(coords);
    }
    
    function pintaTrayectoria(coords){
        palabraFormada=""; coordenadas="";
        $(".onSelection").removeClass("onSelection");
        $.each(coords, function(index, element){
            $("td[coord='"+element+"']").addClass("onSelection");
            palabraFormada += $("td[coord='"+element+"']").text();
            coordenadas += element+"_";
        });
        coordenadas=coordenadas.slice(0, -1);
    }
}