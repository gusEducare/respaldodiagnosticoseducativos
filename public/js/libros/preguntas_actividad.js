arrRespuestasLineas = new Object();
obj = new Object();
objRespuestasUsuario = new Object();
objPreguntas = new Object();
aciertos = 0;
cartuchoquemado = new Array();
esUltima = false;
tipoMensaje = 'normal';//animacion     normal
ocultarAnimacion = true;
duracionAnimacion = 9000;
numCirculo = 1;
opcionA=0, opcionB=0;
var esCorrectaGlobal;
var numeroInciso = 0;
var numeroDeLineasVerdes = 0;
var canvas, stage, exportRoot, fps, animacion;
arrPalabrasSeleccionadas=[];
var grupoCadenas={cadenas:[], estatus:false}; 
// JSON para pintar la retroalimentacion de los test


var contenedorRespuestaGlobal=0;
if(ocultarAnimacion){
    duracionAnimacion = 500;//Sino hay animación se quita el tiempo de espera para la siguiente pregunta
}

$(document).ready(function(){
    console.log('text');
   idPreguntaGlobal = 0;
    $('.btn_cancelar').on('click', function(){
        $('a.close-reveal-modal').trigger('click');
    });
    $('.btn_evaluar').on('click', function( event, params){
        var _objRespuestas;
        var _strBoton = 'fin';
        var idPregunta = idPreguntaGlobal;
        esUltima = true;
        if( typeof params !== 'undefined'){
            var indiceContestado = parseInt(params.idContestado - 1);   
            if(c03id_tipo_pregunta === "8"){//Si es arrastra cortas con textos largos
                var cartaValor=carta.split("texto");
                contenedorRespuesta=parseInt(cartaValor[1])-1;
                contenedorRespuestaGlobal=contenedorRespuesta;                                                                      
            }
            guardaRespuesta(_objRespuestas, _strBoton, idPregunta, indiceContestado);
        }else{            
            guardaRespuesta(_objRespuestas, _strBoton, idPregunta);
        }
        console.log('finalizar');
        finalizarExamen(_objRespuestas);
    });
    
    if (catalogoActividades === true) {
        if (typeof json === 'undefined' && idActividad!=="null") {
            console.log('El id: ' + idActividad + ' no existe, favor de verificarlo.');
        }
    } else {
        if (typeof json === 'undefined') {
            console.log('El id: ' + idActividad + ' no existe, favor de verificarlo.');
        }
    }
    obj = json[idPreguntaGlobal];
    obj.preguntasTotal = json;
        
        //Seteo de variables default        
        if(obj.pregunta.c03id_tipo_pregunta === '5'){
            if( obj.pregunta['tipo'] === undefined && obj.contenedoresFilas === undefined){
                obj.pregunta['tipo'] = 'vertical';
            }
        }
        if(obj.pregunta.c03id_tipo_pregunta === '3'){
            if( typeof obj.pregunta['forma'] === 'undefined'){
                obj.pregunta['forma'] = 'circulo';
            }
            if( typeof obj.pregunta['texto'] === 'undefined'){
                obj.pregunta['texto'] = true;
            }
            if( typeof obj.pregunta['icono'] === 'undefined'){
                obj.pregunta['icono'] = true;
            }
        }
    var cadenaOrden = $('#orden').attr('value');
//    obj.respuestas = shuffleArrayController(obj.respuestas, cadenaOrden);
    $("#primera").val(true);
    var numPreguntas = obj.preguntasTotal.length;
    if(numPreguntas === 1){
        $('#ultima').val(true);
    }
    $('#total').val( numPreguntas  );
    $('.num_preg a').html('1 / '+$('#total').val());
    for(var i=0; i<numPreguntas; i++){
        var eliminaUltimoElemento = false;
        var idTipoPregunta =  obj.preguntasTotal[i].pregunta['c03id_tipo_pregunta'];    
        var respFinales;        
        if( typeof obj.pregunta['contieneDistractores'] === 'undefined'){
            respFinales =  obj.preguntasTotal[i].respuestas;
        }else{
            if(obj.pregunta['contieneDistractores']){
                respFinales = obj.preguntas;
            }else{
                respFinales =  obj.preguntasTotal[i].respuestas;
            }
        }
        if(obj.preguntasTotal[i].pregunta['c03id_tipo_pregunta'] === "13"){
            respFinales = obj.preguntasTotal[i].preguntas;
        }
        if( typeof obj.pregunta['pintaUltimaCaja'] === 'undefined'){
            //respFinales = obj.preguntasTotal[i].respuestas;
        }else{
            if(!obj.pregunta['pintaUltimaCaja']){            
                eliminaUltimoElemento = true;
            }
        }
        var numRespuestas =  cuentaRespuestasCorrectas( idTipoPregunta, respFinales, eliminaUltimoElemento, i);
        cartuchoquemado[i] = llenaCartuchos( idTipoPregunta, numRespuestas);
    }
    obtenerValoresJSON(obj);
    $( ".navega-pregunta" ).on('click',function( event, params ) {
        var _objRespuestas;
        
        var _strBoton ;           
        var idPregunta = idPreguntaGlobal; // Solo se envía si el boton es irA
        var respuestaUsuario = obtenerRespuestaUsuario();
        if( $(this).hasClass('anterior') ){
            _strBoton = 'anterior';
        }else{
            if( $(this).hasClass('siguiente') ){
              _strBoton  = 'siguiente';
              idPreguntaGlobal++;
          }else{
              _strBoton  = 'irA';
              idPregunta = parseInt( $(this).text());
              idPreguntaGlobal--;
          }
        }
        //arrRespuestasLineas = new Object();
        actualizaRespuesta(respuestaUsuario, _strBoton, idPregunta)//Se comenta para prubeas de diseño, realiza la peticion ajax
        if( typeof params !== 'undefined'){
            var indiceContestado = parseInt(params.idContestado - 1);
            if(c03id_tipo_pregunta === "8"){//Si es arrastra cortas con textos largos
                var cartaValor=carta.split("texto");
                contenedorRespuesta=parseInt(cartaValor[1])-1;
                contenedorRespuestaGlobal=contenedorRespuesta;                                                                      
            }
            guardaRespuesta(_objRespuestas, _strBoton, idPregunta, indiceContestado);
        }else{
            guardaRespuesta(_objRespuestas, _strBoton, idPregunta);
        }
        
        recargarImagenes();
        
    });


    function guardaRespuesta(_objRespuestas, _strBoton, idPregunta, idContestado){


        var contPregunta;
        if (_strBoton !== "irA") {
            if (_strBoton === "siguiente") {
                idPregunta++;
            } else {
                if (_strBoton === "anterior") {
                    idPregunta--;
                }
            }
            contPregunta = idPregunta + 1;
        } else {
            contPregunta = idPregunta;
            idPregunta--;
        }
//        $('#jsonPregunta').val(JSON.stringify(obj.preguntasTotal[idPregunta]));
        $('.num_preg a').html(contPregunta + ' / ' + $('#total').val());
        if (contPregunta === 1) {
            $('#primera').val(1);
        } else {
            $('#primera').val(null);
            if (contPregunta === parseInt($('#total').val())) {
                $('#ultima').val(1);
            } else {
                $('#ultima').val(null);
            }

        }
        console.log(obtenerRespuestaUsuario());
        $("#respuestasRadialGroup").css({backgroundImage:"none"});
        muestraSiguientePregunta(json[idPreguntaGlobal], idPregunta);
        if(c03id_tipo_pregunta === '8'){
            
            revierteCorta();
        }
    }
    
    function desapareceElementoArrastraCorta(idContestado){        
        if(c03id_tipo_pregunta === "8"){  
            if( typeof obj.pregunta.textosLargos === 'undefined' || obj.pregunta.textosLargos === 'no' ){
                //No hace nada
            }else{//Si si son textos largos ejecuta el barrido
                var objCajas = $('#texto div').not('.elementosOcultos').find('span');
                var todasCajasContestadas = seContestaronTodas(objCajas);
                if(todasCajasContestadas){
                    setTimeout(function(){//Delay para esperar a que se muestre la respuesta correcta antes de cambiar a la siguiente                        
                        $('#card'+idContestado).animate( {    opacity: .1     }, tiempoAnimacionBarrido, function() {//1500 Tiempo de la animación   
                             //fin de la animación                      
                        });                                    
                        $('#br'+numeroInciso).animate( {
                            opacity: .1
                        }, tiempoAnimacionBarrido, function() {//1500 Tiempo de la animación                                                                                           
                            $('#br'+numeroInciso).css('display', 'none');
                            $('#br'+(numeroInciso+1)).css('display', 'initial');
                            $('#'+cardNumberGlobal).css('display', 'none');  
                            numeroInciso++;
                            aplicaCambiosImagen();
//                            circuloSiguiente();
                        });
                    }, 1000);//2 segundos
                }
            }      
        }
    }
    
    
    function accionesCorrecta( _strBoton, idPregunta ){          
            if (_strBoton !== "irA") {
                if (_strBoton === "siguiente") {
                    idPregunta++;
                } else {
                    if (_strBoton === "anterior") {
                        idPregunta--;
                    }
                }
                contPregunta = idPregunta + 1;
            } else {
                contPregunta = idPregunta;
                idPregunta--;
            }
            $('#jsonPregunta').val(JSON.stringify(obj.preguntasTotal[idPregunta]));
            $('.num_preg a').html(contPregunta + ' / ' + $('#total').val());
            if (contPregunta === 1) {
                $('#primera').val(1);
            } else {
                $('#primera').val(null);
                if (contPregunta === parseInt($('#total').val())) {
                    $('#ultima').val(1);
                } else {
                    $('#ultima').val(null);
                }
            }
            if(idTipoPregunta === "13"){
                console.log('cambia siguiente pregunta ');
                //$('input:radio').prop('checked',false);
                $("input[type=radio]").attr('disabled', true);
            }
            if (idTipoPregunta === "12" || idTipoPregunta === "1") {//Si es Relaciona Líneas             
                myInterval = setInterval(function () {
                    if (idTipoPregunta === "12") {//Si es Relaciona Líneas                   
                        $('#respuestasRadialGroup').animate({
                            left: "-=1000px"
                        }, tiempoAnimacionBarrido, function () {//1500 Tiempo de la animación   
                            muestraSiguientePregunta($('#jsonPregunta').val(), idPregunta);
//                            circuloSiguiente();
                        });
                    } else {
                        if (testBandera === true && esUltima === true) {
                            //Es una pregunta de tipo Test y es la última por lo que ya no muestra la siguiente
                        } else {
                            muestraSiguientePregunta($('#jsonPregunta').val(), idPregunta);
                        }
//                        circuloSiguiente();
                    }
                    clearInterval(myInterval);//Elimina el timer   
                }, tiempoEsperaReactivo);//Tiempo de espera genérico para todas las preguntas

            } else {
                /*
                if (strAmbiente === 'DEV') {
                    $('img').not('#trofeo_animacion').not('#flecha').each(function () {
                        $(this).attr('src', '../../../../' + strRutaLibro + $(this).attr('src'))
                    });
                }
                */
                console.log('acciones Correcta');
                cambiaSiguientePregunta($('#jsonPregunta').val(), idPregunta);
            }            
    }
    
    
    function finalizarExamen(_objRespuestas){    
        $.ajax({
            type:'POST',
            dataType:'json',            
            url:  $('#urlFinalizaExamen').val(),
            data:{ 
                'idPreguntaActual' : $('#idPregunta').val(),
                'objRespuestas'     : _objRespuestas
                },
            beforeSend: function() {
            },
            success:function(json){
                    if( json ){
                        $('#ir-examenes').foundation('reveal', 'open');
                    }else{
                    }
            },
            error: function(json){
                 console.log("Hubo un error al procesar los datos: " + json);
            }
        });
    }
    
    function marcarDesmarcarPregunta(resp, id){
        if(resp === 'Marcada'){
            $('.marca-preg').addClass('fa-flag').removeClass('fa-flag-o');
            $('#' + $('#idPregunta').val() + ' span').removeClass('desactiva');
        }else{
            $('.marca-preg').removeClass('fa-flag').addClass('fa-flag-o');
            $('#' + $('#idPregunta').val() + ' span').addClass('desactiva');
        }
    }
    
    
    function muestraSiguientePregunta(obj, idPregunta){
        //console.log('muestra siguiente pregunta');

        idPreguntaGlobal = idPregunta;        
        obj = json[idPreguntaGlobal];
        var cadenaOrden = $('#orden').attr('value');
//        obj.respuestas = shuffleArrayController(obj.respuestas, cadenaOrden);

        botonesSeleccionados_arr = new Array();
         if(json[idPreguntaGlobal].pregunta){
            if(typeof  objRespuestasUsuario[idPregunta] !== 'undefined' ){
                var arrRespuestaUsuario = formateaRespuestaUsuario(objRespuestasUsuario[idPregunta], json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta);
                json[idPreguntaGlobal].pregunta.usuario = arrRespuestaUsuario;
            }
             obtenerValoresJSON(json[idPreguntaGlobal]);
             cambiarRutasImagenes();
             if(c03id_tipo_pregunta==="8"){
                if (json[idPreguntaGlobal].pregunta.respuestasLargas === true) {
                    var totalImgs = $('#respuestasRadialGroup img').size(), cont = 0;
                    if( totalImgs ){
                        $('#respuestasRadialGroup img').load(function (e) {
                            console.log('Se cargó la imagen ' + $(this).attr('src'));
                            cont++;
                            if (totalImgs === cont) {
                                console.log('Se cargaron todas las imágenes');
                                ajustarAnchoCajas();
                            }
                        }).error(function () {
                            console.log('Error al cargar la imagen');
                        });                
                    }else{
                        ajustarAnchoCajas();
                    }
                }
            }
             $('#respuestasRadialGroup').css('opacity', '1');
             $('#respuestasRadialGroup').prev().css('opacity', '1');
//             circuloSiguiente();
         }else{
             console.log('No hay pregunta anterior');
         }        
    }
    
    function formateaRespuestaUsuario(objRespUsr, idTipoPregunta){        
        var arrResultado = new Array();    
        var j=0;    
        if( idTipoPregunta === '1' || idTipoPregunta === '2' || idTipoPregunta === '3' ){ // Tipos de pregunta
            var arrResp = $.map(objRespUsr.resp, function(value, index) {
                return [value];
            });
            var numResp = arrResp.length;
            for(var i = 0, k = 1; i < numResp; i++, k++ ){
                if(arrResp[i].selected){
                    arrResultado[j] = k;
                    j++;
                }
            }
        }else if(idTipoPregunta === '12'){
            var arrResp = $.map(objRespUsr, function(value, index) {
                return [value];
            });            
            var numResp = arrResp.length;            
            for(var i = 0; i < numResp; i++){
                if(arrResp[i].id){
                    arrResultado[i] = arrResp[i].selected;
                    //j++;
                }else{
                    arrResultado[i] = '';
                }
            }
        }else if(idTipoPregunta === '8'){
            var arrResp = $.map(objRespUsr.resp, function(value, index) {
                return [value];
            });            
            var numResp = arrResp.length;            
            for(var i = 0; i < numResp; i++){
                if(arrResp[i].id){
                    arrResultado[i] = arrResp[i].selected;
                    //j++;
                }else{
                    arrResultado[i] = '';
                }
            }
        }
        
        return arrResultado;
    }
    
    

   
    // Funciones para el drop
    function DropDown(el) {
            this.dd = el;
            this.opts = this.dd.find('ul.dropdown > li');
            this.val = [];
            this.index = [];
            this.initEvents();
    }
    DropDown.prototype = {
            initEvents : function() {
                    var obj = this;

                    obj.dd.on('click', function(event){
                            $(this).toggleClass('active');
                            event.stopPropagation();
                    });

                    obj.opts.children('label').on('click',function(event){
                            var opt = $(this).parent(),
                                    chbox = opt.children('input'),
                                    val = chbox.val(),
                                    idx = opt.index();

                            ($.inArray(val, obj.val) !== -1) ? obj.val.splice( $.inArray(val, obj.val), 1 ) : obj.val.push( val );
                            ($.inArray(idx, obj.index) !== -1) ? obj.index.splice( $.inArray(idx, obj.index), 1 ) : obj.index.push( idx );
                    });
            },
            getValue : function() {
                    return this.val;
            },
            getIndex : function() {
                    return this.index;
            }
    };
    $(function() {

            var dd = new DropDown( $('#dd') );

            $(document).click(function() {
                    // all dropdowns
                    $('.wrapper-dropdown-4').removeClass('active');
            });
    });
  
    
    function cambiaSiguientePregunta(_strBoton, idPregunta) {
        //muestraSiguientePregunta(_strBoton, idPregunta);
        $("#capaSuperior").css('display', 'block');
        if (obj.length >= 2) {//Solo entra con animación el ejercicio 1
            //if(idPreguntaGlobal > 0){                    
            //console.log('antes del timeout');
            setTimeout(function () {
                //$("#capaSuperior").css({display:'block', zIndex:'101'});
                $('#respuestasRadialGroup').css('opacity', '1');
                $('#respuestasRadialGroup').prev().css('opacity', '1');
                console.log('acciones .5');
                $('#respuestasRadialGroup').prev().animate({
                    opacity: '.1'
                }, 200);
                $('#respuestasRadialGroup').animate({
                    opacity: '.1'
                }, 200, function () {//1500 Tiempo de la animación
                    //fin de la animación
                    muestraSiguientePregunta(_strBoton, idPregunta); 
                    $("#capaSuperior").css('display', 'none');
                console.log('acciones 2');
                });
            }, 500);
            
        }
        //}
        
    }

});


function siguientePregunta( idTipoPregunta, itemRevisar, elementoContenedor ){
    
}

function limpiaArrayLineas(arrRespuestasLineas){
    var i=0;
    var arrResp = new Object();
    $.each(arrRespuestasLineas, function(){       
       arrResp[i] = new Object();
       arrResp[i].id = 0;
       arrResp[i].selected = 0;
    });
    return arrResp;
}

function llenaCartuchos( idTipoPregunta, numCartuchos ){
    var respCartuchos;
    if(idTipoPregunta === "12" || idTipoPregunta === '8' || idTipoPregunta === '2' || idTipoPregunta === '5' || idTipoPregunta === '16' || idTipoPregunta === '15'){
        respCartuchos = new Array();
        for( var i=0; i<numCartuchos; i++){
            respCartuchos[i] = false;
        }
    }
    else if(idTipoPregunta === "13"){
        respCartuchos = new Array();
        for(var i=0; i < numCartuchos; i++){
            respCartuchos[i] = false;
        }
    }
    else{
        respCartuchos = false;
    }
    return respCartuchos;
}

function cuentaRespuestasCorrectas( idTipoPregunta, objRespuesta, eliminaUltimoElemento, idPregunta ){
    var numRespuestasCorrectas = 0;
    if(idTipoPregunta === '12' || idTipoPregunta === '8' || idTipoPregunta === '16' || idTipoPregunta === '15'){
        numRespuestasCorrectas  = eliminaUltimoElemento ? objRespuesta.length - 1 : objRespuesta.length;
    }else if( idTipoPregunta === '2'){
        var numRespuestasTotal = objRespuesta.length;        
        for(var i=0; i<numRespuestasTotal; i++){
            if(objRespuesta[i].t17correcta === '1'){
                numRespuestasCorrectas++;
            }
        }        
    }else if(idTipoPregunta === '5'){
        numRespuestasCorrectas = cuentaOpcionesCorrectasContenedorUnico(idPregunta);
    }
    else if(idTipoPregunta === '13') {//opcion matriz
        var resp = 0;
        var arrNumCorrectas = 0;
        if (obj.pregunta['evaluable']) {
            for (var i = 0; i < objRespuesta.length; i++) {
                arrNumCorrectas = objRespuesta[i].correcta.split(",");
                if (arrNumCorrectas[0] !== '') {
                    resp += arrNumCorrectas.length;
                }
            }
        }else{
            resp = objRespuesta.length;
        }
        numRespuestasCorrectas = resp;
    }
    else{
        numRespuestasCorrectas  = 1;
    }
    return numRespuestasCorrectas;
}

function seCompletoEjercicio( idTipoPregunta, idPregunta ){
    var seCompleto = true;
    var numRespuestas = 0;
    if(idTipoPregunta === '12'){
        seCompleto = false;
        if( typeof objRespuestasUsuario[idPregunta].resp !== 'undefined' ){
            numRespuestas = Object.keys(objRespuestasUsuario[idPregunta].resp).length;            
        }else{
            numRespuestas = Object.keys(objRespuestasUsuario[idPregunta]).length;            
        }        
        if(numRespuestas === $('line').length){
            $('line').each(function(){
                if($(this).css('opacity') === '0'){
                    $(this).remove();
                    //seCompleto = false;
                    //return seCompleto;
                }
            });
        }/*else{
            seCompleto = false;
        }*/
        if(numRespuestas === numeroDeLineasVerdes){
            seCompleto = true;            
        }else{
            seCompleto = false;
        }
        return seCompleto;
    }else if(idTipoPregunta === '8'){
        $('.ui-droppable').each(function(){
            if(!$(this).hasClass('hasCard')){
                seCompleto = false;
                return seCompleto;
            } 
        });
    }else if(idTipoPregunta === '2'){
        var numRespuesas = Object.keys(objPreguntas[idPregunta].respuestas).length;
        var numRespuestasCorrectas = 0; 
        var numRespuestasAcertadas = 0; 
        var respCorrectas = new Array();
        for(var i=0; i<numRespuesas; i++){
            if(objPreguntas[idPregunta].respuestas[i].t17correcta === '1'){
                respCorrectas[numRespuestasCorrectas] = i;
                numRespuestasCorrectas++;
            }
        }
        for(var i=0; i<numRespuestasCorrectas; i++){
            var respSel = respCorrectas[i];            
            if( objRespuestasUsuario[idPregunta].resp[respSel].selected === true){                    
                numRespuestasAcertadas++;

            }
        }
        if(numRespuestasCorrectas !== numRespuestasAcertadas){
            seCompleto = false;
            if( obj.pregunta.secuencia === true ){
               numRespuestasAcertadas = $('.btn_preg_press').not('.press_incorrecta').size(); 
               if(numRespuestasCorrectas !== numRespuestasAcertadas){
                    seCompleto = false;
                }else{
                    seCompleto = true;
                }
            }
        }else{
            if( obj.pregunta.secuencia === true ){
               numRespuestasAcertadas = $('.btn_preg_press').not('.press_incorrecta').size(); 
               if(numRespuestasCorrectas !== numRespuestasAcertadas){
                    seCompleto = false;
                }
            }
        }
    }else if(idTipoPregunta === '5'){
        seCompleto = false;
        var numRespuestas = cuentaOpcionesCorrectasContenedorUnico(idPreguntaGlobal);
        if(obj.pregunta.tipo==="matrizHorizontal"){
            numRespuestas=numRespuestas-contadorRespMostrar;
        }
        else if(obj.contenedoresRespuesta!==undefined){
            numRespuestas=obj.contenedores.length;
        }
        var numRespuesasCompletas=obj.pregunta.contenedorContador===true ? $(".liAcierto").length+1:$(".ui-droppable-disabled").length+1;
        if(numRespuestas === numRespuesasCompletas){
            seCompleto = true;
        }
    }else if(idTipoPregunta === '16'){//Si es sopa de letras
        seCompleto = false;        
        
        if($('.colocadatrue').length === $('.palabraEncontrada').length){
            seCompleto = true;
        }
    }else if(idTipoPregunta === '13'){
        var resp=0;
        var arrNumCorrectas = 0;
        if (obj.pregunta['evaluable']) {
            for(var i=0; i<obj.preguntas.length; i++){
                arrNumCorrectas =  obj.preguntas[i].correcta.split(",");
                if(arrNumCorrectas[0]!==""){
                    resp += arrNumCorrectas.length;                
                }
            }
        }else{
            resp = obj.preguntas.length;
        }
        numRespuestas = resp;
        //numRespuestas = obj.preguntas.length;
        numRespuestasCorrectas = $('input:checked').size();
        if(numRespuestas !== numRespuestasCorrectas){
            seCompleto = false;
        }
    }else if(idTipoPregunta === '15'){
        numRespuestas = Object.keys(objRespuestasUsuario.resp).length;
        for(var i=0;i<numRespuestas;i++){
            if(typeof objRespuestasUsuario.resp[i].selected === 'undefined'){
                seCompleto = false;
                break;
            }
        }
    }
    return seCompleto;
}

function enviaSiguientePregunta(){
    if(c03id_tipo_pregunta !== "2")
        console.log('diferente de 2');
        $('.btn_preg').css('background-color', 'transparent');
    if( !$('.navega-pregunta.siguiente').hasClass('desactiva') ){
        console.log('hay siguiente');
        $('.navega-pregunta.siguiente').trigger('click');
    }else{ // Ultima pregunta
        console.log('trigger click .btn_evaluar');
        $('.btn_evaluar').trigger('click');
    }
 }

function revisaRespuesta(){
    
}
function creaAnimacionHTML(animacion_div, tiempoAnimacionFadeIn) {
    $( '#'+animacion_div ).css('display', 'initial');
    $( '#'+animacion_div ).animate( {    opacity: 1     }, tiempoAnimacionFadeIn, function() {//1500 Tiempo de la animación
       //fin de la animación
    });
}
function eliminaAnimacionHTML(animacion_div) {    
    $( '#'+animacion_div ).css('display', 'none');
    $( '#'+animacion_div ).css('opacity', '0');
}

// Esta funcion solo se utiliza para la variante de respuesta multiple donde debe ir
// eligiendo una secuencia de pasos
function revisaSecuenciaCorrecta(idContestado){ 
    var secuenciaCorrecta = true;
    var id = new Array();
    var elem;
    for(var i=idContestado, j=0; i>=0; i--, j++ ){
        elem = (i+1);
        if(!$('#respuesta'+elem+'_btn').hasClass('btn_preg_press')  && i >= 0){
            id[j] = elem;
            secuenciaCorrecta = false;
        }
    }
    if(secuenciaCorrecta === false && id.length === 1){
        if(id[0] === (idContestado+1)){
            if($('#respuesta'+id[0]+'_btn').hasClass('press_incorrecta')){
                $('#respuesta'+id[0]+'_btn').addClass('btn_preg_press');
                $('#respuesta'+id[0]+'_btn').removeClass('press_incorrecta');
                secuenciaCorrecta = true;
            }            
        }
    }
    return secuenciaCorrecta;
}

function actualizaRespuesta(_objRespuestas, _strBoton, idPregunta) {

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: $('#jq_siguiente_pregunta').val() + '/' + $('#idPregunta').val(),
        data: {
            'botonSeleccionado': _strBoton,
            'objRespuestas': _objRespuestas,
            'idPregunta': idPregunta,
            'tipoPregunta':c03id_tipo_pregunta
        },
        beforeSend: function () {

        },
        success: function (json) {
            if (json) {
                $('#orden').val(json['orden']);
                $('#jsonPregunta').val(json['jsonPregunta']);
                $('#anterior').val(json['anterior']);
                $('#siguiente').val(json['siguiente']);
                $('#primera').val(json['primera']);
                $('#ultima').val(json['ultima']);
                $('#idPregunta').val(json['actual']);
                $('.meter').css('width', json['pctAvance'] + '%');
                $('.meter').parent().attr('title', json['pctAvance'] + '% de avance');
                $('.meter').parent().tooltip({
                    content: json['pctAvance'] + '% de avance'
                });
                //$('.num_preg a').html('<span id="jq_num_pregunta">' + json['numPreg'] + '</span> / ' + json['total']);
                $('.meter').css('width', json['pctAvance'] + '%');
                $('#dd_titulo').html($('#' + $('#idPregunta').val()).html());
//                marcarDesmarcarPregunta(json['marcada']);
//                muestraSiguientePregunta(json);
                $('.nav_style .navega-pregunta').blur();
                var _todasPreguntas = json['pregsExam'];
//                var _preguntaActual = json['actual'];
//                var _ultimoDato = Object.keys(_todasPreguntas).pop();
                if (json['ultima'] == true) {
                    $('#button-finalizar').attr("disabled", false);
                    $('#button-finalizar').removeClass("disabled");
                    $('#button-finalizar').fadeIn('slow');
                } else {
                    $('#button-finalizar').attr("disabled", true);
                    $('#button-finalizar').hide();
                }
            }
        },
        error: function (json) {
            console.log("Hubo un error al procesar los datos: " + json);
        }
    });
}

function obtenerRespuestaUsuario() {
    var respuestaUsuario;
    if (json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === "5") {
        if (json[idPreguntaGlobal].contenedoresFilas !== undefined) {
            agregaCoordenadasMatriz();
            var arrayRespUsuario = [];
            $("#principal .dragRespuesta").each(function (index, value) {
                var idContenedor, idRespuesta;
                idContenedor = $(this).parent().attr("coor");
                arrayRespUsuario.push(idContenedor);
                
            });
            respuestaUsuario = arrayRespUsuario.join("/");
        } else if (json[idPreguntaGlobal].pregunta.tipo === "vertical" || json[idPreguntaGlobal].pregunta.tipo === "horizontal") {
            var arrayCadenaUsuario = [];
            var idSeleccionada;
            var idContenedor;
            $(".dragRespuesta").each(function (indexRespuesta, valRespuesta) {
                idSeleccionada = $(this).attr('value')-1;
                idContenedor = $(this).parent().parent().attr("id").split("_")[1];
                arrayCadenaUsuario[idSeleccionada]=idContenedor;
            });
            respuestaUsuario = arrayCadenaUsuario.join(",");
            
        } else {//Arrastra-Ordena
            var arrayUsuario = [];
            $("#principal .textoCorrecta").each(function (index, value) {
                var id;
                id = $(this).attr("id");
                arrayUsuario.push(id);
            });
            respuestaUsuario = arrayUsuario.join("");
        }
    } else if (json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '13') {
        var arrayRespuesta = [], labelRowSize;
        $(".contenedor-derecha").each(function (index, value) {
            $(this).find(".labelCheck").each(function (indexColumna, valueColumna) {
                labelRowSize = indexColumna < $(this).parent().parent().parent().find(".labelCheck").length-1 ? "_":"";
                if ($(this).hasClass("img-click")) {
                    arrayRespuesta.push(indexColumna+labelRowSize);
                    
                }else{
                    arrayRespuesta.push(''+labelRowSize);
                    
                }
            });
            arrayRespuesta.push(',');
        });
        respuestaUsuario = arrayRespuesta.join("");
    } else if (json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '1' || json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '2') {
        var arrayRespuesta= [];
        $(".btn_preg").each(function (index, value) {
            if ($(this).hasClass("btn_preg_press")) {
                arrayRespuesta.push(index);
            }
        });
        respuestaUsuario = arrayRespuesta.join("");
    } else if (json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '8') {
        var arrayRespuesta = [];
        var respuesta;
        $("#texto span").each(function (index, value) {
            respuesta = $(this).attr("value") !== undefined ? parseInt($(this).attr("value")) - 1 : "";
            arrayRespuesta.push(respuesta);
        });
        var cadena = arrayRespuesta.join("");
        var arrResp = json[idPreguntaGlobal].respuestas;
        var respJson;
        $.each(arrayRespuesta, function (index, value) {
            respJson = value !== '' ? arrResp[this].t17correcta:value;
            arrayRespuesta[index] = respJson;
        });
        respuestaUsuario = arrayRespuesta.join(",");
    }
    else if (json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '15') {//crucigrama
        var arrayCoordenadas = json[idPreguntaGlobal].pocisiones;
        var cadenaTotal = [];
        $.each(arrayCoordenadas, function (index, element) {
            var longPalabra = eliminarParrafos(json[idPreguntaGlobal].respuestas[index].t13respuesta).length;
            var posicionX, posicionY, idCelda;
            var cadenaUsuario = [];
            var separador;
            for (var i = 0; i < longPalabra; i++) {
                separador = i < longPalabra-1 ? ",":"";
                if (element.direccion === 1) {
                    posicionY = element.datoY + i;
                    idCelda = "#elementoEntrada" + element.datoX + "_" + posicionY;
                } else {
                    posicionX = element.datoX + i;
                    idCelda = "#elementoEntrada" + posicionX + "_" + element.datoY;
                }
                cadenaUsuario.push($(idCelda).val()+separador);
            }
            cadenaTotal.push(cadenaUsuario.join(""));
        });
        respuestaUsuario = cadenaTotal.join("_");
    }else if (json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '16') {//Sopa de letras
        var arrWords = [];
        $(".answer").each(function(){
            arrWords.push( $(this).html().replace("&nbsp;&nbsp;", "") );
            
        });
        arrWords.sort();
        respuestaUsuario = arrWords.join("_");
        
    }
    else if (json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '18') {
        var longRespuestas = json[idPreguntaGlobal].respuestas.length;
        var cadenaRespuesta = [];
        for(var i =0; i < longRespuestas; i++){
            var ligados = $("#svgIzquierda" + i).parent().attr("ligados") !== "" ? $("#svgIzquierda" + i).parent().attr("ligados") :"X-X";
            cadenaRespuesta.push(ligados);
            if(i < longRespuestas-1)
                cadenaRespuesta.push("_");
        }
        
        respuestaUsuario = cadenaRespuesta.join("");
        
    }else if(json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === '4'){
        respuestaUsuario = $('.preguntaAbierta').text();
    }
    return respuestaUsuario;
}
function crearCadenaRandom() {
    var cadenaObjetos = "";
    var cadenaRespuestas = "";
    var cadenaPreguntas = ""; //solo aplica en relaciona lineas

    //se genera una cadena aleatoria para los objetos dentro del obj y se aplica el random de acuerdo a la cadena
    for (var co = 0; co < obj.length; co++) {
        cadenaObjetos = cadenaObjetos + co;
    }
//        cadenaObjetos=cadenaObjetos.split('').sort(function(){return 0.5-Math.random()}).join('');
    console.log(cadenaObjetos);
//        if(obj.length>1){
//            obj=shuffleArrayController(obj, cadenaObjetos);
//        }

    cadenaRespuestas = "";
    cadenaPreguntas = "";
    //se genera una cadena aleatoria para las respuestas
    //se evita generar respuestas aleatorias para VF, sopa de letras, Crucigrama, y arrastra contenedor Unico
    if (obj.pregunta.c03id_tipo_pregunta + "" === "3" || obj.pregunta.c03id_tipo_pregunta + "" === "15" || obj.pregunta.c03id_tipo_pregunta + "" === "16" || obj.pregunta.respuestaUnicaMultiple === true) {
    } else {
        for (var i = 0; i < obj.respuestas.length; i++) {
            cadenaRespuestas = cadenaRespuestas + i;
        }
        cadenaRespuestas = cadenaRespuestas.split('').sort(function () {
            return 0.5 - Math.random()
        }).join('');
        //se generan Preguntas aleatorias solo para relaciona lineas
        if (obj.preguntas !== undefined && obj.pregunta.c03id_tipo_pregunta === "12") {
            cadenaPreguntas = cadenaRespuestas.split('').sort(function () {
                return 0.5 - Math.random();
            }).join('');
        }
        obj.respuestas = shuffleArrayController(obj.respuestas, cadenaRespuestas);
    }
    grupoCadenas.cadenas.push({"numeroPregunta": $('#idPregunta').attr('value')-1, "cadenaRespuestas": cadenaRespuestas, "cadenaPreguntas": cadenaPreguntas});
//    if (j === obj.length - 1) {
//        grupoCadenas.estatus = true;
//    }

}

function shuffleArrayController(array, cadena) {
//    var arrayShuffle=[];
//    if(cadena !== 'NA'){
//        var cadenaSplit = cadena.split(',');
////        var currentIndex = cadenaSplit.length - 1, temporaryValue, randomIndex;
////        // While there remain elements to shuffle...
////        while (0 <= currentIndex) {
////            // Pick a remaining element...
////            randomIndex = cadenaSplit[currentIndex];
////            // And swap it with the current element.
////            temporaryValue = array[currentIndex];
////            array[currentIndex] = array[randomIndex];
////            array[randomIndex] = temporaryValue;
////            currentIndex -= 1;
////        }
//        for (var i = 0; i < cadenaSplit.length; i++) {
//            arrayShuffle[i] = array[cadenaSplit[i]];
//        }
//        array = arrayShuffle;
//    }
    
  return array;
}

function agregaCoordenadasMatriz(){
    $(".subContenedor").each(function(index, element){
       $(this).find(".contenedorRespuesta").each(function(indexC, elementC){
           $(this).attr("coor", index+"_"+indexC);
       });
    });
    
}