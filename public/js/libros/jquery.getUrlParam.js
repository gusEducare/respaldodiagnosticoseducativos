/* Copyright (c) 2006-2007 Mathias Bank (http://www.mathias-bank.de)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 * 
 * Version 2.1
 * 
 * Thanks to 
 * Hinnerk Ruemenapf - http://hinnerk.ruemenapf.de/ for bug reporting and fixing.
 * Tom Leonard for some improvements
 * 
 */
jQuery.fn.extend({
/**
* Returns get parameters.
*
* If the desired param does not exist, null will be returned
*
* To get the document params:
* @example value = $(document).getUrlParam("paramName");
* 
* To get the params of a html-attribut (uses src attribute)
* @example value = $('#imgLink').getUrlParam("paramName");
*/ 
 getUrlParam: function(strParamName){
	  strParamName = escape(unescape(strParamName));
	  
	  var returnVal = new Array();
	  var qString = null;
	  
	  if ($(this).attr("nodeName")=="#document") {
	  	//document-handler
		
		if (window.location.search.search(strParamName) > -1 ){
			
			qString = window.location.search.substr(1,window.location.search.length).split("&");
		}
			
	  } else if ($(this).attr("src")!="undefined") {
	  	
	  	var strHref = $(this).attr("src")
	  	if ( strHref.indexOf("?") > -1 ){
	    	var strQueryString = strHref.substr(strHref.indexOf("?")+1);
	  		qString = strQueryString.split("&");
	  	}
	  } else if ($(this).attr("href")!="undefined") {
	  	
	  	var strHref = $(this).attr("href")
	  	if ( strHref.indexOf("?") > -1 ){
	    	var strQueryString = strHref.substr(strHref.indexOf("?")+1);
	  		qString = strQueryString.split("&");
	  	}
	  } else {
	  	return null;
	  }
	  	
	  
	  if (qString==null) return null;
	  
	  
	  for (var i=0;i<qString.length; i++){
			if (escape(unescape(qString[i].split("=")[0])) == strParamName){
				returnVal.push(qString[i].split("=")[1]);
			}
			
	  }
	  
	  
	  if (returnVal.length==0) return null;
	  else if (returnVal.length==1) return returnVal[0];
	  else return returnVal;
	}
});
idActividad =  $(document).getUrlParam("id");
strRutaLibro = obtenerRutaLibro(idActividad);
//strTipoActividad = obtenerTipoActividad(idActividad);
fondoOscuro = $(document).getUrlParam("fondoOscuro") !== null ? $(document).getUrlParam("fondoOscuro") : false;
strAmbiente = 'PROD';  // PROD Para Producción
//strRutaLibro = 'PEF/Delta/';
catalogoActividades=idActividad!==null ? true:false;
calificacionAprobatoria = 80; // Se define el valor minimo aprobatorio
function obtenerRutaLibro(idActividad){
    if(idActividad!==null){
        
        var arrIdActividad = idActividad.split('_');
        var idLibro = arrIdActividad[0];
        var idPrograma = idLibro.substr(0,1);
        var idGrado = idLibro.substr(2,1); 
        var idTipo = idLibro.substr(3,1); // A - Alumno, P - Profesor
        var strPrograma, strGrado, strTipo, strRutaLibro;
        book_color = obtenerColorPrimarioLibro(idLibro);
        switch(idPrograma){
            case 'R':
                strPrograma = 'RTP';
                break;
            case 'F':
                strPrograma = 'PEF';
                break;
            case 'S':
                strPrograma = 'ESP';
                break;
            case 'R':
                strPrograma = 'RTP';
                break;
            case 'G':
                strPrograma = 'GEO';
                break;
            case 'M':
                strPrograma = 'MAT';
                break;
            case 'C':
                strPrograma = 'FCyE';
                break;    
            case 'X':
                strPrograma = 'Intellectus';
                break;   
            case 'H':
                strPrograma = 'HIST';
                break;
            case 'N':
                strPrograma = 'CN';
                break;
            case 'K':
                strPrograma = 'FIS';
                break;
            default:
                strPrograma = 'PEF';
        }
        strPrograma += '/';
        switch(idGrado){
            case '4':
                strGrado = 'Delta';
                break;
            case '5':
                strGrado = 'Epsilon';
                break;
            case '6':
                strGrado = 'Zeta';
                break;
            case '7':
                strGrado = 'Kappa';
                break;
            case '8':
                strGrado = 'Lambda';
                break;
            case '9':
                strGrado = 'Pi';
                break;
            default:
                strGrado = 'Delta';
        }
        strGrado += '/';
        switch(idTipo){
            case 'A':
                strTipo = '';
                break;
            case 'P':
                strTipo = '';
                break;
            default:
                strTipo = '';
        }
        strTipo += '';
        strRutaLibro = strTipo + strPrograma + strGrado; //'PEF/Delta/';
        return strRutaLibro;
   }
}

function obtenerColorPrimarioLibro(idLibro){
    var color_primario = '#0f0';
    return color_primario;
}

function importarJson(ruta, nombre) {    
    var s = document.createElement('script');                 
    s.src = ruta+nombre;        
    document.getElementsByTagName('head')[0].appendChild(s);    
    var idTipoActividad = buscaTipoPregunta(idActividad);
    seleccionaArchivosAcargar(idTipoActividad);
}
function importarScript(ruta, nombre) {    
    document.write('<script type="text/javascript" src="'+ruta+nombre+'"><\/script>');
}
function importarEstilo(ruta, nombre) {    
    document.write('<link href="'+ruta+nombre+'" media="screen" rel="stylesheet" type="text/css">');
}
function seleccionaArchivosAcargar(c03id_tipo_pregunta) {    
    if(c03id_tipo_pregunta === '1'){//Si es Opción Múltiple
        importarScript("js/", "funcionesOpcionMultiple.js");
        importarEstilo("css/", "estilosOpcionMultiple.css");
    }
    if(c03id_tipo_pregunta === '2'){//Si es Respuesta Múltiple
        importarScript("js/", "funcionesOpcionMultiple.js");
        importarEstilo("css/", "estilosOpcionMultiple.css");
    }
    if(c03id_tipo_pregunta === '3'){//Si es Falso Verdadero
        importarScript("js/", "animacionesYSonidosFlash.js");
        importarScript("js/", "funcionesFalsoVerdadero.js");
        importarEstilo("css/", "estilosFalsoVerdadero.css");
    }
    if(c03id_tipo_pregunta === '4'){//Si es Relaciona Combo
        importarScript("js/", "funcionesRelacionaCombo.js");
    }
    if(c03id_tipo_pregunta === '5'){//Si es Arrastra Contenedor
        importarScript("js/", "funcionesArrastraContenedor.js");
        importarEstilo("css/", "estilosArrastraContenedor.css");
    }
    if(c03id_tipo_pregunta === '8'){//Si es Arrastra Corta
        importarScript("js/", "funcionesArrastraCortas.js");
        importarEstilo("css/", "estilosArrastraCorta.css");
    }
    if(c03id_tipo_pregunta === '9'){//Si es Arrastra Ordenar
        importarScript("js/", "funcionesArrastraOrdenar.js");
    }
    if(c03id_tipo_pregunta === '11'){//Si es Número Relacionar
        importarScript("js/", "funcionesRelacionarCajas.js");            
    }
    if(c03id_tipo_pregunta === '12'){//Si es Relaciona Líneas        
        importarScript("js/", "funcionesRelacionarColumnas.js");
        importarEstilo("css/", "estilosRelacionaColumnas.css");
    }
    if(c03id_tipo_pregunta === '13'){//Si es Matriz
        importarScript("js/", "animacionesYSonidosFlash.js");
        importarScript("js/", "funcionesMatriz.js");        
        importarEstilo("css/", "estilosMatriz.css");
    }
    if(c03id_tipo_pregunta === '14'){//Si es Rompecabezas
        importarScript("js/", "funcionesRompecabezas.js");
        importarEstilo("css/", "estiloRompecabezas.css");
    }
    if(c03id_tipo_pregunta === '15'){//Si es Crucigrama       
        importarEstilo("css/", "estilosCrucigrama.css");              
        importarScript("js/", "funcionesCrucigrama.js");                
    }
    if(c03id_tipo_pregunta === '16'){//Si es Sopa de Letras        
        importarScript("js/", "funcionesSopadeLetras.js");
        importarEstilo("css/", "estiloSopadeletras.css");
    }
    if(c03id_tipo_pregunta === '17'){//Si es Relaciona Arrastrar
        importarScript("js/", "funcionesRelacionarArrastrar.js");          
    }
}
function buscaTipoPregunta(idActividad){    
    for(var i=0; i<contents.length; i++){
        for(var j=0; j<contents[i].actividad.length; j++){
            if(contents[i].actividad[j].navegar === idActividad){
                return contents[i].actividad[j].tipoAct;
            }
        }
    }
    return "13"; // Regreso el tipo de actividad matriz para que funcionen las autoevluaciones
}