/*______         __              __                             _______                         __                   
  |   __ \.-----.|  |.---.-.----.|__|.-----.-----.---.-.        |   _   |.----.----.---.-.-----.|  |_.----.---.-.----.
  |      <|  -__||  ||  _  |  __||  ||  _  |     |  _  |        |       ||   _|   _|  _  |__ --||   _|   _|  _  |   _|
  |___|__||_____||__||___._|____||__||_____|__|__|___._|        |___|___||__| |__| |___._|_____||____|__| |___._|__|                                                                                                                                                                                          
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com        
*********************************************************/
var encabezado1 = '';
var encabezado2 = '';
var trazo = '';
var pathFinal = '';
var textoSVG = '';
var encabezado1CajaDerecha = '';
var encabezado2CajaDerecha = '';
var trazosSVGSecundario = '';
var ancho = 375.6;
var formaSVG = '';
var trazosSVG = '';
var separacion = 0;
var texto = '';
var texto2 = '';
var colorSobrePieza = 'lime';
var colorNormalPieza = '#54B084';
var colorNormalPiezaVacia = '#E2E2E2';
var separacionPiezas = 28;
var hizoDrop = false;
var renglones_arr = new Array();
var posRenglones = 0;
var renglonesMaximo;
var alturaTexto = 72;
var colorContornoPieza = '#000000';
var posLeftInicialDerecha = 432;//445
var datosRelacionaArrastra = new Array();
var posicionesTopIniciales_arr = new Array();
var eventoPress;
var eventoRelease;
var eventoMove;
var posLeftInicialIzquierda = 0;//-12 16  dato clave: Posicion X de las cajas de la izquierda
var posXUnidaIzquierda = 26;//48
var posXUnidaDerecha = 415;
var alturaContentAjustada;

function estructuraHTMLRelacionaArrastrar(estructuraBotones ){
    $("#respuestasRadialGroup").html(estructuraBotones);
    creaRelacionaArrastrar();
}

Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});
function preparaReacomodoDePiezas (datos_arr) {
    var ladoIzquierdo;
    var ladoDerecho;
    var union;
    for(var f=0;f<datos_arr.length;f++){
        ladoIzquierdo = datos_arr[f];
        ladoDerecho = datos_arr[f];
        union = datos_arr[f];
        ladoIzquierdo = ladoIzquierdo.substring(0,ladoIzquierdo.indexOf('-',0) );
        ladoDerecho = ladoDerecho.substring( ladoDerecho.indexOf('-',0)+1, ladoDerecho.indexOf('|',0) );
        union = union.substring( union.indexOf('|',0)+1, union.length );
        reubicarPiezas(ladoIzquierdo, ladoDerecho, union, f);
    }
}
function reubicarPiezas(ladoIzquierdo, ladoDerecho, union, incremento) {
    posicionesTopIniciales_arr = posicionesTopIniciales_arr.unique();
    var reemplazadaActualizadaTop = posicionesTopIniciales_arr[incremento];
    var reemplazadaActualizadaLeft1;
    var reemplazadaActualizadaLeft2;
    var cajaIzquierda = '#cajaIzquierda'+ladoIzquierdo;
    var cajaIzquierdaVacia = '#cajaIzquierdaVacia'+ladoIzquierdo;
    var cajaDerecha = '#cajaDerecha'+ladoDerecho;
    var cajaDerechaVacia = '#cajaDerechaVacia'+ladoDerecho;
    
    if(union === '0'){//Sino son piezas unidas
        reemplazadaActualizadaLeft1 = posLeftInicialIzquierda;
        reemplazadaActualizadaLeft2 = posLeftInicialDerecha;
    }else{//Si son piezas unidas
        reemplazadaActualizadaLeft1 = posXUnidaIzquierda;
        reemplazadaActualizadaLeft2 = posXUnidaDerecha;
    }
    
    $( cajaIzquierda ).css({left:reemplazadaActualizadaLeft1, top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja izquierda verde
    $( cajaDerecha ).css({left:reemplazadaActualizadaLeft2, top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja derecha verde
    $( cajaIzquierdaVacia ).css({ top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja izquierda vacia verde
    $( cajaDerechaVacia ).css({ top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja derecha vacia verde
            
    $( cajaIzquierda ).attr('ligados', ladoIzquierdo+'-'+ladoDerecho);//Actualiza ligados de ambas cajas unidas
    $( cajaDerecha ).attr('ligados', ladoIzquierdo+'-'+ladoDerecho);//Actualiza ligados de ambas cajas unidas
    
    $( cajaIzquierda ).attr('haceLinea1', ladoIzquierdo).attr('haceLinea2', ladoDerecho);//Actualiza linea1 y linea2 de ambas cajas unidas
    $( cajaDerecha ).attr('haceLinea1', ladoIzquierdo).attr('haceLinea2', ladoDerecho);//Actualiza linea1 y linea2 de ambas cajas unidas
    
    $( cajaIzquierda ).draggable( 'option', 'revert', false );//Elimina el reverse de ambas cajas unidas
    $( cajaDerecha ).draggable( 'option', 'revert', false );//Elimina el reverse de ambas cajas unidas
        
    reemplazadaActualizadaLeft1 = posLeftInicialIzquierda;   reemplazadaActualizadaLeft2 = posLeftInicialDerecha;
    actualizaPosiciones ( cajaIzquierda , reemplazadaActualizadaLeft1, reemplazadaActualizadaTop);//Actualiza posiciones de ambas cajas unidas
    actualizaPosiciones ( cajaDerecha , reemplazadaActualizadaLeft2, reemplazadaActualizadaTop);//Actualiza posiciones de ambas cajas unidas
    actualizaPosiciones ( cajaIzquierdaVacia , reemplazadaActualizadaLeft1, reemplazadaActualizadaTop);//Actualiza posiciones de ambas cajas vacías unidas
    actualizaPosiciones ( cajaDerechaVacia , reemplazadaActualizadaLeft2, reemplazadaActualizadaTop);///Actualiza posiciones de ambas cajas vacías unidas
    
    if(union === '1'){//Si son piezas unidas
        opacarPieza( cajaIzquierdaVacia );//Oculta ambas cajas vacias unidas
        opacarPieza( cajaDerechaVacia );//Oculta ambas cajas vacias unidas
        aplicaSombra( cajaIzquierda );//Aplica sombra a ambas cajas unidas
        aplicaSombra( cajaDerecha );//Aplica sombra a ambas cajas unidas        
        zIndexPieza ( cajaIzquierda, '4' );//aplica Zindex para enviar al frente la caja izquierda
        zIndexPieza ( cajaDerecha, '3' );//aplica Zindex para enviar al frente la caja izquierda      
    }    
}
function construyeSVGvacio (encabezado, trazos) {
    var formaSVG;
    formaSVG = encabezado;
    formaSVG += trazos + '</svg>';
    return formaSVG;
}
function formaTextoSVG (elemento, contenido){
    /*var textoRegresado = '';    textoRegresado += '<g style="stroke: black; white-space: pre-wrap;"><text id="multiline-text" x='+posXTexto+' y=40 white-space: nowrap;>'+contenido+'</text></g></svg>';    return textoRegresado;*/
}
function creaTextos (elemento, contenido, consecutivo, imprimir, idParrafo){    
    var svgUtilizado = Snap( elemento );
    var sombraDeNumero = '0px 1px 0px #999, 0px 2px 0px #888, 0px 3px 0px #777, 0px 4px 0px #666, 0px 5px 0px #555, 0px 6px 0px #444, 0px 7px 0px #333, 0px 8px 7px #001135';
    var posX;
    var posXtexto;
    var anchoFondo2;
    var parrafo = consecutivo;
    var ajuste = 10;
    consecutivo++;//Para que la numeración empiece en 1
    var codigoID;
    var numeracionSVG = elemento;
    var puntajeLetra;
    var posYtexto = 2;
    var interlineadoTexto = '1.03';
    var distanciaX;
    var distanciaY;
    var anchoImagen;
    var altoImagen;
    var ajuste = 0;
    var imagenAcargar;
    var posicion;
    
    if(consecutivo === 2){
        /*contenido = 'Había una vez una oruga que quería';//1 renglón
        contenido = 'Había una vez una oruga que quería ser mariposa,';//2 renglones
        contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas.';//3 renglones
        contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días';//4 renglones        
        contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses. Y se';//5 renglones
        contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses. Y se encontraba muy desesperada y triste';//6 renglones
        contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses. Y se encontraba muy desesperada y triste. y la pobre oruga';//6 renglones                
        contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses;';//  hermosa mariposa con alas, además ser muy colorida, pero ese momento no llegaba.'; */        
    }
        
    if( typeof obj.pregunta.sizeFuente === 'undefined'){//Sino la trae el json setea el valor por default                        
        if(contenido.length < 211){
            puntajeLetra = '20px';
        }else{
            if(contenido.length > 211 && contenido.length <= 237) {//Si el texto es muy grande
                puntajeLetra = '16px';//Disminuye el tamaño de la fuente
                interlineadoTexto = '1.12';
            }else{
                if(contenido.length >= 238 && contenido.length <= 263){
                    puntajeLetra = '16px';//Disminuye el tamaño de la fuente
                    interlineadoTexto = '1.24';
                }else{
                    if(contenido.length > 263){
                        alert('El ejercicio no soporta más de 263 caracteres');
                    }
                }
            }
        }                
    }else{
        puntajeLetra = obj.pregunta.sizeFuente;
    }  
    
    if( typeof obj.pregunta.anchoImagen === 'undefined'){//Sino la trae el json setea el valor por default
        anchoImagen = 110;//Es el ancho en la que se desea ver la imagen
        altoImagen = 113;//Es la altura en la que se desea ver la imagen
    }else{
        anchoImagen = obj.pregunta.anchoImagen;//Es el ancho en la que se desea ver la imagen
        altoImagen = obj.pregunta.altoImagen;//Es la altura en la que se desea ver la imagen  
    }
    
    if(elemento.substring(0,5) === '#svgI' || elemento.substring(0,13) === '#svgcajaTempI'){
        posX = '-5';
        consecutivo += ' |';
        posXtexto = '0';//34  //Posición X del texto izquierdo
        anchoFondo2 = '396';//336 //Anchura del texto izquierdo
        
        imagenAcargar = contenido;
        posicion = imagenAcargar.indexOf('<img', 0);

        if(posicion >= 0 ){//Si tiene imagen  
            if(elemento.substring(0,5) === '#svgI'){
                if( typeof obj.pregunta.anchoImagen === 'undefined'){//Sino la trae el json setea el valor por default
                    distanciaX = 150;
                    distanciaY = 5;
                }else{
                    var anchoDelSVG = $(elemento).css('width').replace('px', '');                
                    anchoDelSVG = anchoDelSVG/2;
                    anchoDelSVG = anchoDelSVG - anchoImagen/2;
                    anchoDelSVG = anchoDelSVG - 10;
                    var altoDelSVG = $(elemento).css('height').replace('px', '');
                    altoDelSVG = altoDelSVG/2;
                    altoDelSVG = altoDelSVG - altoImagen/2;
                    distanciaX = anchoDelSVG;
                    distanciaY = altoDelSVG;
                }
            }
        }                
    }else{
        if(elemento.substring(0,5) === '#svgD' || elemento.substring(0,13) === '#svgcajaTempD'){
            posX = '340';
            consecutivo = '| '+consecutivo;
            posXtexto = '22';// Posición X del texto derecho
            anchoFondo2 = '439';//320 //Anchura del texto derecho
        }
        
        imagenAcargar = contenido;
        posicion = imagenAcargar.indexOf('<img', 0);

        if(posicion >= 0 ){//Si tiene imagen  
            if(elemento.substring(0,5) === '#svgD'){
                if( typeof obj.pregunta.anchoImagen === 'undefined'){//Sino la trae el json setea el valor por default
                    distanciaX = 180;
                    distanciaY = 5;
                }else{
                    var anchoDelSVG = $(elemento).css('width').replace('px', '');                
                    anchoDelSVG = anchoDelSVG/2;
                    anchoDelSVG = anchoDelSVG - anchoImagen/2;
                    anchoDelSVG = anchoDelSVG + 10;
                    var altoDelSVG = $(elemento).css('height').replace('px', '');
                    altoDelSVG = altoDelSVG/2;
                    altoDelSVG = altoDelSVG - altoImagen/2;
                    distanciaX = anchoDelSVG;
                    distanciaY = altoDelSVG;
                }
            }
        }                
    }

    parrafo = idParrafo;
    numeracionSVG = numeracionSVG.replace('#svgDerecha', 'svgNumeracionDerecha').replace('#svgIzquierda', 'svgNumeracionIzquierda');
            
    if(imprimir === 'sinDatos'){
        consecutivo = '';
    }
    
    if(renglonesMaximo === 1 || renglonesMaximo === 2 || renglonesMaximo === 3){
        alturaTexto = 72;
    }else{
        alturaTexto = (renglonesMaximo+3) * 12;
    }
    alturaTexto = alturaTexto + ajuste;
            
    if( isNaN(alturaTexto) ){
        alturaTexto = 0;
        numeracionSVG = "sinId";
    }        
    
    var cadenaContenido = contenido;
    var palabra5 = cadenaContenido.indexOf('border: 1px solid white', 0);        
    contenido = contenido.replace(/'/g, '"');   
    if(palabra5 >= 0){//Si trae un estilo especial de una raya blanca
        contenido = "";
    }
    
    var contenido2 = contenido;
    contenido2 = contenido2.replace('src=', 'alt=');//Para que muestre el nombre de la imagen si se genera un error en la carga       
    
    var objetoSVG = '<svg>';
    //Se setea display en none para ocultar la numeración
    objetoSVG +=    '<foreignObject width="50" height="100" x="'+posX+'" y="16"><body><p id="'+numeracionSVG+'" style="display: none; font: 26px Arial; text-shadow: '+sombraDeNumero+';">'+consecutivo+'</p></body></foreignObject>';
    objetoSVG +=    '<foreignObject width="'+anchoFondo2+'"; height="'+alturaTexto+'px"; x="'+posXtexto+'" y="'+posYtexto+'"><body><p id="'+parrafo+'" style="white-space: normal; line-height: '+interlineadoTexto+'; font-size: '+puntajeLetra+';">'+contenido2+'</p></body></foreignObject>';
    objetoSVG +=    '</svg>';
    var parseado = Snap.parse( objetoSVG );
    var textoInsertado = svgUtilizado.group().append( parseado );
        
    imagenAcargar = contenido;
    posicion = imagenAcargar.indexOf('<img', 0);
    
    var extension = "";
    if(posicion >= 0 ){//Si tiene imagen                        
        imagenAcargar = imagenAcargar.toLowerCase();        
        var posicion2 = imagenAcargar.indexOf('src=', 0);
        var posicion3 = imagenAcargar.indexOf('.png', 0);
        var posicion4 = imagenAcargar.indexOf('.jpg', 0);
        var posicion5 = imagenAcargar.indexOf('.jpeg', 0);
        var posicion6 = imagenAcargar.indexOf('.gif', 0);
        extension = ".png";
        
        if(posicion3 >= 0){//Si es PNG
            extension = ".png";
        }else{
            if(posicion4 >= 0){//Si es JPG
                extension = ".jpg";
            }else{
                if(posicion5 >= 0){//Si es JPEG
                    extension = ".jpeg";
                }else{
                    if(posicion6 >= 0){//Si es GIF
                        extension = ".gif";
                    }else{
                        extension = "";
                    }
                }
            }
        }
        var nombreImagen = imagenAcargar.substring( posicion2+5,  posicion3 );                   
                
        var tipo = idActividad.charAt(3);//A= Actividad (Alumno),  E= Evaluación        
        if(tipo === 'E'){//Si es para Evaluación
            rutaAbsoluta = "../../../../EVAL/";//Ajuste para que lea la ruta absoluta                
        }else{//Si es para Actividad (Alumno) ó Evidencia
            rutaAbsoluta = "../../../../" + strRutaLibro;//Ajuste para que lea la ruta absoluta                
        }        
        //console.log(rutaAbsoluta+nombreImagen);       
        var g = svgUtilizado.g();
        var image = g.image(rutaAbsoluta+nombreImagen+extension, distanciaX,distanciaY, anchoImagen,altoImagen );//50,5, 110,110        
    }else{//Sino tiene imagen
    }
    $( '#desc').remove();
}
function clasesEspeciales (contenido, idParrafo, i){
    var cadenaContenido = contenido;
    var palabra1 = cadenaContenido.indexOf('fraction', 0);
    var palabra2 = cadenaContenido.indexOf('bottom', 0);
    var palabra3 = cadenaContenido.indexOf('top', 0);
    var palabra4 = cadenaContenido.indexOf('<span', 0);
    var palabra5 = cadenaContenido.indexOf('border: 1px solid white', 0);
    var palabra6 = cadenaContenido.indexOf('<div margin-top: -9px;>', 0);
        
    contenido = contenido.replace(/'/g, '"');
    if(palabra1 >= 0 && palabra2 >= 0 && palabra3 >= 0 && palabra4 >= 0){//Si el contenido trae span y class para pintar fracciones        
        $('#'+idParrafo).html( contenido );
    }else{
        if(palabra5 >= 0){                        
            contenido = contenido.replace('<div style="border: 1px solid white', '<div id="rayaBlanca'+i+'" style="border: 1px solid white');            
            $('#'+idParrafo).html( contenido );            
        }
    }
    
    if(palabra6 >= 0){
        contenido = contenido.replace('<div margin-top: -9px;>', '<div id="textoEspecial'+i+'" margin-top: -9px;>');
        $('#'+idParrafo).html( contenido );
    }            
    //console.log('contenido: '+contenido);
    //console.log('- - - - - - -  -- - - - ');
}
function sanitizarTexto (contenido){
    contenido = contenido.replace(/<p>/g, "");
    contenido = contenido.replace(/<\/p>/g, "");//Sanitizo último campo
    return contenido;
}
function generaBotonDraggableTemporal (cualCaja,i){    
    var sombra;
    nuevoSVG = '<svg id="svg'+cualCaja+'" xmlns="http://www.w3.org/2000/svg" version="1.1" style="width: 100%;" height="92" focusable="false" fill="#54B084" ><path fill="'+colorNormalPieza+'" d="m0.27324,4.9154l0,67.97759c0,3.13333 2.11316,4.97552 5.24649,4.97324l375.05357,-0.27325c3.1333,-0.0023 4.39749,-2.93301 4.4267,-6.06622l0.27325,-29.31136c0.01239,-1.33327 -0.66684,-2 -2,-2l-5.14996,0c-1.13333,3.33334 -2.75003,5 -4.85004,5c-4,0 -6,-3.33333 -6,-10c0,-6.66666 2,-10 6,-10c2.10001,0 3.71649,1.66667 4.85004,5l5.14996,0c1.33316,0 2,-0.66666 2,-2l0,-23.29999c0,-3.13334 -1.56665,-4.7 -4.69995,-4.7l-375.60005,0c-3.13334,0 -4.7,1.56666 -4.7,4.7"/></svg>';
    formaSVG = nuevoSVG;
    
    
    //console.log('nuevoSVG '+nuevoSVG);
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        sombra = '';//No aplica la sombra
    }else{
        sombra = 'class="shadow"';//Aplica la sombra
    }
    
    $( '<div '+sombra+' style="width: 285px; height: 72px; transparent: true;">'+formaSVG+'</div>' ).attr( 'id', cualCaja ).appendTo( '#pilaDeCartasTemporal' ).draggable( {
        containment: '#content',
        stack: '#pilaDeCartasTemporal div',
        cursor: 'move',
        revert: true
    });
}
function generaBotonDroppable (cualCaja, i){        
    formaSVG = obtieneSVG(cualCaja, i, colorNormalPiezaVacia);
    
    $( '<div style="width: 285px; height: 72px;">'+formaSVG+'</div>' ).attr( 'id', cualCaja ).appendTo( '#pilaDeCartasExtra' ).droppable( { //Áreas vacías
        containment: '#content',
        stack: '#pilaDeCartasExtra div',
        drop: sueltaPiezaArrastrar,
        over: sobrePieza,
        out: fueraPieza,
        create: alCrearPieza
    });    
}
function generaBotonDraggable (cualCaja,i){
    var sombra;
    formaSVG = obtieneSVG(cualCaja, i, colorNormalPieza);
    
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        sombra = '';//No aplica la sombra
    }else{
        sombra = 'class="shadow"';//Aplica la sombra
    }            
        
    $('#content').css('width', '100%');
    $('#content').css('border', '1px solid transparent');
    $('#content').css('padding-left', '0px');
    $('#content').css('padding-right', '0px');
    $('#content').css('left', '8px');
    
    $('#content').append('<div id="content2"></div>');
    $('#content2').css('width', '100%');
    $('#content2').css('height', '100%');
    $('#content2').css('border', '1px solid transparent');
    $('#content2').css('padding-left', '0px');
    $('#content2').css('padding-right', '13px');        
    
    var elementoArrastrado = cualCaja;
    var encontrado = elementoArrastrado.indexOf('Izquierda', 0);
    var cualContent;
    if(encontrado >= 1){//Si es un elemento de la izquierda
        cualContent = '#content';
    }else{//Si es un elemento de la derecha
        cualContent = '#content2';
    }
    
    $( '<div '+sombra+' style="width: 285px; height: 72px; transparent: true;">'+formaSVG+'</div>' ).attr( 'cualFila', i ).attr( 'ligados', '' ).attr( 'haceLinea1', i ).attr( 'haceLinea2', i ).data( 'number', i).attr( 'id', cualCaja ).appendTo( '#pilaDeCartas' ).draggable( {
        containment: cualContent,
        stack: '#pilaDeCartas div',
        cursor: 'move',
        scrollSpeed: 150,
        scrollSensitivity: 150,
        revert: true,
        drag: muevePieza,
        start: iniciaPieza,
        stop: detienePieza,
        create: alCrearPieza
    });
}
function obtieneSVG (cualCaja, i, color) {
    var idTemp;
    var nuevoSVG = '';
    var relleno = 'fill="'+color+'"';
    var linea = 'stroke="'+colorContornoPieza+'"';
    
    if(cualCaja.indexOf('Vacia', 0) !== -1){//Si es una caja vacía
        linea = '';//No necesita linea
    }
    
    if(cualCaja.substring(0,5) === 'cajaD'){
        if(cualCaja.indexOf('Vacia', 0) !== -1){//Si es una caja vacía
            idTemp = 'svgDerechaVacia'+i;
        }else{
            idTemp = 'svgDerecha'+i;
        }
        
        nuevoSVG = '<svg id="'+idTemp+'" xmlns="http://www.w3.org/2000/svg" version="1.1" style="width: 100%;" height="'+separacion+'" focusable="false"><path '+relleno+' '+linea;
        
        var altoImagenSeteada;
        if( typeof obj.pregunta.anchoImagen === 'undefined'){//Sino la trae el json setea el valor por default
            altoImagenSeteada = 113;
        }else{
            altoImagenSeteada = obj.pregunta.altoImagen;
        }        
        if(altoImagenSeteada <= 70){
            renglonesMaximo = 3;
        }else{
            if(altoImagenSeteada > 70 && altoImagenSeteada <= 84){
                renglonesMaximo = 4;
            }else{
                if(altoImagenSeteada > 84 && altoImagenSeteada <= 104){
                    renglonesMaximo = 5;
                }else{
                    if(altoImagenSeteada > 104 && altoImagenSeteada <= 124){
                        renglonesMaximo = 6;
                    }
                }
                
            }
            
        }
                
        if(renglonesMaximo <= 3){//SVG Derecha de 1 a 3 renglones
            nuevoSVG += ' d="m 20.805785,6.7 c 0,-3.1333333 1.767357,-4.7 5.302066,-4.7 L 448.31782,2 c 7.16888,0 7.17342,11.052362 7.18218,9.849327 l -0.37601,51.696015 C 455.10124,66.671893 454.67162,72 447.56572,72 L 26.107851,72 c -3.534709,0 -5.302066,-1.566667 -5.302066,-4.7 l 0,-22.3 c 0,-1.333333 -0.752065,-2 -2.256196,-2 l -3.553516,0 C 13.454339,46.333333 11.254545,48 8.3966938,48 3.1322315,48 0.5,44.333333 0.5,37 c 0,-7.333333 2.6322315,-11 7.8966938,-11 2.8578512,0 5.0576452,1.666667 6.5993792,5 l 3.553516,0 c 1.504131,0 2.256196,-0.666667 2.256196,-2 l 0,-22.3"';//nuevoSVG += ' d="m17.69669,6.7c0,-3.13333 1.49675,-4.7 4.49025,-4.7l357.56388,0c6.07123,0 6.07507,11.05236 6.08249,9.84933l-0.31845,51.69601c-0.01926,3.12655 -0.38309,8.45466 -6.40097,8.45466l-356.92695,0c-2.9935,0 -4.49025,-1.56667 -4.49025,-4.7l0,-22.3c0,-1.33333 -0.63691,-2 -1.91074,-2l-3.00942,0c-1.30568,3.33333 -3.16865,5 -5.58893,5c-4.4584,0 -6.6876,-3.66667 -6.6876,-11c0,-7.33333 2.2292,-11 6.6876,-11c2.42028,0 4.28325,1.66667 5.58893,5l3.00942,0c1.27383,0 1.91074,-0.66667 1.91074,-2l0,-22.3"/>';
        }else{
            if(renglonesMaximo === 4){//SVG Derecha de 4 renglones'
                nuevoSVG += ' d="m 20.108581,4.7 c 0,-3.1333333 1.750191,-4.7 5.250574,-4.7 L 444.95823,0 c 3.50036,0 5.25055,1.5666667 5.25055,4.7 l 0,73.6 c 0,3.133333 -1.75019,7.2 -5.25055,7.2 l -419.599075,0 c -3.500384,0 -5.250574,-4.566667 -5.250574,-7.7 l 0,-26.8 c 0,-1.333333 -0.744762,-2 -2.234287,-2 l -3.519001,0 C 12.82853,52.333333 10.6501,54 7.8200035,54 2.6066678,54 0,50.333333 0,43 0,35.666667 2.6066678,32 7.8200035,32 10.6501,32 12.82853,33.666667 14.355293,37 l 3.519001,0 c 1.489525,0 2.234287,-0.666667 2.234287,-2 l 0,-30.3"';//nuevoSVG += ' d="m18.06303,5.5c0,-3.13333 1.50252,-4.7 4.50757,-4.7l360.22185,0c3.005,0 4.50754,1.56667 4.50754,4.7l0,73.60001c0,3.13332 -1.50253,7.2 -4.50754,7.2l-360.22186,0c-3.00505,0 -4.50757,-4.56667 -4.50757,-7.7l0,-26.80001c0,-1.33333 -0.63937,-2 -1.91812,-2l-3.02103,0c-1.3107,3.33333 -3.18086,5 -5.61047,5c-4.4756,0 -6.7134,-3.66667 -6.7134,-11c0,-7.33333 2.2378,-11 6.7134,-11c2.42961,0 4.29977,1.66667 5.61048,5l3.02103,0c1.27875,0 1.91812,-0.66667 1.91812,-2l0,-30.3"/>';
            }
            if(renglonesMaximo === 5){//SVG Derecha de 5 renglones
                nuevoSVG += ' d="m 20.894663,4.9999913 c 0,-3.1333333 1.766567,-4.70000015 5.2997,-4.70000015 l 423.525257,0 c 3.53311,0 5.29967,1.56666685 5.29967,4.70000015 l 0,90.5999997 c 0,3.13333 -1.76656,7.195839 -5.29967,7.200009 l -423.525257,0.5 c -3.533132,0.004 -5.2997,-4.066679 -5.2997,-7.200009 l 0,-34.8 c 0,-1.333333 -0.751732,-2 -2.255198,-2 l -3.551926,0 c -1.541047,3.333333 -3.739861,5 -6.5964403,5 -5.2621178,0 -7.89317589,-3.666667 -7.89317589,-11 0,-7.333333 2.63105809,-11 7.89317589,-11 2.8565793,0 5.0553933,1.666667 6.5964403,5 l 3.551926,0 c 1.503466,0 2.255198,-0.666667 2.255198,-2 l 0,-40.2999997"';//nuevoSVG += ' d="m17.7006,4.99999c0,-3.13333 1.48856,-4.7 4.46569,-4.7l356.87576,0c2.97711,0 4.46567,1.56667 4.46567,4.7l0,90.6c0,3.13333 -1.48856,7.19584 -4.46567,7.20001l-356.87576,0.5c-2.97713,0.004 -4.46569,-4.06668 -4.46569,-7.20001l0,-34.8c0,-1.33333 -0.63344,-2 -1.9003,-2l-2.99297,0c-1.29853,3.33333 -3.15132,5 -5.55837,5c-4.43402,0 -6.65104,-3.66667 -6.65104,-11c0,-7.33333 2.21702,-11 6.65104,-11c2.40705,0 4.25984,1.66667 5.55837,5l2.99297,0c1.26686,0 1.9003,-0.66667 1.9003,-2l0,-40.3"/>';
            }
            if(renglonesMaximo === 6){//SVG Derecha de 6 renglones
                nuevoSVG += ' d="m 20.413884,4.8 c 0,-3.1333334 1.763601,-4.70000005 5.290805,-4.70000005 l 422.814291,0 c 3.52717,0 5.29077,1.56666665 5.29077,4.70000005 l 0,108.6 c 0,3.13333 -1.7636,7.69583 -5.29077,7.7 l -422.814291,0.5 c -3.527204,0.004 -5.290805,-4.06667 -5.290805,-7.2 l 0,-45.3 c 0,-1.333333 -0.75047,-2 -2.251406,-2 l -3.545968,0 c -1.538462,3.333333 -3.733583,5 -6.5853655,5 -5.2532835,0 -7.87992499,-3.666667 -7.87992499,-11 0,-7.333333 2.62664149,-11 7.87992499,-11 2.8517825,0 5.0469035,1.666667 6.5853655,5 l 3.545968,0 c 1.500936,0 2.251406,-0.666667 2.251406,-2 l 0,-48.3"';//nuevoSVG += ' d="m17.37449,4.8c0,-3.13333 1.49906,-4.7 4.49718,-4.7l359.39215,0c2.99811,0 4.49716,1.56667 4.49716,4.7l0,108.6c0,3.13333 -1.49905,7.69583 -4.49716,7.7l-359.39215,0.5c-2.99812,0.004 -4.49718,-4.06667 -4.49718,-7.2l0,-45.3c0,-1.33333 -0.6379,-2 -1.9137,-2l-3.01407,0c-1.3077,3.33333 -3.17355,5 -5.59756,5c-4.4653,0 -6.69794,-3.66667 -6.69794,-11c0,-7.33333 2.23264,-11 6.69794,-11c2.42401,0 4.28986,1.66667 5.59756,5l3.01407,0c1.27579,0 1.9137,-0.66667 1.9137,-2l0,-48.3"/>';
            }
        }
    }else{
        if(cualCaja.indexOf('Vacia', 0) !== -1){//Si es una caja vacía
            idTemp = 'svgIzquierdaVacia'+i;
        }else{
            idTemp = 'svgIzquierda'+i;
        }
        
        nuevoSVG = '<svg id="'+idTemp+'" xmlns="http://www.w3.org/2000/svg" version="1.1" style="width: 100%;" height="'+separacion+'" focusable="false"><path '+relleno+' '+linea;
        
        var altoImagenSeteada;
        if( typeof obj.pregunta.anchoImagen === 'undefined'){//Sino la trae el json setea el valor por default
            altoImagenSeteada = 113;
        }else{
            altoImagenSeteada = obj.pregunta.altoImagen;
        }        
        if(altoImagenSeteada <= 70){
            renglonesMaximo = 3;
        }else{
            if(altoImagenSeteada > 70 && altoImagenSeteada <= 84){
                renglonesMaximo = 4;
            }else{
                if(altoImagenSeteada > 84 && altoImagenSeteada <= 104){
                    renglonesMaximo = 5;
                }else{
                    if(altoImagenSeteada > 104 && altoImagenSeteada <= 124){
                        renglonesMaximo = 6;
                    }
                }
                
            }
            
        }
        
        if(renglonesMaximo <= 3){//SVG Izquierda de 1 a 3 renglones
            nuevoSVG += 'd="m 0.6,6.2 0,60.6 q 0,4.7 5.0051948,4.7 l 399.9896052,0 q 5.0052,0 5.0052,-4.7 l 0,-23.3 q 0,-2 -2.12987,-2 l -5.48441,0 q -1.8104,5 -5.16494,5 -6.38961,0 -6.38961,-10 0,-10 6.38961,-10 3.35454,0 5.16494,5 l 5.48441,0 q 2.12987,0 2.12987,-2 l 0,-23.3 q 0,-4.7 -5.0052,-4.7 L 5.6051948,1.5 Q 0.6,1.5 0.6,6.2"';//nuevoSVG += ' d="m0.6,5.7l0,60.6q0,4.7 4.7,4.7l375.60002,0q4.69998,0 4.69998,-4.7l0,-23.3q0,-2 -2,-2l-5.14999,0q-1.70001,5 -4.85001,5q-6,0 -6,-10q0,-10 6,-10q3.14999,0 4.85001,5l5.14999,0q2,0 2,-2l0,-23.3q0,-4.7 -4.69998,-4.7l-375.60002,0q-4.7,0 -4.7,4.7"/>';
        }else{
            if(renglonesMaximo === 4){//SVG Izquierda de 4 renglones                                
                nuevoSVG += ' d="M 0.27324344,5.415401 0.6986347,78.992984 c 0.0181152,3.133287 1.3965134,6.568833 4.7287417,6.573243 L 403.43785,86.092984 c 3.3322,0.0044 6.89373,-2.533376 6.83467,-5.666217 l -0.56019,-29.711356 c -0.0251,-1.333122 -0.70899,-2 -2.12697,-2 l -5.47686,0 c -1.20529,3.333333 -2.92459,5 -5.15791,5 -4.25392,0 -6.38087,-3.333333 -6.38087,-10 0,-6.666667 2.12695,-10 6.38087,-10 2.23331,0 3.95261,1.666667 5.15791,5 l 5.47686,0 c 1.41798,0 2.12697,-0.666667 2.12697,-2 l 0,-31.3 c 0,-3.1333333 -1.66611,-4.7 -4.9983,-4.7 l -399.4424393,0 c -3.3322315,0 -4.99834726,1.5666667 -4.99834726,4.7"';//nuevoSVG += ' d="m0.27324,4.9154l0.4,73.57758c0.01704,3.13329 1.31316,6.56884 4.44649,6.57325l374.25356,0.52676c3.1333,0.0044 6.48224,-2.53338 6.4267,-5.66621l-0.52673,-29.71135c-0.02362,-1.33313 -0.66669,-2 -2,-2l-5.14996,0c-1.13336,3.33333 -2.75003,5 -4.85004,5c-4,0 -6,-3.33334 -6,-10c0,-6.66667 2,-10 6,-10c2.10001,0 3.71667,1.66666 4.85004,5l5.14996,0c1.33331,0 2,-0.66667 2,-2l0,-31.3c0,-3.13334 -1.56665,-4.7 -4.69995,-4.7l-375.60005,0c-3.13333,0 -4.7,1.56666 -4.7,4.7"/>';
            }
            if(renglonesMaximo === 5){//SVG Izquierda de 5 renglones                
                nuevoSVG += ' d="m 0.27324344,5.415401 0.42539154,91.177583 c 0.0146184,3.133302 1.39653182,5.762136 4.72874462,5.773246 l 398.0107404,1.32675 c 3.33218,0.0111 6.8812,-2.13318 6.83467,-5.266213 l -0.56021,-37.711356 c -0.0198,-1.333203 -0.70897,-2 -2.12695,-2 l -5.47688,0 c -1.20528,3.333333 -2.92458,5 -5.1579,5 -4.25392,0 -6.38087,-3.333333 -6.38087,-10 0,-6.666667 2.12695,-10 6.38087,-10 2.23331,0 3.95261,1.666667 5.1579,5 l 5.47688,0 c 1.41798,0 2.12695,-0.666667 2.12695,-2 l 0,-41.3 c 0,-3.1333333 -1.66609,-4.7 -4.9983,-4.7 l -399.4426861,0 c -3.3322336,0 -4.99835046,1.5666667 -4.99835046,4.7"';
            }
            if(renglonesMaximo === 6){//SVG Izquierda de 6 renglones 
                nuevoSVG += ' d="M 0.27324344,5.0491436 0.69863516,117.02674 c 0.0119031,3.13331 1.39651704,4.97889 4.72874674,4.97325 L 403.4383,121.32674 c 3.33219,-0.006 6.87085,-1.33306 6.83467,-4.46621 l -0.5602,-48.511377 c -0.0154,-1.333254 -0.70899,-2 -2.12696,-2 l -5.47687,0 c -1.20529,3.333333 -2.92459,5 -5.15792,5 -4.25392,0 -6.38088,-3.333333 -6.38088,-10 0,-6.666667 2.12696,-10 6.38088,-10 2.23332,0 3.95262,1.666667 5.15792,5 l 5.47687,0 c 1.41797,0 2.12696,-0.666667 2.12696,-2 l 0,-49.2999994 c 0,-3.1333333 -1.6661,-4.69999998 -4.9983,-4.69999998 l -399.4428739,0 c -3.3322351,0 -4.99835266,1.56666668 -4.99835266,4.69999998"';
            }
        }
    }
    nuevoSVG += '</svg>';
    return nuevoSVG;
}
function obtieneInterlineado(numeroMaximo) {
    var interlineado;
    if(numeroMaximo === 1){
        interlineado = 78;
    }else{
        if(numeroMaximo === 2){
            interlineado = 78;
        }else{
            if(numeroMaximo === 3){
                interlineado = 78;//95  100
            }else{
                if(numeroMaximo === 4){
                    interlineado = 93;//110
                }else{
                    if(numeroMaximo === 5){
                        interlineado = 105;//111 122
                    }else{
                        if(numeroMaximo === 6){                            
                            interlineado = 128;//142
                        }
                    }
                }
            }
        }
    }
    
    
    
    
    return interlineado;
}
function obtieneSeparacion(numeroMaximo) {
    var valor;
    if(numeroMaximo <= 3){
        valor = 74;
    }else{
        if(numeroMaximo === 4){
            valor = 88;
        }else{
            if(numeroMaximo === 5){
                valor = 104;
            }else{
                if(numeroMaximo === 6){
                    valor = 122;
                }
            }
        }
    }
    return valor;
}
function alCrearPieza(event, ui) {
    var cualCaja = event.target.id;
    var id = parseInt( cualCaja.replace('cajaIzquierda', '').replace('cajaDerecha', '').replace('Vacia', '') );
    var posicionX;
    var interlineadoPiezas = obtieneInterlineado(renglonesMaximo);
    var ajuste = id * interlineadoPiezas;//Verificar el ajuste, no debería aplicarse
    var posicionTopInicial = $( '#'+cualCaja ).position().top + ajuste;    
    separacion = obtieneSeparacion(renglonesMaximo);    
    posicionesTopIniciales_arr.push(posicionTopInicial);
    if(cualCaja.substring(0,5) === 'cajaD'){
        posicionX = posLeftInicialDerecha;
    }else{
        posicionX = posLeftInicialIzquierda;
    }
        
    $( '#'+cualCaja ).css({left:posicionX, top:posicionTopInicial, position:'absolute'});
    actualizaPosiciones ( '#'+cualCaja, $( '#'+cualCaja ).position().left, posicionTopInicial );        
}
function actualizaPosiciones (elemento, valorLeft, valorTop) {
    $( elemento ).data({
        'originalLeft': valorLeft,
        'originalTop': valorTop
    });
}
function reseteaPosiciones (elemento) {    
    $( elemento ).css({
        'left': $( elemento ).data('originalLeft'),
        'top':  $( elemento ).data('originalTop')
    });
}
function detienePieza(event, ui) {
    var elementoArrastrado = event.target.id;
    var elementoPar2;
    var vacio;
    var valorLeft;
    var valorTop;
    var pareja;
    var vacioTemporal;    
    
    if( hizoDrop === false && $('#'+elementoArrastrado ).attr('ligados') !== ''){//Entra si hay que desligar
        console.log('IF de ligados');
        elementoPar2  = $( '#'+elementoArrastrado ).attr('ligados');//Obtiene los dos elementos ligados
                                
        if(elementoArrastrado.substring(0,5) === 'cajaD'){
            console.log('**** if');
            elementoPar2 = $( '#'+elementoArrastrado ).attr('haceLinea1');
            pareja = elementoPar2;
            pareja = 'cajaIzquierda'+pareja;
            reseteaPosiciones( '#cajaIzquierda'+elementoPar2 );//Regresa la Caja CONTRARIA liberada 2 (izq o der) a su posición inicial al ser desligada
            $( '#cajaIzquierda'+elementoPar2 ).attr('ligados', '');//Se limpian los ligados
            reseteaSombra( '#cajaIzquierda'+elementoPar2 );
            
            vacioTemporal = '#cajaIzquierda'+elementoPar2;
            vacioTemporal = vacioTemporal.replace('Izquierda', 'IzquierdaVacia');
            mostrarPieza( vacioTemporal );
        }else{
            console.log('**** else');
            elementoPar2 = $( '#'+elementoArrastrado ).attr('haceLinea2');
            pareja = elementoPar2;
            pareja = 'cajaDerecha'+pareja;
            console.log('la caja contraria es: '+'#cajaDerecha'+elementoPar2);
            reseteaPosiciones( '#cajaDerecha'+elementoPar2 );//Regresa la Caja CONTRARIA liberada 2 (izq o der) a su posición inicial al ser desligada - originalLeft - originalTop
            $( '#cajaDerecha'+elementoPar2 ).attr('ligados', '');//Se limpian los ligados
            reseteaSombra( '#cajaDerecha'+elementoPar2 );
            
            vacioTemporal = '#cajaDerecha'+elementoPar2;
            vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia');
            mostrarPieza( vacioTemporal );
        }
        reseteaPosiciones( '#'+elementoArrastrado );//Regresa la Caja DRAGGABLE liberada 1 (izq o der) a su posición inicial al ser desligado
        $( '#'+elementoArrastrado ).draggable( 'option', 'revert', true );//Regresa el reverse de la caja DRAGGABLE
        $( '#'+pareja).draggable( 'option', 'revert', true );//Elimina el reverse de la caja DROPPABLE
        $( '#'+elementoArrastrado ).attr('ligados', '');//Se limpian los ligados
        reseteaSombra( '#'+elementoArrastrado );
        
        vacioTemporal = '#'+elementoArrastrado;
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        mostrarPieza( vacioTemporal );
        
        if(datosRelacionaArrastra.length > 0){//Si hay datos guardados (status interrumpido)        
        }                                
    }    
    if(activaGuardado){
        console.log('llama a guardaStatusInterrumpido dentro de STOP si está activado');
        guardaStatusInterrumpido();
        activaGuardado = false;
    }    
}
function guardaStatusInterrumpido() {    
    var status_arr = new Array();
    var datos;
    var lado1;
    var lado2;
    var unido = 0;    
    var t=0;
    
    posicionesTopIniciales_arr = posicionesTopIniciales_arr.unique();
    
    for ( var i=0; i<preguntasArr.length; i++ ) {
        unido = 0;        
        for (var t=0; t<posicionesTopIniciales_arr.length; t++ ) {                        
            if( $( '#cajaIzquierda'+t ).data('originalTop') === posicionesTopIniciales_arr[i]){                
                lado1 = t; 
                if( $( '#cajaIzquierda'+t ).position().left === posXUnidaIzquierda){                
                    unido = 1;                
                }                
            }            
            if( $( '#cajaDerecha'+t ).data('originalTop') === posicionesTopIniciales_arr[i]){                
                lado2 = t;                
            }
        }
        datos = lado1 + '-' + lado2 + '|' + unido;        
        status_arr[i] = datos;
    }
    console.log('GUARDAR: '+status_arr);
}
function iniciaPieza(event, ui) {    
    hizoDrop = false;
}
function sobrePieza (event, ui) {    
    var cadena1 = event.target.id;
    var cadena2 = ui.draggable.attr('id');
    cadena1 = cadena1.substring(0,5);
    cadena2 = cadena2.substring(0,5);
    
    if(cadena1 !== cadena2){//Si hace over sobre las piezas de la otra columna
        var cajaAPintar = event.target.id;
        cajaAPintar = cajaAPintar.replace("Vacia", "");//Soluciona problema de espacio en blanco
        pintaPieza( "#"+cajaAPintar, colorSobrePieza );
    }
}
function fueraPieza (event, ui) {    
    var cajaAPintar = event.target.id;
    cajaAPintar = cajaAPintar.replace("Vacia", "");//Soluciona problema de espacio en blanco
    pintaPieza( "#"+cajaAPintar, colorNormalPieza );
}
function pintaPieza ( pieza, color ) {
    $( pieza ).contents().find("path").attr({"fill":color});
}
function transparentaPieza ( pieza ) {
    $( pieza ).css('background', 'transparent');
    $( pieza ).css('border-color', 'transparent');
}
function opacarPieza ( pieza ) {
    $( pieza ).css('opacity', '0');
}
function mostrarPieza ( pieza ) {
    $( pieza ).css('opacity', '1');
}
function posicionaPieza ( pieza, left ) {
    $( pieza ).css('left', left);
}
function anchoPieza ( pieza, ancho ) {
    $( pieza ).css('width', ancho);
}
function zIndexPieza ( pieza, zindex ) {
    $( pieza ).css('z-index', zindex);
}
function aplicaSombra ( elemento ) {
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        //No aplica la sombra
    }else{
        $( elemento ).removeClass('shadow');//Aplica la sombra
        $( elemento ).addClass('shadow2');
    }
}
function reseteaSombra ( elemento ) {
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        //No aplica la sombra
    }else{
        $( elemento ).removeClass('shadow2');
        $( elemento ).addClass('shadow');
    }
}
function sueltaPiezaArrastrar ( event, ui ) {
    var cadena;
    var valor1;
    var valor2;
    var numeroId = 0;
    var cajaId = '';
    var cadena1 = event.target.id;
    var cadena2 = ui.draggable.attr('id');
    var droppableVacio2 = event.target.id;
    var datoFila1;
    var datoFila2;
    var cajacontigua;
    var cajaContraria;
    var cajaDroppada;
    var cajaVaciaDesplazada;
    var datoContiguo;
    var cajaADesplazar;
    var cajaUnida;
    var idAcomparar;
    var vacioTemporal;
        
    cadena1 = cadena1.replace('Vacia', '');
    console.log('sueltaPiezaArrastrar: draggable: '+cadena2+' VS droppable: '+cadena1);
    
    if(cadena1.substring(0,5) === cadena2.substring(0,5)){//Si hace drop en la misma columna
        console.log('columna igual.... no hace nada');//No hace nada
        hizoDrop = false;//Seguimiento
        if($( "#"+cadena1 ).hasClass('shadow2')){//Si ya era una caja resuelta
            //Hay que posicionar la caja a los valores iniciales
        }
    }else{//Si hace drop en una columna diferente
        console.log('columna diferente... verifica el drop');
        
        if(cadena1.substring(0,5) === 'cajaD'){
            valor1 = posXUnidaIzquierda+'px';     valor2 = posXUnidaDerecha+'px';
            zIndexPieza ( '#'+cadena1, '3' );
            zIndexPieza ( '#'+cadena2, '4' );
            cajaId = 'cajaIzquierda';
            numeroId = cadena1.substring(11,cadena1.length);
            console.log('************* numeroId: '+numeroId);
            numeroId = +$( '#'+cadena1 ).attr('haceLinea1');
            console.log('************* id arreglado: '+numeroId);
        }else{
            valor1 = posXUnidaDerecha+'px';    valor2 = posXUnidaIzquierda+'px';
            zIndexPieza ( '#'+cadena1, '4' );
            zIndexPieza ( '#'+cadena2, '3' );
            cajaId = 'cajaDerecha';
            numeroId = cadena1.substring(13,cadena1.length);
            console.log('************* numeroId: '+numeroId);
            numeroId = +$( '#'+cadena1 ).attr('haceLinea2');
            console.log('************* id arreglado: '+numeroId);
        }
        
        ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );//Posiciona el DRAGGABLE en el centro y elimina su reverse.
        ui.draggable.draggable( 'option', 'revert', false );//Elimina el reverse de la caja DRAGGABLE
        $( '#'+cadena1).draggable( 'option', 'revert', false );//Elimina el reverse de la caja DROPPABLE
        
        datoFila1 = ui.draggable.attr('cualFila');
        datoFila2 = $( '#'+cadena1).attr('cualFila');
        ui.draggable.attr('ligados', datoFila1+'-'+datoFila2);
        $( '#'+cadena1).attr('ligados', datoFila2+'-'+datoFila1);
        
        console.log('SETEA CAJA DESPLAZADA: '+'#'+cajaId+numeroId+' CON LA FILA: '+datoFila1);
        $( '#'+cajaId+numeroId).attr('cualFila', datoFila1);//Setea caja desplazada con el id del draggable
                        
        datoFila1 = numeroId;//Actualiza la fila actual a la que se mueve la caja
        console.log('SETEAR CAJA DRAGGABLE CON LA FILA: '+datoFila1);
        ui.draggable.attr('cualFila', datoFila1);
            
        cajaDroppada = droppableVacio2.replace('Vacia', '').replace('Vacia', '');
        var cajaDraggadaVacia = ui.draggable.attr('id');
        cajaDraggadaVacia = cajaDraggadaVacia.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
            
        //SE POSICIONAN LAS PIEZAS 
            
        //C A J A    V E R D E    D E S P L A Z A D A  ( POSICIONAMIENTO )
        var reemplazadaActualizadaLeft = $( '#'+cajaId+numeroId ).data('originalLeft');
        var reemplazadaActualizadaTop = $( '#'+ui.draggable.attr('id') ).data('originalTop');
        //console.log('>>>>>> desplazado: '+'#'+cajaId+numeroId + ' a donde se mueve: '+'#'+ui.draggable.attr('id')+' TOP '+ $( '#'+ui.draggable.attr('id') ).position().top  + ' originalTop: '+reemplazadaActualizadaTop);
        $( '#'+cajaId+numeroId ).css({left:reemplazadaActualizadaLeft, top:reemplazadaActualizadaTop, position:'absolute'});//Posiciona la caja verde DESPLAZADA
            
        //C A J A    G R I S     D E S P L A Z A D A   V A C Í A ( POSICIONAMIENTO )
        //console.log('>>>>>> desplazado vacio: '+'#'+cajaId+'Vacia'+numeroId + ' a donde se mueve: '+'#'+ui.draggable.attr('id')+' TOP '+ $( '#'+ui.draggable.attr('id') ).position().top  + ' originalTop: '+reemplazadaActualizadaTop);
        $( '#'+cajaId+'Vacia'+numeroId ).css({left:reemplazadaActualizadaLeft, top:reemplazadaActualizadaTop, position:'absolute'});//Posiciona la caja gris DESPLAZADA vacía
        //console.log(' POSICIONA VACÍA: '+'#'+cajaId+'Vacia'+numeroId);
        
        //C A J A    V E R D E    D R A G G A B LE ( POSICIONAMIENTO )
        var draggadaActualizadaLeft = $( '#'+ui.draggable.attr('id') ).data('originalLeft');
        var draggadaActualizadaTop = $( '#'+cajaDroppada ).data('originalTop');
        //console.log('>>>>>> dragueada: '+'#'+ui.draggable.attr('id') + ' a donde se mueve: '+'#'+cajaDroppada+' TOP '+ $( '#'+cajaDroppada ).position().top  + ' originalTop: '+draggadaActualizadaTop);
        $( '#'+ui.draggable.attr('id') ).css({left:draggadaActualizadaLeft, top:draggadaActualizadaTop, position:'absolute'});//Posiciona la caja DRAGGABLE
            
        //C A J A    G R I S    D R A G G A B LE ( POSICIONAMIENTO )
        var draggadaActualizadaLeft = $( '#'+ui.draggable.attr('id') ).data('originalLeft');
        var draggadaActualizadaTop = $( '#'+cajaDroppada ).data('originalTop');
        console.log('>>>>>> dragueada vacía: '+'#'+cajaDraggadaVacia + ' a donde se mueve: '+'#'+cajaDroppada+' TOP '+ $( '#'+cajaDroppada ).position().top  + ' originalTop: '+draggadaActualizadaTop);
        $( '#'+cajaDraggadaVacia ).css({left:draggadaActualizadaLeft, top:draggadaActualizadaTop, position:'absolute'});//Posiciona la caja DRAGGABLE
        
        //Almacena elementos que hacen línea
        console.log('Obtiene caja contraria '+droppableVacio2);
        if(droppableVacio2.substring(0,5) === 'cajaD'){//Si es de Izquierda a Derecha
            console.log('IF');
            cajaUnida = '#cajaDerecha'+ui.draggable.attr('haceLinea2');//Obtiene la caja que se queda unida al mover su elemento ligado
                
            idAcomparar = ui.draggable.attr('haceLinea2');
            posicionaPieza( '#cajaDerecha'+idAcomparar, $( '#cajaDerecha'+idAcomparar ).data('originalLeft') );//POSICIONA EN X EL DROPPABLE
                
            $( '#'+cajaId+numeroId).attr('haceLinea1', numeroId);//Almacena en la caja DESPLAZADA los elementos que hacen línea
            datoContiguo = $( '#'+ ui.draggable.attr('id') ).attr('haceLinea2');
            $( '#'+cajaId+numeroId).attr('haceLinea2', datoContiguo);//Almacena en la caja DESPLAZADA los elementos que hacen línea
                
            datoContiguo = $( ui.draggable ).attr('haceLinea1');
            $( '#'+cajaDroppada ).attr('haceLinea1', datoContiguo);//Almacena en la caja DROPPADA los elementos que hacen línea
                
            $( ui.draggable ).attr('haceLinea1', ui.draggable.data('number'));//Almacena en el DRAGGABLE los elementos que hacen línea
            datoContiguo = $( '#'+cajaDroppada ).attr('haceLinea2');
            $( ui.draggable ).attr('haceLinea2', datoContiguo);//Almacena en el DRAGGABLE los elementos que hacen línea
                
            cajaVaciaDesplazada = ui.draggable.attr('id');
            cajaVaciaDesplazada = cajaVaciaDesplazada.replace('Izquierda', 'IzquierdaVacia').replace('Derecha', 'DerechaVacia');
            //console.log('cajaVaciaDesplazada: '+'#'+cajaVaciaDesplazada);
                
            //console.log('--- Posiciona Caja desplazada '+'#'+cajaId+numeroId);
            cajaADesplazar = $( '#'+cajaId+numeroId ).attr('haceLinea1');
            //console.log('cajaADesplazar '+cajaADesplazar);
                
            cajacontigua = $( '#'+cajaId+numeroId ).attr('haceLinea2');
            cajaContraria = 'cajaDerecha'+cajacontigua;
            $( '#'+cajaContraria ).attr('haceLinea1', cajaADesplazar);//Almacena en la caja CONTIGUA los elementos que hacen línea
        }else{//Si es de Derecha a Izquierda
            console.log('ELSE');
            cajaUnida = '#cajaDerecha'+ui.draggable.attr('haceLinea1');//Obtiene la caja que se queda unida al mover su elemento ligado
              
            idAcomparar = ui.draggable.attr('haceLinea1');
            posicionaPieza( '#cajaIzquierda'+idAcomparar, $( '#cajaIzquierda'+idAcomparar ).data('originalLeft') );//POSICIONA EN X EL DROPPABLE
                
            $( '#'+cajaId+numeroId).attr('haceLinea2', numeroId);//Almacena en la caja DESPLAZADA los elementos que hacen línea
            datoContiguo = $( '#'+ ui.draggable.attr('id') ).attr('haceLinea1');
            $( '#'+cajaId+numeroId).attr('haceLinea1', datoContiguo);//Almacena en la caja DESPLAZADA los elementos que hacen línea
                
            datoContiguo = $( ui.draggable ).attr('haceLinea2');
            $( '#'+cajaDroppada ).attr('haceLinea2', datoContiguo);//Almacena en la caja DROPPADA los elementos que hacen línea
                
            $( ui.draggable ).attr('haceLinea2', ui.draggable.data('number'));//Almacena en el DRAGGABLE los elementos que hacen línea
            datoContiguo = $( '#'+cajaDroppada ).attr('haceLinea1');
            $( ui.draggable ).attr('haceLinea1', datoContiguo);//Almacena en el DRAGGABLE los elementos que hacen línea
                
            cajaVaciaDesplazada = ui.draggable.attr('id');
            cajaVaciaDesplazada = cajaVaciaDesplazada.replace('Izquierda', 'IzquierdaVacia').replace('Derecha', 'DerechaVacia');
            //console.log('cajaVaciaDesplazada: '+'#'+cajaVaciaDesplazada);
                
            //console.log('--- Posiciona Caja desplazada '+'#'+cajaId+numeroId);
            cajaADesplazar = $( '#'+cajaId+numeroId ).attr('haceLinea2');
            //console.log('cajaADesplazar '+cajaADesplazar);
                                                                                
            cajacontigua = $( '#'+cajaId+numeroId ).attr('haceLinea1');
            cajaContraria = 'cajaIzquierda'+cajacontigua;
            $( '#'+cajaContraria ).attr('haceLinea2', cajaADesplazar);//Almacena en la caja CONTIGUA los elementos que hacen línea
        }
            
        // ************  SE ALMACENAN LOS DATOS **************************
        //C A J A    V E R D E    D E S P L A Z A D A  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+cajaId+numeroId , reemplazadaActualizadaLeft, reemplazadaActualizadaTop);//Actualiza datos de la caja verde desplazada
            
        //C A J A    G R I S     D E S P L A Z A D A  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+cajaId+'Vacia'+numeroId , reemplazadaActualizadaLeft, reemplazadaActualizadaTop);//Actualiza datos de la caja gris desplazada
            
        //C A J A    V E R D E    A R R A S T R A D A  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+ui.draggable.attr('id') , draggadaActualizadaLeft,  draggadaActualizadaTop);//Actualiza datos de la caja verde arrastrada
            
        //C A J A    G R I S    D R A G G A B LE  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+cajaDraggadaVacia , draggadaActualizadaLeft,  draggadaActualizadaTop);//Actualiza datos de la caja gris arrastrada
        
        posicionaPieza( '#'+ui.draggable.attr('id'), valor1 );//POSICIONA EN X EL DRAGGABLE
        posicionaPieza( '#'+cadena1, valor2 );//POSICIONA EN X EL DROPPABLE
        aplicaSombra( '#'+ui.draggable.attr('id') );//APLICA LA SOMBRA FUSIONABLE EN EL DRAGGABLE
        aplicaSombra( '#'+cadena1 );//APLICA LA SOMBRA FUSIONABLE EN EL DROPPABLE
        
        vacioTemporal = ui.draggable.attr('id');
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        opacarPieza( '#'+vacioTemporal );
        
        vacioTemporal = cadena1, valor2;
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        opacarPieza( '#'+vacioTemporal );
        
        pintaPieza( "#"+cadena1, colorNormalPieza );//PINTA EN VERDE NORMAL EL DROPPABLE
        hizoDrop = true;                        
        activaGuardado = true;        
    }
}
var activaGuardado = false;
function muevePieza (event, ui) {
    /*var elementoArrastrado = event.target.id;
    var hit;
    for(var f=0;f<preguntasArr.length;f++){
        hit = $( '#'+elementoArrastrado ).hitTestObject($( '#svgDerecha'+f ));
        if(hit){
            pintaPieza( "#cajaDerecha"+f, colorSobrePieza );
        }else{
            pintaPieza( "#cajaDerecha"+f, colorNormalPieza );
        }
    }*/
}
function calcularLineas($element) {
    var htmlOriginal = $element.html();
    var palabras = htmlOriginal.split(" ");
    var posicionesDeLineas_arr = [];

    for (var i in palabras) {
        palabras[i] = "<span>" + palabras[i] + "</span>";
    }
    $element.html(palabras.join(" "));
    $element.children("span").each(function() {
        var posicionTemp = $(this).position().top;
        if (posicionesDeLineas_arr.indexOf(posicionTemp) == -1) posicionesDeLineas_arr.push(posicionTemp);
    });
    return posicionesDeLineas_arr.length;
}
function centrarTextosEnAltura (idElemento) {
    var alturaSVGcontenedor;
    var posY;
    var posYnumeracion;
    var cuantasLineas;
    var idSVGcontenedor = idElemento;
    var idSVGnumerador = idElemento;
    idSVGcontenedor = idSVGcontenedor.replace('#parrafoIzquierdaTemp', '#svgIzquierda').replace('#parrafoDerechaTemp', '#svgDerecha').replace('#parrafoIzquierda', '#svgIzquierda').replace('#parrafoDerecha', '#svgDerecha');
    idSVGnumerador = idSVGnumerador.replace('#parrafoIzquierdaTemp', '#svgNumeracionIzquierda').replace('#parrafoDerechaTemp', '#svgNumeracionDerecha').replace('#parrafoIzquierda', '#svgNumeracionIzquierda').replace('#parrafoDerecha', '#svgNumeracionDerecha');
    
    alturaSVGcontenedor = $( idSVGcontenedor ).height();
    cuantasLineas = calcularLineas( $( idElemento ) );
    renglones_arr[posRenglones] = cuantasLineas;
    posRenglones++;
    
    if( alturaSVGcontenedor === null){
    }else{
        if(renglonesMaximo <= 3){
            posY = 36 - (cuantasLineas*10);
            posYnumeracion = 0;
        }else{
            alturaSVGcontenedor = renglonesMaximo * 20;
            posY = alturaSVGcontenedor / 2;
            posYnumeracion = (alturaSVGcontenedor - 50) / 4;
            posY = posY - (10 * cuantasLineas);
        }
                
        $( idElemento ).css('padding', posY+'px 2.1% 1px 0%');//Centra verticalmente los textos de contenido
        $( idSVGnumerador ).css('padding', posYnumeracion+'px 2.1% 1px 0%');//Centra verticalmente
    }
}
function eliminaSVGtexto (texto, elemento) {    
    if(texto.indexOf('<img', 0) !== -1){//Si el contenido de la izquierda o derecha trae una imagen
        $( elemento ).remove();//Elimina el svg del texto
        //$( elemento ).text('No acepta imágenes este tipo de actividad');
    }
}
function eliminaBordesBlancos (elemento) {
    $( elemento ).css('border', 'none');//Se eliminan los bordes blancos
}
function ajustaAnchoMinimo ( elemento ) {
    $( elemento ).css('min-width', '322px');//Ajusta el ancho mínimo
}
function eliminaFondo ( elemento ) {
    $( elemento ).css('background', 'none');//Ajusta el ancho mínimo
}
function esMozillaCanonical (  ) {
    var canonical = false;
    navegador=get_browser();
    navegador_version=get_browser_version();
    versionInt = parseInt(navegador_version);
        
    if(navegador === 'Firefox'){
        if(versionInt <= 32){
            canonical = true;
        }
    }
    return canonical;
}
$.fn.hitTestObject = function(selector){
    var compares = $(selector);
    var l = this.size();
    var m = compares.size();
    for(var i = 0; i < l; i++){
        var bounds = this.get(i).getBoundingClientRect();
        for(var j = 0; j < m; j++){
            var compare = compares.get(j).getBoundingClientRect();
            if(!(bounds.right < compare.left ||
                bounds.left > compare.right ||
                bounds.bottom < compare.top ||
                bounds.top > compare.bottom)){
        	return true;
            }
        }
    }
    return false;
};
Array.prototype.shuffle = function() {
    for ( var i = this.length-1; i > 0; i-- ) {
        var j = Math.floor( i * Math.random() );
        var tmp = this[ j ];
        this[ j ] = this[ i ];
        this[ i ] = tmp;
    }
    return this;
}    
function creaRelacionaArrastrar () {
    var texto = '';
    var texto2 = '';
    var dimensiones = new Object();
    var alturaActual;
            
    //datosRelacionaArrastra = new Array('2-0|0','1-1|0','0-2|1','3-3|0');//Recibe los datos del status interrumpido
    //datosRelacionaArrastra = new Array('2-0|0','0-1|1','1-2|0','3-3|1');//Recibe los datos del status interrumpido
    //datosRelacionaArrastra = new Array('0-0|1','3-1|1','1-2|0','2-3|1');//Recibe los datos del status interrumpido    
        
    $('#pilaDeCartasExtra').html( '' );
    $('#pilaDeCartas').html( '' );
    zIndexPieza( '#pilaDeCartas', '4' );
    zIndexPieza( '#pilaDeCartasExtra', '3' );        
    $('#pilaDeCartasExtra').css('display', 'block');    
    
    posicionesTopIniciales_arr = new Array();
    
    //Revuelve ambos arreglos para que aparezca de manera aleatoria
    preguntasArr.shuffle();
    respuestasArr.shuffle();
    
    for ( var i=0; i<preguntasArr.length; i++ ) {//Obtiene el número mayor de renglones
        texto = sanitizarTexto(preguntasArr[i]);
        texto2 = sanitizarTexto(respuestasArr[i]);  
        
        if(texto.indexOf('<img', 0) !== -1 || texto2.indexOf('<img', 0) !== -1){//Si al menos una respuesta trae una imagen            
            renglonesMaximo = 6;//Se setean siempre 6 renglones como contenedor máximo cuando se cargue al menos una imagen
        }
        
        generaBotonDraggableTemporal('cajaTempIzquierda'+i,i);
        generaBotonDraggableTemporal('cajaTempDerecha'+i,i);
        creaTextos('#svgcajaTempIzquierda'+i, texto, i, '', 'parrafoIzquierdaTemp'+i);
        creaTextos('#svgcajaTempDerecha'+i, texto2,i, '', 'parrafoDerechaTemp'+i);
        centrarTextosEnAltura( '#parrafoIzquierdaTemp'+i);
        centrarTextosEnAltura( '#parrafoDerechaTemp'+i);                
    }
    
    renglonesMaximo = Math.max.apply( Math, renglones_arr );
    $( "#pilaDeCartasTemporal" ).remove();
    
    for ( var i=0; i<preguntasArr.length; i++ ) {
        texto = sanitizarTexto(preguntasArr[i]);
        texto2 = sanitizarTexto(respuestasArr[i]);        
        
        if(texto.indexOf('<img', 0) !== -1 || texto2.indexOf('<img', 0) !== -1){//Si al menos una respuesta trae una imagen
            renglonesMaximo = 6;//Se setean siempre 6 renglones como contenedor máximo cuando se cargue al menos una imagen
        }
        texto = texto.replace('<div', '<div style="line-height: '+alturaTexto+'px; height: '+alturaTexto+'px; vertical-align: middle;" ');
        
        generaBotonDroppable('cajaIzquierdaVacia'+i, i);
        generaBotonDroppable('cajaDerechaVacia'+i, i);
        generaBotonDraggable('cajaIzquierda'+i, i);
        generaBotonDraggable('cajaDerecha'+i, i);
        
        anchoPieza( '#cajaIzquierda'+i, '45%' );//42%
        anchoPieza( '#cajaDerecha'+i, '48%' );//42%
        anchoPieza( '#cajaIzquierdaVacia'+i, '45%' );//42%
        anchoPieza( '#cajaDerechaVacia'+i, '48%' );//42%   
        
        creaTextos('#svgIzquierda'+i, texto, i, '', 'parrafoIzquierda'+i);//creaTextos('#svgIzquierdaVacia'+i, 'num'+i, i, '', 'parrafoIzquierda'+i);
        creaTextos('#svgDerecha'+i, texto2,i, '', 'parrafoDerecha'+i);//creaTextos('#svgDerechaVacia'+i, 'num'+i,i, '', 'parrafoDerecha'+i);
        
        centrarTextosEnAltura( '#parrafoIzquierda'+i);
        centrarTextosEnAltura( '#parrafoDerecha'+i);
        eliminaSVGtexto(texto, '#parrafoIzquierda'+i);//Elimina el svg del texto si contiene una imagen
        eliminaSVGtexto(texto2, '#parrafoDerecha'+i);//Elimina el svg del texto si contiene una imagen
                        
        transparentaPieza('#cajaIzquierda'+i);
        transparentaPieza('#cajaDerecha'+i);
        transparentaPieza('#cajaIzquierdaVacia'+i);
        transparentaPieza('#cajaDerechaVacia'+i);
         
        clasesEspeciales(texto, 'parrafoIzquierda'+i, i);
        clasesEspeciales(texto2, 'parrafoDerecha'+i, i);                
        
        dispositivoMovil = esMobile();
        eventoPress = dispositivoMovil ? "touchstart" : "mousedown";
        eventoRelease = dispositivoMovil ? "touchend" : "mouseup";
        eventoMove = dispositivoMovil ? "touchmove" : "mousemove";
        
        //$( '#cajaDerecha'+i ).mousedown(function(event) {
        $( '#cajaDerecha'+i ).bind(eventoPress,function(e){//touchstart ó mousedown sobre cualquier rectángulo de la derecha
            console.log(' ESTE ELEMENTO: '+$( this ).attr('id') + ' LIGA: ' + $( this ).attr('haceLinea1') + ' CON ' + $( this ).attr('haceLinea2'))   //haceLinea
        });
                                    
        //$( '#cajaIzquierda'+i ).mousedown(function(event) {
        $( '#cajaIzquierda'+i ).bind(eventoPress,function(e){//touchstart ó mousedown sobre cualquier rectángulo de la izquierda
            console.log(' ESTE ELEMENTO: '+$( this ).attr('id') + ' LIGA: ' + $( this ).attr('haceLinea1') + ' CON ' + $( this ).attr('haceLinea2'))   //haceLinea
        });
    }
    
    if(datosRelacionaArrastra.length > 0){//Si hay datos guardados (status interrumpido)
        preparaReacomodoDePiezas(datosRelacionaArrastra);
    }
    
    eliminaBordesBlancos( '#div' );
    eliminaBordesBlancos( '#pilaDeCartas div' );
    eliminaBordesBlancos( '#pilaDeCartas' );
    ajustaAnchoMinimo( '#pilaDeCartas div' );
    eliminaFondo( '#div' );
    eliminaFondo( '#pilaDeCartas div' );
    eliminaFondo( '#pilaDeCartas' );
    
    separacionPiezas = 0;
    $( '#pilaDeCartas div' ).css('margin', separacionPiezas+'px');//Separación entre piezas
    $( '#pilaDeCartasExtra div' ).css('margin', separacionPiezas+'px');//Separación entre piezas
                    
    //alturaActual = (alturaTexto * preguntasArr.length) + 160;//Ya no se utiliza este cálculo
    //$('#content').height( alturaActual );            
    var cuantasCajas = preguntasArr.length;
    var alturaSVG = $('#svgIzquierda0').height();    
    var alturaContentAjustada = alturaSVG * cuantasCajas;    
    if(alturaContentAjustada <= 480){
        alturaContentAjustada = 480;
    }
    $('#content').height( alturaContentAjustada );
    
    $('#respuestasRadialGroup').height( $('#content').height() + 80 );// Hack para aumentar el tamaño del div content
        
    for ( var i=0; i<preguntasArr.length; i++ ) {
        $('#rayaBlanca'+i).css('min-width', '0px');
        $('#rayaBlanca'+i).css('border', '1px solid white');
        $('#rayaBlanca'+i).css('width', '39px');
        $('#rayaBlanca'+i).css('height', '0px');
        $('#rayaBlanca'+i).css('margin-left', '193px');
        $('#rayaBlanca'+i).css('margin-top', '-44px');//-24px Sumarle 20
        $('#rayaBlanca'+i).css('margin-right', '0px');
        $('#rayaBlanca'+i).css('margin-bottom', '10px');
        $('#textoEspecial'+i).css('min-width', '94%');
    }
}