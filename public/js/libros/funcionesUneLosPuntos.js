/* _______                   __                   ______               __               
  |   |   |.-----.-----.    |  |.-----.-----.    |   __ \.--.--.-----.|  |_.-----.-----.
  |   |   ||     |  -__|    |  ||  _  |__ --|    |    __/|  |  |     ||   _|  _  |__ --|
  |_______||__|__|_____|    |__||_____|_____|    |___|   |_____|__|__||____|_____|_____|  
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com          
*********************************************************/
var numeroInicial;
var numeroFinal;
var lineaPintada = false;
var cuantosPuntos = 6;
var radio = 7;
var area;
var posXClic1 = 0;
var posYClic1 = 0;
var posXClic2 = 0;
var posYClic2 = 0;
var incremento = 0;
var lineaCreada;
var circuloCreadoIzquierda;
var lineaSVGeliminar;
var objetoActual;

var color = "black";
var anchoStroke = "5";
var tipoPregunta;
var congelaBotonesDerechos = true;
var idBoton1;
var idLinea;
var arrRespuestasLineas = new Object();
var posicionXmouse = 0;
var posicionYmouse = 0;
var areaScrollDerecha = 0;
var areaScrollSuperior = 0;
var direccionTrazado = "";
var desfaseIzquierda;
var desfaseSuperior;
var centroDeCirculo = 0;//Obtiene la posición centro de los puntos           
var arrTmpInit, arrTempFin;
var congelaSonidoIncorrecto = false;
var colorSeleccion = 'black';//'#0066FF';//Color de selección
var colorBien = '#A4CD6D';//Color bien
var presionadoGlobal;
var contornoCirculo = 'black';
var rellenoCirculo = 'black';
var thisIdGlobal;
var idBoton1Global;
var idLineaGlobal;
var evitarPintadoCirculo = false;
var eventoPress;
var eventoRelease;
var eventoMove;
var id_temp = "null";
var dispositivoMovil;

var posicionesX_arr = new Array('468.5','499.5','530.5','602.5','650.5','711.5','706.5','688.5','668.5','586.5','561.5','541.5','472.5','579.5','631.5','573.5','476.5','402.5','498.5','552.5','406.5','228.5','183.5','205.5');
var posicionesY_arr = new Array('385.5','396.5','385.5','413.5','409.5','398.5','379.5','371.5','379.5','358.5','332.5','338.5','318.5','297.5','234.5','199.5','193.5','197.5','216.5','236.5','233.5','286.5','338.5','392.5');

function estructuraHTMLuneLosPuntos(estructuraBotones){
    var estructuraBotones3 = '<svg id="elementosSVG" height="100%" xmlns=svgNS xmlns:xlink=svgNSLink href:"images/une_puntos1.png"></svg>';    
    $("#respuestasRadialGroup").html(estructuraBotones);
    $("#respuestasRadialGroup").append(estructuraBotones3);
    
    var svgImagen = document.createElementNS('http://www.w3.org/2000/svg','image');
    svgImagen.setAttributeNS(null,'width','800');
    svgImagen.setAttributeNS(null,'height','800');    
    svgImagen.setAttributeNS(svgNSLink,'href', 'images/une_puntos1.png');
    svgImagen.setAttributeNS(null,'x','50');
    svgImagen.setAttributeNS(null,'y','-88');    
    $('#elementosSVG').append(svgImagen);    
    $( '#elementosSVG' ).css('width', '890px');
    $( '#elementosSVG' ).css('height', '640px');
                
    preguntaUneLosPuntos();
    //funcionesPosicionMOuse();
}
function funcionesPosicionMOuse() {
    var theThing = document.querySelector("#thing");    
    var container = document.querySelector("#elementosSVG");
    container.addEventListener("click", getClickPosition, false);

    function getClickPosition(e) {
        var parentPosition = getPosition(e.currentTarget);
        var xPosition = e.clientX - parentPosition.x - (theThing.clientWidth / 2);
        var yPosition = e.clientY - parentPosition.y - (theThing.clientHeight / 2);
        console.log('xPosition '+xPosition);
        console.log('yPosition '+yPosition);
        console.log('-------------');
        theThing.style.left = xPosition + "px";
        theThing.style.top = yPosition + "px";
    }
    function getPosition(element) {
        var xPosition = 0;
        var yPosition = 0;
        while (element) {
            xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
            yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
            element = element.offsetParent;
        }
        return { x: xPosition, y: yPosition };
    }
}
function preguntaUneLosPuntos(){    
    //cuantosPuntos = preguntasArr.length;
    cuantosPuntos = 24;//hardcode temp        
    var arrRespuesta = new Array();
    var areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    centroDeCirculo = 0;
    terminoTrazo = false;
    
    for ( j=1, i=0; j<=cuantosPuntos; j++, i++ ) {        
        //CREA CIRCULOS DE LA IZQUIERDA
        var ajustePunto = 33;
        var posicionX_ = parseInt(posicionesX_arr[j-1])+ajustePunto;
        var posicionY_ = parseInt(posicionesY_arr[j-1])+ajustePunto;

        circuloCreadoIzquierda = document.createElementNS(svgNS,"circle");//Se declara la instancia de círculo
        circuloCreadoIzquierda.setAttributeNS(null,"id","i_"+j);
        circuloCreadoIzquierda.setAttributeNS(null,"cx",posicionX_);
        circuloCreadoIzquierda.setAttributeNS(null,"cy",posicionY_); // Se aumenta el margen que se le agrego a cada opcion
        circuloCreadoIzquierda.setAttributeNS(null,"r",radio);
        circuloCreadoIzquierda.setAttributeNS(null, 'style', 'fill: '+rellenoCirculo+'; stroke: '+contornoCirculo+'; str oke-width: 4px;' );
        //circuloCreadoIzquierda.setAttributeNS(null,"fill-opacity",0.6);//Transparencia del punto
        circuloCreadoIzquierda.setAttributeNS(null,"stroke","1");
        circuloCreadoIzquierda.setAttributeNS(null,"cursor","pointer");
        
        dispositivoMovil = esMobile();
        eventoPress = dispositivoMovil ? "touchstart" : "mousedown";
        eventoRelease = dispositivoMovil ? "touchend" : "mouseup";
        eventoMove = dispositivoMovil ? "touchmove" : "mousemove";
        
        document.getElementById(nombreSVG).appendChild(circuloCreadoIzquierda);
                
        $( '#i_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha            
            numeroInicial = this.id;
            numeroInicial = numeroInicial.substring(numeroInicial.indexOf('_',0)+1, numeroInicial.length );                        
            lineaPintada = false;
            iniciarTrazado(e,this.id);
            eventosDeUneLosPuntos(this.id);            
        });
                 
        $( '#i_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha
            numeroFinal = this.id;            
            numeroFinal = numeroFinal.substring(numeroFinal.indexOf('_',0)+1, numeroFinal.length );            
            
            if(parseInt(numeroInicial) === parseInt(numeroFinal)-1 || parseInt(numeroInicial) === parseInt(numeroFinal)+1 ){//Si son puntos contiguos
                console.log('correcto');
                finDeTrazado(e,this.id);
                lineaPintada = true;
                //sndClic.play(); 
                //reproduceSonidoFlash('bien', 'bien_snd', 24);//animacion, sonido, fps. Reproduce el audio "bien_snd.mp3" con el nuevo método.
                 
            }else{//Si no son puntos contiguos
                console.log('incorrecto');
            }                                                                                    
        });    
    }
}
function iniciarTrazado (e,thisId){
    evitarPintadoCirculo = false;
    congelaSonidoIncorrecto = false;
    terminoTrazo = false;
    permiteOver = true;
    incremento = incremento + 1;//Checar si aplica se agregó para que funcione hacer varias lineas ya montado en el proyecto
    congelaBotonesDerechos = false;                                
    objetoActual = document.getElementById(e.target.id);
    
    $ ( '#'+e.target.id ).css('fill', colorSeleccion);
    $ ( '#'+e.target.id ).css('stroke', colorSeleccion);

    arrTmpInit = objetoActual.getAttribute('id').split('_');   
    //arrRespuestasLineas[arrTmpInit[1] - 1] = arrTmpInit[1];
    //objetoActual.getAttribute('id');

    posXClic1 = obtenerDimensiones(objetoActual).x;
    posYClic1 = obtenerDimensiones(objetoActual).y;        
    
    desfaseIzquierda = $("#elementosSVG").offset().left;
    desfaseSuperior = $("#elementosSVG").offset().top;
    areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    centroDeCirculo = parseInt(e.target.getAttribute("r"));//Obtiene la posición centro de los puntos
    posXClic1 = (posXClic1 - desfaseIzquierda) + centroDeCirculo;
    posYClic1 = (posYClic1 - desfaseSuperior) + centroDeCirculo;                                    
    posYClic1 = posYClic1 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic                
    idBoton1 = thisId;         
}
function finDeTrazado (e,thisId){
    terminoTrazo = true;
    evitarPintadoCirculo = true;        
    congelaSonidoIncorrecto = true;

    /*objetoActual = document.getElementById(thisId);
    arrTmpFin = objetoActual.getAttribute('id').split('_');
    arrRespuestasLineas[arrTmpInit[1] - 1]  = {'id': arrTmpInit[1], 'selected': arrTmpFin[1]};
    actualizaArrayRespuesta(arrTmpInit[1] - 1, arrTmpFin[1]); //Comentado para une los puntos     */

    posXClic2 = obtenerDimensiones(objetoActual).x;
    posYClic2 = obtenerDimensiones(objetoActual).y;//Para crear la linea al liberar el mouse sobre circulo de la derecha                                
    var desfaseIzquierda = $("#elementosSVG").offset().left;
    var desfaseSuperior = $("#elementosSVG").offset().top;

    areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    centroDeCirculo = parseInt(e.target.getAttribute("r"));//Obtiene la posición centro de los puntos

    posXClic2 = (posXClic2-desfaseIzquierda) + centroDeCirculo;
    posYClic2 = (posYClic2-desfaseSuperior) + centroDeCirculo;
    posYClic2 = posYClic2 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
    
    crearLaLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, colorBien, anchoStroke);
    //eliminarLaLineaAnterior("lineaCreada");//Comentado para une los puntos
    incremento = incremento + 1;//Checar si se suma 1 o 2
    congelaBotonesDerechos = true;              

    thisIdGlobal = thisId;
    idBoton1Global = idBoton1;
    idLineaGlobal = idLinea;        

    posColores = posColores + 1;
    if(posColores == colores_arr.length){
        posColores = 0;
    }

    congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
    direccionTrazado = "";//Resetea el status de trazado
    permiteOver = false;
    //siguientePregunta( c03id_tipo_pregunta, arrTmpInit[1] );//Al parecer no va a la siguiente pregunta
}
function congelarBotones (){
    $( '#'+thisIdGlobal ).unbind(eventoPress);
    $( '#'+thisIdGlobal ).unbind(eventoRelease);
    $( '#'+idBoton1Global ).unbind(eventoPress);
    $( '#'+idBoton1Global ).unbind(eventoRelease);
    $( '#'+thisIdGlobal ).css({cursor:"default"});
    $( '#'+idBoton1Global ).css({cursor:"default"});
}
function eventosDeUneLosPuntos (id){
    presionadoGlobal = id;        
    $( 'body' ).bind(eventoMove,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha        
        obtienePositionMouse(e, this);
    });
    $(window).scroll(function(event) {        
        if(areaScrollDerecha != $(document).scrollLeft()){
            posicionXmouse -= areaScrollDerecha;
            areaScrollDerecha = $(document).scrollLeft();
            posicionXmouse += areaScrollDerecha;
        }
        if(areaScrollSuperior != $(document).scrollTop()){
            posicionYmouse -= areaScrollSuperior;
            areaScrollSuperior = $(document).scrollTop();
            posicionYmouse += areaScrollSuperior;
        }                    
        procesarTrazado(desfaseIzquierda, desfaseSuperior, centroDeCirculo);
    });
    function obtienePositionMouse(e, _this){        
        if( dispositivoMovil ){//Si es un dispositivo Móvil            
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];                        
            var elm = $(_this).offset();
            var x = touch.pageX - elm.left;
            var y = touch.pageY - elm.top;

            if(x < $(_this).width() && x > 0){
                if(y < $(_this).height() && y > 0){
                    posicionXmouse = touch.pageX;
                    posicionYmouse = touch.pageY;
                }
            }
        }else{//Sino es un dispositivo Móvil
            if (!e) { var e = window.event; }
            if (e.target) {    area = e.target;
            } else if (e.srcElement) {
                area = e.srcElement;
            }
            if (document.layers){
                posicionXmouse = e.x;            posicionYmouse = e.y;
            }else if (document.all){
                posicionXmouse = event.clientX;  posicionYmouse = event.clientY;
            }else if (document.getElementById){
                posicionXmouse = e.clientX;      posicionYmouse = e.clientY;
            }
        }
        
        obtieneElSegundoElemento(posicionXmouse, posicionYmouse);
        procesarTrazado(desfaseIzquierda, desfaseSuperior, centroDeCirculo);
    }
                
    $(document).on(eventoRelease,function(event){//document.onmouseup=function(e){
        eliminarLaLineaActual("lineaCreada");        
        $(document).unbind('mousemove');//Elimina el evento onmousemove
        $( 'body' ).unbind(eventoMove);
        $(window).unbind('scroll');                        
        congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
        direccionTrazado = "";//Resetea el status de trazado
        
        if(evitarPintadoCirculo === false){
            $ ( '#'+presionadoGlobal ).css('stroke', contornoCirculo);
            $ ( '#'+presionadoGlobal ).css('fill', rellenoCirculo);
        }
        
        if(congelaSonidoIncorrecto === false){            
            sndIncorrecto();
            $(document).unbind(eventoRelease);
        }
    });
}
function obtieneElSegundoElemento (){
    if( dispositivoMovil ){
        var lado;
        var endTarget = document.elementFromPoint(
            posicionXmouse,
            posicionYmouse
        );
        id_temp = $(endTarget).attr('id');
        if(id_temp !== 'undefined'){
            if(direccionTrazado === 'izquierda-derecha'){
                lado = 'derecha';
            }else{
                lado = 'izquierda';
            }
            if(id_temp.indexOf('_') !== -1 ){
                var num = parseInt( id_temp.substring( id_temp.indexOf('_', 0)+1, id_temp.length  ) );
                num = num - 1;                
                $('#'+lado+'_'+num).addClass('pintaCuadro');                
            }else{
                for(var d=0;d<preguntasArr.length;d++){
                    $('#'+lado+'_'+d).removeClass('pintaCuadro');
                }
            }
        }
    }
}
function removerLosEventos (){
    document.onmousemove = detenerEventos;
    $(document).unbind('mousemove');//Elimina el evento onmousemove
    $(window).unbind('scroll');
    document.onmouseup = detenerEventos;
    $(document).unbind(eventoRelease);    
    congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
    direccionTrazado = "";//Resetea el status de trazado
}
function procesarTrazado (desfaseIzquierda, desfaseSuperior, centroDeCirculo){
    posXClic2 = posicionXmouse;
    posXClic2 = (posXClic2-desfaseIzquierda) - centroDeCirculo/4;
        
    if(direccionTrazado === 'izquierda-derecha'){
        posXClic2 = posXClic2 - 1;//Se resta un pixel para que el puntero pueda tocar el botón derecho y no obstruya la línea trazada
    }else{        
        posXClic2 = posXClic2 + 1;//Se resta un pixel para que el puntero pueda tocar el botón derecho y no obstruya la línea trazada
    }            
    posYClic2 = posicionYmouse;
    posYClic2 = (posYClic2-desfaseSuperior) - centroDeCirculo/4;
    
    if(navegador === 'Firefox'){
        if(versionInt <= 31){
            posXClic2 = posXClic2 + 2;
            posYClic2 = posYClic2 + 15;
        }
    }        
    crearLaLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, 'black', anchoStroke);//crearLaLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke);
    eliminarLaLineaAnterior("lineaCreada");
}
function eliminarLaLineaActual(identificador){
    if(lineaPintada === false){        
        var d = document.getElementById(nombreSVG);
        lineaSVGeliminar = document.getElementById(identificador+(incremento));//Asigna la linea actual para ser eliminada
        if(lineaSVGeliminar !== null){
            d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
        }
        lineaPintada = true;
    }
}
function eliminarLaLineaAnterior(identificador){    
    var d = document.getElementById(nombreSVG);
    lineaSVGeliminar = document.getElementById(identificador+(incremento-1));//Asigna la linea anterior para ser eliminada    
    try {        
        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
    }
    catch(err) {        
        //document.getElementById("demo").innerHTML = err.message;
    }
}
/**
 * Funcion que actuliza la informacion del array de arrRespuestasLineas,
 * esto se hace porque hay ocasiones donde se borran dos lineas y solo se 
 * actualiza una linea en el array
 * @param {type} indice
 * @param {type} posicionColocada
 * @returns {undefined}
 */
function actualizaArrayRespuesta(indice, posicionColocada){
    var arrResp = Object.keys(arrRespuestasLineas);
    var longResp = arrResp.length;
    for(var i=0; i<longResp; i++){        
        if(parseInt(arrRespuestasLineas[i].selected) === parseInt(posicionColocada)
            && indice !== i){
            arrRespuestasLineas[i] = {'id': 0, 'selected': 0};
        }                
    }
}
function crearLaLinea(identificador, posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke){
    incremento++;
    lineaCreada = document.createElementNS(svgNS,"line");//Se declara la instancia de linea
    lineaCreada.setAttributeNS(null,"id",identificador+incremento);
    lineaCreada.setAttributeNS(null,"x1",posXClic1+4);
    lineaCreada.setAttributeNS(null,"y1",posYClic1+4);
    
    if(terminoTrazo){
       lineaCreada.setAttributeNS(null,"x2",posXClic2+5);//Posición final del trazado (posicion xy del mouse)
       lineaCreada.setAttributeNS(null,"y2",posYClic2+5);
    }else{
      if(direccionTrazado === 'izquierda-derecha'){
          lineaCreada.setAttributeNS(null,"x2",posXClic2-1);//Posición final del trazado (posicion xy del mouse)
          lineaCreada.setAttributeNS(null,"y2",posYClic2-1);//Importante para que no estorbe la linea
      }else{
          lineaCreada.setAttributeNS(null,"x2",posXClic2+5);//Posición final del trazado (posicion xy del mouse)
          lineaCreada.setAttributeNS(null,"y2",posYClic2+5);
      }
    }
           
    lineaCreada.setAttributeNS(null,"stroke",color);
    lineaCreada.setAttributeNS(null,"stroke-width",anchoStroke);
    lineaCreada.setAttributeNS(null,"class", "linea_relacionar");
    idLinea = identificador+incremento;
        
    //document.getElementById(nombreSVG).appendChild(lineaCreada);//Crea una linea nueva
    $('#i_1').before(lineaCreada);//Crea una linea nueva
}
function obtenerDimensiones (oElement) {
    var x, y, w, h;
    x = y = w = h = 0;        
    navegador=get_browser();
    navegador_version=get_browser_version();
    versionInt = parseInt(navegador_version);    
    if (document.getBoxObjectFor) {//Mozilla
        var oBox = document.getBoxObjectFor(oElement);
        x = oBox.x-1;
        w = oBox.width;
        y = oBox.y-1;
        h = oBox.height;
    }  else if ( typeof oElement !== 'undefined'){
        if(oElement.getBoundingClientRect) {//IE
            var oRect = oElement.getBoundingClientRect();
            x = oRect.left-2;
            w = oElement.clientWidth;
            y = oRect.top-2;
            h = oElement.clientHeight;
            if(navegador === 'Firefox'){
                if(versionInt <= 32){
                    x = x + 2;
                    y = y + 15;
                }
            }
        }
    }    
    return {x: x, y: y, w: w, h: h};
}
function detenerEventos(e) {
    if (!e) e = window.event;
    if (e.stopPropagation) {
        e.stopPropagation();
    } else {
        e.cancelBubble = true;
    }
}