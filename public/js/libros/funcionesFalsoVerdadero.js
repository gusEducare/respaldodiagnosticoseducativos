 /*_______ __                             ___ ___                __           __                   
  |    ___|  |.---.-.-----.-----.        |   |   |.-----.----.--|  |.---.-.--|  |.-----.----.-----.
  |    ___|  ||  _  |__ --|  _  |        |   |   ||  -__|   _|  _  ||  _  |  _  ||  -__|   _|  _  |
  |___|   |__||___._|_____|_____|         \_____/ |_____|__| |_____||___._|_____||_____|__| |_____|                                                                           
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com     
*********************************************************/
function estructuraHTMLfalsoVerdadero (estructuraBotones ){    
    estructuraBotones = '<div id="intentalo_div" class="intentaloDiv circuloIntentalo"></div>' + estructuraBotones;        
    $("#respuestasRadialGroup").html(estructuraBotones);    
    var heightFrame=$( window ).height()-1;
    $('#respuestasRadialGroup').append('<div id="capaSuperior" style="display:none"></div>');
    $("#capaSuperior").height(heightFrame);
    circuloSiguiente();
}
function falsoVerdadero (){
    var textoBtn_1=obj.respuestas[0].t13respuesta;
    var textoBtn_2=obj.respuestas[1].t13respuesta;
    var colorFuente = fondoOscuro === 'true' ? '#FFF' : '#000';
    if(obj.pregunta.imagenes){           
        var rutaOjito =  strAmbiente === 'DEV' ?
            '../../Libro/assets/resources/actividades/img/examenes/ojito_2.png' :
                    'img/examenes/ojito_2.png';
        var rutaImg1=$(".opcionImg a img").first().attr("src");
        var rutaImg2=$(".opcionImg a img").last().attr("src");
        $("section").css({width:"90%", height:"auto", margin:"0 auto"});
        $("#preguntaTexto").css({position:"relative", height:"auto"});
        $("input:radio").css({display:"none"});
        $("input:radio").next('label').css({display:"inline-block", 
            background: "#5cc2d7", width:"90px", height:"90px", borderRadius:"50%", 
            cursor: "pointer", border: "7px solid #FFF"});
        $(".columnasImg").css({display:"inline-block", width:"50%", textAlign:"center"});
        $("form").css({display:"initial"});
        $(".opcionImg").css({width:"276px", height:"288px", margin:"0 auto", marginBottom:"20px"});
        $(".numero-preguntaFV").css({display:"none"});
        $(".div-pregunta-FV").css({position:"relative", color:"#FFF"});
        $("#preguntaTexto").css({width:"100%"});
        $(".preguntaTit div h1").css({color:colorFuente, fontSize:"20px", fontWeight:"normal"});
        $(".opcionImg img").css({height:$(".opcionImg").height()});
        $(".swipebox").swipebox();
        $(".opcionImg a").first().attr("href",$(".opcionImg img").first().attr("src"));
        $(".opcionImg a").last().attr("href",$(".opcionImg img").last().attr("src"));
        $(".opcionImg").first().css({backgroundImage:'url('+rutaImg1+')', 
            backgroundSize: "100% 400px", backgroundRepeat:"no-repeat"});
        $(".opcionImg a img").first().attr("src", rutaOjito);
        $(".opcionImg").last().css({backgroundImage:'url('+rutaImg2+')', 
            backgroundSize: "100% 400px", backgroundRepeat:"no-repeat"});
        $(".opcionImg a img").last().attr("src", rutaOjito);
        $('.h1-FV br').remove();
        $('.div-pregunta-FV').css({marginBottom: "0px"});
        $('#preguntaTexto').css({marginBottom: "0px"});
    }else{
        $(".numero-preguntaFV").css({width:"100%", position:"relative", 
            margin:"0 auto"});
        $("input:radio").css({display:"none"});
        $("#respuestasRadialGroup").css({width:"250px", display:"inline-block"});
        $(".pregunta").css({width:"70%", height:$(window).height()-74, margin: "0px", display:"inline-block", cursor:"default"});
        $("form").css({height:$(window).height()-74, margin: "0px", 
            display:"table-cell", verticalAlign:"middle", width:"100%"});
        $("section").css({width:"30%", height:"auto", margin:"0 auto", display:"table"});
        $("input:radio").css({display:"none"});
        if(obj.pregunta.forma==="cuadrado"){
            var topBefore1=55, topBefore2=55; 
            $("input:radio").next('label').css({display:"inline-block", width:"220px", 
            height:"100px", cursor: "pointer", border: "11px solid #FFF", 
            boxShadow: "0px 11px 44px #888888", margin:"0 auto", fontFamily: "FF-Poppins-Semibold"});
            if(textoBtn_1.length>14){
                console.log("2 renglones B1");
                topBefore1=((parseInt($("input:radio").next('label').css("height"))-82)/2)+25;
            }
            if(textoBtn_2.length>14){
                topBefore2=((parseInt($("input:radio").next('label').css("height"))-82)/2)+25;
                console.log("2 renglones B1");
            }
            $('<style>.btn1::before{content:"'+eliminarParrafos(textoBtn_1)+'"; \n\
                width: 100%; top:'+topBefore1+'px;}</style>').appendTo("head");
            $('<style>.btn2::before{content:"'+eliminarParrafos(textoBtn_2)+'"; \n\
                width: 100%; top:'+topBefore2+'px;}</style>').appendTo("head");
            $(".btn1").bind('mousedown', function(){                
                var topBefore2Zoom=topBefore1+5;
                $(this).css({width:"235", height:"115", fontSize:"28px", margin: "-7.5px auto 0px auto"});
                $('<style>.btn1::before{content:"'+eliminarParrafos(textoBtn_1)+'"; \n\
                width: 100%; top:'+topBefore2Zoom+'px;}</style>').appendTo("head");
            });
            $(".btn1").bind('mouseup mouseleave', function(){
                $(this).css({width:"220", height:"100", fontSize:"24px", margin: "0px auto"});
                $('<style>.btn1::before{width: 100%; top:'+topBefore1+'px;}</style>').appendTo("head");
            });
            $(".btn2").bind('mousedown', function(){
                var topBefore2Zoom=topBefore2+5;
                $(this).css({width:"235", height:"115", fontSize:"28px", margin: "-7.5px auto 0px auto"});
                $('<style>.btn2::before{content:"'+eliminarParrafos(textoBtn_2)+'"; \n\
                width: 100%; top:'+topBefore2Zoom+'px;}</style>').appendTo("head");
            });
            $(".btn2").bind('mouseup mouseleave', function(){
                $(this).css({width:"220", height:"100", fontSize:"24px", margin: "0px auto"});
                $('<style>.btn2::before{width: 100%; top:'+topBefore2+'px;}</style>').appendTo("head");
            });
            $(".btn1").css({background:"#9362a4", color:"#FFF", 
                border:"none", fontWeight:"bold"});
            $(".btn2").css({background:"#31bcaa", color:"#FFF", 
                border:"none", fontWeight:"bold"});
            $(".opcionBoton").first().css({marginBottom:"90px", height:"120px"});
            $(".opcionBoton").css({height:"120px"});
            $(".btn2").text("Hola");
        }else if(obj.pregunta.forma==="circulo"){
            var btn1_color, btn2_color;
            var btn1_icono, btn2_icono, btn1_iconoBase, btn2_iconoBase,
                    btn1_iconoClick, btn2_iconoClick;
            var topTexto, topIcono;
            if(obj.pregunta.texto===true){
                if(obj.pregunta.icono===true){
                    btn1_color="#a4cd6d";
                    btn2_color="#f84c56";
                    btn1_icono="verde";
                    btn2_icono="roja";
                }else{
                    btn1_color="#927aa7";
                    btn2_color="#4dc4d5";
                    btn1_icono="morada";
                    btn2_icono="azul";
                }
            }else{
                btn1_color="#927aa7";
                btn2_color="#4dc4d5";
                btn1_icono="morada";
                btn2_icono="azul";
            }

            if(obj.pregunta.icono===true){
                btn1_iconoBase='url("images/opcion_multiple_palomita.png")';
                btn2_iconoBase='url("images/opcion_multiple_tache.png")';
                btn1_iconoClick='url("images/opcion_multiple_palomita_'+btn1_icono+'.png")';
                btn2_iconoClick='url("images/opcion_multiple_tache_'+btn2_icono+'.png")';
                topTexto="130px";
                if(obj.pregunta.texto===true)
                    topIcono="50% 35%";
                else{
                    topIcono="50% 50%"
                }

            }else{
                btn1_iconoBase="none";
                btn2_iconoBase="none"
                topTexto="100px";
                btn1_iconoClick="none";
                btn2_iconoClick="none";
            }
            $("input:radio").next('label').css({display:"inline-block", width:"210px", 
                height:"210px", borderRadius:"50%", cursor: "pointer", border: "11px solid #FFF", 
                boxShadow: "0px 11px 44px #888888", fontFamily: "FF-Poppins-Light"});

            $(".btn1").bind('mousedown', function(){                
                $(this).css({border: '11px solid '+btn1_color+'', background:"#FFF",
                backgroundImage:btn1_iconoClick, 
                backgroundRepeat:"no-repeat", backgroundPosition: topIcono, backgroundSize: "50px 50px"});                  
                $('<style>.btn1::before{color:'+btn1_color+';}</style>').appendTo("head");
            });
            $(".btn2").bind('mousedown',function(){
                $(this).css({border: '11px solid '+btn2_color+'', background:"#FFF", 
                    backgroundImage:btn2_iconoClick, 
                backgroundRepeat:"no-repeat", backgroundPosition: topIcono, backgroundSize: "45px 45px"});                  
                $('<style>.btn2::before{color:'+btn2_color+';}</style>').appendTo("head");
            });
            $(".btn1").bind('mouseup mouseleave',function(){                   
                $(this).css({border: "11px solid #FFF", background:btn1_color, 
                    backgroundImage:btn1_iconoBase, 
                    backgroundRepeat:"no-repeat", backgroundPosition: topIcono, backgroundSize: "50px 50px"});                  
                $('<style>.btn1::before{color:#FFF;}</style>').appendTo("head");
            });
            $(".btn2").bind('mouseup mouseleave',function(){                   
                $(this).css({border: "11px solid #FFF", background:btn2_color,
                    backgroundImage:btn2_iconoBase, 
                    backgroundRepeat:"no-repeat", backgroundPosition: topIcono, backgroundSize: "45px 45px"});   
                $('<style>.btn2::before{color:#FFF;}</style>').appendTo("head");
            });
            if(obj.pregunta.texto===true){
                $('<style>.btn1::before{content:"'+eliminarParrafos(textoBtn_1)+'"; \n\
                    color:#FFF; left:0px; top:'+topTexto+'; width: 100%;}</style>').appendTo("head");
                $('<style>.btn2::before{content:"'+eliminarParrafos(textoBtn_2)+'"; \n\
                    color:#FFF; left:0px; top:'+topTexto+'; width: 100%;}</style>').appendTo("head");
                $(".btn1").css({background:btn1_color});
                $(".btn2").css({background:btn2_color});
            }else{
                $(".btn1").css({background:"#927aa7"});
                $(".btn2").css({background:"#4dc4d5"});
            }

            $(".btn1").css({backgroundImage:btn1_iconoBase,
                backgroundRepeat:"no-repeat", backgroundPosition: topIcono, backgroundSize: "50px 50px"});
            $(".btn2").css({backgroundImage:btn2_iconoBase,
                backgroundRepeat:"no-repeat", backgroundPosition: topIcono, backgroundSize: "45px 45px"});
            $(".opcionBoton").first().css({marginBottom:"6px"});

        }else if(obj.pregunta.forma==="circuloNoTexto"){}

        $(".opcionBoton").css({textAlign:"center"});                                                    
    }
}
function estilosFalsoVerdadero (){
    if(obj.pregunta.imagenes){            
    }else{
        var heightFrame=$( window ).height()-2;            
//        $( '#fondoPregunta' ).css({backgroundImage:'url("images/tramaFoV.png")', backgroundColor:"#f2f2f2", marginTop:"2px", cursor:"default"});
        $( '#respuestasRadialGroup' ).height( heightFrame +'px');
        $( '#respuestasRadialGroup' ).css('margin', '0px');
        $( '#respuestasRadialGroup' ).css('padding', '0px');
    }
    $( '#barraCreada' ).css('margin-left', '0em');
}
