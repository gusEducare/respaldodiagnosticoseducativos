/* _______              __                       _______         __ __   __         __        
  |       |.-----.----.|__|.-----.-----.        |   |   |.--.--.|  |  |_|__|.-----.|  |.-----.
  |   -   ||  _  |  __||  ||  _  |     |        |       ||  |  ||  |   _|  ||  _  ||  ||  -__|
  |_______||   __|____||__||_____|__|__|        |__|_|__||_____||__|____|__||   __||__||_____|
           |__|                                                             |__|              
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com     
*********************************************************/
var respuestasTextoTest=[], contadorPreguntasTest=0;
function estructuraHTMLopcionMultiple (estructuraBotones ){
    $("#respuestasRadialGroup").css({width:'100%'});
    $(".pregunta").css({height:'auto'});
    $("#respuestasRadialGroup").html(estructuraBotones);
}
function banderaMultiple ( ){
    if(resultados[0].respuesta!==undefined){
        $('section').css({marginTop:"25px"});
        $('.btn_preg').css({marginBottom:"20px"});
    }else{        
        $('section').css({marginTop:"45px"});
        $('.btn_preg').css({marginBottom:"45px"});
    }
    $(".iconoEvaluar").css({display:"none"});
    $(".no-margen").css({width:"100%"});
    $(".preguntaTit").children().css({color:"#FFF"});
}
function accionesMultiple ( ){
    $(".contenedor-player, #fondoPregunta").css({height:"100%"});
    $("form").css({width:"100%"});
    if(obj.pregunta.dosColumnas===true && obj.pregunta.preguntasMultiples!==true){
        var alturaDisponible=json.length>1 ? 0 : $(window).height();
        $(".circulo-opcMultiple").css({marginLeft:"1px"});
        $(".pregunta").css({width:"50%"});
        $('#fondoPregunta').children().first().css({height:'100%'});        
        $('.respuesta, .respuesta section').css({height:'100%'});
        $("#respuestasRadialGroup").css({width:"50%", top: "0px"});
        $("#respuestasRadialGroup form").css({height:alturaDisponible, display:"inline-table"});
        $("form ul").css({display:"table-caption", verticalAlign:"top"});
        $(".iconoEvaluar").css({width:"10%"});
        $(".btn_preg").css({padding:"7px 0px 10px 50px"});
    }
}
function estilos2Multiple ( ){
    if (obj.pregunta.t11pregunta.length === 0) {
        $(".pregunta").css("display", "none");
    }
}
function estilos1Multiple ( ){
//    $("#fondoPregunta").css("background", "#f2f2f2");
    $("#respuestasRadialGroup").css("height","auto");
    $('.contenedor-player').append('<div id="capaSuperior" style="display:none;"></div>'); 

    
//    contenedorPreguntas
    
    
    if( json[idPreguntaGlobal].pregunta.secuencia === true ){
        $("#respuestasRadialGroup").css("height",$(window).height());
        var urlImg = '../../../../EVAL/'+ json[idPreguntaGlobal].pregunta.url;
        if(strAmbiente === 'DEV') {
            $("#respuestasRadialGroup").css({backgroundImage: 'url('+urlImg+')',
                backgroundRepeat: "no-repeat"});
        } else {
            $("#respuestasRadialGroup").css({backgroundImage: 'url("' + json[idPreguntaGlobal].pregunta.url + '")',
                backgroundRepeat: "no-repeat"});
        }
        $(".pila2").css({background: "#639BCD"});
        $('.btn_preg').addClass("btn_preg_secuencia");
        $('.btn_preg_secuencia').css({padding: "0px", border:"1px solid red"}).html('');
        $('.iconoEvaluar').addClass('iconoEvaluarSecuencia');
        $('.iconoEvaluarSecuencia').css({width:"99%"});
        $('.no-margen').addClass("no-margen_secuencia");
        $('.no-margen_secuencia').css({width:"1%"});
        $('<style>.btn_preg:before {display:none;}</style>').appendTo('head');
        $(".no-margen_secuencia a").each(function (index) {
            coordenadas = json[idPreguntaGlobal].respuestas[index].coordenadas.split(",");
            topPos = parseInt(coordenadas[0]);
            leftPos = parseInt(coordenadas[1]);
            $(this).parent().parent().css({top: topPos, left: leftPos, position: "absolute"});
            widthCont = 45;
            heightCont = 45;
            $(this).css({width: widthCont, height: heightCont});
        });
    }else{
        $('<style>.btn_preg:before {display:inline-block;}</style>').appendTo('head');
        $('.btn_preg').removeClass("btn_preg_secuencia");
        $('.iconoEvaluar').removeClass('iconoEvaluarSecuencia');
        $('.no-margen').removeClass("no-margen_secuencia");
    }
}
function alturayEstiloMultiple ( ){
    var heightFrame=$( window ).height()-15;
    $("#respuestasRadialGroup").css({margin:"0px"});
    $(".preguntaTit").css({margin:"0"});
    $("#fondoPregunta").height(heightFrame + 'px');
    $("#preguntaTexto").css({background:"#ef7d00", color:"#FFF", display: "table"});
    $("#preguntaTexto div").css({textAlign:"center", fontSize:"20px", 
        height: "inherit", display: "table-cell", verticalAlign: "middle"});
    $("#preguntaTexto").parent().css({paddingLeft:"0", paddingRight:"0"});
    
}
function cadenasParrafo ( ){
    cadenaTemp = respuestasArr[j];
    cadenaTemp = cadenaTemp.replace("<p>", "");//Se elimina la etiqueta del párrafo
    cadenaTemp = cadenaTemp.replace("</p>", "");//Se elimina la etiqueta del párrafo
    respuestasArr[j] = cadenaTemp;
}
function marcarSeleccionado(elemento) {//Marca el boton azul seleccionado
    desmarcaTodos();
    respuestaSeleccionada = elemento;
    if(testBandera){
        respuestasTextoTest[contadorPreguntasTest]=obj.respuestas[elemento-1].t13respuesta;
        contadorPreguntasTest++;
    }
    $('#respuesta' + elemento + '_btn').parent().next("div").height($('#respuesta' + elemento + '_btn').parent().height());
    if (c03id_tipo_pregunta === "1") {
        $('.btn_preg').removeClass('btn_preg_press');
        $('.btn_preg').css({backgroundColor:"transparent"});
        $('#respuesta' + elemento + '_btn').addClass('btn_preg_press');
    } else {
        if($('#respuesta' + elemento + '_btn').hasClass("btn_preg_press")){
            $('#respuesta' + elemento + '_btn').removeClass('btn_preg_press');
            $('#respuesta' + elemento + '_btn').css({background: "transparent"});
        }else{
            var posicion = botonesSeleccionados_arr.indexOf(elemento);
            if (posicion === -1) {//Si no lo encuentra es que no está seleccionado el botón
                $('#respuesta' + elemento + '_btn').addClass('btn_preg_press');
                botonesSeleccionados_arr.push(elemento);
            } else {//si ya está seleccionado el boton
                $('#respuesta' + elemento + '_btn').removeClass('btn_preg_press');
                $('#respuesta' + elemento + '_btn').addClass('btn_preg');
                delete botonesSeleccionados_arr[posicion];
                $('.btn_preg').css({backgroundColor:"transparent"});
            }
        }
    }
    $('#respuesta' + elemento + '_btn').prev().attr('id', 'respuesta' + elemento + '_btn');
    $('#respuesta' + elemento+'_btn').prev().attr('class', 'respuestas');
    $('.btn_preg_press').css('background-color', '#00E7FF');
}
function desmarcaTodos () {//desmarca todos los botones azules
    if(c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "3"){
        for (var j=1; j<=cuantosBotonesDeRespuestaLocal; j++) {
            $('#respuesta'+j+'_btn').removeClass('btn_preg_press');
            $('#respuesta'+j+'_btn').addClass('btn_preg');
        }
    }
}
function mouseOut(elemento){
    //$('#respuesta'+elemento+'_btn').css("color", "white");
}
function agrupaMultipleGroup(){
    var contadorColumna=0, widthColumna=100/obj.pregunta.columnas;
    if(obj.pregunta.columnas!==undefined){
        
        var agruparPor="respMultipleGroup";
        if(obj.pregunta.preguntasMultiples===true){agruparPor="contenedorPreguntas";}
        
        var respuestasPorColumna=Math.ceil($("."+agruparPor).length/obj.pregunta.columnas);
        $("."+agruparPor).each(function(index){
            if(index%respuestasPorColumna===0){
                if(index!==0){
                    contadorColumna++;
                }
                $("form ul").append('<div class="columaMultiple" id="columnaMutiple'+contadorColumna+'"></div>');
                
                console.log("Cont"+index+" ContC:"+contadorColumna);
            }
            $(this).appendTo($("#columnaMutiple"+contadorColumna));
            
        });
        $(".btn_preg").css({padding:"10px 0px 10px 60px"});
        $(".iconoEvaluar").css({width:"10%"})
    }
    $(".columaMultiple").css({width:widthColumna+"%", display:"inline-block", verticalAlign:"top"});

}