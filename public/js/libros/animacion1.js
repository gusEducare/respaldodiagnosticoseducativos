var anchoFrase;
var posicionFrase;
var ajusteMovimiento = 10;

$(document).ready(function() {
    creaEstructuraAnimacion2();               
});
function creaEstructuraAnimacion2 (idDiv){
    $( 'body' ).append(''+
    '<div id="principal_div">' +
        '<div id="animacion0_div" class="alturaDivs"></div>' +
        '<div id="animacion1_div" class="divElementos_De_Animacion1 alturaDivs"></div>' +
        '<div id="animacion2_div" class="mascaraLateral alturaDivs"></div>' +
        '<div id="animacion3_div" class=" alturaDivs"></div>' +
    '</div>' +
    '<div id="divFondo" class="div_fondo"></div>');

    crearAnimacion('Leccion 1', "Me comunico elocuentemente", "", '#E07000', '#503070', '220px', '230px');//# de sesión , Nombre de sesión parte 1, Nombre de sesión parte 2, Color del banner, Color de letra del # de la sesión, ancho de la imagen central, alto de la imagen central
}
function crearAnimacion (numeroSesion , frase1, frase2, colorFondo, colorNumSesion, anchoImagen, altoImagen){
    var tiempoDeEspera = 350;
    var duracionAnimacionMedia = 1000;
    var duracionAnimacionCorta = 300;

    $('#divFondo').css('background', colorFondo);

    $('#animacion1_div').append('<div id="idTriangulo" class="trianguloInicial" >'+
        '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="600" height="600" x="0">' +                
            '<polygon points="0,0 410,0 64,361 0,360" fill="#7FCCDF"/>' +
        '</svg>'+
    '</div>');

    $('#animacion1_div').css('left', '-400px');
    $('#animacion1_div').delay(tiempoDeEspera).show().animate({ left: 0, opacity: '1' }, {duration: duracionAnimacionMedia, easing: 'easeOutBounce', complete: function(){//Animación para bajar Sesión # al centro del banner           
       $('#animacion2_div').append('<div id="idCenterDiv"><div id="idDivInterno"><img class="" id="imagenBanner" src="images/sesion15.png" data-polyclip="487, 4, 500, 239, 19, 239, 43, 195"></div></div>');            
       //$('#principal_div').append('<div class="triangulo"></div>');
       $('#imagenBanner').css('position', 'absolute');
       $('#imagenBanner').css('left', '460px');
       $('#imagenBanner').css('top', '60px');
       $('#imagenBanner').css('width', anchoImagen);
       $('#imagenBanner').css('height', altoImagen);
       $('#imagenBanner').delay(tiempoDeEspera).show().animate({ left: 60, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'easeOutBack', complete: function(){   
            $('#animacion0_div').append('<div id="divInterior"><div id="idNombreSesion" class="nombre_sesion" >'+frase1+'</div></div></div>');//Se agrega al stage la frase 1 del nombre de la sesión
            $('#idNombreSesion').css('width', '600px');
            $('#idNombreSesion').css('white-space', 'normal');                                                
            $('#idNombreSesion').css('position', 'absolute');
            $('#idNombreSesion').css('left', '-700px');
            $('#idNombreSesion').css('top', '80px');            
            $('#idNombreSesion').delay(tiempoDeEspera).show().animate({ left: 330, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'easeOutQuart', complete: function(){                                                   
                $('#animacion0_div').append('<div id="idNumeroSesion" class="numero_Sesion" >'+numeroSesion+'</div>');
                $('#idNumeroSesion').css('color', colorNumSesion);
                $('#idNumeroSesion').css('font-size', '35px');
                $('#idNumeroSesion').css('white-space', 'nowrap');
                $('#idNumeroSesion').css('left', '-200px');//-200 560
                $('#idNumeroSesion').css('top', '290px');                       
                $('#idNumeroSesion').delay(tiempoDeEspera).show().animate({ left: 560, opacity: '1' }, {duration: duracionAnimacionCorta, easing: 'easeOutQuart', complete: function(){//Animación para bajar Sesión # al centro del banner                            
                }}); 
            }});   
        }});
    }});
}
function obtieneCentrado (idDiv){
    var dato = 512 - $('#'+idDiv).width() / 2;
    return dato;
}