 /*______                                         __                            
  |   __ \.-----.--------.-----.-----.----.---.-.|  |--.-----.-----.---.-.-----.
  |      <|  _  |        |  _  |  -__|  __|  _  ||  _  |  -__|-- __|  _  |__ --|
  |___|__||_____|__|__|__|   __|_____|____|___._||_____|_____|_____|___._|_____|
                         |__|                                                   
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com    
*********************************************************/
function rompecabezasMain (){
    $(function () {
        var medidaGrid = t11columnas;
        var rompecabezasClase = {
        startTime: new Date().getTime(),
        iniciarJuego: function (imagen, medidaGrid) {       
            this.setearImagen(imagen, medidaGrid);            
            $('#sortable').randomize();
            this.habilitarSwap('#sortable li');                
        },
        habilitarSwap: function (elemento) {
            $(elemento).draggable({
                snap: '#droppable',
                snapMode: 'outer',
                revert: "invalid",
                helper: "clone"
            });
            $(elemento).droppable({
                drop: function (event, ui) {
                    var $dragElemento = $(ui.draggable).clone().replaceAll(this);
                    $(this).replaceAll(ui.draggable);
                    currentList = $('#sortable > li').map(function (i, el) { return $(el).attr('data-value'); });
                    if (esRandomizada(currentList)){                    
                        $('#cuadroImagenActual').empty().html($('#juegoTerminado').html());
                    }else {                    
                    }
                    rompecabezasClase.habilitarSwap(this);
                    rompecabezasClase.habilitarSwap($dragElemento);
                }
            });
        },
        setearImagen: function (imagen, medidaGrid) {        
            medidaGrid = medidaGrid || 4; // Si medidaGrid es null por default cambia a 4.
            var porcentaje = 100 / (medidaGrid - 1);                
            var image = imagen;
            $('#imagenActual').attr('src', imagen);
            $('#sortable').empty();
            
            for (var i = 0; i < medidaGrid * medidaGrid; i++) {
                var posicionX = (porcentaje * (i % medidaGrid)) + '%';
                var posicionY = (porcentaje * Math.floor(i / medidaGrid)) + '%';                
                var li = $('<li class="item" data-value="' + (i) + '"></li>').css({
                    'background-image': 'url(' + imagen + ')',                
                    'background-size': (medidaGrid * 100) + '%',
                    'background-position': posicionX + ' ' + posicionY,
                    'width': 396 / medidaGrid,
                    'height': 396 / medidaGrid
                });
                $('#sortable').append(li);
            }        
            $('#sortable').randomize();        
        }
    };    
    function esRandomizada(arr) {
        for (var i = 0; i < arr.length - 1; i++) {
            if (arr[i] != i)
                return false;
        }
        return true;
    }
    $.fn.randomize = function (selector) {
        var $Elementos = selector ? $(this).find(selector) : $(this).children(),
            $parents = $Elementos.parent();
        $parents.each(function () {
            $(this).children(selector).sort(function () {
                return Math.round(Math.random()) - 0.5;
            }).remove().appendTo(this);
        });
        return this;
    };    
        rompecabezasClase.iniciarJuego(imagen, medidaGrid);                            
    });              
}