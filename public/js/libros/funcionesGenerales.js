/*_______                    __                               _______                                __              
 |    ___|.--.--.-----.----.|__|.-----.-----.-----.-----.    |     __|.-----.-----.-----.----.---.-.|  |.-----.-----.
 |    ___||  |  |     |  __||  ||  _  |     |  -__|__ --|    |    |  ||  -__|     |  -__|   _|  _  ||  ||  -__|__ --|
 |___|    |_____|__|__|____||__||_____|__|__|_____|_____|    |_______||_____|__|__|_____|__| |___._||__||_____|_____|
 @author:           Grupo Educare S.A. de C.V.
 @desarrolladores   Greg: gregorios@grupoeducare.com
                    Juan Pablo: jpgomez@grupoeducare.com
                    Juan José: jlopez@grupoeducare.com
 @estilos css:      Claudia:  cgochoa@grupoeducare.com     
 *********************************************************/
var t11id_pregunta;
var c03id_tipo_pregunta;
var t11pregunta;
var t11instruccion;
var t11columnas;
var t11usuario;
var t13respuesta;
var t13estatus;
var svgNS = "http://www.w3.org/2000/svg";
var svgNSLink = "http://www.w3.org/1999/xlink";
var nombreSVG = "elementosSVG";
var valor = $('#jsonPregunta').val();
var incisos_arr;
var obj;
var arregloDeCadenas;
var colores_arr = ["#006600", "#CC0000", "#FF6600", "#CC6666", "#333399", "#3399FF", "#FF9933", "#9933CC", "#003366", "#666666", "#666699", "#FF66CC", "#663333", "#009900", "#669933", "#0066CC", "#999933", "#990000", "#FF9933", "#9966FF"];
var preguntasArr = new Array();
var respuestasArr = new Array();
var posColores = 0;
var numEstilo;//1=RadialButtons, 2=Botones azules  3=cajasColumnas
var numeracion = "1";//1= ABCDARIO 2= abcdario  3=Numérica 4=Romana
var contieneRespuestas = true;
var primeraVezEspacios = true;
var textos_arr = ["Erase una vez un", ". Como habia dejado de ", ", de una terrible ", ", ansiosos de libertad, salieron de sus ", " y empezaron a corretear por la alfombra recien ", ".Hare un ", ", un nenito precioso, redondo, con ojos de ", " y convirtio al nenito en su inseparable ", ", durante los tristes dias de aquel ", ". empezaron a ser mas largos y los ", ", aqui termina el cuento."];

// Paso 1 - Rompecabezas, pizarron
var imagen;
var imagenesPizarron;
var pizarron = false; //sin pizarron
var tipoMultimedia = "imagen";//texto imagen audio video
var idioma = 'es';//es-español   en-inglés
var subtipo = 'frase';//con frase  //sin frase
var preguntasBidimensional_arr;
var cuantasPreguntas = 2;
var navegador;
var navegador_version;
var versionInt;
var svgRadial = false;
numeroPreguntaGlobal = 0;
var versionFV = "2";
var cuantasRelaciona = 0;
var tiempoAnimacionBarrido = 1200;
var tiempoEsperaReactivo = 1000;
var posicionesCrucigrama_arr = new Array();

function obtenerValoresJSON(obj) {
    t11id_pregunta = obj.pregunta.t11id_pregunta;
    c03id_tipo_pregunta = obj.pregunta.c03id_tipo_pregunta;
    var numRespuestas = typeof obj.respuestas === 'object' ? Object.keys(obj.respuestas).length : obj.respuestas.length;
    pizarron = t11id_pregunta === "1147"; // || t11id_pregunta === "932" ? true : false;
    testBandera = obj.pregunta.test;
    testEvaluable = obj.pregunta.evaluable;
    t11pregunta = obj.pregunta.t11pregunta;
    t11instruccion = obj.pregunta.t11instruccion;
    t11columnas = obj.pregunta.t11columnas;
    t11usuario = typeof obj.pregunta.usuario === 'undefined' ? [] : obj.pregunta.usuario;
    pintaLetrerosComunes();
    preguntasArr = new Array();
    respuestasArr = new Array();
    contenedoresArr = new Array();
    if (typeof obj.preguntas !== 'undefined') {
        for (j = 0; j < obj.preguntas.length; j++) {
            t11pregunta = obj.preguntas[j].t11pregunta; // Decodifico el html que venga de la base de datos para que se muestre bien
            t11instruccion = obj.preguntas[j].t11instruccion;
            t11columnas = obj.preguntas[j].t11columnas;
            preguntasArr[j] = t11pregunta;
            if (json[idPreguntaGlobal].pocisiones !== undefined) {
                posicionesCrucigrama_arr[j] = obj.pocisiones[j];
            }
        }
    }

    if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
        numRespuestas = json[idPreguntaGlobal].contenedores.length;
    }
    if(c03id_tipo_pregunta === "8"){
        json[idPreguntaGlobal].respuestas = shuffleArray(json[idPreguntaGlobal].respuestas);
    }
    for (j = 0; j < numRespuestas; j++) {
        if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
            //t13respuesta = obj.contenedores[j].Contenedor[4];
            t13respuesta = json[idPreguntaGlobal].respuestas[j].t13respuesta;
        } else {
            t13respuesta = json[idPreguntaGlobal].respuestas[j].t13respuesta; // Decodifico el html que venga de la base de datos para que se muestre bien
            contenedoresArr[j] = json[idPreguntaGlobal].respuestas[j].t17correcta;
            t13estatus = json[idPreguntaGlobal].respuestas[j].t13estatus;
        }
        if (t13respuesta == null) {
            t13respuesta = "";//Para evitar que marque error al regresar null
        }
        if (typeof t13respuesta !== 'undefined') { // Soluciona el problema de las preguntas cortas que puede haber un texto mas que las respuestas
            if (typeof t13respuesta !== 'null') {
                //t13respuesta = t13respuesta.replace(" ", "&nbsp;");//Soluciona problema de espacio en blanco
            } else {
                t13respuesta = '&nbsp;';
            }
        }
        
        /*var rutaAbsoluta = "../../../../" + strRutaLibro;//Ajuste para que lea la ruta absoluta
        t13respuesta = t13respuesta.replace('<img src="', '<img src="'+rutaAbsoluta);
        t13respuesta = t13respuesta.replace("<img src='", "<img src='"+rutaAbsoluta);
        console.log('t13respuestaaaa '+t13respuesta);*/
        
        respuestasArr[j] = t13respuesta;
    }

    if(json[idPreguntaGlobal].pregunta.agrupaContenedores===true){
        var contenedoresTotales=""; //variable que se encargara de almacenar todos los contenedores del json en t17correcta
            $.each(json[idPreguntaGlobal].contenedores, function(index, element){ //se obtienen los contenedores
                contenedoresTotales=contenedoresTotales+index+",";
            });
        contenedoresTotales=contenedoresTotales.split(",");//se separan todos los contenedores encontrados
        contenedoresArr=contenedoresTotales.filter(function(item, pos, self){//se eliminan aquellos contenedores que se encuentren repetidos
            if(item!==""){
                return self.indexOf(item) === pos;
            }
        });         
    }
    if(!json[idPreguntaGlobal].pregunta.preguntasMultiples){
        $('.pregunta').html('<b>' + eliminarParrafos(json[idPreguntaGlobal].pregunta.t11pregunta) + '</b>');
    }else{
        $('.pregunta').html("");
    }

    seleccionaEstilo(numRespuestas);
    for (j = 0; j < numRespuestas; j++) {
        if (c03id_tipo_pregunta !== "4" && c03id_tipo_pregunta !== "8" && c03id_tipo_pregunta !== "18" && c03id_tipo_pregunta !== "16" && c03id_tipo_pregunta !== "30") {//Si es de relacionar combo no toma en cuenta el numero de respuestas. Se considera una sola respuesta. //Si es Actividad no evaluable: Click and view
            crearDivs();
        }
    }
    if (c03id_tipo_pregunta === "4" || c03id_tipo_pregunta === "8" || c03id_tipo_pregunta === "18" || c03id_tipo_pregunta === "16" || c03id_tipo_pregunta === "30") {//Si es relaciona arrastrar. //Si es Actividad no evaluable: Click and view
        crearDivs();
    }
    cambiarRutasImagenes();
    redimensionaAlturaDivPrincipal();
    eliminaTextos();
    //aplicaEstilosFraccion(); //Descomentar
    if(json[idPreguntaGlobal].pregunta.opcionesTexto === true){
        $('.tarjetas li .resp_arra_cont p').css({textAlign: 'center'});
    }      
    // Variante de Imagen de Fondo
    if(json[idPreguntaGlobal].pregunta.relacionaImagenFondo === true){
        //$('#centro').css( {backgroundImage: json[idPreguntaGlobal].pregunta.url});
        generaEstilosImagenFondo();
    }

    if (c03id_tipo_pregunta === "5") {

        modificaEstilosCss()
    }

}
function redimensionaAlturaDivPrincipal() {
    var dimensiones = new Object();
    if (c03id_tipo_pregunta === "9") {
        $('#respuestasRadialGroup').height($('.dhe-example-section').height() + 80);
    }
    if (c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "2" || c03id_tipo_pregunta === "3") {
        //$('#respuestasRadialGroup').height( $('form').height() + $('.exam_footer').height());
    }
    if (c03id_tipo_pregunta !== "8" && c03id_tipo_pregunta !== "13") {
    }
    if (c03id_tipo_pregunta === "8") {//Si es arrastra cortas
        //dimensiones.height = $('#pilaDeCartas').height() + $('#espaciosDeCartas').height();//dimensiones.height = $('#respuestasRadialGroup').height();                       
        //dimensiones.height = $('#pilaDeCartas').height() + $('#texto').height();//dimensiones.height = $('#respuestasRadialGroup').height();
        //$('#content').height(dimensiones.height + 100);                
    }
}
function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes) then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}
function htmlDecode(value) {
    return value;//  return $('<div/>').html(value).text();
}
function trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g, "");
}
function ltrim(stringToTrim) {
    return stringToTrim.replace(/^\s+/, "");
}
function rtrim(stringToTrim) {
    return stringToTrim.replace(/\s+$/, "");
}
function seleccionaEstilo(numRespuestas) {
    contieneRespuestas = true;
    if (c03id_tipo_pregunta === "1") {
        numEstilo = "2";//Puede ser 1: radialGroup ó 2: botones personalizados
    }
    if (c03id_tipo_pregunta === "2") {
        numEstilo = "5";//Puede ser 5: checkBox ó 2: botones personalizados
    }
    if (c03id_tipo_pregunta === "11") {//Si es relacionar columnas con cajitas
        numEstilo = "3";
    }
    if (c03id_tipo_pregunta === "6") {//Si es pregunta corta con textarea
        contieneRespuestas = false;
        numEstilo = "4";
    }
    if (c03id_tipo_pregunta === "4") {//Si es pregunta Relacionar Combo
        contieneRespuestas = false;
        numEstilo = "6";
    }
    if (c03id_tipo_pregunta === "7") {//Si es Cortas Horizontal
        contieneRespuestas = true;
        numEstilo = "7";
    }
    if (c03id_tipo_pregunta === "3") {//Si es Verdadero Falso
        numEstilo = "1";
    }
    if (c03id_tipo_pregunta === "10") {//Si es Ordenar con números
        numEstilo = "9";
    }
    if (c03id_tipo_pregunta === "12") {//Si es Relacionar Líneas        
        numEstilo = "10";
    }
    if (c03id_tipo_pregunta === "9") {//Si es Arrastra Ordenar
        numEstilo = "11";
    }
    if (c03id_tipo_pregunta === "17") {//Si es Corta Numérica
        contieneRespuestas = false;
        numEstilo = "12";
    }
    if (c03id_tipo_pregunta === "8") {//Si es Arrastra Cortas
        contieneRespuestas = false;
        numEstilo = "13";
    }
    if (c03id_tipo_pregunta === "5") {//Si es Arrastra Contenedor
        numEstilo = "14";
    }
    if (c03id_tipo_pregunta === "14") {//Si es Rompecabezas
        contieneRespuestas = false;
        imagen = respuestasArr[0];
        numEstilo = "15";
    }
    if (c03id_tipo_pregunta === "13") {//Si es Matriz        
        numEstilo = "16";
    }
    if (c03id_tipo_pregunta === "16") {//Si es Sopa de Letras
        contieneRespuestas = false;
        numEstilo = "17";
    }
    if (c03id_tipo_pregunta === "18") {//Si es Relaciona Arrastrar
        numEstilo = "18";
    }
    if (c03id_tipo_pregunta === "19") {//Si es Une los puntos
        contieneRespuestas = false;
        numEstilo = "20";
    }
    if (c03id_tipo_pregunta === "15") {//Si es Crucigrama      
        contieneRespuestas = false;
        numEstilo = "21";
    }
    if (c03id_tipo_pregunta === "30") {//Si es Actividad no evaluable: Click and view
        numEstilo = "30";
    }    
    prepararDivs(numRespuestas);//Función que se le pasa cuantos botones va a crear dinámicamente   
}
function pintaLetrerosComunes() {//Texto de la pregunta
    $('.instrucciones_pregunta').html(t11instruccion);
    var numPregt = $('.num_preg a').html().split('/');
    var numeroPregunta = numPregt[0];
//    var rutaAbsoluta = "../../../../" + strRutaLibro;//Ajsute para que lea la ruta absoluta
//    t11pregunta = t11pregunta.replace('<img src="', '<img src="'+rutaAbsoluta);
    if (c03id_tipo_pregunta === "12" || c03id_tipo_pregunta === "16" || c03id_tipo_pregunta === "13" || c03id_tipo_pregunta === "8" || c03id_tipo_pregunta === "19" || c03id_tipo_pregunta === "15" || c03id_tipo_pregunta === "18" || c03id_tipo_pregunta === "30") {//Las preguntas de tipo Matriz, Relaciona Líneas, sopa de letras, Une los puntos, crucigrama, relaciona arrastrar no llevan el texto de la pregunta. //Si es Actividad no evaluable: Click and view
        t11pregunta = "";
        $('#preguntaTexto').css({height:"0px", margin:"0px"});
    }
    if (c03id_tipo_pregunta === "3") {//Si es una pregunta de tipo FV
        numeroPregunta = numeroPregunta.replace(' ', '');
        t11pregunta = t11pregunta.replace('<img', '<img id="imagenFV' + numeroPregunta + '"');
        $('#preguntaTexto').html("<div class='div-pregunta-FV'><h1 class='h1-FV'><span class='numero-preguntaFV'>" + numeroPregunta.trim() + "</span><br>" + t11pregunta + '</h1></div>');
        
        var srcImagen = $('#imagenFV' + numeroPregunta).attr('src');
        var existeImagen = contieneImagen();
        if (existeImagen) {
            $('#preguntaTexto').css('opacity', '0');
            $('#imagenFV' + numeroPregunta).load(function (e) {
                $('#preguntaTexto').css('opacity', '1');//Termina de cargar la imagen
            }).error(function () {
                //Error al cargar la imagen
            }).attr('src', srcImagen);
        }

    } else if(json[idPreguntaGlobal].pregunta.preguntasMultiples!==true){
        numeroPreguntaGlobal = numeroPregunta.trim();
        if (json[idPreguntaGlobal].pregunta.hayImagenTexto === true) {
            $('#preguntaTexto').html("<div style='margin-top: -21px;'>" + t11pregunta + "</div>");
        } else {
            $('#preguntaTexto').html("<div>" + t11pregunta + "</div>");
        }
        if (c03id_tipo_pregunta === "5") {
            $('#preguntaTexto').css({height:"0px", margin:"0px"});
        }
        $('#preguntaTexto').addClass('preguntaTit');
        $('#preguntaTexto').find('span').each(function () {
            $(this).removeAttr('style');
        });// Elimino todos los estilos de los span que inserta el editor de textos
    }

    $('.imgsAnt').addClass('text-center');

    if ($('#primera').val()) {
        $('.anterior').addClass('desactiva');
    } else {
        $('.anterior').removeClass('desactiva');
    }
    if ($('#ultima').val()) {
        $('.siguiente').addClass('desactiva');
    } else {
        $('.siguiente').removeClass('desactiva');
    }
    cambiarRutasImagenes();
}
function get_browser() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return 'Opera ' + tem[1];
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return M[0];
}
function get_browser_version() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) {
            return 'Opera ' + tem[1];
        }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) {
        M.splice(1, 1, tem[1]);
    }
    return M[1];
}
$(document).ready(function () {
    idPreguntaGlobal = 0;
    $("#primera").val(true);
    $('#txtCrecer').keypress(function () {
        $('#referenciaCrecer').html($(this).val().replace(/\n/g, "<br />"));
        $(this).height($('#referenciaCrecer').height());
    });
    if (svgRadial) {//Si aplica el radialMain Personalizado
        radialInicio();
    }
   // cambiarRutasImagenes();
    //if (typeof json[idPreguntaGlobal].pregunta.respuestasLargas !== 'undefined') {
        if (json[idPreguntaGlobal].pregunta.respuestasLargas === true) {
            var totalImgs = $('#respuestasRadialGroup img').size(), cont = 0;
            if( totalImgs ){
                $('#respuestasRadialGroup img').load(function (e) {
                    console.log('Se cargó la imagen ' + $(this).attr('src'));
                    cont++;
                    if (totalImgs === cont) {
                        console.log('Se cargaron todas las imágenes');
                        ajustarAnchoCajas();
                    }
                }).error(function () {
                    console.log('Error al cargar la imagen');
                });
            }else{
                ajustarAnchoCajas();
            }
        }
    //}
    $('.exam_footer').css({position: 'fixed'});
});
function esMobile() {
    var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    return mobile;
}

var cuantosBotonesDeRespuestaLocal;
var botonesSeleccionados_arr = new Array();
var arrayValores = new Array();
var arrayValores2 = new Array();
var restriccionCaracteres;
var pintarUltimaCaja = true;
var vueltasRespuesta = 1;
var imagenesCargadas = 0;
var respuestaTemporal;
var pocisionDeEspacioGlobal;
var ceroInicialUtilizado = 0;
var sueltaEnDrop = false;
var myInterval;
var elementoParaContenedor;
var posTempX;
var posTempY;
var textoTemp;
var vuelta = 0;
var mobile;
var carta = 0;
var respuestaSeleccionada;
var contenedorDroppeado;
var contadorRespMostrar = 0;
var valueCont=1;

function prepararDivs(cuantosBotonesDeRespuesta) {
    $("#respuestasRadialGroup").empty();
    $("#respuestasRadialGroup").addClass('large-12');
    $("#respuestasRadialGroup").addClass('small-12');
    $("#respuestasRadialGroup").addClass('columns');
    $("#respuestasRadialGroup").addClass('respuesta');

    var estructuraBotonesCompleta = "";
    var estructuraBotones = "";
    var estructuraBotones2 = "";
    var acumulativoEstilos;
    var inciso;
    imagenesCargadas = 0;
    cuantosBotonesDeRespuestaLocal = cuantosBotonesDeRespuesta;

    if (c03id_tipo_pregunta === "10" || c03id_tipo_pregunta === "17") {
        numeracion = "3";
    }
    if (numeracion === "1") {
        acumulativoEstilos = 0;
        restriccionCaracteres = /[A-Za-z]/g;
        incisos_arr = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
    }
    if (numeracion === "2") {
        acumulativoEstilos = 100;
        restriccionCaracteres = /[A-Za-z]/g;
        incisos_arr = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "a", "t", "u", "v", "w", "x", "y", "z"];
    }
    if (numeracion === "3") {
        acumulativoEstilos = 200;
        restriccionCaracteres = /[1234567890]/g;
        incisos_arr = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26"];
    }
    if (numeracion === "4") {
        acumulativoEstilos = 300;
        restriccionCaracteres = /[IVX]/g;
        incisos_arr = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII", "XIV", "XV", "XVI", "XVII", "XVIII", "XIX", "XX", "XXI", "XXIII", "XXIV", "XXV", "XXVI"];
    }

    if (contieneRespuestas) {
        for (var j = 1; j <= cuantosBotonesDeRespuesta; j++) {
            if (numEstilo === "1") {//Múltiple con Botones RadialGroup                
                if (obj.pregunta.imagenes) {
                    estructuraBotones = estructuraBotones + '<div class="columnasImg"><div class="opcionImg"><a rel="gallery-s13_' + j + '" href="img/examenes/S13_G2_2_big.png" class="swipebox">' + obj.respuestas[j - 1].t13respuesta + '</a></div><input type="radio" id="r' + j + '" name="btn_preg" class="respuestas" value="' + j + '"></input><label id="respuesta' + j + '_btn" for="r' + j + '"></label></div>';
                } else {
                    estructuraBotones = estructuraBotones + '<div class="opcionBoton"><input type="radio" id="r' + j + '" name="respuesta btn_preg" class="respuestas" value="' + j + '"></input><label id="respuesta' + j + '_btn" for="r' + j + '"></label></div>';
                }
            }
            if (numEstilo === "5") {//Múltiple con Botones CheckBox                
                if(json[idPreguntaGlobal].pregunta.preguntasMultiples===true){
                    var preguntaActual=0;
                    var incisoActual;
                    var alinear="";
                    while(preguntaActual<json[idPreguntaGlobal].pregunta.t11pregunta.length){
                        incisoActual=1;
                        estructuraBotones=estructuraBotones+"<div style='width: 100%; margin-bottom: 20px;' class='contenedorPreguntas' id='contenedorPreguntas"+preguntaActual+"'>";
                        estructuraBotones=estructuraBotones+"<div style='display: inline-block; width:15px; height:15px; background: rgb(232, 78, 15); border-radius:15px; vertical-align: top; margin-top: 10px;'></div>";
                        estructuraBotones=estructuraBotones+"<p style='margin-left: 10px; display: inline-block; font-size:20px; font-weight: bold; width: calc(100% - 25px);'>"+json[idPreguntaGlobal].pregunta.t11pregunta[preguntaActual]+"</p>";
                        for(var k=1; k<=json[idPreguntaGlobal].respuestas.length; k++){
                            if(preguntaActual+""===json[idPreguntaGlobal].respuestas[k-1].numeroPregunta+""){
                                if(incisoActual===1){alinear="display: inline-block !important;'"}
                                estructuraBotones=estructuraBotones+"<div class='respMultipleGroup'><p "+alinear+" class='no-margen'><a id='respuesta" + k + "_btn' class='btn_preg btn_preg" + (acumulativoEstilos + incisoActual) + "' onMouseDown='marcarSeleccionado(" + k + ")' onmouseOut='mouseOut(" + k + ")' value='" + k + "'> </a></p><div class='iconoEvaluar'></div></div>\n\n";
                                incisoActual++;
                            }
                        }
                        estructuraBotones=estructuraBotones+"</div>";
                        preguntaActual++;
                    }
                    break;
                }else{
                    estructuraBotones = estructuraBotones + '<div class="respMultipleGroup"><p class="no-margen"><a id="respuesta' + j + '_btn" class="btn_preg btn_preg' + (acumulativoEstilos + j) + '" onMouseDown="marcarSeleccionado(' + j + ')" onmouseOut="mouseOut(' + j + ')" value="' + j + '"> </a></p><div class="iconoEvaluar"></div></div>\n\n';
                }                
            }
            if (numEstilo === "2") {//Múltiple y Respuesta Múltiple con Botones Personalizados
                estructuraBotones = estructuraBotones + '<div class="respMultipleGroup"><p class="no-margen"><a id="respuesta' + j + '_btn" class="btn_preg btn_preg' + (acumulativoEstilos + j) + '" onMouseDown="marcarSeleccionado(' + j + ')" onmouseOut="mouseOut(' + j + ')" value="' + j + '"> </a></p><div class="iconoEvaluar"></div></div>\n\n';
            }
            if (numEstilo === "3") {//Relacionar Columnas con cajas           
                estructuraBotones = estructuraBotones + '<p><a id="respuesta' + j + '_btn" class="text-center pregunta columnas_personalizadas columnas_personalizadas' + (acumulativoEstilos + j) + '" ></a></p>\n\n';
                estructuraBotones2 = estructuraBotones2 + '<p> <input type="text" class="textbox textboxGral bordeSemiredondeado" maxlength="1" onkeypress="return restrictCharacters(this,event,restriccionCaracteres) " id="' + j + '" onkeyup="convierte(this)" ><a id="respuesta' + (j + cuantosBotonesDeRespuesta) + '_btn" class="pregunta columnas_personalizadasDerecha" ></a></p>\n\n';
            }
            if (numEstilo === "7") {//Cortas Horizontal
                // aqui se tiene que agregar el spacer <div class="spacer60"></div> cuando la pregunta sea de orientacion vertical
                // En el momento que se pinte el dom, se tiene que ejecutar la siguiente operacion $('.texto-respuesta p').each(function(){$(this).width(170);});
                // donde 170 es el tamaño de la caja mas grande, eso se puede obtener con $('.texto-respuesta p').each(function(){console.log($(this).width());});
                // ver si esto se puede reaplicar en otros tipos, como lo es arrastra corta.
                estructuraBotones = estructuraBotones + '<span id="respuesta' + j + '_btn" class="left texto-respuesta"></span><input type="text" id="' + j + '" class="textboxGral texto-cortas">';
                //estructuraBotones = estructuraBotones +'<span style="float:left" >Has añadido contenido nuevo...</span><input type="text" id="'+j+'" class="textbox2">';//esta seria la vertical con muchos textos
            }
            if (numEstilo === "8") {//Opción Falso Verdadero
                if (j <= 2) {
                    estructuraBotones = estructuraBotones + '<p><button id="respuesta' + j + '_btn">boton</button></p>';
                }
            }
            if (numEstilo === "9") {//Ordenar con Números
                estructuraBotones = estructuraBotones + '<p><a id="respuesta' + j + '_btn" class="columnas_personalizadasOrdenarNumeros text-center" ></a></p>\n\n';
                estructuraBotones2 = estructuraBotones2 + '<div class="large-12 columns"><span style="float:left;margin-left:3em;" > </span><input type="text" maxlength="1" onkeypress="return restrictCharacters(this,event,restriccionCaracteres) " id="' + j + '" onkeyup="convierte(this)" class="cajaRelacionarNumeros textboxGral bordeRedondeado text-center"></a></div>';
            }
            if (numEstilo === "10") {//Relacionar Líneas
                estructuraBotones = estructuraBotones + '<div class="preguntaRelaciona text-right" id="izquierda_' + (j - 1) + '"></div>';
                estructuraBotones2 = estructuraBotones2 + '<div class="preguntaRelaciona" id="derecha_' + (j - 1) + '" ></div>';
            }
            if (numEstilo === "11") {//Arrastra Ordenar
                estructuraBotones = estructuraBotones + '<li class="sortable-item button success" id="' + (j - 1) + '" value="' + j + '"><span id="respuesta' + j + '_btn"></span></li>';
            }
            if (numEstilo === "14") {//Arrastra Contenedor
                var separadorColumnas = "";
                cuantosElementosDosColumnas();
                if (json[idPreguntaGlobal].contenedoresRespuesta !== undefined) {
                    if (j === cuantosElementosDosColumnas()) {
                        separadorColumnas = "<*_*>";
                    } else {
                        separadorColumnas = "";
                    }
                    estructuraBotones = estructuraBotones + '<li class="ui-widget-content" id="li' + j + '"><div id="imgLineas' + j + '" class="imgLineas"></div><div id="respuesta' + j + '_btn" value="' + j + '" class="resp_arra_cont combos"></div></li>' + separadorColumnas;

                } else {
                    if (json[idPreguntaGlobal].pregunta.respuestaUnicaMultiple === true) {
                        for (var i = 0; i < json[idPreguntaGlobal].respuestas[j-1].clones; i++) {
                            estructuraBotones = estructuraBotones + '<li class="ui-widget-content listaTarjetas_'+j+'" id="li' +valueCont+'"><div id="imgLineas'+valueCont+'" class="imgLineas"></div><div id="respuesta'+valueCont+'_btn" value="' + j + '" pila="'+j+'" class="resp_arra_cont combos"></div></li>';
                            valueCont++;
                        }
                    } else {
                        var grupo='';
                        if(json[idPreguntaGlobal].respuestas[j-1].grupoContenedor!==undefined){
                            grupo='grupo="g'+json[idPreguntaGlobal].respuestas[j-1].grupoContenedor+'"';
                        }
                    estructuraBotones = estructuraBotones + '<li class="ui-widget-content" id="li' + j + '" '+grupo+'><div id="imgLineas' + j + '" class="imgLineas"></div><div id="respuesta' + j + '_btn" value="' + j + '" class="resp_arra_cont combos"></div></li>';

                }

            }
            }
            if (numEstilo === "16") {//Matriz RadialGroup
                estructuraBotones = estructuraBotones + '<li><input type="radio" id="r' + j + '" name="radialGroup" value="' + j + '" ><label id="respuesta' + j + '_btn" for="respuesta_' + j + '" class="radio-inline"></label></li>';
                estructuraBotones2 = estructuraBotones2 + '<li><input type="radio" id="r' + j + '" name="radialGroup" value="' + j + '" ><label id="respuesta' + j + '_btn" for="respuesta_' + j + '" class="radio-inline"></label></li>';
            }
            if (numEstilo === "30") {//Si es Actividad no evaluable: Click and view
                console.log('prepara estructura');
                //estructuraBotones = estructuraBotones + '<li>texto<input type="radio" id="r' + j + '" name="radialGroup" value="' + j + '" ><label id="respuesta' + j + '_btn" for="respuesta_' + j + '" class="radio-inline"></label></li>';
                //estructuraBotones = estructuraBotones + '<div id="imagenDeFondo">imagen de fondo</div>';
            }
        }
    } else {
        if (numEstilo === "4") {//Abierta con una caja autoexpandible
            estructuraBotones = '<div id="respuesta_txt"><textarea id="txtCrecer" rows="4" cols="10" class="textboxLargo textboxGral" placeholder="Escribe la respuesta correcta..."></textarea></div>';
        }
        if (numEstilo === "6") {//Relaciona Combo
            estructuraBotones = estructuraBotones + '<p><select id="combo1" class="comboBox" onchange="elementoSeleccionado1(this.value);"><option value="0">Selecciona una opcion</option></select></p>';
            estructuraBotones2 = estructuraBotones2 + '<p><select id="combo2" class="comboBox comboBoxDerecha" onchange="elementoSeleccionado2(this.value);"><option value="0">Selecciona una opcion</option></select></p>';
        }
        if (numEstilo === "12") {//Cortas Numérica
            estructuraBotones = estructuraBotones + '<input type="text" id="' + j + '" style="width: 200px !important;float:left;height: 40px;" onkeypress="return restrictCharacters(this,event,restriccionCaracteres) " id="' + j + '" onkeyup="convierte(this)">';
        }
        if (numEstilo === "13") {//Arrastrar Cortas
            estructuraBotones = estructuraBotones + '';
            verificaUltimaCaja();
        }
        if (numEstilo === "15") {//Rompecabezas
            estructuraBotones = estructuraBotones + '<div id="recuadro"><div id="panelJuego"><div><ul id="sortable" class="sortable"></ul><div id="cuadroImagenActual"><img id="imagenActual"></div></div></div><div id="juegoTerminado" style="display:none;"><div>Terminaste de armar el rompecabezas.</div></div></div>';
        }
        if (numEstilo === "17") {//Sopa de Letras
            estructuraBotones = estructuraBotones + '<div class="grid-Sopadeletras" id="gridSopadeletras"></div>';
        }
        if (numEstilo === "18") {//Relaciona Arrastrar
            estructuraBotones = estructuraBotones + '';
        }
        if (numEstilo === "20") {//Une los puntos
            //estructuraBotones = estructuraBotones + '<div class="" id="unePuntos"><img id="thing" alt="the thing" height="67" src="http://www.kirupa.com/images/smiley_red.png" width="67" style="left: 260.5px; top: 91.5px;"></div>';//Para detectar puntos donde toca el puntero del mouse
            estructuraBotones = estructuraBotones + '<div class="" id="unePuntos"></div>';
        }
        if (numEstilo === "21") {//Crucigrama
            estructuraBotones = estructuraBotones +
                    '<div id="crucigrama_div">' +
                    '<div id="datos">' +
                    '<div class="listaParaCeldas" id="listaHorizontal_div"></div>' +
                    '<div class="listaParaCeldas" id="listaVertical_div"></div>' +
                    '</div>' +
                    '<div class="frameCrucigrama">' +
                    '<div class="celdasCrucigrama">' +
                    '<div class="miCrucigrama" id="miCrucigrama_id"></div>' +
                    '<div class="pieDePaginaCrucigrama" id="pieDePagina_id"></div>' +
                    '<div id="divEstado"></div>' +
                    '</div>' +
                    '</div>    ' +
                    '</div>';
        }
    }
    if (c03id_tipo_pregunta === "7" || c03id_tipo_pregunta === "8") {
        $("#respuestasRadialGroup").removeClass('respuesta').addClass('estilo_resp');   //  clase nueva para pregunta 6 Jon
    } else {
        $("#respuestasRadialGroup").removeClass('estilo_resp').addClass('respuesta');
    }
    if (c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "3") {//Si es Opción Múltiple con Radios Personalizados ó Falso Verdadero con Radios Personalizados        
        if (c03id_tipo_pregunta === "3") {//Si es falso verdadero
            if (obj.pregunta.imagenes === true) {
                estructuraBotones = '<section><form class="ac-custom ac-radio ac-radio2 ac-fill" >' + estructuraBotones + '</form></section>';
            } else {
                estructuraBotones = '<section><form class="ac-custom ac-radio ac-radio2 ac-fill" autocomplete="off">' + estructuraBotones + '</form></section>';
            }
        } else {//Si es de opción múltiple
            estructuraBotones = '<section><form class="ac-custom ac-radio ac-fill no-margen" autocomplete="off"><ul>' + estructuraBotones + '</ul></form></section>';
            if (testBandera === true) {
                alturayEstiloMultiple();
            }
        }
    }
    if (c03id_tipo_pregunta === "2") {////Si es Respuesta Múltiple con Checkboxs Personalizados
        estructuraBotones = '<section><form class="ac-custom ac-checkbox ac-boxfill" autocomplete="off"><ul>' + estructuraBotones + '</ul></form><section>';
    }
    if (c03id_tipo_pregunta === "13") {//Si es Matriz con Radios Personalizados
        estructuraBotones = '<section><form class="ac-custom ac-radio ac-fill formularios" autocomplete="off" ><ul>' + estructuraBotones + '</ul></form></section>';
        estructuraBotones2 = '<section><form class="ac-custom ac-radio ac-fill formularios" autocomplete="off" ><ul>' + estructuraBotones2 + '</ul></form></section>';
    }
    if (c03id_tipo_pregunta === "8") {//Si es Arrastra Cortas
        estructuraBotones = estructuraBotones + '<div id="content" class="large-12 medium-12 small-12 columns"><div id="pilaDeCartas" class="large-12 medium-12 small-12 columns"> </div><div id="pilaDeCartasExtra" class="large-12 medium-12 small-12 columns"> </div><div id="espaciosDeCartas" class="large-12 medium-12 small-12 columns"> </div></div>';
    }
    if (c03id_tipo_pregunta === "18") {//Si es Relaciona Arrastrar
        estructuraBotones = estructuraBotones + '<div id="content" class="large-12 medium-12 small-12 columns"><div id="pilaDeCartas" class="large-12 medium-12 small-12 columns"></div><div id="pilaDeCartasExtra" class="large-12 medium-12 small-12 columns"></div><div id="pilaDeCartasTemporal" class="large-12 medium-12 small-12 columns"></div>';
    }
    if (c03id_tipo_pregunta === "9") {//Si es Arrastra Ordenar        
        estructuraBotones = '<div class="large-2 medium-2 columns" style="visibility:hidden;">.</div><div class="dhe-example-section large-8 medium-8 columns" id="ex-1-1"><div class="dhe-example-section-content"><div id="example-1-1"><ul class="sortable-list">' + estructuraBotones + '</ul></div></div></div><div class="large-2 medium-2 columns" style="visibility:hidden;">.</div>';
    }        
    if(c03id_tipo_pregunta === "18"){//Si es Relaciona Arrastrar
        estructuraHTMLRelacionaArrastrar(estructuraBotones);
    }
    
    if(c03id_tipo_pregunta === "12" || c03id_tipo_pregunta === "11" || c03id_tipo_pregunta === "10" || c03id_tipo_pregunta === "4" || c03id_tipo_pregunta === "13" || c03id_tipo_pregunta === "19" || c03id_tipo_pregunta === "15" || c03id_tipo_pregunta === "30"){//Si es Actividad no evaluable: Click and view
        if(c03id_tipo_pregunta === "13"){//Si es de tipo matriz
            accionesMatriz();
            //if(json[idPreguntaGlobal].pregunta.variante!=='editable'){
            ajustaAlturas();
            //}
        } else if (c03id_tipo_pregunta === "19") {//Si une los puntos               
            estructuraHTMLuneLosPuntos(estructuraBotones);
        } else if (c03id_tipo_pregunta === "15") {//Si es un crucigrama
            estructuraHTMLcrucigrama(estructuraBotones);
        } else if (c03id_tipo_pregunta === "12" || c03id_tipo_pregunta === "11") {//Si es relacionar columnas o relacionar números.
            estructuraHTMLrelacionarColumnas(estructuraBotones, estructuraBotones2);
        } else if (c03id_tipo_pregunta === "30") {//Si es Actividad no evaluable: Click and view
            //estructuraHTMLrelacionarColumnas(estructuraBotones, estructuraBotones2);
            estructuraHTMLclickAndView(estructuraBotones);
            console.log('Crea la estructura');
        }else {//si es relaciona combo o número ordenar     
            $("#respuestasRadialGroup").html('<div id="izquierda"></div><div id="derecha"></div>');
            $("#izquierda").html(estructuraBotones);
            $("#derecha").html(estructuraBotones2);
        }
    } else if (c03id_tipo_pregunta === "5") {
        estructuraHTMLarrastraContenedor(estructuraBotones);
    } else {
        //REVISAR EN TODOS LOS TIPOS
        if (c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "2") {//Si es de opción múltiple o respuesta múltiple
            estructuraHTMLopcionMultiple(estructuraBotones);
        }
        if (c03id_tipo_pregunta === "3") {//Si es falso verdadero
            estructuraHTMLfalsoVerdadero(estructuraBotones);
        }
        if (c03id_tipo_pregunta === "16") {//Si es sopa de letras
            estructuraHTMLsopaDeLetras(estructuraBotones);
        }
        if (c03id_tipo_pregunta === "8") {//Si es arrastra cortas
            estructuraHTMLarrastraCortas(estructuraBotones);
        }
        if (c03id_tipo_pregunta === "6") {//Si es Pregunta abierta            
            estructuraHTMLpreguntaAbierta(estructuraBotones);
        }
        numEstilo === "4"

        $("#respuestasRadialGroup").height('auto');
        if (c03id_tipo_pregunta === "9") {
            $("#respuestasRadialGroup").append('<a href="#" onclick="siguientePregunta(' + c03id_tipo_pregunta + ')">Revisar</a>');
        }
        if (c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "2" || c03id_tipo_pregunta === "3") {
            siguientePregunta(c03id_tipo_pregunta);
            if (c03id_tipo_pregunta === "3") {//Si es Falso Verdadero
                falsoVerdadero();
            } else if (c03id_tipo_pregunta === "1" && testBandera === true) {
                banderaMultiple();
            } else {//Si es opción múltiple o respuesta múltiple
                if (c03id_tipo_pregunta === "2") {
                    estilos2Multiple();
                }
                estilos1Multiple();
                if (c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "2") {
                    accionesMultiple();
                }
            }
        }
    }
    if (c03id_tipo_pregunta !== "19") {
        if (vuelta === 0) {//Sólo crea la barra la primera vez
            if (c03id_tipo_pregunta === "8") {//Si es arrastra cortas                
                }
            if (c03id_tipo_pregunta === "5") {
                $("#barraCreada").css("top", $(window).height() - $("#barraCreada").height());
            }
        }
        vuelta++;
    }
}
function eliminaAcentos(texto) {
    if (typeof texto !== 'undefined') {
        texto = texto.replace('&aacute;', 'a');
        texto = texto.replace('&eacute;', 'e');
        texto = texto.replace('&iacute;', 'i');
        texto = texto.replace('&oacute;', 'o');
        texto = texto.replace('&uacute;', 'u');

        texto = texto.replace('á', 'a');
        texto = texto.replace('é', 'e');
        texto = texto.replace('í', 'i');
        texto = texto.replace('ó', 'o');
        texto = texto.replace('ú', 'u');
    }
    return texto;
}
function eliminarParrafos(texto) {
    try{
       if (typeof texto !== 'undefined') {
        texto = texto.replace('<p>', '');
        texto = texto.replace('<\/p>', '');
    }
    }catch(e){
        console.log(e);
    }
    return texto; 
    
}
function crearDivs() {
    var cadenaTemp;
    if (numEstilo === "2") {
        cadenasParrafo();
    }
    if (c03id_tipo_pregunta === "13") {//Si es una pregunta de tipo Matriz        
        estilosMatriz();
    }
    if (numEstilo === "1" || numEstilo === "5") {
        var sinParrafo = respuestasArr[j];
        sinParrafo = sinParrafo.replace("<p>", "");//Se elimina la etiqueta del párrafo
        sinParrafo = sinParrafo.replace("</p>", "");//
        respuestasArr[j] = sinParrafo;
    }
    if (numEstilo === "2" || numEstilo === "3" || numEstilo === "8" || numEstilo === "7" || numEstilo === "9" || numEstilo === "14" || numEstilo === "16") { // Revisar estilo 14 y 16
        inciso = "";
    } else {
        inciso = "";
    }
    if (c03id_tipo_pregunta === "7") {//Si es Pregunta Corta Horizontal.
        t13respuesta = preguntasArr[j] + "\u00a0\u00a0";

        if (primeraVezEspacios === false) {
            t13respuesta = "\u00a0\u00a0" + preguntasArr[j] + "\u00a0\u00a0";
        }
        primeraVezEspacios = false;
    }
    if (c03id_tipo_pregunta === "11" || c03id_tipo_pregunta === "10" || c03id_tipo_pregunta === "4") {//Si es de Relacionar Columnas con cajitas ó relaciona Combo tomas los valores del arreglo temporal
        t13respuesta = preguntasArr[j];
        $('#respuesta' + ((j + 1) + cuantosBotonesDeRespuestaLocal) + '_btn').html(respuestasArr[j]);
        if (c03id_tipo_pregunta === "4") {
            accionesRelacionaCombo();
        }
    }
    if (c03id_tipo_pregunta !== "7" && c03id_tipo_pregunta !== "11"  && c03id_tipo_pregunta !== "30") {//Si no es Actividad no evaluable: Click and view
        respuestaTemporal = respuestasArr[j];
        if (c03id_tipo_pregunta === "3") {//Si es Verdadero-Falso
            respuestaTemporal = '';//Se limpia la variable porque se seteará con el content
        }
        if (typeof respuestaTemporal !== 'undefined') {//Sólo aplica si respuestaTemporal es diferente de undefined
            respuestaTemporal = respuestaTemporal.replace('<img ', '<img id="imagen' + (j + 1) + '" ');
            respuestasArr[j] = respuestaTemporal;
        }
        if (json[idPreguntaGlobal].pregunta.respuestaImagen && inciso + respuestasArr[j].search(".png") > 0) {
            var elementoLista="", contEL=j+1;
            if (json[idPreguntaGlobal].pregunta.respuestaUnicaMultiple === true) {
                elementoLista=$(".listaTarjetas_"+contEL);
            }else{
                elementoLista=$('#respuesta' + (j + 1) + '_btn').parent()
            }
            if (strAmbiente === 'DEV') {
                elementoLista.css({backgroundImage: 'url("../../../../' + strRutaLibro + inciso + respuestasArr[j] + '")',
                    backgroundRepeat: "no-repeat", backgroundSize: "100% 100%"});
                /*$("#columnaContenedores").css({backgroundImage: 'url("../../../../' + strRutaLibro + obj.pregunta.url + '")',
                 backgroundRepeat: "no-repeat"});*/
            } else {
                elementoLista.css({backgroundImage: 'url(' + inciso + respuestasArr[j] + ')', 
                backgroundRepeat: "no-repeat", backgroundSize: "100% 100%"});
            }
            if (json[idPreguntaGlobal].contenedores.length === 1) {
                elementoLista.css({backgroundSize: "auto 100%", backgroundColor: "transparent", backgroundPosition: '50%'});
            }

        } else {
            var value=j+1;            
            if($('li div[value="'+value+'"]').size() && respuestasRandom){
                if(json[idPreguntaGlobal].respuestas[j].etiqueta!==undefined){
                    $('#respuesta'+value+'_btn').html(inciso + respuestasArr[j]);   
                }else{
                    $('li div[value="'+value+'"]').html(inciso + respuestasArr[j]);                                    
                }
            }else{
                $('#respuesta' + (j + 1) + '_btn').html(inciso + respuestasArr[j]);
            }
            $('#li' + (j + 1) + ' span p').width(362);
        }
        var idImagen = '#imagen' + (j + 1);
        var imagenRespuesta = $(idImagen).attr('src');
        $('#imagen' + (j + 1)).load(function (e) {
            imagenesCargadas++;
            if (imagenesCargadas === respuestasArr.length) {//Si se cargaron todas las imágenes
                redimensionaAlturaDivPrincipal();//Se emplea para volver a ajustar la altura del contenedor principal (respuestasRadialGroup) una vez cargadas todas las imágenes del rectivo
            }
        }).error(function () {
            //Error al cargar la imagen
        }).attr('src', imagenRespuesta);
    } else {
        $('#respuesta' + (j + 1) + '_btn').html(inciso + preguntasArr[j]);
    }
    if (c03id_tipo_pregunta === "7" && c03id_tipo_pregunta === "11") {
        $('#respuesta' + (j + 1) + '_btn').find('img').addClass('tam-imagen-relacionar');
    }
    if (c03id_tipo_pregunta === "3") {//Si es falso verdadero
        estilosFalsoVerdadero();
    }

    if (c03id_tipo_pregunta === "12") {
        preguntaRelacionarColumnas();
        $(".bottom").css({borderTop:"1px solid #FFFAFA", paddingTop: "4px"});
    }
    if (c03id_tipo_pregunta === "9") {
        preparaSorteable();
    }//Arrastra Ordenar
    if (c03id_tipo_pregunta === "8") {
        creaArrastraCortas();
    }//Arrastra Cortas
    if (c03id_tipo_pregunta === "14") {
        rompecabezasMain();
    }//Rompecabezas
    if (c03id_tipo_pregunta === "16") {//Sopa de letras
        funcionesSopa();
        leeInformacionBds();
    }
    if (c03id_tipo_pregunta === "30") {//Si es Actividad no evaluable: Click and view
        console.log('Llama accionesClicAndView');
        accionesClicAndView( json[idPreguntaGlobal].pregunta.url );
    }
}//fin de la función
function restrictCharacters(myfield, e, restrictionType) {
    if (!e)
        var e = window.event
    if (e.keyCode)
        code = e.keyCode;
    else if (e.which)
        code = e.which;
    var character = String.fromCharCode(code);
    if (code == 27) {
        this.blur();
        return false;
    }//Si el usuario presiona ESC se quita el foco
    if (!e.ctrlKey && code != 9 && code != 8 && code != 36 && code != 37 && code != 38 && (code != 39 || (code == 39 && character == "'")) && code != 40) {//Para ignorar si el usuario presiona otras teclas
        if (character.match(restrictionType)) {
            return true;
        } else {
            return false;
        }
    }
}
function circuloSiguiente() {
    var numPregunta = parseInt(numeroPreguntaGlobal);
    var preguntasTotal;

    if (c03id_tipo_pregunta === "8") {//Arrastra Cortas
        if (typeof obj.pregunta.textosLargos === 'undefined' || obj.pregunta.textosLargos === 'no') {
            preguntasTotal = (obj.preguntasTotal).length;
        } else {
            preguntasTotal = obj.preguntas.length;//Si contiene muchos textos largos
            numeroPreguntaGlobal++;
            numPregunta = numPregunta + 1;
        }
    } else {
        preguntasTotal = (obj.preguntasTotal).length;
    }

    for (var g = 1; g <= preguntasTotal; g++) {
        if (numPregunta === g) {
            $('#circulo' + g).addClass('circulo-activo').css({backgroundColor: book_color});// Se marca como activo el circulo de la pregunta actual
            $('#circulo' + g).removeClass('circulo-inactivo');
            $('#circulo' + g).removeClass('circulo');
        } else if (numPregunta < g) {
            $('#circulo' + g).removeClass('circulo-activo');
            $('#circulo' + g).addClass('circulo-inactivo');// Quedan inactivos los circulos de las preguntas que no se han contestado
            $('#circulo' + g).removeClass('circulo');
        } else {
            $('#circulo' + g).removeClass('circulo-activo');
            $('#circulo' + g).removeClass('circulo-inactivo');
            $('#circulo' + g).addClass('circulo');// Se marcan como resueltos los circulos que ya se han contestado
        }
    }
}
function cambiarRutasImagenes() {
    imgFaltante = '';
    if (strAmbiente === 'DEV') {        
        $('#fondoPregunta img').each(function (index) {            
            if(!$(this).hasClass('rutaEditada')){                
                $(this).attr('src', '../../../../' + strRutaLibro + $(this).attr('src'));                
                $(this).addClass('rutaEditada');
            }
        });
    }
}
function contieneImagen() {
    return $('#fondoPregunta img').size() ? true : false;
}

function aplicaEstilosFraccion(){
    var i=0;
    $('sup').each(function(){
        console.log('Next: ' + $(this).next().length );
        i=i+$(this).next().length;
    });
    if(i>0){
        $( "<span class='fraction'></span>" ).insertBefore( $('sup') );  

        $('.fraction').each(function (key, value) {
            $this = $(this);
            $parent = $(this).parent();
            //var split = $this.html().split("/");
            //if (split.length === 2) {
            $this.html('<span class="top">' + $parent.find('sup').html() + '</span><span class="bottom">' + $parent.find('sub').html() + '</span>');
            $this.next().remove();
            $this.next().remove();        
            //$parent.html($this);
            //}
            //$('.fraction span, .fraction').css({border: 'none', background:'transparent', color: 'rgba(0, 0, 0, 0.9)'});
        });

        $('.fraction').css( {display: 'inline-block', textAlign: 'center'});
        $('.top, .bottom, .fraction').css( {padding: '0 5px'});
        $('.bottom').css( {borderTop: '1px solid #000', display: 'block'}); 
    }
}

function eliminaTextos() {
    if (json[idPreguntaGlobal].pregunta.secuencia === true) {
        $('.btn_preg').each(function () {
            $(this).html('');
        });
    }
}
function numerosRandom(total){
    var serieTotal=[], arrayRandom=[], indexRandom;
    for(var i=0; i<total; i++){
        serieTotal[i]=i;
    } 
    $.each(serieTotal, function (index, element) {
        indexRandom = Math.floor((Math.random() * serieTotal.length));
        arrayRandom[index]=serieTotal[indexRandom];
        delete serieTotal[indexRandom];
        serieTotal.splice(indexRandom, 1);
    });
    return arrayRandom;
}

function shuffleArray(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

$(window).load(function(){
    recargarImagenes();
//    agrupaMultipleGroup();
});

function recargarImagenes(){
    var allImg=$("#fondoPregunta img");
    var allBackgroundImg=$("#fondoPregunta *[style*='url(']");
    
    $.each(allImg, function(index, element){
        var lastImg=$(element).attr("src").split("/");
        for(var i=0; i<lastImg.length; i++){
            // if(lastImg[i].search("png")!==-1){
            if(lastImg[i].search("png")!==-1 || lastImg[i].search("gif")!==-1){
                lastImg="../../../../EVAL/"+lastImg[i];
                break;
            }
        }
        $(element).attr("src", lastImg);
    });
    
    $.each(allBackgroundImg, function(index, element){
        var lastBackground=$(element).css("background").split("/");
        for(var i=0; i<lastBackground.length; i++){
            if((lastBackground[i]).search("png")!==-1){
                var longitudIdImagen=(lastBackground[i].split(".png")[0]).length;
                lastBackground=lastBackground[i].substring(0,longitudIdImagen+4);
                break;
            }            
        }
        var newBackground='url("../../../../EVAL/'+lastBackground+'")';
        $(element).css({'background-image': newBackground});
    });
    
    setTimeout(function(){
        if(c03id_tipo_pregunta === "8"){//reajusta el tamaño del espacio para arrastrar tarjetas
            var contentHeight;
            contentHeight = document.getElementById("fondoPregunta").scrollHeight;
            contentHeight = contentHeight > ($("body").height() - 40) ? contentHeight : contentHeight-25;
            
            $("#content").height(contentHeight);             
        }            
    }, 250);
}


///Funciones para responder actividades de acuerdo a una cadena
//$(document).ready(function(){
//    if(c03id_tipo_pregunta==="1" || c03id_tipo_pregunta==="2"){respondeMultiples("1,3");}//
//    if(c03id_tipo_pregunta==="13"){respondeMatriz("0_4,3,1_5");}//
//    if(c03id_tipo_pregunta==="18"){respondeLineas("0_1,2_2");}//
//});


$(window).load(function(){
    if(strAmbiente === 'DEV'){
        if(idActividad.charAt(3)==="E"){
            recargarImagenes("../../../../EVAL/");
        }else{
            recargarImagenes('../../../../'+strRutaLibro);
        }        
    }   
//     if(c03id_tipo_pregunta==="8"){respondeCorta(",4,2");}
});    


function respondeMultiples(cadena) {
        cadena = cadena.split(",");
        $.each(cadena, function (index, element) {
            $("#respuesta" + element + "_btn").addClass("btn_preg_press");
        });
    $(".btn_preg_press").css({background: "#00E7FF"});
}

var elementosResueltos=true;
function respondeCorta(cadena) {
    if(elementosResueltos){
        cadena = cadena.split(",");
        var posX; var posY;
        $.each(cadena, function (index, element) {
            if (element !== "") {
                posX = -1 * $("#card" + element).position().left;
                posY=""+(($("#texto" + (index + 1)).position().top)-($("#card" + element).position().top));
                posY=posY.charAt(0)==="-"? -1*posY : posY*1;
                $("#card" + element).addClass("dropped dropped-text");
                $("#card" + element).position({of: $("#texto" + (index + 1)), my: 'left top', at: 'left top'})
                $("#texto" + (index + 1)).addClass("hasCard card" + element);
            }
        });       
        elementosResueltos=false;
    }  
}

function respondeMatriz(cadena){
    cadena=cadena.split(",");
    $.each(cadena, function(i, e1){
        var cadenaElementos=e1.split("_");
        $.each(cadenaElementos, function(j, e2){
            if(e2!==""){
                $("#derecha"+i+" .tituloColumnaMatriz:nth-child("+e2+") .labelCheck").addClass("img-click");   
            }
        });
    });    
}

function respondeLineas(cadena){
    cadena=cadena.split(",");
    $.each(cadena, function(index, element){
        var cadenaElementos=element.split("_");
        resuelveLineas( cadenaElementos[0], cadenaElementos[1])
    });
}

function resuelveLineas( izquierda, derecha) {
    var valor1;
    var valor2;
    var numeroId = 0;
    var cajaId = '';
    var cadena1 = "cajaIzquierdaVacia"+izquierda;
    var cadena2 = "cajaDerecha"+derecha;
    var droppableVacio2 = "cajaIzquierdaVacia"+izquierda;
    var datoFila1;
    var datoFila2;
    var cajacontigua;
    var cajaContraria;
    var cajaDroppada;
    var cajaVaciaDesplazada;
    var datoContiguo;
    var cajaADesplazar;
    var cajaUnida;
    var idAcomparar;
    var vacioTemporal;
        
    cadena1 = cadena1.replace('Vacia', '');
    if(cadena1.substring(0,5) === cadena2.substring(0,5)){
        hizoDrop = false;
        if($( "#"+cadena1 ).hasClass('shadow2')){
        }
    }else{
        if(cadena1.substring(0,5) === 'cajaD'){
            valor1 = posXUnidaIzquierda+'px';     valor2 = posXUnidaDerecha+'px';
            zIndexPieza ( '#'+cadena1, '3' );
            zIndexPieza ( '#'+cadena2, '4' );
            cajaId = 'cajaIzquierda';
            numeroId = cadena1.substring(11,cadena1.length);
            numeroId = +$( '#'+cadena1 ).attr('haceLinea1');
        }else{
            valor1 = posXUnidaDerecha+'px';    valor2 = posXUnidaIzquierda+'px';
            zIndexPieza ( '#'+cadena1, '4' );
            zIndexPieza ( '#'+cadena2, '3' );
            cajaId = 'cajaDerecha';
            numeroId = cadena1.substring(13,cadena1.length);
            numeroId = +$( '#'+cadena1 ).attr('haceLinea2');
        }
        $("#cajaDerecha"+derecha).position( { of: $("#cajaIzquierdaVacia"+izquierda), my: 'left top', at: 'left top' } );
        $("#cajaDerecha"+derecha).draggable( 'option', 'revert', false );
        $( '#'+cadena1).draggable( 'option', 'revert', false );
        datoFila1 = $("#cajaDerecha"+derecha).attr('cualFila');
        datoFila2 = $( '#'+cadena1).attr('cualFila');
        $("#cajaDerecha"+derecha).attr('ligados', datoFila1+'-'+datoFila2);
        $( '#'+cadena1).attr('ligados', datoFila2+'-'+datoFila1);
        $( '#'+cajaId+numeroId).attr('cualFila', datoFila1);
        datoFila1 = numeroId;
        $("#cajaDerecha"+derecha).attr('cualFila', datoFila1);
            
        cajaDroppada = droppableVacio2.replace('Vacia', '').replace('Vacia', '');
        var cajaDraggadaVacia = $("#cajaDerecha"+derecha).attr('id');
        cajaDraggadaVacia = cajaDraggadaVacia.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        var reemplazadaActualizadaLeft = $( '#'+cajaId+numeroId ).data('originalLeft');
        var reemplazadaActualizadaTop = $( '#'+$("#cajaDerecha"+derecha).attr('id') ).data('originalTop');
        $( '#'+cajaId+numeroId ).css({left:reemplazadaActualizadaLeft, top:reemplazadaActualizadaTop, position:'absolute'});
        $( '#'+cajaId+'Vacia'+numeroId ).css({left:reemplazadaActualizadaLeft, top:reemplazadaActualizadaTop, position:'absolute'});
        var draggadaActualizadaLeft = $( '#'+$("#cajaDerecha"+derecha).attr('id') ).data('originalLeft');
        var draggadaActualizadaTop = $( '#'+cajaDroppada ).data('originalTop');
        $( '#'+$("#cajaDerecha"+derecha).attr('id') ).css({left:draggadaActualizadaLeft, top:draggadaActualizadaTop, position:'absolute'});
        var draggadaActualizadaLeft = $( '#'+$("#cajaDerecha"+derecha).attr('id') ).data('originalLeft');
        var draggadaActualizadaTop = $( '#'+cajaDroppada ).data('originalTop');
        $( '#'+cajaDraggadaVacia ).css({left:draggadaActualizadaLeft, top:draggadaActualizadaTop, position:'absolute'});
        if(droppableVacio2.substring(0,5) === 'cajaD'){
            cajaUnida = '#cajaDerecha'+$("#cajaDerecha"+derecha).attr('haceLinea2');
            idAcomparar = $("#cajaDerecha"+derecha).attr('haceLinea2');
            posicionaPieza( '#cajaDerecha'+idAcomparar, $( '#cajaDerecha'+idAcomparar ).data('originalLeft') );
            $( '#'+cajaId+numeroId).attr('haceLinea1', numeroId);
            datoContiguo = $( '#'+ $("#cajaDerecha"+derecha).attr('id') ).attr('haceLinea2');
            $( '#'+cajaId+numeroId).attr('haceLinea2', datoContiguo);
            datoContiguo = $( $("#cajaDerecha"+derecha) ).attr('haceLinea1');
            $( '#'+cajaDroppada ).attr('haceLinea1', datoContiguo);
            $( $("#cajaDerecha"+derecha) ).attr('haceLinea1', $("#cajaDerecha"+derecha).data('number'));
            datoContiguo = $( '#'+cajaDroppada ).attr('haceLinea2');
            $( $("#cajaDerecha"+derecha) ).attr('haceLinea2', datoContiguo);
            cajaVaciaDesplazada = $("#cajaDerecha"+derecha).attr('id');
            cajaVaciaDesplazada = cajaVaciaDesplazada.replace('Izquierda', 'IzquierdaVacia').replace('Derecha', 'DerechaVacia');
            cajaADesplazar = $( '#'+cajaId+numeroId ).attr('haceLinea1');
            cajacontigua = $( '#'+cajaId+numeroId ).attr('haceLinea2');
            cajaContraria = 'cajaDerecha'+cajacontigua;
            $( '#'+cajaContraria ).attr('haceLinea1', cajaADesplazar);
        }else{
            cajaUnida = '#cajaDerecha'+$("#cajaDerecha"+derecha).attr('haceLinea1');
            idAcomparar = $("#cajaDerecha"+derecha).attr('haceLinea1');
            posicionaPieza( '#cajaIzquierda'+idAcomparar, $( '#cajaIzquierda'+idAcomparar ).data('originalLeft') );
            $( '#'+cajaId+numeroId).attr('haceLinea2', numeroId);
            datoContiguo = $( '#'+ $("#cajaDerecha"+derecha).attr('id') ).attr('haceLinea1');
            $( '#'+cajaId+numeroId).attr('haceLinea1', datoContiguo);
            datoContiguo = $( $("#cajaDerecha"+derecha) ).attr('haceLinea2');
            $( '#'+cajaDroppada ).attr('haceLinea2', datoContiguo);
            $( $("#cajaDerecha"+derecha) ).attr('haceLinea2', $("#cajaDerecha"+derecha).data('number'));
            datoContiguo = $( '#'+cajaDroppada ).attr('haceLinea1');
            $( $("#cajaDerecha"+derecha) ).attr('haceLinea1', datoContiguo);
            cajaVaciaDesplazada = $("#cajaDerecha"+derecha).attr('id');
            cajaVaciaDesplazada = cajaVaciaDesplazada.replace('Izquierda', 'IzquierdaVacia').replace('Derecha', 'DerechaVacia');
            cajaADesplazar = $( '#'+cajaId+numeroId ).attr('haceLinea2');
            cajacontigua = $( '#'+cajaId+numeroId ).attr('haceLinea1');
            cajaContraria = 'cajaIzquierda'+cajacontigua;
            $( '#'+cajaContraria ).attr('haceLinea2', cajaADesplazar);
        }
        actualizaPosiciones ( '#'+cajaId+numeroId , reemplazadaActualizadaLeft, reemplazadaActualizadaTop);
        actualizaPosiciones ( '#'+cajaId+'Vacia'+numeroId , reemplazadaActualizadaLeft, reemplazadaActualizadaTop);
        actualizaPosiciones ( '#'+$("#cajaDerecha"+derecha).attr('id') , draggadaActualizadaLeft,  draggadaActualizadaTop);
        actualizaPosiciones ( '#'+cajaDraggadaVacia , draggadaActualizadaLeft,  draggadaActualizadaTop);
        posicionaPieza( '#'+$("#cajaDerecha"+derecha).attr('id'), valor1 );
        posicionaPieza( '#'+cadena1, valor2 );
        aplicaSombra( '#'+$("#cajaDerecha"+derecha).attr('id') );
        aplicaSombra( '#'+cadena1 );
        vacioTemporal = $("#cajaDerecha"+derecha).attr('id');
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        opacarPieza( '#'+vacioTemporal );
        vacioTemporal = cadena1, valor2;
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        opacarPieza( '#'+vacioTemporal );
        pintaPieza( "#"+cadena1, colorNormalPieza );
        hizoDrop = true;                        
        activaGuardado = true;        
    }
}
function respondeActividad(respuestaUsuario){
    var elementosRespondidos = respuestaUsuario.split(','), numContenedor;
    var idLi = "";
    if(json[idPreguntaGlobal].pregunta.c03id_tipo_pregunta === "5"){
        var ordenaFondoImagenEtiquetas=json[idPreguntaGlobal].pregunta.tipo==='ordenar' && json[idPreguntaGlobal].pregunta.imagen===true && json[idPreguntaGlobal].respuestas[0].etiqueta !==undefined ? true:false; //etiquetas
        var ordenaFondoImagenTexto = json[idPreguntaGlobal].pregunta.tipo==='ordenar' && json[idPreguntaGlobal].pregunta.imagen===true && json[idPreguntaGlobal].respuestas[0].etiqueta === undefined ? true:false;
        var arrastraImagen = json[idPreguntaGlobal].pregunta.tipo==='ordenar' && json[idPreguntaGlobal].pregunta.imagen===true && json[idPreguntaGlobal].respuestas[0].etiqueta ===undefined ? true:false;
        if(json[idPreguntaGlobal].pregunta.tipo === "vertical" || json[idPreguntaGlobal].pregunta.tipo === "horizontal"){
            elementosRespondidos = "2,4,5,3,1,".split(',');
            console.log('respVertical'+respuestaUsuario);
                    for(var i=0; i<elementosRespondidos.length; i++){
                        if (elementosRespondidos[i] !== "") {
                            $('#li'+(i+1)).css('display', 'none');
                            idLi = i+1;
                            $('.contenedorRespuesta').eq(elementosRespondidos[i]).html('<div class="dragRespuesta" value='+idLi+'><div class="imgLineas"></div><div class="textoCorrecta combos" value=' + (i+1) + '>' + json[idPreguntaGlobal].respuestas[i].t13respuesta + '</div><a href="#" class="revertir">X</a></div>');
                            $('.contenedorRespuesta').eq(elementosRespondidos[i]).droppable('disable');
                        }
                    }
            $('.dragRespuesta *').css({margin:0, padding:0});
            //$('.dragRespuesta').css({height: 'inherit', padding: 0, margin: 0, width: '100%', display: 'inline-table', verticalAlign: 'top'});
            $('.textoCorrecta').css({width: '337px', height: 'inherit', display: 'inline-table', verticalAlign: 'top'});
            $('.contenedorRespuesta').css({display:'table'});
            $('.dragRespuesta').draggable({revert: true, revertDuration: 1000, containment: "document", cursor: "move", zIndex: 100,
                start: function (event, ui) {
                    dragElement = $(this);
                    if (json[idPreguntaGlobal].pregunta.imagen) {

                        dragColor = dragElement.css("background-color");
                    }
                },
                scroll: false
            });
            coloresRandom('.dragRespuesta');
        }else if(json[idPreguntaGlobal].contenedoresFilas !== undefined && json[idPreguntaGlobal].pregunta.tipo !=='matrizHorizontal'){
            console.log('respuesta matriz: '+respuestaUsuario);
            elementosRespondidos = ",,,,,,,".split(',');
            var contColumna = 0, contFila=0;
            $('.subContenedor').each(function(){
               $(this).find('.contenedorRespuesta').each(function(){
                   $(this).attr('coordenada', contColumna+'_'+contFila);
                   contFila++;
               });
               contColumna++;
               contFila = 0;
            }); 
            for(var i=0; i<elementosRespondidos.length; i++){
                $('.contenedorRespuesta').each(function(){
                   if($(this).attr('coordenada') === elementosRespondidos[i]){
                       idLi = i+1;
                       $('#li'+(i+1)).css('display', 'none');
                       $(this).html('<div class="dragRespuesta" value='+idLi+'><div class="imgLineas"></div><div class="textoCorrecta combos" value=' + (i+1) + '>' + json[idPreguntaGlobal].respuestas[i].t13respuesta + '</div><a href="#" class="revertir">X</a></div>');
                       $(this).find('.dragRespuesta').css('background', $('#li'+(i+1)).css('background'));
                       $(this).droppable('disable');
                   }
                });
            }
            agregaEstilosArrastraContenedor();
        }else if(json[idPreguntaGlobal].pregunta.tipo==='matrizHorizontal'){
            elementosRespondidos = ",0_1,1_0,1_1,2_0,2_1,3_0,3_1".split(',');
            $('.mostrarRespuesta').css({display:'none'});
            var contColumna = 0, contFila=0;
            $('.subContenedor').each(function(indexSub, subCont){
               $(this).find('.contenedorMatriz').each(function(indexCM, CM){
                   $(this).attr('coordenada', indexSub+'_'+indexCM);
               });
            });
            for(var i=0; i<elementosRespondidos.length; i++){
                $('.contenedorMatriz').each(function (index, elemet) {
                    if ($(this).attr('coordenada') === elementosRespondidos[i] && !$(this).hasClass('mostrar')) {
                        idLi = i+1;
                        $('#li' + (i + 1)).css('display', 'none');
                        $(this).html('<div class="dragRespuesta" value='+idLi+'><div class="imgLineas"></div><div class="textoCorrecta combos" value=' + (i + 1) + '>' + json[idPreguntaGlobal].respuestas[i].t13respuesta + '</div><a href="#" class="revertir">X</a></div>');
                        $(this).droppable('disable');
                    }
                });
            }
            agregaEstilosArrastraContenedor();
            $('.dragRespuesta p').css({color:'#000'});
        }
        else if(json[idPreguntaGlobal].pregunta.tipo === 'ordenar'){
            console.log('O');
            if(ordenaFondoImagenEtiquetas){
                console.log('et');
                elementosRespondidos = "0,1,2,3,4".split(','), numContenedor;
                for(var i=0; i<elementosRespondidos.length; i++){
                    if (elementosRespondidos[i] !== "") {
                        idLi = i+1;
                        $('#li' + (i + 1)).css('display', 'none');
                        $(".subContenedor").eq(elementosRespondidos[i]).find('.contenedorRespuesta').html('<div class="dragRespuesta" value='+idLi+'><div class="imgLineas"></div><div class="textoCorrecta combos" value=' + (i + 1) + '>' + json[idPreguntaGlobal].respuestas[i].etiqueta + '</div><a href="#" class="revertir">X</a></div>');
                        $('.contenedorRespuesta').eq(elementosRespondidos[i]).droppable('disable');
                    }
                }
            }else if(arrastraImagen){
                console.log('aI');
                elementosRespondidos = "0,1,2,3,4,5,6,7,8,9,10,11,".split(','), numContenedor;
                for(var i=0; i<elementosRespondidos.length; i++){
                    if (elementosRespondidos[i] !== "") {
                        idLi = i+1;
                        $('#li' + (i + 1)).css('display', 'none');
                        $(".subContenedor").eq(elementosRespondidos[i]).find('.contenedorRespuesta').html('<div class="dragRespuesta fondoImg" value='+idLi+' style="background-"><div class="textoCorrecta combos" value=' + (i + 1) + '></div><a href="#" class="revertir">X</a></div>');
                        $(".subContenedor").eq(elementosRespondidos[i]).find('.dragRespuesta').css({backgroundImage:'url("'+json[idPreguntaGlobal].respuestas[i].t13respuesta+'")', backgroundRepeat:'no-repeat', backgroundSize:'100% 100%'});
                        $('.contenedorRespuesta').eq(elementosRespondidos[i]).droppable('disable');
                    }
                }
            }else{
                elementosRespondidos = "0,1,2,3,4".split(',');
                for(var i=0; i<elementosRespondidos.length; i++){
                    if (elementosRespondidos[i] !== "") {
                        idLi = i+1;
                        $('#li' + (i + 1)).css('display', 'none');
                        $(".subContenedor").eq(elementosRespondidos[i]).find('.contenedorRespuesta').html('<div class="dragRespuesta" value='+idLi+'><div class="imgLineas"></div><div class="textoCorrecta combos" value=' + (i + 1) + '>' + json[idPreguntaGlobal].respuestas[i].t13respuesta + '</div><a href="#" class="revertir">X</a></div>');
                        $(".subContenedor").eq(elementosRespondidos[i]).find('.dragRespuesta').css({background:$("#li"+(i + 1)).css('background')});
                        $('.contenedorRespuesta').eq(elementosRespondidos[i]).droppable('disable');
                    }
                }
            }
            agregaEstilosArrastraContenedor();
        }
    }else if(obj.pregunta.c03id_tipo_pregunta === "15"){
        console.log("cadena crucigrama");
        var arrayCoordenadas = obj.pocisiones;
        var palabras = respuestaUsuario.split('_');
        $.each(arrayCoordenadas, function (index, element) {
            var longPalabra = eliminarParrafos(obj.respuestas[index].t13respuesta).length;
            var posicionX, posicionY, idCelda;
            var letrasPalabra = palabras[index].split(',');
            for (var i = 0; i < longPalabra; i++) {
                if (element.direccion === 1) {
                    posicionY = element.datoY + i;
                    idCelda = "#elementoEntrada" + element.datoX + "_" + posicionY;
                } else {
                    posicionX = element.datoX + i;
                    idCelda = "#elementoEntrada" + posicionX + "_" + element.datoY;
                }
                $(idCelda).val(letrasPalabra[i]);
            }
        });
        $('table *').css({height:'10px !important', width:'10px !important', fontSize:'10px !important'});
    }else if(obj.pregunta.c03id_tipo_pregunta === "1" || obj.pregunta.c03id_tipo_pregunta === "2"){
        cadena = cadena.split(",");
        $.each(cadena, function (index, element) {
            $("#respuesta" + element + "_btn").parent().parent().addClass("btn_preg_press");
        });
        $(".btn_preg_press").css({background: "#00E7FF"});
    }else if(obj.pregunta.c03id_tipo_pregunta === "8"){
        cadena = cadena.split(",");
        var posX;
        var posY;
        $.each(cadena, function (index, element) {
            if (element !== "") {
                posX = -1 * $("#card" + element).position().left;
                posY = "" + (($("#texto" + (index + 1)).position().top) - ($("#card" + element).position().top));
                posY = posY.charAt(0) === "-" ? -1 * posY : posY * 1;
                $("#card" + element).addClass("dropped dropped-text");
                $("#card" + element).position({of: $("#texto" + (index + 1)), my: 'left top', at: 'left top'})
                $("#texto" + (index + 1)).addClass("hasCard card" + element);
            }
        }); 
    }else if(obj.pregunta.c03id_tipo_pregunta === "13"){
        cadena = cadena.split(",");
        $.each(cadena, function (i, e1) {
            var cadenaElementos = e1.split("_");
            $.each(cadenaElementos, function (j, e2) {
                if (e2 !== "") {
                    $("#derecha" + i + " .tituloColumnaMatriz:nth-child(" + e2 + ") .labelCheck").addClass("img-click");
                }
            });
        }); 
    }else if(obj.pregunta.c03id_tipo_pregunta === "18"){
        respondeLineas(respuestaUsuario);
    }
    eventoRevertirClick();
}

function agregaEstilosArrastraContenedor(){
    $('.dragRespuesta *').css({margin:0, padding:0});
            //$('.dragRespuesta').css({height: 'inherit', padding: 0, margin: 0, width: '100%', display: 'inline-table', verticalAlign: 'top'});
            $('.textoCorrecta').css({width: '88%', height: 'inherit', display: 'inline-table', verticalAlign: 'top'});
            //$('.contenedorRespuesta').css({display:'inline-table'});
            $('.dragRespuesta').draggable({revert: true, revertDuration: 1000, containment: "document", cursor: "move", zIndex: 100,
                start: function (event, ui) {
                    dragElement = $(this);
                    if (obj.pregunta.imagen) {

                        dragColor = dragElement.css("background-color");
                    }
                },
                scroll: false
            });
            //coloresRandom('.dragRespuesta');
            $('.dragRespuesta p').css({textAlign:'left'});
}
