$(document).ready(function(){
	$('#helpPassActual').hide();
	$(document).on("click",'#cerrar-reveal',cerrarReveal);
        
        $('#btnAceptar').on('click', function(){
           $('#modalUpdtMensaje').foundation('reveal', 'close');
        });
	
	$("#btnActualizarPass").click(function(){
		if($("#frmCambiarPass").valid() == true){
			$('#modalPass').foundation('reveal','open');
		}
	});
		
	$("#btnModalPass").click(function(){
		
		if($("#txtPassActual").val() != ""){
			$("#modalQuestion").show();
			$("#modalSucess").hide();
			$("#modalFootQuestion").show();
			$("#modalFootSuccess").hide();
			
			$.ajax({
				url: $("#urlActualizarPass").attr("value"),
				dataType: 'json',
				cache: false,
				type: 'POST',
				data: { "_strPassActual" : $("#txtPassActual").val(),
						"_strNewPass" : $("#strNewPass").val()
					},
				beforeSend: function() {
						//TODO: mensaje loading
						$("#loadingPass").show();
					},
				success: function(data) {
						$("#loadingPass").hide();
						if(data["success"] === true){
							$("#modalQuestion").hide();
							$("#modalSucess").show();
							$("#modalFootQuestion").hide();
							$("#modalFootSuccess").show();
							$("#txtConfirmaPass").val("");
							$("#strNewPass").val("");
						}
						else{
							$('#helpPassActual').show();
							$("#txtPassActual").val("");
							$("#grpContPass").addClass("error");
							$("#helpPassActual").html(data["msj"]);
						}
					},
				error: function(data) {
						$("#loadingPass").hide();
					}
				});
			
			}
			else{
				$('#helpPassActual').show();
				$("#grpContPass").addClass("error");
				$("#helpPassActual").html("Ingresa tu password actual");
			}
		});
                
                $("#btnActualizarPass").click(function(){
		if($("#frmCambiarPass").valid() == true){
			$('#modalPass').foundation('reveal','open');
		}
	});
       //Ligar grupo
        $("#btnAgregarGrupo").click(function(){
            if($("#frmAgregarGrupo").valid() == true){	
                $('#modalLigar').foundation('reveal','open');

                $.ajax({
                    url: $("#urlLigarGrupo").attr("value"),
                    dataType: 'json',
                    cache: false,
                    type: 'POST',
                    data: { "strCodigo" : $("#strCodigo").val()},
                    beforeSend: function() {
                        $("#loadingLigar").show();   
                        $("#modalLigaError").hide();
                        $('#modalAjaxError').hide();
                    },
                    success: function(data) {
                        $("#loadingLigar").hide();
                        if(data === true){
                            $("#modalLigaSucces").show();
                            $("#modalLigaError").hide();
                        }
                        else{
                            $("#modalLigaSucces").hide();
                            $("#modalLigaError").show();
                        }
                        $("#modalBtnAceptar").show();
                    },
                    error: function(data) {
                        $('#loadingLigar').hide();
                        $("#modalLigaError").hide();
                        $("#modalLigaSucces").hide();
                        $('#modalAjaxError').show();
                        $('#modalBtnAceptar').show();
                    }
                });
            }
        });
        $("#cerrar-reveal-ligar").click(function(){
           if($("#modalLigaSucces").css('display') == "block"){
               location.reload();
           }else{
               $('#modalLigar').foundation('reveal','close');

           }                
        });
        
        //Agregar licencia
        $("#btnAgregarLicencia").click(function(){
            if($("#frmAgregarLicencia").valid() == true){	
                $('#modalActivaLicencia').foundation('reveal','open');

                $.ajax({
                    url: $("#urlActivarLicencia").attr("value"),
                    dataType: 'json',
                    cache: false,
                    type: 'POST',
                    data: { "strCodigoLicencia" : $("#strCodigoLicencia").val()},
                    beforeSend: function() {
                        $("#loadingActivar").show();   
                        $("#modalActivarError").hide();
                        $('#modalAjaxErrorLic').hide();
                    },
                    success: function(data) {
                        $("#loadingActivar").hide();
                        if(data === true){
                            $("#modalActivarSucces").show();
                            $("#modalActivarError").hide();
                        }
                        else{
                            $("#modalActivarSucces").hide();
                            $("#modalActivarError").show();
                        }
                        $("#modalBtnLicenciaAceptar").show();
                    },
                    error: function(data) {
                       $('#loadingActivar').hide();
                        $("#modalActivarError").hide();
                        $("#modalActivarSucces").hide();
                        $('#modalAjaxErrorLic').show();
                        $('#modalBtnLicenciaAceptar').show();
                    }
                });
            }
        });
        $("#cerrar-reveal-activar").click(function(){
           if($("#modalActivarSucces").css('display') == "block"){
               location.reload();
           }else{
               $('#modalActivaLicencia').foundation('reveal','close');

           }                
        });
        
        $("#btnActualizarLicencia").click(function(){
                if($("#txtRenovarLicencia").val() != ""){
                        $.ajax({
                                url			: $('#urlRenovarLicencia').val(),
                                type 		:'POST',
                                dataType 	:'json',
                                data:{
                                        'strLicencia' 	: $("#txtRenovarLicencia").val()
                                },
                                beforeSend: function(){
                                        $("#imgLoadingRenewLic").slideDown("slow");
                                },
                                success: function(json){
                                        $("#imgLoadingRenewLic").slideUp("slow");
                                        if(json["status"] == true){
                                                // lanzar dialogo y recargar pagina
                                                $("#modalRenewLicencia").modal("show");
                                        }
                                        else{
                                                $("#mensajeErrorRenovarLicencia").html(json['mensaje']);
                                                $("#mensajeErrorRenovarLicencia").show();
                                        }


                                },
                                error: function(){
                                        $("#imgLoadingRenewLic").slideUp("slow");
                                        $("#mensajeErrorRenovarLicencia").html(json['mensaje']);
                                        $("#mensajeErrorRenovarLicencia").show();
                                }
                        });
                }
        });

        $("#txtRenovarLicencia").keypress(function(e){
                if(e.which == 13) {
                        $("#btnActualizarLicencia").trigger("click");
            }

        });

        $("#btnAceptarLic").click(function(){
                window.location.reload();
        });
        $('#modalRenewLicencia').on('hidden', function () {
            // do something…
            window.location.reload();
        });
        //--- jq-validate
        //código para agregar licencia validate
        $( "#frmAgregarLicencia" ).validate({
                rules: {
                  strCodigoLicencia: {required: true},
                },
                messages: {
                 strCodigoLicencia: "Por favor escribe un código de licencia valido."
                }
        });
        //código para liga de grupo validate
        $( "#frmAgregarGrupo" ).validate({
                rules: {
                  strCodigo: {required: true},
                },
                messages: {
                 strCodigo: "Por favor escribe un código de grupo valido."
                }
        });
        //Cambiar nombre validate
        $( "#frmCambiarNombre" ).validate({
                rules: {
                  strNombre: {required: true},
                   strApellido: {required: true}
                },

                messages: {
                  strNombre: "Por favor escribe tu nombre.",
                  strApellido:"Por favor escribe tu apellido."
                }
        });
        //Cambiar usuario validate
        $( "#frmCambiarUserName" ).validate({
                rules: {
                   strUserName: {required: true,minlength: 8,username: true, email: true}
                },

                messages: {
                  strUserName:{
                            required: "Por favor escribe tu nuevo nombre de usuario.",
                            minlength: "Tu Nombre de usuario debe tener por lo menos 8 caracteres.",
                            username: 'Tu nombre de usuario no puede iniciar con "@", contener caracteres especiales ni espacios en blanco.',
                            email:'Debes usar un correo valido.'
                  },
                }
        });
        //cambiar pass validate
        $( "#frmCambiarPass" ).validate({
                rules: {
                        strNewPass: {
                                required: true,
                                minlength: 6
                        },
                        txtConfirmaPass: {
                                required: true,
                                minlength: 6,
                                equalTo: "#strNewPass"
                        }
                },
                messages: {
                        strNewPass: {
                                required: "Por favor escribe tu nueva contraseña.",
                                minlength: "Tu contraseña debe tener por lo menos 6 caracteres."
                        },
                        txtConfirmaPass: {
                                required: "Por favor confirma tu nueva contraseña.",
                                minlength: "Tu contraseña debe tener por lo menos 6 caracteres.",
                                equalTo: "Tu contraseña y su confirmación no coinciden.  Por favor verifica que sean iguales."
                        }
                }
        });
        //Actualiza Nombre real de Usuario ajax
        $("#btnActualizarNombre").click(function(e){
            e.preventDefault();
            if($("#frmCambiarNombre").valid() == true){
                $.ajax({
                        type 		:'POST',
                        dataType 	:'json',
                        url: $('#urlUpdtNombre').val(),
                        data:{
                                'strNombre'    :	$("#strNombre").val(),
                                'strApellido'  :	$("#strApellido").val()
                        },
                        beforeSend: function(){
                            $('#modalUpdt').foundation('reveal','open');
                            $("#modalUpdtAjaxError").hide();
                            $("#loadingUpdt").show();
                            $("#modalUpdt").hide();

                        },
                        success: function(json){
                            $("#loadingUpdt").hide();
                            $('#UpdtMensaje').html(json['mensaje'])
                            $("#modalUpdtMensaje").show();

                        },
                        error: function(){
                           $("#loadingUpdt").hide();
                           $("#modalUpdtAjaxError").show();
                        }
                });
            }
        });
        //Actualiza Nombre de Usuario ajax
       $("#btnActualizarusuario").click(function(e){
            e.preventDefault();
            if($("#frmCambiarUserName").valid() == true){
                $.ajax({
                        type 		:'POST',
                        dataType 	:'json',
                        url: $('#urlUpdtUsuario').val(),
                        data:{
                                'strUserName'    :	$("#strUserName").val()
                        },
                        beforeSend: function(){
                            $('#modalUpdt').foundation('reveal','open');
                            $("#modalUpdtAjaxError").hide();
                            $("#loadingUpdt").show();
                            $("#modalUpdt").hide();

                        },
                        success: function(json){
                            $("#loadingUpdt").hide();
                            $('#UpdtMensaje').html(json['mensaje'])
                            $("#modalUpdtMensaje").show();
                            if(json['update'] == true){
                                $("#strUsuario").val(json['strUserName'])
                            }
                            $("#strUserName").val('')
                        },
                        error: function(){
                           $("#loadingUpdt").hide();
                           $("#modalUpdtAjaxError").show();
                        }
                });
            }
        });
        
    //Validación para el para Activar licencia para el Perfil de PROFESOR.
    $( "#frmAgregarLicenciaProf" ).validate({
        rules: {
          strCodigoLicenciaProf: {required: true},
        },
        messages: {
         strCodigoLicenciaProf: "Por favor escribe un código de licencia valido."
        }
    });        
    
    //Agregar licencia para PROFESOR.
    $('#btnAgregarLicenciaProf').click(function(){
        if ($("#frmAgregarLicenciaProf").valid() == true) {
            $('#modalActivaLicencia').foundation('reveal', 'open');

            $.ajax({
                url: $("#urlActivarLicenciaProf").val(),
                dataType: 'json',
                cache: false,
                type: 'POST',
                data: {
                    'strCodigoLicencia': $("#strCodigoLicenciaProf").val()
                },
                beforeSend: function () {
                    $("#loadingActivar").show();
                    $("#modalActivarError").hide();
                    $('#modalAjaxErrorLic').hide();
                },
                success: function (data) {
                    $("#loadingActivar").hide();
                    if (data === true) {
                        $("#modalActivarSucces").show();
                        $("#modalActivarError").hide();
                    } else {
                        $("#modalActivarSucces").hide();
                        $("#modalActivarError").show();
                    }
                    $("#modalBtnLicenciaAceptar").show();
                },
                error: function (data) {
                    $('#loadingActivar').hide();
                    $("#modalActivarError").hide();
                    $("#modalActivarSucces").hide();
                    $('#modalAjaxErrorLic').show();
                    $('#modalBtnLicenciaAceptar').show();
                }
            });
        }
    });
    });

    function cerrarReveal() 
    {
            $("#modalPass").foundation("reveal","close");
    }

    $.validator.addMethod("username", function(value, element) {
        return this.optional(element) || value.match(/^([a-z]|[A-Z]|[0-9]|_|-|\.|\x)+([a-z]|[A-Z]|[0-9]|_|-|@|\.|\x)+$/);
    }, "Por favor ingresa un nombre de usuario");