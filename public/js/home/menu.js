$(document).ready(function(){
	$(window).scroll(function () {
    	if($(window).width() > 768 && $(window).scrollTop() >= ($("#divMenuTop").height() + $("#ss_header").height() + 5)){
    		//$('#divMnuSlide').slideDown();
    		$( "#divMnuSlide" ).show( 'fold', {}, 500);
	    }
		else{
			
			$( "#divMnuSlide" ).hide( 'fold', {}, 300);
		}
	});
        
        $('.img-beneficios').click(function(){
            var id = $(this).prop('id');
            id =  id.split('-');
            if($(this).hasClass('seleccionado')){
                $(this).removeClass('seleccionado');
                $('#contenido-'+id[1]).slideUp();
            }
            else{
                $('.desc-beneficio').each(function(){
                    
                    if($(this).is(":visible")){
                       $(this).slideUp(); 
                    }
                });
                
                $('#contenido-'+id[1]).slideDown('slow');
                $('#img-'+id[1]).addClass('seleccionado');
            }
            
        });
});