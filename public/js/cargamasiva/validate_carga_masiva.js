
$(document).ready(function(){
	addEvtLister();
	
	/*$("#cargamasiva_list_procesar").joyride({
		  'tipLocation': 'top',
		  'nextButton': true, 
		  'tipAnimation': 'pop',
		  'cookieMonster': true,
		  'cookieName': 'Carga Masiva',
		  'cookieDomain': false,
		});*/
});  


function addEvtLister(){
	$(".txtNombre").blur(function(){
		validaNombre($(this).attr("id"));
	});
	
	$(".txtApellido").blur(function(){
		validaApellido($(this).attr("id"));
	});
	
	$(".txtUser").blur(function(){
		validaUser($(this).attr("id"));
	});
	
	$(".txtPass").blur(function(){
		validaPass($(this).attr("id"));
	});
	
	$(".txtFecha").blur(function(){
		validaFecha($(this).attr("id"));
	});
	
	$(".txtLicencia").blur(function(){
		validaLicencia($(this).attr("id"));
	});
	
}

function validaNombre(id){
	if($("#"+id).val().length > 0){
		if($("#"+id).hasClass("error")){
			$("#"+id).removeClass("error");
		}
		if($("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").hide();
		}
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").html("El campo nombre no puede estar vacio.");
			$("#"+id+"ImgError").show();
		}
	}
	
}

function validaApellido(id){
	if($("#"+id).val().length > 0){
		if($("#"+id).hasClass("error")){
			$("#"+id).removeClass("error");
		}
		if($("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").hide();
		}
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").html("El campo apellido no puede estar vacio.");
			$("#"+id+"ImgError").show();
		}
	}
}

function validaUser(id){
	if($("#"+id).val().length > 0){
		var _boolExiste = false;
		$(".txtUser").each(function(){
			if($(this).val() == $("#"+id).val() && $(this).attr('id') != id){
				_boolExiste = true;
			}
		});
		if(_boolExiste === true){
			if(!$("#"+id).hasClass("error")){
				$("#"+id).addClass("error");
			}
			$("#"+id+"ImgError").html("Este nombre de usuario se repite en algún registro anterior o posterior.");
			if(!$("#"+id+"ImgError").is(":visible")){
				$("#"+id+"ImgError").show();
			}
		}
		else{
		//consulta ajax username disponible
				var urlSitio = $("#urlSitio").val();
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: urlSitio,
						data: {
							"user" : $("#"+id).val(),
							"_boolValidarUsuario": true
						},
						beforeSend: function() {
							$("#"+id).addClass("textProcesando");
						},
						success: function(json) {
							$("#"+id).removeClass("textProcesando");
							if(json["_boolResponse"] === true){
								//TODO: mensaje usuario no disponible 
								if(!$("#"+id).hasClass("error")){
									$("#"+id).addClass("error");
								}
								$("#"+id+"ImgError").html(json["_strMsg"]);
								if(!$("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").show();
								}
							}
							else{
								if($("#"+id).hasClass("error")){
									$("#"+id).removeClass("error");
								}
								if($("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").hide();
								}
							}
						},
						error: function(json) {
							//TODO: agregar mensaje fancy de error 
							console.log("error");
						}									
				});
		}
		
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("El campo usuario no puede estar vacio.");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
}

function validaPass(id){
	if($("#"+id).val().length >= 6){
		if($("#"+id).hasClass("error")){
			$("#"+id).removeClass("error");
		}
		if($("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").hide();
		}
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("El campo contrase&ntilde;a no puede estar vacio.");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
}

function validaLicencia(id){
	var idInput = id.split("licencia");
	if($("#"+id).val().length > 0){
		var _boolExiste = false;

		$(".txtLicencia").each(function(){
			if($(this).val() == $("#"+id).val() && $(this).attr('id') != id){
				_boolExiste = true;
			}
		});
		if(_boolExiste === true){
			$('#nomExamen'+idInput[1]).val("");
			if(!$("#"+id).hasClass("error")){
				$("#"+id).addClass("error");
			}
			$("#"+id+"ImgError").html("La licencia se repite en algún registro anterior o posterior.");
			if(!$("#"+id+"ImgError").is(":visible")){
				$("#"+id+"ImgError").show();
			}
		}
		else{
		//consulta ajax licencia disponible
				var urlSitio = $("#urlSitio").val();
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: urlSitio,
						data: {
							"_strLicencia" 		: $("#"+id).val(),
							'_boolValidarLlave' : true
						},
						beforeSend: function() {
							$("#"+id).addClass("textProcesando");
						},
						success: function(json) {
							$("#"+id).removeClass("textProcesando");
							
							if(json['valida'] === false){
								//TODO: mensaje usuario no disponible 
								$('#nomExamen'+idInput[1]).val("");
								if(!$("#"+id).hasClass("error")){
									$("#"+id).addClass("error");
								}
								$("#"+id+"ImgError").html(json['msj']);
								if(!$("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").show();
								}
							}
							else{
								$('#nomExamen'+idInput[1]).val(json['nombre_examen']);
								if($("#"+id).hasClass("error")){
									$("#"+id).removeClass("error");
								}
								if($("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").hide();
								}
							}
						},
						error: function(json) {
							//TODO: agregar mensaje fancy de error 
							
						}									
				});
		}
	}else{
		$('#nomExamen'+idInput[1]).val("");
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("El campo licencia no puede estar vacio.");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
}

$('#urlProcesarCargaMasivaAjax').click(function(){
	if($("#urlProcesarCargaMasivaAjax").hasClass("disabled") === false){
		var boolError = false;
		$(".filaUser td input[type=text]").each(function(){
			
			if($(this).hasClass("error")){
				$(this).effect("pulsate");
				boolError = true;
			}
		});
		
		if(!boolError){
					var _arrAlum = new Array();
					var iterador = 0;
					$(".filaUser").each(function(){
						
						if($(this).is(":visible")){
							var _arrData = new Array();
							var strRowId = $(this).attr("id").split('row');
							_arrData[0] = $("#nombre"+strRowId[1]).val();
							_arrData[1] = $("#apellido"+strRowId[1]).val();
							_arrData[2] = $("#user"+strRowId[1]).val();
							_arrData[3] = $("#pass"+strRowId[1]).val();
							_arrData[4] = $("#licencia"+strRowId[1]).val();
							_arrAlum.push( _arrData);
							iterador++;
						}
						
					});
					var urlSitio 	 = $("#urlSitio").val();
					var index 	 	 = $("#index").val();
					var codigoGrupo  = $("#_idGrupo").val();
					//Se agregan campos para abrir examenes a que el usuario responda el examen que quiera.
					var idExamen 	 = $('#idExamen1').val();
					var nomExamen	 = $('#nomExamen1').val();
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: urlSitio,
							data: {
								"_arrAlum" 		: _arrAlum,
								"_strCodigo" 	:codigoGrupo,
								"_idExamen"		: idExamen,
								"_nomExamen"	: nomExamen,
							},
							beforeSend: function() {
								$.blockUI({ 
							        message: '<img src="/../images/loader_back.gif"/><h5 style="color:#fff">Cargando...</h5>', 
							        css: { top: '20%',
							        	   backgroundColor: '#000', 
							               '-webkit-border-radius': '10px', 
							               '-moz-border-radius': '10px', 
							               opacity: .5 }
							    });
								
								$("#urlProcesarCargaMasivaAjax").addClass("disabled");
							},
							success: function(json) {
								$.unblockUI();
								if(json["accion"] === true){
									$.mensaje("crearMensaje", {tipo_mensaje : 'informativo', mensaje : json.msg, ruta: index});
	
								}else{
									$.mensaje("crearMensaje", {tipo_mensaje : 'alerta', mensaje : json.msg});
									
								}
								
							},
							error: function(json) 
							{
								$.unblockUI();
								$.mensaje("crearMensaje", {tipo_mensaje : 'error', mensaje: "Ocurrió un error al procesar tu solicitud, Por favor intenta más tarde."});
							}									
					});
		}
		else{
			$.mensaje("crearMensaje", {tipo_mensaje : 'error', mensaje: "Se deben corregir todos los errores para poder enviar el archivo."});
		}
	}
});
	
$('#urlCancelarCargaMasiva').click(function(){
	window.location = $("#index").val();
});
/**
 * Se agrega funcion que reacciona cuando existe un cambio
 * de examen en el combobox implementado en la vista CargaMasiva.phtml.
 * 
 * 
 */
function cambiarExamen( examenes ) 
{
	var id = examenes.selectedIndex;
	var contenido = examenes.options[id].innerHTML;
	for ( var int = 1; int < 100; int++) {
		$('#nomExamen'+int).val(contenido);
	}
	$("#idExamen1").val(examenes.options[id].id);
}



