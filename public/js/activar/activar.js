$(document).ready(function(){
    
    $('#btnProcesar').click(function(){
        activarExamen();
    });
});
function activarExamen(){
    $.ajax({
        type:'POST',
        dataType: 'json',
        url: $('#urlProcesarActivar').val(),
        data:{
                

        },
        beforeSend: function(){
                if($('#btnProcesar').is(':visible')){
                    $('#btnProcesar').hide();
                    $('#spanProcesando').show();
                }
        },
        success: function(json){
                if(json['done'] === true){
                    alert('Se han insertado los registros correctamente.');
                    $('#spanProcesando').hide();
                }
                else{
                    if(json['porcentaje'] > 100){
                        json['porcentaje'] = 100;
                    }
                    $('#'+json['barra']).css('width',json['porcentaje']+'%' );
                    activarExamen();
                }

        },
        error: function(json){
                alert('Ocurrió un error inesperado, por favor intenta más tarde.');
        }
    });
    
}