$(document).ready(function(){
	$(document).on("click",'#btnAgregar', agregarVariable);
	$(document).on("click","#btnModificar", modificarVariable);
	$(document).on("click",'#btnGuardar', guardarVariable);
	$(document).on("click",'#btnCancelar',cancelarVariable);
});

function agregarVariable() 
{	
	if( $('#btnAgregar').hasClass('disabled') === false )
	{
		$('#btnGuardar').removeClass('disabled');
		$('#btnCancelar').removeClass('disabled');
		
		$('#btnAgregar').addClass('disabled');
		$('#btnModificar').addClass('disabled');
		
		var htmlVar = '<div style="padding-top:7px; height:33px; width:250px; float:left; text-align:center; ">'+
						 '<input  id="nameVar"  type="text" placeholder="Nombre de la nueva variable."/>'+
					  '</div>'+
					  '<div style="margin-bottom:40px; padding-top:7px; height:33px; width:550px; float:left; text-align:center;">'+
						 '<input  id="valueVar"  type="text" placeholder="Valor de la nueva variable."/>'+
					  '</div>';
		$('#addVar').html(htmlVar);
	}
}

function modificarVariable() 
{
    if( $('#btnModificar').hasClass('disabled') === false )
    {
        var urlModificar = $('#urlModificarVar').val();
        var urlConfig    = $('#urlConfig').val();
        var datosJson 	 = {};
        //hardcode solo soporta 100 variables se podrian insertar mas aumentando 100 al numero que se necesita
        for(var index = 0; index<100; index++){
            var element = document.getElementById(index);
            if( element !== null){
                var dato = element.name;
                var modificar = element.value;
                datosJson[dato] = modificar;
            }else{
                index = 100;		
            }
        }
        $.ajax({
            type:'POST',
            dataType:'json',
            url: urlModificar,
            data: {
                'datos' :datosJson
            },
            beforeSend: function() {
            },
            success:function(json){	
                if(json['success']){
                    $.mensaje("crearMensaje",{
                        tipo_mensaje:"alerta",
                        mensaje:'Se modifico exitosamente el archivo.',
                        ruta : urlConfig
                    });
                }else{
                    $.mensaje("crearMensaje",{
                        tipo_mensaje:"error",
                        mensaje:json['msg']
                    });
                }
            },
            error: function(json){
                $.mensaje("crearMensaje",{
                    tipo_mensaje:"error",
                    mensaje:"error en el proceso"
                });
            }
        });
    }
}

function guardarVariable() 
{
    if( $('#btnGuardar').hasClass('disabled') === false )
    {
        var guardarVar = $('#urlGuardarVar').val();
        var urlConfig  = $('#urlConfig').val();
	var nameVar    = $('#nameVar').val();
	var valueVar   = $('#valueVar').val();

        $.ajax({
            type:'POST',
            dataType:'json',
            url: guardarVar,
            data:{
                'nameVar'  : nameVar,
                'valueVar' : valueVar
            },
            beforeSend: function(){
            },
            success:function(json){
                if(json['success']){
                    $.mensaje("crearMensaje",
                    {
                        tipo_mensaje:"alerta",
                        mensaje:'Se agrego la variable satisfactoriamente.',
                        ruta: urlConfig
                    }
                            );
                }else{
                    $.mensaje("crearMensaje",
                    {
                        tipo_mensaje:"error",
                        mensaje:json['msg']
                    }
                            );				
                }
            },
            error:function(json){    
            }
        });
    }
}

function cancelarVariable() 
{
	if( $('#btnCancelar').hasClass('disabled') === false )
	{
		window.location = $('#urlConfig').val();
	}
}

function borrarVariable( nameconst ) {
	var eliminarVar = $('#urlEliminarVar').val();
	var urlConfig   = $('#urlConfig').val();
	$.ajax({
            type:'POST',
            dataType:'json',
            url:eliminarVar,
            data:{
                'nameconst' : nameconst
            },
            beforeSend:function(){},
            success: function(json){
                if(json['success']){
                    $.mensaje('crearMensaje',{tipo_mensaje:'alerta', mensaje:'Se elimino satisfactoriamente la variable.', ruta:urlConfig });
                }else{
                    $.mensaje('crearMensaje',{tipo_mensaje:'error', mensaje:json['msg']});
                }
            },
            error: function(json){}
        });
}
