        
$(document).ready(function(){	


	$("#_btnCambiaPassword").click(function(){
	
	$('#_txtconfirmarPassword').removeClass("error");
	$('#_txtPassword').removeClass("error");
	
	var _strPassword = $("#_txtconfirmarPassword").val();
	var _strPasswordConfirmacion = $("#_txtPassword").val();
	
	if(_strPassword.length > 0 && _strPasswordConfirmacion.length > 0){
		//verificar que sean igual
		if(_strPassword == _strPasswordConfirmacion ){
			
			$.ajax({
				type:'POST',
				dataType:'json',
				url:  $('#urlSitioCambiarPass').val(),
				data:{ 
					'_strPassword': _strPassword,
					'_strCode' : $("#_txtCode").val()
					},
				beforeSend: function() {
					$("#imgLoading").show();
					$("#changePassForm").slideUp("slow");
				},
				success:function(json){
					$("#imgLoading").hide();
					if(json ){
						$("#changePassForm").hide();
						//mensaje se envio correctamente
						$('#msgResultado').html('<div id="_gen" class="row" ><div class="alert alert-success">'+
					            	'<h4 class="alert-heading">Cambio de contrase&#241;a</h4>'+
					          
									'<p>El cambio de contrase&#241;a fue exitoso</p>'+''+''+'<p><a id="butt_comienza" class=" button info text-center butt_style" href="../../">Pagina principal</a></p>'+
									'</div>'+
							'</div>');
						
						
					}else{
						$("#changePassForm").show();
						$('#msgResultado').html('<div id="_gen" class="row" > <div class="span12 " ><h3 class="tit_rojo" >El cambio de contrase&#241;a fallo.</h3></div>'+
								'<div class="span12"  > Intenta m&aacute;s tarde.</div>'+
							'</div>');	
						

					}
				},
				error: function(json){
					$("#changePassForm").show();
					$.mensaje("crearMensaje",
							{tipo_mensaje:"error",
							 mensaje:json
							}
						);
					$("#imgLoading").hide();
				}
			});
			
		}else{
			$("#_txtconfirmarPassword").addClass('textError');
			$("#_txtPassword").addClass('textError');
		}
		
	}else{
		//
		$("#_txtconfirmarPassword").addClass('textError');
		$("#_txtPassword").addClass('textError');
	}
	
});
$("#regresaindex").click(function(){
    
});

});
	