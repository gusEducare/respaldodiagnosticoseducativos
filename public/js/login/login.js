var contTxtLicencias = 0;
$(document).ready(function () {
    $('#botonCancelar').hide('fast');
    validarMsg();
    $(document).on('click', '#abrirModalValidacion', abrirModalValidacion);
    $(document).on('click', '#abrirModalLogin', abrirModalLogin);
    $(document).on('click', '#abrirModalIntellectus', abrirModalIntellectus);
    $(document).on('click', '#abrirModalIC', abrirModalIC);
    $(document).on('click', '#abrirModalVideo', abrirModalVideo);
    $(document).on('click', '#resetcontrasena', abrirModalRecuperaContrasena);
    $(document).on('click', '#recuperacambialic', abrirModalRecuperaPassLic);
    $(document).on('click', '#recuperacambiaCorreo', abrirModalRecuperaContrasena);
    $(document).on('click', '#recuperacontrasena', validarRecuperar);
    $(document).on('click', '#submitbutton', validarCorreo);
    $(document).on('click', '#yatengocorreo', ocultarRedesSociales);
    $(document).on('click', '#botonCancelar', mostrarRedesSociales);

    arrMsgs = jQuery.parseJSON($('#arrMsgs').val());

    msjMinLength6 = arrMsgs.MSJMINLENGTH6;
    msjRequiere = arrMsgs.MSJREQUIERE;

    $("[name='checkboxprofesor']").change(function () {
        console.log("entra");
   $('#codigogrupo').attr('disabled',this.checked);
   $('#codigogrupo').val("");
});
//----------------------------------------
var idPaquete = 0;
    $('#frmLicencias').validate({
        validClass: "success",
        rules: {
            codigogrupo: {required: true, minlength: 6, codeGroup: true},
            licenciaexamen: {required: true, minlength: 6, validarLicencia: true}
        },
        messages: {
            codigogrupo: {
                required: 'código de grupo',
                minlength: msjMinLength6
            },
            licenciaexamen: {
                required: 'licencia de examen',
                minlength: msjMinLength6
            }
        },
        submitHandler: function() 
        {
            var codigogrupo = $('#codigogrupo').val();
            var licenciaexamen = $('#licenciaexamen').val();
            $('#modalValidacion').foundation('reveal', 'close');
            $('#vercodigogrupo').val(codigogrupo);
            if (codigogrupo == "") {
                $("[name='t01nombre']").rules('add', {required: false});
                $("[name='t01apellidos']").rules('add', {required: false});
                $("#t01correo").rules('add', {required: false});
                $("#t01contrasena").rules('add', {required: false});
                $("[name='confPass']").rules('add', {required: false});
            } else {
                $("[name='t01nombre']").rules('add', {required: true, minlength: 3});
                $("[name='t01apellidos']").rules('add', {required: true, minlength: 3});
                $("#t01correo").rules('add', {required: true, email: true, minlength: 5});
                $("#t01contrasena").rules('add', {required: true, minlength: 5});
                $("[name='confPass']").rules('add', {equalTo: '#t01contrasena'});
            }
            $('#verlicenciaexamen').val(licenciaexamen);
            $('#idPaquete').val(idPaquete);
            $('#modalRegistro').foundation('reveal', 'open');
        }
    });

    
    $.validator.addMethod('validarLicencia', function () {
        var licenciaexamen = $('#licenciaexamen').val();
        var verificar = false;
        $.ajax({
            url: '/login/validarlicencia',
            type: 'POST',
            dataType: 'json',
            data:
                    {
                        licenciaexamen: licenciaexamen
                    },
            async: false,
            success: function (data)
            {
                if (data['success'])
                {
                    verificar = true;
                    idPaquete = data['id_paquete'];
                } else {
                    verificar = false;
                }
            }
        });
        return verificar;
    }, 'La licencia no es válida');
   
    $.validator.addMethod('codeGroup', function ()
    {
        var codigogrupo = $('#codigogrupo').val();
        var verificar = false;

        $.ajax({
            url: $('#urlValidarCodigos').val(),
            type: 'POST',
            dataType: 'json',
            data: {
                codigogrupo: codigogrupo
            },
            async: false,
            success: function (data)
            {

                if (data['success'])
                {
                    verificar = true;
                } else {
                    verificar = false;
                    /* $('#modalValidacion').foundation('reveal', 'close');
                     $('#msgModal').html('<h3>'+data['msg']+'</h3><a class="close-reveal-modal">&#215;</a>');
                     setTimeout(function(){ 
                     $('#msgModal').foundation('reveal', 'open'); 					
                     }, 1000);*/
                }
            }
        });
        return verificar;
    }, 'Código de grupo no existe');
            
    
    $('#frmUsuarios').validate({
    	rules: {
            verlicenciaexamen:  {equalTo: '#licenciaexamen'},
            vercodigogrupo:     {equalTo: '#codigogrupo'},
            t01nombre:          {required: true ,minlength: 3   },
            t01apellidos:       {required: true ,minlength: 3   },
            t01correo:          {required: true,email:true,minlength: 5},
            t01contrasena:      { required: true, minlength: 5},
            confPass:           { equalTo: '#t01contrasena' }
        },
        messages:{
            verlicenciaexamen:  { equalTo : 'licencia de examen no coincide'},
            vercodigogrupo:     { equalTo : 'código de grupo no coincide'},
            t01nombre:          { required: 'Requiere Nombre', minlength: 'Minimo 3 caracteres'},
            t01apellidos:       { required: 'Requiere Apellido', minlength: 'Minimo 3 caracteres'},
            t01contrasena:      { required: 'Requiere Contraseña', minlength: 'Minimo 6 caracteres'},
            confPass:           { equalTo : 'No coincide' },
            t01correo:          {required: 'Requiere Correo', email:'Correo Incorrecto',minlength: 'minimo 5 caracteres'}
        }
    });
    $('input[type="submit"]').click(function () {
        console.log();
        console.log('submit: ' + $(this).attr('id'));
    });

    $('#submitRegistro').click(function () {
        $("[name='t01nombre']").rules('add', {required: true, minlength: 3});
        $("[name='t01apellidos']").rules('add', {required: true, minlength: 3});
        $("#t01correo").rules('add', {required: true, email: true, minlength: 5});
        $("#t01contrasena").rules('add', {required: true, minlength: 5});
        $("[name='confPass']").rules('add', {equalTo: '#t01contrasena'});
        if($('#frmUsuarios').valid() === true){
            $('#modalValidacion').foundation('reveal', 'close');
        }
    });

    $('.redSocial').click(function () {
        $("[name='t01nombre']").rules('add', {required: false});
        $("[name='t01apellidos']").rules('add', {required: false});
        $("#t01correo").rules('add', {required: false});
        $("#t01contrasena").rules('add', {required: false});
        $("[name='confPass']").rules('add', {required: false});
        if($('#frmUsuarios').valid() === true){
            $('#modalValidacion').foundation('reveal', 'close');
        }
    });
//---------------------------------------------


});

/**
 * Funcion para abrir modal de validacion.
 */
function abrirModalValidacion()
{
	$('#modalValidacion').foundation('reveal', 'open');
}
/*
 * 
 * Funcion para abrir modal de recuperar pass.
 */
function abrirModalRecuperaContrasena(){
     $('#modalRecuperaContrasena').foundation('reveal', 'open');
}
function abrirModalRecuperaPassLic(){
     $('#modalRecuperaContrasenaLic').foundation('reveal', 'open');
}
/**
 * Funcion para validar modal de login.
 */
function abrirModalLogin()
{
    $('#modalLogin').foundation('reveal', 'open');
}
/**
 * Funcion para abrir modal de infografías.
 */
function abrirModalIntellectus()
{
    $('#modalIntellectus').foundation('reveal', 'open');
}
/*
 * /**
 * Funcion para abrir modal de infografías.
 */
function abrirModalIC()
{
    $('#modalIC').foundation('reveal', 'open');
}
function abrirModalVideo()
{
    $('#modalPyMDE').foundation('reveal', 'open');
}
/*
 * valida cada uno de los forms que se encuntran dentro del formulario
 * @returns {undefined}
 */

function validarCorreo() 
{
    $('#frmLogin').validate({
       rules: {
           //t01correo:       {required: true, email: true},
           t01correo:       {required: true },
           t01contrasena:   { required: true, minlength: 5}
       },
       messages: {
           //t01correo:       {required: 'Requiere Correo', email: 'Correo Incorrecto'},
           t01correo:       {required: 'Requiere Correo' },
           t01contrasena:   {required: 'Requiere Contraseña', minlength: 'Minimo 5 caracteres'}
       }
    });
    
    //valida si existe o no el correo electronico
            $('#t01correo').rules('add',{myEMail: true});
            jQuery.validator.addMethod('myEMail', function (){        
            var t01correo = jQuery('#t01correo').val();
            var ajaxUrl = '/login/validarcorreo';
            var data = {'accion':true,'t01correo':t01correo };
            var verificar = false;
                $.ajax({
                type:'POST',
                url : ajaxUrl,
                data: data,
                dataType:'json',
                async :false,
                success: function(response)
                {
                    //verificar = response.accion;
                    if(response.accion === data.accion){
                        verificar = false;
                    }else{
                        verificar = true;
                    }
                }
            });
            return verificar;
    },'Correo Registrado');
                        
    
}
/**
 * 
 * @actualiza id de t01correo por nameUser y placeholder
 * muestra input t01correo con valores falsos
 * agrega input #idUser con las nuevas validaciones
 * valida el campo de usuario sin espacios con la funcion noSpace 
 */
function ocultarRedesSociales()
{
    document.getElementById('t01correo').setAttribute('id','nameUser');
    document.getElementById('nameUser').placeholder = 'Nombre de Usuario';
    
    jQuery('#nameUser').rules('add',{required: false,email: false,minlength: false});
     
    $('#nameUser').rules('add',{required: true,minlength: 6,noSpace: true,myUser: true,
        messages:{minlength:'minimo 6 Caracteres',required: 'Requiere Nombre de Usuario'}
    }); 
    jQuery.validator.addMethod('noSpace', function (){        
        var campoVacio = /\s/;
        var nombreUsuario    = jQuery('#nameUser').val();
        if(campoVacio.test(nombreUsuario)) 
        {
            return false;
        }else{
            return true;
        }
    },'no se aceptan espacios');
    
    //valida si existe o no el usuario  
            jQuery.validator.addMethod('myUser', function (){        
            var t01correo = jQuery('#nameUser').val();
            var ajaxUrl = '/login/validarcorreo';
            var data = {'accion':true,'t01correo':t01correo};
            var verificar = false;
            $.ajax({
                type:'POST',
                url : ajaxUrl,
                data: data,
                dataType:'json',
                async :false,
            success: function(response)
            {
                if(response.accion === data.accion){
                    verificar = false;
                }else{
                    verificar = true;
                }
            }
        });
        return verificar;
    },'Usuario Registrado');
    
    $('#redes-sociales').hide('fast'); 
    $('#yatengocorreo').hide('fast');
    $('#botonCancelar').show('fast');
}

/*
 * @devuelve los atributos de t01correo
 * @Muestra las redes sociales habilitando los parametros de validacion para t01cooreo
 * 
 */
function mostrarRedesSociales()
{
    document.getElementById('nameUser').setAttribute('id','t01correo');
    document.getElementById('t01correo').placeholder = 'Correo Electrónico'; 
    $('#t01correo').rules('add',{required: true,email: true,minlength: 5,
                      messages:{required: 'Requiere Correo', email:'Correo Incorrecto',minlength: 'minimo 5 caracteres'}
        });       
    $('#redes-sociales').show('fast'); 
    $('#yatengocorreo').show('fast');
    if($('#yatengocorreo').show('fast')){
        $('#botonCancelar').hide('fast');
    }
}

/**
 * Funcion para validar mensajes flash de php.
 */
function validarMsg()
{
	if($('#msgModal h3').length > 0 )
	{
		 $('#msgModal').foundation('reveal', 'open'); 
	}
}



//function agregarCajaLicencia(event){
//    contTxtLicencias++;
//    if($(this).val()){
//        console.log('Aqui creo otra caja');
//        if( !existeClonVacio() ){
//            $(this).clone().attr('id','licenciaexamen-'+contTxtLicencias).val('').insertBefore($('#validarcodigos'));
//             //$(this).clone().attr('id','txtLicencia-'+contTxtLicencias).val('').insertBefore($('#validarcodigos'));
//        }
//        
//    }else{
//        console.log('Borro la caja de abajo');
//                       
//        if($(this).next().next().is('.licenciausuario')){
//            $('.licenciausuario').last().focus();
//            if($(this).next().is('.error')){
//                $(this).next().remove();
//            }
//            $(this).remove();
//            
//        }else{
//            
//             if(event.keyCode !== 8 && event.keyCode !== 9 && event.keyCode !== 16){
//                $('.licenciausuario').last().remove();
//            }
//        }
//    }
//    
//}

function existeClonVacio(){ 
    var existe = false;
    $('.licenciausuario').each(function(){
        if(!$(this).val()){
            existe = true;
        }
    });
    return existe;
}




/**
 * 
 *valida que el correo exista en la base de adatos
 */
function validarRecuperar()
{
	var msj				    = 'Correo no existe';
	
        
    //valida el formulario de Recuperar correo y que los campos no esten vacios
    $('#frmRecuperaContrasena').validate({
    	validClass: "success",
    	rules: {
            contrasenarecupera:    { required: true},
           
        },
        messages:{
            contrasenarecupera:{
                required: 'Se requiere correo electrónico',
                email:'El formato del correo es invalido'
                
            },
           
        },

     submitHandler: function() 
	    {
                    recuperar_pass();
            }
    });


    }
    
//    function  recuperaPass(){
//         console.log( "You clicked " );
//         
//       
//        $.ajax({ 
//                    url: $('#urlRecuperaPass').val(),
//                    dataType: 'json',
//                    cache: false,
//                    type: 'POST',
//                    data: { _strCorreo : $("#contrasenarecupera").val(),},
//                    beforeSend: function() {
//                     console.log( "You clicked a paragraph!" );	
//                    },
//                    success: function(data) {
//
//                             console.log( "You win!" );	
//                    }
//                });
//        
//        
//
//    }
//    
//    
//    function validarRecuperarLic()
//{
//    //valida el formulario de Recuperar correo y que los campos no esten vacios
//    $('#frmRecuperaContrasenalic').validate({
//    	validClass: "success",
//    	rules: {
//            contrasenarecuperalic:    { required: true},
//           
//        },
//        messages:{
//            contrasenarecuperalic:{
//                required: 'Licencia',
//               
//            },
//           
//        },
//
//     submitHandler: function() 
//	    {
//                    recuperaPassLic();
//            }
//    });
//
//    }
//    
//    function  recuperaPassLic(){
//         
//         
//       
//        $.ajax({ 
//                    url: $('#urlRecuperaPasslic').val(),
//                    dataType: 'json',
//                    cache: false,
//                    type: 'POST',
//                    data: { _strLicencia : $("#contrasenarecuperalic").val(),},
//                    beforeSend: function() {
//                    
//                    },
//                    success: function(data) {
//                        if(data[0] == 'DISPONIBLE' && data[1]==true ){
//                             console.log( "You win!" );	
//                             $strCorreocambio=data[3];
//			}else{
//                             console.log( "You lose!" );	
//                        }
//                            
//                    }
//                });
//        
//        
//
//    }
    
    
    function recuperar_pass(){	
		
	//$('#txtCorreoVal').removeClass('textError');
	
	var _strCorreo = $("#contrasenarecupera").prop("value");
	
	//validacion de correo
	if (_strCorreo != ''){
		//validar que la nomenclatura del correo sea correcta
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(_strCorreo)){
			 $.ajax({ 
						url: $('#urlRecuperaPass').val(),
						dataType: 'json',
						cache: false,
						type: 'POST',
						data: {"_strCorreo":_strCorreo},
						beforeSend: function() {
							//Agregar imagen de cargando...
                                                                $("#recuperacontrasena").hide();
								$("#imgLoadingRecuperar").slideDown("slow");
								$("#modalRecuperarHeader").slideUp("slow");
							},
						success: function(data) {
							$("#imgLoadingRecuperar").slideUp("slow");
								if(data['respuesta'] == true){
									$("#recuperacontrasena").hide();
									$('#_Msg').addClass('alert alert-success');
									$('#_Msg').html('Se ha enviado un e-mail a tu cuenta de correo con las instrucciones necesarias para resetear tu contraseña.');		
									$("#_Msg").slideDown("slow");
								}
								else{
									$("#modalRecuperarHeader").slideDown("slow");	
									$('#_Msg').addClass('alert alert-info');
									$('#_Msg').html(data['msg']);		
									setTimeout('$("#_Msg").slideUp("slow");',6000);
									$("#_Msg").slideDown("slow");
			
								}
								
							
								
							},
						error: function(data) {
							$("#imgLoadingRecuperar").slideUp("slow");
							$("#modalRecuperarHeader").show();	
							$('#_Msg').addClass('alert alert-info');
							$('#_Msg').html('Hubo un error en el proceso intenta más tarde.');		
							setTimeout('$("#_Msg").slideUp("slow");',6000);
							$("#_Msg").slideDown("slow");
						}
				});
				
		}else{
//			$('#txtCorreoVal').addClass('textError');	
//			$('#_Msg').addClass('alert alert-error');
//			$('#_Msg').html('El formato de correo es incorrecto.');		
//			setTimeout('$("#_Msg").slideUp("slow");',6000);
//			$("#_Msg").slideDown("slow");
		}
		
	}else{
		$('#txtCorreoVal').addClass('textError');		
	}
	
        
        
        

}


