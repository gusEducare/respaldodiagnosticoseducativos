$(document).ready(function() {

    
    $calendar = $('#calendar');
    $('.fechas').hide();
    var idinsertado = 0;
    var id = 1;
   
	 
   $calendar.weekCalendar({
      displayOddEven:true,
      //con esta linea se define el ancho de las filas de la hora
      timeslotsPerHour : 2,
      allowCalEventOverlap : true,
      overlapEventsSeparate: true,
      firstDayOfWeek : 1,
      //en esta linea de  codigo se define el horario en que empieza y termina el dia
      
      businessHours :{start: 0, end: 24, limitDisplay: true },
      daysToShow : 1,
      newEventText: 'Nuevo Evento',
      //Aqui se esta cambiando el lenguaje a español 
      shortDays:['Dom','Lun','Mar','Mie','Jue','Vie','Sab'],
      longDays:['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
      shortMonths:['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
      longMonths:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
      buttonText: { today: "Hoy" },
      dateFormat: "d-M-Y",

      switchDisplay: { 'Día Completo': 1, 'Semana Completa': 7 },
      title: function(daysToShow) {
			return daysToShow === 1 ? '%date%' : '%start% - %end%';
      },
    
		
    
      
      height : function($calendar) {
         return  1090;//$(window).height() - $("h1").outerHeight() - 1;
      },
      eventRender : function(calEvent, $event) {
         if (calEvent.end.getTime() < new Date().getTime()) {
            $event.css("backgroundColor", "#aaa");
            $event.find(".wc-time").css({
               "backgroundColor" : "#aaa",
               "border" : "1px solid #888"
            });
         }
      },
      draggable : function(calEvent, $event) {
//         return calEvent.readOnly != true;
    	  return false;
      },
      resizable : function(calEvent, $event) {
//         return calEvent.readOnly != true;
         return false;
      },
      eventNew : function(calEvent, $event) {
         var $dialogContent = $("#event_edit_container");
         resetForm($dialogContent);
         var fechaInicio = $calendar.weekCalendar("formatDate", calEvent.start);
         var fechaDefault = $calendar.weekCalendar("formatDate", calEvent.start, 'Y-m-d');
         var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
         var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
         var titleField = $dialogContent.find("select[name='title']");
         var examenField = $dialogContent.find("select[name='examen']");
         var bodyField = $dialogContent.find("textarea[name='body']");
         var dateStartField = $dialogContent.find("input[name='dateStart']");
         var dateEndField = $dialogContent.find("input[name='dateEnd']");
         var multiplesDias = $dialogContent.find("input[name=multiples-dias]");
         //var grupoField = $dialogContent.find("textarea[name='nombreGrupo']");

       
        $('#dateStart').datepicker({defaultDate: fechaDefault});
         //if(calEvent.id){
             $('#texto-grupos').hide();
             $('#texto-examenes').hide();
             $('#cont-grupos').show();
             $('#cont-examenes').show();
         //}
         $dialogContent.dialog({
            modal: true,
            resizable: false,
            title: "Agendar Examen",
            close: function() {
               $dialogContent.dialog("destroy");
               $dialogContent.hide();
               $('#calendar').weekCalendar("removeUnsavedEvents");
            },
            buttons: {
                Guardar : function() {
                    calEvent.id = id;
                    id++;
                    calEvent.accion = "guardar";
                    calEvent.editar = "0";
                    calEvent.start = startField.val();
                    calEvent.end = endField.val();
//                    calEvent.start = $calendar.weekCalendar("formatDate", calEvent.start,'Y-m-d H:i:s');
//                    calEvent.end   = $calendar.weekCalendar("formatDate", calEvent.end,'Y-m-d H:i:s');
                    calEvent.title = titleField.val();
                    calEvent.examen = examenField.val();
                    calEvent.body = bodyField.val();
                    calEvent.multiplesDias = multiplesDias.is(':checked');
                    calEvent.dateStart = dateStartField.val();
                    calEvent.dateEnd = dateEndField.val();
                    // calEvent.grupo = grupoField.val();
                    if($("#frmEventos").valid()){
                        $calendar.weekCalendar("removeUnsavedEvents");
                        $calendar.weekCalendar("updateEvent", calEvent);
                        $dialogContent.dialog("close");
                        guardaEvento(calEvent, $calendar);
                    }
                },
                Cancelar : function() {
                    $dialogContent.dialog("close");
                }
            }
        }).show();
         $dialogContent.find(".date_holder").text( fechaInicio );
         setupStartAndEndTimeFields(startField, endField, calEvent, $calendar.weekCalendar("getTimeslotTimes", calEvent.start));
    },
    eventDrop : function(calEvent, $event) {
    },
    eventResize : function(calEvent, $event) {
    },
    eventClick : function(calEvent, $event) {
        if (calEvent.readOnly) {
            return;
        }
        var $dialogContent = $("#event_edit_container");
        resetForm($dialogContent);
        idEditar=calEvent.id==='1'?idinsertado:calEvent.id;
        var idField = $dialogContent.find("input[name='id']").val(idEditar);
        var startField = $dialogContent.find("select[name='start']").val(calEvent.start);
        var endField = $dialogContent.find("select[name='end']").val(calEvent.end);
        var titleField = $dialogContent.find("select[name='title']").val(calEvent.grupo).trigger('change');
        var examenField = $dialogContent.find("select[name='examen']").val(calEvent.examen).trigger('change');
        var bodyField = $dialogContent.find("textarea[name='body']").val(calEvent.body);
        bodyField.val(calEvent.body);
        if(calEvent.id){
            $('#texto-grupos').show();
            $('#texto-examenes').show();
            $('#texto-grupos').html('Grupo: ' + calEvent.nombreGrupo);
            $('#texto-examenes').html('Examen: ' + calEvent.title);
            $('#cont-grupos').hide();
            $('#cont-examenes').hide();
        }else{
        
        }
        $dialogContent.dialog({
            modal: true,
            resizable: false,
            //width: 600,
            //editar
            //en esta linea va a ponert de titulo el evento k le concatenemos
            title: "Editar examen " + calEvent.title + " para el grupo " + calEvent.nombreGrupo,
            close: function() {
                $dialogContent.dialog("destroy");
                $dialogContent.hide();
                $('#calendar').weekCalendar("removeUnsavedEvents");
            },
            buttons: {
                Guardar : function() {
                    calEvent.accion = "editar";
                    calEvent.editar = "";
                    calEvent.id = idField.val();
                    calEvent.start = startField.val();
                    calEvent.end = endField.val();
                    calEvent.title = calEvent.grupo; //titleField.val();
                    ////calEvent.examen = calEvent.examen; //examenField.val();
                    calEvent.body = bodyField.val();
                    $calendar.weekCalendar("updateEvent", calEvent);
                    $dialogContent.dialog("close");
                    guardaEvento(calEvent, $calendar);  
                },
                Eliminar : function() {
                    calEvent.id = idField.val();
                    calEvent.accion = "eliminar";
                    $calendar.weekCalendar("removeEvent", calEvent.id);
                    $dialogContent.dialog("close");
                    guardaEvento(calEvent, $calendar);
                },
                Cancelar : function() {
                    $dialogContent.dialog("close");
                }
            }
        }).show();
        var startField = $dialogContent.find("select[name='start']");
        var endField = $dialogContent.find("select[name='end']");
        var dateStartField = $dialogContent.find("input[name='dateStart']").val(calEvent.start.dateFormat('d/m/Y'));
        var dateEndField = $dialogContent.find("input[name='dateEnd']").val(calEvent.end.dateFormat('d/m/Y'));
        $dialogContent.find(".date_holder").text($calendar.weekCalendar("formatDate", calEvent.start));
        setupStartAndEndTimeFields(startField, endField, calEvent, $calendar.weekCalendar("getTimeslotTimes", calEvent.start));
        $(window).resize().resize(); //fixes a bug in modal overlay size ??
    },
    eventMouseover : function(calEvent, $event) {
    
    },
    eventMouseout : function(calEvent, $event) {
    
    },
    noEvents : function() {
    
    },
    data : function(start, end, callback) {
        callback(getEventData(agenda));
    }
});
   

   function resetForm($dialogContent) {
      $dialogContent.find("input").val("");
      $dialogContent.find("textarea").val("");
   }
  
   function getEventData(obj_agenda) {
      var year = new Date().getFullYear();
      var month = new Date().getMonth();
      var day = new Date().getDate();

      
     // newEventText:["Nuevo Evento"]
      return {
         events : obj_agenda
         
      };
   }


   function setupStartAndEndTimeFields($startTimeField, $endTimeField, calEvent, timeslotTimes) {

      $startTimeField.empty();
      $endTimeField.empty();         

      for (var i = 0; i < timeslotTimes.length; i++) {
         var startTime = timeslotTimes[i].start;
         var endTime = timeslotTimes[i].end;
         var startSelected = "";
         if (startTime.toLocaleTimeString() === calEvent.start.toLocaleTimeString()) {
            startSelected = "selected=\"selected\"";
         }
         var endSelected = "";
         if (endTime.toLocaleTimeString() === calEvent.end.toLocaleTimeString()) {
            endSelected = "selected=\"selected\"";
         }
         $startTimeField.append("<option value=\"" + startTime + "\" " + startSelected + ">" + timeslotTimes[i].startFormatted + "</option>");
         $endTimeField.append("<option value=\"" + endTime + "\" " + endSelected + ">" + timeslotTimes[i].endFormatted + "</option>");
         
         $timestampsOfOptions.start[timeslotTimes[i].startFormatted] = startTime.getTime();
         $timestampsOfOptions.end[timeslotTimes[i].endFormatted] = endTime.getTime();

      }
      $endTimeOptions = $endTimeField.find("option");
      $startTimeField.trigger("change");
   }

   var $endTimeField = $("select[name='end']");
   var $endTimeOptions = $endTimeField.find("option");
   var $timestampsOfOptions = {start:[],end:[]};

   //reduces the end time options to be only after the start time options.
   $("select[name='start']").change(function() {
      var startTime = $timestampsOfOptions.start[$(this).find(":selected").text()];
      var currentEndTime = $endTimeField.find("option:selected").val();
      $endTimeField.html(
            $endTimeOptions.filter(function() {
               return startTime < $timestampsOfOptions.end[$(this).text()];
            })
            );

      var endTimeSelected = false;
      $endTimeField.find("option").each(function() {
         if ($(this).val() === currentEndTime) {
            $(this).attr("selected", "selected");
            endTimeSelected = true;
            return false;
         }
      });

      if (!endTimeSelected) {
         //automatically select an end date 2 slots away.
         $endTimeField.find("option:eq(1)").attr("selected", "selected");
      }

   });
   


   
//valida los filtros por Grupo y Examen
   $("#frmFiltros").validate({

	   errorPlacement: function(error, element) {
        
           if (element.attr("name") == "selGrupo") {
               error.appendTo('#errorGrupo');
           }
       },
       rules: {
  
    	   selGrupo: {required: true,
    			   
        		   min: 1},
       },
       messages: {
    	 
    	   selGrupo: "Favor de seleccionar un grupo",
       }
});

   
   $( "#selGrupo" ).change(function() {
       $.ajax({
           type:'POST',
           dataType:'json',
           url:  $('#urlObtenerExamen').val(),
           data:{
               'idGrupo'   : $('#selGrupo').val()
           },
           beforeSend: function() {
               $('#selExamen').html('');
           },
           success:function(json){
               var html=	'<option value="-1">Seleccionar Examen</option>';
               for(var x in json  ){
                   html+='<option value='+json[x]['id']+'>'+json[x]['name']+'</option>';
               }
               $('#selExamen').html(html);
           },
           error: function(json){
           }
       });
       if( $( this ).val() !== '-1'){
           $( "#selGrupoForm" ).val( $( this ).val()).trigger('change');
       }else{
           $( "#selGrupoForm" ).val('-1');
       }
       actualizaCalendario( $calendar );
   });
   
    $( "#selGrupoForm" ).change(function() {
        $.ajax({
            type:'POST',
            dataType:'json',
            url:  $('#urlObtenerExamen').val(),
            data:{
                'idGrupo'   : $('#selGrupoForm').val(),
                'Examenes'   : 1
            },
            beforeSend: function() {
                $('#selExamenForm').html('');
            },
            success:function(json){
                var html=	'<option value="-1">Seleccionar Examen</option>';
                for(var x in json  ){
                    html+='<option value='+json[x]['id']+'>'+json[x]['name']+'</option>';
                }
                $('#selExamenForm').html(html);
            },
            error: function(json){
                //alert(" error");
            }
        });
    });
                
    $( "#selExamen" ).change(function() {
        actualizaCalendario( $calendar );
                 
        if( $( this ).val() !== '-1'){
            //$( "#selGrupoForm" ).trigger('change');
            $( "#selExamenForm" ).val( $( this ).val()).trigger('change');
           
        }else{
            $( "#selExamenForm" ).val('-1');
        }
    });
    
    $('#multiples-dias').change(function(){
        if($(this).is(':checked')){
            //console.log('checked');
           $('.fechas').show();
        }else{
            $('.fechas').hide();
        }
    });
    
    function actualizaCalendario($calendar){
        
        $.ajax({
            type:'POST',
            dataType:'json',
            //url:  '/agenda',
            url:  $('#agenda').val(),
            data:{
                'idExamen'  : $('#selExamen').val(),
                'idGrupo'   : $('#selGrupo').val(),
                'conEstatus'  : null
            },
            beforeSend: function() {
            },
            success:function(json){
                //$calendar.weekCalendar('clear');
                
                $calendar.weekCalendar({
                    data : function(start, end, callback){
                        callback(getEventData(json));
                    }
                });
                $calendar.weekCalendar("refresh");
            },
            error: function(json){
                //alert(" error" );
            }
        });
    }
    
    function guardaEvento(calEvent, $calendar){
           calEvent.start = $calendar.weekCalendar("formatDate", calEvent.start,'Y-m-d H:i:s');
           calEvent.end = $calendar.weekCalendar("formatDate", calEvent.end,'Y-m-d H:i:s');
           var fechaInicio = $('#dateStart').val();
           var fechaFinal  = $('#dateEnd').val();
           var dateStart   = (fechaInicio ==  "") ? "" : darFormato(fechaInicio);
           var dateEnd     = (fechaFinal ==   "") ? "" : darFormato(fechaFinal);
          
	   $.ajax({
           type:'POST',
           dataType:'json',
           url:  $('#urlCreaEvento').val(),
           data:{ 
                'idExamen'  : $('#selExamenForm').val(),
                'idGrupo'   : $('#selGrupoForm').val(),
                'dateStart' : dateStart,
                'dateEnd'   : dateEnd,
                'calEvent'  : calEvent
               },
           beforeSend: function() {
           },
           
           success:function(json){
               datoexiste = typeof json['duplicado'] !== 'undefined' && json['duplicado']=== '1' ? true : false;
               if(datoexiste === true){
                    $calendar = $('#calendar');
                    var $dialogConfirma = $("#modalExamen");
                    //var x = formatoFechas(calEvent.start);
                    $(".dateInicioAnterior").text(json['fechainicio']); 
                    $(".dateFinalAnterior").text(json['fechafinal']);
                    $(".dateInicioActual").text(formatoFechas(calEvent.start));
                    $(".dateFinalActual").text(formatoFechas(calEvent.end));
                    $("#idagenda").val(json['id_agenda']);
                    $dialogConfirma.dialog({
                        width:500,
                        modal: true,
                        title: "Examen Agendado",
                        close: function() {
                            $dialogConfirma.dialog("destroy");
                            $dialogConfirma.hide();
                            $dialogConfirma.dialog("close");
                            $('#calendar').weekCalendar("serializeEvents");
                        },  
                        buttons:{
                            Si: function(){
                                                    calEvent.accion = "editar";
                                                    
                                                    calEvent.id = $("#idagenda").val();
//                                                    calEvent.start = new Date(dateInicioActual);
//                                                    calEvent.end = new Date(dateFinalActual);
                                                    calEvent.title = $('#selGrupoForm').val(); //titleField.val();
                                                    calEvent.examen = $('#selExamenForm').val(); //examenField.val();
                                                    calEvent.body = $('#body').val()
                                                    $calendar.weekCalendar("updateEvent", calEvent); 
                                                    guardaEvento(calEvent, $calendar); 
                                                     $dialogConfirma.dialog("close");
                            },
                            No : function(){
                                $dialogConfirma.dialog("close");
                            }
                        }
                    }).show();
                     //$calendar.weekCalendar("refresh");
               }
               
                console.log(json);
                $calendar.weekCalendar({
                    data : function(start, end, callback){
                        callback(getEventData(json));
                    }
                });
                $calendar.weekCalendar("refresh");
           },
           error: function(json){
                //  $.mensaje("crearMensaje", { tipo_mensaje: "alerta", mensaje: "Hubo un error al procesar los datos" });
                console.log("Hubo un error al procesar los datos: " + json);
           }
	   }); 
   }
   

    function darFormato(strfecha){
      var arrfechahora = strfecha.split("/");
       return arrFechaHora =arrfechahora[2] + "-" + arrfechahora[1] + "-" + arrfechahora[0];
    }

    function formatoFechas(strfecha){
	var arrfechahora = strfecha.split(" ");
        var arrfecha = arrfechahora[0].split("-");
	return arrFechaHora = arrfecha[2] + "/" + arrfecha[1] + "/" + arrfecha[0] + " " + arrfechahora[1];
}
   
      /*
   $('#start').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
        startDate:	'1986/01/05'
    });*/
    //$('#dateStart').datetimepicker({value:'2015/04/15 05:03',step:10});
    //$('#dateEnd').datetimepicker({value:'2015/04/15 05:03',step:30});
    // $.datepicker.regional[ "es" ],
    $('#dateStart').datepicker({ 
        minDate: -5,
        
        //changeMonth: true,            
        onClose: function( selectedDate ) {
            $( "#dateEnd" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    $('#dateEnd').datepicker({
        //defaultDate: "+1w",
        //changeMonth: true,        
        onClose: function( selectedDate ) {
            $( "#dateStart" ).datepicker( "option", "maxDate", selectedDate );
        }
    });
    
     $("#frmEventos").validate({
            errorPlacement: function (error, element) {
                if (element.attr("name") === "title") {
                    error.appendTo("#errorGrupoEvent");
                }
                if (element.attr("name") === "examen") {
                    error.appendTo("#errorExamenEvent");
                }
                if (element.attr("name") === "body") {
                    error.appendTo("#errorDescripcionEvent");
                }
            },
            rules: {
                title: {
                    required: true,
                    min: 1
                },
                examen: {
                    required: true,
                    min: 1
                },
                body: {
                    required: true,
                }
            },
            messages: {
                title: "Favor de seleccionar un grupo",
                examen: "Favor de seleccionar un examen",
                body : "Favor de ingresar una descripcion",
                //"Favor de ingresar una descripcion"
            }
        });

});
