$(document).ready(function() {
	$(document).foundation();			
	
	$.plantilla({
		urlSitio	: $('#urlSitio').val(),
		nombreSitio 	: 'Diagn&oacute;sticos Educativos',
		header		: true,
		footer		: true,
                urlLogo         : '/css/images/encabezado_pagina/Logo_HomeDE.png',
	/*	urlFacebook	: '[url del facebook del sitio en cuestión]',
		urlTwitter	: '[url del twitter en cuestión]',
		urlYoutube	: '[url de youtube en cuestión]',*/
		headColor	: '#333333',
                colorBotones	: 'azul',
                colorBotonesSec : 'naranja',
		footColor	: '#333333'
	});
	
});