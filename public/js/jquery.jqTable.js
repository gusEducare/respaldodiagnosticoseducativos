(function($){	
	/*var idSeleccionado='';
	var boton = '';*/
	var contGlobal  = 0;
	methods = {
		init:function () {
		},
		createContainers:function(data){
			 var settings = $.extend( {
					'targetContainer' : '',
					'classRow'  : 'row',
					'classLarge': 'large-4',
					'classSmall': 'small-12',
					'classColum' : 'columns',
	    			'classEnd' : 'end',
	    			'subTitleClass' : '', 
	    			'tooltipTitle' : '',
	    			'classIconTitle' : '',
	    			'classIconDelete' : '',
	    			'tooltipDelete' : '',
	    			'tooltipContent1' :'',
	    			'classIconContent1' : '',
	    			'classContent1' : '',
	    			'tooltipContent2' : '',
	    			'classIconContent2' : '',
	    			'classContent2' : ''
			}, data);
			var contadorClaseEnd = 0;
			var strClassEnd = "";
			var dataGroup = data.dataGroup;
			var html = '<div class="'+ settings.classRow +'">';
			$.each( dataGroup, function( keyGroup, valueGroup ){
				var value = $.extend({
					'id' : '',
    				'title' :  '',
    				'subTitle' : '',
    				'content1' : '',
    				'content2' : '',
    				'urlDeleteElement' : '',
    				'buttons' : {}
				}, valueGroup);
				
				contadorClaseEnd++;
				if(dataGroup.length == contadorClaseEnd){
					//classEnd
					strClassEnd = settings.classEnd;
				}
				else{
					strClassEnd = "";
				}
				html += '<div id="table-'+value.id+'" class="principal-container '+settings.classLarge+' '+settings.classSmall+' '+settings.classColum+' '+strClassEnd+'">';// inicia div contenedor
					html += '<div id="table-container-'+value.id+'"  class="table-container">';
						//inicio titulo
						html += '<div class="tituloTable">';
                                                html += '<div class="row">';
                                                html += '<div class="large-10 small-10 columns">';
							if(settings.tooltipTitle.length != 0){
								html += '<span data-tooltip aria-haspopup="true"  data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite tituloGrupo" title="'+settings.tooltipTitle+'">';
                                                                //html += '<input id="table-container-27" class="large-8 medium-7 small-8 columns tamano valid" value="General" maxlength="40" aria-invalid="false"'+settings.tooltipTitle+'">';
							}
								if(settings.classIconTitle.length != 0){
									html += '<i  class="'+settings.classIconTitle+'"></i> &nbsp;'
								}
        
								html += value.title;
							if(settings.tooltipTitle.length != 0){
								html += '</span>';
							}
                                                html += '</div>';
                                                html += '<div class="large-2 small-2 columns">';
                                                        
                                                        
                                                        
                                                        
                                                        if(typeof(value.buttons) != 'undefined' && typeof(value.buttons) == 'object') {
                                                                $.each(value.buttons, function(keyButtons, valButtonsShow){
                                                                        var valButtonsShow= $.extend({
                                                                                        'id' : '',
                                                                                'clase' : '',
                                                                                'type' : '',
                                                                                'classIcon' : '',
                                                                                'text' : '',
                                                                                'toolTip' : '',
                                                                                'action' : '',
                                                                                'subElements' : null
                                                                                },valButtonsShow);

                                                                        if(valButtonsShow.type == 'showSubTable'){
                                                                                //agregar funcionalidad para mostrar subelementos 
                                                                                if(valButtonsShow.toolTip.length != 0){
                                                                                        html += '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite deleteTable" title="'+valButtonsShow.toolTip+'">';
                                                                                }
                                                                                if(valButtonsShow.classIcon.length != 0){
                                                                                        if(typeof(valButtonsShow.clase) == 'undefined'){
                                                                                                valButtonsShow.clase = '';
                                                                                        }
                                                                                        if(typeof(valButtonsShow.id) == 'undefined'){
                                                                                                valButtonsShow.id = '_' + Math.random().toString(36).substr(2, 9);
                                                                                        }
                                                                                        html += '<i id="'+valButtonsShow.id+'" class="fa fa-chevron-circle-down fa-2x colorWhite showSubTable " ></i>';
                                                                                }
                                                                                if(valButtonsShow.toolTip.length != 0){
                                                                                        html += '</span >';
                                                                                }

                                                                        }
                                                                });
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
						html += '</div>';
						html += '</div>';
						html += '</div>';//close tituloTable
						//fin titulo
                                                //btnSubElements 
                                                
                                                
						//inicia contenido
						
						if(value.subTitle.length != 0){
							html += '<div class="'+settings.subTitleClass+'">';
							html += value.subTitle;
							html += '</div>'; //close suntitle class
						}
						
						if(value.content1.length != 0){
							html += '<div class="'+settings.classContent1+'">';
							if(settings.tooltipContent1.length != 0 ){
								html += '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite" title="'+settings.tooltipContent1+'">'
							}
							
							
							if(settings.classIconContent1.length != 0 ){
								html += '<i  class="'+settings.classIconContent1+'" ></i>&nbsp;';
							}
							
								html += value.content1;
							
							if(settings.tooltipContent1.length != 0){
								html += '</span>';
							}
							html += '</div>';
						}
						
						if(value.content2.length != 0){
							html += '<div class="'+settings.classContent2+'">';
							
								if(settings.tooltipContent2.length != 0 ){
									html += '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite" title="'+settings.tooltipContent2+'">';
								}
								if(settings.classIconContent2.length != 0 ){
									html += '<i  class="'+settings.classIconContent2+'" ></i>';
								}
								if(settings.tooltipContent2.length != 0){
									html += '</span>&nbsp;';
								}
								html += value.content2;
							html += '</div>';
								
						}
						
						//fincontenido 
						//inicia botones
						var buttons = value.buttons;
						
						html += '<div class="buttonsContainer">';
							if (typeof(buttons) != 'undefined' && typeof(buttons) == 'object') {
								$.each(buttons, function(keyButtons, valButtons){
									var valueButtons= $.extend({
											'id' : '',
				    						'clase' : '',
				    						'type' : '',
				    						'classIcon' : '',
				    						'text' : '',
				    						'toolTip' : '',
				    						'action' : '',
				    						'subElements' : null
										},valButtons);
									switch(valueButtons.type){
                                                                                case 'button':
											if(valueButtons.toolTip.length != 0){
												html += '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite" title="'+valueButtons.toolTip+'">';
											}
											if(valueButtons.classIcon.length != 0){
												if(typeof(valueButtons.clase) == 'undefined'){
													valueButtons.clase = '';
												}
												if(typeof(valueButtons.id) == 'undefined'){
													valueButtons.id = '_' + Math.random().toString(36).substr(2, 9);
												}
                                                                                                if(typeof(valueButtons.classIcon) == 'undefined'){
													valueButtons.classIcon = 'fa fa-square';
												}
												html += '<i id="'+valueButtons.id+'" class="'+valueButtons.classIcon+' buttonTable" onclick="'+valueButtons.action+'" ></i>';
											}
											if(valueButtons.toolTip.length != 0){
												html += '</span >';
											}
										break;
                                                                                case 'link':
											html += '<a href="'+valueButtons.action+'">';
											if(valueButtons.toolTip.length != 0){
												html += '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite" title="'+valueButtons.toolTip+'">';
											}
											if(valueButtons.classIcon.length != 0){
												if(typeof(valueButtons.clase) == 'undefined'){
													valueButtons.clase = '';
												}
												if(typeof(valueButtons.id) == 'undefined'){
													valueButtons.id = '_' + Math.random().toString(36).substr(2, 9);
												}
                                                                                                if(typeof(valueButtons.classIcon) == 'undefined'){
													valueButtons.classIcon = 'fa fa-square';
												}
												html += '<i id="'+valueButtons.id+'" class="'+valueButtons.classIcon+' buttonTable"  ></i>';
											}
											if(valueButtons.toolTip.length != 0){
												html += '</span >';
											}
                                                                                        html += '</a>';
										break;
										
										case 'buttonsGroup':
									        //crear menu de iconos
											if(valueButtons.toolTip.length != 0){
												html += '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite" title="'+valueButtons.toolTip+'">';
											}
											if(valueButtons.classIcon.length != 0){
												if(typeof(valueButtons.clase) == 'undefined'){
													valueButtons.clase = '';
												}
												if(typeof(valueButtons.id) == 'undefined'){
													valueButtons.id = '';
												}
												html += '<i id="'+valueButtons.id+'"  class="'+valueButtons.classIcon+' buttonTable '+valueButtons.clase+'" data-dropdown="dropMenu'+value.id+'" aria-controls="dropMenu'+value.id+'" aria-expanded="true" data-options="align:left"   ></i>';
												/*
												 * <i class="fa fa-cog buttonGrupo" ></i>
												 */
											}
											if(valueButtons.toolTip.length != 0){
												html += '</span >';
											}
											
											var subButtons = valueButtons.subElements;
											
											if (typeof(subButtons) != 'undefined' && typeof(subButtons) == 'object') {
											html += '<div id="dropMenu'+value.id+'" data-dropdown-content class="large f-dropdown content" >';
												html += '<div class="icon-bar three-up">';
												$.each(subButtons, function(keySubButtons, valueSubButtons){
													var href= 'href="#"';
													if(valueSubButtons.type.length != 0){
														switch(valueSubButtons.type){
															case 'link':
																href = ' href="'+valueSubButtons.action+'" ';
																break;
															case 'button':
																href = '  href="#" onclick=\''+valueSubButtons.action+'; return false;\'';
																break;
														}
													}
													if(typeof(valueSubButtons.clase) == 'undefined'){
														valueSubButtons.clase = '';
													}
													if(typeof(valueSubButtons.id) == 'undefined'){
														valueSubButtons.id = '';
													}
													html += '<a '+href+' class="item '+valueSubButtons.clase+'" id="'+valueSubButtons.id+'">';
														if(valueSubButtons.classIcon.length != 0){
															html += '<i  class="'+valueSubButtons.classIcon+'"></i>';
														}
														if(valueSubButtons.text.length != 0){
															html += '<label>'+valueSubButtons.text+'</label>';
														}
													html += '</a>';
													
												});
												html += '</div>';
											html += '</div>';
											}
											break;
										default:
											//default code block
									}
								});
							}
                                                        
                                                        //urlDeleteElement
                                                        if(settings.tooltipDelete.length != 0){
								html += '<span data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" class="has-tip tip-top  colorWhite deleteTable" title="'+settings.tooltipDelete+'">'
							}
							if(value.urlDeleteElement.length != 0){
								html += '<a class="buttonDeleteElement" href="'+valueGroup.urlDeleteElement+'" >';
								if(settings.classIconDelete.length != 0){
									html += '<i  class="'+settings.classIconDelete+'   buttonGrupo buttonTable" ></i>';
								}
								else{
									html += 'Delete item';
								}
								html += '</a>';
							}
							if(settings.tooltipDelete.length != 0){
								html += '</span>';
							}
						html += '</div>';
						//fin botones
					html += '</div>';//close table-container
					//inicia subTable
					html += '<div class="subTableContainer"  id="subTable-'+value.id+'" style="display:none;" >';
						html += '<input type="hidden" id="urlSubTable-'+value.id+'" value="'+value.urlSubTable+'" />';
						html += '<input type="hidden" id="postDataSubTable-'+value.id+'" value=\''+value.postData+'\' />';//inicia  panelGroup
						html += '<div style="width:100%;"><br/></div>';//separador
						html += '<div id="panelGroup-'+value.id+'" class="row"  >';//inicia  panelGroup
						
						html += '</div>';//fin  panelGroup*/
					html += '</div>';//close subTable
				html += '</div>';//close div contenedor
			});
			html += '</div>';//close row
			
			$('#'+settings.targetContainer).html(html);
			//$(document).foundation();
			
			$('.showSubTable').bind('click', function(){
				//expand table + id
				//get id
				var id = $(this).prop('id').split("-");
				id = id[1];
				
				if($('#subTable-'+id).is(":visible")){
					$('#subTable-'+id).slideUp('slow',function(){
						$( "#table-"+id ).animate({'width': current_width}, 500, function(){
							$( "#table-"+id ).css('width','');
						});
					});
					
					$('#btnShowUser-'+id).removeClass('fa-chevron-circle-up');
					$('#btnShowUser-'+id).addClass('fa-chevron-circle-down');
					
				}
				else{
					
					$('#btnShowUser-'+id).removeClass('fa-chevron-circle-down');
					$('#btnShowUser-'+id).addClass('fa-chevron-circle-up');
					
					current_width = $( "#table-"+id ).css('width');
					if($('#panelGroup-'+id).is(':empty')){
						$.ajax({
							type:'POST',
							dataType: 'json',
							url: $('#urlSubTable-'+id).val(),
							data:JSON.parse( $('#postDataSubTable-'+id).val()),
							beforeSend: function(){
								$( "#table-container-"+id ).block({ 
					                message: '<i class="fa fa-spinner fa-spin fa-2x"></i>', 
					            }); 
							},
							success: function(json){
								$( "#table-container-"+id ).unblock(); 
								
									var htmlSubTable = methods.crateSubTable(json);
									$('#panelGroup-'+id).html(htmlSubTable);
								
								$( "#table-"+id ).animate({width: '100%'}, 500, function(){
									$('#subTable-'+id).slideDown('slow');
								});
								methods.addHandlerEdit();
								$(".tips").foundation();
							},
							error: function(json){
								$( "#table-"+id ).unblock();
							}
						});
					}
					else{
						$( "#table-"+id ).animate({width: '100%'}, 500, function(){
							$('#subTable-'+id).slideDown('slow');
						});
						
					}
					
				}
				
			});
			
		},
		crateSubTable:function(arrSubTable){
			
			
			arrSubTable = $.extend({
				'classRow'  : 'row',
		    	'classLarge': 'large-12',
		    	'classSmall': 'small-12',
		    	'classSizeContainerButtons' : '',
		    	'classColum' : 'columns',
		    	'classEnd' : 'end',
		    	'inlineEdit': true,
		    	'dataSubElement' : {}
			},arrSubTable);
			var contadorEnd = 0;
			var dataSubElement = arrSubTable.dataSubElement;
			var strClassEndSub = '';
			var htmlSub = '';
			$.each( dataSubElement, function( keySubElement, valSubElement ) {
				var valueSubElement = $.extend({
					'id' : "",
					'fields' : {},
					'buttons' : {}
				},valSubElement);
				contadorEnd++;
				
				if(dataSubElement.length == contadorEnd){
					strClassEndSub = arrSubTable.classEnd;
				}
				else{
					strClassEndSub = "";
				}
				htmlSub += '<div id="subElementContainer-'+valueSubElement.id+'" class="'+arrSubTable.classLarge+' '+arrSubTable.classSmall+' '+arrSubTable.classColum+' '+strClassEndSub+'">';//inicia contenedor sub elemento
					
						htmlSub += '<div class="subElementContainer">';//inicia subElementContainer
						var fields = valueSubElement.fields;
						var contadorEndField = 0;
						var strClassEndSub = '';
							$.each( fields, function( keyFields, valFields ) {
								var valueFields = $.extend({
									'id' : '',
									'classSize' : 'small-3',
				    				'iconClass' : '',
				    				'labelText' : '',
				    				'fielName' : '',
				    				'name' : '',
				    				'value' : '',
				    				'editInline' : false,
				    				'urlEdit': '',
				    				'postData' : ''
				    				
								},valFields);
								contadorEndField++;
								
								if(fields.length == contadorEndField){
									strClassEndSub = arrSubTable.classEnd;
								}
								else{
									strClassEndSub = "";
								}
								htmlSub += '<div  class="'+valueFields.classSize+' '+arrSubTable.classColum+' '+strClassEndSub+'">';//inicia div contenedor field
											htmlSub += '<div  class="fieldSubElement">';//inicia div field
											if(valueFields.iconClass.length != 0){
												//htmlSub += '<span class="fa-stack fa-lg iconRow" style="color: rgb(193, 193, 193);">';
												//htmlSub += '<i class="fa fa-circle fa-stack-2x"></i>';
												htmlSub += '<i class="'+valueFields.iconClass+' iconField""></i>';
												//htmlSub += '</span>';
											}
											else if(valueFields.labelText.length != 0){
												htmlSub += ' <span class="labelField" >';
													htmlSub += valueFields.labelText;
												htmlSub += '</span>';
											
											}
											var editClass = "";
											if(valueFields.editInline === true){
												editClass = "editInline";
											}
											htmlSub += '<label id="label-'+valueFields.id+'" class="labelFielValue '+editClass+'" >'+valueFields.value+'</label>';
											htmlSub += '<input id="'+valueFields.id+'"  class="inputSubElementRow no-margin disabled" type="text" name="'+valueFields.fieldName+'"  placeholder="'+valueFields.name+'" value="'+valueFields.value+'" style="display:none;"/> ';
										htmlSub += '</div>';//fin div field
										htmlSub += '<div  class="containerErrorMsj" id="error-'+valueFields.id+'" style="display:none;">';//inicia div field
										htmlSub += '</div>';//inicia div field
										htmlSub += '<input id="url-'+valueFields.id+'" type="hidden" value="'+valueFields.urlEdit+'"/> ';
										htmlSub += '<input id="postData-'+valueFields.id+'" type="hidden" value=\''+valueFields.postData+'\' /> ';
										htmlSub += '<input id="validate-'+valueFields.id+'" type="hidden" value=\''+valueFields.validate+'\' /> ';
										
								htmlSub += '</div>';//fin div contenedor field
							});
							
							if(typeof(valueSubElement.buttons) != "undefined"){
								var buttonSubElement = valueSubElement.buttons;
								if(buttonSubElement.length > 0 ){
									htmlSub += '<div class="'+arrSubTable.classSizeContainerButtons+' '+arrSubTable.classColum+' '+arrSubTable.classEnd+' containerButton text-right ">';//inicia div containerButton
										$.each( buttonSubElement, function( keyButton, valButton ) {
											
											var valueButton = $.extend({
												'id' : '',
					    						'tooltip' : '',
					    						'clase': '',
					    						'type' : 'link',
					    						'iconClass' : '',
					    						'toolTip' : '',
					    						'action' : ''
												
											},valButton);
											if(valueButton.tooltip.length != 0){
												var tip =  'data-tooltip aria-haspopup="true" data-options="disable_for_touch:true" title="'+valueButton.tooltip+'"';
												var classTip = ' tips has-tip tip-top   ';
											}
											var tagLink = '';
											var tagLinkClose = '';
											var action = "";
											if(valueButton.type == "link"){
												 tagLink = '<a class="'+valueButton.clase+'" href="'+valueButton.action+'">';
												 tagLinkClose = '</a>';
											}
											else if(valueButton.type == "button"){
												action = "onclick='"+valueButton.action+"'";
											}											
												htmlSub += tagLink;
												htmlSub += '<span '+tip+' class="fa-stack fa-lg buttonSubElement '+classTip+'" '+action+' >';
													htmlSub += '<i class="fa fa-circle fa-stack-2x"></i>';
													htmlSub += '<i class="'+valueButton.iconClass+' fa-stack-1x fa-inverse"></i>';
												htmlSub += '</span>';
												htmlSub += tagLinkClose;
										});
									htmlSub += '</div>';//fin div containerButton
								}
							
							}
							
						htmlSub += '</div>';//fin subElementContainer
				htmlSub += '</div>';//fin contenedor sub elemento
						
					});
			contGlobal++;
			return htmlSub;
		},
		addHandlerEdit:function(){
			//
			
			$('.editInline').bind('click', function(){
				var id = $(this).prop('id').split('-');
				if($("#label-"+id[1]+'-'+id[2]).hasClass('labelError')){
					$("#label-"+id[1]+'-'+id[2]).removeClass('labelError');
				}
				if($("#error-"+id[1]+'-'+id[2]).is(":visible")){
					$("#error-"+id[1]+'-'+id[2]).slideUp();
				}
				
				$(this).animate({
					width: "toggle",
					opacity: "toggle"
				},300 ,function(){
					$("#"+id[1]+'-'+id[2]).animate({
						width: "toggle",
						opacity: "toggle"
					},300,function(){
						$("#"+id[1]+'-'+id[2]).focus();
					});
				});
				
			});
			
			 $('.inputSubElementRow').focusout(function() {
				 
				 if($(this).is(':visible')){
					 var id = $(this).prop('id');
					 methods.editRow(id);
				 }
				 
			 });
			 
			 $('.inputSubElementRow').keypress(function(e) {
				 
				 if(e.which == 13) {
					 if($(this).is(':visible')){
						 var id = $(this).prop('id');
						 methods.editRow(id);
					 }
				}
			});
			 
			 
		},
		editRow:function(id){
				 var urlEdit = $('#url-'+id).val();
				 var postData = JSON.parse($('#postData-'+id).val());
				 //agregar campo modificado y nombre del campo modificado
				 var sendValue = $('#'+id).val().trim();
				 postData[$('#'+id).prop('name')] = sendValue;
				 postData['value'] = sendValue;
					$.ajax({
						type:'POST',
						dataType: 'json',
						url: urlEdit,
						data:postData,
						beforeSend: function(){
							$("#label-"+id).closest('.fieldSubElement').block({
								message: '<i class="fa fa-spinner fa-spin"></i>',
							}); 
						},
						success: function(json){
							
							//revisar si el response 
							if(json['result']!= true){
								//mostrar mensaje de error y agregar la clase error al label 
								var htmlError = '<i class="'+json['iconClassError']+' iconField"></i>';
									htmlError += '<span class="">'+json['msj']+'</span>';
								$('#error-'+id).html(htmlError);
								$('#error-'+id).slideDown();
								$('#label-'+id).addClass("labelError");
								 
									
							}
							$("#label-"+id).html(sendValue);
							$("#label-"+id).closest('.fieldSubElement').unblock();
							//alert('done');
							$('#'+id).animate({
								 width: "toggle",
								 opacity: "toggle"
							 },300 ,function(){
								 $("#label-"+id).animate({
									 width: "toggle",
									 opacity: "toggle"
								 },300, function(){});
								 
							 });
							
						},
						error: function(json){
							
							$("#label-"+id).closest('.fieldSubElement').unblock(); 
							$("#label-"+id).html(sendValue);
							htmlError += '<label class="">Ocurrió un error inesperado, por favor intenta más tarde.</label>';
							$('#error-'+id).html(htmlError);
							$('#error-'+id).slideDown();
							$('#label-'+id).addClass("labelError");
							$("#label-"+id).animate({
								 width: "toggle",
								 opacity: "toggle"
							 },300, function(){});
						}
					});					
			
		}
	};
	$.fn.jqTable = function (method){
		if(methods[method]){
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		}else if(typeof method === 'object' || ! method ) {			
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'El metodo ' +  method + ' no existe en jqMensaje' );
		}   
	}; 
$.table=$.fn.jqTable;
})(jQuery);