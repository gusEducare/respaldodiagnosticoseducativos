var readyToSend = true;
var waitingResponse = false;
$(document).ready(function(){
    
    
    $(document).ajaxStart(function() {
        $('#btnConfirmarLicencias').addClass('disabled');
        $('#buttons-send').block({
            message: '<i class="fa fa-spinner fa-spin "></i>Procesando...' 
        });
    });

    $(document).ajaxStop(function() {
        $('#btnConfirmarLicencias').removeClass('disabled');
        $('#buttons-send').unblock();
    });
    
    $('.btnDeleteRow').click(function(){
        var id = $(this).prop('id'); 
        id = id.split('-');
        $('#idDeleteRow').val(id[1]);
        $('#modalDeleteRow').foundation('reveal', 'open');
       
    });
    
    $('#confirmDeleteRow').click(function(){
         $('#row-'+$('#idDeleteRow').val()).remove();
         $('#modalDeleteRow').foundation('reveal', 'close');
         
    });
    
    $('#cancelDeleteRow').click(function(){
        $('#modalDeleteRow').foundation('reveal', 'close');
    });
    
    
    $('.txtLicencia').change(function(){
        validaLicencia($(this).prop('id'));
    });
    
    $('#btnConfirmarLicencias').click(function(){
        var boolError = false;
        $(".txtLicencia").each(function(){			
            if($(this).hasClass("error")){
                boolError = true;
            }
        });
        if(!boolError){
            if(!$('#btnConfirmarLicencias').hasClass('disabled')){
                var arrUsers = new Array();
                
                $('.txtLicencia').each(function(){
                    var id = $(this).prop('id').split('-');
                    var arrTempUser =  new Array(id[1], $(this).val() ); 
                    arrUsers.push(arrTempUser);
                });
                if(arrUsers.length > 0){
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: $('#urlActivarLicencia').val(),
                        data: {
                            "arrUsers" : arrUsers
                        },
                        beforeSend: function() {
                            $('body').block({ 
                                message: '<i class="fa fa-spinner fa-spin fa-2x"></i><h5 >Procesando...</h5>', 
                            });
                        },
                        success: function(json) {
                            $('body').unblock();
                            var errors = 0;
                            var licTemp = "";
                            $.each(json, function(i, item) {
                                
                                console.log(item);
                                licTemp = "";
                                if(item[0] === false){
                                    $('#row-'+item[2]).css('background-color', '#ec5840');
                                    $('#row-'+item[2]).css('border-bottom','3px solid #000000');
                                    errors ++;
                                }
                                else{
                                    $('#row-'+item[2]).css('background-color', '#43ac6a');
                                    $('#row-'+item[2]).css('border-bottom','3px solid #000000');
                                    $('#row-'+item[2]).addClass('success');
                                    $('#action-'+item[2]).html('<i class="fa fa-check-circle fa-2x" style="color:#FFFFFF;" ></i>');
                                    licTemp = $('#txtLicencia-'+item[2]).val();
                                    $('#txtLicencia-'+item[2]).remove();
                                    $('#tdTxtLicencia-'+item[2]).html('<strong>'+licTemp+'</strong>');
                                    
                                }
                                
                                if(errors > 0){
                                    $.blockUI({ 
                                        message: '<p>Algunos elementos de la lista no pudieron agregarse de forma correcta. Por favor intenta nuevamente.</p>', 
                                        timeout: 6000 
                                    }); 
                                }
                                else{
                                    $('#btnConfirmarLicencias').hide();
                                }
                                
                                
                            });     
                        },
                        error: function(json) {
                            $('body').unblock();
                            $.blockUI({ 
                                message: '<h1>Ocurrió un error al procesar tu solicitud. Por favor intenta más tarde.</h1>', 
                                timeout: 2000 
                            }); 

                        }
                    });
                }
            }
        }
        else{
            
        }
    });
    
    
    
});

function validaLicencia(id){
    
        var arrayId = id.split('-');
        $(".txtLicencia").each(function(){
            $(this).removeClass('error');
            $('#'+$(this).prop('id')+"ImgError").hide();
        });
	var idInput = id.split("-");
	if($("#"+id).val().length > 0){
		var _boolExiste = false;

		$(".txtLicencia").each(function(){
			if($(this).val() == $("#"+id).val() && $(this).prop('id') != id){
                            $(this).addClass('error');
                            $("#"+id).addClass('error');
				_boolExiste = true;
			}
		});
		if(_boolExiste === true){
			
			if(!$("#"+id).hasClass("error")){
				$("#"+id).addClass("error");
			}
			$("#"+id+"ImgError").html("La licencia se repite en algún registro anterior o posterior. La licencia solo puede ser utilizada para un usuario.");
			if(!$("#"+id+"ImgError").is(":visible")){
				$("#"+id+"ImgError").show();
			}
		}
		else{
                    
		//consulta ajax licencia disponible
				var urlWebService = $("#urlWebService").val();
                                
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: urlWebService,
						data: {
							"_strLicencia" 		: $("#"+id).val()
						},
						beforeSend: function() {
							$("#row-"+arrayId[1]).block({ 
							        message: '<i class="fa fa-spinner fa-spin fa-2x"></i><h5 >Procesando...</h5>', 
							    });
						},
						success: function(json) {
							$("#row-"+arrayId[1]).unblock();
							if(json['valida'] === false){
								//TODO: mensaje usuario no disponible 
								
								if(!$("#"+id).hasClass("error")){
									$("#"+id).addClass("error");
								}
								$("#"+id+"ImgError").html(json['msj']);
								if(!$("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").show();
								}
							}
							else{
								
								if($("#"+id).hasClass("error")){
									$("#"+id).removeClass("error");
								}
								if($("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").hide();
								}
							}
						},
						error: function(json) {
							//TODO: agregar mensaje fancy de error 
							
						}									
				});
		}
	}else{
		$('#nomExamen'+idInput[1]).val("");
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("El campo licencia no puede estar vacio.");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
}



