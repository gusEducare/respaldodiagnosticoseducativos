
$(document).ready(function(){
	addEvtLister();
	$(document).foundation();
	
});  



function addEvtLister(){
	$(".txtNombre").blur(function(){
		validaNombre($(this).attr("id"));
	});
	
	$(".txtApellido").blur(function(){
		validaApellido($(this).attr("id"));
	});
	
	$(".txtUser").blur(function(){
		validaUser($(this).attr("id"));
	});
	
	$(".txtPass").blur(function(){
		validaPass($(this).attr("id"));
	});
	
	$(".txtFecha").blur(function(){
		validaFecha($(this).attr("id"));
	});
	
	$(".txtLicencia").blur(function(){
		validaLicencia($(this).attr("id"));
	});
	
	$("#txtInicioCiclo").blur(function(){		
		validaIncioCiclo();
	});
	$("#txtFinCiclo").blur(function(){
		validaFinCiclo();
	});
	$("#txtDescripcion").blur(function(){
		validaDescripcionGrupo();
	});
}


function validaDescripcionGrupo(){
	if($("#txtDescripcion").val().length < 1){
		$("#errorDescripcion").html("Este campo no puede estar vacio.");
		$("#errorDescripcion").slideDown();
		$("#lblDescripcion").addClass("error");
		$("#txtDescripcion").addClass("error");
	}
}


function validaNombre(id){
	if($("#"+id).val().length > 0){
		if($("#"+id).hasClass("error")){
			$("#"+id).removeClass("error");
		}
		if($("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").hide();
		}
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").html("El campo nombre no puede estar vacio.");
			$("#"+id+"ImgError").show();
		}
	}
	
}

function validaIncioCiclo(){
	if($("#txtInicioCiclo").val().length < 1){
		$("#errorInicioCiclo").html("Este campo no puede estar vacio.");
		$("#errorInicioCiclo").slideDown();
		$("#lblInicioCiclo").addClass("error");
		$("#txtInicioCiclo").addClass("error");
	}
	else if(!$("#txtInicioCiclo").val().match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/)){
		
		$("#errorInicioCiclo").html("La fecha debe tener un formato dd/mm/aaaa  p. ej. 26/02/2015");
		$("#errorInicioCiclo").slideDown();
		$("#lblInicioCiclo").addClass("error");
		$("#txtInicioCiclo").addClass("error");
	}
}


function validaFinCiclo(){
	if($("#txtFinCiclo").val().length < 1){
		$("#errorFinCiclo").html("Este campo no puede estar vacio.");
		$("#errorFinCiclo").slideDown();
		$("#lblFinCiclo").addClass("error");
		$("#txtFinCiclo").addClass("error");
	}
	else if(!$("#txtFinCiclo").val().match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/)){
		$("#errorFinCiclo").html("La fecha debe tener un formato dd/mm/aaaa  p. ej. 26/02/2015");
		$("#errorFinCiclo").slideDown();
		$("#lblFinCiclo").addClass("error");
		$("#txtFinCiclo").addClass("error");
	}
}



function validaApellido(id){
	if($("#"+id).val().length > 0){
		if($("#"+id).hasClass("error")){
			$("#"+id).removeClass("error");
		}
		if($("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").hide();
		}
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").html("El campo apellido no puede estar vacio.");
			$("#"+id+"ImgError").show();
		}
	}
}

function validaUser(id){
	if($("#"+id).val().length > 0){
		var _boolExiste = false;
		$(".txtUser").each(function(){
			if($(this).val() == $("#"+id).val() && $(this).attr('id') != id){
				_boolExiste = true;
			}
		});
		if(_boolExiste === true){
			if(!$("#"+id).hasClass("error")){
				$("#"+id).addClass("error");
			}
			$("#"+id+"ImgError").html("Este nombre de usuario se repite en algún registro anterior o posterior.");
			if(!$("#"+id+"ImgError").is(":visible")){
				$("#"+id+"ImgError").show();
			}
		}
		else{
		//consulta ajax username disponible
				var urlSitio = $("#urlSitio").val();
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: urlSitio,
						data: {
							"user" : $("#"+id).val(),
							"_boolValidarUsuario": true
						},
						beforeSend: function() {
							$("#"+id).addClass("textProcesando");
						},
						success: function(json) {
							$("#"+id).removeClass("textProcesando");
							if(json["_boolResponse"] === true){
								//TODO: mensaje usuario no disponible 
								if(!$("#"+id).hasClass("error")){
									$("#"+id).addClass("error");
								}
								$("#"+id+"ImgError").html(json["_strMsg"]);
								if(!$("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").show();
								}
							}
							else{
								if($("#"+id).hasClass("error")){
									$("#"+id).removeClass("error");
								}
								if($("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").hide();
								}
							}
						},
						error: function(json) {
							//TODO: agregar mensaje fancy de error 
							console.log("error");
						}									
				});
		}
		
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("El campo usuario no puede estar vacio.");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
}

function validaPass(id){
	if($("#"+id).val().length >= 6){
		if($("#"+id).hasClass("error")){
			$("#"+id).removeClass("error");
		}
		if($("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").hide();
		}
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("El campo contrase&ntilde;a no puede estar vacio.");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
}

function validaLicencia(id){
	var idInput = id.split("licencia");
	if($("#"+id).val().length > 0){
		var _boolExiste = false;

		$(".txtLicencia").each(function(){
			if($(this).val() == $("#"+id).val() && $(this).attr('id') != id){
				_boolExiste = true;
			}
		});
		if(_boolExiste === true){
			$('#nomExamen'+idInput[1]).val("");
			if(!$("#"+id).hasClass("error")){
				$("#"+id).addClass("error");
			}
			$("#"+id+"ImgError").html("La licencia se repite en algún registro anterior o posterior.");
			if(!$("#"+id+"ImgError").is(":visible")){
				$("#"+id+"ImgError").show();
			}
		}
		else{
		//consulta ajax licencia disponible
				var urlSitio = $("#urlSitio").val();
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: urlSitio,
						data: {
							"_strLicencia" 		: $("#"+id).val(),
							'_boolValidarLlave' : true
						},
						beforeSend: function() {
							$("#"+id).addClass("textProcesando");
						},
						success: function(json) {
							$("#"+id).removeClass("textProcesando");
							
							if(json['valida'] === false){
								//TODO: mensaje usuario no disponible 
								$('#nomExamen'+idInput[1]).val("");
								if(!$("#"+id).hasClass("error")){
									$("#"+id).addClass("error");
								}
								$("#"+id+"ImgError").html(json['msj']);
								if(!$("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").show();
								}
							}
							else{
								$('#nomExamen'+idInput[1]).val(json['nombre_examen']);
								if($("#"+id).hasClass("error")){
									$("#"+id).removeClass("error");
								}
								if($("#"+id+"ImgError").is(":visible")){
									$("#"+id+"ImgError").hide();
								}
							}
						},
						error: function(json) {
							//TODO: agregar mensaje fancy de error 
							
						}									
				});
		}
	}else{
		$('#nomExamen'+idInput[1]).val("");
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("El campo licencia no puede estar vacio.");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
}
$(document).ready(function(){

$('#urlProcesarCargaMasivaAjax').click(function(){
	
	if($("#urlProcesarCargaMasivaAjax").hasClass("disabled") === false){
		var boolError = false;
		$(".filaUser td input[type=text]").each(function(){			
			if($(this).hasClass("error")){
				boolError = true;
			}
		});
		
		$(".grupoField").each(function(){
			
			if($(this).hasClass("error")){
				
				boolError = true;
			}
		});
		
		if(!boolError){
					var _arrAlum = new Array();
					var iterador = 0;
					$(".filaUser").each(function(){
						
						if($(this).is(":visible")){
							var _arrData = new Array();
							var strRowId = $(this).attr("id").split('row');
							_arrData[0] = $("#nombre"+strRowId[1]).val();
							_arrData[1] = $("#apellido"+strRowId[1]).val();
							_arrData[2] = $("#user"+strRowId[1]).val();
							_arrData[3] = $("#pass"+strRowId[1]).val();
							_arrData[4] = $("#licencia"+strRowId[1]).val();
							_arrAlum.push( _arrData);
							iterador++;
						}
						
					});
					var urlSitio 	 = $("#urlSitio").val();
					var index 	 	 = $("#index").val();
					var _idGrupo  = $("#_idGrupo").val();
					
					
					/*
					//Se agregan campos para abrir examenes a que el usuario responda el examen que quiera.
					var idExamen 	 = $('#idExamen1').val();
					var nomExamen	 = $('#nomExamen1').val();
					*/
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: urlSitio,
							data: {
								"_arrAlum" 		: _arrAlum,
								"_intIdGrupo" 	: _idGrupo,
								'incioCiclo' 	: $('#txtInicioCiclo').val(), 
								'finCiclo' 	 	: $('#txtFinCiclo').val(),
								'descGrupo'		: $('#txtDescripcion').val()
								
							},
							beforeSend: function() {
                                                            
                                                            $('#divContainerTabla').block({ 
							        message: '<i class="fa fa-spinner fa-spin fa-2x"></i><h5 >Procesando...</h5>', 
							    });
								
								//$("#urlProcesarCargaMasivaAjax").addClass("disabled");
							},
							success: function(json) {
								$('#divContainerTabla').unblock();
								if(json["accion"] === true){
									//$.mensaje("crearMensaje", {tipo_mensaje : 'informativo', mensaje : json.msg, ruta: index});
                                                                        $("#modalCargaSuccess").foundation('reveal','open');
                                                                        $("#modalCargaSuccess").bind('reveal:close', function () {
                                                                            window.location = $('#urlGrupos').val();
                                                                          });
								}else{
									//$.mensaje("crearMensaje", {tipo_mensaje : 'alerta', mensaje : json.msg});
									//$("#msjModalCarga").html(json.msg);
									$("#msjModalCarga").html(json.msg);
									$("#modalCarga").foundation('reveal','open');
								}
								
							},
							error: function(json) 
							{
								$('#divContainerTabla').unblock();
								
								//$.mensaje("crearMensaje", {tipo_mensaje : 'error', mensaje: ""});
								$("#msjModalCarga").html("Ocurrió un error al procesar tu solicitud, Por favor intenta más tarde.");
								$("#modalCarga").foundation('reveal','open');
							}									
					});
		}
		else{
			//$.mensaje("crearMensaje", {tipo_mensaje : 'error', mensaje: "Se deben corregir todos los errores para poder enviar el archivo."});
			$("#msjModalCarga").html("Revisa los registros que acabas de ingresar, al parecer contienen errores.");
			$("#modalCarga").foundation('reveal','open');
		}
	}
});

	
$('#urlCancelarCargaMasiva').click(function(){
	window.location = $("#index").val();
});

});
/**
 * Se agrega funcion que reacciona cuando existe un cambio
 * de examen en el combobox implementado en la vista CargaMasiva.phtml.
 * 
 * 
 */
function cambiarExamen( examenes ) 
{
	var id = examenes.selectedIndex;
	var contenido = examenes.options[id].innerHTML;
	for ( var int = 1; int < 100; int++) {
		$('#nomExamen'+int).val(contenido);
	}
	$("#idExamen1").val(examenes.options[id].id);
}



