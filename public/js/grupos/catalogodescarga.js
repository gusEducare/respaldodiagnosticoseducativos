$(document).ready(function(){
   
        $('.solicitacargamasiva').click(function(){
            var arrId = $(this).prop('id').split('-');

                cargaMasivaGrupo(arrId[1]);
		$('#modalUploadCargaMasiva').foundation('reveal', 'open');
                
	//carga masiva de examen
	$('#btnSelectFileExam').click(function(){
		$('#fileUploadFileExam').trigger('click');
	});
	$('#fileUploadFileExam').change(function(){
		$('#txtUploadFileExam').val($('#fileUploadFileExam').val());
	});
	$('#txtUploadFileExam').focus(function(){
		$('#fileUploadFileExam').trigger('click');
	});

	$('#btnUploadFileExam').click(function(){
          
		if($('#fileUploadFileExam').val() != ''){
			$('#frmUploadFileExam').submit();
		}
	});
        
	});
        
       $('.solicitaexamenimpreso').click(function(){
           var arrId = $(this).prop('id').split('-');
             DescargaExamenGrupo(arrId[1]);
           
       });
        
       
});

function DescargaExamenGrupo(id){
	//$('#idGrupoCM').val(id);
       // $('#idGrupoCMExamen').val(id);
       // $('.tab-resultados').show();     
       // $('.content-resultados,.content-usuarios').removeAttr('style');
        var idGrupo = id//$('#idGrupoCM').val();
        
            $.ajax({
                        url: $("#urlListaExamenes").val() , //url hacia donde se manda la peticion
                        dataType: 'json',//formato de respuesta

                        type: 'POST',
                        data: { idGrupo : idGrupo},
                        beforeSend: function(){
                              $("#listado-descargaExamen").html("");
                              $('body').block({
                                        message: '<h5 ><i class="fa fa-spinner fa-spin fa-2x"></i>Procesando...</h5>'
                                    });
                        },


                        success: function(data) {

                                var btnHtml;
                                if(data[0]){
                                btnHtml= '<ul class="inline-list">';
                                    
                                 for(examen in data){
                                     var clave=data[examen].clave;
                                     var arrclave=clave.split("º");
                                    var grado=arrclave["1"].trim().toLowerCase(); 
                                   // var grado2=grado.trim().toLowerCase();
                                    var claves=arrclave["0"];
                                    
                                        var urlcompleta
                                    switch(grado) {
                                            case "prim":
                                                 urlcompleta=claves+$("#urlDescargaExamenPrimaria").val();
                                                break;
                                            case "prep":
                                                urlcompleta=claves+$("#urlDescargaExamenPrepa").val();
                                                break;
                                          
                                            case "sec":
                                                urlcompleta=claves+$("#urlDescargaExamenSecundaria").val();
                                                break;
                                            
                                            case "kind":
                                                urlcompleta=claves+$("#urlDescargaExamenKinder").val();
                                                break;
                                        } 
                                    
                                    
                                     //btnHtml='<ul> <li>Para realizar la descarga de clic en el sigueinte link .........................<a class="itemExa"  href="'+$("#urlDescargaExamen").val()+urlcompleta+' " target="_blank"> <span class="examen-grado">'+data[examen].name+'</span> </a></li>';
                                     btnHtml+='<li><a  href="'+$("#urlDescargaExamen").val()+urlcompleta+' " target="_blank"><span class="linkFormato"><i class="fa fa-download"></i>&nbsp;'+data[examen].name+'</span> </a></li>';
                                     
                                     $('body').unblock();
                                     $('#modaldescargaexamen').foundation('reveal', 'open');
                                     
                                     
                                 }   
                                 btnHtml+='</ul>';
                                 $( "#listado-descargaExamen" ).html( btnHtml );

                                }else{
                                      btnHtml='<b>Este grupo no cuenta con exámenes asignados. </b>';
                                      $( "#listado-descargaExamen" ).append( btnHtml );
                                  
                                }


                        },
                        error: function(data) {
                                 
                        }
                });
	
}
 function cargaMasivaGrupo(id){
	//$('#idGrupoCM').val(id);
        //
       // $('#idGrupoCMExamen').val(id);
       // $('.tab-resultados').show();     
       // $('.content-resultados,.content-usuarios').removeAttr('style');
        var idGrupo = id//$('#idGrupoCM').val();
        console.log("enviar peticion");
            $.ajax({
                        url: $("#urlListaExamenes").val() , //url hacia donde se manda la peticion
                        dataType: 'json',//formato de respuesta

                        type: 'POST',
                        data: { idGrupo : idGrupo},
                        beforeSend: function(){
                              $("#listado-examenes").html("");
                        },


                        success: function(data) {

                                var btnHtml;
                                if(data[0]){

                                    
                                    btnHtml= '<ul class="inline-list">';
                                 for(examen in data){
                                     btnHtml+='<li><a  target="_blank" onclick="obtenerPlantillaResultados('+idGrupo+','+data[examen].id+')"> <span class="linkFormato"><i class="fa fa-download"></i>&nbsp;'+data[examen].name+'</span> </a></li>';

                                 }   
                                 btnHtml += '</ul>';

                                }else{

                                   btnHtml='<h3>Este grupo no tiene un grado asignado. </h3>';
                                  
                                }
                                
                                $( "#listado-examenes" ).html( btnHtml );
                                
                                


                        },
                        error: function(data) {
                                 console.log("no se ejecuto correctamente");
                        }
                });
	$('#modalUploadGrupo').foundation('reveal','open');
}





function obtenerPlantillaResultados(idGrupo,idExamen){
    console.log(idGrupo,idExamen);
    
    window.location=$("#urlCreaPlantilla").val()+"?idGrupo="+idGrupo+"&idExamen="+idExamen;

}

