
$(document).ready(function(){
	//addEvtLister();
	$(document).foundation();
         $('#urlProcesarCargaMasivaAjax').on('click',almacenarCalificaciones);
          $('.botonEliminar').on('click',actualizaTotal);
         $('#urlCancelarCargaMasiva').click(function(){
             ruta = '../../'
	window.location = ruta;
        });

});  


function addEvtLister(){
    $(".txtNombre").blur(function(){
		validaNombre($(this).attr("id"));
	});
	
	$(".txtLicencia").blur(function(){
		validaLicencia($(this).attr("id"));
	});
	
	$(".txtCalificacion").blur(function(){
                
                
                               $(this).val($(this).val().toUpperCase());
     
		validaCalificacion($(this).attr("id"));
	});
    
    
    
}



function validaNombre(id){
	if($("#"+id).val().length > 0){
		if($("#"+id).hasClass("error")){
			$("#"+id).removeClass("error");
		}
		if($("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").hide();
		}
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").html("*");
			$("#"+id+"ImgError").show();
		}
	}
	
}

function validaLicencia(id){
    var idExamen = $("#idExamen").prop("value");
    var _strLicencia = $("#"+id).val();
	if($("#"+id).val().length >= 6){
				$.ajax({
					url: $("#urlExisteLicencia").val() , //url hacia donde se manda la peticion
					dataType: 'json',//formato de respuesta
					cache: false,
					type: 'POST',
					data: { _strLicencia : _strLicencia,_idExamen :idExamen},
					
					
					
					success: function(data) {
						
						if(data[0] == false ){
							$("#"+id+"ImgError").addClass("error");
							$("#"+id+"ImgError").html("*");
							$("#"+id+"ImgError").show();
							//setTimeout('$("#"+id+"ImgError").slideUp("slow");',3000);
						}
                                                else{
							$("#"+id+"ImgError").removeClass("error");
							$("#"+id+"ImgError").hide();
						}
					},
					error: function(data) {
						//TODO: aplicar <<recursividad>> limitada
					}
				});
                                                
	
	}
	else{
		if(!$("#"+id).hasClass("error")){
			$("#"+id).addClass("error");
		}
		$("#"+id+"ImgError").html("*");
		if(!$("#"+id+"ImgError").is(":visible")){
			$("#"+id+"ImgError").show();
		}
	}
    }
    
    function validaCalificacion(id){
        $("#"+id).val().toUpperCase() ;
        var cal=$("#"+id).val().toUpperCase() ;
	if(cal.length <= 1){
            if( cal.match(/[a-dA-D\s]/i) || cal.length==0){
                //console.log("entra al si");
                //console.log(cal);
                if($("#"+id).hasClass("error")){
                    $("#"+id).removeClass("error");
                }
                if($("#"+id+"ImgError").is(":visible")){
                    $("#"+id+"ImgError").hide();
                }
            }else{
                //console.log("entra al no");
                //console.log(cal);
                if(!$("#"+id).hasClass("error")){
                $("#"+id).addClass("error");
            }
            $("#"+id+"ImgError").html("**");
            if(!$("#"+id+"ImgError").is(":visible")){
                $("#"+id+"ImgError").show();
            }
                
            
            }
        }else{
            if(!$("#"+id).hasClass("error")){
                $("#"+id).addClass("error");
            }
            $("#"+id+"ImgError").html("*");
            if(!$("#"+id+"ImgError").is(":visible")){
                $("#"+id+"ImgError").show();
            }
	}
        
    }
    
    
    function almacenarCalificaciones (){
        
        
        if($("#urlProcesarCargaMasivaAjax").hasClass("disabled") === false){
           
            var boolError = false;
		$(".filaUser td input[type=text]").each(function(){	
                    
			if($(this).hasClass("error")){
                          boolError = true;
			}
		});
                

            if(!boolError){
               var idExamen= $('#idExamen').val();
               
               var totalRegistros = 0;
               var registrosProcesados = 0;
               $(".filaUser:visible").each(function(){
                   totalRegistros ++;
               });
               $('#msjModalConfirma').html(
                    '<div class="row">'+
                         '<div class="small-12 large-12 columns">'+
                             '<div class="row">'+
                                 '<div class="small-10 large-centered columns">'+
                                     '<div class="progress radius round " >'+
                                         '<span id="progressMeter" class="meter" style="width: 0%;"></span>'+
                                     '</div>'+
                                 '</div>'+
                             '</div>'+
                             '<div class="row">'+
                                 '<div class="small-10 large-centered columns" style="text-align:center;">'+
                                     '<h5 style="" ><i class="fa fa-spinner fa-spin fa-2x"></i>Procesando...<span id="numPorcentaje">0%</span></h5>'+
                                  '</div>'+
                             '</div>'+
                         '</div>'+
                     '</div>');
               $("#modalConfirma").foundation('reveal','open');
               
               $(".filaUser:visible").each(function(){
                  var id = $(this).prop('id');
                  id= id.split('row');
                  //id[0] 
                   var arrRespuestas=new Array();
                    var a=0;
                    var strLlave= $('#licencia'+id[1]).val();
                     $('.calif'+id[1]).each(function(){
                        arrRespuestas[a]= formateaObjetoRespuesta($(this).val());
                        a++;
                     });
                    
                    
                    $.ajax({
					url: $("#urlCargamasiva").val() , //url hacia donde se manda la peticion
					dataType: 'json',//formato de respuesta
					cache: false,
					type: 'POST',
                                        
					data: { strLlave : strLlave,
                                                idExamen :idExamen,
                                                objRespuestas :arrRespuestas
                                                //contador : contador    
                                                },
					beforeSend: function(){
                                            
                                            
                                        },
					
					
					success: function(data) {
						
                                                
                                                if(data){
                                                    registrosProcesados++;
                                                    //$('#row'+i).slideUp("slow");
                                                    limpiaActualizaTotal($('#row'+id[1]));
                                                    $('#progressMeter').css('width',(registrosProcesados / totalRegistros * 100 )+'%');
                                                    $("#numPorcentaje").html((registrosProcesados / totalRegistros * 100 )+ '%');
                                                    if(registrosProcesados == totalRegistros || registrosProcesados > totalRegistros){
                                                        $("#msjModalConfirma").html("<h2 id='modalTitle'>Los datos fueron guardados correctamente</h2>" + 
                                                        "<p class='lead'>Da clic en el siguiente enlace para regresar al listado de grupos" +
                                                        "<br><br><a href='"+$('#urlGrupos').val()+"'> Regresar</a></p>"
                                                        );
                                                    }
                                                }else{
                                                    
                                                  
                                                }
                                                
                                                
                                             
                                            
					},
					error: function(data) {
						//TODO: aplicar <<recursividad>> limitada
					}
				});
               });
               
               
               
               
                                                
               
                
            }else{
                //$.mensaje("crearMensaje", {tipo_mensaje : 'error', mensaje: "Se deben corregir todos los errores para poder enviar el archivo."});
			$("#msjModalCarga").html("Revisa los registros que acabas de ingresar, al parecer contienen errores.");
			$("#modalCarga").foundation('reveal','open');
                
            }
                
            
            
        }
        
    }
    
   function actualizaTotal(){
     $(this).parent().parent().slideUp('slow',function(){
         $('#totalRegistro').html($(".filaUser:visible").length);
     });
     $(this).parent().parent().html('');
       
   }
    function limpiaActualizaTotal($selector){
     $selector.slideUp('slow',function(){
         $('#totalRegistro').html($(".filaUser:visible").length);
     });
     $selector.html('');
       
   }
   
    
   function formateaObjetoRespuesta(letra){
       letra=letra.toUpperCase();
       var obj=new Object();
       obj["resp"]=new Object();
       
       for(var i=0;i<4;i++){
           
        obj["resp"][i]= new Object();
        obj["resp"][i].id=i+1;
        obj["resp"][i].selected=false;
      
               
       }
        switch(letra){
           
           case "A":
                obj["resp"][0].selected=true;
               
               break;
            
           case "B":
                obj["resp"][1].selected=true;
               
               break;
              
           case "C":
                obj["resp"][2].selected=true;
               
               break;
            
           case "D":
                obj["resp"][3].selected=true;
               
               break;
               
            }   
       return obj;
       
   }
   
