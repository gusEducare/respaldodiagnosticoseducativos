var current_width = 0;
$(document).ready(function(){
	$(document).foundation();
	
	//carga masiva de examen
	$('#btnSelectFileExam').click(function(){
		$('#fileUploadFileExam').trigger('click');
	});
	$('#fileUploadFileExam').change(function(){
		$('#txtUploadFileExam').val($('#fileUploadFileExam').val());
	});
	$('#txtUploadFileExam').focus(function(){
		$('#fileUploadFileExam').trigger('click');
	});

	$('#btnUploadFileExam').click(function(){
		if($('#fileUploadFileExam').val() != ''){
			$('#frmUploadFileExam').submit();
		}
	});
	
	$('#btnSelectFile').click(function(){
		$('#fileUploadFile').trigger('click');
	});
	
	$('#fileUploadFile').change(function(){
		$('#txtUploadFile').val($('#fileUploadFile').val());
	});
	
	$('#txtUploadFile').focus(function(){
		$('#fileUploadFile').trigger('click');
	});
	
	$('#btnUploadFile').click(function(){
		if($('#fileUploadFile').val() != ''){
			$('#frmUploadFile').submit();
		}

	});
        
        
        //carga masiva Llaves
        $('#btnSelectFileLicencias').click(function(){
		$('#fileUploadFileLicencias').trigger('click');
	});
	
	$('#fileUploadFileLicencias').change(function(){
		$('#txtUploadFileLicencias').val($('#fileUploadFileLicencias').val());
	});
	
	$('#txtUploadFileLicencias').focus(function(){
		$('#fileUploadFileLicencias').trigger('click');
	});
	
	$('#btnUploadFileLicencias').click(function(){
		if($('#fileUploadFileLicencias').val() != ''){
			$('#frmUploadFileLicencias').submit();
		}
	});
        

        $('#btnDeleteGrupoCancel').click(function(){
            $('a.close-reveal-modal').trigger('click');
        });
	
	
	//handler para abrir modal crear grupo con carga masiva
	$('#uploadGroup').click(function(){
		$('#idGrupoCM').val('-1');
                $('.content-resultados,.tab-resultados').hide(); 
                $('.content-usuarios').show();                
		$('#modalUploadGrupo').foundation('reveal','open');
	});
	
	
	//limpiar el value del input file  de carga masiva al iniciar
	$(document).on('open.fndtn.reveal', '[data-reveal]', function () {
		$('#txtUploadFile').val('');
		$('#fileUploadFile').val('');
	});
	
	
	var json = JSON.parse($('#dataGrupos').val());
	//$.table();
	$.table("createContainers", json);
	
	
	$('#buttonCancelPass').click(function(){
		$('#modalPass').foundation('reveal', 'close');
	});
        
        $('.buttonDeleteElement').click(function(e){
           e.preventDefault();
           var strLink = $(this).prop('href');
           $('#btnDeleteGrupoOk').prop('href', strLink);
           $('#modalAskDeleteGrupo').foundation('reveal', 'open');
        });
        
        $('#btnDeleteGrupoOk').click(function(){
           $('#modalAskDeleteGrupo').block({
                    message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...',
            }); 
        });
	
	$('#buttonDoPass').click(function(){
		var boolProceed = true;
		//enviar peticion ajax
		
		$('#errorPassLength').slideUp();
		$('#errorPassConfirm').slideUp();
		 
		
		if($('#newPass').val().length < 6){
			boolProceed = false;
			$('#errorPassLength').html('La contraseña debe tener al menos 6 caracteres.');
			$('#errorPassLength').slideDown();
		}
		
		if($('#newPass').val() !=  $('#confirmPass').val()){
			boolProceed = false;
			$('#errorPassConfirm').html('La contraseña no coincide.');
			$('#errorPassConfirm').slideDown();
		}
		
		if(boolProceed === true){
			//lanzar peticion AJAX
			
			$.ajax({
				type:'POST',
				dataType: 'json',
				url: $('#urlChangePass').val(),
				data:{
					'idGrupo' : $("#idGrupoPass").val(),
					'idAlumno' : $("#idAlumnoPass").val(),
					'newPass': $("#newPass").val() 
					
				},
				beforeSend: function(){
					$('#frmChangePass').block({
						message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...',
					}); 
				},
				success: function(json){
					$('#frmChangePass').unblock();
					$("#msjModalResult").html(json['msj']);
					$("#modalResultPass").foundation('reveal', 'open');
					
					
				},
				error: function(json){
					$('#frmChangePass').unblock();
					$("#msjModalResult").html('<i class="fa fa-times"></i>&nbsp;Ocurrió un error al procesar la solicitud, por favor intenta más tarde.');
					$("#modalResultPass").foundation('reveal', 'open');
				}
			});	
		}
	});
	
	
	
	$('#btnDeleteOk').click(function(){
		$.ajax({
			type:'POST',
			dataType: 'json',
			url: $('#urlDeleteUser').val(),
			data:{
				'idGrupo' : $('#idUserGrupoDelete').val(),
				'idAlumno' :$('#idUserDelete').val()
			},
			beforeSend: function(){
				$('#modalPass').foundation('reveal', 'close');
				$('#subElementContainer-'+$('#idUserDelete').val()).block({
					message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...',
				}); 
			},
			success: function(json){
				
				$('#subElementContainer-'+$('#idUserDelete').val()).unblock();
				if(json['result'] === true){
					
					$('#subElementContainer-'+$('#idUserDelete').val()).height($('#subElementContainer-'+$('#idUserDelete').val()).height());
					$('#subElementContainer-'+$('#idUserDelete').val()).animate({
						 width: "toggle",
						 opacity: "toggle"
					 },500 ,function(){
						 $('#subElementContainer-'+$('#idUserDelete').val()+' > div').html(json['msj']);
						 $('#subElementContainer-'+$('#idUserDelete').val()).animate({
							 width: "toggle",
							 opacity: "toggle"
						 },500, function(){
							 $('#subElementContainer-'+$('#idUserDelete').val()).css('height','auto');
							 $(document).foundation();
							 $('#subElementContainer-'+$('#idUserDelete').val()).delay(3000).slideUp('slow');
							 
						 });
						 
					 });
					
				}
				
			},
			error: function(json){
				$('#subElementContainer-'+$('#idUserDelete').val()).unblock();
			}
		});
	});
	
	$('#btnDeleteCancel').click(function(){
		$('#modalPass').foundation('reveal', 'close');
	});
	
	
	$('#buttonRestore').click(function(){
		$('#modalRestoreGrupo').foundation('reveal','open');
		$.ajax({
			type:'POST',
			dataType: 'json',
			url: $('#urlRestoreGrupo').val(),
			data:{
				'consulta' : 1
			},
			beforeSend: function(){
				$("#containerDeletedGroups").html('<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...');
			},
			success: function(json){
				$("#containerDeletedGroups").html(json['html']);
			},
			error: function(json){
				
			}
		});
	});
	// evento on blur aplicado a la grupos!!!!!!!!!
	$('.idGrupo').on('blur keyup',function (e){
            if (e.type == 'blur' || e.keyCode == '13') {
            var nombreGrupo = this.value;
            var idGrupo = this.id;
            idGrupo = idGrupo.split('-');
            idGrupo = idGrupo[1];
            
            $.ajax({
                type:'POST',
		dataType: 'json',
		url: $('#actualizaNombreGrupo').val(),
                data:{
                    'idGrupo': idGrupo,
                    'nombre': nombreGrupo 
                },
                beforeSend: function(){
                            
                },
                success: function(json){
                   
                },
                error: function(json){
                    console.log('conexion error');
		}
            })
        }
        });
        
        
	/*$('.idUsuario').on('blur keyup',function (e){
            console.log("Si entro aqui");
            
            if (e.type == 'blur' || e.keyCode == '13') {
            var nombreUsuario = this.value;
            var idUsuario = this.id;
            idUsuario = idUsuario.split('-');
            idUsuario = idUsuario[1];
            
            
            console.log(idUsuario);
    
            
            
            $.ajax({
                type:'POST',
		dataType: 'json',
		url: $('#actualizaNombreGrupoUsuario').val(),
                data:{
                    'idUsuario': idUsuario,
                    'nombre': nombreUsuario 
                },
                beforeSend: function(){
                            
                },
                success: function(json){
                   
                },
                error: function(json){
                    console.log('conexion error');
		}
            })
           
        }
        });*/
        
        
    });
	
function restoreGrupo(idGrupo){
	$.ajax({
		type:'POST',
		dataType: 'json',
		url: $('#urlRestoreGrupo').val(),
		data:{
			'idGrupo' : idGrupo
		},
		beforeSend: function(){
			$("#elementGrupo-"+idGrupo).block({
				message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...',
			}); 
		},
		success: function(json){
			console.log(json['msj']);
			
			$("#elementGrupo-"+idGrupo).unblock();
			
			if(json['result'] == true){
				$("#elementGrupo-"+idGrupo).animate({
					 width: "toggle",
					 opacity: "toggle"
				 },500 ,function(){
					 $("#elementGrupo-"+idGrupo).html(json['msj']);
					 $("#elementGrupo-"+idGrupo).animate({
						 width: "toggle",
						 opacity: "toggle"
					 },500 ,function(){
						 $(document).foundation();
						 $("#elementGrupo-"+idGrupo).delay(3000).slideUp('slow');
						 
					 });
					 
					 
				 });
				
				reloadTables();
			}
			
			
		},
		error: function(json){
			$("#elementGrupo-"+idGrupo).unblock();
		}
	});
}
function reloadTables(){
	$.ajax({
		type:'POST',
		dataType: 'json',
		url: $('#urlReloadTables').val(),
		data:{
			json : 1
		},
		beforeSend: function(){
			$("#grupos").block({
				message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Actualizando...'
			}); 
		},
		success: function(json){
			$("#grupos").unblock();
			if(json.length != false ){
				$.table("createContainers", json);
			}
			else{
				$("#grupos").html("<p class='lead'>No existen grupos.</p>");
			}
			
		},
		error: function(json){
			$("#grupos").unblock();
		}
	});
	
	
}

$( window ).load(function() {
	$(".btnOpciones").hover(function(){
		$(".btnOpciones").addClass("fa-spin");
	},
	function(){
		$(".btnOpciones").removeClass("fa-spin");
	}
);
	});
function modificarPass(idGrupo,idAlumno){
	$("#legendChangePass").html('Actualizar contraseña: '+$("#label-nombre-"+idAlumno).html()+' '+$("#label-apellido-"+idAlumno).html());
	$("#idGrupoPass").val(idGrupo);
	$("#idAlumnoPass").val(idAlumno);
	$('#newPass').val("");
	$('#confirmPass').val("");
	$('#errorPassLength').hide();
	$('#errorPassConfirm').hide();
	$('#modalPass').foundation('reveal', 'open');
}




function deleteUser(idGrupo,idAlumno){
	//
	//
	$('#idUserDelete').val(idAlumno);
	$('#idUserGrupoDelete').val(idGrupo);
	$('#nameDeleteUser').html($("#label-nombre-"+idAlumno).html()+' '+$("#label-apellido-"+idAlumno).html());
	$('#modalAskDelete').foundation('reveal','open');
	
}
function obtenerPlantillaResultados(idGrupo,idExamen){
    console.log(idGrupo,idExamen);
    window.location=$("#urlCreaPlantilla").val()+"?idGrupo="+idGrupo+"&idExamen="+idExamen;

}


function cargaMasivaGrupo(id){
	$('#idGrupoCM').val(id);
        $('#idGrupoCMExamen').val(id);
        $('.tab-resultados').show();     
        $('.content-resultados,.content-usuarios').removeAttr('style');
        var idGrupo = $('#idGrupoCM').val();
        console.log("enviar peticion");
            $.ajax({
                        url: $("#urlListaExamenes").val() , //url hacia donde se manda la peticion
                        dataType: 'json',//formato de respuesta

                        type: 'POST',
                        data: { idGrupo : idGrupo},
                        beforeSend: function(){
                              $("#listado-examenes").html("");
                        },


                        success: function(data) {

                                var btnHtml;
                                if(data){

                                    console.log(" se ejecuto correctamente");
                                 for(examen in data){
                                     btnHtml='<a class="item" target="_blank" onclick="obtenerPlantillaResultados('+idGrupo+','+data[examen].id+')"> <span class="examen-grado">'+data[examen].clave+'</span> </a>';

                                        $( "#listado-examenes" ).append( btnHtml );
                                 }   

                                }else{

                                  console.log("no se ejecuto correctamente");
                                }


                        },
                        error: function(data) {
                                //TODO: aplicar <<recursividad>> limitada
                        }
                });
	$('#modalUploadGrupo').foundation('reveal','open');
}

function desbloqueaExamenes(idGrupo){    
        
        $.ajax({
            type:'POST',
            dataType: 'json',
            url: $('#urlObtenerExamenesInterrumpidos').val(),
            data:{
                'idGrupo' : idGrupo
        },
        beforeSend: function(){
//            $('#frmChangePass').block({
//                message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...'
//            }); 
        },
        success: function(json){
//            $('#frmChangePass').unblock();
//            $("#msjModalResult").html(json['msj']);
//            $("#modalResultPass").foundation('reveal', 'open');
//              console.log('Ya procesó con id: ' + json);
            if(json.length){
                actualizaTabla(json, idGrupo);
            }else{
                $('#modalUnblockExams h2').html('No hay evaluaciones interrumpidas para este grupo');
                $('.contenido_interrumpidos').html('');
            }            
        },
        error: function(json){
//            $('#frmChangePass').unblock();
//            $("#msjModalResult").html('<i class="fa fa-times"></i>&nbsp;Ocurrió un error al procesar la solicitud, por favor intenta más tarde.');
//            $("#modalResultPass").foundation('reveal', 'open');
        }
    });	
    $('#idGrupoDE').val(idGrupo);        
    $('#modalUnblockExams').foundation('reveal','open');
}

function muestraCalificaciones(idGrupo){  
        
        $.ajax({
            type:'POST',
            dataType: 'json',
            url: $('#urlObtenerCalificaciones').val(),
            data:{
                'idGrupo' : idGrupo
        },
        beforeSend: function(){
//            $('#frmChangePass').block({
//                message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...'
//            }); 
        },
        success: function(json){
//            $('#frmChangePass').unblock();
//            $("#msjModalResult").html(json['msj']);
//            $("#modalResultPass").foundation('reveal', 'open');
//              console.log('Ya procesó con id: ' + json);
            actualizaTablaCalificaciones(json, idGrupo);
        },
        error: function(json){
//            $('#frmChangePass').unblock();
//            $("#msjModalResult").html('<i class="fa fa-times"></i>&nbsp;Ocurrió un error al procesar la solicitud, por favor intenta más tarde.');
//            $("#modalResultPass").foundation('reveal', 'open');
        }
    });	
    $('#idGrupoDE').val(idGrupo);        
    $('#modalMostrarCalif').foundation('reveal','open');
}


function desbloquearExamen(idGrupo, idExamenUsuario){
	$.ajax({
		type:'POST',
		dataType: 'json',
		url: $('#urlDesbloquearExamen').val(),
		data:{
			'idExamenUsuario'   : idExamenUsuario,
                        'idGrupo'           : idGrupo
		},
		beforeSend: function(){
//			$("#grupos").block({
//				message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Actualizando...'
//			}); 
		},
		success: function(json){
//			$("#grupos").unblock();
//			if(json.length != false ){
//				$.table("createContainers", json);
//			}
//			else{
//				$("#grupos").html("<p class='lead'>No existen grupos.</p>");
//			}
			actualizaTabla(json, idGrupo);
		},
		error: function(json){
			$("#grupos").unblock();
		}
	});
	
	
}

function resetearExamen(idGrupo, idExamenUsuario){
	$.ajax({
		type:'POST',
		dataType: 'json',
		url: $('#urlResetearExamen').val(),
		data:{
			'idExamenUsuario'   : idExamenUsuario,
                        'idGrupo'           : idGrupo
		},
		beforeSend: function(){
//			$("#grupos").block({
//				message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Actualizando...'
//			}); 
		},
		success: function(json){
//			$("#grupos").unblock();
//			if(json.length != false ){
//				$.table("createContainers", json);
//			}
//			else{
//				$("#grupos").html("<p class='lead'>No existen grupos.</p>");
//			}
			actualizaTabla(json, idGrupo);
		},
		error: function(json){
			$("#grupos").unblock();
		}
	});
	
	
}

function actualizaTabla(json, idGrupo){    
    var _strHtml = '';
    var intExamenes = json.length;            
    for(var i=0; i<intExamenes; i++){
        _strHtml += '<div class="hov">' +
                    '<div class="large-3 medium-4 columns usuario" style="height:40px;">'+json[i]['t01apellidos']+', '+json[i]['t01nombre']+'</div>'+
                    '<div class="large-3 medium-4 columns examne" style="height:40px;">'+json[i]['t12nombre']+'</div>'+
                    '<div class="large-2 medium-2 columns licencia" style="height:40px;">'+json[i]['t03licencia']+'</div>'+
                    '<div class="large-2 medium-1 columns accion" style="height:40px;">\n\
                        <a href="#" class="fa fa-unlock button tiny" onclick="desbloquearExamen('+idGrupo+', '+json[i]['t04id_examen_usuario']+')"></a>\n\
                    </div>'+
                    '<div class="large-2 medium-1 columns accion" style="height:40px;">\n\
                        <a href="#" class="button tiny" onclick="resetearExamen('+idGrupo+', '+json[i]['t04id_examen_usuario']+')"><i class="fa fa-sync-alt">Resetear</i></a>\n\
                    </div>'+
                    '</div>';
    }
    $('.hov').remove();
    $('.table_bg').append(_strHtml);
}

function actualizaTablaCalificaciones(json, idGrupo){   
    var _strHtml = '';
    var intExamenes = json.length;            
    var urlGeneraPdfAjax = $('#urlGeneraPdfAjax').val();
    for(var i=0; i<intExamenes; i++){
        /**
         
         
    `t03`.`t03licencia` AS `t03licencia`,
    `t04`.`t04estatus` AS `t04estatus`,
    `t12`.`t12id_examen` AS `t12id_examen`
         
         */
        var _strEstatus = json[i]['t04estatus'];
        var _strRutaReconocimiento = '';
        if( _strEstatus === 'APROBADO' ){
            var _strDocumento = json[i]['t12id_examen'] == '6' ? 'Certificación' : 'Reconocimiento';
            _strRutaReconocimiento = ' - <a href="'+urlGeneraPdfAjax+'?_strNombreCompleto='+
                    json[i]['t01nombre']+' '+json[i]['t01apellidos']+'&_intGrado='+
                    json[i]['t12id_examen']+'&_strLlave='+json[i]['t03licencia']+'"' +
                    ' title="Ver '+_strDocumento+'"><i class="fa fa-file-text-o"></i></a>';
        }
        _strHtml += '<div class="hov">' +
                    '   <div class="large-5 medium-5 columns usuario" style="height:40px;">'+json[i]['t01apellidos']+', '+json[i]['t01nombre']+'</div>'+
                    '   <div class="large-5 medium-5 columns examne" style="height:40px;">'+json[i]['t12nombre']+'</div>'+
                    '   <div class="large-2 medium-2 columns licencia" style="height:40px;">'+json[i]['t04calificacion']+
                           _strRutaReconocimiento +
                    '   </div>'+
                    '</div>';
    }
    $('.hov').remove();
    $('.table_bg_calif').append(_strHtml);
}


function showReportesOptions(intIdGrupo){
    
    $("#modalReportes").foundation('reveal','open');
    $.ajax({
        type:'POST',
        dataType: 'json',
        url: $('#urlListReportes').val(),
        data:{
            'intIdGrupo' : intIdGrupo
        },
        beforeSend: function(){
            $('#divTableReportes').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...');
        },
        success: function(json){
            $('#divTableReportes').html(json['html']);
        },
        error: function(json){
             $('#divTableReportes').html('Ocurrió un error al procesar tu solicitud, por favor intenta más tarde.');
        }
    });
}



//////


function addLicenciasGrupo(idGrupo){
    
    $('#idGrupoCMLicencias').val(idGrupo);
    var strNombreGrupo = $('#table-container-'+idGrupo).find('span.tituloGrupo').html();
    var tempStrNombreGrupo = strNombreGrupo.split("</i>");
    strNombreGrupo = tempStrNombreGrupo[1];
    strNombreGrupo.replace("&nbsp;", " "); 
    console.log(strNombreGrupo.trim());
    $('#spanNombreGrupo').html(strNombreGrupo.trim());
    $("#modalCargaLlaves").foundation('reveal','open');
}


$(document).ready(function(){
    $('#btnPlatillaLicencias').click(function(){
        var idGrupo = $('#idGrupoCMLicencias').val();
        console.log(idGrupo);
        window.open($("#urlPlantillaLicencias").val()+"?idGrupo="+idGrupo);
    });
});