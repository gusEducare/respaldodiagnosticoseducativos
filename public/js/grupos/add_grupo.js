$(document).ready(function(){
	$('#btnAgregarGrupo').click(function( event ) {
		event.preventDefault();
		var boolProceder = true;
		//console.log($("#txtInicioCiclo").val().length);
		if($("#txtInicioCiclo").val().length < 1){
			$("#errorInicioCiclo").html("Este campo no puede estar vacio.");
			$("#errorInicioCiclo").slideDown();
			$("#lblInicioCiclo").addClass("error");
			
			boolProceder = false;
		}
		else if(!$("#txtInicioCiclo").val().match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/)){
			boolProceder = false;
			$("#errorInicioCiclo").html("La fecha debe tener un formato dd/mm/aaaa  p. ej. 26/02/2015");
			$("#errorInicioCiclo").slideDown();
			$("#lblInicioCiclo").addClass("error");
		}
		
		if($("#txtFinCiclo").val().length < 1){
			boolProceder = false;
			$("#errorFinCiclo").html("Este campo no puede estar vacio.");
			$("#errorFinCiclo").slideDown();
			$("#lblFinCiclo").addClass("error");
		}
		else if(!$("#txtFinCiclo").val().match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/)){
			$("#errorFinCiclo").html("La fecha debe tener un formato dd/mm/aaaa  p. ej. 26/02/2015");
			$("#errorFinCiclo").slideDown();
			$("#lblFinCiclo").addClass("error");
			boolProceder = false;
		}
		
		if($("#txtDescripcion").val().length < 1){
			boolProceder = false;
			$("#errorDescripcion").html("Este campo no puede estar vacio.");
			$("#errorDescripcion").slideDown();
			$("#lblDescripcion").addClass("error");
		}
		
		if(boolProceder === true){
                        $('#frmNewGrupo').block({ 
                            message: '<i class="fa fa-spinner fa-spin fa-2x"></i><h5 >Procesando...</h5>'
                        });
			$('#frmNewGrupo').submit();
		}
	});
	
	$("#txtInicioCiclo" ).focus(function(){
		$("#errorInicioCiclo").slideUp();
		$("#lblInicioCiclo").removeClass("error");
	});
	$("#txtFinCiclo" ).focus(function(){
		$("#errorFinCiclo").slideUp();
		$("#lblFinCiclo").removeClass("error");
	});
	$("#txtDescripcion" ).focus(function(){
		$("#errorDescripcion").slideUp();
		$("#lblDescripcion").removeClass("error");
	});
	
	$(".fecha").fdatepicker({
		language: 'es',
		format: 'dd/mm/yyyy', 
	});
        
        if($("#txtInicioCiclo" ).val() != ''){
            $("#txtInicioCiclo" ).focus();
            $("#txtInicioCiclo" ).fdatepicker('hide');
            $("#txtFinCiclo" ).focus();
            $("#txtFinCiclo" ).fdatepicker('hide');
        }

});