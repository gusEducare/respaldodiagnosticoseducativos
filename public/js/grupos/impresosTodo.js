$(document).ready(function(){
   
       $(".bloques").hide();
       $(".grados").hide();
       $("#tablaGrados").hide();
       
       $("#examen").click(function (){
            $(".bloques").show();
            $("#examen").hide();
       });


       $('.bloque').click(function(){
           var bloque = $(this).attr("id");
            $(".bloques").hide();
            $("#tablaGrados").show();
         $('#tablaGrados tbody').append('<tr>' +
                        '<td width="25%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-23">1° Primaria</a></td>' +
                        '<td width="25%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-26">4° Primaria</a></td>' +
                        '<td width="50%" style="text-align: center;"><a <a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-29">1° Secundaria</a></td>'+
                        '</tr>' +
                        '<tr>' +
                        '<td width="25%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-24">2° Primaria</a></td>' +
                        '<td width="25%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-27">5° Primaria</a></td>' +
                        '<td width="50%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-30">2° Secundaria</a></td>'+
                        '</tr>' +
                        '<tr>' +
                        '<td width="25%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-25">3° Primaria</a></td>' +
                        '<td width="25%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-28">6° Primaria</a></td>' +
                        '<td width="50%" style="text-align: center;"><a href="'+$("#urlGetMaterias").val()+'/'+bloque+'-31">3° Secundaria</a></td>'+
                        '</tr>'
                   );
        });
});
