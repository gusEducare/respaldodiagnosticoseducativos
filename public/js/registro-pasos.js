/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var duration = 2000;
$(document).ready(function(){
    $(document).foundation();
var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);
    $('.back-to-alumno').click(function() {
        $('html, body').animate({
            scrollTop:$("#div-alumno").offset().top
        }, duration);
    });
    $('.back-to-profesor').click(function() {
        $('html, body').animate({
            scrollTop:$("#div-profesor").offset().top
        }, duration);
    });
 
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('.back-to-top').fadeIn();
        } else {
            $('.back-to-top').fadeOut();
        }
    });

    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });

});
               
      
