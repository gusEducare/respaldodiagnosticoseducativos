$(document).ready(function () {
    //actualiza el indice de secciones para que las preguntas sean movidas a otra seccion
    $('.moveToSeccion').on('click', function () {
        console.log('actualiza index de secciones');
        var contentSeccion = $(this).parent().parent().parent().parent().parent().attr('id').split('_');
        var idSec      = contentSeccion[1];
        var select = $(this).attr('id').split('-');
        $('#dropMenuToolPreg'+select[1]).children().find('#selEviaSeccion').each(function(){
            $(this).children().remove();
            var optionDefault = '<option value="0" id="default">Mover a sección</option>';
            var miSelect =    $(this);
            $(miSelect).append(optionDefault);
            $('.contenedor_preguntas').each(function(){
                var ids = $(this).attr('id').split('_');
                var valor = $(this).find('input').eq(0).val();
                var options = '<option value='+ids[1]+' >'+valor+'</option>';
                if(idSec === ids[1]){}else{
                    $(miSelect).append(options);
                }
            });
        });
    });
    $.validator.addMethod('selectComboBox',function(value,element,arg){
        return arg !== value;
    });

    $('#addSection').on('click', addsection);

    /**
     * 
     * crea un clon de la seccion para añadir mas preguntas dentro del contedor.
     */
    function addsection() {
        if($('#seccion_nueva0').length < 1){
            var idConsecutivo = 0;
        }else{
            var arrText = $('.contenedor_preguntas:last').attr('id').split('_'); 
            var idConsecutivo = arrText[1];  
            idConsecutivo = parseInt(idConsecutivo.substr(5))+1;
        }
        if($('.contenedor_preguntas').length === 0){
            console.log('la caja esta vacia y no puedo clonar');
        }else{
            //$('.contenedor_preguntas:first').clone().appendTo('#secciones');
            $('.contenedor_preguntas:first').clone().insertBefore('#erroPonderacion');
            var idContenedor = 'seccion_nueva' + idConsecutivo;
            //configura div de titulo
            $('.contenedor_preguntas').last().attr('id', idContenedor);
            $('#' + idContenedor).children().eq(0).attr('id', 'titulo_seccion_' + idConsecutivo);
            $('#' + idContenedor).children().eq(0).attr('name', 'seccion_nueva' + idConsecutivo);
            $('#' + idContenedor).children().eq(0).children().children().eq(0).attr('id', 'seccion_nueva' + idConsecutivo);
            $('#' + idContenedor).children().eq(0).children().children().eq(0).val('Nueva Sección');
            $('#' + idContenedor).children().eq(0).children().children().eq(1).children().attr('id','seccion_nueva' + idConsecutivo);
            $('#' + idContenedor).children().eq(0).children().children().eq(1).children().attr('value','0');
            $('#' + idContenedor).children().eq(0).children().children().eq(2).children().children().attr('id','btnOpcionesNueva-' + idConsecutivo);
            $('#' + idContenedor).children().eq(0).children().children().eq(2).children().children().attr('data-dropdown','dropMenuToolNueva-' + idConsecutivo);
            //configura dropdown menu de configuración
            $('#' + idContenedor).children().eq(1).attr('id', 'dropMenuToolNueva-'+ idConsecutivo);
            $('#' + idContenedor).children().eq(1).children().eq(3).children().attr('onClick','eliminaSeccion('+idConsecutivo+')');
            $('#' + idContenedor).children().eq(1).children().eq(6).children().children().attr('id','evalSwitchNueva' + idConsecutivo);
            $('#' + idContenedor).children().eq(1).children().eq(6).children().children().attr('for','evalSwitchNueva' + idConsecutivo);
            //contenedor de preguntas limpio
            //$('#' + idContenedor).children().eq(2).attr('id','contenedor_seccion_nueva_' + idConsecutivo);
            $('#' + idContenedor).children().eq(2).attr('id','contenedor_seccion_nueva' + idConsecutivo);
            $('#' + idContenedor).children().eq(2).css('min-height','60px');
            $('#' + idContenedor).children().eq(2).html('');
            drag_pregunta_seccion();
        }
    }
    
    $('#grdpregexam').on('click', ligarpreguntas);

    if ($('.pregunta-seleccionada').length > 1) {
        $('.pregunta-seleccionada').each(function () {
            var miId = $(this).attr('id');
            if (miId === undefined) {
                $(this).addClass('hide');
            }
        });
    }
    
    /**
     * opciones de busqueda avanzada con chosen
     */
    $('.busquedaAvanzada').chosen().change(function(){
        var opcion = $('#busquedaAvanzada').val();       
        console.log('opcion '+opcion);
        if(opcion !== null){
            if (opcion.indexOf('1') > -1) {
                console.log('opcion general');
                //$('#listadopreguntas').addClass('sortable');
                //$('#fieldTextSearch').removeAttr('disabled');
                $.ajax({
                    type: 'POST',
                    url: $('#mostrarpreguntas').val(),
                    data: {
                        'idExamen': $('#idExamen').val()
                    },
                    dataType: 'json',
                    error: function () {
                        console.log('error peticion ajax');
                    },
                    beforeSend:function(){
                        $('#contenedorTabla').html('');
                    },
                    success: function (json) {
                        loadDatatableSortable(json);
                    }
                });
            }
            if (opcion.indexOf('2') > -1) {
                $.ajax({
                    type: 'POST',
                    url: $('#listaCategorias').val(),
                    dataType: 'json',
                    error: function () {
                        console.log('error en la opcion categoria preguntas');
                    },
                    beforeSend: function () {
                        $('.listaCategoria_chosen').show();
                    },
                    success: function (json) {
                        var html = '';
                        for (var x in json) {
                            if (x !== 'length') {
                                html += '<option value=' + json[x]['c08id_categoria'] + '>' + x + '</option>';
                            }
                        }
                        $('#listaCategoria').html(html);
                        $("#listaCategoria").chosen().trigger("chosen:updated");
                    }
                });
            } else {
                $('.listaCategoria_chosen').hide();
            }
            
            if(opcion.indexOf('3') > -1){
                $.ajax({
                    type: 'POST',
                    url: $('#muestratiposajax').val(),
                    dataType: 'json',
                    error: function () {
                        console.log('error peticion ajax');
                    },
                    beforeSend: function () {
                        $('.listaTipos_chosen').show();
                    },
                    success: function (json) {
                        var html = '';
                        for (var x in json) {
                            if (x !== 'length') {
                                html += '<option value=' + x + '>' + json[x] + '</option>';
                            }
                        }
                        $('#listaTipos').html(html);
                        $("#listaTipos").chosen().trigger("chosen:updated");
                    }
                });
                $('#fieldTextSearch').attr('disabled', 'disabled');
            }else{
                $('.listaTipos_chosen').hide();
            }
            if (opcion.indexOf('4') > -1){
                $.ajax({
                    type: 'POST',
                    url: $('#listaEtiquetas').val(),
                    dataType: 'json',
                    error: function(){
                        console.log('error muestra etiquetas ajax');
                    },
                    beforeSend: function(){
                        $('.listaEtiqueta_chosen').show();
                    },
                    success: function(json){
                        console.log(json);
                        var html = '';
                        for(var x in json){
                            if(x !== 'length'){
                                html += '<option value='+x+'>'+json[x]+'</option>';
                            }
                        }
                        $('#listaEtiqueta').html(html);
                        $('#listaEtiqueta').chosen().trigger('chosen:updated');
                    }
                });
            }else{
                $('.listaEtiqueta_chosen').hide();
            }
        }else{
            $('#contenedorTabla').html('');
            $('.listaCategoria_chosen').hide();
            $('.listaTipos_chosen').hide();
            $('.listaEtiqueta_chosen').hide();
        }
    });
    
    
    $('#listaTipos').chosen().change(function (){
        var listaTipos = $('#listaTipos').val();
        if (listaTipos !== null) {
            $.ajax({
                type: 'POST',
                url: $('#mostrarpreguntatipo').val(),
                dataType: 'json',
                data: {
                    'idExamen'      : $('#idExamen').val(),
                    'idCategoria'   : $('#listaCategoria').val(),
                    'tipoPreg'      : listaTipos
                },
                error: function () {
                    console.log('error peticion ajax');
                },
                beforeSend: function () {
                    $('#contenedorTabla').html('');
                },
                success: function (json) {
                    console.log('lista de tipos');
                    loadDatatableSortable(json);
                }
            });
            
        } else {
            $('#contenedorTabla').html('');
        }
    });
    
//lista de categorias
$('#listaCategoria').chosen().change(function (){
    var listaCategoria = $('#listaCategoria').val();
    var listaTipos = $('#listaTipos').val();        
    if(listaCategoria !== null){

        $.ajax({
            type: 'POST',
            url: $('#mostrarCategoria').val(),
            dataType: 'json',
            data: {
                'idExamen'      :   $('#idExamen').val(),
                'idCategoria'   :   listaCategoria,
                'idTipos'       :   listaTipos
            },
            error: function () {
                console.log('Error mostrar categoria');
            },
            beforeSend: function () {
                $('#contenedorTabla').html('');
            },
            success: function (json) {
                    loadDatatableSortable(json);
            }
        });
    }else{
        $('#contenedorTabla').html('');
    }
});
    
    //lista de etiquetas
    $('#listaEtiqueta').chosen().change(function(){
        var listaCategoria  = $('#listaEtiqueta').val();
        var listaTipos      = $('#listaTipos').val();        
        var listaEtiqueta   = $('#listaEtiqueta').val();
        if(listaCategoria !== null){
            $.ajax({
                type: 'POST',
                url: $('#mostrarEtiqueta').val(),
                dataType: 'json',
                data: {
                    'idExamen'      :   $('#idExamen').val(),
                    'idCategoria'   :   listaCategoria,
                    'idTipos'       :   listaTipos,
                    'idEtiqueta'    :   listaEtiqueta
                },
                error: function () {
                    console.log('Error mostrar etiqueta');
                },
                beforeSend: function () {
                    $('#contenedorTabla').html('');
                },
                success: function (json) {
                    console.log('prueba funcion');
                    loadDatatableSortable(json);
                }
            });
        }else{
            $('#contenedorTabla').html('');
        }
    });
    
    $('.listaEtiqueta_chosen').hide();
    $('.listaTipos_chosen').hide();
    $('.listaCategoria_chosen').hide();
    
        
        //valida radios al eliminar seccion
        $( "#formEliminaSecc" ).validate({
                errorPlacement: function(error, element) {
                    if (element.attr("name") === "jq_nombre") {
                            error.appendTo('#reg01_errorNombre');
                    }
                    if (element.attr("name") === "jq_email") {
                            error.appendTo('#reg01_errorMail');
                    }
                },
                rules: {
                  radioTarea: {required: true},
                  selEviaSeccion: {min: 1}
                },
                messages: {
                 radioTarea: "*Debes elegir una opción",
                  selEviaSeccion: {min: "*Debes elegir una sección para mover las preguntas."}
                },
                submitHandler: function (){
                    
                }
        });
        
    //cierra el modal dropdown de foundation 
    $('.cerrar').on('click',function(){
        $(document).foundation('dropdown', 'close', $('.f-dropdown'));
    });
    MoverPregSeccion();
    MoverPregBanco();
    if($('.contenedor_preguntas').length === 0){
        console.log('la caja de secciones esta vacia');
    }
    
    //verifica si la pregunta ya pertenece a un examen que ya esta publicado de ser asi se copia la pregunta en caso de aceptar,
    //de lo contrario se edita la pregunta directamente
    $('.clonar_pregunta').on('click', function () {
        console.log('copiar pregunta');
        var idPregunta = $(this).parent().parent().parent().parent().attr('id').split('_');
        var idSeccionPregunta = $(this).parent().parent().parent().parent().parent().attr('id').split('_');
        idPregunta = idPregunta[1];
        idSeccionPregunta = idSeccionPregunta[2];
        $('#idPreguntaClonar').val(idPregunta);
        $('#idPreguntaSeccion').val(idSeccionPregunta);
        $.ajax({
            url: $('#urlPreguntaInExa').val(),
            dataType: 'json',
            type: 'POST',
            data: {
                "_idPregunta": $('#idPreguntaClonar').val()
            },
            success: function (json) {
                console.log(json);
                if(json === false){
                    var url = $('#redirecEditPregunta').val();
                    $(location).attr('href',url+'/'+idPregunta);
                }else{
                    //var url = $('#redirecEditPregunta').val();
                    //$(location).attr('href',url+'/'+idPregunta);
                    $('#modalClonarPregunta').foundation('reveal','open');
                }        
            },
            error: function (json) {
                console.log('error en la peticion: ' + json);
            }
        });
    });
    //copiar pregunta
    $('#acepta-clonar-pregunta').on('click',function(){
        //peticion ajax que clona la pregunta verificando si la pregunta ya existe en un examen publicado
        console.log('iniciar clonar pregunta');
        $.ajax({
            url: $('#urlCopiarPregunta').val(),
            dataType: 'json',
            type:'POST',		
            data:{
                "_idPregunta" : $('#idPreguntaClonar').val(),
                "_idPreguntaSeccion": $('#idPreguntaSeccion').val()
            },
            beforeSend: function(){

            },
            success: function(json){
                var idpreguntanueva = json['_idPregunta'];
                window.location = '/preguntas/edit/'+idpreguntanueva;
                $('#modalClonarPregunta').foundation('reveal','close');
            },                    
            error: function(json){
                    console.log('error en la peticion: '+json);
            }
        });
    });
    
    $('#publicar').on('click',function(){
        //verificar si ahy alguna seccion vacia, de ser asi no dejara publicar el examen hasta su eliminacion
        var boolEmpty    = false;
        $('.contenedor_preguntas').each(function(i){
            var idSeccion = $(this).attr('id').split('_');
            var seccionEmpty = $('#contenedor_seccion_'+idSeccion[1]).children().length;
            if(seccionEmpty === 0){
                return boolEmpty = true;
            }
        });
        if(boolEmpty === true){
            console.log('no debes publicar el examen con secciones vacias');
            $('#modalpublicExamEmptySeccion').foundation('reveal','open');
        }else{
            console.log('verifica que has guardado los cambios realizados');
            $('#confirmPublicar').foundation('reveal','open');
        }        
    });
    
    $('#aceptaAclamacion').on('click',function(){
        $('#modalpublicExamEmptySeccion').foundation('reveal','close');
    });
    
    //publicar el examen cambia el estatus a publicado t12 examen
    $('#acepta-publicarExamen').on('click',function(){
        $.ajax({
            url: $('#urlPublicarExamen').val(),
            dataType: 'json',
            type:'POST',		
            data:{
                "idExamen" : $('#idExamenPublicar').val()
            },
            beforeSend: function(){

            },
            success: function(json){
                console.log('Examen publicado correctamente'+json);
                window.location = '/examenes';
            },                    
            error: function(json){
                    console.log('error en la peticion: '+json);
            }
        });
    });
    
    $('#confirmPublicarBtn').on('click',function(){
        $('#confirmPublicar').foundation('reveal','close');
        $('#idExamenPublicar').val($('#idExamen').val());
        $('#modalPublicarExamen').foundation('reveal','open');
    });
});

//Mueve una pregunta a una seccion especifica
function MoverPregSeccion(){
    $('.selEviaSeccion').change(function(){
        var idSeccionSelect       = $(this).val();
        var seccionOrigen   = $(this).parent().parent().parent();
        console.log(idSeccionSelect);
        console.log(seccionOrigen);
        $(document).foundation('dropdown', 'close', $('.f-dropdown'));
        var seccionDestino  = $('#contenedor_seccion_'+idSeccionSelect);
        seccionOrigen.appendTo(seccionDestino);
        $('strong').each(function (i) {
            var strong = $(this);
            strong.html(++i + '.- ');
        });
    });
}
//control para eliminar pregunta enviar al banco de preguntas 
//de inmediato rompe la relacion que hay entre examen y pregunta
function MoverPregBanco() {
    $('.fa-trash').on('click', function () {
        console.log('click mover Banco pregunta');
        var seccionOrigen = $(this).parent().parent().parent();
        var isClick = false;
        var idSec = $(seccionOrigen).parent().attr('id');
        var cutIdSeccion = idSec.split('seccion_');
        var id_seccion = cutIdSeccion[1];
        var id_examen = $('#idExamen').val();
        //conseguir cada uno de los ids de cada pregunta de una seccion especifica;
        var contenedorPreguntas = $('#'+idSec).children().last('div').attr('id');
        
        $('#deleteEmptySeccion').on('click',function(){
            console.log('eliminar seccion vacio');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#moverPregBancoGral').val(),
                data: {
                    'id_examen': id_examen,
                    'id_seccion': id_seccion
                },
                success: function () {
                    if ($('.contenedor-preguntas').val() === "") {
                        //oculta las preguntas y cierra la notificacion
                        $('#' + contenedorPreguntas).parent().addClass('hide');
                        console.log('el contenedor de preguntas esta vacio');
                    }
                    $('#modalEliminarSecc').foundation('reveal', 'close');
                    console.log("Enlace de la pregunta seccion eliminada correctamente");
                    location.reload();
                },
                error: function (json) {
                    console.log("Hubo un error al procesar los datos: " + json);
                }
            });
        });
        
        $('input[type=radio][name=radioTarea]').change(function () {
            
            //envia todas las preguntas al banco que pertenecen a un id en especifico y rompe la relacion que hay entre pregunta examen.
            if(this.value === '1'){
                console.log('preguntas enviadas al banco'+contenedorPreguntas);
                $('#btnConfirEliminaSecc').on('click',function(){
                    
                    //si el contenedor de la seccion esta vacio recibe el id del examen y rompe la relacion seccion examen
                    //y si no recibe un array de preguntas y el id de la seccion para romper la relacion y las manda al banco de preguntas
                    if($('#'+contenedorPreguntas).children().length === 0){
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: $('#moverPregBancoGral').val(),
                            data: {
                                'id_examen': id_examen,
                                'id_seccion': id_seccion
                            },
                            success: function () {
                                if ($('.contenedor-preguntas').val() === "") {
                                    //oculta las preguntas y cierra la notificacion
                                    $('#' + contenedorPreguntas).parent().addClass('hide');
                                    console.log('el contenedor de preguntas esta vacion');
                                }
                                $('#modalEliminarSecc').foundation('reveal', 'close');
                                console.log("Enlace de la pregunta seccion eliminada correctamente");
                            },
                            error: function (json) {
                                console.log("Hubo un error al procesar los datos: " + json);
                            }
                        });
                    }else{
                        var arrPreg = [];
                        $('#'+contenedorPreguntas).children().each(function(){
                            var cutIdPreg = $(this).attr('id').split('sortable_');
                            var id_preg = cutIdPreg[1];
                            arrPreg.push(id_preg);
                        });
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            url: $('#moverPregBancoGral').val(),
                            data: {
                                'id_preg': arrPreg,
                                'id_seccion': id_seccion
                            },
                            success: function () {
                                $('#' + contenedorPreguntas).parent().addClass('hide');
                                $('#modalEliminarSecc').foundation('reveal', 'close');
                                console.log("Enlace de la pregunta seccion eliminada correctamente");
                            },
                            error: function (json) {
                                console.log("Hubo un error al procesar los datos: " + json);
                            }
                        });                        
                    }
                });
            }
            //envia todas las preguntas a otra seccion y elimina la seccion rompiendo la relacion seccion-examen
            if(this.value === '2'){
                var idSeccionCut = seccionOrigen.parent().attr('id').split('seccion_');
                var idSeccion = idSeccionCut[1];
                $('#divSelEviaSeccion').show(); 
                $('.selEviaSeccion1').change(function () {
                    var numSeccioDestino = this.value;
                    console.log(numSeccioDestino);
                    var idSecciondestino = $('#contenedor_seccion_' + numSeccioDestino);
                    console.log('seccion destino: ' + idSecciondestino);
                    var idsPreguntas = [];
                    var objetoPregunta = $('#seccion_' + idSeccion).children('#contenedor_seccion_' + idSeccion).children().each(function () {
                        idsPreguntas.push(this.id);
                    });
                    $('#btnConfirEliminaSecc').on('click', function () {
                        console.log('click en el boton');
                        for (var i = 0; i < idsPreguntas.length; i++) {
                            $(idSecciondestino).append($('#' + idsPreguntas[i]));
                        }
                        seccionOrigen.parent().addClass('hide');
                        //seccionOrigen.parent().remove();
                        $('#modalEliminarSecc').foundation('reveal', 'close');
                    });
                });
            }else {
                $('#divSelEviaSeccion').hide();
            }
        });
        //envia una pregunta en especifica al banco de pregunta 
        //y rompe la relacion que hay entre pregunta examen.
        if (isClick === false) {
            console.log('eliminar pregunta ');
            $('#btnAceptarEliminarPreg').on('click', function () {
                seccionOrigen.removeClass('ui-state-highlight');
                seccionOrigen.addClass('ui-state-default');
                seccionOrigen.children('div:last').remove();
                seccionOrigen.children('div:last').remove();
                
                var cutIdPreg = $(seccionOrigen).parent().attr('id').split('sortable_');
                var id_preg = cutIdPreg[1];
                console.log(seccionOrigen);
                
                var cutIdSeccion = seccionOrigen.parent().parent().attr('id').split('seccion_');
                console.log(cutIdSeccion);
                 
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: $('#moverPregBancoGral').val(),
                    data: {
                        'id_preg': id_preg,
                        'id_seccion': cutIdSeccion[1]
                    },
                    success: function () {
                        console.log("Enlace de la pregunta seccion eliminada correctamente");
                        //$('#sortable_'+id_preg).addClass('hide');
                        $('#sortable_'+id_preg).remove();
                    },
                    error: function (json) {
                        console.log("Hubo un error al procesar los datos: " + json);
                    }
                });
                if($('.contenedor-preguntas').val() === ""){
                    seccionOrigen.addClass('hide');
                }
                $('#modalEliminarPreg').foundation('reveal', 'close');
            });
        }
    });
}

function creaObjetoExamen(){
    var _ProjJson = new Object();
    var objSeccionPregunta = new Object();
    var orden = 1;
    //$('.contenedor_preguntas').not('.hide').each(function (){
    $('.contenedor_preguntas').each(function (){
        if(this.id){
            var arrText  = $(this).attr('id').split('_'); 
            var idSecc   = arrText[1];
            var nomSecc  = $(this).children().eq(0).children().children().eq(0).val();
            //var nomSecc = $('#nombreSecc'+idSecc).html();
            var valPorcent = $(this).children().eq(0).children().children().eq(1).children().val();
            //var valPorcent = $('#valorPorcent'+idSecc).val();
            var tipoEv = ($(this).children().eq(1).children().eq(4).children().children().prop('checked') === false) ? 1 : 2 ;
            //var tipoEv = ($('#evalSwitch'+idSecc).prop('checked') === false) ? 1 : 2 ;
            
            //almacena id de las preguntas
            var preguntas =[];
            var pregunta =[];
    
            
            $(this).children().children().each(function (){
                if(this.id){
                    var arrText = $(this).attr('id').split('_'); 
                    var idPreg = arrText[1];
                    var preguntaa = {
                        orden : {
                            'id' : idPreg,
                            'orden' : orden
                        }
                    };
                    pregunta = [orden,idPreg];
                    preguntas.push(pregunta);
                    orden ++;
                }
                
               });
           _ProjJson.seccion    = nomSecc;
           _ProjJson.porcentaje = valPorcent;
           _ProjJson.tipoEv     = tipoEv;
           _ProjJson.preguntas  = preguntas;
           objSeccionPregunta[idSecc] = _ProjJson;
           _ProjJson = new Object();
       }
   });
   return objSeccionPregunta;
}

function ligarpreguntas(_objExamen, id_examen){
    
    _objExamen = creaObjetoExamen();
    console.log(_objExamen);
    var sumatoria = 0;
    var _resultado = 0;
    
    $('.typeNumber').each(function (){
        var valorInp = $(this).val();
        sumatoria += +valorInp;
    });
    if(sumatoria > 100){
        _resultado = sumatoria - 100;
        $('#erroPonderacion').html('El porcentaje de ponderación del total de las secciones no debe exceder del 100 %, te sobraron ' + _resultado);
        $('#erroPonderacion').removeClass('hide');
    }else if(sumatoria < 100){
        _resultado =  100 - sumatoria;
        $('#missPonderacion').html('El porcentaje de ponderación del total de las secciones no debe ser menos del 100 %, te faltan ' + _resultado);
        $('#missPonderacion').removeClass('hide');
    }
    else{
        $('#erroPonderacion').addClass('hide');
    
    $.ajax({
            type:'POST',
            dataType:'json',
            url:  $('#urlgrdPreguntas').val(),
            data:{
                'objPreguntas'    : _objExamen,
                'idExamen'      : $('#idExamen').val() 
            },
            beforeSend: function() {
                
            },
            success:function(json){
                console.log("Los datos se almacenaron correctamente");
                $('#modalGrdCambios').foundation('reveal','open');                
                $('#erroPonderacion').addClass('hide');
                $('#missPonderacion').addClass('hide');
            },
            error: function(json){
                console.log("Hubo un error al procesar los datos: " + json);
            }
        });
    }
}

drag_pregunta_seccion();
function drag_pregunta_seccion(){
    $(function(){
        $( ".sortable" ).sortable({
            helper: 'clone',
            appendTo: 'body',
            connectWith: "tbody, .sortable",
            start: function(evt, ui){
                ui.placeholder.html(ui.item.html());//espacio entre placeholder al mover la pregunta
            },
            stop: function(evt, ui){
                var listadopreguntasTable   = $(ui.item).parent().parent().attr('id');
                if(listadopreguntasTable === 'listadopreguntasTable'){
                    $(ui.item).children().first().children().remove('strong');
                    var content = $(ui.item).children().first().html();
                    var idPregunta = $(ui.item).attr('id').split('_');
                    idPregunta= idPregunta[1];
                    console.log('arrastraste pregunta al contenedor');
                    console.log(content);
                    var div = contentToTable(idPregunta,content);
                    $(ui.item).replaceWith(div);
                }else{
                    console.log('movimiento entre secciones');
                    $('strong').each(function (i) {
                        var strong = $(this);
                        strong.html(++i + '.- ');
                    });
                }
                MoverPregBanco();
                //MoverPregSeccion();
            }
        });
    });
}

function verificarLista(contenedorSecciones, nuevoId){
    var duplicado = false;
    $("."+contenedorSecciones).each(function(){
        var idLista = $(this).attr('id');
        if (idLista === nuevoId){
            duplicado = true;
        }
    });
    
    return !duplicado;
}
function editaPregunta(_idExamen){
    alert(_idExamen);
}

function eliminaSeccion(_idSeccEx){
    console.log('elimina seccion'+_idSeccEx);
    var miIdSave = $('#seccion_nueva'+_idSeccEx).length;
    var ifExistsPregunta;
    var miId;
    if(miIdSave !== 0 ){
        miId = $('#seccion_nueva'+_idSeccEx).attr('id').split('_');
        ifExistsPregunta = $('#contenedor_seccion_nueva'+_idSeccEx).children().length;
    }else{
        ifExistsPregunta = $('#contenedor_seccion_'+_idSeccEx).children().length;
        miId = $('#seccion_'+_idSeccEx).attr('id').split('_');
    }
    if(ifExistsPregunta === 0){
        if($.isNumeric(miId[1]) === false){
            $('#selEviaSeccion').val(0);
            $('#hiddenIdSeccEx').val(_idSeccEx);
            //$('#modalEliminarSecc').foundation('reveal', 'open');
            //MoverPregBanco();
            $('#modalDeleteSeccionEmpty').foundation('reveal', 'open');
        }else{
            $('#hiddenIdSeccEx1').val(_idSeccEx);
            $('#modalDeleteSeccionEmpty').foundation('reveal', 'open');
        }
    }else{
        $('#selEviaSeccion').val(0);
        $('#hiddenIdSeccEx').val(_idSeccEx);
        $('#modalEliminarSecc').foundation('reveal', 'open');
        ocultaOpciones(_idSeccEx);
    }
    
    $('#deleteEmptySeccion').on('click', function () {
        //console.log('eliminar seccion vacio');
        $('#seccion_nueva'+_idSeccEx).remove();
        $('#modalDeleteSeccionEmpty').foundation('reveal', 'close');
    });
}

function ocultaModalEliminaSecc(){
    $('#modalEliminarSecc').foundation('reveal', 'close');
    $('#divSelEviaSeccion').hide();
    $('input[type=radio][name=radioTarea]').prop('checked', false);
}

function ocultaOpciones (_idSeccEx){
    $('#selEviaSeccion option').each(function (){
        if(this.value === _idSeccEx){
            this.style.display='none';
        }else{
            this.style.display='block';
        }
    });
}

function eliminaPreguntaSecc(_idSeccEx){
    $('#hiddenIdPreg').val(_idSeccEx);
    $('#modalEliminarPreg').foundation('reveal', 'open');
}


 function consultaIdSeccionName(){
     var objExam = creaObjetoExamen();
     var option='';
     for(var seccion in objExam){
         option += '<option value='+seccion+'>'+objExam[seccion].seccion+'</option>';
     }
     return option;
 }
 
 function contentToDiv(idPregunta,content,optionHtml){
     var elementoPregunta= $(
            '<div id="sortable_'+idPregunta+'" class="large-12 columns pregunta-seleccionada ui-state-highlight item ui-sortable-handle">'+
                '<div class="text-left large-11 medium-11 small-11 columns seleccionada" id=preg_'+idPregunta+'>'+
                '<strong>nuevo strong</strong>'+
                    content+
                '</div>'+
                '<div class="large-1 medium-1 small-1 columns">' +
                    '<span data-tooltip="Configurar" aria-haspopup="true" class="has-tip tip-top" title="Configurar" >' +
                        '<i id="btnOpcPreg-'+idPregunta+'" class="confPregunta fa fa-cog fa-2x" data-dropdown="dropMenuToolPreg'+idPregunta+'" aria-controls="dropMenuToolPreg'+idPregunta+'" aria-expanded="false"></i>' +
                    '</span>' +
                '</div>' +
                '<div id="dropMenuToolPreg' + idPregunta + '" data-dropdown-content class="tiny f-dropdown" aria-hidden="true">' +
                    '<div style="text-align: right"><i class="cerrar fa fa-times columns"></i>'+
                    '</div><br>' +
                    '<div class="text-center">' +
                        '<a data-tooltip class="has-tip tip-top" title="Editar Pregunta">' +
                            '<i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;&nbsp;' +
                        '</a>' +
                        '<a href="#" onclick="eliminaPreguntaSecc(' + idPregunta + ')" id="btnElimina' + idPregunta + '" data-tooltip class="has-tip tip-top" title="Regresar pregunta al banco">' +
                            '<i class="fa fa-trash fa-lg"></i>' +
                        '</a>' +
                    '</div>' +
                    '<br>' +
                    '<div class="small-12 medium-12 large-12 columns">' +
                        '<select id="selEviaSeccion" name="selEviaSeccion" required data-tooltip class="tip-top selEviaSeccion" title="Mover pregunta a otra sección">' +
                            '<option value="0">Mover a sección</option>'+optionHtml+
                        '</select>' +
                    '</div><br>' +
                    '<div class="small-12 medium-12 large-12 columns hide">' +
                        '<select id="selTipoPreg" name="selEviaSeccion" required data-tooltip class="tip-top" title="Cambiar el tipo de pregunta">' +
                            '<option id="selSecc" value="">tipo pregunta</option>' +
                        '</select>' +
                    '<div>' +
                '</div>'+
            '</div>');
    return elementoPregunta;
 }
 function contentToTable(idPregunta,content){
     var elementoTable = $(
             '<tr role="row" class="even ui-sortable-handle">'+
                '<td class="sorting_1">'+
                    '<div id="elemento_'+idPregunta+'" class="ui-state-default item" style="overflow-x: hidden;">'+
                        '<div id='+idPregunta+' class="lista-preguntas">'+
                        content+
                        '</div>'+
                    '</div>'+
                '</td>'+
             '</tr>'
             );
     return elementoTable;
 }
 
 function loadDatatableSortable(json) {
    if (json === false) {
        var contPregunta = 'No hay preguntas disponibles de este tipo';
        var div1 = $('<div id="mensajeVacio" class="lista-preguntas" >' + contPregunta + '</div>');
        $('#contenedorTabla').append(div1);
    } else {
        var arrPreg = [];
        var Tabla = '<table id="listadopreguntasTable" cellspacing="0" class="display compact contenedor-preguntas list dropfalse"></table>';
        for (var c in json) {
            var dataPregunta = json[c]['t11pregunta'];
            var dataIdPregunta = json[c]['t11id_pregunta'];
            var div1 = '<div id="elemento_' + dataIdPregunta + '" class="ui-state-default item ui-sortable-handle" style="overflow-x: hidden;">' +
                    '<div id="' + dataIdPregunta + '" class="lista-preguntas">' + dataPregunta +
                    '</div>' +
                    '</div>';
            arrPreg.push([div1]);
        }
        $('#contenedorTabla').append(Tabla);
        $('#listadopreguntasTable').dataTable({
            //"bJQueryUI": true,
            data: arrPreg,
            columns: [{title: 'Preguntas'}]
        });
        $('tbody').sortable({
            connectWith: '.sortable',
            stop: function (event, ui) {
                var getIdlistaTable = $(ui.item).parent().parent().attr('id');
                if (getIdlistaTable === 'listadopreguntasTable') {
                    console.log('pregunta arrastrada en la misma tabla');
                } else {
                    console.log('tabla --> seccion');
                    var optionHtml = consultaIdSeccionName();
                    var content = $(ui.item).children().children().children().html();
                    var idPregunta = $(ui.item).children().children().children().attr('id');
                    var div = contentToDiv(idPregunta, content, optionHtml);
                    $(ui.item).replaceWith(div);
                    $('strong').each(function (i) {
                        var strong = $(this);
                        strong.html(++i + '.- ');
                    });
                }  
                MoverPregSeccion();
            }
        }).disableSelection();
    }
}