$(document).ready(function () {
    $('.jq_tipo_pregunta').on('click', function () {
        $('#id_tipo_pregunta').val($(this).attr('id')).trigger('change');
    });
    $('#id_tipo_pregunta').on('change', cargaFormulario);

    function cargaFormulario() {
        var idTipo = $('#id_tipo_pregunta').val();
        console.log($('#' + idTipo));
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $('#urlCambiaTipoPregunta').val(),
            data: {
                'idTipoPregunta': idTipo
            },
            beforeSend: function () {

            },
            success: function (json) {

                if (json) {
                    //console.log(json);
                    var elemResp = json['formResp'];
                    for (var elem in elemResp) {
                        var attrElem = elemResp[elem];
                        for (var attr in attrElem) {
                            var valAttr = attrElem[attr];
                            if (valAttr !== null && typeof valAttr === "object") {
                                if (typeof valAttr.name !== 'undefined') {
                                    // console.log(  valAttr.name  );
                                    $('.' + valAttr.name).each(function () {
                                        if (valAttr.name === 'correcta') {
                                            $('.respuesta_style').each(function () {

                                                $(this).find('label').attr('class', valAttr.class).removeClass('_objPregResp').addClass('large-4 medium-4 small-10 cont_correcta');
                                                
                                            });
                                        }
                                        if (valAttr.name === 'respuesta') {
                                            $('.respuesta_style').each(function () {
                                                $(this).find('label').attr('class', valAttr.class)
                                                        .removeClass('_objPregResp')
                                                        .removeClass('t13respuesta')
                                                        .removeClass('respuesta')
                                                        .addClass('cont_texto_resp');
                                            });
                                        }
                                        
                                        $(this).get(0).type = valAttr.type;
                                        $(this).attr('placeholder', valAttr.placeholder);
                                    });
                                }
                            }
                        }
                    }
                    
                    $('#boton-nueva,.boton-borrar').removeClass('hide');
                    limpiarFormulario(json['minimo_maximo'].minimo);
                    muestraOcultaCamposRespuestas(json['minimo_maximo'].minimo, json['minimo_maximo'].maximo);
                    //jsPanelTipos.smallify();
                    tinymce.remove();
                    
                    tinymce.init(objEditorElemental);

                    tinymce.init(objEditorBasico);
                    
                    jsPanelTipos.close();

                }
            },
            error: function (json) {
                console.log("Hubo un error al procesar los datos");
            }
        });
    }

    function muestraOcultaCamposRespuestas(intMinimoRespuestas, intMaximoRespuestas) {

        var iCorr = 0;
        var tipoPregunta = parseInt($('#id_tipo_pregunta').val());
        for (var i = 0; i < 20; i++) {
            iCorr = i + 1;
        }
        if (intMinimoRespuestas === intMaximoRespuestas) {
            $('#boton-nueva').addClass('hide');
            $('.boton-borrar').addClass('hide');
        } else {
            $('#boton-nueva').removeClass('hide');
        }
        // Agrego el caso de verdadero falso donde las respuestas no cambian
        if (tipoPregunta === 3) {
            tinymce.editors[2].setContent('Verdadero'); //.getBody().setAttribute('contenteditable', false);
            tinymce.editors[3].setContent('Falso'); //.getBody().setAttribute('contenteditable', false);
            
        } else {
            //$('.respuesta').val('').removeAttr('disabled');
        }

    }

    function limpiarFormulario(_intMinimoRespuestas) {

        var i = 0;
        $('._objPregResp').each(function () {
            $(this).not('#id_tipo_pregunta').val('');
            $(this).removeAttr('selected');
            $(this).removeAttr('checked');
        });
        $('.boton-borrar').each(function () {
            if (i < _intMinimoRespuestas) {
                //console.log($(this).parent().parent());
                $(this).parent().parent().removeClass('hide');
                $(this).parent().parent().find('.mce-tinymce').each(function () {
                    $(this).removeClass('hide');
                });
            } else {
                
                $(this).parent().parent().addClass('hide');
            
            }
            i++;
        });
        var numEditors = tinymce.editors.length;
        for (var i = 0; i < numEditors; i++) {
            tinymce.editors[i].setContent('');
        }
    }

});