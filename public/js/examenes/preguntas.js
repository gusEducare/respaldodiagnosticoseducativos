$(document).ready(function () {
    //$(":submit").click(function() { return false; });
    if ($('#urlPaginaActual').val() === 'add') {
        cargarJsPanel();
    } else {
        cargaFormulario();
    }

    var id_tipo_pregunta = 1;
    var id_pregunta = 0;

    $('#boton-almacenar').on('click', function (e) {
        console.log('boton almacenar');
        var valCategoria = $($('#textAreaCategoria').tagEditor('getTags')[0].tags).length;
        var valEtiqueta = $($('#textAreaEtiqueta').tagEditor('getTags')[0].tags).length;
        var miForm = $('#Pregunta').valid();
        e.preventDefault();
        if(valCategoria === 0){
            miForm = false;
            //mostrar error categorias
            $('#errorSelectCategoria').show();
        }
        else{
            miForm = true;
            $('#errorSelectCategoria').hide();
        }
        if(valEtiqueta === 0){
            miForm = false;
            //mostrar error categorias
            $('#errorSelectTag').show();
        }
        else{
            miForm = true;
            $('#errorSelectTag').hide();
        }
        if(!$('#Pregunta').valid()){
            //miForm = false;
        }
        
        if (miForm === true) {
            console.log(true);
            $('#modalConfir').foundation('reveal', 'open');
            _objPregunta = creaObjetoPregunta();            
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#urlCreaPregunta').val(),
                data: {
                    'arrObjPregunta': _objPregunta,
                    'idPregunta': id_pregunta
                },
                beforeSend: function () {

                },
                success: function (json) {
                    console.log(json);
                    $('#loading').hide();
                    if (json['bolGuarda'] === true) {
                        $("#cerrar-reveal-add").prop('href',window.location.href+'/../edit/'+json['_idPregunta']);
                        $("#modalSucces").show();
                        $("#modalError").hide();
                    }
                    else {
                        $("#cerrar-reveal-add").prop('href','#');
                        $("#modalSucces").hide();
                        $("#modalError").show();
                    }
                    $("#modalBtnAceptar").show();

                },
                error: function (json) {
                    $('#loading').hide();
                    $("#modalError").hide();
                    $("#modalSucces").hide();
                    $('#modalAjaxError').show();
                    $('#modalBtnAceptar').show();
                    console.log("ERROR: Hubo un error al procesar los datos: " + json);
                }
            });
             
        }
    });

    $('#boton-editar').on('click', function (e) {        
        e.preventDefault();
        var valCategoria = $($('#textAreaCategoria').tagEditor('getTags')[0].tags).length;
        var valEtiqueta = $($('#textAreaEtiqueta').tagEditor('getTags')[0].tags).length;
        var miForm = $('#Pregunta').valid();
        console.log(miForm);
        if(valCategoria === 0){
            miForm = false;
            //mostrar error categorias
            $('#errorSelectCategoria').show();
        }
        else{
            $('#errorSelectCategoria').hide();
        }
        if(valEtiqueta === 0){
            miForm = false;
            //mostrar error categorias
            $('#errorSelectTag').show();
        }
        else{
            $('#errorSelectTag').hide();
        }
        if(!$('#Pregunta').valid()){
            miForm = false;
        }
        if (miForm === true) {
            console.log('miForm is true');
            _objPregunta = creaObjetoRespuestas();
            console.log(_objPregunta);
            $('#modalConfir').foundation('reveal', 'open');
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#urleditaPreguntas').val(),
                data: {
                    '_objPregunta': _objPregunta
                },
                beforeSend: function () {
                },
                success: function (json) {
                    console.log(json);
                    $("#loading").hide();
                    if (json === true) {
                        $("#modalSucces").show();
                        $("#modalError").hide();
                    }
                    else {
                        $("#modalSucces").hide();
                        $("#modalError").show();
                    }
                    $("#modalBtnAceptar").show();
                },
                error: function (json) {
                    $('#loading').hide();
                    $("#modalError").hide();
                    $("#modalSucces").hide();
                    $('#modalAjaxError').show();
                    $('#modalBtnAceptar').show();
                    console.log("ERROR: Hubo un error al procesar los datos: " + json);
                }
            });
        }
    });

    $("#cerrar-reveal").click(function () {
        $('#modalConfir').foundation('reveal', 'close');
        location.reload();
    });

    $('.respuesta').each(function () {
        $('input[type=radio].hide').parent().parent().addClass('hide');
        $('.respuesta_style').children().children().removeClass('hide');

        $('input[type=text].hide').val('');
    });




    function cargarJsPanel() {
        jsPanelTipos = $.jsPanel({
            //panelstatus: "maximized", 
            ajax: {
                url: $('#urlMostrarTipos').val()
            },
            //selector: ".row-contenedor",
            title: "Elige tipo de pregunta",
            // offset:   { top: 0, left: -202 },
            theme: "default",
            position: "center",
            size: {width: 900, height: 640},
            draggable: "disabled",
            resizable: "disabled",
            // content: $(".contenedor_tipos").html(),
            controls: {maximize: true, minimize: true, smallify: true, normalize: true, close: true}
            //removeHeader:  true
        });
    }

    /*
     $.jsPanel({
     
     ajax: {
     url:    $('#urlMostrarBuscador').val()
     },
     
     selector: ".row_pregunta",
     title: "Buscador",
     theme: "success",      
     // offset:   { top: 0, left: -202 },  
     position: "top right", 
     size:     { width: 430, height: 470 },
     resizable: "disabled",
     //     content: '<div>Hello</div>',
     controls: {maximize: true, close: true}
     });    
     */

    //jsPanelTipos.maximize();

    // Oculto el combo de tipo de pregunta, ya que el tipo se elegira desde
    // los iconos
    $('#id_tipo_pregunta').hide();


    // Agrego estilos a los elementos del formulario
    $('._objPregunta').addClass('small-12 large-12 medium-8 columns');
    $('label').addClass('small-12 large-4 medium-1 columns');
    $('input[type="text"],select').not('.t13respuesta').wrap("<div class='large-12 medium-12 small-12 columns'> </div>");
    $('.t13respuesta').wrap("<div class='large-7 medium-7 small-12 columns cont_respuesta'> </div>");
    //$('.t13respuesta').parent().addClass('large-10 medium-10');

    //Creo los handlers  

    $('#boton-reset').on('click', cancelaPregunta);


    //$('.pregunta').on('click', agregaPregunta);     
    //$("#pregunta").kendoEditor();
    //$(".t13respuesta").kendoEditor();
    // Oculto el contenedor de la botonera
    //$('.contenedor-botonera').css('display', 'none');

    // Link para abrir el dialog de tipo de pregunta
    $("#dialog-tipo").click(function (event) {
        obtenerTiposPreguntas();
        $("#dialog_tipo").dialog("open", "position", {position: {my: "left top", at: "left bottom", of: window}});
        event.preventDefault();
    });

    // Hover states on the static widgets
    $("#dialog-tipo, #icons li").hover(function () {
        $(this).addClass("ui-state-hover");
    },
            function () {
                $(this).removeClass("ui-state-hover");
            }
    );

    // Link para abrir el dialog de configuración
    $("#dialog-config").click(function (event) {
        $("#dialog_config").dialog("open", "position", {position: {my: "left top", at: "left bottom", of: window}});
        event.preventDefault();
    });

    // Hover states on the static widgets
    $("#dialog-config, #icons li").hover(function () {
        $(this).addClass("ui-state-hover");
    },
            function () {
                $(this).removeClass("ui-state-hover");
            }
    );

    // Link para abrir el dialog de banco de preguntas
    $("#dialog-banco").click(function (event) {
        obtenerPreguntas();
        $("#dialog_banco").dialog("open", "position", {position: {my: "left top", at: "left bottom", of: window}});
        event.preventDefault();
    });

    // Hover states on the static widgets
    $("#dialog-banco, #icons li").hover(function () {
        $(this).addClass("ui-state-hover");
    },
            function () {
                $(this).removeClass("ui-state-hover");
            }
    );
    $("#dialog_tipo").dialog({
        position: {my: "left top", at: "right bottom", of: $('.container')},
        autoOpen: false,
        width: 400
    });

    $("#dialog_config").dialog({
        position: {my: "left top", at: "right bottom", of: $('.container')},
        autoOpen: false,
        width: 400
    });

    $("#dialog_banco").dialog({
        position: {my: "left top", at: "right bottom", of: $('.container')},
        autoOpen: false,
        width: 400
    });

    $('#chk-tiempo').change(function () {
        if ($(this).is(':checked')) {
            $('#t11tiempo_limite').removeAttr('disabled');
        } else {
            $('#t11tiempo_limite').attr('disabled', 'disabled');
            $('#t11tiempo_limite').val('');
        }
    });

    var msgRequire = 'Campo Requerido';

    function cancelaPregunta() {
        console.log('Acción de cancelar');
    }

    //$('.boton-borrar').on('click',borrarRespuesta);

    function borrarRespuesta() {
        var classRespuesta = $('.respuesta_style:not(.hide)').length;
        var isValid = false;
        if (classRespuesta > 3) {
            console.log('limpio mi formulario aqui ................', classRespuesta);
            $(this).parent().parent().addClass('hide');
            limpiarCampos($(this).parent().parent());
            //{
            // $(this).addClass('hide');
            //});
            // $(this).parent().prev().find('input').addClass('hide').removeAttr('checked').removeAttr('selected');
        } else {
            console.log('Las preguntas de opción multiple deben contener al menos tres respuestas');
        }

        var elemeVisibles = $('input:text.respuesta:visible').length;
        for (var i = elemeVisibles; i < 20; i++) {
            $('label[for="respuesta_' + i + '"].error').hide();
        }
    }

    $('#boton-nueva').click(function () {
        //creaNuevaRespuesta();
        muestraNuevaRespuesta();
    });

    function creaPregunta(_ProjJson, _intIdPregunta) {

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $('#urlCreaPregunta').val(),
            data: {
                'objJson': _ProjJson,
                'idPregunta': _intIdPregunta
            },
            beforeSend: function () {

            },
            success: function (json) {
                if (json === true) {
                    $("#modalSucces").show();
                    $("#modalError").hide();
                }
                else {
                    $("#modalSucces").hide();
                    $("#modalError").show();
                }
                $("#modalBtnAceptar").show();

            },
            error: function (json) {
                $('#loading').hide();
                $("#modalError").hide();
                $("#modalSucces").hide();
                $('#modalAjaxError').show();
                $('#modalBtnAceptar').show();
                console.log("ERROR: Hubo un error al procesar los datos: " + json);
            }
        });
    }

    function obtenerRespuestas() {
        console.log('obtenerRespuestas');
        var _strRespuestas = '';/*
         if( id_tipo_pregunta ){
         $('#boton-almacenar').css('display', 'block');
         $('#boton-actualizar').css('display', 'none');
         }else{
         $('#boton-almacenar').css('display', 'none');
         $('#boton-actualizar').css('display', 'block');
         }
         $('.contenedor-botonera').css('display', 'block');*/
        if (id_pregunta) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#urlObtenerRespuestas').val(),
                data: {
                    'tipoPregunta': id_tipo_pregunta,
                    'idPergunta': id_pregunta
                },
                beforeSend: function () {

                },
                success: function (json) {
                    if (json) {
                        //var i = 0, j = 1;
                        var numRespuestas = json['numRespuestas'];
                        var _strChecked = '';
                        var _strRespuesta = '';
                        for (var i = 0, j = 1; i < numRespuestas; i++, j++) {
                            if (typeof json['respuestas'][i] !== 'undefined') {
                                _strRespuesta = json['respuestas'][i].t13respuesta;

                                if (json['respuestas'][i].t17correcta === "1") {
                                    _strChecked = 'respuesta_' + j;
                                }
                            }
                            _strRespuestas += generaCodigoRespuesta(i, j, _strRespuesta);
                        }
                        $('#contenedor-respuestas').html(_strRespuestas);
                        
                        $('#' + _strChecked).attr('checked', 'checked');

                        if (typeof json['pregunta'] !== 'undefined') {
                            $('#t11nombre_corto_clave').val(json['pregunta']['t11nombre_corto_clave']);
                            $('#t11instruccion').html(json['pregunta']['t11instruccion']);
                            $('#t11pregunta').html(json['pregunta']['t11pregunta']);
                            $('#c03id_tipo_pregunta').val(json['pregunta']['c03id_tipo_pregunta']);
                        } else {
                            /*
                             $('#t11nombre_corto_clave').val(''); 
                             $('#t11instruccion').val(''); 
                             $('#t11pregunta').val(''); 
                             $('#c03id_tipo_pregunta').val(id_tipo_pregunta);
                             */
                        }


                    } else {
                        //  $.mensaje("crearMensaje", { tipo_mensaje: "alerta", mensaje: "Hubo un error al procesar los datos" });
                    }
                },
                error: function (json) {
                    //  $.mensaje("crearMensaje", { tipo_mensaje: "alerta", mensaje: "Hubo un error al procesar los datos" });
                    console.log("Hubo un error al procesar los datos");
                }
            });
        } else {
            $('#id_tipo_pregunta').val(1).trigger('change');
        }
    }

    function obtenerTiposPreguntas() {
        var strTipos = '';

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $('#urlObtenerTipos').val(),
            data: {
            },
            beforeSend: function () {

            },
            success: function (json) {
                if (json) {
                    var numTipos = json.length;

                    for (var i = 1; i <= numTipos; i++) {
                        console.log(numTipos);
                        strTipos +=
                                '<a href="#" id="' + json[i] + '" ' +
                                '   value="' + i + '"' +
                                '       class="boton-gral boton-tipo"> ' +
                                //  '           <span class="ui-icon ' + json[i].c03url_icono_tipo+ '"></span>' +
                                json[i] +
                                '</a><br />';
                    }
                    $('#dialog_tipo').html(strTipos);
                    creaHandlers('.boton-tipo');

                } else {

                }
            },
            error: function (json) {
                console.log("Hubo un error al procesar los datos");
            }
        });
    }

    function obtenerPreguntas() {
        var strTipos = '';
        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $('#urlObtenerPreguntas').val(),
            data: {},
            beforeSend: function () {

            },
            success: function (json) {
                if (json) {
                    var numTipos = json.length;
                    for (var i = 0; i < numTipos; i++) {
                        strTipos +=
                                '<a href="#" id="' + json[i].t11nombre_corto_clave + '" ' +
                                '   value="' + json[i].t11id_pregunta + '"' +
                                '       class="boton-gral boton-banco"> ' +
                                json[i].t11pregunta +
                                '</a><br />';
                    }
                    $('#dialog_banco').html(strTipos);
                    creaHandlers('.boton-banco');

                } else {

                }
            },
            error: function (json) {
                console.log("Hubo un error al procesar los datos");
            }
        });
    }

    /**
     * Función que limpia todos los editores de text de la respuesta que se esta
     * eliminando, tambien quita todos los radios o checks seleccionados
     * @param {type} $_selector
     * @returns {undefined}
     */
    function limpiarCampos($_selector) {
        //tinymce.activeEditor.selection.setContent('<strong>Some contents</strong>');
        /*_selector.find('.mce-tinymce').each(function (){
         $(this).selection.setContent('');
         });*/
        var idSelector = $_selector.find('.t13respuesta').prop('id');
        var idSelectorBasico = $_selector.find('input.correcta').prop('id');
        var $itemToMove = $('.respuesta_style').find("#" + idSelector).parent().parent();
        //tinymce.get(idSelector).setContent('');
        tinymce.get(idSelector).setContent('');
        tinymce.get(idSelector).remove();
        if ($('#' + idSelectorBasico).prop('type') === 'text') {
            tinymce.get(idSelectorBasico).setContent('');
            tinymce.get(idSelectorBasico).remove();
        }
        $('.contenedor_respuestas').append($itemToMove);
        $_selector.find(':checkbox').removeAttr('checked').removeAttr('selected');
        $_selector.find(':radio').removeAttr('checked').removeAttr('selected');
    }

    function muestraNuevaRespuesta() {
        //$('.mce-tinymce.hide').first().removeClass('hide').attr('visibility', 'visible');
        //Vuelvo a crear todos los inputs para que sean editores
        tinymce.init(objEditorElemental);
        tinymce.init(objEditorBasico);
        formValidate();
        $('.respuesta_style.hide').first().removeClass('hide');
        //$('.correcta.hide').first().removeClass('hide');        
        //$('.boton-borrar.hide').first().removeClass('hide');
        //$(':text.hide').first().removeClass('hide');
    }
    /*
     function generaCodigoRespuesta(i,j,_strRespuesta){
     var _strResp = 
     '<div class="large-1 columns"> ' +
     '    <input type="radio" name="respuesta" value="'+j+'" id="respuesta_'+j+'" class="_objPregResp respuesta"> ' + 
     '</div> ' + 
     '<div class="large-11 columns"> ' +                             
     '        <div ' +
     '       id="t13respuesta_'+i+'" ' +
     '       class="_objPregResp t13respuesta edit contorno respuesta" >'+ _strRespuesta + '</div> ' + 
     '</div>';
     return _strResp;        
     }
     
     function creaNuevaRespuesta(){
     var _intTotalRespuestas = $('#contenedor-respuestas').find('.t13respuesta').length;
     $('#contenedor-respuestas').append( generaCodigoRespuesta( _intTotalRespuestas, _intTotalRespuestas + 1, '' ));    
     tinymce.init(jsonInit);
     }
     */
    function creaHandlers(_strBoton) {

        $(_strBoton).on('click', function () {
            if (_strBoton === '.boton-tipo') {
                id_tipo_pregunta = $(this).attr('value');
                id_pregunta = 0;
            }
            if (_strBoton === '.boton-banco') {
                id_pregunta = $(this).attr('value');
                //id_tipo_pregunta = 0;
            }

            obtenerRespuestas(id_tipo_pregunta, id_pregunta);

        });
    }

    function creaObjetoPregunta() {

        var _ProjJson = new Object();
        _ProjJson.pregunta = new Object();
        _ProjJson.respuestas = new Object();
        _ProjJson._isValid = true;
        var _esCorrecta;
        var i = 1;
        $('._objPregResp').each(function () {
            if ($(this).val() !== '1800') { // validar con plugin validate
                if (($(this).hasClass('t13respuesta') && !$(this).hasClass('hide'))) {
                    if (!$(this).parent().parent().hasClass('hide')) {
                        var _arrTmpResp = $(this).prop('id').split('_');
                        if (typeof _ProjJson.respuestas[_arrTmpResp[1]] === 'undefined') {
                            _ProjJson.respuestas[_arrTmpResp[1]] = new Object();
                        }
                        if (typeof _ProjJson.respuestas[_arrTmpResp[1]][_arrTmpResp[0]] === 'undefined') {
                            _ProjJson.respuestas[_arrTmpResp[1]][_arrTmpResp[0]] = new Object();
                        }
                        if (tinymce.get($(this).prop('id'))) {
                            _ProjJson.respuestas[_arrTmpResp[1]][_arrTmpResp[0]] = tinymce.get($(this).prop('id')).getContent();
                        } else {
                            _ProjJson.respuestas[_arrTmpResp[1]][_arrTmpResp[0]] = $(this).val();
                        }
                        /*
                         _ProjJson.respuestas[_arrTmpResp[1]] = 
                         $.extend( { 'id_respuesta'   : 0 },  
                         _ProjJson.respuestas[_arrTmpResp[1]]);
                         _ProjJson.respuestas[_arrTmpResp[1]] = 
                         $.extend( { 'id_respuesta_parent'   : 0 },  
                         _ProjJson.respuestas[_arrTmpResp[1]]);
                         _ProjJson.respuestas[_arrTmpResp[1]] = 
                         $.extend( { 'orden'   : 1 },  
                         _ProjJson.respuestas[_arrTmpResp[1]]);
                         _ProjJson.respuestas[_arrTmpResp[1]] = 
                         $.extend( { 'estatus'   : 1 },  
                         _ProjJson.respuestas[_arrTmpResp[1]]);
                         */
                        var intContRespuestas = parseInt(parseInt(_arrTmpResp[1]) + 1);
                        if ($('#correcta_' + intContRespuestas).attr('type') !== 'text') {
                            _esCorrecta = $('#correcta_' + intContRespuestas).is(':checked') ? 1 : 0;
                        } else {
                            _esCorrecta = tinymce.get('correcta_' + intContRespuestas).getContent();
                            //$('#correcta_' + intContRespuestas ).val();
                        }
                        _ProjJson.respuestas[_arrTmpResp[1]] =
                                $.extend({'correcta': _esCorrecta}, {'orden': i},
                                _ProjJson.respuestas[_arrTmpResp[1]]);
                        i++;
                    }
                } else {
                    if (!$(this).hasClass('correcta')) {

                        if (typeof _ProjJson.pregunta[$(this).attr('id')] === 'undefined') {
                            _ProjJson.pregunta[$(this).attr('id')] = new Object();
                        }
                        if ($(this).attr('id') === 'correcta_1') {
                            console.log($(this).attr('id'));
                        }
                        if (tinymce.get($(this).prop('id'))) {
                            _ProjJson.pregunta[$(this).attr('id')] = tinymce.get($(this).prop('id')).getContent();
                        } else {
                            _ProjJson.pregunta[$(this).attr('id')] = $(this).val();
                        }
                    }
                }
            } else {
                _ProjJson._isValid = false;
            }
        });
        /*
         _ProjJson.pregunta['c03id_tipo_pregunta'] = $('#c03id_tipo_pregunta').val();       
         _ProjJson.pregunta = $.extend( { 't11id_pregunta_parent' : 0 },  _ProjJson.pregunta);
         _ProjJson.pregunta = $.extend( { 't11id_pregunta'        : 0 },  _ProjJson.pregunta);
         _ProjJson.pregunta = $.extend( { 't11estatus'            : 1 },  _ProjJson.pregunta);
         */
        return _ProjJson;
    }


    $.div = '<div class="edit contorno respuesta">Escribe una respuesta...</div>';
    $('#add_resp').click(function () {
        $('#contenedor').append($.div);
        //tinymce.init(jsonInit);
        //console.log('It works!'); 
    });
    /*
     $('#remove_resp').click(function(){
     var numResp = $('#contenedor').find('div.respuesta').length;
     $.lastChild = $('#contenedor').children().last();
     
     if($.lastChild.hasClass('respuesta') && numResp > 1){
     $.lastChild .remove();
     }
     //tinymce.init(jsonInit);
     //console.log('It works!'); 
     });           
     */
    $('.instruccion').click(function () {
        //console.log( 'Texto: ' + $(this).html() ); 
        if ($(this).html() === 'Escribe las instrucciones...') {
            //$(this).html('_');
        }
    });

    $(".contenedor_respuestas").sortable({
        connectWith: "ul",
        opacity: 0.1,
        cursor: 'move',
        stop: function (event, ui)
        {
            tinymce.init(objEditorElemental);
            tinymce.init(objEditorBasico);
        },
        start: function (event, ui)
        {
            var idSelector = $(ui.item).find('.t13respuesta').prop('id');
            var idSelectorBasico = $(ui.item).find('input.correcta').prop('id');
            tinymce.get(idSelector).remove();
            if ($(ui.item).find('input.correcta').length) {
                if ($('#' + idSelectorBasico).prop('type') === 'text') {
                    tinymce.get(idSelectorBasico).remove();
                }
            }
        }
    });

    $('#boton-preview').on('click', function (e) {
        var x = screen.width / 2 - 1000 / 2;
        var y = screen.height / 2 - 900 / 2;
        console.log(window.location.href);
        console.log(document.URL);
        window.open('../vistaprevia/0', '_blank', 'height=900,width=1000,left=' + x + ',top=' + y);
    });

    /*
     objEditorPregunta = {
     selector: "#pregunta",
     menubar: false,
     statusbar: false,
     language : "es_MX",
     plugins: [
     "advlist autolink lists link image charmap print preview anchor",
     "searchreplace visualblocks code fullscreen",
     "insertdatetime media table contextmenu paste filemanager"
     ],
     toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media "
     };
     */

    objEditorElemental = {
        selector: ".t13respuesta,#pregunta,#instruccion",
        convert_urls: false,
        //menubar: true,
        statusbar: false,
        language: "es_MX",
        plugins: [
            "autolink autoresize charmap code contextmenu image link media paste preview table ",
            "visualblocks   ", //es necesario revisar si estos plugins en verdad son útiles
        ],
        media_scripts: [
            {filter: 'http://www.youtube.com'},
            {filter: 'http://www.ivoox.com/', width: 100, height: 200}
        ],
        
        toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | link image media | preview",
        setup: function (editor, val) {
            editor.on('keyup', function (event){
                var idActive   = tinyMCE.activeEditor.id;
                var textActive = tinyMCE.activeEditor.getContent();
                var inputActive = '#'+idActive;
                if(textActive.indexOf('<img src=') === -1){
                    $(inputActive).val($(textActive).text());
                    console.log($(inputActive).val());
                }else{
                    $(inputActive).val($(textActive));
                    console.log($(inputActive).val());
                }
                formValidate();
                //tinyMCE.triggerSave();
                //$('#' + editor.id).valid();
            });
        },
        target_list: false,
        target_list: [
            {title: 'Same page', value: '_self'},
            {title: 'New page', value: '_blank'},
            {title: 'Lightbox', value: '_lightbox'}
        ],

        allow_script_urls: true,
        media_filter_html: true,
        media_alt_source: false,
        media_poster: false,
        media_dimensions: false,
        menubar: "edit insert format tools",
        force_p_newlines: false,
        autoresize_max_height: 500,
        resize: "both",
        
        nonbreaking_force_tab: true
    };

    objEditorBasico = {
        selector: "input[type=text].correcta",
        convert_urls: false,
        menubar: false,
        statusbar: false,
        language: "es_MX",
        plugins: [
            "autolink autoresize contextmenu image link media paste preview table ",
            "visualblocks code insertdatetime charmap ", //es necesario revisar si estos plugins en verdad son útiles
        ],
        toolbar: "bold italic | link | alignleft aligncenter alignright| preview  ",
        //toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media "
        //actualiza la validacion cada que cambia el estado de tinymce
        setup: function (editor) {
            editor.on('keyup', function () {
                var idActive   = tinyMCE.activeEditor.id;
                var textActive = tinyMCE.activeEditor.getContent();
                var inputActive = '#'+idActive;
                $(inputActive).val($(textActive).text());
                console.log($(inputActive).val());
                //formValidate();
                $('#Pregunta').validate({
                   rules: {
                       correcta: {required: true}
                   } 
                });
                //tinyMCE.triggerSave();
                //$('#' + editor.id).valid();
            });
        },
        force_p_newlines: false,
        autoresize_max_height: 500,
        resize: "both"
    };

    //tinymce.init(objEditorPregunta);       
    tinymce.init(objEditorElemental);
    tinymce.init(objEditorBasico);
    formValidate();
    //----------------------------------
    var _objPregunta = creaObjetoPregunta();
    //mensajes de jquery.validate.js
    var msgSelectComboBox = 'Debes seleccionar una opcion';
    var msgRadioButton = 'Selecciona una respuesta';
    var msgMinlength5 = 'Mínimo 5 caracteres';
    var msgNumber = 'Sólo números';
    var cdnMinlength = 5;
    var msgMaxlength20 = 'Sólo se permiten 10000 caracteres';
    var cdnMaxlength = 600;

    //var contenidoTexto = $('#instruccion_ifr').contents().find('p').text(); //obtiene texto de tinymce
    //verifica que los campos del form pregunta no esten vacios y que tenga un maximo y minimo de caracteres
    
    function formValidate(){
        $('#Pregunta').validate({
            //ignore: '.hide :input, .estatus-inactivo, :hidden.correcta',
            ignore: $('.hide, .estatus-inactivo, input.correcta:hidden'),
            rules: {
                //id_tipo_pregunta    :{selectComboBox: true},
                pregunta: {required: true, minlength: cdnMinlength, maxlength: cdnMaxlength},
                //nombre_corto_clave  :{required: true, minlength:cdnMinlength, maxlength: cdnMaxlength},
                instruccion: {required: true, minlength: cdnMinlength, maxlength: cdnMaxlength},
                //tiempo_limite       :{required: true, minlength:cdnMinlength, maxlength: cdnMaxlength, number: true},
                respuesta: {required: true, maxlength: cdnMaxlength},
                correcta: {required: true, maxlength: cdnMaxlength}
                //categoria: {selectComboBox: true},
                //etiqueta: {selectComboBox: true}
                //id_numeracion           :{selectComboBox: true}
            },
            messages: {
                pregunta: {required: msgRequire, minlength: msgMinlength5, maxlength: msgMaxlength20},
                //nombre_corto_clave  :{required: msgRequire, minlength:msgMinlength5, maxlength: msgMaxlength20},
                instruccion: {required: msgRequire, minlength: msgMinlength5, maxlength: msgMaxlength20},
                //tiempo_limite       :{required: msgRequire, minlength:msgMinlength5, maxlength: msgMaxlength20, number:msgNumber},
                respuesta: {required: msgRequire, maxlength: msgMaxlength20},
                correcta: {required: msgRequire, maxlength: msgMaxlength20}
            }
        });
    }
    //valida que se selecciona alguna de las opciones del comboBox
       /* jQuery.validator.addMethod('selectComboBox',function (value,element){
           if(element.value === 'default'){
                return false;
           } else return true;
        },msgSelectComboBox);   

    validator.focusInvalid = function () {
        if (this.settings.focusInvalid) {
            try {
                var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
                if (toFocus.is("input[type=text]")) {
                    tinyMCE.get(toFocus.attr("id")).focus();
                } else {
                    toFocus.filter(":visible").focus();
                }
            } catch (e) {

            }
        }
    };*/

    //----------------------------------




    /*
     // Inicializo el tinymce para los distintos tipos de datos
     tinymce.init({
     selector: "textarea",
     theme: "modern",
     plugins: [
     "advlist autolink lists link image charmap print preview anchor",
     "searchreplace visualblocks code fullscreen",
     "insertdatetime media table contextmenu paste imgsurfer"
     ],
     // no existe plugin moxiemanager
     toolbar: " undo redo |  bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | imgsurfer",
     language : "es_MX",
     menubar: false
     
     });
     
     var jsonInit = {
     selector: "div.edit",
     theme: "modern",
     plugins: [
     ["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker"],
     ["searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking"],
     ["save table contextmenu directionality emoticons template paste imgsurfer"]
     ],
     add_unload_trigger: false,
     schema: "html5",
     inline: true,
     toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | imgsurfer",
     statusbar: false,
     menubar: false,
     language : "es_MX"
     };
     
     tinymce.init(jsonInit);
     
     tinymce.init({
     selector: "h3.edit",
     theme: "modern",
     add_unload_trigger: false,
     schema: "html5",
     inline: true,
     toolbar: "undo redo",
     statusbar: false,
     menubar: false,
     language : "es_MX"
     });
     */
    //carga la relacion de categorias que tiene almacenada la pregunta en la vista Editar Pregunta
    if(!$('#urlIdPregunta').val()){
        console.log('nueva pregunta');
        $('#textAreaCategoria').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: { collision: 'flip' }, // automatic menu position up/down
                source : $('#urlConsultaCat').val(),
                minLength: 0
            },
            forceLowercase: false,
            placeholder: 'Agregar categorias...'
        });	 
        $('#textAreaEtiqueta').tagEditor({
            autocomplete: {
                delay: 0, // show suggestions immediately
                position: { collision: 'flip' }, // automatic menu position up/down
                source : $('#urlConsultaTags').val(),
                minLength: 0
            },
            forceLowercase: false,
            placeholder: 'Agregar etiquetas...'
        });        
    }else{        
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $('#urlConsultaEditCat').val(),
            data: {
                'idPregunta': $('#urlIdPregunta').val()
            },
            success: function(json){
                var arr_cat =[];
                for(var a in json){
                    arr_cat.push(json[a]['c08nombre']);
                }
                $('#textAreaCategoria').tagEditor({
                    initialTags: arr_cat,
                    autocomplete: {
                        delay: 0,
                        position: {collision: 'flip'},
                        source: $('#urlConsultaCat').val(),
                        minLength: 0
                    },
                    forceLowercase: false,
                    placeholder: 'Agregar categorias...'
                });
            },
            error: function(){
                console.log('error en la peticion');
            }
        });
        //carga la relacion de etiquetas que tiene almacenada la pregunta en la vista Editar Pregunta
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: $('#urlConsultaEditEtiq').val(),
            data: {
                'idPregunta': $('#urlIdPregunta').val()
            },
            success: function(json){
                var arr_tags = [];
                for(var a in json){
                    arr_tags.push(json[a]['c09nombre']);
                }
                $('#textAreaEtiqueta').tagEditor({
                    initialTags: arr_tags,
                    autocomplete: {
                        delay: 0,
                        position: {collision: 'flip'},
                        source: $('#urlConsultaTags').val(),
                        minLength: 0
                    },
                    forceLowercase: false,
                    placeholder: 'Agregar etiquetas...'
                });
            },
            error: function(){
                console.log('error en la peticion');
            }
        });
    }
       
});

function creaObjetoRespuestas() {
    var _ProjJson = new Object();
    var objPregunta = new Object();
    var orden = 1;
    var cont = 0;
    var arrResp = [];
    var arrText;
    var idRespuesta;
    var respuesta;
    var correcta;
    var idSubPreg;
    var estatus;
    var idTipoPregunta = $('#id_tipo_pregunta').val();
    var arrPreg = new Object();
    arrPreg['id_pregunta'] = $('#id_pregunta').val();
    arrPreg['pregParent'] = $('#id_pregunta_parent').val();
    arrPreg['id_tipo_pregunta'] = $('#id_tipo_pregunta').val();
    arrPreg['pregunta'] = $('#pregunta').parent().find('iframe').contents().find('#tinymce').html();
    arrPreg['nombre_corto_clave'] = $('#nombre_corto_clave').val();
    arrPreg['instruccion'] = $('#instruccion').parent().find('iframe').contents().find('#tinymce').html();
    arrPreg['tiempo_limite'] = $('#tiempo_limite').val();
    arrPreg['columnas'] = $('#columnas').val();
    arrPreg['id_numeracion'] = $('#id_numeracion').val();
    //var fecha_actualiza    => date('Y/m/d g:i:s');
    arrPreg['estatus'] = $('#estatus').val();
    arrPreg['textAreaCategoria'] = $('#textAreaCategoria').tagEditor('getTags')[0].tags;
    arrPreg['textAreaEtiqueta'] = $('#textAreaEtiqueta').tagEditor('getTags')[0].tags;
    objPregunta['pregunta'] = arrPreg;
    $('.contenedor_respuestas').children().not('.hide').each(function () {
        if (this.id) {

            arrText = $(this).attr('id').split('_');
            idRespuesta = arrText[1];
            respuesta = $(this).find('.cont_respuesta').find('iframe').contents().find('#tinymce').html();//)children().eq(2).children().eq(1).val();
            if (idRespuesta == 'nuevo') {
                if (idTipoPregunta == 7 || idTipoPregunta == 8 || idTipoPregunta == 11 || idTipoPregunta == 12) {//se debe agregar el tipo 4 cuando sea remplazado por el 17
                    correcta = $('#' + arrText[0] + '_' + idRespuesta).children().eq(3).children().find('iframe').contents().find('#tinymce').html();
                } else {
                    correcta = ($('#' + arrText[0] + '_' + idRespuesta).children().eq(3).children().prop('checked') === false) ? 0 : 1;
                }
            } else {
                if (idTipoPregunta == 7 || idTipoPregunta == 8 || idTipoPregunta == 11 || idTipoPregunta == 12) {//se debe agregar el tipo 4 cuando sea remplazado por el 17
                    correcta = $('#respuesta_' + idRespuesta).children().eq(3).children().find('iframe').contents().find('#tinymce').html();
                } else {
                    correcta = ($('#respuesta_' + idRespuesta).children().eq(3).children().prop('checked') === false) ? 0 : 1;
                }
            }
            idSubPreg = $('#respuesta_' + idRespuesta).find('#idSubPreg').val();
            estatus = $('#estatus_' + idRespuesta).val() == null ? $('#estatus_' + idRespuesta + arrText[0]).val() : $('#estatus_' + idRespuesta).val();
            _ProjJson = new Object();
            _ProjJson.id = idRespuesta;
            _ProjJson.respuesta = respuesta;
            _ProjJson.orden = ($('#respuesta_' + idRespuesta).hasClass('estatus-inactivo')) ? 0 : orden;
            _ProjJson.correcta = correcta;
            _ProjJson.idSubPreg = idSubPreg;
            _ProjJson.estatus = estatus;
            arrResp[cont] = _ProjJson;

            orden = ($('#respuesta_' + idRespuesta).hasClass('estatus-inactivo') == true) ? orden : orden + 1;
            cont++;
        }

    });

    objPregunta['respuesta'] = arrResp;

    return objPregunta;
}

function cambia_estatus(idRespuesta) {
    if ($('#estatus_' + idRespuesta).val() == 1) {
        if ($('.contenedor_respuestas').children().not('.hide').not('.estatus-inactivo').length > 3 && $('#respuesta_' + idRespuesta + ' [name=correcta]').prop('checked') != true) {
            $('#estatus_' + idRespuesta).val(2);
            $('#respuesta_' + idRespuesta).addClass('estatus-inactivo');
            $('#respuesta_' + idRespuesta + ' [name=correcta]').attr('disabled', 'disabled');
            //$('#respuesta_'+idRespuesta).parent().parent().children().eq(1).children().eq(1).addClass('estatus-inactivo');
            $('#respuesta_' + idRespuesta + ' :button[name=borrar]').removeClass('fa-trash-o');
            $('#respuesta_' + idRespuesta + ' :button[name=borrar]').addClass('fa-undo');
            $('#respuesta_' + idRespuesta + ' :button[name=borrar]').removeClass('alert');
            $('#respuesta_' + idRespuesta + ' :button[name=borrar]').parent().attr('title', 'Restaurar respuesta')
            //$('#respuesta_'+idRespuesta).removeClass('estatus-activo');
        } else {
            $('#modalAlerta').foundation('reveal', 'open');
        }
    } else {
        $('#estatus_' + idRespuesta).val(1);
        $('#respuesta_' + idRespuesta).removeClass('estatus-inactivo');
        $('#respuesta_' + idRespuesta + ' [name=correcta]').removeAttr('disabled');
        //$('#respuesta_'+idRespuesta).parent().parent().children().eq(1).children().eq(1).removeClass('estatus-inactivo');
        $('#respuesta_' + idRespuesta + ' :button[name=borrar]').parent().attr('title', 'Eliminar respuesta')
        $('#respuesta_' + idRespuesta + ' :button[name=borrar]').removeClass('fa-undo');
        $('#respuesta_' + idRespuesta + ' :button[name=borrar]').addClass('fa-trash-o');
        $('#respuesta_' + idRespuesta + ' :button[name=borrar]').addClass('alert');
        //$('#respuesta_'+idRespuesta).addClass('estatus-activo');
    }
}

function cambia_estatus_nueva(idRespuesta) {
    if ($('#estatus_' + idRespuesta).val() == 1) {
        if ($('.contenedor_respuestas').children().not('.hide').not('.estatus-inactivo').length > 3 && $('#estatus_' + idRespuesta).parent().find('[name=correcta]').prop('checked') != true) {
            $('#estatus_' + idRespuesta).val(2);
            $('#estatus_' + idRespuesta).parent().addClass('estatus-inactivo');
            $('#estatus_' + idRespuesta).parent().children().eq(3).children().attr('disabled', 'disabled');
            $('#estatus_' + idRespuesta).parent().addClass('estatus-inactivo')
            $('#estatus_' + idRespuesta).parent().children().eq(4).children().removeClass('fa-trash-o');
            $('#estatus_' + idRespuesta).parent().children().eq(4).children().addClass('fa-undo');
            $('#estatus_' + idRespuesta).parent().children().eq(4).children().removeClass('alert');
            $('#estatus_' + idRespuesta).parent().children().eq(4).attr('title', 'Restaurar respuesta')
//                $('#'+idRespuesta).removeClass('estatus-activo');
        } else {
            $('#modalAlerta').foundation('reveal', 'open');
        }
    } else {
        $('#estatus_' + idRespuesta).val(1);
        $('#estatus_' + idRespuesta).parent().removeClass('estatus-inactivo');
        $('#estatus_' + idRespuesta).parent().children().eq(3).children().removeAttr('disabled');
        $('#estatus_' + idRespuesta).parent().removeClass('estatus-inactivo')
        $('#estatus_' + idRespuesta).parent().children().eq(4).attr('title', 'Eliminar respuesta')
        $('#estatus_' + idRespuesta).parent().children().eq(4).children().removeClass('fa-undo');
        $('#estatus_' + idRespuesta).parent().children().eq(4).children().addClass('fa-trash-o');
        $('#estatus_' + idRespuesta).parent().children().eq(4).children().addClass('alert');
        //$('#'+idRespuesta).addClass('estatus-activo');
    }

}

function cargaFormulario() {
    var idTipo = $('#id_tipo_pregunta').val();
    //console.log($('#' + idTipo));
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: $('#urlCambiaTipoPregunta').val(),
        data: {
            'idTipoPregunta': idTipo
        },
        beforeSend: function () {

        },
        success: function (json) {

            if (json) {
                //console.log(json);
                var elemResp = json['formResp'];
                for (var elem in elemResp) {
                    var attrElem = elemResp[elem];
                    for (var attr in attrElem) {
                        var valAttr = attrElem[attr];
                        if (valAttr !== null && typeof valAttr === "object") {
                            if (typeof valAttr.name !== 'undefined') {
                                // console.log(  valAttr.name  );
                                $('.' + valAttr.name).each(function () {
                                    if (valAttr.name === 'correcta') {
                                        $('.respuesta_style').each(function () {

                                            $(this).find('label').attr('class', valAttr.class).removeClass('_objPregResp').addClass('large-4 medium-4 small-10 cont_correcta');

                                        });
                                    }
                                    if (valAttr.name === 'respuesta') {
                                        $('.respuesta_style').each(function () {
                                            $(this).find('label').attr('class', valAttr.class)
                                                    .removeClass('_objPregResp')
                                                    .removeClass('t13respuesta')
                                                    .removeClass('respuesta')
                                                    .addClass('cont_texto_resp');
                                        });
                                    }

                                    $(this).get(0).type = valAttr.type;
                                    $(this).attr('placeholder', valAttr.placeholder);
                                });
                            }
                        }
                    }
                }

                $('#boton-nueva,.boton-borrar').removeClass('hide');
                tinymce.init(objEditorElemental);
                tinymce.init(objEditorBasico);
                //Marco los check que deben estar marcados
                $("input[type=checkbox]").each(function(){
                    if( typeof $(this).attr('checked') !== 'undefined'){
                        if($(this).attr('checked') === 'checked'){
                            $(this).prop('checked', true);
                        }
                    }
                });
            }
        },
        error: function (json) {
            console.log("Hubo un error al procesar los datos");
        }
    });
}

function muestraOcultaCamposRespuestas(intMinimoRespuestas, intMaximoRespuestas) {

    var iCorr = 0;
    var tipoPregunta = parseInt($('#id_tipo_pregunta').val());
    for (var i = 0; i < 20; i++) {
        iCorr = i + 1;
    }
    if (intMinimoRespuestas === intMaximoRespuestas) {
        $('#boton-nueva').addClass('hide');
        $('.boton-borrar').addClass('hide');
    } else {
        $('#boton-nueva').removeClass('hide');
    }
    // Agrego el caso de verdadero falso donde las respuestas no cambian
    if (tipoPregunta === 3) {
        tinymce.editors[2].setContent('Verdadero'); //.getBody().setAttribute('contenteditable', false);
        tinymce.editors[3].setContent('Falso'); //.getBody().setAttribute('contenteditable', false);

    } else {
        //$('.respuesta').val('').removeAttr('disabled');
    }

}

