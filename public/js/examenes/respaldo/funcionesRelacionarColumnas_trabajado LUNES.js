/*______         __              __                                  ______         __                                   
 |   __ \.-----.|  |.---.-.----.|__|.-----.-----.---.-.----.        |      |.-----.|  |.--.--.--------.-----.---.-.-----.
 |      <|  -__||  ||  _  |  __||  ||  _  |     |  _  |   _|        |   ---||  _  ||  ||  |  |        |     |  _  |__ --|
 |___|__||_____||__||___._|____||__||_____|__|__|___._|__|          |______||_____||__||_____|__|__|__|__|__|___._|_____|       
@author:           Grupo Educare S.A. de C.V.
@desarrolladores   Greg: gregorios@grupoeducare.com
                   Juan Pablo: jpgomez@grupoeducare.com
                   Juan José: jlopez@grupoeducare.com
@estilos css:      Claudia:  cgochoa@grupoeducare.com         
****************************************************************/
var cuantosPuntos = 6;
var radio = 12;//8
var espaciadoImagen = 70;
var area;
var tname;
var posXClic1 = 0;
var posYClic1 = 0;
var posXClic2 = 0;
var posYClic2 = 0;
var incremento = 0;
var lineaCreada;
var circuloCreadoIzquierda;
var circuloCreadoDerecha;
var textoPreguntaCreado;
var lineaSVGeliminar;
var objetoActual;
var valorXTextoPregunta = 10;
var color = "black";
var anchoStroke = "2";
var tipoPregunta;
var congelaBotonesDerechos = true;
var idBoton1;
var idLinea;
var arrRespuestasLineas = new Object();
//Variables temporales, sólo para pruebas
var preguntasArr = ["Es una técnica ideada por Kenneth Andrews y Roland Christensen hace más de 20 años más.","FODA se divide en:","Se remonta a la década de los años sesenta (60´s) del siglo pasado.","Está consituída por factores o elementos que forman parte de la misma organización","Encontramos las amenazas que son todas las variables negativas que afectan directa o indirectamente a la organización","Fortalezas que benefician a la organización y las debilidades."];
var respuestasArr = ["Análisis FODA","Fortalezas, Oportunidades, debilidades y amenazas","El Génesis del FODA","La situación interna","Ambiente externo","Ambiente interno"];
var posicionXmouse = 0;
var posicionYmouse = 0;
var areaScrollDerecha = 0;
var areaScrollSuperior = 0;
var direccionTrazado = "";
var desfaseIzquierda;
var desfaseSuperior;
var centroDeCirculo = 0;//Obtiene la posición centro de los puntos           
var arrTmpInit, arrTempFin;
var contornoCirculo = '#000000';
var rellenoCirculo = '#000000';
var evitarTriggerIzquierda = false;
var evitarTriggerDerecha = false;
var dispositivoMovil;
var conRectangulos = true;
var eventoPress;
var eventoRelease;
var eventoMove;
    
/***********************************************************
 F U N C I O N E S    R E L A CI O N A R    C O L U M N A S
***********************************************************/
function preguntaRelacionarColumnas(){
    dispositivoMovil = esMobile();
    eventoPress = dispositivoMovil ? "touchstart" : "mousedown";
    eventoRelease = dispositivoMovil ? "touchend" : "mouseup";
    eventoMove = dispositivoMovil ? "touchmove" : "mousemove";
        
    $('#centro').css('left', '0px');
    //$('#centro').css('width', '960px');
    $('#centro').css('width', '100%');    
    $('#centro').css('z-index', '3');
    $('#centro').css('position', 'absolute');
    
    //$('#elementosSVG').width( '100%' );
    
    //$("#centro").addClass("disabledbutton");
    $(document).unbind(eventoRelease);//Elimina el evento onmousemove 
        
    deshabilitaSeleccionDeTexto();
    cuantosPuntos = preguntasArr.length;
    
    arrTmpInit = new Array();
    arrTempFin = new Array();
    var alturaSVG;
    var arrRespuesta = new Array();
    var arrRespuestaDer = new Array();
    var textoRespuestaIzquierda;
    var textoRespuestaDerecha;
    var imagenRespuesta = 'null';
    var imagenRespuestaDer = 'null';
//  var font_size = 22;
//  var font_color = "#2689b2";
//  var font_text = "sans-serif";
// Muevo estas variables a la función ya que se tienen que inicializar los tamaños 
// en cada pregunta nueva
    var espaciado = 10;
    var espaciadoDer = 10;
    var valorXIzquierda = 0;
    var valorXDerecha = 0;
    var margen = 14;
    var areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    var imagenesCargadas = 0;
    centroDeCirculo = 0;
    terminoTrazo = false;
    
    for ( j=1, i=0; j<=cuantosPuntos; j++, i++ ) {
        // Lleno el array de respuestas usuario desde el inicio
        if(typeof arrRespuestasLineas[i] === 'undefined'){
            arrRespuestasLineas[i] = new Object();
        }
        if(typeof arrRespuestasLineas[i].id === 'undefined'){
            arrRespuestasLineas[i].id = 0;
        }
        if(typeof arrRespuestasLineas[i].selected === 'undefined'){
            arrRespuestasLineas[i].selected = 0;
        }
            
        //CREA TEXTOS DE LAS PREGUNTAS
        arrRespuesta = separaUrls(preguntasArr[j-1]);
        arrRespuestaDer = separaUrls(respuestasArr[j-1]);
        
        textoRespuestaIzquierda = htmlDecode(preguntasArr[j-1]); // Paso por el filtro decodificador
        textoRespuestaDerecha = htmlDecode(respuestasArr[j-1]); // Paso por el filtro decodificador
        imagenRespuesta = arrRespuesta[1];
        imagenRespuestaDer = arrRespuestaDer[1];
        if( imagenRespuesta ){//Si el reactivo contiene imágenes
//            var imagen = obtenerDimensionesImagen(imagenRespuesta[0], 50);
//            createSVGImage(imagenRespuesta[0], valorXIzquierda-254, (espaciadoImagen*j)-20, imagen.width, imagen.height);
//            valorXIzq = valorXIzquierda-100;
            textoRespuestaIzquierda = '<img id="'+(j-1)+'" src="'+imagenRespuesta+'">';                                    
        }                     
        if( imagenRespuestaDer ){//Si el reactivo contiene imágenes
//            var imagen = obtenerDimensionesImagen(imagenRespuesta[0], 50);
//            createSVGImage(imagenRespuesta[0], valorXIzquierda-254, (espaciadoImagen*j)-20, imagen.width, imagen.height);
//            valorXIzq = valorXIzquierda-100;
            textoRespuestaDerecha = '<img id="'+(j-1)+'" src="'+imagenRespuestaDer+'">';                                    
        }                     
        
        //textoPreguntaCreado = createSVGtext(textoRespuestaIzquierda, valorXIzq-90, (espaciado*j), font_size, font_color, "start", font_text);
        //document.getElementById(nombreSVG).appendChild(textoPreguntaCreado);

        //CREA TEXTOS DE LAS RESPUESTAS
        //textoPreguntaCreado = createSVGtext(textoRespuestaDerecha, valorXDerecha, (espaciado*j), font_size, font_color, "start", font_text);
        //document.getElementById(nombreSVG).appendChild(textoPreguntaCreado);
                
        $('#izquierda_' + (j-1)).html(textoRespuestaIzquierda);
        $('#derecha_' + (j-1)).html(textoRespuestaDerecha);
        valorXDerecha = $('#centro').width() -366;
        espaciado += $('#izquierda_' + (j-2)).height();                
        
        $('#'+(j-1)).load(function (e) {                                        
            imagenesCargadas++;
            if(imagenesCargadas === cuantosPuntos){//Si se cargaron todas las imágenes                        
                espaciadoGlobal = 10;
                for(var j=1;j<=cuantosPuntos;j++){                            
                    ajustaAlturasRelacionaLineas(j-1);                            
                }                
                reacomodaPuntosNegros();
            }           
        }).error(function () {
            console.log('Error al cargar la imagen');                    
        }).attr('src', imagenRespuesta);               
                         
        var anchoDivRespuestasIzquierda = $('#izquierda_' + (j-1)).css('width');                
        anchoDivRespuestasIzquierda = anchoDivRespuestasIzquierda.replace('px', '');
        anchoDivRespuestasIzquierda = parseInt(anchoDivRespuestasIzquierda) - 7;
                
        var numero1 = $('#izquierda_' + (j-1)).css('height');
        numero1 = parseInt( numero1.replace('px', '') );        
        var numero2 = $('#derecha_' + (j-1)).css('height');
        numero2 = parseInt( numero2.replace('px', '') );        
        var valores = [numero1, numero2];
        var maximo=Math.max.apply(null, valores);
        var altoDivRespuestasIzquierda = maximo;
        
        var xDivRespuestasIzquierda =  $('#izquierda_' + (j-1)).position().left + 20;
        var yDivRespuestasIzquierda =  $('#izquierda_' + (j-1)).position().top;
                
        rellenoCirculo = 'pink';
        var rectanguloCreadoIzquierda = document.createElementNS(svgNS,"rect");//Se declara la instancia de círculo
        rectanguloCreadoIzquierda.setAttributeNS(null,"id","ic_"+j);    
        rectanguloCreadoIzquierda.setAttributeNS(null,"width", anchoDivRespuestasIzquierda);
        rectanguloCreadoIzquierda.setAttributeNS(null,"height", altoDivRespuestasIzquierda);        
        rectanguloCreadoIzquierda.setAttributeNS(null,"x",xDivRespuestasIzquierda);
        rectanguloCreadoIzquierda.setAttributeNS(null,"y",yDivRespuestasIzquierda);                
        rectanguloCreadoIzquierda.setAttributeNS(null, 'style', 'fill: '+rellenoCirculo+'; ' );
        rectanguloCreadoIzquierda.setAttributeNS(null,"cursor","pointer");
        rectanguloCreadoIzquierda.setAttributeNS(null,"opacity","0.1");          
        $('#'+nombreSVG).append(rectanguloCreadoIzquierda);
        
        //CREA CIRCULOS DE LA IZQUIERDA
        var valorXIzquierda = anchoDivRespuestasIzquierda;              
        valorXIzquierda = parseInt(valorXIzquierda) + 56;
        circuloCreadoIzquierda = document.createElementNS(svgNS,"circle");//Se declara la instancia de círculo
        circuloCreadoIzquierda.setAttributeNS(null,"id","i_"+j);
        circuloCreadoIzquierda.setAttributeNS(null,"cx",valorXIzquierda);
        circuloCreadoIzquierda.setAttributeNS(null,"cy",(espaciado + ( margen * j ))); // Se aumenta el margen que se le agrego a cada opcion
        circuloCreadoIzquierda.setAttributeNS(null,"r",radio);
        circuloCreadoIzquierda.setAttributeNS(null,"fill","black");
        circuloCreadoIzquierda.setAttributeNS(null,"stroke","none");
        circuloCreadoIzquierda.setAttributeNS(null,"cursor","pointer");
                        
        circuloCreadoIzquierda.onmouseover=function(e){//Clic sobre cualquier circulo de la derecha
            if(direccionTrazado !== "" && direccionTrazado === "derecha-izquierda"){                
                colocaRecuadrosIzquierda(e);
            }
        }; 
        circuloCreadoIzquierda.onmouseout=function(e){//Clic sobre cualquier circulo de la derecha                        
            if(direccionTrazado === "derecha-izquierda"){                
                eliminarRecuadrosIzquierda(e);
            }
        };    
        document.getElementById(nombreSVG).appendChild(circuloCreadoIzquierda);
                
        $( '#i_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha
            e.preventDefault();
            evitarTriggerIzquierda = true;
            evitarTriggerDerecha = false;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'izquierda-derecha';
            }
            if(direccionTrazado === 'izquierda-derecha'){   
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{
                //no hace nada
                removerEventos();//Verificar si se aplicará
            }
        });        
        $( '#ic_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha
            e.preventDefault();
            evitarTriggerIzquierda = true;
            evitarTriggerDerecha = false;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'izquierda-derecha';
            }
            if(direccionTrazado === 'izquierda-derecha'){   
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{
                //no hace nada
                removerEventos();//Verificar si se aplicará
            }
        });
        $( '#i_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha
            e.preventDefault();
            if(evitarTriggerIzquierda === true){//Si es dispositivo móvil
                $('#'+id_temp).trigger(eventoRelease);
            }else{//Sino es dispositivo móvil
                if(direccionTrazado === ""){
                    direccionTrazado = 'izquierda-derecha';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    finTrazado(e,this.id);
                }else{
                    //no hace nada
                }
            }
        });
        $( '#ic_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha            
            e.preventDefault();
            if(evitarTriggerIzquierda === true){//Si es dispositivo móvil
                $('#'+id_temp).trigger(eventoRelease);
            }else{//Sino es dispositivo móvil
                if(direccionTrazado === ""){
                    direccionTrazado = 'izquierda-derecha';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    finTrazado(e,this.id);
                }else{
                    //no hace nada
                }
            }
        });        
        
        espaciadoDer += $('#derecha_' + (j-2)).height();        
        
        var anchoCentroVacio = $('#centroVacio').css('width');
        anchoCentroVacio = anchoCentroVacio.replace('px', '');
        anchoCentroVacio = parseInt(anchoCentroVacio);     
        valorXDerecha = valorXIzquierda + anchoCentroVacio;
        valorXDerecha = valorXDerecha - 10;                
        
        var anchoDivRespuestasDerecha = $('#derecha_' + (j-1)).css('width');
        
        
        var numero1 = $('#izquierda_' + (j-1)).css('height');
        numero1 = parseInt( numero1.replace('px', '') );        
        var numero2 = $('#derecha_' + (j-1)).css('height');
        numero2 = parseInt( numero2.replace('px', '') );        
        var valores = [numero1, numero2];
        var maximo=Math.max.apply(null, valores);
        var altoDivRespuestasDerecha = maximo;
        
        var xDivRespuestasDerecha =  valorXDerecha + 20;                        
        var yDivRespuestasDerecha =  $('#derecha_' + (j-1)).position().top;
                
        var rectanguloCreadoDerecha = document.createElementNS(svgNS,"rect");//Se declara la instancia de círculo
        rectanguloCreadoDerecha.setAttributeNS(null,"id","dc_"+j);
        rectanguloCreadoDerecha.setAttributeNS(null,"width",anchoDivRespuestasDerecha);
        rectanguloCreadoDerecha.setAttributeNS(null,"height", altoDivRespuestasDerecha);
        rectanguloCreadoDerecha.setAttributeNS(null,"x",xDivRespuestasDerecha);
        rectanguloCreadoDerecha.setAttributeNS(null,"y",yDivRespuestasDerecha);         
        rectanguloCreadoDerecha.setAttributeNS(null, 'style', 'fill: '+rellenoCirculo+'; ' );
        rectanguloCreadoDerecha.setAttributeNS(null,"cursor","pointer");
        rectanguloCreadoDerecha.setAttributeNS(null,"opacity","0.1");        
        $('#'+nombreSVG).append(rectanguloCreadoDerecha); 
        
        //CREA CIRCULOS DE LA DERECHA
        circuloCreadoDerecha = document.createElementNS(svgNS,"circle");//Se declara la intancia de círculo
        circuloCreadoDerecha.setAttributeNS(null,"id","d_"+j);
        circuloCreadoDerecha.setAttributeNS(null,"cx",valorXDerecha);
        circuloCreadoDerecha.setAttributeNS(null,"cy",(espaciadoDer + ( margen * j )) ); // Se aumenta el margen que se le agrego a cada opcion
        circuloCreadoDerecha.setAttributeNS(null,"r",radio);
        circuloCreadoDerecha.setAttributeNS(null,"fill","black");        
        circuloCreadoDerecha.setAttributeNS(null,"stroke","none");
        circuloCreadoDerecha.setAttributeNS(null,"cursor","pointer");        
        
        circuloCreadoDerecha.onmouseover=function(e){//Clic sobre cualquier circulo de la derecha                        
            if(direccionTrazado !== "" && direccionTrazado === "izquierda-derecha"){
                colocaRecuadrosDerecha(e);
            }  
        };
        circuloCreadoDerecha.onmouseout=function(e){//Clic sobre cualquier circulo de la derecha                                    
            if(direccionTrazado === "izquierda-derecha"){
                 eliminarRecuadrosDerecha(e);
            }
        };
        
        document.getElementById(nombreSVG).appendChild(circuloCreadoDerecha);
        
        $( '#d_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown cualquier circulo de la derecha
            e.preventDefault();
            evitarTriggerIzquierda = false;
            evitarTriggerDerecha = true;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'derecha-izquierda';
            }
            if(direccionTrazado === 'derecha-izquierda'){
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{                        
                removerEventos();                                 
            }             
        });
        $( '#dc_'+j ).bind(eventoPress,function(e){//touchstart ó mousedown cualquier circulo de la derecha
            e.preventDefault();
            evitarTriggerIzquierda = false;
            evitarTriggerDerecha = true;
            
            if(direccionTrazado === ""){
                direccionTrazado = 'derecha-izquierda';
            }
            if(direccionTrazado === 'derecha-izquierda'){
                inicioTrazado(e,this.id);
                eventosDePuntos(this.id);
            }else{                        
                removerEventos();                                 
            }             
        });
        $( '#d_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha       
            e.preventDefault();
            if(evitarTriggerDerecha === true){//Si es dispositivo móvil                
                $('#'+id_temp).trigger(eventoRelease); 
            }else{
                if(direccionTrazado === ""){
                    direccionTrazado = 'derecha-izquierda';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    //no hace nada
                }else{
                    finTrazado(event,this.id);
                }
            }            
        });  
        $( '#dc_'+j ).bind(eventoRelease,function(e){//touchstart ó mousedown sobre cualquier circulo de la derecha       
            e.preventDefault();
            if(evitarTriggerDerecha === true){//Si es dispositivo móvil                
                $('#'+id_temp).trigger(eventoRelease); 
            }else{
                if(direccionTrazado === ""){
                    direccionTrazado = 'derecha-izquierda';
                }
                if(direccionTrazado === 'derecha-izquierda'){
                    //no hace nada
                }else{
                    finTrazado(event,this.id);
                }
            }            
        });
        
        document.getElementById(nombreSVG).appendChild(circuloCreadoIzquierda);
        document.getElementById(nombreSVG).appendChild(circuloCreadoDerecha);
        if(cuantosPuntos === j){
            alturaSVG = circuloCreadoDerecha.cy.baseVal.value + 80;
        }

        ajustaAlturasRelacionaLineas(j-1);
                
        if( j === cuantosPuntos && imagenRespuestaDer === null && imagenRespuesta === null){//Si es la última vuelta y no existen imágenes.
            ajustaAltura();            
            pintaLineasResueltas();
        }
    }
}

function ajustaAltura (){     
     //espaciadoDer += $('#derecha_' + (j-2)).height();
    var _strTextoDivMayor = '';
    if($('#derecha').height() > $('#izquierda').height()){
        //$('#izquierda_' + (j-1)).height($('#derecha_' + (j-1)).height());
        _strTextoDivMayor = 'derecha';
    }else{
        //$('#derecha_' + (j-1)).height($('#izquierda_' + (j-1)).height());
        _strTextoDivMayor = 'izquierda';
    }
    $('#centro').height($('#' + _strTextoDivMayor).height() + 10);
    
    var anchura = $('#centro').width();
    anchura = anchura + 30;
    
    //$('#elementosSVG').width( anchura );
    $('#elementosSVG').width( '100%' );
    $('#elementosSVG').css('left', '-5px');
    $('#elementosSVG').css('position', 'absolute');    
    $('#respuestasRadialGroup').height( $('#centro').height());//$('#respuestasRadialGroup').height( alturaSVG  +  'px');
}
var espaciadoGlobal = 0;
var margen = 6;

function ajustaAlturasRelacionaLineas (id){
    if($('#derecha_' + (id)).height() > $('#izquierda_' + (id)).height()){
        $('#izquierda_' + (id)).height($('#derecha_' + (id)).height()  );
    }else{
        $('#derecha_' + (id)).height($('#izquierda_' + (id)).height()  );
    }
}

function reacomodaPuntosNegros (){
    var cir;
    /*for(var id=0;id<cuantosPuntos;id++){        
        if(id === 0){
             espaciadoGlobal = 40;             
        }else{
            espaciadoGlobal += $('#izquierda_' + (id-1)).height() + margen ;            
        }
        cir = document.getElementById( "i_"+(id+1) );
        cir.setAttributeNS(null, "cy", espaciadoGlobal );
        cir = document.getElementById( "d_"+(id+1) );
        cir.setAttributeNS(null, "cy", espaciadoGlobal );
    }*/
    
    for(var id=0;id<cuantosPuntos;id++){                
        var elemento = $('#izquierda_' + (id));
        var posicion = elemento.position();        
        
        cir = document.getElementById( "i_"+(id+1) );
        cir.setAttributeNS(null, "cy", posicion.top + 22 );
        cir = document.getElementById( "d_"+(id+1) );
        cir.setAttributeNS(null, "cy", posicion.top + 22 );
    }
    ajustaAltura();    
    pintaLineasResueltas();
}

function inicioTrazado (e,thisId){
    evitarPintadoCirculo = false;
    congelaSonidoIncorrecto = false;
    terminoTrazo = false;
    permiteOver = true;
    incremento = incremento + 1;//Checar si aplica se agregó para que funcione hacer varias lineas ya montado en el proyecto
    congelaBotonesDerechos = false;                                
    objetoActual = document.getElementById(e.target.id);    
       
    //arrRespuestasLineas[arrTmpInit[1] - 1] = arrTmpInit[1];
    //objetoActual.getAttribute('id');
    
    arrTmpInit = objetoActual.getAttribute('id').split('_');
    posXClic1 = obtenerDimensiones(objetoActual).x;
    posYClic1 = obtenerDimensiones(objetoActual).y;

    desfaseIzquierda = $("#elementosSVG").offset().left;
    desfaseSuperior = $("#elementosSVG").offset().top;
    areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    //centroDeCirculo = parseInt(e.target.getAttribute("r"));//Obtiene la posición centro de los puntos
    
    if(arrTmpInit[0] === 'ic' || arrTmpInit[0] === 'dc'){//Se da clic en los rectángulos  
        centroDeCirculo = 0;                        
        if(dispositivoMovil){//Si es un dispositivo móvil
            posXClic1 = e.originalEvent.touches[0].pageX;
            posYClic1 = e.originalEvent.touches[0].pageY;
            posXClic1 = posXClic1 - desfaseIzquierda;//Se ajustan estos datos por la diferencia de coordenadas entre el SVG de las líneas y el SVG de los rectángulos
            posYClic1 = posYClic1 - desfaseSuperior;
            //posYClic1 = posYClic1 + areaScroleada;
        }else{//Sino es un dispositivo móvil
            posXClic1 = e.pageX;
            posYClic1 = e.pageY;                        
            posXClic1 = posXClic1 - desfaseIzquierda;//Se ajustan estos datos por la diferencia de coordenadas entre el SVG de las líneas y el SVG de los rectángulos
            posYClic1 = posYClic1 - desfaseSuperior;                        
        }
    }else{//Si da click en los círculos        
        centroDeCirculo = parseInt(e.target.getAttribute("r"));//Obtiene la posición centro de los puntos
        posXClic1 = (posXClic1 - desfaseIzquierda) + centroDeCirculo;
        posYClic1 = (posYClic1 - desfaseSuperior) + centroDeCirculo;
        posYClic1 = posYClic1 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic        
    }            
    
    idBoton1 = thisId;
    
    if(direccionTrazado === 'izquierda-derecha'){
        colocaRecuadrosIzquierda(e);
    }else{
        colocaRecuadrosDerecha(e);
    }    
}
function finTrazado (e,thisId){
    terminoTrazo = true;
    if(congelaBotonesDerechos == false){
        congelaSonidoIncorrecto = true;
        objetoActual = document.getElementById(e.target.id);
        arrTmpFin = objetoActual.getAttribute('id').split('_');
        
        //arrRespuestasLineas[arrTmpInit[1] - 1]  = {'id': arrTmpInit[1], 'selected': arrTmpFin[1]};
        //.select = arrTmpFin[1]; 
        if(direccionTrazado === 'izquierda-derecha'){
            arrRespuestasLineas[arrTmpInit[1] - 1]  = {'id': arrTmpInit[1], 'selected': arrTmpFin[1]};
            actualizaArrayRespuesta(arrTmpInit[1] - 1, arrTmpFin[1]);
        }else{
            arrRespuestasLineas[arrTmpFin[1] - 1]  = {'id': arrTmpFin[1], 'selected': arrTmpInit[1]};
            actualizaArrayRespuesta(arrTmpFin[1]- 1, arrTmpInit[1] );
        }
        
        posXClic2 = obtenerDimensiones(objetoActual).x;
        posYClic2 = obtenerDimensiones(objetoActual).y;//Para crear la linea al liberar el mouse sobre circulo de la derecha
        var desfaseIzquierda = $("#elementosSVG").offset().left;
        var desfaseSuperior = $("#elementosSVG").offset().top;

        areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
        centroDeCirculo = parseInt(e.target.getAttribute("r"));//Obtiene la posición centro de los puntos
        
        if(arrTmpInit[0] === 'ic' || arrTmpInit[0] === 'dc'){//Se da clic en los rectángulos
            var cadena = thisId;
            cadena = cadena.substring(0,2);
            centroDeCirculo = 0;
            centroDeCirculo2 = 15;
            posXClic2 = e.pageX;
            posYClic2 = e.pageY;
            posYClic2 = posYClic2-desfaseSuperior;
            
            var thisIdGlobalTemp = idBoton1;
            if(arrTmpInit[0] === 'ic'){
                thisIdGlobalTemp = thisIdGlobalTemp.replace('ic', 'i');
            }
            if(arrTmpInit[0] === 'dc'){
                thisIdGlobalTemp = thisIdGlobalTemp.replace('dc', 'd');
            }
            posXClic1 = $( '#'+thisIdGlobalTemp ).position().left;
            posYClic1 = $( '#'+thisIdGlobalTemp ).position().top;
                        
            posXClic1 = (posXClic1-desfaseIzquierda) + centroDeCirculo;
            posYClic1 = (posYClic1-desfaseSuperior) + centroDeCirculo2;
            
            posXClic1 = posXClic1 + 10;//Se ajustan estos datos por la diferencia de coordenadas entre el SVG de las líneas y el SVG de los rectángulos
            posYClic1 = posYClic1 - 5;            
            //posYClic1 = posYClic1 + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
            obtienePosicionesCirculos(thisId);//nueva area rectangular            
            
            if(conRectangulos){
                /*var idContrario = thisIdGlobalTemp;
                idContrario = idContrario.replace('ic', 'd');
                idContrario = idContrario.replace('dc', 'i');
                thisId = thisId.replace('ic', 'i');
                thisId = thisId.replace('dc', 'd');
                
                posXClic2 = $( '#'+thisId ).position().left;
                posYClic2 = $( '#'+thisId ).position().top;
                posXClic2 = posXClic2 - desfaseIzquierda;
                posYClic2 = posYClic2 - desfaseSuperior;
                posYClic2 = posYClic2 + 6;
                
                posXClic1 = $( '#'+idContrario ).position().left;
                posXClic1 = posXClic1 - 28;
                 
                posYClic1 = $( '#'+idContrario ).position().top - desfaseSuperior;
                posYClic1 = posYClic1 + 6;*/
            }
        }else{//Si da click en los círculos
            var cadena = thisId;
            cadena = cadena.substring(0,2);
            obtienePosicionesCirculos(thisId);            
        }
        thisIdGlobal = thisId;//Analizar
        idBoton1Global = idBoton1;//Analizar
        
        crearLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke);
        idLineaGlobal = idLinea;//Analizar
        
        eliminarLineaAnterior("lineaCreada");
        incremento = incremento + 1;//Checar si se suma 1 o 2
        congelaBotonesDerechos = true;        
        
        thisId = thisId.replace('ic', 'i');
        thisId = thisId.replace('dc', 'd');        
        idBoton1 = idBoton1.replace('ic', 'i');
        idBoton1 = idBoton1.replace('dc', 'd');
        
        cambiaColorObjeto(thisId);
        cambiaColorObjeto(idBoton1);
        cambiaColorObjeto(idLinea);
        
        posColores = posColores + 1;
        if(posColores == colores_arr.length){
            posColores = 0;
        }
               
        if(direccionTrazado === 'izquierda-derecha'){        
            eliminarLineasAnteriores(posXClic1, posYClic1, posXClic2,posYClic2,  arrTmpInit[1]);
        }else{    
            eliminarLineasAnteriores(posXClic1, posYClic1, posXClic2,posYClic2, arrTmpInit[1]);
        }
        congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
        direccionTrazado = "";//Resetea el status de trazado
        eliminarRecuadros();
        permiteOver = false;         
    }
}
function obtienePosicionesCirculos (thisId){
    var cadena2;
    var data2;
    var data3;
    
    cadena2 = thisId;
    cadena2 = cadena2.replace('c', '');                            
    data2 = $( '#'+cadena2 ).attr('cx');
    data3 = $( '#'+cadena2 ).attr('cy');
        
    posXClic2 = parseInt(data2) + radio;
    
    if(direccionTrazado === 'izquierda-derecha'){
        posXClic2 = posXClic2 - 16;
    }else{
        posXClic2 = posXClic2 - 8;
    }        
    posYClic2 = (parseInt(data3) - radio);//posYClic2 = (parseInt(data3) - radio*2) + 4;
}
function eventosDePuntos (id){
    presionadoGlobal = id;
            
    $( 'body' ).bind(eventoMove,function(e){ //$(document).mousemove(function(event) {
        obtienePositionMouse(e, this);
    });
    $(window).scroll(function(event) {        
        if(areaScrollDerecha != $(document).scrollLeft()){
            posicionXmouse -= areaScrollDerecha;
            areaScrollDerecha = $(document).scrollLeft();
            posicionXmouse += areaScrollDerecha;
        }
        if(areaScrollSuperior != $(document).scrollTop()){
            posicionYmouse -= areaScrollSuperior;
            areaScrollSuperior = $(document).scrollTop();
            posicionYmouse += areaScrollSuperior;
        }                    
        procesaTrazado(desfaseIzquierda, desfaseSuperior, centroDeCirculo);
    });
    function obtienePositionMouse(e, _this){           
        if( dispositivoMovil ){//Si es un dispositivo Móvil            
            e.preventDefault();
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];                        
            var elm = $(_this).offset();
            var x = touch.pageX - elm.left;
            var y = touch.pageY - elm.top;

            /*if(x < $(_this).width() && x > 0){
                if(y < $(_this).height() && y > 0){
                    posicionXmouse = touch.pageX;
                    posicionYmouse = touch.pageY;
                }
            }*/ //Comentado porque afecta el trazado de lineas en tablets
            posicionXmouse = touch.pageX;
            posicionYmouse = touch.pageY;
        }else{//Sino es un dispositivo Móvil
            if (!e) { var e = window.event; }
            if (e.target) {    area = e.target;
            } else if (e.srcElement) {
                area = e.srcElement;
            }
            if (document.layers){
                posicionXmouse = e.x;            posicionYmouse = e.y;
            }else if (document.all){
                posicionXmouse = event.clientX;  posicionYmouse = event.clientY;
            }else if (document.getElementById){
                posicionXmouse = e.clientX;      posicionYmouse = e.clientY;
            }
        }
        
        areaScroleada = window.pageYOffset;
        posicionYmouse = posicionYmouse - areaScroleada ;//Ajuste de area scrolleada cuando mueve el mouse
        
        obtieneSegundoElemento(posicionXmouse, posicionYmouse);
        procesaTrazado(desfaseIzquierda, desfaseSuperior, centroDeCirculo);
    }
              
    $(document).on(eventoRelease,function(event){//document.onmouseup=function(e){
        eliminarLineaActual("lineaCreada");
        $(document).unbind('mousemove');//Elimina el evento onmousemove
        $( 'body' ).unbind(eventoMove);
        $(window).unbind('scroll');
                        
        congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
        direccionTrazado = "";//Resetea el status de trazado
        eliminarRecuadros();
        
        /*if(evitarPintadoCirculo === false){
            $ ( '#'+presionadoGlobal ).css('stroke', contornoCirculo);
            $ ( '#'+presionadoGlobal ).css('fill', rellenoCirculo);            
        }*/  //No aplica para la versión de certificaciones
        
        if(congelaSonidoIncorrecto === false){            
            //sndIncorrecto();//No aplica
            $(document).unbind(eventoRelease);
        }
    });
}
function obtieneSegundoElemento (){    
    if( dispositivoMovil ){
        var lado;
        var endTarget = document.elementFromPoint(
            posicionXmouse,
            posicionYmouse
        );
        id_temp = $(endTarget).attr('id');
        if(id_temp !== 'undefined'){
            if(direccionTrazado === 'izquierda-derecha'){
                lado = 'derecha';
            }else{
                lado = 'izquierda';
            }            
            if(typeof id_temp === 'undefined'){
                id_temp = "";
            }
            if(id_temp.indexOf('_') !== -1 ){
                var num = parseInt( id_temp.substring( id_temp.indexOf('_', 0)+1, id_temp.length  ) );
                num = num - 1;               
                $('#'+lado+'_'+num).addClass('pintaCuadro');//Pinta los puntos (lo que hace mouseOver)                
            }else{
                for(var d=0;d<preguntasArr.length;d++){
                    $('#'+lado+'_'+d).removeClass('pintaCuadro');//Pinta los puntos (lo que hace mouseOver)                
                }
            }
        }
    }
}
function removerEventos (){
    //circuloCreadoIzquierda.onmousedown = detenerEventos;
    document.onmousemove = detenerEventos;
    $(document).unbind('mousemove');//Elimina el evento onmousemove
    $(window).unbind('scroll');
    document.onmouseup = detenerEventos;
    congelaBotonesDerechos = false;//Resetea el estatus para que nunca congele los botones
    direccionTrazado = "";//Resetea el status de trazado
    eliminarRecuadros();
}
function procesaTrazado (desfaseIzquierda, desfaseSuperior, centroDeCirculo){        
    posXClic2 = posicionXmouse;
    posXClic2 = (posXClic2-desfaseIzquierda) - centroDeCirculo/4;
    posXClic2 = posXClic2 - 1;//Se resta un pixel para que el puntero pueda tocar el botón derecho y no obstruya la línea trazada                
    posYClic2 = posicionYmouse;
    posYClic2 = (posYClic2-desfaseSuperior) - centroDeCirculo/4;
    
    if(navegador === 'Firefox'){             
        if(versionInt <= 31){
            posXClic2 = posXClic2 + 2;
            posYClic2 = posYClic2 + 15;
        }
    } 
    //Se comenta para corregir el desfase cuando se escrolea
    //var areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    //posYClic2 = posYClic2 + areaScroleada;//Corrige el area de desfase del area escrolleada
    
    crearLinea("lineaCreada", posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke);                  
    eliminarLineaAnterior("lineaCreada");
}
function colocaRecuadrosIzquierda (e){
    var circuloActual = document.getElementById(e.target.id);
    var datosObtenidos_arr = circuloActual.getAttribute('id').split('_');
    var idObtenidoInt = ( parseInt(datosObtenidos_arr[1]) ) - 1;    
    $('#izquierda_'+idObtenidoInt).addClass('pintaCuadro');    
}
function colocaRecuadrosDerecha (e){
    var circuloActual = document.getElementById(e.target.id);
    var datosObtenidos_arr = circuloActual.getAttribute('id').split('_');
    var idObtenidoInt = ( parseInt(datosObtenidos_arr[1]) ) - 1;    
    $('#derecha_'+idObtenidoInt).addClass('pintaCuadro');
}
function eliminarRecuadros (){    
    for(var g=0;g<cuantosPuntos;g++){       
       $('#izquierda_'+g).removeClass('pintaCuadro');
       $('#derecha_'+g).removeClass('pintaCuadro');       
    }
}
function eliminarRecuadrosIzquierda (){    
    for(var g=0;g<cuantosPuntos;g++){
        $('#izquierda_'+g).removeClass('pintaCuadro');
    }
}
function eliminarRecuadrosDerecha (){    
    for(var g=0;g<cuantosPuntos;g++){
       $('#derecha_'+g).removeClass('pintaCuadro');
    }
}
function eliminarLineaActual(identificador){
    var d = document.getElementById(nombreSVG);    
    lineaSVGeliminar = document.getElementById(identificador+(incremento));//Asigna la linea actual para ser eliminada
    if(lineaSVGeliminar !== null){
        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
    }
}
function eliminarLineaAnterior(identificador){
    var d = document.getElementById(nombreSVG);    
    
    lineaSVGeliminar = document.getElementById(identificador+(incremento-1));//Asigna la linea anterior para ser eliminada
    
    try {
        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior        
    }
    catch(err) {//ENTRA AL CATCH');
        //document.getElementById("demo").innerHTML = err.message;
    }
            
    /*if(lineaSVGeliminar == null){        
        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
    }*/
}

/**
 * Funcion que elimina las lineas anteriores que existian en el mismo punto
 * @param {type} identificador
 * @param {type} posXClic1
 * @param {type} posYClic1
 * @returns {undefined}
 */
function eliminarLineasAnteriores(posXClic1, posYClic1, posXClic2,posYClic2, respPerm){     // Revisar a detalle    
    var d = document.getElementById(nombreSVG);    
    var $lineaActual = $('line').last();    
    
    $('line').each(function(){
        var limite = 15; // margen de error que le doy a las lineas 
        if(direccionTrazado === 'izquierda-derecha'){
            if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea        
                ((posXClic1 > parseInt($(this).attr('x1')) - limite && posXClic1 < parseInt($(this).attr('x1')) + limite)  && 
                (posYClic1 > parseInt($(this).attr('y1')) - limite && posYClic1 < parseInt($(this).attr('y1')) + limite) ||
                (posXClic2 > parseInt($(this).attr('x1')) - limite && posXClic2 < parseInt($(this).attr('x1')) + limite)  && 
                (posYClic2 > parseInt($(this).attr('y1')) - limite && posYClic2 < parseInt($(this).attr('y1')) + limite))        
                    && $(this).attr('id') !== $lineaActual.attr('id') ){                              
                    $(this).remove(); 
             }
             if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea
                ((posXClic2 > parseInt($(this).attr('x2')) - limite && posXClic2 < parseInt($(this).attr('x2'))+ limite)  && 
                (posYClic2 > parseInt($(this).attr('y2')) - limite && posYClic2 < parseInt($(this).attr('y2')) + limite) ||
                (posXClic1 > parseInt($(this).attr('x2')) - limite && posXClic1 < parseInt($(this).attr('x2'))+ limite)  && 
                (posYClic1 > parseInt($(this).attr('y2')) - limite && posYClic1 < parseInt($(this).attr('y2')) + limite)) 
                     && $(this).attr('id') !== $lineaActual.attr('id') ){     
                 $(this).remove();
             }
             //Elimino las lineas que se trazan en la misma columna
//             if(parseInt($(this).attr('x1')) > parseInt($(this).attr('x2')) - 5 && parseInt($(this).attr('x1')) > parseInt($(this).attr('x2')) + 5 ){
//                 $(this).remove();
//             }
        }else{
            if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea        
                ((posXClic1 > parseInt($(this).attr('x2')) - limite && posXClic1 < parseInt($(this).attr('x2')) + limite)  && 
                (posYClic1 > parseInt($(this).attr('y2')) - limite && posYClic1 < parseInt($(this).attr('y2')) + limite) ||
                ((posXClic2 > parseInt($(this).attr('x2')) - limite && posXClic2 < parseInt($(this).attr('x2')) + limite)  && 
                (posYClic2 > parseInt($(this).attr('y2')) - limite && posYClic2 < parseInt($(this).attr('y2')) + limite)))  
                 && $(this).attr('id') !== $lineaActual.attr('id') ){            
                 $(this).remove();
             }
             if( // Doy 10 pixeles de tolerancia donde se haya colocado la linea
                ((posXClic2 > parseInt($(this).attr('x1')) - limite && posXClic2 < parseInt($(this).attr('x1'))+ limite)  && 
                (posYClic2 > parseInt($(this).attr('y1')) - limite && posYClic2 < parseInt($(this).attr('y1')) + limite) ||
                (posXClic1 > parseInt($(this).attr('x1')) - limite && posXClic1 < parseInt($(this).attr('x1'))+ limite)  && 
                (posYClic1 > parseInt($(this).attr('y1')) - limite && posYClic1 < parseInt($(this).attr('y1')) + limite))
                 && $(this).attr('id') !== $lineaActual.attr('id') ){           
                 $(this).remove();
             }
             //Elimino las lineas que se trazan en la misma columna
//             if(parseInt($(this).attr('y1')) > parseInt($(this).attr('y2')) - 5 && parseInt($(this).attr('y1')) > parseInt($(this).attr('y2')) + 5 ){
//                 $(this).remove();
//             }
        }
                                        
        //var arrPosLinea[]
    });
    //lineaSVGeliminar = document.getElementById(identificador+(incremento-1));//Asigna la linea anterior para ser eliminada
//    if(lineaSVGeliminar !== null){
//        d.removeChild(lineaSVGeliminar);//Elimina la linea anterior
//    }
}

/**
 * Funcion que actuliza la informacion del array de arrRespuestasLineas,
 * esto se hace porque hay ocasiones donde se borran dos lineas y solo se 
 * actualiza una linea en el array
 * @param {type} indice
 * @param {type} posicionColocada
 * @returns {undefined}
 */
function actualizaArrayRespuesta(indice, posicionColocada){
    var arrResp = Object.keys(arrRespuestasLineas);
    var longResp = arrResp.length;
    for(var i=0; i<longResp; i++){
        if(parseInt(arrRespuestasLineas[i].selected) === parseInt(posicionColocada)
        && indice !== i){
            arrRespuestasLineas[i] = {'id': 0, 'selected': 0};
        }
    }
}
function crearLinea(identificador, posXClic1, posYClic1,posXClic2,posYClic2, color, anchoStroke){
    incremento++;
    lineaCreada = document.createElementNS(svgNS,"line");//Se declara la instancia de linea      
    
    lineaCreada.setAttributeNS(null,"id",identificador+incremento);
    lineaCreada.setAttributeNS(null,"x1",posXClic1+4);
    lineaCreada.setAttributeNS(null,"y1",posYClic1+4);
        
    if(terminoTrazo){                
       lineaCreada.setAttributeNS(null,"x2",posXClic2+5);//Posición final del trazado (posicion xy del mouse)
       lineaCreada.setAttributeNS(null,"y2",posYClic2+12);
    }else{
      var areaScroleada = window.pageYOffset;
      posYClic2 = posYClic2 + areaScroleada;
      
      if(direccionTrazado === 'izquierda-derecha'){         
        if( dispositivoMovil ){
            //posYClic2 = posYClic2 + areaScroleada;
            //var desfaseSuperior = $("#elementosSVG").offset().top;
            //posYClic2 = posYClic2 + desfaseSuperior;
          lineaCreada.setAttributeNS(null,"x2",posXClic2-6);//Posición final del trazado (posicion xy del mouse)
          lineaCreada.setAttributeNS(null,"y2",posYClic2-6);
        }else{//Si es en la PC           
            //posYClic2 = posYClic2 + areaScroleada;            
            lineaCreada.setAttributeNS(null,"x2",posXClic2-3);//Posición final del trazado (posicion xy del mouse)
            lineaCreada.setAttributeNS(null,"y2",posYClic2-3);
        }
      }else{
          if( dispositivoMovil ){
              //posYClic2 = posYClic2 + areaScroleada;
            lineaCreada.setAttributeNS(null,"x2",posXClic2+8);//Posición final del trazado (posicion xy del mouse)
            lineaCreada.setAttributeNS(null,"y2",posYClic2+8);
          }else{//Si es en la PC
            //posYClic2 = posYClic2 + areaScroleada;             
            lineaCreada.setAttributeNS(null,"x2",posXClic2+5);//Posición final del trazado (posicion xy del mouse)
            lineaCreada.setAttributeNS(null,"y2",posYClic2+5);
          }          
      }
    }
    
    lineaCreada.setAttributeNS(null,"stroke",color);
    lineaCreada.setAttributeNS(null,"stroke-width",anchoStroke);
    lineaCreada.setAttributeNS(null,"class", "linea_relacionar");
    idLinea = identificador+incremento;    
    document.getElementById(nombreSVG).appendChild(lineaCreada);//Crea una linea nueva    
}
function createSVGtext(etiqueta, x, y,tamano,relleno, alineacion, fuente) {    
    var textoPreguntaCreado = document.createElementNS(svgNS, 'text');
    var maximoCaracteresPorLinea = 35;
    var altoLinea = 20;
    var palabras = etiqueta.split(" ");
    var linea = "";

    textoPreguntaCreado.setAttributeNS(null, 'x', x);
    textoPreguntaCreado.setAttributeNS(null, 'y', y);
    textoPreguntaCreado.setAttributeNS(null, 'font-size', tamano);
    textoPreguntaCreado.setAttributeNS(null, 'fill', relleno);
    textoPreguntaCreado.setAttributeNS(null, 'text-anchor', alineacion);
    textoPreguntaCreado.setAttributeNS(null, 'font-family', fuente);
    textoPreguntaCreado.setAttributeNS(null, 'contentEditable', "true");
    
    for (var n = 0; n < palabras.length; n++) {
        var lineaPrueba = linea + palabras[n] + " ";
        if (lineaPrueba.length > maximoCaracteresPorLinea){
            var spanSVG = document.createElementNS(svgNS, 'tspan');
            spanSVG.setAttributeNS(null, 'x', x);
            spanSVG.setAttributeNS(null, 'y', y);
            var nodoTextoSpan = document.createTextNode(linea);
            spanSVG.appendChild(nodoTextoSpan);
            textoPreguntaCreado.appendChild(spanSVG);
            linea = palabras[n] + " ";
            y += altoLinea;
        }else {
            linea = lineaPrueba;
        }
    }    
    var spanSVG = document.createElementNS(svgNS, 'tspan');
    spanSVG.setAttributeNS(null, 'x', x);
    spanSVG.setAttributeNS(null, 'y', y);
    var nodoTextoSpan = document.createTextNode(linea);
    spanSVG.appendChild(nodoTextoSpan);
    textoPreguntaCreado.appendChild(spanSVG);
    return textoPreguntaCreado;
}
function createSVGImage(image, posX, posY, anchoImg, altoImg ){
    var img = document.createElementNS(svgNS,'image');
    img.setAttributeNS(null,'height',altoImg);
    img.setAttributeNS(null,'width',anchoImg);
    img.setAttributeNS('http://www.w3.org/1999/xlink','href', image); // 'http://grupoeducare.com/web/images/inicio/ldca.png'
    img.setAttributeNS(null,'x',posX);
    img.setAttributeNS(null,'y',posY);
    img.setAttributeNS(null, 'visibility', 'visible');
    document.getElementById(nombreSVG).appendChild(img);        
}
function separaUrls(cuerpo){ 
   var searchText = cuerpo;
   var strTexto = strip_tags(cuerpo);   
   var arrRespuesta = new Array();
    // urls will be an array of URL matches
    var urls = searchText.match(/\b(http|https)+(:\/\/)?(\S*)\.(\w{2,4})\b/ig);

    // you can then iterate through urls
//    if(urls !== null){
//        for (var i = 0, il = urls.length; i < il; i++) {
//            // do whatever with urls[i]
//            console.log(urls[i]);
//        }
//    }
    arrRespuesta[0] = sustraeTextoHTML(strTexto);
    arrRespuesta[1] = urls;
    
    return arrRespuesta;
}
function strip_tags(input, allowed) {
  //  discuss at: http://phpjs.org/functions/strip_tags/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Luke Godfrey
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //    input by: Pul
  //    input by: Alex
  //    input by: Marc Palau
  //    input by: Brett Zamir (http://brett-zamir.me)
  //    input by: Bobby Drake
  //    input by: Evertjan Garretsen
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Eric Nagel
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Tomasz Wesolowski
  //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
  //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
  //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
  //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
  //   returns 2: '<p>Kevin van Zonneveld</p>'
  //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
  //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
  //   example 4: strip_tags('1 < 5 5 > 1');
  //   returns 4: '1 < 5 5 > 1'
  //   example 5: strip_tags('1 <br/> 1');
  //   returns 5: '1  1'
  //   example 6: strip_tags('1 <br/> 1', '<br>');
  //   returns 6: '1 <br/> 1'
  //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
  //   returns 7: '1 <br/> 1'

  allowed = (((allowed || '') + '')
    .toLowerCase()
    .match(/<[a-z][a-z0-9]*>/g) || [])
    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return input.replace(commentsAndPhpTags, '')
    .replace(tags, function($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}
function obtenerDimensionesImagen(url, ancho){
    var img = new Image();
    var nuevasDimensiones = new Object();
    var anchoDefault = ancho;
    img.src = url;
    var resolucion = img.width / anchoDefault;
    nuevasDimensiones.width = anchoDefault;
    nuevasDimensiones.height = img.height / resolucion;
    return nuevasDimensiones;
}
function sustraeTextoHTML(strTexto){
    return strTexto.replace("&nbsp", " ");
}