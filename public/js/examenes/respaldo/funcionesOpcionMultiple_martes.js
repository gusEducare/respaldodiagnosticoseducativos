/*   ______                _                                  
    |  ____|              (_)                                 
    | |__ _   _ _ __   ___ _  ___  _ __   ___  ___   
    |  __| | | | '_ \ / __| |/ _ \| '_ \ / _ \/ __|   
    | |  | |_| | | | | (__| | (_) | | | |  __/\__ \ 
    |_|   \__,_|_| |_|\___|_|\___/|_| |_|\___||___/ 
   ____             _   __              __  __   __  _ _   _       _      
  / __ \           (_) /_/             |  \/  | /_/ | | | (_)     | |     
 | |  | |_ __   ___ _  ___  _ __       | \  / |_   _| | |_ _ _ __ | | ___ 
 | |  | | '_ \ / __| |/ _ \| '_ \      | |\/| | | | | | __| | '_ \| |/ _ \
 | |__| | |_) | (__| | (_) | | | |     | |  | | |_| | | |_| | |_) | |  __/
  \____/| .__/ \___|_|\___/|_| |_|     |_|  |_|\__,_|_|\__|_| .__/|_|\___|
        | |                                                 | |           
        |_|                                                 |_|          
@proyecto:         NUEVA PLATAFORMA DE EVALUACIONES 2015
@programa:         Funciones de Opción Múltiple (código fuente)
@author:           Grupo Educare S.A. de C.V.
@desarrollador     gregorios@grupoeducare.com
@fecha de inicio:  01 de Noviembre DE 2014
@fecha de entrega: Febrero 2015                               -->
**********************************************************
   V A R I A B L E S     O P C I Ó N     M Ú L T I P L E
*********************************************************/
var textoBoton;
var botonAnterior = "0";
var respuestaAnterior;
var cuantosBotonesDeRespuestaLocal;
var botonesSeleccionados_arr = new Array();
var arrayValores = new Array();
var arrayValores2 = new Array();
var restriccionCaracteres;
var pintarUltimaCaja = true;
var vueltasRespuesta = 1;
var imagenesCargadas = 0;
var respuestaTemporal;
var pocisionDeEspacioGlobal;
var ceroInicialUtilizado = 0;
var sueltaEnDrop = false;
var myInterval;
var elementoParaContenedor;
var posTempX;
var posTempY;
var textoTemp;

/***********************************************************
  F U N C I O N E S     O P C I Ó N      M Ú L T I P L E
***********************************************************/
function prepararDivs(cuantosBotonesDeRespuesta){
    $("#respuestasRadialGroup").empty();
    $("#respuestasRadialGroup").addClass('large-12');
    $("#respuestasRadialGroup").addClass('small-12');
    $("#respuestasRadialGroup").addClass('columns');
    $("#respuestasRadialGroup").addClass('respuesta');
    
    var estructuraBotonesCompleta = "";
    var estructuraBotones = "";
    var estructuraBotones2 = "";
    var estructuraBotones3 = "";
    var acumulativoEstilos;
    var inciso;
    imagenesCargadas = 0;
    cuantosBotonesDeRespuestaLocal = cuantosBotonesDeRespuesta;
        
    if(c03id_tipo_pregunta === "10" || c03id_tipo_pregunta === "17"){    numeracion = "3";  }
    if(numeracion === "1"){ acumulativoEstilos = 0;   restriccionCaracteres = /[A-Za-z]/g;      incisos_arr = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];}
    if(numeracion === "2"){ acumulativoEstilos = 100; restriccionCaracteres = /[A-Za-z]/g;      incisos_arr = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","a","t","u","v","w","x","y","z"];}
    if(numeracion === "3"){ acumulativoEstilos = 200; restriccionCaracteres = /[1234567890]/g;  incisos_arr = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26"];}
    if(numeracion === "4"){ acumulativoEstilos = 300; restriccionCaracteres = /[IVX]/g;         incisos_arr = ["I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII","XIII","XIV","XV","XVI","XVII","XVIII","XIX","XX","XXI","XXIII","XXIV","XXV","XXVI"];}
    
    if(preguntasArr.length > respuestasArr.length){
        pintarUltimaCaja = false;
    }else{
        pintarUltimaCaja = true;
    }
    if(contieneRespuestas){
        for (var j=1; j<=cuantosBotonesDeRespuesta; j++) {
            if(numEstilo === "1"){//Múltiple con Botones RadialGroup
                //estructuraBotones = estructuraBotones + '<input type="radio" class="respuestas" name="respuesta" value="'+j+'" id="respuesta_'+j+'"><label id="respuesta'+j+'_btn" for="respuesta_'+j+'" class="respuesta"></label></br>';
                 estructuraBotones = estructuraBotones + '<li><input type="radio" id="r'+j+'" name="respuesta" class="respuestas" value="'+j+'"></input><label id="respuesta'+j+'_btn" for="r'+j+'"></label></li>';
            }
            if(numEstilo === "5"){//Múltiple con Botones CheckBox
                //estructuraBotones = estructuraBotones + '<input type="checkbox" class="respuestas" name="respuesta" value="'+j+'" id="respuesta_'+j+'"><label id="respuesta'+j+'_btn" for="respuesta_'+j+'" class="respuesta"></label></br>';
                estructuraBotones = estructuraBotones + '<li><input id="c'+j+'" name="c1" type="checkbox" class="respuestas" value="'+j+'"></input><label id="respuesta'+j+'_btn" for="c'+j+'"></label></li>';                
            }
            if(numEstilo === "2"){//Múltiple y Respuesta Múltiple con Botones Personalizados
                estructuraBotones = estructuraBotones + '<p><a id="respuesta'+j+'_btn" href="#" class="btn_preg btn_preg'+(acumulativoEstilos+j)+'" onClick="marcarSeleccionado('+j+')" onmouseOut="mouseOut('+j+')" value="'+j+'"> </a></p>\n\n';
            }
            if(numEstilo === "3"){//Relacionar Columnas con cajas 
                estructuraBotones = estructuraBotones + '<p><a id="respuesta'+j+'_btn" class="text-center pregunta columnas_personalizadas columnas_personalizadas'+(acumulativoEstilos+j)+'" ></a></p>\n\n';
                estructuraBotones2 = estructuraBotones2 + '<p> <input type="text" class="textbox textboxGral bordeSemiredondeado" maxlength="1" onkeypress="return restrictCharacters(this,event,restriccionCaracteres) " id="'+j+'" onkeyup="convierte(this)" ><a id="respuesta'+(j+cuantosBotonesDeRespuesta)+'_btn" class="pregunta columnas_personalizadasDerecha" ></a></p>\n\n';
            }
            if(numEstilo === "7"){//Cortas Horizontal
                // aqui se tiene que agregar el spacer <div class="spacer60"></div> cuando la pregunta sea de orientacion vertical
                // En el momento que se pinte el dom, se tiene que ejecutar la siguiente operacion $('.texto-respuesta p').each(function(){$(this).width(170);});
                // donde 170 es el tamaño de la caja mas grande, eso se puede obtener con $('.texto-respuesta p').each(function(){console.log($(this).width());});
                // ver si esto se puede reaplicar en otros tipos, como lo es arrastra corta.
                estructuraBotones = estructuraBotones +'<span id="respuesta'+j+'_btn" class="left texto-respuesta"></span><input type="text" id="'+j+'" class="textboxGral texto-cortas">';
                //esta seria la vertical con muchos textos
                //estructuraBotones = estructuraBotones +'<span style="float:left" >Has añadido contenido nuevo...</span><input type="text" id="'+j+'" class="textbox2">';
            }
            if(numEstilo === "8"){//Opción Falso Verdadero
                if(j<=2){
                    estructuraBotones = estructuraBotones + '<p><button id="respuesta'+j+'_btn">boton</button></p>';
                }
            }
            if(numEstilo === "9"){//Ordenar con Números
                estructuraBotones = estructuraBotones + '<p><a id="respuesta'+j+'_btn" class="columnas_personalizadasOrdenarNumeros text-center" ></a></p>\n\n';
                estructuraBotones2 = estructuraBotones2 + '<div class="large-12 columns"><span style="float:left;margin-left:3em;" > </span><input type="text" maxlength="1" onkeypress="return restrictCharacters(this,event,restriccionCaracteres) " id="'+j+'" onkeyup="convierte(this)" class="cajaRelacionarNumeros textboxGral bordeRedondeado text-center"></a></div>';
            }
            if(numEstilo === "10"){//Relacionar Líneas
                estructuraBotones = estructuraBotones + '<div class="preguntaRelaciona text-right" id="izquierda_'+(j-1)+'"></div>';
                estructuraBotones2 = estructuraBotones2 + '<div class="preguntaRelaciona" id="derecha_'+(j-1)+'"></div>';
            }
            if(numEstilo === "11"){//Arrastra Ordenar
                estructuraBotones = estructuraBotones + '<li class="sortable-item button success" id="'+(j-1)+'" value="'+j+'"><span id="respuesta'+j+'_btn"></span></li>';
            }
            if(numEstilo === "14"){//Arrastra Contenedor
                estructuraBotones = estructuraBotones + '<li class="ui-widget-content" id="li'+j+'"><span id="respuesta'+j+'_btn" class="resp_arra_cont combos"></span></li>';
            }
            if(numEstilo === "16"){//Matriz RadialGroup
                //estructuraBotones = estructuraBotones + '<input type="radio" name="respuesta" value="'+j+'" ><label id="respuesta'+j+'_btn" for="respuesta_'+j+'" class="respuesta"></label></br>';
                //estructuraBotones2 = estructuraBotones2 + '<li><input type="radio" id="r'+j+'" name="respuesta" value="'+j+'"></input><label id="respuesta'+j+'_btn" for="respuesta_'+j+'"></label></li>';
                estructuraBotones = estructuraBotones + '<li><input type="radio" id="r'+j+'" name="radialGroup" value="'+j+'" ><label id="respuesta'+j+'_btn" for="respuesta_'+j+'" class="radio-inline"></label></li>';
                estructuraBotones2 = estructuraBotones2 + '<li><input type="radio" id="r'+j+'" name="radialGroup" value="'+j+'" ><label id="respuesta'+j+'_btn" for="respuesta_'+j+'" class="radio-inline"></label></li>';
            }
        }
        if(numEstilo === "10"){//Relacionar Líneas
            estructuraBotones3 = estructuraBotones3 + '<svg id="elementosSVG" height="100%" xmlns=svgNS xmlns:xlink=svgNSLink></svg>';
        }
    }else{
        if(numEstilo === "4"){//Abierta con una caja autoexpandible
            estructuraBotones = '<textarea id="txtCrecer" rows="4" cols="10" class="textboxLargo textboxGral"></textarea><span id="referenciaCrecer" style="display:none;" >  </span>';
        }
        if(numEstilo === "6"){//Relaciona Combo
            estructuraBotones = estructuraBotones +   '<p><select id="combo1" class="comboBox" onchange="elementoSeleccionado1(this.value);"><option value="0">Selecciona una opcion</option></select></p>';
            estructuraBotones2 = estructuraBotones2 + '<p><select id="combo2" class="comboBox comboBoxDerecha" onchange="elementoSeleccionado2(this.value);"><option value="0">Selecciona una opcion</option></select></p>';
        }
        if(numEstilo === "10"){//Relacionar Líneas
            estructuraBotones = estructuraBotones + '<svg id="elementosSVG" width="100%" height="100%" xmlns=svgNS xmlns:xlink=svgNSLink></svg>';
        }
        if(numEstilo === "12"){//Cortas Numérica
            estructuraBotones = estructuraBotones +'<input type="text" id="'+j+'" style="width: 200px !important;float:left;height: 40px;" onkeypress="return restrictCharacters(this,event,restriccionCaracteres) " id="'+j+'" onkeyup="convierte(this)">';
        }
        if(numEstilo === "13"){//Arrastrar Cortas
            estructuraBotones = estructuraBotones + '';
        }
        if(numEstilo === "15"){//Rompecabezas
            estructuraBotones = estructuraBotones +'<div id="recuadro"><div id="panelJuego"><div><ul id="sortable" class="sortable"></ul><div id="cuadroImagenActual"><img id="imagenActual"></div></div></div><div id="juegoTerminado" style="display:none;"><div>Terminaste de armar el rompecabezas.</div></div></div>';
        }
        if(numEstilo === "17"){//Sopa de Letras
            estructuraBotones = estructuraBotones +'<div id="gridSopadeletras"></div>';
        }
        if(numEstilo === "18"){//Relaciona Arrastrar
            estructuraBotones = estructuraBotones + '';
        }
    }
    if(c03id_tipo_pregunta === "7" || c03id_tipo_pregunta === "8"){
        $("#respuestasRadialGroup").removeClass('respuesta').addClass('estilo_resp');   //  clase nueva para pregunta 6 Jon
    }else{
        $("#respuestasRadialGroup").removeClass('estilo_resp').addClass('respuesta');
    }
    if(c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "3"){//Si es Opción Múltiple con Radios Personalizados ó Falso Verdadero con Radios Personalizados
        estructuraBotones = '<section><form class="ac-custom ac-radio ac-fill" autocomplete="off"><ul>' + estructuraBotones + '</ul></form></section>';
    }
    if(c03id_tipo_pregunta === "2"){////Si es Respuesta Múltiple con Checkboxs Personalizados
        estructuraBotones = '<section><form class="ac-custom ac-checkbox ac-boxfill" autocomplete="off"><ul>' + estructuraBotones + '</ul></form><section>';
    }
    if(c03id_tipo_pregunta === "13"){//Si es Matriz con Radios Personalizados
        estructuraBotones = '<section><form class="ac-custom ac-radio ac-fill formularios" autocomplete="off" ><ul>' + estructuraBotones + '</ul></form></section>';
        estructuraBotones2 ='<section><form class="ac-custom ac-radio ac-fill formularios" autocomplete="off" ><ul>' + estructuraBotones2 + '</ul></form></section>';
    }
    if(c03id_tipo_pregunta === "8"){//Si es Arrastra Cortas
        estructuraBotones = estructuraBotones + '<div id="content" class="large-12 medium-12 small-12 columns"><div id="pilaDeCartas" class="large-12 medium-12 small-12 columns"> </div><div id="pilaDeCartasExtra" class="large-12 medium-12 small-12 columns"> </div><div id="espaciosDeCartas" class="large-12 medium-12 small-12 columns"> </div></div>';
        $('#fondoPregunta').height( $('#content').height());
    }else{
       // $('#fondoPregunta').height( $('form').height());
    }
    if(c03id_tipo_pregunta === "4"){//Si es Relaciona Arrastrar
        estructuraBotones = estructuraBotones + '<div id="content" class="large-12 medium-12 small-12 columns"><div id="pilaDeCartas" class="large-12 medium-12 small-12 columns"></div><div id="pilaDeCartasExtra" class="large-12 medium-12 small-12 columns"></div><div id="pilaDeCartasTemporal" class="large-12 medium-12 small-12 columns"></div>';
        $('#fondoPregunta').height( $('#content').height());
    }
    if(c03id_tipo_pregunta === "9"){//Si es Arrastra Ordenar
        estructuraBotones = '<div class="large-2 medium-2 columns" style="visibility:hidden;">.</div><div class="dhe-example-section large-8 medium-8 columns" id="ex-1-1"><div class="dhe-example-section-content"><div id="example-1-1"><ul class="sortable-list">' + estructuraBotones + '</ul></div></div></div><div class="large-2 medium-2 columns" style="visibility:hidden;">.</div>';
    }
    if(c03id_tipo_pregunta === "5"){//Si es Arrastra Contenedor
        estructuraBotones = '<div ><ul id="tarjetas" class="tarjetas ui-helper-reset ui-helper-clearfix">'+estructuraBotones+'</ul></div><div id="contenedor" class="combo-uno"></div>';
    }
    if(c03id_tipo_pregunta === "12" || c03id_tipo_pregunta === "11" || c03id_tipo_pregunta === "10" || c03id_tipo_pregunta === "4" || c03id_tipo_pregunta === "18" || c03id_tipo_pregunta === "13"){    
        if(c03id_tipo_pregunta === "13"){
            var strEncabezados = '';
            for(var j=0; j<Object.keys(obj.respuestas).length; j++){
                strEncabezados += '<span class="color">'+obj.respuestas[j].t13respuesta+'</span>'
            }
            $("#respuestasRadialGroup").append('<div class="contenedor-renglon large-12 medium-12 columns"><div id="izquierdaInicial" class="large-6 medium-6 columns"></div><div id="derechaInicial" class="large-6 medium-6 columns">'+strEncabezados+'</div></div>');
           for(var h=0;h<preguntasArr.length;h++){
               //$("#respuestasRadialGroup").append('<div id="izquierda'+h+'" style="height: 40px;"></div><div id="derecha'+h+'" style="height: 50px;"></div>');//Esta linea dió problemas al recargar por segunda vez
               $("#respuestasRadialGroup").append('<div class="contenedor-renglon large-12 medium-12 columns"><div id="izquierda'+h+'"></div><div id="derecha'+h+'"></div></div>');
               $("#izquierda"+h).addClass("izquierdaTexto");
               $("#izquierda"+h).addClass("large-6");
               $("#izquierda"+h).addClass("medium-6");
               $("#izquierda"+h).addClass("columns");
               preguntasArr[h] = eliminaParrafos(preguntasArr[h]);
               $("#izquierda"+h).html(preguntasArr[h]);
               
               if(h === 0){
                   estructuraBotones2 = estructuraBotones2.replace(/radialGroup/gi, 'radialGroup'+h);
                   $("#derecha"+h).append(estructuraBotones2);  
               }else{                   
                   estructuraBotones = estructuraBotones.replace(/radialGroup/gi, 'radialGroup'+h);                  
                   $("#derecha"+h).append(estructuraBotones);
               }
               $("#derecha"+h).addClass("large-6");
               $("#derecha"+h).addClass("medium-6");
               $("#derecha"+h).addClass("columns");
               $("#derecha"+h).addClass("contenedor-derecha");
               radialMain();               
            }
        }else if(c03id_tipo_pregunta === "12" || c03id_tipo_pregunta === "11" ){
            var _strIzquierda   = '<div id="izquierda" onmousedown="return false" class="large-5 medium-5 small-5 columns"></div>';
            var _strCentro      = '<div id="centro" class="large-2 medium-2 small-2 columns"></div>';
            var _strCentroVacio      = '<div id="centroVacio" class="large-2 medium-2 small-2 columns"></div>';
            var _strDerecha     = '<div id="derecha" onmousedown="return false" class="large-5 medium-5 small-5 columns"></div>';
            var _strHtml        = _strCentro + _strIzquierda + _strCentroVacio + _strDerecha;
            $("#respuestasRadialGroup").html(_strHtml);
            $("#izquierda").html(estructuraBotones);
            $("#derecha").html(estructuraBotones2);
            $("#centro").html(estructuraBotones3);
            $("#centroVacio").html(estructuraBotones3);
        }else{            
            $("#respuestasRadialGroup").html('<div id="izquierda"></div><div id="derecha"></div>');
            $("#izquierda").html(estructuraBotones);
            $("#derecha").html(estructuraBotones2);
        }
    }else{
        $("#respuestasRadialGroup").html(estructuraBotones);
        if(c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "2" || c03id_tipo_pregunta === "3"){            
            radialMain();
        }
    }
}
function eliminaParrafos(sinParrafo){
//    sinParrafo = sinParrafo.replace("<p>", "");//Se elimina la etiqueta del párrafo
//    sinParrafo = sinParrafo.replace("</p>", "");//
//    respuestasArr[j] = sinParrafo;
    return sinParrafo;
}

function crearDivs(){
    respuestasArr[j] = eliminaParrafos(respuestasArr[j]);
    if(c03id_tipo_pregunta === "13"){//Si es una pregunta de tipo Matriz
        respuestasArr[j] = "";//No llevan textos las respuestas
        $('li').addClass('listas');//Colocar horizontalmente los radios
        /*$(".ac-custom label").css('margin-left', '0rem');//Espaciado entre Circulos naranjas (radios personalizados) y posicion de radio original
        $(".ac-custom label").css('margin-top', '0rem');//Espaciado entre Circulos naranjas (radios personalizados) y posicion de radio original
        $(".ac-custom label").css('left', '-45px');//Posición X de los Circulos naranjas (radios personalizados)        
        $(".ac-custom input").css('left', '-45px');//Posición X de los Circulos naranjas (radios personalizados)  
        $(".ac-custom svg").css('margin-left', '-54px');//Posición X del svg (raya verde)
        $(".ac-custom svg").css('margin-top', '-12px');//Posición Y del svg (raya verde)                
        $("section").css('margin', '0px -150px -30px 0px');*/
                
//        $(".ac-custom label").css('margin-left', '-40px');//Espaciado entre Circulos naranjas (radios personalizados) y posicion de radio original
//        $(".ac-custom label").css('margin-top', 'auto');//Espaciado entre Circulos naranjas (radios personalizados) y posicion de radio original
//        $(".ac-custom label").css('left', '-4px');//Posición X de los Circulos naranjas (radios personalizados)        
//        $(".ac-custom input").css('left', '-40px');//Posición X de los Circulos naranjas (radios personalizados)  
        $(".ac-custom svg").css('margin-left', '-54px');//Posición X del svg (raya verde)
        $(".ac-custom svg").css('margin-top', '-12px');//Posición Y del svg (raya verde)                
        //$("section").css('margin', '0px -150px -30px 0px');
        $(".ac-custom label").addClass('ajusteLabel'); // Estilos para label
        $(".ac-custom input").addClass('ajusteInput').addClass('respuestas'); // Estilos para input
        $(".ac-custom input").addClass('pregunta_matriz').addClass('pregunta_' + (j+1)); 
        //$(".ac-custom svg").addClass('ajusteSvg'); // Estilos para svg
    }
    if(numEstilo==="1" || numEstilo==="5" ){
        var sinParrafo = respuestasArr[j];
        sinParrafo = sinParrafo.replace("<p>", "");//Se elimina la etiqueta del párrafo
        sinParrafo = sinParrafo.replace("</p>", "");//
        respuestasArr[j] = sinParrafo;        
    }
    if(numEstilo==="2" || numEstilo==="3" || numEstilo==="8" || numEstilo==="7" || numEstilo==="9"  || numEstilo==="14" || numEstilo==="16"){ // Revisar estilo 14 y 16
        inciso = "";
    }else{
        inciso = "";
        //inciso = incisos_arr[j]+". ";
    }
    if(c03id_tipo_pregunta === "7"){//Si es Pregunta Corta Horizontal.
        t13respuesta = preguntasArr[j]+"\u00a0\u00a0";
        if(primeraVezEspacios === false){
           t13respuesta = "\u00a0\u00a0"+preguntasArr[j]+"\u00a0\u00a0";
        }
        primeraVezEspacios = false;
    }
    if(c03id_tipo_pregunta === "3"){//Si es verdadero o falso
        if(j===0){    t13respuesta = "Verdadero";    }
        if(j===1){    t13respuesta = "Falso";        }
    }
    if(c03id_tipo_pregunta === "11" || c03id_tipo_pregunta === "10" || c03id_tipo_pregunta === "4"){//Si es de Relacionar Columnas con cajitas ó relaciona Combo tomas los valores del arreglo temporal
       t13respuesta = preguntasArr[j];
       //$('#respuesta'+((j+1)+cuantosBotonesDeRespuestaLocal)+'_btn').html(incisos_arr[j]+". "+respuestasArr[j]);              
       $('#respuesta'+((j+1)+cuantosBotonesDeRespuestaLocal)+'_btn').html(respuestasArr[j]);              
       if(c03id_tipo_pregunta === "18"){
            for(d=0;d<preguntasArr.length;d++){
                arrayValores[d]= new Array(preguntasArr[d]);
                arrayValores2[d]= new Array(respuestasArr[d]);
            }
            var list = $('#combo1')[0];
            $.each(arrayValores, function(index, text) {
                list.options[list.options.length] = new Option(arrayValores[index], index, text);
            });
            list = $('#combo2')[0];
            $.each(arrayValores2, function(index, text) {
                list.options[list.options.length] = new Option(arrayValores2[index], index, text);
            });
        }
    }
        
    if(c03id_tipo_pregunta !== "7" && c03id_tipo_pregunta !== "11"){                        
        respuestaTemporal = respuestasArr[j];        
        
        if(typeof respuestaTemporal !== 'undefined'){//Sólo aplica si respuestaTemporal es diferente de undefined
            respuestaTemporal = respuestaTemporal.replace('<img ','<img id="imagen'+(j+1)+'" ');
            respuestasArr[j] = respuestaTemporal;   
        }
               
        $('#respuesta'+(j+1)+'_btn').html(inciso+respuestasArr[j]);        
        
        var idImagen = '#imagen'+(j+1);        
        var imagenRespuesta = $(idImagen).attr('src');                
        $('#imagen'+(j+1)).load(function (e) {                     
            imagenesCargadas++;            
            if(imagenesCargadas === respuestasArr.length){//Si se cargaron todas las imágenes
                redimensionaAlturaDivPrincipal();//Se emplea para volver a ajustar la altura del contenedor principal (respuestasRadialGroup) una vez cargadas todas las imágenes del rectivo
            }
        }).error(function () {
            //Error al cargar la imagen
        }).attr('src', imagenRespuesta);
    }else{
        $('#respuesta'+(j+1)+'_btn').html(inciso+preguntasArr[j]);
    }
    if(c03id_tipo_pregunta === "7" && c03id_tipo_pregunta === "11"){
        $('#respuesta'+(j+1)+'_btn').find('img').addClass('tam-imagen-relacionar');
    }
    //$('#respuesta'+(j+1)+'_btn').children().children().attr('id', 'imagen_'+ (j+1)).addClass('f-dropdown').attr('aria-hidden', 'true').attr('tabindex', '-1').attr('data-dropdown-content', '');
//    $('#respuesta'+(j+1)+'_btn').prev().attr('id', 'respuesta_' + (j+1)); // Revisar comportamiento
//    $('#respuesta'+(j+1)+'_btn').prev().attr('class', 'respuestas');      // Revisar comportamiento
    $("#fondoPregunta").css('background-image', '');
    if(c03id_tipo_pregunta === "12"){    preguntaRelacionarColumnas();  }
    if(c03id_tipo_pregunta === "9") {    preparaSorteable();    }//Arrastra Ordenar
    if(c03id_tipo_pregunta === "8") {    creaArrastraCortas();    }//Arrastra Cortas
    if(c03id_tipo_pregunta === "4") {    creaRelacionaArrastrar();}//Relaciona Arrastrar
    if(c03id_tipo_pregunta === "14"){ // Caso para las preguntas de Rompecabezas
        rompecabezasMain();
    }
    if(c03id_tipo_pregunta === "16"){// Caso para las preguntas de Sopa de letras        
        leeInformacionBds();
    } 
    if(c03id_tipo_pregunta === "5") {//Arrastra Contenedor
        $( '#contenedor' ).css('min-height', '67.26666px');
        $( '#tarjetas' ).css('min-height', '20px');
        $( '#revertir_btn' ).hide();
        
        $(function() {
            var $tarjetas = $( "#tarjetas" ),
            $contenedor = $( "#contenedor" );
                        
            //Para hacer dragueables los elementos que se llevan al contenedor
            $( "li", $tarjetas ).draggable({revert: true,containment: "document",cursor: "move"}); //Para hacer dragueables los elementos que se llevan al contenedor
            
            $contenedor.droppable({//Para que el contenedor sea dropable y acepte las tarjetas
                activeClass: "ui-state-highlight",
                drop: function( event, ui ) {
                    borrarTarjeta( ui.draggable );
                }
            });
            $tarjetas.droppable({//Permite que las tarjetas sean dropables aceptando elementos del contenedor
                accept: "#contenedor li",
                activeClass: "custom-state-active",
                drop: function( event, ui ) {
                    creaImagenEnContenedor( ui.draggable,ui );
                    var pos = ui.draggable.offset(), dPos = $(this).offset();
                },
            });
            function borrarTarjeta( $elemento ) {//Borrar imagen
                console.log('borrarTarjeta');
                $( '#revertir_btn' ).show(0);
                
                elementoParaContenedor = $elemento.attr('id');
                textoTemp = $( '#'+elementoParaContenedor + ' span').text();
                
                $elemento.fadeOut(function() {                    
                    var $lista = $( "ul", $contenedor ).length ?                    
                    $( "ul", $contenedor ) :
                    $( "<ul class='tarjetas ui-helper-reset' />" ).appendTo( $contenedor );
                    
                    $lista.attr('id', 'listaContenedor');
                                        
                    $elemento.css({left:'left top', top:'left top', position:'relative'});//Reubicar la caja izquierda verde
                    $elemento.appendTo( $lista ).fadeIn(function() {
                    });//Aparece la tarjeta con efecto fade-in
                });
            }
            crearClon = function( elemento ) {
                $('#tarjetas').append('<li id="clon" class="ui-widget-content ui-draggable ui-draggable-handle" ><span id="respuesta8_btn" class="resp_arra_cont combos"><p>'+textoTemp+'</p></span></li>');
                
                var f = $( '#'+elementoParaContenedor ).position().left;
                f = (f - $( '#'+elemento ).position().left ) * -1;
                posTempX = f;

                var renglonesMaximo = 12;//Máximo número de renglones que puede tener <tarjetas>
                var altoTarjetasTemp;
                var altoContenedorTemp;
                var lineasTarjetas;
                var lineasContenedor;
                        
                for(var g=1;g<=renglonesMaximo;g++){                            
                    altoContenedorTemp = 67*g +.26666;
                    console.log('altoContenedorTemp '+altoContenedorTemp);
                    if( $('#contenedor').height() === altoContenedorTemp ){                                
                        lineasContenedor = g;
                    }
                }                
                for(var t=1;t<=renglonesMaximo;t++){
                    altoTarjetasTemp = t*67;
                    if( $('#tarjetas').height() === altoTarjetasTemp ){
                        lineasTarjetas = t;
                        console.log('el clon se formó en la '+lineasTarjetas+' linea de <tarjetas>. El <contenedor> es de '+lineasContenedor+' lineas');
                        posTempY = calculaRevertEnY(lineasTarjetas,lineasContenedor);
                    }
                }
                    
                /*posTempY = calculaRevertEnY(1,1);//(47 * 1)  - (67*1 - 0) = -20  ---> El 0 se obtiene de (0+0*10)     //'-20px';
                posTempY = calculaRevertEnY(2,1);//(47 * 2) - (67*1 - 20) = 47   ---> El 20 se obtiene de (1+1*10)    //'47px';
                posTempY = calculaRevertEnY(3,1);//(47 *3) - (67*1 - 40) = 114   ---> El 40 se obtiene de (2+2*10)    //'114px';
                posTempY = calculaRevertEnY(4,1);//(47 *4) - (67*1 - 60) = 181  ---> El 60 se obtiene de (3+3*10)     //181px*/                                
                console.log('posTempY '+posTempY);
                $( '#clon' ).remove();//Se elimina el clon
            }
                        
            moverTarjeta = function( $elemento ) {                                
                $elemento.appendTo( $tarjetas ).show(0);                                
                $elemento.css('left', '');
                $elemento.css('top', '');                                
            }            
            creaImagenEnContenedor = function( $elemento ) {                
                $elemento.fadeOut(function() {
                    $elemento
                    .appendTo( $tarjetas )
                    .show(0)
                });
            }
            $( "ul.tarjetas > li" ).click(function( event ) {//Resuelve los iconos
                var $elemento = $( this ),
                $area = $( event.area );
                if ( $area.is( "a.ui-icon-contenedor" ) ) {
                  borrarTarjeta( $elemento );
                }
                return false;
            });
        });
    }
}
function revertir() {
    $( '#revertir_btn' ).hide();
    crearClon('clon');//Crea el clon
    $( '#'+elementoParaContenedor ).animate({//Regresala a la parte superior
        left: posTempX,
        top: posTempY,
        position: "relative"
    }, 800, function() {//Tiempo de la animación //animación completada
        moverTarjeta( $('#'+elementoParaContenedor).draggable() );        
    });
}
function calculaRevertEnY(cuantasLineasTarjetas, cuantasLineasContenedor) {
    var valor1;
    var valor2;
    var valor3;
    var valor4;
                    
    for(var t=0;t<cuantasLineasTarjetas;t++){
        valor1 = 47 * (t+1);
        valor2 = (67*cuantasLineasContenedor) - ( (t+t)*10 );
        valor3 = valor1 - valor2;
        valor4 = valor3 + 'px';
    }
    return valor4;
}

function marcarSeleccionado(elemento) {//Marca el boton azul seleccionado
    desmarcaTodos();
    if(c03id_tipo_pregunta === "1"){
        $('#respuesta'+elemento+'_btn').addClass('btn_preg_press');
    }else{
        var posicion = botonesSeleccionados_arr.indexOf(elemento);
        if(posicion === -1){//Si no lo encuentra es que no está seleccionado el botón
            $('#respuesta'+elemento+'_btn').addClass('btn_preg_press');
            botonesSeleccionados_arr.push(elemento);
            }else{//si ya está seleccionado el boton
            $('#respuesta'+elemento+'_btn').removeClass('btn_preg_press');
            $('#respuesta'+elemento+'_btn').addClass('btn_preg');
            delete botonesSeleccionados_arr[posicion];
        }
    }
    $('#respuesta'+elemento+'_btn').prev().attr('id', 'respuesta' + elemento+'_btn');
    $('#respuesta'+elemento+'_btn').prev().attr('class', 'respuestas');
}
function desmarcaTodos () {//desmarca todos los botones azules
    if(c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "3"){
        for (var j=1; j<=cuantosBotonesDeRespuestaLocal; j++) {
            $('#respuesta'+j+'_btn').removeClass('btn_preg_press');
            $('#respuesta'+j+'_btn').addClass('btn_preg');
        }
    }
}
function elementoSeleccionado1(value){
    var valor=$("#combo1").val();
    var texto=$("#combo1 option:selected").text();
    alert("Esta seleccionado el valor "+valor+": "+texto);
}
function elementoSeleccionado2(value){
    var valor=$("#combo2").val();
    var texto=$("#combo2 option:selected").text();
    alert("Esta seleccionado el valor "+valor+": "+texto);
}
function convierte(e){
    if(numeracion === "1"){
        e.value=e.value.toUpperCase();
    }
    if(numeracion === "2"){
        e.value=e.value.toLowerCase();
    }
    if(numeracion === "4"){
        e.value=e.value.toUpperCase();
    }
}
function mouseOut(elemento){
    $('#respuesta'+elemento+'_btn').css("color", "white");
}
function restrictCharacters(myfield, e, restrictionType) {
    if (!e) var e = window.event
    if (e.keyCode) code = e.keyCode;
    else if (e.which) code = e.which;
    var character = String.fromCharCode(code);
    if (code==27) { this.blur(); return false; }//Si el usuario presiona ESC se quita el foco
    if (!e.ctrlKey && code!=9 && code!=8 && code!=36 && code!=37 && code!=38 && (code!=39 || (code==39 && character=="'")) && code!=40) {//Para ignorar si el usuario presiona otras teclas
        if (character.match(restrictionType)) {
       	    return true;
        } else {
            return false;
        }
    }
}
function marcaOpcionSeleccionada(t11usuario){
    var j, i;
    if(numEstilo === "1" || numEstilo === "5"){
        for(j=0; j<t11usuario.length; j++){
            //$('#respuesta_'+t11usuario[j]).attr('checked', true);
            $('#r'+t11usuario[j]).trigger('click');
            $('#c'+t11usuario[j]).trigger('click');
//            if(c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "3"){//Si es Opción Múltiple con Radios Personalizados ó Falso Verdadero con Radios Personalizados
//                $('#idSVG'+t11usuario[j]).html('<path style="stroke-dasharray: 499.664, 499.664; stroke-dashoffset: 0; transition: stroke-dashoffset 0.8s ease-in-out 0s;" d="M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809"></path>');
//            }
//            if(c03id_tipo_pregunta === "2"){//Si es Respuesta Múltiple con Radios Personalizados
//                $('#idSVG'+t11usuario[j]).html('<path style="stroke-dasharray: 1009.6, 1009.6; stroke-dashoffset: 0; transition: stroke-dashoffset 0.8s ease-in 0s;" d="M6.987,4.774c15.308,2.213,30.731,1.398,46.101,1.398 c9.74,0,19.484,0.084,29.225,0.001c2.152-0.018,4.358-0.626,6.229,1.201c-5.443,1.284-10.857,2.58-16.398,2.524 c-9.586-0.096-18.983,2.331-28.597,2.326c-7.43-0.003-14.988-0.423-22.364,1.041c-4.099,0.811-7.216,3.958-10.759,6.81 c8.981-0.104,17.952,1.972,26.97,1.94c8.365-0.029,16.557-1.168,24.872-1.847c2.436-0.2,24.209-4.854,24.632,2.223 c-14.265,5.396-29.483,0.959-43.871,0.525c-12.163-0.368-24.866,2.739-36.677,6.863c14.93,4.236,30.265,2.061,45.365,2.425 c7.82,0.187,15.486,1.928,23.337,1.903c2.602-0.008,6.644-0.984,9,0.468c-2.584,1.794-8.164,0.984-10.809,1.165 c-13.329,0.899-26.632,2.315-39.939,3.953c-6.761,0.834-13.413,0.95-20.204,0.938c-1.429-0.001-2.938-0.155-4.142,0.436 c5.065,4.68,15.128,2.853,20.742,2.904c11.342,0.104,22.689-0.081,34.035-0.081c9.067,0,20.104-2.412,29.014,0.643 c-4.061,4.239-12.383,3.389-17.056,4.292c-11.054,2.132-21.575,5.041-32.725,5.289c-5.591,0.124-11.278,1.001-16.824,2.088 c-4.515,0.885-9.461,0.823-13.881,2.301c2.302,3.186,7.315,2.59,10.13,2.694c15.753,0.588,31.413-0.231,47.097-2.172 c7.904-0.979,15.06,1.748,22.549,4.877c-12.278,4.992-25.996,4.737-38.58,5.989c-8.467,0.839-16.773,1.041-25.267,0.984 c-4.727-0.031-10.214-0.851-14.782,1.551c12.157,4.923,26.295,2.283,38.739,2.182c7.176-0.06,14.323,1.151,21.326,3.07 c-2.391,2.98-7.512,3.388-10.368,4.143c-8.208,2.165-16.487,3.686-24.71,5.709c-6.854,1.685-13.604,3.616-20.507,4.714 c-1.707,0.273-3.337,0.483-4.923,1.366c2.023,0.749,3.73,0.558,5.95,0.597c9.749,0.165,19.555,0.31,29.304-0.027 c15.334-0.528,30.422-4.721,45.782-4.653"></path>');
//            }
        }
    }
    if(numEstilo === "16"){
        for(j=0; j<t11usuario.length; j++){
            $('#derecha'+j+' #r'+t11usuario[j]).trigger('click');
        }
    }
    if(numEstilo === "2"){
        for(j=0; j<t11usuario.length; j++){
            $('#respuesta'+t11usuario[j]+'_btn').addClass('btn_preg_press');
        }
    }
    if(numEstilo === "3"){
        for(j=0, i=1; j<t11usuario.length; j++, i++){
            $('#'+i).val(t11usuario[j]);
        }
    }

    if(numEstilo === "4"){ // Caso para las preguntas abiertas
        for(j=0; j<t11usuario.length; j++, i++){
            $('#txtCrecer').val(t11usuario[j]);
        }
    }
    if(numEstilo === "7" || numEstilo === "9"){ // Caso para las preguntas cortas o Ordenar numero, se comportan de manera similar
        for(j=1, i=0; j<=t11usuario.length; j++, i++){
            $('#' + j).val(t11usuario[i]);
        }
    }
    if(numEstilo === "13"){ // Caso para las preguntas de arrastra corta
        //Este código se envió a la función posicionaCajasArrastradas() para colocar las cajas arrastradas resueltas una vez hechos los ajustes
        //de reposicionamiento de elementos
    }
    if(numEstilo === "11"){ // Caso para las preguntas de arrastra ordenar
        // primero los retiro del contenedor y luego los acomodo segun haya
        // contestado el usuario
        //$('#2').appendTo('.sortable-list');
        if(t11usuario.length){
            $('.sortable-item').each(function(){
               $(this).appendTo('#respuestasRadialGroup');
            });
            var respUsuario;
            for(var i=0; i < t11usuario.length; i++){
                respUsuario = parseInt( t11usuario[i] ) - 1;
                $('#' + respUsuario).appendTo('.sortable-list');
            }
        }
    }
    if(numEstilo === "10"){ // Caso para las preguntas de relacionar con lineas
        //Este código se envió a la función pintaLineasResueltas() para trazar las líneas resueltas una vez hechos los ajustes
        //de reposicionamiento de elementos al detectar que se han cargado todas las imágenes ó sino hay imágenes.
        //pintaLineasResueltas();
    }
    if(numEstilo === "14"){ // Para las preguntas de arrastra contenedor
        // Creo un clon de las respuestas en el contenedor
        $('#tarjetas').clone().appendTo('#contenedor').removeClass('ui-helper-clearfix')
                        .removeClass('ui-droppable').removeAttr('id');
        // Elimino las tarjetas que se clonaron, ya que solo me interesaba el ul
        $('#contenedor .tarjetas li').remove();
        // Muevo las tarjetas que selecciono el usuario al contenedor
        for(i=0; i<t11usuario.length; i++){
            var $objLi = $('#tarjetas li').find('#respuesta'+t11usuario[i]+'_btn').parent();
            $objLi.appendTo('#contenedor ul');
        }
    }
}

function posicionaCajasArrastradas (){
    var posTextTop, posTextLeft, posCardTop, posCardLeft, posFinalTop, posFinalLeft, idCard;
    //reacomodaDivsCajasFlotantes();

    for(j=1, i=0; j<=t11usuario.length; j++, i++){
        if(t11usuario[i]){
            idCard = parseInt(t11usuario[i]) - 1;
            // Revisar
            //console.log('es '+ $('#texto'+j).attr('id'));
            var cadena = "Es es una cadena de ejemplo";
            var numero = cadena.indexOf("cadena");
            //console.log('numero '+numero);
                        
            if(typeof $('#texto'+j).attr('id') === 'undefined'){
                posTextTop = $('#'+j).offset().top;
                posTextLeft = $('#'+j).offset().left;
            }else{
                posTextTop = $('#texto'+j).offset().top;
                posTextLeft = $('#texto'+j).offset().left;
            }

            posCardTop = $('#card'+ idCard).offset().top;
            posCardLeft = $('#card'+idCard).offset().left;
            posFinalTop = posTextTop - posCardTop;
            posFinalLeft = posTextLeft - posCardLeft;
            
            $('#card'+idCard).addClass('dropped').css('top', posFinalTop).css('left', posFinalLeft).css('z-index', 1000);
            $('#texto'+j).addClass('hasCard').addClass('card' + idCard).attr('value', t11usuario[i]).attr('id', j);
            // Hack para las preguntas con imagenes
            
            if(pizarron){
                $('.dropped').addClass('dropped-image');
                $('.dropped').removeClass('dropped-text');
            }else{
                $('.dropped').removeClass('dropped-image');
                $('.dropped').addClass('dropped-text');
                   
            }
            //$('#card' + i).val(t11usuario[i]);
        }
    }
}

function reacomodaDivsCajasFlotantes(){
    var arregloTemporal = new Array();
    var colocados_arr = new Array();
    var reordenado_arr = new Array();
    var ultimoElemento; 
    var primerElemento;
    
    console.log('llega '+t11usuario);         
    for(i=0; i<t11usuario.length; i++){
       arregloTemporal[i] = i+1;
    }     
    for(i=0; i<respuestasArr.length; i++){
       arregloTemporal[i] = i+1;
    }        
    
    console.log('seguimiento0: '+arregloTemporal);
    for(i=0, j=0;i<t11usuario.length; i++){
        if(t11usuario[i]){            
            colocados_arr[j] = t11usuario[i];
            j++;
        }        
    }     
    console.log('colocados_arr: '+colocados_arr);
    for(i=0;i<colocados_arr.length; i++){                
        for(j=0; j<arregloTemporal.length; j++){
            if(parseInt(arregloTemporal[j]) === parseInt(colocados_arr[i])){
                delete arregloTemporal[j];
            }
        }
    }   
    arregloTemporal = $.grep(arregloTemporal,function(n){//Función para eliminar elementos vacíos de un array
        return(n);
    });  
    
    console.log('seguimiento1: '+arregloTemporal);
    for(j=0; j<arregloTemporal.length; j++){        
        arregloTemporal[j] = parseInt(arregloTemporal[j]) - 1;
    }            
        
    console.log('seguimiento2: '+arregloTemporal);        
    for(j=0; j<arregloTemporal.length; j++){               
        primerElemento = arregloTemporal[0];
        //console.log('primerElemento '+primerElemento);
        ultimoElemento = arregloTemporal[arregloTemporal.length-1];               
        arregloTemporal.unshift(arregloTemporal[arregloTemporal.length-1]);
        arregloTemporal.pop();
        //console.log('ultimoElemento: '+ultimoElemento+"   primerElemento: "+primerElemento);        
        $('#card'+ultimoElemento).insertBefore('#card'+primerElemento);                      
        
        if(parseInt(arregloTemporal[0]) !== 0 && ceroInicialUtilizado === 1){            
            $('#card7').insertBefore('#card0');
        }        
    }
    ceroInicialUtilizado++;    
}
function pintaLineasResueltas(){    
    var posInicioX, posInicioY, posFinX, posFinY;
    var color = "black";
    var anchoStroke = "2";      
    var areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
    var centroDeCirculo = 0//Obtiene la posición centro de los puntos
    posColores = 0;//Se resetea la variable de posición de los colores
    incremento = 0;//Se resetea la variable de incremento de los lineas
    
    if(t11usuario.length >= 1){//Si hay datos por pintar
        for(j=1, i=0, g=1; j<=t11usuario.length; j++, i++){
            if(t11usuario[i]){
                 var desfaseIzquierda = $("#elementosSVG").offset().left;
                 var desfaseSuperior = $("#elementosSVG").offset().top;

                 areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
                 centroDeCirculo = parseInt($('#i_'+j).attr("r"));//Obtiene la posición centro de los puntos

                 posInicioX =  obtenerDimensiones($('#i_'+j)[0]).x;
                 posInicioY = obtenerDimensiones($('#i_'+j)[0]).y;

                 posInicioX = (posInicioX - desfaseIzquierda) + centroDeCirculo;
                 posInicioY = (posInicioY - desfaseSuperior) + centroDeCirculo;
                 posInicioY = posInicioY + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic

                 posFinX = obtenerDimensiones($('#d_'+t11usuario[i])[0]).x;
                 posFinY = obtenerDimensiones($('#d_'+t11usuario[i])[0]).y;

                 posFinX = (posFinX - desfaseIzquierda) + centroDeCirculo;
                 posFinY = (posFinY - desfaseSuperior) + centroDeCirculo;
                 posFinY = posFinY + areaScroleada;//Obtiene el area escroleada para sumarla a la posición Y del clic
                 posFinY = posFinY + 2;//Ajuste en Y para pintado correcto de lineas marcadas
                 
                 var areaScroleada = window.pageYOffset;//Obtiene el area escroleada para sumarla a la posición Y del clic
                 posFinY = posFinY - areaScroleada;//Se aplica este ajuste para que pinte correctamente las líneas cuando se le dé refresh a la página y se encuentre escroleada
                 crearLinea("lineaCreada", posInicioX, posInicioY,posFinX,posFinY, color, anchoStroke);

                 //Este fragmento de código pinta los circulos y las lineas
                 cambiaColorSVG($('#i_'+j)[0]);
                 cambiaColorSVG($('#d_'+t11usuario[i])[0]);
                 cambiaColorObjeto( 'lineaCreada'+g );//Cambia el color a la linea
                 g++;

                 posColores = posColores + 1;//Incrementa la siguiente posición del array colores.
                 if(posColores == colores_arr.length){
                     posColores = 0;
                 }                
                 arrRespuestasLineas[i] = {'id': j, 'selected': t11usuario[i]};                               
             }else{
                 arrRespuestasLineas[i] = {'id': j, 'selected': 0};
             }          
         }
    }            
}

//FUNCIONES PARA ARRASTRA CORTAS
function creaArrastraCortas() {
    var finalContieneCaja = false;//true
    var palabras = new Array(); 
    var dato='';
    var texto;
    var sonidosArr;
    var etiquetaSonido = '';   
    var dimensiones = new Object();    
                
    if(pizarron){
        finalContieneCaja = typeof respuestasArr[respuestasArr.length-1] !== 'undefined' ? true : false ;        
        $("#pilaDeCartas").css('height', '220px');
        $("#pilaDeCartasExtra").css('height', '220px');
        if(tipoMultimedia === "audio") {  
            imagenesPizarron = ["5audio.png","4audio.png","1audio.png","3audio.png","6audio.png"];
            if(idioma === "es") {    sonidosArr = ["5.mp3","4.mp3","1.mp3","3.mp3","6.mp3"];  }
            if(idioma === "en") {    sonidosArr = ["5en.mp3","4en.mp3","1en.mp3","3en.mp3","6en.mp3"];  }
            for ( var i=0; i<sonidosArr.length; i++ ) {
                etiquetaSonido = etiquetaSonido + '<audio id="audio'+i+'"><source src="http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/'+sonidosArr[i]+'"></source></audio>';
            }
        }
        if(tipoMultimedia === "imagen") {
            imagenesPizarron = ["5.png","4.png","1.png","3.png","6.png"];
        }
    }else{
        tipoMultimedia = "texto";//siempre texto
        finalContieneCaja = typeof respuestasArr[respuestasArr.length-1] !== 'undefined' ? true : false ;
    }
    
    $('#pilaDeCartas').html( '' );
    $('#pilaDeCartasExtra').html( '' );
    $('#espaciosDeCartas').html( '' );        
    
    // Crea la pila de cartas revueltas      
    if( !finalContieneCaja ){
        respuestasArr.pop();//Monitorear elimina el último elemento del arreglo  
    }    
    if(respuestasArr.length <= 4){ // 4 es el tamaño de elementos que caben en un renglon
        $('#espaciosDeCartas').css('top', '90px');
    }else{
        $('#espaciosDeCartas').css('top', '160px');
    }
    for ( var i=0; i<respuestasArr.length; i++ ) {
        if(pizarron){
            if(tipoMultimedia === "imagen") {
                //dato = '<img id="'+i+'" src="http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/'+imagenesPizarron[i]+'" />';
                dato = respuestasArr[i];
                texto = '';
            }
            if(tipoMultimedia === "audio") {
                dato = '<img id="'+i+'" src="http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/'+imagenesPizarron[i]+'"/>'+etiquetaSonido;
                texto = '';
            }
        }else{
            if(tipoMultimedia === "texto"){
                dato = '';
                texto = respuestasArr[i];                                            
            }
        }                         
        
        //Áreas vacías        
        $('<div><span></span></div>').prepend(dato).attr( 'id', 'vacio'+i ).appendTo( '#pilaDeCartasExtra' ).draggable( {
        containment: '#content',
        stack: '#pilaDeCartasExtra div'
        } );        
            
      $('<div><span style="vertical-align:bottom; !important; text-align: center;">' + texto + '</span></div>').data( 'text', texto ).data( 'number', i).prepend(dato).attr( 'id', 'card'+i ).appendTo( '#pilaDeCartas' ).draggable( {
        containment: '#content',
        stack: '#pilaDeCartas div',
        cursor: 'move',
        revert: true,
        drop: handleCardReturn
      } );
     
     if(i === respuestasArr.length-1){         
         $( '#card'+i ).css('clear', 'left');//Al inicio aplica un clear a la última caja superior verde para evitar que se desacomoden las demás
     }
        
    
    $( '#card'+i ).mouseup(function() {              
        var idActual = this;      
        var idActual2 = $(this).attr('id');            
        sueltaEnDrop = false;
        
        $( idActual ).draggable( 'option', 'revert', false );
        
        myInterval = setInterval(function () {            
            clearInterval(myInterval);//Elimina el timer
            
            if(sueltaEnDrop){
                //No hace nada
            }else{//Si soltaste la caja fuera del drop regresa a su posición                                                                
                if( $( idActual ).hasClass('dropped-text') ){//Si es una caja dropeada                    
                    $( idActual ).removeClass('dropped-text');          
                    $( idActual ).removeClass('dropped');                    
                    $( idActual2 ).removeClass('dropped-text');          
                    $( idActual2 ).removeClass('dropped');
                    $( idActual2 ).draggable( 'option', 'revert', false );                    
                }else{                    
                    if( $( idActual2 ).hasClass('dropped-text') ){                        
                        animarCajaVerde( idActual );
                    }else{
                       //No hace nada
                    }                    
                }
                animarCajaVerde( idActual );
            }                        
        },100);//Tiempo de espera para iniciar la animación         

    });
        
     /*$( '#card'+i ).mousedown = $( '#card'+i ).mouseup (function( event ) {
        $(this).css('width', '');//Se eliminan estilos para evitar que se desfasen los cuadros superiores verdes en el resize
        $(this).css('height', '');
        $(this).css('right', '');
        $(this).css('bottom', '');
        $(this).css('left', '');
        $(this).css('top', '');                
        //$( '#card'+i ).css('clear', '');//Al inicio aplica un clear a la última caja superior verde para evitar que se desacomoden las demás        
        //$( this ).css('clear', '');
        if(i === respuestasArr.length-1){         
         $( '#card'+i ).css('clear', 'left');//Al inicio aplica un clear a la última caja superior verde para evitar que se desacomoden las demás                     
        //$( this ).css('float', 'left'); 
    });}*/
    
      //$('#card'+i).css('height', '36px');      
      //$('#espaciosDeCartas').addClass('large-12 medium-12 small-12 columns');
      if(tipoMultimedia === "audio") { 
        $('#card'+i).click(function( event ) {//Resuelve los iconos
            var audio = document.getElementById("audio"+event.target.id);
            audio.play();
        });
    }
      if(pizarron){
        // $('#card'+i).css('font-size', '40px');
         $('#card'+i).css('height', 'auto');
         $('#card'+i).css('background-color', 'transparent');
         $('#card'+i).css('border', 'transparent');        
      }                        
    }   
    
    for ( var i=1; i<=respuestasArr.length; i++ ) {
         palabras[i-1] = ''; //Crea las cajas inferiores
    }
    var saltoLinea;
    for ( var i=1; i<=preguntasArr.length; i++ ) {
        // mismo caso de las preguntas de tipo corta, hay que agregar este elemento:
        // <div class="texto-respuesta-spacer"></div> el detalle vino que cuando
        // se agrega, también afecta al funcionamiento del tipo de pregunta, se
        // tendría que revisar los estilos para corregir todas las fallas
                        
        if(i===preguntasArr.length && !pintarUltimaCaja ){
            //dato = '<div id="texto'+i+'" class="texto-respuesta">'+preguntasArr[i-1]+'</div>';
            dato += preguntasArr[i-1];//+'.'; //Agrega un punto final en el último enunciado
            dato = dato.replace(/<p>/g, "");
            dato = dato.replace(/<\/p>/g, ""); // Sanitizo último campo
        }else{            
            var pregArr = preguntasArr[i-1];
            
            if(i === 1){
                saltoLinea = '';//Se agrega un salto de línea sólo antes de la primera caja inferior punteada
            }else{
                saltoLinea = '';//No se agregan saltos de línea en las cajas inferiores punteadas
            }            
            palabras[i-1] = '...';//Se setean puntos suspensivos sólo para que no se quede vacio y se solucione el conflicto de las cajas punteadas cortadas.                        
        
            dato += saltoLinea+ pregArr.replace(/<\/p>/, "") + '<span id="texto'+i+'" style="line-height:60px;"  class="texto-respuesta-sub">' + palabras[i-1] + '</span>';            
            dato = dato.replace(/<p>/g, "");//Se eliminan los párrafos (abrir párrafo)
            dato = dato.replace(/<\/p>/g, "");//Se eliminan los párrafos (cerrar párrafo)                        
        }
        
        if(pizarron){        
            //$('#texto'+i).css('font-size', '85px');
            $('#cajaPunteada'+i).css('font-size', '40px');
            $('#espaciosDeCartas').css('margin', '150px 0px 0px 40px');
            
            if(tipoMultimedia === "texto"){
              $('#cajaPunteada'+i).css('height', 'auto');
            }
            if(tipoMultimedia === "imagen" || tipoMultimedia === "audio"){           
              $('#cajaPunteada'+i).css('height', '160px');
              $('.texto-respuesta-sub').css('height', '62px').css('width', '60px');
            }
        }else{            
            //$('#espaciosDeCartas').css('margin', '20px 0px 0px 0px');
        }
       $('.ui-droppable').data('elemDentro', 0);

        $('.texto'+i).droppable('disable');
        $('#texto'+i).droppable('disable');
        
        $('#texto'+i).removeClass('ui-droppable');
        $('#texto'+i).removeClass('ui-droppable-disabled');
        $('#texto'+i).removeClass('ui-state-disabled');
    }    
    var ultimoCaracter = dato.substring(dato.length-1);
    if( ultimoCaracter !== '.' ){
        dato += '.';
    }
    $('<div id="texto" style="border-style: none; ">'+dato+'</div>').appendTo('#espaciosDeCartas');//Textos de los incisos    
    
    $( '.texto-respuesta-sub' ).droppable({
              accept: '#pilaDeCartas div',
              hoverClass: 'hovered',
              out: handleCardReturn,
              drop: handleCardDrop
        });
    if(pizarron){
        dimensiones = obtenerDimensionesImagen("http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/pizarron17.png", 992);  
        $('#fondoPregunta').height(dimensiones.height + 40 );
        $('#content').width('875px');
        //$('#pilaDeCartas div').css('width', 'auto');
        //$('#espaciosDeCartas').css('width', 'auto');
    }else{
        
        dimensiones.height = $('#pilaDeCartas').height() + $('#texto').height() ;//dimensiones.height = $('#respuestasRadialGroup').height();
        $('#content').height( dimensiones.height + 100 );
        
        $('#respuestasRadialGroup').height( dimensiones.height + 100 );  
        
        $('#texto img').each(function(){
        $(this).load(function(){
           imagenesCargadas++;
           console.log("Cuantas imagenes: " + imagenesCargadas);
           if(imagenesCargadas === numImagenes){//Si se cargaron todas las imágenes 
               console.log("Entro el console " );
               dimensiones.height = $('#pilaDeCartas').height() + $('#espaciosDeCartas').height();//dimensiones.height = $('#respuestasRadialGroup').height();
               $('#content').height( dimensiones.height + 120 );
               $('#respuestasRadialGroup').height( dimensiones.height + 120 );  
            }
        });
    });
        //$('#content').width('auto');
        //$('#content').width('70%');
    }
    $('#content').height(dimensiones.height + 40);
    var imagenesCargadas = 0;
    var numImagenes = $('#texto img').length;
    

     
    // Hack para aumentar el tamaño del div content
    seteaFloatLeft();
    posicionaCajasArrastradas();
    redimensionaAlturaDivPrincipal();//Se emplea para volver a ajustar la altura del contenedor principal (respuestasRadialGroup)
    //DETECTAR ESTA PARTE: una vez cargadas todas las imágenes del rectivo
}

$( window ).resize(function() {
   seteaFloatLeft();
   redimensionaAlturaDivPrincipal();//Se emplea para volver a ajustar la altura del contenedor principal (respuestasRadialGroup) una vez cargas todas las imágenes del rectivo
   //posicionaCajasArrastradas();
});
function seteaFloatLeft() {//Función para solucionar el reacomodo de cajas superiores verdes
    for ( var i=0; i<respuestasArr.length; i++ ) {                       
        $( '#card'+i ).css('float', 'left');//Aplica un clear a la última caja superior verde para evitar que se desacomoden las demás             
        if(i === respuestasArr.length-1){              
           $( '#card'+i ).css('clear', '');//Al inicio aplica un clear a la última caja superior verde para evitar que se desacomoden las demás
        }      
    }
}
function handleCardDrop( event, ui ) {
    sueltaEnDrop = true;    
    if( $(this).hasClass('hasCard') ){
        console.log('No se puede colocar un elemento dentro de un contenedor ocupado');
    }else{
        var elem = $(this);
        var cardNumber = ui.draggable.data( 'text' );
        var numeroDeEspacio = elem.data( 'number' );
        var pocisionDeEspacio = ui.draggable.data( 'number' ) + 1;
        elem.data( 'elemDentro', elem.data( 'elemDentro' ) + 1 );
        var elemDentro = elem.data( 'elemDentro' );
        //console.log('elem: ' + elemDentro);
        //console.log('num: ' + numeroDeEspacio);
        //console.log('card: ' + cardNumber);
        //Si la carta fue soltada en lugar correcto cambia su color, posición directamente en la parte superior y evitar que sea dragueada de nuevo.
        ui.draggable.addClass( 'dropped' );        
        //ui.draggable.css('float', '');        
        ui.draggable.addClass( 'dropped-text' );
        
        elem.data('elemDentro');
        var idElem  = ui.draggable.attr('id');
        $(this).attr('value', pocisionDeEspacio);
        $(this).attr('id', numeroDeEspacio);

        //ui.draggable.draggable( 'disable' );
        //$(this).droppable( 'disable' );
        
        /*t11usuario[pocisionDeEspacio-1] = pocisionDeEspacio;
        pocisionDeEspacioGlobal = pocisionDeEspacio;
        reacomodaDivsCajasFlotantes();      */  
        
        ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );
        ui.draggable.draggable( 'option', 'revert', false );
        elem.attr('class', 'ui-droppable hasCard ' + idElem );        
    }
}
function animarCajaVerde(idActual){
    $( idActual ).animate({//Regresala a la parte superior
        left: "0px",
        top: "0px"
    }, 500, function() {//Tiempo de la animación
        //animación completada
       sueltaEnDrop = false;
    });
}
function handleCardReturn(event, ui){
    if( $(this).hasClass('hasCard') && !$(this).hasClass(ui.draggable.attr('id')) ){
        console.log('No se puede colocar un elemento dentro de un contenedor ocupado');
    }else{
        ui.draggable.removeClass('dropped');
        $(this).removeAttr('id');
        $(this).removeAttr('value');
        $(this).removeClass('dropped');
        //$(this).addClass('ui-droppable');
        $(this).attr('class', 'ui-droppable');
        //console.log($(this));
        //console.log(ui);
        //.log('Regresa a su estado normal');
    }
}

//FUNCIONES PARA ARRASTRA CONTENEDOR 1
function arrastraElemento(elemento, event) {
    event.dataTransfer.setData('Identificador de Objeto', elemento.id);
    console.log('arrastraElemento');
}
function sueltaElemento(area, event) {
    var item = event.dataTransfer.getData('Identificador de Objeto');
    area.appendChild(document.getElementById(item));

}


//FUNCIONES PARA EL ROMPECABEZAS
function rompecabezasMain (){
    $(function () {
        var medidaGrid = t11columnas;
        var rompecabezasClase = {
        startTime: new Date().getTime(),
        iniciarJuego: function (imagen, medidaGrid) {       
            this.setearImagen(imagen, medidaGrid);            
            $('#sortable').randomize();
            this.habilitarSwap('#sortable li');                
        },
        habilitarSwap: function (elemento) {
            $(elemento).draggable({
                snap: '#droppable',
                snapMode: 'outer',
                revert: "invalid",
                helper: "clone"
            });
            $(elemento).droppable({
                drop: function (event, ui) {
                    var $dragElemento = $(ui.draggable).clone().replaceAll(this);
                    $(this).replaceAll(ui.draggable);
                    currentList = $('#sortable > li').map(function (i, el) { return $(el).attr('data-value'); });
                    if (esRandomizada(currentList)){                    
                        $('#cuadroImagenActual').empty().html($('#juegoTerminado').html());
                    }else {                    
                    }
                    rompecabezasClase.habilitarSwap(this);
                    rompecabezasClase.habilitarSwap($dragElemento);
                }
            });
        },
        setearImagen: function (imagen, medidaGrid) {        
            medidaGrid = medidaGrid || 4; // Si medidaGrid es null por default cambia a 4.
            var porcentaje = 100 / (medidaGrid - 1);                
            var image = imagen;
            $('#imagenActual').attr('src', imagen);
            $('#sortable').empty();
            
            for (var i = 0; i < medidaGrid * medidaGrid; i++) {
                var posicionX = (porcentaje * (i % medidaGrid)) + '%';
                var posicionY = (porcentaje * Math.floor(i / medidaGrid)) + '%';                
                var li = $('<li class="item" data-value="' + (i) + '"></li>').css({
                    'background-image': 'url(' + imagen + ')',                
                    'background-size': (medidaGrid * 100) + '%',
                    'background-position': posicionX + ' ' + posicionY,
                    'width': 396 / medidaGrid,
                    'height': 396 / medidaGrid
                });
                $('#sortable').append(li);
            }        
            $('#sortable').randomize();        
        }
    };
    
    function esRandomizada(arr) {
        for (var i = 0; i < arr.length - 1; i++) {
            if (arr[i] != i)
                return false;
        }
        return true;
    }
    $.fn.randomize = function (selector) {
        var $Elementos = selector ? $(this).find(selector) : $(this).children(),
            $parents = $Elementos.parent();

        $parents.each(function () {
            $(this).children(selector).sort(function () {
                return Math.round(Math.random()) - 0.5;
            }).remove().appendTo(this);
        });
        return this;
    };    
        rompecabezasClase.iniciarJuego(imagen, medidaGrid);                            
    });
              
}


function radialMain() {
    if( document.createElement('svg').getAttributeNS ) {  
        var incremento = 1;
        var checkbxsCross = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-cross input[type="checkbox"]' ) ),
        radiobxsFill = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-fill input[type="radio"]' ) ),
        checkbxsCheckmark = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-checkmark input[type="checkbox"]' ) ),
        radiobxsCircle = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-circle input[type="radio"]' ) ),
        checkbxsBoxfill = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-boxfill input[type="checkbox"]' ) ),
        radiobxsSwirl = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-swirl input[type="radio"]' ) ),
        checkbxsDiagonal = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-diagonal input[type="checkbox"]' ) ),
        checkbxsList = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-list input[type="checkbox"]' ) ),
        pathDefs = {
            cross : ['M 10 10 L 90 90','M 90 10 L 10 90'],
            fill : ['M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809'],
            checkmark : ['M16.667,62.167c3.109,5.55,7.217,10.591,10.926,15.75 c2.614,3.636,5.149,7.519,8.161,10.853c-0.046-0.051,1.959,2.414,2.692,2.343c0.895-0.088,6.958-8.511,6.014-7.3 c5.997-7.695,11.68-15.463,16.931-23.696c6.393-10.025,12.235-20.373,18.104-30.707C82.004,24.988,84.802,20.601,87,16'],
            circle : ['M34.745,7.183C25.078,12.703,13.516,26.359,8.797,37.13 c-13.652,31.134,9.219,54.785,34.77,55.99c15.826,0.742,31.804-2.607,42.207-17.52c6.641-9.52,12.918-27.789,7.396-39.713 C85.873,20.155,69.828-5.347,41.802,13.379'],
            boxfill : ['M6.987,4.774c15.308,2.213,30.731,1.398,46.101,1.398 c9.74,0,19.484,0.084,29.225,0.001c2.152-0.018,4.358-0.626,6.229,1.201c-5.443,1.284-10.857,2.58-16.398,2.524 c-9.586-0.096-18.983,2.331-28.597,2.326c-7.43-0.003-14.988-0.423-22.364,1.041c-4.099,0.811-7.216,3.958-10.759,6.81 c8.981-0.104,17.952,1.972,26.97,1.94c8.365-0.029,16.557-1.168,24.872-1.847c2.436-0.2,24.209-4.854,24.632,2.223 c-14.265,5.396-29.483,0.959-43.871,0.525c-12.163-0.368-24.866,2.739-36.677,6.863c14.93,4.236,30.265,2.061,45.365,2.425 c7.82,0.187,15.486,1.928,23.337,1.903c2.602-0.008,6.644-0.984,9,0.468c-2.584,1.794-8.164,0.984-10.809,1.165 c-13.329,0.899-26.632,2.315-39.939,3.953c-6.761,0.834-13.413,0.95-20.204,0.938c-1.429-0.001-2.938-0.155-4.142,0.436 c5.065,4.68,15.128,2.853,20.742,2.904c11.342,0.104,22.689-0.081,34.035-0.081c9.067,0,20.104-2.412,29.014,0.643 c-4.061,4.239-12.383,3.389-17.056,4.292c-11.054,2.132-21.575,5.041-32.725,5.289c-5.591,0.124-11.278,1.001-16.824,2.088 c-4.515,0.885-9.461,0.823-13.881,2.301c2.302,3.186,7.315,2.59,10.13,2.694c15.753,0.588,31.413-0.231,47.097-2.172 c7.904-0.979,15.06,1.748,22.549,4.877c-12.278,4.992-25.996,4.737-38.58,5.989c-8.467,0.839-16.773,1.041-25.267,0.984 c-4.727-0.031-10.214-0.851-14.782,1.551c12.157,4.923,26.295,2.283,38.739,2.182c7.176-0.06,14.323,1.151,21.326,3.07 c-2.391,2.98-7.512,3.388-10.368,4.143c-8.208,2.165-16.487,3.686-24.71,5.709c-6.854,1.685-13.604,3.616-20.507,4.714 c-1.707,0.273-3.337,0.483-4.923,1.366c2.023,0.749,3.73,0.558,5.95,0.597c9.749,0.165,19.555,0.31,29.304-0.027 c15.334-0.528,30.422-4.721,45.782-4.653'],
            swirl : ['M49.346,46.341c-3.79-2.005,3.698-10.294,7.984-8.89 c8.713,2.852,4.352,20.922-4.901,20.269c-4.684-0.33-12.616-7.405-14.38-11.818c-2.375-5.938,7.208-11.688,11.624-13.837 c9.078-4.42,18.403-3.503,22.784,6.651c4.049,9.378,6.206,28.09-1.462,36.276c-7.091,7.567-24.673,2.277-32.357-1.079 c-11.474-5.01-24.54-19.124-21.738-32.758c3.958-19.263,28.856-28.248,46.044-23.244c20.693,6.025,22.012,36.268,16.246,52.826 c-5.267,15.118-17.03,26.26-33.603,21.938c-11.054-2.883-20.984-10.949-28.809-18.908C9.236,66.096,2.704,57.597,6.01,46.371 c3.059-10.385,12.719-20.155,20.892-26.604C40.809,8.788,58.615,1.851,75.058,12.031c9.289,5.749,16.787,16.361,18.284,27.262 c0.643,4.698,0.646,10.775-3.811,13.746'],
            diagonal : ['M16.053,91.059c0.435,0,0.739-0.256,0.914-0.768 c3.101-2.85,5.914-6.734,8.655-9.865C41.371,62.438,56.817,44.11,70.826,24.721c3.729-5.16,6.914-10.603,10.475-15.835 c0.389-0.572,0.785-1.131,1.377-1.521'],
            list : ['M1.986,8.91c41.704,4.081,83.952,5.822,125.737,2.867 c17.086-1.208,34.157-0.601,51.257-0.778c21.354-0.223,42.706-1.024,64.056-1.33c18.188-0.261,36.436,0.571,54.609,0.571','M3.954,25.923c9.888,0.045,19.725-0.905,29.602-1.432 c16.87-0.897,33.825-0.171,50.658-2.273c14.924-1.866,29.906-1.407,44.874-1.936c19.9-0.705,39.692-0.887,59.586,0.45 c35.896,2.407,71.665-1.062,107.539-1.188']
        },
        animDefs = {
            cross : { speed : .2, easing : 'ease-in-out' },
            fill : { speed : .8, easing : 'ease-in-out' },
            checkmark : { speed : .2, easing : 'ease-in-out' },
            circle : { speed : .2, easing : 'ease-in-out' },
            boxfill : { speed : .8, easing : 'ease-in' },
            swirl : { speed : .8, easing : 'ease-in' },
            diagonal : { speed : .2, easing : 'ease-in-out' },
            list : { speed : .3, easing : 'ease-in-out' }
        };

        function createSVGEl( def ) {            
            var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
            if( def ) {
                svg.setAttributeNS( null, 'viewBox', def.viewBox );
                            svg.setAttributeNS( null, 'id', 'idSVG'+incremento );                        
                svg.setAttributeNS( null, 'preserveAspectRatio', def.preserveAspectRatio );                        
            }
            else {
                            svg.setAttributeNS( null, 'id', 'idSVG'+incremento );
                svg.setAttributeNS( null, 'viewBox', '0 0 100 100' );
            }
                    incremento++;
            svg.setAttribute( 'xmlns', 'http://www.w3.org/2000/svg' );
            return svg;
        }

        function controlCheckbox( el, type, svgDef ) {
            var svg = createSVGEl( svgDef );
            el.parentNode.appendChild( svg );

            el.addEventListener( 'change', function() {
                if( el.checked ) {
                    draw( el, type );
                }
                else {
                    reset( el );
                }
            } );
        }

        function controlRadiobox( el, type ) {
            var svg = createSVGEl();
            el.parentNode.appendChild( svg );
            el.addEventListener( 'change', function() {
                resetRadio( el );
                draw( el, type );
            } );
        }

        checkbxsCross.forEach( function( el, i ) { controlCheckbox( el, 'cross' ); } );
        radiobxsFill.forEach( function( el, i ) { controlRadiobox( el, 'fill' ); } );
        checkbxsCheckmark.forEach( function( el, i ) { controlCheckbox( el, 'checkmark' ); } );
        radiobxsCircle.forEach( function( el, i ) { controlRadiobox( el, 'circle' ); } );
        checkbxsBoxfill.forEach( function( el, i ) { controlCheckbox( el, 'boxfill' ); } );
        radiobxsSwirl.forEach( function( el, i ) { controlRadiobox( el, 'swirl' ); } );
        checkbxsDiagonal.forEach( function( el, i ) { controlCheckbox( el, 'diagonal' ); } );
        checkbxsList.forEach( function( el ) { controlCheckbox( el, 'list', { viewBox : '0 0 300 100', preserveAspectRatio : 'none' } ); } );

        function draw( el, type ) {
            var paths = [], pathDef, 
                animDef,
                svg = el.parentNode.querySelector( 'svg' );

            switch( type ) {
                case 'cross': pathDef = pathDefs.cross; animDef = animDefs.cross; break;
                case 'fill': pathDef = pathDefs.fill; animDef = animDefs.fill; break;
                case 'checkmark': pathDef = pathDefs.checkmark; animDef = animDefs.checkmark; break;
                case 'circle': pathDef = pathDefs.circle; animDef = animDefs.circle; break;
                case 'boxfill': pathDef = pathDefs.boxfill; animDef = animDefs.boxfill; break;
                case 'swirl': pathDef = pathDefs.swirl; animDef = animDefs.swirl; break;
                case 'diagonal': pathDef = pathDefs.diagonal; animDef = animDefs.diagonal; break;
                case 'list': pathDef = pathDefs.list; animDef = animDefs.list; break;
            };

            paths.push( document.createElementNS('http://www.w3.org/2000/svg', 'path' ) );

            if( type === 'cross' || type === 'list' ) {
                paths.push( document.createElementNS('http://www.w3.org/2000/svg', 'path' ) );
            }

            for( var i = 0, len = paths.length; i < len; ++i ) {
                var path = paths[i];
                svg.appendChild( path );

                path.setAttributeNS( null, 'd', pathDef[i] );

                var length = path.getTotalLength();
                // Clear any previous transition
                //path.style.transition = path.style.WebkitTransition = path.style.MozTransition = 'none';
                // Set up the starting positions
                path.style.strokeDasharray = length + ' ' + length;
                if( i === 0 ) {
                    path.style.strokeDashoffset = Math.floor( length ) - 1;
                }
                else path.style.strokeDashoffset = length;
                // Trigger a layout so styles are calculated & the browser
                // picks up the starting position before animating
                path.getBoundingClientRect();
                // Define our transition
                path.style.transition = path.style.WebkitTransition = path.style.MozTransition  = 'stroke-dashoffset ' + animDef.speed + 's ' + animDef.easing + ' ' + i * animDef.speed + 's';
                // Go!
                path.style.strokeDashoffset = '0';
            }
        }

        function reset( el ) {
            Array.prototype.slice.call( el.parentNode.querySelectorAll( 'svg > path' ) ).forEach( function( el ) { el.parentNode.removeChild( el ); } );
        }

        function resetRadio( el ) {
            Array.prototype.slice.call( document.querySelectorAll( 'input[type="radio"][name="' + el.getAttribute( 'name' ) + '"]' ) ).forEach( function( el ) { 
                var path = el.parentNode.querySelector( 'svg > path' );
                if( path ) {
                    path.parentNode.removeChild( path );
                }
            } );
        }

    }
}



//FUNCIONES PARA RELACIONA ARRASTRAR
var encabezado1 = '';
var encabezado2 = '';
var trazo = '';
var pathFinal = '';
var textoSVG = '';
var encabezado1CajaDerecha = '';
var encabezado2CajaDerecha = '';
var trazosSVGSecundario = '';
var ancho = 375.6;
var formaSVG = '';
var trazosSVG = '';
var separacion = 0;
var texto = '';
var texto2 = '';
var colorSobrePieza = 'lime';
var colorNormalPieza = '#54B084';
var colorNormalPiezaVacia = '#E2E2E2';
var separacionPiezas = 28;
var hizoDrop = false;
var renglones_arr = new Array();
var posRenglones = 0;
var renglonesMaximo;
var alturaTexto = 72;
var colorContornoPieza = '#000000';
var posLeftInicialIzquierda = 16;
var posLeftInicialDerecha = 445;
var datosRelacionaArrastra = new Array();
var posicionesTopIniciales_arr = new Array();
var posXUnidaIzquierda = 48;
var posXUnidaDerecha = 415;

Array.prototype.unique=function(a){
  return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
});
function preparaReacomodoDePiezas (datos_arr) {
    var ladoIzquierdo;
    var ladoDerecho;
    var union;
        
    for(var f=0;f<datos_arr.length;f++){
        ladoIzquierdo = datos_arr[f];
        ladoDerecho = datos_arr[f];
        union = datos_arr[f];
        
        ladoIzquierdo = ladoIzquierdo.substring(0,ladoIzquierdo.indexOf('-',0) );
        ladoDerecho = ladoDerecho.substring( ladoDerecho.indexOf('-',0)+1, ladoDerecho.indexOf('|',0) );
        union = union.substring( union.indexOf('|',0)+1, union.length );
        //console.log(datos_arr[f]+'  ladoIzquierdo: '+ladoIzquierdo+'  ladoDerecho: '+ladoDerecho+'  union: '+union);
        reubicarPiezas(ladoIzquierdo, ladoDerecho, union, f);
    }
}
function reubicarPiezas(ladoIzquierdo, ladoDerecho, union, incremento) {
    posicionesTopIniciales_arr = posicionesTopIniciales_arr.unique();
    var reemplazadaActualizadaTop = posicionesTopIniciales_arr[incremento];
    var reemplazadaActualizadaLeft1;
    var reemplazadaActualizadaLeft2;
    var cajaIzquierda = '#cajaIzquierda'+ladoIzquierdo;
    var cajaIzquierdaVacia = '#cajaIzquierdaVacia'+ladoIzquierdo;
    var cajaDerecha = '#cajaDerecha'+ladoDerecho;
    var cajaDerechaVacia = '#cajaDerechaVacia'+ladoDerecho;
    
    if(union === '0'){//Sino son piezas unidas
        reemplazadaActualizadaLeft1 = posLeftInicialIzquierda;
        reemplazadaActualizadaLeft2 = posLeftInicialDerecha;
    }else{//Si son piezas unidas
        reemplazadaActualizadaLeft1 = posXUnidaIzquierda;
        reemplazadaActualizadaLeft2 = posXUnidaDerecha;
    }
    
    $( cajaIzquierda ).css({left:reemplazadaActualizadaLeft1, top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja izquierda verde
    $( cajaDerecha ).css({left:reemplazadaActualizadaLeft2, top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja derecha verde
    $( cajaIzquierdaVacia ).css({ top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja izquierda vacia verde
    $( cajaDerechaVacia ).css({ top:reemplazadaActualizadaTop, position:'absolute'});//Reubicar la caja derecha vacia verde
            
    $( cajaIzquierda ).attr('ligados', ladoIzquierdo+'-'+ladoDerecho);//Actualiza ligados de ambas cajas unidas
    $( cajaDerecha ).attr('ligados', ladoIzquierdo+'-'+ladoDerecho);//Actualiza ligados de ambas cajas unidas
    
    $( cajaIzquierda ).attr('haceLinea1', ladoIzquierdo).attr('haceLinea2', ladoDerecho);//Actualiza linea1 y linea2 de ambas cajas unidas
    $( cajaDerecha ).attr('haceLinea1', ladoIzquierdo).attr('haceLinea2', ladoDerecho);//Actualiza linea1 y linea2 de ambas cajas unidas
    
    $( cajaIzquierda ).draggable( 'option', 'revert', false );//Elimina el reverse de ambas cajas unidas
    $( cajaDerecha ).draggable( 'option', 'revert', false );//Elimina el reverse de ambas cajas unidas
        
    reemplazadaActualizadaLeft1 = posLeftInicialIzquierda;   reemplazadaActualizadaLeft2 = posLeftInicialDerecha;
    actualizaPosiciones ( cajaIzquierda , reemplazadaActualizadaLeft1, reemplazadaActualizadaTop);//Actualiza posiciones de ambas cajas unidas
    actualizaPosiciones ( cajaDerecha , reemplazadaActualizadaLeft2, reemplazadaActualizadaTop);//Actualiza posiciones de ambas cajas unidas
    actualizaPosiciones ( cajaIzquierdaVacia , reemplazadaActualizadaLeft1, reemplazadaActualizadaTop);//Actualiza posiciones de ambas cajas vacías unidas
    actualizaPosiciones ( cajaDerechaVacia , reemplazadaActualizadaLeft2, reemplazadaActualizadaTop);///Actualiza posiciones de ambas cajas vacías unidas
    
    if(union === '1'){//Si son piezas unidas
        opacarPieza( cajaIzquierdaVacia );//Oculta ambas cajas vacias unidas
        opacarPieza( cajaDerechaVacia );//Oculta ambas cajas vacias unidas
        aplicaSombra( cajaIzquierda );//Aplica sombra a ambas cajas unidas
        aplicaSombra( cajaDerecha );//Aplica sombra a ambas cajas unidas        
        zIndexPieza ( cajaIzquierda, '4' );//aplica Zindex para enviar al frente la caja izquierda
        zIndexPieza ( cajaDerecha, '3' );//aplica Zindex para enviar al frente la caja izquierda      
    }    
}

function construyeSVGvacio (encabezado, trazos) {
    var formaSVG;
    formaSVG = encabezado;
    formaSVG += trazos + '</svg>';
    return formaSVG;
}
function formaTextoSVG (elemento, contenido){
    /*var textoRegresado = '';    textoRegresado += '<g style="stroke: black; white-space: pre-wrap;"><text id="multiline-text" x='+posXTexto+' y=40 white-space: nowrap;>'+contenido+'</text></g></svg>';    return textoRegresado;*/
}
function creaTextos (elemento, contenido, consecutivo, imprimir, idParrafo){
    var svgUtilizado = Snap( elemento );
    var sombraDeNumero = '0px 1px 0px #999, 0px 2px 0px #888, 0px 3px 0px #777, 0px 4px 0px #666, 0px 5px 0px #555, 0px 6px 0px #444, 0px 7px 0px #333, 0px 8px 7px #001135';
    var posX;
    var posXtexto;
    var anchoFondo2;
    var parrafo = consecutivo;
    var ajuste = 10;
    consecutivo++;//Para que la numeración empiece en 1
    var codigoID;
    var numeracionSVG = elemento;
    
    if(consecutivo === 2){
        //contenido = 'Había una vez una oruga';//1 renglón
        //contenido = 'Había una vez una oruga que quería ser mariposa,';//2 renglones
        //contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas.';//3 renglones
        //contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses.';//4 renglones
        //contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses. Y se encontraba muy desesperada y triste.';//5 renglones
        //contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses. Y se encontraba muy desesperada y triste. y la pobre oruga quería ser una linda y';//6 renglones
        //contenido = 'Había una vez una oruga que quería ser mariposa, tenía mucha ilusión de transformarse porque quería tener alas. Pasaron días, noches, semanas y meses;';//  hermosa mariposa con alas, además ser muy colorida, pero ese momento no llegaba.';
    }
    
    if(elemento.substring(0,5) === '#svgI' || elemento.substring(0,13) === '#svgcajaTempI'){
        posX = '-5';
        consecutivo += ' |';
        posXtexto = '34';
        anchoFondo2 = '336';
    }else{
        if(elemento.substring(0,5) === '#svgD' || elemento.substring(0,13) === '#svgcajaTempD'){
            posX = '340';
            consecutivo = '| '+consecutivo;
            posXtexto = '22';
            anchoFondo2 = '320';
        }
    }
    parrafo = idParrafo;
    numeracionSVG = numeracionSVG.replace('#svgDerecha', 'svgNumeracionDerecha').replace('#svgIzquierda', 'svgNumeracionIzquierda');
            
    if(imprimir === 'sinDatos'){
        consecutivo = '';
    }
    
    if(renglonesMaximo === 1 || renglonesMaximo === 2 || renglonesMaximo === 3){
        alturaTexto = 72;
    }else{
        alturaTexto = (renglonesMaximo+3) * 12;
    }
    alturaTexto = alturaTexto + ajuste;
            
    if( isNaN(alturaTexto) ){
        alturaTexto = 0;
        numeracionSVG = "sinId";
    }
    
    var objetoSVG = '<svg>';
    objetoSVG +=    '<foreignObject width="50" height="100" x="'+posX+'" y="16"><body><p id="'+numeracionSVG+'" style="font: 26px Arial; text-shadow: '+sombraDeNumero+';">'+consecutivo+'</p></body></foreignObject>';
    objetoSVG +=    '<foreignObject width="'+anchoFondo2+'"; height="'+alturaTexto+'px"; x="'+posXtexto+'"><body><p id="'+parrafo+'" style="white-space: normal; line-height: normal;">'+contenido+'</p></body></foreignObject>';
    objetoSVG +=    '</svg>';
    var parseado = Snap.parse( objetoSVG );
    var textoInsertado = svgUtilizado.group().append( parseado );
    
    $( '#desc').remove();
}
function sanitizarTexto (contenido){
    contenido = contenido.replace(/<p>/g, "");
    contenido = contenido.replace(/<\/p>/g, "");//Sanitizo último campo
    return contenido;
}
function generaBotonDraggableTemporal (cualCaja,i){
    var sombra;
    nuevoSVG = '<svg id="svg'+cualCaja+'" xmlns="http://www.w3.org/2000/svg" version="1.1" style="width: 100%;" height="92" focusable="false" fill="#54B084" ><path fill="'+colorNormalPieza+'" d="m0.27324,4.9154l0,67.97759c0,3.13333 2.11316,4.97552 5.24649,4.97324l375.05357,-0.27325c3.1333,-0.0023 4.39749,-2.93301 4.4267,-6.06622l0.27325,-29.31136c0.01239,-1.33327 -0.66684,-2 -2,-2l-5.14996,0c-1.13333,3.33334 -2.75003,5 -4.85004,5c-4,0 -6,-3.33333 -6,-10c0,-6.66666 2,-10 6,-10c2.10001,0 3.71649,1.66667 4.85004,5l5.14996,0c1.33316,0 2,-0.66666 2,-2l0,-23.29999c0,-3.13334 -1.56665,-4.7 -4.69995,-4.7l-375.60005,0c-3.13334,0 -4.7,1.56666 -4.7,4.7"/></svg>';
    formaSVG = nuevoSVG;
    
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        sombra = '';//No aplica la sombra
    }else{
        sombra = 'class="shadow"';//Aplica la sombra
    }
    
    $( '<div '+sombra+' style="width: 285px; height: 72px; transparent: true;">'+formaSVG+'</div>' ).attr( 'id', cualCaja ).appendTo( '#pilaDeCartasTemporal' ).draggable( {
        containment: '#content',
        stack: '#pilaDeCartasTemporal div',
        cursor: 'move',
        revert: true
    });
}
function generaBotonDroppable (cualCaja, i){
    formaSVG = obtieneSVG(cualCaja, i, colorNormalPiezaVacia);
    
    $( '<div style="width: 285px; height: 72px;">'+formaSVG+'</div>' ).attr( 'id', cualCaja ).appendTo( '#pilaDeCartasExtra' ).droppable( { //Áreas vacías
        containment: '#content',
        stack: '#pilaDeCartasExtra div',
        drop: sueltaPiezaArrastrar,
        over: sobrePieza,
        out: fueraPieza,
        create: alCrearPieza
    });
}
function generaBotonDraggable (cualCaja,i){
    var sombra;
    formaSVG = obtieneSVG(cualCaja, i, colorNormalPieza);        
    
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        sombra = '';//No aplica la sombra
    }else{
        sombra = 'class="shadow"';//Aplica la sombra
    }
    
    $( '<div '+sombra+' style="width: 285px; height: 72px; transparent: true;">'+formaSVG+'</div>' ).attr( 'cualFila', i ).attr( 'ligados', '' ).attr( 'haceLinea1', i ).attr( 'haceLinea2', i ).data( 'number', i).attr( 'id', cualCaja ).appendTo( '#pilaDeCartas' ).draggable( {
        containment: '#content',
        stack: '#pilaDeCartas div',
        cursor: 'move',
        revert: true,
        drag: muevePieza,
        start: iniciaPieza,
        stop: detienePieza,
        create: alCrearPieza
    });
}
function obtieneSVG (cualCaja, i, color) {
    var idTemp;
    var nuevoSVG = '';
    var relleno = 'fill="'+color+'"';
    var linea = 'stroke="'+colorContornoPieza+'"';        
    
    if(cualCaja.indexOf('Vacia', 0) !== -1){//Si es una caja vacía
        linea = '';//No necesita linea
    }
    
    if(cualCaja.substring(0,5) === 'cajaD'){
        if(cualCaja.indexOf('Vacia', 0) !== -1){//Si es una caja vacía
            idTemp = 'svgDerechaVacia'+i;
        }else{
            idTemp = 'svgDerecha'+i;
        }
        
        nuevoSVG = '<svg id="'+idTemp+'" xmlns="http://www.w3.org/2000/svg" version="1.1" style="width: 100%;" height="'+separacion+'" focusable="false"><path '+relleno+' '+linea;
        
        if(renglonesMaximo <= 3){//SVG Derecha de 1 a 3 renglones
            nuevoSVG += ' d="m17.69669,6.7c0,-3.13333 1.49675,-4.7 4.49025,-4.7l357.56388,0c6.07123,0 6.07507,11.05236 6.08249,9.84933l-0.31845,51.69601c-0.01926,3.12655 -0.38309,8.45466 -6.40097,8.45466l-356.92695,0c-2.9935,0 -4.49025,-1.56667 -4.49025,-4.7l0,-22.3c0,-1.33333 -0.63691,-2 -1.91074,-2l-3.00942,0c-1.30568,3.33333 -3.16865,5 -5.58893,5c-4.4584,0 -6.6876,-3.66667 -6.6876,-11c0,-7.33333 2.2292,-11 6.6876,-11c2.42028,0 4.28325,1.66667 5.58893,5l3.00942,0c1.27383,0 1.91074,-0.66667 1.91074,-2l0,-22.3"/>';
        }else{
            if(renglonesMaximo === 4){//SVG Derecha de 4 renglones'
                nuevoSVG += ' d="m18.06303,5.5c0,-3.13333 1.50252,-4.7 4.50757,-4.7l360.22185,0c3.005,0 4.50754,1.56667 4.50754,4.7l0,73.60001c0,3.13332 -1.50253,7.2 -4.50754,7.2l-360.22186,0c-3.00505,0 -4.50757,-4.56667 -4.50757,-7.7l0,-26.80001c0,-1.33333 -0.63937,-2 -1.91812,-2l-3.02103,0c-1.3107,3.33333 -3.18086,5 -5.61047,5c-4.4756,0 -6.7134,-3.66667 -6.7134,-11c0,-7.33333 2.2378,-11 6.7134,-11c2.42961,0 4.29977,1.66667 5.61048,5l3.02103,0c1.27875,0 1.91812,-0.66667 1.91812,-2l0,-30.3"/>';
            }
            if(renglonesMaximo === 5){//SVG Derecha de 5 renglones
                nuevoSVG += ' d="m17.7006,4.99999c0,-3.13333 1.48856,-4.7 4.46569,-4.7l356.87576,0c2.97711,0 4.46567,1.56667 4.46567,4.7l0,90.6c0,3.13333 -1.48856,7.19584 -4.46567,7.20001l-356.87576,0.5c-2.97713,0.004 -4.46569,-4.06668 -4.46569,-7.20001l0,-34.8c0,-1.33333 -0.63344,-2 -1.9003,-2l-2.99297,0c-1.29853,3.33333 -3.15132,5 -5.55837,5c-4.43402,0 -6.65104,-3.66667 -6.65104,-11c0,-7.33333 2.21702,-11 6.65104,-11c2.40705,0 4.25984,1.66667 5.55837,5l2.99297,0c1.26686,0 1.9003,-0.66667 1.9003,-2l0,-40.3"/>';
            }
            if(renglonesMaximo === 6){//SVG Derecha de 6 renglones
                nuevoSVG += ' d="m17.37449,4.8c0,-3.13333 1.49906,-4.7 4.49718,-4.7l359.39215,0c2.99811,0 4.49716,1.56667 4.49716,4.7l0,108.6c0,3.13333 -1.49905,7.69583 -4.49716,7.7l-359.39215,0.5c-2.99812,0.004 -4.49718,-4.06667 -4.49718,-7.2l0,-45.3c0,-1.33333 -0.6379,-2 -1.9137,-2l-3.01407,0c-1.3077,3.33333 -3.17355,5 -5.59756,5c-4.4653,0 -6.69794,-3.66667 -6.69794,-11c0,-7.33333 2.23264,-11 6.69794,-11c2.42401,0 4.28986,1.66667 5.59756,5l3.01407,0c1.27579,0 1.9137,-0.66667 1.9137,-2l0,-48.3"/>';
            }
        }
    }else{
        if(cualCaja.indexOf('Vacia', 0) !== -1){//Si es una caja vacía
            idTemp = 'svgIzquierdaVacia'+i;
        }else{
            idTemp = 'svgIzquierda'+i;
        }
        
        nuevoSVG = '<svg id="'+idTemp+'" xmlns="http://www.w3.org/2000/svg" version="1.1" style="width: 100%;" height="'+separacion+'" focusable="false"><path '+relleno+' '+linea;
        
        if(renglonesMaximo <= 3){//SVG Izquierda de 1 a 3 renglones            
            nuevoSVG += ' d="m0.6,5.7l0,60.6q0,4.7 4.7,4.7l375.60002,0q4.69998,0 4.69998,-4.7l0,-23.3q0,-2 -2,-2l-5.14999,0q-1.70001,5 -4.85001,5q-6,0 -6,-10q0,-10 6,-10q3.14999,0 4.85001,5l5.14999,0q2,0 2,-2l0,-23.3q0,-4.7 -4.69998,-4.7l-375.60002,0q-4.7,0 -4.7,4.7"/>';
        }else{
            if(renglonesMaximo === 4){//SVG Izquierda de 4 renglones
                nuevoSVG += ' d="m0.27324,4.9154l0.4,73.57758c0.01704,3.13329 1.31316,6.56884 4.44649,6.57325l374.25356,0.52676c3.1333,0.0044 6.48224,-2.53338 6.4267,-5.66621l-0.52673,-29.71135c-0.02362,-1.33313 -0.66669,-2 -2,-2l-5.14996,0c-1.13336,3.33333 -2.75003,5 -4.85004,5c-4,0 -6,-3.33334 -6,-10c0,-6.66667 2,-10 6,-10c2.10001,0 3.71667,1.66666 4.85004,5l5.14996,0c1.33331,0 2,-0.66667 2,-2l0,-31.3c0,-3.13334 -1.56665,-4.7 -4.69995,-4.7l-375.60005,0c-3.13333,0 -4.7,1.56666 -4.7,4.7"/>';
            }
            if(renglonesMaximo === 5){//SVG Izquierda de 5 renglones
                nuevoSVG += ' d="m0.27324,4.9154l0.4,91.17758c0.01375,3.1333 1.31318,5.76214 4.44649,5.77325l374.25356,1.32676c3.13327,0.01111 6.47046,-2.13319 6.4267,-5.26622l-0.52673,-37.71136c-0.01862,-1.3332 -0.66669,-2 -2,-2l-5.14996,0c-1.13336,3.33333 -2.75003,5 -4.85004,5c-4,0 -6,-3.33333 -6,-10c0,-6.66667 2,-10 6,-10c2.10001,0 3.71667,1.66667 4.85004,5l5.14996,0c1.33331,0 2,-0.66667 2,-2l0,-41.3c0,-3.13333 -1.56665,-4.7 -4.69995,-4.7l-375.60005,0c-3.13333,0 -4.7,1.56667 -4.7,4.7"/>';
            }
            if(renglonesMaximo === 6){//SVG Izquierda de 6 renglones
                nuevoSVG += ' d="m0.27324,4.9154l0.4,111.97758c0.0112,3.13331 1.31316,4.97889 4.44649,4.97325l374.25356,-0.67326c3.1333,-0.006 6.46072,-1.33306 6.4267,-4.46621l-0.52673,-48.51136c-0.01453,-1.33326 -0.66669,-2 -2,-2l-5.14996,0c-1.13336,3.33333 -2.75003,5 -4.85004,5c-4,0 -6,-3.33334 -6,-10c0,-6.66667 2,-10 6,-10c2.10001,0 3.71667,1.66666 4.85004,5l5.14996,0c1.33331,0 2,-0.66667 2,-2l0,-49.3c0,-3.13333 -1.56665,-4.7 -4.69995,-4.7l-375.60005,0c-3.13333,0 -4.7,1.56667 -4.7,4.7"/>';
            }
        }
    }
    nuevoSVG += '</svg>';
    return nuevoSVG;
}
function obtieneInterlineado(numeroMaximo) {
    var interlineado;
    if(numeroMaximo <= 3){
        interlineado = 100;
    }else{
        if(numeroMaximo === 4){
            interlineado = 110;
        }else{
            if(numeroMaximo === 5){
                interlineado = 122;
            }else{
                if(numeroMaximo === 6){
                    interlineado = 142;
                }
            }
        }
    }
    return interlineado;
}
function obtieneSeparacion(numeroMaximo) {
    var valor;
    if(numeroMaximo <= 3){
        valor = 74;
    }else{
        if(numeroMaximo === 4){
            valor = 88;
        }else{
            if(numeroMaximo === 5){
                valor = 104;
            }else{
                if(numeroMaximo === 6){
                    valor = 122;
                }
            }
        }
    }
    return valor;
}
function alCrearPieza(event, ui) {
    var cualCaja = event.target.id;
    var id = parseInt( cualCaja.replace('cajaIzquierda', '').replace('cajaDerecha', '').replace('Vacia', '') );
    var posicionX;
    var interlineadoPiezas = obtieneInterlineado(renglonesMaximo);
    var ajuste = id * interlineadoPiezas;//Verificar el ajuste, no debería aplicarse
    var posicionTopInicial = $( '#'+cualCaja ).position().top + ajuste;
    //console.log('ACTUALIZA '+'#'+cualCaja + ' --- ' + $( '#'+cualCaja ).position().left + ' --- ' + posicionTopInicial );
    separacion = obtieneSeparacion(renglonesMaximo);
    
    posicionesTopIniciales_arr.push(posicionTopInicial);
    if(cualCaja.substring(0,5) === 'cajaD'){
        posicionX = posLeftInicialDerecha;
    }else{
        posicionX = posLeftInicialIzquierda;
    }
        
    $( '#'+cualCaja ).css({left:posicionX, top:posicionTopInicial, position:'absolute'});
    actualizaPosiciones ( '#'+cualCaja, $( '#'+cualCaja ).position().left, posicionTopInicial );        
}
function actualizaPosiciones (elemento, valorLeft, valorTop) {
    $( elemento ).data({
        'originalLeft': valorLeft,
        'originalTop': valorTop
    });
}
function reseteaPosiciones (elemento) {    
    $( elemento ).css({
        'left': $( elemento ).data('originalLeft'),
        'top':  $( elemento ).data('originalTop')
    });
}
function detienePieza(event, ui) {
    var elementoArrastrado = event.target.id;
    var elementoPar2;
    var vacio;
    var valorLeft;
    var valorTop;
    var pareja;
    var vacioTemporal;
    
    console.log('STOP de: '+elementoArrastrado+' TRAE LIGADOS: '+$( '#'+elementoArrastrado ).attr('ligados')+'  hizoDrop: '+hizoDrop);
    
    if( hizoDrop === false && $('#'+elementoArrastrado ).attr('ligados') !== ''){//Entra si hay que desligar
        console.log('IF de ligados');
        elementoPar2  = $( '#'+elementoArrastrado ).attr('ligados');//Obtiene los dos elementos ligados
                                
        if(elementoArrastrado.substring(0,5) === 'cajaD'){
            console.log('**** if');
            elementoPar2 = $( '#'+elementoArrastrado ).attr('haceLinea1');
            pareja = elementoPar2;
            pareja = 'cajaIzquierda'+pareja;
            reseteaPosiciones( '#cajaIzquierda'+elementoPar2 );//Regresa la Caja CONTRARIA liberada 2 (izq o der) a su posición inicial al ser desligada
            $( '#cajaIzquierda'+elementoPar2 ).attr('ligados', '');//Se limpian los ligados
            reseteaSombra( '#cajaIzquierda'+elementoPar2 );
            
            vacioTemporal = '#cajaIzquierda'+elementoPar2;
            vacioTemporal = vacioTemporal.replace('Izquierda', 'IzquierdaVacia');
            mostrarPieza( vacioTemporal );
        }else{
            console.log('**** else');
            elementoPar2 = $( '#'+elementoArrastrado ).attr('haceLinea2');
            pareja = elementoPar2;
            pareja = 'cajaDerecha'+pareja;
            console.log('la caja contraria es: '+'#cajaDerecha'+elementoPar2);
            reseteaPosiciones( '#cajaDerecha'+elementoPar2 );//Regresa la Caja CONTRARIA liberada 2 (izq o der) a su posición inicial al ser desligada - originalLeft - originalTop
            $( '#cajaDerecha'+elementoPar2 ).attr('ligados', '');//Se limpian los ligados
            reseteaSombra( '#cajaDerecha'+elementoPar2 );
            
            vacioTemporal = '#cajaDerecha'+elementoPar2;
            vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia');
            mostrarPieza( vacioTemporal );
        }
        reseteaPosiciones( '#'+elementoArrastrado );//Regresa la Caja DRAGGABLE liberada 1 (izq o der) a su posición inicial al ser desligado
        $( '#'+elementoArrastrado ).draggable( 'option', 'revert', true );//Regresa el reverse de la caja DRAGGABLE
        $( '#'+pareja).draggable( 'option', 'revert', true );//Elimina el reverse de la caja DROPPABLE
        $( '#'+elementoArrastrado ).attr('ligados', '');//Se limpian los ligados
        reseteaSombra( '#'+elementoArrastrado );
        
        vacioTemporal = '#'+elementoArrastrado;
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        mostrarPieza( vacioTemporal );
        
        if(datosRelacionaArrastra.length > 0){//Si hay datos guardados (status interrumpido)
        
        }
        
                
        
    }
    
    if(activaGuardado){
        console.log('llama a guardaStatusInterrumpido dentro de STOP si está activado');
        guardaStatusInterrumpido();
        activaGuardado = false;
    }
    
}
function guardaStatusInterrumpido() {    
    var status_arr = new Array();
    var datos;
    var lado1;
    var lado2;
    var unido = 0;    
    var t=0;
    
    posicionesTopIniciales_arr = posicionesTopIniciales_arr.unique();
    
    for ( var i=0; i<preguntasArr.length; i++ ) {
        unido = 0;        
        for (var t=0; t<posicionesTopIniciales_arr.length; t++ ) {                        
            if( $( '#cajaIzquierda'+t ).data('originalTop') === posicionesTopIniciales_arr[i]){                
                lado1 = t; 
                if( $( '#cajaIzquierda'+t ).position().left === posXUnidaIzquierda){                
                    unido = 1;                
                }                
            }            
            if( $( '#cajaDerecha'+t ).data('originalTop') === posicionesTopIniciales_arr[i]){                
                lado2 = t;                
            }
        }
        datos = lado1 + '-' + lado2 + '|' + unido;        
        status_arr[i] = datos;
    }
    console.log('GUARDAR: '+status_arr);
}
function iniciaPieza(event, ui) {
    hizoDrop = false;
}
function sobrePieza (event, ui) {
    //console.log('OVER '+event.target.id+' DROPPABLE: '+ui.draggable.attr('id'));
    var cadena1 = event.target.id;
    var cadena2 = ui.draggable.attr('id');
    cadena1 = cadena1.substring(0,5);
    cadena2 = cadena2.substring(0,5);
    
    if(cadena1 !== cadena2){//Si hace over sobre las piezas de la otra columna
        var cajaAPintar = event.target.id;
        cajaAPintar = cajaAPintar.replace("Vacia", "");//Soluciona problema de espacio en blanco
        pintaPieza( "#"+cajaAPintar, colorSobrePieza );
    }
}
function fueraPieza (event, ui) {
    //console.log('OUT: '+event.target.id+' ui.droppable: '+ui.droppable);
    var cajaAPintar = event.target.id;
    cajaAPintar = cajaAPintar.replace("Vacia", "");//Soluciona problema de espacio en blanco
    pintaPieza( "#"+cajaAPintar, colorNormalPieza );
}
function pintaPieza ( pieza, color ) {
    $( pieza ).contents().find("path").attr({"fill":color});
}
function transparentaPieza ( pieza ) {
    $( pieza ).css('background', 'transparent');
    $( pieza ).css('border-color', 'transparent');
}
function opacarPieza ( pieza ) {
    $( pieza ).css('opacity', '0');
}
function mostrarPieza ( pieza ) {
    $( pieza ).css('opacity', '1');
}
function posicionaPieza ( pieza, left ) {
    $( pieza ).css('left', left);
}
function anchoPieza ( pieza, ancho ) {
    $( pieza ).css('width', ancho);
}
function zIndexPieza ( pieza, zindex ) {
    $( pieza ).css('z-index', zindex);
}
function aplicaSombra ( elemento ) {
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        //No aplica la sombra
    }else{
        $( elemento ).removeClass('shadow');//Aplica la sombra
        $( elemento ).addClass('shadow2');
    }
}
function reseteaSombra ( elemento ) {
    if( esMozillaCanonical() ){//Si corre en Linux con Ubuntu Canonical
        //No aplica la sombra
    }else{
        $( elemento ).removeClass('shadow2');
        $( elemento ).addClass('shadow');
    }
}
function sueltaPiezaArrastrar ( event, ui ) {
    var cadena;
    var valor1;
    var valor2;
    var numeroId = 0;
    var cajaId = '';
    var cadena1 = event.target.id;
    var cadena2 = ui.draggable.attr('id');
    var droppableVacio2 = event.target.id;
    var datoFila1;
    var datoFila2;
    var cajacontigua;
    var cajaContraria;
    var cajaDroppada;
    var cajaVaciaDesplazada;
    var datoContiguo;
    var cajaADesplazar;
    var cajaUnida;
    var idAcomparar;
    var vacioTemporal;
        
    cadena1 = cadena1.replace('Vacia', '');
    console.log('sueltaPiezaArrastrar: draggable: '+cadena2+' VS droppable: '+cadena1);
    
    if(cadena1.substring(0,5) === cadena2.substring(0,5)){//Si hace drop en la misma columna
        console.log('columna igual.... no hace nada');//No hace nada
        hizoDrop = false;//Seguimiento
        if($( "#"+cadena1 ).hasClass('shadow2')){//Si ya era una caja resuelta
            //Hay que posicionar la caja a los valores iniciales
        }
    }else{//Si hace drop en una columna diferente
        console.log('columna diferente... verifica el drop');
        
        if(cadena1.substring(0,5) === 'cajaD'){
            valor1 = posXUnidaIzquierda+'px';     valor2 = posXUnidaDerecha+'px';
            zIndexPieza ( '#'+cadena1, '3' );
            zIndexPieza ( '#'+cadena2, '4' );
            cajaId = 'cajaIzquierda';
            numeroId = cadena1.substring(11,cadena1.length);
            console.log('************* numeroId: '+numeroId);
            numeroId = +$( '#'+cadena1 ).attr('haceLinea1');
            console.log('************* id arreglado: '+numeroId);
        }else{
            valor1 = posXUnidaDerecha+'px';    valor2 = posXUnidaIzquierda+'px';
            zIndexPieza ( '#'+cadena1, '4' );
            zIndexPieza ( '#'+cadena2, '3' );
            cajaId = 'cajaDerecha';
            numeroId = cadena1.substring(13,cadena1.length);
            console.log('************* numeroId: '+numeroId);
            numeroId = +$( '#'+cadena1 ).attr('haceLinea2');
            console.log('************* id arreglado: '+numeroId);
        }
        
        ui.draggable.position( { of: $(this), my: 'left top', at: 'left top' } );//Posiciona el DRAGGABLE en el centro y elimina su reverse.
        ui.draggable.draggable( 'option', 'revert', false );//Elimina el reverse de la caja DRAGGABLE
        $( '#'+cadena1).draggable( 'option', 'revert', false );//Elimina el reverse de la caja DROPPABLE
        
        datoFila1 = ui.draggable.attr('cualFila');
        datoFila2 = $( '#'+cadena1).attr('cualFila');
        ui.draggable.attr('ligados', datoFila1+'-'+datoFila2);
        $( '#'+cadena1).attr('ligados', datoFila2+'-'+datoFila1);
        
        console.log('SETEA CAJA DESPLAZADA: '+'#'+cajaId+numeroId+' CON LA FILA: '+datoFila1);
        $( '#'+cajaId+numeroId).attr('cualFila', datoFila1);//Setea caja desplazada con el id del draggable
                        
        datoFila1 = numeroId;//Actualiza la fila actual a la que se mueve la caja
        console.log('SETEAR CAJA DRAGGABLE CON LA FILA: '+datoFila1);
        ui.draggable.attr('cualFila', datoFila1);
            
        cajaDroppada = droppableVacio2.replace('Vacia', '').replace('Vacia', '');
        var cajaDraggadaVacia = ui.draggable.attr('id');
        cajaDraggadaVacia = cajaDraggadaVacia.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
            
        //SE POSICIONAN LAS PIEZAS 
            
        //C A J A    V E R D E    D E S P L A Z A D A  ( POSICIONAMIENTO )
        var reemplazadaActualizadaLeft = $( '#'+cajaId+numeroId ).data('originalLeft');
        var reemplazadaActualizadaTop = $( '#'+ui.draggable.attr('id') ).data('originalTop');
        //console.log('>>>>>> desplazado: '+'#'+cajaId+numeroId + ' a donde se mueve: '+'#'+ui.draggable.attr('id')+' TOP '+ $( '#'+ui.draggable.attr('id') ).position().top  + ' originalTop: '+reemplazadaActualizadaTop);
        $( '#'+cajaId+numeroId ).css({left:reemplazadaActualizadaLeft, top:reemplazadaActualizadaTop, position:'absolute'});//Posiciona la caja verde DESPLAZADA
            
        //C A J A    G R I S     D E S P L A Z A D A   V A C Í A ( POSICIONAMIENTO )
        //console.log('>>>>>> desplazado vacio: '+'#'+cajaId+'Vacia'+numeroId + ' a donde se mueve: '+'#'+ui.draggable.attr('id')+' TOP '+ $( '#'+ui.draggable.attr('id') ).position().top  + ' originalTop: '+reemplazadaActualizadaTop);
        $( '#'+cajaId+'Vacia'+numeroId ).css({left:reemplazadaActualizadaLeft, top:reemplazadaActualizadaTop, position:'absolute'});//Posiciona la caja gris DESPLAZADA vacía
        //console.log(' POSICIONA VACÍA: '+'#'+cajaId+'Vacia'+numeroId);
        
        //C A J A    V E R D E    D R A G G A B LE ( POSICIONAMIENTO )
        var draggadaActualizadaLeft = $( '#'+ui.draggable.attr('id') ).data('originalLeft');
        var draggadaActualizadaTop = $( '#'+cajaDroppada ).data('originalTop');
        //console.log('>>>>>> dragueada: '+'#'+ui.draggable.attr('id') + ' a donde se mueve: '+'#'+cajaDroppada+' TOP '+ $( '#'+cajaDroppada ).position().top  + ' originalTop: '+draggadaActualizadaTop);
        $( '#'+ui.draggable.attr('id') ).css({left:draggadaActualizadaLeft, top:draggadaActualizadaTop, position:'absolute'});//Posiciona la caja DRAGGABLE
            
        //C A J A    G R I S    D R A G G A B LE ( POSICIONAMIENTO )
        var draggadaActualizadaLeft = $( '#'+ui.draggable.attr('id') ).data('originalLeft');
        var draggadaActualizadaTop = $( '#'+cajaDroppada ).data('originalTop');
        console.log('>>>>>> dragueada vacía: '+'#'+cajaDraggadaVacia + ' a donde se mueve: '+'#'+cajaDroppada+' TOP '+ $( '#'+cajaDroppada ).position().top  + ' originalTop: '+draggadaActualizadaTop);
        $( '#'+cajaDraggadaVacia ).css({left:draggadaActualizadaLeft, top:draggadaActualizadaTop, position:'absolute'});//Posiciona la caja DRAGGABLE
        
        //Almacena elementos que hacen línea
        console.log('Obtiene caja contraria '+droppableVacio2);
        if(droppableVacio2.substring(0,5) === 'cajaD'){//Si es de Izquierda a Derecha
            console.log('IF');
            cajaUnida = '#cajaDerecha'+ui.draggable.attr('haceLinea2');//Obtiene la caja que se queda unida al mover su elemento ligado
                
            idAcomparar = ui.draggable.attr('haceLinea2');
            posicionaPieza( '#cajaDerecha'+idAcomparar, $( '#cajaDerecha'+idAcomparar ).data('originalLeft') );//POSICIONA EN X EL DROPPABLE
                
            $( '#'+cajaId+numeroId).attr('haceLinea1', numeroId);//Almacena en la caja DESPLAZADA los elementos que hacen línea
            datoContiguo = $( '#'+ ui.draggable.attr('id') ).attr('haceLinea2');
            $( '#'+cajaId+numeroId).attr('haceLinea2', datoContiguo);//Almacena en la caja DESPLAZADA los elementos que hacen línea
                
            datoContiguo = $( ui.draggable ).attr('haceLinea1');
            $( '#'+cajaDroppada ).attr('haceLinea1', datoContiguo);//Almacena en la caja DROPPADA los elementos que hacen línea
                
            $( ui.draggable ).attr('haceLinea1', ui.draggable.data('number'));//Almacena en el DRAGGABLE los elementos que hacen línea
            datoContiguo = $( '#'+cajaDroppada ).attr('haceLinea2');
            $( ui.draggable ).attr('haceLinea2', datoContiguo);//Almacena en el DRAGGABLE los elementos que hacen línea
                
            cajaVaciaDesplazada = ui.draggable.attr('id');
            cajaVaciaDesplazada = cajaVaciaDesplazada.replace('Izquierda', 'IzquierdaVacia').replace('Derecha', 'DerechaVacia');
            //console.log('cajaVaciaDesplazada: '+'#'+cajaVaciaDesplazada);
                
            //console.log('--- Posiciona Caja desplazada '+'#'+cajaId+numeroId);
            cajaADesplazar = $( '#'+cajaId+numeroId ).attr('haceLinea1');
            //console.log('cajaADesplazar '+cajaADesplazar);
                
            cajacontigua = $( '#'+cajaId+numeroId ).attr('haceLinea2');
            cajaContraria = 'cajaDerecha'+cajacontigua;
            $( '#'+cajaContraria ).attr('haceLinea1', cajaADesplazar);//Almacena en la caja CONTIGUA los elementos que hacen línea
        }else{//Si es de Derecha a Izquierda
            console.log('ELSE');
            cajaUnida = '#cajaDerecha'+ui.draggable.attr('haceLinea1');//Obtiene la caja que se queda unida al mover su elemento ligado
              
            idAcomparar = ui.draggable.attr('haceLinea1');
            posicionaPieza( '#cajaIzquierda'+idAcomparar, $( '#cajaIzquierda'+idAcomparar ).data('originalLeft') );//POSICIONA EN X EL DROPPABLE
                
            $( '#'+cajaId+numeroId).attr('haceLinea2', numeroId);//Almacena en la caja DESPLAZADA los elementos que hacen línea
            datoContiguo = $( '#'+ ui.draggable.attr('id') ).attr('haceLinea1');
            $( '#'+cajaId+numeroId).attr('haceLinea1', datoContiguo);//Almacena en la caja DESPLAZADA los elementos que hacen línea
                
            datoContiguo = $( ui.draggable ).attr('haceLinea2');
            $( '#'+cajaDroppada ).attr('haceLinea2', datoContiguo);//Almacena en la caja DROPPADA los elementos que hacen línea
                
            $( ui.draggable ).attr('haceLinea2', ui.draggable.data('number'));//Almacena en el DRAGGABLE los elementos que hacen línea
            datoContiguo = $( '#'+cajaDroppada ).attr('haceLinea1');
            $( ui.draggable ).attr('haceLinea1', datoContiguo);//Almacena en el DRAGGABLE los elementos que hacen línea
                
            cajaVaciaDesplazada = ui.draggable.attr('id');
            cajaVaciaDesplazada = cajaVaciaDesplazada.replace('Izquierda', 'IzquierdaVacia').replace('Derecha', 'DerechaVacia');
            //console.log('cajaVaciaDesplazada: '+'#'+cajaVaciaDesplazada);
                
            //console.log('--- Posiciona Caja desplazada '+'#'+cajaId+numeroId);
            cajaADesplazar = $( '#'+cajaId+numeroId ).attr('haceLinea2');
            //console.log('cajaADesplazar '+cajaADesplazar);
                                                                                
            cajacontigua = $( '#'+cajaId+numeroId ).attr('haceLinea1');
            cajaContraria = 'cajaIzquierda'+cajacontigua;
            $( '#'+cajaContraria ).attr('haceLinea2', cajaADesplazar);//Almacena en la caja CONTIGUA los elementos que hacen línea
        }
            
        // ************  SE ALMACENAN LOS DATOS **************************
        //C A J A    V E R D E    D E S P L A Z A D A  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+cajaId+numeroId , reemplazadaActualizadaLeft, reemplazadaActualizadaTop);//Actualiza datos de la caja verde desplazada
            
        //C A J A    G R I S     D E S P L A Z A D A  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+cajaId+'Vacia'+numeroId , reemplazadaActualizadaLeft, reemplazadaActualizadaTop);//Actualiza datos de la caja gris desplazada
            
        //C A J A    V E R D E    A R R A S T R A D A  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+ui.draggable.attr('id') , draggadaActualizadaLeft,  draggadaActualizadaTop);//Actualiza datos de la caja verde arrastrada
            
        //C A J A    G R I S    D R A G G A B LE  ( ALMACENAMIENTO )
        actualizaPosiciones ( '#'+cajaDraggadaVacia , draggadaActualizadaLeft,  draggadaActualizadaTop);//Actualiza datos de la caja gris arrastrada
        
        posicionaPieza( '#'+ui.draggable.attr('id'), valor1 );//POSICIONA EN X EL DRAGGABLE
        posicionaPieza( '#'+cadena1, valor2 );//POSICIONA EN X EL DROPPABLE
        aplicaSombra( '#'+ui.draggable.attr('id') );//APLICA LA SOMBRA FUSIONABLE EN EL DRAGGABLE
        aplicaSombra( '#'+cadena1 );//APLICA LA SOMBRA FUSIONABLE EN EL DROPPABLE
        
        vacioTemporal = ui.draggable.attr('id');
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        opacarPieza( '#'+vacioTemporal );
        
        vacioTemporal = cadena1, valor2;
        vacioTemporal = vacioTemporal.replace('Derecha', 'DerechaVacia').replace('Izquierda', 'IzquierdaVacia');
        opacarPieza( '#'+vacioTemporal );
        
        pintaPieza( "#"+cadena1, colorNormalPieza );//PINTA EN VERDE NORMAL EL DROPPABLE
        hizoDrop = true;
        
        
        console.log('activaGuardado dentro de Soltar la caja');
        activaGuardado = true;        
    }
}
var activaGuardado = false;
function muevePieza (event, ui) {
    /*var elementoArrastrado = event.target.id;
    var hit;
    for(var f=0;f<preguntasArr.length;f++){
        hit = $( '#'+elementoArrastrado ).hitTestObject($( '#svgDerecha'+f ));
        if(hit){
            pintaPieza( "#cajaDerecha"+f, colorSobrePieza );
        }else{
            pintaPieza( "#cajaDerecha"+f, colorNormalPieza );
        }
    }*/
}
function calcularLineas($element) {
    var htmlOriginal = $element.html();
    var palabras = htmlOriginal.split(" ");
    var posicionesDeLineas_arr = [];

    for (var i in palabras) {
        palabras[i] = "<span>" + palabras[i] + "</span>";
    }
    $element.html(palabras.join(" "));
    $element.children("span").each(function() {
        var posicionTemp = $(this).position().top;
        if (posicionesDeLineas_arr.indexOf(posicionTemp) == -1) posicionesDeLineas_arr.push(posicionTemp);
    });
    return posicionesDeLineas_arr.length;
}
function centrarTextosEnAltura (idElemento) {
    var alturaSVGcontenedor;
    var posY;
    var posYnumeracion;
    var cuantasLineas;
    var idSVGcontenedor = idElemento;
    var idSVGnumerador = idElemento;
    idSVGcontenedor = idSVGcontenedor.replace('#parrafoIzquierdaTemp', '#svgIzquierda').replace('#parrafoDerechaTemp', '#svgDerecha').replace('#parrafoIzquierda', '#svgIzquierda').replace('#parrafoDerecha', '#svgDerecha');
    idSVGnumerador = idSVGnumerador.replace('#parrafoIzquierdaTemp', '#svgNumeracionIzquierda').replace('#parrafoDerechaTemp', '#svgNumeracionDerecha').replace('#parrafoIzquierda', '#svgNumeracionIzquierda').replace('#parrafoDerecha', '#svgNumeracionDerecha');
    
    alturaSVGcontenedor = $( idSVGcontenedor ).height();
    cuantasLineas = calcularLineas( $( idElemento ) );
    renglones_arr[posRenglones] = cuantasLineas;
    posRenglones++;
    
    if( alturaSVGcontenedor === null){
    }else{
        if(renglonesMaximo <= 3){
            posY = 36 - (cuantasLineas*10);
            posYnumeracion = 0;
        }else{
            alturaSVGcontenedor = renglonesMaximo * 20;
            posY = alturaSVGcontenedor / 2;
            posYnumeracion = (alturaSVGcontenedor - 50) / 4;
            posY = posY - (10 * cuantasLineas);
        }
        
        //console.log(idElemento + ' tiene: ' + cuantasLineas + '   posY: '+posY);
        $( idElemento ).css('padding', posY+'px 2.1% 1px 0%');//Centra verticalmente los textos de contenido
        $( idSVGnumerador ).css('padding', posYnumeracion+'px 2.1% 1px 0%');//Centra verticalmente
    }
}
function eliminaSVGtexto (texto, elemento) {    
    if(texto.indexOf('<img', 0) !== -1){//Si el contenido de la izquierda o derecha trae una imagen
        $( elemento ).remove();//Elimina el svg del texto
    }
}
function eliminaBordesBlancos (elemento) {
    $( elemento ).css('border', 'none');//Se eliminan los bordes blancos
}
function ajustaAnchoMinimo ( elemento ) {
    $( elemento ).css('min-width', '322px');//Ajusta el ancho mínimo
}
function eliminaFondo ( elemento ) {
    $( elemento ).css('background', 'none');//Ajusta el ancho mínimo
}
function esMozillaCanonical (  ) {
    var canonical = false;
    navegador=get_browser();
    navegador_version=get_browser_version();
    versionInt = parseInt(navegador_version);
        
    if(navegador === 'Firefox'){
        if(versionInt <= 32){
            canonical = true;
        }
    }
    return canonical;
}
$.fn.hitTestObject = function(selector){
    var compares = $(selector);
    var l = this.size();
    var m = compares.size();
    for(var i = 0; i < l; i++){
        var bounds = this.get(i).getBoundingClientRect();
        for(var j = 0; j < m; j++){
            var compare = compares.get(j).getBoundingClientRect();
            if(!(bounds.right < compare.left ||
                bounds.left > compare.right ||
                bounds.bottom < compare.top ||
                bounds.top > compare.bottom)){
        	return true;
            }
        }
    }
    return false;
};

function creaRelacionaArrastrar () {
    var texto = '';
    var texto2 = '';
    var dimensiones = new Object();
    var alturaActual;
            
    //datosRelacionaArrastra = new Array('2-0|0','1-1|0','0-2|1','3-3|0');//Recibe los datos del status interrumpido
    //datosRelacionaArrastra = new Array('2-0|0','0-1|1','1-2|0','3-3|1');//Recibe los datos del status interrumpido
    //datosRelacionaArrastra = new Array('0-0|1','3-1|1','1-2|0','2-3|1');//Recibe los datos del status interrumpido    
        
    $('#pilaDeCartasExtra').html( '' );
    $('#pilaDeCartas').html( '' );
    zIndexPieza( '#pilaDeCartas', '4' );
    zIndexPieza( '#pilaDeCartasExtra', '3' );
    
    posicionesTopIniciales_arr = new Array();
    
    for ( var i=0; i<preguntasArr.length; i++ ) {//Obtiene el número mayor de renglones
        texto = sanitizarTexto(preguntasArr[i]);
        texto2 = sanitizarTexto(respuestasArr[i]);
        
        if(texto.indexOf('<img', 0) !== -1 || texto2.indexOf('<img', 0) !== -1){//Si al menos una respuesta trae una imagen
            renglonesMaximo = 6;//Se setean siempre 6 renglones como contenedor máximo cuando se cargue al menos una imagen
        }
        
        generaBotonDraggableTemporal('cajaTempIzquierda'+i,i);
        generaBotonDraggableTemporal('cajaTempDerecha'+i,i);
        creaTextos('#svgcajaTempIzquierda'+i, texto, i, '', 'parrafoIzquierdaTemp'+i);
        creaTextos('#svgcajaTempDerecha'+i, texto2,i, '', 'parrafoDerechaTemp'+i);
        centrarTextosEnAltura( '#parrafoIzquierdaTemp'+i);
        centrarTextosEnAltura( '#parrafoDerechaTemp'+i);
    }
    renglonesMaximo = Math.max.apply( Math, renglones_arr );
    $( "#pilaDeCartasTemporal" ).remove();
    
    for ( var i=0; i<preguntasArr.length; i++ ) {
        texto = sanitizarTexto(preguntasArr[i]);
        texto2 = sanitizarTexto(respuestasArr[i]);
        
        if(texto.indexOf('<img', 0) !== -1 || texto2.indexOf('<img', 0) !== -1){//Si al menos una respuesta trae una imagen
            renglonesMaximo = 6;//Se setean siempre 6 renglones como contenedor máximo cuando se cargue al menos una imagen
        }
        texto = texto.replace('<div', '<div style="line-height: '+alturaTexto+'px; height: '+alturaTexto+'px; vertical-align: middle;" ');
        
        generaBotonDroppable('cajaIzquierdaVacia'+i, i);
        generaBotonDroppable('cajaDerechaVacia'+i, i);
        generaBotonDraggable('cajaIzquierda'+i, i);
        generaBotonDraggable('cajaDerecha'+i, i);
                
        creaTextos('#svgIzquierda'+i, texto, i, '', 'parrafoIzquierda'+i);//creaTextos('#svgIzquierdaVacia'+i, 'num'+i, i, '', 'parrafoIzquierda'+i);
        creaTextos('#svgDerecha'+i, texto2,i, '', 'parrafoDerecha'+i);//creaTextos('#svgDerechaVacia'+i, 'num'+i,i, '', 'parrafoDerecha'+i);
        
        centrarTextosEnAltura( '#parrafoIzquierda'+i);
        centrarTextosEnAltura( '#parrafoDerecha'+i);
        eliminaSVGtexto(texto, '#parrafoIzquierda'+i);//Elimina el svg del texto si contiene una imagen
        eliminaSVGtexto(texto2, '#parrafoDerecha'+i);//Elimina el svg del texto si contiene una imagen
                        
        transparentaPieza('#cajaIzquierda'+i);
        transparentaPieza('#cajaDerecha'+i);
        transparentaPieza('#cajaIzquierdaVacia'+i);
        transparentaPieza('#cajaDerechaVacia'+i);
        
        anchoPieza( '#cajaIzquierda'+i, '40%' );
        anchoPieza( '#cajaDerecha'+i, '40%' );
        anchoPieza( '#cajaIzquierdaVacia'+i, '40%' );
        anchoPieza( '#cajaDerechaVacia'+i, '40%' );
        
        $( '#cajaDerecha'+i ).mousedown(function(event) {
            console.log(' ESTE ELEMENTO: '+$( this ).attr('id') + ' LIGA: ' + $( this ).attr('haceLinea1') + ' CON ' + $( this ).attr('haceLinea2'))   //haceLinea
        });
                                    
        $( '#cajaIzquierda'+i ).mousedown(function(event) {
            console.log(' ESTE ELEMENTO: '+$( this ).attr('id') + ' LIGA: ' + $( this ).attr('haceLinea1') + ' CON ' + $( this ).attr('haceLinea2'))   //haceLinea
        });
    }
    
    if(datosRelacionaArrastra.length > 0){//Si hay datos guardados (status interrumpido)
        preparaReacomodoDePiezas(datosRelacionaArrastra);
    }
    
    eliminaBordesBlancos( '#div' );
    eliminaBordesBlancos( '#pilaDeCartas div' );
    eliminaBordesBlancos( '#pilaDeCartas' );
    ajustaAnchoMinimo( '#pilaDeCartas div' );
    eliminaFondo( '#div' );
    eliminaFondo( '#pilaDeCartas div' );
    eliminaFondo( '#pilaDeCartas' );
        
    separacionPiezas = 0;
    $( '#pilaDeCartas div' ).css('margin', separacionPiezas+'px');//Separación entre piezas
    $( '#pilaDeCartasExtra div' ).css('margin', separacionPiezas+'px');//Separación entre piezas
                    
    alturaActual = (alturaTexto * preguntasArr.length) + 160;
    $('#content').height( alturaActual );
    $('#respuestasRadialGroup').height( $('#content').height() + 80 );// Hack para aumentar el tamaño del div content
}
