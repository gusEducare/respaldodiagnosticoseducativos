$(document).ready(function(){   
    /**Ocultar el boton de finalizar **/
    $('#button-finalizar').attr("disabled", true);
    $('#button-finalizar').addClass("disabled");
    $('#button-finalizar').hide();
    //$('#button-finalizar').css("background-color","#5D9FE2");
    //$('#button-finalizar').css("color","#FFFFFF");
    // Cambio el tamano del div a lo ancho de la pantalla
    var altoExamen = $(window).height() - $('.naveg-examen').height() - 
                        $('.encabezado-examen').height() - 
                        $('.detalle_exa').height() - 
                        $('.encabezado-instrucciones').height() - 
                        $('.encabezado-ayuda').height() - 200;
    //$('.border-exa').height(altoExamen);
   // $('#respuestasRadialGroup').height('250');
    $(window).on('resize', function(){altoExamen = $(window).height() - $('.naveg-examen').height() - 
                        $('.encabezado-examen').height() - 
                        $('.detalle_exa').height() - 
                        $('.encabezado-instrucciones').height() - 
                        $('.encabezado-ayuda').height() -200;
        //$('.border-exa').height(altoExamen);
        //$('#respuestasRadialGroup').height('250');
    });
    
    // Agrego los tooltips
//    $('#button-finalizar').tooltip({
//                               content: 'Presiona aqui para finalizar la evaluacion'
//                            });
    $('.btn_cancelar').on('click', function(){
        $('a.close-reveal-modal').trigger('click');
    });
    $('.btn_evaluar').on('click', function(){
              $('#segundocandado').foundation('reveal', 'open');
    });
    
    //Segundo Modal
    $('#btnAceptar').on('click', function(){
        var _objRespuestas = creaObjetoRespuesta();
        finalizarExamen(_objRespuestas);
    });
    
    $( ".navega-pregunta" ).on('click',function( event ) {
        //event.preventDefault();
        //var link = $(this).attr('href');    
        var _objRespuestas = creaObjetoRespuesta();
        var _strBoton ;    
        var idPregunta = 0; // Solo se envía si el boton es irA
        if( $(this).hasClass('anterior') ){
            _strBoton = 'anterior';
        }else{
            if( $(this).hasClass('siguiente') ){
              _strBoton  = 'siguiente';
          }else{
              _strBoton  = 'irA';
              idPregunta = $(this).prop('id');
          }
        }
        
        arrRespuestasLineas = new Object();
        guardaRespuesta(_objRespuestas, _strBoton, idPregunta);
       
        //}else{
            //muestraSiguientePregunta();
        //}        
    });
    $( ".marca-preg" ).on('click', function( event ) {
        var _bolEstatusMarcada = $(this).hasClass('fa-flag-o') ? false : true;
        marcaDesmarcaPregunta( _bolEstatusMarcada );        
    });
    
    
    function guardaRespuesta(_objRespuestas, _strBoton, idPregunta){
        
        $.ajax({
            type:'POST',
            dataType:'json',            
            url:  $('#jq_siguiente_pregunta').val() + '/' + $('#idPregunta').val(),
            //url:  $('#urlGuardaRespuesta').val(),
            data:{ 
                    //'anterior'      : $('#anterior').val(),
                    //'siguiente'      : $('#siguiente').val(),
                    'botonSeleccionado' : _strBoton,
                    'objRespuestas'     : _objRespuestas,
                    'idPregunta'        : idPregunta,
                    'demo'              : $("#demo").val()

                },
            beforeSend: function() {

            },
            success:function(json){
                    if( json ){
                       // console.log('La respuesta se ha guardado'); 
                        //window.location = link;
//                        var pregSiguiente = json.numPreg + 1;
//                        var pregAnterior = json.numPreg - 1;
//                        $('.siguiente').prop('title', 'Haz clic para ir a la pregunta ' + pregSiguiente);        
//                        $('.anterior').prop('title', 'Haz clic para ir a la pregunta ' + pregAnterior);        
                        $('#jsonPregunta').val(json['jsonPregunta']);
                        $('#anterior').val(json['anterior']);
                        $('#siguiente').val(json['siguiente']);
                        $('#primera').val(json['primera']);
                        $('#ultima').val(json['ultima']);
                        $('#idPregunta').val(json['actual']);
                        $('.meter').css('width',json['pctAvance']+'%' );
                        $('.meter').parent().attr('title', json['pctAvance']+'% de avance');
                        //$('.pct_avc').html(json['pctAvance']+'%' );
                        $('.meter').parent().tooltip({
                            content: json['pctAvance']+'% de avance'
                        });
                        $('.num_preg a').html('<span id="jq_num_pregunta">' + json['numPreg']+'</span> / '+json['total']);
                        $('.meter').css('width',json['pctAvance']+'%' );
                        $('#dd_titulo').html($('#' + $('#idPregunta').val()).html());
                        marcarDesmarcarPregunta(json['marcada']);
                        muestraSiguientePregunta(json);
                        $('.nav_style .navega-pregunta').blur();
                        var _todasPreguntas = json['pregsExam'];
                        var _preguntaActual = json['actual'];
                        var _ultimoDato = Object.keys(_todasPreguntas).pop();
                        if ( json['ultima'] == true) {
                             $('#button-finalizar').attr("disabled", false);
                             $('#button-finalizar').removeClass("disabled");
                             $('#button-finalizar').fadeIn('slow');
                                //$('#button-finalizar').css("background-color","#FFFFFF");
                                //$('#button-finalizar').css("color","#1F93DC");
                        }else{
                             $('#button-finalizar').attr("disabled", true);
                             $('#button-finalizar').hide();
                            
                             //$('#button-finalizar').css("background-color","#5D9FE2");
                             //$('#button-finalizar').css("color","#FFFFFF");
                        }
                    }
            },
            error: function(json){
                 console.log("Hubo un error al procesar los datos: " + json);
                 //$('#dd_titulo').html('Lista de preguntas');
            }
        });
    }
    
    function marcaDesmarcaPregunta( _bolEstatus ){        
        $.ajax({
            type:'POST',
            dataType:'json',            
            url:  $('#marcaDesmarcaPregunta').val(),
            //url:  $('#urlGuardaRespuesta').val(),
            data:{ 
                    'idPregunta'    : $('#idPregunta').val(),
                    'estatus'       : _bolEstatus
                },
            beforeSend: function() {

            },
            success:function(json){
                if( json ){
                    marcarDesmarcarPregunta(json);
                }
            },
            error: function(json){
                 console.log("Hubo un error al procesar los datos: " + json);
            }
        });
    }
    
    function finalizarExamen(_objRespuestas){
        $.ajax({
            type:'POST',
            dataType:'json',            
            url:  $('#urlFinalizaExamen').val(),
            data:{ 
                'idPreguntaActual' : $('#idPregunta').val(),
                'objRespuestas'     : _objRespuestas
                },
            beforeSend: function() {
            },
            success:function(json){
                    if( json ){
                        $('#ir-examenes').foundation('reveal', 'open');
                        //muestraSiguientePregunta(json);
                        /*
                        $('#ir-examenes').removeClass('hide').dialog({
                            close: function( event, ui ) {
                                window.location = $('#urlMisExamenes').val();
                            },
                            resizable: false,
                            height:250,
                            width:500,
                            modal: true,
                            buttons: {
                                "Regresar": function() {
                                    $( this ).dialog( "close" );
                                    window.location = $('#urlMisExamenes').val();
                                }
                            }
                        }); 
                        */
                    }else{
                    }
            },
            error: function(json){
                 console.log("Hubo un error al procesar los datos: " + json);
            }
        });
    }
    
    function marcarDesmarcarPregunta(resp, id){
        if(resp === 'Marcada'){
            $('.marca-preg').addClass('fa-flag').removeClass('fa-flag-o');
            $('#' + $('#idPregunta').val() + ' span').removeClass('desactiva');
        }else{
            $('.marca-preg').removeClass('fa-flag').addClass('fa-flag-o');
            $('#' + $('#idPregunta').val() + ' span').addClass('desactiva');
        }
    }
    
    function creaObjetoRespuesta(){
        var _objResponse = new Object();
        var itemSelected = $( "input:checkbox,input:radio,.btn_preg" ); // Esto funciona para el tipo 1 y 2
        
        //btn_preg
        // Esto es para los botones seleccionados
        //btn_preg_press
        
        
        var i = 0, j = 1, cons=1, _esMatriz = false;
        //var isSelected = true;
        if(typeof itemSelected === 'undefined' || itemSelected.length === 0){
            //_objResponse = null;
            var textResolved = contestoAlgo();
            if( textResolved ){    
                $('.textboxGral,.ui-droppable,.sortable-item').each(function(){
                    if( $(this).prop('id') === 'contenedor' || $(this).prop('id') === 'tarjetas'){
                        if( $(this).prop('id') === 'contenedor' ){
                            $('.resp_arra_cont').each(function(){
                                if(typeof _objResponse.resp === 'undefined'){
                                    _objResponse.resp = new Object();
                                }
                                if(typeof _objResponse.resp[i] === 'undefined'){
                                    _objResponse.resp[i] = new Object();
                                }            
                                _objResponse.resp[i].id = j;
                                _objResponse.resp[i].selected = $('#contenedor ul #respuesta'+j+'_btn').length ? true : false;
                                i++; j++;
                            });
                        }
                    }else{
                        if(typeof _objResponse.resp === 'undefined'){
                            _objResponse.resp = new Object();
                        }
                        if(typeof _objResponse.resp[i] === 'undefined'){
                            _objResponse.resp[i] = new Object();
                        }                    
                        _objResponse.resp[i].id = j;
                        _objResponse.resp[i].selected = $(this).hasClass('textboxGral') ? 
                                                        $(this).val() : $(this).attr('value');
                        i++; j++;
                    }
                }); 
            }else{
                //Agrego el caso para las preguntas de relacionar con linea
                if(typeof arrRespuestasLineas !== 'undefined'){
                    if(!$.isEmptyObject(arrRespuestasLineas)){
                        textResolved = contestoAlgoLineas();
                        if( textResolved ){
                            _objResponse = arrRespuestasLineas;
                        }else{
                            _objResponse = '';
                        }
                    }
                }else{
                    _objResponse = '';
                }
            }
        }else{
            $('.respuestas').each(function(){
                if(typeof  _objResponse.resp === 'undefined'){
                    _objResponse.resp = new Object();
                }
                if(typeof  _objResponse.resp[i] === 'undefined'){
                    _objResponse.resp[i] = new Object();
                }
                if($(this).hasClass('pregunta_matriz')){                    
                    if(typeof  _objResponse.resp === 'undefined'){
                        _objResponse.resp = new Object();
                    }
                    if(typeof  _objResponse.resp[cons -1] === 'undefined'){
                        _objResponse.resp[cons - 1] = new Object();
                    }
                    if($(this).is(':checked')){
                        _objResponse.resp[cons - 1].id = cons;
                        _objResponse.resp[cons - 1].selected = $(this).attr('value');
                        _esMatriz = true;
                        //_objResponse.resp[cons - 1].selected = obtenerDatos($(this).attr('value'));
                        cons++;
                    }                    
                }else{
                    _objResponse.resp[i].id = $(this).attr('value');
                    _objResponse.resp[i].selected = $(this).is(':checked');
                    i++;
                }
            }); 
//            if(_esMatriz){
//                var itemDelete = Object.keys(_objResponse.resp).length - 1;
//                delete _objResponse.resp[itemDelete];
//            }
            // Agrego el caso de los botones azules
            $('.btn_preg').each(function(){
                if(typeof  _objResponse.resp === 'undefined'){
                    _objResponse.resp = new Object();
                }
                if(typeof  _objResponse.resp[i] === 'undefined'){
                    _objResponse.resp[i] = new Object();
                }
                _objResponse.resp[i].id = $(this).attr('value');
                _objResponse.resp[i].selected = $(this).hasClass('btn_preg_press');
                i++;
            });            
        }
        return _objResponse;
    }
    
    function muestraSiguientePregunta(json){
        //console.log('Muestra Siguiente Pregunta');
         var obj = jQuery.parseJSON($('#jsonPregunta').val());    
         if(obj['pregunta']){
             obtenerValoresJSON(json);
         }else{
             console.log('No hay pregunta anterior');
         }
        
    }
    
    /**
     * Funcion que se encarga de verificar si se contesto algo en la pregunta
     * por el momento esta funcion no se utilizara de esa forma, ya que ahora no 
     * importa si el usuario respondio algo, mas bien lo que tiene que hacer es
     * verificar si el tipo de pregunta no es relacionar con linea
     * @returns {Boolean}
     */
    function contestoAlgo(){
        var respondio = false;
        $('.textboxGral,.ui-droppable').each(function(){
            if($(this).hasClass('ui-droppable')){
                if($(this).hasClass('hasCard')){
                    respondio = true;
                }else{ // Agrego el caso para las preguntas de tipo 5
                    if( $('#contenedor ul li').length > 0 ){
                        respondio = true;
                    }
                }
            }else{
                if($(this).val()){
                    respondio = true;
                }
            }
        });
        respondio = $('.sortable-item').length > 0 ? true : respondio;
        respondio = $('#contenedor ul li').length > 0 ? true : respondio;
        respondio = typeof arrRespuestasLineas === 'undefined' || typeof arrRespuestasLineas[0] === 'undefined' ? true: false;
        return respondio;
    }
    
    function contestoAlgoLineas(){
        var respondio = false;
        numElementos = $('circle').length / 2;
        for(var i=0; i<numElementos; i++){
            if(arrRespuestasLineas[i].selected !== 0 || arrRespuestasLineas[i].id !== 0 ){
                respondio = true;
            }
        }
        return respondio;
    }
    /*
    $(function() {        
        $('#button-evaluar').click(function(){    
            $('#confirma-finalizar').removeClass('hide').dialog({
                resizable: false,
                height:250,
                width:500,
                modal: true,
                buttons: {
                    Aceptar: function() {
                        $( this ).dialog( "close" );
                        finalizarExamen();
                    },
                    Cancelar: function() {
                        $( this ).dialog( "close" );
                    }
                }
            }); 
        });
    }); 
    */
    // Funciones para el drop
    function DropDown(el) {
            this.dd = el;
            this.opts = this.dd.find('ul.dropdown > li');
            this.val = [];
            this.index = [];
            this.initEvents();
    }
    DropDown.prototype = {
            initEvents : function() {
                    var obj = this;

                    obj.dd.on('click', function(event){
                            $(this).toggleClass('active');
                            event.stopPropagation();
                    });

                    obj.opts.children('label').on('click',function(event){
                            var opt = $(this).parent(),
                                    chbox = opt.children('input'),
                                    val = chbox.val(),
                                    idx = opt.index();

                            ($.inArray(val, obj.val) !== -1) ? obj.val.splice( $.inArray(val, obj.val), 1 ) : obj.val.push( val );
                            ($.inArray(idx, obj.index) !== -1) ? obj.index.splice( $.inArray(idx, obj.index), 1 ) : obj.index.push( idx );
                    });
            },
            getValue : function() {
                    return this.val;
            },
            getIndex : function() {
                    return this.index;
            }
    };
    $(function() {

            var dd = new DropDown( $('#dd') );

            $(document).click(function() {
                    // all dropdowns
                    $('.wrapper-dropdown-4').removeClass('active');
            });
    });
});
