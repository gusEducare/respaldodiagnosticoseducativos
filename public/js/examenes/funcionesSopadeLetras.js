/*   ______                _                                  
    |  ____|              (_)                                 
    | |__ _   _ _ __   ___ _  ___  _ __   ___  ___   
    |  __| | | | '_ \ / __| |/ _ \| '_ \ / _ \/ __|   
    | |  | |_| | | | | (__| | (_) | | | |  __/\__ \ 
    |_|   \__,_|_| |_|\___|_|\___/|_| |_|\___||___/ 
   _____                         _        _      _                 
  / ____|                       | |      | |    | |                
 | (___   ___  _ __   __ _    __| | ___  | | ___| |_ _ __ __ _ ___ 
  \___ \ / _ \| '_ \ / _` |  / _` |/ _ \ | |/ _ \ __| '__/ _` / __|
  ____) | (_) | |_) | (_| | | (_| |  __/ | |  __/ |_| | | (_| \__ \
 |_____/ \___/| .__/ \__,_|  \__,_|\___| |_|\___|\__|_|  \__,_|___/
              | |                                                  
              |_|                                                  

       
@proyecto:         NUEVA PLATAFORMA DE EVALUACIONES 2015
@programa:         Funciones de Sopa de letras (código fuente)
@author:           Grupo Educare S.A. de C.V.
@desarrollador     gregorios@grupoeducare.com
@fecha de inicio:  01 de Junio DE 2015
@fecha de entrega: Julio 2015                               -->
**********************************************************
   V A R I A B L E S     S O P A     D E     L E T R A S
*********************************************************/
var jsons_arr = new Array();
var palabrasContestadas_arr = new Array();
var letrasContestadas_arr = new Array();
var fuente = 1;//1. Ninguna (se genera Aleatorio sólo la primera vez)    2.- Base de datos
var incremento = 0;
var terminaPintadoInterrumpido = false;
var ajusteSopa = 11;
var coloresHexadecimalesIDs = new Array();//= coloresHexadecimales[ rand(0, coloresHexadecimales.length - 1) ];//Para obtener un random
var cuantosColores = 83;
var posicionID = 0;
var band = false;

/***********************************************************
  F U N C I O N E S     S O P A     D E     L E T R A S
***********************************************************/
function shuffle( myArr ){
  return myArr.sort( function() Math.random() - 0.5 );
}
function llenaArregloIDs(estructuraSopa){
    for(var f=0;f<=cuantosColores;f++){
        coloresHexadecimalesIDs[f] = f;
    }    
    //coloresHexadecimalesIDs = shuffle( coloresHexadecimalesIDs );        
}
function leeInformacionBds(){
    if(fuente === 1){    
    }else{   
        jsons_arr = ['T','F','E','K','G','G','F','A','I','G','X','J',
                     'J','I','S','A','T','U','R','N','O','L','F','U',
                     'U','P','E','Z','M','N','G','W','T','R','H','B',
                     'P','K','L','R','V','E','E','H','V','H','Y','L',
                     'I','R','Z','U','R','M','R','P','J','W','Q','M',
                     'T','Q','H','R','T','A','H','C','T','P','M','V',
                     'E','U','Z','N','K','O','Q','U','U','U','P','Z',
                     'R','Y','I','O','E','X','N','R','M','R','N','I',
                     'Q','K','V','E','N','U','S','A','K','H','I','O',
                     'M','A','R','T','E','U','Q','N','U','U','C','O',
                     'F','A','B','J','F','T','B','O','E','X','F','R',
                     'L','R','E','M','L','F','D','F','J','H','Q','X'];
        palabrasContestadas_arr = [0,3];//Indica las palabras respondidas al momento
        letrasContestadas_arr = ['28-41-54-67-80-93-106-119','108-109-110-111-112'];//Posiciones de las celdas respondidas al momento
    }
    var palabras_arr = "mercurio,venus,tierra,marte,jupiter,saturno,urano,neptuno,pluton";
    var medida = 12;
    llenaArregloIDs();
    console.log("coloresHexadecimalesIDs "+coloresHexadecimalesIDs);
    $("#gridSopadeletras").sopa({"listadepalabras" : palabras_arr,"medidaGrid" : medida});
    
    $('<style>li#lista0::before{background-image: "nada"}</style>').appendTo('head');
    
    var css = '<style id="pseudo">#lista0::after{background-image: "" !important;}</style>';
    document.head.insertAdjacentHTML( 'beforeEnd', css );
}

function guardarSopaEnBDs(estructuraSopa){
    estructuraSopa = eliminaElementosDuplicados(estructuraSopa);
    console.log("GUARDA estructuraSopa: "+estructuraSopa);
}
function guardaPalabrasContestadas(indicePalabra){        
    palabrasContestadas_arr.push(indicePalabra);
    palabrasContestadas_arr = eliminaElementosDuplicados(palabrasContestadas_arr);
    console.log("GUARDA palabrasContestadas_arr: "+palabrasContestadas_arr);    
}
function guardaLetrasContestadas(indicePalabra){
    letrasContestadas_arr.push(indicePalabra);
    letrasContestadas_arr = eliminaElementosDuplicados(letrasContestadas_arr);
    console.log("GUARDA letrasContestadas_arr: "+letrasContestadas_arr);
}
function eliminaElementosDuplicados(arr) {
    var i,
        len=arr.length,
        out=[],
        obj={};

    for (i=0;i<len;i++) {
       obj[arr[i]]=0;
    }
    for (i in obj) {
       out.push(i);
    }
    return out;
}
function rand(min, max) {
  var offset = min;
  var range = (max - min) + 1;

  var randomNumber = Math.floor( Math.random() * range) + offset;
  return randomNumber;
}

function funcionesSopa (){
    (function( $, undefined ) {
        $.widget("claseSopa.sopa", $.ui.mouse, {
            options : {
                listadepalabras : null,
                medidaGrid : 10},
            _mapearEventoACelda: function(event) {                
                var columnaActual = Math.ceil((event.pageX - this._celdaX) / this._celdaAncho);
                var filaActual = Math.ceil((event.pageY - this._cellY) / this._celdaAlto);
                var el = $('#tablaGrid tr:nth-child('+filaActual+') td:nth-child('+columnaActual+')');
                return el;
            },
            _create : function () {
                this.modelo      = ayudanteJuego.preparaGrid(this.options.medidaGrid, this.options.listadepalabras);
                this.inicio  = new Principal();
                this.zonaprincipal    = new areaPrincipal();
                this.trazos       = new Trazos();				                
                ayudanteJuego.renderearJuego(this.element[0],this.modelo);
                this.options.distance=0;//Setea la propiedades del mouse
                this._mouseInit();
                var cell = $('#tablaGrid tr:first td:first');
                this._celdaAncho = cell.outerWidth();
                this._celdaAlto = cell.outerHeight();
                this._celdaX = cell.offset().left;
                this._cellY = cell.offset().top;                                
            },            
            destroy : function () {                
                this.zonaprincipal.limpiar();
                this.trazos.limpiar();
                this.inicio.limpiar();                
                this._mouseDestroy();
                return this;                
            },
            _mouseStart: function(event) {//Eventos del mouse
                band = false;
                var panel = $(event.target).parents("div").attr("id");
                if ( panel === 'contenedorJuego') {
                    this.inicio.seteaPrincipal( event.target );
                    this.zonaprincipal.creaZona( event.target );
                }
                else if ( panel === 'contenedorPalabra') {//Si el usuario requiere ayuda para encontrar la palabra, se identifica la palabra en el grid, hacemos referencia al tf en las celdas que forman la palabra					
                    /*var idx = $(event.target).parent().children().index(event.target);                
                    var palabraSeleccionada = this.modelo.listaPalabras.get(idx);
                    $(palabraSeleccionada.cellsUsed).each ( function () {
                        Visualizador.iluminar($(this.td));
                    }); */
                }                
            },
            _mouseDrag : function(event) {
                event.target = this._mapearEventoACelda(event);                              
                if (this.inicio.esMismaCelda(event.target)) {                    
                    this.trazos.regresaANormal();
                    this.zonaprincipal.seteaResto(-1);
                    return;
                }                                
                if ($(event.target).hasClass("trazado") || $(event.target).hasClass("rf-glowing") ) {//Si es evento en celda armada                             
                    var eligeUno = this.zonaprincipal.index(event.target);//Si es en la zona activa
                    if (eligeUno!= -1) {                             
                        this.zonaprincipal.seteaResto(eligeUno);//Setea el resto de la celda armada                                                
                        this.trazos.deduceTrazado(this.inicio.root, eligeUno);                        
                    }else {
                        this.trazos.glowTo(event.target);
                    }
                }
            },            
            _mouseStop : function (event) {                
                var palabraseleccionada = '';//Obtiene palabra
                var letrasSeleccionadas = '';//Obtiene las letras seleccionadas para guardalas                
                //$('.rf-glowing, .iluminado', this.element[0]).each(function() {//Se cambia esta condicion para que permita seguir seleccionando
                $('.rf-glowing', this.element[0]).each(function() {                    
                    var u = $.data(this,"cell");
                    palabraseleccionada += u.value;                    
                });                                
                var palabra_strIndex = this.modelo.listaPalabras.esUnaPalabraPresente(palabraseleccionada);                                         
                if (palabra_strIndex!=-1) {
                    var inc = 0;                                                                                
                    //$('.rf-glowing, .iluminado', this.element[0]).each(function() {                    
                      $('.rf-glowing', this.element[0]).each(function() {
                        Visualizador.select(this);
                        var a = this;
                        Object.keys(a).forEach(function (key) {
                            inc++;                            
                            var akey_string;
                            var indicePalabra_int = parseInt(a[key]);
                            indicePalabra_int = indicePalabra_int - ajusteSopa;
                            akey_string = indicePalabra_int.toString();
                            
                            if(inc == 1){
                                letrasSeleccionadas = akey_string;
                            }else{
                                letrasSeleccionadas = letrasSeleccionadas + '-' + akey_string;
                            }
                        });
                        $.data(this,"selected", "true");                         
                    });                      
                    guardaLetrasContestadas(letrasSeleccionadas);
                    ayudanteJuego.preparaMarca(palabra_strIndex);                    
                }                      
                this.zonaprincipal.regresaANormal();//Regresa las celdas a estados normales
                this.inicio.regresaANormal();
                this.trazos.regresaANormal();      
                
                if(band === true){   posicionID++;    }                
            }            
        }//Fin del widget
    );

    function Trazos() {//Trazos
        this.trazos = null;    
        this.deduceTrazado = function (root, idx) {
            this.regresaANormal(); //Limpia trazos antiguos
            var ix = $(root).parent().children().index(root);        
            this.trazos = new Array();//Crea los nuevos trazos        
            switch (idx) {
                case 0: //izquierda - horizontal
                    this.trazos = $(root).prevAll();    break;
                case 1: //derecha horizontal
                    this.trazos = $(root).nextAll();    break;
                case 2: //arriba vertical
                    var $n = this.trazos;
                    $(root).parent().prevAll().each( function() {
                        $n.push($(this).children().get(ix));
                    });    break;
                case 3: //abajo vertical
                    var $o = this.trazos;
                    $(root).parent().nextAll().each( function() {
                        $o.push($(this).children().get(ix));
                    }); break;
                case 4: //diagonal derecha arriba
                    var $p = this.trazos;                
                    var currix = ix;
                    $(root).parent().prevAll().each( function () {
                        $p.push($(this).children().get(++currix));
                    }); break;
                case 5: //derecha diagonal arriba
                    var $q = this.trazos;                
                    var currixq = ix;
                    $(root).parent().prevAll().each( function () {
                        $q.push($(this).children().get(--currixq));
                    });break;
                case 6 : //izquierda diagonal abajo
                    var $r = this.trazos;                
                    var currixr = ix;
                    $(root).parent().nextAll().each( function () {
                        $r.push($(this).children().get(++currixr));
                    });break;
                case 7: //derecha diagonal abajo
                    var $s = this.trazos;                
                    var currixs = ix;
                    $(root).parent().nextAll().each( function () {
                        $s.push($(this).children().get(--currixs));
                    });break;
            }
            for (var x=1;x<this.trazos.length;x++) {
                Visualizador.arm(this.trazos[x]);
            }
        };
        this.glowTo = function (upto) {//Encender las celdas de arriba 
            var to = $(this.trazos).index(upto);            
            for (var x=1;x<this.trazos.length;x++) {            
                if (x<=to) {
                    Visualizador.glow(this.trazos[x]);
                }
                else {
                    Visualizador.arm(this.trazos[x]);                                   
                }
            }            
        }	
        this.regresaANormal = function () {//Limpia los trazos
            if (!this.trazos) return;
            for (var t=1;t<this.trazos.length;t++) { //No limpia zona principal
                Visualizador.restaura(this.trazos[t]);                                             
            }     
        }        
        this.limpiar = function() {
            $(this.trazos).each(function () {
               Visualizador.limpiar(this); 
            });
        }
    }

    function areaPrincipal() {    
        this.elems = null;            
        this.creaZona = function (root) {//Selecciona las zonas cercanas
        this.elems = new Array();         
        var $tgt = $(root);
        var ix = $tgt.parent().children().index($tgt);
        var above = $tgt.parent().prev().children().get(ix);
        var below = $tgt.parent().next().children().get(ix);        
        this.elems.push($tgt.prev()[0],$tgt.next()[0]);//horizontal
        this.elems.push( above, below, 
            $(above).next()[0],$(above).prev()[0], //diagonal
            $(below).next()[0],$(below).prev()[0] //diagonal
        );
        $(this.elems).each( function () {
            if ($(this)!=null) {
                Visualizador.arm(this);
            }
        });       
    }    

    this.index = function (elm) {//Dar a la zona principal algo de inteligencia
        return $(this.elems).index(elm);
    }

    this.seteaResto = function (eligeUno) {
        for (var x=0;x<this.elems.length;x++) {
            Visualizador.arm(this.elems[x]);
        }
        if (eligeUno != -1) {            
            Visualizador.glow(this.elems[eligeUno]);
        }
    }

    this.regresaANormal = function () {        
        for (var t=0;t<this.elems.length;t++) {            
            Visualizador.restaura(this.elems[t]);            
        }        
    }

    this.limpiar = function() {
            $(this.elems).each(function () {
               Visualizador.limpiar(this); 
            });
        }    
    }

    function Principal() {//Primera celda clickeada
        this.root = null;    
        this.seteaPrincipal = function (root) {
            this.root = root;                        
            Visualizador.glow(this.root);
        }    
        this.regresaANormal = function () {
            Visualizador.restaura(this.root);
        }
        this.esMismaCelda = function (t) {
            return $(this.root).is($(t));
        }    
        this.limpiar = function () {
            Visualizador.limpiar(this.root);
        }
    }

    var Visualizador = {//Objeto para manipular la celda desplegada basada en métodos llamados
        glow : function (c) {
            $(c).removeClass("trazado").removeClass("palabraSeleccionada").addClass("rf-glowing").removeClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]).addClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);             
        },    
        arm : function (c) {          
            $(c).removeClass("rf-glowing").addClass("trazado").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);            
        },   
        restaura : function (c) {            
            $(c).removeClass("trazado").removeClass("rf-glowing").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);
            if ( c!=null && $.data(c,"selected") == "true" ) {
                $(c).addClass("palabraSeleccionada");                
            }
        },    
        select : function (c) {           
            $(c).removeClass("trazado").removeClass("rf-glowing").removeClass("iluminado").addClass("palabraSeleccionada").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]).removeClass("iluminado"+coloresHexadecimalesIDs[posicionID]).addClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]);            
        },
        resetea : function (c){
           $(c).addClass("palabraSeleccionada").removeClass("iluminado").addClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]).removeClass("iluminado"+coloresHexadecimalesIDs[posicionID]);                       
       },
        iluminar : function (c) {            
            //$(c).removeClass("trazado").removeClass("palabraSeleccionada").addClass("iluminado").removeClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]).addClass("iluminado"+coloresHexadecimalesIDs[posicionID]);            
            $(c).removeClass("trazado").removeClass("palabraSeleccionada").addClass("iluminado");            
        },
        marcaPalabraEncontrada : function (w,idx) {//Marcar con linea atravezada y cambio de color la parabra encontrada
            $(w).css("background",'transparent');
            $(w).addClass('palabraEncontrada');
            $('#lista'+idx).addClass('palabraEncontrada');
            band = true;
        },
        limpiar : function (c) {            
            $(c).removeClass("trazado").removeClass("rf-glowing").removeClass("palabraSeleccionada").removeClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]).removeClass("palabraSeleccionada"+coloresHexadecimalesIDs[posicionID]);
            $.removeData($(c),"selected");            
        }
    }

    //Objetos que representan celdas individuaes del grid
    function Cell() {
        this.DEFAULT = "-";
        this.isHighlighted = false;
        this.value = this.DEFAULT;
        this.parentGrid = null;
        this.isUnwritten = function () {
            return (this.value == this.DEFAULT);
        };
        this.isSelected = false;
        this.isSelecting = true;
        this.td = null;   
    }

    function Grid() {
        this.cells = null;
        this.directions = [
            "LeftDiagonal",
            "Horizontal",
            "RightDiagonal",
            "Vertical"
        ];    
        this.inicializaGrid= function(size) {
            this.cells = new Array(size);                
            for (var i=0;i<size;i++) {
                this.cells[i] = new Array(size);
                for (var j=0;j<size;j++) {
                    var c = new Cell();                                           
                    c.parentgrid = this;
                    this.cells[i][j] = c;
                }
            }

        }        
        this.getCell = function(row,col) {           
            return this.cells[row][col];
        }    
        this.createHotZone = function(uic) {
            var $tgt = uic;
            var hzCells = new Array(); 
            var ix = $tgt.parent().children().index($tgt);
        }    
        this.size = function() {
            return this.cells.length;
        }
        this.put = function(row, col, palabra_str) {//Coloca palabra en el grid en la ubicación sugerida
            var populator = eval("new "+ eval("this.directions["+Math.floor(Math.random()*4)+"]") +"Populator(row,col,palabra_str, this)");
            var esColocado= populator.populate();                
            if (!esColocado) {//Sino consigue ubicarla forza si es posible
                for (var x=0;x<this.directions.length;x++) {
                    var populator2 = eval("new "+ eval("this.directions["+x+"]") +"Populator(row,col,palabra_str, this)");
                    var esColocado2= populator2.populate();
                    if (esColocado2) break;                
                }            
            }
        }    
        this.llenarGrid = function() {           
            for (var i=0;i<this.size();i++) {
                for (var j=0;j<this.size();j++) {                
                    if (this.cells[i][j].isUnwritten()) {
                        this.cells[i][j].value = String.fromCharCode(Math.floor(65+Math.random()*26));//Llena de letras aleatorias la sopa (letras de relleno)
                    }
                }
            }        
        }
    }//Fin de la función Grid

    function HorizontalPopulator(row, col, palabra_str, grid) {//Crea los trazos horizontales    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {                        
            if (this.willWordFit()) {//Trata de ubicar palabra en la fila, checa si la fila contigua está bloqueda
                this.writeWord();
            }
            else {            
                for (var i=0;i<this.size;i++) {//Recorrre todas las filas empezando en la actual
                    var xRow = (this.row+i)%this.size;                
                    var startingPoint = this.findContigousSpace(xRow, palabra_str);//Trata de iniciar con algun trazo
                    if (startingPoint == -1) {//Si no trata de ver si podemos cruza la palabra solo si existe
                        var overlapPoint = this.isWordOverlapPossible(xRow, palabra_str);
                        if (overlapPoint == -1) {//Sino se repite el proceso                        
                            continue;
                        }
                        else {
                            this.row = xRow;
                            this.col = overlapPoint;
                            this.writeWord();
                            break;
                        }
                    }
                    else {
                        this.row = xRow;
                        this.col = startingPoint;
                        this.writeWord();
                        break;
                    }
                }
            }
            // Si no lo logra regresa false, sino logra posicionarlo necesitaremos intentarlo en otra dirección
            return (palabra_str.esColocado);                    
        }

        //Escribe una palabra en el grid dando una ubicación, recuerda cada celda usada para desplegar la palabra
        this.writeWord = function () {
            var chars = palabra_str.chars;        
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i];
                this.cells[this.row][this.col+i] = c;
                palabra_str.containedIn(c);
                palabra_str.esColocado = true;
            }
        }

        //Checa si la palabra puede ser cambiada o intercambiar celdas con el mismo contenido
        this.isWordOverlapPossible = function (row, palabra_str) {
            return -1;
        }

        //Checa si la palabra se encontrará en la misma locación
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            for (var i=col;i<this.size;i++) {
                if (this.cells[row][i].isUnwritten() || this.cells[row][i] == chars[i] ) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
            }
            return isFree;
        }

        //Checa si hay un espacio contiguo en algún lugar de la línea
        this.findContigousSpace = function (row, palabra_str) {
            var freeLocation = -1;
            var freeCounter=0;
            var chars = palabra_str.chars;
            for (var i=0;i<this.size;i++) {
                if (this.cells[row][i].isUnwritten() || this.cells[row][i] == chars[i]) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = (i - (palabra_str.size-1));
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
            }
            return freeLocation;
        }
    }//HorizontalPopulator

    //Crea los trazos verticales
    function VerticalPopulator(row, col, palabra_str, grid) {    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {           
            if (this.willWordFit()) {
                this.writeWord();
            }
            else {           
                for (var i=0;i<this.size;i++) {
                    var xCol = (this.col+i)%this.size; // loop through all rows starting at current;                
                    var startingPoint = this.findContigousSpace(xCol, palabra_str);
                    if (startingPoint == -1) {                    
                        var overlapPoint = this.isWordOverlapPossible(xCol, palabra_str);
                        if (overlapPoint == -1) {                        
                            continue;
                        }
                        else {
                            this.row = overlapPoint;
                            this.col = xCol;
                            this.writeWord();
                            break;
                        }
                    }
                    else {
                        this.row = startingPoint;
                        this.col = xCol;
                        this.writeWord();
                        break;
                    }
                }
            }        
            return (palabra_str.esColocado);                    
        }       
        this.writeWord = function () {
            var chars = palabra_str.chars;
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i];
                this.cells[this.row+i][this.col] = c;
                palabra_str.containedIn(c);
                palabra_str.esColocado = true;
            }        
        }    
        this.isWordOverlapPossible = function (col, palabra_str) {
            return -1;
        }
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            for (var i=row;i<this.size;i++) {
                if (this.cells[i][col].isUnwritten() || chars[i] == this.cells[i][col].value) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
            }
            return isFree;
        }        
        this.findContigousSpace = function (col, palabra_str) {
            var freeLocation = -1;
            var freeCounter=0;
            var chars = palabra_str.chars;
            for (var i=0;i<this.size;i++) {
                if (this.cells[i][col].isUnwritten() || chars[i] == this.cells[i][col].value) { 
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = (i - (palabra_str.size-1));
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
            }
            return freeLocation;        
        }
    }

    //Crea estrategia para diagonal derecha
    function LeftDiagonalPopulator(row, col, palabra_str, grid) {    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {           
            if (this.willWordFit()) {
                this.writeWord();
            }
            else {
                var output = this.findContigousSpace(this.row,this.col, palabra_str);
                if (output[0] != true) {                                
                    OUTER:for (var col=0, row=(this.size-palabra_str.size); row>=0; row--) {
                        for (var j=0;j<2;j++) {
                            var op = this.findContigousSpace( (j==0)?row:col, (j==0)?col:row, palabra_str);
                            if (op[0] == true) {
                                this.row = op[1];
                                this.col = op[2];
                                this.writeWord();
                                break OUTER;
                            }
                        }                    
                    }
               }
                else {
                    this.row = output[1];
                    this.col = output[2];
                    this.writeWord();
                }
            }        
            return (palabra_str.esColocado);                    
        }           
        this.writeWord = function () {
            var chars = palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i];
                this.cells[lrow++][lcol++] = c;
                palabra_str.containedIn(c);
                palabra_str.esColocado=true;
            }
        }    
        this.isWordOverlapPossible = function (row, palabra_str) {
            return -1;
        }   
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            var i=0;
            while (lcol < this.grid.size() && lrow < this.grid.size()) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++] ) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
                lrow++;
                lcol++;            
            }
            return isFree;
        }    
        this.findContigousSpace = function (xrow, xcol,palabra_str) {
            var freeLocation = false;
            var freeCounter=0;
            var chars = palabra_str.chars;
            var lrow = xrow;
            var lcol = xcol;

            while (lrow > 0 && lcol > 0) {
                lrow--;
                lcol--;
            }
            var i=0;
            while (true) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++]) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = true;
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
                lcol++;
                lrow++;
                if (lcol >= this.size || lrow >= this.size) {
                    break;
                }
            }
            if (freeLocation) {
                lrow = lrow - palabra_str.size+1;
                lcol = lcol - palabra_str.size+1;
            }
            return [freeLocation,lrow,lcol];        
        }
    }

    function RightDiagonalPopulator(row, col, palabra_str, grid) {    
        this.grid = grid;
        this.row =  row;
        this.col = col;
        this.palabra_str = palabra_str;
        this.size = this.grid.size();
        this.cells = this.grid.cells;

        this.populate = function() {           
            var rr=0;
            if (this.willWordFit()) {
                this.writeWord();
            }
            else {
                var output = this.findContigousSpace(this.row,this.col, palabra_str);
                if (output[0] != true) {                                
                    OUTER:for (var col=this.size-1, row=(this.size-palabra_str.size); row>=0; row--) {
                        for (var j=0;j<2;j++) {
                            var op = this.findContigousSpace( (j==0)?row:(this.size-1-col), (j==0)?col:(this.size-1-row), palabra_str);
                            if (op[0] == true) {
                                this.row = op[1];
                                this.col = op[2];
                                this.writeWord();
                                break OUTER;
                            }
                        }                    
                    }
               }
               else {
                    this.row = output[1];
                    this.col = output[2];
                    this.writeWord();
                }
            }        
            return (palabra_str.esColocado);                    
        }        
        this.writeWord = function () {
            var chars = palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            for (var i=0;i<palabra_str.size;i++) {
                var c = new Cell();
                c.value = chars[i];
                this.cells[lrow++][lcol--] = c;
                palabra_str.containedIn(c);
                palabra_str.esColocado = true;
            }
        }    
        this.isWordOverlapPossible = function (row, palabra_str) {
            return -1;
        }
        this.willWordFit = function() {
            var isFree = false;
            var freeCounter=0;
            var chars = this.palabra_str.chars;
            var lrow = this.row;
            var lcol = this.col;
            var i=0;
            while (lcol >= 0 && lrow < this.grid.size()) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++] ) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        isFree = true;
                        break;
                    }
                }
                else {
                    break;
                }
                lrow++;
                lcol--;

            }
            return isFree;
        }    
        this.findContigousSpace = function (xrow, xcol,palabra_str) {
            var freeLocation = false;
            var freeCounter=0;
            var chars = palabra_str.chars;
            var lrow = xrow;
            var lcol = xcol;

            while (lrow > 0 && lcol < this.size-1) {
                lrow--;
                lcol++;
            }
            var i=0;
            while (lcol >= 0 && lrow < this.grid.size()) {
                if (this.cells[lrow][lcol].isUnwritten() || this.cells[lrow][lcol] == chars[i++]) {
                    freeCounter++;
                    if (freeCounter == palabra_str.size) {
                        freeLocation = true;
                        break;
                    }
                }
                else {
                    freeCounter=0;
                }
                lrow++;
                lcol--;
            }
            if (freeLocation) {
                lrow = lrow - palabra_str.size+1;
                lcol = lcol + palabra_str.size-1;
            }
            return [freeLocation,lrow,lcol];        
        }
    }

    function Model() {
        this.grid= null;
        this.listaPalabras= null;
        this.init = function(grid,list) {
            this.grid = grid;
            this.listaPalabras = list;    
            for (var i=0;i<this.listaPalabras.size();i++) {
                grid.put( Utilidad.random(this.grid.size()), Utilidad.random(this.grid.size()), this.listaPalabras.get(i) );                
            }
        }    
    }
    function Word(val) {
        this.value = val.toUpperCase();
        this.valorOriginal = this.value;
        this.isFound= false;
        this.cellsUsed = new Array();
        this.esColocado = false;
        this.row = -1;
        this.col = -1;
        this.size = -1;
        this.chars = null;

        this.init = function () {
            this.chars = this.value.split("");
            this.size = this.chars.length;
        }
        this.init();

        this.containedIn = function (cell) {                
            this.cellsUsed.push(cell);        
        }		    
        this.checarSiEsSimilar = function (w) {
            if (this.valorOriginal == w || this.value == w) {
                this.isFound = true;
                return true;
            }
            return false;
        }    
    }

    function ListaPalabras() {
        this.palabras = new Array();    
        this.cargaPalabras = function (csvpalabras) {
            var $n = this.palabras;
            $(csvpalabras.split(",")).each(function () {
                $n.push(new Word(this));
            });        
        }    
        this.add = function(palabra_str) {     
            if (Math.random()*10 >5) {
                var s="";
                for (var i=palabra_str.size-1;i>=0;i--) {
                    s = s+ palabra_str.value.charAt(i);
                }
                palabra_str.value = s;            
                palabra_str.init();
            }
            this.palabras[this.palabras.length] = palabra_str;
        }    
        this.size = function() {
            return this.palabras.length;
        }    
        this.get = function(index) {        
            return this.palabras[index];
        }    
        this.esUnaPalabraPresente = function(checarPalabra2) {
            for (var x=0;x<this.palabras.length;x++) {
                if (this.palabras[x].checarSiEsSimilar(checarPalabra2)) return x;
            }
            return -1;
        }
    }

    var Utilidad = {
        random : function(max) {    return Math.floor(Math.random()*max);
        },    
        log : function (msg) {
            $("#logger").append(msg);
        }
    } 

    var ayudanteJuego = {
        preparaGrid : function (size, palabras) {
            var grid = new Grid();
            grid.inicializaGrid(size);
            var listaPalabras = new ListaPalabras();
            listaPalabras.cargaPalabras(palabras);
            var modelo = new Model();
            modelo.init(grid, listaPalabras);
            grid.llenarGrid();
            return modelo;
        },	
        renderearJuego : function(contenedor, modelo) {
            var grid = modelo.grid;
            var cells = grid.cells;
            var estructura = "<div id='contenedorJuego'><table id='tablaGrid' cellspacing=0 cellpadding=0 class='estiloTabla'>";
            var sopaParaGuardar = new Array();
            for (var i=0;i<grid.size();i++) {
                estructura += "<tr>";            
                for (var j=0;j<grid.size();j++) {
                    if(jsons_arr[0] === undefined){//Si no es un examen interrumpido se prepara el array para guardarlo en la BDs
                        sopaParaGuardar[incremento] = cells[i][j].value;
                    }else{
                        cells[i][j].value = jsons_arr[incremento];//Para que tome los valores (letras)de la base de datos                    
                    }
                    incremento++;
                    estructura += "<td  class='estiloGrid'>"+cells[i][j].value+"</td>";                                                                
                }
                estructura += "</tr>";
            }

            if(jsons_arr[0] === undefined){//Si no es un examen interrumpido se prepara el array para guardarlo en la BDs
                guardarSopaEnBDs(sopaParaGuardar);            
            }

            estructura += "</table></div>";
            $(contenedor).append(estructura);
            var x=0;
            var y=0;
            $("tr","#tablaGrid").each(function () {
                $("td", this).each(function (col){
                    var c = cells[x][y++];
                    $.data(this,"cell",c);
                    c.td = this;
                })
                y=0;
                x++;
            });
            var palabras = "<div id='contenedorPalabra'><ul>"
            var inc = 0;
            $(modelo.listaPalabras.palabras).each(function () {
                palabras += '<li id="lista'+inc+'" class=colocada'+this.esColocado+'>'+this.valorOriginal+'</li>';
                inc++;
            });
            palabras += "</ul></div>";
            $(contenedor).append(palabras); //Código para mostrar el menú de palabras

            if(jsons_arr[0] === undefined){
                terminaPintadoInterrumpido = true;
            }else{
                var palabraContestada;
                var cadena;
                for(var idx=0;idx<palabrasContestadas_arr.length;idx++){
                    palabraContestada = modelo.listaPalabras.get(idx);
                    cadena = letrasContestadas_arr[idx];
                    var celdasExtraidas = cadena.split("-");

                    for(var f=0;f<celdasExtraidas.length;f++){
                        Visualizador.iluminar($("#tablaGrid").find("td").eq(celdasExtraidas[f]));                                                                            
                        $("#tablaGrid").find("td").eq(celdasExtraidas[f]).addClass("rf-glowing"+coloresHexadecimalesIDs[posicionID]);                                                 
                        //Visualizador.resetea($("#tablaGrid").find("td").eq(celdasExtraidas[f]));//Importante: NO APLICA sino se borra lo seleccionado                                                                      
                    }
                    posicionID++;                    
                    ayudanteJuego.preparaMarca(palabrasContestadas_arr[idx]);
                }
                terminaPintadoInterrumpido = true;
            }
        },
        preparaMarca : function(idx) {
            var w = $("li").get(idx);
            Visualizador.marcaPalabraEncontrada(w,idx);//Marca la palabra encontrada en la lista de palabras
            if(terminaPintadoInterrumpido){
                guardaPalabrasContestadas(idx);
            }        
        }
    }
    })(jQuery);
}