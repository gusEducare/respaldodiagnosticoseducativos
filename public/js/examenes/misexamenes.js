$(document).ready(function(){
    $('#detallesExamen').foundation();
    
    $('.jq_detalles').on('click', function(){
        $('#detallesExamen').foundation('reveal', 'open'); 
    });
    $('#detallesExamen').foundation('reveal', 'close');
    $('a.close-reveal-modal').trigger('click');
     $(document).on('click', '#solicitarcorreo', abrirModalSolicitarReactivacion);
     //noagendado
     $(document).on('click','.examenNoAgendado',abrirModalNoAgendado);
     //nodisponible
     $(document).on('click','.examenNoDisponible',abrirModal);
     //interrumpido
     $(document).on('click','.examenInterrumpido',abrirModalSolicitarReactivacion);
     //interrumpidovencido
     $(document).on('click','.examenInterrumpidoVencido',abrirModal);
     //interrumpidoresolver
     $(document).on('click','.examenInterrumpidoResolver',abrirModal);
     //reprobado 
     $(document).on('click','.examenReprobado',abrirModal);
     //aprobado
     $(document).on('click','.examenAprobado',abrirModal);
     $(document).on('click', '#enviarSolicitud', solicitud_reactivacion);
     $("#cerrarModal").click(function(){$('#solicitarReactivacion').foundation('reveal','close'); });
});
function abrirModalSolicitarReactivacion(){
    $('#interrumpidoModal').foundation('reveal', 'open');
}

    function abrirModalNoAgendado(){
     $('#generalModal').foundation('reveal','open');
     $('.titulo').html('Examen No Agendado');
     $('.subtitulo').html('Espera a que tu profesor agende el examen.');
    }

    function abrirModal(parametro){
         var idExaUser = $(this).attr('id').split('_');
         var idExamen = idExaUser[1];
         var idUsuario = idExaUser[2];
         var claveEstatus = idExaUser[3];
         var idGrupo = idExaUser[4];
         console.log(idExaUser,idExamen,idUsuario,claveEstatus,idGrupo);
        $.ajax({
           type:'POST',
           dataType:'json',
           url : $('#urlExamenCalificacion').val(),
           data : {
               "_idexamen"        : idExamen,
               "_idExamenUsuario" : idUsuario,
               "_claveEstatus"    : claveEstatus ,
               "_idGrupo"         : idGrupo
           },
           success : function(data) {
               if(data['t04calificacion']){
                    $('.calif').html(data['t04calificacion']);
               if(claveEstatus === 'APROBADO' ){
                    $('#generalModal').foundation('reveal','open');
                    $('.titulo').html('Examen Aprobado');
                    $('.subtitulo').html('El examen tiene una calificación de ' + data['t04calificacion'] + '.');
                }
                if(claveEstatus  === 'REPROBADO'){
                   $('#generalModal').foundation('reveal','open');
                   $('.titulo').html('Examen Reprobado');
                   $('.subtitulo').html('El examen tiene una calificacion de ' + data['t04calificacion'] + '.');
               }
           }else{
                 if(claveEstatus === 'NODISPONIBLE'){
                        $('#generalModal').foundation('reveal', 'open');
                        $('.titulo').html('Examen No Disponible');
                        $('.subtitulo').html('El examen ya fue agendado y estará disponible el día ' + data['fechaInicio'] +
                                             ' a las ' + data['horaInicio'] + ' hasta el día ' + data['fechaFinal'] + ' a las '
                                              + data['horaFinal'] + '.');
                 }
                 if(claveEstatus === 'INTERRUMPIDOVENCIDO'){
                        $('#generalModal').foundation('reveal', 'open');
                        $('.titulo').html('Examen Vencido');
                        $('.subtitulo').html('Tu examen se encuentra interrumpido pero ha vencido, las fechas para presentarlo fueron: del dia ' +
                                             data['fechaInicio'] + ' a las ' + data['horaInicio'] + ' hasta el día ' + data['fechaFinal'] + ' a las ' + 
                                             data['horaFinal'] + ' espera a que tu profesor reactive el examen.')
                 }
                 if(claveEstatus === 'INTERRUMPIDORESOLVER'){
                        $('#generalModal').foundation('reveal', 'open');
                        $('.titulo').html('Examen Por Resolver');
                        $('.subtitulo').html('El examen ya fue agendado y estará disponible el día ' + data['fechaInicio'] + ' a las ' + data['horaInicio'] + '.');
                }
            }
           },
           error : function(json){
               console.log('error al cargar los datoss: '+json);
           }
        });
    }
    
    function solicitud_reactivacion(){	
		
	//$('#txtCorreoVal').removeClass('textError');
	
	var _strCorreo = $("#correoProfimput").prop("value");
	var _strDatosalumno=$("#datosalumno").prop("value");
        var _strCorreoalumno=$("#correolumno").prop("value");
        var _stridUserPer=$("#iduspermput").prop("value");
        var _stridExaUser=$("#idexausrimput").prop("value");
        
	//validacion de correo
	if (_strCorreo != ''){
		//validar que la nomenclatura del correo sea correcta
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(_strCorreo)){
			 $.ajax({ 
						url: $('#urlSolicitaReactivacion').val(),
						dataType: 'json',
						cache: false,
						type: 'POST',
						data: {"_strCorreo":_strCorreo,
                                                "_strDatosalumno":_strDatosalumno,
                                                "_strCorreoalumno":_strCorreoalumno,
                                                "_stridExaUser":_stridExaUser,
                                                "_stridUserPer":_stridUserPer},
						beforeSend: function() {
                                                     $('#enviarSolicitud').attr("disabled", true);
							},
						success: function(data) { 
                                                    $('#solicitarReactivacion').unblock();
                                                   // $('#enviarSolicitud').hide();
                                                  //  $("#imgLoadingRecuperar").slideUp("slow");
								if(data['respuesta'] == true){
                                                                    
									$('#_Msg').addClass('alert alert-success');
									$('#_Msg').html('Se ha enviado un e-mail a la cuenta de correo de su profesor solicitando la reactivacion.');		
									$("#_Msg").slideDown("slow");
                                                                        $('#enviarSolicitud').addClass('hide');
                                                                        $('#cerrarModal').removeClass('hide');
								}
                                                                else if(data['respuesta'] == false){
                                                                        $('.tituloModal').html('Ya se envio el correo a tu profesor.');
                                                                        $('.subtituloModal').addClass('hide');
                                                                        $('#correoProfimput').hide();
                                                                        $('#enviarSolicitud').addClass('hide');
                                                                        $('#cerrarModal').removeClass('hide');
                                                                }
								else{
									$("#modalRecuperarHeader").slideDown("slow");	
									$('#_Msg').addClass('alert alert-info');
									$('#_Msg').html(data['msg']);		
									setTimeout('$("#_Msg").slideUp("slow");',6000);
									$("#_Msg").slideDown("slow");
			
								}
							},
						error: function(data) {
							$("#imgLoadingRecuperar").slideUp("slow");
							$("#modalRecuperarHeader").show();	
							$('#_Msg').addClass('alert alert-info');
							$('#_Msg').html('Hubo un error en el proceso intenta más tarde.');		
							setTimeout('$("#_Msg").slideUp("slow");',6000);
							$("#_Msg").slideDown("slow");
						}
				});		
		}else{
                }
	}else{
		$('#txtCorreoVal').addClass('textError');		
	}
}