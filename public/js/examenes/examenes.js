$(document).ready(function(){  
    
    
    $('#boton-editar').on('click',function(e){
        console.log('editar pregunta');
        var valCategoria = $($('#textAreaCat').tagEditor('getTags')[0].tags).length;
        var valEtiqueta = $($('#textAreaTag').tagEditor('getTags')[0].tags).length;
        var miForm = $('#Examen').valid();
        e.preventDefault();
        e.preventDefault();
        if(valCategoria === 0){
            miForm = false;
            $('#errorSelectCategoria').show();
        }
        else{
            $('#errorSelectCategoria').hide();
        }
        if(valEtiqueta === 0){
            miForm = false;
            $('#errorSelectTag').show();
        }
        else{
            $('#errorSelectTag').hide();
        }
        if(miForm === true){
            console.log('form validado');
            
            var arrStrCategorias    = $('#textAreaCat').tagEditor('getTags')[0].tags;
            var arrStrEtiquetas     = $('#textAreaTag').tagEditor('getTags')[0].tags;
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#urlUpdateTagCate').val(),
                data: {
                    'urlIdExamen': $('#urlIdExamen').val(),
                    'arrStrCategorias': arrStrCategorias,
                    'arrStrEtiquetas': arrStrEtiquetas
                },
                beforeSend: function () {
                },
                success: function (json) {
                    $('#modalConfir').foundation('reveal', 'open');
                    $("#modalSucces").show();
                    $("#modalBtnAceptar").show();
                    
                },
                error: function (json) {
                    console.log('error en el json'+ json);
                }
            });
        }
    });
    
    $("#cerrar-reveal").click(function () {
        $('#modalConfir').foundation('reveal', 'close');
        $('#Examen').submit();
    });
    
    $("#modalBtnCancel").click(function () {
        $('#modalPregunta').foundation('reveal', 'close');
    });
    
    $('#acepta-delete').on('click', function () {
        $.ajax({
            type: 'POST',
            url: $('#eliminar').val(),
            data: {
                'id_preg': $('#idEliminaPreg').val()
            },
            dataType: 'json',
            error: function () {
                console.log('error peticion ajax');
                $('#modalAjaxError').show();
                $("#modalBtnAceptar").show();
            },
            beforeSend: function () {
                $('#modalPregunta').foundation('reveal', 'open');
            },
            success: function (json) {
                console.log(json);
                if (json == true) {
                    $("#modalSucces").show();
                    $("#modalError").hide();
                    $('#modalAjaxError').hide();
                } else {
                    $("#modalError").show();
                    $("#modalSucces").hide();
                    $('#modalAjaxError').hide();
                }
                $("#modalBtnAceptar").show();

                $("#cerrar-reveal").click(function () {
                    parent.window.location.reload(true);
                });
            }
        });
    });
   
    $('.elimina_pregunta').on('click',function (){
        var id = $(this).attr('id').split('_');
        id = id[1];
        $('#idEliminaPreg').val(id);        
        $('#modalAlert').foundation('reveal', 'open');
           
    });
    
    
    $('#id_escala').change(function(){
        if ($('#id_escala').val() === 'default') {
            $('#divMinAprobatorio').slideUp();
        }
        else{
            $('#divMinAprobatorio').slideDown();
            var arrEscala = $("#id_escala option[value='"+$('#id_escala').val()+"']").text().split('-');
            
            //$('#sliderEscala').attr('data-options', "start: "+ arrEscala[0].trim() +"; end: "+ arrEscala[1].trim()+";");
            //$(document).foundation('slider', 'reflow');
            
            updateSliderRange(arrEscala[0].trim() ,arrEscala[1].trim() ,1);
             
        }
    });
    
    
    
    function setValue(){
        $(document).foundation({
            slider: {
                on_change: function(){
                    $('#sliderOutput3').html($('#sliderEscala').attr('data-slider'));
                    $('#minimo_aprobatorio').val($('#sliderEscala').attr('data-slider'));
                }
            }
        });
    }
            
    function updateSliderRange(left,right,value){
        value = (value)? value : left; // default to left if unspecified

        var data_options = "display_selector: #sliderOutput; start: "+left+"; end: "+right+";";
        // set the new options
        $('#sliderEscala').attr('data-options',data_options);
        // clear the styles
        $('#sliderEscala span').attr('style','');

        // push the html back to break DOM bindings
        $('#sliderEscala')[0].outerHTML = $('#sliderEscala')[0].outerHTML;

        // get foundation to rebind
        $(document).foundation('slider','reflow');
        $('#sliderEscala').foundation('slider','set_value',value); // set the slider
    }
    
    
    var id_examen = 0;
    
    //Creo los handlers
    
    $('#boton-reset').on('click', cancelaExamen);
    
    
    
    function cancelaExamen(){
        //window.location = $('#urlCancelarEvento').val();
    }
    
   
    
    function agregaPregunta(evt){
        //console.log('Pregunta: ' + evt.target.id);
        $('.jq_borrar_preg').unbind('click', eliminaPregunta);   
        var idPregunta = evt.target.id;
        var pregunta = $('#'+idPregunta).html();
        //var firstChevron = $('.jq_borrar_preg').length ? '<a class="tiny jq_up fa fa-chevron-up" href="#"></a>' : '';
        var divPregunta = 
                            '<div class="large-12 columns pregunta-seleccionada ui-state-highlight item">'+
                                '<div class="text-left large-12 medium-12 small-12 columns seleccionada" id="preg_'+idPregunta+'">'+
                                    pregunta+
                                '</div>'+
                        //        '<div class="text-left large-2 medium-2 small-2 columns">'+
                       //             '<a class="tiny jq_borrar_preg fa fa-trash-o" href="#"></a>'+
                     //               '<a class="tiny jq_down fa fa-chevron-down" href="#"></a>'+
        //                            firstChevron+
                        //        '</div>'+
                            '</div>';
        $('.seccion-general').append(divPregunta);        
        $('#'+idPregunta).parent().hide();       
        $('.fa-chevron-down').removeClass('hide');
        if($('.fa-chevron-down').length > 1){
            $('.fa-chevron-down').last().addClass('hide');       
        }
        $('.jq_borrar_preg').bind('click', eliminaPregunta);        
    }
    
    function eliminaPregunta(evt){
        var strPregunta = evt.target.parentElement.previousSibling.id;
        var arrPregunta = strPregunta.split('_');
        var idPregunta = arrPregunta[1];
        //console.log('Preg: ' + idPregunta);
        $('#'+idPregunta).parent().show(); 
        $('#preg_'+idPregunta).parent().remove();
    }
    
    function grdPregExamen(){
        var ids = [];
        $('.pregunta-seleccionada').each(function (){
            if(this.id){
                var arrText = $(this).attr('id').split('elemento');
                var idNum = arrText[1];
                ids.push(idNum);
            }
            console.log('grdPregExamen()',ids);
        });
        console.log('grdPregExamen()',ids);
    }
    
    
    
    function creaObjetoPregunta(){
        
       var _ProjJsonPreg = new Object();
       _ProjJsonPreg.seccion = new Object();       
       
       $('.seccion-general').each(function(){           
            var strSeccion = $(this).attr('id');
            var arrSeccion = strSeccion.split('_');
            var idSeccion = arrSeccion[1];
           _ProjJsonPreg[idSeccion] = new Object();
           _ProjJsonPreg[idSeccion].preguntas = new Object();
           $(this).children('.pregunta-seleccionada').each(function(){
                var strPreg = $(this).children().attr('id');
                var arrPreg = strPreg.split('_');
                var idPreg = arrPreg[1];
                _ProjJsonPreg[idSeccion].preguntas[idPreg] = $(this).children().html();
               // _ProjJsonPreg['seccion'].preguntas['value'] = 1;
           });
       });   
       
       return _ProjJsonPreg;
    }
    
    
        
    function limpiarFormulario(){
        var i=0;
        $('._objExamen').each(function(){
           $(this).val('');
           $(this).removeAttr('selected');
           $(this).removeAttr('checked');
        });        
    }   
    
    if($('#urlPaginaActual').val() === 'preguntas'){
        console.log('index preguntas');
        $('.display').dataTable();
    }else{
        if($('#urlIdExamen').val()===''){
            console.log('add nuevo examen');
            $('#textAreaCat').tagEditor({
                autocomplete : {
                    delay: 0, // show suggestions immediately
                    position : { collision: 'flip' }, // automatic menu position up/down
                    source   : $('#urlConsultaCat').val(),
                    minLength: 0
                },
                forceLowercase: false,
                placeholder: 'Agregar categorias...'
            });	 
            $('#textAreaTag').tagEditor({
                autocomplete: {
                    delay: 0, // show suggestions immediately
                    position: { collision: 'flip' }, // automatic menu position up/down
                    source : $('#urlConsultaTags').val(),
                    minLength: 0
                },
                forceLowercase: false,
                placeholder: 'Agregar etiquetas...'
            });
            $(document).foundation({
                slider: {
                    on_change: function(){
                        $('#sliderOutput3').html($('#sliderEscala').attr('data-slider'));
                        $('#minimo_aprobatorio').val($('#sliderEscala').attr('data-slider'));
                    }
                }
            });
        }else{
            console.log('actualizar el examen');
            //carga la relacion de categorias que tiene almacenada el examen en la vista Editar examen
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#urlConsultaEditCat').val(),
                data: {
                    'idPregunta': $('#urlIdExamen').val()
                },
                success: function(json){
                    var arr_cat =[];
                    for(var a in json){
                        arr_cat.push(json[a]['c08nombre']);
                    }
                    $('#textAreaCat').tagEditor({
                        initialTags: arr_cat,
                        autocomplete: {
                            delay: 0,
                            position: {collision: 'flip'},
                            source: $('#urlConsultaCat').val(),
                            minLength: 0
                        },
                        forceLowercase: false,
                        placeholder: 'Agregar categorias...'
                    });
                },
                error: function(){
                    console.log('error en la peticion');
                }
            });
            //carga la relacion de etiquetas que tiene almacenada el examen en la vista Editar examen
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $('#urlConsultaEditEtiq').val(),
                data: {
                    'idPregunta': $('#urlIdExamen').val()
                },
                success: function(json){
                    var arr_tags = [];
                    for(var a in json){
                        arr_tags.push(json[a]['c09nombre']);
                    }
                    $('#textAreaTag').tagEditor({
                        initialTags: arr_tags,
                        autocomplete: {
                            delay: 0,
                            position: {collision: 'flip'},
                            source: $('#urlConsultaTags').val(),
                            minLength: 0
                        },
                        forceLowercase: false,
                        placeholder: 'Agregar etiquetas...'
                    });
                },
                error: function(){
                    console.log('error en la peticion');
                }
            });
            
            //muestra valor de minimo aprobatorio
            var valEscala = $('#id_escala').val();
            var minAproEdit = $('#minimo_aprobatorio').val();
            //$('#sliderOutput3').html(minAproEdit);
            if (valEscala === '1'){
                updateSliderRange(0,10,minAproEdit);
                setValue();
            }
            if (valEscala === '2'){
                updateSliderRange(0,100,minAproEdit);
                setValue();                
            }
            if (valEscala === '3'){
                updateSliderRange(0,1000,minAproEdit);
                setValue();
            }
            $('#divMinAprobatorio').removeAttr('style');
        }
    }
    
});

$(document).ready(function(){
    $('#boton-almacenar').click(function(e){
        e.preventDefault();
        $('#txtCategorias').val($('#textAreaCat').tagEditor('getTags')[0].tags);
        $('#txtEtiquetas').val($('#textAreaTag').tagEditor('getTags')[0].tags);
       
        var boolSumbit = true;
         if(!$('#txtCategorias').val()){
            boolSumbit = false;
            //mostrar error categorias
            $('#errorSelectCategoria').show();
        }
        else{
            $('#errorSelectCategoria').hide();
        }
        if(!$('#txtEtiquetas').val()){
            boolSumbit = false;
            //mostrar error categorias
            $('#errorSelectTag').show();
        }
        else{
            $('#errorSelectTag').hide();
        }
        if(!$('#Examen').valid()){
            boolSumbit = false;
        }
        
        //verificar categorias y tags
        if(boolSumbit === true){
            //get all  elements
            var arrDataExamen = new Array();
            $('.fieldExamen').each(function(){
                arrDataExamen[$(this).attr('id')] = $(this).val();
            });
            console.log(arrDataExamen);
            
            var _intIdExamen = -1;
            if($('#IdExamen').val()){
                _intIdExamen = $('#IdExamen').val();
            }
            
            
            var _ProjJson = new Object();
           _ProjJson.data = new Object();  
           _ProjJson.categoria  = new Object(); 
           _ProjJson.etiqueta   = new Object();
           

           $('.fieldExamen').each(function(){
                    if(typeof   _ProjJson.data[$(this).attr('id')] === 'undefined'){
                        _ProjJson.data[$(this).attr('id')] = new Object();
                    }
                    _ProjJson.data[$(this).attr('id')] = $(this).val();
                    _ProjJson.categoria['textAreaCategoria'] = $('#textAreaCat').tagEditor('getTags')[0].tags;
                    _ProjJson.etiqueta['textAreaEtiqueta'] = $('#textAreaTag').tagEditor('getTags')[0].tags;
           });
            $.ajax({
                type:'POST',
                dataType:'json',
                url:  $('#urlCreaExamen').val(),
                data:{
                    'arrDataExamen':_ProjJson,
                    'idExamen'  : _intIdExamen
                },
                beforeSend: function() {
                    $('#Examen').block({
						message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Procesando...',
					}); 
                },
                success:function(json){
                    $('#Examen').unblock();
                        if( json ){
                        
                            $('#modalSuccessExamen').foundation('reveal', 'open').on('close.fndtn.reveal', '[data-reveal]', function () {
                                window.location = $('#urlExamenesIndex').val();
                            });

                        }else{
                            alert('Ocurrió un error al generar el examen, por favor intenta nuevamente. ');

                        }
                },
                error: function(json){
                    $('#Examen').unblock();
                     alert("Hubo un error al procesar los datos: ");
                }
            });
        }
    });
    
    $('#btnAnotherExamen').click(function(){
        location.reload();
    });
    
    
    
 });
 
 
  
    
    function creaObjetoExamen(){
       var _ProjJson = new Object();
       _ProjJson.examen = new Object();       
       _ProjJson._isValid = true;
       
       $('.fieldExamen').each(function(){
            if(typeof   _ProjJson.examen[$(this).attr('id')] === 'undefined'){
                _ProjJson.examen[$(this).attr('id')] = new Object();
            }
            _ProjJson.examen[$(this).attr('id')] = $(this).val();
            _ProjJson.examen['textAreaCategoria'] = $('#textAreaCat').tagEditor('getTags')[0].tags;
            _ProjJson.examen['textAreaEtiqueta'] = $('#textAreaTag').tagEditor('getTags')[0].tags;
       });
       
       
       //almacena id de las preguntas
       var ids = [];
       $('.pregunta-seleccionada').each(function (){
           if(this.id){
               var arrText = $(this).attr('id').split('elemento');
               var idNum = arrText[1];
               ids.push(idNum);
           }
       });
            _ProjJson.preguntas = ids;
            
       return _ProjJson;
    }
    
    function creaExamen( _ProjJson, _intIdExamen, _objPregunta ){
        
        
        $.ajax({
                type:'POST',
                dataType:'json',
                url:  $('#urlCreaExamen').val(),
                data:{ 
                        'objJson'   : _ProjJson, 
                        'objPreg'   : _objPregunta,
                        'idExamen'  : _intIdExamen  
                    },
                beforeSend: function() {
                },
                
                success:function(json){
                        if( json ){
                        console.log(json);
                        $('#myModal').foundation('reveal', 'open');
                           
                        }else{
                            alert('failed loading modal');
                            //  $.mensaje("crearMensaje", { tipo_mensaje: "alerta", mensaje: "Hubo un error al procesar los datos" });
                        }
                        //console.log("Los datos se almacenaron correctamente");
                        limpiarFormulario();  
                },
                error: function(json){
                     //  $.mensaje("crearMensaje", { tipo_mensaje: "alerta", mensaje: "Hubo un error al procesar los datos" });
                     console.log("Hubo un error al procesar los datos: " + json);
                }
        });
    }
    
    




$(document).ready(function(){
    var numMin = 1;
    var msgMinlength        = 'Minimo '+numMin+' caracteres';
    var msgRequire          = 'campo requerido';
    var msgMinlength5       = 'Minimo '+cdnMinlength+' caracteres';
    var cdnMinlength        = 3;
    var msgNumeros          = 'solo numeros';
    var msgSelectComboBox   = 'Debes seleccionar una Opcion';
    var msgMaxlength20      = 'solo se permite 20 caracteres';
    var cdnMaxlength        = 20;
    var msgMaxlengthIns     = 'solo se permite 500 caracteres';
    var cdnMaxlengthIns     = 500;
    var _objExamen = new Object();
    
    if($('#Examen').length === 0){
        
    }else{
        jQuery('#Examen').validate({
           ignore: ":hidden:not(select)",
           rules:{
               nombre            :{required: true, minlength: cdnMinlength },
               nombre_corto_clave:{required: true, minlength: cdnMinlength },
               instrucciones     :{required: true, minlength: cdnMinlength},
               descripcion       :{required: true, minlength: cdnMinlength},
               num_intentos      :{required: true, minlength: numMin,number: true},
               tiempo            :{selectComboBox: true},
               id_tipo_examen    :{selectComboBox: true},
               id_escala         :{selectComboBox: true},
               id_nivel          :{selectComboBox: true}
           },
           messages:{
               nombre            :{required: msgRequire,minlength:msgMinlength5},
               nombre_corto_clave:{required: msgRequire,minlength:msgMinlength5},
               instrucciones     :{required: msgRequire,minlength:msgMinlength5},
               descripcion       :{required: msgRequire,minlength:msgMinlength5},
               num_intentos      :{required: msgRequire,minlength:msgMinlength,number: msgNumeros}
           }
        });
        validarComboBox();
    }
             
       function validarComboBox(){
           jQuery.validator.addMethod('selectComboBox', function (value, element) {
               var boolResponse = true;
                if (element.value === '') {
                    boolResponse = false;
                }
                if(element.value === 'default'){
                    return false;
               } else return true;

                return boolResponse;
            }, msgSelectComboBox);
       }
       
       
       
       
});
   $('#btnCancelCopiarPreg').on('click',function(){
       $('#modalClonarPregunta').foundation('reveal', 'close');
   });
   
    $('#acepta-clonar-pregunta').on('click', function () {
        console.log('acepta clonar pregunta');
        $.ajax({
            url: $('#urlCopiarPregunta').val(),
            dataType: 'json',
            type: 'POST',
            data: {
                "_idPregunta": $('#idPreguntaCopiar').val()
            },
            success: function (json) {
                console.log(json['_idPregunta']);
                window.location = '/preguntas/edit/' + json['_idPregunta'];
                console.log(json);
                $('#modalClonarPregunta').foundation('reveal', 'close');
            },
            error: function (json) {
                console.log('error en la peticion: ' + json);
            }
        });
    });
    //verifica si la pregunta esta alamacenada en un examen publicada de ser asi se copia la pregunta 
    //en caso contrario manda a la edicion directa de la pregunta
    $('.editar-pregunta').on('click',function(event){
        event.preventDefault();
        var getId = $(this).attr('id').split('_');
        getId = getId[1];
        $('#idPreguntaCopiar').val(getId);        
        $.ajax({
            url: $('#urlPreguntaInExa').val(),
            dataType: 'json',
            type: 'POST',
            data: {
                "_idPregunta": $('#idPreguntaCopiar').val()
            },
            success: function (json) {
                if(json === false){
                     window.location = location.pathname+'/edit/'+getId;
                }else{
                    $('#modalClonarPregunta').foundation('reveal','open');
                }        
            },
            error: function (json) {
                console.log('error en la peticion: ' + json);
            }
        });
    });