$(document).ready(function(){
	$(document).foundation();
//	reloadTables(); 
        $('.display').dataTable();
        $(document).on('click','#btnCancelarEliminaExamen', cerraModalAdvertencia);
        
    $('#btnTerminaCopia').click(function(){
            cerrarModalCopiar(true);
        });
        
        $('#btnCancelarCopia').click(function(){
            cerrarModalCopiar(false);
        });
        $('#btnAceptarCopia').click(function(e){
           e.preventDefault();
           if($("#formCopiarExamen").valid() === true){
                $.ajax({
                    url: $('#urlCopiarExamenes').val(),
                    dataType: 'json',
                    type:'POST',		
                    data:{
                        "_idExamen" : $('#idCopiarExamen').val(),
                        "_opCopia" : $('input[name=radioCopia]:checked').val()
                    },
                    beforeSend: function(){
                            
                    },
                    success: function(json){
                            $('#formCopiarExamen').hide();;
                            $('#copia_mensaje').html(json['_msg']);
                            $('#contenedor_fin_copia').show();
                    },                    
                    error: function(){
                            $('#formCopiarExamen').hide();;
                            $('#copia_mensaje').html('No se pudo completar la copia, por favor intenta mas tarde');
                            $('#contenedor_fin_copia').show();
                    }
                });
            }
        });
        
        $( "#formCopiarExamen" ).validate({
                rules: {
                  radioCopia: {required: true}
                },
                messages: {
                 radioCopia: "Debes elegir que tipo de copia necesitas"
                }
        });
        
});

function cerraModalAdvertencia(){
    $('#advertenciaEliminaExamen').foundation('reveal', 'close');
}

function abrirModalCopiar(_idExamen){
    $('#idCopiarExamen').val(_idExamen);
    $('#ayudaCopiarExamen').foundation('reveal', 'open');
}

function cerrarModalCopiar(_refresh){
    $('#ayudaCopiarExamen').foundation('reveal', 'close');
    if(_refresh === true){
        location.reload();
    }
}

function reloadTables(){
	$.ajax({
		type:'POST',
		dataType: 'json',
		url: $('#urlDataExamenes').val(),
		data:{
			
		},
		beforeSend: function(){
			/*$("#grupos").block({
				message: '<i class="fa fa-spinner fa-spin"></i>&nbsp;Actualizando...'
			});*/ 
		},
		success: function(json){
			if(json.length !== false ){
				$.table("createContainers", json);
				$(document).foundation();
			}
			else{
				//$("#grupos").html("<p class='lead'>No existen grupos.</p>");
			}
			
		},
		error: function(json){
			//$("#grupos").unblock();
		}
	});
}


function eliminarExamen(intIdExamen){
    var urlDeleteExamenes = $('#urlDeleteExamenes').val();
    $('#advertenciaEliminaExamen').foundation('reveal', 'open');
    
    $('#btnEliminaExamen').on('click', function (){
        alert(urlDeleteExamenes+'/'+intIdExamen);
    });
                   	
}
//verifica si el examen ya esta publicada o no 
function siExamPublicada(idExamen){ 
        $.ajax({
            url: $('#urlPreguntaInExa').val(),
            dataType: 'json',
            type: 'POST',
            data: {
                "_idExamen": idExamen
            },
            success: function (json) {
                if(json === false){
                    $('.editarExamen').on('click',function(){
                        
                    });
                    
                    console.log('examen '+idExamen+' sin publicar');
                    var url = $('#redirecEditLigarPregExam').val();
                    $(location).attr('href',url+'/'+idExamen);
                }else{
                    console.log('examen '+idExamen+' publicado');
                    $('#confirmaCopiaExamen').foundation('reveal','open');
                    $('#btnConfirmAceptarCopia').on('click',function (){
                        $('#confirmaCopiaExamen').foundation('reveal','close');
                        abrirModalCopiar(idExamen);
                    });
                    $('#btnConfirmCancelaCopia').on('click',function(){
                        $('#confirmaCopiaExamen').foundation('reveal','close');
                    });
                }        
            },
            error: function (json) {
                console.log('error en la peticion: ' + json);
            }
        });
}

function siExamPublicadaEdit(idExamen){
    $.ajax({
            url: $('#urlPreguntaInExa').val(),
            dataType: 'json',
            type: 'POST',
            data: {
                "_idExamen": idExamen
            },
            success: function (json) {
                if(json === false){
                    console.log('examen '+idExamen+' sin publicar');
                    var url = $('#redirecEditExam').val();
                    $(location).attr('href',url+'/'+idExamen);
                }else{
                    console.log('examen '+idExamen+' publicado');
                    $('#confirmaCopiaExamen').foundation('reveal','open');
                    $('#btnConfirmAceptarCopia').on('click',function (){
                        $('#confirmaCopiaExamen').foundation('reveal','close');
                        abrirModalCopiar(idExamen);
                    });
                    $('#btnConfirmCancelaCopia').on('click',function(){
                        $('#confirmaCopiaExamen').foundation('reveal','close');
                    });
                }        
            },
            error: function (json) {
                console.log('error en la peticion: ' + json);
            }
        });
}


