/*   ______                _                                 
    |  ____|              (_)                               
    | |__ _   _ _ __   ___ _  ___  _ __   ___  ___    
    |  __| | | | '_ \ / __| |/ _ \| '_ \ / _ \/ __| 
    | |  | |_| | | | | (__| | (_) | | | |  __/\__ \  
    |_|   \__,_|_| |_|\___|_|\___/|_| |_|\___||___/   
      _____                           _           
     / ____|                         | |          
    | |  __  ___ _ __   ___ _ __ __ _| | ___  ___ 
    | | |_ |/ _ \ '_ \ / _ \ '__/ _` | |/ _ \/ __|
    | |__| |  __/ | | |  __/ | | (_| | |  __/\__ \
     \_____|\___|_| |_|\___|_|  \__,_|_|\___||___/
@proyecto:         NUEVA PLATAFORMA DE EVALUACIONES 2015
@programa:         Funciones generales (código fuente);
@author:           Grupo Educare S.A. de C.V.
@desarrollador     gregorios@grupoeducare.com
@fecha de inicio:  01 de Noviembre de 2014
@fecha de entrega: Febrero 2015                               -->
***************************************************
      V A R I A B L E S     G E N E R A L E S
***************************************************/
var t11id_pregunta;
var t11id_pregunta_parent;
var c03id_tipo_pregunta;
var t11pregunta;
var t11nombre_corto_clave;
var t11instruccion;
var t11tiempo_limite;
var t11columnas;
var t11numerar;
var t11fecha_registro;
var t11fecha_actualiza;
var t11estatus;
var t11usuario;
var t13id_respuesta;
var t13id_respuesta_parent;
var t13respuesta;
var t13orden;
var t13fecha_registro;
var t13fecha_actualiza;
var t13estatus;
var t13Imagen;
var svgNS = "http://www.w3.org/2000/svg";
var svgNSLink = "http://www.w3.org/1999/xlink";
var nombreSVG = "elementosSVG";
var valor = $('#jsonPregunta').val();
var incisos_arr;
var obj;
var arregloDeCadenas;
var colores_arr = ["#006600","#CC0000","#FF6600","#CC6666","#333399","#3399FF","#FF9933","#9933CC","#003366","#666666","#666699","#FF66CC","#663333","#009900","#669933","#0066CC","#999933","#990000","#FF9933","#9966FF"];
var preguntasArr = new Array();
var respuestasArr = new Array();
var posColores = 0;
var numEstilo;//1=RadialButtons, 2=Botones azules  3=cajasColumnas
var numeracion = "3";//1= ABCDARIO 2= abcdario  3=Numérica 4=Romana
var contieneRespuestas = true;
var primeraVezEspacios = true;
var textos_arr = ["Erase una vez un",". Como habia dejado de ",", de una terrible ",", ansiosos de libertad, salieron de sus "," y empezaron a corretear por la alfombra recien ",".Hare un ",", un nenito precioso, redondo, con ojos de "," y convirtio al nenito en su inseparable ",", durante los tristes dias de aquel ",". empezaron a ser mas largos y los ",", aqui termina el cuento."];

// Paso 1 - Rompecabezas, pizarron
var imagen;
var imagenesPizarron;
var pizarron = false; //sin pizarron
var tipoMultimedia = "imagen";//texto imagen audio video
var idioma = 'es';//es-español   en-inglés
var subtipo = 'frase';//con frase  //sin frase
var preguntasBidimensional_arr;
var cuantasPreguntas = 2;
var navegador;
var navegador_version;
var versionInt;
//var parametrosMatriz = ["E","MB","B","R","M"];
var vuelta = 0;

/**************************************************
     F U N C I O N E S     G E N E R A L E S
**************************************************/
function obtenerValoresJSON (){    
    //$('#jsonPregunta').val( '{"respuestas":[{"t13id_respuesta":"4143","t13respuesta":"E","t13orden":"1","t13fecha_registro":"2015-04-15 12:19:39","t13fecha_actualiza":"2015-04-15 12:19:39"},{"t13id_respuesta":"4144","t13respuesta":"MB","t13orden":"2","t13fecha_registro":"2015-04-15 12:19:39","t13fecha_actualiza":"2015-04-15 12:19:39"},{"t13id_respuesta":"4145","t13respuesta":"B","t13orden":"3","t13fecha_registro":"2015-04-15 12:19:39","t13fecha_actualiza":"2015-04-15 12:19:39"},{"t13id_respuesta":"4146","t13respuesta":"R","t13orden":"4","t13fecha_registro":"2015-04-15 12:19:39","t13fecha_actualiza":"2015-04-15 12:19:39"},{"t13id_respuesta":"4147","t13respuesta":"M","t13orden":"5","t13fecha_registro":"2015-04-15 12:19:39","t13fecha_actualiza":"2015-04-15 12:19:39"}],"preguntas":[{"t11id_pregunta":"1079","t11id_pregunta_parent":"0","c03id_tipo_pregunta":"1","t11pregunta":"¿Cómo consideras el servicio de comedor de la empresa?","t11nombre_corto_clave":"","t11instruccion":"\u003Cp\u003EInstrucci\u00f3n - 
    ////Opci\u00f3n M\u00faltiple\u003C\/p\u003E","t11tiempo_limite":"","t11columnas":null,"c06id_numeracion":"0","t11fecha_registro":"2015-04-15 12:19:39","t11fecha_actualiza":"2015-04-15 12:19:39","t11estatus":"ACTIVA"},{"t11id_pregunta":"1079","t11id_pregunta_parent":"0","c03id_tipo_pregunta":"1","t11pregunta":"¿Qué te parece el sazón de los platillos?","t11nombre_corto_clave":"","t11instruccion":"\u003Cp\u003EInstrucci\u00f3n - Opci\u00f3n M\u00faltiple\u003C\/p\u003E","t11tiempo_limite":"","t11columnas":null,"c06id_numeracion":"0","t11fecha_registro":"2015-04-15 12:19:39","t11fecha_actualiza":"2015-04-15 12:19:39","t11estatus":"ACTIVA"},{"t11id_pregunta":"1079","t11id_pregunta_parent":"0","c03id_tipo_pregunta":"1","t11pregunta":"¿Cómo consideras el trato y la atención del personal?","t11nombre_corto_clave":"","t11instruccion":"\u003Cp\u003EInstrucci\u00f3n - Opci\u00f3n M\u00faltiple\u003C\/p\u003E","t11tiempo_limite":"","t11columnas":null,"c06id_numeracion":"0",
    ////"t11fecha_registro":"2015-04-15 12:19:39","t11fecha_actualiza":"2015-04-15 12:19:39","t11estatus":"ACTIVA"}],"pregunta":{"t11id_pregunta":"1079","t11id_pregunta_parent":"0","c03id_tipo_pregunta":"1","t11pregunta":"NO LLEVA TEXTO","t11nombre_corto_clave":"","t11instruccion":"\u003Cp\u003EInstrucci\u00f3n - Opci\u00f3n M\u00faltiple\u003C\/p\u003E","t11tiempo_limite":"","t11columnas":null,"c06id_numeracion":"0","t11fecha_registro":"2015-04-15 12:19:39","t11fecha_actualiza":"2015-04-15 12:19:39","t11estatus":"ACTIVA","usuario":[1]}}');
    obj = jQuery.parseJSON($('#jsonPregunta').val());
    if(typeof $('#preguntasTotal').val()  !== 'undefined'){
        objPreguntas = jQuery.parseJSON($('#preguntasTotal').val());
        obj.preguntasTotal = objPreguntas;        
    }
    t11id_pregunta = obj.pregunta.t11id_pregunta;
    t11id_pregunta_parent = obj.pregunta.t11id_pregunta_parent;
    c03id_tipo_pregunta = obj.pregunta.c03id_tipo_pregunta;
    var numRespuestas = typeof obj.respuestas === 'object' ? Object.keys(obj.respuestas).length : obj.respuestas.length;
    pizarron =  t11id_pregunta === "1147" ; // || t11id_pregunta === "932" ? true : false;
                
    //if(c03id_tipo_pregunta === "11"){    c03id_tipo_pregunta = "4"; }//Esta línea convierte la pregunta de Columnas en Relacionar Combos
    //c03id_tipo_pregunta = "6";//Esta línea permite visualizar la pregunta de tipo Abierta
    //c03id_tipo_pregunta = "3";//Esta linea permite visualizar la pregunta de tipo Falso Verdadero
    //if(c03id_tipo_pregunta === "11"){    c03id_tipo_pregunta = "12"; }//Esta línea convierte la pregunta de Columnas en Relacionar Líneas
    //if(c03id_tipo_pregunta === "11"){    c03id_tipo_pregunta = "10"; }//Esta línea convierte la pregunta de Columnas en Número Ordenar
    //c03id_tipo_pregunta = "9";//Esta linea permite visualizar la pregunta de tipo Arrastra Ordenar
    //c03id_tipo_pregunta = "17";//Esta linea permite visualizar la pregunta de tipo Corta Numérica//Esta línea convierte la pregunta de Columnas en Arrastra Corta
    //if(c03id_tipo_pregunta === "7"){    c03id_tipo_pregunta = "8"; }//Esta línea convierte la pregunta de Columnas en Arrastra Cortas
    //c03id_tipo_pregunta = "5";//Esta linea permite visualizar la pregunta de tipo Arrastra Contenedor
    //if(c03id_tipo_pregunta === "1"){    c03id_tipo_pregunta = "14"; }//Esta línea convierte la pregunta de Múltiple en Rompecabezas
    //if(c03id_tipo_pregunta === "1"){    c03id_tipo_pregunta = "13"; }//Esta línea convierte la pregunta de Múltiple en Matriz
    // if(c03id_tipo_pregunta === "1"){    c03id_tipo_pregunta = "16"; }//Esta línea convierte la pregunta de Múltiple en Sopa de letras    
    
//    console.log('c03id_tipo_pregunta '+c03id_tipo_pregunta);
//    if(c03id_tipo_pregunta === "12"){    c03id_tipo_pregunta = "18"; }//Esta línea convierte la pregunta de Columnas en Relaciona Arrastrar
    console.log('c03id_tipo_pregunta '+c03id_tipo_pregunta);
    t11pregunta = obj.pregunta.t11pregunta;
    t11nombre_corto_clave = obj.pregunta.t11nombre_corto_clave;
    t11instruccion = obj.pregunta.t11instruccion;
    t11tiempo_limite = obj.pregunta.t11tiempo_limite;
    t11columnas = obj.pregunta.t11columnas;
    t11numerar = obj.pregunta.t11numerar;
    t11fecha_registro = obj.pregunta.t11fecha_registro;
    t11fecha_actualiza = obj.pregunta.t11fecha_actualiza;
    t11estatus = obj.pregunta.t11estatus;
    t11usuario = obj.pregunta.usuario;
    pintaLetrerosComunes();
    preguntasArr = new Array();
    respuestasArr = new Array();
    for ( j=0; j<obj.preguntas.length; j++) {
        t11pregunta = obj.preguntas[j].t11pregunta; // Decodifico el html que venga de la base de datos para que se muestre bien
        t11nombre_corto_clave = obj.preguntas[j].t11nombre_corto_clave;
        t11instruccion = obj.preguntas[j].t11instruccion;
        t11tiempo_limite = obj.preguntas[j].t11tiempo_limite;
        t11columnas = obj.preguntas[j].t11columnas;
        t11numerar = obj.preguntas[j].t11numerar;
        t11fecha_registro = obj.preguntas[j].t11fecha_registro;
        t11fecha_actualiza = obj.preguntas[j].t11fecha_actualiza;
        t11estatus = obj.preguntas[j].t11estatus;
        preguntasArr[j] = t11pregunta;
    }
    for ( j=0; j<numRespuestas; j++) {
        t13id_respuesta = obj.respuestas[j].t13id_respuesta;
        t13id_respuesta_parent = obj.respuestas[j].t13id_respuesta_parent;
        t13respuesta = obj.respuestas[j].t13respuesta; // Decodifico el html que venga de la base de datos para que se muestre bien        
        if(t13respuesta == null){            
            t13respuesta = "";//Para evitar que marque error al regresar null
        }
        if(typeof t13respuesta !== 'undefined'){ // Soluciona el problema de las preguntas cortas que puede haber un texto mas que las respuestas
            if(typeof t13respuesta !== 'null'){
                //t13respuesta = t13respuesta.replace(" ", "&nbsp;");//Soluciona problema de espacio en blanco
            }else{
                t13respuesta = '&nbsp;';
            }
        }
        t13orden = obj.respuestas[j].t13orden;
        t13fecha_registro = obj.respuestas[j].t13fecha_registro;
        t13fecha_actualiza = obj.respuestas[j].t13fecha_actualiza;
        t13estatus = obj.respuestas[j].t13estatus;
        respuestasArr[j] = t13respuesta;
    }
    seleccionaEstilo();
    for ( j=0; j<numRespuestas; j++) {
        if(c03id_tipo_pregunta != "4" && c03id_tipo_pregunta != "8" && c03id_tipo_pregunta != "18"){//Si es de relacionar combo no toma en cuenta el numero de respuestas. Se considera una sola respuesta.
            crearDivs();
        }
    }
    if(c03id_tipo_pregunta === "4" || c03id_tipo_pregunta === "8"  || c03id_tipo_pregunta === "18"){
        crearDivs();
    }
    redimensionaAlturaDivPrincipal();
    marcaOpcionSeleccionada(t11usuario);
}
function redimensionaAlturaDivPrincipal(){
    var dimensiones = new Object();
    if(c03id_tipo_pregunta === "9" ){
         $('#respuestasRadialGroup').height( $('.dhe-example-section').height() + 80);         
         $('#fondoPregunta').height($('#respuestasRadialGroup').height()+$('.pregunta').height()+ 80 + 20); // 80 es la suma de los margenes + 20 del nuevo margen
    }    
    if(c03id_tipo_pregunta === "1" || c03id_tipo_pregunta === "2" || c03id_tipo_pregunta === "3"){
        $('#respuestasRadialGroup').height( $('form').height() + $('.exam_footer').height());
    }
    if(c03id_tipo_pregunta !== "8" && c03id_tipo_pregunta !== "13"){
        $('#fondoPregunta').height($('form ul').height() + $('.pregunta').height() + $('.exam_footer').height());
        //$('#fondoPregunta').height( $('form').height());
    }        
    if(c03id_tipo_pregunta === "8"){//Si es arrastra cortas
        //dimensiones.height = $('#pilaDeCartas').height() + $('#espaciosDeCartas').height();//dimensiones.height = $('#respuestasRadialGroup').height();                       
//        dimensiones.height = $('#pilaDeCartas').height() + $('#texto').height();//dimensiones.height = $('#respuestasRadialGroup').height();
//        $('#content').height(dimensiones.height + 100);                
    }    
}
function htmlEncode(value){
  //create a in-memory div, set it's inner text(which jQuery automatically encodes)
  //then grab the encoded contents back out.  The div never exists on the page.
  return $('<div/>').text(value).html();
}

function htmlDecode(value){
  return $('<div/>').html(value).text();
}
function trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
    return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
    return stringToTrim.replace(/\s+$/,"");
}
function seleccionaEstilo (){
    contieneRespuestas = true;
    var numRespuestas = typeof obj.respuestas === 'object' ? Object.keys(obj.respuestas).length : obj.respuestas.length;
    if(c03id_tipo_pregunta === "1"){
        numEstilo = "1";//Puede ser 1: radialGroup ó 2: botones personalizados
    }
    if(c03id_tipo_pregunta === "2"){
        numEstilo = "5";//Puede ser 5: checkBox ó 2: botones personalizados
    }
    if(c03id_tipo_pregunta === "11"){//Si es relacionar columnas con cajitas
        numEstilo = "3";
    }
    if(c03id_tipo_pregunta === "6"){//Si es pregunta corta con textarea
        contieneRespuestas = false;
        numEstilo = "4";
    }
    if(c03id_tipo_pregunta === "4"){//Si es Relaciona Arrastrar
        //contieneRespuestas = false;
        numEstilo = "18";
    }
    if(c03id_tipo_pregunta === "7"){//Si es Cortas Horizontal
        contieneRespuestas = true;
        numEstilo = "7";
    }
    if(c03id_tipo_pregunta === "3"){//Si es Verdadero Falso
        numEstilo = "1";
    }
    if(c03id_tipo_pregunta === "10"){//Si es Ordenar con números
        numEstilo = "9";
    }
    if(c03id_tipo_pregunta === "12"){//Si es Relacionar Líneas
        //contieneRespuestas = false;
        numEstilo = "10";
    }
    if(c03id_tipo_pregunta === "9"){//Si es Arrastra Ordenar
        numEstilo = "11";
    }
    if(c03id_tipo_pregunta === "17"){//Si es Corta Numérica
        contieneRespuestas = false;
        numEstilo = "12";
    }
    if(c03id_tipo_pregunta === "8"){//Si es Arrastra Cortas
        contieneRespuestas = false;
        numEstilo = "13";
    }
    if(c03id_tipo_pregunta === "5"){//Si es Arrastra Contenedor
        numEstilo = "14";
    }
    if(c03id_tipo_pregunta === "14"){//Si es Rompecabezas
        contieneRespuestas = false;
        imagen = respuestasArr[0];
        numEstilo = "15";
    }
    if(c03id_tipo_pregunta === "13"){//Si es Matriz        
        numEstilo = "16";
    }
    if(c03id_tipo_pregunta === "16"){//Si es Sopa de Letras
        contieneRespuestas = false;
        numEstilo = "17";
    }  
    if(c03id_tipo_pregunta === "18"){//Si es pregunta Relacionar Combo
        contieneRespuestas = false;
        numEstilo = "6";
    }  
    prepararDivs(numRespuestas);//Función que se le pasa cuantos botones va a crear dinámicamente
   
}
function pintaLetrerosComunes (){
   // $('#instruccionesPregunta').html('<i class="fa fa-lightbulb-o fa-2x"></i>');
    //$('#instruccionesPreguntaTexto').html(t11instruccion);
    $('.instrucciones_pregunta').html(t11instruccion);
    if(c03id_tipo_pregunta === "13"){//Las preguntas de tipo Matriz no llevan el texto de la pregunta.  
        t11pregunta = "";
    }  
//    t11pregunta = t11pregunta.replace("<p>", "");
//    t11pregunta = t11pregunta.replace("</p>", "");
    $('#preguntaTexto').html("<div>"+t11pregunta+"</div>");
    $('#preguntaTexto').addClass('preguntaTit');
    // Elimino todos los estilos de los span que inserta el editor de textos
    $('#preguntaTexto').find('span').each(function(){$(this).removeAttr('style');});
    
    
    $('.imgsAnt').addClass('text-center');
    
    if($('#primera').val()){
        $('.anterior').addClass('desactiva');
    }else{
        $('.anterior').removeClass('desactiva');
    }
    if($('#ultima').val()){
        $('.siguiente').addClass('desactiva');
    }else{
        $('.siguiente').removeClass('desactiva');
    }
}
function checkwhere(e) {//Esta función sirve para detectar la posición del puntero
    var xCoord = 0;
    var yCoord = 0;
    if (document.layers){
        xCoord = e.x;
        yCoord = e.y;
    }else if (document.all){
        xCoord = event.clientX;
        yCoord = event.clientY;
    }else if (document.getElementById){
        xCoord = e.clientX;
        yCoord = e.clientY;
    }
    document.getElementsByTagName("h2")[0].innerHTML = "ENTRA A checkwhere: "+xCoord+"  "+yCoord;
}
function obtenerDimensiones (oElement) {
    var x, y, w, h;
    x = y = w = h = 0;
        
    navegador=get_browser();
    navegador_version=get_browser_version();
    versionInt = parseInt(navegador_version);
    
    if (document.getBoxObjectFor) {//Mozilla        
        var oBox = document.getBoxObjectFor(oElement);
        x = oBox.x-1;
        w = oBox.width;
        y = oBox.y-1;
        h = oBox.height;
    }  else if ( typeof oElement !== 'undefined'){        
        if(oElement.getBoundingClientRect) {//IE
            var oRect = oElement.getBoundingClientRect();
            x = oRect.left-2;
            w = oElement.clientWidth;
            y = oRect.top-2;
            h = oElement.clientHeight;            
            if(navegador === 'Firefox'){             
                if(versionInt <= 32){
                    x = x + 2;
                    y = y + 15;
                }
            }            
        }
    }
    
    return {x: x, y: y, w: w, h: h};
}
function get_browser(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return 'IE '+(tem[1]||'');
        }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return 'Opera '+tem[1];}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return M[0];
}
function get_browser_version(){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];                                                                                                                         
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1]||'');
        }
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return 'Opera '+tem[1];}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return M[1];
}

function accionesMouse(){
    document.onmousemove = checkwhere;
    if(document.captureEvents) {
        document.captureEvents(Event.MOUSEMOVE);
    }
}
function deshabilitaSeleccionDeTexto (){
 //   document.onselectstart = function() {return false;};//Deshabilitar la selección de texto
 //   document.onmousedown = function() {return false;};//Deshabilitar la selección de texto
}
function deshabilitarBoton(idRecibido){
    var boton;
    boton = document.getElementById(idRecibido);
    boton.onmouseup = detenerEventos;
    boton.onmousedown = detenerEventos;
    boton.setAttributeNS(null,"cursor","auto");
}
function detenerEventos(e) {
    if (!e) e = window.event;
    if (e.stopPropagation) {
        e.stopPropagation();
    } else {
        e.cancelBubble = true;
    }
}
function cambiaColorSVG(idRecibido){
    var objetoSVG = idRecibido;    
    objetoSVG.setAttributeNS(null,"stroke",colores_arr[posColores]);
    objetoSVG.setAttributeNS(null,"fill",colores_arr[posColores]);
}
function cambiaColorObjeto(idRecibido){
    var objetoSVG;
    objetoSVG = document.getElementById(idRecibido);
    objetoSVG.setAttributeNS(null,"stroke",colores_arr[posColores]);
    objetoSVG.setAttributeNS(null,"fill",colores_arr[posColores]);
}
function crearCajaTextoEntrada(idInput,tamanoInput){
    var caja;
    caja = document.createElement("input");
    caja.setAttribute("type", "text");
    caja.setAttribute("id", idInput);
    caja.setAttribute("size", tamanoInput);
    document.getElementById("contenedor").appendChild(caja);
}
function cargarImagenEnCanvas(){
    var canvas = document.getElementById('miCanvas');
    var contexto = canvas.getContext('2d');
    var imagen = new Image();
    imagen.onload = function(){
	contexto.drawImage(imagen,0,0);  // imagen completa en la posición (0,0)
    };
    imagen.src = 'http://www.aulaclic.es/articulos/graficos/cabeza_pato.png';
}
$(document).ready(function(){
   //funcionesSopa();  
   obtenerValoresJSON();
    $('#txtCrecer').keypress(function(){
    $('#referenciaCrecer').html($(this).val().replace(/\n/g, "<br />"));
        $(this).height($('#referenciaCrecer').height());
    });                
    //preparaSorteable();  
    if( document.createElement('svg').getAttributeNS ) {

    	var checkbxsCross = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-cross input[type="checkbox"]' ) ),
    		radiobxsFill = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-fill input[type="radio"]' ) ),
    		checkbxsCheckmark = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-checkmark input[type="checkbox"]' ) ),
    		radiobxsCircle = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-circle input[type="radio"]' ) ),
    		checkbxsBoxfill = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-boxfill input[type="checkbox"]' ) ),
    		radiobxsSwirl = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-swirl input[type="radio"]' ) ),
    		checkbxsDiagonal = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-diagonal input[type="checkbox"]' ) ),
    		checkbxsList = Array.prototype.slice.call( document.querySelectorAll( 'form.ac-list input[type="checkbox"]' ) ),
    		pathDefs = {
    			cross : ['M 10 10 L 90 90','M 90 10 L 10 90'],
    			fill : ['M15.833,24.334c2.179-0.443,4.766-3.995,6.545-5.359 c1.76-1.35,4.144-3.732,6.256-4.339c-3.983,3.844-6.504,9.556-10.047,13.827c-2.325,2.802-5.387,6.153-6.068,9.866 c2.081-0.474,4.484-2.502,6.425-3.488c5.708-2.897,11.316-6.804,16.608-10.418c4.812-3.287,11.13-7.53,13.935-12.905 c-0.759,3.059-3.364,6.421-4.943,9.203c-2.728,4.806-6.064,8.417-9.781,12.446c-6.895,7.477-15.107,14.109-20.779,22.608 c3.515-0.784,7.103-2.996,10.263-4.628c6.455-3.335,12.235-8.381,17.684-13.15c5.495-4.81,10.848-9.68,15.866-14.988 c1.905-2.016,4.178-4.42,5.556-6.838c0.051,1.256-0.604,2.542-1.03,3.672c-1.424,3.767-3.011,7.432-4.723,11.076 c-2.772,5.904-6.312,11.342-9.921,16.763c-3.167,4.757-7.082,8.94-10.854,13.205c-2.456,2.777-4.876,5.977-7.627,8.448 c9.341-7.52,18.965-14.629,27.924-22.656c4.995-4.474,9.557-9.075,13.586-14.446c1.443-1.924,2.427-4.939,3.74-6.56 c-0.446,3.322-2.183,6.878-3.312,10.032c-2.261,6.309-5.352,12.53-8.418,18.482c-3.46,6.719-8.134,12.698-11.954,19.203 c-0.725,1.234-1.833,2.451-2.265,3.77c2.347-0.48,4.812-3.199,7.028-4.286c4.144-2.033,7.787-4.938,11.184-8.072 c3.142-2.9,5.344-6.758,7.925-10.141c1.483-1.944,3.306-4.056,4.341-6.283c0.041,1.102-0.507,2.345-0.876,3.388 c-1.456,4.114-3.369,8.184-5.059,12.212c-1.503,3.583-3.421,7.001-5.277,10.411c-0.967,1.775-2.471,3.528-3.287,5.298 c2.49-1.163,5.229-3.906,7.212-5.828c2.094-2.028,5.027-4.716,6.33-7.335c-0.256,1.47-2.07,3.577-3.02,4.809'],
    			checkmark : ['M16.667,62.167c3.109,5.55,7.217,10.591,10.926,15.75 c2.614,3.636,5.149,7.519,8.161,10.853c-0.046-0.051,1.959,2.414,2.692,2.343c0.895-0.088,6.958-8.511,6.014-7.3 c5.997-7.695,11.68-15.463,16.931-23.696c6.393-10.025,12.235-20.373,18.104-30.707C82.004,24.988,84.802,20.601,87,16'],
    			circle : ['M34.745,7.183C25.078,12.703,13.516,26.359,8.797,37.13 c-13.652,31.134,9.219,54.785,34.77,55.99c15.826,0.742,31.804-2.607,42.207-17.52c6.641-9.52,12.918-27.789,7.396-39.713 C85.873,20.155,69.828-5.347,41.802,13.379'],
    			boxfill : ['M6.987,4.774c15.308,2.213,30.731,1.398,46.101,1.398 c9.74,0,19.484,0.084,29.225,0.001c2.152-0.018,4.358-0.626,6.229,1.201c-5.443,1.284-10.857,2.58-16.398,2.524 c-9.586-0.096-18.983,2.331-28.597,2.326c-7.43-0.003-14.988-0.423-22.364,1.041c-4.099,0.811-7.216,3.958-10.759,6.81 c8.981-0.104,17.952,1.972,26.97,1.94c8.365-0.029,16.557-1.168,24.872-1.847c2.436-0.2,24.209-4.854,24.632,2.223 c-14.265,5.396-29.483,0.959-43.871,0.525c-12.163-0.368-24.866,2.739-36.677,6.863c14.93,4.236,30.265,2.061,45.365,2.425 c7.82,0.187,15.486,1.928,23.337,1.903c2.602-0.008,6.644-0.984,9,0.468c-2.584,1.794-8.164,0.984-10.809,1.165 c-13.329,0.899-26.632,2.315-39.939,3.953c-6.761,0.834-13.413,0.95-20.204,0.938c-1.429-0.001-2.938-0.155-4.142,0.436 c5.065,4.68,15.128,2.853,20.742,2.904c11.342,0.104,22.689-0.081,34.035-0.081c9.067,0,20.104-2.412,29.014,0.643 c-4.061,4.239-12.383,3.389-17.056,4.292c-11.054,2.132-21.575,5.041-32.725,5.289c-5.591,0.124-11.278,1.001-16.824,2.088 c-4.515,0.885-9.461,0.823-13.881,2.301c2.302,3.186,7.315,2.59,10.13,2.694c15.753,0.588,31.413-0.231,47.097-2.172 c7.904-0.979,15.06,1.748,22.549,4.877c-12.278,4.992-25.996,4.737-38.58,5.989c-8.467,0.839-16.773,1.041-25.267,0.984 c-4.727-0.031-10.214-0.851-14.782,1.551c12.157,4.923,26.295,2.283,38.739,2.182c7.176-0.06,14.323,1.151,21.326,3.07 c-2.391,2.98-7.512,3.388-10.368,4.143c-8.208,2.165-16.487,3.686-24.71,5.709c-6.854,1.685-13.604,3.616-20.507,4.714 c-1.707,0.273-3.337,0.483-4.923,1.366c2.023,0.749,3.73,0.558,5.95,0.597c9.749,0.165,19.555,0.31,29.304-0.027 c15.334-0.528,30.422-4.721,45.782-4.653'],
    			swirl : ['M49.346,46.341c-3.79-2.005,3.698-10.294,7.984-8.89 c8.713,2.852,4.352,20.922-4.901,20.269c-4.684-0.33-12.616-7.405-14.38-11.818c-2.375-5.938,7.208-11.688,11.624-13.837 c9.078-4.42,18.403-3.503,22.784,6.651c4.049,9.378,6.206,28.09-1.462,36.276c-7.091,7.567-24.673,2.277-32.357-1.079 c-11.474-5.01-24.54-19.124-21.738-32.758c3.958-19.263,28.856-28.248,46.044-23.244c20.693,6.025,22.012,36.268,16.246,52.826 c-5.267,15.118-17.03,26.26-33.603,21.938c-11.054-2.883-20.984-10.949-28.809-18.908C9.236,66.096,2.704,57.597,6.01,46.371 c3.059-10.385,12.719-20.155,20.892-26.604C40.809,8.788,58.615,1.851,75.058,12.031c9.289,5.749,16.787,16.361,18.284,27.262 c0.643,4.698,0.646,10.775-3.811,13.746'],
    			diagonal : ['M16.053,91.059c0.435,0,0.739-0.256,0.914-0.768 c3.101-2.85,5.914-6.734,8.655-9.865C41.371,62.438,56.817,44.11,70.826,24.721c3.729-5.16,6.914-10.603,10.475-15.835 c0.389-0.572,0.785-1.131,1.377-1.521'],
    			list : ['M1.986,8.91c41.704,4.081,83.952,5.822,125.737,2.867 c17.086-1.208,34.157-0.601,51.257-0.778c21.354-0.223,42.706-1.024,64.056-1.33c18.188-0.261,36.436,0.571,54.609,0.571','M3.954,25.923c9.888,0.045,19.725-0.905,29.602-1.432 c16.87-0.897,33.825-0.171,50.658-2.273c14.924-1.866,29.906-1.407,44.874-1.936c19.9-0.705,39.692-0.887,59.586,0.45 c35.896,2.407,71.665-1.062,107.539-1.188']
    		},
    		animDefs = {
    			cross : { speed : .2, easing : 'ease-in-out' },
    			fill : { speed : .8, easing : 'ease-in-out' },
    			checkmark : { speed : .2, easing : 'ease-in-out' },
    			circle : { speed : .2, easing : 'ease-in-out' },
    			boxfill : { speed : .8, easing : 'ease-in' },
    			swirl : { speed : .8, easing : 'ease-in' },
    			diagonal : { speed : .2, easing : 'ease-in-out' },
    			list : { speed : .3, easing : 'ease-in-out' }
    		};

    	function createSVGEl( def ) {
    		var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    		if( def ) {
    			svg.setAttributeNS( null, 'viewBox', def.viewBox );
    			svg.setAttributeNS( null, 'preserveAspectRatio', def.preserveAspectRatio );
    		}
    		else {
    			svg.setAttributeNS( null, 'viewBox', '0 0 100 100' );
    		}
    		svg.setAttribute( 'class', 'elemSVG' );
    		svg.setAttribute( 'xmlns', 'http://www.w3.org/2000/svg' );
    		return svg;
    	}

    	function controlCheckbox( el, type, svgDef ) {
    		var svg = createSVGEl( svgDef );
    		el.parentNode.appendChild( svg );
    		
    		el.addEventListener( 'change', function() {
    			if( el.checked ) {
    				draw( el, type );
    			}
    			else {
    				reset( el );
    			}
    		} );
    	}

    	function controlRadiobox( el, type ) {
    		var svg = createSVGEl();
    		el.parentNode.appendChild( svg );
    		el.addEventListener( 'change', function() {
    			resetRadio( el );
    			draw( el, type );
    		} );
    	}

    	checkbxsCross.forEach( function( el, i ) { controlCheckbox( el, 'cross' ); } );
    	radiobxsFill.forEach( function( el, i ) { controlRadiobox( el, 'fill' ); } );
    	checkbxsCheckmark.forEach( function( el, i ) { controlCheckbox( el, 'checkmark' ); } );
    	radiobxsCircle.forEach( function( el, i ) { controlRadiobox( el, 'circle' ); } );
    	checkbxsBoxfill.forEach( function( el, i ) { controlCheckbox( el, 'boxfill' ); } );
    	radiobxsSwirl.forEach( function( el, i ) { controlRadiobox( el, 'swirl' ); } );
    	checkbxsDiagonal.forEach( function( el, i ) { controlCheckbox( el, 'diagonal' ); } );
    	checkbxsList.forEach( function( el ) { controlCheckbox( el, 'list', { viewBox : '0 0 300 100', preserveAspectRatio : 'none' } ); } );

    	function draw( el, type ) {
    		var paths = [], pathDef, 
    			animDef,
    			svg = el.parentNode.querySelector( 'svg' );

    		switch( type ) {
    			case 'cross': pathDef = pathDefs.cross; animDef = animDefs.cross; break;
    			case 'fill': pathDef = pathDefs.fill; animDef = animDefs.fill; break;
    			case 'checkmark': pathDef = pathDefs.checkmark; animDef = animDefs.checkmark; break;
    			case 'circle': pathDef = pathDefs.circle; animDef = animDefs.circle; break;
    			case 'boxfill': pathDef = pathDefs.boxfill; animDef = animDefs.boxfill; break;
    			case 'swirl': pathDef = pathDefs.swirl; animDef = animDefs.swirl; break;
    			case 'diagonal': pathDef = pathDefs.diagonal; animDef = animDefs.diagonal; break;
    			case 'list': pathDef = pathDefs.list; animDef = animDefs.list; break;
    		};
    		
    		paths.push( document.createElementNS('http://www.w3.org/2000/svg', 'path' ) );

    		if( type === 'cross' || type === 'list' ) {
    			paths.push( document.createElementNS('http://www.w3.org/2000/svg', 'path' ) );
    		}
    		
    		for( var i = 0, len = paths.length; i < len; ++i ) {
    			var path = paths[i];
    			svg.appendChild( path );

    			path.setAttributeNS( null, 'd', pathDef[i] );

    			var length = path.getTotalLength();
    			// Clear any previous transition
    			//path.style.transition = path.style.WebkitTransition = path.style.MozTransition = 'none';
    			// Set up the starting positions
    			path.style.strokeDasharray = length + ' ' + length;
    			if( i === 0 ) {
    				path.style.strokeDashoffset = Math.floor( length ) - 1;
    			}
    			else path.style.strokeDashoffset = length;
    			// Trigger a layout so styles are calculated & the browser
    			// picks up the starting position before animating
    			path.getBoundingClientRect();
    			// Define our transition
    			path.style.transition = path.style.WebkitTransition = path.style.MozTransition  = 'stroke-dashoffset ' + animDef.speed + 's ' + animDef.easing + ' ' + i * animDef.speed + 's';
    			// Go!
    			path.style.strokeDashoffset = '0';
    		}
    	}

    	function reset( el ) {
    		Array.prototype.slice.call( el.parentNode.querySelectorAll( 'svg > path' ) ).forEach( function( el ) { el.parentNode.removeChild( el ); } );
    	}

    	function resetRadio( el ) {
    		Array.prototype.slice.call( document.querySelectorAll( 'input[type="radio"][name="' + el.getAttribute( 'name' ) + '"]' ) ).forEach( function( el ) { 
    			var path = el.parentNode.querySelector( 'svg > path' );
    			if( path ) {
    				path.parentNode.removeChild( path );
    			}
    		} );
    	}

    }
});
function esMobile() {
    var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
    return mobile;
}
function preparaSorteable (){
    $('#example-1-1 .sortable-list').sortable();
}
    
/*  OK if(c03id_tipo_pregunta === "1"){    preguntaMultiple(); }//Si es de tipo de Pregunta Opción Múltiple
    OK if(c03id_tipo_pregunta === "2"){    preguntaRespuestaMultiple(); }//Si es de tipo de Pregunta Respuesta Multiple
    OK if(c03id_tipo_pregunta === "3"){    preguntaVerdaderoFalso(); }//Si es de tipo de Pregunta Verdadero Falso
    OK if(c03id_tipo_pregunta === "4"){    preguntaRelacionaCombo(); }//Si es de tipo de Pregunta Relaciona Combo
    OK if(c03id_tipo_pregunta === "5"){    preguntaArrastraContenedor(); }//Si es de tipo de Pregunta Arrastra Contenedor
    OK if(c03id_tipo_pregunta === "6"){    preguntaAbierta(); }//Si es de tipo de Pregunta Abierta
    OK if(c03id_tipo_pregunta === "7"){    preguntaCorta(); }//Si es de tipo de Pregunta Corta Horizontal
    OK if(c03id_tipo_pregunta === "8"){    preguntaArrastraCorta(); }//Si es de tipo de Pregunta Arrastra Corta
    OK if(c03id_tipo_pregunta === "9"){    preguntaArrastraOrdenar(); }//Si es de tipo de Pregunta Arrastra Ordenar
    OK if(c03id_tipo_pregunta === "10"){   preguntaNumeroOrdenar(); }//Si es de tipo de Pregunta Número Ordenar
    OK if(c03id_tipo_pregunta === "11"){   preguntaNumeroRelacionar(); }//Si es de tipo de Pregunta Relacionar con Números ó Letras
    OK if(c03id_tipo_pregunta === "12"){   preguntaRelacionarLinea(); }//Si es de tipo de Pregunta Relacionar Lineas
       if(c03id_tipo_pregunta === "13"){   preguntaMatriz(); }//Si es de tipo de Pregunta Matriz
       if(c03id_tipo_pregunta === "14"){   preguntaRompecabezas(); }//Si es de tipo de Pregunta Rompecabezas
       if(c03id_tipo_pregunta === "15"){   preguntaCrucigrama(); }//Si es de tipo de Pregunta Crucigrama
       if(c03id_tipo_pregunta === "16"){   preguntaSopaDeLetras(); }//Si es de tipo de Pregunta Sopa de Letras
    OK if(c03id_tipo_pregunta === "17"){   preguntaCortaNumerica(); }//Si es de tipo de Pregunta Corta Numérica */
