json = [
{  
  "respuestas":[  
  {  
    "t13respuesta":"<p>1</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>6</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>2</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>3</p>",
    "t17correcta":"1"
  },
  {  
    "t13respuesta":"<p>5</p>",
    "t17correcta":"0"
  }
  ],
  "pregunta":{  
    "c03id_tipo_pregunta":"1",
    "t11pregunta":"Da click en el ícono e identifica en el texto ¿En qué inciso puedes encontrar los factores abióticos del ecosistema?",
    "box": "texto-bio.png",
    "box_type": "doc"
  }
},
{  
  "respuestas":[  
  {  
    "t13respuesta":"<p>Selva húmeda</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>Selva baja</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>Bosque</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>Pastizal-matorral</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>Ninguno de los cuatro</p>",
    "t17correcta":"1"
  }
  ],
  "pregunta":{  
    "c03id_tipo_pregunta":"1",
    "t11pregunta":"Da click en el ícono y responde. ¿De qué ecosistema realizaron la ficha técnica?",
    "box": "texto-bio.png",
    "box_type": "doc"
  }
},
{  
  "respuestas":[  
  {  
    "t13respuesta":"<p>Ciclo del Carbono</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>Ciclo del Nitrógeno</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>Ciclo del agua</p>",
    "t17correcta":"1"
  }
  ],
  "pregunta":{  
    "c03id_tipo_pregunta":"1",
    "t11pregunta":"Da click en el ícono y responde. En el enunciado: “La precipitación total anual de 700-1200 mm.” ¿A qué ciclo biogeoquímico se refiere?",
    "box": "texto-bio.png",
    "box_type": "doc"
  }
},
{  
  "respuestas":[  
  {  
    "t13respuesta":"<p>El calentamiento global</p>",
    "t17correcta":"0"
  },
  {  
    "t13respuesta":"<p>Modificación del entorno como la deforestación</p>",
    "t17correcta":"1"
  },
  {  
    "t13respuesta":"<p>Agroquímicos, desechos sólidos, residuos domésticos</p>",
    "t17correcta":"1"
  }
  ],
  "pregunta":{  
    "c03id_tipo_pregunta":"2",
    "t11pregunta":"Da click en el ícono y responde. Son factores que influyen en la degradación del hábitat estudiado (Selecciona dos respuestas).",
    "box": "texto-bio.png",
    "box_type": "doc"
  }
},
{
  "respuestas": [
  {
    "t13respuesta": "Falso",
    "t17correcta": "0"
  },
  {
    "t13respuesta": "Verdadero",
    "t17correcta": "1"
  }
  ],
  "preguntas": [
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "La caza no controlada de pato rabudo provocaría la sobrepoblación de Ninfas.",
    "correcta": "0"
  },
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "Una de las principales causas de la desecación del lago de Almoloya de Juárez son las altas temperaturas en la zona.",
    "correcta": "0"
  },
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "Las actividades agrícolas e industriales entre otras, son las principales causantes de la contaminación del río.",
    "correcta": "1"
  },
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "La actividad económica de la zona no es un factor que provoque la contaminación de la ciénega.",
    "correcta": "0"
  }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "<p>Da click en el ícono y responde. De los siguientes enunciados señala si son verdadero o falso según corresponda.<\/p>",
    "descripcion": "",
    "variante": "editable",
    "anchoColumnaPreguntas": 70,
    "evaluable": true,
    "box": "texto-bio.png",
    "box_type": "doc"
  }
},
{
  "respuestas":[
  {
    "t13respuesta":"\u003Cp\u003EConstrucción de represas a lo largo del río para evitar el exceso de agua en el caudal.\u003C\/p\u003E",
    "t17correcta":"1"
  },
  {
    "t13respuesta":"\u003Cp\u003EEvitar los residuos sólidos contaminantes en el lago de Almoloya.\u003C\/p\u003E",
    "t17correcta":"0"
  },
  {
    "t13respuesta":"\u003Cp\u003EControlar la reproducción de los factores bióticos.\u003C\/p\u003E",
    "t17correcta":"1"
  },
  {
    "t13respuesta":"\u003Cp\u003ELas industrias cercanas tienen que instalar plantas tratadoras de agua.\u003C\/p\u003E",
    "t17correcta":"0"
  },
  {
    "t13respuesta":"\u003Cp\u003ERegular la explotación del agua para riego en los sembradíos.\u003C\/p\u003E",
    "t17correcta":"0"
  },
  {
    "t13respuesta":"\u003Cp\u003EColocar pastillas de Hipoclorito de Sodio (Cloro) en el Lago para desinfectar.\u003C\/p\u003E",
    "t17correcta":"1"
  }
  ],
  "pregunta":{
    "c03id_tipo_pregunta":"5",
    "t11pregunta":"Lee la propuesta de la CONABIO para la conservación de la ciénega de municipio de Lerma y selecciona las posibles acciones que se podrían llevar a cabo.<br><b>Conservación:</b> La principal preocupación es la desecación de las lagunas de Almoloya del Río. En época de secas el río Lerma sólo lleva aguas residuales, lo cual indica la necesidad de tratamiento intensivo de sus aguas.",
    "tipo":"vertical"
  },
  "contenedores":[ 
    "Posibles Acciones"
  ]
},
{
  "respuestas": [
  {
    "t13respuesta": "que es apta para encontrar larvas y gusanos bajo el fango.",
    "t17correcta": "1"
  },
  {
    "t13respuesta": "es una característica adaptativa para camuflarse entre los juncos.",
    "t17correcta": "2"
  },
  {
    "t13respuesta": "para evitar ser atrapado por algún depredador.",
    "t17correcta": "3"
  },
  {
    "t13respuesta": "asegura la reproducción.",
    "t17correcta": "4"
  }
  ],
  "preguntas": [
  {
    "t11pregunta": "Patas largas y pico largo en forma de aguja..."
  },
  {
    "t11pregunta": "Tiene un plumaje de color marrón, negro y blanco. En el dorso presentan vetas longitudinales claras..."
  },
  {
    "t11pregunta": "Su morfología le ayuda a emprender el vuelo rápidamente ..."
  },
  {
    "t11pregunta": "Migración en invierno..."
  }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "12",
    "t11pregunta": "Da click en el ícono y relaciona la columna como corresponda.",
    "box": "agachadizo.jpg",
    "box_type": "img"
  }
},
{
  "respuestas": [
  {
    "t13respuesta": "<p>Ninfa<\/p>",
    "t17correcta": "0"
  },
  {
    "t13respuesta": "<p>Protozoarios<\/p>",
    "t17correcta": "0"
  },
  {
    "t13respuesta": "<p>Ajolote<\/p>",
    "t17correcta": "1"
  },
  {
    "t13respuesta": "<p>Charal<\/p>",
    "t17correcta": "1"
  },
  {
    "t13respuesta": "<p>Pato Rabudo<\/p>",
    "t17correcta": "1"
  },
  {
    "t13respuesta": "<p>Pupo del Valle<\/p>",
    "t17correcta": "1"
  }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "5",
    "t11pregunta": "De la biodiversidad de la ciénega. Arrastra a la tabla los seres vivos que son autótrofos y los que son heterótrofos, según corresponda.",
    "tipo": "vertical"
  },
  "contenedores":[ 
    "Autótrofos",
    "Heterótrofos"
  ]
},
{
  "respuestas": [
  {
    "t13respuesta": "Falso",
    "t17correcta": "0"
  },
  {
    "t13respuesta": "Verdadero",
    "t17correcta": "1"
  }
  ],
  "preguntas": [
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "La medicina actual es fruto de la investigación de la medicina ancestral y la herbolaria.",
    "correcta": "1"
  },
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "En la antigüedad para poder curar enfermedades se utilizaban seres vivos y plantas.",
    "correcta": "1"
  },
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "Las plantas y hongos son los elementos que utilizan para hacer todas las medicinas en la actualidad.",
    "correcta": "0"
  },
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "La herbolaria mexicana es una de las más reconocidas a nivel mundial.",
    "correcta": "1"
  },
  {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "Al usar tantas medicinas procesadas, la herbolaria en la actualidad ya no cumple con sus funciones.",
    "correcta": "0"
  }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
    "descripcion": "",
    "variante": "editable",
    "anchoColumnaPreguntas": 70,
    "evaluable": true
  }
},
{
  "respuestas": [
  {
    "t13respuesta": "<p>branquial<\/p>",
    "t17correcta": "9"
  },
  {
    "t13respuesta": "<p>sexual<\/p>",
    "t17correcta": "4"
  },
  {
    "t13respuesta": "<p>respiración<\/p>",
    "t17correcta": "1"
  },
  {
    "t13respuesta": "<p>anaerobio<\/p>",
    "t17correcta": "7"
  },
  {
    "t13respuesta": "<p>heterosexual<\/p>",
    "t17correcta": "10"
  },
  {
    "t13respuesta": "<p>gases<\/p>",
    "t17correcta": "2"
  }, 
  {
    "t13respuesta": "<p>energía<\/p>",
    "t17correcta": "3"
  },
  {
    "t13respuesta": "<p>asexual<\/p>",
    "t17correcta": "5"
  },
  {
    "t13respuesta": "<p>aerobio<\/p>",
    "t17correcta": "6"
  },
  {
    "t13respuesta": "<p>pulmonar<\/p>",
    "t17correcta": "8"
  },
  ],
  "preguntas": [
  {
    "t11pregunta": "<p>La <\/p>"
  },
  {
    "t11pregunta": "<p>&nbsp;&nbsp;también forma parte de las características del metabolismo.<br> Hay dos procesos importantes dentro de la respiración: el intercambio de <\/p>"
  },
  {
    "t11pregunta": "<p>&nbsp;&nbsp;entre ambiente y seres vivos y la respiración celular ya que se obtiene <\/p>"
  },
  {
    "t11pregunta": "<p>&nbsp;&nbsp;en el interior de las células.<br>Los seres vivos se pueden reproducir de dos formas: <\/p>"
  },
  {
    "t11pregunta": "<p>&nbsp;&nbsp;o <\/p>"
  },
  {
    "t11pregunta": "<p><br>La respiración en los seres vivos puede ser de tipo: <\/p>"
  },
  {
    "t11pregunta": "<p>&nbsp;&nbsp;o <\/p>"
  }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "8",
    "t11pregunta": "Completar los enunciados.",
    "respuestasLargas": false,
    "pintaUltimaCaja": true,
    "contieneDistractores":true
  }
}
]

correctas = ["00010", "00001", "001", "011", "10100110", "0-0_0-0_0-0", "1-1,2-2,3-3,4-4", "0-0_0-0_1-1_1-1_1-1_1-1", "0101100110", "1-1_2-2_3-3_4-4_5-5_6-6_7-7"];