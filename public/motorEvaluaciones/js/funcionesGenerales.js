var letrasArr = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
letra = letrasArr.split("");
var preguntaActual=0; 
var json;
var resultados;
var strAmbiente;
var libro;
var evaluacion;
var rutaLibro;
var idActividad;
var mostrarBarra=false;
var lastResort;
var primerVuelta = true;
var direccionCambio="-100";//define la direccion que tomara al cambiar de pregunta en la actividad
var finalizaActividad = true;//define si es evaluable el tipo de actividad "graficas"
//variables de evaluacion
var aciertos = 0;
var totalPreguntas=0;
var intentosRestantes=0;
var esUltima = false;
var avancePregunta;
var evaluable = true;
var bookColor, bookColorAlpha;//colores del libro
var incisos=false; //variable arrastra corta incisos
var conRestantes;//variable de agrupa contenedor    
var strEvaluacion;
var objCadenas=[];//almacena todas las cadenas generadas a lo largo de la evaluacion
var maxWidth;
$(window).bind("load", function() {//se inicia la actividad al terminas de cargar los recursos
    if(lastResort!==undefined){
        parent.window.preguntaActual = window.preguntaActual;
        iniciarActividad();
        evaluacion &&  strAmbiente === "PROD" ? restaurarActividad():"";
    }
    if(primerVuelta){
        if(evaluacion){
            $(".respuestaDrag").hide();
            $(".respuestaDrag").fadeIn(500);
        }
        primerVuelta=false;
    }
    evaluacion || !finalizaActividad ? cargarObjCadenas() : "";
    libro === "PROF" ? cargarRecursosProf()  : "";
    if(strAmbiente === "PROD" && !evaluacion){
        //se obtiene bookColor
        bookColor = parent.window.book_color;
        $("body").append("<style id='bColor'>.bookColor{background-color: "+bookColor+" !important;}</style>");
        bookColorAlpha = hexToRgb(bookColor.replace("#",""));
        bookColorAlpha = "rgba("+bookColorAlpha+", 0.35)";
        $(".bookColor:not(.off)").css("background-color", bookColor);
    }
 });
$(document).ready(function(){
    init();//cambio de eventos mouse/touch
    var rutaProyecto = window.location.pathname;
    strAmbiente = rutaProyecto.search("LI-TODO")!==-1 ? "DEV" : "PROD";
    evaluacion = rutaProyecto.search("motorEvaluaciones") !== -1;
    strEvaluacion = evaluacion && strAmbiente === "PROD" ?  "/motorEvaluaciones/":"";
    if(strAmbiente === "DEV"){
        libro = rutaProyecto.search("index_Prof")!==-1 ? "PROF" : "ALM";
    }else if(!evaluacion){
        libro = parent.window.book_ID.charAt(3) === "P" ? "PROF" : "ALM";
    }
    loadWindow();
});
function loadWindow(){
    var rutaIframe = window.location.href;
    if(rutaIframe.indexOf("?id=")!==-1){
        $("#barraNav").hide();
        cargarjsonActividad(rutaIframe);
    }else{
        json === undefined ? window.json = parent.window.json : "";
        window.objCadenas = parent.window.objCadenas;
        loadJson();
    }   
    $("#reload").on("click touchend", function(){
        $(this).css({"transform":"scale(1.25, 1.25)", transition:"transform 0.15s"}).fadeOut(150);
        setTimeout(function(){
            setTimeout(function(){
                location.reload();
            }, 250);
        }, 300);
    });
}
var navTimeout;
function showNavBar(){
    clearTimeout(navTimeout);
    $("#barraNav").removeClass("barraNavHidden");
    $("#barraNav").css("border-color", "#E3E3E3");
    $("#progressBar").fadeOut(350);
    setTimeout(function(){
        $("#buttonsNav").fadeIn(350);
    }, 200);
    navTimeout = setTimeout(hideNavBar, 9000);
}
function hideNavBar(){
    $("#buttonsNav").fadeOut(350);
    setTimeout(function(){
        $("#progressBar").fadeIn(350);
        $("#barraNav").addClass("barraNavHidden");
        if(strAmbiente==="PROD"){//aplica book color a la barra de avance
            $("#barraNav").css("border-color", bookColorAlpha);
        }else{
            $("#barraNav").css("border-color", "rgba(239, 125, 0, 0.35)");
        }
    }, 200);    
    clearTimeout(navTimeout);
}
function cargarjsonActividad(rutaIframe){
    //obtener ruta del objeto json
    idActividad = rutaIframe.split("?id=")[1];
    evaluacion = idActividad.charAt(5) === "B" && idActividad.charAt(9) === "N";
    evaluacion ? $("#contenidoActividad").addClass("actividadEvaluacion") : "";
    var materia = idActividad.charAt(0);
    var grado = idActividad.charAt(2)*1;
    var materias2={"C":"FCyE", "F":"PEF", "G":"GEO", "H":"HIST", "I":"PITG", "M":"MAT", "N":"CN", "R":"RTP", "S":"ESP", T:"TEC"};
    var grados={"4":"Delta", "5":"Epsilon", "6":"Zeta", "7":"Kappa", "8":"Lambda", "9":"Pi"};
    if(strAmbiente === "DEV"){
        var location = window.location.pathname.split("/LI-TODO")[0];
        var RutaEval =  evaluacion ? "Libro/assets/resources/help/evaluaciones/" : "";
        if(location !== ""){
            rutaLibro = location+"/LI-TODO/paquetes-json/"+RutaEval+materias2[materia]+"/"+grados[grado];
        }else{
            rutaLibro = "../../"+RutaEval+materias2[materia]+"/"+grados[grado];
        }
    }else{
        rutaLibro = "../assets/resources/actividades";
    }
    var loadScript = document.createElement("script");
    loadScript.id="jsonActividad";
    loadScript.type="text/javascript";
    loadScript.onload = function(){
        loadJson();
    };
    loadScript.onerror = function(){
        if(idActividad !== idActividad.toUpperCase()){
            idActividad = idActividad.toUpperCase();
            window.location = "../resources/index_Rev.html?id="+idActividad;
        }else{
            $("body").html("<center><p style='color: orangered; font-size: 22px; font-weight: bold;'>No se encontró la actividad</p></center>");
        }
    };
    loadScript.src = rutaLibro+"/"+idActividad+".js";
    document.getElementsByTagName("head")[0].appendChild(loadScript);//se carga el script
}
function loadJson(){
    if (json !== undefined && json !== null) {//se calcula el tipo de actividad
        iniciar();//carga todos los recursos e inica la actividad
        if (evaluacion && libro !== "PROF") {
            //se cargan los recursos adicionales para el motor de evaluaciones
            var scriptEval = document.createElement("script");
            scriptEval.id = "funcionesEval";
            scriptEval.type = "text/javascript";
            scriptEval.onload = function () {
                initEval();
                
            };
            scriptEval.src = strEvaluacion+"js/funcionesEval.js";
            document.getElementsByTagName("head")[0].appendChild(scriptEval);
            $("#estilosEval").attr("href", strEvaluacion+"css/estilosEval.css");
        }
    } else {
        console.log('No se encontró la actividad');
    }
}
var recursoActividad;
function iniciar(){
    //se detiene la actividad hasta que el alumno decida iniciarla
    var numeroActividad = obtenerTipoActividad(preguntaActual);
    //2.1 = respuestaMultiple (se usaran los recursos de opcion multiple)
    recursoActividad = {
        1.1: "OpcionMultiple", 
        1.2: "Test",
        2.1: "OpcionMultiple", 
        2.2: "RespuestaMultipleFondo",
        3:   "FalsoVerdadero",
        4:   "ListasDesplegables",
        5.1: "ArrastraVertical", 
        5.2: "ArrastraHorizontal",
        5.3: "ArrastraMatriz", 
        5.4: "ArrastraMatrizHorizontal",
        5.5: "ArrastraOrdenar", 
        5.6: "ArrastraImagen",
        5.7: "ArrastraImagenes", 
        5.8: "ArrastraContenedorUnico",
        5.9: "ArrastraRespuestaMultiple",
        6:   "PreguntaAbierta",
        7:   "ArrastraEtiquetas",
        8:   "ArrastraCorta",
        9:   "OrdenaElementos",
        10:  "Graficas",
        11:  "ConectaPiezas",
        12:  "RelacionaLineas",
        13:  "Matriz", 
        14:  "MapasDeBits",
        15:  "Crucigrama",
        16:  "SopaLetrasV2",
        18:  "RelacionaLineas"
    };
    var recurso = recursoActividad[numeroActividad];
    //quita las varientes de actividades no son validas para Evaluacion
    var actividadCortaIncisos = recurso === "ArrastraCorta" && (json[preguntaActual].pregunta.textosLargos === true || json[preguntaActual].pregunta.textosLargos === "si");
    var actividadTest = recurso === "Test";
    var actividadVF = recurso === "FalsoVerdadero";
    if(evaluacion && (actividadCortaIncisos || actividadTest || actividadVF)){
        recurso="";
    }
    if(recurso !== lastResort){
        lastResort = recurso;
        //se carga la actividad
        cargaRecursosActividad(recurso);//se carga el script y los estilos del tipo de actividad
        if((!evaluacion || libro==="PROF") && preguntaActual===0 && json.length > 1 && primerVuelta){//se mustra barra de avance si hay mas de una pregunta
            $("#barraNav").show();
            json.length > 16 ? $("#buttonsNav>p").remove() : "";
            avancePregunta = 100 / json.length;
            $.each(json, function(){//se agregan circulos en barra de avance
               $("#buttonsNav").append("<div class='off bookColor'></div>");
            });
            $(".off:last").removeClass("off");
            $("#progressBar").hide();
            setTimeout(hideNavBar, 4000);
            $("#progressBar, #barraNav").on("click touchend", function(){//muestra barra de avance
                if($("#barraNav").hasClass("barraNavHidden")){
                    showNavBar();
                }
            });
            $("#buttonsNav").on("click touchend", hideNavBar);//oculta barra de avance
            libro === "PROF" ? $("#progressBar").width(avancePregunta+"%") : "";
        }//elimina barra de avance al haber solo una pregunta
    }else{
        iniciarActividad();
        libro === "PROF" ? resolverActividades()  : "";
    }
}
function cargaRecursosActividad(recursoActividad){
    $("#funcionesActividad").remove();
    $("#contenidoActividad").html("");
    //se carga script controlador del tipo de actividad
    $("#estilosActividad").attr("href",strEvaluacion+"css/actividades/estilos"+recursoActividad+".css");//se cargan estilos
    var loadScript = document.createElement("script");
    loadScript.id="funcionesActividad";
    loadScript.type="text/javascript";
    loadScript.onload = function(){
        if(!primerVuelta){
            iniciarActividad();
            libro === "PROF" ? resolverActividades()  : "";
        }
        setTimeout(function(){
            json[preguntaActual].pregunta.tamañoFuente !== undefined ? $("#contenidoActividad").addClass("font"+json[preguntaActual].pregunta.tamañoFuente) : "";
            $(".respuestaDrag").length > 0 ? dragAndDrop() : "";//eventos drag and drop genericos
        }, 100);
        //cambia el tamaño de la fuente minimo 18px, maximo 22px
        actualizarMarcador();
        esUltima = preguntaActual === json.length -1 ? true : false;
    };
    loadScript.src = strEvaluacion+"js/actividades/funciones"+recursoActividad+".js";
    document.getElementsByTagName("head")[0].appendChild(loadScript);//se carga el script
}
function cargarRecursosProf(){
    $("#estilosProfesor").attr("href", "./css/estilosProfesor.css");//se cargan estilos
var loadScript = document.createElement("script");
    loadScript.id="funcionesProfesor";
    loadScript.type="text/javascript";
    loadScript.onload = function(){
        resolverActividades();
    };
    loadScript.src = "./js/funcionesProfesor.js";
    document.getElementsByTagName("head")[0].appendChild(loadScript);//se carga el script
}
function obtenerTipoActividad(numPregunta){
    var numActividad = json[numPregunta].pregunta.c03id_tipo_pregunta;
    var tipoActividad=false;
    if(numActividad+""==="5"){//se obtine el subtipo Arratra contenedor
        tipoActividad = json[numPregunta].pregunta.tipo === "vertical" ? "5.1" : tipoActividad;//Arrastra vertical
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "horizontal" ? "5.2" : tipoActividad;//Arrastra Horizontal
        tipoActividad = tipoActividad===false && json[numPregunta].contenedores !== undefined && json[numPregunta].contenedoresFilas !== undefined && json[numPregunta].pregunta.tipo === undefined ? "5.3" : tipoActividad;//Arrastra matriz
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "matrizHorizontal" ? "5.4" : tipoActividad;//Arrastra matriz horizontal
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.respuestaUnicaMultiple === true ? "5.9" : tipoActividad;//Arrastra cerdito
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].contenedores.length === 1 ? "5.8" : tipoActividad;//Arrastra acontenedor unico
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].pregunta.imagen === true && json[numPregunta].pregunta.respuestaImagen === true ? "5.7" : tipoActividad;//Arrastra imagenes /Arrastra grupos
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].pregunta.imagen === true ? "5.6" : tipoActividad;//Arrastra a imagen
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" ? "5.5" : tipoActividad;//Arrastra ordenar
    }else if(numActividad+""==="1"){
        tipoActividad =  json[numPregunta].pregunta.test === true ? "1.2" : tipoActividad;//opcion multiple TEST
        tipoActividad =  tipoActividad===false ? "1.1" : tipoActividad;//opcion multiple basico
    }else if(numActividad+""==="2"){
        tipoActividad = tipoActividad===false && json[numPregunta].respuestas[0].coordenadas !== undefined ? "2.2":tipoActividad;//respuesta multiple imagen
        tipoActividad = tipoActividad===false && tipoActividad === false ? "2.1":tipoActividad;//opcion multiple basico
    }else if(!isNaN(numActividad*1)){
        tipoActividad = numActividad+"";
    }
    return tipoActividad;
}
///funciones compartidas por los tipos de actividades
function sndClick(){
   $("#snd_clic").trigger('play');
}
function sndCorrecto() {
   $("#snd_correcta").trigger('play');
}
function sndIncorrecto() {
   $("#snd_incorrecta").trigger('play');
}
function cambiarRutaImagen(element){
    var rLibro = "";
	var raiz = "/";
    if(strAmbiente === "PROD" && evaluacion){
        rLibro = "";
        rutaLibro = "";
		raiz = "";
    }else{
        rLibro = idActividad.charAt(5) === "B" && idActividad.charAt(9) === "N" ? "../../EVAL" : rutaLibro;
    }
    element = eliminarP(element);//elimina etiquetas p
    if(element.search("<img")!==-1){
        var url, elements;
        elements = element.match(/<img.*?src='(.*?)'/g);
        elements === null ? elements = element.match(/<img.*?src="(.*?)"/g) : "";
//        element.match(/<img .*?>/g    //expresion regular etiqueta img
        $.each(elements, function(i, e){
            e.search("src='")!==-1 ? url = e.match(/<img.*?src='(.*?)'/)[1] : "";
            e.search('src="')!==-1 ? url = e.match(/<img.*?src="(.*?)"/)[1] : "";
            url = url === undefined ? (e.split(/<img.*?src=/)[1].split(".png")[0])+".png" : url;
            element = element.replace(e.split(/<img.*?src=/)[1], "'"+rLibro+raiz+url+"'");
        });
    }else if(element.search(".png")!==-1){
        element = rLibro+raiz+element;
        element = "url('"+element+"')";
    }
    return element;
}
function eliminarP(element){
    element = element.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
    return element;
}
function eliminarAcentos(element) {
    var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
    var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
    for (var i=0; i<acentos.length; i++) {
        element = element.replace(acentos.charAt(i), original.charAt(i));
    }
    return element;
}
function sigPregunta(){//determina si hay mas preguntas o se finaliza la evaluacion
    $("#contenidoActividad").addClass("lock");
    libro === "PROF" ? $("#progressBar").width((avancePregunta*(preguntaActual+2))+"%") : $("#progressBar").width((avancePregunta*(preguntaActual+1))+"%");
    esUltima && libro !== "PROF" && !evaluacion && finalizaActividad ? finalizarActividad() : siguientePregunta();
    parent.window.preguntaActual = window.preguntaActual;
    $("#contenidoActividad").trigger("cambioDePregunta");
}
function siguientePregunta(){
    preguntaActual++;
    $("#contenidoActividad").css({left:direccionCambio+"%", "transition":"left 1s"});
    setTimeout(function(){
        $("#contenidoActividad").css({left:(direccionCambio*-1)+"%", "transition":"none"});
        libro !== "PROF" ? $(".off:last").removeClass("off").addClass("bookColor") : "";
        strAmbiente === "PROD" ? $(".bookColor:not(.off)").css("background-color", bookColor) : "";
        //cambia el tamaño de la fuente minimo 18px, maximo 22px
        json[preguntaActual].pregunta.tamañoFuente !== undefined ? $("#contenidoActividad").addClass("font"+json[preguntaActual].pregunta.tamañoFuente) : "";
        iniciar();
        $(".respuestaDrag").length > 0 ? dragAndDrop() : "";//eventos drag and drop
        $("#contenidoActividad").removeClass("lock");
        actualizarMarcador();
        esUltima = preguntaActual === json.length -1 ? true : false;
        setTimeout(function(){
            $("#contenidoActividad").css({left:"0%", "transition":"left 1s"});
        }, 20);
    }, 1000);
}
function finalizarActividad(){
    actualizarMarcador();
    $("#barraNav").fadeOut(350);
    try{
        if(evaluable){
            var obj = {};
            obj.id = idActividad;
            obj.score = aciertos;
            obj.score_max = totalPreguntas;
            obj.attempts = 0;
            window.parent.saveProgressReport(obj);
        }
    }catch(e){
        console.log(e);
    }
    //define si es autoevaluacion
    evaluable === false ? $("#estadisticas").html("Autoevaluación finalizada") : $("#estadisticas").html("Has obtenido "+aciertos+" de "+totalPreguntas+" aciertos");
    !finalizaActividad ? $("#estadisticas").html("Actividad finalizada") : "";
    $("#modal").removeClass("hidden");
    setTimeout(function(){
        $("#trofeo").css({transform:"scale(1.25, 1.25)", opacity:"1"});
        setTimeout(function(){
            $("#trofeo").css({transform:"scale(1, 1)", opacity:"1"});
            $("#estadisticas").removeClass("hidden");
            setTimeout(function(){
                $("#estadisticas").addClass("hidden");
                $("#trofeo").css({width:"60px", top:"15px", right:"90px"});
                setTimeout(function(){
                    json.length > 1 ? $("#barraNav").fadeIn(350) : "";
                    $("#modal").css("background","transparent");
                },2000);
            }, 2500);
        }, 350);
    }, 300);
}
function actualizarMarcador(){
    var intento = intentosRestantes===1 ? "Intento" : "Intentos"; 
    var correcta = aciertos===1 ? "Correcta" : "Correctas";
    $("#buttonsNav>p").fadeOut(350);
    setTimeout(function(){
        $("#buttonsNav>p").html(intentosRestantes+" "+intento+"<br>"+aciertos+" "+correcta);
        $("#buttonsNav>p").fadeIn(350);
    }, 350);    
}
////////////////funcione cque se pueden compartir para todas las actividades////
function hexToRgb(hex) {//cambia hexadecimal a rgb
    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;
    return r + "," + g + "," + b;
}
function swapNodes(a, b) {//intercambia posiciones entre elementos
    var aparent = a.parentNode;
    var asibling = a.nextSibling === b ? a : a.nextSibling;
    b.parentNode.insertBefore(a, b);
    aparent.insertBefore(b, asibling);
}
function fijarCartas(){
    $("#posFixer").remove();
    var cartas = $(".respuestaDrag:not(.contenedor .respuestaDrag)");
    var timeOutPositions;
    setTimeout(setPosIn, 20);
    $("*").on('DOMSubtreeModified webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
        clearTimeout(timeOutPositions);
        timeOutPositions = setTimeout(setPosIn, 10);
    });
    function setPosIn(){
        var posiciones = [];
        cartas.css({position:"relative", top:0, left:0, transition:"none"});
        $.each(cartas, function(index, element){//obtine posiciones
            posiciones.push($(element).offset());
        });
        var top, left;
        $.each(cartas, function(index, element){//fija posiciones
            top = posiciones[index].top;
            left = posiciones[index].left;
            $(element).attr({top:top, left:left}).css({position:"absolute"}).offset(posiciones[index]);
        });
    }
}
function cartasRandom(cadena){//aplica random a las posiciones de los elementos
    if(!evaluacion){
        cadena = cadena.slice(0, -1);//se quita la coma al final
        cadena = cadena.split(",").sort(function(){return 0.75-Math.random();});//se divide en elementos
        $.each(cadena, function(index, element){//se reposicionan los elementos
            if(index+"" !== element+""){
                $(".respuestaDrag:nth("+index+"):not(.contenedor .respuestaDrag)").insertBefore($(".respuestaDrag:nth("+element+"):not(.contenedor .respuestaDrag)"));
            }
        });
    }
}
var coloresTarjeta = ["51c3af", "f8b559", "accb69", "61c2d6", "3e82c1", "9a9a9a", "e16fa6",
    "fecb2f", "e65c73", "ce92bc", "b6cd4f", "b664a4", "eb7c8f", "639bcd", "ed7d21"];
function coloresRandom(selector) {
    coloresTarjeta.sort();
    var contador = parseInt(Math.random()*coloresTarjeta.length);
    $(selector).each(function(index, element){
        $(element).css("background-color", "#"+coloresTarjeta[contador]);
        contador === coloresTarjeta.length-1 ? contador=0 : contador++;
    });
}
////////////////////via alterna para enventos del tipo elementFromPoint/////////
function elementsFromPoint(x,y){//obtiene todos los elementos del dom respecto a un punto de la pantalla
	var elements = [], previousPointerEvents = [], current, i, d;
        // get all elements via elementFromPoint, and remove them from hit-testing in order
	while ((current = $.elementFromPoint(x,y)) && elements.indexOf(current)===-1 && current != null) {
            // push the element and its current style
		elements.push(current);
		previousPointerEvents.push({
                value: current.style.getPropertyValue('pointer-events'),
                priority: current.style.getPropertyPriority('pointer-events')
            });
            // add "pointer-events: none", to get to the underlying element
		current.style.setProperty('pointer-events', 'none', 'important'); 
	}
        // restore the previous pointer-events values
	for(i = previousPointerEvents.length; d=previousPointerEvents[--i]; ) {
		elements[i].style.setProperty('pointer-events', d.value?d.value:'', d.priority); 
	}
        // return our results
	return elements;
}
(function ($){//$.elementFromPoint(x, y) //obtiene elemento html respecto a pocision el la pantalla
  var check=false, isRelative=true;
  $.elementFromPoint = function(x,y)
  {
    if(!document.elementFromPoint) return null;
    if(!check)
    {
      var sl;
      if((sl = $(document).scrollTop()) >0)
      {
       isRelative = (document.elementFromPoint(0, sl + $(window).height() -1) == null);
      }
      else if((sl = $(document).scrollLeft()) >0)
      {
       isRelative = (document.elementFromPoint(sl + $(window).width() -1, 0) == null);
      }
      check = (sl>0);
    }
    if(!isRelative)
    {
      x += $(document).scrollLeft();
      y += $(document).scrollTop();
    }
    return document.elementFromPoint(x,y);
  }
})(jQuery);
///////////////Funciones compartidas de tipo drag and drop/////////////////////
var endTimeout;
function dragAndDrop(){
    if(evaluacion){
        $(".contenedor").addClass("contenedorEval");
        $(".contenedor:not(.mostrar)").droppable({tolerance:"pointer"});
        $("#respuestas .respuestaDrag").css({margin:"5px", float:"left"}).removeClass("resph");
    }
    $("#contenidoActividad, #contenidoActividad *").each(function(ind, ele){//elimina eventos pasivos para permitir el uso de preventDefault
        ele.addEventListener("mousemove", {passive:false});
        ele.addEventListener("touchmove", {passive:false});
    });
    fijarCartas(); //fija la posicion de las cartas
    //eventos drag and drop
    var respuestaDrag, esCorrecta, posIn;
    $(".respuestaDrag").on("mousedown touchstart", sndClick);
    $(".respuestaDrag").draggable({
        start:function(e){
            $("*").off('DOMSubtreeModified webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
            clearTimeout(endTimeout);
            esCorrecta=false;
            respuestaDrag = $(this);
            respuestaDrag.parents(".columna, .contenedor").addClass("onDrag");
            respuestaDrag.removeClass("correctAnswer");
            respuestaDrag.css({transition:"none"});
            posIn = respuestaDrag.position();
            respuestaDrag.addClass("dragElement");
            $("#contenidoActividad *").on("mousemove touchmove", function(event){
                event.preventDefault();
            });
        },
        stop:function(e, ui){
            $(".onDrag").removeClass("onDrag");
            respuestaDrag.parents(".contenedor").length>0 ? respuestaDrag.addClass("correctAnswer") : "";
            $(".respuestaDrag").removeClass("dragElement");
            !esCorrecta ? respuestaDrag.addClass("dragStop").css({top: posIn.top, left: posIn.left, transition:"top 1s, left 1s"}) :"";
            clearTimeout(endTimeout);
            endTimeout = setTimeout(function(){$(".respuestaDrag").removeClass("dragStop");}, 1000);
            $("#contenidoActividad *").off("mousemove touchmove");
            $("#contenidoActividad *").on("mousemove touchmove");
        }
    });
    var contenedor;
    $(".contenedor:not(.mostrar)").droppable({
        drop: function(){
            contenedor === undefined ? contenedor = $(this) : "";
            var t17 = respuestaDrag.attr("t17");
            var t11 = contenedor.attr("t11");
            //define si es correcta
            $.each(t17.split(","), function(i, e){
                if(t11*1 === (e*1)){esCorrecta = true; return false;}
            });
            //define en arrastra grupos si el contenedor ya esta ocupado por algun grupo y evalua si son identicos
            var contenedorOcupado = contenedor.find(".respuestaDrag").length > 0;
            var grupoOcupado = $(".correctAnswer[grupo='"+respuestaDrag.attr("grupo")+"']");
            if(grupoOcupado.length>0){
                 grupoOcupado = grupoOcupado.first().parent(".contenedor").index() === contenedor.index();
                esCorrecta = grupoOcupado;
            }
            if(contenedorOcupado && esCorrecta){
                esCorrecta = esCorrecta  && respuestaDrag.attr("grupo") === contenedor.find(".correctAnswer:first").attr("grupo");
            }
            //eventos correcta/incorrecta
            if(esCorrecta || evaluacion){
                intentosRestantes > 0 ? aciertos++ : "";
                var animarCartas = true;
                if(!evaluacion){
                    sndCorrecto();
                    contenedor.addClass("lock");
                    respuestaDrag.draggable("disable");
                }else{//eventos especificos de las evaluaciones drag and drop
                    respuestaDrag.attr("W")===undefined ? respuestaDrag.attr("W", respuestaDrag.width()).attr("H", respuestaDrag.height()) : "";//almacena las dimenciones iniciales de las respuestas
                    if(contenedor.find(".respuestaDrag").length>0){
                        var contenedorAnterior = respuestaDrag.parents(".contenedor");
                        var respuestaAnterior = $(contenedor.find(".respuestaDrag"));
                        if(contenedorAnterior.length>0){//reubica respuestas entre contenedores ocupados
                            respuestaAnterior.prependTo(contenedorAnterior);
                        }else{//ubica una nueva respuesta en un contenedor ocupado
                            var width = maxWidth === undefined ? respuestaAnterior.attr("W") : maxWidth;
                            respuestaAnterior.width(width);
                            respuestaAnterior.height(respuestaAnterior.attr("H"));//revierte a las dimenciones iniciales
                            respuestaAnterior.removeClass("correctAnswer").insertBefore(respuestaDrag);
                            animarCartas = false;
                        }
                    }
                }
                animacionCartas(respuestaDrag, animarCartas);
                if(lastResort === "OrdenaElementos"){
                    if($(".contenedor:not(.noHeight)").length === $(".contenedor:has(.respuestaDrag)").length){
                        $(".contenedor.noHeight:first").removeClass("noHeight");
                    }
                    fijarCartas();
                }
                !animarCartas ? fijarCartas() : "";
                $(".contenedorOcupado").removeClass("contenedorOcupado");
                $(".contenedor:has(.respuestaDrag)").addClass("contenedorOcupado");
                var numPregunta = incisos ? 0 : preguntaActual;
                if(json[numPregunta].pregunta.agrupaContenedores === true){
                    $("#respuestas .respuestaDrag[grupo='"+respuestaDrag.attr("grupo")+"']").length === 0 ? contenedor.droppable("disable") : "";
                    !evaluacion && !incisos && $(".correctAnswer").length === conRestantes ? sigPregunta() : "";
                }else{
                    if(!evaluacion){
                        !incisos && $(".correctAnswer").length === $(".contenedor").not(".mostrar").length ? sigPregunta() : "";
                        incisos && $(".correctAnswer").length === $(".contenedor").length ? sigInciso() : "";
                        contenedor.droppable("disable")
                    }
                }
            }else{
                contenedor.removeClass("ui-state-highlight");
                respuestaDrag.css({top: posIn.top, left: posIn.left, transition:"top 1s, left 1s"});
                sndIncorrecto();
            }
            if(intentosRestantes > 0){
                intentosRestantes--;
                actualizarMarcador();
            }
            if(evaluacion){
                $(".overAnswer").css("opacity","1");
                $(".overAnswer").removeClass("overAnswer");
                $(".ui-state-highlight").removeClass("ui-state-highlight");
            }
        },
        over: function(){
            contenedor = $(this);
            if(contenedor.find(".respuestaDrag").html() !== respuestaDrag.html()){
                contenedor.addClass("ui-state-highlight");
            }
        },
        out: function(){
            $(".ui-state-highlight").removeClass("ui-state-highlight");
        }
    });
    function animacionCartas(respuestaDrag, animarCartas){
        if(respuestaDrag.parents(".contenedor").length===0 && respuestaDrag.index() < $(".respuestaDrag:not(.correctAnswer):last").index()){
            var top = respuestaDrag.attr("top");
            var left = respuestaDrag.attr("left");
            var fixElement = $(".respuestaDrag:not(.correctAnswer):last").addClass("lock");
            $(respuestaDrag.prop("outerHTML")).insertBefore(respuestaDrag).css({top:0, left:0, visibility:"hidden"}).attr("id","posFixer");
            respuestaDrag.addClass("correctAnswer").appendTo(contenedor);
            var dScrollTop = ($("#contenidoActividad").scrollTop() + fixElement.offset().top) - top;
            animarCartas ? fixElement.attr({top:top, left:left}).offset({top:fixElement.offset().top - dScrollTop, left:left}).css({transition:"top 0.8s, left 0.8s"}) : "";
            setTimeout(function(){
                if(fixElement.hasClass("resph")){
                    fixElement.css({top:0});
                }
                fixElement.removeClass("lock resph").insertBefore($("#posFixer"));
                $("#posFixer").remove();
            }, 800);
        }else{
            respuestaDrag.addClass("correctAnswer").appendTo(contenedor);
        }
    }
}
///////////////guarda las respuestas de los usuarios en actividades no evaluables o de evaluacion/////////////////////
//almacena la cadena en localStorage si se trata de actividad, no evaluacion
function cargarObjCadenas() {
    if (objCadenas.length === 0) {
        objCadenas = localStorage.getItem("avencesActividad:"+idActividad);
        if(objCadenas !== null){
            objCadenas = JSON.parse(objCadenas);
            restaurarAvace();
        }else{objCadenas = [];
            while (json.length > objCadenas.length) {
                objCadenas.push(null);
            }
        }
    } else {
        restaurarAvace();
    }
}
function guardarAvance(){
    var respuestaUsuario=[];
    switch(lastResort){
        case "Graficas":
            var selected;
            $(".categoria").each(function(index, element){
                selected = $("tr td:nth-child("+($(element).index()+1)+")").filter("td.selected");
                if(selected.length>0){
                    respuestaUsuario.push($(selected).parent().index()+","+$(selected).index());//tr,td
                }else{
                    respuestaUsuario.push("x,x");
                }
            });
            respuestaUsuario = respuestaUsuario.join("-");
            break
        case "MapasDeBits":
            $("td:not(#cellEmpty)").each(function(index, element){
                if(element.className.indexOf("pincel")!==-1){
                    respuestaUsuario.push(index+","+element.className.split("pincel")[1]);
                }
            });
            respuestaUsuario = respuestaUsuario.join("-");
            break
    }
    objCadenas[preguntaActual] = respuestaUsuario;
    evaluacion ? guardarCadenaUsuario(respuestaUsuario) : "";
    !evaluacion ? localStorage.setItem("avencesActividad:"+idActividad, JSON.stringify(objCadenas)) : "";
}
function restaurarAvace(){
    if(objCadenas[0]!==undefined && objCadenas[preguntaActual]!==null){
        switch(lastResort){
            case "Graficas":
                var cadena = objCadenas[preguntaActual].split("-");
                var element, serie;
                $.each(cadena, function(i, e){
                    if(e.indexOf("x")===-1){
                        element = $("tr").eq(e.split(",")[0]).find("td").eq(e.split(",")[1]);
                        $(element).addClass("selected");
                        serie = $(".serie").eq(i).addClass("selected");
                        calcularAltura(serie, element);
                    }
                });
                break;
            case "MapasDeBits":
                var cadena = objCadenas[preguntaActual].split("-");
                $.each(cadena, function(i, e){
                    $("td:not(#cellEmpty)").eq(e.split(",")[0]).addClass("pincel"+e.split(",")[1])
                });
                break
        }
    }
}
