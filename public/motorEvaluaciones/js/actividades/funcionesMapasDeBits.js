function iniciarActividad(){
    finalizaActividad = false;
    $("#contenidoActividad").html("<div id='pincels'></div><div id='map'></div><style id='colores'></style>");
    var color, rgbaColor;
    $("#pincels").append("<div class='pincel white selected'>&nbsp</div>");
    $.each(json[preguntaActual].pinceles, function(index, element){
        $("#pincels").append("<div class='pincel'>&nbsp</div>");
        color = element.color.replace(" ","");
        if(color.indexOf("#")!==-1){//obtine el color del relleno en rgba
            rgbaColor = hexToRgb(color.replace("#",""));
            rgbaColor = "rgba("+rgbaColor+", 0.7)";
        }else if(color.indexOf("rgba")!==-1){
            color = color.replace(color.split(",").pop().replace(")",""), "1");
            rgbaColor = color.replace(color.split(",").pop(), "0.7)");
        }else if(color.indexOf("rgb")!==-1){
            rgbaColor = color.replace("rgb", "rgba").replace(")", ", 0.7)");
        }else{
            $(".pincel:last").css({borderColor:color});
            rgbaColor= $(".pincel:last").css("border-color");
            rgbaColor = rgbaColor.replace("rgb","rgba").replace(")",", 0.7)");
        }
        $("#colores").append(".pincel[pincel='pincel"+index+"']{\n\
                border-color:"+color+";\n\
                box-shadow: 3px 3px 6px rgba(0,0,0, 0.45), inset 1px 1px 6px 1px "+rgbaColor+";\n\
                background: white;\n\
            }\n\
            td.pincel"+index+"{\n\
                background: "+rgbaColor+" !important;\n\
                box-shadow: 0 0 6px "+color+" !important;\n\
            }");
        $(".pincel:last").attr("pincel", "pincel"+index);
    });
    $("#map").append("<table id='grid'></table>");//se añade el grid
    var alto=11, ancho=11;
    var custom = json[preguntaActual].pregunta.dimensiones!==undefined;
    if(custom){//dimensiones personalizadas
        var dimensiones = json[preguntaActual].pregunta.dimensiones;
        alto=dimensiones.split(",")[0]*1;
        alto = alto > 8 ? alto : 8;//se define un minimo de 8 celdas
        ancho=dimensiones.split(",")[1]*1;
        ancho = ancho > 8 ? ancho : 8;//se define un minimo de 8 celdas
    }else{
        alto = 11; ancho = 11;
    }
    //se añaden las celdas
    while($("#grid tr").length<alto){
        $("#grid").append("<tr></tr>");
    }
    while($("#grid tr:first td").length<ancho){
        $("#grid tr").append("<td></td>");
    }
    $("td").width($("td:first").height());
    $(".pincel").on("click touchend", function(){//cambio de pincel
        $(".pincel.selected").removeClass("selected");
        $(this).addClass("selected");
        color = $(this).css("border-color");
        rgbaColor = $(this).css("box-shadow").split("rgba").pop().split(")")[0].replace("(","");
        rgbaColor = "rgba("+rgbaColor+")";
    });
    //eventos drag and drop
    //se comienza a trazar sobre el lienzo
    var onDrag=false, dragInterval, posicion=[];
    $("td:not(#cellEmpty)").on("mousedown touchstart", function(event){
        onDrag = true;
        posicion[0]=event.pageX;
        posicion[1]=event.pageY;
        clearInterval(dragInterval);
        var celda, pincel;
        celda = $(this);
        celda.removeAttr("class");
        pincel = $(".pincel.selected").attr("pincel");
        celda.addClass(pincel);
        dragInterval = setInterval(function(){
            celda = $(elementsFromPoint(posicion[0], posicion[1])).filter("td");
            if(celda.length>0){
                celda.removeAttr("class");
                if(pincel!==undefined){
                    celda.addClass(pincel);
                }
            }
        }, 10);
    });
    $("table").on("mousemove touchmove", function(event){
        if(event.pageX!==undefined){
            posicion[0]=event.pageX;
            posicion[1]=event.pageY;
        }
        return false;
    });
    $("*").on("mouseup touchend", function(){
        clearInterval(dragInterval);
        onDrag ? onDrag = false : "";
    });
    //eventos de guardar, avanzar y restaurar actividad
    !finalizaActividad ? restaurarAvace() : "";
    $("tr:last").append("<td id='cellEmpty'></td>")
    $("#cellEmpty").html("<div id='guardarRespuesta' title='Guardar avance'></div><div id='next' title='Siguiente pregunta'></div><div id='back' title='Pregunta anterior'></div><div id='reiniciar' title='Reiniciar actividad'></div>");
    $("#cellEmpty>div").addClass("button");
    $(".button").on("click touchend", function(){
        var element = $(this);
        element.addClass("lock").css({transform:"scale(0.5,0.5)", transition:"transform 0.15s linear"});
        setTimeout(function(){
            element.removeClass("lock").css({transform:"scale(1,1)"});
        }, 150);
    });
    //reinicia la actividad
    $("#reiniciar").on("click touchend", function(){
        localStorage.removeItem("avencesActividad:"+idActividad);
        setTimeout(function(){
            location.reload();
        }, 150);
        return false;
    });
    //guarda la pregunta
    $("#guardarRespuesta").on("click touchend", function(){//boton guardar
        guardarAvance();
        return false;
    });
    $("#next").on("click touchend", function(){//boton siguiente pregunta
         preguntaActual = preguntaActual===json.length-1 ? -1 : preguntaActual;
        sigPregunta();
        return false;
    });
    $("#back").on("click touchend", function(){//boton pregunta anterior
        preguntaActual = preguntaActual===0 ? json.length-2 : preguntaActual-2;
        sigPregunta();
        return false;
    });
}