function iniciarActividad(){
    var countMostrar = mostrarRespuestasCount();
    intentosRestantes = json[preguntaActual].respuestas.length - countMostrar;
    totalPreguntas += intentosRestantes;
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $("#contenedores").append('<table><tr class="encabezado"><td class="textos blank">&nbsp;</td></tr></table>');
    $.each(json[preguntaActual].contenedoresFilas, function (index, element) {
        $(".encabezado").append('<td class="textos">'+cambiarRutaImagen(element)+'</td>');
    });
    var maxTableHeight = getMaxSizeHeader();
    $.each(json[preguntaActual].contenedores,function(index, element){
        $("table").append('<tr id="fila'+index+'"><td class="textoFilas textos">'+cambiarRutaImagen(element)+'</td></tr>');
    });        
    var fila="", t17="", t11 = "", mostrar="", classMostrar="";
    
    //determina el maximo alto de las filas
    var maxHeight = $("tr:last>td:first").height(), cadena="";
    //se insertan las celdas
    $.each(json[preguntaActual].respuestas, function (index, element) {
        classMostrar = element.mostrar ? "mostrar":"";
        if(Object.prototype.toString.call(element.t17correcta)==="[object Array]"){ 
            console.log("es un array");
            t17 = element.t17correcta.join("-");
            fila = t17.split("-")[0].split(",")[0];
            t11 = t17.split("-")[0];
        }else{ 
            t17 = element.t17correcta;
            fila = t17.split(",")[0];
            t11 = t17;
            
        }
        mostrar = element.mostrar === undefined ? "":'<div>'+cambiarRutaImagen(element.t13respuesta)+'</div>';
        if(element.mostrar !== true){
            cadena+=index+",";
            $("#respuestas").append('<div t17="'+t17+'" class="respuestaDrag resph '+mostrar+'"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
            
        }
        $("#fila"+fila).append('<td class="contenedor '+classMostrar+'" t11="'+t11+'" indice='+index+'>'+mostrar+'</td>');
    });
    cartasRandom(cadena);
    var anchoContenedor = (100 -25) / json[preguntaActual].contenedoresFilas.length;
    $("tr:not(tr:first), .respuestaDrag").css({maxHeight:maxHeight+"px", minHeight:maxHeight+"px", height:maxHeight+"px"}); //fija el alto de las filas
    $(".contenedor").css({height:0, width:anchoContenedor+"%"});//define el alto para las celdas
    $(".respuestaDrag").css({height:0, width: $(".contenedor:last").width()+"px"});//define las dimenciones para las respuestas
    var indexRespuesta = ""; t17 = "";
    $(".respuestaDrag").each(function(){
        indexRespuesta = ""; t17 = $(this).attr("t17").split("-");
        $.each(t17, function(index, element){
            indexRespuesta += $(".contenedor[t11='"+element+"']").attr("indice")+",";
        });
        indexRespuesta = indexRespuesta.slice(0, -1);
        $(this).attr("t17", indexRespuesta);
        $(".contenedor[t11='"+t17[0]+"']").attr("t11", $(".contenedor[t11='"+t17[0]+"']").attr("indice"));
    });
    var tituloMatriz  = json[preguntaActual].pregunta.descripcion ? json[preguntaActual].pregunta.descripcion : "Tema";
    $(".blank").append(tituloMatriz); coloresRandom("tr:not(tr:first)"); apilaElementos();
}

function getMaxSizeHeader(){
    var sizeTable = [];
    $("table").each(function(){
        sizeTable.push($(this).height());
    });
    var max = Math.max.apply(null, sizeTable);
    return  max;
}

function apilaElementos(){
    $(".respuestaDrag").addClass("resph");
    $(".respuestaDrag:first").removeClass("resph");
}

function mostrarRespuestasCount(){
    var count = 0;
    $.each(json[preguntaActual].respuestas, function (index, element) {
        if(element.mostrar === true){
            count++;
        }
    });
    return count;
}