function iniciarActividad(){
    $("#contenidoActividad").html("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div><div id='respuestasActividad'></div>")
    $("#contenidoActividad").append("<textarea></textarea>");
    var rspUsuario = localStorage.getItem("rspUsuarioPreguntaAbierta");
    if(rspUsuario!==null){
        $("textArea").val(rspUsuario);
    }
    $("textArea").on("keyup", function(){
        localStorage.setItem("rspUsuarioPreguntaAbierta", $("textArea").val());
    });
}