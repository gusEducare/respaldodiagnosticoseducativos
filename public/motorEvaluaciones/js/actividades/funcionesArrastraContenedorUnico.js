function iniciarActividad() {
    var cadena = "";
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }

    var espacioDisponible = $(window).height() - ((7 * json[preguntaActual].contenedores.length));
    var altoContenedores = altoContenedores = (espacioDisponible / json[preguntaActual].respuestas.length) - 4;
    var dimensiones, widthContenedor, heightContenedor, posicion, top, left;

    var anchoImagen = json[preguntaActual].pregunta.anchoImagen !== undefined ? json[preguntaActual].pregunta.anchoImagen : 70;
    var anchoOpciones = json[preguntaActual].pregunta.anchoImagen !== undefined ? 100 - json[preguntaActual].pregunta.anchoImagen : 30;
    $("#contenedores").width(anchoImagen+"%");
    $("#respuestas").width(anchoOpciones+"%");
    $.each(json[preguntaActual].respuestas, function (index, element) {
        $("#respuestas").append('<div t17="'+element.t17correcta+'" class="respuestaDrag" ></div>');
        $(".respuestaDrag").eq(index).css({backgroundImage: cambiarRutaImagen(element.t13respuesta), backgroundRepeat:"no-repeat"})
        cadena += index+",";
    });
    $.each(json[preguntaActual].contenedores, function (index, element) {
        $("#contenedores").append('<div class="parent"></div>');
        dimensiones = element.Contenedor[3].split(",");
        posicion = element.Contenedor[1].split(",");
        widthParent = dimensiones[0].trim();
        heightContenedor = dimensiones[1].trim();
        top = posicion[0].trim();
        left = posicion[1].trim();
        $(".parent").last().css({width:widthParent, height:heightContenedor, top:top+"px", left:left+"px", position:"absolute"});
        $.each(json[preguntaActual].respuestas, function (indexR, elementR) {            
            if(parseInt(elementR.t17correcta) === index){
                $(".parent").last().append('<div class="contenedor" t11='+index+'></div>');
            }
        });
    });
    json[preguntaActual].pregunta.tamanyoReal === true ? $(".respuestaDrag").css("background-size", "auto"):"";
    json[preguntaActual].pregunta.borde === false ? $(".contenedor").css("border", "none"):"";
    $("#contenedores").css({backgroundImage:cambiarRutaImagen(json[preguntaActual].pregunta.url)})
    intentosRestantes = $(".contenedor").length;
    totalPreguntas += intentosRestantes;
    $(".respuestaDrag").height(altoContenedores);
    cartasRandom(cadena);
    
}
