var cerditoInterval;
var total;
var moneda;
var sumatoria;
var cerdito = true;
function iniciarActividad(){
    total = 0, moneda = "", sumatoria = 0; intentosRestantes=0;
    var tamanyoReal = json[preguntaActual].pregunta.tamanyoReal === true;
    $("#contenidoActividad").html("<div id='pregunta'></div><div id='respuestas'></div>");
    cerdito = json[preguntaActual].respuestas[0].valor !== undefined;
    if(cerdito){
        //se inserta el cerdito :B
        $("#pregunta").append("<div id='cerdito' class='contenedor'></div><img id='dialog'  class='contenedor' src='img/cerdito/dialog.png'/>");
            $("#cerdito").append("<img id='body' src='img/cerdito/cerdito.png'/>\n\
                <img id='eyes' src='img/cerdito/eyes.png'/>\n\
                <img id='feet' src='img/cerdito/feet.png'/>\n\
                <img id='shadow' src='img/cerdito/shadow.png'/>\n\
                <img id='ears' src='img/cerdito/ears.png'/>");
    }else{
        $("#pregunta").css({backgroundImage:cambiarRutaImagen(json[preguntaActual].pregunta.url), backgroundRepeat:"no-repeat", backgroundPosition:"center"});
        var top, left, width, height;
        $.each(json[preguntaActual].contenedores, function(index, element){
            $("#pregunta").append("<table class='contenedores'></table>");
            top=element.Contenedor[1].split(",")[0];//top
            left=element.Contenedor[1].split(",")[1];//left
            width=element.Contenedor[3].split(",")[0];//width
            height=element.Contenedor[3].split(",")[1];//height
            $(".contenedores:last").css({top:top+"px", left:left+"px", width:width+"px"});
            for(var x=0; x<json[preguntaActual].pregunta.renglones; x++){
                $(".contenedores:last").append("<tr></tr>");
            }
            for(var x=0; x<json[preguntaActual].pregunta.filas; x++){
                $("tr").append("<td class='contenedor'></td>");
            }
            !tamanyoReal ? $(".contenedores:last").css({height: height+"px"}) : $(".contenedores:last .contenedor").css({height: height+"px"});
        });
        //aplica bandera borde
        json[preguntaActual].pregunta.borde === false ? $("td").css("border", "none") : "";
    }
    //aplica bandera anchoImagen
    var imgWidth = json[preguntaActual].pregunta.anchoImagen !== undefined ? json[preguntaActual].pregunta.anchoImagen : 0;
    imgWidth > 0 ? ($("#pregunta").css("width", imgWidth+"%"), $("#respuestas").css("width", (100 - imgWidth)+"%")) : "";
    json[preguntaActual].pregunta.moneda !== false ? moneda = "$" : "";
//    se calcula el total que se debe lograr y se insertan los elementos arrastrables
    $.each(json[preguntaActual].respuestas, function(index, element){
        cerdito ? total += (element.numeroCorrectas * element.valor) : total+=element.numeroCorrectas;
        //define variables de evaluacion
        intentosRestantes += element.numeroCorrectas;
        totalPreguntas += element.numeroCorrectas;
        $("#respuestas").append("<div class='columna'></div>");
        var t17 = element.numeroCorrectas > 0 ? 0 : 1;
        for(var i=0; i<element.clones; i++){//se añaden las tarjetas de respuesta
            $(".columna:last").append("<div class='respuesta' t17='"+t17+"' cntdr='"+element.numeroCorrectas+"' valor='"+element.valor+"'></div>");
            $(".respuesta:last").css({backgroundImage:cambiarRutaImagen(element.t13respuesta), right:10*i+"px", zIndex:(i+1)});
            //aplica bandera alto y ancho
            element.ancho !== undefined ? $(".respuesta:last").width(element.ancho) : "";
            element.alto !== undefined ? $(".respuesta:last").parent(".columna").height(element.alto) : "";
        }
    });
    if(cerdito){
        //se inserta indicador de pregunta
        $("#pregunta").append("<div id='total'>"+moneda+total+"</div>");
        //se inserta contador de avance
        $("#pregunta").append("<div id='avance'>"+moneda+"0</div>");
        //animacion de entrada
        $("#respuestas").hide();
        $("#cerdito").css({transform:"scale(0.1, 0.1)", opacity:0});
        $("#total").css({transform:"scale(0.1, 0.1)", opacity:0});
        $("#cerdito #ears").css({top:"80px", transform:"scale(0.1, 0.1)"});
        $("#cerdito #feet").css({bottom:"30px", transform:"scale(0.1, 0.1)"});
        $("#cerdito #shadow, #dialog, #avance").hide();
        setTimeout(function(){
            $("#cerdito").css({transform:"scale(1.15, 1.15)", opacity:1, transition:"opacity 0.35s, transform 0.35s"});
            $("#total").css({transform:"scale(1.15, 1.15)", opacity:"1", transition:" bottom .35s, transform 0.35s"});
            setTimeout(function(){
                $("#cerdito").css({transform:"scale(1, 1)", opacity:1, transition:"transform 0.35s"});
                $("#total").css({transform:"scale(1, 1)", opacity:1, transition:"transform 0.35s"});
                setTimeout(function(){
                    $("#cerdito #shadow").fadeIn(500);
                    $("#cerdito #feet").css({transform:"scale(1, 1)", bottom:"4px", transition:" bottom .5s, transform 0.5s"});
                    $("#cerdito #ears").css({transform:"scale(1, 1)", top:"3px", transition:" top .5s, transform 0.5s"});
                    $("#respuestas").toggle("slide", {direction: "right"}, 900);
                }, 200);
            }, 300);
            setTimeout(function(){$("#dialog, #avance").toggle("blind", {direction:"up"}, 500);}, 1000);
            setTimeout(function(){$("#cerdito #eyes").fadeOut(200);}, 1750);
        }, 10);
    }else{
        $(".columna").css({height:$(".contenedor:first").height()})
        $(".respuesta").css({width:$(".contenedor:first").width()});//aplica ancho y alto acorde a contenedores
    }
    //aplica bandera tamanyoReal
    tamanyoReal === true ? $("#pregunta").css("background-size","auto") : $("#pregunta").css("background-size","contain");
    //eventos drag and drop
    document.getElementById("contenidoActividad").addEventListener("mousemove",{passive:false});
    document.getElementById("contenidoActividad").addEventListener("touchmove",{passive:false});
    setDragElements($(".columna .respuesta:last-child"));
}

function setDragElements(selector){
    var dragElement, posEnd=[], posIn;
    selector.draggable({
        start: function(){
            $(".dragEvent").removeClass("dragEvent");
            sndClick();
            dragElement = $(this);
            dragElement.css({transition:"none"});
            dragElement.parent(".columna").addClass("dragEvent");
            posIn = dragElement.position();
            $("#contenidoActividad").on("mousemove, touchmove", function(event){
                event.preventDefault();
            });
        },
        drag: function(event){
            posEnd[0] = event.pageX;
            posEnd[1] = event.pageY; 
        },
        stop:function(){
            $("#contenidoActividad").off("mousemove, touchmove");
            $("#contenidoActividad").on("mousemove, touchmove");
            var esCorrecta = false;
            var dropElement = $(elementsFromPoint(posEnd[0], posEnd[1])).filter(".contenedor");
            if(dropElement.length>0){
                if(cerdito){
                    if(dragElement.attr("t17")*1 === 0 && dragElement.parent(".columna").find(".correctAnswer").length < dragElement.attr("cntdr")*1){
                        esCorrecta = true;
                        $(".dragEvent").removeClass("dragEvent");
                        //animaciones del elemento arrastrado
                        dragElement.css("pointer-events","none").toggle("drop", {direction: "down"}, 200);
                        setTimeout(function(){
                            dragElement.draggable('disable').addClass("correctAnswer");
                            dragElement.insertBefore(dragElement.parent(".columna").find(".respuesta:first"));
                            setDragElements($(".columna .respuesta:last-child"));
                        }, 350);
                        //animaciones cerdito
                        $("#cerdito #eyes").fadeIn(200);
                        setTimeout(function(){
                            $("#cerdito #eyes").fadeOut(200);
                        },800);
                        //aimaciones del globo de dialogo
                        $("#avance, #dialog").css({transform: "scale(0.1, 0.1)", opacity: 0, transition:"opacity 0.5s, transform 0.5s"});
                        setTimeout(function(){
                            //calcula sumatoria
                                sumatoria += dragElement.attr("valor")*1;//obtiene el valor
                                sumatoria = Math.round( sumatoria * 10 ) / 10;//redondea
                                sumatoria = parseFloat(sumatoria).toFixed(1)*1;//pasa a decimal
                                $("#avance").html(moneda+sumatoria);//envia la sumatoria al globo de dialogo
                                sumatoria === total ? setTimeout(sigPregunta, 400) : "";//siguiente pregunta (al finalizar)
                            //concluye animacion
                            $("#avance, #dialog").css({transform: "scale(1,1)", opacity: 1, transition:"opacity 0.4s, transform 0.4s"});
                        }, 400);
                        esCorrecta=true;
                        sndCorrecto();
                    }else{
                        sndIncorrecto();
                    }
                }else{
                    var dropElement = $(".contenedor:not(.dropped):last");
                    if(dragElement.attr("t17")*1 === dropElement.parents("table").index()){
                        dropElement.addClass("dropped correctAnswer");
                        dragElement.appendTo(dropElement).draggable("disable");
                        setDragElements($(".columna .respuesta:last-child"));
                        sndCorrecto(); esCorrecta=true;
                        $(".correctAnswer").length === total ? setTimeout(sigPregunta, 500) : "";
                    }else{
                        sndIncorrecto();
                    }
                }
                if(intentosRestantes > 0){
                    esCorrecta ? aciertos++ : "";
                    intentosRestantes--
                    actualizarMarcador();
                }             
            }
            
            if(!esCorrecta){
                dragElement.css({top: posIn.top, left: posIn.left, transition:"top 1s, left 1s"});
            }
        }
    });
}

// "moneda"     //false     //remueve el simbolo $ predefinido por la actividad