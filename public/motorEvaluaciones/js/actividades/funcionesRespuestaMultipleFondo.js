var freeInterval;
function iniciarActividad(){
    if(evaluacion){
        $("#contenidoActividad").bind("cambioDePregunta", function(){
            clearInterval(freeInterval);
            $("#contenidoActividad").css("background","none");
        });
    }
    $("#contenidoActividad").html("").css({backgroundImage:cambiarRutaImagen(json[preguntaActual].pregunta.url)});
    $.each(json[preguntaActual].respuestas, function(index, element){
        var top=element.coordenadas.split(",")[0], left = element.coordenadas.split(",")[1];
        $("#contenidoActividad").append("<div class='respuesta free' t17='"+element.t17correcta+"' style='top: "+top+"px; left: "+left+"px;'></div>");
        if(json[preguntaActual].pregunta.forma !== undefined && json[preguntaActual].pregunta.forma === "circulo"){
            $(".respuesta:last").css({borderRadius:"50%"});
        }
    });
    
    //activa bandera tamañoRespuestas
    var radio = json[preguntaActual].pregunta.radio;
    if(radio!==undefined){
        $(".respuesta").css({width: radio+"px", height: radio+"px"});
    }
    //activa bandera color
    var color = json[preguntaActual].pregunta.color;
    if(color!==undefined){
        $(".respuesta").css({backgroundColor:color, borderColor:color});
        var backColor = $(".respuesta").first().css("background-color");
        backColor = backColor.replace("rgb","rgba").replace(")",", 0.4)");
        var borderColor = backColor.replace("rgb","rgba").replace(")",", 0.6)");
        $(".respuesta").css({backgroundColor:backColor, borderColor:borderColor});
    }
    setTimeout(function(){
        $(".respuesta").css({transform:"scale(1, 1)"});
        $(".free").css({opacity:"1"});
        setTimeout(function(){
            $(".free").css({opacity:"0.5"});
        },1250);
        freeInterval = setInterval(function(){
            $(".free").css({opacity:"1"});
            setTimeout(function(){
                $(".free").css({opacity:"0.5"});
            }, 1250);
        }, 2500);
    }, 100);
    
    
    var contador=0; var correctasInciso = $("div[t17=1]").length;
    totalPreguntas += correctasInciso; intentosRestantes = correctasInciso;
    //eventos de respuesta
    $(".free").on("click touchend", function(){
        var element = this;
        if(evaluacion){
            sndClick();
               $(element).css({transform:"scale(0.1, 0.1)", opacity:"0", transition: "none"});
                setTimeout(function(){
                    $(element).hasClass("selected") ? $(element).removeClass("selected").addClass("free") : $(element).removeClass("free").addClass("selected");
                    $(element).css({transform:"scale(1, 1)", opacity:"1", transition: "0.35s"});
                }, 50);
        }else{
            contador++;
            var enOrden;
            if(json[preguntaActual].pregunta.ordenados !== true){
                enOrden = true;
            }else{
                var indexAnterior = $("div[t17='1']").index(element) - 1;
                indexAnterior === -1 ? enOrden=true : enOrden = $("div[t17=1]:nth("+indexAnterior+")").hasClass("correctAnswer");
            }
           if($(element).attr("t17") === "1" && enOrden){
               sndCorrecto();
                contador <= correctasInciso ? aciertos++ : "";
               $(element).css({transform:"scale(0.1, 0.1)", opacity:"0", transition: "none"});
                setTimeout(function(){
                    $(element).addClass("correctAnswer").removeClass("free");
                    $(element).css({transform:"scale(1, 1)", opacity:"1", transition: "0.35s"});
                    setTimeout(function(){
                        if($(".correctAnswer").length === correctasInciso){
                            sigPregunta();
                            window.clearInterval(freeInterval);
                        }
                    }, 600);
                }, 50);
           }else{
               sndIncorrecto();
               $(element).css({transform:"scale(0.1, 0.1)", opacity:"0", transition: "none"});
                setTimeout(function(){
                    $(element).addClass("badAnswer").removeClass("free");
                    $(element).css({transform:"scale(1, 1)", opacity:"1", transition: "0.35s"});
                    setTimeout(function(){
                        if(json[preguntaActual].pregunta.ordenados === true && $(element).attr("t17")==="1"){
                            $(element).removeClass("badAnswer").addClass("free");
                        }else{
                            $(element).css({opacity:"0.5"});
                        }
                    }, 600);
                }, 50);
           }
            if(contador <= correctasInciso && intentosRestantes > 0){
                 intentosRestantes--;
                 actualizarMarcador();
            }
        }
    });
}

///////repertorio de banderas nuevas
//  "radio":        define el alto y ancho de las cajas de respuesta
//  "forma":        define la forma de la respuesta circulo o cuadrado(predefinido)
//  ""color:        define el color que tendra el elemento de respuesta