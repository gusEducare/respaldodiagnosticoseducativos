var coloresEtiqueta=["#816dbb","#9a9a9a","#b664a4","#41abc3","#38b29f","#3d82c3"];
var coloresRespuesta=["#a698cf","#b8b8b8","#cb92bf","#79c4d4","#73c8bb","#76a7d4"];
var posEnd=[], dragInterval, inDrag=false;
function iniciarActividad(){
    //se genera la estructura html de la actividad
    $("#contenidoActividad").html("<div id='contenido'></div><div id='colRespuestas'></div>");
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    //se añaden las preguntas
    var contenido="", colorIndex = parseInt(Math.random()*coloresEtiqueta.length);
    var colorEtiqueta, colorRespuesta;
    
    $.each(json[preguntaActual].respuestas, function(index, element){
        colorEtiqueta = coloresEtiqueta[colorIndex];
        colorRespuesta = coloresRespuesta[colorIndex];
        contenido += "<div class='respuesta' style='background-color: "+colorRespuesta+"; border-color: "+colorEtiqueta+" !important;'><div class='etiqueta' t17='"+
                element.t17correcta+"' style='background: "+colorEtiqueta+"'>"+element.etiqueta+"</div>"+cambiarRutaImagen(element.t13respuesta)+"</div>";
        colorIndex = colorIndex === coloresEtiqueta.length-1 ? 0 : colorIndex+1;
    });
    $("#colRespuestas").html(contenido);
    
    contenido=""; var cadenaRandom="";
    //se añade el contenido
    var contenedor = "<div class='contenedor'></div>";
    $.each(json[preguntaActual].preguntas, function(index, element){
        //habilita bandera columnas
        if(json[preguntaActual].pregunta.columnas === true){
            cadenaRandom += index+",";
            contenido += "<div class='columna' t11='"+index+"'>"+contenedor+cambiarRutaImagen(element.t11pregunta)+"</div>";
        }else{
            contenido += cambiarRutaImagen(element.t11pregunta);
            contenido += index<json[preguntaActual].preguntas.length-1 ? contenedor : "";
        }
    });
    
    //habilita bandera pintaUltimaCaja
    contenido += json[preguntaActual].pregunta.columnas !== true && json[preguntaActual].pregunta.pintaUltimaCaja === true ? "<div class='contenedor'></div>" : "";
    //se inserta el contenido
    $("#contenido").html(contenido);
    //se aplica random a las columnas
    if($(".columna").length>0){
        cadenaRandom = cadenaRandom.slice(0, -1).split(",").sort(function(){return 0.5-Math.random()});
        $.each(cadenaRandom, function(index, element){
            if(index+"" !== element+""){
                $(".columna:nth("+element+")").insertBefore($(".columna:nth("+index+")"));
            }
        });
    }
    //aplica vandera "orientacion"
    if(json[preguntaActual].pregunta.orientacion!==undefined){
        $("#contenido, #colRespuestas").addClass(json[preguntaActual].pregunta.orientacion);
    }    
    //define el tamaño para el contenido
    if($("#contenido").hasClass("vertical")){
        $("#contenido").width($("#contenidoActividad").width() - 30 - $("#colRespuestas").width());
    }else{
        $("#contenido").height($("#contenidoActividad").height() - 10 - $("#colRespuestas").height());
    }
    
    //se inician variables de evaluacion 
    intentosRestantes = $(".contenedor").length;
    totalPreguntas+=intentosRestantes;
    
    //eventos drag and drop
    document.getElementById("contenidoActividad").addEventListener("mousemove",{passive:false});
    document.getElementById("contenidoActividad").addEventListener("touchmove",{passive:false});
    $(".respuesta").each(function(index, element){//se añade evento para iniciar arrastre
        element.addEventListener("mousedown", function(event){
            dragStart(event, this);
            return false;
        }, true);
    });
    function dragStart(event, respuesta){
        sndClick();
        clearInterval(dragInterval);
        inDrag=true;
        $("*").css("cursor","pointer");
        var nuevaEtiqueta = $(respuesta).find(".etiqueta").prop("outerHTML");
        $("#contenidoActividad").append(nuevaEtiqueta);
        while($("#contenidoActividad>.etiqueta").length>1){
           $("#contenidoActividad>.etiqueta:last").remove();
        }
        var etiqueta = $("#contenidoActividad>.etiqueta");
        //se pocisiona la etiqueta
        event.pageX>0 ? posEnd[0]=event.pageX : ""; 
        event.pageY>0 ? posEnd[1]=event.pageY : "";
        $(etiqueta).css({top:posEnd[1]-20, left:posEnd[0]-20, transition:"none"});
        //se bloquea la generacion de nuevas etiquetas
        $(".respuesta").addClass("lock");
        //intervalo de arrastre
        $(etiqueta).css({transform:"scale(1, 1)", opacity:"1", transition:"top 50ms, left 50ms"});
        dragInterval = setInterval(function(){
           $(etiqueta).css({top:(posEnd[1]-20)+"px", left:(posEnd[0]-20)+"px"});
        }, 30);
        $("#contenidoActividad").on("mousemove touchmove", function(event){
            event.preventDefault();
        });
    }
    document.body.addEventListener("mousemove", function(event){//actualiza coordenasdas durante el arrastre
        if(event.pageX !== undefined){
            event.pageX>0 ? posEnd[0]=event.pageX : ""; 
            event.pageY>0 ? posEnd[1]=event.pageY : "";
        }
    }, true);
    document.body.addEventListener("mouseup", function(event){//se finaliza el arrastre
        $("#contenidoActividad").off("mousemove touchmove");
        $("#contenidoActividad").on("mousemove touchmove");
        if(inDrag){
            inDrag=false;
            clearInterval(dragInterval);
            $("*:not(.respuesta, .etiqueta)").css("cursor","initial");
            //se obtiene el elemento dropeado
            var elements = elementsFromPoint(event.pageX, event.pageY);
            var dropElement = $(elements).filter(".contenedor");
            if($(dropElement)[0]!==undefined){//verifica que sea un contenedor
                //se evalua la respuesta
                var esCorrecta=false;
                var dragElement = $("#contenidoActividad>.etiqueta");
                var t11;
                t11 = json[preguntaActual].pregunta.columnas === true ? $(dropElement).parent(".columna").attr("t11") : $(".contenedor").index(dropElement);
                var t17 = $(dragElement).attr("t17");
                $.each(t17.split(","), function(i, e){
                    if(t11*1 === (e*1)){
                        esCorrecta = true;
                        return false;
                    }
                });
                //eventos correcta/incorrecta
                if(esCorrecta || evaluacion){
                    if(!evaluacion){
                        sndCorrecto();
                        intentosRestantes > 0 ? aciertos++ : "";
                        $(dropElement).addClass("lock");
                    }else{
                        $(dropElement).find(".etiqueta").remove();
                    }
                    $(dragElement).css({top:0, left:0, margin: 0, paddin: 0});
                    $(dropElement).append($(dragElement));
                    var color = $(dragElement).css("background-color");
                    $(dropElement).css({background:color, borderColor:color});
                    $(".respuesta").removeClass("lock");
                    !evaluacion && $(".contenedor .etiqueta").length === $(".contenedor").length ? sigPregunta() : "";
                    $("#contenidoActividad>.etiqueta:last").remove();
                }else{
                    sndIncorrecto();
                    $("#contenidoActividad>.etiqueta:first").css({background:"#e1385a"})
                    setTimeout(eliminarEtiqueta, 200);
                }
                //ACTUALIZA MARCADOR
                if(intentosRestantes > 0){
                    intentosRestantes--;
                    actualizarMarcador();
                }                
            }else{
                eliminarEtiqueta();
            }
        }
    }, true);
}

function eliminarEtiqueta(){
    $("#contenidoActividad>.etiqueta:first").css({transform:"scale(0.1, 0.1)", opacity:"0", transition:"transform 0.35s, opacity 0.35s"});
    setTimeout(function(){
        $("#contenidoActividad>.etiqueta").remove();
        $(".respuesta").removeClass("lock");
    },375);
}