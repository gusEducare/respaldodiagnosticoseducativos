
function iniciarActividad() {
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    var cadena = "";
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    var espacioDisponible = $(window).height() - ((7 * json[preguntaActual].contenedores.length));
    var altoContenedores = (espacioDisponible / json[preguntaActual].respuestas.length) - 4;
    $.each(json[preguntaActual].respuestas, function (index, element) {
        $("#respuestas").append('<div t17="'+element.t17correcta+'" class="respuestaDrag"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
        cadena += index+",";
    });
    $.each(json[preguntaActual].contenedores, function (index, element) {
        $("#contenedores").append('<div class="contenedor" t11='+index+'><div class="textoContenedor">'+element.Contenedor[0]+'</div></div>');
    });
    $(".contenedor, .respuestaDrag").height(altoContenedores);
    coloresRandom(".respuestaDrag");
    cartasRandom(cadena);
}
