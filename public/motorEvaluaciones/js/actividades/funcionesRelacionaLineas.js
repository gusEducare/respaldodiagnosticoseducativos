function iniciarActividad(){
    lineasCorrectas=[]; contador=0; aciertosLineas=0;
    var cadenaShuffle="";
    $("#contenidoActividad").html("<div id='contenedorIzquierda'></div><div id='contenedorDerecha'></div><svg id='svgLine' height='100%' xmlns=svgNS xmlns:xlink=svgNSLink><line></line></svg>");
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $.each(json[preguntaActual].preguntas, function(index, element){
        cadenaShuffle += index + ","; totalPreguntas++; intentosRestantes++;
        $("#contenedorIzquierda").append("<div class='columnas'><div class='colIzquierda' t11='"+(index+1)+"'><div></div></div><div class='circuloLineas'></div></div>");
        $(".colIzquierda:last>div").append(cambiarRutaImagen(element.t11pregunta));
    });
    $.each(json[preguntaActual].respuestas, function(index, element){
        $("#contenedorDerecha").append("<div class='columnas'><div class='colDerecha'   t17='"+element.t17correcta+"'><div>"+cambiarRutaImagen(element.t13respuesta)+"</div></div><div class='circuloLineas'></div></div>");
    });
    $(".circuloLineas").hide();
    $(".columnas").append("<div class='dragLine'></div>");
    var imgTimeout;
    if($("#contenidoActividad img").length>0){//si hay imagenes
        $("img").bind("load error", function(){//se posicionan los cisculos hasta que carguen
            clearTimeout(imgTimeout);
            imgTimeout = setTimeout(colocarCirculos, 50);
        });
    }else{//si no hay imagenes
        setTimeout(colocarCirculos, 50);
    }
    function colocarCirculos(){
        var posIzquierda=0, posDerecha=0;
        //posicion de los circulos en la columna derecha
        $(".colIzquierda").each(function(index, element){
            posIzquierda = element.scrollWidth > posIzquierda ? element.scrollWidth+10 : posIzquierda;
        });
        //posicion de los circulos en la columna izquierda
        $(".colDerecha").each(function(index, element){
            posDerecha = element.scrollWidth > posDerecha ? element.scrollWidth+10 : posDerecha;
        });
        $("#contenedorIzquierda .circuloLineas").css({left: posIzquierda});
        $("#contenedorDerecha .circuloLineas").css({right: posDerecha});
        $(".circuloLineas").show();
    }
    
    $("#contenidoActividad img").css("pointer-events","none");
    var maxHeigth = 0;//se definen las dimenciones de los contenedores
    if(!evaluacion && $(".colIzquierda").length > 3){//si hay mas de 3 filas
        //se divide el alto de la pantalla entre el numero de filas
        maxHeigth = ($("#contenedorIzquierda").height() / $(".colIzquierda").length)-15;
    }else{//si hay menos de 4 filas
        $(".colIzquierda, .colDerecha").each(function(index, element){//se define el alto comun para las filas
            maxHeigth = maxHeigth<$(element)[0].scrollHeight ? $(element)[0].scrollHeight : maxHeigth;
        });
    }

    //se aplican banderas alto y ancho para imagenes
    var imgWidth, imgHeight;
    imgWidth = $("#contenidoActividad img").length > 1 && json[preguntaActual].pregunta.anchoImagen !== undefined ? json[preguntaActual].pregunta.anchoImagen : "auto";
    imgHeight = $("#contenidoActividad img").length > 1 && json[preguntaActual].pregunta.altoImagen !== undefined ? json[preguntaActual].pregunta.altoImagen : maxHeigth;
    $(".columnas>div:has(img)").css({background:"transparent", width:imgWidth});
    $("#contenidoActividad img").css({width: imgWidth, height:imgHeight});
    $(".colIzquierda, .colDerecha").css("height", imgHeight).find("img").css("height", imgHeight-8);
    
    /*** LightBox ***/
    if(json[preguntaActual].pregunta.box !== null && json[preguntaActual].pregunta.box !== undefined && json[preguntaActual].pregunta.box != ''){
        // $('#contenidoActividad').prepend('<div class="btn-box"><img id="box" src="icon-' + json[preguntaActual].pregunta.box_type + '.png"></div>');
        $('#preguntaActividad').append('<div class="btn-box"><img id="box" src="icon-' + json[preguntaActual].pregunta.box_type + '.png"></div>');
        $('#preguntaActividad').children('#cuestionMarker').next().css({
            "display": "inline-block",
            "vertical-align": "top",
            "width": "90%"
        });
        $('#box').click(function() {
            $('#contenidoActividad').append('<div class="box"><div class="internal-box"><div class="cerrar"><span id="cerrar">x</span></div><img src="' + json[preguntaActual].pregunta.box + '"><div></div>');
            $('#cerrar').click(function() {
                $('.box > .internal-box').addClass('remove');
                $('.box').addClass('remove');
                setTimeout(function() {
                    $('.box').remove();
                }, 1000);
            });
        });
    }else{
    }
    /*** Fin LightBox ***/
    
    //se genera el random de los elementos
    if(evaluacion && strAmbiente === "PROD"){
        cadenaShuffle = cadenaShuffle.slice(0, -1);
        var random1=cadenaShuffle.split(",").sort(function(){return 0.75-Math.random()});
        var random2=cadenaShuffle.split(",").sort(function(){return 0.5-Math.random()});
            columnasRandom(random1, "Izquierda");
            columnasRandom(random2, "Derecha");
    }
    //eventos mouse/touch
    document.getElementById("contenidoActividad").addEventListener("mousemove",{passive:false});
    document.getElementById("contenidoActividad").addEventListener("touchmove",{passive:false});
    $("#contenidoActividad")[0].onscroll = function () {
       $(this).scrollLeft(0);
    };
    $(".dragLine").draggable({revert:true, revertDuration:0, start:function(){
            if(inDrag){
                $("#contenidoActividad").on("mousemove, touchmove", function(event){
                    event.preventDefault();
                });
            }
        }, stop:function(){
            $("#contenidoActividad").off("mousemove, touchmove");
            $("#contenidoActividad").on("mousemove, touchmove");
        } 
    }).mousedown(function(event){
        var element;
        element = $(elementsFromPoint(event.pageX, event.pageY)).filter(".circuloLineas, .colDerecha, .colIzquierda");
        if(element.length>0){
            element = $(this).parents(".columnas");
            element.length > 0 ? iniciarTrazo(firstElement=element) : "";
        }
    }).mousemove(function(event){ inDrag ? trayectoriaTrazo(event) : ""; }).mouseup(function(event){ inDrag ? finTrazo(event) : ""; });
}

var posIn=[], posEnd=[], timeDrag, firstElement, inDrag=false;
function iniciarTrazo(element){
    inDrag = true;
    clearInterval(timeDrag);
    var circuloInicio, scrollTop;
    sndClick();
    $(".circuloLineas").css({transition:"none"});
    circuloInicio = $(element).find(".circuloLineas");
    scrollTop = parseInt($("#contenidoActividad").scrollTop());
    var offset = $(circuloInicio).offset();
    posIn[0]=parseInt(offset.left + 22);            posEnd[0] = posIn[0];
    posIn[1]=parseInt(offset.top + 22)+scrollTop;   posEnd[1] = posIn[1];
    if(evaluacion && lineasCorrectas.length>0){
        //elimina las lineas sobre las cuales se incia un nuevo trazo
        var indexLinea, relacionLinea;
        relacionLinea = circuloInicio.parent(".columnas").attr("relacion");
        indexLinea = relacionLinea !== undefined ? $("line[relacion='"+relacionLinea+"']").index() : -1;
        if(indexLinea>-1){
            lineasCorrectas.splice(indexLinea, 1);
            $("line[relacion='"+relacionLinea+"']").remove();
            $(".columnas[relacion='"+relacionLinea+"']").removeAttr("relacion").removeClass("correctAnswer");
        }
    }
    $(".onLineSelection").removeClass("onLineSelection");
    $(circuloInicio).addClass("onLineSelection");
    $("#svgLine line:last").attr({"x1": posIn[0], "y1": posIn[1], "x2": posIn[0], "y2": posIn[1]}).css({stroke:"transparent"});
    isFirst=true;
    timeDrag = setInterval(function(){
        scrollTop = parseInt($("#contenidoActividad").scrollTop());
        $("#svgLine line:last").attr({"x2":posEnd[0], "y2":posEnd[1]+scrollTop});
    }, 10);
}

var isFirst=false;
function trayectoriaTrazo(event){
    if(isFirst){
        isFirst = false;
        setTimeout(function(){
            $("#svgLine line:last").removeAttr("style");
        }, 10);
    }
    if(event.pageX!==undefined){
        posEnd[0]=parseInt(event.pageX); posEnd[1]=parseInt(event.pageY);
    }
}

var lineasCorrectas;
function finTrazo(event) {
    inDrag = false;
    sonIguales = false;
    clearInterval(timeDrag);
    var lastElement = $(elementsFromPoint(event.pageX, event.pageY)).filter(".circuloLineas, .colDerecha, .colIzquierda").parents(".columnas");
    var esRespuesta = $(lastElement).find(".colIzquierda, .colDerecha").length === 1 ? true : false;
    var t17 = "", t11 = "", correcta;
    var diferenteColumna=true;
    var relacion;
    if (esRespuesta && $(lastElement).parents("#contenedorDerecha").length === 1) {
        if ($(firstElement).parents("#contenedorIzquierda").length === 1) {
            relacion = $(firstElement).find(".colIzquierda").attr("t11") + "-" + $(lastElement).find(".colDerecha").attr("t17");
            t17 = $(lastElement).find("div[t17]").attr("t17");
            t11 = $(firstElement).find("div[t11]").attr("t11");
            !evaluacion ? correcta = evaluaRelaciona(t17, t11) :  "";
        }else{
            diferenteColumna=false;
        }
    } else if (esRespuesta && $(lastElement).parents("#contenedorIzquierda").length === 1) {
        if ($(firstElement).parents("#contenedorDerecha").length === 1) {
            relacion = $(lastElement).find(".colIzquierda").attr("t11")+ "-" + $(firstElement).find(".colDerecha").attr("t17");
            t17 = $(firstElement).find("div[t17]").attr("t17");
            t11 = $(lastElement).find("div[t11]").attr("t11");
            !evaluacion ? correcta = evaluaRelaciona(t17, t11) : "";
        }else{
            diferenteColumna=false;
        }
    }

    var ultimoCirculo = $(lastElement).find(".circuloLineas");
        if(ultimoCirculo.length > 0 && diferenteColumna){
            posEnd[0] = parseInt($(ultimoCirculo).offset().left + 22);
            posEnd[1] = parseInt($(ultimoCirculo).offset().top + 22)+ parseInt($("#contenidoActividad").scrollTop());
            if(evaluacion && lineasCorrectas.length>0){
                //elimina las lineas EXISTENTES sobre las cuales se FINALIZA UN TRAZO
                var indexLinea, relacionLinea;
                relacionLinea = ultimoCirculo.parent(".columnas").attr("relacion");
                indexLinea = relacionLinea !== undefined ? $("line[relacion='"+relacionLinea+"']").index() : -1;
                if(indexLinea>-1){
                    lineasCorrectas.splice(indexLinea, 1);
                    $("line[relacion='"+relacionLinea+"']").remove();
                    $(".columnas[relacion='"+relacionLinea+"']").removeAttr("relacion").removeClass("correctAnswer");
                }
            }
            $(".columnas[relacion='"+$(lastElement).attr("relacion")+"']").removeAttr("relacion");
            $(".columnas[relacion='"+$(firstElement).attr("relacion")+"']").removeAttr("relacion");
            $(firstElement).attr("relacion", relacion);
            $(lastElement).attr("relacion", relacion);
            $("#svgLine line:last").attr({"x2":posEnd[0], "y2":posEnd[1]}).attr("relacion", relacion);
            if (correcta || evaluacion) {
                $(lastElement).addClass("correctAnswer");
                $(firstElement).addClass("correctAnswer");
                $(".onLineSelection").parent(".columnas").addClass("correctAnswer");
                !evaluacion ? $(".correctAnswer").addClass("lock") : "";
                trazarLineas();
            } else{
                $(lastElement).addClass("badAnswer");
                $(".onLineSelection").parent(".columnas").addClass("badAnswer");
                $("#svgLine line:last").css({stroke: "#e1385a"});
                $(".columnas:not(.correctAnswer)").css("pointer-events","none");
                setTimeout(function () {
                    $("#svgLine line:last").css({"fill-opacity": "0", "stroke-opacity": "0"});
                    setTimeout(function(){
                        $(".badAnswer").removeClass("badAnswer");
                        $(".circuloLineas").css({transition: "background 0.35s, border 0.35s"});
                    },100);                        
                    setTimeout(function () {
                        $("line:last").removeAttr("x1 x2 y1 y2");
                        $(".columnas:not(.correctAnswer)").css("pointer-events","all");
                    }, 350);
                }, 350);
            }
        }else{
            $("line:last").removeAttr("x1 x2 y1 y2");
        }
    $(".onLineSelection").removeClass("onLineSelection");
    $(".correctAnswer").length === $(".circuloLineas").length && !evaluacion ? sigPregunta() : "";//si ya se respondieron las preguntas
    return false;
}

var contador, aciertosLineas;
function evaluaRelaciona(t17, t11){//evaluacion de la actividad
    var correcta=false; contador++;
    t17===t11 ? correcta=true : "";
    if(correcta && contador <= json[preguntaActual].respuestas.length){
        aciertosLineas++; aciertos++;
    }
    if(intentosRestantes > 0){
         intentosRestantes--;
         actualizarMarcador();
    }
    correcta ? sndCorrecto() : sndIncorrecto();
    return correcta;
}

function trazarLineas(){
    lineasCorrectas = [];
    var lineas="";
    var element;
    $("line[x1][x2][y1][y2]").each(function(){
        element = $(this);
        lineasCorrectas.push({x1:element.attr("x1"), y1:element.attr("y1"), x2:element.attr("x2"), y2:element.attr("y2")});
        lineas+="<line x1='"+element.attr("x1")+"' x2='"+element.attr("x2")+"' y1='"+element.attr("y1")+"' y2='"+element.attr("y2")+"' class='correctLine' relacion='"+element.attr("relacion")+"'></line>";
    });
    lineas+="<line></line>";
    $("#svgLine").html(lineas);
}

function columnasRandom(cadena, columna){//aplica random a las posiciones de los elementos html
    $.each(cadena, function(index, element){
        if(index+"" !== element+""){
            $("#contenedor"+columna+" .columnas:nth("+element+")").insertBefore($("#contenedor"+columna+" .columnas:nth("+index+")"));
        }
    });
}

// "iconos" bandera innecesaria