function iniciarActividad() {
    var cadena="";
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $.each(json[preguntaActual].contenedores, function (index, element) {//añade contenedores
        $("#contenedores").append('<table class="columna"><tr class="encabezado"><td>'+cambiarRutaImagen(element)+'</td></tr></table>');
    });
    $.each(json[preguntaActual].respuestas, function (index, element) {//añade respuestas
        cadena+=index+",";
        $("#respuestas").append('<div t17="'+element.t17correcta+'" class="respuestaDrag resph"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
        $(".columna").eq(parseInt(element.t17correcta)).append('<tr><td class="contenedor" t11='+parseInt(element.t17correcta)+'></td></tr>');
    });
    cartasRandom(cadena);
    $(".respuestaDrag p:has(img)").addClass("img").parents(".respuestaDrag").addClass("img");
    $("#contenidoActividad *").addClass("prevHIdden");
    coloresRandom(".respuestaDrag, .columna");//aplica colores
    $(".respuestaDrag:first").removeClass("resph");//muestra primer respuesta
    var maxWidth = parseInt(100/$(".columna").length);//define el ancho comun en columnas, contenedores y respuestas
    $(".columna").css({width:"calc("+maxWidth+"% - "+($(".columna").length*4)+"px)"});
    $(".respuestaDrag").css({width:($(".contenedor:first").width()-35)+"px"});
    setTimeout(function(){//espera a que se terminen de cargar todos los elementos y estilos
        var maxHeight = 0;//define el alto comun en contenedores y respuestas
        //aplica alto comun a las cabeceras
        $(".encabezado").each(function(index, element){
             maxHeight = maxHeight<element.scrollHeight ? element.scrollHeight : maxHeight;
        });
        $(".encabezado").css({height:maxHeight+"px", minHeight:maxHeight+"px", maxHeight:maxHeight+"px"});
        //aplica alto comun a contenedores
        maxHeight = 0;
        $(".respuestaDrag").each(function(index, element){
            maxHeight = maxHeight<element.scrollHeight ? element.scrollHeight : maxHeight;
        });
        var maxRows = 0, rows;
        $(".columna").each(function(index, element){
            rows = $(element).find(".contenedor").length;
            maxRows = maxRows < rows ? rows : maxRows;
        });
        maxRows++;
        if(maxRows>4){
            var heightLimit = ($("#contenidoActividad").height() - $(".encabezado:first").height() - 15 )/maxRows;
            maxHeight = parseInt(heightLimit - 3);
        }
        $("#respuestas").css("height", maxHeight+"px");
        $("#contenedores").css("height","calc(100% - "+(maxHeight+10)+"px)");
        $(".contenedor").css({height:maxHeight+"px", minHeight:maxHeight+"px", maxHeight:maxHeight+"px"});
        $(".respuestaDrag").css({height:maxHeight-10});
        $("#contenidoActividad *").removeClass("prevHIdden");
    }, 100);
}