//variables de actividad
var palabrasSopa=[];
var gridLength=0;
var maxGridLength=0;
var posIn=[], posEnd=[], dragInterval, palabrasEvaluadas=[];

//inicio de la actividad
function iniciarActividad(){
    gridLength = 11;
    cargarHtml();
    maxGridLength = gridLength+1;
    palabrasSopa = palabrasSopa.sort(function(a, b){//se reorganizan las palabras de mayor a menor
        if(a.length < b.length){
            return 1;
        }else if(b.length < a.length){
            return -1;
            return 0;
        }
    });
    cargarSopa();
    setTimeout(function(){//al terminar de crear la sopa de letras
        intentosRestantes = $(".palabra").length;
        totalPreguntas += intentosRestantes;
        //activa bandera t11ayudas
        if(json[preguntaActual].pregunta.t11ayudas === "si" || json[preguntaActual].pregunta.t11ayudas === true){
            $("#espacioSopa").css({width:"auto", position: "absolute", left:"calc(50% - "+$("#espacioSopa").width()/2+"px)"});
        }else{
            $("#espacioPalabras").removeClass("hidden");
        }
        //se calcula el tamaño de las celdas
        gridLength > 16 ? $("td").css({fontSize:"105%", border:"0"}) : "";
        var tamañoCelda = $("td:first").height();
        $("td").css({width:""+tamañoCelda+"px"});
        
         $("table").removeClass("hidden");
         $("td").removeClass("load");
        setTimeout(function(){
            $("td").on("mousedown touchstart", iniciarTrazo);
            $("table").on("mousemove touchmove", continuarTrazo);
            $("*").on("mouseup touchend", finTrazo);
            $("table").css("background","#afdde8");
        }, 350);
    }, 200);
}

function cargarHtml(){
    palabrasSopa=[];
    $("#contenidoActividad").html("<div id='espacioSopa'></div><div id='espacioPalabras'  class='hidden'></div>");
    //se obtine el tamaño de la palabra mas grande
    $.each(json[preguntaActual].respuestas, function(index, element){
        var elem = element.t13respuesta;
        elem = cambiarRutaImagen(elem);
        elem = obtieneTextoPrentesis(elem);
        if(elem.length < 17){
            $("#espacioPalabras").append("<div class='palabra' word='"+elem.toUpperCase()+"'>"+cambiarRutaImagen(element.t13respuesta)+"</div>");//se agregan palabras de ayuda
            gridLength = gridLength < elem.length ? elem.length : gridLength;//se define el tamaño maximo del grid
            palabrasSopa.push(elem.toUpperCase());
        }
    });
}
function cargarSopa(){
    $("#espacioSopa").append("<table class='hidden'></table>");//se añade el grid
    creaGrid("tr", gridLength);
    creaGrid("td", gridLength);
    //se acomodan las palabras en el grid
    $.each(palabrasSopa, function(index, element){
        var contador = 0;//intentos para acomodar la palabra
        var acomodado = false;
        while(contador <= 50 && !acomodado){//maximo 20 intentos para acomodar una palabra
            acomodado = acomodarPalabra(element);
            contador ++;
        }
        if(!acomodado){//si no se logra acomodar una palabra
            if(gridLength<=maxGridLength){//se extiende el tamaño de la sopa
                gridLength++;
                cargarHtml();
                cargarSopa();
            }else{//al no poder colocar todas las palabras
                console.log("No se lograron acomodar todas las palabras");
            }
        }
    });
    //se llenan las celdas vacias
    var letraRandom;
    $("td").each(function(index, element){
        $(element).attr("coord", $(element).parent("tr").index()+"-"+$(element).index());
        if($(element).text()===""){
            letraRandom = letrasArr[parseInt(Math.random()*letrasArr.length)];
            $(element).text(letraRandom);
        }
        
    });
}

function obtieneTextoPrentesis(texto){//si se usan oraciones que contienen la palabra a buscar een parentesis
    if(texto.indexOf("(")!==-1 && texto.indexOf(")")!==-1){
        texto = texto.substring(texto.indexOf("(")+1, texto.indexOf(")"));
    }
    return texto;
}
function creaGrid(element, size){//inserta elementos de tabla
    var selector = element === "tr" ? "table" : "table tr";
    element = element === "tr" ? "<tr></tr>" : "<td class='load'></td>";
    for(var i=0; i<size; i++){
        $("#espacioSopa "+selector).append(element);
    }
}
function acomodarPalabra(palabra){//acomoda las palabras emn el grid
    var colocado=false;
    //se obtine una direccion para acomodar la palabra de manera aleatoria
    var direccion = parseInt(Math.random()*4);
    //se determinan los rangos de coordenadas para acomodar las palabras
    var range1 = palabra.length - gridLength;
    range1 = range1 < 0 ? 0 : range1;
    var range2;
        direccion === 0 ? range2 = gridLength : "";
        direccion === 1 ? range2 = gridLength : "";
        direccion === 2 || direccion === 3 ? range2 = range1 : "";
    //determina las coordenadas para iniciar a  colocar la palabra
    var  cellStartX;
    var  cellStartY;
    if(direccion === 0){//coordenadas palabra en horizontal
        cellStartX = parseInt(Math.random()*range2);
        cellStartY = range1 > 1 ? parseInt(Math.random()*range1) : range1;
    }else if(direccion === 1){//coordenadas palabra en vertical
        cellStartY = parseInt(Math.random()*range2);
        cellStartX = range1 > 1 ? parseInt(Math.random()*range1) : range1;
    }else{
        cellStartX = range1 > 1 ? parseInt(Math.random()*range1) : range1;
        cellStartY = range1 > 1 ? parseInt(Math.random()*range1) : range1;
    }
    var start = [cellStartX, cellStartY];
    var cell, content;
    
    //si la direccion es diagonal izquierda, se recorren las coordenadas
    direccion === 2 ? start[0] = start[0]+palabra.length-1 : "";
    
    //se inicia el acomodo de la palabra
    if(direccion === 0 || direccion === 1){//0 : horizontal    1 : vertical
        //se determina la siguiente celda
        $.each(palabra.split(""), function(index, element){
            if(direccion === 1){
                cell = $("tr:nth("+start[1]+")>td:nth("+(start[0]+index)+")");
            }else{
                cell = $("tr:nth("+(start[1]+index)+")>td:nth("+start[0]+")");
            }
            //se evalua si se puede acomodar la letra actual
            content=$(cell).text();
            content === "" ? $(cell).addClass("colocando") : "";
            if(content === "" || content === element){
                $(cell).text(element);
                if(index === palabra.length-1){//al colocar la ultima letra
                    colocado = true;
                    $(".colocando").removeClass("colocando");
                }
            }else{
                $(".colocando").text("").removeClass("colocando");
                return false;
            }
        });
    }else{//2 :diagonalIzquierda   3 : diagonaDerecha
        var incremento = 0;
        $.each(palabra.split(""), function(index, element){
           cell = $("tr:nth("+(start[1]+index)+")>td:nth("+(start[0]+incremento)+")");
           content=$(cell).text();
           content === "" ? $(cell).addClass("colocando") : "";
            if((content === "" || content === element) && cell[0] !== undefined){
                $(cell).text(element);
                if(index === palabra.length-1){//al colocar la ultima letra
                    colocado = true;
                    $(".colocando").removeClass("colocando");
                }
            }else{
                $(".colocando").text("").removeClass("colocando");
                return false;
            }
           direccion === 2 ? incremento-- : incremento++;
           
        });
    }
    return colocado;
}

//eventos drag an drop
function iniciarTrazo(){//se inicia la seleccion de la palabra
    clearInterval(dragInterval);
    posIn[0]=event.pageX; posIn[1]=event.pageY;
    posEnd[0]=event.pageX; posEnd[1]=event.pageY;
    var element = document.elementFromPoint(event.pageX, event.pageY);
    $(element).addClass("onSelection");
    var cell=($(element).attr("coord")+"").split("-");
    firstPosition = [cell[1] * 1, cell[0] * 1];//X, Y
    lastPosition = [cell[1] * 1, cell[0] * 1];//X, Y
    
    dragInterval = setInterval(function(){
        var element = document.elementFromPoint(posEnd[0], posEnd[1]);
        if($(element).is("td")){
            var cell=($(element).attr("coord")+"").split("-");
            if(lastPosition[0]!==cell[1]*1 || lastPosition[1]!==cell[0]*1){
                lastPosition=[cell[1]*1, cell[0]*1];//X, Y
                calcularTrayectoria();
            }
        }
    }, 50);
}
function continuarTrazo(){//se actualizan coordenadas
    posEnd[0]=event.pageX; posEnd[1]=event.pageY;
}
function finTrazo(){//se finaliza la seleccion de la palabra
    clearInterval(dragInterval);
    posEnd[0]=event.pageX; posEnd[1]=event.pageY;
    var element = document.elementFromPoint(posEnd[0], posEnd[1]);
    if($(element).is("td")){
        var cell=($(element).attr("coord")+"").split("-");
            if(lastPosition[0]!==cell[1]*1 || lastPosition[1]!==cell[0]*1){
                lastPosition=[cell[1]*1, cell[0]*1];//X, Y
                calcularTrayectoria();
            }        
    }
    //se obtiene la palabra seleccionada
    var elements = $(".onSelection");
    if(elements.length > 2){
        var palabraSeleccionada="";
        $.each(elements, function(i, e){
            palabraSeleccionada += $(e).text();
        });
        
        //se evalua si es correcta
        var esCorrecta=false;
        $.each(palabrasSopa, function(i, e){
            if(palabraSeleccionada === e){
                esCorrecta = true;
                return false;
            }
        });
        
        if(esCorrecta){
            //se valida si ya fue resuelta
            var resuelta = false;
            if(palabrasEvaluadas.length > 0){
                $.each(palabrasEvaluadas, function(i, e){
                    if(palabraSeleccionada === e){
                        resuelta = true;
                        return false;
                    }
                });
            }
            //eventos de la evaluacion
            if(!resuelta){
                sndCorrecto();
                palabrasEvaluadas.push(palabraSeleccionada);
                $(".onSelection").addClass("cellSolved");
                $(".palabra[word='"+palabraSeleccionada+"']").addClass("wordSolved");
                intentosRestantes > 0 ? aciertos++ : "";
                palabrasEvaluadas.length === json[preguntaActual].respuestas.length ? sigPregunta() : "";
            }
        }else{//es incorrecta
            sndIncorrecto();
        }
        //se reducen los intentos disponibles
        if(intentosRestantes > 0){
             intentosRestantes--;
             actualizarMarcador();
        }         
    }
    $(".onSelection").removeClass("onSelection");
}


//define la direccion del trazo
var firstPosition=[], lastPosition=[], contadorSelected=0;
function calcularTrayectoria(){
    contadorSelected=0;
    var x1=firstPosition[0], x2=lastPosition[0], y1=firstPosition[1], y2=lastPosition[1];
    var despX = x2 < x1-1 || x2 > x1+1 ? true : false;
    var despY = y2 < y1-1 || y2 > y1+1 ? true : false;
    if(despX && despY){//trayectoria diagonal
        var i, j, final, coords=[];
        var dx = posIn[0] < posEnd[0] ? posEnd[0] - posIn[0] :  posIn[0] - posEnd[0];//delta x
        var dy = posIn[1] < posEnd[1] ? posEnd[1] - posIn[1] :  posIn[1] - posEnd[1];//delta y
        i = dx > dy ? x1 : y1;//se define la diagonal para tomar a x o y como base para trazar la linea
        j = dx > dy ? y1 : x1;
        final = dx > dy ? x2 : y2;
        while(i !== final){//se itera para obtener todas las coordenadas de la linea trazada
            if(dx > dy){
                coords.push(j+"-"+i);
                j = y1 < y2 ? j+1 :  j-1;
                i = x1 < x2 ? i+1 :  i-1;
            }else{
                coords.push(i+"-"+j);
                j = x1 < x2 ? j+1 :  j-1;
                i = y1 < y2 ? i+1 :  i-1;
            }
        }
        dx > dy ? coords.push(j+"-"+i) : coords.push(i+"-"+j);
        pintaTrayectoria(coords);
        
    }else if(despY && !despX){//trayectoria recta en y
        var i=y1, coords=[];
        while(i!==y2){
            coords.push(i+"-"+x1);
            i = y1 < y2 ? i+1 :  i-1;
        }
        coords.push(i+"-"+x1);
        pintaTrayectoria(coords);
        
    }else if(despX && !despY){//trayectoria recta en X
        var i=x1, coords=[];
        while(i!==x2){
            coords.push(y1+"-"+i);
            i = x1 < x2 ? i+1 :  i-1;
        }
        coords.push(y1+"-"+i);
        pintaTrayectoria(coords);
    }else{
        var coords=[];
        coords.push(y1+"-"+x1);
        coords.push(y2+"-"+x2);
        pintaTrayectoria(coords);
    }
    
    function pintaTrayectoria(coords){
        $(".onSelection").removeClass("onSelection");
        $.each(coords, function(index, element){
            $("td[coord='"+element+"']").addClass("onSelection");
            contadorSelected = $("td[coord='"+element+"']").hasClass("wordSelected") ? contadorSelected+1: contadorSelected;
        });
    }
}