function iniciarActividad(){
    $("#contenidoActividad").html("<div id='hideTrigger'></div><div id='padding-top'></div><div id='droppedBox' class='droppedBoxHidden'></div>");
    var pregunta = json[preguntaActual].pregunta.t11pregunta !== undefined;
    if(pregunta){
        $("<div id='preguntaActividad'>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div>").insertAfter($("#padding-top"));
    }
    $.each(json[preguntaActual].preguntas, function(index, element){
        $("#contenidoActividad").append(cambiarRutaImagen(element.t11pregunta));
        if(index < json[preguntaActual].preguntas.length-1){//añade las listas desplegables
            $("#contenidoActividad").append("<div class='dropList' t17='"+json[preguntaActual].respuestas[index].t17correcta+"'><div class='arrow'></div></div>");
            $.each(json[preguntaActual].respuestas[index].t13respuesta, function(i, e){
                $(".dropList:last").append("<p class='hidden'>"+e+"</p>");//añade las respuestas
            });
        }
    });
    
    intentosRestantes = $(".dropList").length;
    totalPreguntas += intentosRestantes;
    //eventos click/touch
    $(".dropList").on("click touchend", function(){
        var element = $(this);
        if(element.find(".arrow").hasClass("displayed")){
            hideList();
        }else{
            $(".arrow").removeClass("displayed");
            //se muestra la lista desplegable
            if(!$("#droppedBox").hasClass("droppedBoxHidden")){
                hideList();
                setTimeout(function(){
                    addListElements(element);
                }, 210);
            }else{
                addListElements(element);
            }
            $("#droppedBox .hidden").removeClass("hidden");
        }
    });
    $("#hideTrigger").on("click touchend", hideList);
}

function addListElements(element) {
    sndClick();
    element.find(".arrow").addClass("displayed");
    $("#droppedBox").html(element.find("p").clone()).attr("t17", element.attr("t17"));
    $("#droppedBox p").addClass("respuesta").removeClass("hidden");
    $("#droppedBox").css({top: element[0].offsetTop+30, left: element[0].offsetLeft+7}).removeClass("droppedBoxHidden");
    //se evalua la respuesta seleccionada
    $(".respuesta").on("click touchend", function(){
        if(($("#droppedBox").attr("t17") === $(this).index()+"") || evaluacion){
            hideList(); var hidden = false;
            if(!evaluacion){
                sndCorrecto();
                intentosRestantes > 0 ? aciertos++ : "";
                element.addClass("correctAnswer lock");
            }else{
                element.find(".selected").removeClass("selected");
                if($(this).hasClass("selected")){
                    hidden = true;
                    element.removeClass("selected");
                }else{
                    element.addClass("selected");
                    element.find("p:nth("+$(this).index()+")").addClass("selected");
                }
            }
            $(this).index() === 0 ? "" : element.find("p:nth("+$(this).index()+")").insertBefore(element.find("p:first"))
            setTimeout(function(){
                hidden ? element.find("p:first").addClass("hidden") : element.find("p:first").removeClass("hidden");
                !evaluacion && $(".correctAnswer").length === $(".dropList").length ? setTimeout(sigPregunta, 500) : "";
            }, 10);
        }else{
            sndIncorrecto();
            element.find("p:nth("+$(this).index()+")").addClass("badAnswer lock");
            $(this).addClass("badAnswer lock");
        }
        
        intentosRestantes > 0 ? (intentosRestantes--, actualizarMarcador()) : "";
    });
}

function hideList(){
    if(!$("#droppedBox").hasClass("droppedBoxHidden")){
        $(".arrow").removeClass("displayed");
        $("#droppedBox").addClass("droppedBoxHidden");
    }
}