var buttonInterval;
var evaluo;
function iniciarActividad(){
    totalPreguntas++; intentosRestantes=1; evaluo=false;
    $("#contenidoActividad").html("<div id='fondoPregunta'></div>");
    $("#contenidoActividad").append("<div id='verdadero' class='button' t17='"+json[preguntaActual].respuestas[0].t17correcta+"'><p>"+cambiarRutaImagen(json[preguntaActual].respuestas[0].t13respuesta)+"</p></div>\n\
        <div id='falso' class='button' t17='"+json[preguntaActual].respuestas[1].t17correcta+"'><p>"+cambiarRutaImagen(json[preguntaActual].respuestas[1].t13respuesta)+"</p></div>");
    $("#contenidoActividad").append("<div id='preguntaActividad'>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div>");
    $("#contenidoActividad").append("<div id='msgError' class='hidden'><img src='img/fv_intentalo.png'><p></p></div>");
    $("#falso").append("<img src='img/opcion_multiple_tache.png'>");
    $("#verdadero").append("<img src='img/opcion_multiple_palomita.png'>");

    //activa banderas text/forma
    if(json[preguntaActual].pregunta.forma === "cuadrado"){
        $(".button").addClass("custom");
        $("#falso, #verdadero").find("img").remove();
    }else if(json[preguntaActual].pregunta.texto === true || json[preguntaActual].pregunta.icono === false){
        $(".button").addClass("text");
        $("#falso, #verdadero").find("img").remove();
    }
    
    //determina la posicion vertical de la pregunta
    setTimeout(function(){
        $("#preguntaActividad").css({top:"calc(50% - "+$("#preguntaActividad").height()/2+"px)", left:"0px"});
    }, 50);

    $(".button").on("click touchend", function(){
        var element = this;
        $(element).addClass("lock").css({transition:"0.25s", opacity:"0", visibility:"hidden", transform:"scale(1.25, 1.25)"});
        if($(element).attr("t17") === "1"){
            sndCorrecto();
            intentosRestantes > 0 ? aciertos ++ : "";
            setTimeout(function(){
                !esUltima ? $("#contenidoActividad *").css({transition: "0.5s", opacity: "0"}) : "";
                if(esUltima){
                    setTimeout(function(){
                        $(element).css({transition:"none", opacity:"0", visibility:"hidden", transform:"scale(0.1, 0.1)"});
                        setTimeout(function(){
                            $(element).css({transition:"0.25s", opacity:"1", visibility:"visible", transform:"scale(1, 1)"});
                        }, 50);
                    }, 350);                
                }
                setTimeout(sigPregunta, 300);
            },350);
        }else{
            sndIncorrecto();
            //muestra mensaje de error
            $("#msgError").removeClass("hidden");
            hideNavBar();
            var timeout = setTimeout(function(){
                $("#msgError").addClass("hidden");
            }, 1500);
            $("#msgError").on("click touchend", function(){
                clearTimeout(timeout);
                $("#msgError").addClass("hidden");
            });
            //animaciones del boton
            setTimeout(function(){
                $(element).css({transition:"none", opacity:"0", visibility:"hidden", transform:"scale(0.1, 0.1)"});
                setTimeout(function(){
                    $(element).addClass("off").css({transition:"0.25s", opacity:"0.75", visibility:"visible", transform:"scale(1, 1)"});
                }, 50);
            }, 350);
        }
       //evaluacion de la actividad
       if(!evaluo){   
        intentosRestantes=0;
        actualizarMarcador();
        evaluo = true;
       }
    });
}