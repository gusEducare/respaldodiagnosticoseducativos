function iniciarActividad(){
    var cadena = "";
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $("#contenedores").append('<table class="columna"><tr class="encabezado"><td class="blank"></td></tr></table>');
    $.each(json[preguntaActual].contenedores, function (index, element) {//añade columnas
        $(".encabezado").append('<td class="textos">'+cambiarRutaImagen(element)+'</td>');
    });
    var maxTableHeight = getMaxSizeHeader();
    $.each(json[preguntaActual].contenedoresFilas,function(index, element){//añade filas
        $(".columna").append('<tr><td class="textoFilas textos">'+cambiarRutaImagen(element)+'</td></tr>');
        while($("tr:last .contenedor").length < $("tr:first > .textos").length){//se añaden contenedores
            $("tr:last").append('<td class="contenedor" t11="'+$("tr:last .contenedor").length+","+($("tr:last").index()-1)+'"></td>');
        }
    });        
    var t17="";
    $.each(json[preguntaActual].respuestas, function (index, element) {//se añaden respuestas
        cadena += index+",";
        t17 = element.t17correcta;
        if(Object.prototype.toString.call(element.t17correcta)==="[object Array]"){
            t17 = t17.join("-");
        }
        $("#respuestas").append('<div t17="'+t17+'" class="respuestaDrag resph"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
    });
    var maxContainer = json[preguntaActual].contenedoresFilas.length;
    var espacioDisponible = $("#contenedores").height() - maxTableHeight - (maxContainer*6);
    var anchoContenedor = $(".columna").length > 1 ? 200 : 350;
    $(".encabezado").height(maxTableHeight-8);
    var altoContenedores = (espacioDisponible / maxContainer);
    altoContenedores = altoContenedores > 100 ? 100 : altoContenedores;
    coloresRandom(".respuestaDrag");
    var indexRespuesta = "";
    t17 = "";
    $(".respuestaDrag").each(function(index, element){
        indexRespuesta = "";
        anchoContenedor = element.scrollHeight > anchoContenedor? element.scrollHeight : anchoContenedor;
        anchoContenedor = anchoContenedor > 300 ? 300 : anchoContenedor;//se determina el ancho comun
        t17 = $(element).attr("t17").split("-");
        $.each(t17, function(index, element){//se obtiene t17
            indexRespuesta += $(".contenedor").index($(".contenedor[t11='"+element+"']"))+",";
        });
        indexRespuesta = indexRespuesta.slice(0, -1);
        $(element).attr("t17", indexRespuesta);//se determina t17
    });
    $(".contenedor").each(function(index, element){
        $(element).attr("t11", index);//se determina t11
    });
    //se igualan las dimenciones de las respuesta y contenedores
    $(".contenedor, .respuestaDrag").css({height:altoContenedores, width:anchoContenedor+"px"});
    $(".contenedor, .respuestaDrag").css({width:$(".contenedor").width()});
    cartasRandom(cadena);
    apilaElementos();
}

function getMaxSizeHeader(){
    var sizeTable = [];
    $("table").each(function(){
        sizeTable.push($(this).height());
    });
    var max = Math.max.apply(null, sizeTable);
    return  max;
}

function apilaElementos(){
    $(".respuestaDrag").addClass("resph");
    $(".respuestaDrag:first").removeClass("resph");
}