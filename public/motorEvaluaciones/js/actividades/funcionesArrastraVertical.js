function iniciarActividad() {
    var cadena="";
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $.each(json[preguntaActual].contenedores, function (index, element) {
        $("#contenedores").append('<table class="columna"><tr class="encabezado"><td>'+element+'</td></tr></table>');
    });
    var totalTableHeight = getTotalSizeHeader();
    var espacioDisponible = $("#contenidoActividad").height() - ((totalTableHeight) + (6 * $("table").length));
    var altoContenedores = (espacioDisponible / json[preguntaActual].respuestas.length) - 6;
    var espacioRespuestas = ($("#contenidoActividad").height()-(altoContenedores * json[preguntaActual].respuestas.length))/json[preguntaActual].respuestas.length;
    $.each(json[preguntaActual].respuestas, function (index, element) {
        cadena+=index+",";
        $("#respuestas").append('<div t17="'+element.t17correcta+(evaluacion? "="+index : "")+'" class="respuestaDrag"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
        $(".columna").eq(parseInt(element.t17correcta)).append('<tr><td  class="contenedor" t11='+parseInt(element.t17correcta)+'></td></tr>');
    });
    $(".respuestaDrag").css({"marginBottom": espacioRespuestas});
    $(".contenedor, .respuestaDrag").height(altoContenedores);

    coloresRandom(".respuestaDrag");
    cartasRandom(cadena);
}

function getTotalSizeHeader(){
    var sizeHeader = 0;
    $("table").each(function(){
        sizeHeader = sizeHeader + $(this).height();
    });

    return  sizeHeader;
}