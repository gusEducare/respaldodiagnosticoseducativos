var palabrasSopa=[];
var posIn=[], posEnd=[], dragInterval, palabrasEvaluadas=[];
var maxLength, maxHeight, gridLimit;
var extendido = false;
var palabrasRestantes=[];
function iniciarActividad(){
    maxLength=11; maxHeight=11; gridLimit=11;//resetea valores iniciales
    cargarHtml();
    maxLength++;
    gridLimit = maxLength + 8;
    gridLimit > 24 ? gridLimit=24 : "";
    palabrasEvaluadas=[];
    palabrasSopa = palabrasSopa.sort(function(a, b){//se reorganizan las palabras de mayor a menor
        if(a.length < b.length){
            return 1;
        }else if(b.length < a.length){
            return -1;
            return 0;
        }
    });
    var restaurar = evaluacion && objCadenas !== undefined && objCadenas[preguntaActual] !== undefined && objCadenas[preguntaActual] !== null;
    ////////////////////////
    if(!restaurar){//si es actividad o la primer entrada en evaluacion
        cargarSopa();
        maxIntentos=0;
        while(palabrasRestantes.length>0 && maxIntentos<=50){
            $.each(palabrasRestantes, function(index, element){
                 var contador = 0;//intentos para acomodar la palabra
                 var acomodado = false;
                 while(contador <= 5 && !acomodado){//maximo 20 intentos para acomodar una palabra
                    acomodado = acomodarPalabra(element, -1);
                    if(element.length<13){
                        acomodado = !acomodado ?  acomodarPalabra(element, 2) : acomodado;
                        acomodado = !acomodado ?  acomodarPalabra(element, 3) : acomodado;
                    }
                    acomodado = !acomodado ?  acomodarPalabra(element, 0) : acomodado;
                    acomodado = !acomodado ?  acomodarPalabra(element, 1) : acomodado;
                    contador ++;
                 }
                 if(acomodado){
                     maxIntentos=0;
                     palabrasRestantes.splice(index, 1);
                     return false;
                 }else if(!acomodado && maxLength<=gridLimit){//se extiende el tamaño de la sopa maximo 4 celdas mas a la palabra mas larga
                    maxLength++;
                    $("tr").append("<td class='load'></td>");
                }             
            });
            maxIntentos++;
        }
        palabrasRestantes.length>0 ? function(){alert("No se acomodaron todas las palabras"); location.reload();} : "";
        //se llenan las celdas vacias
        var letraRandom;
        $("td").each(function(index, element){
            $(element).attr("coord", $(element).parent("tr").index()+"-"+$(element).index());
            if(!$(element).hasClass("colocado")){
                letraRandom = letrasArr[parseInt(Math.random()*letrasArr.length)];
                $(element).text(letraRandom);
            }
        });
        //define variables de evalaucion
        intentosRestantes = $(".palabra").length;
        totalPreguntas += intentosRestantes;
    }else{
        var dimensionesGrid = objCadenas[preguntaActual].split("[CS]")[0];
        var letrasSopa = objCadenas[preguntaActual].split("[CS]")[1].split("[RU]")[0];
        var respuestasUsuario = objCadenas[preguntaActual].split("[RU]")[1];
        //envia las dimensiones de la sop
        maxHeight = dimensionesGrid.split("-")[0];
        maxLength = dimensionesGrid.split("-")[1];
        $("#espacioSopa").append("<table></table>");//se añade el grid
        creaGrid("tr", maxHeight);
        creaGrid("td", maxLength);
//        inserta las letras en la sopa
        $("td").each(function(index, element){
            $(element).attr("coord", $(element).parent("tr").index()+"-"+$(element).index());
            $(element).text(letrasSopa.charAt(index));
        });
//        restaura las respuestas del usuario
        if(respuestasUsuario!==""){
            respuestasUsuario = respuestasUsuario.split("=");
            var palabraSeleccionada;
            $.each(respuestasUsuario, function(index, element){
                palabraSeleccionada = "";
                $.each(element.split(","), function(i, e){//forma las palabras elegidas
                    palabraSeleccionada += $("td[coord='"+e+"']").addClass("cellSolved").text();
                });
                $("#resueltas").append("<div class='palabraResuelta' coords='"+element+"'><div class='close'></div>"+palabraSeleccionada+"</div>");
                eventosPalabraResuelta();
            });
        }
    }
    ///////////////////////terminan estructuras de evaluacion
    //
    //activa la sopa de letras extendida
    if(maxLength>12 || evaluacion){
        $("#espacioSopa, #espacioPalabras").addClass("Extendido");
        $("#espacioPalabras").insertBefore($("#espacioSopa"));
        extendido = true;
    }
    //habilita bandera t11ayudas
    if(json[preguntaActual].pregunta.t11ayudas === "si" || json[preguntaActual].pregunta.t11ayudas === true){
        extendido = true;
        $("#espacioPalabras").hide();
        $("#espacioSopa").css({height:"calc(100% - 10px)", width:"calc(100% - 10px)", maxWidth:"calc(100% - 10px)", margin:"5px"});
        $("table").css("max-width","100%");
        $("td").width($("td:first")[0].scrollHeight);
    }
    setTimeout(function(){//al terminar de crear la sopa de letras
        //inicia el juego
        $("table").removeClass("hidden");
        $("td").removeClass("load");
        setTimeout(function(){
            ////si la sopa se extiende, determina el tamaño de cada elemento de la caja de palabras
            if(extendido){
                var maxWidth = 0;
                var maxElement = 0;
                $.each($(".palabra"), function(index, element){
                    maxWidth = $(element)[0].scrollWidth + 10 > maxWidth ? $(element)[0].scrollWidth + 10 : maxWidth;
                });
                maxWidth = parseInt( $("#espacioPalabras").width() / maxWidth);
                maxWidth = maxWidth<1 ? 1 : maxWidth;
                maxWidth = $("#espacioPalabras").width() / maxWidth;
                $(".palabra").css("width", maxWidth+"px");
                var scrollHeight = $("#espacioPalabras")[0].scrollHeight;
                $.each($(".palabra"), function(index, element){
                    element.offsetTop  < 40 ? maxElement++ : "";
                });
                if(scrollHeight > 58){
                    //se añaden los botones que permiten ocultar o mostrar palabras
                    $("<div id='lastWord' class='button'></div>").insertBefore($("#espacioPalabras"));
                    $("<div id='nextWord' class='button'></div>").insertAfter($("#espacioPalabras"));
                    //se ocultan o muestran palabras
                    $(".wordHidden").length === 0 ? $("#lastWord").addClass("hidden") : "";
                    $("#nextWord").on("click touchend", function(event){
                        animacionBoton(this);
                        $("#lastWord").removeClass("hidden");
                        if($(".palabra:not(.wordHidden)").length > maxElement){
                            $(".palabra:not(.wordHidden):first").css({width:"0px", margin:"0px", transition:"width 0.35s, margin 0.35s"}).addClass("wordHidden");
                            $(".palabra:not(.wordHidden)").length === maxElement ? $("#nextWord").addClass("hidden") : "";
                        }
                    });
                    $("#lastWord").on("click touchend", function(event){
                        animacionBoton(this);
                        $("#nextWord").removeClass("hidden");
                        $(".wordHidden:last").css({width:maxWidth+"px", marginLeft:"5px", marginRight:"5px",transition:"width 0.35s, margin 0.35s"}).removeClass("wordHidden");
                        $(".wordHidden").length === 0 ? $("#lastWord").addClass("hidden") : "";
                    });
                }
            }else{
                $(".palabra").css("width","100%");
            }
            if(json[preguntaActual].pregunta.t11ayudas !== "si" && json[preguntaActual].pregunta.t11ayudas !== true){
                $("#espacioPalabras").removeClass("hidden");
            }        
            $("td").on("mousedown touchstart", function(event){iniciarTrazo(this, event);});
            $("table").on("mousemove touchmove", function(event){ inDrag ? continuarTrazo(event) : ""; });
            $("*").on("mouseup touchend", function(event){ inDrag ? finTrazo(event) : ""; });
            $("table").css("background","#afdde8");
        }, 350);
    }, 10);
}
function cargarHtml(){
    palabrasSopa=[]; var insertaPregunta = "";
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        insertaPregunta = "<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>";
    }
    $("#contenidoActividad").html("<div id='espacioSopa'>"+insertaPregunta+"</div><div id='espacioPalabras' class='hidden'></div>");
    evaluacion ? $("#contenidoActividad").append("<div id='resueltas'></div>") : "";
    //se obtine el tamaño de la palabra mas grande
    $.each(json[preguntaActual].respuestas, function(index, element){
        var elem = element.t13respuesta;
        elem = cambiarRutaImagen(elem);
        elem = obtieneTextoPrentesis(elem);
        if(elem.length < 17){
            maxLength = maxLength < elem.length ? elem.length : maxLength;//se define el tamaño maximo del grid
            json[preguntaActual].pregunta.acentos===false ? elem = eliminarAcentos(elem) : "";
            palabrasSopa.push(elem.toUpperCase());
            $("#espacioPalabras").append("<div class='palabra' word='"+elem.toUpperCase()+"'>"+cambiarRutaImagen(element.t13respuesta)+"</div>");//se agregan palabras de ayuda
        }
    });
    maxLength > 16 ? maxLength = 16 : "";
    if(evaluacion){
        $(".palabra").on("click touchend", function(){
            $(this).hasClass("mark") ? $(this).removeClass("mark") : $(this).addClass("mark");
        });
    }
}
function creaGrid(element, size){//inserta elementos de tabla
    var selector = element === "tr" ? "table" : "table tr";
    element = element === "tr" ? "<tr></tr>" : "<td class='load'></td>";
    for(var i=0; i<size; i++){
        $("#espacioSopa "+selector).append(element);
    }
}
function cargarSopa(){
    $("#espacioSopa").append("<table></table>");//se añade el grid
    creaGrid("tr", maxHeight);
    creaGrid("td", maxLength);
    //se acomodan las palabras en el grid
    $.each(palabrasSopa, function(index, element){
        var contador = 0;//intentos para acomodar la palabra
        var acomodado = false;
        while(contador <= 5 && !acomodado){//maximo 20 intentos para acomodar una palabra
            acomodado = acomodarPalabra(element, -1);//primero intenta con una direccion aleatoria
            //despues intenta con direcciones preestablecidas
            if(element.length<13){
                acomodado = !acomodado ?  acomodarPalabra(element, 2) : acomodado;
                acomodado = !acomodado ?  acomodarPalabra(element, 3) : acomodado;
            }
            acomodado = !acomodado ?  acomodarPalabra(element, 0) : acomodado;
            acomodado = !acomodado ?  acomodarPalabra(element, 1) : acomodado;
            contador ++;
        }
        if(!acomodado){//si no se logra acomodar una palabra
            palabrasRestantes.push(element);
        }
    });
}
function acomodarPalabra(palabra, direccion){//acomoda las palabras emn el grid
    var colocado=false;
    //se obtine una direccion para acomodar la palabra de manera aleatoria
    if(direccion===-1){
        direccion = palabra.length>11 ? 1 : parseInt(Math.random()*4);
    }
    //se determinan los rangos de coordenadas para acomodar las palabras
    var range1 = palabra.length > 11 ? maxLength - palabra.length : maxHeight-palabra.length;
    range1 = range1 < 0 ? 0 : range1;
    var range2;
        direccion === 0 ? range2 = maxLength : "";
        direccion === 1 ? range2 = maxHeight : "";
        direccion === 2 || direccion === 3 ? range2 = range1 : "";
    //determina las coordenadas para iniciar a  colocar la palabra
    var  cellStartX;
    var  cellStartY;
    if(direccion === 0){//coordenadas palabra en horizontal
        cellStartX = parseInt(Math.random()*range2);
        cellStartY = range1 > 1 ? parseInt(Math.random()*range1) : range1;
    }else if(direccion === 1){//coordenadas palabra en vertical
        cellStartY = parseInt(Math.random()*range2);
        cellStartX = range1 > 1 ? parseInt(Math.random()*range1) : range1;
    }else{
        cellStartX = range1 > 1 ? parseInt(Math.random()*range1) : range1;
        cellStartY = range1 > 1 ? parseInt(Math.random()*range1) : range1;
    }
    var start = [cellStartX, cellStartY];
    var cell, content;
    //si la direccion es diagonal izquierda, se recorren las coordenadas
    direccion === 2 ? start[0] = start[0]+palabra.length-1 : "";
    //se inicia el acomodo de la palabra
    if(direccion === 0 || direccion === 1){//0 : horizontal    1 : vertical
        //se determina la siguiente celda
        $.each(palabra.split(""), function(index, element){
            if(direccion === 1){
                cell = $("tr:nth("+start[1]+")>td:nth("+(start[0]+index)+")");
            }else{
                cell = $("tr:nth("+(start[1]+index)+")>td:nth("+start[0]+")");
            }
            //se evalua si se puede acomodar la letra actual
            content=$(cell).text();
            if(content === "" || content === element){
                $(cell).addClass("colocando");
                $(cell).text(element);
                if(index === palabra.length-1){//al colocar la ultima letra
                    colocado = true;
                    $(".colocando").addClass("colocado").removeClass("colocando");
                }
            }else{
                $(".colocando:not(.colocado)").text("").removeClass("colocando");
                return false;
            }
        });
    }else{//2 :diagonalIzquierda   3 : diagonaDerecha
        var incremento = 0;
        $.each(palabra.split(""), function(index, element){
           cell = $("tr:nth("+(start[1]+index)+")>td:nth("+(start[0]+incremento)+")");
           content=$(cell).text();
            if((content === "" || content === element) && cell[0] !== undefined){
                $(cell).addClass("colocando");
                $(cell).text(element);
                if(index === palabra.length-1){//al colocar la ultima letra
                    colocado = true;
                    $(".colocando").addClass("colocado").removeClass("colocando");
                }
            }else{
                $(".colocando:not(.colocado)").text("").removeClass("colocando");
                return false;
            }
           direccion === 2 ? incremento-- : incremento++;
           
        });
    }
    return colocado;
}
function obtieneTextoPrentesis(texto){//si se usan oraciones que contienen la palabra a buscar een parentesis
    if(texto.indexOf("(")!==-1 && texto.indexOf(")")!==-1){
        texto = texto.substring(texto.indexOf("(")+1, texto.indexOf(")"));
    }
    return texto;
}
function animacionBoton(element){
    $(element).css({transform:"scale(1.25, 1.25)", opacity:"0", transition:"0.2s"});
    setTimeout(function(){
        $(element).css({transition:"none", transform:"scale(0.1, 0.1)", opacity:"0"});
        setTimeout(function(){
            $(element).css({transition:"0.2s", transform:"scale(1, 1)", opacity:"0.4"});
        }, 50);
    }, 200);
}
//eventos drag an drop
var inDrag=false;
function iniciarTrazo(element, event){//se inicia la seleccion de la palabra
    clearInterval(dragInterval);
    sndClick(); inDrag=true;
    if(event.pageX !== undefined && event.pageY !== undefined){
        event.pageX>0 ? posIn[0]=event.pageX : "";//posIn
        event.pageY>0 ? posIn[1]=event.pageY : "";
        event.pageX>0 ? posEnd[0]=event.pageX : "";//posEnd
        event.pageY>0 ? posEnd[1]=event.pageY : "";
    }
    $(element).addClass("onSelection");
    var cell=($(element).attr("coord")+"").split("-");
    firstPosition = [cell[1] * 1, cell[0] * 1];//X, Y
    lastPosition = [cell[1] * 1, cell[0] * 1];//X, Y
    
    dragInterval = setInterval(function(){
        var element = $.elementFromPoint(posEnd[0], posEnd[1]);
        if($(element).is("td")){
            var cell=($(element).attr("coord")+"").split("-");
            if(lastPosition[0]!==cell[1]*1 || lastPosition[1]!==cell[0]*1){
                lastPosition=[cell[1]*1, cell[0]*1];//X, Y
                calcularTrayectoria();
            }
        }
    }, 50);
}
function continuarTrazo(event){//se actualizan coordenadas
    if(event.pageX !== undefined && event.pageY !== undefined){
        event.pageX>0 ? posEnd[0]=event.pageX : "";
        event.pageY>0 ? posEnd[1]=event.pageY : "";
    }
    event.preventDefault();
}
function finTrazo(event){//se finaliza la seleccion de la palabra
    inDrag = false;
    clearInterval(dragInterval);
    if(event.pageX !== undefined && event.pageY !== undefined){
        posEnd[0]=event.pageX; posEnd[1]=event.pageY;
    }
    var element = $.elementFromPoint(posEnd[0], posEnd[1]);
    if($(element).is("td")){
        var cell=($(element).attr("coord")+"").split("-");
            if(lastPosition[0]!==cell[1]*1 || lastPosition[1]!==cell[0]*1){
                lastPosition=[cell[1]*1, cell[0]*1];//X, Y
                calcularTrayectoria();
            }        
    }
    //se obtiene la palabra seleccionada
    var elements = $(".onSelection"), coordinates="";
    if(elements.length > 2){
        var palabraSeleccionada="";
        $.each(elements, function(i, e){
            palabraSeleccionada += $(e).text();
            coordinates += $(e).attr("coord")+",";
        });
        //se evalua si es correcta
        var esCorrecta=false;
        $.each(palabrasSopa, function(i, e){
            if(palabraSeleccionada === e){
                esCorrecta = true;
                return false;
            }
        });
        if(esCorrecta || evaluacion){
            //se valida si ya fue resuelta
            var resuelta = false;
            if(palabrasEvaluadas.length > 0){
                $.each(palabrasEvaluadas, function(i, e){
                    if(palabraSeleccionada === e){
                        resuelta = true;
                        return false;
                    }
                });
            }
            //eventos de la evaluacion
            if(!resuelta && palabrasEvaluadas.length < palabrasSopa.length){
                palabrasEvaluadas.push(palabraSeleccionada);
                $(".onSelection").addClass("cellSolved");
                !evaluacion ? $(".palabra[word='"+palabraSeleccionada+"']").addClass("wordSolved") : "";
                intentosRestantes > 0 ? aciertos++ : "";
                palabrasEvaluadas.length === json[preguntaActual].respuestas.length && !evaluacion ? sigPregunta() : "";
                if(evaluacion){
                    //añade palabra a la lista de resueltos
                    coordinates=coordinates.slice(0, -1);
                    $("#resueltas").append("<div class='palabraResuelta hidde' coords='"+coordinates+"'><div class='close'></div>"+palabraSeleccionada+"</div>");
                    setTimeout(function(){
                        $(".palabraResuelta:last").removeClass("hidde");
                    }, 50);
                    eventosPalabraResuelta();
                }else{
                    sndCorrecto();
                }
            }
        }else{//es incorrecta
            sndIncorrecto();
        }
        //se reducen los intentos disponibles
        if(intentosRestantes > 0){
             intentosRestantes--;
             actualizarMarcador();
        }
    }
    $(".onSelection").removeClass("onSelection");
}
function eventosPalabraResuelta(){
    $(".palabraResuelta:last .close").on("click touchend", function(){
        var resuelta = $($(this).parent());
        palabrasEvaluadas.splice(resuelta.index(), 1);
        limpiarRespuesta(resuelta.attr("coords"), "remove");
        resuelta.addClass("hidde");
        setTimeout(function(){
            resuelta.remove();
        }, 510);
        return false;
    });
    $(".palabraResuelta:last").on("click touchend", function(){
        if($(this).hasClass("search")){
            $("td.search").addClass("cellSolved").removeClass("search");
            $(this).removeClass("search");
        }else{
            $(".palabraResuelta").removeClass("search");
            $(this).addClass("search");
            limpiarRespuesta($(this).attr("coords"), "search");
        }
    });
}
//define la direccion del trazo
var firstPosition=[], lastPosition=[], contadorSelected=0;
function calcularTrayectoria(){
    contadorSelected=0;
    var x1=firstPosition[0], x2=lastPosition[0], y1=firstPosition[1], y2=lastPosition[1];
    var despX = x2 < x1-1 || x2 > x1+1 ? true : false;
    var despY = y2 < y1-1 || y2 > y1+1 ? true : false;
    if(despX && despY){//trayectoria diagonal
        var i, j, final, coords=[];
        var dx = posIn[0] < posEnd[0] ? posEnd[0] - posIn[0] :  posIn[0] - posEnd[0];//delta x
        var dy = posIn[1] < posEnd[1] ? posEnd[1] - posIn[1] :  posIn[1] - posEnd[1];//delta y
        i = dx > dy ? x1 : y1;//se define la diagonal para tomar a x o y como base para trazar la linea
        j = dx > dy ? y1 : x1;
        final = dx > dy ? x2 : y2;
        while(i !== final){//se itera para obtener todas las coordenadas de la linea trazada
            if(dx > dy){
                coords.push(j+"-"+i);
                j = y1 < y2 ? j+1 :  j-1;
                i = x1 < x2 ? i+1 :  i-1;
            }else{
                coords.push(i+"-"+j);
                j = x1 < x2 ? j+1 :  j-1;
                i = y1 < y2 ? i+1 :  i-1;
            }
        }
        dx > dy ? coords.push(j+"-"+i) : coords.push(i+"-"+j);
        pintaTrayectoria(coords);
        
    }else if(despY && !despX){//trayectoria recta en y
        var i=y1, coords=[];
        while(i!==y2){
            coords.push(i+"-"+x1);
            i = y1 < y2 ? i+1 :  i-1;
        }
        coords.push(i+"-"+x1);
        pintaTrayectoria(coords);
        
    }else if(despX && !despY){//trayectoria recta en X
        var i=x1, coords=[];
        while(i!==x2){
            coords.push(y1+"-"+i);
            i = x1 < x2 ? i+1 :  i-1;
        }
        coords.push(y1+"-"+i);
        pintaTrayectoria(coords);
    }else{
        var coords=[];
        coords.push(y1+"-"+x1);
        coords.push(y2+"-"+x2);
        pintaTrayectoria(coords);
    }
    function pintaTrayectoria(coords){
        $(".onSelection").removeClass("onSelection");
        $.each(coords, function(index, element){
            $("td[coord='"+element+"']").addClass("onSelection");
            contadorSelected = $("td[coord='"+element+"']").hasClass("wordSelected") ? contadorSelected+1: contadorSelected;
        });
    }
}
//funciones evaluacion
function limpiarRespuesta(coordinates, action){
    var cells="";
    $("td.search").addClass("cellSolved").removeClass("search");
    $(".palabraResuelta").each(function(){
        cells+=$(this).attr("coords")+",";
    });
    cells.slice(0,-1);
    var contador, repetido;
    $.each(coordinates.split(","), function(index, element){
        if(action==="remove"){
            contador=0;
            $.each(cells.split(","), function(i, e){
                element === e ? contador++ : "";
                repetido = contador>1 ? true : false;
                return !repetido;
            });
            !repetido ? $("td[coord='"+element+"']").removeClass("cellSolved").removeClass("search") : "";
        }
        action==="search" ? $("td[coord='"+element+"']").removeClass("cellSolved").addClass("search") : "";
    });
}

///Repertorio de banderas
//  "acentos" : false   //desabilita los acentos en la sopa de letras