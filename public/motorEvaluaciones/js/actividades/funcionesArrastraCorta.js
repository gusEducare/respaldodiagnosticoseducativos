var jsonIncisos=[];
var contadorIncisos=0;
var maxWidth = 0;
var incisos;
function iniciarActividad(){
    incisos = json.length<2 && (json[preguntaActual].pregunta.textosLargos === true || json[preguntaActual].pregunta.textosLargos === "si");
    $("#contenidoActividad").html("");
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    //se genera informacion en caso de tener habilitada la bandera incisos
    if(incisos){
        incisos = true;
        obtenerJsonIncisos();
        if(jsonIncisos.length > 1 ){
            esUltima = false;
            $("#barraNav").css("display","block")
            avancePregunta = 100 / jsonIncisos.length;
            $.each(jsonIncisos, function(){//se agregan circulos en barra de avance
               $("#buttonsNav").append("<div class='off bookColor'></div>");
            });
            $(".off:last").removeClass("off");
            $("#barraNav").show();
            $("#progressBar").hide();
            setTimeout(hideNavBar, 4000);
        }
        $("#progressBar, #barraNav").on("click touchend", function(){//muestra barra de avance
            if($("#barraNav").hasClass("barraNavHidden")){
                showNavBar();
            }
        });
        $("#buttonsNav").on("click touchend", hideNavBar);//oculta barra de avance
    }
    //habilita banderas "orientacion"
    var clacesActividad = "";
    clacesActividad += json[preguntaActual].pregunta.orientacion !== undefined ? json[preguntaActual].pregunta.orientacion : "";
    $("#contenidoActividad").append("<div id='cartas' class='"+clacesActividad+"'></div><div id='contenido' class='"+clacesActividad+"'></div>");
    var clacesCarta = "";
    var cadena="";
    var estructuraContenido = ""; var lastT11 = 0;
    //habilita la bandera "soloTexto"
    clacesCarta = json[preguntaActual].pregunta.soloTexto === true ? "" : "iconDrag";
    //se agregan las cartas
    $.each(json[preguntaActual].respuestas, function(index, element){
        cadena+=index+","; lastT11++;
        $("#cartas").append("<div class='respuestaDrag "+clacesCarta+"' t17='"+element.t17correcta+"'><p>"+cambiarRutaImagen(element.t13respuesta)+"</p></div>");
    });
    //se añade el contenido
    if(!incisos){//arrastra corta
        contadorIncisos=0;
        $.each(json[preguntaActual].preguntas, function(index, element){
            estructuraContenido += cambiarRutaImagen(json[preguntaActual].preguntas[index].t11pregunta);
            estructuraContenido += index < json[preguntaActual].preguntas.length-1 ? "<div class='contenedor' t11='"+(index+1)+"'></div>" : "";
            contadorIncisos++;
        });
        //aplica bandera pintaUltimaCaja
        estructuraContenido += json[preguntaActual].pregunta.pintaUltimaCaja !== false ? "<div class='contenedor' t11='"+contadorIncisos+"'></div>" : "";    
    }else{//arrastra corta incisos
        estructuraContenido += jsonIncisos[preguntaActual];
    }
    //aplica bandera punto final//eliminada
    //estructuraContenido += json[preguntaActual].pregunta.ocultaPuntoFinal === true ? "" : ".";
    //aplica la bandera soloTexto
    clacesCarta = json[preguntaActual].pregunta.soloTexto === true ? $(".respuestaDrag").css("text-align","center") : "";
    //inserta el contenido
    $("#contenido").html(estructuraContenido);
    incisos ? $("#contenido").css("text-align","center") : "";//centra el texto //bandera incisos
    //elimina contenedores innecesarios
    while($(".contenedor").length > $(".respuestaDrag:not(.respuestaDrag[t17='0'])").length){
        $(".contenedor:last").remove();
    }
    if($("#contenidoActividad img").length>0){
        $("img:last").bind("load error", function(){
            ajustarDimenciones(cadena);
        });
    }else{
        setTimeout(function(){ajustarDimenciones(cadena)}, 50);
    }
    //se ace el conteo de intentos y respuestas totales
    intentosRestantes = $(".contenedor").length;
    totalPreguntas += intentosRestantes;
}

function obtenerJsonIncisos(){
    var jsonString = "";
    $.each(json[0].preguntas, function(index, element){
        contadorIncisos++;
        jsonString += cambiarRutaImagen(element.t11pregunta);
        jsonString += contadorIncisos < json[0].preguntas.length ? "<div class='contenedor' t11='"+contadorIncisos+"'></div>" : "";
        jsonString = jsonString.replace("<br>", "*replace*");
    });
//    /<br\s*[\/]?>/gi //expresion regular <br>
//    jsonIncisos.shift();//elimina primer elemento
    jsonIncisos = jsonString.split("*replace*");
    var contador=0;
    while(contador<jsonIncisos.length){//elimina elementos que no tengan contenedor
        if(jsonIncisos[contador].search("class='contenedor'") === -1){
            jsonIncisos.splice(contador, 1);
            contador= 0;
        }else{
            contador++;
        }
    }
}

function ajustarDimenciones(cadena){
    //aplica bandera respuestas largas
    var currentWidth, maxHeight=0, img;
//    var respLargas = json[preguntaActual].pregunta.respuestasLargas === true || evaluacion;
    var customWidth = json[preguntaActual].pregunta.anchoRespuestas !== undefined;// && respLargas;
    if(customWidth){
        json[preguntaActual].pregunta.anchoRespuestas !== undefined ? maxWidth = json[preguntaActual].pregunta.anchoRespuestas : "";
        $(".respuestaDrag").css("width", maxWidth+"px");
    }
    $(".respuestaDrag").each(function(index, element){
        img = $(element).find("img")[0] !== undefined;
        if(!customWidth){
            currentWidth = img ? $(element).find("img")[0].scrollWidth : element.scrollWidth;
            maxWidth = currentWidth > maxWidth ? currentWidth : maxWidth;
        }
        maxHeight = element.scrollHeight > maxHeight ? element.scrollHeight : maxHeight;
    });
    if(!customWidth){
        maxWidth = maxWidth < 80 ? 80 : maxWidth;
        $(".respuestaDrag").css("width", maxWidth+"px");
    }
    $(".contenedor").width($(".respuestaDrag:first")[0].scrollWidth);
    //aplica el mismo alto a los contenedores y terjetas
    $(".respuestaDrag, .contenedor").height(maxHeight);
    //se acomodan de manera aleatoria las cartas
    cartasRandom(cadena);
    $(".respuestaDrag").css({position: "relative", top:0, left:0});
    //se hacen estaticas las dimenciones del contenedor de cartas
    $("#cartas").height($("#cartas")[0].scrollHeight);
    $("#cartas").width($("#cartas")[0].scrollWidth);
    //determina el tamaño del espacio de contenido segun el espacio restante de la actividad
    $("#contenido").hasClass("vertical") ? $("#contenido").width($("#contenidoActividad").width() - $("#cartas"). width() - 40) : $("#contenido").height($("#contenidoActividad").height() - $("#cartas"). height() - 5);
    //se fijan las pocisiones de las cartas
    fijarCartas();
}

function sigInciso(){
    $("#contenidoActividad").addClass("lock");
    $("#progressBar").width((avancePregunta*(preguntaActual+1))+"%");
    esUltima && libro !== "PROF" ? finalizarActividad() : setTimeout(siguienteInciso, 500);
}

function siguienteInciso(){
    preguntaActual++;
    $("#contenido").css({left:"-100%", transition:"left 1s"});
    $(".respuestaDrag.solved").remove();//elimina elementos arrastrados
    setTimeout(function(){
        $("#contenido").css({left:"100%", transition:"none"});
        libro !== "PROF" ? $(".off:last").removeClass("off") : "";
        //grega nuevo contenido
        $("#contenido").html(jsonIncisos[preguntaActual]);
        //aplica bandera pintaUltimaCaja
        preguntaActual === jsonIncisos.length-1 &&  json[0].pregunta.pintaUltimaCaja !== false ? $("#contenido").append("<div class='contenedor' t11='"+contadorIncisos+"'></div>") : "";
        //elimina contenedores innecesarios
        while($(".contenedor").length > $(".respuestaDrag:not(.respuestaDrag[t17='0'])").length){
            $(".contenedor:last").remove();
        }        
        //obtiene daos de evaluacion
        intentosRestantes = $(".contenedor").length;
        totalPreguntas += intentosRestantes;
        //aplica el mismo alto a los contenedores
        $.each($(".respuestaDrag"), function(index, element){
            var t17 = $(element).attr("t17").split(",");
            $.each(t17, function(i, e){
                $(".contenedor[t11='"+e+"']").width(element.scrollWidth);
            });
        });
        libro === "PROF" ? resolverActividades() : "";
        //continua actividad
        $("#contenidoActividad").removeClass("lock");
        actualizarMarcador();
        esUltima = preguntaActual === jsonIncisos.length -1 ? true : false;
        setTimeout(function(){
            dragAndDrop();
            $("#contenido").css({left:"0%", transition:"left 1s"});
        }, 20);
    }, 1000);    
}
///////////////repertorio de banderas///////////////////

//////pregunta
// "orientacion":""         // vertical : horizontal        //define la manera en que se muetran los contenedores de cartas y contenido