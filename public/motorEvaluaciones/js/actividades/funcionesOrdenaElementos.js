function iniciarActividad(){
    var pregunta = "";
    $("#contenidoActividad").html("<div id='actividadOrdena'></div>");
    if(json[preguntaActual].pregunta.t11pregunta !== undefined || json[preguntaActual].pregunta.t11pregunta !== null){
        pregunta = json[preguntaActual].pregunta.t11pregunta;
        pregunta !== "" && pregunta.length > 5 ? $("#contenidoActividad").html("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div><div id='actividadOrdena'></div>") : "";
    }
    var cadena = "", etiqueta;
    $.each(json[preguntaActual].respuestas, function(index, element){//añade respuestas
        cadena += index+",";
        $("#actividadOrdena").append("<div class='respuestaDrag' t17='"+element.t17correcta+"'>"+element.t13respuesta+"</div>");
        etiqueta = element.etiqueta === undefined ? "" : element.etiqueta;
        $("<div class='contenedor "+(index>0 ? "noHeight" : "")+"' t11='"+index+"'><div class='mark bookColor'></div><div class='mask'><p>"+etiqueta+"</p></div></div>").insertBefore($(".respuestaDrag:first"));
    });
    //conteo de correctas
    intentosRestantes = $(".contenedor").length;
    totalPreguntas += intentosRestantes;
    $(".contenedor:first").removeClass("noHeight");//muestra el primer contenedor
    coloresRandom($(".respuestaDrag"));//aplica colores
    cartasRandom(cadena);//aplica random a los elementos
    if($("#contenidoActividad img").length>0){
        $("img").on("load", calculaDimensiones);
    }else{
        setTimeout(calculaDimensiones, 10);
    }
    $(".respuestaDrag").draggable({axis:"y"});
}
function calculaDimensiones(){
    maxHeight = 0; maxWidth=0;
    $(".respuestaDrag").each(function(i, e){
        maxHeight = e.scrollHeight > maxHeight ? e.scrollHeight : maxHeight;
        maxWidth = e.scrollWidth > maxWidth ? e.scrollWidth : maxWidth;
    });
    $(".respuestaDrag").css({height: maxHeight, width: maxWidth-40});
    $(".contenedor").css({height: maxHeight, width: maxWidth});
}