var horizontales=[];//almacena indices de preguntas horizontales
var verticales=[];//almacena indices de preguntas verticales
var indexV=0, indexH=0;
var rows = 11, cols = 11;//establece filas y columnas
var dirEscritura;//direccion de escritura   tr = vertical   td = horizontal
verticalesResueltos=[];
horizontalesResueltos=[];
function iniciarActividad(){
    //se recetean variables;
    intentosRestantes=0; contadorCorrectas = 0;
    horizontales = []; verticales = [];
    intentosRestantes=json[preguntaActual].respuestas.length;
    totalPreguntas+=intentosRestantes;
    
    //se añade estructura html base
    $("#contenidoActividad").html("<div id='preguntasActividad' class='horizontal'></div><div id='crucigrama' class='horizontal'></div>");
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    //se añaden encabezados
    $("#preguntasActividad").append("<div id='H' class='header'><p>Horizontal</p></div><div class='contenedor'></div>");
    $("#preguntasActividad").append("<div id='V' class='header'><p>Vertical</p></div><div class='contenedor'></div>");
    //se añaden botones de navegacion
    $(".header").append("<div id='back' class='button'></div><div id='next' class='button'></div>");
    //se cra el grid
    $("#crucigrama").html("<table></table>");
    //se apican banderas alto y ancho //innecesario. El grid se extiende en automatico
            //    var alto = json[preguntaActual].pregunta.alto !== undefined ? json[preguntaActual].pregunta.alto : 0;
            //    var ancho = json[preguntaActual].pregunta.ancho !== undefined ? json[preguntaActual].pregunta.ancho : 0;
            //    alto > rows ? rows=alto : "";
            //    ancho > cols ? cols=ancho : "";
                
    //se añaden filas y columnas
    creaGrid("tr", rows);
    creaGrid("td", cols);
    
    //se definen los elementos horizontales y verticales
    var vert=0, hor=0;
    $.each(json[preguntaActual].pocisiones, function(index, element){//se cuenta que elementos tienen mayor numero
        element.direccion === 1 ? vert++ : hor++;
    });
    
    $.each(json[preguntaActual].pocisiones, function(index, element){//se reacomodan los indices para que coincidan ennnumero de pregunta
        if(vert > hor && element.direccion===1){
            verticales.push(index);//se obtienen los indices de las verticales
            $.each(json[preguntaActual].pocisiones, function(i, e){//se organizan las horizontales
                if(element.datoX === e.datoX && element.datoY === e.datoY && e.direccion===0){
                    horizontales.push(i);//se obtienen los indices de las horizontales
                }
            });
            if(verticales.length>horizontales.length){
                horizontales.push(false);
            }
        }else if(vert <= hor && element.direccion===0){
            horizontales.push(index);//se obtienen los indices de las horizontales
            $.each(json[preguntaActual].pocisiones, function(i, e){//se organizan las verticales
                if(element.datoX === e.datoX && element.datoY === e.datoY && e.direccion===1){
                    verticales.push(i);//se obtienen los indices de las verticales
                }
            });
            if(horizontales.length>verticales.length){
                verticales.push(false);
            }
        }
    });
    
    //se completan los indices faltantes
    var esPrimero;
    var nuevoIndex=0;
    $.each(json[preguntaActual].pocisiones, function(index, element){
        esPrimero=true;
         if(vert > hor && element.direccion===0){
             var existe= false;
             $.each(horizontales, function(i, e){
                 if(index === e){
                     existe= true;
                     return false;
                 }else if(e===false && esPrimero){
                     esPrimero = false;
                     nuevoIndex = i;
                 }
             });
             !existe ? horizontales[nuevoIndex] = index : "";
         }else if(vert <= hor && element.direccion===1){
             var existe= false;
             $.each(verticales, function(i, e){
                 if(index === e){
                     existe= true;
                     return false;
                 }else if(e===false && esPrimero){
                     esPrimero = false;
                     nuevoIndex = i;
                 }
             });
             !existe ? verticales[nuevoIndex] = index : "";
         }
    });
    
    //se eliminan elementos false
    var temVert=[], tempHor=[];
    $.each(verticales, function(index, element){
       element!==false ?  temVert.push(element) : "";
    });
    $.each(horizontales, function(index, element){
       element!==false ?  tempHor.push(element) : "";
    });
    horizontales = tempHor;
    verticales = temVert;
    //se añade el contenido
    var numeroPregunta = 0;
    $.each(verticales, function(index, element){
        var datoX, datoY;
        var palabra;
        numeroPregunta++;
        datoX = json[preguntaActual].pocisiones[element].datoX;
        datoY = json[preguntaActual].pocisiones[element].datoY;
        palabra = cambiarRutaImagen(json[preguntaActual].respuestas[element].t13respuesta);
        crearCeldas(datoX, datoY, palabra, "tr", numeroPregunta);
    });
    numeroPregunta = 0;
    $.each(horizontales, function(index, element){
        var datoX, datoY;
        var palabra;
        numeroPregunta++;
        datoX = json[preguntaActual].pocisiones[element].datoX;
        datoY = json[preguntaActual].pocisiones[element].datoY;
        palabra = cambiarRutaImagen(json[preguntaActual].respuestas[element].t13respuesta);
        crearCeldas(datoX, datoY, palabra, "td", numeroPregunta);
    });
    
    //elimina la ultima fila si se encuentra vacia
    var filaVacia=true;
    $.each($("tr:last td"), function(index, element){
       if($(element).hasClass("cell")){
           filaVacia = false;
       } 
    });
    filaVacia ? $("tr:last").remove()  : "";
    
    //llena contenedores con la primer pregunta en vertical y horizontal
    $(".contenedor:first").html("<h6>1.&nbsp</h6>"+cambiarRutaImagen(json[preguntaActual].preguntas[horizontales[0]].t11pregunta));
    $(".contenedor:last").html("<h6>1.&nbsp</h6>"+cambiarRutaImagen(json[preguntaActual].preguntas[verticales[0]].t11pregunta));
    
    
    //////////////////////////////////////////
    //eventos al escribir/////////////////////
    $(".cell").on("click touchend", function(event){
        var cell = this;
        dirEscritura="";//resetea la direccion de escritura
        $(cell).find("input").focus().setCursorToTextEnd();
        obtieneDireccion(cell);
        //cambia la pregunta de acuerdo a la celda en la que se ubique
        var nH = $(cell).attr("pregh");
        var nV = $(cell).attr("pregv");
        if(nH!==undefined){indexH=nH*1-1; cambiaPregunta("first");}
        if(nV!==undefined){indexV=nV*1-1; cambiaPregunta("last");}
    }).keydown(function(event){//eventos al presionar tecla
        var key = event.key === undefined ? String.fromCharCode(event.which) : event.key;
        var esCaracterCorrecto = letrasArr.indexOf(key.toUpperCase()) !== -1 ? true : false;
        key === " " ? esCaracterCorrecto=true : "";
        if(esCaracterCorrecto){
            if(!$(this).hasClass("correctAnswer")){
                $(this).find("input").val(key);
                !evaluacion ? evaluaPalabra() : "";
            }
            navegaSiguiente(this, 1);//navega a la siguiente celda
        }else{
            //eventos de teclas especiales
            event.which === 37 || event.which === 39 ? dirEscritura = "td" : "";
            event.which === 38 || event.which === 40 ? dirEscritura = "tr" : "";
            if(event.which === 8 || event.which === 37 || event.which === 38){//tecla backspace//left//up
                event.which === 8 && !$(this).hasClass("correctAnswer") ? $(this).find("input").val("") : "";
                navegaSiguiente(this, -1);//navega a la celda anterior
            }else if(event.which === 39 || event.which === 40){//right//down
                navegaSiguiente(this, 1);
            }
            if(event.which === 9){//evento tecla TAB //obtiene direccion de escritura
                obtieneDireccion(this);
            }
            return false;
        }
    }).keypress(function(){return false;}).keyup(function(){return false;});
    
    
    //navcegacion en las cajas de preguntas///////////
    $("#back.button").on("click touchend", function(){//pregunta anterior
        var element = this, cell;
        $(element).css({left:"5px"});
        setTimeout(function(){
            $(element).css({left:"20px"});
        }, 130);
        var pregunta = $(element).parent(".header").attr("id");
        if(pregunta==="H"){
            indexH = indexH === 0 ? horizontales.length-1 : indexH-1;
            dirEscritura = "td";
            cambiaPregunta("first");
            //selecciona la celda de acuerdo al numero de pregunta
            var cellX=json[preguntaActual].pocisiones[horizontales[indexH]].datoX;
            var cellY=json[preguntaActual].pocisiones[horizontales[indexH]].datoY;
            cell = $("tr:nth("+(cellY-1)+")>td:nth("+(cellX-1)+")");
            $(cell).find("input").focus();
            seleccionaCelda("H", cell);
        }else{
            indexV = indexV === 0 ? verticales.length-1 : indexV-1;
            dirEscritura = "tr";
            cambiaPregunta("last");
            cell = $("tr:nth()>th:nth()");
            //selecciona la celda de acuerdo al numero de pregunta
            var cellX=json[preguntaActual].pocisiones[verticales[indexV]].datoX;
            var cellY=json[preguntaActual].pocisiones[verticales[indexV]].datoY;
            cell = $("tr:nth("+(cellY-1)+")>td:nth("+(cellX-1)+")");
            $(cell).find("input").focus();
            seleccionaCelda("V", cell);            
        }
    });
    $("#next.button").on("click touchend", function(){//siguiente pregunta
        var element = this, cell;
        $(element).css({right:"1px"});//animacion del boton
        setTimeout(function(){
            $(element).css({right:"21px"});
        }, 130);        
        var pregunta = $(element).parent(".header").attr("id");
        if(pregunta==="H"){
            indexH = indexH === horizontales.length-1 ? 0 : indexH+1;
            dirEscritura = "td";
            cambiaPregunta("first");
            //selecciona la celda de acuerdo al numero de pregunta
            var cellX=json[preguntaActual].pocisiones[horizontales[indexH]].datoX;
            var cellY=json[preguntaActual].pocisiones[horizontales[indexH]].datoY;
            cell = $("tr:nth("+(cellY-1)+")>td:nth("+(cellX-1)+")");
            $(cell).find("input").focus();
            seleccionaCelda("H", cell);
        }else{
            indexV = indexV === verticales.length-1 ? 0 : indexV+1;
            dirEscritura = "tr";
            cambiaPregunta("last");
            //selecciona la celda de acuerdo al numero de pregunta
            var cellX=json[preguntaActual].pocisiones[verticales[indexV]].datoX;
            var cellY=json[preguntaActual].pocisiones[verticales[indexV]].datoY;
            cell = $("tr:nth("+(cellY-1)+")>td:nth("+(cellX-1)+")");
            $(cell).find("input").focus();
            seleccionaCelda("V", cell);
        }
    });
    
    //seleccion de pregunta
    $(".contenedor:first, #H").on("click touchend", function(){
        dirEscritura = "td";
        var cellX=json[preguntaActual].pocisiones[horizontales[indexH]].datoX;
        var cellY=json[preguntaActual].pocisiones[horizontales[indexH]].datoY;
        $("tr:nth("+(cellY-1)+")>td:nth("+(cellX-1)+")").find("input").focus();
        cambiaPregunta("first");
    });
    $(".contenedor:last, #V").on("click touchend", function(){
        dirEscritura = "tr";
        var cellX=json[preguntaActual].pocisiones[verticales[indexV]].datoX;
        var cellY=json[preguntaActual].pocisiones[verticales[indexV]].datoY;
        $("tr:nth("+(cellY-1)+")>td:nth("+(cellX-1)+")").find("input").focus();
        cambiaPregunta("last");
    });
}

function creaGrid(element, size){//inserta elementos de tabla
    var selector = element === "tr" ? "table" : "table tr";
    element = element === "tr" ? "<tr></tr>" : "<td></td>";
    for(var i=0; i<size; i++){
        $(selector).append(element);
    }
}
var fila;
function crearCeldas(x, y, palabra, selector, numeroPregunta){
    var esPrimero = false;
    x--; y--;
    
    fila = fila===undefined ? $("tr:first").prop('outerHTML')+"" : fila;
    $.each(palabra.split(""), function(index, element){
        if(y > $("tr").length-1){//extiende el grid si es necesario
            var newExtend = y - ($("tr").length-1);
            for(var i=0; i<newExtend; i++){
                $("table").append(fila);
            }
        }else if(x > $("tr:first>td").length-1){
            var newExtend = x - ($("tr:first>td").length-1);
            for(var i=0; i<newExtend; i++){
                $("<td></td>").appendTo($("tr"));
            }
        }        
        //acomoda la palabra
        var direccion = selector === "tr" ? "V" : "H";
        var celda = "tr:nth("+y+")>td:nth("+x+")";
        if(!$(celda).hasClass("cell")){
            $(celda).html("<input  maxlength='1' type='text'>");
            $(celda).addClass("cell");
        }
        $(celda).attr("preg"+direccion, numeroPregunta);
        if(!esPrimero){//se añade el numero de pregunta a la primera celda de la palabra
            esPrimero=true;
            if($(celda).find("label").length===0){
                $(celda).append("<label>"+numeroPregunta+"</label>");
            }
        }
        selector === "tr" ? y++ : x++;
    });
}

function cambiaPregunta(selector){
    $(".contenedor").removeClass("solved focus");
    dirEscritura === "tr" ? $(".contenedor:last").addClass("focus") : $(".contenedor:first").addClass("focus");
    if(selector==="first"){
        $(".contenedor:first").html("<h6>"+(indexH+1)+".&nbsp</h6>"+cambiarRutaImagen(json[preguntaActual].preguntas[horizontales[indexH]].t11pregunta));
        var indexActual = $(".contenedor:first").find("h6").text().replace(/\s/g, "").replace(".","");
        var seRespondio=false;
        if(horizontalesResueltos.length>0){
            $.each(horizontalesResueltos, function(index, element){
                if(indexActual-1 === element){
                    seRespondio=true;
                    return false;
                }
            });
        }
        seRespondio ? $(".contenedor:first").addClass("solved") : $(".contenedor:first").addClass("focus");
        seRespondio ? $(".correctAnswer").find("input").blur() : "";
    }else{
        $(".contenedor:last").html("<h6>"+(indexV+1)+".&nbsp</h6>"+cambiarRutaImagen(json[preguntaActual].preguntas[verticales[indexV]].t11pregunta));
        var indexActual = $(".contenedor:last").find("h6").text().replace(/\s/g, "").replace(".","");
        var seRespondio=false;        
        if(verticalesResueltos.length>0){
            $.each(verticalesResueltos, function(index, element){
                if(indexActual-1 === element){
                    seRespondio=true;
                    return false;
                }
            });
        }
        seRespondio ? $(".contenedor:last").addClass("solved") : $(".contenedor:last").addClass("focus");
        seRespondio ? $(".correctAnswer").find("input").blur() : "";
    }
}

function seleccionaCelda(dir, cell){
    if(dir==="H"){
        //cambia la pregunta de la caja vertical si la celda esta en una pocicion cruzada
        dirEscritura = "td";            
        if($(cell).attr("pregv")!==undefined){
            indexV = $(cell).attr("pregv")*1 - 1;
            cambiaPregunta("last");
        }
    }else{
        dirEscritura = "tr";
        //cambia la pregunta de la caja vertical si la celda esta en una pocicion cruzada
        if($(cell).attr("pregh")!==undefined){
            indexH = $(cell).attr("pregh")*1 - 1;
            cambiaPregunta("first");
        }          
    }
}

function navegaSiguiente(element, incremento){
            var columnaActual = $(element).index();
            var filaActual = $(element).parent("tr").index();
            var siguiente;
            var limpiarSiguiente = false;
            if(dirEscritura==="tr"){
                siguiente = filaActual+incremento;
                var filaSiguiente = $(dirEscritura+":nth("+siguiente+")>td:nth("+columnaActual+")");
                limpiarSiguiente ? $(filaSiguiente).find("input").val("") : "";
                $(filaSiguiente).find("input").focus();
            }else{
                siguiente = columnaActual+incremento;
                var columnaSiguiente = $("tr:nth("+filaActual+")").find(dirEscritura+":nth("+siguiente+")");
                $(columnaSiguiente).find("input").focus();
                limpiarSiguiente ? $(columnaSiguiente).val("") : "";
            }    
}

function obtieneDireccion(element){//establece la direccion que llevara la escritura
    var randomDir = function(){
        var dir = parseInt(Math.random()*2);
        dir = dir === 0 ? "tr" : "td";
        return dir;
    };
    if($(element).attr("pregv")!== undefined && $(element).attr("pregh")!== undefined){
        //celda en escritura vertical y horizontal
        var columnaActual = $(element).index();
        var filaActual = $(element).parent("tr").index();
        var columnaSiguiente = $("tr:nth("+filaActual+")").find("td:nth("+(columnaActual+1)+")");
        var filaSiguiente = $("tr:nth("+(filaActual+1)+")");

        if($(columnaSiguiente).find("input").val() !== ""){
        //determina, si ya se respondio en horizontal, la direcciocion sera vertical
            if($(filaSiguiente).find("td:nth("+columnaActual+")").find("input").val() === ""){
                dirEscritura = "tr";
            }else{
                dirEscritura = randomDir();
            }
        }else if($(filaSiguiente).find("td:nth("+columnaActual+")").find("input").val() !== ""){
            //determina, si ya se respondio en vertical, la direcciocion sera horizontal
            if($(columnaSiguiente).find("input").val() === ""){
                dirEscritura = "td";
            }else{
                dirEscritura = randomDir();
            }
        }else{
            //de lo cantrario se genera una direccion random
                dirEscritura = randomDir();
        }

    }else if($(element).attr("pregv")!== undefined){//escritura vertical
        dirEscritura = "tr";
    }else if($(element).attr("pregh")!== undefined){//escritura horizontal
        dirEscritura = "td";
    }            
}

var contadorCorrectas=0;
function evaluaPalabra(){
    var numeroPregunta = dirEscritura==="tr" ? "pregv="+(indexV+1) : "pregh="+(indexH+1);
    var seCompleto=true;
    var palabraFormada="";
    $.each($(".cell["+numeroPregunta+"]"), function(index, element){
        if($(element).find("input").val()===""){
            seCompleto=false;
            return false;
        }
        palabraFormada += $(element).find("input").val();
    });
    
    var esCorrecta=false;
    if(seCompleto){//si se llenaron todas las celdas de la pregunta
        var indicePalabra = dirEscritura==="tr" ? verticales[indexV] : horizontales[indexH];
        var palabra = cambiarRutaImagen(json[preguntaActual].respuestas[indicePalabra].t13respuesta);
        palabra = eliminarAcentos(palabra).toUpperCase();
        palabraFormada = palabraFormada.toUpperCase();
        if(palabraFormada === palabra){//es correcta la respuesta
           contadorCorrectas++;
           dirEscritura==="tr" ? verticalesResueltos.push(indexV) : horizontalesResueltos.push(indexH);
           dirEscritura==="tr" ? $(".contenedor:last").addClass("solved") : $(".contenedor:first").addClass("solved");
           intentosRestantes > 0 ?  aciertos++ : "";
           $(".cell["+numeroPregunta+"]").addClass("correctAnswer");
           $(".cell["+numeroPregunta+"]").find("input");
           esCorrecta = true;
           sndCorrecto();
        }
        intentosRestantes--;
        actualizarMarcador();
        contadorCorrectas === json[preguntaActual].respuestas.length ? sigPregunta() : "";
        if(!esCorrecta){
            sndIncorrecto();
            $(".cell["+numeroPregunta+"] input").blur();
            $(".cell["+numeroPregunta+"]").addClass("badAnswer").css("transition","none");
            $(".cell["+numeroPregunta+"] input").css("transition","none");
            setTimeout(function(){
                $(".cell["+numeroPregunta+"]").removeClass("badAnswer").css("transition", "box-shadow 0.5s");
                $(".cell["+numeroPregunta+"] input").css({color: "transparent", transition:"color 0.5s"});
                setTimeout(function(){
                    $(".cell["+numeroPregunta+"]:not(.correctAnswer)").find("input").val("");
                    $(".cell["+numeroPregunta+"]:not(.correctAnswer) input").css({color: "black", transition:"none"});
                    $(".cell["+numeroPregunta+"]:not(.correctAnswer):first input").removeAttr("disabled").focus();
                }, 500);
            }, 500);
        }
        setTimeout(function(){$(".correctAnswer input").blur();}, 100);
    }
}

//envia el cursor al final del texto en las celdas
(function($){
    $.fn.setCursorToTextEnd = function() {
        var $initialVal = this.val();
        this.val($initialVal);
    };
})(jQuery);