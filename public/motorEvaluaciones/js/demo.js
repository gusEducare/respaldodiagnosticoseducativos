json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<div class=\"imgsAnt\"><img src=\"\/IC4-1P-teclado.jpg\" alt=\"\" data-mce-src=\"recursos\/imagenes\/IC4-1P-teclado.jpg\"><\/div>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<div class=\"imgsAnt\"><img src=\"\/IC4-1P-monitor.jpg\" alt=\"\" data-mce-src=\"recursos\/imagenes\/IC4-1P-monitor.jpg\"><\/div>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<div class=\"imgsAnt\"><img src=\"\/IC4-1P-raton.jpg\" alt=\"\" data-mce-src=\"recursos\/imagenes\/IC4-1P-raton.jpg\"><\/div>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<div class=\"imgsAnt\"><img src=\"\/IC4-1P-bocinas.jpg\" alt=\"\" data-mce-src=\"recursos\/imagenes\/IC4-1P-bocinas.jpg\"><\/div>",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "<p>Selecciona la imagen que represente al Rat\u00f3n o Mouse.<\/p>",
            "t11instruccion":"Da clic en el botón de la respuesta correcta."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Hoja de c\u00e1lculo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Linux<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Logo<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Mac OS<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Procesador de textos<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "<p>Selecciona los programas que pertenecen al tipo de software de aplicaci\u00f3n.<\/p>",
            "t11instruccion":"Selecciona los programas que pertenecen a cada tipo de software. (Selecciona 2 opciones)"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Linux<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Logo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Mac OS<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>MS Windows<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Scratch<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "<p>Selecciona los programas que pertenecen al tipo de software de programaci\u00f3n.<\/p>",
            "t11instruccion":"Selecciona los programas que pertenecen a cada tipo de software. (Selecciona 2 opciones)"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Monitor<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Teclado<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Procesador de texto<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Navegador de Internet<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>CPU<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Rat\u00f3n<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Antivirus<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>C\u00e1mara Web<\/p>",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "tipo":"vertical",
            "t11pregunta": "<p>Arrastra al contenedor todos los elementos que consideres pertenecen a la categor\u00eda de Hardware.<\/p>",
            "t11instruccion":"Arrastra los elementos al contenedor"
        }, 
        "contenedores":["Hardware"]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>bulbos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>transistores<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>PC's<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>microprocesadores<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>En los a\u00f1os 30 a 50 las primeras computadoras utilizaban <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>, no ten\u00edan monitor ni teclado ni rat\u00f3n. En los a\u00f1os 60 las computadoras eran del tama\u00f1o de una lavadora y usaban<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. En los a\u00f1os 70 surgieron las primeras computadoras personales o <\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>. En la actualidad las computadoras utilizan<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<p>Arrastra al contenedor la palabra que corresponda.<\/p>",
            "t11instruccion":"Arrastra al contenedor la palabra que corresponda."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>CORTAR<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>COPIAR<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>PEGAR<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>A).-<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br data-mce-bogus=\"1\"><\/p><p>B).-<\/p>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<p>.<br data-mce-bogus=\"1\">C).-<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<div class=\"imgsAnt\"><img src=\"http:\/\/certificacionestics.com\/img\/examenes\/IC4SBE3I~teclas_combinacion.jpg\" alt=\"\" data-mce-src=\"..\/..\/img\/examenes\/IC4SBE3I~teclas_combinacion.jpg\"><\/div><p>Selecciona la funci\u00f3n de la combinaci\u00f3n de teclas indicada en la imagen:&nbsp;<\/p>",
            "pintaUltimaCaja":true,
            "t11instruccion":"Arrastra al contenedor la palabra que corresponda."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Monitor<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>CPU<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Rat\u00f3n<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Teclado<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Bocinas<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Impresora<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "11",
                "t11pregunta": "<p>Nos permite ver lo que hacemos en la computadora.<\/p>"
            },
            {
                "c03id_tipo_pregunta": "11",
                "t11pregunta": "<p>Es la parte m\u00e1s importante de la computadora.<\/p>"
            },
            {
                "c03id_tipo_pregunta": "11",
                "t11pregunta": "<p>Nos ayuda a seleccionar, apuntar, elegir y mover objetos en la computadora.<\/p>"
            },
            {
                "c03id_tipo_pregunta": "11",
                "t11pregunta": "<p>Tiene muchas teclas, nos ayuda a introducir la informaci\u00f3n en la computadora.<\/p>"
            },
            {
                "c03id_tipo_pregunta": "11",
                "t11pregunta": "<p>Nos permite escuchar las canciones que tiene la computadora.<\/p>"
            },
            {
                "c03id_tipo_pregunta": "11",
                "t11pregunta": "<p>Muestra en papel la informaci\u00f3n que se tiene en la pantalla de la computadora.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>Une con una l\u00ednea la opci\u00f3n que describa cada parte de la computadora.<\/p>",
            "t11instruccion":"Relaciona las columnas con una línea arrastrando de un punto a otro."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>.bmp &nbsp; .jpg &nbsp; .png<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>.docx &nbsp; .txt &nbsp; .rtf<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>.xls &nbsp; &nbsp;.xlw &nbsp; .xlo &nbsp; .ods<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>.mp3 &nbsp; .m4a &nbsp; .wma<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>.zip &nbsp; .rar<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>.ppt &nbsp; .pps &nbsp; .pptx<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>.wmv &nbsp; .avi &nbsp; .mpg<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>.htm &nbsp; .php &nbsp; .asp<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Dibujos o fotos<\/p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Documentos de texto<\/p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Hoja de c\u00e1lculo<\/p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>M\u00fasica<\/p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Archivos comprimidos<\/p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Presentaciones<\/p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Videos<\/p>"
            },
            {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "<p>Internet y Web<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>\u00bfA qu\u00e9 extensi\u00f3n corresponde cada tipo de archivo?<\/p>",
            "t11instruccion":"Relaciona las columnas con una línea arrastrando de un punto a otro."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "<p>Honduras se ubica en america del norte<\/p>",
            "t11instruccion":"Selecciona falso/verdadero"
        }
    }
];

correctas = ["0010", "10001", "01001", "1_demo", "1-0_2-1_3-2_4-3", "1-0_2-1_3-2", "1-2-3-4-5-6_1-2-3-4-5-6_1-1-1-1-1-1", "1-2-3-4-5-6-7-8_1-2-3-4-5-6-7-8_1-1-1-1-1-1-1-1", "01"];
