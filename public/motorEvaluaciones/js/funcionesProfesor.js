var inicioProf = true;
$(document).ready(function(){
    
});
function resolverActividades(){
    if(inicioProf){
        inicioProf=false;
        $("#buttonsNav > p").remove();
        $("#buttonsNav > div").on("click touchend", function(){
            clearTimeout(navTimeout);
            clearTimeout(navTimeout);
            navTimeout = setTimeout(hideNavBar, 9000);
            $(this).addClass("buttonSelected");
            $("#buttonsNav > div").addClass("off");
            while(!$(".off:last").hasClass("buttonSelected")){
                $(".off:last").removeClass("off");
            }
            $(".buttonSelected").removeClass("off buttonSelected");
            preguntaActual = $("#buttonsNav>div:not(.off)").length-2;
            incisos === true ? sigInciso() : sigPregunta();
            return false;
        });
    }
    var t17, agrupar, contenedor, grupo, insertar;
    switch(lastResort){
        case "ArrastraVertical":
        case "ArrastraHorizontal":
        case "ArrastraMatriz":
        case "ArrastraMatrizHorizontal":
        case "ArrastraOrdenar":
        case "ArrastraImagenes":
        case "ArrastraContenedorUnico":
        case "ArrastraCorta":
            var numPregunta = incisos ? 0 : preguntaActual;
            $(".textoContenedor").remove();
            $("#cartas").css("opacity","0");
            if(incisos){//eventos para arrastra corta incisos
                $("#cartas").html("");
                var clacesCarta = json[numPregunta].pregunta.soloTexto === true ? "" : "iconDrag";
                $.each(json[numPregunta].respuestas, function(index, element){
                    $("#cartas").append("<div class='respuestaDrag "+clacesCarta+"' t17='"+element.t17correcta+"'><p>"+cambiarRutaImagen(element.t13respuesta)+"</p></div>");
                });
            }
            grupos = json[numPregunta].pregunta.agrupaContenedores === true;
            $(".respuestaDrag").each(function(index, element){
                insertar=false;
                t17=$(element).attr("t17").split(",");
                $.each(t17, function(i, e){
                    agrupar = $(element).attr("grupo") !== undefined;
                    if(!agrupar){
                        contenedor = $(".contenedor[t11='"+e+"']:not(.ocupado):first");
                        if(contenedor.length>0 && !contenedor.hasClass("ocupado")){
                            insertar = true;
                        }
                    }else{//variable de agrupa contenedores
                        contenedor = $(".contenedor[t11='"+e+"']");
                        grupo = $(contenedor.find(".respuestaDrag"));
                        if(grupo.length === 0 || grupo.first().attr("grupo")*1 === $(element).attr("grupo")*1){
                            insertar = true;
                        }
                    }
                    if(insertar){
                        contenedor.addClass("ocupado");
                        $(element).addClass("respuestaColocada").appendTo(contenedor);
                        return false;
                    }
                });
            });
        $(".contenedor .respuestaDrag").css({border:0, "background-color":"transparent", color:"magenta"});
        $(".contenedor .respuestaDrag[style *= 'background-image']").css({"background-color":"transparent", color:"magenta"});
        $(".contenedor .respuestaDrag:has(img)").css({"background-color":"Magenta", color:"white"});
        $(".resph").removeClass("resph");
        break;
        //
        case "RelacionaLineas":
            function solveActivity(){
                var lineas = "";
                var CI, CD;
                var x1=0, x2=0, y1=0, y2=0;
                var baseCoords = $("#contenidoActividad").offset();
                $(".colIzquierda").each(function(index, element){
                    CI = $(element); CD = $(".colDererecha[t17='"+CI.attr("t11")+"']");
                    x1 = (CI.next().offset().left + 22) - baseCoords.left;
                    x2 = (CD.next().offset().left + 22) - baseCoords.left;
                    y1 = (CI.next().offset().top + 22) - baseCoords.top;
                    y2 = (CD.next().offset().top + 22) - baseCoords.top;
                    lineas += "<line x1='"+x1+"' x2='"+x2+"' y1='"+y1+"' y2='"+y2+"'></line>";
                });
                $("svg").html(lineas);
            }
            if($("#contenidoActividad img").length>0){
                $("img").bind("load", solveActivity);
            }else{
                solveActivity();
            }
        break;
        //
        case "ListasDesplegables":
            $(".dropList").each(function(inidex, element){
                $(element).attr("t17")*1 === 0 ? "" : $(element).find("p:nth("+$(element).attr("t17")+")").insertBefore($(element).find("p:first"));
                $(element).find("p:first").addClass("optionSolved").removeClass("hidden");
            });
        break;
        //
        case "OpcionMultiple":
            $(".respuesta[t17='1']").addClass("OMSolved");
        break;
        //
        case "RespuestaMultipleFondo":
            $(".respuesta[t17='1']").addClass("RMFSolved");
        break;
        //
        case "FalsoVerdadero":
            $(".button").css({background:"grey", color: "white"});
            $(".button[t17='1']").css({background:"magenta"});
        break;
        //
        case "Matriz":
            $(".row").each(function(index, element){
                $.each($(element).attr("t17").split(","), function(i, e){
                    $(element).find(".answer:nth("+e+") .check").addClass("checkSolved");
                });
            });
        break;
        //
        case "Crucigrama":
            var palabra, nPregunta;
            $(".cell:has(label)").each(function(index, element){
                if($(element).attr("pregv")!==undefined){
                    nPregunta = $(element).attr("pregv")*1;
                    palabra = cambiarRutaImagen(json[preguntaActual].respuestas[verticales[nPregunta-1]].t13respuesta);
                    palabra = eliminarAcentos(palabra).toUpperCase();
                    $.each(palabra.split(""), function(i, e){
                        $(".cell[pregv='"+nPregunta+"']:nth("+i+")").find("input").val(e);
                    });
                }
                if($(element).attr("pregh")!==undefined){
                    nPregunta = $(element).attr("pregh")*1;
                    palabra = cambiarRutaImagen(json[preguntaActual].respuestas[horizontales[nPregunta-1]].t13respuesta);
                    palabra = eliminarAcentos(palabra).toUpperCase();
                    $.each(palabra.split(""), function(i, e){
                        $(".cell[pregh='"+nPregunta+"']:nth("+i+")").find("input").val(e);
                    });
                }
            });
            $("input").on("keydown keypress keyup", function(){
                return false;
            });
        break;
        //
        case "ArrastraImagen":
        case "ArrastraEtiquetas":
        var columnas = json[preguntaActual].pregunta.columnas === true;
        var selector, answer, t17;
            $(".etiqueta").each(function(index, element){
                t17 = $(element).attr("t17").split(",");
                answer = element.outerHTML;
                $.each(t17, function(i, e){
                    selector = columnas ? ".columna:nth("+e+") .contenedor" : ".contenedor:nth("+e+")";
                    $(answer).appendTo(selector);
                    $(selector).css("border-color", $(answer).css("background-color"));
                });
            });
            lastResort === "ArrastraImagen" ? $(".contenedor .etiqueta").css({backgroundColor:"magenta"})  : "";
        break;
        //
        case "ArrastraRespuestaMultiple":
            $(".columna").each(function(index, element){
                while($(element).find(".respuesta").length>1){
                    $(element).find(".respuesta:last").remove();
                }
                $(element).append("<p class='xCount'>x"+$(element).find(".respuesta").attr("cntdr")+"</p>");
            });
            $(".respuesta").css("right","80px");
        break;
    }
}