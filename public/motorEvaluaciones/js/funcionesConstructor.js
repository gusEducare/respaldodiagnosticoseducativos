var letrasArr = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
var idActividad="";
var rutaLibro;
var json;
var Evaluacion = false;
var iframe;
//parent.window.json = window.json
$(document).ready(function(){
    eventosHerramientas();
    eventosMenuGeneral();
    restaurarRecientes();
});
//////////////////eventos para las cajas de herramientas//////////////////////
function eventosHerramientas(){
    var menuHeight;
    $(".submenu.herramientas h1").on("click touchend", function(){
        var element = $(this).parent();
        menuHeight = $("#contenedorHerramientas").height() - ($(".submenu.herramientas").length * 36);
        $(".submenu.herramientas .contenedor").css({height:0, opacity:0});
        if(element.hasClass("selected")){
            element.removeClass("selected");
        }else{
            element.parent().find(".submenu.herramientas").removeClass("selected");
            element.addClass("selected");
            element.find(".contenedor").css({height:menuHeight+"px", opacity:1});
        }
    });
    $(window).resize(function(){
        menuHeight = $("#contenedorHerramientas").height() - ($(".submenu.herramientas").length * 36);
        $(".submenu.herramientas.selected .contenedor").css({height:menuHeight+"px", opacity:1});
    });
}
//////////////////////////Eventos del menu "General"////////////////////////////
function eventosMenuGeneral(){
    borrarRecientes();
    $("input[type='radio']").on("click touchend", function(){//eventos de los checkbox
        var element = $(this);
        element.parent().find("input[type='radio']").removeClass("selected");
        element.addClass("selected");
        Evaluacion = $("input[name='plataforma'].selected").attr("value")==="Evaluaciones";
        if(element.attr("name") === "plataforma"){
            $(".actividades.recientes li.liReciente").removeClass("selected");
            $(".subContenedor:contains(Nivel) li").removeClass("disabled");
            $(".subContenedor:contains(Clase) input").removeClass("disabled");
            buscarActividades();
        }else if(element.attr("name") === "tipoActividad"){
             obtenerNuevoId();
        }
        if(Evaluacion){
            $(".subContenedor:contains('Leccion')").addClass("disabled");
            $(".subContenedor:contains('Clase')").addClass("disabled");
            $(".subContenedor:contains('Nivel')").removeClass("disabled");
        }else{
            $(".subContenedor:contains('Clase')").removeClass("disabled");
            $(".subContenedor:contains('Leccion')").removeClass("disabled");
            $(".subContenedor:contains('Nivel')").addClass("disabled");
        }
    });
    eventosListas();
    var leccion;
    $("#leccionSelect").on("input", function(){//al cambiar de leccion con el slider
        leccion = letrasArr.charAt($(this).val());
        $("#leccion").html(leccion);
    });
    $("#buscarActividades").on("click touchend", buscarActividades);//buscar actividades
    //eventos del formulario
    $("#reloadActivity").on("click touchend", function(){
        if($("#actividad").attr("src")!==undefined){
            $("#actividad")[0].src += '';
        }
    });
    //eventos para la caja de texto del id de actividad
        $("#idActividad").on("paste",function(e){
            var text = e.originalEvent.clipboardData.getData('text');
               text.length === 12 ? validarIdActividad(text) : "";//se obtiene el id de la caja de texto
        }).keydown(function(event){
            if(event.which===13){
                var text=$(this).val()+"";
                text.length === 12 ? validarIdActividad(text) : "";//se obtiene el id de la caja de texto
            }
        });
    //eventos al seleccionar un id de actividad
    $("li.liActividad").on("click touchend", function(){
        var element = $(this);
        if(element.hasClass("nuevaAct")){
            $(".subContenedor:contains('Clase')").removeClass("disabled");
        }else{
            $(".subContenedor:contains('Clase')").addClass("disabled");
        }
    });
}
function eventosListas(){
    $(".slectMenu, .slectMenu li:not(selected)").off("click touchend");
    $(".slectMenu li:not(selected)").on("click touchend", function(){//eventos de los items en las listas desplegables
        var element = $(this);
        element.parent().find("li").removeClass("selected");
        element.addClass("selected");
        element.parent().attr("lastScroll", element.parent().scrollTop());
        element.parent().addClass("closed").hasClass("materia") ? ocultaGrados(element.attr("materia")) : "";
        return false;
    });
    $(".slectMenu").on("click touchend", function(){//muestra/oculta la liste de items del menu seleccionado
        var element = $(this);
        element.hasClass("closed") ? element.removeClass("closed") : element.addClass("closed");
        element.scrollTop(element.attr("lastScroll"));
        return false;
    });
}
function ocultaGrados(materia){
    $(".grado").parent().removeClass("disabled");
    var ocultaMenu = materia==="CN7" || materia==="CN8" || materia==="CN9" || materia==="PITG";
    if(ocultaMenu){
//        deshabilita el menu para aquellas materias que no tengas mas de un grado
        $(".grado").parent().addClass("disabled");
        var nGrado;
        if(materia!=="CN" && "CN7CN8CN9".indexOf(materia)!==-1){
            nGrado=materia.charAt("2");
        }else if(materia==="PITG"){
            nGrado="4";
        }
        $(".grado li").removeClass("selected").filter("li:contains("+nGrado+")").addClass("selected");
    }else{
        $(".grado").removeClass("disabled");
        var grados=[];
//        elige los grados que estaran visibles para cada materia
        materia === "CN" ? grados = ["Delta","Epsilon","Zeta"] : "";
        materia === "FCyE" ? grados = ["Delta","Epsilon","Zeta","Lambda","Pi"] : "";
        materia === "GEO" ? grados = ["Delta","Epsilon","Zeta","Kappa"] : "";
        materia === "HIST" ? grados = ["Delta","Epsilon","Zeta"] : "";
        materia === "TEC" ? grados = ["Kappa","Lambda","Pi"] : "";
        if(grados.length>0){
            $(".grado li[grado]").addClass("disabled");
            $.each(grados, function(i,e){
                $(".grado li[grado='"+e+"']").removeClass("disabled");
            });
        }
    }
}
///validacion de id de la actividad introducida en la caja de texto
function validarIdActividad(text){
        Evaluacion = text.charAt(3) === "E";
        var grado, bloque, leccion, tipoActividad;
        var letraMateria = text.charAt(0);
        var materia = text.substring(0, 3);
        grado = text.charAt(2);
        bloque = Evaluacion ? (text.substring(6,8)*1)-1 : text.charAt(6);
        leccion = letrasArr.indexOf(text.charAt(7).toUpperCase());
        tipoActividad = text.charAt(9);
        //verifica que los caracteres del id sean validos
            var esGrado, esBloque, esLeccion, esMateria, esTipo, esPlataforma, esBloqueLeccion;
            esGrado = $("li[grado]:contains("+grado+")").length>0;
            esBloque = $("li[bloque='"+bloque+"']").length>0;
            esLeccion = leccion!==-1 && leccion>0;
            esMateria = $("li[letra='"+letraMateria+"']").length>0;
            esTipo = $("input[tipoactividad='"+tipoActividad+"']").length>0 || Evaluacion;
            esPlataforma = "AE".indexOf(text.charAt(3))!==-1;
            esBloqueLeccion = "BS".indexOf(text.charAt(5))!==-1;
        if(esPlataforma && esBloqueLeccion && esGrado && esBloque && esLeccion && esMateria && esTipo){
            idActividad = text;
            $("#idActividad").removeClass("idIncorrecto");
            var materiasCn = {NI7:"CN7", NI8:"CN8", NI9:"CN9"};
            var selectorMateria = "li[letra='"+letraMateria+"']";
            materiasCn[materia] !== undefined ? selectorMateria = "li[materia='"+materiasCn[materia]+"']" : "";
            //se llena el formulario en base al id indicado
            $(selectorMateria).trigger("click");
            $(".grado li:contains("+grado+")").trigger("click");
            $(".bloque li[bloque='"+bloque+"']").trigger("click");
            if(Evaluacion){
                $("input[value='Evaluaciones']").trigger("click");
            }else{
                //autollenado del formulario
                $("#leccionSelect").val(leccion).trigger("input");
                $("input[value='Actividades']").trigger("click");
            }
        }else{
            $("#idActividad").addClass("idIncorrecto");
            console.log("id de actividad incorrecto");
            //se limpia el formulario
        }
}
//////////////Eventos paa cargar la actividad//////////////////////////////
function buscarActividades(){
    var materia, letraMateria, grado, nGrado, bloque, leccion="", nivel="", clase;
    materia = $("li[materia].selected").attr("materia");
    letraMateria = $("li[materia].selected").attr("letra");
    bloque = $("li[bloque].selected").attr("bloque");
    var idBusqueda;
    //define variables en caso de materias sin grados
    if(materia!=="CN" && "CN7CN8CN9".indexOf(materia)!==-1){
        nGrado=materia.charAt("2");
        materia = materia.slice(0, -1);
        grado = nGrado==="7" ? "Kappa" : nGrado==="8" ? "Lambda" : "Pi";
    }else if(materia==="PITG"){
        grado = "Delta";
        nGrado="4";
    }else{
        grado = $("li[grado].selected").attr("grado");
        nGrado = $("li[grado].selected").text();
    }
    //define variables independientes a cada plataforma
    if(Evaluacion){
        rutaLibro = "assets/resources/help/evaluaciones/"+materia+"/"+grado+"/";
        nivel = $(".slectMenu.nivel li.selected").attr("nivel");
        bloque++;
        (bloque+"").length===1 ? bloque="0"+bloque : "";
        idBusqueda = letraMateria+"I"+nGrado+"E"+"_B"+bloque;
    }else{
        leccion = $("#leccion").text();
        rutaLibro = "../"+materia+"/"+grado+"/";
        idBusqueda = letraMateria+"I"+nGrado+"A"+"_S"+bloque+leccion+nivel;
    }
    buscar();
    function buscar(){
        $(".slectMenu.actividades.buscar li.liActividad:not(.nuevaAct)").remove();
        $(".slectMenu.actividades.buscar li.liActividad.nuevaAct").addClass("selected");
        lastIdBusqueda = idBusqueda;
        $.ajax({
          url: rutaLibro,
          async:false,
          success: function(data){
              json = undefined;
              $(data).find("a:contains(.js)").filter("a:contains("+idBusqueda+")").each(function(index, element){
                  $(".slectMenu.actividades.buscar").append("<li class='liActividad'>"+$(element).attr("href").replace(".js","")+"</li>");
              });
              eventosListas();
              eventosActividad();
              if(idActividad.length===12 && idActividad.indexOf(idBusqueda)!==-1){
                  $(".actividades.buscar .liActividad:contains("+idActividad+")").trigger("click");
              }else{
                  idActividad = idBusqueda;
                  $(".actividades.buscar .liActividad:not(.nuevaAct):first").trigger("click");
              }
          }
        });
    }
    if(isNaN(leccion*1) && $(".slectMenu.actividades.buscar li").length<3){//busca en caso de que la leccion tenga letra minuscula
        var letraLeccion = idBusqueda.charAt(7).toLocaleLowerCase();
        idBusqueda = idBusqueda.slice(0,-1)+letraLeccion;
        buscar();
    }
    return false;
}
function borrarRecientes(){
    $("#borrarRecientes").on("click touchend", function(){
        $(".slectMenu.actividades.recientes li").not(".whitespace, .liReciente:first").remove();
        recientes = [$(".slectMenu.actividades.recientes li.liReciente").text()];
        localStorage.setItem("listaActividadesRecientes", JSON.stringify(recientes));
        return false;
    });
}
function eventosActividad(){
    $("li.liActividad.nuevaAct").removeClass("hidden noHeight");
    if(Evaluacion){
        $($("li.liActividad").length===4) ? $("li.liActividad.nuevaAct").addClass("hidden noHeight") : "";
    }
    $("li.liActividad").on("click touchend", function(){
        var element = $(this);
        var clase, nivel;
        $(".subContenedor:contains(Nivel) li").removeClass("disabled");
        $(".subContenedor:contains(Clase) input").removeClass("disabled");
        if(!element.hasClass("nuevaAct")){
            idActividad = element.text();
            if(Evaluacion){
                nivel = idActividad.charAt(11);
                $("ul.nivel li").removeClass("selected").filter("li[nivel='"+nivel+"']").addClass("selected");
                $(".subContenedor:contains(Nivel) li:not(.selected)").addClass("disabled");
            }else{
                clase = idActividad.charAt(9);
                $("input[tipoactividad").removeClass("selected").filter("input[tipoactividad='"+clase+"']").addClass("selected").prop("checked", true);
                $(".subContenedor:contains(Clase) input:not(.selected)").addClass("disabled");
            }
            //carga del la actividad
            $("#idActividad").val(idActividad);
        }else{
             obtenerNuevoId();
        }
        añadirRecientes();
        $("#contenedorActividad").html("<iframe "+(Evaluacion ? "class='evaluacion'" : "")+" id='actividad' src='./resources/index_Rev.html?id="+idActividad+"'></iframe>");
        json === undefined ? cargarMenuActividad() : "";
        return false;
    });
    $(".slectMenu.nivel li").on("click touchend", function(){
        if($("li.liActividad.nuevaAct").hasClass("selected")){
             obtenerNuevoId();
        }
    });
}
function obtenerNuevoId(){
    if($("li.liActividad.nuevaAct").hasClass("selected")){
        var subIdActividad = idActividad.substring(0,8);
        if(Evaluacion){
            idActividad = subIdActividad+"_N0"+$("li[nivel].selected").attr("nivel");
        }else{
            var siguienteActividad = ($(".slectMenu.actividades.buscar li").length-1)+"";
            siguienteActividad.length===1 ? siguienteActividad="0"+siguienteActividad : "";
            idActividad = subIdActividad+"_"+$("input[tipoactividad].selected").attr("tipoactividad")+siguienteActividad;
        }
    }
    if($("li.liActividad:contains("+idActividad+")").length>0){
        $("li.liActividad:contains("+idActividad+")").trigger("click");
    }else{
        $("#idActividad").val(idActividad);
        validarIdActividad(idActividad);
        
    }
}
var recientes=[];
function añadirRecientes(){
    $(".actividades.recientes li.liReciente").removeClass("selected");
    if($(".actividades.recientes li.liReciente:contains("+idActividad+")").length>0){
        var i2Move = recientes.indexOf(idActividad);
        if(recientes.length>1 && i2Move>0){//mueve el id al principio de la lista
            recientes.move(i2Move, 0);
        }
    }else{
        if($(".slectMenu.actividades.recientes li").length>30){
            $(".slectMenu.actividades.recientes li:last").remove();
            recientes = recientes.slice(0, -1);
        }
        recientes.unshift(idActividad);
        $(".slectMenu.actividades.recientes").html("");
        $.each(recientes, function(i, e){
            $(".slectMenu.actividades.recientes").append("<li class='liReciente'>"+e+"</li>");
        });
    }
    localStorage.setItem("listaActividadesRecientes", JSON.stringify(recientes));
    $(".actividades.recientes li.liReciente:contains("+idActividad+")").addClass("selected");
    $(".actividades.recientes li.liReciente").off("click touchend");
    $(".actividades.recientes li.liReciente").on("click touchend", function(){
        var element = $(this);
        if(idActividad!==element.text()){
            element.parent().addClass("closed").find("li").removeClass("selected");
            element.addClass("selected");
            validarIdActividad(element.text());
            var li = $(".actividades.buscar .liActividad:contains("+idActividad+")");
            li.length === 0 ? $(".actividades.buscar .liActividad.nuevaAct").trigger("click") : "" ;
        }
        return false;
    });
}
function restaurarRecientes(){
    var recientesStorage = localStorage.getItem("listaActividadesRecientes");
    recientesStorage!==null ? recientes = JSON.parse(recientesStorage) : "";
    $.unique(recientes);
    $.each(recientes, function(i, e){
        $(".slectMenu.actividades.recientes").append("<li class='liReciente'>"+e+"</li>");
    });
    if(recientes.length>0){
        validarIdActividad(recientes[0]);
        var li = $(".actividades.buscar .liActividad:contains("+idActividad+")");
        li.length === 0 ? $(".actividades.buscar .liActividad.nuevaAct").trigger("click") : "" ;
    }else{
        buscarActividades();
    }
}
//mueve elementos en un arrary
Array.prototype.move = function(from, to) {
    this.splice(to, 0, this.splice(from, 1)[0]);
};
///////////////////////Eventos del menu "Pantallas"////////////////////////////
function cargarMenuActividad(){
    var listaTipos = "<ul class='slectMenu closed tiposActividad'>\
                        <li tipo='OpcionMultiple' class='selected'>Opcion Multiple</li>\n\
                        <li tipo='Test'>OM: test</li>\n\
                        <li tipo='OpcionMultiple'>Respuesta múltiple</li>\n\
                        <li tipo='RespuestaMultipleFondo'>RM: fondo con imágen</li>\n\
                        <li tipo='FalsoVerdadero'>Falso o verdadero</li>\n\
                        <li tipo='ListasDesplegables'>Listas desplegables</li>\n\
                        <li tipo='ArrastraVertical'>AC: vertical</li>\n\
                        <li tipo='ArrastraHorizontal'>AC: horizontal</li>\n\
                        <li tipo='ArrastraMatriz'>AC: matriz</li>\n\
                        <li tipo='ArrastraMatrizHorizontal'>AC: matriz horizontal</li>\n\
                        <li tipo='ArrastraOrdenar'>AC: ordenar</li>\n\
                        <li tipo='ArrastraImagen'>AC: imágen</li>\n\
                        <li tipo='ArrastraImagenes'>AC:imagenes</li>\n\
                        <li tipo='ArrastraContenedorUnico'>AC: contenedor único</li>\n\
                        <li tipo='ArrastraRespuestaMultiple'>AC: respuesta multiple</li>\n\
                        <li tipo='PreguntaAbierta'>Pregunta abierta</li>\n\
                        <li tipo='ArrastraEtiquetas'>Arrastra etiquetas</li>\n\
                        <li tipo='ArrastraCorta'>Arrastra corta</li>\n\
                        <li tipo='MapasDeBits'>Mapas de bits</li>\n\
                        <li tipo='Graficas'>Graficas de barras</li>\n\
                        <li tipo='RelacionaLineas'>Relaciona lineas</li>\n\
                        <li tipo='Matriz'>Matriz</li>\n\
                        <li tipo='SopaLetrasV2'>Sopa de letras</li>\n\
                        <li tipo='Crucigrama'>Crucigrama</li>\n\
                    </ul>";
    $("#actividad").on("load", function(){
        iframe = $("#actividad").off("load")[0].contentWindow;
        json = iframe.json;
        var numeroActividad;
        $("#listaPantallas .subContenedor").remove();
        $.each(json, function(i, e){
            numeroActividad = iframe.obtenerTipoActividad(i);
            tipoActividad = iframe.recursoActividad[numeroActividad];
            $("#listaPantallas").append("<div class='subContenedor'>"+(i+1)+". "+listaTipos+"<div class='button remove'></div></div>");
            seleccionPantalla($("#listaPantallas .subContenedor:last"));
            console.log(tipoActividad);
        });
        animacionListas($("#listaPantallas .tiposActividad li"));
    });
}
function seleccionPantalla(element){
    element.find(".remove").on("click touchend", function(){
        console.log("Holi remove");
        return false;
    });
}
function animacionListas(elements){
    elements.on("click touchend", function(){//eventos de los items en las listas desplegables
        var element = $(this);
        element.parent().find("li").removeClass("selected");
        element.addClass("selected");
        element.parent().attr("lastScroll", element.parent().scrollTop());
        element.parent().addClass("closed").hasClass("materia") ? ocultaGrados(element.attr("materia")) : "";
        return false;
    });
    elements.parents(".slectMenu").on("click touchend", function(){//muestra/oculta la liste de items del menu seleccionado
        var element = $(this);
        element.parents(".contenedor").find(".subContenedor").css({zIndex:0}).find(".slectMenu").not(element).addClass("closed");
        element.parents(".subContenedor").css({zIndex:10});
        element.hasClass("closed") ? element.removeClass("closed") : element.addClass("closed");
        element.scrollTop(element.attr("lastScroll"));
        return false;
    });
}
//$("iframe")[0].contentWindow.json
//$("iframe")[0].contentWindow.iniciarActividad()
//$("#actividad")[0].contentWindow.lastResort