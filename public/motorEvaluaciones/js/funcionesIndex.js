var materias={"C":"Formacion", "F":"Finanzas", "G":"Geografía", "H":"Historia", "I":"Tecnologia", "M":"Matematicas", "N":"Naturales", 
    "NI7":"Biología", "NI8":"Física", "NI9":"Química", "R":"Robotopia", "S":"Español", "T":"TecSEP"};
var letraSesion="0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
var letraSesionMin="0123456789abcdefghijklmnñopqrstuvwxyz";
var actividadActual=0;
var materia;
var grado;
var bloque;
var leccion;
var pseudoMateria;
var nombreMateria;
var idActividad;
$(document).ready(function(){
    init();
    var location = window.location.search;
    if(location.indexOf("?id=") !== -1){
        var lastLocation = $("input[type='text']").val();
        if(lastLocation !== location){
            cargarActividad(location);
        }else{
            location.reload();
        }
    }
    location = window.location.pathname;
    if(idActividad.charAt(5) === "B" && idActividad.charAt(9) === "N"){
        $(".handleFrame").remove();
        $("#dragFrame").addClass("frameEval");
    }
    
    $("#idActividad").click(function(){
        $("input[type='text']").focus();
    });
    $("#idActividad input[type='text']").on("paste",function(e){
        var text = e.originalEvent.clipboardData.getData('text');
           text.length === 12 ? compararId(text) : "";//se obtiene el id de la caja de texto
    }).keydown(function(event){
        if(event.which===13){
            var text=$(this).val()+"";
            text.length === 12 ? compararId(text) : "";//se obtiene el id de la caja de texto
        }
    });
});

function compararId(text){
        if(text.length===12){
            idActividad = text;
           obtenerDatosId();
           validarIdActividad();
        }else{
            $("#notificacionesIndex").html("");
            $("#idActividad input[type='text']").css({color:"grey"});
        }    
}

function obtenerDatosId(){
//    NI4A_S22_A01
    //variable titulo de actividad
    idActividad = idActividad.toUpperCase();
    materia = idActividad.charAt(0);
    grado = idActividad.charAt(2)*1;
    bloque = (idActividad.charAt(6)*1)+1;
    leccion = (idActividad.charAt(7));
    idActividad = idActividad.substring(0,7)+letraSesionMin.charAt(letraSesion.indexOf(leccion))+idActividad.substring(8, idActividad.length);
    leccion = letraSesion.indexOf(leccion) +1;

    pseudoMateria = idActividad.substring(0, 3);
    nombreMateria = materias[pseudoMateria] !== undefined ? materias[pseudoMateria] : materias[materia] !== undefined ? materias[materia] : "";
}
function validarIdActividad(){
    var location;
    //validacion del id de actividad introducido
    var esMateria = nombreMateria!=="" ? true : false;
    var esGrado = (grado+"").search("NaN")===-1 && grado > 3 && grado < 10 ? true : false;
    var esBloque = (bloque+"").search("NaN")===-1 && bloque > 0 && bloque < 6 ? true : false;
    var esLeccion = leccion > 0 ? true : false;
    var esActividad;
        //verifica id de actividad
        esActividad = (idActividad.charAt(9) === "A" || idActividad.charAt(9) === "I" || idActividad.charAt(9) === "E" || idActividad.charAt(9) === "V") && idActividad.charAt(5) === "S" ? true : false;
        //verifica id de evaluacion
        esActividad = esActividad === false && idActividad.charAt(5) === "B" && idActividad.charAt(9) === "N" ? true : esActividad;
        if(esMateria && esGrado && esBloque && esLeccion && esActividad){//se valida el id de la actividad introducida
            //carga el contenido
            var winLocation = window.location.pathname;
            location = winLocation.indexOf("revisionProf") !== -1 ? "revisionProf" : winLocation.indexOf("revisionEval") !== -1 ? "revisionEval" :  "revision002";
            window.location = "../Libro/"+location+".html?id="+idActividad;
        }else{
            $("#idActividad input[type='text']").css({color:"OrangeRed"});
            $("#notificacionesIndex").html("Id de actividad incorrecto").css({color:"OrangeRed"});
            $('iFrame').attr("src","");
        }    
}
function cargarActividad(location){
    var id = location.split("?id=").pop();
    idActividad = id;
    obtenerDatosId();
    $("#idActividad input[type='text']").css({color:"LightSkyBlue"});
    $("#notificacionesIndex").html(nombreMateria+" "+grado+" | Bloque "+bloque+" | Lección "+leccion);
    $("input[type='text']").val(id).css({color:"LightSkyBlue"});
    var location = window.location.pathname.indexOf("revisionProf") !== -1 ? "index_Prof" : "index_Rev";
    $('iFrame').attr("src","resources/"+location+".html?id="+id);
}