json=[  
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":"<p>Contraposición.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Combate.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Conformar.</p>" },
         {   "t17correcta":"1","t13respuesta":"<p>Conciliación.</p>" }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":"<p><strong>Confrontación, conflicto, contienda</strong>. ¿Cuál de las siguientes palabras que se presentan es diferente a las anteriores?</p>"
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":"<p>Se parecen en: la flora y la fauna, los océanos con los que colindan, los idiomas que hablan, en el huso horario que manejan y pertenecen a la ONU. Son diferentes en: que se encuentran en continentes distintos, los climas que tienen y la distancia con respecto al Ecuador.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Se parecen en: los climas que tienen, son atravesados por el mismo meridiano y el mismo paralelo, en ambos se habla inglés y pertenecen a la ONU. Son diferentes en: las regiones naturales que poseen, los idiomas oficiales, el huso horario que manejan, los océanos con los que colindan y los continentes en los que están.</p>" },
         {   "t17correcta":"1","t13respuesta":"<p>Se parecen en: que son atravesados por el mismo paralelo, tienen los mismos tipos de clima, el segundo idioma más hablado es el inglés y pertenecen a la ONU. Son diferentes en: los continentes en que se encuentran, sus idiomas oficiales, en los océanos con que colindan y en los husos horarios que manejan.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Se parecen en: que colindan con los mismos océanos, hablan los mismos idiomas, son atravesados por el mismo meridiano y están en los mismos continentes. Son diferentes en: que están a distinta distancia del Ecuador, tienen diferentes climas, no se habla inglés en uno de ellos y no pertenecen a la ONU.</p>" }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":'<p lang="en-US" style="text-align: justify;" align="justify" data-mce-style="text-align: justify;"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">Lee el siguiente texto:</span></span></span></p><p lang="en-US" style="text-align: justify;" align="justify" data-mce-style="text-align: justify;"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">México y la India son países que se encuentran en continentes diferentes, el primero está en América&nbsp;</span></span></span><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">y el segundo en Asia, pero tienen algunos elementos en común. Ambos son países que tienen mucha&nbsp;</span></span></span>diversidad en su flora y su fauna. Esto se debe a que ambos son atravesados por el mismo paralelo, lo que quiere decir que tienen la misma distancia con respecto al Ecuador (línea que divide el planeta en dos hemisferios). Que se encuentren a la misma altura implica que tengan los mismos tipos de climas: templados, cálidos y secos.</p><p lang="en-US" style="text-align: justify;" align="justify" data-mce-style="text-align: justify;"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">En México el idioma oficial es el español, en India el hindi, pero en los dos el segundo idioma más&nbsp;</span></span></span>hablado es el inglés. En cuanto a sus fronteras naturales, mientras India colinda con el Océano Índico, México limita con el Pacífico y el Atlántico. Otro dato interesante es que sus habitantes no duermen al mismo tiempo, ya que se ubican en diferentes meridianos (líneas que atraviesan el Ecuador) y no coinciden sus husos horarios.</p><p lang="en-US" align="justify"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">Como muchos otros países, ambos pertenecen a la Organización de las Naciones Unidas.</span></span></span></p><p><strong>¿En qué se parecen México e India y en qué son diferentes?</strong></p>'
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":"<p>La diosa Panacea apoyaba a su padre en la búsqueda de remedios.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>El descubrimiento de la panacea sería de gran ayuda para la humanidad.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Panacea era hija de Asclepio y Epíone, tenía hermanos y junto con ellos ayudaba a su padre.</p>" },
         {   "t17correcta":"1","t13respuesta":"<p>Panacea significa remedio para todo.</p>" }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":"<p>Lee el siguiente texto:</p><p>Panacea es una palabra larga, proviene de vocablo griego “panákeia” que evoca a una diosa griega cuyo nombre, a su vez, proviene de las raíces pan (que significa todo) y akos (que significa remedio). De ahí que el término signifique: “remedio para todo” o “remedio universal”. Para tener una idea más clara del significado de esta palabra, es importante conocer que la diosa Panacea era hija de Asclepio, dios que se dedicaba a la medicina, y de Epíone o Lampetia. Panacea, junto con sus hermanos, ayudaba a su padre en la creación de medicinas. Por ello durante mucho tiempo, y especialmente durante la Edad Media, que se buscaba la medicina que curara todos los males, se le llamaba a este proceso: “la búsqueda de la panacea”. Definitivamente, si la panacea fuera descubierta, a todos nos gustaría recibir ese jarabe.</p><p><strong>¿Cuál es la idea central del texto?</strong></p>"
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":"<p>Salir de la escuela, llevarse la tarea, visitar a su abuelita, tomar su comida favorita y gelatina de chocolate, salir a jugar.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Salir de la escuela, pasar a saludar a su abuelita, regresarse solo, pasar a la lavandería, comer y salir a jugar.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Salir de la escuela, esperar las vacaciones, visitar a su abuelita, pasar a la lavandería, tomar su comida favorita y gelatina de chocolate, salir a jugar.</p>" },
         {   "t17correcta":"1","t13respuesta":"<p>Salir de la escuela, visitar a su abuelita, pasar a la lavandería, tomar su comida favorita y helado, salir a jugar.</p>" }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":'<div class="imgsAnt"><p lang="en-US" align="justify"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">Lee el siguiente texto:</span></span></span></p><p lang="en-US" align="justify"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">Hoy Leonardo ha salido muy contento de la escuela. Ayer no fue así ya que tuvo mucha tarea, pero por fin acabó y con ansias esperaba llegar a estas vacaciones. Ahora le espera una agradable tarde.</span></span></span></p><p lang="en-US" align="justify"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">Lo primero que hace es pasar a saludar a su abuelita, igual que lo ha hecho todos los días desde que regresa solo de la escuela. Recordó que ya lleva dos años haciéndolo y que el primer día se sintió muy orgulloso de haberse ganado la confianza de su mamá. Al salir de la casa de su abuelita también pasa por otro encargo: recoger en la lavandería unas prendas que olvidó ayer con las prisas por llegar a casa.</span></span></span></p><p lang="en-US" align="justify"><span style="color: #000000;" data-mce-style="color: #000000;"><span style="font-family: Futura Bk;" data-mce-style="font-family: Futura Bk;"><span style="font-size: small;" data-mce-style="font-size: small;">Al llegar a su casa disfruta de su comida favorita, la que pide cada vez que hay un evento especial, y aunque olvidó recordarle a su mamá que su postre favorito es la gelatina de chocolate, disfruta plenamente del helado que le compró como sorpresa. Y por fin llega el momento de salir a jugar, hoy no tiene deberes como ayer y pasará la tarde con sus amigos de la cuadra.</span></span></span></p></div><p><strong>¿Qué hizo Leonardo en este relato?</strong></p>'
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":"<p>Camila.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Axel.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Lili.</p>" },
         {   "t17correcta":"1","t13respuesta":"<p>Vero.</p>" }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":"<p>En la carrera final de la olimpiada escolar, los competidores han recorrido las siguientes fracciones de la pista: <strong>David 1/2, Vero 4/5, Axel 6/10, Lili 2/4 y Camila 5/10. </strong></p><p><strong>¿Quién va en primer lugar?</strong></p>"
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-A.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-A.jpg"></div>' },
         {   "t17correcta":"1","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-B.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-B.jpg"></div>' },
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-C.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-C.jpg"></div>' },
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-D.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14-D.jpg"></div>' }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":'<div class="imgsAnt">¿Cuál de las siguientes fracciones corresponde al grupo mostrado en la imagen?</div><div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIMRV14~pag14.jpg"></div><p><br><br></p>'
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":"<p>El que no planifica su tiempo probablemente lo destina para cosas innecesarias.</p>" },
         {   "t17correcta":"1","t13respuesta":"<p>Siempre acudimos a lo que nos apremia y no le dedicamos tiempo a lo verdaderamente importante.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Si supiéramos el valor del tiempo, aprovecharíamos más cada instante sin negarnos a nada.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Quien atiende los pendientes sabe cuánto tiempo tiene para disfrutar de la vida.</p>" }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":'<p>¿A qué se refiere Quino con su frase: <strong>"Como siempre: lo urgente no deja tiempo para lo importante"</strong>?</p>'
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":"<p>Andrés.</p>" },
         {   "t17correcta":"1","t13respuesta":"<p>Juana.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Carlos.</p>" },
         {   "t17correcta":"0","t13respuesta":"<p>Liliana.</p>" }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":"<p>Liliana, Juana, Andrés y Carlos son cuatro amigos que son atletas. Carlos siempre es más veloz que Liliana. Andrés es más rápido que Liliana pero menos que Juana, y Juana es mucho más veloz que Carlos. En la competencia del martes, <strong>¿quién crees que será el&nbsp;más veloz?</strong></p>"
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-A.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-A.jpg"></div>' },
         {   "t17correcta":"1","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-B.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-B.jpg"></div>' },
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-C.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-C.jpg"></div>' },
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-D.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM32~RM_6oPri_32_1-D.jpg"></div>' }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":"<p>Observa los relojes mostrados en la imagen. El reloj que con sus manecillas forma un ángulo de 90º es:</p>"
      }
   },
   
   {  
      "respuestas":[  
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-A.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-A.jpg"></div>' },
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-B.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-B.jpg"></div>' },
         {   "t17correcta":"1","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-C.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-C.jpg"></div>' },
         {   "t17correcta":"0","t13respuesta":'<div class="imgsAnt"><img src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-D.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1-D.jpg"></div>' }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1", "t11pregunta":'<div>Observa la imagen:</div><div class="imgsAnt" style="text-align: center;" data-mce-style="text-align: center;"><strong>Si en</strong>&nbsp;<img src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_1.jpg">&nbsp;<strong>hay 24</strong>&nbsp;<img src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_2.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_2.jpg"></div><div class="imgsAnt" style="text-align: center;" data-mce-style="text-align: center;"><strong>Entonces con 360&nbsp;</strong><img src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_2.jpg" alt="" data-mce-src="http://certificacionestics.com/img/examenes/I6PRIM54~RM_6oPri_54_2.jpg"><strong>, tendremos:</strong></div><p>&nbsp;</p>'
      }
   }
];

//             1       2        3       4      5        6       7       8      9       10
correctas = ["0001", "0010", "0001", "0001", "0001", "0100", "0100", "0100", "0100", "0010"];