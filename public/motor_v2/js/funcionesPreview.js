var parentData;
var noEvaluable
var url = window.location.href;
var idActividad = url.split("?id=")[1];
var strAmbiente = url.indexOf("Libro")!==-1 ? "DEV" : "PROD";
var evaluacion = idActividad.charAt(3)==="E" && idActividad.charAt(5)==="B" && idActividad.charAt(9)==="A";
var rutaLibro, bookColor, bookColor2;
var libroProf = false, setProfMode = false;
var lang = "es";
if(strAmbiente === "DEV"){
    document.addEventListener("DOMContentLoaded", function(event){
        parentLoad(event);
    });
}else{
    window.addEventListener("message", function(event){ parentLoad(event); }, false);
}
function parentLoad(event){
//    window.onerror = function(msg, url, line, col, error) {//help to debug in mobile
//      // do something clever here
//      var extra = !col ? '' : '\ncolumn: ' + col;
//       extra += !error ? '' : '\nerror: ' + error;
//      document.getElementById("previewActividad").innerHTML += "Error: " + msg + "\nurl: " + url + "\nline: " + line + extra;
//    };
    if(event !== undefined && event.data !== undefined){
        parentData = event.data;
        lang = parentData.lang;
    }
    if(lang === "en"){
        document.getElementById("txtIniciar").innerHTML = "Play";
        document.querySelector("label.lbl_cal").innerHTML = "Score";
        var lblAttemps = document.querySelector("label.lbl_attemps");
        lblAttemps.innerHTML = lblAttemps.innerHTML .replace("Intentos:", "Attempts:");
    }
    //obtiene el tipo de actividad
    rutaActividad();
    importScript(rutaLibro, idActividad, "Json", function(){//importa json
        //ejecuta eventos //DEV - PROD
        var tipo = tiposActividad[json[0].pregunta.c03id_tipo_pregunta];
        var tipoAc = obtenerTipoActividad(0);
        noEvaluable = tipoAc === "1.2" || json[0].pregunta.evidencio === false;
        aplicarColores(tipo, parentData);
        if(strAmbiente === "DEV" || evaluacion){//salta la vista previa en caso de ser evaluacion
            init();
            document.querySelector("#previewActividad").className = "hidden";
        }else{
            setProfMode = parentData !== undefined && parentData.setProfMode === true;
            //aplica estilos segun tipo de actividad
            document.querySelector("#previewActividad").style.backgroundImage = "url(img/preview/"+tipo.tipo+".png)";
            libroProf = parentData !== undefined && parentData.book_ID.charAt(3) === "P";
            previewActividad(parentData);//vista previa
        }
    });
}
function rutaActividad(){
    var materias={"C":"FCyE", "F":"PEF", "G":"GEO", "H":"HIST", "I":"PITG", "M":"MAT", "N":"CN", "R":"RTP", "S":"ESP", T:"TEC", "E":"ING"};
    var grados={"1":"Alfa","2":"Beta","3":"Gamma","4":"Delta", "5":"Epsilon", "6":"Zeta", "7":"Kappa", "8":"Lambda", "9":"Pi", "10":"A", "11":"B", "12":"C"};
    //obtener ruta del objeto json
    var materia = idActividad.charAt(0);
    var grad = idActividad.charAt(2);
    var grado = "";
  
    if(grad === 'A' || grad === 'B' || grad === 'C'){
        if(grad === 'A'){
            grado = 10;
            
        }else if(grad === 'B'){
            grado = 11;
            
        }else if(grad === 'C'){
            grado = 12;
        }
        
    }else{
        grado = idActividad.charAt(2)*1;
    }

    if(strAmbiente === "DEV"){
        var location = url.split("Libro/")[0].split("?id=")[0];
        if(location !== ""){
            rutaLibro = location+materias[materia]+"/"+grados[grado]+"/";
        }else{
            rutaLibro = "../../"+materias[materia]+"/"+grados[grado]+"/";
        }
    }else{
        var location = url.split("index_rev.html")[0].split("?id=")[0];
        if(evaluacion){
            rutaLibro = location+"examenes/"+materias[materia]+"/"+grados[grado]+"/";
        }else{
            rutaLibro = location;
        }
    }
    
}
function previewActividad(data){//establece una vista previa de la actividad
    //evita cargar los recursos de la actividad hasta que el usuario lo indique
    var t1=0, t2=0, dc1=0, dc2=0;
    var calificacion = 0, attemps = 0;
    if(strAmbiente === "PROD" && !libroProf){
        calificacion = data.calificacion;
        attemps = data.attemps;
    }
    if(attemps > 0 && !noEvaluable){
        document.querySelector("#previewActividad").className = "ly2";
        calificacion += "";//calificacion obtenida
        if(calificacion.indexOf(".")!==-1){//define la longitud de cifras
            var fix = (calificacion+"").split(".")[1].length > 1 ? 2 : 1;
            calificacion = (calificacion*1).toFixed(fix);
        }
        calificacion > 9.99 ? calificacion = 10 : "";
        calificacion < 0 ? calificacion = 0 : "";
        var crecimiento = parseInt(calificacion * 36);
        //dc1 y dc2 diferencia de crecimiento //t1 y t2 tiempos que tendra la transicion
        var left, right;
        if(crecimiento > 180){
            dc1="0";
            t1 = "0.75";
            dc2 = crecimiento - 360;
            t2 = ((180+dc2)/250).toFixed(2);//250 = 1000 - 750 // 1000 = 1s //750 = 0.75s
            setTimeout(function(){
                left = document.querySelector(".left>.half.brdr_bookColor2");
                left.style.transition = t2+"s linear";
                left.style.transform = "rotate("+dc2+"deg)";
            }, (t1*1000)+10);
        }else{
            dc1 = crecimiento - 180;
            t1 = ((180+dc1)/250).toFixed(2);
        }
        right = document.querySelector(".right>.half.brdr_bookColor2");
        right.style.transition = t1+"s linear";
        document.querySelector(".lbl_attemps strong").innerHTML = attemps;
        document.querySelector("aside>.btn_cal p").innerHTML = calificacion+"";
        setTimeout(function(){ right.style.transform = "rotate("+dc1+"deg)"; }, 10);
    }
    document.querySelector("aside .btn.btn_start").addEventListener("touchend", function(){ click2Start(this); });
    document.querySelector("aside .btn.btn_start").addEventListener("click", function(){ click2Start(this); });
    function click2Start(element){
        element.style.pointerEvents = "none";
        setTimeout(function(){
            element.className += " loading";
            var loading = document.querySelector(".loading .loading");
            var loadBar = document.querySelector(".loading .loading>div");
            var contador = 0;
            loading.style.transform = "rotate("+(contador+=360)+"deg)";//contenedor barra de avance
            loadingInterval = window.setInterval(function(){
                loading.style.transform = "rotate("+(contador+=360)+"deg)";
            }, 2000);
            loadBar.style.transform = "rotate(0deg)";//barra de avance
            setTimeout(function(){
                loadBar.style.transform = "rotate(180deg)";
            }, 1250);
            loadingBarInterval = window.setInterval(function(){
                loadBar.style.transform = "rotate(0deg)";
                setTimeout(function(){
                    loadBar.style.transform = "rotate(180deg)";
                }, 1250);
            }, 2500);
            
        }, 250);
        init();//inicia la carga de la actividad
        return false;
    }
}
var loadingInterval, loadingBarInterval;
function init(){//aqui comienza toda la magia :3
    importScript("js/", "jquery-3.2.0.min", "JQuery", function(){//carga jquery
        importScript("js/", "jquery-ui.min", "JQueryUI", function(){//carga la ui de jquery
            importScript("js/", "fixMouseTouch", "fixTouch", function(){//traduce enventos touch/mouse
                initTouch();
                importCss("css/", "estilosGenerales", "Generales", true, function(){fGenerales();});//carga funciones generales
            });
        });
    });
}
function aplicarColores(tipo, data){
    //aplica book color
    if(data !== undefined){
        bookColor = data.book_color;
        bookColor2 = data.secundario;
    }
    bookColor = bookColor === undefined ? "#672273" : bookColor;
    bookColor2 = bookColor2 === undefined ? "#83cddc" : bookColor2;
    var css, textCss;
    textCss = document.createTextNode("\
                .bg_bookColor2 {background-color: "+bookColor2+" !important;}\n\
                .bg_bookColor {background-color: "+bookColor+" !important;}\n\
                .brdr_bookColor2 {border-color: "+bookColor2+" !important;}\n\
                .brdr_bookColor {border-color: "+bookColor+" !important;}\n\
                .clr_bookColor2 {color: "+bookColor2+" !important;}\n\
                .clr_bookColor {color: "+bookColor+" !important;}\n\
            ");
    css = document.createElement("style");
    css.appendChild(textCss);
    document.head.appendChild(css);
}
//////////////////////////////Funciones para importar archivos////////////////
var args;
function importCss(ruta, archivo, id, js, arguments){//funcion para importar css
//    si requiere que importe un archivo js, la variable js debe ser true y en arguments debera
//    incluir el codigo a ejecutar tras cargar el script
    args = arguments;
    //elimina el css si ya existe
    var oldCss = document.getElementById("css"+id);
    oldCss!== null ? document.head.removeChild(oldCss) : "";
    //añade el nuevo css
    var css = document.createElement("link");
    css.rel = "stylesheet";
    css.type = "text/css";
    css.id = "css"+id;
    css.href = ruta+archivo+".css";
    css.addEventListener('load', function(){
        js ? importScript(ruta.replace("css", "js"), archivo.replace("estilos", "funciones"), id, args) : "";
    });
    document.head.insertBefore(css, document.head.childNodes[0]);
}
function importScript(ruta, archivo, id, arguments){//funcion para cargar dinamicamente archivos js
//    arguments debe estar definido como una funcion, la cual se ejecutara al cargar el script
    args = arguments;
    //elimina el escript si ya existe
    var oldScript = document.getElementById("scr"+id);
    oldScript!==null ? document.head.removeChild(oldScript) : "";
    //añade el nuevo script
    var script = document.createElement("script");
    script.src = ruta+archivo+".js";
    script.id = "scr"+id;
    script.onload = args;
    document.head.appendChild(script);
}

var tiposActividad = {
     1:{ tipo:"OpcionMultiple",     titulo:"Respuesta única",     variantes:{1.1: "OpcionMultiple", 1.2: "Test"} },
     2:{ tipo:"OpcionMultiple",     titulo:"Respuesta múltiple",  variantes:{2.1: "OpcionMultiple", 2.2: "RespuestaMultipleFondo"} },
     3:{ tipo:"FalsoVerdadero",     titulo:"Falso-Verdadero",     variantes:{3:   "FalsoVerdadero"} },
     4:{ tipo:"ListasDesplegables", titulo:"Listas desplegables", variantes:{4:   "ListasDesplegables"} },
     5:{ tipo:"ArrastraContenedor", titulo:"Arrastrar y soltar",  variantes:{5.1: "ArrastraVertical", 5.2: "ArrastraHorizontal", 5.3: "ArrastraMatriz", 5.4: "ArrastraMatrizHorizontal", 5.5: "ArrastraOrdenar", 5.6: "ArrastraImagen", 5.7: "ArrastraImagenes", 5.8: "ArrastraContenedorUnico", 5.9: "ArrastraRespuestaMultiple"} },
     6:{ tipo:"PreguntaAbierta",    titulo:"Respuesta libre",     variantes:{6:   "PreguntaAbierta"} },
     7:{ tipo:"ArrastraContenedor", titulo:"Arrastrar y soltar",  variantes:{7:   "ArrastraEtiquetas"} },
     8:{ tipo:"ArrastraContenedor", titulo:"Arrastrar y soltar",  variantes:{8:   "ArrastraCorta"} },
     9:{ tipo:"ArrastraContenedor", titulo:"Arrastrar y soltar",  variantes:{9:   "OrdenaElementos"} },
    10:{ tipo:"Graficas",           titulo:"Graficas",            variantes:{10:  "Graficas"} },
    11:{ tipo:"Calculadora",        titulo:"Calculadora",         variantes:{11:  "Calculadora"} },
    12:{ tipo:"RelacionaLineas",    titulo:"Relaciona-Lineas",    variantes:{12:  "RelacionaLineas"} },
    13:{ tipo:"Matriz",              titulo:"Matriz",              variantes:{13:  "Matriz"} },
    14:{ tipo:"MapasDeBits",        titulo:"Mapa de trazos",      variantes:{14:  "MapasDeBits"} },
    15:{ tipo:"Crucigrama",         titulo:"Crucigrama",          variantes:{15:  "Crucigrama"} },
    16:{ tipo:"SopaLetras",         titulo:"Sopa de letras",      variantes:{16:  "SopaLetrasV2"} },
    17:{ tipo:"Mosaico",            titulo:"Mosaico",             variantes:{17:  "Mosaico"} },
    18:{ tipo:"RelacionaLineas",    titulo:"Relaciona-Lineas",    variantes:{18:  "RelacionaLineas"} }
};
function obtenerTipoActividad(numPregunta){
    var numActividad = json[numPregunta].pregunta.c03id_tipo_pregunta;
    var tipoActividad=false;
    if(numActividad+""==="5"){//se obtine el subtipo Arratra contenedor
        tipoActividad = json[numPregunta].pregunta.tipo === "vertical" ? "5.1" : tipoActividad;//Arrastra vertical
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "horizontal" ? "5.2" : tipoActividad;//Arrastra Horizontal
        tipoActividad = tipoActividad===false && json[numPregunta].contenedores !== undefined && json[numPregunta].contenedoresFilas !== undefined && json[numPregunta].pregunta.tipo === undefined ? "5.3" : tipoActividad;//Arrastra matriz
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "matrizHorizontal" ? "5.4" : tipoActividad;//Arrastra matriz horizontal
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.respuestaUnicaMultiple === true ? "5.9" : tipoActividad;//Arrastra cerdito
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].contenedores.length === 1 ? "5.8" : tipoActividad;//Arrastra acontenedor unico
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].pregunta.imagen === true && json[numPregunta].pregunta.respuestaImagen === true ? "5.7" : tipoActividad;//Arrastra imagenes /Arrastra grupos
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].pregunta.imagen === true ? "5.6" : tipoActividad;//Arrastra a imagen
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" ? "5.5" : tipoActividad;//Arrastra ordenar
    }else if(numActividad+""==="1"){
        tipoActividad =  json[numPregunta].pregunta.test === true ? "1.2" : tipoActividad;//opcion multiple TEST
        tipoActividad =  tipoActividad===false ? "1.1" : tipoActividad;//opcion multiple basico
    }else if(numActividad+""==="2"){
        tipoActividad = tipoActividad===false && json[numPregunta].respuestas[0].coordenadas !== undefined ? "2.2":tipoActividad;//respuesta multiple imagen
        tipoActividad = tipoActividad===false && tipoActividad === false ? "2.1":tipoActividad;//opcion multiple basico
    }else if(!isNaN(numActividad*1)){
        tipoActividad = numActividad+"";
    }
    return tipoActividad;
}