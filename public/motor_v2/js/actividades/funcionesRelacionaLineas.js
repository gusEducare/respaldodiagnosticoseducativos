function iniciarActividad(){
    $("#contenidoActividad").html("<svg id='lineas'><line></line></svg><div class='col top left'></div><div class='col top right'></div>");
    var maxRows = Math.min(json[preguntaActual].preguntas.length, json[preguntaActual].respuestas.length);
    var cadena="", rightElement;
    $.each(json[preguntaActual].preguntas, function(i, e){
        cadena += i+","; intentosRestantes++; totalPreguntas++;
        i<maxRows ?  $(".col.top.left").append("<div class='col child left' side='left' t11='"+(i+1)+"' ><p>"+cambiarRutaImagen(e.t11pregunta)+"</p><hr class='circle'></div>") : "";
    });
    $.each(json[preguntaActual].respuestas, function(i, e){
        i<maxRows ?  $(".col.top.right").append("<div class='col child right' side='right' t17='"+e.t17correcta+"' ><p>"+cambiarRutaImagen(e.t13respuesta)+"</p><hr class='circle'></div>") : "";
    });
    cadena = (cadena.slice(0,-1).split(","));
    columnasRandom(cadena, "left"); columnasRandom(cadena, "right");
    maxWidth=0, maxHeight=0;
    if($(".col.child img").length>0){//si encuentra imagenes entre las opciones
        //se aplican banderas alto y ancho para imagenes
        var imgWidth=0, imgHeight=0;
        imgWidth = json[preguntaActual].pregunta.anchoImagen !== undefined ? parseInt(0+json[preguntaActual].pregunta.anchoImagen)+"px" : "auto";
        imgHeight = json[preguntaActual].pregunta.altoImagen !== undefined ? parseInt(0+json[preguntaActual].pregunta.altoImagen)+"px" : "auto";
        $(".col.child img").css({width: imgWidth, height: imgHeight});
        //iguala las dimensiones de las columnas
        var loadTimeout = setTimeout(function(){}, 10);
        $(".col.child img").on("load error", function(){
            clearTimeout(loadTimeout);//espera la carga de las imagenes
            loadTimeout = setTimeout(function(){
                $(".col.top.left img").length > 0 ? calcularDimensiones("left", true) : "";
                $(".col.top.right img").length > 0 ? calcularDimensiones("right", true) : "";
            }, 25);
        });
    }else{
        calcularDimensiones("left", false); calcularDimensiones("right", false);
    }
    $(".col.child:has(img)").css("background", "transparent");
    try{
        document.body.addEventListener("mousemove", false);
        document.body.addEventListener("touchmove", false);
    }catch(e){}
    //funciones al trazar lineas
    trazarRuta();
    //funciones adicionales en caso de evaluacion
    if(evaluacion){
        if(json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
            $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
        }
        $(".col.child").append("<div class='handler'></div>");
        $(".col.child").on("mouseup touchend", function(){ $(".handler").removeAttr("style"); });
        var cActividad = $("#contenidoActividad");
        $(".col.child .handler").draggable({
            scroll:true,
            scrollSensitivity: 150,
            scrollSpeed: 14,
            drag:function(){ cActividad.scrollLeft(0); }
        });
    }
}
var maxWidth, maxHeight;
function calcularDimensiones(columna, width){
    $(".col.child."+columna+">p").each(function(index, element){
        maxWidth = maxWidth < element.scrollWidth ? element.scrollWidth : maxWidth;
        maxHeight = maxHeight < element.scrollHeight ? element.scrollHeight : maxHeight;
    });
    width ? $(".col.child."+columna).css({width: maxWidth+"px"}) : "";
    $(".col.child").css({height: maxHeight+"px"});
}
function columnasRandom(cadena, columna){//aplica random a las posiciones de los elementos html
    cadena = cadena.sort(function(){ return 0.5-Math.random(); });
    $.each(cadena, function(index, element){
        index !== element*1 ? $(".col.child."+columna).eq(index+1).insertBefore($(".col.child."+columna).eq(element)) : "";
    });
}
function trazarRuta(){
    var inDrag = false;
    var dTop, dLeft, scroll;
    var cActividad = $("#contenidoActividad");
    var lineStart, cr1, line, lineEnd, cr2;
    $(".col.child").on("mousedown touchstart", function(event){//inicia trazo
        lineStart = undefined; lineEnd = undefined;
        dTop = cActividad.offset().top; dLeft = cActividad.offset().left;
        inDrag = true; lineStart = $(this).addClass("onSelection");
        cr1 = lineStart.find(".circle");
        sndClick(); line = $("line:last");
        scroll = cActividad.scrollTop();
        //elimina linea existente
        removerLinea(lineStart);
        //define coordenadas de inicio
        if(event.pageX !== undefined){//trazo del primer punto
            line.attr({ x1:cr1.offset().left + 25 - dLeft, y1:cr1.offset().top + 25 - dTop + scroll, x2:event.pageX - dLeft, y2:event.pageY - dTop + scroll});
        }
        cActividad.on("touchmove mousemove", function(event){//continua trazo
            scroll = cActividad.scrollTop();
            if(event.pageX !== undefined){ line.attr("x2", event.pageX - dLeft); line.attr("y2", event.pageY - dTop + scroll); }
            event.preventDefault();
        });
    });
    $(".col.child").on("mouseup touchend", function(event){//finaliza trazo dentro de una respuesta
        if(inDrag){
            scroll = cActividad.scrollTop();
            lineEnd = document.elementFromPoint(event.pageX, event.pageY);
            lineEnd = $(lineEnd).hasClass("child") ? $(lineEnd) : $(lineEnd).parents(".col.child");
            cr2 = lineEnd.find(".circle");
            var mismaColumna = lineStart.attr("side") === lineEnd.attr("side");
            var t11, t17;
            if(lineStart.attr("side") === "left"){//determina t17 y t11 acorde a la direccion del trazo
                t11 = lineStart.attr("t11"); t17 = lineEnd.attr("t17");
            }else{
                t17 = lineStart.attr("t17"); t11 = lineEnd.attr("t11");
            }
            var esCorrecta = t11 === t17;
            if(!mismaColumna && lineEnd.hasClass("child")){
                if(esCorrecta || evaluacion){
                    removerLinea(lineEnd);//elimina linea existente sobre la seleccion
                    lineEnd.addClass("correctLine"); lineStart.addClass("correctLine");
                    line.addClass("correctLine").attr({ x2:cr2.offset().left + 25 - dLeft, y2:cr2.offset().top + 25 - dTop + scroll});
                    if(!evaluacion){//eventos de actividad evaluable
                        intentosRestantes > 0 ? aciertos++ : "";
                        lineEnd.addClass("lock"); lineStart.addClass("lock");
                        sndCorrecto();
                        $("line.correctLine").length === $(".col.child").length/2 ? sigPregunta() : "";
                    }else{//eventos evaluacion
                        if(lineStart.attr("side") === "left"){
                            lineStart.attr("t17", t17);
                            lineEnd.attr("t11", t11);
                        }else{
                            lineStart.attr("t11", t11);
                            lineEnd.attr("t17", t17);
                        }
                        line.attr({t11:t11, t17:t17});
                    }
                $("#contenidoActividad").trigger("endEvent");
                }else{
                    sndIncorrecto();
                    lineEnd.addClass("lock wrongLine"); lineStart.addClass("lock wrongLine");
                    line.addClass("wrongLine").attr({ x2:cr2.offset().left + 25 - dLeft, y2:cr2.offset().top + 25 - dTop});
                    var wrongLine = line;
                    setTimeout(function(){
                        $(".col.child.wrongLine").removeClass("wrongLine");
                        wrongLine.addClass("fadeOut").on(transitionEnd, function(){ $(this).remove(); });
                    }, 300);
                }
                 var newLine = document.createElementNS('http://www.w3.org/2000/svg','line');
                $("svg").append(newLine);
                intentosRestantes--;
            }else{
                $("line:not(.correctLine)").removeAttr("x1 x2 y1 y2");
            }
            $(".onSelection").removeClass("onSelection");
            $("#contenidoActividad").off("touchmove mousemove").on("touchmove mousemove");
        }
        return (inDrag = false);
    }).on(transitionEnd, function(){ $(this).removeClass("lock"); });
    $("#contenidoActividad").on("mouseup touchend", function(){//finaliza trazo fuera de una respuesta
        if(inDrag){
            $(".onSelection").removeClass("onSelection");
            $("line:not(.correctLine)").removeAttr("x1 x2 y1 y2");
            $("#contenidoActividad").off("touchmove mousemove").on("touchmove mousemove");
            return (inDrag = false);
        }
    });
}
function removerLinea(element){
    if(evaluacion){
        var t11 = element.attr("t11"), t17 = element.attr("t17");
        if(t11 !== undefined && t17 !== undefined){
            $(".col.child[t11='"+t11+"'][t17='"+t17+"']").each(function(i,e){
                $(e).removeClass("correctLine");
                $(e).attr("side") === "left" ? $(e).removeAttr("t17") : $(e).removeAttr("t11");
            });
            $("line[t11='"+t11+"'][t17='"+t17+"']").remove();
        }
    }
}