var maxHeight=0;
function iniciarActividad() {
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    var cadena = "";
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    $.each(json[preguntaActual].respuestas, function (index, element) {
        $("#respuestas").append('<div index="'+index+'" t17="'+element.t17correcta+'" class="respuestaDrag"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
        cadena += index+",";
    });
    $.each(json[preguntaActual].contenedores, function (index, element) {
        $("#contenedores").append('<div class="contenedor" t11='+index+'><div class="textoContenedor">'+element.Contenedor[0]+'</div></div>');
    });
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    if($("#contenidoActividad img").length>0){
        $("img").on("load error", function(){
            ajustarDimensiones();
        });
    }else{
        ajustarDimensiones();
    }
    coloresRandom(".respuestaDrag");
    cartasRandom(cadena);
    function ajustarDimensiones(){
        maxHeight = ($("#contenidoActividad").height() / $(".respuestaDrag").length)-7;
        var height = 0;
        $(".respuestaDrag").each(function(i, e){
            height = e.scrollHeight > height ? e.scrollHeight : height;
        });
        $(".contenedor, .respuestaDrag").css({height:parseInt(height)+"px"});
        !evaluacion ? $(".contenedor, .respuestaDrag").css({maxHeight:parseInt(maxHeight)+"px"}) : "";
    }
}
