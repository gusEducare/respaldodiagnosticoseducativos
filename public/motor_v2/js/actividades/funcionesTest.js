var respuesta=[],contadorA=0, contadorB=0;
function iniciarActividad(){
    esUltima = preguntaActual === json.length-1 ? true : false;
    $("#contenidoActividad").html("<div id='preguntaTest'>"+json[preguntaActual].pregunta.t11pregunta+"</div><div id='respuestasActividad'></div>");
    $.each(json[preguntaActual].respuestas, function(index, element){
        $("#respuestasActividad").append("<div class='respuesta' t17='"+element.t17correcta+"'><p class='inciso'>"+letra[index]+"</p><div>"+element.t13respuesta+"</div><div class='clickEvent'></div></div>");
    });
    $(".respuesta").on("click touchend", function(){
        $(this).addClass("selected");
        $(".respuesta").addClass("lock");
        sndClick();
        if(resultados[0].respuesta !== undefined){
            respuesta.push($(this).index());
        }else{
            var t17 = $(this).attr("t17");
            t17*1 === 0 ? contadorB++ : contadorA++;
        }
        setTimeout(function(){
            if(esUltima){
                var resultado;
                var texto;
                if(resultados[0].respuesta !== undefined){
                    texto = obtenerTextoRespuesta();
                    resultado = "<img src='img/test_mensaje_01.png'><br>"+texto;
                }else{
                    if(contadorB > contadorA){//mayoria en resultado b
                        texto = resultados[0].texto2 !== undefined ? resultados[0].texto2 : "¡Actividad finalizada!";
                        resultado="<img src='img/test_mensaje_02.png'><br>"+texto;
                    }else if(contadorB < contadorA){//mayoria en resultado a
                        texto = resultados[0].texto1 !== undefined ? resultados[0].texto1 : "¡Actividad finalizada!";
                        resultado="<img src='img/test_mensaje_01.png'><br>"+texto;
                    }else{
                        texto = resultados[0].texto3 !== undefined ? resultados[0].texto3 : "¡Actividad finalizada!";
                        resultado="<img src='img/test_mensaje_03.png'><br>"+texto;
                    }
                }
                finalizaTest(resultado);
                try{
                    totalPreguntas=1; aciertos=1;
                    var obj = {};
                    obj.id = idActividad;           obj.score = aciertos;
                    obj.score_max = totalPreguntas; obj.attempts = 0;
                    window.top.postMessage({saveProgress:true, obj:obj}, "*");
                }catch(e){ console.log(e); }
            }else{
                sigPregunta();
            }
        }, 500);
    });
}
function obtenerTextoRespuesta(){
    var oracion = resultados[0].respuesta.split("%");
    var textoFinal="";
    var contador=0;
    $.each(oracion, function(index, element){
        if(element !== ""){
            !isNaN(element*1) ? textoFinal += json[element*1-1].respuestas[respuesta[element*1-1]].t13respuesta : textoFinal += element;
            !isNaN(element*1) ? contador++ : "";
        }else{
            textoFinal += element;
        }
    });
    return textoFinal;
}
function finalizaTest(resultado){
    $('<img src="img/flecha.png" id="reload" alt=""/>').appendTo("#modal").on("click touchend", function(){//Reinicia la actividad
        $(this).css({"transform":"scale(1.25, 1.25)", transition:"transform 0.15s"}).fadeOut(150);
        setTimeout(function(){ location.reload(); }, 500);
    });
    $("#modal").addClass("test").removeClass("hidden");
    $("#modalContent").html(resultado);
    setTimeout(function(){
        var top = ($("body").height() - $("#modalContent").height() )/2;
        $("#modalContent").animate({top:top-30}, 400).animate({top:top}, 250);
    }, 500);
}