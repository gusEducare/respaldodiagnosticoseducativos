var t11 = 0;
var maxHeight = 0;
function iniciarActividad(){
    var cadena="", cambiaRespuestas = json[preguntaActual].pregunta.cambiaRespuestas === true;
    var fijaRespuestas = json[preguntaActual].pregunta.fijaRespuestas === true;
    if(preguntaActual === 0 || !fijaRespuestas || cambiaRespuestas){//añade nuevas respuestas
        t11=0;
        $("#contenidoActividad").html("\
            <div id='contenido'></div>\n\
            <div id='respuestas'></div>\n\
            <input id='scroll' type='range' val='0' min='0'>");
        respuestas = "";
        $.each(json[preguntaActual].respuestas, function(i,e){//añade respuestas
            cadena+=i+",";
            $("#respuestas").append("<div class='respuestaDrag horizontal' index='"+i+"' t17='"+e.t17correcta+"'>"+cambiarRutaImagen(e.t13respuesta)+"</div>");
        });
    }else{//mantiene las respuestas anteriores
        $("#contenido").off("transitionend").css({"left": (direccionCambio*-1)+"%", transition:"none"}).html("");
        setTimeout(function(){
            $("#contenido").prop("style").removeProperty("transition");
            $("#contenido").css("left","0%");
            $("#preguntaActividad").remove();
        }, 10);
    }
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    libro === "PROF" ? $("#respuestas").css("opacity",0) : "";
    var contenedores = "";
    $.each(json[preguntaActual].preguntas, function(i,e){//////añade contenedores
        t11++; contenedores+=cambiarRutaImagen(e.t11pregunta)+"<div class='contenedor' t11='"+t11+"'></div>";
    });
    $("#contenido").append(contenedores);
    intentosRestantes = $(".contenedor").length; totalPreguntas+=intentosRestantes;
    //centra el contenido para la variante incisos
    fijaRespuestas && json[preguntaActual].pregunta.textoDerecha !== true ? $("#contenido").addClass("incisos") : $("#contenido").removeClass("incisos");
    //iguala el color de texto
    $(".respuestaDrag .fraction.black, .respuestaDrag fraction.black").length>0 ? $(".respuestaDrag").css("color","black") : "";
    //aplica bandera pintaultimacaja
    if(json[preguntaActual].pregunta.pintaUltimaCaja === false){
        $(".contenedor:last").remove();
        totalPreguntas--; intentosRestantes--; t11--;
    }
    //elimina contenedores innnecesarios
    while($(".contenedor").length > $(".respuestaDrag:not(.respuestaDrag[t17='0'])").length){
        $(".contenedor:last").remove();
        totalPreguntas--; intentosRestantes--; t11--;
    }
    //fija las dimensiones de los elementos
    if(preguntaActual === 0 || !fijaRespuestas || cambiaRespuestas){
        if($(".respuestaDrag img").length > 0){
            $("img").on("load error", fijarTamaños);
        }else{
            fijarTamaños();
        }
        //aplica random a las respuestas
        cartasRandom(cadena);
    }else{//ajusta las dimensiones de los contenedores
        $(".contenedor").css({width:maxWidth+"px", height:maxHeight+"px"});
    }
    //eventos drag and drop
    dragEvents();
    $(window).resize(fijarTamaños);
    $("#contenidoActividad").scroll(function(){
        $(this).scrollTop(0);
    });
}
function fijarTamaños(){
    $(".respuestaDrag:has(img)").css({padding:"0"});
    setTimeout(function(){
        $(".respuestaDrag").each(function(i, e){
            maxHeight = maxHeight < e.scrollHeight ? e.scrollHeight : maxHeight;
            maxWidth = maxWidth < e.scrollWidth ? e.scrollWidth : maxWidth;
        });
        $(".respuestaDrag:not(.respuestaDrag:has(img))").css({width:(maxWidth - 20)+"px", height:maxHeight+"px", textAlign:"center"});
        $(".respuestaDrag:has(img), .contenedor").css({width:maxWidth+"px", height:maxHeight+"px"});
        var respuestasHeight = $("#respuestas")[0].scrollHeight;
        var pregScroll = $("#preguntaActividad")[0] === undefined ? 0 : $("#preguntaActividad")[0].scrollHeight+15;
        $("#contenido").css("height", "calc(100% - "+(respuestasHeight + 40 + pregScroll + 5 )+"px)");
        //añade la barra de scroll
        updateScrollBar();
    }, 10);
}
function updateScrollBar(){
    var range =  $("#respuestas")[0].scrollWidth - ($("#respuestas").width()+10);
    if(range > 0){
        var scrollLeft = $("#respuestas").scrollLeft();
        scrollLeft = scrollLeft > range ? range : scrollLeft;
        $("#contenido").css("padding-bottom", "0");
        $("#scroll").width( range/2 ).attr("max", range).val(scrollLeft).on("input", function(){
            $("#respuestas").scrollLeft($(this).val());
        });
    }else{ $("#contenido").css("padding-bottom", "45px"); }
}
function dragEvents(){
    var dragElement, dropElement;
    $("#contenidoActividad")[0].addEventListener("mousemove", {passive:false});
    $("#contenidoActividad")[0].addEventListener("touchmove", {passive:false});
    $("#respuestas").on("mousedown touchstart", function(){
        $("#contenidoActividad").on("mousemove touchmove", function(event){
            event.preventDefault();
        });
    });
    $("#contenidoActividad").on("mouseup touchend", function(){ $("#contenidoActividad").off("mousemove touchmove").on("mousemove touchmove"); });
    $(".respuestaDrag").draggable({//draggable events
        helper:"clone",
        appendTo:"#contenidoActividad",
        zIndex: 100,
        revert:true,
        tolerance:"intersect",
        start:function(){
            sndClick();
            dragElement = $(this);
            $(this).css({opacity:0});
            evaluacion ? dragElement.draggable({revert:true}): "";
        },
        stop:function(){
            $(this).css({opacity:1});
        }
    });
    $(".contenedor").droppable({//droppable events
        hoverClass:"ui-state-highlight",
        drop:function(){
            dropElement = $(this);
            var t17 = dragElement.attr("t17").split(",");
            var t11 = dropElement.attr("t11");
            var esCorrecta = t17.indexOf(t11) !== -1;
            if(esCorrecta || evaluacion){
                dropElement.droppable("option", "disabled", true);
                $(".contenedor[t11='"+dragElement.attr("t11")+"']").droppable("option", "disabled", false);
                var parentRespuestas = dragElement.parent().attr("id")==="respuestas";
                var pseudo = $("<div class='pseudo' style='width: "+maxWidth+"px; transition: 0.5s;'></div>");
                pseudo.insertAfter(dragElement);
                dragElement.draggable({revert:false}).attr("t11",dropElement.attr("t11"));
                parentRespuestas ? dragElement.appendTo($("#contenido")) : "";
                dragElement.css("position","absolute").offset({top:dropElement.offset().top+2, left:dropElement.offset().left+2});
                parentRespuestas ? dragElement.hasClass("horizontal") ? pseudo.addClass("noWidth") : pseudo.addClass("noHeight") : pseudo.remove();
                if(!evaluacion){
                    dragElement.addClass("lock");
                    intentosRestantes>0 ? aciertos++ : "";
                    sndCorrecto();
                    $(".respuestaDrag.lock").length === $(".contenedor").length ? siguienteCorta() : "";
                }
                setTimeout(function(){ pseudo.remove(); updateScrollBar(); }, 500);
            }else{
                sndIncorrecto();
            }
            intentosRestantes--;
            $("#contenidoActividad").trigger("endEvent");
        }
    });
    if(evaluacion){
        $("#respuestas").droppable({
            drop: function(){
                if(dragElement.parent().attr("id")!=="respuestas"){
                    $(".contenedor[t11='"+dragElement.attr("t11")+"']").droppable("option", "disabled", false);
                    revertResp(dragElement.removeAttr("t11"), "prependTo");
                    setTimeout(updateScrollBar, 700);
                }
            }
        });
    }
}
function siguienteCorta(){
    if(!esUltima){
        var fijaRespuestas = json[preguntaActual].pregunta.fijaRespuestas === true;
        var nextPage = json[preguntaActual+1].pregunta;
        if(fijaRespuestas && (nextPage.cambiaRespuestas !== true && nextPage.fijaRespuestas === true)){
            preguntaActual++;
            $("#contenidoActividad").trigger("cambioDePregunta");
            esUltima = preguntaActual === json.length -1;
            setTimeout(function(){
                $("#contenidoActividad>.respuestaDrag").remove();
                $("#contenido").css("left", direccionCambio+"%").on("transitionend", iniciarActividad);
            }, 750);
        }else{
            sigPregunta();
        }
    }else{
        sigPregunta();
    }
}