function iniciarActividad(){
    var hasCuestion = json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta.replace(" ","") !== "";
    if(hasCuestion){
        $("#contenidoActividad").html("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div><div id='contenido'></div>")
    }else{
        $("#contenidoActividad").html("<div id='contenido'></div>")
    }
    var maxLength = 0;
    var contenido = "", resp;
    $.each(json[preguntaActual].preguntas, function(i,e){//añade cajas de texto
        resp = json[preguntaActual].respuestas[i];
        maxLength = resp === undefined ? -0 : resp.t17correcta;
        if(maxLength !== undefined){//evita añadir cajas de texto innecesarias
            intentosRestantes++; totalPreguntas++;
            maxLength = maxLength.length;//define la longitud de caracteres para cada caja de texto
            contenido +=  maxLength!==-0 ? cambiarRutaImagen(e.t11pregunta)+"<textarea class='area' r='"+i+"' rows='1' cols='"+maxLength+"' maxlength='"+maxLength+"'></textarea>" : cambiarRutaImagen(e.t11pregunta);
        }
    });
    $("#contenido").append(contenido);
    //aplica bandera pintaUltima caja
    if(json[preguntaActual].pregunta.pintaUltimaCaja === false){
        $("textarea.area:last").remove();
        intentosRestantes--; totalPreguntas--;
    }
    //evalua la respuesta escrita
    var keyTime;
    if(!evaluacion){
        clearTimeout(keyTime);
        var keyLock = false;
        $("textarea.area").keyup(function(){
            if(keyLock){ return false; }
            $(this).width(this.scrollWidth - 4);
            if($(this).val().length === $(this).attr("cols")*1){
                var rUsuario = $(this).val().toUpperCase(),
                    rJson = json[preguntaActual].respuestas[$(this).attr("r")].t17correcta.toUpperCase();
                var esCorrecta = json[preguntaActual].pregunta.acentos === false ? eliminarAcentos(rUsuario) === eliminarAcentos(rJson) : rUsuario === rJson;
                if(esCorrecta){
                    sndCorrecto();
                    $(this).addClass("correct lock").blur();
                    if(intentosRestantes > 0){
                        aciertos++;
                    }
                    $("textarea.correct").length === $("textArea.area").length ? sigPregunta() : "";
                }else{
                    sndIncorrecto();
                    keyLock = true;
                    var element = $(this);
                    element.addClass("incorrect lock");
                    setTimeout(function(){
                        element.css({color:"transparent", transition:"color 0.5s, border-color 0.35s, box-shadow 0.35s"}).removeClass("incorrect");
                        setTimeout(function(){
                            element.removeClass("lock").removeAttr("style").val("");
                            keyLock = false;
                        }, 500);
                    }, 500);
                }
                intentosRestantes--;
            }
            keyTime = setTimeout(function(){
                $("#contenidoActividad").trigger("endEvent");
            }, 500);
        }).on("keydown keypress", function(){ return !keyLock; });
    }
}