var cssBottom = "5px", progressTimeout = setTimeout(hideProgress, 3000);
function barraAvance(){
    for(var i = 1; i<=json.length; i++){
        $("#progressBar").append("<div class='mark unsolved bg_bookColor2'><hr class='bar bg_bookColor2'><p class='num'>"+i+"</p></div>");
    }
    $("#progressBar .mark:last .bar").remove();
    $("#progressBar .mark").eq(preguntaActual).addClass("current").removeClass("unsolved");
    if(libroProf){
        $("<img src='img/flecha.png' id='reload'>").on("click", function(){ location.reload(); }).appendTo("body");
    }
    if(setProfMode){
        if(libro === "PROF"){
            $("#progressBar .mark").on("click touch", function(){
                showProgress();
                if(preguntaActual !== $(this).index()){
                    actualizaProgreso($(this));
                    preguntaActual = $(this).index() - 1;
                    incisos ? siguienteCorta() : sigPregunta();
                }
            });
        }
    }else{
        cssBottom = "-40px";
        $("#progressBar").on("click touch", hideProgress);
    }
    $("#contenidoActividad").on("cambioDePregunta", function(){
        if(!esUltima){
            showProgress();
        }else if (libro === "PROF" && esUltima) {
            showProgress();
        }
        setTimeout(function(){
            actualizaProgreso($("#progressBar .mark").eq(preguntaActual));
        }, 500);
    });
}
function hideProgress(){
    $("#progressBar").css("bottom", cssBottom);
}
function showProgress(){
    clearTimeout(progressTimeout);
    $("#progressBar").css("bottom", "5px");
    progressTimeout = setTimeout(hideProgress, 6000);
}
function actualizaProgreso(element){
    $("#progressBar .mark").addClass("unsolved").removeClass("solved current").each(function(i,e){
        if(i === element.index()){
            $(e).addClass("current").removeClass("unsolved");
            return false;
        }else{
            $(e).addClass("solved").removeClass("unsolved");
        }
    });
}