var range, calificacion="0.0", scrollMomentum, scrollInterval;
function initEval(){
    $.each(objCadenas, function(){ arrCorrectas.push(null); });
    $("body").append("<nav id='preguntas' class='noLeft'></nav>\n\
            <div id='btn-extras' tittle='Mostrar contenido extra'></div>\n\
            <div id='modal-extras'><div class='contenedor-modal'></div></div>");
    barraEvaluaciones();
    $("#contenidoActividad").addClass("actividadEvaluacion");
    $("#previewActividad, #progressBar").remove();
    if(strAmbiente === "PROD"){
        parent.$('body').css({overflow:"hidden"});
        parent.$('body').find(".contenedor_body iframe").css({//cambia la altura al iframe de la actividad
            height:"calc(100vh - "+(parent.$('body').find(".contenedor_body")[0].scrollHeight-27)+"px)"
        });
        $("html, body").css({width:"100%"});
    }
    //dice al padre el maximo de preguntas para determinar el rango de progreso
    range = 100/json.length;
    progress = getProgress(0, objCadenas);
    //boton de informacion extra
    btnExtras();
    $("#btn-extras").on("click touchend", function(){
                if($(this).hasClass("modal-open")){
                    $("#modal-extras").removeClass("reveal");
                    $(this).removeClass("modal-open");
                }else{
                    $("#modal-extras").addClass("reveal");
                    $(this).addClass("modal-open");
                }
                return false;
            });
        
    //al presionar el boton para finalizar examen
    strAmbiente === "PROD" ? $(top.document).find("#button-finalizar").on("click touchend", function(){
        setCal();
    }) : "";
    //restaura la actividad
    restaurarActividad();
    $("#contenidoActividad").on("transitionEnded", function(){
        $("#preguntas").removeClass("lock");
        var loadTime = setTimeout(restaurarActividad, 100);
        $("img").on("load", function(){
            clearTimeout(loadTime);
            loadTime = setTimeout(restaurarActividad, 100);
        });
    }).on("siguiente", function(){
        $("#preguntas").addClass("lock");
    });
    //scroll con momentum para la barra de preguntas
    importScript("js/", "impetus.min", "impetus", function(){
        if( $("#preguntas")[0].scrollWidth > $("#preguntas").width() ){//valida que haya scroll en la barra de navegacion
            var barraPreguntas = $("#preguntas");
            var max, stop;
            // 56 * 16 -> 56px de ancho por respuesta y 16 respuestas visibles en pantalla
            max = ( barraPreguntas[0].scrollWidth - (56 * 16) ) * -1;
            scrollMomentum = new Impetus({
                source: barraPreguntas[0],
                multiplier: 0.7,
                update: function(x, y) {
                    x = x<max ? max : x;
                    x = x>0 ? 0 : x;
                    barraPreguntas.scrollLeft(-x);
                    clearTimeout(stop);
                    stop = setTimeout(function(){
                        scrollMomentum.setValues(x,y);
                    }, 10);
                }
            });
            var sLeft;
            barraPreguntas.scroll(function(){
                sLeft = barraPreguntas.scrollLeft();
                sLeft === 0 ? barraPreguntas.addClass("noLeft") : barraPreguntas.removeClass("noLeft");
                -sLeft === max+4 ? barraPreguntas.addClass("noRight") : barraPreguntas.removeClass("noRight");
            });
        }else{
            $("#preguntas").addClass("noPseudo");
        }
    });
    $("#contenidoActividad").scroll(function(){ $(this).scrollLeft(0); });
}
function barraEvaluaciones(){
    $("nav#preguntas").append("<hr class='left'>");
    $.each(json, function(i){
        $("nav#preguntas").append("<div class='nPregunta'>"+(i+1)+"</div>");
    });
    $("nav#preguntas").append("<hr class='right'>");
    $("nav#preguntas .nPregunta:first").addClass("selected");
    $("nav#preguntas .nPregunta").on("click", function(){
        var element = $(this);
        if(element.hasClass("selected")){
            element.hasClass("flag") ? element.removeClass("flag") : element.addClass("flag");
        }else{
            $("#modal-extras").removeClass("reveal");
            $("#btn-extras").removeClass("modal-open");
            $("nav#preguntas .nPregunta").removeClass("selected");
            obtenerRespuestaUsuario();
            element.addClass("selected");
            preguntaActual = element.index()-2;
            $("#contenidoActividad").trigger("nexteval");
            siguientePregunta();
            btnExtras();
            progress = getProgress(0, objCadenas);
        }
    });
    var scroll;
    $("nav#preguntas>hr").on("mousedown touchstart", function(){
        scrollMomentum.pause();
        var el = $("nav#preguntas"),
            der = $(this).hasClass("right");
        scroll = el.scrollLeft();
        scrollInterval = setInterval(function(){
            scroll += der ? +2 : -2;
            el.scrollLeft(scroll);
            if((der && el.hasClass("noRight"))  || (!der && el.hasClass("noLeft"))){
                clearInterval(scrollInterval);
                scrollMomentum.resume();
                scrollMomentum.setValues(-scroll, 0);
            }
        },1);
    }).on("mouseup touchend", function(){
        clearInterval(scrollInterval);
        scrollMomentum.resume();
        scrollMomentum.setValues(-scroll, 0);
    });
}
function btnExtras(){
    var extraContent = json[preguntaActual].pregunta.t11instruccion;
    if(extraContent !== undefined && extraContent.replace(/ /g,"") !== ""){
        $("#btn-extras").addClass("reveal");
        $("#modal-extras>.contenedor-modal").html(cambiarRutaImagen(extraContent));
    }else{
        $("#btn-extras").removeClass("reveal");
        $("#modal-extras>.contenedor-modal").html("");
    }
}
function getProgress(progreso, obj){
    var contador=0;
    $.each(obj, function(i,e){
        if(e === null || e.replace(/ /g,"") === ""){
            contador = 0;
        }else{//valida que no venga en null la cadena
            contador = 1;
            var numeroVariante = obtenerTipoActividad(i);
            var numeroPregunta = json[i].pregunta.c03id_tipo_pregunta;
            var recurso = tiposActividad[numeroPregunta].variantes[numeroVariante];
            switch (recurso) {
                case "ArrastraMatriz":
                case "ArrastraMatrizHorizontal":
                case "ArrastraOrdenar":
                case "ArrastraImagenes":
                case "ArrastraCorta":
                case "OrdenaElementos":
                case "RelacionaLineas":
                    e.indexOf("x") !== -1 ? contador = 0 : "";
                    break;
                case "OpcionMultiple":
                case "RespuestaMultipleFondo":
                case "Matriz":
                    this.indexOf("1") === -1 ? contador=0 : "";
                    break;
                case "PreguntaAbierta":
                    e.replace(/𠮷/g,"").replace(" ","") === "" ? contador = 0 : "";
                    break
                default:
                    respuestaUsuario = null;
                    break
            }
        }
        progreso += contador;
    });
    strAmbiente === "PROD" ? top.window.updateProgress(progreso, json.length, range) : "";
    if(strAmbiente === "DEV"){//funciones adicionales para desarrollo
        console.log("Cal: "+calificacion+"  |   Prog: "+((progreso * range).toFixed(1)));
        setPrentData(calificacion, (progreso * range).toFixed(1));
    }
    if(strAmbiente === "PROD" && (progreso*range)+range>=100){
        if(!$("#contenidoActividad").hasClass("endEvent")){
            $("#contenidoActividad").addClass("endEvent").on("endEvent", function(){
                obtenerRespuestaUsuario();
                getProgress(progreso, [objCadenas[preguntaActual]]);
                return false;
            });
        }
    }else{ $("#contenidoActividad").removeClass("endEvent").off("endEvent"); }
}
function obtenerRespuestaUsuario(){
    var respuestaUsuario = "";
    var arrRespuestas = [];
    switch (lastResort) {
        case "ArrastraMatriz":
        case "ArrastraMatrizHorizontal":
        case "ArrastraOrdenar":
        case "ArrastraImagenes":
        case "ArrastraCorta":
        case "OrdenaElementos":
            var div, t17, index;
            $(".contenedor").each(function(i,e){
                div = $(".respuestaDrag[t11='"+$(this).attr("t11")+"']");
                if(div.length>0){
                    t17 = div.attr("t17").replace(/ /g,"");
                    index = div.attr("index");
                }else{
                    t17="x"; index="x";
                }
                respuestaUsuario += t17+"-"+index+" ";
            });
            respuestaUsuario = respuestaUsuario.slice(0,-1);
            break;
        case "OpcionMultiple":
        case "RespuestaMultipleFondo":
            var rLength = $(".respuesta").length, isSelected,
            clase = lastResort==="OpcionMultiple" ? "OMSelectedEval" : "selected";
            for(var i=0; i<rLength; i++){
                isSelected = $(".respuesta[index='"+i+"']").hasClass(clase) ? 1 : 0;
                arrRespuestas.push(isSelected);
            }
            respuestaUsuario = arrRespuestas.join("");
            break;
        case "RelacionaLineas":
            var t17;
            $(".col.child.left").each(function(){
                t17 = $(this).attr("t17");
                t17 = t17 === undefined ? "x" : t17;
                respuestaUsuario += $(this).attr("t11")+"-"+t17+",";
            });
            respuestaUsuario = respuestaUsuario.slice(0,-1);
            break
        case "Matriz":
            $(".check").each(function (index, element) {
                element.className.indexOf("checked") !== -1 ? arrRespuestas.push("1") : arrRespuestas.push("0");
            });
            respuestaUsuario = arrRespuestas.join("");
            break;
        case "Graficas":
            respuestaUsuario = guardarAvance();//definido en funciones generales   //aplicado tambien cuando no es evaluable el tipo de actividad
            break
        case "PreguntaAbierta":
            $("textarea.area").each(function(i){
                respuestaUsuario+= $(this).val()+(i===$("textarea.area").length-1 ? "":"𠮷");
            });
            break
        default:
            respuestaUsuario = null;
            break
    }
   calificaRespuesta(respuestaUsuario);
   guardarCadenaUsuario(respuestaUsuario);
   setCal();
}
function calificaRespuesta(respuestaUsuario){
    var correcta = 1;
    switch (lastResort) {
        case "ArrastraMatriz":
        case "ArrastraMatrizHorizontal":
        case "ArrastraOrdenar":
        case "ArrastraImagenes":
        case "ArrastraCorta":
        case "OrdenaElementos":
            var rDrag, t11, t17;
            $(".contenedor").each(function(i,e){
                t11 = $(e).attr("t11");
                rDrag = $(".respuestaDrag[t11='"+t11+"']");
                if(rDrag.length > 0 ){
                    t17 = rDrag.attr("t17").split(",");
                    if(t17.indexOf(t11) === -1){
                        correcta = 0;
                        return false;
                    }
                }else{
                    correcta = 0;
                    return false;
                }
            });
            break;
        case "OpcionMultiple":
        case "RespuestaMultipleFondo":
            var t17, hasClass;
            $(".respuesta").each(function(){
                t17 = $(this).attr("t17")*1;
                hasClass = $(this).hasClass("OMSelectedEval") || $(this).hasClass("selected");
                if( (t17===1 && !hasClass) || (t17!==1 && hasClass) ){
                    correcta = 0;
                    return false;
                }
            });
            break;
        case "RelacionaLineas":
            var relacion;
            $.each(respuestaUsuario.split(","), function(i,e){
                relacion = e.split("-");
                if(relacion[0]*1 !== relacion[1]*1){
                    correcta = 0;
                    return false;
                }
            });
            break
        case "Matriz":
            var t17;
            $("tr[t17]").each(function(i,e){
                t17=$(e).attr("t17").split(",");
                if($(e).find(".check.checked").length>0){
                    $(e).find(".check.checked").each(function(){
                        if( t17.indexOf( ($(this).parent().index()-1)+"" ) === -1){
                            correcta = 0;
                            return false;
                        }
                    });
                    if(correcta === 0){
                        return false;
                    }
                }else{
                    correcta = 0;
                    return false;
                }
            });
            break;
        case "PreguntaAbierta":
            var respuestas = json[preguntaActual].respuestas;
            $("textarea.area").each(function(i,e){
                if(eliminarAcentos($(e).val()) !== eliminarAcentos(respuestas[i].t17correcta+"")){
                    correcta = 0;
                    return false;
                }
            });
            break
        default:
            var correcta = 1;
            break
    }
    arrCorrectas[preguntaActual] = correcta;
}
function guardarCadenaUsuario(respuestaUsuario){
    var conectarBD = true;
    if(respuestaUsuario !== "" && respuestaUsuario !== objCadenas[preguntaActual]){//si hay cambio en la pregunta
        objCadenas[preguntaActual] = respuestaUsuario;
    }else{ conectarBD = false; }
    if(conectarBD && strAmbiente==="PROD"){//conecta a la base de datos
        window.top.setPMessage({
            orden:"",
            correcta:arrCorrectas.join(""),
            respuesta:objCadenas.join("¬"),
            timer:180
        });
    }
}
function restaurarActividad(){
    var objActual = objCadenas[preguntaActual];
    if(objActual !== null && objActual !== undefined && objActual.replace(/ /g,"") !== ""){
        switch (lastResort) {
            case "ArrastraMatriz":
            case "ArrastraMatrizHorizontal":
            case "ArrastraOrdenar":
            case "ArrastraImagenes":
            case "ArrastraCorta":
            case "OrdenaElementos":
                //las respuestas deben tener la propiedad index para identificarlas con mayor presicion
                var orden = objCadenas[preguntaActual].split(" ");
                var t17, t11, index, contenedor, respuestaDrag;
                //crear evento para restaurar
                $(".contenedor.noHeight").css("transition","none");
                $(".contenedor").each(function(i,e){
                    if(orden[i].indexOf("x") === -1){
                        contenedor = $(e);
                        t17 = orden[i].split("-")[0];
                        t11 = contenedor.attr("t11");
                        index = orden[i].split("-")[1];
                        respuestaDrag = $(".respuestaDrag[t17='"+t17+"'][index='"+index+"']");
                        respuestaDrag.attr("t11",t11);
                        contenedor.attr("t17", t17).droppable("option", "disabled", true).addClass("disabled").removeClass("noHeight");
                        var parent = $("#fondoPregunta, #contenido");
                        if(contenedor[0].style.top !== ""){
                            var t, l;
                            t = contenedor.position().top;
                            l = contenedor.position().left;
                            respuestaDrag.css({position:"absolute", //fija la posicion de la respuesta
                                top:Math.round(t + (contenedor.css("border-top-width").replace("px","")*1)) + "px",
                                left:Math.round(l + (contenedor.css("border-left-width").replace("px","")*1)) + "px"
                            }).addClass("correctAnswer").appendTo(parent);
                        }else{
                            respuestaDrag.css({position:"absolute", //fija la posicion de la respuesta
                                top: Math.round(contenedor.offset().top - parent.offset().top + parseInt(contenedor.css("border-top-width"))) + "px",
                                left:Math.round(contenedor.offset().left - parent.offset().left + parseInt(contenedor.css("border-left-width"))) + "px"
                            }).addClass("correctAnswer").appendTo(parent);
                        }
                    }
                });
                $(".contenedor.noHeight").css("transition","");
                $(".contenedor.disabled").length === $(".contenedor:not(.noHeight)").length ?  $(".contenedor.noHeight:first").removeClass("noHeight") : "";
                lastResort === "ArrastraCorta" ? updateScrollBar() : "";
                break
            case "OpcionMultiple":
            case "RespuestaMultipleFondo":
                var clase;
                clase = lastResort==="OpcionMultiple" ? "OMSelectedEval" : "selected";
                $.each(objCadenas[preguntaActual].split(""), function(index, element){
                    element === "1" ? $(".respuesta[index='"+index+"']").addClass(clase) : "";
                });
                break;
            case "Matriz":
                $.each(objCadenas[preguntaActual].split(""), function(index, element){
                    element === "1" ? $(".check").eq(index).css("background-image","url('img/opcion_multiple_palomita.png')").addClass("checked") : "";
                });
                break
            case "RelacionaLineas"://4-4,1-1,2-x,3-x    t11-t17
                var imgTime;
                imgTime = setTimeout(fixLines, 100);
                $("#contenidoActividad img").on("load error", function(){
                    clearTimeout(imgTime);
                    imgTime = setTimeout(fixLines, 100);
                });
                function fixLines(){
                    var t17, t11, line,
                        l1, l2, cr1, cr2,
                        dLeft, dTop, scroll;
                    $.each(objCadenas[preguntaActual].split(","), function(i,e){
                        t11 = e.split("-")[0];
                        t17 = e.split("-")[1];
                        if(t17.indexOf("x")===-1){//verifica que haya relacion
                            dTop = $("#contenidoActividad").offset().top;
                            dLeft = $("#contenidoActividad").offset().left;
                            scroll = $("#contenidoActividad").scrollTop();
                            line = $("line:not(.correctLine):first");
                            l1 = $(".col.child.left[t11='"+t11+"']");
                            cr1 = l1.find(">.circle");
                            l2 = $(".col.child.right[t17='"+t17+"']");
                            cr2 = l2.find(">.circle");
                            line.addClass("correctLine").attr({
                                x1:cr1.offset().left + 25 - dLeft,
                                y1:cr1.offset().top + 25 - dTop + scroll,
                                x2:cr2.offset().left + 25 - dLeft,
                                y2:cr2.offset().top + 25 - dTop + scroll
                            });
                            //asigna los valores
                            l1.attr("t17",t17).addClass("correctLine");
                            l2.attr("t11", t11).addClass("correctLine");
                            line.attr({t11:t11, t17:t17});
                            $("#lineas").append(document.createElementNS('http://www.w3.org/2000/svg','line'));
                        }
                    });
                }
                break
                case "Graficas":
                    restaurarAvace();
                    break
                case "PreguntaAbierta":
                    $.each(objCadenas[preguntaActual].split("𠮷"), function(i,e){
                        $("textarea.area").eq(i).val(e);
                    });
                    break
        }
    }
    $(".button, #actSelect").removeClass("lock");
    return false;
}

function setCal(){
    var aciertos = arrCorrectas.join("").replace(/null/g ,"").replace(/0/g, "").length;
    calificacion = (aciertos/json.length * 100).toFixed(1);
    strAmbiente === "PROD" ? window.parent.calificacion = calificacion : "";
}
function setPrentData(cal, prog){
    top.window.postMessage({
        type:"setStats",
        score:cal,
        progress:prog
    },"*");
}