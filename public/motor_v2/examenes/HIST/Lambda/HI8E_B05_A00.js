
json = [
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al término de la guerra fría se establece en la mayor parte del mundo el sistema capitalista.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El avance tecnológico se ha desarrollado a tal punto que toda la población cuenta con un smartphone.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las computadoras se han vuelto un instrumento fundamental en la vida de los hombres, tanto que muchos de ellos utilizan una en su día a día para trabajar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Internet se ha vuelto una necesidad básica de manera que cada vez más y más personas tienen acceso a él.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda. ",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//2
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Inglaterra",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Francia",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Japón",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "China",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Corea de Norte",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
          {  
            "t13respuesta":     "México",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Australia",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Cuba",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Bielorrusia",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Rusia",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>¿Cuáles países formaban el bloque liderado por Estados Unidos?",
         "¿Cuáles de los siguientes países no formaban parte del bloque económico liderado por la URSS?"]
      }
   },//3
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EFidel Castro\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EErnesto Che Guevara\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EValentín Gómez Farías\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFulgencio Batista\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ELa Bahía de Cochinos en 1961\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELas islas de los Cayos en 1962\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa Habana en 1965\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa Habana en 1960\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":"\u003Cp\u003E1962\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1965\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1945\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1960\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EVietnam\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECorea\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003EHong Kong\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003EJapón\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },//
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>Cuba fue transformada en un país socialista tras el movimiento revolucionario encabezado por:",
         "Ante la transformación de Cuba en un país socialista, Estados Unidos envió cubanos exiliados armados para derrocar a Fidel Castro, promoviendo una invasión en:",
         "Ante los problemas que presentaba Estados Unidos con Cuba, Fidel Castro, apoyado por la URSS, colocó misiles en la isla en:",
         "Después de la crisis de los misiles, el gobierno americano intervino en un país asiático en el que perdió la guerra."]
      }
   },//4
    {
        "respuestas": [
            {
                "t13respuesta": "...sería continuado por países como China, Vietnam o Cuba.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...su régimen socialista desde su fundación en 1948.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...la isla ha implementado ciertas modificaciones al sistema para tener mayor libertad comercial.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...se ha mantenido como una de las potencias socialistas más fuertes, pero con un sistema económico inclinado al capitalismo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...pues los países que formaban parte de ella en Europa, decidieron adoptar la democracia como forma de gobierno.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Ante la caída de la URSS, el sistema socialista..."
            },
            {
                "t11pregunta": "Corea del Norte ha logrado mantener..."
            },
            {
                "t11pregunta": "Cuba se ha mantenido con un sistema socialista desde su implementación con Fidel Castro, sin embargo, desde el ascenso de Raúl Castro al poder como reemplazo de Fidel..."
            },
            {
                "t11pregunta": "A pesar de que la República Popular de China fue fundada en 1949..."
            },
            {
                "t11pregunta": "La transición democrática europea se dio con la caída de la URSS..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona con líneas las columnas según corresponda."
        }
    },//5
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con la desintegración de la URSS la mayoría de los países adoptaron el sistema capitalista.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A partir de que la mayoría de los países adoptó el modelo capitalista como modelo económico comenzó a utilizarse el término globalización.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una de las consecuencias de la globalización es la creación de empresas demasiado grandes abarcando más de un sector económico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Organismos financieros creados después de la Segunda Guerra Mundial, como el banco internacional, ya no existen.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//6
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EUna organización internacional integrada en una comunidad política.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EUn tratado para el libre comercio.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EUn acuerdo antibélico que impide iniciar una guerra en Europa.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EUna organización monetaria internacional con cobertura continental.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003E1993\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1995\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1989\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1898\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          {  
            "t13respuesta":"\u003Cp\u003ELogró basar su comercio en las exportaciones alrededor del mundo.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELogró juntar distintas economías en una sola.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELogró combatir la crisis uniendo economías y permitiendo el libre comercio.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELogró consolidar una entidad financiera continental que permitió controlar el flujo monetario.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
          ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Qué es la Unión Europea?",
         "¿En qué año entró en vigor el Tratado de la Unión Europea que permitía el libre comercio y libre circulación de ciudadanos de países miembros?",
         "¿Cómo logró consolidarse la UE como una potencia económica?"]
      }
   },//7
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Por la creación del estado de Israel.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Por el desplazamiento de los habitantes de raza árabe.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Por el petróleo de la región.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Por la minas ricas en níquel de la región.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
          {  
            "t13respuesta":     "Hinduísmo",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Islam",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Cristianismo",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Judaísmo",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          {  
            "t13respuesta":     "Han aumentado desde su descolonización.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Por las naciones que se han formado en la búsqueda de una identidad nacional basada en la etnia.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Han aumentado desde el fin de la guerra fría.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Por la creación de estados artificiales sin una base cultural plausible.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
          {  
            "t13respuesta":     "Un sistema político de segregación racial implementado en Sudáfrica.",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Seguía el principio de que las personas de raza negra no podían votar.",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Un sistema político de cohesión social implementado en Sudáfrica.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Seguía el principio de que las personas negras podrían compartir el mismo espacio que las personas blancas.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },//
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
          "preguntasMultiples":true,
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>¿Por qué surge el conflicto de la “Franja de Gaza”?",
         "¿Qué religiones tienen el mayor peso en la región del subcontinente de India?",
         "¿Desde cuándo y por qué han aumentado las guerras étnicas en África?",
         "¿Qué fue y en qué se basaba el Apartheid africano?"]
      }
   },//8
   {
        "respuestas": [
            {
                "t13respuesta": "...las guerras o invasiones.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...el desplazamiento de aproximadamente 11 millones de personas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...recibir a todas aquellas personas que sean desplazadas por cualquier situación de conflicto que ponga en riesgo sus vidas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...no estaba prohibido ni era un delito.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...el narcotráfico se ha convertido en una actividad concurrida debido a las cuantiosas ganancias que genera.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La principal causa de desplazamiento de una población han sido..."
            },
            {
                "t11pregunta": "En 2011 estalló la guerra civil Siria, la cual provocó..."
            },
            {
                "t11pregunta": "Los acuerdos internacionales estipulan que las naciones están obligadas a..."
            },
            {
                "t11pregunta": "Antes del siglo XX el consumo y comercio de drogas..."
            },
            {
                "t11pregunta": "Desde la prohibición de drogas y enervantes..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona con líneas las columnas según corresponda."
        }
    },//9
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El movimiento de los derechos civiles en norteamérica surge como producto de la discriminación de los afroamericanos, consecuencia del esclavismo y dominio de la población blanca.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Martin Luther King, pastor de la iglesia baptista, inició el movimiento por la lucha pacífica de los derechos civiles a mediados de 1950.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La lucha iniciada por Luther King estuvo inspirada en la lucha de Nelson Mandela.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los movimientos estudiantiles surgen motivados por una rebeldía en contra de un sistema que coartaba su creatividad y el derecho a expresar sus ideas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El feminismo es un movimiento social que surge tras el histórico trato desigual hacia las mujeres, que habían sido vistas como inferiores a los hombres.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//10
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Un organismo civil que busca poder participar en la implementación de políticas públicas.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Un organismo civil que busca la participación activa de la población en la toma de decisiones de los gobiernos con repercusión directa en la vida de las personas.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Un organismo civil que busca concientizar a las personas acerca de un tema en específico.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Un organismo civil que busca consolidarse como un partido político.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//1
         {  
            "t13respuesta":     "Un medio de búsqueda de una sociedad más igualitaria.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Un medio de búsqueda de derechos y garantías para la población civil.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Un medio de cohesión social dentro de una ciudad.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Un medio de consolidación política de algunos grupos.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//2
          {  
            "t13respuesta":     "Que los jóvenes pudieran expresarse.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Que los estudiantes pudieran formar parte de la toma de decisiones de los gobiernos.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Que los estudiantes pudieran aportar ideas fundamentadas en la educación que recibían.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Que los estudiantes pudieran participar libremente en las relaciones internacionales.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//3
          {  
            "t13respuesta":     "Una igualdad de derechos y oportunidades entre hombres y mujeres.",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Un alto a la discriminación en contra de las mujeres.",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Un trato digno y respetuoso para todas las personas por el hecho de ser personas.",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Una justicia social para las mujeres por el hecho de ser mujeres.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },//4
          {  
            "t13respuesta":     "Implementó una política intervencionista donde apoyó incluso hasta a dictadores.",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":     "Implementó un organismo de seguridad nacional como la CIA o el FBI.",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":     "Implementó una política de apoyo a países, basada en la Doctrina Monroe.",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":     "Implementó una política de no intervención para mantener la paz.",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },//5
          {  
            "t13respuesta":     "Política, económica y de seguridad nacional.",
            "t17correcta":"1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":     "Económica, religiosa y de seguridad legal.",
            "t17correcta":"1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":     "Religiosa, social y económica.",
            "t17correcta":"1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta":     "Legal, política y de paz mundial.",
            "t17correcta":"0",
            "numeroPregunta":"5"
         },//6
         {  
            "t13respuesta":     "Apoyar a los países en vías de desarrollo.",
            "t17correcta":"1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta":     "Clamar el expansionismo e imperialismo de norteamérica.",
            "t17correcta":"1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta":     "Reafirmar el sistema capitalista neoliberal.",
            "t17correcta":"1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta":     "Llevar la paz a las naciones latinoamericanas.",
            "t17correcta":"0",
            "numeroPregunta":"6"
         },//7
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
          "preguntasMultiples":true,
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>¿Qué es una organización no gubernamental?",
         "Los movimientos civiles surgen como:",
         "Los movimientos estudiantiles del siglo pasado buscaban:",
         "El feminismo busca:",
         "Con el fin de evitar que surgieran más gobiernos socialistas en el territorio del continente americano, Estados Unidos:",
         "Las intervenciones norteamericanas tienen tras de sí intereses de índole:",
         "El apoyo que recibieron las dictaduras militares de Latinoamérica por parte de norteamérica tenía el propósito de:"]
      }
   },//11
   {
        "respuestas": [
            {
                "t13respuesta": "...un sistema económico en donde la mayor parte de sus riqueza se concentra en un grupo no mayor al 10% de su población.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...la economía dependa de un pequeño grupo, además del estancamiento comercial por la falta de poder adquisitivo del resto de la población.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...su producción petrolera, sus exportaciones y la agricultura.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...no contaba con una competitividad tecnológica y de calidad.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...presiones internacionales y para cumplir con acuerdos establecidos por instituciones económicas internacionales.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Los países latinoamericanos tienen una situación económica difícil, producto de..."
            },
            {
                "t11pregunta": "El hecho de que la mayor parte de la riqueza esté acumulada en un pequeño grupo de personas provoca que..."
            },
            {
                "t11pregunta": "La economía mexicana se desarrolla con base en..."
            },
            {
                "t11pregunta": "México decidió implementar impuestos a los productos importados para apoyar la economía local a pesar de que..."
            },
            {
                "t11pregunta": "México adoptó el sistema neoliberal por..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona con líneas las columnas según corresponda."
        }
    },//12
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se pueden distinguir dos clases de consumo: para la subsistencia y el que es solo por moda.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al darse cuenta que la base de la economía estaba en el consumo, las empresas decidieron hacer que sus productos tuvieran una vida útil programada, para que exista más consumo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El consumo masivo genera un problema de contaminación en todo el mundo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los medios de comunicación masiva han frenado el consumismo creando conciencia en la población.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La economía de consumo es la vía de desarrollo económico actual, por lo que debe ser la única.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//13
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Los lanzamientos de diferentes satélites soviéticos.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "La llegada del hombre a la Luna.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Los lanzamientos de sondas de exploración para la Luna y Marte.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "La llegada del hombre a otras galaxias.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//1
         {  
            "t13respuesta":     "La colocación en órbita del telescopio Hubble en 1990.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "La construcción de la Estación Espacial Internacional iniciada en 1998.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "El lanzamiento de la misión Curiosity en 2011 con el fin de explorar Marte.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "El descenso y reconocimiento de sonda en los planetas Saturno y Júpiter en 2000.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//2
          {  
            "t13respuesta":     "El descubrimiento del ADN por J. Watson y F. Crick en 1953.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "La creación del mapa del genoma humano completo en 2003.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "El descubrimiento e implementación de la modificación genética en humanos en 2008.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "La clonación de diversas especies con fines de consumo y o recreativos desde 2001.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//3
          {  
            "t13respuesta":     "El rayo láser.",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "La fibra óptica.",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "La luz eléctrica.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "El internet.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },//4
          
          
    
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
          "preguntasMultiples":true,
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>¿Cuáles fueron los frutos de la carrera espacial?",
         "En la historia reciente de los avances de exploración del Universo tenemos acontecimientos como:",
         "En el campo de la genética algunos de los grandes descubrimientos y avances han sido:",
         "En la historia reciente podríamos considerar grandes descubrimientos, como:"]
      }
   },//14
    {
        "respuestas": [
            {
                "t13respuesta": "...desde la Revolución Industrial.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...noventa por ciento de los productos que consumimos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...promedio en el planeta. Este fenómeno es conocido como calentamiento global.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...de protección del medio ambiente y para llegar a un desarrollo sustentable.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Nunca antes en la historia de la humanidad se había causado tanto daño al ambiente como se ha hecho..."
            },
            {
                "t11pregunta": "El plástico está presente en..."
            },
            {
                "t11pregunta": "Podemos aseverar que nosotros somos culpables del aumento de la temperatura..."
            },
            {
                "t11pregunta": "Actualmente los gobiernos han adoptado medidas..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona con líneas las columnas según corresponda."
        }
    },//15
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El surgimiento y auge de las máquinas se dio con la Revolución Industrial a finales del siglo XVIII, en Europa.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los griegos fueron los primeros en crear máquinas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El surgimiento de las máquinas ha tenido como consecuencia el aumento en la producción de la industria a nivel mundial.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las máquinas surgieron por la necesidad del hombre de mantener una agricultura estable y capaz de satisfacer a su población.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El surgimiento de las máquinas y la Revolución Industrial han permitido el desarrollo de nuevas tecnologías con las que vivimos actualmente.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    }
];