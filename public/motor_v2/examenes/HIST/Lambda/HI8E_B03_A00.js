
json=[
//reactivo 1 relacionar columnas
{
"respuestas": [
            {
                "t13respuesta": "La revolución industrial y la lucha del liberalismo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Lucro mediante la consolidación, que permitiría el crecimiento de imperios europeos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Una competencia entre países que daría paso a conflictos de escalas nunca antes vistas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "El socialismo y el constitucionalismo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Son consecuencia de la revolución industrial.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El mundo se transformó a consecuencia de:"
            },
            {
                "t11pregunta": "La revolución industrial representó una nueva forma de lucro:"
            },
            {
                "t11pregunta": "La revolución trajo también un gran avance científico y tecnológico que desencadenó: "
            },
            {
                "t11pregunta": "Algunos de los grandes movimientos que surgieron durante la industrialización fueron:"
            },
            {
                "t11pregunta": "Las telecomunicaciones, los medios de transporte y la energías no renovables."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"<b>Relaciona las columnas según corresponda.</b>"
        }
    },





//reactivo 2 falso o verdadero
//correcto 0 falso 1


{
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El socialismo es un movimiento religioso nacido en Francia a finales del siglo XVI.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El constitucionalismo surge junto con el socialismo como movimientos sociales a causa de la revolución industrial.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El socialismo, a diferencia del comunismo, no se opone a la propiedad privada.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Revolución Industrial tuvo un impacto positivo en la sociedad, pero negativo con el ambiente.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El socialismo pretende que todas la personas de la sociedad estén involucradas en la administración de los medios de producción.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b><p>Elige falso (F) o verdadero (V) según corresponda.<\/p></b>",
            "descripcion":"Reactivos",
            "evaluable":false,
            "evidencio": false
        }
    },

    //reativo 3 opcion multiple

    {  
      "respuestas":[
         {  
            "t13respuesta": "El imperialismo",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La colonización",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La conquista",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "El expansionismo",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta": "El imperialismo",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "La colonización",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "La conquista",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "El expansionismo",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
           {  
            "t13respuesta": "Le permitiera establecer sus industrias en el territorio.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Pagara impuestos a la corona inglesa.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Firmara un acta diplomática.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta": "Inglaterra no reconociera a México como independiente.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
            {  
            "t13respuesta": "El carbón",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "El petróleo",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "El gas",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta": "La gasolina",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
          {  
            "t13respuesta": "La industria metalúrgica",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "La industria mercantil",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "La industria naval",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }, 
         {  
            "t13respuesta": "La agricultura ",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }

        
      ],
      "pregunta":{  
         
         "c03id_tipo_pregunta":"1",
         
         "t11pregunta": ["<b>Selecciona la respuesta correcta.</b><br><br>Consistió en el gran poder e influencia que desarrollaron algunas naciones para dominar a otras:",
          "El poder político y económico obtenido de industrialización y la capacidad militar fueron posibles gracias a: ","México obtuvo el reconocimiento de Inglaterra como país independiente a cambio de que:",
          "Con el surgimiento de las máquinas de energía se comenzó a utilizar como fuente de energía:","Antes de la revolución industrial el carbón se utilizaba exclusivamente en los hogares y en:"],

         "preguntasMultiples": true,

         "columnas":1
         
      }
   },
   //reactivo 4 verdadero falso
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mayor problema ecológico de la industrialización fue la alta emisión de CO2.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las luchas de los movimientos obreros estaban impulsadas por el humanismo, la ilustración y el socialismo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uno de los descubrimientos que más impacto tuvo fue el petróleo, pues permitió el desarrollo de la industria maderera.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El panorama de las ciudades decayó y obligó a sus habitantes desplazarse al campo.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las migraciones del campo a la ciudad provocaron asentamientos a las orillas de la ciudad y, con ello, cinturones de miseria.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las extremas condiciones de pobreza de los obreros  orillaron a este grupo a iniciar levantamientos que buscaban  mejores condiciones de trabajo, un mejor sueldo y derechos laborales.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b><p>Elige falso (F) o verdadero (V) según corresponda.<\/p></b>",
            "descripcion":"Reactivos",
            "evaluable":false,
            "evidencio": false
        }
    },

   //reactivo 5 opcion multiples respuestas
    {  
      "respuestas":[
         {  
            "t13respuesta": "Son estados con múltiples sistemas de gobierno.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Surgen después de las revoluciones liberales, en el siglo XIX.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Son estados conformados por dos o más naciones, controlados por un gobierno.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Surgen a finales del siglo XVII, después del inicio de la modernidad.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta": "El imperio ruso",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El Imperio otomano",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El imperio mexicano",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "El imperio francés",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
           {  
            "t13respuesta": "Fue fundado en 1804 por el emperador Francisco I.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Estaba conformado por poblaciones de alemanes, serbios, eslovacos, eslovenos, checos, polacos, rumanos, húngaros italianos y ucranianos.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Antes de ser un imperio habían sido seminómadas dedicados a la ganadería.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta": "Su extensión abarcaba desde Asia hasta Europa, por lo que diferentes comunidades étnicas habitaban en él. ",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
            {  
            "t13respuesta": "Abarcaba desde Asia hasta Europa, por lo que diferentes comunidades étnicas habitaban en él. ",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Estaba bajo el dominio de un zar.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Fue fundado en 1804 por el emperador Francisco I.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta": "Su monarca era llamado sultán.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
          {  
            "t13respuesta": "Era dirigido por un sultán.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Adoptó el Islam como religión por la expansión musulmana.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Estaba bajo el dominio de un zar.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }, 
         {  
            "t13respuesta": "Fue fundado en 1804 por el emperador Francisco I. ",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Democracia parlamentaria.",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Crear un sentimiento nacionalista en sus habitantes.",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Monarquía.",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }, 
         {  
            "t13respuesta": "Expandir la tolerancia a las múltiples religiones que conformaban las etnias.",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }

        
      ],
      "pregunta":{  
         
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["<b>Selecciona todas las respuestas correctas para cada pregunta. <br><br/></b>¿Qué son y cuándo surgieron los estados multinacionales?",
          "¿Cuáles son algunos de los estados multinacionales?","El imperio austriaco...",
          "El imperio ruso...","El imperio otomano...","¿Qué forma de gobierno tenían y qué buscaban los estados multinacionales?"],

         "preguntasMultiples": true,

         "columnas":1
         
      }
   },

   //reactivo 6 relaciona columnas
{
"respuestas": [
            {
                "t13respuesta": "Fue el único camino visible para alcanzar su emancipación de los reinos europeos. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Motivaron la independencia de las colonias de América",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Complicaron el desarrollo de las naciones latinoamericanos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Buscaba la abolición de la esclavitud.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Afectaría profundamente su economía.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Permitió la creación de los estados confederados.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La lucha armada para México, centro y el sur de América:"
            },
            {
                "t11pregunta": "La revolución de las trece colonias y la revolución francesa:"
            },
            {
                "t11pregunta": "Las intervenciones de los Estados Unidos y las potencias europeas: "
            },
            {
                "t11pregunta": "La guerra de secesión estadounidense:"
            },
            {
                "t11pregunta": "Los grupos conservadores se negaban a abolir la esclavitud porque:"
            },
            {
                "t11pregunta": "Cuando Lincoln ganó la presidencia los estados del sur:"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"<b>Relaciona las columnas según corresponda.</b>"
        }
    },
    //reactivo 7 falso verdadero

    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Primera Guerra Mundial estalló a causa del asesinato del archiduque Francisco Fernando en Serbia en 1914.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Triple Alianza estaba formada por Inglaterra, Francia y Rusia.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Triple Entente estaba conformada por Alemania, Italia y el imperio Austrohúngaro.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Revolución Mexicana fue producto de los abusos de la dictadura porfirista, estallando en 1910.",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de la Revolución China se tenía un gobierno democrático, después instauraron la monarquía.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Revolución Rusa, liderada por Lenin, fue una de las causas de la Primera Guerra Mundial.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b><p>Elige falso (F) o verdadero (V) según corresponda.<\/p></b>",
            "descripcion":"Reactivos",
            "evaluable":false,
            "evidencio": false
        }
    },
    //reactivo 8 opcion multiple

 {  
      "respuestas":[
         {  
            "t13respuesta": "Charles Darwin",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Nikola Tesla",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Isaac Newton",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Antonio Vivaldi",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta": "Religiosa",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Política",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Científica",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Bucólica",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
           {  
            "t13respuesta": "Karl Marx: “No basta pensar el mundo, hay que transformarlo”.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Friedrich Nietzsche: “No hay hechos, solo interpretaciones”",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Friedrich Engels: “Una onza de acción vale una tonelada de teoría”.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta": "Karl Popper: “La democracia consiste en poner bajo control el poder”.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
            {  
            "t13respuesta": "Sigmund Freud",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Melanie Klein",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Carl Jung",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta": "Jacques Lacan ",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
          {  
            "t13respuesta": "Expresionismo",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Dadaísmo",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Barroco",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }, 
         {  
            "t13respuesta": "Impresionismo ",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }

        
      ],
      "pregunta":{  
         
         "c03id_tipo_pregunta":"1",
         
         "t11pregunta": ["<b>Selecciona la respuesta correcta.</b><br><br>Publicó en 1859 su obra titulada El origen de las especies de donde se desprendía la teoría de la evolución",
          "Antes de la propuesta darwinista, la única explicación para el origen del hombre era: ","Este personaje inició como periodista. Defendió los movimientos obreros en europa y después fue el inventor del socialismo.",
          "Aportó y desarrolló el concepto del inconsciente:","Surge en Alemania como movimiento artístico a principios del S.XX."],

         "preguntasMultiples": true,

         "columnas":1
         
      }
   },
   //reactivo 9 falso verdadero

   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La primera feria mundial se organizó en México en 1851 y se llamó “La Gran Exposición”.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las ferias servían para dar a conocer los inventos, además tenían utilidad económica y social.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los avances médicos en el S.XX permitieron el aumento demográfico. ",
                "correcta"  : "1"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La temática sexual antes del S.XX permitía el control natal.",
                "correcta"  : "0"
            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El baby boom se desarrollo durante la década de los cincuenta en E.U.A.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b><p>Elige falso (F) o verdadero (V) según corresponda.<\/p></b>",
            "descripcion":"Reactivos",
            "evaluable":false,
            "evidencio": false
        }
    }
   
];   