json=[
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la Edad Moderna, a mediados del s. XVII, el sistema de gobierno heredado por la Edad Media seguía siendo el más rentable.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La ilustración buscaba limitar el poder de las monarquías y formar un sistema de gobierno en el que el pueblo participara.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Ilustración es fruto del Siglo de las Luces: siglo en que los avances y los descubrimientos científicos y tecnológicos querían “llevar la razón de la humanidad a la luz”.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El liberalismo fue un movimiento que a través de la lucha armada derrocaría a los gobiernos democráticos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El liberalismo buscaba la libertad y defensa de los derechos de algunas sociedades, lo que desencadenó revoluciones; primero la de las Trece Colonias y después La Revolución Francesa.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "...las migraciones hacia las colonias donde la identidad y cultura de las poblaciones se modificó.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...generó rencor social, que impulsado por las críticas de periódicos y revistas, terminaría en luchas armadas para derrocar a los gobiernos monárquicos en América.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...Inglaterra. Fue fruto del desarrollo tecnológico donde las máquinas de combustión interna jugaron un papel importante",
                "t17correcta": "3"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Los cambios sociales en un principio fueron causa de..."
            },
            {
                "t11pregunta": "Los abusos de los conquistadores hacia la población oriunda y posteriormente hacia la población que no fuera aristócrata..."
            },
            {
                "t11pregunta": "La Revolución Industrial durante la segunda mitad del siglo XVIII se desarrolló en..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "España",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Francia",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Inglaterra",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Alemania",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Monarquías parlamentarias",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Monarquías feudales",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Monarquías absolutas",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Monarquías bicéfala",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Felipe II",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Luis XVI",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Napoleón I",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Luis XIV",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Ludismo",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Reforma protestante",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Ilustración",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Filosofía",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Divulgar el conocimiento",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Imponer la democracia",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Reformar la iglesia",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Acabar con el feudalismo",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la respuesta correcta.<br><br>Fue la primera sociedad donde, gracias a la lucha de los burgueses, se logró consolidar un Parlamento para contrarrestar el poder autoritario del monarca:",
                "En este  tipo de gobierno el monarca hacía lo que quería y bajo ese precepto aumentaba su poder:",
                "¿Qué rey francés es el referente principal de un monarca absolutista?",
                "Este movimiento buscaba terminar con las monarquías, bajo la premisa de “iluminar con la luz de la razón al oscurantismo de los siglos anteriores”:",
                "Los ilustrados eran un grupo de funcionarios, empresarios y profesionistas que buscaban:"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Después de la Guerra de los Siete Años la corona inglesa no tenía dinero para cubrir los gastos de la guerra, así que decidió que la solución perfecta era subir los impuestos a las colonias en América.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Que Inglaterra subiera los impuestos a las colonias americanas alegró mucho a los colonos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las colonias de América querían más independencia, pero Inglaterra quería más control sobre los territorios colonizados.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1774 Inglaterra continuaba intentando imponer impuestos a las colonias americanas, mientras con su ejército obligaba a los colonos a aceptarlos, esto desencadenó que los americanos boicotearan el comercio.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El rey de Inglaterra decidió dejar las colonias en paz y firmó el acta de independencia.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "Reactivó su economía",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Redujo gastos reales",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Pidió préstamos a otros países",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Cobró altos impuestos a sus súbditos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "10%",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "40%",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "70%",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "90%",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Controlaba sus gastos",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Decidió bajar los impuestos",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Se preocupó por alimentar a su pueblo",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Se daba una vida de lujos y ostentosidad",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Redactar una constitución política para Francia",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Arrestar, sentenciar y ejecutar al rey Luis XIV",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Proclamar la lucha en contra del monarca",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Tomar el palacio de Versalles",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "11 de marzo de 1789",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "10 de septiembre de 1789",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "14 de julio de 1789",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "21 de octubre de 1789",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la respuesta correcta.<br><br>Al haber ayudado a las Trece Colonias en su guerra de independencia, las arcas de la corona francesa se vaciaron, por lo que Francia:",
                "¿Qué porcentaje de la población francesa del siglo XVIII era pobre y se dedicaba básicamente al campo y a las artesanías?",
                "Ante la situación de crisis y hambruna que se vivía en Francia, Luis XIV:",
                "El pueblo enojado y desesperado por la situación constituyó una Asamblea Nacional el 16 de junio de 1789 para:",
                "¿En qué fecha fue tomada la prisión de la Bastilla destruyendo así uno de los máximos símbolos de opresión de la monarquía?"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "...lucharían contra el pueblo francés para consolidar su poder y reclamar Francia como trofeo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...Napoleón Bonaparte.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...además de que a sus 27 años había logrado convertir a un grupo de hombres hambrientos y desmoralizados en una máquina bélica.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...después de probar que podía ser un buen líder y gobernante con sus súbditos, en 1804 se hizo coronar Emperador.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...esto desencadenó las guerras de independencia de las colonias americanas.",
                "t17correcta": "5"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Ante la Revolución Francesa otras monarquías europeas..."
            },
            {
                "t11pregunta": "El personaje que terminaría el desorden y la anarquía francesa sería..."
            },
            {
                "t11pregunta": "Después de ganar varias batallas Napoleón se consolidó como un gran líder..."
            },
            {
                "t11pregunta": "Napoleón dio un golpe de estado al gobierno francés, convocó a elecciones y asumió el cargo de cónsul..."
            },
            {
                "t11pregunta": "Napoleón invadió España y nombró rey a su hermano José Bonaparte..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    // Reactivo 7
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La invasión francesa a España favoreció los movimientos independentistas de las colonias dependientes de los virreinatos de la Nueva España y Perú.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Desde finales del siglo XVIII las colonias españolas estaban inconformes, en especial el grupo de los criollos, ya que no podía acceder a los altos puestos públicos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una de las motivaciones de los movimientos independentistas fue la Guerra de los Siete Años.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La guerra de independencia de las Trece Colonias y la Revolución Francesa motivaron a los grupos hispanoamericanos independentistas a llevar a cabo su lucha.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "<p>James Watt<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>máquina<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Revolución Industrial<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>fábricas modernas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>transporte<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>tecnológico<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>disparidad<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>reemplazar<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>desempleo<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>avaricia<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>injusticia<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>Revolución Francesa<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>Johannes Gutenberg<\/p>",
                "t17correcta": "13"
            },
        ],
        "preguntas": [{
                "t11pregunta": "La necesidad de sacar agua de las minas llevó al inventor escocés "
            },
            {
                "t11pregunta": " a inventar la "
            },
            {
                "t11pregunta": " de vapor, misma que sería clave en la "
            },
            {
                "t11pregunta": ". Este sería uno de los hechos históricos más importantes de la época, ya que con esta revolución se daría el origen a las "
            },
            {
                "t11pregunta": ", a la producción en masa, a una mejora en el "
            },
            {
                "t11pregunta": " y a un cambio radical para el mundo en general. La invención de las máquinas que pudieran llevar a cabo las tareas del hombre significó un gran avance "
            },
            {
                "t11pregunta": " para la humanidad, pero también creó  "
            },
            {
                "t11pregunta": " e injusticia en las clases bajas, ya que las máquinas terminarían por "
            },
            {
                "t11pregunta": " a los hombres en las fábricas y esto generaría "
            },
            {
                "t11pregunta": " y pobreza gracias a la "
            },
            {
                "t11pregunta": " de los dueños de las industrias."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen los enunciados.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 9
    {
        "respuestas": [{
                "t13respuesta": "La explotación laboral",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Los bajos salarios",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La falta de comercio",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La falta de comida",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La falta de seguridad para los trabajadores",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Fourier",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Goethe",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Robert Owen",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Julio Verne",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Saint Simon",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Un salario mínimo para el obrero",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Que todo hombre mayor de 21 años tuviera derecho al voto",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Reducir los impuestos",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Que el voto fuera secreto para evitar represalias",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Que el parlamento se eligiera cada año",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "James du Potiers",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Karl Marx",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Jean Jacques Rousseau",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Friedrich Engels",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "George Washington",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "El ferrocarril",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "El barco de vapor",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "La producción en masa",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "La luz eléctrica",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "El entretenimiento",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": [
                "Selecciona las respuestas correctas para cada pregunta.<br><br>Los movimientos obreros surgen a causa de:",
                "Algunos de los autores que desarrollaron el pensamiento socialista fueron:",
                "Algunas de las demandas que exigían los cartistas al Parlamento inglés fueron:",
                "¿Quiénes desarrollaron el socialismo científico?",
                "La Revolución Industrial trajo consigo avances tecnológicos como:"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 10
    {
        "respuestas": [{
                "t13respuesta": "...motivando así a las demás colonias americanas a independizarse.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...deshacerse de las injusticias, el abuso y la explotación.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...el no tener un proyecto de nación sólido, ni una planeación de cómo alcanzarlo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...esto provocó que los pueblos se dividieran en conservadores y liberales.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...adoptar un sistema de gobierno como el de Francia o el de Estados Unidos.",
                "t17correcta": "5"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Las Trece Colonias se independizaron de  Inglaterra en 1776..."
            },
            {
                "t11pregunta": "A pesar de que todas las luchas independentistas fueron distintas, coincidían en..."
            },
            {
                "t11pregunta": "Uno de los factores que complicaría la vida independiente de los países nuevos era..."
            },
            {
                "t11pregunta": "A pesar de que la clase rica luchó por la independencia, no quería perder sus privilegios..."
            },
            {
                "t11pregunta": "La mayoría de los nuevos países querían..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    // Reactivo 11
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las nuevas naciones hispanoamericanas eran capaces de mantenerse a flote tanto social como económicamente después de su guerra de independencia.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ante las situación hispanoamericana, las potencias europeas trataron de aprovecharse de la situación.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Inglaterra fue el primer país en reconocer como independientes a otras naciones en América.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estados Unidos estableció la “Doctrina Monroe”, que reclamaba América para los americanos, impidiendo así que hubiera alguna intervención europea en el continente después de las guerras de independencia.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1864 Francia instauró una monarquía en México, con Maximiliano de Habsburgo como regente.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 12
    {
        "respuestas": [{
                "t13respuesta": "Comedia",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Consolación",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Ensayo",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Novela",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Alemania",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "España",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Francia",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Inglaterra",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Jonathan Swift",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Julio Verne",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Edgar Allan Poe",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Francois de Sade",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Libertad de prensa",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Libertad",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Libertinaje",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Derecho a la comunicación",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Relevo",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Invención",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Secularización",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Politización",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la respuesta correcta.<br><br>Género literario que aparece en el siglo XVIII:",
                "¿En qué país surge el folletín durante la época del romanticismo?",
                "Los viajes de Gulliver es una sátira a la vanidad y la hipocresía de las cortes de su época, ¿por quién fue escrita?",
                "De la misma manera en que el hombre es libre, las ideas también deben de ser libres de ser compartidas y expresadas. Esto se conoce como:",
                "Al proceso en el que el Estado toma el control y construcción de las escuelas en su manos, relevando a la iglesia de ese cargo, se le conoce como:"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 13
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el año 428 a.C.  hubo una epidemia de peste en Atenas, Grecia.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En Siracusa, en la Anatolia 396 a.C. se presentó una epidemia de gripe.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La peste Amarilla atacó Inglaterra en el año 550 a.C.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La epidemia más famosa y mortífera del mundo fue la Peste Negra, que en el año 1350 d. C. se extendió por toda Europa.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La viruela fue llevada  hacia América por los españoles.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 14
    {
        "respuestas": [{
                "t13respuesta": "...el sector textil.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...la vestimenta se convirtió en una necesidad básica.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...gracias a que la Revolución Industrial apoyó el desarrollo e hizo crecer la industria textil.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...contaban con una amplia gama de colores.",
                "t17correcta": "4"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Uno de los sectores que más se desarrolló gracias a la Revolución Industrial fue..."
            },
            {
                "t11pregunta": "Gracias a los avances tecnológicos de la industria..."
            },
            {
                "t11pregunta": "La moda y la vestimenta se convirtieron en un negocio muy lucrativo..."
            },
            {
                "t11pregunta": "Ya  durante el siglo XVIII, las prendas textiles..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
];
