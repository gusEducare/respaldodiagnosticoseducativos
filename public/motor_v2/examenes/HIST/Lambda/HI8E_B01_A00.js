json = [
    //1
   
   {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EUn felino\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EUn équido\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EUn primate\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EUn paquidermo\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },


            {
                "t13respuesta": "\u003Cp\u003EGriega\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "\u003Cp\u003EGermana\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "\u003Cp\u003ERomana\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "\u003Cp\u003EChavín\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },



            
            {
                "t13respuesta": "\u003Cp\u003EEl Rin\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "\u003Cp\u003EEl Nilo\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },{
                "t13respuesta": "\u003Cp\u003EEl Mediterráneo\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "\u003Cp\u003EEl Mar Rojo\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },




        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br/><br/>Según la teoría científica más aceptada, la humanidad proviene de:",
                "Cual de las siguientes civilizaciones no forman parte de la civilizaciones antiguas:",
                "La civilización Griega se desarrolló, junto con la romana y la fenicia, en:",
                ],
            "preguntasMultiples": true
        }
    },
    //2 Relaciona columnas
    {
        "respuestas": [
            {
                "t13respuesta": "maya, olmeca y teotihuacana.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "chavín , nazca y maché.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Asia, al igual que la cultura China.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "a las orillas del Mar Mediterráneo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "la civilización egipcia.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Fueron civilizaciones que se desarrollaron en Mesoamérica:"
            },
            {
                "t11pregunta": "Estas civilizaciones son oriundas del sur de América:"
            },
            {
                "t11pregunta": "Mesopotamia e India se desarrollaron en..."
            },
            {
                "t11pregunta": "Las civilizaciones romana y griega se desarrollaron..."
            },
            {
                "t11pregunta": "El Nilo es el río más largo del continente africano y cuna de..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    //3 respuestas multiples
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EEl descubrimiento de América\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EEl fin de la Guerra Franco-Prusiana\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003ELa caída de Constantinopla\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EEl fin de la Guerra de los 100 años\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },




            {
                "t13respuesta": "\u003Cp\u003EEl fin de la Contrarreforma\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "\u003Cp\u003ELa invención de la máquina de vapor\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "\u003Cp\u003ELa Revolución Francesa\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "\u003Cp\u003ELa invención de la imprenta\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },



        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona la(s) respuesta(s) correcta(s):<br/><br/>¿Qué acontecimientos se toman como referente del inicio de la Edad Moderna?",
                "¿Qué acontecimientos marcan el fin de la Edad Moderna?",],
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //4 falso o verdadero

    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Edad Moderna abarca aproximadamente desde el siglo XV hasta nuestro días.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El proceso de la Reforma y la Contrarreforma se dió durante la Edad Moderna.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las revoluciones más importantes a nivel mundial durante la Edad Moderna, fueron  la Revolución Francesa y la Revolución Mexicana.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El periodo histórico conocido como Renacimiento, se ubica a finales de la modernidad.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uno de los pensadores más importantes de la modernidad, fue René Descartes.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda:<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true,
        }
    },
    //5 Opcion multiple
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EEl Imperio romano\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EEl Imperio chino\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EEl Imperio mongol\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EEl Imperio turco\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },




            {
                "t13respuesta": "\u003Cp\u003EAkbar\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "\u003Cp\u003EKublai Khan\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "\u003Cp\u003EGengis Khan\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "\u003Cp\u003EMehmed II\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },



         
            {
                "t13respuesta": "\u003Cp\u003EUna joven persa-musulmana, esposa de un príncipe mongol, que murió al dar a luz.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "\u003Cp\u003EUna batalla ganada a los indios y la resistencia ante la expansión del imperio mongol.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },{
                "t13respuesta": "\u003Cp\u003EEl dios de la región, que había favorecido a Khan en la expansión de su imperio.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "\u003Cp\u003EEl poder de los mongoles que se manifestaba a través de las culturas que iban  conquistando.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },




            {
                "t13respuesta": "\u003Cp\u003EURSS\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "\u003Cp\u003ETaiwán\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "\u003Cp\u003EChina\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "\u003Cp\u003ECorea\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },


            
            {
                "t13respuesta": "\u003Cp\u003EMegalópolis\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003ECiudadelas\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003EBurgos\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003ECentrales\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>¿Qué imperio fue el más extenso en toda la historia de Asia y Europa?",
                "¿Quién fue el líder fundador del Imperio mongol?",
                "Una de las construcciones de India más famosas es el Taj Mahal, construida en honor a:",
                    "Uno de los imperios más antiguos de Asia fue:",
                     "Ante la necesidad creciente de establecer sus negocios, para vender el excedente de producción de los campesinos, los centros urbanos europeos comenzaron a crecer y recibieron el nombre de:"],
            "preguntasMultiples": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "España",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Portugal",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Francia",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Inglaterra",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para el siglo XV el reino de Granada, se mantenía bajo el control musulmán.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Monarquía formada principalmente de normandos y daneses, tenía un  parlamento.",
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La última dinastía reinante en ella fueron los Capetos.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Este reino formaba parte del territorio perteneciente a los hispanos. Ocupado por los musulmanes y recuperado por los cristianos en el siglo X.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El matrimonio de Fernando de Aragón e Isabel de Castilla, logrando consolidar la monarquía y expulsar a los musulmanes. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1558 la primer mujer monarca asciende al trono con el nombre de Isabel I.",
                "correcta": "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Esta monarquía culminó con una de las revoluciones más grandes e importantes del mundo occidental.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su primer rey fue Alfonso I, quien mantuvo la independencia de este reino.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona el país que se describe en los enunciados.<\/p>",
            "descripcion": "Descripción",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //7  falso Verdadero
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Milán, Florencia, Génova, Venecia, entre otras fueron importantes ciudades de Grecia.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el siglo XV, en las principales ciudades de la península itálica, surge en la clase rica el interés por la cultura y arte de las antiguas culturas griega y romana, de manera que realizan construcciones imitando la arquitectura y escultura de estas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Venecia y Génova fueron ciudades que gracias a su ubicación geográfica y vocación naval, lograron controlar el paso del comercio proveniente de oriente hacia europa.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las Ciudades-Estado surgen a partir de una clase política de regentes que procedían de dinastías con cientos de años de existencia.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las Ciudades-Estado se denominan de tal manera pues tenían un gobierno propio y eran independientes.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda:<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true
        }
    },
    //8 Arrastra corta
    {
        "respuestas": [
            {
                "t13respuesta": "Edad Media",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "periodo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "conocimiento",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "italianas",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "filosófica",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Humanismo",
                "t17correcta": "6"
            },
            {
                "t13respuesta": " la imprenta",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "investigación",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "Nicolás Copérnico",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "centro del universo",
                "t17correcta": "10"
            }, {
                "t13respuesta": "Prehistoria",
                "t17correcta": "13"
            },{
                "t13respuesta": "educación",
                "t17correcta": "13"
            },{
                "t13respuesta": "reyes",
                "t17correcta": "13"
            },{
                "t13respuesta": "francesas",
                "t17correcta": "13"
            },
            
        ],
        "preguntas": [
            {
                "t11pregunta": "Suele extenderse, malamente, la idea de que la "
            },
            {
                "t11pregunta": " es una época de oscurantismo con nulos avances, pues la cultura del progreso científico y tecnológico no se dio durante este periodo. Sin embargo hay que recordar que, más que un oscurantismo, la Edad Media fue un "
            },
            {
                "t11pregunta": " de conformación social y de transmisión cultural. Si los monjes de los conventos no hubieran hecho toda la tarea de copiar la información, cultura y conocimientos que se tenían hasta la época probablemente se hubieran perdido para siempre. Hasta cierto punto este proceso fue responsable de que todo este "
            },
            {
                "t11pregunta": " , ideas, cultura e información llegara hasta el periodo en que se recuperó todo esto. <br>En algunas ciudades "
            },
            {
                "t11pregunta": " comenzó un interés por las culturas antiguas y esto propició una revolución cultural, una especie de renacer de estas culturas antiguas. Este periodo dio paso a una nueva corriente de pensamiento, "
            },
            {
                "t11pregunta": " y científica llamada "
            },
            {
                "t11pregunta": ", misma que sería la que se encargaría de cimentar las bases sobre las que descansan la ciencia y sus métodos, mismo que actualmente se siguen utilizando.  <br>Con este periodo llegó también uno de los inventos más importantes del mundo occidental: "
            },
            {
                "t11pregunta": " tipográfica, misma que relevó a los monjes de su quehacer como guardianes y celadores de la cultura. <br>Por otra parte, los humanistas se dieron cuenta que la mejor manera de hacer nuevos descubrimientos era la "
            },
            {
                "t11pregunta": ", para esto establecieron un método en el que el principio más importante es que los resultados debían ser comprobables, es decir, cualquier experimentación en cualquier lugar debía siempre ser demostrable y con los mismos resultados en cada ocasión. Gracias a ello "
            },
            {
                "t11pregunta": " desarrollaría una de las teorías científicas más importantes y que cambiarían la manera en que el mundo se veía: la tierra ya no era el "
            },
            {
                "t11pregunta": "."
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra el concepto o palabra al espacio según corresponda:",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //9 RESPUESTA multiple
    {
        "respuestas": [
            {
                "t13respuesta": "Quitar a la iglesia romana como intermediaria entre Dios y el hombre",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Cada persona puede ser su propio guía espiritual.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "Que el mundo ya no era el centro del universo como decía la iglesia.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "La iglesia era un nido de corrupción e incongruencias.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona las respuestas correctas:<br/><br/>¿Cuales eran las consideraciones de Martín Lutero para fundar el movimiento protestantista?",
               ],
            "preguntasMultiples": true,
            "evaluable": true,

        }
    },

    //10 Opción múltiple
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EBizantinos\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EEgipcios\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EVenecianos\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003ETurcos otomanos\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "\u003Cp\u003EJudíos\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },





            {
                "t13respuesta": "\u003Cp\u003EAnkara\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EBurdur\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EEstambul\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ETokat\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EKarabük\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
         

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>¿Cuál de los siguientes pueblos era en un principio seminómada dedicado a la ganadería de caballos y al comercio?",
                "Después de su caída, Constantinopla cambió de nombre bajo la administración de los turcos otomanos, ¿cuál sería su nuevo nombre?",
            ],
            "preguntasMultiples": true
        }
    },
   
    //11 Falso o Verdadero
 {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando los españoles reclamaron sus posesiones en América les fue fácil ya que los pueblos originarios no presentaron resistencia alguna.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cristóbal Colón sería quien pasaría a la historia como el gran conquistador del Imperio mexica cuando logró su caída en 1521.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El inicio y el fin del virreinato marcaron la composición de la sociedad mexicana para siempre.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El virreinato es el periodo en el que lo imperios mesoamericanos cayeron a manos de las colonias asiáticas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El periodo conocido como virreinato abarca tres siglos  a lo largo de los cuales hubo 63 virreyes.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al final del siglo XVI, la población que habitaba la Nueva España estaba compuesta por solo españoles.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los criollos eran aquellos que eran hijos de españoles nacidos en los nuevos territorios.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La mezcla de españoles y mestizos resultaban en los castizos.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true
        }
    },

 
 //12 Relaciona Columnas
 {
        "respuestas": [
            {
                "t13respuesta": "romana y griega, gracias al humanismo que recuperó las expresiones artísticas de estos pueblos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "ser muy realista, buscaba representar a las personas, animales u objetos de la manera más real posible.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Da Vinci, Miguel Ángel Buonarroti y Rafael.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Dante Alighieri, quien es considerado como un gran humanista.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "la exuberancia ornamental, así como la preeminencia de las cúpulas y planta de cruz latinas en los templos.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "y de esta última están grandes exponentes como Johannes Sebastian Bach o Antonio Vivaldi.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El arte Renacentista tuvo una gran influencia..."
            },
            {
                "t11pregunta": "La corriente artística del humanismo se caracterizó por..."
            },
            {
                "t11pregunta": "Entre los artistas más famosos del renacimiento se encuentran..."
            },
            {
                "t11pregunta": "La Divina Comedia es una de las obras más famosas del renacimiento, escrita por..."
            },
            {
                "t11pregunta": "El arte Barroco italiano se caracteriza por..."
            },
            {
                "t11pregunta": "El Barroco abarcó otras áreas como la pintura, la literatura, la escultura o la música..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },






//13 Matriz Editable
 {
        "respuestas": [
            {
                "t13respuesta": "Arte islámico",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Arte chino",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Arte japonés",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se caracterizan las mezquitas por sus cúpulas y adornados de figuras geométricas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su construcciones, como las pagodas, se extendían por toda Asia y estas fungen principalmente como templos. Uno de los más representativos puede ser el Templo del Cielo.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su arte está fuertemente influenciado por el arte chino, por lo que sus construcciones arquitectónicas son muy similares.",
                "correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada una.<\/p>",
            "descripcion": "Características",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },

//14 Falso verdadero

 {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La historia de la navegación tiene sus antecedentes en la Edad Antigua.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los egipcios desarrollaron la implementación de velas en sus barcas, aprovechando así la energía eólica.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La navegación formó parte de la vida de los pueblos mediterráneos hasta la edad moderna.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de que los navegantes se hubieran aventurado a ultramar, lo que hacían era viajar cerca de las costas rodeandolas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La carabela fue una de las embarcaciones más grandes de su tiempo, y podía llegar a alcanzar hasta los 50 km/h.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda:<\/p>",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,
        }
    },
//15Respuesta múltiple

{
        "respuestas": [
            {
                "t13respuesta": "Se desarrolla a partir del siglo XV.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "La Ilustración confrontó a los aristócratas y personas privilegiadas.",
                "t17correcta": "1", 
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Una gran cantidad de avances científicos y tecnológicos.",
                "t17correcta": "1", 
                 "numeroPregunta": "0"
            }, {
                "t13respuesta": "Surgió el liberalismo.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EFue un movimiento intelectual surgido en Italia a inicios del siglo XIV.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EBuscaban dominar sobre los pobres para explotarlos y generar más riqueza.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EDominaba fuertemente una ideología teísta.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EUno de los movimientos intelectuales más importantes fue el espiritismo. \u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas.<br/><br/>¿Cuáles son algunas características de la Edad Moderna?"             
              ],
            "preguntasMultiples": true,
            "evaluable": true
        }
    },

   
//16Falso verdadero




    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una revolución se refiere exclusivamente a un enfrentamiento armado entre dos bandos diferentes.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Edad Moderna comienza en Asia y avanza lentamente hacia Europa y posteriormente a América.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la Revolución Industrial las máquinas de vapor jugaron un papel esencial.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uno de los libros más influyentes durante la Ilustración fue la Enciclopedia.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la Edad Moderna se dieron diferentes levantamientos que exigían la independencia de diferentes regiones.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A la época anterior a la Ilustración se le conoce como Oscurantismo.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,
        }
    },
    //17 Relaciona columans
    {
        "respuestas": [
            {
                "t13respuesta": "Era el espacio donde se reunían intelectuales durante el siglo XVIII para intercambiar ideas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es el movimiento intelectual y revolucionario de mediados del siglo XVIII que influyó fuertemente para instaurar un gobierno democrático.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Ideología que buscaba proteger los derechos de todos los hombres y buscaba contrarrestar las monarquías absolutas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Los gobernantes se creían elegidos por un “derecho divino” que los legitimaba para gobernar como quisieran.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Los gobernantes se encontraban influenciados por ideas de la Ilustración y simpatizaban con estas ideas para llegar al poder.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Uno de los libros más influyentes de la Ilustración, compiló una gran cantidad de conocimientos.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Institución que limitaba el poder de los monarcas.",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Salones literarios"
            },
            {
                "t11pregunta": "Ilustración"
            },
            {
                "t11pregunta": "Liberalismo"
            },
            {
                "t11pregunta": "Absolutismo"
            },
            {
                "t11pregunta": "Despotismo ilustrado"
            },
            {
                "t11pregunta": "Enciclopedia"
            },
            {
                "t11pregunta": "Parlamento"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las líneas según corresponda:"
        }
    }, 
//18  Arrastra corta
{
        "respuestas": [
            {
                "t13respuesta": "<p>John Locke<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>D’Alembert<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Luis XIV<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>siete años<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Inglaterra<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Austriaco<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>España<\/p>",
                "t17correcta": "7"
            },
            
        ],
        "preguntas": [
            {
                "t11pregunta": "<p> <\/p>"
            },
            {
                "t11pregunta": " fue un pensador y erudito inglés que planteó las bases del pensamiento político liberal a finales del siglo XVII.<br>La enciclopedia fue un gran compendio de conocimientos científicos, filosóficos y artísticos que hicieron sobre todo [Denis Diderot] y Jean Le Rond "
            },
            {
                "t11pregunta": ".<br>La monarquía absoluta era caracterizada porque los reyes tenían un poder ilimitado sobre su población y hacían gran cantidad de excentricidades, como ejemplo podemos ver el caso de "
            },
            {
                "t11pregunta": "<p>.<br>La Guerra de los <\/p>"
            },
            {
                "t11pregunta": "<p>fue un conflicto  por la extensión del territorio en el que participaron <\/p>"
            },
            {
                "t11pregunta": "<p>, Prusia, Portugal y el reino de [Hannover]; por un lado, y por otro, el Imperio "
            },
            {
                "t11pregunta": ", Sajonia, Francia, Rusia, Suecia y "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": true,
            "contieneDistractores":true
        }
    },
   
//19 Opción múltiple


{
        "respuestas": [
            {
                "t13respuesta": "4 de julio de 1776",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "4 de junio de 1776",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "16 de septiembre de 1810",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "4 de mayo de 1780",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },





            {
                "t13respuesta": "Thomas Jefferson",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "John Addams",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "John F. Kennedy",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Abraham Lincoln",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
           



           {
                "t13respuesta": "Voltaire, Montesquieu y Rousseau.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Robespierre, Jean Luc Godard y Aviñon.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Rene Descartes, Paul Ricoeur y Jacques Derrida.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Jean Paul Levy, Guy de Maupassant y Emile Zolá.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },




             {
                "t13respuesta": "Fraternidad, igualdad y libertad",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Riqueza, expansión y dominio",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Guerra, sangre y justicia",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Orden, paz y seguridad",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br/><br/>El congreso norteamericano declaró la independencia el día:",
                "Una de las figuras más importantes al redactar la Declaración de la Independencia de los Estados Unidos fue:",
                "Algunos intelectuales que propiciaron la Revolución francesa fueron:",
                "¿Cuáles fueron los valores exaltados durante la Revolución Francesa?"
            ],
            "preguntasMultiples": true
        }
    },
  //20  Respuesta múltiple





  {
        "respuestas": [
            {
                "t13respuesta": "Acabar con la tiranía",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "División del poder en tres partes",
                "t17correcta": "1", 
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "La Constitución limita y otorga poder a los ciudadanos",
                "t17correcta": "1", 
                 "numeroPregunta": "0"
            }, {
                "t13respuesta": "La libertad es un valor fundamental",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003ELos aristócratas aseguran su riqueza\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EBuscan extender el territorio nacional\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003ESe lleva a cabo una recuperación de la cultura clásica\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "\u003Cp\u003EPropone asesinar a los que se opongan al régimen\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },



             {
                "t13respuesta": "La oferta y la demanda",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "Todas las personas son ciudadanos con los mismo derechos",
                "t17correcta": "1", 
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Cualquier persona puede participar en la economía como más le agrade",
                "t17correcta": "1", 
                 "numeroPregunta": "1"
            }, {
                "t13respuesta": "La industria es parte del Estado",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "\u003Cp\u003EÚnicamente los aristócratas pueden hacer negocio\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "\u003Cp\u003ELos precios de las cosas no pueden cambiar\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "\u003Cp\u003ELos más privilegiados conservarán sus privilegios\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona las respuestas correctas para cada pregunta. <br/><br/>¿Cuáles fueron algunas características del Liberalismo?",
            "Selecciona algunas características del liberalismo económico."           
              ],
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
 
   //21 falso verdadero


    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Congreso de Viena y la Santa Alianza fue una reunión en la que se acordó regresar la monarquía como forma de gobierno a algunos países.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ideales liberales y revolucionarios fueron promovidos por los monarcas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Revolución Industrial generó una nueva clase social con pocos privilegios, los obreros.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El sufragio universal quiere decir que todos los hombres pueden elegir a sus gobernantes.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "África fue explotada por los europeos debido a su alta cultura, civilización y avances tecnológicos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El racismo fue una forma de pensar común de los europeos tanto en la conquista de Américo como la de África.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,
        }
    },
//22 Arrastra matris  Horizontal
 {
        "respuestas": [
            {
                "t13respuesta": "<sup>Desorden político<br>en Francia",
                "t17correcta": "1,"
            },  {
                "t13respuesta": "<sup>Napoleón es<br>nombrado<br>emperador.",
                "t17correcta": "2"
            }, {
                "t13respuesta": "<sup>España luchó<br>contra el ejército<br>de Napoleón.",
                "t17correcta": "3"
            },{
                "t13respuesta": "<sup>En 1808 José<br>Bonaparte es<br>nombrado rey de<br>España.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<sup>Los criollos<br>aprovecharon la<br>invasión francesa<br>a España.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<sup>Miguel Hidalgo y<br>Costilla convocó<br>la independencia",
                "t17correcta": "6"
            },
           
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td style='width:150px'><sup>Guerras napoleónicas</td>\n\
                                  <td>"
            },             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        <sup></td><td><sup>Napoleón<br>defiende a<br>francia de Austria</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "       </td><td><sup>Napoleón es<br>derrotado por el<br>zar Alejandro I en<br>1812.</sup></td>\n\
                                   <sup> </tr><tr><td><sup>Invasión<br>francesa a<br>España</td><td>\n\
                                    "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "      <sup>  </td> <td><sup>Napoleón<br>prepara un<br>ejército más<br>grande y fuerte.</td>       <td><sup>La batalla de los<br>Arapiles, es<br>ganada por<br>España.</td>\n\
                                     <sup>  </tr><tr> <td><sup>Independencias<br>en América</td> <td><sup>Nacionalismo<br>criollo en<br>América </td>  <td><sup>Ideas<br>revolucionarias<br>de Estados<br>Unidos y Francia.</td > <td>"
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td> \n\
                                    </tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },


//23 Relaciona Columnas
{ 
 "respuestas":[ 
{ 
 "t13respuesta":"Hizo algunas mejoras a la máquina de vapor.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Sector social de clase acomodada que son propietarios de las fábricas y los medios de producción.", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"Ideología que proponía el bienestar de la mayoría, eliminar la explotación de los trabajadores, etc. Un representante de esta corriente fue Saint Simón.", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"Movimiento que pedía cosas como el derecho al voto para los hombres mayores de 21 años, el voto secreto, participación de los trabajadores en la política entre otras cosas.", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"Fue una teoría filosófica que buscaba explicar los modelos socioeconómicos de la sociedad real y resolver sus problemas mediante el análisis.", 
 "t17correcta":"5", 
 }, 
{ 
 "t13respuesta":"Fue un filósofo alemán que desarrolló junto con F. Engels el socialismo científico.", 
 "t17correcta":"6", 
 }, 
{ 
 "t13respuesta":"Es una asociación agrupada por trabajadores que defiende los intereses de éstos.", 
 "t17correcta":"7", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"James Watt" 
 }, 
{ 
 "t11pregunta":"Burgueses" 
 }, 
{ 
 "t11pregunta":"Socialismo" 
 }, 
{ 
 "t11pregunta":"Cartismo" 
 }, 
{ 
 "t11pregunta":"Socialismo científico" 
 }, 
{ 
 "t11pregunta":"Karl Marx" 
 }, 
{ 
 "t11pregunta":"Sindicato" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11instruccion": "", 
  "t11pregunta": "Relaciona las columnas según corresponda.",
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 },

 //24 falso o verdadero
 {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los salarios de los obreros en las colonias europeas eran bien pagados.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La máquina de vapor se utilizó en diferentes instrumentos, medios de transporte y mecanismos industriales.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Revolución Industrial conlleva mejor calidad de vida y condiciones igualitarias para todos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los obreros que trabajaban en fábricas pudieron acumular grandes riquezas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Revolución Industrial agravó la escasa producción de comida que el campo generaba.  ",
                "correcta": "0"
            },
            

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,
        }
    },
];
