json = [
{  
    //Reactivo 1
      "respuestas":[  
         {  
            "t13respuesta":"20 años para que otra guerra mundial estallara.",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Diez años para sufrir la mayor crisis económica a nivel mundial.",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Dos periodos de paz antes de la guerra fría.",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"50 años para que el avión fuera inventado.",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Políticos, como la conformación de un bloque capitalista y otro comunista.",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Territoriales, como la repartición de territorios y la creación de nuevos estados.",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Sociales, como un cambio en la distribución de la riqueza.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Religiosos, crearon una crisis religiosa.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Entrara en una profunda crisis que duraría poco más de una década.",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Cuestionara el modelo económico capitalista.",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Las personas confiaran en el sistema feudal nuevamente.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"La economía creciera fuertemente después de unas semanas.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":["Selecciona todas las respuestas correctas para cada pregunta.<br>Después del fin de la Primera Guerra Mundial en 1919 pasaron solamente:","La Primera Guerra Mundial dejó tras de sí conflictos y discusiones de tipo:","A consecuencia de la Primera Guerra Mundial, la gran depresión, nacida en Estados Unidos, hizo que el mundo:"],
          "preguntasMultiples":true
      }
   },
     //Reactivo 2
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Gran Guerra<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>crisis<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Europa <\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>América<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Wall Street<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>1929<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>Alemania<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Tratado de Versalles<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>injusto<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>Partido Obrero Socialista Alemán<\/p>",
                "t17correcta": "10"
            },            
            {
                "t13respuesta": "<p>Partido Obrero<\/p>",
                "t17correcta": "11"
            },                      
            {
                "t13respuesta": "<p>Nacionalsocialista Alemán<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>Adolf Hitler<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>Segunda Guerra Mundial<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>Asia<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>África<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>1914<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>Tratado de Núremberg<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>Benito Mussolini<\/p>",
                "t17correcta": "19"
            }            
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Después del impacto mundial de la <\/p>"
            },
            {
                "t11pregunta": "<p> tanto económica como socialmente cada una de las potencias se enfrentó con una <\/p>"
            },
            {
                "t11pregunta": "<p>, consecuencia de la guerra. En <\/p>"
            },
            {
                "t11pregunta": "<p> las naciones aliadas estaban en quiebra, debían reparar los estragos que la guerra había causado a sus países. En <\/p>"
            },
            {
                "t11pregunta": "<p>, al no haber sufrido daños por la guerra, Estados Unidos gozó de una buena economía, hasta la caída de la bolsa de valores en <\/p>"
            },
            {
                "t11pregunta": "<p> en <\/p>"
            },
            {
                "t11pregunta": "<p>, desde ese punto las crisis se agudizaron causando un panorama económico mundial terrible.<br>En el caso de <\/p>"
            },
            {
                "t11pregunta": "<p>, aunado a la crisis, estaba el <\/p>"
            },
            {
                "t11pregunta": "<p> que limitaba a Alemania como pueblo y le había obligado a ceder todas su posesiones fuera de Europa, su sociedad se dividió, y muchos alemanes lo percibían como <\/p>"
            },
            {
                "t11pregunta": "<p>. Esto provocó distintos movimientos políticos y obreros que buscaban mejorar su situación. Al frente de uno de estos movimientos políticos, el <\/p>"
            },
            {
                "t11pregunta": "<p> posteriormente llamado <\/p>"
            },            
            {
                "t11pregunta": "<p> <\/p>"
            },
            {
                "t11pregunta": "<p>, se encontraba un hombre que innovaría la manera de hacer política en Alemania, su nombre era <\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
     //Reactivo 3
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "v",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Segunda Guerra Mundial fue un conflicto bélico que se desarrolló únicamente en Europa.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los bandos que lucharon la Segunda Guerra Mundial eran tres: El Eje, Los Aliados y los Asiáticos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el grupo de los Aliados se encontraban Inglaterra, Francia, Estados Unidos y posteriormente la URSS.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Del lado de las potencias del Eje se encontraban Alemania, Italia y Japón.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El conflicto comenzó a desarrollarse cuando el emperador Hirohito invadió Polonia.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
     //Reactivo 4
    {
        "respuestas": [
            {
                "t13respuesta": "se dividió en dos bloques económicos: capitalistas y socialistas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "los países que tenían una economía de libre mercado como Estados Unidos, Francia e Inglaterra",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "principalmente los rusos, pero respaldados por todos los países que se habían anexado a ellos, voluntariamente o no, formando así la URSS.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Después de la Segunda Guerra Mundial el mundo..."
            },
            {
                "t11pregunta": "Del lado capitalista militaban..."
            },
            {
                "t11pregunta": "Del lado socialista militaban..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>Relaciona las líneas según corresponda.<\/p>",
        }
    },
     //Reactivo 5
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Europa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>escombros<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>padres<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>mundo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>guerra<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>conflicto<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>sin hogar<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Franz Ferdinand<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>cavó trincheras<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>triple entente<\/p>",
                "t17correcta": "10"
            },            
            {
                "t13respuesta": "<p>perdimos<\/p>",
                "t17correcta": "11"
            },                      
            {
                "t13respuesta": "<p>casa<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>Francia, Inglaterra y los americanos<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>Italia, Rusia y los otomanos<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>Asia<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>paz<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>Sarajevo<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>Berlín<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>Kenia, España y Alemania<\/p>",
                "t17correcta": "19"
            }            
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Después de la guerra, en <\/p>"
            },
            {
                "t11pregunta": "<p> estaba todo hecho trizas. Muchas ciudades habían caído completamente, eran nada más <\/p>"
            },
            {
                "t11pregunta": "<p>; muchos niños ya no volverían a ver a sus <\/p>"
            },
            {
                "t11pregunta": "<p> o hermanos, muchas madres ya no verían otra vez a sus hijos o a sus esposos. Todos habíamos perdido ahí. ¿Cómo fue posible que el <\/p>"
            },
            {
                "t11pregunta": "<p> haya luchado una <\/p>"
            },
            {
                "t11pregunta": "<p> de tal magnitud? ¿No se dieron cuenta del daño que ese <\/p>"
            },
            {
                "t11pregunta": "<p> haría? Muchas personas están ahora <\/p>"
            },
            {
                "t11pregunta": "<p>. En el caso de un terremoto, erupción o cualquier otro desastre pudimos haber culpado a Dios, pero ahora no podemos, fuimos nosotros mismos quienes nos hicimos esto. Dios no asesinó a <\/p>"
            },
            {
                "t11pregunta": "<p>, quien lo hizo fue un hombre; Dios no <\/p>"
            },
            {
                "t11pregunta": "<p> y mató a todos esos jóvenes, fueron los humanos. Ahora nos lamentamos, no queda mucho y algunas partes ya ni siquiera existen. La <\/p>"
            },
            {
                "t11pregunta": "<p> ganó la guerra, pero aún no sé realmente qué ganó. Sé que nosotros <\/p>"
            },            
            {
                "t11pregunta": "<p> mucho en <\/p>"
            },
            {
                "t11pregunta": "<p>, incluso creo que llegué a escuchar que Alemania y el resto del mundo se lo repartieron entre <\/p>"
            },
            {
                "t11pregunta": "<p>. Nosotros debemos levantar nuestras casas porque nadie más lo hará. Todo esto después de que nuestros padres y hermanos lucharon en el frente, y muchos ni siquiera regresaron. Ahora quedamos nosotros para reconstruir el mundo, espero que lo hagamos mejor.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
     //Reactivo 6
   {  
      "respuestas":[  
         {  
            "t13respuesta":"La Gran Depresión",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La Revolución Industrial",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La reforma",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La guerra de secesión",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Crisis económica",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Economía estable",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Economía inestable",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Situación de emergencia",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Pusieron altos impuestos a todo producto procedente del extranjero.",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Crearon alianzas diplomáticas para vender sus productos libremente.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Establecieron acuerdos de intercambio internacional.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Crearon un sistema de mercado local con miras a recuperar la economía internacional.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
          {  
            "t13respuesta":"Adolf Hitler, Francisco Franco, Benito Mussolini y Stalin.",
            "t17correcta":"1",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Porfirio Díaz, Juan Perón, Lázaro Cárdenas y Wilson Woodrow.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Francisco Franco, Juan Perón, Benito Mussolini y Wilson Woodrow.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Adolf Hitler, Lázaro Cárdenas, Iósif Stalin y Porfirio Díaz.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona la respuesta correcta.<br>Fue un suceso histórico que inició a causa de la sobreproducción de la industria norteamericana.", "Las sobreproducción en América y su falta de eficacia para venderla creó una:", "La crisis fue causada por varios factores, uno de ellos era la imposibilidad de vender en su país lo producido, por lo que los países afectados por la crisis:","Ante la crisis muchos movimientos obreros quisieron hacer algo al respecto, esto desencadenó dictaduras y gobiernos totalitaristas, de entre los líderes más famosos de gobiernos totalitarios del siglo XX se encuentran:"],
          "preguntasMultiples":true
          
      }
   },
     //Reactivo 7
    {
        "respuestas": [
            {
                "t13respuesta": "1 de septiembre de 1939 con la invasión Alemana  a Polonia.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Pacto de Acero.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "judíos, gitanos, eslavos, homosexuales entre otros. Cuando los capturaba los ponía en campos de concentración.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "cuando Francia ya había sido vencida.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "las potencias del Eje.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La Segunda Guerra Mundial comienza el"
            },
            {
                "t11pregunta": "Alemania e Italia se habían aliado por medio del famoso"
            },
            {
                "t11pregunta": "La ideología alemana nazi perseguía"
            },
            {
                "t11pregunta": "Italia no entró a la guerra sino hasta 1940"
            },
            {
                "t11pregunta": "Japón, Italia y Alemania eran conocidos como"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>Relaciona las líneas según corresponda.<\/p>",
        }
    },
     //Reactivo 8
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "v",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Francia fue el último país Europeo que quedó en pie al luchar con la Alemania Nazi.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los campos de concentración y exterminio alemanes eran centros donde recluyeron y asesinaron únicamente judíos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El día “D” es el día en que las tropas aliadas desembarcaron en las playas de Normandía comenzando así la contraofensiva contra el régimen nazi.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ingleses y los americanos fueron quienes penetraron en Berlín terminando así la guerra.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
     //Reactivo 9
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Estados Unidos y la URSS",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Alemania e Inglaterra",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"España y Francia",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La URSS y Alemania",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Socialista",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Capitalista",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Nacionalista",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Feudal",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Capitalismo",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Socialismo",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Nacionalismo",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Liberalismo",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
          {  
            "t13respuesta":"El muro de Berlín.",
            "t17correcta":"1",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Un nuevo Reichstag.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Campos de concentración como Auschwitz.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Una nueva ciudad.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona la respuesta correcta.<br>La Guerra Fría más que una confrontación armada fue una disputa entre:", "Para ese entonces la URSS representaba en el mundo al bloque:", "Este sistema defendía la libertad económica y de los individuos para encontrar su posición en ella.","Una de las consecuencias de la Segunda Guerra Mundial fue el reparto de Alemania, que terminaría por desencadenar en la construcción de:"],
          "preguntasMultiples":true
          
      }
   },
     //Reactivo 10
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Ayudar a los judíos a establecerse en medio oriente.",
            "t17correcta":"1",
         },
         {  
            "t13respuesta":"Escuchar a la ONU y dividir su colonia palestina en dos.",
            "t17correcta":"1",
         },
         {  
            "t13respuesta":"Dejar que los judíos se dispersaran por europa.",
            "t17correcta":"0",
         },
         {  
            "t13respuesta":"Olvidarlos y dejar que siguieran con su propias decisiones.",
            "t17correcta":"0",
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas para cada pregunta.<br>Después de la persecución judía en Alemania, Inglaterra optó por:",
          
          
      }
   },
     //Reactivo 11
      {
        "respuestas": [
            {
                "t13respuesta": "México",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Argentina",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Brasil",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con el populismo su gobierno se formó como una dictadura represiva. El coronel Juan Domingo Perón fue quien asumió el cargo de presidente con el apoyo del ejército.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Quien impulsara en un primer momento el populismo en este país sería el presidente Lázaro Cárdenas, emprendiendo acciones que buscaban el bienestar de la población.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Getulio Vargas llegaría a la presidencia de su país con el apoyo de los productores de café. Después se proclamaría dictador y se confrontaría con quienes lo apoyaron.",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada país.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
     //Reactivo 12
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Un liderazgo carismático de un personaje salido del pueblo.",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Existencia de una participación emotiva de las masas.",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"En los discursos se usan palabras como “pueblo”, “nación” y “patria”.",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Atención y preocupación por ciertas clases de la población.",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Establecimiento de estratos marcados de la sociedad.",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Se apoya en las clases desprotegidas y se enriquece a costa de la clase rica.",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Explota los sentimientos de necesidad del pueblo.",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Marca las diferencias de la población y se preocupa por un solo sector.",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },          
         {  
            "t13respuesta":"Se recurre totalmente a los discursos masivos.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"La movilización del electorado es directa.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         }        
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":["Selecciona la respuesta correcta para cada pregunta.<br>¿Cuáles son algunas de las principales características del populismo clásico?", "¿Cuáles de las siguientes no son características del populismo?"],
          "preguntasMultiples":true
          
      }
   },
     //Reactivo 13
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "v",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el periodo de 1920 a 1960 las sociedades continuaron con la tendencia de que la mayoría de su población era rica.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gracias a las guerras mundiales hubo países que tuvieron un buen desarrollo económico como México en el Segunda Guerra Mundial.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La pobreza se centra en la carencia de las personas  para obtener los elementos básicos para un desarrollo integral y para la subsistencia, como la alimentación.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los países que se apoyaron en la guerra para el desarrollo de su país lograron sacar de la pobreza a una parte de su población, estas personas se convirtieron en la clase media.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
     //Reactivo 14
    {
        "respuestas": [
            {
                "t13respuesta": "mejorar la calidad de vida de las personas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "que fueran los gobiernos los que se comprometieran en alcanzar una mejor calidad de vida para su población.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "esto permitió que, gracias al desarrollo de la medicina, aumentara la esperanza de vida.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "el aumento de la población.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "entre ellos sobre población, delincuencia y contaminación.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Las luchas sociales buscaban"
            },
            {
                "t11pregunta": "Las luchas sociales exigían de igual manera"
            },
            {
                "t11pregunta": "Gracias al avance de la democracia y los derechos humanos, los gobiernos se preocuparon más por la clase trabajadora,"
            },
            {
                "t11pregunta": "El incremento de la salud y de la esperanza de vida desencadenó"
            },
            {
                "t11pregunta": "El aumento de las poblaciones en las ciudades provocó distintos problemas,"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>Relaciona las líneas según corresponda.<\/p>",
        }
    },
     //Reactivo 15
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Segunda Guerra Mundial<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Guerra Fría<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>bomba atómica<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>la carrera espacial<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>televisor<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>telecomunicaciones <\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>medios de transporte<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Primera Guerra Mundial<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>Guerra franco-prusiana<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>Guerra de Vietnam<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>Obús<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>Carrera armamentista<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>Radio<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>Medios de producción<\/p>",
                "t17correcta": "14"
            },
            
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Después de la <\/p>"
            },
            {
                "t11pregunta": "<p> y la <\/p>"
            },
            {
                "t11pregunta": "<p> los avances científicos y tecnológicos avanzaron de manera acelerada. La <\/p>"
            },
            {
                "t11pregunta": "<p> había sido un parteaguas en los avances científicos; posteriormente <\/p>"
            },
            {
                "t11pregunta": "<p> llevó al hombre al espacio y después a la luna. Inventos como el <\/p>"
            },
            {
                "t11pregunta": "<p> y las <\/p>"
            },
            {
                "t11pregunta": "<p> eran cada vez más comunes alrededor del globo, mientras los automóviles prosperaban en las ciudades junto con otros <\/p>"
            },
            {
                "t11pregunta": "<p>. El mundo que existió antes de la <\/p>"
            },
            {
                "t11pregunta": "<p> es un recuerdo, la sociedad había llegado a un punto único en el que no hay regreso, un mundo moderno y tecnológico. Nuestro mundo.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
     //Reactivo 16
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "v",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El desarrollo de una nación o de un pueblo está íntimamente relacionado a su desarrollo científico y tecnológico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estados Unidos y la URSS llegaron a ser líderes en el mundo de la innovación tecnológica y científica durante la Guerra Fría.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Otro cambio latente en el hombre después de las guerras mundiales se dio también en el pensamiento.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El existencialismo, corriente filosófica, surge como una forma de cuestionar la vida tras las atrocidades cometidas en la Segunda Guerra Mundial.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
     //Reactivo 17
    {  
      "respuestas":[  
         {  
            "t13respuesta":"La alimentación.",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La guerra.",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La religión.",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La sociedad.",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"La agricultura y la ganadería.",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"La agricultura y el comercio.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"El comercio.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"El comercio y la ganadería.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Se utilizó tecnología para producir alimento sintético al que nuestro organismo no está acostumbrado.",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"La comida ya no se producía en los campos, ahora se produce de manera acelerada en granjas.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Las fábricas tomaron el control de la industria alimenticia y se vendía a más altos precios.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Se produjeron alimentos de baja calidad.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
          {  
            "t13respuesta":"Obesidad, diabetes e hipertensión arterial.",
            "t17correcta":"1",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Epidemias, gripes y enfermedades inmunes.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"SIDA, cáncer y leucemia.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"Lupus, meningitis y Münchausen.",
            "t17correcta":"0",
             "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona la respuesta correcta.<br>La <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> a lo largo de la historia ha sido modificada por diversas circunstancias y ha representado un gran reto, por ejemplo, antes de la invención de la agricultura los primeros humanos debían luchar, esforzarse.", "Durante la mayor parte de la historia de la humanidad la alimentación se ha basado en:","La Revolución Industrial representó uno de los más grandes cambios en la dieta y alimentación del hombre, pues:", "La modificación a la dieta de las personas trajo consigo enfermedades y padecimientos como:"],
          "preguntasMultiples":true
          
      }
   },
    //Reactivo 18
    {
        "respuestas": [
            {
                "t13respuesta": "tener luz por las noches, calor en tiempos de frío e incluso usarlo para la creación de herramientas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "ayudó al hombre a modificar su ambiente.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "otra fuente de energía como lo es la energía atómica.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "sus aplicaciones bélicas han demostrados ser demasiado destructiva e incluso innecesaria.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El descubrimiento del fuego permitió al hombre"
            },
            {
                "t11pregunta": "El fuego fue por mucho tiempo una fuente de energía que"
            },
            {
                "t11pregunta": "Deberían pasar miles de años para que el hombre descubriera"
            },
            {
                "t11pregunta": "La energía atómica se cataloga como mala pues"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>Relaciona las líneas según corresponda.<\/p>",
        }
    },
    
];