json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Edad Media<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Europa<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>V<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>XV<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>1000 años<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>imperio romano<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>476<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>1492<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>descubrimiento<\/p>",
                "t17correcta": "9"
            }
            ,
            {
                "t13respuesta": "<p>de América<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>1453<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>imperio bizantino<\/p>",
                "t17correcta": "12"
            }
            //////
            ,
            {
                "t13respuesta": "<p>mundo antiguo<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>mundo moderno<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>reinos<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>facciones políticas<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>romanos<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>alta<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>baja<\/p>",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "<p>alta edad media<\/p>",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "<p>política<\/p>",
                "t17correcta": "21"
            }
            ///////
            ,
            {
                "t13respuesta": "<p>social<\/p>",
                "t17correcta": "22"
            },
            {
                "t13respuesta": "<p>enclaustraban<\/p>",
                "t17correcta": "23"
            },
            {
                "t13respuesta": "<p>cosechaban el campo<\/p>",
                "t17correcta": "24"
            },
            {
                "t13respuesta": "<p>baja edad media<\/p>",
                "t17correcta": "25"
            },
            {
                "t13respuesta": "<p>sosiego social<\/p>",
                "t17correcta": "26"
            },
            {
                "t13respuesta": "<p>oscurantismo<\/p>",
                "t17correcta": "27"
            },
            {
                "t13respuesta": "<p>intelectual<\/p>",
                "t17correcta": "28"
            },
            {
                "t13respuesta": "<p>Edad Antigua<\/p>",
                "t17correcta": "29"
            },
            {
                "t13respuesta": "<p>100 años<\/p>",
                "t17correcta": "30"
            },
            {
                "t13respuesta": "<p>Imperio Griego<\/p>",
                "t17correcta": "31"
            },
            {
                "t13respuesta": "<p>1480<\/p>",
                "t17correcta": "32"
            },
            {
                "t13respuesta": "<p>democracias<\/p>",
                "t17correcta": "33"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>La <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;es un proceso histórico que se vivió únicamente en<\/p>"
            },
            {
                "t11pregunta": "<p>, este periodo está comprendido entre los siglos&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p> y <\/p>"
            },
            {
                "t11pregunta": "<p>. Se trata, de una etapa histórica con una duración de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Convencionalmente se suele indicar que la edad media comienza con la caída del&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de occidente, en el año&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;d. C. y que finaliza en el año&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, es decir, acaba con el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;o, según algunas fuentes, en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;con la caída del&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. <br /><br />Durante este periodo se vivió, como su nombre lo indica, un periodo de transición del<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;al&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Por toda Europa comenzaron a formarse&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y&nbsp; <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en espacios que antes eran controlados en su mayoría por los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Se suele dividir en 2 periodos: la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;edad media y la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;edad media. <br /><br />La&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se distingue por ser un periodo de inestabilidad<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en el cual las personas se<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en monasterios,&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;con el temor de ser atacados por los bárbaros o bien se unían a los ejércitos que luchaban contra toda facción que no perteneciera a ellos. Por su parte la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;es un periodo de<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y político en donde se empiezan a consolidar las primeras instituciones que más adelante permitirían a la humanidad avanzar a la modernidad. <br /><br />Se suele propagar la idea de que la edad media representa una época de<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;un lapso en la historia en donde abundan el atraso<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y cultural, sin embargo, la realidad es distinta.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "China",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "India",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Japón",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Surge la dinastía Gupta ubicada entre los siglos IV al VII",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se consolida el gobierno imperial. El budismo se convierte en la religión principal en el siglo V.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Surge el uso del dinero y el comercio fuerte en el siglo X.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se someten a las invasiones mongolas del siglo XIII",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Comienzan los gobiernos Samuráis en el siglo XII.",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Relaciona cada hecho con el país donde aconteció.<\/p>",
            "descripcion": "Aspectos a valorar",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
      {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El imperio romano llegó a su fin justo después de la derrota del emperador Rómulo Augusto ante los bárbaros.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los pueblos bárbaros eran nómadas de origen asiático, todo el tiempo huían de las múltiples invaciones presentadas en Asia Central.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Después de la caída del imperio romano de occidente, el imperio romano de oriente dio paso a la consolidación del imperio Bizantino.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los pueblos bárbaros eran compuesto únicamente por dos grandes grupos: los germanos y los turcos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El imperio romano llegó a  su fin por las constantes invasiones por parte de los pueblos bárbaros.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La división religiosa fue uno de los principales culpables de la caída del imperio romano.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "anchoColumnaPreguntas": 65,
            "evaluable":false,
            "evidencio": false
        }
    },
   {
      "respuestas":[
         {
            "t13respuesta":     "Por sus diversas alianzas con el imperio romano.",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Por su vasto armamento.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Por su gran poderío militar.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Por su increíble astucia política.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Por la necesidad de Roma de un mejor ejército.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "Italia, Francia, España, Alemania, Inglaterra y el norte de África.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Italia, Francia, España, Alemania, Inglaterra y el norte de Asia.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Italia, Portugal, España, Alemania, Inglaterra y el norte de África.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Portugal, Alemania, Inglaterra, Holanda, Luxemburgo y el este de Asia.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Portugal, Francia, España, Alemania, Polonia y el norte de África.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "Francia, Suiza, Austria, Bélgica, Holanda y Luxemburgo.",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Alemania, Holanda, Hungría, la República Checa, Luxemburgo y Croacia.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Francia, Italia, Austria, Bélgica, España y Luxemburgo.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Alemania, Italia, Hungría, la República Checa, Eslovaquia y Croacia.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Francia, Suiza, Polonia, Bélgica, Holanda e Inglaterra.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "",
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Por qué los bárbaros comenzaron a tener influencia en el imperio romano?",
                        "Son los nombres de los países en donde el imperio germano asentó su imperio, después de la caída de Roma.",
                        "Son los principales países que abarcaban el reinado de Carlomagno."],
       "preguntasMultiples": true
      }
    },
    {
      "respuestas":[
         {
            "t13respuesta":     "Que era él quien proponía, imponía y llevaba a cabo todas y cada una de las ideas que tuviera, sin ayuda de la tecnología.",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Que era el único capaz de proponer una visión del mundo, a partir de la cual interpretaba todo lo que en él había.",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Que existía un sistema de medición como el métrico, pero basado en los hombres.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Que todo lo que se hacía se media con base en la estatura de los hombres.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         
         /////////////////////////////////////////
         {
            "t13respuesta":     "La cama.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Vivir en un castillo.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La vivienda.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "El fogón.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Tener un telescopio.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "Falta de cloacas y drenaje.",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Alumbrado público.",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Abastecimiento de agua.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Iglesias y edificios público.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Gobiernos democráticos.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11instruccion": "En este periodo [...], <b>el hombre era la medida de todas las cosas</b>; no había un referente tecnológico que lo apoyara y la mano artesanal era la vía expedida para darle forma y cuerpo a la materia para fines de confort y utilidad de los seres humanos. <br />Se producía la mitad de lo que hoy día se produce y no rendía lo suficiente; el campesino, que por las condiciones de vida en feudos de terratenientes y hombres de armas, era el siervo, tenía que dejar la mitad de la cosecha para su amo y con el resto alimentar a su familia. <br /><b>La cama</b>, ese espacio en donde hoy dormimos, descansamos y procreamos, era un lujo; se dormía sobre paja o en el suelo; [...] lo que comúnmente se usaba como plato eran rebanadas de pan seco. <br /><b>Los ricos</b>, que desde el inicio de la civilización han existido, y que ciertamente son quienes han dado forma a la civilidad moderna [...], vivían en castillos de piedra y su riqueza se medía por el espesor de los muros y la solidez de las fortificaciones exteriores; los campesinos se hacían unas chozas de adobe que a menudo se incendiaban y había que reconstruirlas. <br />Pero algo que impacta de aquel periodo [...], es que al no haber alcantarillado ni sistema alguno de conducción de aguas, las calles de las ciudades, fortaleza o aldeas, parecían cenagales todas las épocas del año, por supuesto el mal olor era parte de la cotidianidad y el cultivo de enfermedades endémicas que azotaron a la población [...]. <br />El agua había que irla a buscar al pozo o a la fuente, la luz era proporcionada por las velas y alguna que otras antorchas resinosas que despedían tanto humo como luz. [...] Pero no todo, en esa cotidianidad, era rupestre, había un gran conocimiento de la naturaleza, de las bondades de las plantas para la salud, de saber orientarse con las estrellas y los movimientos del Sol; se poseía una vista ágil y una mano diestra, se conocía el espacio en razón del mandato de sus constantes cambios y se respetaba la pureza de los bosques porque solo se talaba lo necesario para beneficio humano. <br />En una palabra, el hombre medieval se valía de la pasión para mejorar su vida, no para satisfacer su morbo; la caza, por ejemplo, a diferencia del hombre contemporáneo, no era lujo ni pasatiempo, sino trabajo, que tenía a la vez algo de deporte, de festín y de guerra, pero cuyo botín iba destinado para alimento del cazador y los suyos.",
         "t11pregunta":["Selecciona todas las respuestas correctas para cada pregunta.<br><br>¿Qué quiere decir, en el texto, que el hombre era la medida de todas la cosas?",
                        "¿Cuáles eran algunos de los lujos de esa época?",
                        "¿Cuáles eran algunas de las carencia que se tenían en las ciudades y poblados?"],
       "preguntasMultiples": true
      }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A la cabeza de una organización medieval monárquica se encontraba el Rey, seguido del pueblo y en última instancia los líderes religiosos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los feudos eran administrados directamente por el rey, quien los dejaba bajo encargo de campesinos y siervos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El comercio en la época medieval era una de las principales fuentes de ingreso que se tenía, dado a la grata situación que le permitía prosperar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al ocupar todo su tiempo labrando el campo, las clases bajas eran explotadas en el sistema feudal.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cristianismo tuvo su máximo esplendor en la edad media, ya que estaba presente en casi todos los aspectos de la vida social.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Toda la cultura y el arte medieval están basadas en la religión católica.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "anchoColumnaPreguntas": 65,
            "evaluable":false,
            "evidencio": false
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "griegos, sirios, armenios y búlgaros.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "el imperio Turco Otomano.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "los islamistas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "los cristianos de la Iglesia Ortodoxa.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "el desarrollo intelectual del imperio, protegiendo la cultura y fomentando las ciencias.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La sociedad de la cultura bizantina estaba formada por..."
            },
            {
                "t11pregunta": "El imperio Bizantino llegó a su fin con la caída de Constantinopla a manos de..."
            },
            {
                "t11pregunta": "El imperio Bizantino perdió territorio en  el sur, por la invasión de..."
            },
            {
                "t11pregunta": "La religión que surgió en el imperio Bizantino fue la de..."
            },
            {
                "t11pregunta": "El emperador Constantino Porfirogénito se concentró en..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
     {
      "respuestas":[
         {
            "t13respuesta":     "Porque se dedicó a desarrollar la educación, la cultura y el respeto a las leyes por parte de su pueblo.",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Porque se dedicó a tratar de devolverle la grandeza a Roma, conquistando diversas partes del mundo.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Porque hizo un trato con la religión cristiana para lograr un mayor poder político en su imperio.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Porque recibió el apoyo de la nueva religión dominante, el Islam.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         
         /////////////////////////////////////////
         {
            "t13respuesta":     "Los constantes ataques e invasiones por parte de la cultura islámica y los bárbaros.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La traición por parte de la religión cristiana a la corona bizantina.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La falta de poderío militar y naval en sus tierras.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La desigualdad social que se vivía fuertemente tras la peste negra.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         
         /////////////////////////////////////////
         {
            "t13respuesta":     "El imperio turco-otomano.",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "El imperio de Babilonia.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "El imperio romano de occidente.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "El imperio Islámico.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "El imperio Bizantino alcanzó su mayor esplendor en el siglo VI, durante el reinado de Justiniano. Con él, Bizancio (Constantinopla) se convirtió en el centro económico y cultural más importante de su tiempo. <br />Con deseos de reconstruir el antiguo imperio romano, Justiniano extendió sus dominios en el norte de África, Italia y las Islas del Mediterráneo Occidental. <br />Durante su gobierno, ordenó al jurista Triboniano, adecuar, ordenar y recopilar las leyes elaboradas durante siglos por los romanos, en un código llamado “Cuerpo de Derecho Civil” o “Código Justiniano”.  <br />Después de la muerte de Justiniano comenzó una época de continuos peligros para el Imperio. En el siglo VIl d. C., surge en Arabia una nueva potencia: el Islam. Estos invadieron muchos territorios del imperio: Siria, Egipto y norte de África. Al mismo tiempo es ocupada, en sus fronteras danubianas, por los búlgaros y eslavos. A pesar de encontrarse debilitados, los bizantinos lograron cristianizar, helenizar y formar unidades políticas (Bulgaria y Servia) entre los invasores (pueblos todavía bárbaros). Este fue el primer logro cultural de Bizancio. <br />A partir del siglo XI, el Imperio Bizantino se fue debilitando. Las cruzadas representaron un factor que influyó a la inestabilidad del Imperio (debido a su ubicación geográfica, Bizancio era el paso obligado). Aunque el motivo principal entre cristianos y musulmanes era religioso, muy pronto surgieron los intereses comerciales, por lo que los bizantinos combatieron también por el dominio de las rutas de comercio.  <br />Durante el siglo XIII el imperio formado por los turcos otomanos (provenientes de Turquestán, y asentados en un pequeño territorio de Anatolia), se enfrenta al imperio Bizantino y los turcos logran conquistar Asia Menor. En el siglo XIV, pasan al continente europeo, y se anexan los estados balcánicos y gran parte de Suiza. La ciudad de Constantinopla cae en su poder en 1453.",
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Por qué el imperio Bizantino alcanzó su esplendor con el reinado de Justiniano l?",
                        "¿Qué factores provocaron el declive del imperio, tras la muerte de Justiniano l?",
                        "¿Cuál fue el imperio que conquistó la ciudad de Constantinopla y puso fin al imperio Bizantino?"],
       "preguntasMultiples": true
      }
    },
     {
      "respuestas": [
          {
              "t13respuesta": "HI6E_B04_A09_07.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "HI6E_B04_A09_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "HI6E_B04_A09_03.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "HI6E_B04_A09_04.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "HI6E_B04_A09_05.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "HI6E_B04_A09_06.png",
              "t17correcta": "5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra las etiquetas de los países a la ubicación que le corresponda.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"HI6E_B04_A09_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["0", "59,146", "cuadrado", "148, 48", ".","transparent"]},
          {"Contenedor": ["1", "132,227", "cuadrado", "148, 48", ".","transparent"]},
          {"Contenedor": ["2", "349,107", "cuadrado", "148, 48", ".","transparent"]},
          {"Contenedor": ["3", "49,396", "cuadrado", "148, 48", ".","transparent"]},
          {"Contenedor": ["4", "114,482", "cuadrado", "148, 48", ".","transparent"]},
          {"Contenedor": ["5", "427,482", "cuadrado", "148, 48", ".","transparent"]},
          {"Contenedor": ["5", "427,259", "cuadrado", "148, 48", ".","transparent"]}
      ]
  },

{
      "respuestas":[
         {
            "t13respuesta":     "Musulmanes",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Islam",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Vikingos",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Mongoles",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Cristianos Coptos",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         
         /////////////////////////////////////////
         {
            "t13respuesta":     "Tierra Santa, que era controlada por el islam.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "El mundo conocido.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Todo el mediterráneo y sus costumbres.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Las instituciones religiosas ajenas al cristianismo.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Egipto.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "Ganar el paraíso y otros beneficios espirituales.",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Ganar prestigio y oro.",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Ganar la libertad de los hombres.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Ganar la fé de los hombre y extender la religión.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Ganar la paz de Europa.",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         //////////////////////////////////////////////////
        {
            "t13respuesta":     "Económica, pues era la entrada y punto estratégico de todo el comercio con oriente. ",
            "t17correcta":"1",
          "numeroPregunta":"3"
         },
          {
            "t13respuesta":     "Era el principal camino de la famosa ruta de la seda.",
            "t17correcta":"1",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "Social, era la cuna del arte y el último bastión que conformaba la extensión del antiguo imperio romano de oriente.",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "Cultural, co-habitaban tres religiones distintas en un mismo territorio de manera pacífica.",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "Histórica, existía una gran cantidad de recursos antropo-históricos que debían ser preservados.",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "Política, fungía como capital de la cultura de todo el mundo conocido.",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11instruccion": "",
         "t11pregunta":["Selecciona todas las respuestas correctas para cada pregunta.<br><br>En el año 1095, en que al papa Urbano II lanzó la primera cruzada, Jerusalén y Damasco estaban bajo el dominio de:",
                        "Las cruzadas fueron una serie de acciones bélicas que se llevaron a cabo para poder tomar el control de:",
                        "Ante la oportunidad de… las personas asistían voluntariamente a luchar en las cruzadas.",
                        "Existen especulaciones que indican que las cruzadas fueron una treta política para hacerse de Tierra Santa cuya verdadera intención no era religiosa sino..."],
       "preguntasMultiples": true
      }
    },
    {
      "respuestas":[
         {
            "t13respuesta":     "Mahoma es su mesías y escritor de su libro sagrado.",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Profesan a un solo dios, lleno de amor e inmenso poder.",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Promueven la guerra santa.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Se debe rezar cinco veces al día.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Profesan la paz por sobre todas las cosas.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
          {
            "t13respuesta":     "Era una religión politeísta.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
          {
            "t13respuesta":     "Alá promueve la destrucción de todos los fieles.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
          {
            "t13respuesta":     "Profesan a  Jehová como su único Dios.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11instruccion": "",
         "t11pregunta":["Selecciona todas las respuestas correctas. <br><br>¿Qué características pertenecen a la cultura islámica?"],
       "preguntasMultiples": true
      }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de la muerte de Mahoma, la religión islámica ya se había expandido por todo el imperio Bizantino.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El imperio Bizantino fue derrotado por los musulmanes tras la caída de la ciudad de Constantinopla.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Poco más de 100 años después de la muerte de su mesías, la religión islámica se extendió por todo el medio oriente.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La península ibérica fue la primera región donde comenzó la expansión de la cultura islámica.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Meca es la más importante de todas las ciudades santas para la cultura islámica.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "anchoColumnaPreguntas": 65,
            "evaluable":false,
            "evidencio": false
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Europa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>edad media<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>orientales<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>India<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Gupta<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>brahmanismo<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>budismo<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>islam<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>musulmanes<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>hindúes<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>cero<\/p>",
                "t17correcta": "11"
            },{
                "t13respuesta": "<p>sistema decimal<\/p>",
                "t17correcta": "12"
            },{
                "t13respuesta": "<p>China<\/p>",
                "t17correcta": "13"
            },{
                "t13respuesta": "<p>emperadores<\/p>",
                "t17correcta": "14"
            },{
                "t13respuesta": "<p>VII<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>comercio<\/p>",
                "t17correcta": "16"
            },{
                "t13respuesta": "<p>dinero<\/p>",
                "t17correcta": "17"
            },{
                "t13respuesta": "<p>Japón<\/p>",
                "t17correcta": "18"
            },{
                "t13respuesta": "<p>influenciado<\/p>",
                "t17correcta": "19"
            },{
                "t13respuesta": "<p>idioma<\/p>",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "<p>escritura<\/p>",
                "t17correcta": "21"
            },{
                "t13respuesta": "<p>arquitectura<\/p>",
                "t17correcta": "22"
            },{
                "t13respuesta": "<p>religión<\/p>",
                "t17correcta": "23"
            },{
                "t13respuesta": "<p>Samuráis<\/p>",
                "t17correcta": "24"
            },
            {
                "t13respuesta": "<p>voluntad<\/p>",
                "t17correcta": "25"
            },{
                "t13respuesta": "<p>Literatura<\/p>",
                "t17correcta": "26"
            },{
                "t13respuesta": "<p>arte<\/p>",
                "t17correcta": "27"
            },{
                "t13respuesta": "<p>VI<\/p>",
                "t17correcta": "28"
            },{
                "t13respuesta": "<p>Asia<\/p>",
                "t17correcta": "29"
            },{
                "t13respuesta": "<p>Taiwán<\/p>",
                "t17correcta": "30"
            },{
                "t13respuesta": "<p>Ninjas<\/p>",
                "t17correcta": "31"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Mientras que en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se desarrollaba la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, el resto del mundo seguía con sus quehaceres y ocupaciones. Las civilizaciones&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;lograron un gran desarrollo científico, cultural y tecnológico.<\/p>"
            },
            {
                "t11pregunta": "<p>, por ejemplo, fue una civilización que surgió del antiguo imperio&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, cuya religión tenía fuertes influencias del&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, fueron invadidos por los musulmanes y convertidos por la fuerza al&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. <br />Fueron los<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;quienes introdujeron a Europa los aportes que habían tomado de los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, como el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y el uso del&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<br />Por su parte,&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, vivía entre conflictos que dividían su territorio. Hubo&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;que lograban unificarla, pero a la muerte de estos China se dividía nuevamente. Durante este periodo, China se hizo saltó a la fama por haber tenido una mujer de gobernante en el siglo&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, sin embargo, fue derrocada y reemplazada por su hijo. Los chinos impulsaron el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, utilizando el<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;como herramienta, lo que favoreció su desarrollo. <br /><\/p>"
            },
            {
                "t11pregunta": "<p>, en su lugar, fue un país&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;grandemente por China, se nota en su&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>,&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Adoptó por&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;el budismo, esto les permitió con facilidad tener un régimen gubernamental bajo la figura de un emperador. El legado japonés se reconoce fácilmente en los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;cuya vida estaba guiada por una estricta disciplina y una&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;que sortea a toda adversidad.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente párrafo con las palabras que corresponda.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
      {
        "respuestas": [
            {
                "t13respuesta": "Imperio Gupta durante el siglo IV.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "surge con las clases pobres alrededor del siglo V.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "la brújula, el papel, la imprenta y la pólvora.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "estuvo conformada por la clase militar, integrada por los samuráis.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "por tramos, con la finalidad de resguardar a la población.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La historia de India tiene sus orígenes con la dinastía del..."
            },
            {
                "t11pregunta": "La religión del hinduismo..."
            },
            {
                "t11pregunta": "La civilización china dio aportes al mundo como..."
            },
            {
                "t11pregunta": "En japón, durante el siglo XII, la clase dirigente..."
            },
            {
                "t11pregunta": "La gran muralla china fue construida, por los emperadores..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    {
      "respuestas":[
         {
            "t13respuesta":     "Las pulgas de las ratas negras.",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Los mongoles.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Los mercaderes de Caffa.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Los ciudadanos de Caffa.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Las ratas negras.",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         
         /////////////////////////////////////////
         {
            "t13respuesta":     "60% de la población europea.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "40% de la población mundial.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "35% de la población europea.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Menos de 50% de la población mundial.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Más de 80% de la población europea.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
        
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "La peste negra de mediados del siglo XIV se extendió rápidamente por las regiones de la cuenca mediterránea y el resto de Europa en pocos años. El punto de partida se situó en la ciudad comercial de Caffa (actual Feodosia), en la península de Crimea, a orillas del mar Negro. En 1346, Caffa estaba asediada por el ejército mongol, en cuyas filas se manifestó la enfermedad. Se dijo que fueron los mongoles quienes extendieron el contagio a los sitiados arrojando sus muertos mediante catapultas al interior de los muros, pero es más probable que la bacteria se propagara por medio de ratas infectadas con las pulgas a cuestas. En todo caso, cuando tuvieron conocimiento de la epidemia, los mercaderes genoveses que mantenían allí una colonia comercial huyeron despavoridos, llevando consigo los bacilos hacia el punto de destino, Italia, desde donde se difundió por el resto del continente. <br />La transmisión se produjo por medio de barcos y personas que transportaban los fatídicos agentes, las ratas y las pulgas infectadas, entre las mercancías o en sus propios cuerpos, y de este modo propagaban la peste, sin darse cuenta, allí adonde llegaban. Las grandes ciudades comerciales eran los principales focos de recepción. Desde ellas, la plaga se transmitía a los burgos y las villas cercanas, que, a su vez, irradiaban el mal hacia otros núcleos de población próximos y hacia el campo circundante. Al mismo tiempo, desde las grandes ciudades la epidemia se proyectaba hacia otros centros mercantiles y manufactureros situados a gran distancia en lo que se conoce como «saltos metastásicos», por los que la peste se propagaba a través de las rutas marítimas, fluviales y terrestres del comercio internacional, así como por los caminos de peregrinación. <br />A pesar de que muchos contemporáneos huían al campo cuando se detectaba la peste en las ciudades (lo mejor, se decía, era huir pronto y volver tarde), en cierto modo las ciudades eran más seguras, dado que el contagio era más lento porque las pulgas tenían más víctimas a las que atacar. <br />En cuanto al número de muertes causadas por la peste negra, los estudios recientes arrojan cifras espeluznantes. El índice de mortalidad pudo alcanzar 60 por ciento en Europa, ya sea como consecuencia directa de la infección, por los efectos indirectos de la desorganización social provocada por la enfermedad, desde las muertes por hambre hasta el fallecimiento de niños y ancianos por abandono o falta de cuidados. <br />",
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cuál fue el principal medio de contagio de la peste negra?",
                        "¿A cuánto asciende el porcentaje de muerte en Europa por la peste negra?"],
       "preguntasMultiples": true
      }
    },
    {
      "respuestas":[
         {
            "t13respuesta":     "La victoria de la epidemia sobre la sociedad humana.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La pobreza extrema que se vivía en la época.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La degradación social, producto de las constantes muertes.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La unión de la sociedad para combatir la plaga.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "La estabilidad social tras el fin de la epidemia.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "El contagio por la falta de salubridad.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         
         
        
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11instruccion": "",
         "t11pregunta":["Selecciona todas las respuestas correctas. <br /><img src='HI6E_B04_A16_01.png'><br />","¿Cuáles de las siguientes características de la peste negra se pueden observar en la pintura?" ],
       "preguntasMultiples": true
      }
    }
];