json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Encuentra, estudia y preserva los restos materiales dejados por el ser humano.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Capta la evolución del funcionamiento social en el transcurso del tiempo.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Estudia las leyes y normas que las sociedades crean para regular la convivencia entre sus miembros.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Estudia al ser humano como una criatura cultural.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Estudia el funcionamiento de la sociedad.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Ayuda a comprender los actos de personajes que en su momento se consideraron locos o poseídos.",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "Estudia todas las actividades que los seres humanos llevan a cabo para cubrir sus necesidades materiales.",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Arqueología"
            },
            {
                "t11pregunta": "Historia"
            },
            {
                "t11pregunta": "Derecho"
            },
            {
                "t11pregunta": "Antropología"
            },
            {
                "t11pregunta": "Sociología"
            },
            {
                "t11pregunta": "Psicología"
            },
            {
                "t11pregunta": "Economía"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    //2

    {
        "respuestas": [
            {
                "t13respuesta": "Biología",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Geología",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Química",
                "t17correcta": "2",
            }, {
                "t13respuesta": "Medicina",
                "t17correcta": "3",
            }, {
                "t13respuesta": "Oceanografía",
                "t17correcta": "4",
            }
        ], "preguntas": [

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Características que los seres vivos transmiten a su descendencia a través de los genes.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Permite conocer la antigüedad de un objeto.",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estudia cómo se formó y cómo evoluciona nuestro planeta.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Localiza restos de barcos para revelar la información que contienen.",
                "correcta": "4",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Busca remedios a las enfermedades.",
                "correcta": "3",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nos permite saber que la Tierra se transforma constantemente.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada ciencia.",
            "t11instruccion": "",
            "descripcion": "Aspectos",
            "variante": "editable",
            "anchoColumnaPreguntas": 20,
            "evaluable": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Monedas y medallas",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Oro y plata",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Pueblos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Personas muertas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La paleología",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La medicina",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La geología",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "El derecho",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Selecciona la respuesta correcta.<br><br>¿Qué estudia la numismática?", "Los jeroglíficos egipcios son estudiados por...",],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "arqueólogos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "artefactos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "humano",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "plantas",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "transportables",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "filósofos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "paisajes",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "casas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "reciclables",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Los "
            },
            {
                "t11pregunta": " clasifican los vestigios hallados para poder llevar un registro detallado de cada uno de ellos, para lo cual existen tres grandes grupos, los "
            },
            {
                "t11pregunta": " los ecofactos y las estructuras.<br>Los artefactos son objetos elaborados por el ser "
            },
            {
                "t11pregunta": " los ecofactos son los paisajes humanizados, así como el suelo, "
            },
            {
                "t11pregunta": " y animales y las estructuras son todos los productos de la actividad humana que no son "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //5

    {
        "respuestas": [
            {
                "t13respuesta": "Física",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Cultural",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Geográfica",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Espacial",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Analiza la diversidad religiosa.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Analiza procesos de cambio sociocultural.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Reflexiona sobre la relación entre la cultura y el poder.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Estudia las causas y consecuencias de migraciones internas y externas.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Determina enfermedades crónicas.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Estudia la mente de las personas.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>¿En qué ramas se divide la antropología?", "¿Qué aportaciones da la antropología a la sociedad?",],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "<font size=3>Lingüística histórica</font>",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "<font size=3>Paleontología lingüística</font>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<font size=3>Sociolingüística</font>",
                "t17correcta": "2",
            }, {
                "t13respuesta": "<font size=3>Lingüística descriptiva</font>",
                "t17correcta": "3",
            }, {
                "t13respuesta": "<font size=3>Antropología lingüística</font>",
                "t17correcta": "4",
            }
        ], "preguntas": [

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estudia la diversidad de lenguas habladas por el ser humano.",

                "correcta": "4",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estudia las variaciones del lenguaje a largo plazo.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Compara las características de las lenguas que se hablan en la actualidad.",
                "correcta": "3",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estudia las variaciones actuales del lenguaje en su contexto social.",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Busca reconstruir la lengua a partir del análisis del vocabulario.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": " Observa la siguiente matriz y marca la opción que corresponda a cada rama de la lingüística.",
            "descripcion": "Aspectos",
            "variante": "editable",
            "anchoColumnaPreguntas": 20,
            "evaluable": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Cultura",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Cosmovisión",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Igualdad",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Vestimenta",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Interculturalidad",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Intercomunicación",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Internacionalización",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Intervención",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Promover la horizontalidad y saber que no hay cultura superior e inferior",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Promover los usos adecuados de los derechos de las minorías",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Promover el derecho a vivir en un país democrático y libre",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Promover el reconocimiento de las ciudades patrimonio mundial",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Equidad",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Estabilidad",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Empatía",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Admiración",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "La migración",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "La democratización",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Los refugiados de guerra",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "La economía política",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Selecciona la respuesta correcta de cada pregunta <br><br>Formas y expresiones que dotan de sentido e identidad a un grupo, se manifiesta por rasgos culturales como costumbres, religión, lengua, etc.",
                "Es legitimar, reconocer y aceptar la diversidad", "El reconocer la interculturalidad nos ayuda a ",
                "La interculturalidad se fundamenta en la ", "Es un ejemplo de interculturalidad"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }

    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Nos ayuda a sentirnos pertenecientes a un lugar o grupo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Fomenta el respeto entre todos los ciudadanos del mundo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Ayuda a reconocer y respetar la vida",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Promueve el respeto y la aceptación del otro",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Fomenta mejores resultados económicos y sociales",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Fomenta el respeto al medio ambiente",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Nos determina como seres moldeados de la misma forma",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": " Selecciona todas la respuestas correctas<br><br>Son enunciados que resaltan la importancia de la diversidad",
            "t11instruccion": "",
        }

    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "Constantinopla",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Andinas",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Civilizaciones",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Clásico",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "La caída de Constantinopla.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Capital del Imperio bizantino."
            },
            {
                "t11pregunta": "Civilizaciones que se ubicaban en el sur de América."
            },
            {
                "t11pregunta": "Son los lugares que están organizados por un gobierno y desarrollan conocimientos científicos, sociales y tecnológicos."
            },
            {
                "t11pregunta": "Periódo en el cual las civilizaciones mesoamericanas tuvieron cambios significativos en cultura, arte, organización social, etc."
            },
            {
                "t11pregunta": "El Imperio turco-otomano amplió su territorio después de..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },

        ], "preguntas": [

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunos de los descubrimientos más importantes de la civilización egipcia son la rueda, el calendario y el cero.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las civilizaciones fundadas en África son la egipcia y la india.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mesopotamia es considerada como la cuna de las civilizaciones porque en ella existieron muchos descubrimientos.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El latín fue una de las aportaciones más significativas de la civilización griega.",
                "correcta": "1",
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": " Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    // reactivo 11
    {
      "respuestas": [
          {
              "t13respuesta": "Turcos otomanos",
              "t17correcta": "1",
              "numeroPregunta": "0"
          }, {
              "t13respuesta": "Mesoamérica",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Egipcia",
              "t17correcta": "0",
              "numeroPregunta": "0"
          }, {
              "t13respuesta": "Bizantina",
              "t17correcta": "0",
              "numeroPregunta": "0"
          }, {
              "t13respuesta": "Agricultura",
              "t17correcta": "1",
              "numeroPregunta": "1"
          }, {
              "t13respuesta": "Minería",
              "t17correcta": "0",
              "numeroPregunta": "1"
          }, {
              "t13respuesta": "Turismo",
              "t17correcta": "0",
              "numeroPregunta": "1"
          }, {
              "t13respuesta": "Pesca",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>Civilización que derrotó al imperio de Constantinopla.",
          "Actividad que ayudó al sedentarismo.",

         ],
          "t11instruccion": "",
          "contieneDistractores": true,
          "preguntasMultiples": true,
      }

    },
    //reactivo 12
    {
      "respuestas": [        
        {
           "t13respuesta":  "Mesoamérica",
             "t17correcta": "1"
         },
        {
           "t13respuesta":  "naturales",
             "t17correcta": "2"
         },
        {
           "t13respuesta":  "agricultura",
             "t17correcta": "3"
         },
 {
            "t13respuesta":  "Constantinopla",
              "t17correcta": "0"
          },
         {
            "t13respuesta":  "sociales",
              "t17correcta": "0"
          },
         {
            "t13respuesta":  "minería",
              "t17correcta": "0"
          },
    ],
    "preguntas": [
      {
      "t11pregunta": ""
      },
      {
      "t11pregunta": " es un territorio en el cual se desarrollaron varias civilizaciones importantes, ya que cuenta con una gran variedad de recursos "
      },
      {
      "t11pregunta": " lo que la hace adecuada para el desarrollo de la "
      },
      {
      "t11pregunta": ". Las condiciones geográficas de esta zona favorecieron el avance de las civilizaciones como los olmecas, mayas y teotihuacanos.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "",
       "t11instruccion": "",
       "respuestasLargas": true,
       "pintaUltimaCaja": false,
       "contieneDistractores": true
     }
    },
    //reactivo 13
    {
      "respuestas":[
{
 "t13respuesta":"<p>Civilización persa</p>",
 "t17correcta":"0",
 etiqueta:"1"
 },
{
 "t13respuesta":"<p>Imperio romano</p>",
 "t17correcta":"1",
 etiqueta:"2"
 },
{
 "t13respuesta":"<p>Propagación de la peste en Europa</p>",
 "t17correcta":"2",
 etiqueta:"3"
 },
{
 "t13respuesta":"<p>Caída de Constantinopla</p>",
 "t17correcta":"3",
 etiqueta:"4"
 },
],
 "pregunta":{
 "c03id_tipo_pregunta":"9",
 "t11pregunta":"Ordena cronológicamente los siguientes elementos:",
 "t11instruccion": "",
     }
    },

    //reactivo 14
    {
      "respuestas":[
{
"t13respuesta":"Chavín",
"t17correcta":"1",
},
{
"t13respuesta":"Tiahuanaco",
"t17correcta":"1",
},
{
"t13respuesta":"Nazca",
"t17correcta":"1",
},
{
"t13respuesta":"Moche",
"t17correcta":"1",
},
{
"t13respuesta":"Egipcia",
"t17correcta":"0",
},
{
"t13respuesta":"Teotihuacana",
"t17correcta":"0",
},
{
"t13respuesta":"Maya",
"t17correcta":"0",
},
],
"pregunta":{
"c03id_tipo_pregunta":"2",
"t11pregunta": "¿Qué culturas antecedieron a los incas?",
"t11instruccion": "",
}
    },

// reactivo 15
{
  "respuestas": [
      {
          "t13respuesta": "Falso",
          "t17correcta": "0",
      },
      {
          "t13respuesta": "Verdadero",
          "t17correcta": "1",
      },

  ], "preguntas": [

      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "De las civilizaciones india y china se tiene muy poca información debido a que son muy antiguas.",
          "correcta": "1",
      },
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "La civilización más antigua es la griega.",
          "correcta": "0",
      },
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "La civilización griega es de la que más información se tiene.",
          "correcta": "0",
      },
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "La civilización china inventó la imprenta, la pólvora y la brújula.",
          "correcta": "1",
      },

  ],
  "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": " Elige Falso o Verdadero según corresponda.",
      "t11instruccion": "",
      "descripcion": "Enunciados",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
  }
},
//reactivo 16
{
  "respuestas": [
      {
          "t13respuesta": "alrededor del año 776 a. C.",
          "t17correcta": "1",
      },
      {
          "t13respuesta": "algunos ciudadanos se reunían en plazas",
          "t17correcta": "2",
      },
      {
          "t13respuesta": "públicas y debatían sobre las acciones necesarias para desarrollar su organización.",
          "t17correcta": "3",
      },
      {
          "t13respuesta": "logró unificar a todas las ciudades griegas.",
          "t17correcta": "4",
      },
      {
          "t13respuesta": "eran ciudades con gobierno propio, en donde los habitantes decidían y opinaban sobre asuntos de la ciudad.",
          "t17correcta": "5",
      },
      {
          "t13respuesta": "el consejo de ancianos, la asamblea y el consejo de ciudadanos.",
          "t17correcta": "6",
      },
  ],
  "preguntas": [
      {
          "t11pregunta": "Las primeras olimpiadas se llevaron a cabo"
      },
      {
          "t11pregunta": "El origen de la democracia se dio en el año 500 a."
      },
      {
          "t11pregunta": "C. cuando"
      },
      {
          "t11pregunta": "La amenaza de la invasión del imperio persa"
      },
      {
          "t11pregunta": "Las polis griegas"
      },
      {
          "t11pregunta": "En el gobierno de Esparta existían"
      },
  ],
  "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda",
      "t11instruccion": "",
      "altoImagen": "100px",
      "anchoImagen": "200px"
  }
},
];
