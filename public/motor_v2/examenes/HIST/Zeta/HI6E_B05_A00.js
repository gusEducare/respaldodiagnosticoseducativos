
json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Renacimiento<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>europea<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>inicio<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>descubrimiento de América<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>1492<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>bárbaros<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>población<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>peste negra<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>tercera parte<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>naval<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>vía marítima<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>Miguel Ángel<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>Rafael<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>Leonardo da Vinci<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>italiana<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>griego<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>latinos<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>humano<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>XV<\/p>",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "<p>XVI<\/p>",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "<p>Edad Media<\/p>",
                "t17correcta": "21"
            },
            {
                "t13respuesta": "<p>africana<\/p>",
                "t17correcta": "22"
            },
            {
                "t13respuesta": "<p>Descubrimiento de Australia<\/p>",
                "t17correcta": "23"
            },
            {
                "t13respuesta": "<p>1567<\/p>",
                "t17correcta": "24"
            },
            {
                "t13respuesta": "<p>Leonardo Buenarrosa<\/p>",
                "t17correcta": "25"
            },
            {
                "t13respuesta": "<p>Social<\/p>",
                "t17correcta": "26"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;es una época&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en la cual resurge de entre las cenizas del mundo una nueva cultura. Se suele ubicar el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;del renacimiento junto con el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Las personas ahora ya no viven con el temor de caer muertos a manos de los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, las ciudades han comenzado a aumentar su&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, hay avances políticos y los descubrimientos se encuentran a la vuelta de cada esquina. Entre otras cosas es necesario recordar que Europa había sido asolada por la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;perdiendo al menos una&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de su población, por ello es importante recalcar la importancia en el aumento de la población. Por otro lado, el desarrollo&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;había avanzado en demasía, debido a la necesidad latente de comerciar por&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Además en el renacimiento surgen grandes artistas como&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;Buonarroti,<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;Sanzio o&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en la península&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, ellos recuperaron el arte&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;dándole un nuevo lugar en la cultura y retomaron las ideas de pensadores griegos y&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en busca de la noción de qué significa ser&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. El renacimiento se vivió entre los siglos&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras de los recuadros que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//2
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Consolidación de las Ciudades Estado o Repúblicas Italianas.<\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>Fin de la guerra de los Cien años entre Inglaterra y Francia.<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Se unen los reinos de Castilla y Aragón con el matrimonio de Isabel y Fernando.<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>Primeros viajes de exploración y Descubrimiento de América por Cristóbal Colón; Inicio del Renacimiento.<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 4"
            },
            {
                "t13respuesta": "<p>Inicio de la reforma religiosa y primer viaje de Hernán Cortés a tierras mesoamericanas.<\/p>",
                "t17correcta": "4",
                etiqueta:"Paso 5"
            },
            {
                "t13respuesta": "<p>Conquista de México-Tenochtitlán.<\/p>",
                "t17correcta": "5",
                etiqueta:"Paso 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Ordena los siguientes elementos del 1 al 6.<\/p>",
        }
    },//3
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La peste negra en Europa causó la muerte de millones de personas, esto propició que se establecieran cercos sanitarios.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fue hasta después de cien años de que la peste asoló Europa que la población comenzó a recuperarse.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La peste negra se expandió rápidamente a pesar de las altas condiciones de salubridad de la población europea.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los enfermos de peste negra eran condenados a la horca.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debido a la mala alimentación de la población y la pobreza extrema, la peste fue difícilmente erradicada, de manera que se extendió hasta el siglo XV.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando  la población comenzó a recuperarse, los campos estaban inactivos, por eso la economía colapsó.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11instruccion":"",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//4
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELas antiguas civilizaciones griega y romana.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa antigua civilización egipcia y babilonia.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELas lejanas civilizaciones del lejano oriente.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELas civilizaciones nativas del continente americano.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ELos árabes\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELos franceses\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELos chinos\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELos hindúes\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ECiudades Estado\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECiudades País\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPaíses Democráticos\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPaíses Monárquicos\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EEl descubrimiento de América\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa producción industrial\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa explosión demográfica\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa creación de la banca\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta según corresponda.<br><br>Durante el siglo XIV en Italia, en la ciudades importantes, surgió un interés por parte de las clases ricas por la cultura y el arte de...",
         "¿Quiénes fueron los primeros en recuperar los conocimientos de los griegos y romanos?",
         "¿Cuál fue el nombre que recibieron las ciudades que fueron  gobernadas por los propios comerciantes, y, aunque se regían de forma independiente, seguían bajo el dominio del reinado?",
         "El hecho de que los comerciantes venecianos y de Génova dominaban las rutas de comercio, además de que los árabes establecieron altos impuestos a las mercancías de Oriente, obligó a preparar el terreno necesario para..."]
      }
   },//5
    {
        "respuestas": [
            {
                "t13respuesta": "...surgiendo así el sistema llamado monarquía.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...se trataba más de una idea que iba realizándose y corrigiéndose sobre la marcha.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...formaron un vínculo de identidad en las personas que habitaban en el mismo territorio.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "A finales del siglo XVI, se había consolidado la idea de sistema que concentra todo el poder en una persona..."
            },
            {
                "t11pregunta": "La monarquía no se trataba de un método de gobierno planeado..."
            },
            {
                "t11pregunta": "La geografía, las leyes y el idioma fueron importantes herramientas de desarrollo para el concepto de país pues..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona con líneas las columnas según corresponda.",
        }
    },//6
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Castilla",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Aragón",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Portugal",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Firenze",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Atenas",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":     "Fernando de Aragón",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Isabel de Castilla",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "María Antonieta de Borgoña",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Enzio di Firenze",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "José Bonaparte",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":     "Se firmó un tratado de paz con la ayuda del Papa.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Luchó con las armas desde tiempos de Enrique de borgoña.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Se firmó una alianza comercial.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Hubo una guerra que duró cien años.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Pagó a la corona por su territorio.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
         {  
            "t13respuesta":     "Danés",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Normando",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Francés",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Griego",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":     "Alemán",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },//
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "preguntasMultiples":true,
         "dosColumnas": false,
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>En el siglo XV, España tenía el control de cinco reinos, ¿cuáles eran algunos de sus nombres?",
         "Para expulsar a los musulmanes de territorio hispano se concertó una alianza matrimonial entre las dos casas más poderosas de la nobleza española, ¿cuáles fueron sus nombres?",
         "Portugal era un reino perteneciente a España, cuando quiso buscar su independencia Alfonso VII de León y Castilla lo impidió, ¿cómo logró su independencia?",
         "La monarquía inglesa está principalmente formada por personas de origen…"]
      }
   },//7
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Constantinopla fue la capital del imperio Bizantino y fungió además como la continuidad del Imperio Romano.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En sus últimos años Constantinopla vivió una época de prosperidad y paz, sin tener problema con otros pueblos u hordas nómadas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A pesar de que los constantinopolitanos habían logrado detener el avance de los turcos otomanos hacia Europa, su ciudad cayó y fue tomada por ellos en 1453",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los turcos otomanos eran un pueblo sedentario establecido a las orillas del mar mediterráneo, sus principales actividades eran la agricultura y la guerra.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando la ciudad de Constantinopla fue tomada por los turcos otomanos, el control comercial de las mercancías provenientes del lejano oriente pasó a sus manos, por lo que decidieron aumentar los impuestos a toda mercancía que cruzara por su territorio, esto propició una casi extinción del comercio en esa zona.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Como consecuencia de la invasión turca, los europeos obtuvieron un aumento excesivo en los impuestos de su mercancía.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11instruccion":"",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//8
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Humanismo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Renacimiento<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>pensamiento<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>humano<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>escolástica<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>médicos<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>filosofía natural<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>poesía<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>filosofía<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>vivía en el mundo<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>interpretaba<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>divina<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>Iglesia<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>ser humano<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>Erasmo de Rotterdam<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>Elogio a la locura<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>torpeza<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>una corriente<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>quién es el hombre<\/p>",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "<p>pensar<\/p>",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "<p>Existencialismo<\/p>",
                "t17correcta": "21"
            },
            {
                "t13respuesta": "<p>Antigüedad<\/p>",
                "t17correcta": "22"
            },
            {
                "t13respuesta": "<p>Macedonio<\/p>",
                "t17correcta": "23"
            },
            {
                "t13respuesta": "<p>Griegos antiguos<\/p>",
                "t17correcta": "24"
            },
            {
                "t13respuesta": "<p>duda<\/p>",
                "t17correcta": "25"
            },
            {
                "t13respuesta": "<p>1790<\/p>",
                "t17correcta": "26"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;surge en italia junto con el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, se trata de una corriente de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;que intenta recuperar el estudio de lo&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Anteriormente la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;había servido como método de enseñanza y estudio, el problema fue que se trataba de una corriente demasiado utilitarista, de manera que servía únicamente para preparar&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, abogados o teólogos. El humanismo, a diferencia de la escolástica, no se funda solo en la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, la lógica y la teología, sino en las letras humanas: la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;moral o en toda ciencia humana.<br>En esencia, lo que se buscaba era poner al hombre como centro de la realidad del mundo, es decir, el hombre era quien&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y, por tanto, era él quien lo&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, ya no se quería confiar en la potestad&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;impuesta por la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Se trataba de redefinir qué es&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, por ejemplo,<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;fue un humanista que escribió un libro que lleva por título“<\/p>"
            },
            {
                "t11pregunta": "<p>”, en el cual trata la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;humana y cómo es algo propio del hombre. “Es propio de la naturaleza humana, que no haya ingenio alguno sin grandes defectos…” escribe en su libro Erasmo, pues él considera a todo ser humano torpe, no importa su edad, género, posición social, religión o raza, si se es humano por fuerza se es torpe. Así, podemos decir que el humanismo es&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;que trata sobre el hombre, más precisamente sobre&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<br>Esta representa la libertad de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, crear y difundir.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras de los recuadros para completar el texto.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//9
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El arte renacentista estuvo fuertemente influenciado por los etruscos y los selyúcidas, despreciando el arte griego y romano.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Influenciado por el humanismo, el arte renacentista intentaba plasmar a las personas, animales u objetos de la forma más real posible.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre los artistas más destacados se encuentran Miguel Ángel Buonarroti, Rafael,  Leonardo da Vinci, Alberto Durero, Peter Breughel y Sofonisba Anguissola.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante el Humanismo y el Renacimiento el científico era quien intentaba encontrar una explicación a lo que compone el mundo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nicolás Copérnico fue un médico famoso del Renacimiento que estuvo al servicio del Papa",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La investigación no era la mejor forma de aportar nuevos descubrimientos para los humanistas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11instruccion":"",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//10
    {
        "respuestas": [
            {
                "t13respuesta": "Teoría geocéntrica",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Teoría heliocéntrica",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Tierra era el centro del Universo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cuerpos celestes giran alrededor del Sol.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cosmología de la época estaba basada en los textos sagrados.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Tierra es redonda, no plana.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se comprobó esta teoría con los viajes de Colón.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11instruccion":"",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada teoría de concepción del mundo.",
            "descripcion": "Concepto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },//11
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAmerico Vespucio\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECristóbal Colón\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEnzio di Firenze\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ERafael Sanzio\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ECristóbal Colón\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EFernando de Magallanes\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EHernán Cortés\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAmérico Vespucio\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":"\u003Cp\u003E3 de agosto de 1492, desde el puerto de Palos.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003E24 de octubre de 1495, desde el puerto de Sevilla.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003E11 de marzo de 1493, desde el puerto de Barcelona.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003E22 de febrero de 1496, desde el puerto de Palos.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EEl intercambio de especies diversas de flora y fauna, la mezcla cultural y social de tradiciones y costumbres, y el esparcimiento de enfermedades.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003EUna relación diplomática fructífera de distintas sociedades e intercambios comerciales.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEl nacimiento de religiones y nuevos feligreses para la religión católica.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003EComercio desmedido que provocaría una crisis.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },//
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cuál era el nombre del marino y cosmógrafo italiano quien aseguró que en el siglo XV se había descubierto un nuevo continente?",
         "Fue un personaje cuyo descubrimiento impactó grandemente al mundo. En un principio buscaba una ruta marítima comercial hacia Oriente.",
         "¿De qué puerto y en qué fecha partió Colón de España para ir a tratar de cumplir su misión?",
         "El impacto del choque cultural de estos dos mundos se vio reflejado en…"]
      }
   },//12
   {
        "respuestas": [
            {
                "t13respuesta": "<p>América<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>mundo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>hombres<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>la faz de la tierra<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>modernos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>nativos<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>tradiciones<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>sociales<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>anglosajones<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>conquista<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>cultural<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>romanos<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>México<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>diversidad humana<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>Europa<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>planeta<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>Gente<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>Antigua<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>Civilización<\/p>",
                "t17correcta": "19"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>De algún modo el descubrimiento de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;significó un cambio radical en la visión que del&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se tenía, esto debido a que nunca antes nadie había considerado la posibilidad de existencia de otros&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;sobre&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. El hecho de repensar el mundo, obligó a los hombres&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;a replantearse la idea que tenían de ellos mismos como hombres, ¿acaso los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de mesoamérica eran menos hombres que los europeos por sus costumbres,&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y creencias?<br>Además de ello la cruza de aspectos&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, culturales, de tradiciones y de idiomas produjo una extraña mezcla que nos regaló lo que hoy es el continente americano como lo conocemos. Es importante decir que, a diferencia de los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, los hispanos y demás pueblos latinos, llegaron a efectuar una&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;no sólo territorial, sino de igual manera&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;, como hacía siglos lo habían hecho los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;. Esto resultó que en los territorios del norte, no hubiera más que colonias y asentamientos de europeos en un nuevo continente, mientras que en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;, centroamérica y sudamérica hubo una fusión de culturas que expandió la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras de los recuadros que completen el texto.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//13
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Que se comienzan a criticar los dogmas establecidos por la iglesia cristiano-católica.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Que los dogmas de la Iglesia dejan de ser aceptados universalmente.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Ninguno, se mantiene el mismo dogma de fé, de creer en la Iglesia sobre todas las cosas.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Se convierte más en una estrategia política que en una institución espiritual.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":     "Porque encabezó el movimiento del protestantismo.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Porque gracias a los protestantes se quitó a la Iglesia el poder absoluto sobre Europa.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Porque solventó los valores actuales de la Iglesia.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Porque es reconocido como el teólogo más influyente en la historia de la religión cristiana.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>¿Qué cambio se percibe en la concepción de la religión con la frase de Lutero?",
         "¿Por qué Martín Lutero es tan relevante en la historia de la religión cristiano-católica?"]
      }
   },//14
    {
        "respuestas": [
            {
                "t13respuesta": "...es como funcionaba la imprenta tipográfica inventada por Johannes Gutenberg.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...la biblia, que además fue la primera edición de toda Europa.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...el conocimiento quedaba siempre en los altos gremios de personas cultas y religiosos, pues las clases bajas eran analfabetas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": " ...la necesidad de que la población aprendiera a leer y escribir.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...los chinos ya había inventado una imprenta, aunque con un mecanismo completamente distinto debido a la naturaleza de su escritura.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<font size='4'>Mediante la utilización de placas metálicas a las que se les aplicaba tinta, se les colocaba algún tipo de material o papel y posteriormente se realizaba presión..."
            },
            {
                "t11pregunta": "Al inventarse la imprenta, la primera edición impresa en ella constó de 120 ejemplares y se trataba de..."
            },
            {
                "t11pregunta": "A pesar de que la imprenta tuvo una rápida difusión en toda Europa..."
            },
            {
                "t11pregunta": "Con la creación de la imprenta surgió una nueva necesidad para la sociedad europea..."
            },
            {
                "t11pregunta": "Suele decirse que se le atribuye erróneamente el invento de la imprenta a Gutenberg, debido a que..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona con líneas  las columnas según corresponda.",
        }
    },//15
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELa tierra era redonda.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEl mundo era un lugar megadiverso.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa ciencia y no la teología tenía las respuestas.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa humanidad no tenía conocimiento de la verdad.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EMestizaje, entre distintas razas.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EArte retórico barroco.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EConocimiento absoluto de la temperatura.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENuevo orden político conocido como dictadura.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EEl comercio\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa filosofía\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa guerra\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa ciencia\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
        
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>Con el descubrimiento de América se consolidó la idea de que...",
         "Con la conquista de territorios, su colonización y el proceso de adaptación de la población a sus nuevos regímenes y administraciones se produjo el...",
         "Desde el origen de la civilización había sido el motor del desarrollo y el causante del despertar y caída de las sociedades."]
      }
   }
];