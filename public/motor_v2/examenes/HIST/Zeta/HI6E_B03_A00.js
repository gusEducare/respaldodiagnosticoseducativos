json=[
    {
        "respuestas": [
            {
                "t13respuesta": "1500 - 250 a.C.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "1200 - 400 a.C.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "450 - 650 d.C.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "1200-1522 d.C.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1325 - 1519 d.C.",
                "t17correcta": "4"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Esplendor de la cultura Olmeca",
                "valores": ['x','x','x','x','x'],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Surgimiento de la cultura Chavín",
                "valores": ['x','x','x','x','x'],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Surgimiento del Imperio Inca",
                "valores": ['x','x','x','x','x'],
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Surgimiento del Imperio Mexica",
                "valores": ['x','x','x','x','x'],
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Esplendor de Teotihuacan",
                "valores": ['x','x','x','x','x'],
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><strong>Observa la siguiente matriz y marca la opción que corresponda a la ubicación temporal de las culturas prehispánicas.</strong></p>",
            "descripcion": "Título preguntas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los principales imperios de la cultura mesoamericana fueron los incas y los mexicas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se les conoce como civilizaciones andinas por estar ubicadas en la cordillera de los Andes de Sudamérica.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los principales imperios de la cultura mesoamericana fueron los Mayas y los mexicas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mesoamérica abarca desde Canadá hasta la zona de centroamérica.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los principales imperios de mesoamérica estaban ubicados en el centro de lo que actualmente es México.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><strong>Elige falso (F) o verdadero (V) según corresponda.</strong></p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "... se desarrollaron 3 de las más grandes civilizaciones: la mexica, teotihuacana y tolteca.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "… se desarrollaron culturas como la Tarasca y la Mezcala.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "… los pueblos que habitaban en ella dependían de los ríos cercanos y su canalización.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...en esta zona se desarrollaron principalmente los Olmecas y Totonacas.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "… fue la cuna de civilizaciones como los Mixtecos y los Zapotecos.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...en ella surgió, obviamente, la civilización Maya, una de las más complejas.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "...los grupos de humanos se vuelven sedentarios y se descubre la agricultura.",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "...surgen las grandes civilizaciones. Se desarrollan las ciudades-Estado y áreas del conocimiento como la escritura, las matemáticas, la astronomía, el calendario.",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "...se desarrollan las guerras y el arte bélico. Algunos pueblos comienzan el desarrollo metalúrgico.",
                "t17correcta": "9"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "En el centro de México, cuyo espacio geográfico está conformado por valles de clima templado, semiárido y árido..."
            },
            {
                "t11pregunta": "En el Occidente de México, que es una zona rica en metales y piedras preciosas..."
            },
            {
                "t11pregunta": "La zona cultural perteneciente al norte, es de clima principalmente árido y semidesértico..."
            },
            {
                "t11pregunta": "La zona del Golfo se caracteriza por ser un clima caluroso y valles templados..."
            },
            {
                "t11pregunta": "Los valles de Oaxaca, cuyo clima es diverso (desde árido hasta cuencas tropicales)..."
            },
            {
                "t11pregunta": "La zona Maya es la más amplia de mesoamérica..."
            },
            {
                "t11pregunta": "Durante el periodo Preclásico..."
            },
            {
                "t11pregunta": "Durante el periodo Clásico..."
            },
            {
                "t11pregunta": "Durante el periodo Posclásico..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p><strong>Relaciona las columnas según corresponda.</strong></p>"
        }
    },


    {  
      "respuestas":[
         {  
            "t13respuesta": "Llegaron a su fin por una serie de enfrentamientos contra los pueblos que estaban sometidos a sus dominios.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Tenían por deidades principales el Sol, la Luna y el Mar.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Su gobierno estaba formado en una manera teocrática.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Estaban conformados en una ciudad-Estado sin comercio.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Se distinguió porque no existían clases sociales en su cultura.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Habitó en el sur de México y se extendía hasta el norte de Colombia.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Su principal aliado era el pueblo Moche.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Es la civilización más longeva en el territorio de los Andes.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Existió sólo por un par de años y después desapareció.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Se caracterizaba por sus expresiones artísticas que sólo se podían apreciar desde las alturas.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Habitó en el antiguo Perú.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Desarrollaron una religión compleja en la que se incluían deidades tales como Quetzalcoatl, Tlaloc y Huitzilopochtli",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
         ,
         {  
            "t13respuesta": "Se caracterizaron por su cultura bélica.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tenía una manera de sepultar a sus gobernantes muy similar a la cultura egipcia.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Su gobierno estaba formado de una manera teocrática.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Se distinguió porque no existían  clases sociales en su cultura.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["<strong>Selecciona todas las respuestas correctas para cada pregunta.</strong><br /><br />Son características pertenecientes a la cultura Chavín: <br />",
         "Son características pertenecientes a la cultura Tiahuanaco:<br />",  "Son características de la cultura Nazca: <br />", "Son características que pertenecen a la cultura Huari: <br />"],
         "preguntasMultiples": true
      }
   },


   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los incas fueron la última civilización de los andes. Se consolidaron como un imperio cuyo poder se extendía desde el actual Perú hasta territorios lejanos como Argentina.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La lengua que hablaban en la sociedad Inca era el náhuatl clásico, que hoy en día se sigue hablando en algunos lugares.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la civilización Inca, la tierra pertenecía al Estado, él cedía parte de ésta para que la trabajasen sus súbditos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La aristocracia del imperio Inca se encargaba de elegir al próximo gobernante.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La organización cultural de la civilización Inca estaba basada en la religión.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las áreas menos destacadas de la ciencia y tecnología en el Imperio Inca eran su sistema de agricultura, la astronomía, la medicina y las matemáticas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><strong>Elige falso (F) o verdadero (V) según corresponda.</strong></p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },

    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Su lengua era el náhuatl.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "La agricultura era la base de su economía.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Tuvieron contacto con pueblos asiáticos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Cuando los europeos llegaron la civilización estaba en su esplendor.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"<p style='margin-bottom: 5px;'><strong>Selecciona todas las respuestas correctas.</strong><br>¿Qué características no compartían los Mexicas y los Incas?<div  style='text-align:center;'></div></p>"
      }
  },

  {
        "respuestas": [
            {
                "t13respuesta": "<p>tlatoanis<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>mercado<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>trueque<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>comerciantes<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Tlatelolco<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>limpio<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>productos<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>animales<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>comida<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>piedras preciosas<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>españoles<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>rutas<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>costas<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>pescado<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>hojas de palma<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>reyes<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>Monte Albán<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>Armas<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>sucio<\/p>",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "<p>tabaco<\/p>",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "<p>palacio<\/p>",
                "t17correcta": "21"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Se cuenta que por allá en los años que reinaban los </p> "
            },
            {
                "t11pregunta": "<p>, existió un <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp; de convivencia y <\/p>"
            },
            {
                "t11pregunta": "<p>, un lugar a donde acudían todos los <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp; de mesoamérica cuando querían intercambiar sus productos: estaba en <\/p>"
            },
            {
                "t11pregunta": "<p>. Se trataba de un lugar <\/p>"
            },
            {
                "t11pregunta": "<p>, ordenado y clasificado de acuerdo al tipo de <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp; que se vendían: en una parte estaban los <\/p>"
            },
            {
                "t11pregunta": "<p>, en otra la <\/p>"
            },
            {
                "t11pregunta": "<p>, en otra metales y <\/p>"
            },
            {
                "t11pregunta": "<p>, por lo que los <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp; quedaron maravillados. Se dice que los comerciantes se encargaron de abarcar <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp; muy largas, por ejemplo, de las <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp; llegaba a este mercado <\/p>"
            },
            {
                "t11pregunta": "<p>, que era ofrecido sobre <\/p>"
            },
            {
                "t11pregunta": "<p>, pues así se mantenía fresco.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<strong>Arrastra las palabras que completen el párrafo.</strong>",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
];