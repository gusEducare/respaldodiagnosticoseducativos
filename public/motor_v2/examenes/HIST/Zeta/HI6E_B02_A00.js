json=[
  //1
  {
        "respuestas": [
            {
                "t13respuesta": "800 - 338 a. C. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "3500 - 600 a. C. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "1800 - 1300 a. C.   ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1350 - 1521 d. C.  ",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "509 a. C - 476 d. C.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "1200 - 1532 d. C.   ",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "2500 -1700 a. C. ",
                "t17correcta": "7"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Grecia"
            },
            {
                "t11pregunta": "Mesopotamia"
            },
            {
                "t11pregunta": "Babilonia"
            },
            {
                "t11pregunta": "Aztecas"
            },
            {
                "t11pregunta": "Roma"
            },
            {
                "t11pregunta": "Inca"
            },
            {
                "t11pregunta": "Valle del Indio"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    //2
    {
      "respuestas":[
         {
            "t13respuesta":"<p>Entre el río Tigris y Éufrates, actualmente en los territorios de Irak e Israel.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Entre el río Tigris y Éufrates, actualmente en los territorios de Jordania y Siria.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Entre el río Nilo y Éufrates, actualmente en los territorios de Irak y Siria.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Entre el río Tigris y Éufrates, actualmente en los territorios de Irak y Siria.<\/p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Entre el río Tigris y Nilo, actualmente en los territorios de Irak y Siria.<\/p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>¿Cuál es la ubicación geográfica de Mesopotamia?"
      }
   },
   //3
   {
      "respuestas":[
         {
            "t13respuesta":"<p>Sistemas de riego y calendario.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Mejores utensilios de caza, sistemas de riego y una religión.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Escritura, matemáticas, mejores utensilios de caza, el desarrollo de la agricultura.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Sistemas de riego, un calendario, comercio con otros pueblos, escritura y las matemáticas.<\/p>",
            "t17correcta":"1"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Cuáles fueron los principales avances que lograron las civilizaciones antiguas, a raíz de su cercanía con grandes ríos?"
      }
   },
   //4
   {
         "respuestas": [
             {
                 "t13respuesta": "Egipto",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "India",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "Mesopotamia",
                 "t17correcta": "3"
             },
             {
                 "t13respuesta": "China",
                 "t17correcta": "4"
             }
         ],
         "preguntas": [
             {
                 "t11pregunta": "Todo el poder del gobierno recaía en el faraón. Creían que su gobernante era una especie de Dios."
             },
             {
                 "t11pregunta": "Primera civilización en tener la concepción del 0 en su sistema numérico.Creían en la reencarnación como medio para purificar el alma."
             },
             {
                 "t11pregunta": "Se dice que fue la civilización que inventó la escritura y la rueda. "
             },
             {
                 "t11pregunta": "Inventaron el ábaco como una herramienta de cálculo. Profesaban la religión budista."
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "12",
             "t11pregunta":"Relaciona las columnas según corresponda."
         }
     },
     //5
    {
      "respuestas":[
         {
            "t13respuesta":"<p>Persas<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Griegos<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Romanos<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Fenicios<\/p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Babilonios<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Indios<\/p>",
            "t17correcta":"0"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Qué civilización surgió a partir del comercio entre Egipto y Mesopotamia por el mar Mediterráneo?"
      }
   },
   //6
   {
      "respuestas": [
          {
              "t13respuesta": "HI6E_B02_A06_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "HI6E_B02_A06_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "HI6E_B02_A06_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "HI6E_B02_A06_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"HI6E_B02_A06_01.png",
          "respuestaImagen":true,
          "tamanyoReal":true,
          "bloques":false,
          "borde":false

      },
      "contenedores": [
          {"Contenedor": ["", "410,481", "cuadrado", "150, 40", ".","transparent"]},
          {"Contenedor": ["", "456,297", "cuadrado", "150, 40", ".","transparent"]},
          {"Contenedor": ["", "180,137", "cuadrado", "150, 40", ".","transparent"]},
          {"Contenedor": ["", "260,393", "cuadrado", "150, 40", ".","transparent"]},
      ]
  },
  //7
  {
      "respuestas":[
         {
            "t13respuesta":"<p>La innata habilidad de conquista por parte de Filipo ll.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>El inicio de la guerra del Peloponeso.<\/p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>La convicción de su hijo Alejandro Magno.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>La constante hostilidad entre las diferentes polis, que no se sentían identificadas con el resto.<\/p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>El conflicto con Esparta y Grecia.<\/p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las respuestas correctas<br><br>¿Cuáles fueron las principales razones de la conquista de la magna Grecia, por parte de Filipo ll rey de Macedonia?"
      }
   },
   //8
   {
      "respuestas":[
         {
            "t13respuesta":"<p>Por sus increíbles avances en arte, arquitectura, filosofía y literatura.<\/p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Por la convicción de Alejandro Magno de conectar todo el mundo, a partir de la difusión de sus diferentes costumbres.<\/p>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"<p>Por el vasto territorio que logró conquistar Alejandro Magno.<\/p>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"<p>Por ser una cultura de conquistadores que se apropiaban de otras culturas.<\/p>",
            "t17correcta":"0"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Analiza detenidamente las imágenes y selecciona las respuestas correctas.<br><br>Las siguientes imágenes son unos de los principales exponentes culturales de la época helenística. En primer lugar, está la escultura del <i>“Laocoonte y sus hijos”</i>, que data del siglo ll a.C.; el segundo es el mítico faro de Alejandría ubicado en Egipto, que data del siglo lll a.C. (considerado una de las siete maravillas del mundo antiguo) y, por último, está la famosa biblioteca de Alejandría, la cual contenía todo el conocimiento del gran imperio que logró forjar Alejandro Magno y se estima que albergaba más de un millón de volúmenes.<br><br><center><img src='HI6E_B02_A08_01.png'><img src='HI6E_B02_A08_02.png'><img src='HI6E_B02_A08_03.png'></center><br><br>¿Por qué el periodo de la cultura helénica se caracteriza por su desarrollo y difusión cultural?"
      }
   },
   //9
   {
       "respuestas": [
           {
               "t13respuesta": "<p>imperio romano<\/p>",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "<p>emperador<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p>monarcas y nobles<\/p>",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "<p>senadores<\/p>",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "<p>asuntos públicos<\/p>",
               "t17correcta": "5"
           },
           {
               "t13respuesta": "<p>orden ecuestre<\/p>",
               "t17correcta": "6"
           },
           {
               "t13respuesta": "<p>administrativas<\/p>",
               "t17correcta": "7"
           },
           {
               "t13respuesta": "<p>magistrados<\/p>",
               "t17correcta": "8"
           },
           {
               "t13respuesta": "<p>administradores<\/p>",
               "t17correcta": "9"
           },
           {
               "t13respuesta": "<p>plebe<\/p>",
               "t17correcta": "10"
           },
           {
               "t13respuesta": "<p>imperio<\/p>",
               "t17correcta": "11"
           },
           {
               "t13respuesta": "<p>gubernamentales<\/p>",
               "t17correcta": "12"
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "Durante la época del "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " existía una organización política bastante desigual dentro de la sociedad romana. En primer lugar y a la cabeza de la sociedad estaba el "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " quien se encargaba de gobernar y gozaba de una posición privilegiada y tratos especiales que eran propios de "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ". En segundo lugar se encontraban los "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", que si bien invertían gran parte de su tiempo en la resolución de los "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", lo hacían en aras de su posición social. En un tercer nivel estaban los hombres pertenecientes a la "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", quienes gozaban de una posición social privilegiada y cuyas funciones eran "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " y militares. En un cuarto eslabón estaban los "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", ciudadanos acomodados que únicamente desempeñaban funciones públicas como "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ". Finalmente, y como base de la pirámide social del imperio, se encontraba la "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", los gentiles; es decir, gente común que sólo habitaba en el "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " y no tenía ninguna injerencia en sus asuntos "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " o de administración."
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "t11pregunta": "Observa la siguiente imagen y arrastra las palabras que completen el párrafo.<br><br>Arrastra los números que hacen falta en la sucesión.",
          "contieneDistractores":true,
           "anchoRespuestas": 70,
           "soloTexto": true,
           "respuestasLargas": true,
           "pintaUltimaCaja": false,
           "ocultaPuntoFinal": true,
           "t11instruccion": "<center><img src='HI6E_B02_A10.png' style='width: 96%;'/></center>"
       }
   },
   //10
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cristianismo nace con aparición de Jesús de Nazaret en la provincia de Palestina.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los musulmanes fueron los que acusaron a Jesús de agitación política y exigieron su crucifixión.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cristianismo pasó del monoteísmo al politeísmo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El catolicismo es la religión con más adeptos en el mundo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los creyentes de la religión católica sobrepasan el 17% de la población mundial.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pablo de Tarso era perseguidor de creyentes de la religión cristiana antes de convertirse en el principal promulgador de ésta por todo el oriente del mediterráneo.",
                "correcta"  : "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero (V) o falso (F) según corresponda.<\/p>",
            "descripcion":"Cristianismo y características",
            "evaluable":false,
            "evidencio": false
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": " El faro de Alejandría",
                "t17correcta": "1"
            },
            {
                "t13respuesta": " La pirámide de Guiza",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Jardines colgantes de Babilonia ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "  La ciudad de Teotihuacan",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "El coloso de Rodas   ",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='HI6E_B02_A13_01.png'/>"
            },
            {
                "t11pregunta": "<img src='HI6E_B02_A13_02.png'/>"
            },
            {
                "t11pregunta": "<img src='HI6E_B02_A13_03.png'/>"
            },
            {
                "t11pregunta": "<img src='HI6E_B02_A13_04.png'/>"
            },
            {
                "t11pregunta": "<img src='HI6E_B02_A13_05.png'/>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12"
        }
    },
    //12
  {
        "respuestas": [
            {
                "t13respuesta": "<p>Nilo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Dios<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>agricultura<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>limo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>ciclos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>dioses<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>faraones<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>oscuridad<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>ciudades<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>tumbas<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>panteones<\/p>",
                "t17correcta": "11"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>La civilización egipcia veneraba el río <\/p>"
            },
            {
                "t11pregunta": "<p> como si fuera un <\/p>"
            },
            {
                "t11pregunta": "<p>, pues los beneficios que traía a su civilización eran muchos. Entre ellos, la posibilidad de desarrollar la <\/p>"
            },
            {
                "t11pregunta": "<p>, pues el río contenía un elemento poco común llamado <\/p>"
            },
            {
                "t11pregunta": "<p>, que permitía que las tierras fueran más fértiles. Sin embargo, así como este río traía vida y prosperidad, también les otorgaba desesperación y muerte, debido a que existía inestabilidad en sus <\/p>"
            },
            {
                "t11pregunta": "<p>, fenómeno que los egipcios le atribuían a los <\/p>"
            },
            {
                "t11pregunta": "<p>. Por eso los <\/p>"
            },
            {
                "t11pregunta": "<p> realizaban ofrendas al río para atraer buenas cosechas. Para los egipcios este río representaba la relación entre la vida y la muerte, entre la luz y la <\/p>"
            },
            {
                "t11pregunta": "<p>; esa fue la causa de que los egipcios construyeran las <\/p>"
            },
            {
                "t11pregunta": "<p> del lado del río que salía el sol y las <\/p>"
            },
            {
                "t11pregunta": "<p> de los faraones y los <\/p>"
            },
            {
                "t11pregunta": "<p>, del lado en que se ponía.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
];
