json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EActividades.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EEventos públicos.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EHechos trascendentales\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ENoticias documentadas.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EComprender aspectos de la vida humana.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ELlevar un registro de la evolución del hombre.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EEvitar cometer los mismos errores.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ERegistrar eventos para estudio de futuras generaciones.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003Ea partir de las cuales podemos conocer sobre el pasado.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003Ea partir de las cuales se puede predecir el futuro.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003Edocumentadas y validadas por una autoridad.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003Eaceptadas por todos como verdaderas.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003Efuentes históricas.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003Ehechos válidos.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003Efuentes secundarias.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003Eencuentra nueva evidencia.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003Epropone nuevas hipótesis.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003Ela misma no conviene al momento.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la opción que mejor conteste o complete lo siguiente.<br/><br/>Son hechos históricos que se llevan a cabo de manera individual o en colectivo. ",
                "El estudio de la historia permite:",
                "Las fuentes históricas son aquellas...",
                "Materiales arqueológicos, notas del periódico y fotografías son ejemplos de:",
                "Un buen historiador cambia su perspectiva cuando:"],
            "preguntasMultiples": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando existe más de una versión sobre los mismos hechos, todas son válidas, pero no necesariamente atinadas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se puede entender el nombre de un país si se conocen los hechos que dieron lugar a este nombre.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las hipótesis son afirmaciones confirmadas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Solo las personas que juegan un papel en la toma de decisiones en un país, pueden ser consideradas sujetos históricos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lo que ha sucedido en nuestra vida nos hace ser lo que somos.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Determina si las siguientes oraciones son falsas o verdaderas.<\/p>",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "El tiempo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "acontecimientos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "periodos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "políticos",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "momentos",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "líneas",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "fechas",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "La vida",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": " es un concepto que se mide en horas, días, semanas, años o siglos. Es útil ordenar la sucesión de "
            },
            {
                "t11pregunta": " para saber si sucedieron antes o después de otros. Los historiadores dividen la historia en "
            },
            {
                "t11pregunta": " con cierta duración, que resultan útiles para explicar fragmentos en una línea de tiempo. Los periodos se nombran, en ocasiones basándose en procesos "
            },
            {
                "t11pregunta": ", culturales y sociales.<br>También es útil fijar "
            },
            {
                "t11pregunta": " específicos (como el nacimiento de Cristo) para marcar los hechos que sucedieron antes o después de este punto.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona la opción para llenar el espacio que complete el resumen. Hay tres opciones que no necesitas utilizar.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "espacio",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿cuándo?",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "¿dónde?",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "el planeta",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "tiempo",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "¿quién?",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "lugar",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "el tiempo",
                "t17correcta": "8"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Los hechos ocurren en un tiempo y "
            },
            {
                "t11pregunta": " determinados. El tiempo es el momento en que ocurre el hecho, y responde a la pregunta "
            },
            {
                "t11pregunta": ", mientras que el espacio da respuesta a la pregunta "
            },
            {
                "t11pregunta": "<br>Los nombres de los espacios están relacionados con su ubicación en "
            },
            {
                "t11pregunta": ". No obstante, el nombre con que se identifica el espacio puede cambiar con el "
            },
            {
                "t11pregunta": ". De igual forma, las características de un mismo espacio y sus habitantes, plantas, animales o humanos, pueden cambiar.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<p>Selecciona la opción para llenar el espacio que complete el resumen. Hay tres opciones que no necesitas utilizar.</p>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E20 de noviembre de 1810.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EA principios de siglo.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ELa Edad de Piedra.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EComprender aspectos de la vida humana.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EEspacio.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ETiempo.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ETiempo y espacio.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003Elos sujetos y el tiempo.  \u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003Elos lugares y las causas.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003Eel tiempo y el cómo suceden.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003Eno son relevantes para el autor del libro.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003Eno sucedieron como se cuentan.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003Eson poco importantes para la historia.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003Eno corresponden al contexto.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003Eel segundo es ficticio.  \u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003Eel primero no es real.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003Eel cuento no es historia.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "\u003Cp\u003Ela historia no es siempre verdadera.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la opción que mejor conteste o complete lo siguiente.<br/><br/>Es ejemplo de una manera precisa de medir el tiempo:",
                "La Muralla China fue construida en un área que comprende desde la frontera con Corea hasta el desierto de Gobi. La anterior es una referencia de:",
                "Los cuatro elementos que se necesitan para la comprensión de un hecho histórico son el espacio, el contexto…",
                "Algunos eventos no se mencionan en los libros de historia porque aparentemente…",
                "La diferencia entre un texto de historia y un libro de cuentos es que:"],
            "preguntasMultiples": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre los historiadores pueden existir distintas versiones de los hechos debido a que cada uno tiene su propia perspectiva.  ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de la escritura, no había forma de transmitir los hechos históricos a las siguientes generaciones. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para que el conocimiento de un hecho histórico  sea recordado, hace falta transmitirlo de generación en generación. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gracias a las historias cantadas es que algunos hechos históricos fueron conocidos por personas que vivían en otros lugares.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Solo se puede dejar constancia de un hecho histórico en el tiempo si este se escribe y se comparte. ",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Determina si las siguientes oraciones son falsas o verdaderas.<\/p>",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cambios que percibimos en nuestra persona y en el entorno físico nos habla del paso del tiempo.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los objetos no cambian por las necesidades humanas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las continuidades (permanencias) nos permiten identificar las cosas poco relevantes para una sociedad.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La sincronía nos permite establecer relaciones de causas y consecuencias. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "¿Es más fácil entender el desarrollo económico de un país si estudiamos su futuro?",
                "correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Determina si las siguientes oraciones son falsas o verdaderas.<\/p>",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "El país está compuesto de intercambios y relaciones entre distintos pueblos con costumbres diversas.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "El país está habitado por una mayoría mestiza que discrimina a los indígenas.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La nación mexicana pertenece a los países con mayor megadiversidad biológica del planeta",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La mayoría de los mexicanos profesa la religión católica.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "indígenas, negros y españoles",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "criollos y españoles",
                "t17correcta": "0",
                "numeroPregunta": "1"

            },
            {
                "t13respuesta": "españoles, indígenas y portugueses",
                "t17correcta": "0",
                "numeroPregunta": "1"

            },
            {
                "t13respuesta": "molcajete",
                "t17correcta": "1",
                "numeroPregunta": "2"

            },
            {
                "t13respuesta": "petate",
                "t17correcta": "0",
                "numeroPregunta": "2"

            },
            {
                "t13respuesta": "karate",
                "t17correcta": "0",
                "numeroPregunta": "2"

            },
            {
                "t13respuesta": "ajolote",
                "t17correcta": "0",
                "numeroPregunta": "2"

            },
            {
                "t13respuesta": "Pastorela",
                "t17correcta": "1",
                "numeroPregunta": "3"

            },
            {
                "t13respuesta": "Día de muertos",
                "t17correcta": "0",
                "numeroPregunta": "3"

            },
            {
                "t13respuesta": "Guelaguetza",
                "t17correcta": "0",
                "numeroPregunta": "3"

            },
            {
                "t13respuesta": "El día de las madres",
                "t17correcta": "0",
                "numeroPregunta": "3"

            },
            { 
                "t13respuesta":"comunicación radiofónica", 
                "t17correcta":"1", 
                "numeroPregunta": "4"

                }, 
               { 
                "t13respuesta":"televisión a color", 
                "t17correcta":"0", 
                "numeroPregunta": "4"

                }, 
               { 
                "t13respuesta":"alarma sísmica", 
                "t17correcta":"0", 
                "numeroPregunta": "4"

                }, 
               { 
                "t13respuesta":"comunicación internacional", 
                "t17correcta":"0", 
                "numeroPregunta": "4"

                }, 
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br><br> La multiculturalidad de México se refiere a que:",
                "En México existió un proceso de migración entre:",
                "Es un utensilio de la cocina mexicana herencia de los indígenas:",
                "Es una tradición heredada desde la colonia, fue introducida por los españoles con la finalidad de la evangelización.", 
                "La <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>  sigue existiendo a pesar del paso del tiempo porque es un medio ágil para reportar incidentes en tiempo real como los sismos.", 
            ],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },

];
