json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Aridoamérica",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Oasisamérica",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Mesoamérica",
                "t17correcta": "3"
            }
        ],
        "preguntas": [{
                "t11pregunta": "San Luis Potosí, Nuevo León, Baja California"
            },
            {
                "t11pregunta": "Nuevo México, Chihuahua, Sonora"
            },
            {
                "t11pregunta": "Oaxaca, Chiapas, Puebla"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "Surgen las primeras capitales",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Se desarrolla la agricultura.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Se comienza a cazar y domesticar animales.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Hay un alto crecimiento de la población.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Lo primeros hombres llegan por el norte.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Surge el sistema numérico.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Surgen construcciones majestuosas.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Comienzan a utilizarse el metal (metalurgia).",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Surgen las culturas maya, teotihuacana y zapoteca.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Surgen la cultura olmeca, quechua y mexica.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Se intensifican las guerras entre ciudades-estado.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Se inventa la rueda.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "El domino de la Triple Alianza.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Surge la civilización mexica.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Surge la civilización chichimeca.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Nace la cultura tarasca.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Se inventan el arco y la flecha.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Se funda la Villa Rica de la Vera Cruz.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona las respuestas correctas en cada pregunta.<br><br>¿Qué hechos importantes ocurrieron durante el periodo preclásico tardío?",
                "¿Que hechos remarcables sucedieron durante todo el periodo clásico?",
                "¿Que hechos notables se dieron durante el periodo posclásico tardío?"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "<p>maya<\/p>",
                "t17correcta": "1,2"
            },
            {
                "t13respuesta": "<p>teotihuacana<\/p>",
                "t17correcta": "1,2"
            },
            {
                "t13respuesta": "<p>2500 a. C.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>200 a. C.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>posclásico<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>clásico<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>preclásico<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>zapotecas<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>olmeca<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>mexica<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>25000 a. C.<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>5000 a. C.<\/p>",
                "t17correcta": "12"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Las culturas que se desarrollaron durante el periodo clásico fueron la olmeca, la "
            },
            {
                "t11pregunta": ", la zapoteca y la "
            },
            {
                "t11pregunta": ". Dicho periodo comprende los años del "
            },
            {
                "t11pregunta": " al "
            },
            {
                "t11pregunta": ". De las culturas mencionadas sólo la maya y la zapoteca lograron avanzar hasta los periodos clásico y "
            },
            {
                "t11pregunta": ". La cultura teotihuacana llegó hasta el "
            },
            {
                "t11pregunta": ", mientras que la  olmeca no pasó del periodo "
            },
            {
                "t11pregunta": "."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra y coloca los conceptos que correspondan.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "1500 a. C.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "18000 a. C.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "1800",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "400 a. C.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "1800 a. C",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "A la guerra y sacrificios.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "A la pintura y las artes.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "A la agricultura intensiva de riego.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "A experimentar con plantas.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "A la astronomía.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "San Luis Potosí",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Querétaro",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Sonora",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Oaxaca",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Chiapas",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Mexica",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Maya",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Tarasca",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Mixteca",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Chichimeca",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Predicaba la inmortalidad del alma.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Era monoteísta.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Tenía templos de asistencia dominical.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Es bastante conocido y estudiado.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "No se ha logrado comprender dado su complejidad.",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿En qué año surge la civilización olmeca?",
                "¿A qué se dedicaban principalmente los zapotecas?",
                "¿En qué estado de la república mexicana se asentaron los mixtecas?",
                "¿A qué cultura pertenece el Popol Vuh?",
                "El culto religioso de la civilización mexica:"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "<p>Existe un dios de la dualidad.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Creían en Tláloc.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Vivíamos en la era del “Quinto Sol”.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>El hombre fue formado de la tierra.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Vivíamos en la era del “Séptimo Sol”.<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>El hombre nació por causa de la madre Coatlicue.<\/p>",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las respuestas correctas.<br><br>Dentro de las creencias religiosas de los toltecas:"
        }
    },
    {
        "respuestas": [{
                "t17correcta": "68"
            },
            {
                "t17correcta": "discriminación"
            },
            {
                "t17correcta": "Oaxaca"
            },
        ],
        "preguntas": [{
                "t11pregunta": "¿Cuántas lenguas indígenas existen en la actualidad?"
            },
            {
                "t11pregunta": "<br>¿Los grupos indígenas en la actualidad sufren de?"
            },
            {
                "t11pregunta": "<br>¿Menciona el estado donde se ubican los zapotecas?"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "evaluable": true,
            "t11pregunta": "Responde correctamente con una palabra o un número según corresponda."
        }
    }

];