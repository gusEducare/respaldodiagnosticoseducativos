json=[

//1
   {  
      "respuestas":[
         {  
            "t13respuesta": "Génova",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Sevilla",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Amsterdam",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "París",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Isabel I de Castilla y Fernando II de Aragón",
            "t17correcta": "1",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Maria Antonieta de Austria y Luis XVI de Francia",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Enrique VII de Inglaterra y Catalina de Aragón",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Federico III de Habsburgo y Leonor de Portugal",
            "t17correcta": "0",
            "numeroPregunta":"1"
         } ,
         {  
            "t13respuesta": "Cuatro viajes",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Dos viajes",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Cinco viajes",
            "t17correcta": "0",
            "numeroPregunta":"2"
         } ,
         {  
            "t13respuesta": "Nueve viajes",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Partió del Puerto de Palos de la Frontera, para llegar a la Isla de Guanahani.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Partió del puerto de Corduba, para llegar a la Isla de Cuba.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         } ,
         {  
            "t13respuesta": "Partió del puerto de Nueva Cartago, para llegar a la Península de Yucatán.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Partió de las Islas Canarias, para llegar a la Isla de Guanahani.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
        
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<strong>Selecciona la respuesta correcta.</strong> <br> ¿Cuál es el nombre de la ciudad en que nació Cristóbal Colón?","¿Fueron los reyes que dieron apoyo a Colón para realizar sus expediciones?","¿Cuántos viajes realizó Colón a América?","Durante su primer viaje de exploración, ¿cuál fue  el puerto de donde partió Colón y cuál fue el lugar a donde llegó?"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },
   
   //2 respues multiple
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Los turcos-otomanos habían aplicado un impuesto elevado a quienes transportaban mercancía de Asia a Europa.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "La caída de Constantinopla a manos de los turcos-otomanos cerró las rutas comerciales.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Los avances tecnológicos marítimos pintaban un panorama ideal para explorar el mundo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "No había esclavismo en Europa y necesitaban esclavos para trabajar la tierra.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Las incursiones bárbaras eran molestas para las coronas europeas.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"<strong>Lee la siguiente pregunta y selecciona las respuestas correctas.</strong><br>¿Gracias a qué hechos históricos fue posible el descubrimiento de América?"
      }
   },
   
   // 3 opcion multiple
    {  
      "respuestas":[
         {  
            "t13respuesta": "11 naves, 518 infantes y 16 jinetes, entre otros.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "20 naves, 1030 infantes y 40 jinetes, entre otros.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "3 naves, 108 infantes y 6 jinetes, entre otros.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "15 naves, 492 infantes y 12 jinetes, entre otros.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "23 naves, 987 infantes y 19 jinetes, entre otros.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "La Villa Rica de la Vera Cruz.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "La nueva Villa del Carmen de Hispania.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "La Villa de la Santísima Inmaculada.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         } ,
         {  
            "t13respuesta": "La Villa de la Nueva Sevilla y Granada.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "La Villa Rica de Santa María de la Victoria.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
        
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<strong>Selecciona la respuesta correcta.</strong><br> ¿Cuántos elementos conformaban la armada de Hernán Cortés para las expediciones realizadas a tierras mesoamericanas?","Fue la primera ciudad fundada por Hernán Cortés en las costas de mesoamérica:"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },
   
   //4 opcion multiple
   
    {  
      "respuestas":[
         {  
            "t13respuesta": "Deshacerse del dominio mexica.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Formar un nuevo imperio.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Recrear rutas comerciales.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Participar en la guerra.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Formar una nueva alianza.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "De grupos indígenas prehispánicos.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "De los españoles.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "De los mayas.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         } ,
         {  
            "t13respuesta": "De los otomíes.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "De la corona española y Hernán Cortés.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },{  
            "t13respuesta": "A que ese ejército no era oriundo de esas tierras.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "A que el ejército era imaginario.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "A que el ejército no era imponente.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta": "A que no era la guerra de ese ejército.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "A que ese ejército no tenía injerencia en la política local.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
        
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<strong>Lee el texto y selecciona la respuesta correcta.</strong><br>¿Cuáles eran los intereses de los grupos indígenas al aliarse con Cortés?","¿Gracias al apoyo de quiénes lograron los españoles conquistar Tenochtitlán?","En el texto, ¿a qué se refiere al autor cuando dice que la última guerra del México prehispánico fue dirigida por un ejército que no pertenecía a esa historia?"],
         "preguntasMultiples": true,
         "t11instruccion": "EPÍLOGO <br><br>Los totonacos vieron en Cortés un aliado aceptable para sacudirse el dominio mexica, y sería un disparate reprocharles esa alianza, pues no tenían modo de saber, por ejemplo, que después vendrían la viruela, la tosferina y el tifo, y que sus ciudades quedarían desiertas y sus milpas y huertas se convertirían con el paso del tiempo en pastizales. Los tlaxcaltecas cesaron su resistencia inicial y optaron por aliarse con los españoles porque les pareció que esa alianza garantizaría, precisamente, la integridad de su territorio; ellos no debían lealtad alguna a los mexicas, más bien todo lo contrario, eran enemigos. Desde el arribo de Cortés a Yucatán hasta la conclusión del sitio de Tlatelolco con la captura de Cuauhtémoc, hubo muchos señoríos que, tras ser derrotados en combate o después de haber celebrado un acuerdo con Cortés, se sumaron a sus filas. La conquista de Tenochtitlán fue una victoria de los tlaxcaltecas, de los texcocanos, de los totonacos y de muchos otros grupos indígenas: fue la última guerra del México prehispánico, dirigida sin embargo por un pequeño ejército que no pertenecía a esa historia.<br>-Pablo Escalante Gonzalbo, “El México Antiguo”, Nueva Historia Mínima de México, Colegio de México, México, 2008, p. 109",
         "columnas":1,
          
        
      }
   },
   
   //5 om
   {  
      "respuestas":[
         {  
            "t13respuesta": "Por la derrota y pérdida de hombres.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Porque se sentía triste y desolado.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Porque tenía miedo y estaba preocupado.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Hernán Cortés no lloró.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Pedro de Alvarado",
            "t17correcta": "1",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Francisco de Gante",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Hernán Cortés",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Bernal del Castillo",
            "t17correcta": "0",
            "numeroPregunta":"1"
         } ,
         
        
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<strong>Lee el texto y selecciona la respuesta correcta.</strong><br>¿Por qué lloró Cortés?","¿Quién fue el personaje que provocaría el episodio de la Noche Triste?"],
         "preguntasMultiples": true,
         "t11instruccion": "Uno de los episodios más trágicos sufrido por los conquistadores españoles fue aquel que sucedió la noche del primero de julio de 1520. Este hecho ocurrió a partir de que Pedro de Alvarado había sido el responsable de masacrar, con anterioridad, a la clase dirigente de Tenochtitlán para evitar un ataque contra el reducido número de españoles que custodiaban a Moctezuma II. Esto desencadenó una lucha que duró cerca de una semana en un estado de sitio para los invasores, así que el 30 de junio a la medianoche, decidieron escapar. Esto produjo una persecución y matanza de los españoles y sus aliados indígenas. Después, cuando todo se hubo apaciguado, Cortés se sentó y lloró. Este episodio de la conquista se conoce como la Noche Triste.",
         "columnas":1,
          
        
      }
   },
   //6 
  {  
      "respuestas":[
         {  
            "t13respuesta": "Católica",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Judía",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Musulmana",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Calvinista",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Protestante",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "África",
            "t17correcta": "1",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "China",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Rusia",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Oceanía",
            "t17correcta": "0",
            "numeroPregunta":"1"
         } , 
         {  
            "t13respuesta": "Europa",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         
        
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<strong>Selecciona la respuesta correcta.</strong><br>Religión introducida a los mexicas durante la conquista:","Al darse cuenta de que los indígenas no podían hacer trabajos pesados, los españoles importaron esclavos de:"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },   
   
   //7 
    {  
      "respuestas":[
         {  
            "t13respuesta": "Luchaban para expulsar a los moros de sus tierras.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Uno de sus objetivos era poblar y evangelizar nuevas tierras.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Normalmente, en las batallas no dejaban a ningún soldado del ejército contrario vivo.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Utilizaban el metal y la pólvora como parte de su instrumental básico.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Sacrificaban en altares a su prisioneros de guerra.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Establecieron campamentos organizados que después se convertirían en ciudades.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Practicaban las guerras floridas para capturar prisioneros que posteriormente sacrificaban.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Las guerras eran un asunto religioso.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Se distinguían entre dos clases de soldados: guerrero águila y guerrero jaguar.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         } , 
         {  
            "t13respuesta": "Formaban la cabeza de la triple alianza.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Utilizaban espadas y armaduras.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         } , 
         {  
            "t13respuesta": "Tenían un gran ejército organizado por Cortés.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
            
        
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["<strong>Selecciona las respuestas correctas.</strong><br>¿Cuáles de las siguientes características describen mejor a un conquistador español?","¿Cuáles de las siguientes características describen a los guerreros mexicas?"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },   
   
   //8 
     {  
      "respuestas":[
         {  
            "t13respuesta": "Europa",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "América",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "África.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         
         {  
            "t13respuesta": "Asia",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "El chile, el frijol, el maíz, el aguacate y el venado.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El venado, las ovejas y las reses.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El maíz, el aguacate, el ganado bovino y los productos derivados de la leche como el queso y la mantequilla.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         } , 
         {  
            "t13respuesta": "El trigo, el maíz, los nopales y las tunas.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         
        
            
        
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<strong>Lee el texto y selecciona la respuesta correcta.</strong><br>Según el texto anterior,  ¿de qué continente proceden los productos de origen animal?","¿Cuáles fueron algunos de los productos alimenticios en los que los mesoamericanos basaban su dieta?"],
         "t11instruccion":"Después de la conquista hubo muchos factores que cambiaron, uno de ellos es  la gastronomía: los indígenas y pueblos de mesoamérica utilizaban el maíz, el chile, el frijol, el chocolate, los tamales, el nopal, las tunas, el aguacate y el venado. Los españoles, por otro lado, introdujeron el trigo, el ganado ovino y bovino, los productos derivados de origen animal como la leche, el queso, la mantequilla, etcétera. Además, con la introducción del trigo se gestó una cultura alimenticia nueva en la que se incluían elementos como el pan y las galletas.",
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },   
   
   //9 relC
   
      {
        "respuestas": [
            {
                "t13respuesta": "<p>Productos originarios de Mesoamérica<\/p>",
                "t17correcta": "1,3"
            },
            {
                "t13respuesta": "<p>Productos originarios de Europa<\/p>",
                "t17correcta": "2,4"
            },
            {
                "t13respuesta": "<p>Productos originarios de Mesoamérica<\/p>",
                "t17correcta": "3,1"
            },
            {
                "t13respuesta": "<p>Productos originarios de Europa<\/p>",
                "t17correcta": "4,2"
            },
            
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><p><br><img src=\"HI4E_B03_A09_01.png\" style='width:200px'><\/p>"
            },
            {
                "t11pregunta": "<br><p><br><img src=\"HI4E_B03_A09_02.png\" style='width:200px'><\/p>"
            },
            {
                "t11pregunta": "<br><p><br><img src=\"HI4E_B03_A09_03.png\" style='width:200px'><\/p>"
            },
            {
                "t11pregunta": "<br><p><br><img src=\"HI4E_B03_A09_04.png\" style='width:200px'><\/p>"
            },
            

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "pintaUltimaCaja": true,
            "textosLargos": "si",
            "anchoRespuesta":true,
            "soloTexto":true,
            "respuestasLargas":true,
            "contieneDistractores": false,
            "ocultaPuntoFinal":true,
            "t11pregunta": "<strong>Arrastrar al contenedor según corresponda.</strong>"
        }
    },
   
   
   
   
   

  
];
