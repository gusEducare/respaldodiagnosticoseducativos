
json=[

    {
        "respuestas": [
            {
                "t13respuesta": "<p>Surge la conspiración conocida como “De los machetes”.<\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>El cura Don Miguel Hidalgo y Costilla pronuncia el “Grito de Dolores”.<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Se gana “La batalla del monte de las cruces”.<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>El cura Don Miguel Hidalgo y Costilla es aprehendido  y ejecutado.<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 4"
            },
            {
                "t13respuesta": "<p>El cura José María Morelos y Pavón toma el liderazgo del movimiento de independencia.<\/p>",
                "t17correcta": "4",
                etiqueta:"Paso 5"
            },
            {
                "t13respuesta": "<p>La “Constitución de Cádiz” es abolida.<\/p>",
                "t17correcta": "5",
                etiqueta:"Paso 6"
            },
            {
                "t13respuesta": "<p>Se elabora el “Plan de Iguala”.<\/p>",
                "t17correcta": "6",
                etiqueta:"Paso 7"
            },
            {
                "t13respuesta": "<p>Se firma el “Tratado de Córdoba”.<\/p>",
                "t17correcta": "7",
                etiqueta:"paso 8"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11instruccion":"",
            "t11pregunta": "Ordena del 1 al 8 los siguientes elementos acerca de la Independencia de México.",
        }
    },//2
   
      {
      "respuestas": [
          {
              "t13respuesta": "HI4E_B05_A02_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B05_A02_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B05_A02_04.png",
              "t17correcta": "2",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B05_A02_05.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B05_A02_06.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B05_A02_01.png",
              "t17correcta": "5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Selecciona la opción de acuerdo al lugar en el mapa  que corresponda.<br><br>¿En dónde ocurrieron los siguientes acontecimientos?<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"HI4E_B05_A02_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "224,40", "cuadrado", "250, 70", ".","transparent"]},
          {"Contenedor": ["", "40,258", "cuadrado", "250, 70", ".","transparent"]},
          {"Contenedor": ["", "138,296", "cuadrado", "250, 70", ".","transparent"]},
          {"Contenedor": ["", "318,8", "cuadrado", "250, 70", ".","transparent"]},
          {"Contenedor": ["", "242,388", "cuadrado", "250, 70", ".","transparent"]},
          {"Contenedor": ["", "400,8", "cuadrado", "250, 70", ".","transparent"]}
      ]
  },//3
   {
        "respuestas": [
            {
                "t13respuesta": "Internas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Externas",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La represión del gobierno por miedo a la Revolución Francesa.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La escasez de alimento que derivó en la muerte de más de cien mil ciudadanos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Revolución Francesa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caída de la monarquía en Francia.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La desigualdad social.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La invasión de Napoleón Bonaparte a España.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La aparición de los criollos",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El auge del pensamiento ilustrado",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Identifica las causas internas y externas de la independencia de México.<\/p>",
            "descripcion": "Causas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },//4
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Porque representa los ideales del pensamiento ilustrado.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Por su intervención política en España.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Porque formó parte del movimiento armado.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Porque es el modelo ideológico de las estrategias militares de los insurgentes.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Por ser el modelo ideológico del virreinato.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>¿Por qué Napoleón es una figura importante en nuestro país?"
      }
   },//5
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EIgualdad, libertad, autonomía y un principio de gobierno basado en los estandartes de la Revolución Francesa.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EUna hegemonía política sobre todos aquellos que no pertenecieran a la herencia mexica.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EIgualdad, autonomía y crear un gobierno monárquico propio.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAutonomía y volver a establecer la religión de nuestros ancestros.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto y selecciona la respuesta correcta.<br><br>Con base en los puntos fundamentales del documento, contesta:<br>¿Cuál es la ideología sociopolítica que refleja Morelos en los<br>“Sentimientos de la Nación”?",
          "t11instruccion":"José María Morelos y Pavón, plasmó su pensamiento sociopolítico<br>en un documento que llamó los “Sentimientos de la nación”; dicho<br>documento fue presentado el 14 de septiembre de 1813 en el<br>Congreso de Chilpancingo.<br><br>Los puntos fundamentales de este documento son los siguientes:<br><br>1. Declarar la independencia y libertad de América de España,<br>de cualquier otra Nación, gobierno o monarquía.<br>2. Reafirmar la religión católica como la única aceptada sin<br>tolerancia de otra.<br>5. Establecer que la soberanía dimanaría del pueblo y del<br><b><em>Supremo Congreso Nacional Americano</em></b> compuesto por<br>representantes de cada provincia en igualdad de números,<br>eliminando la figura del rey de España.<br>6. Una vez reconocida la soberanía de la nación y con una<br>política inspirada en la Revolución Francesa, el gobierno se<br>dividiría en tres poderes: legislativo, ejecutivo y judicial.<br>9. Con la finalidad de proteger a los americanos, los empleos<br>quedarían reservados para estos.<br>11. Se cambiaría la forma de gobierno, se eliminaría el sistema<br>monárquico y se establecería un gobierno liberal.<br>12. Socialmente se pretende una mayor igualdad, reduciendo<br>el tiempo de los jornales y procurando mejores costumbres<br>para las clases marginadas.<br><br>De esta manera Morelos exhortó al Congreso trazar leyes para<br>moderar la opulencia y la pobreza y lograr así una mayor<br>igualdad social<br><br>15. Se proscribe la esclavitud y la distinción de castas para<br>siempre y todos queden iguales.<br>22. Se desaparecen las alcabalas, estancos y el tributo de los<br>indígenas.",
      }
   },//6
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELas diferentes formaciones institucionales de cada uno, religiosa por parte de Hidalgo y militar por parte de Allende.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa falta de estrategias militares buenas y coordinadas.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa desigualdad entre castas, la poca relación del pueblo en la política y la diferencia de puntos en cuanto a estrategias.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         //
         {  
            "t13respuesta":"\u003Cp\u003EVicente Guerrero en la Sierra Madre del sur, Guadalupe Victoria en Puente del Rey y Nicolás Bravo en el sur.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EVicente Guerrero en la Sierra Madre del sur, Francisco Xavier Mina en San Luis Potosí y Miguel Hidalgo en Guanajuato.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EVicente Guerrero en el norte, Guadalupe Victoria en la Sierra Madre sur y Nicolás Bravo en Puente del Rey.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EVicente Guerrero en el sur, Guadalupe Victoria en la Sierra Madre sur y Nicolás Bravo en Dolores Hidalgo.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgustín de Iturbide en el norte, Guadalupe Victoria en el sur y Nicolás Bravo en la Sierra Madre Occidental.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cuál es la razón principal de las diferencias en el pensamiento sociopolítico de Hidalgo y Allende?",
         "Tras la muerte de Morelos y a falta de un líder que guiara la lucha de Independencia, ¿quiénes fueron los líderes del movimiento y en qué zonas ejercían relevancia?"],
      }
   },//7
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Mucha, porque eran los sucesores del movimiento de independencia tras la muerte de Morelos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Bastante, porque eran las últimas figuras que trataban de plasmar los ideales propuestos por Morelos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Irrelevante, la lucha hubiera continuado de la misma manera sin ellos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Regular, tuvieron su peso en la victoria, pero no fueron indispensables.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>¿Qué tan importante fue para el movimiento de independencia que figuras como Guerrero y Guadalupe Victoria hayan rechazado el indulto ofrecido por la corona española?"
      }
   },//8
    {
        "respuestas": [
            {
                "t13respuesta": "Externas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Internas",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La victoria de la monarquía española sobre Napoleón.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La revisión de privilegios del clero.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La búsqueda de la independencia de España por parte de Iturbide.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El regreso del rey Fernando Vll al trono.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La incapacidad de Iturbide para derrotar a Guerrero.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El rechazo al indulto por parte de varios generales del ejército insurgente.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La abolición de la constitución de Cádiz.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El expansionismo de Estados Unidos de América.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Identifica las causas internas y externas de la independencia de México.<\/p>",
            "descripcion": "Causas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },//9
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ENo, porque él buscaba el poder político y no la igualdad entre castas.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENo, porque él buscaba la supremacía de la corona española.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENo, porque trabajaba con la corona española para engañar al ejército insurgente.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ESí, porque él fue quien propició el fin de la independencia.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ESí, porque fue proclamado el primer gobernante en la historia del México libre.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11instruccion":"En el Congreso de México se trató de erigir estatuas a los jefes de la revolución y hacer honores fúnebres. A estos mismos jefes había yo perseguido y volvería a perseguir si regresáramos a aquel tiempo, y así poder decir quién tiene la razón, si el Congreso o yo. Es necesario no refutar que la voz de Insurrección no significa Independencia, libertad justa, ni era el objeto reclamar los derechos de la Nación, sino exterminar a todo europeo, destruir posesiones... Si tales hombres merecen estatuas, ¿qué se reserva de los que no se separan de la senda de la virtud?<br><br><div style='text-align:right'>-&nbsp;&nbsp;&nbsp;Agustín de Iturbide.</div>",
         "t11pregunta":"Lee el texto y selecciona la respuesta correcta.<br><br>¿Agustín de Iturbide fue una figura que luchaba por los ideales de la Independencia de México?"
      }
   },//10
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ESe logró liberar de la monarquía española, pero el pueblo seguía con las mismas desigualdades sociales y económicas.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETotalmente, solo necesitaban un periodo de adaptación con el nuevo gobierno de Iturbide.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ESe logró llegar a una estabilidad social, pero no se logró la total independencia de la corona.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETotalmente, se podía palpar la nueva estabilidad de nuestra nación.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENada, la situación seguía siendo la misma, solo con otros tiranos.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EGertrudis Bocanegra Mendoza, Josefa Ortiz de Domínguez, Altagracia Mercado y Leona Vicario.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EGertrudis Bocanegra Domínguez, Josefa Ortiz de Mendoza y Leona Mercado.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EJosefa Ortiz de Mendoza, Leona Bocanegra Domínguez y Gertrudis Mercado.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EJosefa Bocanegra Mendoza, Leona Ortiz de Domínguez y Frida Kahlo\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EPor el parteaguas que crearon para establecer un papel más activo de la mujer en la sociedad.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque el papel de las mujeres es igual de importante que el de los hombres.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque formaron parte importante de la lucha de independencia.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPor la equidad de género.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11instruccion":"",
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>Acorde a los ideales presentados por Miguel Hidalgo y solventados por Morelos, ¿qué tanto se lograron respetar dichos ideales al término de la independencia?","¿Quiénes fueron las principales figuras femeninas que jugaron un papel fundamental en la independencia?","¿Por qué es importante resaltar el papel de las mujeres en la independencia?"]
      }
   }
];