
json = [
  {
      "respuestas": [
          {
              "t13respuesta": "HI4E_B04_A01_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B04_A01_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B04_A01_03.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "HI4E_B04_A01_04.png",
              "t17correcta": "3",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra las etiquetas de los países a la ubicación que le corresponda.<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"HI4E_B04_A01_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "353,140", "cuadrado", "120, 50", ".","transparent"]},
          {"Contenedor": ["", "383,280", "cuadrado", "120, 50", ".","transparent"]},          
          {"Contenedor": ["", "263,210", "cuadrado", "120, 50", ".","transparent"]},
          {"Contenedor": ["", "315,515", "cuadrado", "120, 50", ".","transparent"]},
          
      ]
  },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"1535 a 1821",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"1492 a 1810",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"1512 a 1811",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"1535 a 1810",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"1546 a 1808",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Carlos V de Alemania",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Carlos III de Austria",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Carlos el Beodo",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Carlos el Loco",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Carlos II el conquistador",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"63",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"60",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"68",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"69",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"65",
            "t17correcta":"0",
             "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona la respuesta correcta.<br>¿En qué año inició el virreinato de la Nueva España y en qué año terminó?","Carlos I rey de España y la Nueva españa fue también conocido como:","¿Cuántos virreyes hubo durante todo el virreinato de la Nueva españa?"],
          "preguntasMultiples":true
      }
   },
    {
      "respuestas": [
          {
              "t13respuesta": "HI4E_B04_A03_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B04_A03_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "HI4E_B04_A03_03.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "HI4E_B04_A03_04.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "HI4E_B04_A03_05.png",
              "t17correcta": "4",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra las etiquetas de las clases sociales a la ubicación que le corresponda.<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"HI4E_B04_A03_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "56,113", "cuadrado", "130, 50", ".","transparent"]},
          {"Contenedor": ["", "149,161", "cuadrado", "130, 50", ".","transparent"]},          
          {"Contenedor": ["", "238,210", "cuadrado", "130, 50", ".","transparent"]},
          {"Contenedor": ["", "327,258", "cuadrado", "130, 50", ".","transparent"]},          
          {"Contenedor": ["", "415,307", "cuadrado", "130, 50", ".","transparent"]},
          
      ]
  },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"El Rey",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"El Virrey",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"La Real Audiencia",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"El Consejero de Indias",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"El Gobernador",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Administrar y gobernar en representación de la corona española.",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Ordenar y dar mayor impulso al comercio de España con la Nueva España.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Reafirmar la supremacía de la justicia del Rey por sobre la de los gobernantes.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Regular lo relativo a la organización y funcionamiento de la ciudad.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Daban su opinión acerca de los asuntos que determinar los gobernantes.",
            "t17correcta":"0",
             "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona la respuesta correcta.<br>¿Quién era el máximo representante gubernamental de la Nueva España?","La función del Virrey de la Nueva España era:"],
          "preguntasMultiples":true
      }
   },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"San Luis Potosí",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Zacualpan",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Guanajuato",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Chihuahua",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Monterrey",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Sinaloa",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Tlaxcala",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Agricultura",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Minería",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Comercio",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Caza",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Maquilación",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Pesca",
            "t17correcta":"0",
             "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":["Selecciona todas las respuestas correctas a cada pregunta.<br>Fueron ciudades fundadas por la explotación de plata.","Fueron las principales actividades económicas de la Nueva España."],
          "preguntasMultiples":true
      }
   },    
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Borrar y reemplazar cualquier vestigio del mundo prehispánico para evangelizar.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Gobernar y tener el control del pueblo.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Crear y construir ciudades y pueblos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Explotar a los indígenas para trabajos manuales.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Explorar los nuevos territorios.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿Cuál fue la función de los misioneros en la Nueva España?"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Indígenas y españoles",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Negros y españoles",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Criollos y españoles",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Mulatos y españoles",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Asiáticos y españoles",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Alemanes y españoles",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas.<br>¿Qué clasificaciones se tiene para las luchas que ocurrieron entre los españoles y diversos grupos en la Nueva España?"
      }    
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Sor Juana Inés de la Cruz",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Juana de Arco",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Malala Yusuf",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Sor Teresita de Jesús",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>Fue una poetisa del virreinato:"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Asiáticos",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Africanos",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Europeos protestantes",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Turcos",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Otomanos",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Trabajos agrícolas",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Trabajos que requerían un gran esfuerzo",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Trabajos de minería",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Trabajo de evangelización",
            "t17correcta":"0",
             "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":["Selecciona todas las respuestas correctas.<br>¿Qué otras culturas influenciaron en la evolución de la sociedad virreinal?","Los esclavos negros eran utilizados como trabajadores para:"],
          "preguntasMultiples":true
      }
   }
];