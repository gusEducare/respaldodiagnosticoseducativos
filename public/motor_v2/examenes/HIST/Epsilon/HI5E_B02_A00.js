json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Era un jefe liberal que había ganado la guerra.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Ante todo, había defendido la soberanía nacional.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Se encontraba en una época en la que ser político era redituable.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "El pueblo le amaba por ser de procedencia indígena.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Había ganado la guerra y eso le valía las lisonjas.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Dos liberales y un conservador: Juárez, Tejada y Miramón.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Dos conservadores y un liberal: Mejía, Miramón y Juárez.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Tres conservadores: Mejía, Miramón y Maximiliano",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Tres liberales: Juárez, Lerdo de Tejada y Porfirio Díaz",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Por que el contrato ya estaba firmado y debía respetarse.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "La comunicación entre el puerto de Veracruz y la Cd. de México era necesaria.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Juárez estaba comprometido con el desarrollo y el progreso del país y necesitaba reactivar la economía.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Los liberales querían conseguir un medio de transporte moderno. Por  ello Juárez toma esta iniciativa.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Lee el siguiente texto y selecciona la respuesta correcta.<br><br>¿Por qué cuando Juárez entró a la Ciudad de México triunfal, el pueblo lo aclamaba?",
                "¿Quiénes contendieron en las elecciones organizadas en agosto por Juárez?",
                "¿Por qué razón Juárez reconoció y consumó el contrato firmado por el imperio para la creación de un ferrocarril que fuera de Veracruz a México?"
            ],
            "preguntasMultiples": true,
            "t11instruccion": "Derrumbado el imperio, el 16 de julio de 1876 Juárez volvió a la ciudad de México. Esta vez el pueblo, que valoraba su lucha por preservar la soberanía nacional, lo recibió con verdadero júbilo. El triunfo de la república anulaba finalmente la opción monarquista, aunque no daba fin a desórdenes y levantamientos,  ahora generados por ambiciones políticas de los propios liberales.<br>Juárez se apresuró a convocar elecciones para agosto. La desaparición del partido conservador de la contienda política enfrentó a tres liberales: Juárez, Sebastián Lerdo de Tejada y Porfirio Díaz, el héroe militar de la guerra. Aunque el triunfo favoreció a Juárez, sus enemigos se multiplicaron, tanto por su reelección como porque promovía la reforma de la constitución, lo que parecía contradecir su empeño en haberla defendido. [...]<br>La república resentía los años de guerras y requería impulsar la economía. El comercio volvía a ser víctima del desorden [...]. Como buen liberal, comprometido con el desarrollo y el progreso, Juárez deseaba favorecer todas las ramas productivas: inversiones, comunicaciones (en especial las líneas telegráficas, caminos y ferrocarriles) y colonización. No sólo aprobó algunos proyectos de inversión norteamericana, sino que reconoció el contrato que el imperio había firmado para construir el ferrocarril de Veracruz a México.<br>[...] Normalizar las relaciones de México fue otra de sus preocupaciones fundamentales, pues la guerra Había provocado la ruptura con Gran Bretaña, Francia y España, pero tropezó con un contexto internacional desfavorable. La  distancia y la falta de de comunicaciones obstaculizaban el contacto con los países iberoamericanos, además de que existían problemas fronterizos con Guatemala; por estas razones, Juárez trató de impedir que algo nublara las relaciones con Estados Unidos.<br><br>Vázquez J. (2008). Nueva Historia Mínima de México. México: El colegio de México"
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "Liberales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Conservadores",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Arroyo del Mortero, 1860",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Loma Alta, 1860",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mazatlán, 1859",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Silao, 1860",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuevitas, 1858",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Salamanca, 1858",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tacubaya, 1859",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Albarrada, 1859",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Calpulapan, 1860",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Durante la Guerra de Reforma en el S.XIX hubo distintas batallas. ¿Cuáles fueron las principales ganadas por los liberales y cuáles por los conservadores? Selecciona la opción correcta.",
            "descripcion": "Batalla",
            "variante": "editable",
            "anchoColumnaPreguntas": 35,
            "evaluable": true,
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "Los conservadores",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Los liberales",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...una monarquía",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...igualdad ante la ley para todos los ciudadanos y una república federal",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...prensa",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Estaban a favor del proteccionismo económico."
            },
            {
                "t11pregunta": "Buscaban una liberalización de la economía."
            },
            {
                "t11pregunta": "La forma de gobierno que buscaban los conservadores era..."
            },
            {
                "t11pregunta": "Entre otras cosas, los liberales perseguían..."
            },
            {
                "t11pregunta": "Los liberales, junto con la libertad de culto y educación, buscaban la libertad de..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "Predicador del Estado",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Benemérito de las Américas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Padre de la Patria",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Dictador Vitalicio",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Para proteger la soberanía nacional.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Para acallar las críticas de su gobierno.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Para procesar una nueva constitución.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Para poder crear su propio periódico.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Eliminar los fueros del ejército y la iglesia, garantizar la libertad de expresión, prohibir los impuestos eclesiásticos.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Eliminar los privilegios de la clase alta, recortal las garantías de algunos ciudadanos, reactivar la economía por medio de los impuesto.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Eliminar las fiestas patronales, prohibir los cultos públicos y bajar los impuestos.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la respuesta correcta.<br><br>¿Cómo se autoproclamó Santa Anna después de tomar el poder y hacerse llamar “Alteza Serenísima”?",
                "Santa Anna limitó la libertad de prensa, ¿por qué motivo?",
                "¿Cuales eran los principales objetivos de las leyes de reforma?"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "1860",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "1880",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "1865",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "1867",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Contra la opresión del gobierno.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Contra la mala administración liberal.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Contra la reelección de Juárez.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Contra el maltrato de los estratos bajos de la sociedad.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "1890",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "1885",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "1872",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "1867",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Ayutla",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Las Casas",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Tuxtepec",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Río Blanco",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la respuesta correcta.<br><br>¿En qué año subió Juárez a la presidencia como presidente constitucional?",
                "¿Contra qué protestaba el plan de la Noria?",
                "¿En qué año subió Lerdo de Tejada a la presidencia?",
                "Al quererse reelegir Lerdo de Tejada, Porfirio Díaz se levantó en armas proclamando la revolución de:"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "Eliminación del fuero militar y religioso. Todo ciudadano es Igual ante la ley.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Estableció la libertad de imprenta, pretendía garantizar la libertad de expresión en medios escritos y públicos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Desamortización de fincas y propiedades de corporaciones civiles y religiosas, para ponerlas a la venta y reactivar la economía.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Prohíbe el cobro de derechos y obvenciones, así como el cobro del diezmo a los pobres.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Ley Juárez"
            },
            {
                "t11pregunta": "Ley Lafragua"
            },
            {
                "t11pregunta": "Ley Lerdo"
            },
            {
                "t11pregunta": "Ley Iglesias"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    // Reactivo 7
    {
        "respuestas": [{
                "t13respuesta": "Sí",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "No",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No se sabe",
                "t17correcta": "2"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las guerras internas fueron un peso importante para definir la cultura y la sociedad mexicanas en las primeras décadas del México independiente.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los liberales no querían recuperar el pasado prehispánico, pues no servía de nada a la cultura.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ignacio Manuel Altamirano, Ignacio Ramírez, Vicente Riva Palacio y Manuel Acuña fueron los autores más representativos de la época.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La clase media surgió como producto de una crisis monetaria de las clases altas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Benito Juárez buscaba la anexión de territorios centroamericanos a la República.",
                "correcta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la respuesta correcta.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable": true,
        }
    },
    //Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "Los conservadores",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La iglesia",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "El estado",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Los liberales",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Teología y dogmas religiosos",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Tradición europea y asiática",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La filosofía positivista y las explicaciones científicas",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "El cientificismo y la filosofía antropológica",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Sociedad Orquestal Nacional",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Sociedad Musical Mexicana",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Sociedad Julián Carrillo",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Sociedad Filarmónica Mexicana",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la respuesta correcta.<br><br>Fue el grupo político que instauró la educación como pública, obligatoria y laica:",
                "La fundación de la Escuela Nacional Preparatoria estuvo basada en:",
                "Esta sociedad es antecedente del Conservatorio Nacional de Música:"
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 9
    {
        "respuestas": [{
                "t13respuesta": "...México suspendió la deuda externa.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...se llevó a cabo la Batalla de Puebla.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...el general Ignacio Zaragoza estuvo a cargo del ejército mexicano.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...un telegrama enviado por Zaragoza a Juárez indicando el triunfo sobre los franceses.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...era el mejor ejército del mundo",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "ganó gracias a su unidad y valentía.",
                "t17correcta": "6"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Francia invadió México porque..."
            },
            {
                "t11pregunta": "El 5 de mayo de 1862..."
            },
            {
                "t11pregunta": "Durante la Batalla de Puebla..."
            },
            {
                "t11pregunta": "“Las armas del supremo gobierno se han cubierto de gloria [...]” es una frase que aparece en..."
            },
            {
                "t11pregunta": "Durante la intervención francesa se consideraba que el ejército francés..."
            },
            {
                "t11pregunta": "Durante la Batalla de Puebla el ejército mexicano..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas con una línea, según corresponda."
        }
    },
    // Reactivo 10
    {
        "respuestas": [{
                "t13respuesta": "<p>El Despertador Americano<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hidalgo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Constitución de 1824<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>libertad de prensa<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>El Monitor Republicano<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>expresarse<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Santa Anna<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Constitución de 1857<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>Fragua<\/p>",
                "t17correcta": "9"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Los primeros periódicos que surgieron en la nueva españa o en el México independentista fueron periódicos que buscaban transmitir los ideales de la lucha independentista, como "
            },
            {
                "t11pregunta": ", publicado por "
            },
            {
                "t11pregunta": ". Años después la "
            },
            {
                "t11pregunta": " fue la primera en establecer como derecho la "
            },
            {
                "t11pregunta": ", para expresar y difundir el pensamiento de las personas. Ante esta situación surgieron periódicos como El Ateneo Mexicano, "
            },
            {
                "t11pregunta": " o El Siglo Diez y Nueve.<br>La libertad de prensa, entre otras cosas, aseguraba el derecho de los mexicanos a "
            },
            {
                "t11pregunta": " y dar un juicio de valor sobre algo o alguien. Sin embargo, "
            },
            {
                "t11pregunta": " limitó este derecho para acallar las críticas a su gobierno. Finalmente, la "
            },
            {
                "t11pregunta": " restauró este derecho por medio de la ley "
            },
            {
                "t11pregunta": "."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Coloca la palabra o concepto en el espacio correspondiente.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
];