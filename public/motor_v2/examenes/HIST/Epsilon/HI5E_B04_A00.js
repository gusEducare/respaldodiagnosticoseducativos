json = [{
  "respuestas": [{
      "t13respuesta": "1920",
      "t17correcta": "0"
    },
    {
      "t13respuesta": "1930",
      "t17correcta": "1"
    },
    {
      "t13respuesta": "1940",
      "t17correcta": "2"
    },
    {
      "t13respuesta": "1950",
      "t17correcta": "3"
    },
    {
      "t13respuesta": "1960",
      "t17correcta": "4"
    }
  ],
  "preguntas": [{
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Llegada del hombre a la luna",
      "correcta": "4"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Creación del PNR",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Expropiación petrolera",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Creación del IMSS",
      "correcta": "2"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Creación de la TV a color",
      "correcta": "4"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Creación de la SEP",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Voto de la Mujer",
      "correcta": "3"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Cardenismo",
      "correcta": "1"
    }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada década en la que ocurrieron los hechos.<\/p>",
    "descripcion": "Título preguntas",
    "variante": "editable",
    "anchoColumnaPreguntas": 30,
    "evaluable": true
  }
},
{
  "respuestas": [{
      "t13respuesta": "Falso",
      "t17correcta": "0"
    },
    {
      "t13respuesta": "Verdadero",
      "t17correcta": "1"
    }
  ],
  "preguntas": [{
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "La Constitución de 1917 reemplaza a la anticuada constitución de 1857.",
      "correcta": "1"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "En la Constitución de 1917 se le hacen mejoras a temas relativos con la educación, derechos laborales, repartición de tierras y a la protección de la salud de las personas.",
      "correcta": "1"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Las instituciones mexicanas que surgieron en la segunda década del siglo XX eran privadas no gubernamentales.",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Obregón y Carranza fueron los únicos sobrevivientes a la Revolución Mexicana.",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Para la segunda mitad del siglo XX la población nacional no había aumentado mucho.",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "La economía nacional en el siglo XX mejora con la expropiación petrolera y el desarrollo de las industrias.",
      "correcta": "1"
    }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
    "descripcion": "Reactivo",
    "variante": "editable",
    "anchoColumnaPreguntas": 30,
    "evaluable": true
  }
},
{
  "respuestas": [{
      "t13respuesta": "Venustiano Carranza",
      "t17correcta": "1",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "Álvaro Obregón",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "Emiliano Zapata",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "Francisco Villa",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "Álvaro Obregón",
      "t17correcta": "1",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Plutarco Elías Calles",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Emiliano Zapata",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Guadalupe Victoria",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Plutarco Elías Calles",
      "t17correcta": "1",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Pascual Orozco",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "José Agustín Iturbide",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Emiliano Zapata",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Porque a Calles se le conocía  como el máximo jefe.",
      "t17correcta": "1",
      "numeroPregunta": "3"
    },
    {
      "t13respuesta": "Porque es el periodo con la máxima derrama económica en el territorio.",
      "t17correcta": "0",
      "numeroPregunta": "3"
    },
    {
      "t13respuesta": "Porque se había llevado el desarrollo de instituciones y tecnología al máximo.",
      "t17correcta": "0",
      "numeroPregunta": "3"
    },
    {
      "t13respuesta": "Porque el país había alcanzado su máximo reconocimiento a nivel internacional gracias al cine de oro.",
      "t17correcta": "0",
      "numeroPregunta": "3"
    },
    {
      "t13respuesta": "De 1934 a 1982",
      "t17correcta": "1",
      "numeroPregunta": "4"
    },
    {
      "t13respuesta": "De 1934 a 1952",
      "t17correcta": "0",
      "numeroPregunta": "4"
    },
    {
      "t13respuesta": "De 1924 a 1982",
      "t17correcta": "0",
      "numeroPregunta": "4"
    },
    {
      "t13respuesta": "De 1924 a 1952",
      "t17correcta": "0",
      "numeroPregunta": "4"
    }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "1",
    "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Quién fue el primer caudillo revolucionario que logró llegar a la presidencia?",
      "¿Quién fue el caudillo que quería llegar a la presidencia e ideó un plan para quitar a Carranza de la misma?",
      "¿Quién fue el presidente sucesor de Obregón, de quien recibió el apoyo con la condición de obedecer sus órdenes?",
      "¿Por qué se le conoce al periodo de entre 1928 y 1934 como el Maximato?",
      "¿En qué años se desarrolla el presidencialismo mexicano?"
    ],

    "preguntasMultiples": true
  }
},
//4
{
  "respuestas": [
      {
          "t13respuesta": "Falso",
          "t17correcta": "0"
      },
      {
          "t13respuesta": "Verdadero",
          "t17correcta": "1"
      },
  ],
  "preguntas" : [
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "México ha sido un país democrático desde los tiempos de Juárez, su democracia ha sido un modelo internacional.",
          "correcta"  : "0"
      },
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "A causa de la dictadura de Díaz, uno de los lemas de la Revolución fue “No Reelección”.",
          "correcta"  : "1"
      },
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "México intentó adoptar los modelos de democracia creados por Estados Unidos y algunos países de Europa.",
          "correcta"  : "1"
      },
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "Los clubes políticos del tiempo de Juárez son los primeros partidos políticos en México.",
          "correcta"  : "0"
      },
      {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "En 1918 se creó la Ley Electoral para poner orden a las elecciones, considerando los partidos políticos como instrumentos para las elecciones.",
          "correcta"  : "1"
      }
  ],
  "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
      "descripcion": "Reactivo",   
      "variante": "editable",
      "anchoColumnaPreguntas": 30,
      "evaluable"  : true
  }        
},
//5
{
  "respuestas": [
      {
          "t13respuesta": "<p>rebelión cristera<\/p>",
          "t17correcta": "1"
      },
      {
          "t13respuesta": "<p>Iglesia Católica<\/p>",
          "t17correcta": "2"
      },
      {
          "t13respuesta": "<p>Estado Mexicano<\/p>",
          "t17correcta": "3"
      },
      {
          "t13respuesta": "<p>Elías Calles<\/p>",
          "t17correcta": "4"
      },
      {
          "t13respuesta": "<p>artículo 130<\/p>",
          "t17correcta": "5"
      },
      {
          "t13respuesta": "<p>no puede tratar<\/p>",
          "t17correcta": "6"
      }, 
      {
          "t13respuesta": "<p>misas<\/p>",
          "t17correcta": "7"
      },
      {
          "t13respuesta": "<p>feligreses<\/p>",
          "t17correcta": "8"
      },
      {
          "t13respuesta": "<p>conflicto armado<\/p>",
          "t17correcta": "9"
      },
      {
          "t13respuesta": "<p>Guerra Cristera<\/p>",
          "t17correcta": "10"
      },
      {
        "t13respuesta": "<p>tres años<\/p>",
        "t17correcta": "11"
      },
      {
        "t13respuesta": "<p>Revolución Mexicana<\/p>",
        "t17correcta": "00"
      },
      {
        "t13respuesta": "<p>Iglesia Ortodoxa<\/p>",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "<p>Artículo 3°<\/p>",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "<p>Conglomerados<\/p>",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "<p>Doce años<\/p>",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "<p>300 000<\/p>",
        "t17correcta": "0"
      },
      {
      "t13respuesta": "<p>250 000<\/p>",
      "t17correcta": "12"
      },
  ],
  "preguntas": [
      {
          "t11pregunta": "<p><br>La <\/p>"
      },
      {
          "t11pregunta": "<p> fue un conflicto armado entre la <\/p>"
      },
      {
          "t11pregunta": "<p> y el <\/p>"
      },
      {
          "t11pregunta": "<p>. El inicio del conflicto se originó cuando <\/p>"
      },
      {
          "t11pregunta": "<p> intentó, por los medios legales, disminuir el poder de la iglesia. Así, el presidente Calles aplicó el <\/p>"
      },
      {
          "t11pregunta": "<p> 一 que hace referencia a que la iglesia <\/p>"
      },
      {
          "t11pregunta": "<p> temas del gobierno o tener injerencia alguna en ellos 一 con mayor exigencia, esto causó molestia a la iglesia. Ante dicha acción la iglesia suspendió todas las <\/p>"
      },
      {
          "t11pregunta": "<p> culpando al gobierno de que sucediera esto, además incitó a los <\/p>"
      },
      {
          "t11pregunta": "<p> a tomar medidas contra las políticas que el gobierno había implantado en contra de la iglesia. Estas acciones por parte de ambos bandos generó la suficiente tensión para que estallara un <\/p>"
      },
      {
          "t11pregunta": "<p> en 1926. La <\/p>"
      },
      {
          "t11pregunta": "<p> duró <\/p>"
      },
      {
          "t11pregunta": "<p> y tuvo un saldo de <\/p>"
      },
      {
          "t11pregunta": "<p> muertos y un número similar de desplazados por la guerra.<\/p>"
      }
  ],
  "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que completen el párrafo.",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores":true
  }
},
//6
{
  "respuestas": [{
      "t13respuesta": "La parte centro occidental del país.",
      "t17correcta": "1",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "La parte noreste del país.",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "La parte centro oriental del país.",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "La parte sureste del país.",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "La reforma agraria que estaba en proceso.",
      "t17correcta": "1",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Los derechos y fueros del ejército.",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "La libertad de expresión y de prensa.",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "La promulgación de una nueva Carta Magna.",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Vecinos de zonas rurales sin experiencia militar.",
      "t17correcta": "1",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Militares católicos con poder gubernamental.",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Políticos religiosos de clase media.",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Ciudadanos comunes con una fuerte creencia en la iglesia.",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "1",
    "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué parte del país fue principalmente afectada por el conflicto de cristeros?",
      "Los cristeros, además de defender su religión, ¿contra qué cosa luchaban?",
      "¿Quiénes eran los militantes de la milicia de los cristeros?",
    ],
    "t11instruccion": "El gobierno de Calles, por sus afanes de ampliación y consolidación estatal, tuvo grandes conflictos con otra institución de alcance nacional: la Iglesia Católica. El enfrentamiento fue de una magnitud enorme, pues implicaba competencias culturales, educativas, sociales y políticas [...] Este conflicto asoló duramente por casi tres años, de finales de 1926 a mediados de 1929, al sector rural de varios estados centro occidentales [...] Además de defender sus creencias religiosas, los cristeros provenían de zonas con un alto número de rancheros, los que veían en la reforma agraria una amenaza más que una promesa [...] Sus limitaciones militares fueron notables: nunca llegaron a conformar un ejército con un mando unificado [...]; se trataba más bien de fuerzas defensivas locales, encabezadas por vecinos con poca o nula experiencia militar;  sufrieron además limitaciones económicas, lo que se reflejó en su pobre armamento [...]; por último, fueron combatidos por el ejército gubernamental y por fuerzas organizadas de agraristas y obreros. A pesar de que no tenían la fuerza suficiente para derrocar al gobierno, era evidente que los guerrilleros cristeros tampoco serían fácilmente derrotados, lo que provocaría una inestabilidad endémica. Por ello el gobierno accedió a negociar con los jerarcas de la iglesia católica: éstos acatarían la autoridad gubernamental y se abstendrían de actuar en la política abiertamente, y aquel aceptó que no intentaría poner en vigor los elementos más jacobinos de la Constitución de 1917.<br><br>Javier Garciadiego, “La revolución”, Nueva<br> Historia Mínima de México, ColMex, 2008, <br>pp. 464-466",
    "preguntasMultiples": true
  }
},
//7
{
  "respuestas": [
      {
          "t13respuesta": "con la llegada de Lázaro Cárdenas al poder.",
          "t17correcta": "1"
      },
      {
          "t13respuesta": "con la expropiación petrolera.",
          "t17correcta": "2"
      },
      {
          "t13respuesta": "dura seis años y ya no cuatro.",
          "t17correcta": "3"
      },
      {
          "t13respuesta": "ingreso económico que le permitiría crear institutos como el IMSS.",
          "t17correcta": "4"
      },
      {
        "t13respuesta": "un proyecto de nación a largo plazo.",
        "t17correcta": "5"
      },
      {
          "t13respuesta": "trajo como consecuencia el empobrecimiento de la clase media, la falta de resultados esperados y el malestar de obreros y campesinos.",
          "t17correcta": "6"
      },
  ],
  "preguntas": [
      {
          "t11pregunta": "Después de la Revolución y la Guerra Cristera, México comenzaría su desarrollo económico..."
      },
      {
          "t11pregunta": "El desarrollo económico se logró gracias al ingreso económico que surgió..."
      },
      {
          "t11pregunta": "Fue a partir de Cárdenas que el periodo presidencial..."
      },
      {
          "t11pregunta": "Durante el periodo de Cárdenas, México tendría un importante..."
      },
      {
          "t11pregunta": "El principal problema de México sería la falta de..."
      },
      {
          "t11pregunta": "La mezcla de malas decisiones tomadas sin información, pedir préstamos externos y el arraigamiento de la corrupción..."
      }
  ],
  "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda."
  }
},
//8
{
  "respuestas": [{
      "t13respuesta": "Las compañías petroleras extranjeras desafiaron al gobierno.",
      "t17correcta": "1",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "El estado mexicano necesitaba recursos.",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "El gobierno quería marcar un cambio en la economía.",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "Los trabajadores demandaban que el petróleo se hiciera suyo.",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "La jerarquía católica, los empresarios, obreros, campesinos, intelectuales y artistas.",
      "t17correcta": "1",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Empresarios, otros países y la cámara de comercio.",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "El senado de la república y el clero, apoyados por el pueblo.",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "El estado, el ejército y las empresas norteamericanas.",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "La idea de nación arraigada en el pueblo.",
      "t17correcta": "1",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "La felicidad nacional ante las decisiones gubernamentales.",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "La empatía nacional para las manifestaciones.",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "La esperanza ante un futuro mejor.",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "1",
    "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Entre otras causas, ¿cuál fue el motivo por el que el gobierno expropió el petróleo?",
      "La decisión del gobierno para expropiar el petróleo fue apoyada por:",
      "Ante la situación que condujo a la expropiación petrolera, ¿qué fue visto como nunca antes en el país?",
    ],
    "t11instruccion": "A fines de 1937 y principios de 1938 el gobierno cardenista se vio sometido a una dura prueba. Las compañías petroleras extranjeras desafiaron abiertamente al  Estado mexicano al desatender un fallo de la Suprema Corte de Justicia que favorecía a los trabajadores. La respuesta del gobierno fue la expropiación petrolera, anunciada el 18 de marzo de 1938. La jerarquía católica, los empresarios, obreros, campesinos, intelectuales y artistas respaldaron la audaz decisión del presidente Cárdenas. Fue entonces cuando la idea de nación cobró gran vigor, quizá como nunca antes en la historia del país. A pesar de los esfuerzos de las compañías extranjeras por sabotearla, la industria petrolera nacional salió bien librada gracias a los obreros y técnicos mexicanos y también, hay que decirlo, al escaso apoyo que aquellas compañías recibieron del gobierno norteamericano, cuya máxima preocupación era el inminente estallido de la guerra mundial. Pocos meses después de la expropiación nació la empresa Petróleos Mexicanos (Pemex), cuya fragilidad inicial obligó al gobierno a subsidiarla de distintas maneras.<br><br>Luis Aboites Aguilar, “El último Tramo,<br> 1929-2000”, Nueva Historia Mínima de<br> México, ColMex, 2008, pp. 480-482",
    "preguntasMultiples": true
  }
},
//9
{
  "respuestas": [
      {
          "t13respuesta": "<p>Segunda Guerra Mundial<\/p>",
          "t17correcta": "1"
      },
      {
          "t13respuesta": "<p>Europa<\/p>",
          "t17correcta": "2"
      },
      {
          "t13respuesta": "<p>XX<\/p>",
          "t17correcta": "3"
      },
      {
          "t13respuesta": "<p>Partido Nacional<\/p>",
          "t17correcta": "4"
      },
      {
        "t13respuesta": "<p>socialista Obrero Alemán<\/p>",
        "t17correcta": "5"
      },
      {
      "t13respuesta": "<p>Polonia<\/p>",
      "t17correcta": "6"
      },
      {
          "t13respuesta": "<p>Adolf Hitler<\/p>",
          "t17correcta": "7"
      },
      {
          "t13respuesta": "<p>México<\/p>",
          "t17correcta": "8"
      }, 
      {
          "t13respuesta": "<p>países del Eje<\/p>",
          "t17correcta": "9"
      },
      {
          "t13respuesta": "<p>1941<\/p>",
          "t17correcta": "10"
      },
      {
          "t13respuesta": "<p>Manuel Ávila Camacho<\/p>",
          "t17correcta": "11"
      },
      {
          "t13respuesta": "<p>barcos<\/p>",
          "t17correcta": "12"
      },
      {
        "t13respuesta": "<p>relaciones diplomáticas<\/p>",
        "t17correcta": "13"
      },
      {
        "t13respuesta": "<p>1942<\/p>",
        "t17correcta": "14"
      },
      {
        "t13respuesta": "<p>Potrero del Llano<\/p>",
        "t17correcta": "15"
      },
      {
        "t13respuesta": "<p>Faja de Oro<\/p>",
        "t17correcta": "16"
      },
      {
        "t13respuesta": "<p>Aliados<\/p>",
        "t17correcta": "17"
      },
      {
        "t13respuesta": "<p>Escuadrón 201<\/p>",
        "t17correcta": "18"
      },
      {
        "t13respuesta": "<p>Primera Guerra Mundial<\/p>",
        "t17correcta": "0"
      },
      {
      "t13respuesta": "<p>Venezuela<\/p>",
      "t17correcta": "0"
      },
      {
      "t13respuesta": "<p>Países de la triple entente<\/p>",
      "t17correcta": "0"
      },
      {
      "t13respuesta": "<p>Presidente Cárdenas<\/p>",
      "t17correcta": "0"
      },
      {
      "t13respuesta": "<p>1958<\/p>",
      "t17correcta": "0"
      },
      {
      "t13respuesta": "<p>Unidad militar 201<\/p>",
      "t17correcta": "0"
      }
  ],
  "preguntas": [
      {
          "t11pregunta": "<p>La <\/p>"
      },
      {
          "t11pregunta": "<p> fue un conflicto armado que surgió en <\/p>"
      },
      {
          "t11pregunta": "<p> a finales de la primera mitad del siglo <\/p>"
      },
      {
          "t11pregunta": "<p>. Alemania, bajo el poder del <\/p>"
      },
      {
        "t11pregunta": "<p> <\/p>"
      },
      {
          "t11pregunta": "<p>, invadió <\/p>"
      },
      {
          "t11pregunta": "<p>, esto ocasionó una serie de reacciones por parte de los aliados de dicho país que terminarían por hacer declaraciones de guerra ante la Alemania de <\/p>"
      },
      {
          "t11pregunta": "<p>.<br><br>Durante el conflicto bélico, <\/p>"
      },
      {
          "t11pregunta": "<p> se mantuvo neutral prestando su apoyo a su vecino del norte y protestando por las acciones injustas que llevaban a cabo los <\/p>"
      },
      {
          "t11pregunta": "<p> por toda Europa. La supuesta neutralidad que México tenía ante el conflicto Europeo cayó, cuando en <\/p>"
      },
      {
          "t11pregunta": "<p>, durante el mandato de <\/p>"
      },
      {
          "t11pregunta": "<p>, el gobierno ordenó la incautación de los <\/p>"
      },
      {
          "t11pregunta": "<p> alemanes e italianos, hecho que enturbió las <\/p>"
      },
      {
          "t11pregunta": "<p> con los países del Eje.<br><br>Finalmente en mayo de <\/p>"
      },
      {
          "t11pregunta": "<p>, después de que los submarinos alemanes hubiesen hundido los buques “el <\/p>"
      },
      {
          "t11pregunta": "<p>” y “el <\/p>"
      },
      {
          "t11pregunta": "<p>”, México dejó de lado su neutralidad y se declaró explícitamente del lado de los <\/p>"
      },
      {
          "t11pregunta": "<p>, su participación en la guerra fue principalmente con apoyo y recursos a los aliados. México solo batalló, de manera simbólica, en el frente  del Pacífico, con el <\/p>"
      },
      {
          "t11pregunta": "<p>” al lado de la Fuerza Aérea Estadounidense.<\/p>"
      },
  ],
  "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que completen el párrafo.",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores":true
  }
},
//10
{
  "respuestas": [
      {
          "t13respuesta": "<p>Cárdenas lleva a cabo la expropiación petrolera y se crea PEMEX.<\/p>",
          "t17correcta": "0",
          etiqueta:"Paso 1"
      },
      {
          "t13respuesta": "<p>Alemania invade Polonia. Francia e Inglaterra protestan.<\/p>",
          "t17correcta": "1",
          etiqueta:"Paso 2"
      },
      {
          "t13respuesta": "<p>Manuel Ávila Camacho sube a la presidencia en 1940.<\/p>",
          "t17correcta": "2",
          etiqueta:"Paso 3"
      },
      {
          "t13respuesta": "<p>México se une a los países Aliados.<\/p>",
          "t17correcta": "3",
          etiqueta:"Paso 4"
      },
      {
          "t13respuesta": "<p>México comienza a abastecer la demanda petrolera de Estados Unidos y sus buques son hundidos por submarinos alemanes.<\/p>",
          "t17correcta": "4",
          etiqueta:"Paso 5"
      },
      {
          "t13respuesta": "<p>México envía a combatir al frente del Pacífico al Escuadrón 201.<\/p>",
          "t17correcta": "5",
          etiqueta:"Paso 6"
      },
      {
          "t13respuesta": "<p>La derrama económica propiciada por la guerra hace crecer a México.<\/p>",
          "t17correcta": "6",
          etiqueta:"Paso 7"
      },
      {
          "t13respuesta": "<p>Los ejidos creados por Cárdenas comenzaban a perder su apoyo gubernamental.<\/p>",
          "t17correcta": "7",
          etiqueta:"Paso 8"
      }
  ],
  "pregunta": {
      "c03id_tipo_pregunta": "9",
      "t11pregunta": "Ordena los siguientes elementos según sucedieron.",
  }
},
//11
{
  "respuestas": [{
      "t13respuesta": "Falso",
      "t17correcta": "0"
    },
    {
      "t13respuesta": "Verdadero",
      "t17correcta": "1"
    }
  ],
  "preguntas": [{
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Los mexicanos buscaron sin saberlo, durante la Revolución, que el gobierno adoptara la función de velar por la Seguridad Social del pueblo.",
      "correcta": "1"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "En México desde la conquista en 1492 existieron los hospitales públicos y el sistema de seguridad social.",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Antes de la existencia de los servicios de salud, las personas contaban solamente con el apoyo de su familia y amigos si sufrían enfermedades o accidentes.",
      "correcta": "1"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "El Instituto Mexicano del Seguro Social (IMSS) fue fundado en 1900 para proteger a las mujeres y niños en situación precaria.",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "El Instituto de Seguridad y Servicio Social para los  Trabajadores del Estado fue fundado en 1959 para la protección de quienes trabajaban para el gobierno.",
      "correcta": "1"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "En 1910 la población apenas alcanzaba los 15 millones de habitantes y para 1929 había crecido a 119 millones de habitantes.",
      "correcta": "0"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Lo que generó la explosión demográfica fue la implementación del modelo de seguridad social.",
      "correcta": "1"
    },
    {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "En los lugares con menos infraestructura que habían sido los menos poblados, la población aumentó de forma impresionante.",
      "correcta": "0"
    }
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "13",
    "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
    "descripcion": "Reactivo",
    "variante": "editable",
    "anchoColumnaPreguntas": 30,
    "evaluable": true
  }
},
  //12
  {
    "respuestas": [{
        "t13respuesta": "No, a pesar de que había tenido una participación importante en la Revolución.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Sí, siempre se le ha considerado en la política.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Sí, fue la primera vez que la mujer tenía participación política.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "No, a las mujeres no les interesaba ser parte de la política del país.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      }, {
        "t13respuesta": "No sé, no se menciona nada sobre ello en la carta magna.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Buscaba el derecho al voto y la participación política de la mujer.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Buscaba la igualdad social y salarial entre los hombres y las mujeres.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Buscaba que las mujeres fueran superiores a los hombres en derechos.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Buscaba el derecho al aborto y a la decisión sobre su cuerpo.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Buscaba la destitución de los hombres de la política.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "San Luis Potosí",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Querétaro",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Aguascalientes",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Nuevo León",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Chihuahua",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Permitía a las mujeres votar en elecciones municipales y ser votadas.",
        "t17correcta": "1",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Permitía a las mujeres el derecho de libre expresión, asociación y libertad de culto.",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Permitía a las mujeres votar en elecciones estatales y ser votadas.",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Permitía a las mujeres votar en elecciones federales y ser votadas.",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Permitía a las mujeres ingresar a las universidades y recibirse en alguna carrera.",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Cuando se elaboró la Constitución de 1917, ¿se tomó en cuenta a la mujer?",
        "¿Qué buscaba el Frente Único Pro Derechos de la Mujer?",
        "En 1924, ¿qué estado le otorga el derecho al voto a la mujer?",
        "En diciembre de 1946 el Congreso aprobó una reforma, propuesta por el presidente Miguel Alemán, a la Constitución en la que:",
      ],
      "preguntasMultiples": true,
    }
  },
  //13
  {
    "respuestas": [
        {
            "t13respuesta": "<p>siglo XX<\/p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>revueltas armadas<\/p>",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "<p>país rural<\/p>",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "<p>país urbano<\/p>",
            "t17correcta": "4"
        },
        {
          "t13respuesta": "<p>instituciones<\/p>",
          "t17correcta": "5"
      },
        {
          "t13respuesta": "<p>cultura<\/p>",
          "t17correcta": "6"
        },
        {
        "t13respuesta": "<p>la cultura mexicana<\/p>",
        "t17correcta": "7"
        },
        {
            "t13respuesta": "<p>prehispánica<\/p>",
            "t17correcta": "8"
        },
        {
            "t13respuesta": "<p>Porfirio Díaz<\/p>",
            "t17correcta": "9"
        }, 
        {
            "t13respuesta": "<p>Revolución<\/p>",
            "t17correcta": "10"
        },
        {
            "t13respuesta": "<p>cuarenta<\/p>",
            "t17correcta": "11"
        },
        {
            "t13respuesta": "<p>radio<\/p>",
            "t17correcta": "12"
        },
        {
            "t13respuesta": "<p>televisión<\/p>",
            "t17correcta": "13"
        },
        {
          "t13respuesta": "<p>Siglo XIX<\/p>",
          "t17correcta": "0"
        },
        {
        "t13respuesta": "<p>Iglesia<\/p>",
        "t17correcta": "0"
        },
        {
        "t13respuesta": "<p>País rural<\/p>",
        "t17correcta": "0"
        },
        {
        "t13respuesta": "<p>La cultura maya<\/p>",
        "t17correcta": "0"
        },
        {
        "t13respuesta": "<p>colonial<\/p>",
        "t17correcta": "0"
        },
        {
        "t13respuesta": "<p>Francisco I. Madero<\/p>",
        "t17correcta": "0"
        },
        {
          "t13respuesta": "<p>50<\/p>",
          "t17correcta": "0"
          }
    ],
    "preguntas": [
        {
            "t11pregunta": "<p>Durante el final de la primera mitad del <\/p>"
        },
        {
            "t11pregunta": "<p>, México cambió como nación. Ya no era un México plagado de <\/p>"
        },
        {
            "t11pregunta": "<p>, se había transformado; había dejado de ser un <\/p>"
        },
        {
            "t11pregunta": "<p> caótico para dar paso a un <\/p>"
        },
        {
          "t11pregunta": "<p> gobernado por <\/p>"
        },
        {
            "t11pregunta": "<p>. Este nuevo México estaba nutrido principalmente por la <\/p>"
        },
        {
            "t11pregunta": "<p> adquirida en los años de consolidación como país ㅡ pues recordemos que finalmente <\/p>"
        },
        {
            "t11pregunta": "<p> es una mezcla de costumbres, tradiciones y herencias <\/p>"
        },
        {
            "t11pregunta": "<p>, europea, asiática y africana. A ello se le añade, además, todos los pequeños detalles que moldearon posteriormente la cultura, como el afrancesamiento propiciado por <\/p>"
        },
        {
            "t11pregunta": "<p> o la cultura del corrido que surgió durante la <\/p>"
        },
        {
            "t11pregunta": "<p> ㅡ. Así, con la prosperidad que se vivió después de la década de los años <\/p>"
        },
        {
            "t11pregunta": "<p>, en México surgieron grandes expresiones artísticas, además los medios de comunicación ( <\/p>"
        },
        {
            "t11pregunta": "<p> y <\/p>"
        },
        {
            "t11pregunta": "<p> ) se extendieron y con ellos llegó el cine. <\/p>"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "Arrastra las palabras que completen el párrafo.",
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "contieneDistractores":true
    }
},
//14
{
  "respuestas": [{
      "t13respuesta": "David Alfaro Siqueiros",
      "t17correcta": "1",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "José Clemente Orozco",
      "t17correcta": "1",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "Diego Rivera",
      "t17correcta": "1",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "José Revueltas",
      "t17correcta": "0",
      "numeroPregunta": "0"
    }, {
      "t13respuesta": "Juan Gabriel",
      "t17correcta": "0",
      "numeroPregunta": "0"
    },
    {
      "t13respuesta": "Mariano Azuela",
      "t17correcta": "1",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Juan Rulfo",
      "t17correcta": "1",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Octavio Paz",
      "t17correcta": "1",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "José Martí",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Jorge Luis Borges",
      "t17correcta": "0",
      "numeroPregunta": "1"
    },
    {
      "t13respuesta": "Pedro Infante",
      "t17correcta": "1",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Jorge Negrete",
      "t17correcta": "1",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "María Félix",
      "t17correcta": "1",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Germán Valdés",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
    {
      "t13respuesta": "Libertad Lamarque",
      "t17correcta": "0",
      "numeroPregunta": "2"
    },
  ],
  "pregunta": {
    "c03id_tipo_pregunta": "2",
    "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>¿Quiénes son los principales representantes del muralismo mexicano?",
      "¿Cuáles son los nombres de algunos de los más destacados escritores mexicanos?",
      "¿Cuáles son algunos de los actores mexicanos que formaron parte de la llamada “época de oro del cine mexicano”?",
    ],
    "preguntasMultiples": true,
  }
},
  //15
  {
    "respuestas": [
        {
            "t13respuesta": "se puso un esfuerzo enorme en la educación en México.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "las personas no querían asistir a la escuela.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "existían comunidades en las que no se hablaba el español como la lengua nacional.",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "80% de la población en México era analfabeta.",
            "t17correcta": "4"
        },
        {
          "t13respuesta": "la educación debía ser obligatoria, laica y gratuita.",
          "t17correcta": "5"
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "Desde el comienzo del gobierno de Díaz..."
        },
        {
            "t11pregunta": "Uno de los principales problemas fue que..."
        },
        {
            "t11pregunta": "Otro de los grandes retos en la educación de un país de habla hispana fue que..."
        },
        {
            "t11pregunta": "Después de la Revolución Mexicana..."
        },
        {
            "t11pregunta": "En la Constitución de 1917 se logró estipular que..."
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda."
    }
},
  //16
  {
    "respuestas": [{
        "t13respuesta": "1920-1939",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "1930-1940",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "1941-1950",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "1951-1980",
        "t17correcta": "3"
      },
    ],
    "preguntas": [{
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "José Vasconcelos es el encargado de la SEP",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Se crea el Instituto Nacional de Bellas Artes",
        "correcta": "2"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Se crea el Instituto Politécnico Nacional (IPN)",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Esta década se caracterizó por la creación de escuelas",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Se reduce el analfabetismo en un 50% en la población adulta",
        "correcta": "3"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada suceso.<\/p>",
      "descripcion": "Título preguntas",
      "variante": "editable",
      "anchoColumnaPreguntas": 30,
      "evaluable": true
    }
  },
  //17
  {
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "México siempre ha mantenido una política pacifista y de no intervención ante los conflictos de otros países.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los mexicanos son un pueblo apático ante los problemas o desgracias de otras naciones.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La Guerra Civil española inició en 1936 y terminó en 1945.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Ante la Guerra Civil muchos españoles huyeron hacia México, fueron recibidos por Lázaro Cárdenas.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "“Los niños de Morelia”, así conocidos los 56 niños españoles que fueron enviados a México por sus padres, con la esperanza de que sobrevivieran a la guerra.",
            "correcta"  : "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
        "descripcion": "Reactivo",
        "variante": "editable",
        "anchoColumnaPreguntas": 30,
        "evaluable"  : true
    }
 },
 //18
   {
          "respuestas": [
              {
                  "t13respuesta": "sucedió el 2 de octubre de 1968.",
                  "t17correcta": "1"
              },
              {
                  "t13respuesta": "cuando la policía y el ejército ingresaron a las escuelas vocacionales del IPN y la UNAM.",
                  "t17correcta": "2"
              },
              {
                  "t13respuesta": "enviar desde el palacio de gobierno tanques militares para dispersar a los manifestantes.",
                  "t17correcta": "3"
              },
              {
                  "t13respuesta": "ante ello un grupo militar, vestidos de civiles, fue enviado a asesinar a las personas reunidas en ese lugar.",
                  "t17correcta": "4"
              },
              {
                  "t13respuesta": "pretendía que se culpara a los estudiantes por la matanza.",
                  "t17correcta": "5"
              }
          ],
          "preguntas": [
              {
                  "t11pregunta": "La matanza de Tlatelolco fue uno de los más lamentables hechos ocurridos en México..."
              },
              {
                  "t11pregunta": "Los problemas entre los estudiantes y el gobierno surgieron..."
              },
              {
                  "t11pregunta": "Ante la violación de sus derechos los estudiantes comenzaron a manifestarse y la medida tomada por el gobierno fue..."
              },
              {
                  "t11pregunta": "El 2 de octubre miles de personas se reunieron en la Plaza de las Tres culturas en Tlatelolco, para continuar con las protestas..."
              },
              {
                  "t11pregunta": "El gobierno bajo las órdenes del presidente Gustavo Díaz Ordaz..."
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "12",
              "t11pregunta":"Relaciona las columnas según corresponda."
          },
      }
  ];
