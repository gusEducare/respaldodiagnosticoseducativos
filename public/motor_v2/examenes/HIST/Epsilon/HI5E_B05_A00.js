
json = [
    {
        "respuestas": [
            {
                "t13respuesta": "1980",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "1990",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "2000",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "2010",
                "t17correcta": "3"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derrumbe de las torres gemelas",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Alternancia en el poder presidencial",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Caída del Muro de Berlín",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Levantamiento armado indígena EZLN",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Crisis  económica mundial",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Expropiación de los bancos mexicanos",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Regreso del PRI a la presidencia",
                "correcta"  : "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz editable y marca cada opción según la década en que ocurrieron los acontecimientos.",
            "descripcion": "Año",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },//2
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Terremoto en la Ciudad de México.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Carlos Salinas gana las elecciones presidenciales.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "La Ciudad de México es la más poblada del mundo.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Caída del muro de Berlín.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":     "Pobreza",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Falta de planeación urbana.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "No ha existido alternancia en el poder político.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Falta de medios de comunicación.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":     "Elección del primer presidente estadounidense de origen afroamericano.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Manifestaciones internacionales en contra de la Guerra de Irak.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Crisis Hipotecaria en Estados Unidos.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Guerra del Golfo.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Plantón de Andrés Manuel López Obrador.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "preguntasMultiples":true,
         "dosColumnas": false,
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>Acontecimientos importantes dentro del territorio mexicano en la década de los ochenta.","Problemáticas actuales que padece el territorio mexicano.","Acontecimientos internacionales importantes en la década de los 2000."]
      }
   },//3
   {
        "respuestas": [
            {
                "t13respuesta": "<p>fin de la guerra<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>crisis mundial<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>préstamos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>1976<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>devaluación del peso<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>consumo básico<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>inflación<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>deuda externa<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>José López Portillo<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>respaldo económico<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>Petróleos Mexicanos (PEMEX)<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>1982<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>Manuel Ávila Camacho<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>Superávit<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>Consumo de alto precio<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>Crisis regional<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>Invermás<\/p>",
                "t17correcta": "17"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El precio del petróleo bajó debido al&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, creando una&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y, por supuesto, una crisis en la economía mexicana. México había pedido&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y planeado sus ingresos con base en un cierto precio del petróleo,  al caer este  provocó el derrumbe de todos esos planes. Desde&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se había desencadenado la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;frente al dólar estadounidense, y por consecuencia el incremento en el costo de muchos productos de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;a esto se le llama&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. El gobierno mexicano trató de tomar medidas que resultaron contraproducentes, pues pidió préstamos que incrementaron la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, aunado a que los gobiernos de los presidentes Luis Echeverría y&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;gastaron más de lo que el país tenía. Como una solución, el gobierno empezó a hacer más billetes y monedas sin tener el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;suficiente, provocando que el peso se devaluara todavía más. Otras causas de dicha devaluación también fueron los malos manejos de las empresas del gobierno como&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y Teléfonos de México (TELMEX).<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Selecciona las palabras de los recuadros que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//4
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El crecimiento de las ciudades mexicanas se debió a las  oportunidades y condiciones favorables de empleo que existían en las zonas rurales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La migración rural hacia las ciudades mexicanas provocó cinturones de pobreza y carencia de servicios básicos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las inconformidades sociales en México se debieron a la igualdad de oportunidades y distribución equitativa de las riquezas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La implementación del modelo Neoliberal en México tuvo repercusiones negativas en la vida de los obreros, generando inconformidad social.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debido a la inconformidad nacional general respecto a la corrupción política del país y la desigualdad en el año 2000, existió alternancia en el poder.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//5
    {
        "respuestas": [
            {
                "t13respuesta": "...con la finalidad de dar mayor transparencia a las elecciones.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...pues ha habido elecciones en las que se ha dado un gran abstencionismo.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...en el año 2000.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...es el cambio del poder de una persona o grupo, en este caso un partido político.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...en 1997.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...para reforzar el mensaje de la responsabilidad que implica el acudir a votar.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El Instituto Federal Electoral se creó..."
            },
            {
                "t11pregunta": "La participación ciudadana ha tenido altibajos..."
            },
            {
                "t11pregunta": "Por primera vez un candidato del PAN consigue ganar las elecciones presidenciales..."
            },
            {
                "t11pregunta": "La alternancia en el poder..."
            },
            {
                "t11pregunta": "Por primera vez el PRI pierde las elecciones de Jefe de Gobierno de la capital..."
            },
            {
                "t11pregunta": "Se emprendieron campañas para promover la participación de los ciudadanos en las elecciones..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta":"Relaciona las columnas por medio de líneas según corresponda.",
        }
    },//6
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELa investigación y desarrollo científico que el mundo ha aportado.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEl alto grado de inversión en el desarrollo tecnológico mexicano.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa investigación y desarrollo científico estadounidense.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ELa creación y desarrollo de nuevas tecnologías.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa creación de libros digitales.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEl desarrollo de la ciencia biológica.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ELos teléfonos celulares, las pantallas de LED y la radio.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa ciencia y sus laboratorios.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELos avances médicos respecto al genoma humano.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EAcademia Mexicana de Ciencias.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECentro de Investigación y de Estudios Avanzados.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAcademia Internacional de Ciencias.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EGenoma humano.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEstructura del gen.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta":"\u003Cp\u003EGenomica molecular.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>Los aportes tecnológicos como Facebook, Twitter, Youtube y Google se dieron gracias a:",
         "Con la Revolución Industrial se estimuló:","Actualmente son parte de la vida cotidiana de las personas.",
         "Se fundó con la finalidad de impulsar la inversión en la investigación científica en México.",
         "Es uno de los grandes aportes científicos actuales que se encarga de descifrar  por completo la estructura humana."]
      }
   },//7
    {
        "respuestas": [
            {
                "t13respuesta": "Celular de la década de los ochenta",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Smartphone",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Procesadores integrados",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su costo lo hacía inaccesible a la población en general",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gran velocidad",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Almacenamiento de información",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gran tamaño",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pesa aproximadamente 750 gramos",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada característica asociada con con los siguientes teléfonos móviles.",
            "descripcion": "Característica",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },//8
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Sobreexplotación de los mantos acuíferos.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Creación de sistemas artificiales para abastecer la demanda de agua.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Alteración de los flujos naturales de los cuerpos de agua.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":     "Sistemas de drenaje eficientes para el agua.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":     "Sistemas de captación de agua en algunas zonas de México.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Creación de sistemas artificiales para abastecer la demanda de agua.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Implementación de campañas del cuidado del agua entre la población mexicana.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Un sistema eficiente de saneamiento y calidad del agua a nivel nacional.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":     "Un sistema de monitoreo a nivel nacional de la calidad del agua y su repercusión en la salud.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":     "Mal manejo y tratamiento de los residuos sólidos.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Escasez de agua y sobreexplotación de los mantos acuíferos.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Deforestación de bosques, selvas, manglares y otros ecosistemas.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":     "Escasez de energía solar.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "preguntasMultiples":true,
         "dosColumnas": true,
         "t11pregunta":["Selecciona todas las respuestas correctas<br><br>¿Cuáles son algunas de las principales problemáticas ambientales respecto al tema del agua?",
         "¿Cuáles son algunas de las acciones que se han implementado para intentar solucionar la problemática del agua en México?",
         "¿Cuáles son algunas de las principales problemáticas ambientales que padece México?"]
      }
   },//9
   {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las principales problemáticas ambientales incluyen la gestión de residuos sólidos y la gestión de una economía sustentable.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El desarrollo sustentable considera un equilibrio entre lo económico, el medio ambiente y el bienestar social.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las problemáticas ambientales están estrechamente relacionadas.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11instruccion":"El desarrollo sustentable representa un esfuerzo continuo por integrar y equilibrar tres tipos de fuerzas o pilares: el bienestar social, la prosperidad económica y la protección del medio ambiente. Estos tres pilares serían necesarios para comprender qué es o puede ser un desarrollo sostenible.  Entre los principales problemas ambientales se encuentran: la gestión de residuos sólidos, escasez del agua, falta de recursos energéticos alternativos y el cambio climático, todos están estrechamente relacionados. <center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manuel Rodriguez,<i> “El desarrollo sustentable<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;en la mente del niño y el adolescente”, Editorial<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resma, 2008,</i> pp. 7-8.</center>",
            "t11pregunta": "Lee el párrafo y elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//10
    {
        "respuestas": [
            {
                "t13respuesta": "...son resultado de los orígenes y la mezcla con otras culturas del mundo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...se impulsó la creación y fusión de nuevas modas, tendencias y estilos artísticos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...raíces prehispánicas, españolas, religiosas y de otros países.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...nombrándose como fresas, darketos, rockeros, etcétera.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...es la temática de Hollywood y televisión estadounidense.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...tiene representantes como Carlos Fuentes, Juan Rulfo y Octavio Paz.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Las expresiones culturales mexicanas..."
            },
            {
                "t11pregunta": "Con la llegada del desarrollo tecnológico y las comunicaciones..."
            },
            {
                "t11pregunta": "Las tradiciones y costumbres de la sociedad mexicana tienen..."
            },
            {
                "t11pregunta": "Los jóvenes se han identificado con ciertos grupos..."
            },
            {
                "t11pregunta": "Una de las grandes influencias culturales actuales en México..."
            },
            {
                "t11pregunta": "La literatura mexicana..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona con líneas las columnas según corresponda."
        }
    },//11
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E1985\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1968\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1995\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1975\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ESolidaridad\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EParticipación democrática\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOpulencia\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EDesastres naturales\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECarencia de infraestructura\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECarencia de biodiversidad\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EProtección de sus derechos.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECreación de espacios públicos para la convivencia infantil.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECreación de nuevas tecnologías.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>El terremoto de la Ciudad de México ocurrió en el año de:",
         "Es una de las características del pueblo mexicano:",
         "México es un país que geográficamente está expuesto a:",
         "Es una de las obligaciones de la niñez mexicana de acuerdo al contenido de esta lección."]
      }
   }
];