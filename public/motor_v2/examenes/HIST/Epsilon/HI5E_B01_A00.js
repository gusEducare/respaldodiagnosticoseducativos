json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los acontecimientos del pasado no repercuten en el presente. ",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Malinche fue una mujer que viajó con Cortés desde Europa y le ayudó como intérprete. ",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El malinchismo es una palabra que designa la preferencia que sienten los mexicanos ante los productos extranjeros.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Xenofobia: significa odiar, despreciar o temer a personas o ideas por el simple hecho de venir de fuera.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la colonia las decisiones del reino se tomaban en la Nueva España.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona Falso (F) o Verdadero (V) según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },

    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Cacicazgo ",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Latifundios",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pocas personas son dueñas de las mejores tierras ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los campesinos eran obligados a trabajar en la hacienda ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un hombre decide el destino de todos a su alrededor",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se impone por fuerza, control, miedo...",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los trabajadores deben comprar en la misma tienda",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Lee con atención las siguientes afirmaciones y marca si se refieren al cacicazgo o a los latifundios.</p>",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },

    //3
     {
        "respuestas": [
            {
                "t13respuesta": "contemporáneas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "culturales",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "costumbres",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "mestizaje",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "guerras",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "corrupción",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Antiguas",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "sorprendentes",
                "t17correcta": "9"            },
            {
                "t13respuesta": "iglesias",
                "t17correcta": "9"            },
                 {
                "t13respuesta": "Tenencia de la tierra",
                "t17correcta": "9"            },
        ],
        "preguntas": [
            {
                "t11pregunta": "El pasado es un referente de todas las sociedades "
            },
            {
                "t11pregunta": ", los eventos acontecidos en el pasado determinan rasgos "
            },
            {
                "t11pregunta": " como "
            },
            {
                "t11pregunta": ", creencias, rasgos políticos y sociales, etcétera. Algunos de estos rasgos pueden considerarse positivos, como la diversidad lingüística y el proceso de "
            },
            {
                "t11pregunta": " en el caso de México. Algunos otros son negativos como la destrucción de edificios que han traído las "
            },
            {
                "t11pregunta": ", o el arraigo de la "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Estudia las sociedades a través del tiempo",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "No solo describe los hechos, sino explica sus causas y efectos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La historia nos permite ser conscientes de nuestro presente",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Estudia el pasado",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Estudia solo los sucesos que son producto de la actividad de los seres humanos.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Estudia las erupciones volcánicas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Estudia al hombre como ser cultural",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Estudia los restos humanos y sitios arqueológicos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>Son características de la historia "],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "respuestasMultiples": true,
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El estudio de las elecciones presidenciales del 2018 en México, compete a la historia",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estudiar las consecuencias que generó la erupción de un volcán en alguna sociedad de la antigüedad, compete a la historia",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El estudio de la historia nos permite predecir qué pasaría si estallara una guerra mundial",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estudiar las aguas oceánicas y continentales, compete a la historia",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El estudio de la historia nos permite conocer las motivaciones de los criollos para luchar por la independencia de México",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona Falso (F) o Verdadero (V) según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Identidad nacional",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Folclor",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La historia nacional",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Los héroes de independencia",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La religión católica",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La comida española",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La forma de vestir",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Las tradiciones funerarias",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "culto",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "emancipación",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "hablar ",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "tránsito ",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "territorio",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "paisaje",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "siglo",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "régimen político",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Mestizaje ",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Folclor",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Conocimiento",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Tratado",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Elemento que logra cohesionar a todos los mexicanos bajo un sentimiento de patriotismo",
                "Es uno de los elementos culturales impuesto por España a las tierras de la Nueva España",
                "La libertad de &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 permite garantizar la libertad de los mexicanos y establece que las creencias no pueden imponerse entre la población.",
                "Un elemento que identifica a los mexicanos es que todos nacieron en un mismo",
                "En la Nueva España el &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 permitió el intercambio de rasgos culturales entre los Indígenas, los españoles y los negros.",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser mexicano significa que todos pensamos igual",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser mexicano significa creer en un mismo dios",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser mexicano es identificarse con la forma de ser de los mexicanos ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los mexicanos hemos demostrado a lo largo de la historia que tenemos un sentido de unidad",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser mexicano es reconocernos en historias distintas sin rumbo",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona Falso (F) o Verdadero (V) según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //8 relaciona lineas
     {
        "respuestas": [
            {
                "t13respuesta": "Capacidad de decidir por sí mismo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Compartir un mismo territorio, costumbres, tradiciones, etcétera, a partir de la identidad nacional.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cambio de técnicas y procesos de producción hacia el desarrollo capitalista durante el Porfiriato. ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Grupo social que inicia el proceso de Independencia en México. Estaban descontentos con la corona española.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Leyes diseñadas para aumentar los privilegios de los reyes de España.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "soberanía"
            },
            {
                "t11pregunta": "nacionalismo"
            },
            {
                "t11pregunta": "modernización"
            },
            {
                "t11pregunta": "criollos"
            },
            {
                "t11pregunta": "Reformas Borbónicas"
            },
           
        ],
        "pregunta": {
              "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las líneas según corresponde."

        }
    }, 
    //9 Matris editable
     {
        "respuestas": [
            {
                "t13respuesta": "Movimiento<br>de Independencia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Primer Imperio Mexicano",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Constitución de 1824",
                "t17correcta": "2"
            }, {
                "t13respuesta": "Periodo<br>de<br>Santa<br>Ana",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Leyes de Reforma",
                "t17correcta": "4"
            },
        ],
        
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Promoción de un<br> país laico, leyes<br> en contra de la<br>Iglesia.",
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Guerra<br>México-Texas.",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Prohibió cualquier<br>religión ajena al<br>Catolicismo.",
        
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Base de la<br>Constitución<br>de 1857.",
           
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Agustín I",
                "correcta"  : "1"
            },
             {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Concluyó con los<br>tratados de<br>Córdoba.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada etapa de los primeros 100 años de vida independiente en México.",
            "descripcion": "Enunciado",   
           "variante": "editable",
            "anchoColumnaPreguntas": 20,
            "evaluable"  : true
        }        
    },
    //10 arrastra corta
    {
        "respuestas": [
            {
                "t13respuesta": "independiente",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "gobierno",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "constituciones",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1824",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "1857",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Ignacio Comonfort",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "eclesiásticos",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "civiles",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "Porfirio Díaz",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "conservadores",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "normales",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "presidentes",
                "t17correcta": "12"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Los primeros cien años de la vida <\/p>"
            },
            {
                "t11pregunta": "<p> de México, muestran la existencia de distintos tipos de formas de <\/p>"
            },
            {
                "t11pregunta": "<p>, y la creación de varias <\/p>"
            },
            {
                "t11pregunta": "<p>. La constitución de <\/p>"
            },
            {
                "t11pregunta": "<p> tuvo cortes liberales y conservadores, mientras que la constitución de <\/p>"
            },
            {
                "t11pregunta": "<p> fue de un corte liberal, promulgada por <\/p>"
            },
            {
                "t11pregunta": "<p>, su vocación principal fue lograr la separación entre los asuntos <\/p>"
            },
            {
                "t11pregunta": "<p> y los asuntos <\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //11 Respuesta múltiple
    {  
      "respuestas":[
         {  
            "t13respuesta": "Estar ubicados por debajo de los españoles en la pirámide social.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "No se les permitía llegar a los puestos más altos de la administración del gobierno de la Nueva España.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Limitación a su desarrollo económico por parte de los españoles.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Apoyarse de los negros e indígenas para generar relaciones comerciales.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Se dieron cuenta que estaban vigilados y eran perseguidos  por el poder de la corona española.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Conocieron el esplendor que tuvieron los indígenas en el territorio mexicano",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
        
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona todas las respuestas correctas. <br><br>Son inconformidades de los criollos ante la corona española:",
],
         "preguntasMultiples": true,
      
      }
   },
//12 Falso verdadero

{
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las ideas de la Ilustración buscaron destituir a las dictaduras europeas.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una de las obras más destacadas de la Ilustración fue la Enciclopedia de Diderot.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las ideas de la Ilustración viajaron a América y fomentaron las ideas independentistas.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los indígenas fueron el grupo social que más se valió de las ideas ilustradas para generar la independencia de México.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los criollos humanistas pretendían hacer a todas las personas iguales ante la ley.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los países de América Latina se vieron influenciados por las ideas del renacimiento para inspirar su independencia.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "España fue invadida por el emperador francés Napoleón Bonaparte y esta es una de las causas externas de las independencias americanas.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona Falso (F) o Verdadero (V) según corresponde.",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //13 Matris arastra corta
    {
        "respuestas": [
            {
                "t13respuesta": "Injusticias de los <br>españoles sobre los criollos.",
                "t17correcta": "1,3"
            },
            {
                "t13respuesta": "Invasión de Napoleón<br>Bonaparte a España",
                "t17correcta": "2,4,5"
            },
            {
                "t13respuesta": "Nacionalismo criollo",
                "t17correcta": "3,1"
            },
            {
                "t13respuesta": "Reformas Borbónicas",
                "t17correcta": "2,4,5,"
            },
          
            {
                "t13respuesta": "Las ideas de la Ilustración",
                "t17correcta": "2,4,5"
            },
            
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td style='width:150px'>Causas internas </td><td style='width:150px'>Causas externas</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                  <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                    <td>"
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },

  {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>"
            },
              
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Coloca los enunciados según corresponda a causas internas o externas de la independencia de México. ",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": true,
            "ocultaPuntoFinal": true
        }
    },
];
