json=[
  //1
  {
        "respuestas":[
           {
              "t13respuesta":"Manuel González",
              "t17correcta":"1",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":"Ignacio Comonfort",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":"Francisco I. Madero",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":"Sebastián Lerdo de Tejada",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":"1888, los cambios realizados en la constitución.",
              "t17correcta":"1",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":"1885, la facultad otorgada por el pueblo.",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":"1880, razón de su potestad política.",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":"1887, la permisibilidad de la cámara de senadores.",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":"Porque el gobierno tomó tintes dictatoriales.",
              "t17correcta":"1",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"Por las injusticias sufridas en las clases altas.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"Por motivo de la celebración del centenario de la independencia.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"Por el gobierno nepotista de Díaz y su favoritismo a algunos grupos.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           }
        ],
        "pregunta":{
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["<strong>Selecciona la respuesta correcta.</strong><br><br> En el periodo que va de 1880-1884 el país fue gobernado, con permiso de Porfirio Díaz, por:"
            ,"¿A partir de qué año Díaz retomó  la presidencia y qué le permitió mantener su mandato hasta el año de 1911?"
            ,"¿Por qué la ciudadanía y el pueblo se levantaron en armas contra el régimen de Díaz?"],
           "preguntasMultiples": true
        }
     },
     //2
     {
        "respuestas": [
            {
                "t13respuesta": "<p>siglo XIX</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Porfiriato</p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>cuatro décadas</p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Porfirio Díaz,</p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Revolución Mexicana</p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>1910</p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Emiliano Zapata</p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Pancho Villa</p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>Siglo XVII</p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>Tres décadas</p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>Independencia de México</p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>Venustiano Carranza</p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>1917</p>",
                "t17correcta": "13"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Durante el final del &nbsp</p>"
            },
            {
                "t11pregunta": "<p>&nbsp y principios del siglo XX se desarrolló el</p>"
            },
            {
                "t11pregunta": "<p>.&nbsp Éste periodo, cuya duración fue de casi &nbsp</p>"
            },
            {
                "t11pregunta": "<p>, destacó porque el militar oaxaqueño </p>"
            },
            {
                "t11pregunta": "<p>&nbsp asumió el gobierno del país y lo llevó a un desarrollo económico e industrial como no se había dado antes. Sin embargo, las calidad de vida para las personas marginadas era muy precaria. La tierra estaba dividida en latifundios en los que trabajaban campesinos al servicio de los capataces, entre otros problemas sociales. Todo ello generó un descontento general en el país que desembocó en la </p>"
            },
            {
                "t11pregunta": "<p>,&nbsp en el año de &nbsp</p>"
            },
            {
                "t11pregunta": "<p>. Dicho movimiento se mantuvo vivo por caudillos como&nbsp</p>"
            },
            {
                "t11pregunta": "<p>,&nbspen el sur del país y Doroteo Arango, mejor conocido como&nbsp</p>"
            },
            {
                "t11pregunta": "<p>, en el norte.</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<strong>Arrastra las palabras que completen el párrafo.</strong>",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Sebastián Lerdo de Tejada.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "militares y civiles",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se llevó a cabo en 1976 en contra de la reelección de Lerdo de Tejada.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "la lucha contra Santa Anna en 1854.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Benito Juárez.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "En 1876 Porfirio Díaz se levantó en armas contra la relección de:"
            },
            {
                "t11pregunta": "Después del triunfo de los liberales sobre el segundo imperio mexicano, estos se dividieron en:"
            },
            {
                "t11pregunta": "El Plan de Tuxtepec:"
            },
            {
                "t11pregunta": "Porfirio Díaz había servido al ejército liberal desde:"
            },
            {
                "t11pregunta": "Con su fama entre los liberales, compitió por la presidencia en 1867, pero fue derrotado por:"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"<strong>Relaciona las columnas según corresponda. </strong><br>"
        }
    },
    //4
    {
      "respuestas":[
         {
            "t13respuesta":"La persecución de bandidos y presos políticos.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"La reconciliación de los principales grupos políticos en México.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"La arrogancia de la iglesia y los conservadores.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"La aparición de epidemias e invasores extranjeros.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"La creación de un sistema ferroviario para el uso público",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Atraer la inversión extranjera en el país",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"La integración de México a el mercado asiático",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Generar un clima político amigable para todo mundo",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Comienza a funcionar el Banco Nacional de México y la Lotería de la Academia San Carlos adquiere el nombre Lotería Nacional de México",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"Se firma el tratado Herrera-Mariscal, el cual estableces los límites entre México y Guatemala.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"Se funda el Colegio Hispano de Letras Mexicanas",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"La Escuela Preparatoria Técnica y de Profesiones comienza su labor",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "tresColumnas": true,
         "t11pregunta":["<strong>Selecciona todas las respuestas correctas para cada pregunta.</strong><br><br> Cuando Porfirio Díaz tomó el cargo de presidente por primera vez ¿cuáles fueron los principales problemas que enfrentó?",
         "¿Qué fines perseguía Díaz con la pacificación del país?",
         "¿Qué hechos memorables ocurren en 1882?"],
         "preguntasMultiples": true
      }
   },
   //5
   {
       "respuestas": [
           {
               "t13respuesta": "El capital extranjero.",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "Fue posible gracias a las inversiones de países como EU, Francia, Inglaterra y Alemania.",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "El gobierno otorgó concesiones de construcción de redes ferroviarias a empresas extranjeras.",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "19,000 km que conectaban la capital con la frontera y con los puertos del Pacífico y el Golfo.",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "El gobierno, quien además lo utilizaba para transportar al ejército en caso de que hubiera levantamientos o rebeliones.",
               "t17correcta": "5"
           },
           {
               "t13respuesta": "Se integraran el telégrafo y el teléfono para comunicar las principales ciudades.",
               "t17correcta": "6"
           }
       ],
       "preguntas": [
           {
               "t11pregunta": "Para Díaz, para el desarrollo del país era  fundamental:"
           },
           {
               "t11pregunta": "El desarrollo de la minería, el ferrocarril, la banca y las industrias petrolera, eléctrica y textil: "
           },
           {
               "t11pregunta": "Durante el porfiriato había interés de comunicar a la mayor parte del país, por lo que:"
           },
           {
               "t11pregunta": "La cantidad de kilómetros de vías férreas en 1910 ascendía a:"
           },
           {
               "t11pregunta": "El control del ferrocarril estaba a cargo de:"
           },
           {
               "t11pregunta": "El desarrollo y establecimiento del ferrocarril como medio de transporte innovador permitió además que:"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "12",
           "t11pregunta":"<strong>Relaciona las columnas según corresponda.</strong>"
       }
   },
   //6
   {
     "respuestas":[
        {
           "t13respuesta":"La mayor parte de ellos trabajaban por jornadas de más de 12 horas.",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"A cambio de su trabajo recibían un sueldo miserable.",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"Eran sobajados y privados de derechos.",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"La mayoría no gozaba de prestaciones sin importar el tiempo que había trabajado.",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"Lugares donde sólo se podía comprar con vales entregados por el patrón.",
           "t17correcta":"1",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Lugares donde los trabajadores solo se endeudaban y heredaban la deuda a sus hijos.",
           "t17correcta":"1",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Lugares de intercambio y trueque por productos útiles y de lujo.",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Lugares donde se vendía alcohol y otras sustancias dañinas.",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Lugares donde los jornaleros y campesinos laboraban obligados al haber sido despojados de sus tierras.",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":"Extensiones de tierra pertenecientes a los hacendados, adquiridos por medio de tratos de compraventa con el gobierno.",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":"Lugares donde se sembraba lechugas, coles y otros vegetales.",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":"Lugares de explotación de trabajadores de manera similar a los esclavos, pero un sueldo alto.",
           "t17correcta":"0",
           "numeroPregunta":"2"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"2",
        "tresColumnas": true,
        "t11pregunta":["<strong>Selecciona todas las respuestas correctas para cada pregunta.</strong><br><br> Durante el porfiriato los campesinos, obreros, jornaleros y mineros estaban inconformes debido a que:",
                      "¿Qué eran las tiendas de raya?",
                      "¿Qué eran los latifundios?"],
        "preguntasMultiples": true
     }
  },
  //7
  {
       "respuestas": [
           {
               "t13respuesta": "Falso",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Verdadero",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Los obreros que laboraban en fábricas debían cubrir los daños que pudieran causar al mobiliario o herramientas.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Los obreros gozaban de prestaciones y seguro médico marcados por el gobierno.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En algunas minas, como lo fue la Cananea  (Sonora), se pagaba más a los mineros norteamericanos, mientras que los mexicanos ganaban un salario miserable.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las primeras huelgas de los trabajadores para exigir mejores condiciones laborales terminaron en multas, despido masivo, arrestos, represión y muertes.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En los últimos años del porfiriato la gente se encontraba más tranquila, sobre todo los obreros y los campesinos.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En 1908 Porfirio Díaz afirmó que ya no iría a la contienda por la presidencia en las elecciones que se celebrarían en 1910.",
               "correcta"  : "1"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<p><strong>Elige Falso o Verdadero según corresponda.</strong></p>",
           "descripcion":"Reactivo",
           "evaluable":false,
           "evidencio": false
       }
   },
   //8
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Francisco I. Madero era un campesino, nacido en el seno de una familia humilde.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 1909 surge el Partido Nacional Antirreeleccionista fundado por Madero y otros opositores del régimen porfirista.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando el antirreeleccionismo fue ganando apoyo popular, Díaz mandó felicitar a Madero por considerarlo una precursor de la democracia.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Después de que Madero huyó a Estado Unidos proclamó el Plan de San Luis, en el cual desconocía a Díaz como presidente y convocaba a la Revolución el 20 de noviembre de 1910.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando Madero asumió la presidencia, la inestabilidad política y los partidarios del porfirismo se habían acabado.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lo que provocó que los caudillos de la revolución tomarán nuevamente las armas, fue el golpe de estado hecho por Victoriano Huerta, asesinando a Madero y a Pino Suárez.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><strong>Elige Falso o Verdadero según corresponda.</strong></p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },
    //9
    {
      "respuestas":[
         {
            "t13respuesta":"Constitución de 1917",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"Carta Magna de 1824",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"Garantías Individuales y declaración universal de los derechos humanos",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"Constitución de 1858",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"Villistas y Zapatistas",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Delahuertistas y Porfiristas",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Porfiristas y Maderistas",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Clase burguesa",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"La educación será impartida por el estado, además será libre, laica y gratuita.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"El poder adquisitivo es propio de cada mexicano y puede ejercerse con toda libertad.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"Nadie puede ser privado de su libertad por transitar libremente por el territorio nacional.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"La religión del estado es una, única e inamovible de su puesto.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"La propiedad de las tierras y aguas que estén dentro de los límites del territorio nacional pertenecen a la nación y sólo ella puede transmitir el dominio de ellas",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta":"La propiedad intelectual corresponde al autor de la obra, escrito, pieza artística, objeto o producto y es, además, intransferible.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta":"La asociación pacífica está permitida en todo el territorio nacional, de manera que nadie puede impedirla.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta":"La historia nacional será dictada sólo por el gobierno y los representantes de este que mejor cuiden sus intereses.",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta":"Las leyes para regular las relaciones entre los trabajadores y los patrones; además, se establecen condiciones de todo contrato laboral, como la jornada laboral de 8 horas.",
            "t17correcta":"1",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta":"Las leyes de fianza en los presos, se establecerán de acuerdo a la falta cometida y el perjuicio causado a los afectados.",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta":"El gobierno no puede, ni tiene la capacidad de interferir en conflictos armados internacionales o fuera del territorio nacional sin permiso del congreso.",
            "t17correcta":"0",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta":"La salud es un derecho de todo mexicano y por tanto debe ser provista por el estado sin costo alguno.",
            "t17correcta":"0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["<strong>Selecciona la respuesta correcta.</strong><br><br> Es un documento que se redacta en la primera mitad del siglo XX, ante la necesidad de plantear un marco legal en el cual las personas tengan igualdad de condiciones económicas.",
                        "En la Constitución de 1917 se establecen las principales demandas de grupos revolucionarios como:",
                        "El artículo 3° de la constitución establece que:",
                        "El artículo 27 de la constitución dicta que:",
                        "El artículo 123 establece que:"],
       "preguntasMultiples":true
      }
   },
   //10
   {
        "respuestas": [
            {
                "t13respuesta": "El género musical conocido como corrido.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se le atribuye la composición del famoso corrido “Adelita”.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Expresaron mitos y leyendas de algunos de las caudillos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Las injusticias, los abusos y las ideas que prevalecían en la época.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Las Adelitas.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Cocinaban para las tropas, curaban a los heridos, atendían a los enfermos, cuidaban a los niños y tomaban las armas.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Tenía un gran bigote, sombrero, caballo y arma. Sin temor a la muerte y leal a sus ideales y palabra.",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Fue un medio de difusión de los acontecimientos de la Revolución."
            },
            {
                "t11pregunta": "Al sargento Antonio Gil del Río Armenta:"
            },
            {
                "t11pregunta": "Durante la Revolución las manifestaciones culturales:"
            },
            {
                "t11pregunta": "La manifestaciones culturales sirvieron para expresar:"
            },
            {
                "t11pregunta": "Las mujeres que formaban parte de la tropa de Villa eran conocidas como:"
            },
            {
                "t11pregunta": "Durante la Revolución las mujeres:"
            },
            {
                "t11pregunta": "La imagen popular del revolucionario era la de un hombre que:"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"<strong>Relaciona las columnas según corresponda.</strong>"
        }
    },
    //11
    {
         "respuestas": [
             {
                 "t13respuesta": "Se consolidó en las últimas décadas del siglo XIX y fue iniciado por las leyes liberales.",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "Impedir que los campesinos tuviesen alternativas al comprar productos de consumo diario.",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "Eran víctimas de las injusticias y abusos por parte de los hacendados y latifundistas.",
                 "t17correcta": "3"
             },
             {
                 "t13respuesta": "Los estratos sociales más bajos aún se encontraban en pobreza extrema.",
                 "t17correcta": "4"
             },
             {
                 "t13respuesta": "Los mineros ingleses.",
                 "t17correcta": "5"
             }
         ],
         "preguntas": [
             {
                 "t11pregunta": "El despojo de tierras a campesinos e indígenas:"
             },
             {
                 "t11pregunta": "Las tiendas de raya estaban ideadas para:"
             },
             {
                 "t11pregunta": "Los campesinos en el norte y en el sur se unieron a la Revolución porque:"
             },
             {
                 "t11pregunta": "A pesar de todos lo avances tecnológicos y el progreso alcanzado por el porfiriato:"
             },
             {
                 "t11pregunta": "El futbol fue traído a México por:"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "12",
             "t11pregunta":"<strong>Relaciona las columnas según corresponda.</strong>"
         }
     }
];
