json = [
        {
                "respuestas": [
                        {
                                "t13respuesta": "Conservar el poder",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Expandir el territorio",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Controlar el territorio",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Motivos interétnicos",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Controlar la flora y fauna de un país",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "El ataque terrorista a las torres gemelas",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Conservar los derechos humanos y la paz",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        }, ///////////////////////////////////////////////
                        {
                                "t13respuesta": "Conflictos interétnicos",
                                "t17correcta": "1",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Control de los recursos y del territorio",
                                "t17correcta": "1",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Oposición política al sistema de gobierno",
                                "t17correcta": "1",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Ensayos de bombas nucleares",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Equidad en la salud mental de todos los pobladores",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Financiamientos extranjero de tanques de guerra",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "2",
                        "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta. <br><br>Son las causas de una guerra internacional", "Son causas de conflictos locales"],
                        "t11instruccion": "",
                        "preguntasMultiples": true
                }
        },
        //2
        {
                "respuestas": [
                        {
                                "t13respuesta": "África",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "América",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Europa",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Oceanía",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        }, ////////////////////////////
                        {
                                "t13respuesta": "La intervención y financiamiento extranjero del conflicto",
                                "t17correcta": "1",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "La forma tan oprimente del control de los recursos",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "La guerra de Siria que fue financiada por Estados Unidos",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Los conflictos religiosos",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        }, //////////////////////////////////
                        {
                                "t13respuesta": "Desplazados",
                                "t17correcta": "1",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "Dañados",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "Migrantes",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "Viajeros",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        }, ///////////////////////////////////////

                        {
                                "t13respuesta": "Crisis económicas y políticas",
                                "t17correcta": "1",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "Inseguridad y robos",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "Búsqueda de nuevos territorios por países",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "Cambios en las dinámicas geofísicas de la tierra",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "1",
                        "t11pregunta": ["Selecciona la respuesta correcta según corresponde. <br><br>\n\
 Continente que presenta la mayor cantidad de conflictos territoriales\n\
 ", "En algunos conflictos locales se han internacionalizado, principalmente por:\n\
 ", "Los conflictos internos provocan migraciones internas, \n\
 lo cual genera que la población se convierta en:", "Las consecuencias de los conflictos entre los estados provocan:"],
                        "t11instruccion": "",
                        "preguntasMultiples": true
                }
        },
        //3
        {
                "respuestas": [
                        {
                                "t13respuesta": "Cuando existe el peligro latente de un enfrentamiento abierto",
                                "t17correcta": "1",
                        },
                        {
                                "t13respuesta": "Cuando el conflicto estalla",
                                "t17correcta": "2",
                        },
                        {
                                "t13respuesta": "Cuando los grupos enfrentados se preparan para la guerra",
                                "t17correcta": "3",
                        },
                        {
                                "t13respuesta": "Resolución pacífica de algún conflicto",
                                "t17correcta": "4",
                        },
                        {
                                "t13respuesta": "Conflicto interno de un país, en el las partes en conflicto se infiltran en la sociedad civil",
                                "t17correcta": "5",
                        },
                ],
                "preguntas": [
                        {
                                "t11pregunta": "Crisis"
                        },
                        {
                                "t11pregunta": "Guerra"
                        },
                        {
                                "t11pregunta": "Crisis severa"
                        },
                        {
                                "t11pregunta": "Diálogo y negociación"
                        },
                        {
                                "t11pregunta": "Guerrilla"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "12",
                        "t11pregunta": "Relaciona las columnas según corresponda.",
                        "t11instruccion": "",
                        "altoImagen": "100px",
                        "anchoImagen": "200px"
                }
        },

        //4

        {
                "respuestas": [
                        {
                                "t13respuesta": "histórico",
                                "t17correcta": "1"
                        },
                        {
                                "t13respuesta": "Europa",
                                "t17correcta": "2"
                        },
                        {
                                "t13respuesta": "político",
                                "t17correcta": "3"
                        },
                        {
                                "t13respuesta": "Ilustración",
                                "t17correcta": "4"
                        },
                        {
                                "t13respuesta": "ilustradas",
                                "t17correcta": "5"
                        },
                        {
                                "t13respuesta": "monarquías",
                                "t17correcta": "6"
                        },
                        {
                                "t13respuesta": "conocimiento",
                                "t17correcta": "7"
                        },
                        {
                                "t13respuesta": "razón",
                                "t17correcta": "8"
                        },
                        {
                                "t13respuesta": "América",
                                "t17correcta": "9"
                        },
                        {
                                "t13respuesta": "revolucionarias",
                                "t17correcta": "10"
                        },
                        {
                                "t13respuesta": "Casas reinantes",
                                "t17correcta": "11"
                        },
                        {
                                "t13respuesta": "sabidurías",
                                "t17correcta": "12"
                        },
                ],
                "preguntas": [
                        {
                                "t11pregunta": "El periodo "
                        },
                        {
                                "t11pregunta": " en el cual existieron movimientos intelectuales en "
                        },
                        {
                                "t11pregunta": " que cuestionaron el orden social y  "
                        },
                        {
                                "t11pregunta": " del siglo XVIII se denomina: "
                        },
                        {
                                "t11pregunta": ". Las ideas "
                        },
                        {
                                "t11pregunta": " impulsaron revoluciones que abolieron las "
                        },
                        {
                                "t11pregunta": ", y generaron un orden político. Los avances en ciencia y tecnología fueron trascendentales, para lograr el "
                        },
                        {
                                "t11pregunta": " y tener la vida que conocemos en la actualidad, una de las características más importantes es que buscó anteponer la "
                        },
                        {
                                "t11pregunta": " a la ignorancia.<br>"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "8",
                        "t11pregunta": "Arrastra las palabras que completen el párrafo.",
                        "t11instruccion": "",
                        "respuestasLargas": true,
                        "pintaUltimaCaja": false,
                        "contieneDistractores": true
                }
        },
        //5
        {
                "respuestas": [
                        {
                                "t13respuesta": "F",
                                "t17correcta": "0",
                        },
                        {
                                "t13respuesta": "V",
                                "t17correcta": "1",
                        },


                ], "preguntas": [
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "La Ilustración tuvo su mayor esplendor en Francia",
                                "correcta": "1",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Uno de los postulados de las ideas ilustradas era suponer que todos los hombres debían comportarse distinto según su nivel social.",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "La enciclopedia fue una obra que condensó los conocimientos industriales de la época.",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "La Academia Francesa fue un espacio dedicado al arte y a la ciencia",
                                "correcta": "1",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "El espíritu de las leyes fue escrito por Montesquieu",
                                "correcta": "1",
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
                        "t11instruccion": "",
                        "descripcion": "Reactivo",
                        "variante": "editable",
                        "anchoColumnaPreguntas": 60,
                        "evaluable": true
                }
        },
        //6
        {
                "respuestas": [
                        {
                                "t13respuesta": "Ilustración",
                                "t17correcta": "0",
                        },
                        {
                                "t13respuesta": "Trece colonias",
                                "t17correcta": "1",
                        },
                        {
                                "t13respuesta": "Revolución industrial",
                                "t17correcta": "2",
                        },
                        {
                                "t13respuesta": "Revolución Francesa",
                                "t17correcta": "0",
                        }, {
                                "t13respuesta": "Siglo XIX",
                                "t17correcta": "0",
                        },
                ], "preguntas": [
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Espíritu de las leyes",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Maquinas de combustión interna",
                                "correcta": "2",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Liderados por Samuel Adams",
                                "correcta": "1",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Libertad, Igualdad y fraternidad",
                                "correcta": "3",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "El contrato social",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Creación de sindicatos",
                                "correcta": "4",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Karl Marx",
                                "correcta": "4",
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada época histórica.</p>",
                        "t11instruccion": "",
                        "descripcion": "Época histórica",
                        "variante": "editable",
                        "anchoColumnaPreguntas": 30,
                        "evaluable": true
                }
        },
        //7
        {
                "respuestas": [
                        {
                                "t13respuesta": "El clero, la nobleza y la clase trabajadora",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Los ricos y los pobres",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Los caciques, reyes y condes",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "La burguesía, la nobleza y el clero",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        }, ///////////////////////////////////////////
                        {
                                "t13respuesta": "La Bastilla",
                                "t17correcta": "1",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "La Torre Eiffel",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Marsella",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "El Parque de los Principes",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        }, ///////////////////////////////////////////////
                        {
                                "t13respuesta": "La locomotora",
                                "t17correcta": "1",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "La productividad",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "La educación formal",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "La ciencia",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        }, /////////////////////////////////////////////////
                        {
                                "t13respuesta": "Las revoluciones liberales",
                                "t17correcta": "1",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "Las revoluciones armadas",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "Nuevas formas de gobierno",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "Las ideas de la revolución industrial",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        }, /////////////////////////////////////////////////
                        {
                                "t13respuesta": "discriminación",
                                "t17correcta": "1",
                                "numeroPregunta": "4"
                        },
                        {
                                "t13respuesta": "ayuda",
                                "t17correcta": "0",
                                "numeroPregunta": "4"
                        },
                        {
                                "t13respuesta": "solidaridad",
                                "t17correcta": "0",
                                "numeroPregunta": "4"
                        },
                        {
                                "t13respuesta": "pérdida",
                                "t17correcta": "0",
                                "numeroPregunta": "4"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "1",
                        "t11pregunta": ["Selecciona la respuesta correcta. <br><br>\n\
 En Francia del siglo XVIII existían tres clases sociales, y las diferencias entre estas propiciaron la Revolución Francesa,\n\
  ¿Cuales eran estas clases?", "Sitio representativo del poder político monárquico en Francia, que fue tomado por los grupos\n\
   revolucionarios.", "Durante la Revolución Industrial en los inicios del siglo XIX, cambió los métodos de producción y\n\
    desplazó una gran cantidad de mano de obra humana.", "Durante el siglo XVIII y XIX la geografía mundial cambió\n\
     porque los países de América y Europa fueron influenciados por ", "Las ideas liberales llegaron hasta América y fueron\n\
      claves para lograr la independencia de los estados americanos,\n\
       pues existía una gran <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> hacia las poblaciones originarias. "],
                        "t11instruccion": "",
                        "preguntasMultiples": true
                }
        },
        //8
        {
                "respuestas": [
                        {
                                "t13respuesta": "Clase social formada por comerciantes, artesanos, entre otros.",
                                "t17correcta": "1",
                        },
                        {
                                "t13respuesta": "Siglo de las luces",
                                "t17correcta": "2",
                        },
                        {
                                "t13respuesta": "Movimiento de transformación social y política, que<br>se sustentó en el respeto a los derechos civiles.",
                                "t17correcta": "3",
                        },
                        {
                                "t13respuesta": "Se caracteriza por emplear medios mecánicos para generar producción.",
                                "t17correcta": "4",
                        },
                        {
                                "t13respuesta": "Doctrina o acción de gobernar otro estado por la fuerza militar.",
                                "t17correcta": "5",
                        },
                ],
                "preguntas": [
                        {
                                "t11pregunta": "Burguesía"
                        },
                        {
                                "t11pregunta": "Ilustración"
                        },
                        {
                                "t11pregunta": "Liberalismo"
                        },
                        {
                                "t11pregunta": "Industrializaión"
                        },
                        {
                                "t11pregunta": "Imperialismo"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "12",
                        "t11pregunta": "Relaciona las columnas según corresponda.",
                        "t11instruccion": "",
                        "altoImagen": "100px",
                        "anchoImagen": "200px"
                }
        },
        //9
        {
                "respuestas": [
                        {
                                "t13respuesta": "Pensamiento y filosofía del humanismo.",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "El privilegio de la razón como base del conocimiento, racionalismo.",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Planteó y justificó acabar con el sistema monárquico.",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Pretendía dar paso a un estado democrático.",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Pretendía otorgar más derechos a las monarquías.",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "El oscurantismo.",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "El renacimiento como filosofía primordial para transformar el arte.",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Corriente teocéntrica que asegura que el mundo gira en torno a Dios.",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "2",
                        "t11pregunta": ["Selecciona todas las respuestas correctas.  <br><br>Son características de la ilustración"],
                        "t11instruccion": "",
                        "preguntasMultiples": true
                }
        },
        //10
        {
                "respuestas": [
                        {
                                "t13respuesta": "F",
                                "t17correcta": "0",
                        },
                        {
                                "t13respuesta": "V",
                                "t17correcta": "1",
                        },
                ], "preguntas": [
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "John Locke planteó por primera vez en la historia de la humanidad una constitución como base del derecho natural.",
                                "correcta": "1",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "John Locke fue el principal exponente del movimiento de ilustración francés. ",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "La enciclopedia se propuso plasmar el conocimiento más relevante que la humanidad había adquirido en la edad media.",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Déspotas ilustrados: fueron los reyes que se jactaban de llamarse cultos, gobernaron para el siglo XVIII.",
                                "correcta": "1",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Los monarcas europeos extendieron sus territorios en base a la guerra y la invasión de territorios.",
                                "correcta": "1",
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
                        "t11instruccion": "",
                        "descripcion": "Reactivo",
                        "variante": "editable",
                        "anchoColumnaPreguntas": 60,
                        "evaluable": true
                }
        },

        //11
        {
                "respuestas": [
                        {
                                "t13respuesta": "\u003Cp\u003EIntereses económicos entre los reinos europeos, el control de las colonias americanas y la India. \u003C\/p\u003E",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EBúsqueda del control de África, Asia y Oceanía.\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELos intereses de la corona alemana y francesa sobre el territorio de América.\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELos altos impuestos que se generaron para transportar mercancía por el mar mediterráneo por parte de España. \u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ETratado de San Petersburgo\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ETratado de París y Tratado de Hubertusburgo\u003C\/p\u003E",
                                "t17correcta": "1",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ETratado de la Revolución Francesa\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EAcuerdo de paz París y España\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELa independencia de México\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELa guerra de los siete años\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELa independencia de las trece colonias\u003C\/p\u003E",
                                "t17correcta": "1",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EGuerra de San Petersburgo\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "2"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ESamuel Adams\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EJean Le Rond D` Alambert\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EJohn Locke\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EGeorge Washington\u003C\/p\u003E",
                                "t17correcta": "1",
                                "numeroPregunta": "3"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EUnieron el territorio colonial a la disponibilidad británica sobre el territorio\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "4"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EAbusaron del poder y generaron un régimen despótico en el naciente país.\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "4"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ERetomaron lo mejor  de la ilustración y sentaron las bases para la democracia moderna y el régimen republicano.\u003C\/p\u003E",
                                "t17correcta": "1",
                                "numeroPregunta": "4"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EBuscaron sentarla consolidación de un modelo neoliberal y económico en toda Europa.\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "4"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EBuscó la forma de generar  una política en contra de las leyes y los impuestos\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "5"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EGeneró una forma de organización económica en base al establecimiento de impuestos.\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "5"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ETuvo como propósito ir en contra de la monarquía y limitar los excesos de poder de los monarcas.\u003C\/p\u003E",
                                "t17correcta": "1",
                                "numeroPregunta": "5"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003EBuscó generar una constitución para Europa en la que se respetaran las propuestas de las reformas. \u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "5"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELa creación de tratados de paz a nivel mundial.\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "6"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELa creación de los derechos humanos, políticos y económicos , logrando el cambio de vasallos a ciudadanos. \u003C\/p\u003E",
                                "t17correcta": "1",
                                "numeroPregunta": "6"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELa formación de vasallos libres que tenían la libertad de expresión.\u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "6"
                        },
                        {
                                "t13respuesta": "\u003Cp\u003ELa división social de las capas económicas de las sociedades en América. \u003C\/p\u003E",
                                "t17correcta": "0",
                                "numeroPregunta": "6"
                        }


                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "1",
                        "t11pregunta": ["Selecciona la respuesta correcta según corresponde<br/><br/>La guerra de los siete años se dio a causa de:",
                                "La Guerra de los siete años terminó con: ",
                                "Proceso político y armado que generó la separación de las colonias británicas en América de la corona inglesa en Europa, en el S. XVIII. ",
                                "Organizó un ejército para afrontar el ataque de Inglaterra en las colonias americanas.",
                                "Los constituyentes norteamericanos: ",
                                "El liberalismo: ",
                                "Uno de los principales legados del liberalismo es: "],
                        "preguntasMultiples": true
                }
        },
        //12
        {
                "respuestas": [
                        {
                                "t13respuesta": "Primera Revolución Industrial",
                                "t17correcta": "0",
                        },
                        {
                                "t13respuesta": "Segunda Revolución Industrial",
                                "t17correcta": "1",
                        },
                ], "preguntas": [
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Máquinas de combustión interna",
                                "correcta": "1",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Metalurgia",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Descubrimiento del petróleo",
                                "correcta": "1",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Uso de carbón mineral como combustible",
                                "correcta": "0",
                        },
                        {
                                "c03id_tipo_pregunta": "13",
                                "t11pregunta": "Surge la industria automotriz",
                                "correcta": "1",
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada Revolución Industrial</p>",
                        "t11instruccion": "",
                        "descripcion": "Revolución ",
                        "variante": "editable",
                        "anchoColumnaPreguntas": 30,
                        "evaluable": true
                }
        },
        //13
        {
                "respuestas": [
                        {
                                "t13respuesta": "Discriminación de nativos.",
                                "t17correcta": "1,4"
                        },
                        {
                                "t13respuesta": "Explotación de recursos naturales.",
                                "t17correcta": "2,3"
                        },
                        {
                                "t13respuesta": "Salario mínimo a los nativos<br> por trabajar en empresas inglesas.",
                                "t17correcta": "3,2"
                        },
                        {
                                "t13respuesta": "Establecimiento de gobiernos<br> británicos en las colonias.",
                                "t17correcta": "4,1"
                        },
                ],
                "preguntas": [
                        {
                                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Control económico</td><td>Control político</td></tr><tr><td>Los ingleses impedían a los nativos<br> la creación de industrias propias.</td><td>"
                        },
                        {
                                "t11pregunta": "</td></tr><tr><td>"
                        },
                        {
                                "t11pregunta": "</td><td>Bases militares en cada<br> una de las colonias.</td></tr><tr><td>"
                        },
                        {
                                "t11pregunta": "</td><td>"
                        },
                        {
                                "t11pregunta": "</td></tr></table>"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "8",
                        "t11pregunta": "Arrastra cada elemento donde corresponda.",
                        "t11instruccion": "",
                        "respuestasLargas": true,
                        "pintaUltimaCaja": false,
                        "contieneDistractores": true
                }
        },
        //14
        {
                "respuestas": [
                        {
                                "t13respuesta": "Alemanas",
                                "t17correcta": "1",
                        },
                        {
                                "t13respuesta": "Serbias",
                                "t17correcta": "1",
                        },
                        {
                                "t13respuesta": "Eslovacas",
                                "t17correcta": "1",
                        },
                        {
                                "t13respuesta": "Polacas",
                                "t17correcta": "1",
                        },
                        {
                                "t13respuesta": "Españolas",
                                "t17correcta": "0",
                        },
                        {
                                "t13respuesta": "Francesas",
                                "t17correcta": "0",
                        },
                        {
                                "t13respuesta": "Mexicanas",
                                "t17correcta": "0",
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "2",
                        "t11pregunta": "Selecciona todas las respuestas correctas. <br><br>¿Qué poblaciones conformaban el Imperio austriaco?",
                        "t11instruccion": "",
                }
        },
        //15
        {
                "respuestas": [
                        {
                                "t13respuesta": "Ganadería",
                                "t17correcta": "1",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Textiles",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Agricultura",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Automotriz",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        },
                        {
                                "t13respuesta": "Minería",
                                "t17correcta": "0",
                                "numeroPregunta": "0"
                        }, //////////////////////////////////////////////
                        {
                                "t13respuesta": "Islam",
                                "t17correcta": "1",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Católicismo",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Budista",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Hebreo",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                        {
                                "t13respuesta": "Judía",
                                "t17correcta": "0",
                                "numeroPregunta": "1"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "1",
                        "t11pregunta": ["Selecciona la respuesta correcta. <br><br> ¿A qué se dedicaba el Imperio otomano?", "¿Qué religión adoptó el Imperio otomano?"],
                        "t11instruccion": "",
                        "preguntasMultiples": true
                }
        },
        //16
        {
                "respuestas": [
                        {
                                "t13respuesta": "Revolución francesa",
                                "t17correcta": "1"
                        },
                        {
                                "t13respuesta": "constitución",
                                "t17correcta": "2"
                        },
                        {
                                "t13respuesta": "convivencia",
                                "t17correcta": "3"
                        },
                        {
                                "t13respuesta": "monarquía",
                                "t17correcta": "4"
                        },
                        {
                                "t13respuesta": "Carta Magna",
                                "t17correcta": "5"
                        },
                        {
                                "t13respuesta": "país",
                                "t17correcta": "6"
                        },
                        {
                                "t13respuesta": "sufragio",
                                "t17correcta": "7"
                        },
                        {
                                "t13respuesta": "derecho",
                                "t17correcta": "8"
                        },
                        {
                                "t13respuesta": "elegidos",
                                "t17correcta": "9"
                        },
                        {
                                "t13respuesta": "hombres",
                                "t17correcta": "10"
                        },
                        {
                                "t13respuesta": "Estado",
                                "t17correcta": "0"
                        },
                        {
                                "t13respuesta": "armonía",
                                "t17correcta": "0"
                        },
                        {
                                "t13respuesta": "Alemania",
                                "t17correcta": "0"
                        },
                        {
                                "t13respuesta": "Control político",
                                "t17correcta": "0"
                        },
                        {
                                "t13respuesta": "Caída del imperio",
                                "t17correcta": "0"
                        },
                        {
                                "t13respuesta": "ancianos",
                                "t17correcta": "0"
                        },
                ],
                "preguntas": [
                        {
                                "t11pregunta": "Una de las principales herencias de la "
                        },
                        {
                                "t11pregunta": "  fue la creación de la "
                        },
                        {
                                "t11pregunta": "  de 1791, que define nuevas reglas de "
                        },
                        {
                                "t11pregunta": "  ante la caída de la "
                        },
                        {
                                "t11pregunta": " , es conocida como "
                        },
                        {
                                "t11pregunta": "  o Ley Fundamental, regula la vida política de un "
                        },
                        {
                                "t11pregunta": "  y establece que ninguna ley está por encima de la misma.<br>Otra de las principales herencias es el establecimiento del "
                        },
                        {
                                "t11pregunta": "  como "
                        },
                        {
                                "t11pregunta": "  fundamental del pueblo para elegir a sus gobernantes, los ciudadanos tuvieron el derecho de elegir y ser "
                        },
                        {
                                "t11pregunta": " , sin embargo, este derecho sólo competía a los "
                        },
                        {
                                "t11pregunta": "  y las mujeres fueron relegadas de la vida política.<br>"
                        },
                ],
                "pregunta": {
                        "c03id_tipo_pregunta": "8",
                        "t11pregunta": "Arrastra las palabras que completen el párrafo.",
                        "t11instruccion": "",
                        "respuestasLargas": true,
                        "pintaUltimaCaja": false,
                        "contieneDistractores": true
                }
        },
        /* //17
         {
                 "respuestas": [
                         {
                                 "t13respuesta": "Alemania",
                                 "t17correcta": "1,2"
                         },
                         {
                                 "t13respuesta": "Italia",
                                 "t17correcta": "2,1"
                         },
                         {
                                 "t13respuesta": "Rusia",
                                 "t17correcta": "3,4"
                         },
                         {
                                 "t13respuesta": "Francia",
                                 "t17correcta": "4,3"
                         },
                 ],
                 "preguntas": [
                         {
                                 "t11pregunta": "<br><style>\n\
                 .table img{height: 90px !important; width: auto !important; }\n\
                 </style><table class='table' style='margin-top:-10px;'>\n\
                 <tr><td>Triple Alianza</td><td>Triple Entente</td></tr><tr><td>"
                         },
                         {
                                 "t11pregunta": "</td><td>Gran Bretaña</td></tr><tr><td>"
                         },
                         {
                                 "t11pregunta": "</td><td>"
                         },
                         {
                                 "t11pregunta": "</td></tr><tr><td>Imperio <br>austrohúngaro</td><td>"
                         },
                         {
                                 "t11pregunta": "</td></tr></table>"
                         },
                 ],
                 "pregunta": {
                         "c03id_tipo_pregunta": "8",
                         "t11pregunta": "Arrastra cada país a donde corresponda.",
                         "t11instruccion": "",
                         "respuestasLargas": true,
                         "pintaUltimaCaja": false,
                         "contieneDistractores": true
                 }
         },
         //18
         {
                 "respuestas": [
                         {
                                 "t13respuesta": "El asesinato del archiduque Francisco Fernando.",
                                 "t17correcta": "1",
                                 "numeroPregunta": "0"
                         },
                         {
                                 "t13respuesta": "El control de la península de los Balcanes.",
                                 "t17correcta": "0",
                                 "numeroPregunta": "0"
                         },
                         {
                                 "t13respuesta": "La protección de Rusia.",
                                 "t17correcta": "0",
                                 "numeroPregunta": "0"
                         },
                         {
                                 "t13respuesta": "Enemistades con Serbia.",
                                 "t17correcta": "0",
                                 "numeroPregunta": "0"
                         }, ////////////////////////////////////////////////
                         {
                                 "t13respuesta": "La invención del motor de combustión interna y el uso de la gasolina.",
                                 "t17correcta": "1",
                                 "numeroPregunta": "1"
                         },
                         {
                                 "t13respuesta": "Los carros y aviones militares.",
                                 "t17correcta": "0",
                                 "numeroPregunta": "1"
                         },
                         {
                                 "t13respuesta": "La existencia de submarinos.",
                                 "t17correcta": "0",
                                 "numeroPregunta": "1"
                         },
                         {
                                 "t13respuesta": "La muerte de millones de personas.",
                                 "t17correcta": "0",
                                 "numeroPregunta": "1"
                         }, /////////////////////////////////////////////////////////
                         {
                                 "t13respuesta": "Tratado de Versalles",
                                 "t17correcta": "1",
                                 "numeroPregunta": "2"
                         },
                         {
                                 "t13respuesta": "Constitución Política",
                                 "t17correcta": "0",
                                 "numeroPregunta": "2"
                         },
                         {
                                 "t13respuesta": "Derechos de los Seres Humanos",
                                 "t17correcta": "0",
                                 "numeroPregunta": "2"
                         },
                         {
                                 "t13respuesta": "Tratado de Columbia",
                                 "t17correcta": "0",
                                 "numeroPregunta": "2"
                         },
                 ],
                 "pregunta": {
                         "c03id_tipo_pregunta": "1",
                         "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿Qué inició la primera guerra mundial?", "¿Qué promovió la aparición de nuevas armas?", "¿Qué documento firmaron los países involucrados en la primera guerra mundial al final de la misma?"],
                         "t11instruccion": "",
                         "preguntasMultiples": true
                 }
         },
         //19
         {
                 "respuestas": [
                         {
                                 "t13respuesta": "La teoría de la relatividad de Einstein.",
                                 "t17correcta": "1",
                         },
                         {
                                 "t13respuesta": "Descubrimiento de la penicilina.",
                                 "t17correcta": "1",
                         },
                         {
                                 "t13respuesta": "Desarrollo de armas terrestres, marítimas y aéreas.",
                                 "t17correcta": "1",
                         },
                         {
                                 "t13respuesta": "Descubrimiento de la gasolina.",
                                 "t17correcta": "0",
                         },
                         {
                                 "t13respuesta": "Gases venenosos como armas mortales.",
                                 "t17correcta": "0",
                         },
                 ],
                 "pregunta": {
                         "c03id_tipo_pregunta": "2",
                         "t11pregunta": "Selecciona todas las respuestas correctas. <br><br>¿Cuáles fueron los cambios y avances que hubo después de la primera guerra mundial?",
                         "t11instruccion": "",
                 }
         },
         //20
         {
                 "respuestas": [
                         {
                                 "t13respuesta": "F",
                                 "t17correcta": "0",
                         },
                         {
                                 "t13respuesta": "V",
                                 "t17correcta": "1",
                         },
                 ], "preguntas": [
                         {
                                 "c03id_tipo_pregunta": "13",
                                 "t11pregunta": "La Sociedad de las Naciones fue sustituida por la Organización de las Naciones Unidas en 1946.",
                                 "correcta": "1",
                         },
                         {
                                 "c03id_tipo_pregunta": "13",
                                 "t11pregunta": "Se instauraron regímenes autoritarios y totalitarios como el socialismo, el nazismo y la democracia.",
                                 "correcta": "0",
                         },
                         {
                                 "c03id_tipo_pregunta": "13",
                                 "t11pregunta": "El nazismo fue un movimiento italiano instaurado por Adolf Hitler.",
                                 "correcta": "0",
                         },
                         {
                                 "c03id_tipo_pregunta": "13",
                                 "t11pregunta": "Las crisis económicas generaron disgustos en sectores sociales y políticos que culparon al capitalismo del hambre y de la falta de servicios, educación y empleo.",
                                 "correcta": "1",
                         },
                         {
                                 "c03id_tipo_pregunta": "13",
                                 "t11pregunta": "El socialismo planteaba una igualdad social mediante una administración que dirigiera y vigilara la repartición de recursos entre la población.",
                                 "correcta": "1",
                         },
                         {
                                 "c03id_tipo_pregunta": "13",
                                 "t11pregunta": "El fascismo fue un movimiento alemán instaurado por BEnito Mussolini.",
                                 "correcta": "0",
                         },
                 ],
                 "pregunta": {
                         "c03id_tipo_pregunta": "13",
                         "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
                         "t11instruccion": "",
                         "descripcion": "Reactivo",
                         "variante": "editable",
                         "anchoColumnaPreguntas": 60,
                         "evaluable": true
                 }
         },
         //21
         {
                 "respuestas": [
                         {
                                 "t13respuesta": "Nacionalismo",
                                 "t17correcta": "1",
                         },
                         {
                                 "t13respuesta": "Racismo",
                                 "t17correcta": "1",
                         },
                         {
                                 "t13respuesta": "Militarismo",
                                 "t17correcta": "1",
                         },
                         {
                                 "t13respuesta": "Autoritarismo",
                                 "t17correcta": "1",
                         },
                         {
                                 "t13respuesta": "Democracia",
                                 "t17correcta": "0",
                         },
                         {
                                 "t13respuesta": "Igualdad",
                                 "t17correcta": "0",
                         },
                         {
                                 "t13respuesta": "Respeto",
                                 "t17correcta": "0",
                         },
                 ],
                 "pregunta": {
                         "c03id_tipo_pregunta": "2",
                         "t11pregunta": "Selecciona todas las respuestas correctas. <br><br>¿Qué principios regían la doctrina Nazi?",
                         "t11instruccion": "",
                 }
         },*/
];