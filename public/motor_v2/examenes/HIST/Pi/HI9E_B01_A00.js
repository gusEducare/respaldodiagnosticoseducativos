json = [
    {
        "respuestas": [
            {
                "t13respuesta": "1800 a.C.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "1492 d.C",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "200-900 d.C",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "1518 d.C",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "1535 d.C",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "1521 d.C",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Surgimiento de la cultura olmeca",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Viaje de conquista, liderado por Hernán Cortés y Juan Grijalva",
                "correcta": "3",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Surgen y florecen las civilizaciones maya, teotihuacana, tolteca y zapoteca",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Viajes de exploración y descubrimiento de América",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Inicio de la etapa de colonización con la derrota de Tenochtitlán",
                "correcta": "5",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nombramiento del primer virrey en la Nueva España",
                "correcta": "4",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Marca la opción que corresponda a la época en que ocurrieron los acontecimientos.",
            "t11instruccion": "",
            "descripcion": "Época",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Surgimiento del sedentarismo",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Surge el calendario, la escritura y la numeración",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Surgen los olmecas como civilización",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Auge belico y militar",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Surgen y decaen los Estados mesoamericanos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "El periodo del Virreinato duró 300 años",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Culminó con el estallido de la lucha por la independencia en 1810",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Culminó con la Guerrilla de Castas en 1795",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "El periodo del Virreinato duró 285 años",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>¿Por qué se se caracteriza el Periodo Preclásico (2500 a.C-2000 d.C)?",
                "¿Cuáles son la duración y la fecha de culminación del Virreinato? ",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aridoamérica se caracteriza por tener un territorio seco, fue habitado por pueblos nómadas que vivían en cuevas, dependían de la caza de venados y tenían un constante intercambio comercial con Mesoamérica",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La economía del Clásico, se  basó en el comercio y el control del este, que se componía de actividad agrícola y artesanal",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El pensamiento es el conjunto de conocimientos, creencias, y sistemas de valores que articulan la vida social de los grupos indígenas",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ometéotl es la concepción que predomina en las culturas mesoamericanas sobre la existencia de un Dios a la vez femenino y masculino",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mesoamérica  tenía un oasis en donde se practicaba la “agricultura de temporal”. Se desarrollaron las culturas anasazi, pataya y hohokam",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mesoamérica tenía pueblos sedentarios y agrícolas, que se caracterizaban por el desarrollo de la agricultura del maíz. Desarrollaron un calendario, la arquitectura, el uso de la obsidiana, y clasificaban a su sociedad",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Suceso",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "La Triple Alianza",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Alianza de Texcoco",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Alianza Mexico-Tenochtitlán",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Tributación",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Esclavitud",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Castas",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Postclásico",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Neoclásico",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Clásico",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>Fue la unión de pueblos que se encontraban asentados en la zona de Texcoco.",
                "Es un sistema en donde los pueblos conquistados por el dominio mexica, entregan parte de sus productos agrícolas, artesanías, recursos materiales y humanos.",
                "En este periodo, la Triple Alianza dominó casi todo el territorio de Mesoamérica y concluye con la invasión europea. Surgen los mexicas y los tarascos."],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },

    //5
    {
        "respuestas": [
            {
                "t13respuesta": "económicas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "agricultura",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "España",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "cacao",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Europa",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "arroz",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "ganadería",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "porcinos",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "paisaje",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "minería",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "plata",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "económicamente",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "intercambio cultural",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "asiáticos",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "Real Casa",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "universidades",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "África",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "empresas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "acuicultura",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "socialmente ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Real Hacienda",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Cuando viajamos a la antigua Nueva España, encontramos una variedad de actividades "
            },
            {
                "t11pregunta": " que son clave para entender las transformaciones y los cambios de la época. La "
            },
            {
                "t11pregunta": " fue la actividad que producía alimentos para la población y para la exportación a "
            },
            {
                "t11pregunta": " como "
            },
            {
                "t11pregunta": ", aguacate y jitomate. Además, el clima y las tierras de la Nueva España, permitieron producir plantíos traídos de "
            },
            {
                "t11pregunta": " como "
            },
            {
                "t11pregunta": ", avena, duraznos, entre otros. Mientras tanto, en la "
            },
            {
                "t11pregunta": " traída por primera vez por los españoles, se introdujeron especies animales como los caballos, bovinos y "
            },
            {
                "t11pregunta": "; todo esto en su conjunto, tuvo como consecuencia la transformación del "
            },
            {
                "t11pregunta": ". Otro sector fundamental fue la "
            },
            {
                "t11pregunta": "  debido a la abundancia de "
            },
            {
                "t11pregunta": " que se explotó durante tres siglos. Y entonces, ¿ qué pasó con la inmigración? Al territorio llegaron españoles con la esperanza de prosperar "
            },
            {
                "t11pregunta": ", su influencia fue tal, que su idioma y religión fueron los más prevalecientes; aún así también sucede un "
            },
            {
                "t11pregunta": " con africanos y "
            },
            {
                "t11pregunta": ", varios de ellos en condición de esclavos.<br>Por último, también  se instituyó por decreto del rey, la "
            },
            {
                "t11pregunta": " de Moneda en México, y por parte de los frailes, la creación de las "
            },
            {
                "t11pregunta": " en la Nueva España.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Evangelización",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Cristiandad",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Catolicismo",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La construcción de puertos marítimos, apertura de minas y las condiciones de abastecimiento de agua, agricultura y ganadería.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La importancia política y social del lugar para los indígenas, el número de caminos libres que facilitaban el abastecimiento.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La localización de zonas con población mayoritariamente evangelizada y las condiciones de abastecimiento de agua, agricultura y ganadería.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Mercedes reales",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Encomiendas",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Tributos",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Encomienda",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Proteccionismo",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Tributo",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Real Audiencia",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Visitadores",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Virreinato",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "La ruta de Veracruz hacia el Pánuco y la ruta con dirección al Pacífico hacia el estado Colima.",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "La ruta de Campeche hacia el Pánuco y la ruta con dirección al noreste hacia el estado Colima.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "La ruta de Chiapas hacia el Pánuco y la ruta con dirección al Pacífico hacia el estado Colima.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Latifundios",
                "t17correcta": "1",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Sistemas de cultivo",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Caciques",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "La Noche Triste",
                "t17correcta": "1",
                "numeroPregunta": "7"
            },
            {
                "t13respuesta": "La batalla del Imperio azteca",
                "t17correcta": "0",
                "numeroPregunta": "7"
            },
            {
                "t13respuesta": "La batalla de Tenochtitlán",
                "t17correcta": "0",
                "numeroPregunta": "7"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>Fue el proceso mediante el cual los españoles impusieron a los indígenas el cristianismo como su religión.",
                "La fundación de ciudades en la Nueva España se guiaba por: ",
                "Son aquellos privilegios que otorgaba el rey a cambio de la prestación de servicios en favor de la Corona Española, como en las expediciones y conquistas.",
                "Se caracteriza por una marcada explotación y abuso de los indígenas por parte de los españoles, los cuales exigían que se les pagara un tributo a cambio de brindarles protección.",
                "Se crea por el Rey de España con la finalidad de establecer la paz en sus territorios conquistados.",
                "Son las rutas que utilizaron los españoles para expandirse y colonizar.",
                "Los conquistadores impusieron a los pueblos bautizados como la Nueva España, un pago de tributo y trabajo forzado, que permitió la creación de:",
                "El siguiente acontecimiento sucede el 30 de junio de 1520 y se caracteriza por la derrota de los españoles a manos de los mexicas.",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Casa de contratación",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Comercialización de productos y minerales de la Nueva España en Europa",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Consulados de Comercio",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Flotas de Indias",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Plata",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Puerto de Acapulco",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "Interior y exterior",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Cobraba aranceles por el comercio que realizaban particulares y elaboraba ordenanzas para regular la relación entre comerciantes y navegantes"
            },
            {
                "t11pregunta": "La dominación del comercio mundial por parte de España se debía a"
            },
            {
                "t11pregunta": "Su función era vigilar que el comercio fuera realizado por comerciantes autorizados. Se creó en la Nueva España"
            },
            {
                "t11pregunta": "Se encargaban de transportar las riquezas de los virreinatos de la Nueva España hacia Europa"
            },
            {
                "t11pregunta": "Se utilizaba para la adquisición de productos y servicios, para financiar guerras y para comerciar"
            },
            {
                "t11pregunta": "Servía de enlace para el comercio en Asia con las Filipinas y con Perú en Sudamérica"
            },
            {
                "t11pregunta": "Con la colonización de Filipinas en Oriente por parte de España, el comercio obtuvo las formas de"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Comerciantes y mineros",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Clérigos religiosos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Artesanos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Corporación",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Clases sociales",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Castas",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Indígenas, esclavos, mestizos",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Indígenas, esclavos, artesanos",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Mestizos, artesanos, esclavos",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Artesanos",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Comerciantes",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Alfareros",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Virrey",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Obispo ",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Papa ",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Alcaldes, cabildos, regidores y un procurador de justicia",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Fraile, cabildos  y un procurador de justicia",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Virrey, alcaldes, regidores y un procurador de justicia",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Capitanías",
                "t17correcta": "1",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Cabildos",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Consulados",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "La Iglesia catolica",
                "t17correcta": "1",
                "numeroPregunta": "7"
            },
            {
                "t13respuesta": "La Santa Inquisición",
                "t17correcta": "0",
                "numeroPregunta": "7"
            },
            {
                "t13respuesta": "El Islam ",
                "t17correcta": "0",
                "numeroPregunta": "7"
            },
            {
                "t13respuesta": "Santo Tribunal de Oficio de la Inquisición",
                "t17correcta": "1",
                "numeroPregunta": "8"
            },
            {
                "t13respuesta": "Tribunal de Justicia por la fe divina ",
                "t17correcta": "0",
                "numeroPregunta": "8"
            },
            {
                "t13respuesta": "Tribunal de la Penitencia y la Inquisición",
                "t17correcta": "0",
                "numeroPregunta": "8"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>Proveían abundantes impuestos a la monarquía española, contaban con derechos y privilegios dentro de la organización política.",
                "A los grupos específicos en su función que conformaban la organización social como unidades, durante el Virreinato se les conocía como:",
                "El término de sociedad en la Nueva España excluía a los siguientes sectores de la población:",
                "Este grupo de la Nueva España, estaba integrado por españoles que emigraron de su lugar de origen. Se encontraban en la parte inferior de las corporaciones.",
                "Se caracteriza por ser la máxima autoridad en la Nueva España, con la limitante de que estaba bajo las órdenes del rey.",
                "En la Nueva España se crearon ayuntamientos a nivel local que estaban conformados por:",
                "El territorio de la Nueva España se dividía en:",
                "Fungió un papel fundamental de cohesión, colonización, administración de la justicia y política en los habitantes de la Nueva España, su mandato tenía un carácter divino.",
                "Tenía la función de  vigilar y castigar a aquellos que no fuesen católicos.",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
       },
//9
{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Debido a la Iglesia, se construyeron monasterios, hospitales, escuelas y templos en la Nueva España",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": " Los indígenas comerciaban productos locales en los mercados tradicionales",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El comercio interior en la Nueva España, fue limitado, debido a la falta de infraestructura",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los peones eran los encargados de los latifundios en la Nueva España",
            "correcta": "0",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Selecciona Falso o Verdadero según corresponda.",
        "t11instruccion": "",
        "descripcion": "Enunciado",
        "variante": "editable",
        "anchoColumnaPreguntas": 30,
        "evaluable": true
    }
},
//10
{
    "respuestas": [
        {
            "t13respuesta": "Se encuentran principalmente en la arquitectura y predomina el estilo barroco. También se tiene un carácter religioso",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Se encuentran principalmente en la escultura y predomina el estilo expresionista. También se tiene un carácter religioso",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Se encuentran principalmente en la pintura y predomina el estilo barroco. Tienen un carácter populista",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Leyendas",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "Cuentos",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "Mitos",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "La mujer herrada, la casa del judío, la llorona, la calle del indio triste",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "La llorona, la mujer de Juan, El virrey de Cádiz, la casa del judío",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "La calle del indio triste, la llorona, El cónsul de Cádiz, la mujer de Juan",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "Idioma",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Arquitectura",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Vestimenta",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Iglesia católica",
            "t17correcta": "1",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "Hinduismo",
            "t17correcta": "0",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "Islam",
            "t17correcta": "0",
            "numeroPregunta": "4"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>Son algunas características de las de las expresiones artísticas novohispanas.",
            "Buscan representar aspectos de la vida cotidiana de la población mediante una narración de un acontecimiento real o inventado además, eran famosos en la Nueva España.",
            "Son las leyendas más populares de la época colonial.",
            "Es el ejemplo más claro de la herencia del mestizaje de la Nueva España en la actualidad.",
            "Las costumbres y tradiciones heredadas de la Nueva España, tienen un arraigo o influencia importante de:",],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
   },

//11
{
    "respuestas": [
        {
            "t13respuesta": "Seadogs",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "Franceses",
            "t17correcta": "2",
        },
        {
            "t13respuesta": "Piratería transatlántica",
            "t17correcta": "3",
        },
        {
            "t13respuesta": "Idolatría",
            "t17correcta": "4",
        },
        {
            "t13respuesta": "El Códice Cruz-Badiano",
            "t17correcta": "5",
        },
        {
            "t13respuesta": "Medicina prehispánica",
            "t17correcta": "6",
        },
        {
            "t13respuesta": "Recababan los relatos medicinales indígenas",
            "t17correcta": "7",
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "Se caracterizaban por ser una amenaza marítima para los ingleses"
        },
        {
            "t11pregunta": "Realizaron los primeros ataques de piratería en el actual territorio mexicano"
        },
        {
            "t11pregunta": "Inglaterra extendió una guerra de dominio comercial y marítimo en la época de la colonia mediante"
        },
        {
            "t11pregunta": "Los españoles prohibieron los actos mágico-religiosos de los indígenas debido a"
        },
        {
            "t11pregunta": "Manuscrito que presenta la cultura médica indígena"
        },
        {
            "t11pregunta": "Estaba ligada a la religión y asociada a una cosmovisión, se caracterizaba por atribuir enfermedades a los dioses"
        },
        {
            "t11pregunta": "Los frailes españoles"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda.",
        "altoImagen": "100px",
        "anchoImagen": "200px"
    }
},
// reactivo 12
{
    "respuestas": [
        {
            "t13respuesta": "Revolución Francesa",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Inicio de la Independencia de México",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "Derrota de Napoleón",
            "t17correcta": "2",
        },
        {
            "t13respuesta": "Constitución de Cádiz",
            "t17correcta": "3",
        },
        {
            "t13respuesta": "Primer gobierno independiente de México",
            "t17correcta": "4",
        },

    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "1810",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "1813",
            "correcta": "3",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "1815",
            "correcta": "2",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "1789",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "1821",
            "correcta": "4",
        },

    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada hecho histórico.",
        "t11instruccion": "",
        "descripcion": "Época",
        "variante": "editable",
        "anchoColumnaPreguntas": 20,
        "evaluable": true
    }
},
//reactivo 13
{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Falso",
            "t17correcta": "1",
        },


    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los criollos empezaron a gestar la idea de independizarse de España en el siglo XVIII.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En la Nueva España, después de dos siglos de virreinato, se producen cambios importantes como el aumento de población y territorios.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las reformas borbónicas expandieron el poder de la nobleza española y generaron su aceptación en la población de la Nueva España.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La independencia se gestó debido a la inconformidad y discriminación que vivían los criollos por parte de los españoles.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los insurgentes se conformaron en las ciudades ubicadas en el sur de la Nueva España.",
            "correcta": "0",
        },

    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
        "t11instruccion": "",
        "descripcion": "Época",
        "variante": "editable",
        "anchoColumnaPreguntas": 20,
        "evaluable": true
    }
},

// reactivo 14
{
    "respuestas": [
        {
            "t13respuesta": "campesino, que producía para su consumo, y terratenientes de las haciendas, con producción a gran escala y destinada al comercio.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "campesino, que producía para su consumo  y artesanal, que producía para el comercio exterior.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "minero, que produce para el comercio, y campesino, que producía para su consumo.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "minería",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "maquila",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "textiles",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "textil",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "ganadero",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "artesano",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "Zacatecas",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Puebla",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Hidalgo",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "diezmo",
            "t17correcta": "1",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "ofrenda",
            "t17correcta": "0",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "tributo",
            "t17correcta": "0",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "haciendas o ingenios.",
            "t17correcta": "1",
            "numeroPregunta": "5"
        },
        {
            "t13respuesta": "tierras y artesanía.",
            "t17correcta": "0",
            "numeroPregunta": "5"
        },
        {
            "t13respuesta": "minería.",
            "t17correcta": "0",
            "numeroPregunta": "5"
        },

        {
            "t13respuesta": "expansión del territorio y la inmigración española.",
            "t17correcta": "1",
            "numeroPregunta": "6"
        },
        {
            "t13respuesta": "minería y mestizaje.",
            "t17correcta": "0",
            "numeroPregunta": "6"
        },
        {
            "t13respuesta": "la agricultura y la expansión indígena por el territorio.",
            "t17correcta": "0",
            "numeroPregunta": "6"
        },
        {
            "t13respuesta": "minería",
            "t17correcta": "1",
            "numeroPregunta": "7"
        },
        {
            "t13respuesta": "sector textil",
            "t17correcta": "0",
            "numeroPregunta": "7"
        },
        {
            "t13respuesta": "comercio",
            "t17correcta": "0",
            "numeroPregunta": "7"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>El sector agropecuario de la Nueva España estaba dividido en los siguientes sectores:",
        "Actividad económica de la Nueva España que se basa en la extracción mineral:",
        "El siguiente sector se desarrolló con éxito en la Nueva España y evolucionó con el tiempo de los obrajes a la elaboración de prendas:",
        "El auge minero en Nueva España inició en 1545 en:",
        "La iglesia católica logró acumular riquezas mediante:",
        "Las ganancias del comercio interno de la Nueva España se solían invertir en:",
        "En el siglo XVII el crecimiento poblacional en la Nueva España se debía a:",
        "En la época de la Nueva España las actuales ciudades de Monterrey, Saltillo, Monclova y Durango se desarrollaron gracias a la:"


          ],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
},
//reactivo 15
{
    "respuestas": [
        {
            "t13respuesta": "Reformas borbónicas.",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "El monopolio comercial de las colonias.",
            "t17correcta": "2",
        },
        {
            "t13respuesta": "Intendencias.",
            "t17correcta": "3",
        },
        {
            "t13respuesta": "Monarquía absolutista.",
            "t17correcta": "4",
        },
        {
            "t13respuesta": "Reformas comerciales.",
            "t17correcta": "5",
        },

    ],
    "preguntas": [
        {
            "t11pregunta": "Buscaron incrementar la recaudación de impuestos en la Nueva España y restringir la importancia de los criollos."
        },
        {
            "t11pregunta": "Fue una de las razones de los conflictos bélicos entre España, Inglaterra y Francia."
        },
        {
            "t11pregunta": "Fue la división político-administrativa que establecieron los Borbones en la Nueva España."
        },
        {
            "t11pregunta": "Se caracteriza por tener ideas y principios de la ilustración española formando parte del movimiento de despotismo ilustrado."
        },
        {
            "t11pregunta": "Tenían el objetivo de tener un mejor control del comercio y evitar el contrabando."
        },

    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda.",
        "altoImagen": "100px",
        "anchoImagen": "200px"
    }
},
// reactivo 16
{
    "respuestas": [
        {
            "t13respuesta": "Fuero",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Corporativo",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "De expiación",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "clero, mineros, comerciantes y funcionarios de gobierno.",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "funcionarios de gobierno, indígenas y mineros.",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "artesanos, mineros, agricultores y campesinos.",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "crecimiento poblacional, comercio y expansión económica",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "tensión social, discriminación, migración y desigualdad",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "desigualdad, migración hacia el campo, decrecimiento poblacional",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "las haciendas",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "las corporaciones",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "los ejidos",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },

    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿Cuál es el derecho eclesiástico que tenían los sacerdotes de ser juzgados por tribunales especiales y se usaba para exentar sus faltas?",
        "Los siguientes grupos tenían el derecho de pertenecer a las corporaciones:",
        "Las reformas borbónicas provocaron en las ciudades de la Nueva España:",
        "Fueron una forma de explotar laboralmente a los indígenas en la época de la Nueva España, así como de incrementar la riqueza de los españoles:",


          ],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
},

// reactivo 17

{
  "respuestas": [        
        {
           "t13respuesta":  "Hidalgo",
             "t17correcta": "1"
         },
        {
           "t13respuesta":  "1810",
             "t17correcta": "2"
         },
        {
           "t13respuesta":  "Insurgentes",
             "t17correcta": "3"
         },
        {
           "t13respuesta":  "monarquía",
             "t17correcta": "4"
         },
        {
           "t13respuesta":  "Realistas",
             "t17correcta": "5"
         },
        {
           "t13respuesta":  "social",
             "t17correcta": "6"
         },
        {
           "t13respuesta":  "sentimientos",
             "t17correcta": "7"
         },
        {
           "t13respuesta":  "Cádiz",
             "t17correcta": "8"
         },
        {
           "t13respuesta":  "igualdad",
             "t17correcta": "9"
         },
 {
            "t13respuesta":  "Morelos",
              "t17correcta": "1"
          },
         {
            "t13respuesta":  "anarquía",
              "t17correcta": "2"
          },
         {
            "t13respuesta":  "Ilustradores",
              "t17correcta": "3"
          },
         {
            "t13respuesta":  "pensamientos",
              "t17correcta": "4"
          },
    ],
    "preguntas": [
      {
      "t11pregunta": "El inicio de la lucha por la Independencia convocado por el cura "
      },
      {
      "t11pregunta": " en "
      },
      {
      "t11pregunta": " tuvo como resultado la formación de dos grupos: los que están a favor de la independencia; los "
      },
      {
      "t11pregunta": ", al mando del militar Juan Aldama y el capitán criollo Ignacio José Allende;  y los que quieren continuar bajo el dominio de la "
      },
      {
      "t11pregunta": " española, los "
      },
      {
      "t11pregunta": ".<br>Entonces te preguntarás, ¿qué motivó a los Insurgentes a iniciar el movimiento de independencia? Desde Hidalgo hasta Morelos, todos tienen en común un pensamiento "
      },
      {
      "t11pregunta": ", es decir, para defender al pueblo de la tiranía, por sus ideales de libertad y de justicia. Un ejemplo es el documento “Los "
      },
      {
      "t11pregunta": " de la nación”, de Morelos, y “El decreto de la abolición de la esclavitud”,de Hidalgo. Otra influencia importante en el movimiento fue el liberalismo español y la Constitución de "
      },
      {
      "t11pregunta": " de 1813, que proclamaba la independencia española ante la invasión francesa y el reconocimiento de la independencia de la Nueva España, así como la "
      },
      {
      "t11pregunta": " de sus habitantes.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "",
       "t11instruccion": "",
       "respuestasLargas": true,
       "pintaUltimaCaja": false,
       "contieneDistractores": true
     }
}
    ];
