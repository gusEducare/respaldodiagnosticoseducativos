json = [
// //1
{
    "respuestas": [
        {
            "t13respuesta": "Siglo XVIII",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "1815",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "1810",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "1789",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "1821",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "1776",
            "t17correcta": "5"
        },
        {
            "t13respuesta": "1813",
            "t17correcta": "6"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Inicio de Movimiento de Ilustración",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Revolución francesa",
            "correcta"  : "3"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Inicio de la Independencia de México",
            "correcta"  : "2"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Derrota de Napoleón",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Independencia de los Estados Unidos",
            "correcta"  : "5"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Primer gobierno Independiente de México",
            "correcta"  : "4"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Constitución de Cádiz",
            "correcta"  : "6"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p>Selecciona la casilla que relacione la fecha con el hecho histórico correspondiente.<\/p>",
        "descripcion":"Hechos",
        "evaluable":false,
        "evidencio": false,
        "anchoColumnaPreguntas":"20"
    }
},
    //2
    {
       "respuestas": [
           {
               "t13respuesta": "Falso",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Verdadero",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Los criollos empezaron a gestar la idea de independizarse de España en el siglo XVIII",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En la Nueva España, después de dos siglos de virreinato, se producen cambios importantes como el aumento de población y territorios.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las reformas borbónicas expandieron el poder de la nobleza española y generaron su aceptación en la población de la Nueva España.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La independencia se gestó debido a la inconformidad y discriminación que vivían los criollos por parte de los españoles.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Los Insurgentes se conformaron en las ciudades ubicadas en el sur de la Nueva España.",
               "correcta"  : "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<p>Selecciona verdadero (V) o falso (F) según corresponda.<\/p>",
           "descripcion":"Aspectos a valorar",
           "evaluable":false,
           "evidencio": false
       }
   },
   //3
  {
    "respuestas":[
       {
          "t13respuesta":     "Sector campesino, que producía para su consumo, y sector artesanal, que producía   para el comercio exterior.",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Sector minero, que produce para el comercio, y sector campesino, que producía para su consumo.",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Sector campesino, que producía para su consumo, y el sector de los terratenientes de las haciendas, con producción a gran escala y destinada al comercio.",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Maquila",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Minería",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Textiles",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Manufacturero",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Textil",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Artesano",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Zacatecas",
          "t17correcta":"1",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "Guanajuato",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "Hidalgo",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "Ofrenda",
          "t17correcta":"0",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "Tributo",
          "t17correcta":"0",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "Diezmo",
          "t17correcta":"1",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "Contar con intereses prioritariamente económicos y por fungir como instituciones de crédito.",
          "t17correcta":"1",
        "numeroPregunta":"5"
       },
       {
          "t13respuesta":     "Por la fundación de centros de estudio, iglesias, hospitales y orfanatos.",
          "t17correcta":"0",
        "numeroPregunta":"5"
       },
       {
          "t13respuesta":     "Generar necesidades a la población.",
          "t17correcta":"0",
        "numeroPregunta":"5"
       },
       {
          "t13respuesta":     "Tierras y artesanía.",
          "t17correcta":"0",
        "numeroPregunta":"6"
       },
       {
          "t13respuesta":     "Minería",
          "t17correcta":"0",
        "numeroPregunta":"6"
       },
       {
          "t13respuesta":     "Haciendas o ingenios",
          "t17correcta":"1",
        "numeroPregunta":"6"
       },
       {
          "t13respuesta":     "Minería y mestizaje",
          "t17correcta":"0",
        "numeroPregunta":"7"
       },
       {
          "t13respuesta":     "Expansión del territorio y la inmigración española",
          "t17correcta":"1",
        "numeroPregunta":"7"
       },
       {
          "t13respuesta":     "La agricultura y la expansión indígena por el territorio",
          "t17correcta":"0",
        "numeroPregunta":"7"
       },
       {
          "t13respuesta":     "Minería",
          "t17correcta":"1",
        "numeroPregunta":"8"
       },
       {
          "t13respuesta":     "Sector textil",
          "t17correcta":"0",
        "numeroPregunta":"8"
       },
       {
          "t13respuesta":     "Comercio",
          "t17correcta":"0",
        "numeroPregunta":"8"
       }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta.<br>El sector agropecuario de la Nueva España estaba dividido en los siguientes sectores:",
                      "La actividad industrial surge en Nueva España con:",
                      "El siguiente sector se desarrolló con éxito en la Nueva España y evolucionó con el tiempo de los obrajes a la elaboración de prendas: ",
                      "El auge minero en Nueva España inició en 1545 en: ",
                      "La iglesia católica  logró acumular riquezas mediante:",
                      "En la Nueva españa, el clero secular se caracterizaba por:",
                      "Las ganancias del comercio interno de la Nueva España se solían invertir en:",
                      "En el siglo XVII el  crecimiento poblacional en la Nueva España se debía a: ",
                      "En la época de la Nueva España las actuales ciudades de Monterrey, Saltillo, Monclova  y Durango se desarrollaron gracias a:"],
     "preguntasMultiples": true
    }
 },
 //    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Reformas borbónicas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El monopolio comercial de  las colonias",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Intendencias",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "La muerte del Rey Carlos II",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Monarquía absolutista",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Reformas comerciales",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Buscaron incrementar la recaudación de impuestos en la Nueva España y restringir  la importancia de los criollos."
            },
            {
                "t11pregunta": "Fue una de las razones de los conflictos bélicos entre España, Inglaterra y Francia."
            },
            {
                "t11pregunta": "Fue la división político-administrativa que establecieron los Borbones en la Nueva España."
            },
            {
                "t11pregunta": "Provocó un conflicto internacional entre varias dinastías."
            },
            {
                "t11pregunta": "Se caracteriza por tener ideas y principios de la ilustración española formando parte del movimiento de despotismo ilustrado."
            },
            {
                "t11pregunta": "Tenían el objetivo de tener un mejor control del comercio y evitar el contrabando."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "altoImagen":"150",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    //5
       {
         "respuestas":[
            {
               "t13respuesta":     "Fuero",
               "t17correcta":"1",
             "numeroPregunta":"0"
            },
            {
               "t13respuesta":     "Corporativo",
               "t17correcta":"0",
             "numeroPregunta":"0"
            },
            {
               "t13respuesta":     "De expiación",
               "t17correcta":"0",
             "numeroPregunta":"0"
            },
            {
               "t13respuesta":     "Hacendados",
               "t17correcta":"0",
             "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "Pirámide de clases sociales",
               "t17correcta":"0",
             "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "Corporaciones",
               "t17correcta":"1",
             "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "Funcionarios de gobiernos, indígenas, mineros",
               "t17correcta":"0",
             "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "Artesanos, mineros, agricultores, campesinos",
               "t17correcta":"0",
             "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "Clero, mineros, comerciantes y funcionarios de gobierno",
               "t17correcta":"1",
             "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "Tensión social, discriminación, migración y desigualdad",
               "t17correcta":"0",
             "numeroPregunta":"3"
            },
            {
               "t13respuesta":     "Crecimiento poblacional, comercio y expansión económica",
               "t17correcta":"1",
             "numeroPregunta":"3"
            },
            {
               "t13respuesta":     "Desigualdad, migración hacia el campo, decrecimiento poblacional",
               "t17correcta":"0",
             "numeroPregunta":"3"
            },
            {
               "t13respuesta":     "Las haciendas",
               "t17correcta":"1",
             "numeroPregunta":"4"
            },
            {
               "t13respuesta":     "Las corporaciones",
               "t17correcta":"0",
             "numeroPregunta":"4"
            },
            {
               "t13respuesta":     "Los ejidos",
               "t17correcta":"0",
             "numeroPregunta":"4"
            }
         ],
         "pregunta":{
            "c03id_tipo_pregunta":"1",
            "t11pregunta":["Selecciona la respuesta correcta.<br>¿Cuál es el derecho eclesiástico que tenían los sacerdotes de ser juzgados por tribunales especiales y se usaba para exentar sus faltas?",
                           "En la Nueva España del siglo XVIII predominaban los privilegios de clase con la  creación de:",
                           "Los siguientes grupos tenían el derecho de pertenecer a las corporaciones:",
                           "Las reformas borbónicas provocaron en las ciudades de la Nueva España:",
                           "Fueron una forma de explotar laboralmente a los indígenas en la época de la Nueva España, así como de incrementar  la riqueza de los españoles:"],
          "preguntasMultiples": true
         }
      },
    //6
    {
         "respuestas":[
            {
               "t13respuesta":     "Renacimiento",
               "t17correcta":"0",
             "numeroPregunta":"0"
            },
            {
               "t13respuesta":     "Ilustración",
               "t17correcta":"1",
             "numeroPregunta":"0"
            },
            {
               "t13respuesta":     "Reformas Borbónicas",
               "t17correcta":"0",
             "numeroPregunta":"0"
            },
            {
               "t13respuesta":     "La Revolución Francesa y la independencia de las Trece Colonias",
               "t17correcta":"1",
             "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "La independencia de las Trece Colonias y el movimiento obrero francés",
               "t17correcta":"0",
             "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "La Revolución Francesa y la independencia de la India",
               "t17correcta":"0",
             "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "La invasión árabe en España",
               "t17correcta":"0",
             "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "Los enfrentamientos bélicos con Portugal",
               "t17correcta":"0",
             "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "La invasión francesa de Napoleón Bonaparte en España",
               "t17correcta":"1",
             "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "Indígenas",
               "t17correcta":"0",
             "numeroPregunta":"3"
            },
            {
               "t13respuesta":     "Españoles",
               "t17correcta":"0",
             "numeroPregunta":"3"
            },
            {
               "t13respuesta":     "Criollos",
               "t17correcta":"1",
             "numeroPregunta":"3"
            },
            {
               "t13respuesta":     "Criollos",
               "t17correcta":"1",
             "numeroPregunta":"4"
            },
            {
               "t13respuesta":     "Mulatos",
               "t17correcta":"0",
             "numeroPregunta":"4"
            },
            {
               "t13respuesta":     "Españoles",
               "t17correcta":"0",
             "numeroPregunta":"4"
            },
            {
               "t13respuesta":     "La búsqueda de una sociedad más justa en Nueva España",
               "t17correcta":"0",
             "numeroPregunta":"5"
            },
            {
               "t13respuesta":     "El miedo de los peninsulares a perder sus privilegios",
               "t17correcta":"1",
             "numeroPregunta":"5"
            },
            {
               "t13respuesta":     "El miedo de los criollos a perder sus privilegios",
               "t17correcta":"0",
             "numeroPregunta":"5"
            },
            {
               "t13respuesta":     "La Junta de seguridad",
               "t17correcta":"1",
             "numeroPregunta":"6"
            },
            {
               "t13respuesta":     "El tribunal de la Nueva España",
               "t17correcta":"0",
             "numeroPregunta":"6"
            },
            {
               "t13respuesta":     "El Juicio del Virreinato",
               "t17correcta":"0",
             "numeroPregunta":"6"
            },
            {
               "t13respuesta":     "Un cura del pueblo de Dolores, ex alumno de los franciscanos y continuador del movimiento de independencia.",
               "t17correcta":"0",
             "numeroPregunta":"7"
            },
            {
               "t13respuesta":     "Un cura del pueblo de Dolores, ex alumno de los jesuitas y un iniciador del movimiento de independencia.",
               "t17correcta":"1",
             "numeroPregunta":"7"
            },
            {
               "t13respuesta":     "Un cura del pueblo de Dolores, ex alumno de los jesuitas y un iniciador del movimiento de la Revolución Mexicana.",
               "t17correcta":"0",
             "numeroPregunta":"7"
            },
            {
               "t13respuesta":     "16 de septiembre de 1810",
               "t17correcta":"1",
             "numeroPregunta":"8"
            },
            {
               "t13respuesta":     "14 de septiembre de 1810",
               "t17correcta":"0",
             "numeroPregunta":"8"
            },
            {
               "t13respuesta":     "16 de septiembre de 1812",
               "t17correcta":"0",
             "numeroPregunta":"8"
            }
         ],
         "pregunta":{
            "c03id_tipo_pregunta":"1",
            "t11pregunta":["Selecciona la respuesta correcta.<br>Fue un factor europeo externo de influencia para el inicio de las ideas de independencia en la Nueva España, se basaba en la igualdad social y combatía la tiranía:",
                           "Los criollos de la Nueva España fueron influenciados por los siguientes movimientos para iniciar su guerra de Independencia:",
                           "Fue una de las razones clave por las cuales España dejó de tener control sobre Nueva España:",
                           "Fueron quienes acuñaron el término de “mexicanos”:",
                           "En el año 1760, este grupo de jóvenes de la Nueva España eran preparados académicamente por los jesuitas y desarrollaron orgullo por su ascendencia azteca.",
                           "El golpe de estado de 1808 por parte de los peninsulares en la Nueva España se debió a :",
                           "Los españoles acusaban a los criollos que tenían ideas de independencia en:",
                           "Don Miguel Hidalgo fue:",
                           "El inicio del movimiento de independencia fue en la siguiente fecha:"],
          "preguntasMultiples": true
         }
      },
   //7
   {
       "respuestas": [
           {
               "t13respuesta": "<p>Hidalgo<\/p>",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "<p>1810<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p>insurgentes<\/p>",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "<p>Ignacio José Allende<\/p>",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "<p>monarquía<\/p>",
               "t17correcta": "5"
           },
           {
               "t13respuesta": "<p>Realistas<\/p>",
               "t17correcta": "6"
           },
           {
               "t13respuesta": "<p>social<\/p>",
               "t17correcta": "7"
           },
           {
               "t13respuesta": "<p>Los Sentimientos<\/p>",
               "t17correcta": "8"
           },
           {
               "t13respuesta": "<p>de la Nación<\/p>",
               "t17correcta": "9"
           },
           {
               "t13respuesta": "<p>El decreto de la abolición<\/p>",
               "t17correcta": "10"
           },
           {
               "t13respuesta": "<p>de la esclavitud<\/p>",
               "t17correcta": "11"
           },
           {
               "t13respuesta": "<p>Constitución de Cádiz<\/p>",
               "t17correcta": "12"
           },
           {
               "t13respuesta": "<p>invasión francesa<\/p>",
               "t17correcta": "13"
           },
           {
               "t13respuesta": "<p>igualdad<\/p>",
               "t17correcta": "14"
           },
           {
               "t13respuesta": "<p>guerra de guerrillas<\/p>",
               "t17correcta": "15"
           },
           {
               "t13respuesta": "<p>Vicente Guerrero<\/p>",
               "t17correcta": "16"
           },
           {
               "t13respuesta": "<p>1821<\/p>",
               "t17correcta": "17"
           },
           {
               "t13respuesta": "<p>Plan de Iguala<\/p>",
               "t17correcta": "18"
           },
           {
               "t13respuesta": "<p>Iturbide<\/p>",
               "t17correcta": "19"
           },
           {
               "t13respuesta": "<p>Juan de O´Donojú<\/p>",
               "t17correcta": "20"
           },
           {
               "t13respuesta": "<p>Tratados de Córdoba<\/p>",
               "t17correcta": "21"
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "El inicio de la lucha por la Independencia convocado por el cura "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " en "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " tiene como resultado la formación de dos grupos: los que están a favor de la independencia; los "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", al mando del militar Juan Aldama y el capitán criollo "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ";  y los que quieren continuar bajo el dominio de la "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " española, los "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ". Entonces te preguntarás, ¿qué motivó a los Insurgentes a iniciar el movimiento de independencia? Desde Hidalgo hasta Morelos, todos tienen en común un pensamiento "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", es decir, para defender al pueblo de la tiranía, por sus ideales de libertad y de justicia. Un ejemplo es el documento “"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "”, de Morelos, y “"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "”, de Hidalgo. Otra influencia importante en el movimiento fue el liberalismo español y la "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " de 1813, que proclamaba la independencia española ante la "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " y el reconocimiento de la independencia de la Nueva España, así como la "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " de sus habitantes. Ahora, respecto a la lucha de independencia en la Nueva España, a la muerte de Miguel Hidalgo y de su sucesor Morelos, se entró en una etapa de "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " liderada principalmente por "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " y Guadalupe Victoria.  Por último, la consumación de la guerra de independencia se realiza en el mes de febrero de "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " con la firma del "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ", por Guerrero e "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": ". Del mismo modo, en agosto llega de España "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " (enviado de España), quien firma los "
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": " para ratificar la independencia de la Nueva España."
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "t11pregunta": "Arrastra la palabra al lugar que corresponda:",
          "contieneDistractores":true,
           "anchoRespuestas": 40,
           "soloTexto": true,
           "respuestasLargas": true,
           "pintaUltimaCaja": false,
           "ocultaPuntoFinal": true
       }
   },
   //8
   {
     "respuestas":[
       {
          "t13respuesta":     "El renacimiento y el barroco",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "El barroco y el neoclásico",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "El neoclásico y el renacimiento",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Barroco",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Renacimiento",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Surrealismo",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Luis de Góngora, Juan Rulfo, Jaime Sabines, Miguel de Cervantes",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Miguel de Cervantes, Dante Alighieri, Sor Juana Inés de la Cruz",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Sor Juana Inés de la Cruz, Luis de Góngora, Pedro de Peralta",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Renacimiento",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "Clásico",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "Neoclásico",
          "t17correcta":"1",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "Clásico",
          "t17correcta":"0",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "Neoclásico",
          "t17correcta":"1",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "Surrealismo",
          "t17correcta":"0",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "Real Seminario de Minería, Jardín Botánico de México, Escuela de Bellas Artes",
          "t17correcta":"1",
        "numeroPregunta":"5"
       },
       {
          "t13respuesta":     "Escuela de Bellas Artes, Instituto de Lenguas y Letras, Instituto de Botánica",
          "t17correcta":"0",
        "numeroPregunta":"5"
       },
       {
          "t13respuesta":     "Real Seminario de Bellas Artes, Instituto de Lenguas y Letras, Instituto Científico",
          "t17correcta":"0",
        "numeroPregunta":"5"
       }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11pregunta":["Selecciona la respuesta correcta.<br>La riqueza artística que surge de la mezcla cultural en la Nueva España tiene su esplendor en:",
                       "Tuvo un gran esplendor en la Nueva España y se caracteriza por su influencia religiosa:",
                       "Son exponentes del siglo de oro:",
                       "Está inspirado en el sentido racional que debe prevalecer en el hombre, tratando de mostrar la belleza de la perfección formal:",
                       "Fue llevado a la Nueva España por la clase alta que viajaba a Europa y transmitía las nuevas tendencias, sobre todo en las instituciones académicas:",
                       "Son algunas instituciones fundadas en el siglo XVIII en la Nueva España debido al despotismo ilustrado:",],
        "preguntasMultiples": true
     }
  },
  //9
  {
       "respuestas": [
           {
               "t13respuesta": "Falso",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Verdadero",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En la época virreinal  todas las ciudades que se fundaron eran coloniales, es decir, producto de la colonización española y tenían un diseño similar a las ciudades españolas.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las ciudades coloniales son actualmente en México patrimonio cultural y se procura su conservación debido a su importancia turística y cultural.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las leyendas son narraciones de algún acontecimiento real o inventado, pertenecen o surgieron en  la época moderna.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En la Nueva España no existieron rebeliones indígenas o campesinas durante el virreinato.",
               "correcta"  : "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<p>Selecciona verdadero (V) o falso (F) según corresponda.<\/p>",
           "descripcion":"Aspectos a valorar",
           "evaluable":false,
           "evidencio": false
       }
   }
];
