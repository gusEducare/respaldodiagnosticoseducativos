json = [
// //1
    {
        "respuestas": [
            {
                "t13respuesta": "...un modelo neoliberal.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...como producto de la inconformidad con un sistema gubernamental que desatiende las demandas de la población marginada.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...una prioridad para el cuidado de la salud, el medio ambiente y sus recursos naturales.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "más éxito tenía entre la población",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...en 1990 para dar mayor certidumbre a los procesos electorales.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "México se rige bajo la política económica de..."
            },
            {
                "t11pregunta": "El Movimiento Zapatista de Liberación Nacional surge..."
            },
            {
                "t11pregunta": "El desarrollo industrial, la expansión urbana y el aumento desmedido de la contaminación son..."
            },
            {
                "t11pregunta": "En la década de los ochenta en México el medio de comunicación con..."
            },
            {
                "t11pregunta": "El Instituto Federal Electoral de México se creó..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona con líneas las columnas según corresponda.",
        }
    },
    //2
    {
       "respuestas": [
           {
               "t13respuesta": "F",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "V",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El modelo económico actual de México está pensado para la creación y atracción de industrias que generen empleos y un incremento en el recaudamiento de impuestos para la modernización y manutención de la infraestructura del país.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En México viven aproximadamente 120 millones de personas.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La política económica de México está pensada como un modelo de izquierda.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Algunos de los problemas que aquejan a México como nación son la pobreza, la expansión urbana y el desempleo.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El Tratado de Libre Comercio de América del Norte se firma en 1994 entre México, Argentina y Estados Unidos.",
               "correcta"  : "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
       }
     },
     //3
     {
       "respuestas":[
          {
             "t13respuesta": "Entró en un círculo de inflación y devaluaciones del peso frente al dólar.",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Se consolidó como una de las mejores economías de Latinoamérica.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Aumentó su producción nacional acrecentando su PIB.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Tuvo una crisis económica parecida a la de Wall Street de 1929.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },////////////////////////1
          {
             "t13respuesta": "Luis Echeverría y López Portillo",
             "t17correcta": "1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Vicente Fox y Felipe Calderón",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "López Portillo y Ernesto Zedillo",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Lázaro Cárdenas y Ávila Camacho",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },//////////////////////2
          {
             "t13respuesta": "1994",
             "t17correcta": "1",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta": "1990",
             "t17correcta": "0",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta": "1992",
             "t17correcta": "0",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta": "1996",
             "t17correcta": "0",
             "numeroPregunta":"2"
          },//////////////////////////3
          {
             "t13respuesta": "Cumplir con ciertas medidas para mantener una estabilidad económica.",
             "t17correcta": "1",
             "numeroPregunta":"3"
          },
          {
             "t13respuesta": "Adoptar una moneda internacional como era el dólar.",
             "t17correcta": "0",
             "numeroPregunta":"3"
          },
          {
             "t13respuesta": "Someter su sistema económico a un modelo neoliberal.",
             "t17correcta": "0",
             "numeroPregunta":"3"
          },
          {
             "t13respuesta": "Levantar cercos económicos impuestos a otras naciones.",
             "t17correcta": "0",
             "numeroPregunta":"3"
          },////////////////////////4
          {
             "t13respuesta": "Banco Interamericano de Desarrollo y Banco Mundial.",
             "t17correcta": "1",
             "numeroPregunta":"4"
          },
          {
             "t13respuesta": "Banco Mundial y la Organización de la Naciones Unidas.",
             "t17correcta": "0",
             "numeroPregunta":"4"
          },
          {
             "t13respuesta": "UNICEF y el TLC.",
             "t17correcta": "0",
             "numeroPregunta":"4"
          },
          {
             "t13respuesta": "Organización Mundial de la Salud y la ONU",
             "t17correcta": "0",
             "numeroPregunta":"4"
          }

       ],
       "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Tras la caída del petróleo después de la Segunda Guerra Mundial y haber pedido préstamos internacionales que planeaba pagar con los ingresos basados en un precio alto del petróleo, México:",
                          "¿Durante los gobiernos de qué presidentes el gobierno mexicano  se gastó más de lo que tenía, además de que comenzó a emitir más billetes y monedas sin tener respaldo económico?",
                          "Año en el que la economía mexicana se estabilizó y el dólar se vendía a 4.76 pesos mexicanos.",
                          "Al darse cuenta de que México podría caer en la insolvencia y no pagar los préstamos hechos, el Fondo Monetario Internacional, para seguir prestando dinero a México, condiciona al gobierno a:",
                          "A parte del FMI, otros organismos monetarios que condicionaron a México a cumplir con ciertas medidas fueron:"],
          "preguntasMultiples": true
       }
    },
    //4
    {
      "respuestas":[
         {
            "t13respuesta": "Expropiar la banca en 1982, acción que tuvo efectos negativos para la economía.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Privatizar la banca en 1990, para contrarrestar los efectos de la expropiación de la banca.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Crear un fondo de ahorro para la población mexicana en 1986, remediando así los desastres causados por la devaluación de 1960.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Firmar el TLC en 1994 para poder contrarrestar los efectos de la expropiación de la banca en 1982.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },///////////////////////////1
         {
            "t13respuesta": "Crear un mercado mundial donde todos los países participen.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Mantener una política de austeridad en los gastos del gobierno.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Dar más apoyo del gobierno a empresas para la creación de nuevas empresas.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Expropiación de la banca y los recursos naturales.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },//////////////////////////2
         {
            "t13respuesta": "Dentro de estos núcleos agrícolas no existía la propiedad privada.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Estas extensiones de tierra no podían ser adquiridas para la construcción o explotación de los recursos naturales.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Los ejidatarios controlan su extensión de tierra sin intervención gubernamental.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "El neoliberalismo se basa en el libre mercado.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }

      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>¿Cuáles fueron las medidas del gobierno mexicano para evitar la fuga de capitales?",
                         "El modelo neoliberal adoptado por México consiste entre otras cosas en:",
                         "El modelo de ejidatarios y comuneros frena el avance del neoliberalismo ya que:"],
         "preguntasMultiples": true
      }
    },
    //5
    {
       "respuestas": [
           {
               "t13respuesta": "F",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "V",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En 1988 se creó el CONACULTA con el propósito de promocionar y apoyar eventos que impulsen el arte y la cultura",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El consumismo se genera de acuerdo a las necesidades de las personas, el cuidado ambiental y la responsabilidad social.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La cultura se ha estandarizado a nivel global.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Para Carlos Monsiváis la identidad nacional es resultado de una política gubernamental instrumentada para la unión del pueblo mexicano.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La identidad pluricultural se basa en la conformación de una identidad cultural con referencia a un solo grupo social y étnico.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las series televisivas estadounidenses que se transmitieron en México influyeron en el prototipo de familia citadina adoptado por el cine mexicano.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La cultura mexicana no tiene influencia en las costumbres y modos de vida estadounidenses.",
               "correcta"  : "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
       }
     },
     //6
     {
         "respuestas": [
             {
                 "t13respuesta": "1989",
                 "t17correcta": "0"
             },
             {
                 "t13respuesta": "1991",
                 "t17correcta": "1"
             },
             {
                 "t13respuesta": "1974",
                 "t17correcta": "2"
             },
             {
                 "t13respuesta": "1974-1980",
                 "t17correcta": "3"
             },
             {
                 "t13respuesta": "1947",
                 "t17correcta": "4"
             },
             {
                 "t13respuesta": "1970-1976",
                 "t17correcta": "5"
             },
             {
                 "t13respuesta": "1994",
                 "t17correcta": "6"
             }
         ],
         "preguntas" : [
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Caída del Muro de Berlín",
                 "correcta"  : "0"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Aprobación de la ONU de la carta de Derechos y Deberes de los Estados presentada por México.",
                 "correcta"  : "2"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Disolución de la URSS",
                 "correcta"  : "1"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Entra en vigor el Tratado de Libre Comercio",
                 "correcta"  : "6"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Periodo de gobierno de Luis Echeverría",
                 "correcta"  : "5"
             },
             {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta": "Fin de la Guerra Fría",
                 "correcta"  : "4"
             }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "13",
             "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada época en que ocurrieron los siguientes hechos.",
             "descripcion":"Año",
             "evaluable":false,
             "evidencio": false,
             "anchoColumnaPreguntas":"35"
         }
     },
     //7
     {
       "respuestas":[
          {
             "t13respuesta": "Pobreza, marginación y falta de oportunidades para acceder a un mejor nivel de vida.",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Pobreza, igualdad, marginación.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Pobreza, infraestructura y falta de recursos naturales.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },/////////////////////////////1
          {
             "t13respuesta": "Igualdad social.",
             "t17correcta": "1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Calidad de vida.",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Nacionalismo.",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },////////////////////2
          {
             "t13respuesta": "Contingencia ambiental.",
             "t17correcta": "1",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta": "Resiliencia ambiental.",
             "t17correcta": "0",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta": "Conurbación ambiental.",
             "t17correcta": "0",
             "numeroPregunta":"2"
          },//////////////////////3
          {
             "t13respuesta": "Sistema Cutzamala",
             "t17correcta": "1",
             "numeroPregunta":"3"
          },
          {
             "t13respuesta": "Sistema Xochimilco",
             "t17correcta": "0",
             "numeroPregunta":"3"
          },
          {
             "t13respuesta": "Sistema Hídrico de la Ciudad de México",
             "t17correcta": "0",
             "numeroPregunta":"3"
          },////////////////////////4
          {
             "t13respuesta": "Seguro Popular",
             "t17correcta": "1",
             "numeroPregunta":"4"
          },
          {
             "t13respuesta": "ISSSTE",
             "t17correcta": "0",
             "numeroPregunta":"4"
          },
          {
             "t13respuesta": "IMSS",
             "t17correcta": "0",
             "numeroPregunta":"4"
          },////////////////////////5
          {
             "t13respuesta": "Programa Internacional de Evaluación de los Alumnos.",
             "t17correcta": "1",
             "numeroPregunta":"5"
          },
          {
             "t13respuesta": "Programa Internacional Educativo de Evaluación.",
             "t17correcta": "0",
             "numeroPregunta":"5"
          },
          {
             "t13respuesta": "Programa de las Naciones Unidas para el Desarrollo.",
             "t17correcta": "0",
             "numeroPregunta":"5"
          },////////////////////////////////6
          {
             "t13respuesta": "Consejo Nacional de Ciencia y Tecnología.",
             "t17correcta": "1",
             "numeroPregunta":"6"
          },
          {
             "t13respuesta": "Instituto de las Ciencias y la Tecnología.",
             "t17correcta": "0",
             "numeroPregunta":"6"
          },
          {
             "t13respuesta": "Desarrollo Tecnológico y Científico Mexicano.",
             "t17correcta": "0",
             "numeroPregunta":"6"
          },////////////////////////////7
          {
             "t13respuesta": "Cultura de la legalidad",
             "t17correcta": "1",
             "numeroPregunta":"7"
          },
          {
             "t13respuesta": "Derechos humanos",
             "t17correcta": "0",
             "numeroPregunta":"7"
          },
          {
             "t13respuesta": "Constitucionalismo",
             "t17correcta": "0",
             "numeroPregunta":"7"
          },///////////////////////////8
          {
             "t13respuesta": "Vicente Fox, 2000",
             "t17correcta": "1",
             "numeroPregunta":"8"
          },
          {
             "t13respuesta": "Felipe Calderón, 2006",
             "t17correcta": "0",
             "numeroPregunta":"8"
          },
          {
             "t13respuesta": "Felipe Calderón, 2000",
             "t17correcta": "0",
             "numeroPregunta":"8"
          }

       ],
       "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Son algunas de las problemáticas del México actual.",
                          "Es aquello que no distingue de clases sociales, busca que todos los miembros de una nación gocen de las mismas oportunidades, sin discriminación y respetando su individualidad.",
                          "Debido a la sobreexplotación de los recursos naturales, la falta de conciencia ambiental y la excesiva contaminación nos encontramos en una...",
                          "El abastecimiento de agua potable para la capital del país se da por medio del siguiente sistema, considerado una obra de alta complejidad.",
                          "Los sistemas de salud han alcanzado una cobertura extensa de la población mexicana debido al siguiente programa creado en 2003:",
                          "El siguiente programa ubica a la población estudiantil con carencias graves en comprensión lectora y matemática en México.",
                          "En México, el instituto encargado del desarrollo tecnológico y científico es: ",
                          "Es el estricto respeto y obediencia de las leyes que debe tener un país con sus habitantes, en el entendido de que sobre este principio se debe construir una nación para alcanzar el desarrollo y prosperidad.",
                          "La transición democrática en México se logró en qué año y con cuál presidente."],
          "preguntasMultiples": true
       }
     },
     //8
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la Revolución Mexicana se popularizaron los corridos en los cuales se contaban historias exaltando proezas de los revolucionarios.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para la década de los setenta surgiría el género de protesta ante las dictaduras de Sudamérica y contra el capitalismo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La música alemana del Rock and Roll tiene gran influencia en México.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las redes sociales han creado un mundo virtual de comunicaciones y relaciones con otras personas para intercambiar información, generar redes y comercializar productos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la Revolución Industrial surgió la invención del radio, teléfono y televisión.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }
      }
];
