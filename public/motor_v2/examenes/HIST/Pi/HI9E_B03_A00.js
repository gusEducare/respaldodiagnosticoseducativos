json = [
//1
{
       "respuestas": [
           {
               "t13respuesta": "<p>Primera revolución industrial en Inglaterra.<\/p>",
               "t17correcta": "0",
               etiqueta:"paso 1"
           },
           {
               "t13respuesta": "<p>Consumación de la independencia mexicana. <\/p>",
               "t17correcta": "1",
               etiqueta:"paso 2"
           },
           {
               "t13respuesta": "<p>Anexión de Texas a Estados Unidos.<\/p>",
               "t17correcta": "2",
               etiqueta:"paso 3"
           },
           {
               "t13respuesta": "<p>Intervención Estadounidense en México.<\/p>",
               "t17correcta": "3",
               etiqueta:"paso 4"
           },
           {
               "t13respuesta": "<p>Primer ferrocarril mexicano.<\/p>",
               "t17correcta": "4",
               etiqueta:"paso 5"
           },
           {
               "t13respuesta": "<p>Segunda revolución industrial.<\/p>",
               "t17correcta": "5",
               etiqueta:"paso 6"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "9",
           "t11pregunta": "<p><strong>Ordena los hechos históricos cronológicamente.</strong><br><\/p>",
       }
   },
   //2
   {
     "respuestas":[
        {
           "t13respuesta":"Primera revolución industrial en Inglaterra.",
           "t17correcta":"1"
        },
        {
           "t13respuesta":"Primera revolución industrial en Alemania.",
           "t17correcta":"0"
        },
        {
           "t13respuesta":"Segunda revolución industrial. ",
           "t17correcta":"0"
        },
        {
           "t13respuesta":"Segunda Guerra Mundial.",
           "t17correcta":"0"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11pregunta":"<strong>Selecciona la respuesta correcta.</strong><br></br>Señala el hecho histórico que da inicio en la segunda mitad del S. XVIII."
     }
  },
  //3
  {
       "respuestas":[
          {
             "t13respuesta":     "Consumación de la independencia mexicana y la anexión de Texas a Estados Unidos.",
             "t17correcta":"1"
          },
          {
             "t13respuesta":     "Intervención estadounidense en México.",
             "t17correcta":"1"
          },
          {
             "t13respuesta":     "Segunda revolución industrial.",
             "t17correcta":"0"
          },
          {
             "t13respuesta":     "Primer ferrocarril mexicano.",
             "t17correcta":"0"
          }
       ],
       "pregunta":{
          "c03id_tipo_pregunta":"2",
          "dosColumnas": false,
          "t11pregunta":"<strong>Selecciona todas las respuestas correctas.</strong><br></br>Selecciona los hechos históricos ocurridos en las primeras cinco décadas del S. XIX."
       }
    },
    //4
    {
      "respuestas":[
         {
            "t13respuesta":"El primer ferrocarril mexicano y la segunda revolución industrial.",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"La consumación de la independencia mexicana y la anexión de Texas a Estados Unidos.",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"Primer revolución industrial e intervención estadounidense en México.",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"Primer revolución industrial y el primer ferrocarril mexicano.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"<strong>Selecciona la respuesta correcta.</strong><br></br>Son hechos históricos ocurridos entre 1861 y 1914."
      }
   },
   //5
   {
        "respuestas":[
           {
              "t13respuesta":     "Problemas con la industria textil y minera.",
              "t17correcta":"1",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Supresión del pago de tributo de los indígenas.",
              "t17correcta":"1",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Las clases ricas deseaban un gobierno monárquico. ",
              "t17correcta":"1",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Los indígenas deseaban un gobierno teocrático. ",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Se suprime la deuda externa.",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Republicanos",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Borbonistas",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Iturbidistas",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Monárquicos ",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Renacentistas",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "División del poder ejecutivo, legislativo y judicial.",
              "t17correcta":"1",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"19 estados, 4 territorios y un Distrito Federal.",
              "t17correcta":"1",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"Una república.",
              "t17correcta":"1",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"La república conformada por 32 estados y un Distrito Federal.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"El catolicismo como única religión.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           }
        ],
        "pregunta":{
           "c03id_tipo_pregunta":"2",
           "dosColumnas": false,
           "t11pregunta":["<strong>Selecciona las respuestas correctas.</strong><br></br>Son consecuencias del fin de la guerra de independencia en México:",
                          "El Congreso Constituyente estaba conformado  por las siguientes fuerzas políticas:",
                          "En 1824 el Congreso realiza la primera acta constitucional de los Estados Unidos Mexicanos que establece lo siguiente:"],
           "preguntasMultiples": true
        }
     },
     //6
     {
       "respuestas": [
           {
               "t13respuesta": "Falso",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Verdadero",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El presidente Guadalupe Victoria logró el reconocimiento de México como país por parte de  Inglaterra y Estados Unidos.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En 1836 Santa Anna reconoce la independencia de Texas en el “Tratado de Velasco”.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Francia realiza un bloqueo económico a México en 1838  debido a la falta de indemnización del gobierno mexicano a un pastelero francés.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En el gobierno de Benito Juárez se implementó el telégrafo, el ferrocarril e incluso el teléfono.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En el gobierno de Díaz el erario público se caracteriza por su desorganización, especialmente en el área de comercio.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El gobierno de Díaz permitió un ambiente de trabajo justo que protegía a los trabajadores y exigía altos impuestos a las empresas.",
               "correcta"  : "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<strong>Selecciona verdadero (V) o falso (F) según corresponda.</strong>",
           "descripcion":"Reactivo",
           "evaluable":false,
           "evidencio": false
       }
   },
   //7
   {
        "respuestas":[
           {
              "t13respuesta":"Conservador",
              "t17correcta":"1",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta": "Liberal",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta": "Democrático",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":"Constitucional",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Liberal",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Conservador",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Democrático",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Repúblicano",
              "t17correcta":"0",
              "numeroPregunta":"1"
           },
           {
              "t13respuesta":  "Amplía las garantías y libertades de las personas, establece una forma de gobierno republicana, democrática y representativa.",
              "t17correcta":"1",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"Reduce las garantías y libertades de las personas, establece  una forma de gobierno monárquica y la religión católica como la única en el país.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":     "Amplía las garantías y libertades de las personas, establece la monarquía.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"Reduce las garantías y libertades de las personas, establece un gobierno parlamentario.",
              "t17correcta":"0",
              "numeroPregunta":"2"
           },
           {
              "t13respuesta":"Plan de Tacubaya",
              "t17correcta":"1",
              "numeroPregunta":"3"
           },
           {
              "t13respuesta":"Plan de Iguala",
              "t17correcta":"0",
              "numeroPregunta":"3"
           },
           {
              "t13respuesta":"Plan Trigarante",
              "t17correcta":"0",
              "numeroPregunta":"3"
           },
           {
              "t13respuesta":"Plan de San Luis",
              "t17correcta":"0",
              "numeroPregunta":"3"
           }
        ],
        "pregunta":{
           "c03id_tipo_pregunta":"1",
           "dosColumnas": false,
           "t11pregunta":["<strong>Selecciona la respuesta correcta.</strong><br></br>Partido político que proponía una monarquía constitucional para mantener los privilegios del ejército, la iglesia y los terratenientes.  Su líder era Lucas Alamán:",
                          "Partido político que buscaba la igualdad entre los habitantes de México ante la ley y libertad de culto.",
                          "Son características de la Constitución de 1857:",
                          "Es el documento por el cual se desconoce la Constitución de 1857."],
           "preguntasMultiples": true
        }
     },
     //8
     {
       "respuestas": [
           {
               "t13respuesta": "Falso",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Verdadero",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El porfiriato se caracterizó por la represión hacia la prensa y el uso excesivo de la fuerza para quien estuviera en contra del gobierno.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Durante el porfiriato las inversiones extranjeras en el país eran principalmente de Francia, Estados Unidos e Inglaterra.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La caricatura política no forma parte de la cultura mexicana debido a que se extinguió en la época de la revolución.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Mediante la caricatura política las personas en México expresaban molestias e inconformidades ante la desigualdad social, los abusos y la pobreza del país.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "En 1910 se da inicio a lo que se considera la época de oro del cine mexicano.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Pedro Infante, Emilio “Indio” Fernández, Jorge Negrete, Maria Félix, Tin Tan y Cantinflas fueron parte de la  época de oro del cine mexicano.",
               "correcta"  : "1"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<strong>Selecciona verdadero (V) o falso (F) según corresponda.</strong>",
           "descripcion":"Reactivo",
           "evaluable":false,
           "evidencio": false
       }
     },
     //9
     {
          "respuestas":[
             {
                "t13respuesta":"Gerardo MurillO “Dr. Atl” y José María Velasco",
                "t17correcta":"1",
                "numeroPregunta":"0"
             },
             {
                "t13respuesta": "Vasconcelos y Gómez Morín",
                "t17correcta":"0",
                "numeroPregunta":"0"
             },
             {
                "t13respuesta": "Diego Rivera  y Gerardo Murillo  “Dr. Atl” ",
                "t17correcta":"0",
                "numeroPregunta":"0"
             },
             {
                "t13respuesta":"Tamayo y José María Velasco",
                "t17correcta":"0",
                "numeroPregunta":"0"
             },
             {
                "t13respuesta":"Clasicismo ",
                "t17correcta":"0",
                "numeroPregunta":"1"
             },
             {
                "t13respuesta":"Modernismo",
                "t17correcta":"0",
                "numeroPregunta":"1"
             },
             {
                "t13respuesta": "Romanticismo",
                "t17correcta":"0",
                "numeroPregunta":"1"
             },
             {
                "t13respuesta":"Barroco",
                "t17correcta":"0",
                "numeroPregunta":"1"
             },
             {
                "t13respuesta":  "Romanticismo",
                "t17correcta":"1",
                "numeroPregunta":"2"
             },
             {
                "t13respuesta":"Clasicismo ",
                "t17correcta":"0",
                "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Modernismo",
                "t17correcta":"0",
                "numeroPregunta":"2"
             },
             {
                "t13respuesta":"Barroco",
                "t17correcta":"0",
                "numeroPregunta":"2"
             },
             {
                "t13respuesta":"Modernismo",
                "t17correcta":"1",
                "numeroPregunta":"3"
             },
             {
                "t13respuesta":"Romanticismo",
                "t17correcta":"0",
                "numeroPregunta":"3"
             },
             {
                "t13respuesta":"Clasicismo ",
                "t17correcta":"0",
                "numeroPregunta":"3"
             },
             {
                "t13respuesta":"Barroco",
                "t17correcta":"0",
                "numeroPregunta":"3"
             },
             {
                "t13respuesta":"La creación de la Escuela Normal de 1886, la unificación del método de enseñanza en 1889, la educación básica gratuita y laica y reformas a la Ley para permitir a las mujeres el acceso a las carreras profesionales.",
                "t17correcta":"0",
                "numeroPregunta":"4"
             },
             {
                "t13respuesta":"La unificación del método de enseñanza en 1889, la educación superior gratuita y laica para hombres y la creación del Instituto de Ciencias y Biología de México.",
                "t17correcta":"0",
                "numeroPregunta":"4"
             },
             {
                "t13respuesta":"La educación superior gratuita y laica para hombres, la creación de la Escuela Normal en 1886 y la unificación del método de enseñanza en 1889.",
                "t17correcta":"0",
                "numeroPregunta":"4"
             },
             {
                "t13respuesta":"La educación superior gratuita y laica para hombres, la creación de la Escuela de Ingeniería del IPN para desarrollar más y mejores vías ferroviarias  y la unificación del método de enseñanza en 1980.",
                "t17correcta":"0",
                "numeroPregunta":"4"
             }
          ],
          "pregunta":{
             "c03id_tipo_pregunta":"1",
             "dosColumnas": false,
             "t11pregunta":["<strong>Selecciona la respuesta correcta.</strong><br></br>Pintores que representaron el “Paisajismo” durante el porfiriato en México:",
                            "El siguiente estilo fue traído por los españoles al inicio de la colonia a México y se caracterizó por la evocación del arte griego y romano:",
                            "Es un estilo artístico de origen alemán e inglés que se impone en  el México independiente para romper con el dominio español:",
                            "El siguiente estilo artístico transforma la vida política, cultural y social en México, surge también con los simbolistas:",
                            "Fueron algunas acciones realizadas durante  el gobierno de Díaz en torno a la educación:"],
             "preguntasMultiples": true
          }
       },
];
