json = [
// //1
    {
        "respuestas": [
            {
                "t13respuesta": "1953",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "1920",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "1968",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "1917",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1910 -1917",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "1926",
                "t17correcta": "5"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho al voto de la mujer",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Movimiento estudiantil y matanza de Tlatelolco",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Inicio de la reconstrucción nacional",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Inicio de la Guerra Cristera",
                "correcta"  : "5"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Inicio y fin de la Revolución nacional",
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Promulgación de nueva Constitución en México",
                "correcta"  : "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada hecho según la época en que sucedió.<\/p>",
            "descripcion":"Hechos",
            "evaluable":false,
            "evidencio": false,
            "anchoColumnaPreguntas":"30"
        }
    },
    //2
    {
      "respuestas":[
         {
            "t13respuesta": "Emiliano Zapata",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Francisco I. Madero",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Lerdo de Tejada",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "20 de noviembre de 1908 con el Plan de Iguala",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "20 de noviembre de 1910 con el Plan de San Luis",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "20 de noviembre de 1910 con el Plan de Norte",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Villistas, Zapatistas y Carrancistas",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Zapatistas, Carrancistas y Cardenistas",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
          
         {
            "t13respuesta": "Obregonistas, Peronista y Villistas.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Francisco I. Madero",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "Pascual Orozco",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "Porfirio Díaz",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "Emiliano Zapata",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta": "Francisco Villa",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta": "José Vasconcelos",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
       
         {
            "t13respuesta": "Venustiano Carranza",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {
            "t13respuesta": "Francisco Villa",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {
            "t13respuesta": "Emiliano Zapata",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {
            "t13respuesta": "Venustiano Carranza",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {
            "t13respuesta": "Francisco Villa",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {
            "t13respuesta": "Emiliano Zapata",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {
            "t13respuesta": "Educación, economía nacional, salubridad general, relación entre obreros y patrones, propiedad de la nación sobre sus recursos naturales.",
            "t17correcta": "1",
            "numeroPregunta":"7"
         },
         {
            "t13respuesta": "Economía externa, salubridad general, propiedad de la nación sobre sus recursos naturales, derechos humanos e interculturalidad.",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {
            "t13respuesta": "Administración ferroviaria, Plan Nacional de Importaciones y exportaciones, Ley de Salud universal, Educación y Vivienda",
            "t17correcta": "0",
            "numeroPregunta":"7"
         }

      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br>Crea el Partido “Antireleccionista” en México”:",
                         "Madero desconoce al presidente Díaz y hace el llamado a la Revolución en la siguiente fecha y con el siguiente Plan.",
                         "En 1914 los territorios mexicanos estaban controlados por los grupos.",
                         "Ganó las elecciones de 1911 en México.",
                         "Durante la Revolución Mexicana fue el líder de la región o división del Norte.",
                         "Llega a la presidencia de México  después de derrotar  a Pancho Villa.",
                         "Es asesinado en una emboscada el 20 de Julio de 1923 en México.",
                         "Los principales temas que contempló la Constitución de 1917  son:",      ],

                         
         "preguntasMultiples": true
      }
   },
   //3
   {
       "respuestas": [
           {
               "t13respuesta": "1926",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "1918",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "1934-1982",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "1917-1924",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "1940",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "1928-1934",
               "t17correcta": "5"
           },
           {
               "t13respuesta": "1938",
               "t17correcta": "6"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Modificaciones a la ley mexicana para disminuir el poder de la iglesia",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Presidencialismo",
               "correcta"  : "2"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Fundación de la primera Confederación  Regional Obrera Mexicana CROM",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Fundación de la Confederación Nacional Campesina",
               "correcta"  : "6"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Maximato",
               "correcta"  : "5"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Caudillismo",
               "correcta"  : "3"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El presidente Manuel Ávila Camacho desprotege el campo mexicano",
               "correcta"  : "4"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada hecho según la época en que ocurrió.<\/p>",
           "descripcion":"Hechos",
           "evaluable":false,
         "evidencio": false,
           "anchoColumnaPreguntas":"30",
           "columnaflexibles":"30"
       }
   },
   //4
   {
      "respuestas": [
          {
              "t13respuesta": "Falso",
              "t17correcta": "0"
          },
          {
              "t13respuesta": "Verdadero",
              "t17correcta": "1"
          }
      ],
      "preguntas" : [
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "El presidente Manuel Ávila Camacho tuvo una política presidencial dirigida al sector industrial y su desarrollo.",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "La Segunda Guerra Mundial fue un obstáculo para el desarrollo de México e impidió su crecimiento. ",
              "correcta"  : "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En la primera mitad del siglo XX, México decidió gravar con impuestos altos los productos importados para incentivar el consumo nacional.",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "El milagro mexicano se caracterizó por ser un periodo en donde el desarrollo económico de México logró un nivel muy alto.",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "La petrolización de la economía en México sucede en el año de 1913.",
              "correcta"  : "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "El proteccionismo en México tuvo como consecuencia la limitación de las tecnologías provenientes del extranjero, un rezago en este sector y la desaceleración industrial.",
              "correcta"  : "1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda:<\/p>",
          "descripcion":"Aspectos a valorar",
          "evaluable":false,
          "evidencio": false
      }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "1980",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "1968",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "1939",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "1935",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1954",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "1953",
                "t17correcta": "5"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Explosión demográfica en México",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fundación del Partido Acción Nacional",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Movimiento estudiantil  y matanza de Tlatelolco",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Voto de la mujer e igualdad  frente a los hombres al considerarlos ciudadanos en México",
                "correcta"  : "5"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fundación del Frente Único Pro de Derechos de la Mujer",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fundación del Partido Auténtico de la Revolución Mexicana (PARM)",
                "correcta"  : "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada hecho según la época en que ocurrió.<\/p>",
            "descripcion":"Hechos",
            "evaluable":false,
            "evidencio": false,
            "anchoColumnaPreguntas":"20"
        }
    },
    //6
    {
      "respuestas":[
         {
            "t13respuesta": "Primera Guerra Mundial de 1914 a 1918, mientras que en  México la Revolución Mexicana.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Primera Guerra Mundial de 1912 a 1915, mientras que en México la Guerra Cristera.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "La Revolución Rusa (1917), mientras que México vivía su movimiento de independencia.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Telegrama enviado por Arthur Zimmerman.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Telegrama de la embajada europea.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": " Carta de asuntos internacionales alemana.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Japón",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Alemania",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Estados Unidos",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "España, por la Guerra Civil Española.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "Alemania, por la Primera Guerra Mundial.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "Refugio a Judíos, por la Segunda Guerra Mundial. ",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "Política Interior Proteccionista",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta": "Política Exterior Pacifista",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta": "Política Exterior de Ataque",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {
            "t13respuesta": "Miguel de la Madrid",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {
            "t13respuesta": "Manuel Avila Camacho",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {
            "t13respuesta": "Lazaro Cardenas",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {
            "t13respuesta": "Alemania",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {
            "t13respuesta": "Japón",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {
            "t13respuesta": "Estados Unidos",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
        
         {
            "t13respuesta": "El bloque  occidental capitalista liderado por Estados Unidos y el bloque del Este con una postura comunista liderado por la Unión Soviética",
            "t17correcta": "1",
            "numeroPregunta":"7"
         },
         {
            "t13respuesta": "El bloque comunista liderado por Alemania del Este y China  y el bloque oriental liderado por Estados Unidos con una postura capitalista.",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {
            "t13respuesta": "El bloque liberal de España-Francia- Inglaterra y el bloque socialista de Corea del Norte-China-URSS",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {
            "t13respuesta": "Revolución Mexicana",
            "t17correcta": "0",
            "numeroPregunta":"8"
         },
         {
            "t13respuesta": "Revolución Chilena",
            "t17correcta": "0",
            "numeroPregunta":"8"
         },
         {
            "t13respuesta": "Revolución Cubana",
            "t17correcta": "1",
            "numeroPregunta":"8"
         }

      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br>La Primera Guerra Mundial estallaría en los siguientes años y en México se presentaba el siguiente suceso:",
                         "En este documento se indica el acercamiento del gobierno alemán con el gobierno mexicano para generar una alianza en contra de Estados Unidos en la Primera Guerra Mundial.",
                         "En la Primera Guerra Mundial, el gobierno de Venustiano Carranza decide aliarse con:",
                         "En el año de 1936 el gobierno mexicano dio asilo a los refugiados de qué país y por qué causa:",
                         "Ante la Segunda Guerra Mundial, el gobierno de Lázaro Cárdenas decreta la política de:",
                         "En 1940 llega como presidente al gobierno Mexicano en un ambiente de paz y estabilidad.",
                         "En 1942 este país lanzó torpedos hundiendo un buque petrolero mexicano.",
                         "La Guerra Fría estaba conformada por los bandos:",
                         "Fue un movimiento armado que buscó terminar con la dictadura establecida por Fulgencio Batista, su líder era Fidel Castro."],
         "preguntasMultiples": true
      }
   },
   //7
   {
      "respuestas": [
          {
              "t13respuesta": "Falso",
              "t17correcta": "0"
          },
          {
              "t13respuesta": "Verdadero",
              "t17correcta": "1"
          }
      ],
      "preguntas" : [
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En la década de los cuarenta México comenzó a tener un desarrollo económico que aceleró el desarrollo y el cambio en las principales ciudades. ",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En la década de los cuarenta cambiaron los patrones de consumo de los mexicanos que incorporaron  tecnología y productos de otros países a su vida cotidiana. ",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En la década de los cuarenta existió la característica de la permanencia de la población mexicana en su territorio y la  falta de migración hacia otros países. ",
              "correcta"  : "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En México, los medios de comunicación se transformaron para dar paso a la radio y la televisión durante la tercera década del siglo XX.",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En 1970 la mayoría de las personas en México no tenían acceso a la televisión.",
              "correcta"  : "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "El muralismo mexicano consistió en pintar los exteriores de grandes edificios con el objetivo de luchar por la libertad de expresión en el país.",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Juan Rulfo escribió su novela “El Zarco” y Manuel Altamirano “El llano en llamas” , siendo así representantes de la literatura mexicana. ",
              "correcta"  : "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En 1929 se consagra la Universidad Autónoma de México.",
              "correcta"  : "1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda:<\/p>",
          "descripcion":"Aspectos a valorar",
          "evaluable":false,
          "evidencio": false
      }
    },
    //8
    {
      "respuestas":[
         {
            "t13respuesta": "Se consigue el derecho al voto de la mujer, se participa en elecciones municipales, distribuyen propaganda y trabajan en las fábricas junto con los obreros, algunas también participan en el movimiento armado.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Se organizan los clubes femeniles y participan en los círculos de oposición, distribuyen propaganda y se expresan en la prensa, algunas también participan en el movimiento armado.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Se implantan las primeras industrias lideradas por mujeres, exigen la ilustración mexicana y dotan de estrategias bélicas a los soldados.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "A finales de la primera década del siglo XX",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "A inicios de la primera década del siglo XIX",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Durante los años 50´s",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "Hermilda Sanchez",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Adela Cortina",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "Hermila Galindo",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "1995",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "1970",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta": "1940",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }

      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Son algunas de las participaciones y logros de la mujer durante  y a  finales del Porfiriato.",
                         "En qué década se empiezan a formar los grupos feministas que defienden el derecho de las mujeres. ",
                         "Es una de las máximas representantes del feminismo mexicano.  ",
                         "Fue una época en la que las mujeres se promulgaron por reclamar la opresión femenina, las desigualdades y la reivindicación de sus derechos."],
         "preguntasMultiples": true
      }
   }
];
