json = [
	// Reactivo 1
	{
        "respuestas": [
            {
                "t13respuesta": "<p>Hechos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Opiniones<\/p>",
                "t17correcta": "2"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><table class='table' style='margin-top:-40px;'><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Información o datos objetivos reales y comprobables.</td><td>Percepción personal que se tiene sobre un tema.</td></tr><tr><td>Se presentan en las noticias y en los resultados de una investigación.</td><td>Son apreciaciones subjetivas que reflejan la forma en que una persona valora e interpreta algo.</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras Hechos y Opiniones a la columna que corresponda.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true,
        }
	},
	// Reactivo 2
	{
        "respuestas": [{
                "t13respuesta": "Hecho",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opinión",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los diputados se reunieron para hacer propuestas que apoyen a reducir el uso de platos y vasos desechables.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La mayoría de las personas son flojas y prefieren utilizar platos desechables para no tener que lavar.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los platos y vasos desechables son elaborados con poliestireno, polietileno, polipropileno y polímero de plásticos no biodegradable.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 2016, en Francia se prohibió la venta de platos y cubiertos desechables hechos de plástico.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los restaurantes de comida rápida deberían dejar de utilizar platos y vasos desechables para reducir la contaminación.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee los siguientes enunciados y selecciona (O) si el enunciado corresponde a una opinión, o (H) si se trata de un hecho.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
	},
	// Reactivo 3
	{
        "respuestas": [{
                "t13respuesta": "Se basan en ideas y creencias aceptadas como verdaderas por la sociedad.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se retoman experiencias compartidas como sociedad o sentimientos en común para lograr empatía con el lector y conseguir la aceptación.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Utilizan acontecimientos reales y comprobables para defender una idea. Estos incluyen citas, datos y resultados de investigaciones.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Utilizan declaraciones que confirman lo que se quiere demostrar.",
                "t17correcta": "4"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Argumentos racionales"
            },
            {
                "t11pregunta": "Argumentos que apelan a los sentimientos"
            },
            {
                "t11pregunta": "Argumentos de hechos"
            },
            {
                "t11pregunta": "Argumentos de ejemplificación"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada tipo de argumento con su descripción."
        }
	},
	// Reactivo 4
	{
        "respuestas": [{
                "t13respuesta": "Lo viejo era mejor, mi abuelita tenía un televisor que le duró casi 20 años. Ella lo cuidaba mucho, siempre revisaba su buen funcionamiento y sólo una ocasión lo tuvo que llevar a reparar porque se le cayó el botón de encendido.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El avance tecnológico ha beneficiado enormemente el desarrollo de la sociedad. Actualmente, es muy fácil poder adquirir nuevos productos tecnológicos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Las empresas diseñan productos nuevos con frecuencia y crean estrategias para obligarnos a comprarlos. Por ejemplo, algunas aplicaciones en los teléfonos celulares no pueden ser actualizadas, si no se cuenta con un modelo de teléfono reciente.  Esto obliga al usuario a adquirir uno nuevo, pues su aparato ya es obsoleto.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la opción que sea un  argumento en contra de la obsolescencia programada."
        }
    },
	// Reactivo 5
	{
        "respuestas": [{
                "t13respuesta": "Hacer todo lo posible por llamar la atención.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Evitar el contacto directo con los asistentes, para no intimidarlos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Concluir con una frase contundente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presentar con seguridad la idea principal y los argumentos que la avalan.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presentar únicamente la idea principal para no desviar la atención.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Concluir con un chiste para afianzar la situación.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Establecer contacto directo con los asistentes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Mantener la atención.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee los siguientes enunciados y selecciona todas  las estrategias para persuadir."
        }
	},
	// Reactivo 6
	{
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para persuadir a la audiencia es indispensable levantar la voz y mostrar una actitud agresiva.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una estrategia persuasiva consiste en mostrar estadísticas para fundamentar nuestros argumentos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una vez que se obtuvo la atención de la audiencia, es importante mantenerlos interesados.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es importante exponer los argumentos, sin importar si la audiencia los comprende o no.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al presentar un argumento hay que utilizar tecnicismos y palabras complicadas para mostrar seguridad y dominio del tema.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una estrategia de persuasión consiste en mostrar información recopilada en investigaciones previas.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
	},
	// Reactivo 7
	{
        "respuestas": [{
                "t13respuesta": "Al participar en un panel de discusión, es importante comunicar las ideas de manera ordenada.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No es necesario utilizar lenguaje formal.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se debe levantar el tono de voz para sobresalir ante el resto del grupo e imponer puntos de vista.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Es fundamental respetar los turnos de participación de los participantes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Si algún participante presenta algún argumento con el que no se esté de acuerdo, se debe interrumpir para debatir al respecto.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se debe utilizar lenguaje formal para dirigirse a todos los participantes.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee los siguientes enunciados y selecciona aquellos que mencionen conductas adecuadas durante la participación en un panel de discusión."
        }
	},
	// Reactivo 8
	{
        "respuestas": [{
                "t13respuesta": "Es un texto muy breve en el que el autor escribe a quién le dedica u ofrece su obra.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es el texto que antecede a la lectura y pretende guiar al lector acerca de lo que encontrará en la misma.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Es información que el autor presenta al lector para que la tome en cuenta antes de leer el material.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Es un escrito del autor en el que se encuentra información pertinente sobre la obra.",
                "t17correcta": "4"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Dedicatoria"
            },
            {
                "t11pregunta": "Prólogo"
            },
            {
                "t11pregunta": "Advertencia"
            },
            {
                "t11pregunta": "Presentación"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada descripción con el elemento que corresponda."
        }
	},
	// Reactivo 9
	{
        "respuestas": [{
                "t13respuesta": "<p>Dedicatoria<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Advertencia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Presentación<\/p>",
                "t17correcta": "3"
            },
        ],
        "preguntas": [{
                "t11pregunta": ""
            },
            {
                "t11pregunta": "<br>Dedico este libro a mi esposa, quien fue mi guía en este arduo trabajo, y a mis hijos que fueron mi inspiración. Asimismo, a mis maestros quienes dejaron en mí una huella que me motivó a seguir escribiendo.<br><br>"
            },
            {
                "t11pregunta": ".<br>El contenido de este libro presenta información no apta para menores de 15 años.<br><br>"
            },
            {
                "t11pregunta": "<br>En este libro, el lector encontrará una recopilación de cuentos y poemas escritos por autores latinoamericanos. La cuidadosa selección de los textos aquí presentados fue supervisada por el maestro en Letras Latinoamericanas, Luis Medina Alfaro."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la palabra que corresponda a cada descripción.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
	},
	// Reactivo 10
	{
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su autor se presume como un ingenioso escritor.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El autor presenta un ambiente tranquilo y placentero.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El autor habría querido presentar un libro más hermoso.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las musas presentes en la obra se presumen con esculturales cuerpos.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Realiza la lectura de los fragmentos del prólogo de Don Quijote de la Mancha,  obra de Miguel de Cervantes Saavedra. Después, selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
			"evidencio": false,
			"t11instruccion": "“Despreocupado lector: sin juramento me podrás creer que quisiera que este libro, como hijo del entendimiento, fuera el más hermoso, el más gallardo y más discreto que pudiera imaginarse.  Pero no he podido contravenir al orden de naturaleza, que en ella cada cosa engendra su semejante. Y, así, ¿qué podía engendrar el estéril y mal cultivado ingenio mío, sino la historia de un hijo seco, avellanado, antojadizo y lleno de pensamientos varios y nunca imaginados de otro alguno?” [...]<br><br>[...] “El sosiego, el lugar apacible, la amenidad de los campos, la serenidad de los cielos, el murmurar de las fuentes, la quietud del espíritu son grande parte para que las musas más estériles se muestren fecundas”  [...] <br><br>Cvc.cervantes.es. (2017). CVC. «Don Quijote de la Mancha». Primera parte. Prólogo (1 de 2).. [online] Available at: https://cvc.cervantes.es/literatura/clasicos/quijote/edicion/parte1/prologo/default.htm [Accessed 9 Nov. 2017]."
        }
	},
	// Reactivo 11
	{
        "respuestas": [{
                "t13respuesta": "El autor confiesa que su texto es impreciso.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El autor asegura que su texto es exacto.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El autor anticipa que mostrará referencias en su texto.",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Luego de leer el siguiente párrafo, selecciona la opción que menciona la postura del autor.<br><br>(...) “Este resumen que publico ahora tiene necesariamente que ser imperfecto. No puedo dar aquí referencias y textos en favor de mis diversas afirmaciones, y tengo que contar con que el lector pondrá alguna confianza en mi exactitud. Sin duda se habrán deslizado errores, aunque espero que siempre he sido prudente en dar crédito tan sólo a buenas autoridades. No puedo dar aquí más que las conclusiones generales a que he llegado con algunos; hechos como ejemplos, que espero, sin embargo, serán suficientes en la mayor parte de los casos. (…)”<br><br>Anon, (2017). [ebook] Available at: http://www.rebelion.org/docs/81666.pdf [Accessed 9 Nov. 2017]."
        }
	},
	// Reactivo 12
	{
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El prólogo es el primer encuentro que se da entre el lector y la obra.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un libro puede prescindir del prólogo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El prólogo siempre se presenta de manera coloquial para afianzar el primer encuentro con entre el lector y la obra.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El prólogo suele animar al receptor a leer el material.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las formas de dirigirse al lector en un prólogo se denominan personal e indirecta.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
			"evidencio": false
        }
	},
	// Reactivo 13
	{
        "respuestas": [{
                "t13respuesta": "La función del prólogo es orientar al lector sobre el texto que va a leer.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presenta una reflexión, idea o conclusión de la publicación.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Generalmente aparece al final de la obra literaria.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Puede o no ser escrito por el autor.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las características de un prólogo."
        }
	},
	// Reactivo 14
	{
        "respuestas": [{
                "t13respuesta": "<p>datos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>institución<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>control<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>clase<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>pertenecer<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>responder<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>registrarse<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>documentos<\/p>",
                "t17correcta": "8"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Los "
            },
            {
                "t11pregunta": " a completar en los formularios varían según su propósito. Al llenarlos, estamos brindando a una "
            },
            {
                "t11pregunta": " la información necesaria y adecuada para poder recibir un servicio y para que esta pueda tener un "
            },
            {
                "t11pregunta": "de los participantes.<br><br>La información solicitada para inscribirse a una "
            },
            {
                "t11pregunta": " de natación, no será la misma que la que se pida para "
            },
            {
                "t11pregunta": " a una biblioteca, para "
            },
            {
                "t11pregunta": "una encuesta o para "
            },
            {
                "t11pregunta": " en una página de compras por internet. De igual forma, los "
            },
            {
                "t11pregunta": " probatorios requeridos cambiarán dependiendo el objetivo de llenado del formulario."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la palabra que corresponda a cada descripción.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
	},
	// Reactivo 15
	{
        "respuestas": [{
                "t13respuesta": "Cartilla de vacunación",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Certificado de secundaria",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Acta de nacimiento de ambos padres",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Licencia de conducir",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Comprobante de peso y talla",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Carta de buena conducta",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Acta de nacimiento",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todos los documentos probatorios que se requieren para la inscripción de un alumno a la preparatoria."
        }
	},
	// Reactivo 16
	{
        "respuestas": [{
                "t13respuesta": "Acta de nacimiento",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cartilla de vacunación",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Membresía de un club deportivo",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿De qué documento oficial se puede obtener la CURP de una persona para llenar un formulario?"
        }
	},
	// Reactivo 17
	{
        "respuestas": [{
                "t13respuesta": "Pasaporte vigente expedido por la Secretaría de Relaciones Exteriores",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Membresía de un club de precios",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Credencial para votar expedida por el Instituto Nacional Electoral",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Credencial escolar expedida por la institución educativa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Tarjeta de crédito que incluya el nombre del tarjetahabiente",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todos los documentos que se consideran identificaciones oficiales."
        }
	},
	// Reactivo 18
	{
        "respuestas": [{
                "t13respuesta": "Pamela Adriana H. Sánchez",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Pame Hernández Sánchez",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Pamela Adriana Hernández Sánchez",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Av. Morelos 345 interior 203. Colonia Casablanca, Toluca, Estado de México. C.P. 45678",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Toluca, Estado de México",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Av. Morelos 345. Toluca, Estado de México",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "HESP850912MDFNFB41",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "HESP850912",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "MDFNGL",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "pamehs@",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "pamehs@micorreo.com",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "PAMEHS@MICORREO.COM",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la opción correcta al introducir información en un formulario.<br><br>Nombre del participante:",
                "Dirección:",
                "CURP:",
                "Correo electrónico:"
            ],
            "preguntasMultiples": true
        }
	},
	// Reactivo 19
	{
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando en un formulario se pide el nombre se puede colocar el apodo con lo identifican los amigos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es importante colocar los datos personales para que el formulario sea válido.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los formularios se caracterizan por tener un lenguaje objetivo y utilizar oraciones en modo  impersonal.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El nombre en un formulario puede contener abreviaturas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En los formularios se utilizan sólo verbos en infinitivo.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
	},
	// Reactivo 20
	{
        "respuestas": [{
                "t13respuesta": "No pueden mostrarse físicamente los documentos probatorios que se solicitan.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No es necesario desplazarse.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El tiempo invertido en el trámite es menor que si fuera de forma personal.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El trámite se puede realizar desde cualquier lugar mientras se cuente con un dispositivo y acceso a Internet.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se requiere de una buena conexión a internet.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "No es necesario hacer filas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se pueden realizar pagos en línea.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es necesario desplazarse para poder realizar el trámite.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se dejan de realizar actividades de convivencia con otras personas.",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona los beneficios de realizar un trámite por medios electrónicos."
        }
	},
	// Reactivo 21
	{
        "respuestas": [{
                "t13respuesta": "<p>firma digital<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>línea<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>internet<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>electrónicos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>confiable<\/p>",
                "t17correcta": "5"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Para llenar algunos formularios electrónicos, se necesita una "
            },
            {
                "t11pregunta": ".<br>Cuando se realiza un trámite en "
            },
            {
                "t11pregunta": ", no es necesario acudir a una oficina.<br>Una de las ventajas de "
            },
            {
                "t11pregunta": ", es que permite realizar diferentes trámites en línea.<br>Los formularios "
            },
            {
                "t11pregunta": " no pueden llevar una firma o rúbrica impresa.<br>Para realizar un trámite en línea, es importante verificar que el portal de la organización sea "
            },
            {
                "t11pregunta": "."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen los enunciados.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
	},
];