
json=[  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"La información debe ser presentada en forma ordenada.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Se debe utilizar vocabulario especializado del tema que se presenta.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Los datos deben ser medibles y observables, y apoyarse de gráficos y tablas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"El informe debe contener hechos empíricos y objetivos.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Es un escrito que refleja la opinión del autor.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Es un texto que presenta información subjetiva.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Utiliza lenguaje coloquial para exponer la información.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las respuestas correctas a la pregunta.<br><br>¿Cuáles son las características de un informe de experimento científico?"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<img src='SI9E_B03_A02_02.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='SI9E_B03_A02_01.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='SI9E_B03_A02_03.png'>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='SI9E_B03_A02_04.png'>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Se utiliza para mostrar el crecimiento de la población en una ciudad."
            },
            {
                "t11pregunta": "Se puede utilizar para presentar la cantidad de alumnos en los diferentes grados escolares de una escuela."
            },
            {
                "t11pregunta": "Se utiliza para hacer comparaciones de la información cualitativa."
            },
            {
                "t11pregunta": "Recurso gráfico que sirve para mostrar imágenes precisas de un tema."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.",
            "altoImagen":"100px",
            "anchoImagen":"200px"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hamlet se desarrolla en una época donde la comunicación a distancia no era inmediata.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hamlet se desarrolla en la época actual.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la época de la narración, los matrimonios se hacían según los intereses económicos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hamlet se lleva a cabo en un lugar donde hay una marcada división social.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El texto  es un ejemplo de cuento.",
                "correcta"  : "0"
            },
             {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las obras renacentistas se hablaba exclusivamente de temas religiosos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "William Shakespeare fue un escritor renacentista.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las obras renacentistas prevalece la intención estética del arte.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee el siguiente texto y elige falso o verdadero según corresponda.",
            "t11instruccion":"<center><b>Hamlet</b><br>ACTO I<br>ESCENA VII<br><br>Sala de la casa de Polonio<br>LAERTES, OFELIA</center>LAERTES<br>Ya tengo todo mi equipaje a bordo. Adiós hermana, y cuando los vientos sean favorables y seguro el paso del mar, no te descuides en darme nuevas de ti.<br><br>OFELIA<br>¿Puedes dudarlo?<br><br>LAERTES<br>Por lo que hace al frívolo obsequio de Hamlet, debes considerarle como una mera cortesía, un hervor de la sangre, una violeta que en la primavera juvenil de la naturaleza se adelanta a vivir y no permanece, hermosa, no durable, perfume de un momento y nada más.<br><br>OFELIA<br>¿Nada más?<br><br>LAERTES<br>Pienso que no, porque no solo en nuestra juventud se aumentan las fuerzas y el tamaño del cuerpo, sino que las facultades interiores del talento y del alma crecen también con el templo en que ella reside. Puede ser que él te ame ahora con sinceridad, sin que manche borrón alguno la pureza de su intención; pero debes temer, al considerar su grandeza, que no tiene voluntad propia y que vive sujeto a obrar según a su nacimiento corresponde. Él no puede como una persona vulgar, elegir por sí mismo; puesto que de su elección depende la salud y prosperidad de todo un reino. Y ve aquí, por qué esta elección debe arreglarse a la condescendencia unánime de aquel cuerpo de quien es cabeza. Así, pues, cuando él diga que te ama, será prudencia en ti no darle crédito. Reflexiona  que en el alto lugar que ocupa, nada puede cumplir de lo que promete, sino aquello que obtenga el consentimiento de la parte más principal de Dinamarca. Considera cual pérdida padecería tu honor, si con demasiada credulidad dieras oídos a su voz lisonjera, perdiendo la libertad del corazón o facilitando a sus instancias impetuosas el tesoro de tu honestidad.  Teme, Ofelia, teme querida hermana: no sigas inconsiderada tu inclinación y huye del peligro.(...)<br><br>Shakespeare (1825)",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Escribir para el oído de la audiencia.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Evitar el uso de muletillas.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Apoyarse en sinónimos para no ser redundantes.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Utilizar un lenguaje fácil de entender para la audiencia.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Utilizar tecnicismos para elevar la complejidad del mensaje.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Emplear lenguaje informal.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas a la pregunta.<br><br>¿Qué acciones se deben tomar en cuenta al escribir un texto radiofónico?"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para obtener información de internet hay que seguir tres pasos: buscar, copiar y pegar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de hacer una búsqueda de información en internet, es necesario definir los objetivos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al buscar información en internet, es imprescindible idear una estrategia de cómo y dónde buscar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La información obtenida mediante las TIC requiere de un análisis crítico para corroborar su confiabilidad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Toda la información de internet es confiable.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Se puede consultar información durante la transmisión del programa.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Se eliminan las barreras de tiempo y espacio.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Acceso a música de diferentes géneros y nacionalidades.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Existe comunicación directa e inmediata entre el locutor y la audiencia.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"El acceso a tanta información puede crear desconcierto.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Si el locutor hace una consulta en internet durante el programa, puede perder la atención de su audiencia.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Existe la posibilidad de comunicar información no verídica.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas a la pregunta.<br><br>¿Qué beneficios ofrecen las TIC al producir un programa de radio?"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>culturales</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>vestido</p>",
                "t17correcta": "2"
            },
             {
                "t13respuesta": "<p>derechos</p>",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "<p>iguales</p>",
                "t17correcta": "4"
            },
             {
                "t13respuesta": "<p>naturaleza</p>",
                "t17correcta": "5"
            },
             {
                "t13respuesta": "<p>crecimiento</p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>sociedad</p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>En el mundo existen diferentes manifestaciones </p>"
            },
           {
                "t11pregunta": "<p> como la música, el </p>"
            },
            {
                "t11pregunta": "<p>, la gastronomía, el arte, etc.  Algunas pueden estar apegadas a contextos religiosos, sociales o políticos.<br><br>El respeto de los </p>"
            },
            {
                "t11pregunta": "<p>  humanos y la inclusión son parte fundamental de la diversidad cultural.  Si todos fuéramos </p>"
            },
            {
                "t11pregunta": "<p>, no tendríamos manera de intercambiar experiencias ni de enriquecer nuestro bagaje cultural. <br><br>Las culturas orientales fomentan el cuidado de la </p>"
            },
            {
                "t11pregunta": "<p> y la espiritualidad; por otro lado, en occidente se busca el </p>"
            },
            {
                "t11pregunta": "<p> tecnológico. Sin duda, ambos factores son relevantes para la formación integral de la </p>"
            },
            {
                "t11pregunta": "<p>.</p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
];
