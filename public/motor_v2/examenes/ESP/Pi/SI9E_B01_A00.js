json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Es un texto argumentativo que reflexiona acerca de un tema en determinado.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tiene un estilo personal y es narrado en primera persona.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Presenta planteamiento, desarrollo y cierre.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Respalda opiniones con fundamentos teóricos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tienen un estilo impersonal.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Presenta las opiniones del autor sin reflexionar sobre el tema.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Su estructura se compone de inicio, clímax y desenlace.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas a la pregunta.<br><br>¿Cuáles son las características y funciones de los ensayos?",
            "t11instruccion": "",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "La producción automotriz en 2017 aumentó un 9% con respecto al año anterior. Esto provocó que las agencias ofrecieran más facilidades para la compra de sus productos y esto, a su vez, lo cual desencadenó mayor tráfico vehicular.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "En 2017 se produjeron 3 millones 773 mil 569 automóviles en México, lo que representa un crecimiento de 9%.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Si los ciudadanos utilizarán la bicicleta como medio de transporte, se reduciría la contaminación ambiental, el tráfico vehicular y los índices de obesidad en México.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Argumento"
            },
            {
                "t11pregunta": "Dato"
            },
            {
                "t11pregunta": "Opinión"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "Relaciona las columnas según corresponda. ",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Argumentar es compartir una opinión sin basarse en información o datos que lo avalen. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un argumento se apoya de comentarios, explicaciones, ejemplos, cifras, etc.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los argumentos son valoraciones subjetivas que buscan influir en la forma de pensar y actuar de una persona.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Exaltación de la identidad nacional.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Reivindicar las tradiciones frente a la industrialización.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Escapar a una vida sencilla sin mucha tecnología y exaltación de la naturaleza.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Presentación del amor y la belleza como un ideal loable.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "La guerra.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Apología de la tecnología y la industria.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "El sistema político.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "El dinero.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Cuáles son algunos temas de los que habla el Modernismo?",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [

            {
                "t13respuesta": "SI9E_B01_R05_02.png",
                "t17correcta": "0",
                "columna": "0"
            },


            {
                "t13respuesta": "SI9E_B01_R05_05.png",
                "t17correcta": "1",
                "columna": "0"
            },


            {
                "t13respuesta": "SI9E_B01_R05_08.png",
                "t17correcta": "2",
                "columna": "1"
            },


            {
                "t13respuesta": "SI9E_B01_R05_11.png",
                "t17correcta": "3",
                "columna": "0"
            },


            {
                "t13respuesta": "SI9E_B01_R05_14.png",
                "t17correcta": "4",
                "columna": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Coloca las figuras retóricas en el lugar correspondiente.",
            "t11instruccion":"Arrastra cada elemento donde corresponda.",
            "tipo": "ordenar",
            "imagen": true,
            "url": "SI9E_B01_R05_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["", "98,6", "cuadrado", "125, 127", ".", "transparent", true] },
            { "Contenedor": ["", "98,131", "cuadrado", "125, 127", ".", "transparent", true] },
            { "Contenedor": ["", "98,256", "cuadrado", "125, 127", ".", "transparent", true] },
            { "Contenedor": ["", "98,381", "cuadrado", "125, 127", ".", "transparent", true] },
            { "Contenedor": ["", "98,505", "cuadrado", "125, 127", ".", "transparent", true] },

        ]
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "La ausencia de un ser amado.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sentimiento de plenitud y de alegría.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Afirmación y exaltación nacional.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Sentimiento de angustia y melancolía.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Confesión.",
                "t17correcta": "5",
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Como tu desaparición<br>Es tal mi estupefacción,<br>Mi pasmo, que a veces creo<br>Que ha sido un escamoteo,<br>una burla, una ilusión; "
            },
            {
                "t11pregunta": "Mi vida toda júbilos y encantos,<br>Mi pecho rebosando de pureza,<br>Mi carmen pleno y perfumes y cantos<br>Y muy lejos, muy lejos, la tristeza. "
            },
            {
                "t11pregunta": "Vino, sentimiento, guitarra y poesía<br>Hacen los cantares de la patria <br>Cantares…<br>Quien dice cantares dice Andalucía. "
            },
            {
                "t11pregunta": "Estoy muy mal… Sonrío<br>Porque el desprecio del dolor me asiste,<br>Porque aún miro lo bello en torno mío,<br>Y… por lo triste que es el estar triste."
            },
            {
                "t11pregunta": "Yo soy un hombre sincero<br>De donde crece la palma,<br>Y antes de morirme quiero<br>Echar mis versos del alma."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "Relaciona las columnas según corresponda. ",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "SI9E_B01_R07_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "SI9E_B01_R07_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "SI9E_B01_R07_03.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "SI9E_B01_R07_04.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "SI9E_B01_R07_05.png",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra cada elemento donde corresponda.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "SI9E_B01_R07_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["0", "97,9", "cuadrado", "312, 76", ".", "transparent", true] },
            { "Contenedor": ["1", "174,321", "cuadrado", "312, 76", ".", "transparent", true] },
            { "Contenedor": ["2", "249,9", "cuadrado", "312, 76", ".", "transparent", true] },
            { "Contenedor": ["3", "326,321", "cuadrado", "312, 76", ".", "transparent", true] },
            { "Contenedor": ["4", "402,321", "cuadrado", "312, 76", ".", "transparent", true] }


        ]
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Es una forma de representar información objetiva mediante valores e imágenes.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Especifica el tema y propósito del estudio realizado.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Es un formulario con preguntas que pueden ser abiertas o cerradas.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Es un grupo de personas de quienes se obtiene información.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Son preguntas que no permiten expresar opiniones o juicios de valor a los encuestados.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Son preguntas que permiten responderse con un comentario u opinión.",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "Un tipo de encuesta en el que la identidad de los encuestados no se conoce.",
                "t17correcta": "7",
            },
            {
                "t13respuesta": "Es un documento que arroja los resultados de un trabajo de investigación.",
                "t17correcta": "8",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Gráficas"
            },
            {
                "t11pregunta": "Objetivo de la encuesta"
            },
            {
                "t11pregunta": "Cuestionario"
            },
            {
                "t11pregunta": "Población muestra"
            },
            {
                "t11pregunta": "Preguntas cerradas"
            },
            {
                "t11pregunta": "Preguntas abiertas"
            },
            {
                "t11pregunta": "Encuesta anónima"
            },
            {
                "t11pregunta": "Informe del resultado"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>Relaciona las columnas según corresponda.<\/p>",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    // reactivo 9

    {
        "respuestas": [
            {
                "t13respuesta": "H",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "O",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los diputados se reunieron para hacer propuestas que apoyen a reducir el uso de platos y vasos desechables.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La mayoría de las personas son flojas y prefieren utilizar platos desechables para no tener que lavar.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los platos y vasos desechables son elaborados con poliestireno, polietileno, polipropileno y polímero de plásticos no biodegradable.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En 2016, en Francia se prohibió la venta de platos y cubiertos desechables hechos de plástico.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los resultados de comida rápida deberían dejar de utilizar platos y vasos desechables para reducir la contaminación.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige hecho (H) u opinión (O) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    // reactivo 10
    {
      "respuestas": [
          {
              "t13respuesta": "Se basan en ideas y creencias aceptadas como verdaderas por la sociedad.",
              "t17correcta": "1",
          },
          {
              "t13respuesta": "Se retoman experiencias compartidas como sociedad o sentimientos en común para lograr empatía con el lector y conseguir la aceptación.",
              "t17correcta": "2",
          },
          {
              "t13respuesta": "Utilizan acontecimientos reales y comprobables para defender una idea. Estos incluyen citas, datos y resultados de investigaciones.",
              "t17correcta": "3",
          },

          {
              "t13respuesta": "Utilizan declaraciones que confirman lo que se quiere demostrar.",
              "t17correcta": "4",
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "Argumentos racionales"
          },
          {
              "t11pregunta": "Argumentos que apelan a los sentimientos"
          },
          {
              "t11pregunta": "Argumentos de hechos"
          },
          {
              "t11pregunta": "Argumentos de ejemplificación"
          },

      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11instruccion": "",
          "t11pregunta": "Relaciona las columnas según corresponda. ",
          "altoImagen": "100px",
          "anchoImagen": "200px"
      }
    },
    // reactivo 11
    {
      "respuestas": [
          {
              "t13respuesta": "Es un texto muy breve en el que el autor escribe a quién le dedica u ofrece su obra.",
              "t17correcta": "1",
          },
          {
              "t13respuesta": "Es el texto que antecede a la lectura y pretende guiar al lector acerca de lo que encontrará en la misma.",
              "t17correcta": "2",
          },
          {
              "t13respuesta": "Es información que el autor presenta al lector para que la tome en cuenta antes de leer el material.",
              "t17correcta": "3",
          },

          {
              "t13respuesta": "Es un escrito del autor en el que se encuentra información pertinente sobre la obra.",
              "t17correcta": "4",
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "Dedicatoria"
          },
          {
              "t11pregunta": "Prólogo"
          },
          {
              "t11pregunta": "Advertencia"
          },
          {
              "t11pregunta": "Presentación"
          },

      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11instruccion": "",
          "t11pregunta": "Relaciona las columnas según corresponda. ",
          "altoImagen": "100px",
          "anchoImagen": "200px"
      }
    },
    // reactivo 12
    {
      "respuestas": [        
        {
                   "t13respuesta":  "Dedicatoria",
                     "t17correcta": "1"
       },
        {
           "t13respuesta":  "Advertencia",
             "t17correcta": "2"
         },
        {
           "t13respuesta":  "Presentación",
             "t17correcta": "3"
         },
    ],
    "preguntas": [
      {
        "t11pregunta":""
      },
      {
      "t11pregunta": "<li type='disc'>Dedico este libro a mi esposa, quien fue mi guía en este arduo trabajo, y a mis hijos que fueron mi inspiración. Asimismo, a mis maestros quienes dejaron en mí una huella que me motivó a seguir escribiendo.</li>"
      },
      {
      "t11pregunta": "<br><li>El contenido de este libro presenta información no apta para menores de 15 años.</li>"
      },
      {
      "t11pregunta": "<br><li>En este libro, el lector encontrará una recopilación de cuentos y poemas escritos por autores latinoamericanos. La cuidadosa selección de los textos aquí presentados fue supervisada por el maestro en Letras Latinoamericanas, Luis Medina Alfaro.</li><br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que completen el párrafo.",
       "t11instruccion": "",
       "respuestasLargas": true,
       "pintaUltimaCaja": false,
       "contieneDistractores": true
     }
    },
];
