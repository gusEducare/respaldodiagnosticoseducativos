json = [
    //Reactivo 1
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Es un conjunto de técnicas para obtener información acerca de los gustos y preferencias del consumidor.",
            "t17correcta":"1",
             "numeroPregunta": "0"
         },
         {  
            "t13respuesta":     "Es una encuesta para conocer la opinión del consumidor.",
            "t17correcta":"0",
             "numeroPregunta": "0"
         },
         {  
            "t13respuesta":     "Consiste en desarrollar un nuevo producto que sea del gusto del consumidor.",
            "t17correcta":"0",
             "numeroPregunta": "0"
         },
          {  
            "t13respuesta":     "Es un cuestionario con preguntas de opción múltiple de donde se obtiene información cuantitativa.",
            "t17correcta":"1",
             "numeroPregunta": "1"
         },
         {  
            "t13respuesta":     "Es un cuestionario para poder hacer gráficas acerca de las opiniones del consumidor.",
            "t17correcta":"0",
             "numeroPregunta": "1"
         },
         {  
            "t13respuesta":     "Es una estrategia que consiste en plantear preguntas a los consumidores y escuchar sus respuestas.",
            "t17correcta":"0",
             "numeroPregunta": "1"
         },
          {  
            "t13respuesta":     "Es una técnica de investigación cualitativa en la que se formulan preguntas al consumidor y se toma nota de sus respuestas y reacciones.",
            "t17correcta":"1",
             "numeroPregunta": "2"
         },
         {  
            "t13respuesta":     "Es una técnica de investigación cuantitativa cuyos resultados se pueden graficar.",
            "t17correcta":"0",
             "numeroPregunta": "2"
         },
         {  
            "t13respuesta":     "Es un cuestionario que contiene preguntas de opción múltiple para conocer las preferencias del consumidor.",
            "t17correcta":"0",
             "numeroPregunta": "2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":[
                "Lee el siguiente párrafo y selecciona la respuesta correcta.<br>¿Qué es la investigación de mercados?",
                "¿Qué es una encuesta?",
                "¿Qué es un focus group?"],
           "preguntasMultiples": true,
          "t11instruccion":"<center><strong>Antes de lanzar, hay que probar</center></strong><br><br>Cuando una empresa desarrolla un nuevo producto, corre el riesgo de que este no sea aceptado por el mercado. Por eso, antes de lanzarlo a la venta debe investigar si el producto será del gusto de sus consumidores y así evitar grandes pérdidas económicas. A este proceso se le conoce como investigación de mercados.<br>Existen diferentes técnicas de investigación de mercados. Las más comunes son las encuestas y los <i>focus group.</i><br>Una encuesta  es un estudio cuantitativo en el que se pueden obtener datos precisos a partir de las preguntas y sus opciones de respuesta. Después de aplicarla, se hace un conteo de respuestas, se elaboran gráficas y un reporte con la interpretación de los resultados.<br>A diferencia de la encuesta, el <i>focus group</i> es una técnica cualitativa que consiste en plantear preguntas abiertas. Esta estrategia permite analizar las reacciones del consumidor ante el producto o incluso, ante un anuncio publicitario, así como escuchar sus opiniones y recomendaciones. Es fundamental que haya un moderador que dirija la actividad y un observador que tome nota de las respuestas.  Al final, se elabora un reporte con toda la información recopilada."
      }
   },
    //Reactivo 2
     {
      "respuestas": [
          {
              "t13respuesta": "SI9E_B04_A02_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "SI9E_B04_A02_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "SI9E_B04_A02_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "SI9E_B04_A02_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "SI9E_B04_A02_06.png",
              "t17correcta": "4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la imagen para completar el crucigrama.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"SI9E_B04_A02_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false,
          "t11instruccion":"<div style='float: left; width: 45%'><strong>Horizontal</strong><br>2. Sinónimo de importe<br>5. Sinónimo de avaro<br>6. Antónimo de incierto<br>9. Antónimo de egoísmo<br></div><div style='float: right; width: 45%'><strong>Vertical</strong><br>1. Sinónimo de bello<br>3. Antónimo de distinto<br>4. Sinónimo de económico<br>7. Sinónimo de triunfo<br>8. Sinónimo de abono<br>9. Sinónimo de adquirir <br></div><div style='clear: both'></div>"          
      },
      "contenedores": [
          {"Contenedor": ["", "67,178", "cuadrado", "225, 28", ".","transparent"]},
          {"Contenedor": ["", "158,27", "cuadrado", "225, 28", ".","transparent"]},
          {"Contenedor": ["", "307,366", "cuadrado", "36, 208", ".","transparent"]},
          {"Contenedor": ["", "36,479", "cuadrado", "36, 208", ".","transparent"]},
          {"Contenedor": ["", "277,592", "cuadrado", "36, 179", ".","transparent"]}
      ]
  },
    
    //Reactivo 3
     {
      "respuestas": [
          {
              "t13respuesta": "SI9E_B04_A03_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "SI9E_B04_A03_03.png",
              "t17correcta": "1,2,3",
              "columna":"0"
          },
          {
              "t13respuesta": "SI9E_B04_A03_04.png",
              "t17correcta": "2,3,1",
              "columna":"1"
          },
          {
              "t13respuesta": "SI9E_B04_A03_05.png",
              "t17correcta": "3,1,2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"SI9E_B04_A03_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "48,250", "cuadrado", "150, 86", ".","transparent"]},
          {"Contenedor": ["", "204,405", "cuadrado", "150, 86", ".","transparent"]},
          {"Contenedor": ["", "375,166", "cuadrado", "150, 86", ".","transparent"]},
          {"Contenedor": ["", "375,326", "cuadrado", "150, 86", ".","transparent"]},
      ]
  },
    
    
    //Reactivo 4
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "La lealtad familiar",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "El amor a Julieta",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "La responsabilidad social",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>De acuerdo al fragmento, ¿qué valor hace que Romeo considere a su amada como una enemiga?",
          "t11instruccion":"Lee la siguiente expresión extraída de “Romeo y Julieta” de William Shakespeare.<br>ROMEO:<br>¿Es una Capuleto? ¡Oh, cara acreencia! Mi vida es propiedad de mi enemiga.<br><br><i>(Shakespeare, 1597)</i>"
      }
   },
    //Reactivo 5
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Pronunciación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Entonación",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Volumen",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Expresión facial",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Danza",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Iluminación",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Vestuario",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Maquillaje",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br>¿Qué elementos prosódicos y recursos expresivos se deben utilizar en la lectura dramatizada?",
      }
   },
    //Reactivo 6
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para resolver un problema, es necesario investigar su origen.",               
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La manera más eficaz de resolver un problema es creando un conflicto.",                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunas fuentes para recopilar información acerca de un problema pueden ser las revistas, entrevistas a expertos, periódicos, etcétera.",               
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una historieta puede ser utilizada para plasmar un problema social, político o cultural.",                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para elaborar una historieta, no es necesario hacer una investigación previa.",                
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F)  o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },
    //Reactivo 7
    {
        "respuestas": [
            {
                "t13respuesta": "<p>zzzzz<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>¡riiing!<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>pow<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>crack<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>woeaa woeaa<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>pom pom<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>tic tac tic tac<\/p>",
                "t17correcta": "7"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><center><strong>¡Qué susto!</strong></center>Pedro dormía plácidamente <\/p>"
            },
            {
                "t11pregunta": "<p> en su habitación cuando de pronto sonó su despertador <\/p>"
            },
            {
                "t11pregunta": "<p>. Al poco rato, mientras se estaba bañando escuchó un fuerte golpe <\/p>"
            },
            {
                "t11pregunta": "<p>.<br>Más tarde, un vecino le platicó que un pintor estaba trabajando a cinco metros de altura en un andamio, cuando de repente <\/p>"
            },
            {
                "t11pregunta": "<p> este se quebró y el trabajador se cayó. Cuando llegó la ambulancia <\/p>"
            },
            {
                "t11pregunta": "<p> el corazón del señor latía <\/p>"
            },
            {
                "t11pregunta": "<p>, mientras el reloj de la enfermera hacía <\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //Reactivo 8
     {
      "respuestas": [
          {
              "t13respuesta": "SI9E_B04_A08_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "SI9E_B04_A08_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "SI9E_B04_A08_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "SI9E_B04_A08_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "SI9E_B04_A08_06.png",
              "t17correcta": "4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra la imagen al espacio que corresponda.<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"SI9E_B04_A08_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "60,25", "cuadrado", "278, 27", ".","transparent"]},
          {"Contenedor": ["", "145,453", "cuadrado", "67, 27", ".","transparent"]},
          {"Contenedor": ["", "166,193", "cuadrado", "88, 27", ".","transparent"]},
          {"Contenedor": ["", "237,24", "cuadrado", "148, 53", ".","transparent"]},
          {"Contenedor": ["", "335,408", "cuadrado", "148, 78", ".","transparent"]}
      ]
  },
    //Reactivo 9
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Reduce el uso del automóvil y usa más tu bicicleta.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Planta un árbol.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "No utilices popotes.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Reutiliza los vasos de cartón.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Estudia para tus exámenes.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "No publiques información personal en redes sociales.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Respeta los derechos de tus compañeros.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas a la pregunta.<br>¿Qué frases se pueden utilizar para difundir información acerca del cuidado del medio ambiente?",
      }
   },
    
    
    ];