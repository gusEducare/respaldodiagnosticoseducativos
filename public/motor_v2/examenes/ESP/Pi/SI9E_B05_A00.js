json = [

    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La autobiografía es un texto en el que una persona reconstruye los acontecimientos más relevantes de su vida.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la autobiografía una persona narra desde su nacimiento hasta su muerte.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la autobiografía se incluyen episodios exclusivamente de la vida académica y profesional de quien la escribe.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la autobiografía, el autor es el protagonista.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La autobiografía está narrada en primera persona.",
                "correcta"  : "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Reactivo",
            "evidencio": false,
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<p>ellos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Este<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>él<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>lo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>ella<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Manuel y Josef<\/p>",
                "t17correcta": "0"
            }, 
            {
                "t13respuesta": "<p>Ejercicio<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>Su novia<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Manuel y Josef saldrán a correr todos los días por la mañana, <\/p>"
            },
            {
                "t11pregunta": "<p> quieren iniciar un régimen de dieta y ejercicio. <\/p>"
            },
            {
                "t11pregunta": "<p> será un enorme reto para Manuel, ya que <\/p>"
            },
            {
                "t11pregunta": "<p> es un amante de las papas fritas; Josef lo convenció, pues estaba preocupado por la salud de su amigo.<br><br>La novia de Manuel <\/p>"
            },
            {
                "t11pregunta": "<p> está apoyando con la dieta,<\/p>"
            },
            {
                "t11pregunta": "<p> lo persuadió para que en lugar de papas, consuman verduras.<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
        }
    },
    //3
    {                               
        "respuestas": [
            {
                "t13respuesta": "Crónica",
                "t17correcta": "1,6"
            },
            {
                "t13respuesta": "Origen",
                "t17correcta": "2,7"
            },
             {
                "t13respuesta": "Partida",
                "t17correcta": "3,8"
            },
             {
                "t13respuesta": "Engaño",
                "t17correcta": "4,9"
            },
             {
                "t13respuesta": "Afecto",
                "t17correcta": "5,10"
            },
             {
                "t13respuesta": "Narración",
                "t17correcta": "6,1"
            },
             {
                "t13respuesta": "Inicio",
                "t17correcta": "7,2"
            },
             {
                "t13respuesta": "Huida",
                "t17correcta": "8,3"
            },
             {
                "t13respuesta": "Ingratitud",
                "t17correcta": "9,4"
            },
            {
               "t13respuesta": "Compasión",
               "t17correcta": "10,5"
           }
        ],
        "preguntas": [
            {
               "t11pregunta": "<br><style> .table\n\
                img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-20px;'>\n\
                                      <tr><td>Historia</td>\n\
                                          <td>Nacimiento</td>\n\
                                          <td>Salida</td>\n\
                                          <td>Traición</td>\n\
                                          <td>Sentimiento</td></tr><tr><td>\n\ "},
            {
                "t11pregunta": "</td><td>" 
            },
            {
                "t11pregunta": "</td><td>" 
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>" 
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
            

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.<br><br>¿Qué palabras podrían sustituir las de arriba en algunos contextos?",
            "respuestasLargas": true, 
            "pintaUltimaCaja": false,
        }
    },

    //4
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Nací en la Ciudad de México, el 3 de junio de 1984.<\/p>",
                "t17correcta": "0",
                etiqueta:"paso 1"
            },
            {
                "t13respuesta": "<p>Cuando tenía 5 años, comencé a ayudarle a mi mamá a cocinar. Disfrutaba mucho hacerlo.<\/p>",
                "t17correcta": "1",
                etiqueta:"paso 2"
            },
            {
                "t13respuesta": "<p>Al cumplir 15 años, tomé un curso para ser estilista. Lo terminé exitosamente, pero no era algo que disfrutara hacer.<\/p>",
                "t17correcta": "2",
                etiqueta:"paso 3"
            },
            {
                "t13respuesta": "<p>Al finalizar la preparatoria, ingresé a la escuela de gastronomía. Ahí estudié la licenciatura y me gradué como chef.<\/p>",
                "t17correcta": "3",
                etiqueta:"paso 4"
            },
            {
                "t13respuesta": "<p>Ya con mi título de chef, me mudé a la ciudad de Aguascalientes, ahí trabajé en diferentes restaurantes.<\/p>",
                "t17correcta": "4",
                etiqueta:"paso 5"
            },
            {
                "t13respuesta": "<p>Actualmente, vivo en Los Ángeles. Soy chef pastelero en un importante restaurante en Hollywood y amo mi trabajo.<\/p>",
                "t17correcta": "5",
                etiqueta:"paso 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos del 1 al 6 para escribir una autobiografía.<br><br>¿Cómo ordenarías la siguiente autobiografía?",
        }
    },
        //5
      
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "SI9E_B05_A06_I02.png",
                "t17correcta": "0,1,2,3",
                "columna":"0"
            },
            {
                "t13respuesta": "SI9E_B05_A06_I03.png",
                "t17correcta": "0,1,2,3",
                "columna":"0"
            },
            {
                "t13respuesta": "SI9E_B05_A06_I04.png",
                "t17correcta": "0,1,2,3",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A06_I05.png",
                "t17correcta": "0,1,2,3",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A06_I06.png",
                "t17correcta": "4,5,6,7",
                "columna":"0"
            },
            {
                "t13respuesta": "SI9E_B05_A06_I07.png",
                "t17correcta": "4,5,6,7",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A06_I08.png",
                "t17correcta": "4,5,6,7",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A06_I09.png",
                "t17correcta": "4,5,6,7",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra los elementos y completa la tabla.<br><br>Identifica las oraciones que expresen una opinión personal y las que comuniquen datos y hechos objetivos. Colócalos en donde correspondan.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"SI9E_B05_A06_I01.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":false
            
        },
        "contenedores": [
            {"Contenedor": ["", "109,73", "cuadrado", "247, 89", ".","transparent"]},
            {"Contenedor": ["", "201,73", "cuadrado", "247, 89", ".","transparent"]},
            {"Contenedor": ["", "293,73", "cuadrado", "247, 89", ".","transparent"]},
            {"Contenedor": ["", "385,73", "cuadrado", "247, 89", ".","transparent"]},

            {"Contenedor": ["", "109,323", "cuadrado", "247, 89", ".","transparent"]},
            {"Contenedor": ["", "201,323", "cuadrado", "247, 89", ".","transparent"]},
            {"Contenedor": ["", "293,323", "cuadrado", "247, 89", ".","transparent"]},
            {"Contenedor": ["", "385,323", "cuadrado", "247, 89", ".","transparent"]}
        ]
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "SI9E_B05_A07_02.png",
                "t17correcta": "0,1",
                "columna":"0"
            },
            {
                "t13respuesta": "SI9E_B05_A07_03.png",
                "t17correcta": "0,1",
                "columna":"0"
            },
            {
                "t13respuesta": "SI9E_B05_A07_04.png",
                "t17correcta": "2,3",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A07_05.png",
                "t17correcta": "2,3",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A07_06.png",
                "t17correcta": "4,5",
                "columna":"0"
            },
            {
                "t13respuesta": "SI9E_B05_A07_07.png",
                "t17correcta": "4,5",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A07_08.png",
                "t17correcta": "6,7",
                "columna":"1"
            },
            {
                "t13respuesta": "SI9E_B05_A07_09.png",
                "t17correcta": "6,7",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Completa la tabla con los elementos de los recuadros que correspondan.<br><br>Identifica las fuentes e ideas que necesitas investigar o podrías encontrar de acuerdo al tema solicitado.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"SI9E_B05_A07_01.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":false
            
        },
        "contenedores": [
            {"Contenedor": ["", "91,222", "cuadrado", "199, 99", ".","transparent"]},
            {"Contenedor": ["", "91,424", "cuadrado", "199, 99", ".","transparent"]},
            {"Contenedor": ["", "193,222", "cuadrado", "199, 99", ".","transparent"]},
            {"Contenedor": ["", "193,424", "cuadrado", "199, 99", ".","transparent"]},

            {"Contenedor": ["", "295,222", "cuadrado", "199, 99", ".","transparent"]},
            {"Contenedor": ["", "295,424", "cuadrado", "199, 99", ".","transparent"]},
            {"Contenedor": ["", "397,222", "cuadrado", "199, 99", ".","transparent"]},
            {"Contenedor": ["", "397,424", "cuadrado", "199, 99", ".","transparent"]}
        ]
    },
      { 
        "respuestas": [
            {
                "t13respuesta": "Aquí se presenta el tema que se va a tratar y una posible hipótesis personal.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se exponen los argumentos, razones y hechos que justifican o avalan la hipótesis planteada. Es la parte más extensa del artículo.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se resumen las ideas planteadas durante el artículo y se reafirma el punto de vista inicial.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Son las pruebas, evidencias que se presentan en un artículo. Son argumentos racionales o científicos que no dependen del punto de vista del autor.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Es una  opinión personal y punto de vista del autor respecto a los hechos.",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Introducción"
            },
            {
                "t11pregunta": "Desarrollo"
            },
            {
                "t11pregunta": "Conclusión"
            },
            {
                "t11pregunta": "Hechos y datos"
            },
            {
                "t11pregunta": "Postura del autor"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        },
    },
    ];