json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Una monografía puede elaborarse sólo con la información previa que se tiene sobre el tema.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El tema debe ser específico y riguroso.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Debe relatar hechos fantásticos con personajes de creación propia.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Debe contener aspectos relevantes sobre el tema en su desarrollo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Puede ser imprecisa.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La redacción debe incluir un lenguaje claro y sencillo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El lenguaje a utilizar debe ser complejo.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El autor no puede incluir su opinión.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El investigador puede escribir de manera subjetiva.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La investigación debe sustentarse en fuentes bibliográficas confiables.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee los siguientes enunciados y selecciona los que estén relacionados con la correcta redacción de una monografía."
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una investigación objetiva puede utilizar como fuente algún trabajo previo  para sustentar sus conceptos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una investigación objetiva no toma en cuenta investigaciones previas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Toda cita, paráfrasis o resumen de un texto consultado, debe citarse.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No es necesario citar todas las fuentes de consulta en una investigación seria.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee con atención los siguientes enunciados y selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "Portada",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Índice",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Introducción",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Fuentes consultadas",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Conclusión",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Cuerpo del trabajo",
                "t17correcta": "6"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Contiene  los datos de la institución, título del trabajo, nombre del investigador y el lugar y la fecha del informe."
            },
            {
                "t11pregunta": "Presenta el nombre de  los capítulos, enumeraciones y apartados de la investigación."
            },
            {
                "t11pregunta": "En ella se leen los motivos de la elección del tema, los objetivos de la investigación y la estructura del trabajo."
            },
            {
                "t11pregunta": "Comprende un listado de todas las revistas, libros, enciclopedias y fuentes digitales de las cuales se obtuvo información."
            },
            {
                "t11pregunta": "Contiene la síntesis del trabajo presentado y las recomendaciones a los lectores que quieran realizar investigaciones a futuro sobre el tema."
            },
            {
                "t11pregunta": "Es el desarrollo de la monografía, aquí se incluyen los apartados del tema y en algunos casos elementos gráficos que acompañan al texto."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "Escrito que busca informar sobre un tema del ámbito académico.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Descripción que busca formular un ensayo sobre un tema específico.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "En este tipo de escritos se recopila información de varias fuentes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Solamente está dirigido a un grupo particular.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Debe ser subjetivo.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Debe desarrollar aspectos importantes y relevantes sobre el tema.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee los siguientes enunciados y selecciona los que sean características de una monografía."
        }
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "<p>inicialmente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Después<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Posteriormente<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Finalmente<\/p>",
                "t17correcta": "4"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Para escribir un ensayo, "
            },
            {
                "t11pregunta": " se debe elegir el tema. "
            },
            {
                "t11pregunta": ", hay que delimitar qué se estudiará sobre éste para plantear una hipótesis. "
            },
            {
                "t11pregunta": ", se debe hacer el trabajo de investigación y recopilar la información. "
            },
            {
                "t11pregunta": ", se escribe el texto que se va a presentar."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra el nexo que complete el siguiente texto.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "<p>debido a<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>En cambio<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Por esta razón<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>a diferencia de<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Como conclusión<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>aunque<\/p>",
                "t17correcta": "6"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Patricia nunca desayuna en las mañanas "
            },
            {
                "t11pregunta": " que siempre se levanta tarde. "
            },
            {
                "t11pregunta": ", su hermano Jorge nunca sale de casa sin desayunar. "
            },
            {
                "t11pregunta": ", Patricia siempre está de mal humor y se siente cansada, "
            },
            {
                "t11pregunta": " su hermano que siempre está contento y lleno de energía. "
            },
            {
                "t11pregunta": ", podríamos decir que es importante desayunar "
            },
            {
                "t11pregunta": " se te haga tarde para ir a la escuela."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el siguiente texto.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 7
    {
        "respuestas": [{
                "t13respuesta": "Se dice",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Fue",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Por las noches",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "desaparecerán",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "se desaparecen",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "son desaparecidas",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "se observará",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "observó",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "observaría",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Lee los siguientes enunciados y selecciona la opción que complete el texto.<br><br>_____________ que una enfermera se aparece en los hospitales para curar a los enfermos y nadie más la vuelve a ver.",
                "En el Triángulo de las Bermudas, las embarcaciones ________________ en el mar sin motivo alguno.",
                "Un eclipse de sol _______________ en el continente americano la próxima semana."
            ],
            "preguntasMultiples": true
        }
    },
    // Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "Al día le sigue la noche.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Hoy tomaré agua de limón.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Todos tenemos padres.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "De momento, no es oportuno.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Dos más dos son cuatro.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Frida Kahlo fue una gran artista.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La tierra gira sobre su propio eje.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Las estrellas reflejan la luz del sol.",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee los siguientes enunciados y selecciona los que estén redactados en presente atemporal."
        }
    },
    // Reactivo 9
    {
        "respuestas": [{
                "t13respuesta": "Narrador protagonista",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Narrador testigo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Narrador omnisciente",
                "t17correcta": "2"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Me encontraba plácidamente sentado comiendo mi cereal, cuando de repente, los robots cobraron vida. No sabía la razón, pero todos se acercaban a mí y me llamaban por mi nombre. Había cumplido mi cometido.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Recuerdo que esa mañana vi a Macario entrar al pueblo, todos lo recibían con ovaciones. ¡Era el mejor pugilista de todos los tiempos y por fin regresaba a su tierra natal! Hace más de 200 años que tuvo que dejar a su familia para ir en busca de ese sueño y ahora volvía después de haberlo conseguido. El extraño tratamiento al que se sometió, había sido un éxito.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Emilio desde pequeño mostró una gran habilidad matemática. Su programación le permite solucionar operaciones en segundos. Su abuelo tiene la misma habilidad, él fue quien lo programó. Todos los domingos se reúnen a resolver problemas matemáticos y a comer pizza horneada por la abuela, hecha con la receta secreta de la familia.",
                "correcta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Arrastra el número que corresponda al tipo de narrador a cada uno de los siguientes párrafos.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 10
    {
        "respuestas": [{
                "t13respuesta": "Vida artificial",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Hadas y gnomos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Cultivo de hierbas medicinales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Mundos paralelos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Selvas y su conservación",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Viajes en el tiempo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Recetas para diabéticos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Clonación",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cómo hacer una instalación eléctrica",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Manipulación genética",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Extraterrestres",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee atentamente y selecciona aquellos tema posibles en la narrativa de ciencia ficción."
        }
    },
    // Reactivo 11
    {
        "respuestas": [{
                "t13respuesta": "Tema",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Inicio",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Personajes",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Tiempo",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Escenarios",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Nudo",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Desenlace",
                "t17correcta": "7"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Un concurso de patinaje en los anillos de Saturno."
            },
            {
                "t11pregunta": "Había una vez, una escuela de patinaje que quería hacer algo diferente para festejar su aniversario y decidieron hacer un concurso en los anillos de Saturno."
            },
            {
                "t11pregunta": "La maestra de patinaje, los alumnos y los jueces."
            },
            {
                "t11pregunta": "En el año 3150."
            },
            {
                "t11pregunta": "Los anillos de Saturno y la escuela de patinaje."
            },
            {
                "t11pregunta": "Mientras Samantha practicaba su rutina, se lastimó un pie."
            },
            {
                "t11pregunta": "Desde entonces, Samantha no permite que cualquier obstáculo la detenga."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.<br><center><img src=\"SI7E_B02_A11_01.png\" width=\"600\"></center>"
        }
    },
    // Reactivo 12
    {
        "respuestas": [{
                "t13respuesta": "Escape de Neptuno",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El maravilloso mundo de los insectos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Patinando en Saturno",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Recetas Mixtecas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Misterios de la disciplina en China",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Mi visita al imperio romano",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Humanos acuáticos",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las frases que podrías utilizar como título de un cuento de ciencia ficción."
        }
    },
    // Reactivo 13
    {
        "respuestas": [{
                "t13respuesta": "Hecho",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opinión",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son acontecimientos descritos exactamente como ocurrieron.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Presentan sucesos o datos que son comprobables.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Presentan los comentarios de diversas personas que expresan su postura ante algún acontecimiento.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son comentarios y apreciaciones que pueden estar a favor o en contra de algún suceso.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee los siguientes enunciados y selecciona H si se trata de un hecho, o la letra O si es una opinión.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 14
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La noticia da información sobre diferentes puntos de las costas de Guerrero.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La noticia expone que población quiere hacer donaciones para los damnificados.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La noticia brinda información precisa sobre el día y hora en la que se dio el fenómeno.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La noticia tiene un título que da información sobre su contenido.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La noticia da información detallada sobre los diferentes lugares donde se pueden hacer donaciones.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee la siguiente noticia y selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false,
            "t11instruccion": "<center><b>Tsunami en las costas de Guerrero</b></center><br>El 9 de marzo de 2018 a las 10:45 am se registró un Tsunami en Acapulco, Guerrero.  El fenómeno afectó a la zona hotelera del puerto y dejó daños por hasta 5 mil millones de pesos.<br><br>Una gran cantidad de turistas, de diferentes nacionalidades, están  varados en el lugar y han presentado problemas para comunicarse con sus familiares. Hasta el momento, se dice que hay 78 muertos y 358 personas desaparecidas.  La Cruz Roja espera recibir ayuda humanitaria para hacerla llegar lo antes posible."
        }
    },
    // Reactivo 15
    {
        "respuestas": [{
                "t13respuesta": "Plantear el tema a discutir y dar una opinión al respecto.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tener la plena seguridad de lo que se dice y no estar dispuesto a cambiar de opinión.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Modificar el discurso continuamente, confiando en la intuición.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Fundamentar nuestro punto de vista con hechos verídicos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Argumentar que la subjetividad es correcta.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Exponer cifras, estadísticas y situaciones equivalentes que hayan ocurrido con anterioridad.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee los siguientes enunciados y selecciona los que se refieran a las características que debe tener un argumento para que sea convincente en un debate."
        }
    },
    // Reactivo 16
    {
        "respuestas": [{
                "t13respuesta": "Hecho",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opinión",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El embajador llegó hoy a la ciudad y partirá el día de mañana.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es imposible conocer la realidad de un país en un solo día.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La manifestación contra la violencia de género comenzó a las 10:00 am y acudieron más de mil personas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El embajador llegó con dos horas de retraso a su reunión, debido al tráfico provocado por la manifestación.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las manifestaciones deberían ser prohibidas, ya que provocan caos vial y sólo afectan a los automovilistas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los problemas sociales no pueden ser resueltos sólo con acudir a manifestaciones.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee los siguientes enunciados y selecciona H si se trata de un hecho, o la letra O, si es una opinión.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 17
    {
        "respuestas": [{
                "t13respuesta": "Wikipedia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Páginas oficiales de periódicos reconocidos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Artículos publicados por especialistas e investigadores",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Publicaciones de mis amigos en redes sociales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Páginas oficiales de revistas especializadas en el tema",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las opciones que representan fuentes de consulta confiables para recopilar información."
        }
    },
    // Reactivo 18
    {
        "respuestas": [{
                "t13respuesta": "www.nasa.gov",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "www.milenio.com",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "www.rae.es",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "www.amazon.com",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "www.wordreference.com",
                "t17correcta": "5"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Investigar cuándo será el próximo eclipse solar."
            },
            {
                "t11pregunta": "Leer una noticia sobre las manifestaciones en España."
            },
            {
                "t11pregunta": "Buscar la forma correcta de escribir una palabra en español."
            },
            {
                "t11pregunta": "Comprar en línea una cámara fotográfica."
            },
            {
                "t11pregunta": "Conocer la manera de decir una misma palabra en diferentes idiomas."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada tema con la fuente de consulta para investigarlo."
        }
    },
]