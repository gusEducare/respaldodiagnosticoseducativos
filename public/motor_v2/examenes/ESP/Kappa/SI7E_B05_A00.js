json = [
    //1
     {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la tercera escena Martina perdona fácilmente a Sganarelle.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la cuarta escena Martina perdona a Sganarelle.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la tercera escena Sganarelle golpea a Martina.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la cuarta escena solo aparece Martina.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Martina perdona a Sganarelle.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Martina quiere vengarse de Sganarelle.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee el texto y elige falso (F) o verdadero (V) según corresponda.",
            "t11instruccion": "Lee el siguiente fragmento de “El médico a palos” de Moliére.<br><br>ESCENA III<br>SGANARELLE Y MARTINA<br><br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Volviendo hacia su mujer y estrechándole la mano.) -¡Ea! Hagamos las <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;paces nosotros. ¡Chócala!<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-¡Oh, después de haberme pegado así!<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Eso no es nada. ¡Choca!<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-No quiero.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-¿Eh?<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-No.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-¡Mujercita mía!<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Nones.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Vamos, te digo.<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-No lo haré.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Ven, ven y ven.<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-No. Quiero estar furiosa.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-¡Bah! Es una bagatela. Vamos, vamos.<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Déjame ya.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Choca, te digo.<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Me has sacudido demasiado.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Pues bien; vaya, te pido perdón; choca la mano.<br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Te perdono (bajo, aparte); pero me la pagarás.<br>SGANARELLE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Eres una loca en preocuparte por esto. Son las cosillas necesarias, de cuando en  <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cuando, en la amistad; y cinco o seis palos entre gentes que se quieren sirven<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;para fortalecer el cariño. ¡Ea!, me voy al bosque, y te prometo hoy más de un<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ciento de haces.<br><br>ESCENA IV MARTINA, sola<br><br>MARTINA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-Anda, aunque ponga esta cara, no olvidaré mi resentimiento, y ardo en deseos<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de encontrar la manera de castigarte por los golpes que me das. Ya sé que una                   <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mujer tiene siempre en sus manos con que vengarse de un marido; mas ése es un<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;castigo demasiado suave para mi bergante. Quiero una venganza que se deje<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;sentir un poco más, y eso no bastaría para la injuria que he recibido.<br><br>(Moliere, 1667)",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false,
        }
    },
    //2
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Tiempo cronológico.",
              "t17correcta":"1",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"Tiempo histórico.",
              "t17correcta":"0",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"Tiempo lineal.",
              "t17correcta":"0",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"Lineal.",
              "t17correcta":"1",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"Simultánea.",
              "t17correcta":"0",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"Cronológica.",
              "t17correcta":"0",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"Actos.",
              "t17correcta":"1",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"Escenas.",
              "t17correcta":"0",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"Capítulos.",
              "t17correcta":"0",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"Escenas.",
              "t17correcta":"1",
              "numeroPregunta": "3"
           },
           {  
              "t13respuesta":"Cuadros.",
              "t17correcta":"0",
              "numeroPregunta": "3"
           },
           {  
              "t13respuesta":"Actos.",
              "t17correcta":"0",
              "numeroPregunta": "3"
           },
           {  
              "t13respuesta":"Actos.",
              "t17correcta":"1",
              "numeroPregunta": "4"
           },
           {  
              "t13respuesta":"Escenas.",
              "t17correcta":"0",
              "numeroPregunta": "4"
           },
           {  
              "t13respuesta":"Capítulos.",
              "t17correcta":"0",
              "numeroPregunta": "4"
           }
        ],

        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Selecciona la respuesta correcta.<br><br>Es el tiempo en el que se desarrolla la acción del teatro clásico.",
                         "La acción del teatro clásico es:",
                         "Son las partes que conforman la obra de manera temporal.",
                         "Se determinan por la entrada o salida de los actores y cambian cada vez que entra o sale alguno de ellos.",
                        "Se identifican cuando suben y bajan el telón."],
                         "preguntasMultiples": true,          
        }
     },
     //3
     {  
        "respuestas":[  
           {  
              "t13respuesta":"Personajes.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Autor.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Nombre de la obra.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Actos.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Desenlace.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Nudo.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Actores.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Teatro.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Fechas de presentación.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"Selecciona todas las respuestas correctas. <br><br>Cuáles son los elementos esenciales que se deben mantener al adaptar una obra del teatro clásico a la actualidad."
        }
     },
     //4
     {
        "respuestas": [
            {
                "t13respuesta": "<p>-<\/p>",
                "t17correcta": "1,7"
            },
            {
                "t13respuesta": "<p>...<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>¡<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>!<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>¿<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>?<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>-<\/p>",
                "t17correcta": "7,1"
            },
            {
                "t13respuesta": "<p>(<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>)<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>.<\/p>",
                "t17correcta": "10"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<font sice<p>Extracto  de la obra “Hamlet” de Shakespeare.<br>REINALDO.<\/p>"
            },
            {
                "t11pregunta": "<p>Pero, señor<\/p>"
            },
            {
                "t11pregunta": "<p><br>POLONIO.-<\/p>"
            },
            {
                "t11pregunta": "<p>Ah<\/p>"
            },
            {
                "t11pregunta": "<p>Tú querrás saber con qué fin debes hacer esto,<\/p>"
            },
            {
                "t11pregunta": "<p>eh<\/p>"
            },
            {
                "t11pregunta": "<p><br>REINALDO.- Gustaría de saberlo.<br>POLONIO.<\/p>"
            },
            {
                "t11pregunta": "<p>Pues, señor, mi fin es este; y creo que es proceder con mucha cordura.<br>Cargando esas pequeñas faltas sobre mi hijo<\/p>"
            },
            {
                "t11pregunta": "<p>como ligeras manchas de una obra preciosa<\/p>"
            },
            {
                "t11pregunta": "<p>ganarás por medio de la conversación la confianza de aquel a quien pretendas examinar. Si él está persuadido de que el muchacho tiene los mencionados vicios que tú le imputas, no dudes que él convenga con tu opinión, diciendo: señor mío, o amigo, o caballero… En fin, según el título o dictado de las personas del país<\/p>"
            },
            {
                "t11pregunta": "<p>Shakespeare, W. (1609)<br>Recuperado de: http://www.biblioteca.org.ar/libros/89485.pdf<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el párrafo con las palabras del recuadro.<br><br>",
            "respuestasLargas": true,
            
            "contieneDistractores":true
        }
    },
    //5
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Tlacuache.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Chapulín.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Chocolate.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Jitomate.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Tomate.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Pinacate.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Chilacayote.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Esquite.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Jocoque.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Tepezcuintle.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Chicozapote.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Cereal.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Manzana.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Caballo.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Cerveza.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Mueble.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Almohada.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>¿Qué palabras provienen de lenguas indígenas?"
        }
     },
];