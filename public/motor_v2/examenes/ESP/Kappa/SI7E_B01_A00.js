json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Pedro Páramo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Juan Rulfo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Juan Preciado, Pedro Páramo, madre de Juan, Susana.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Juan Preciado va en busca de su padre. En el pueblo se dará cuenta de la insoportable verdad acerca de su padre y la condición en la que se encuentra el pueblo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Podemos reflexionar acerca del machismo, la idiosincrasia mexicana, el oportunismo y la pobreza.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Es un referente para entender el realismo mágico, integra muchos elementos de dicho movimiento literario.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Título del libro"
            },
            {
                "t11pregunta": "Nombre del autor"
            },
            {
                "t11pregunta": "Personajes"
            },
            {
                "t11pregunta": "Trama"
            },
            {
                "t11pregunta": "Reflexiones o valoración personal"
            },
            {
                "t11pregunta": "Importancia histórica, social, intelectual o  cultural de la obra."
            }
        ],
        "pregunta": {

            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.",
             "t11instruccion": "<p><b>Reseña:</b> Pedro Páramo, de Juan Rulfo <br> Juan Preciado se encuentra junto a su madre en sus últimos momentos de vida, justo antes<br> de morir esta le pide que vaya a Comala a buscar a su padre, Pedro Páramo, a quien<br> nunca conoció; para reclamarle “lo nuestro”. <br><br>Al llegar a Comala, Juan se percata de la tristeza del pueblo, pobre, en ruinas, sin vida,<br> polvoriento, ni un indicio de alegría ronda por aquel pueblo fantasma. Juan se encuentra con<br> una mujer que conoció a su madre, ella le dice que su madre le había avisado de su llegada,<br> él le responde que es imposible porque había muerto hace ya varios días, a lo que ella<br> responde: “Por eso le oía tan bajito”. <br><br>Juan queda confundido y no se atreve a decir palabra alguna. La extraña mujer lo acoge en<br> su casa, le asigna una habitación y lo deja descansar. Durante la noche Juan oye voces,<br> no puede identificar de dónde vienen, se extraña al descubrir que las voces se encuentran en <br>su habitación y hablan de él, una voz femenina le dice que la siga. Juan se encuentra<br> muy confundido y aturdido, oye constantemente voces en el pueblo, no sabe si le hablan a él,<br> si las voces hablan entre ellas, si es el viento o ruidos que hacen las casas. Las voces de<br> los personajes con los que interactúa el protagonista parecen un grito de angustia ahogado<br> por el calor infernal y la soledad del pueblo, parecen como un eco de una vida insoportable. <br><br>A Juan no le queda otra opción que oír atentamente las voces para descubrir la verdad<br> acerca de su padre, pero la incertidumbre, el miedo y la desesperación lo hacen morir. Una<br> vez muerto Juan puede escuchar claramente las voces, puede preguntar a los fantasmas<br> de Comala por su padre e intenta poner orden en aquel pueblo fantasma. Es así que<br> Juan conoce la personalidad de su padre, se entera de una nefasta verdad y de la que no es<br> la única víctima. Todo en aquel pueblo gira en torno a la figura paternal de Pedro Páramo y<br> su desventura amorosa con Susana.<br><br>La novela corta pertenece al movimiento literario del Realismo Mágico y como tal, está llena<br> de metáforas, una prosa poética llena de imágenes y situaciones sumamente cautivadoras<br> y fantásticas. La palabra páramo nos recuerda un lugar inhabitado, solitario, desabrido y es<br> así como nos presentan a Comala. La obra retrata una realidad social del México en tiempos<br> de la Revolución, podemos reflexionar acerca de la pobreza, el recalcitrante machismo y<br> la idiosincrasia de la sociedad. <br>Pedro Páramo es catalogada como una de las novelas más grandes escritas en habla hispana<br> y sin duda, es la obra literaria mexicana más leída alrededor del mundo.<br>J.M.</p>",
         }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para elaborar una reseña primero debemos leer la obra.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reseña es un resumen de la obra, pretende informar a los lectores acerca del contenido del texto para ahorrar su lectura.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reseña contribuye a la crítica literaria, pretende valorar las obras literarias y reflexionar acerca de las temáticas que los autores pretenden mostrar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las reseñas literarias no tienen una estructura definida, pueden hacerse como sea. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la introducción de un texto se presentan los personajes, las situaciones y los antecedentes relevantes para entenderlo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para hacer una buena reseña literaria tenemos que explicar toda la vida del autor.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }
    },
    //3
     {
 "respuestas":[
 {
 "t13respuesta":"Consultar al menos tres diferentes fuentes.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Verificar que el autor sea experto en el tema.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Elegir únicamente fuentes serias de investigación.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Enfocar la investigación en una sola fuente de consulta.",
 "t17correcta":"0",
 },
{
 "t13respuesta":"Leer diferentes autores que se enfoquen en el mismo tema, sean reconocidos o no.",
 "t17correcta":"0",
 },
],
 "pregunta":{
 "c03id_tipo_pregunta":"2",
 "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br>¿Qué factores deben tomarse en cuenta al elegir un material de consulta?",

 "t11instruccion": "",
 }
 },
 //4
 {
        "respuestas": [
            {
                "t13respuesta": "<p>Elección y delimitación del tema.<\/p>",
                "t17correcta": "0",
                etiqueta:"paso 1"
            },
            {
                "t13respuesta": "<p>Plantear las preguntas guía.<\/p>",
                "t17correcta": "1",
                etiqueta:"paso 2"
            },
            {
                "t13respuesta": "<p>Selección del material, fuentes, lecturas y gráficos necesarios.<\/p>",
                "t17correcta": "2",
                etiqueta:"paso 3"
            },
            {
                "t13respuesta": "<p>Abstraer la información en notas, resúmenes, tablas o borradores.<\/p>",
                "t17correcta": "3",
                etiqueta:"paso 4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Ordena los siguientes elementos.<br>¿Cuál es el orden lógico para elegir el tema de una investigación?<\/p>",
        }
    },
    //5
        {
        "respuestas": [
            {
                "t13respuesta": "<img src='SI7A_B1_R03.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "¿Qué fue la República romana? ¿Quiénes gobernaban? ¿Cuándo se fundó la república? ¿Por qué surgieron las guerras civiles? ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "El gobierno romano durante la República desde 509 a.C. hasta el 29 a.C.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "El gobierno romano.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Apoyos gráficos"
            },
            {
                "t11pregunta": "Preguntas guía"
            },
            {
                "t11pregunta": "Delimitación del tema"
            },
            {
                "t11pregunta": "Subtema"
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
    //6
    {
      "respuestas": [
          {
              "t13respuesta": "SI7A_B1_R01_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "SI7A_B1_R01_02.png",
              "t17correcta": "1,2,3",
              "columna":"0"
          },
          {
              "t13respuesta": "SI7A_B1_R01_03.png",
              "t17correcta": "2,3,1",
              "columna":"1"
          },
          {
              "t13respuesta": "SI7A_B1_R01_04.png",
              "t17correcta": "3,1,2",
              "columna":"1"
          },
          {
              "t13respuesta": "SI7A_B1_R01_05.png",
              "t17correcta": "4,5,6",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra los elementos y completa la tabla.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"SI7A_B1_R01_00.png",
          "anchoImagen":75,
          "respuestaImagen":true,
          "tamanyoReal":false,
          "bloques":false,
          "borde":false

      },
      "contenedores": [
          {"Contenedor": ["", "2,2", "cuadrado", "139, 101", ".","trasparent"]},
          {"Contenedor": ["", "107,2", "cuadrado", "139, 101", ".","trasparent"]},
          {"Contenedor": ["", "212,2", "cuadrado", "139, 101", ".","trasparent"]},
          {"Contenedor": ["", "317,2", "cuadrado", "139, 101", ".","trasparent"]},
          {"Contenedor": ["", "422,2", "cuadrado", "139, 101", ".","trasparent"]}
      ]
  },
    //7
       {
        "respuestas": [
            {
                "t13respuesta": "La información se organiza de izquierda a derecha y con llaves. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se utilizan palabras conectoras para organizar la información.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "El contenido de un texto es representado por medio de imágenes y palabras clave.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Sirve para representar un proceso de manera gráfica y ordenada.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Se utiliza para mostrar datos y cifras de manera clara y sencilla.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Cuadro sinóptico"
            },
            {
                "t11pregunta": "Mapa conceptual"
            },
            {
                "t11pregunta": "Mapa mental"
            },
            {
                "t11pregunta": "Diagrama de flujo"
            },
            {
                "t11pregunta": "Gráficas"
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
    //8
       {
 "respuestas":[
 {
 "t13respuesta":"Utilizar lenguaje claro y con palabras adecuadas al tipo de público.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Realizar una investigación profunda con fuentes de consulta serias.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Seleccionar la información relevante del tema de acuerdo al enfoque de la exposición.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"No hacer una exposición de más de 20 minutos.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Improvisar toda la información que se va a exponer.",
 "t17correcta":"0",
 },
 {
 "t13respuesta":"Utilizar tecnicismos sin importar que no sean comprendidos por la audiencia.",
 "t17correcta":"0",
 },
 {
 "t13respuesta":"Basar la exposición en la simpatía y no en la información.",
 "t17correcta":"0",
 },
 {
 "t13respuesta":"Emplear dos horas para hablar de un tema.",
 "t17correcta":"0",
 },
],
 "pregunta":{
 "c03id_tipo_pregunta":"2",
 "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br>¿Qué aspectos deben tomarse en cuenta al planear una exposición oral?",

 "t11instruccion": "",
 }
 },
 //9
  {
        "respuestas": [
            {
                "t13respuesta": "Es breve y señala el tema elegido.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Permite poner en contexto a la audiencia acerca del tema que se va a presentar.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Descripción del contenido del tema y de la información investigada a partir de diferentes materiales de apoyo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Cierre de la exposición y presentación de los resultados de la investigación.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Título"
            },
            {
                "t11pregunta": "Introducción"
            },
            {
                "t11pregunta": "Desarrollo"
            },
            {
                "t11pregunta": "Conclusión"
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de presentar una exposición es importante conocer los intereses y características de la audiencia que la escuchará.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al escuchar una exposición se puede aprovechar el tiempo para revisar mensajes de texto.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El guion debe ser leído durante la exposición.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante una exposición se deben anotar las dudas para plantearlas en la sesión de preguntas y respuestas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los esquemas son de utilidad para mostrar información en una exposición y para tomar notas cuando se participa como audiencia.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }
    },
    //11
     {
 "respuestas":[
 {
 "t13respuesta":"El tema debe estar delimitado.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Su redacción debe ser con un lenguaje claro y sencillo.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"El autor no puede incluir su opinión.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Debe contener aspectos relevantes acerca del tema que desarrolla.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Debe relatar hechos fantásticos con personajes de creación propia.",
 "t17correcta":"0",
 },
 {
 "t13respuesta":"Puede ser imprecisa.",
 "t17correcta":"0",
 },
 {
 "t13respuesta":"El lenguaje a utilizar debe ser complejo. ",
 "t17correcta":"0",
 },
 {
 "t13respuesta":"El investigador puede escribir de manera subjetiva.",
 "t17correcta":"0",
 },
],
 "pregunta":{
 "c03id_tipo_pregunta":"2",
 "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br>¿Cuáles son las características de la monografía?",

 "t11instruccion": "",
 }
 },
   //12
 {
        "respuestas": [
            {
                "t13respuesta": "Contiene  los datos de la institución, título del trabajo, nombre del investigador, el lugar y la fecha del informe.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presenta el nombre de los capítulos, enumeraciones y apartados de la investigación.",
                "t17correcta": "2"
            },

            {
                "t13respuesta": "En ella se leen los motivos de la elección del tema, los objetivos de la investigación y la estructura del trabajo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Es un listado de todas las revistas, libros, enciclopedias y fuentes digitales de las cuales se obtuvo información. ",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Contiene la síntesis del trabajo presentado y <br>las recomendaciones a los lectores que quieran realizar investigaciones a futuro.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Es el desarrollo de la monografía, donde se incluyen los apartados del tema y elementos gráficos.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Portada"
            },
            {
                "t11pregunta": "Índice"
            },
            {
                "t11pregunta": "Introducción"
            },
            {
                "t11pregunta": "Fuentes consultadas"
            },
            {
                "t11pregunta": "Conclusión"
            },
            {
                "t11pregunta": "Cuerpo del trabajo"
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
 ];
