json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Utilizar lenguaje claro y con palabras adecuadas al tipo de público.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Realizar una investigación profunda con fuentes de consulta serias.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Seleccionar la información relevante del tema de acuerdo al enfoque de la exposición.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No hacer una exposición de más de 20 minutos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Improvisar toda la información que se va a exponer.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Utilizar tecnicismos sin importar que no sean comprendidos por la audiencia.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Basar la exposición en la simpatía y no en la información.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Emplear dos horas para hablar de un tema.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br>¿Qué aspectos deben tomarse en cuenta para una exposición oral?",
            
        }
    },
    // Reactivo 2
    {
        "respuestas": [
            {
                "t13respuesta": "Es breve y señala el tema elegido.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Permite poner en contexto a la audiencia acerca del tema que se va a presentar.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Descripción del contenido del tema y de la información investigada a partir de diferentes materiales de apoyo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Cierre de la exposición y presentación de los resultados de la investigación.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Título"
            },
            {
                "t11pregunta": "Introducción"
            },
            {
                "t11pregunta": "Desarrollo"
            },
            {
                "t11pregunta": "Conclusión"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.<br>"
        }
    },
    //Reactivo 3
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la poesía de vanguardia, los autores rompen con las reglas tradicionales del género.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La poesía de vanguardia se basa en tradiciones orientales de China y Japón.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La poesía de vanguardia respeta la rima, la estrofa y la métrica.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los temas de la poesía de vanguardia son exclusivamente de amor.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los caligramas y haikús son un ejemplo de la poesía de vanguardia.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<br><\/p>",
            "descripcion": "Aspectos a valorar",   
            "variante": "editable",
            "evaluable"  : true
        }        
    },
    //Reactivo 4
    {
        "respuestas": [
            {
                "t13respuesta": "Futurismo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Surrealismo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Dadaísmo",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Creacionismo",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Caligrama",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Haikús",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Movimiento literario que plasma situaciones acerca de cómo puede ser el futuro."
            },
            {
                "t11pregunta": "Se basa en destacar la imaginación sobre la razón."
            },
            {
                "t11pregunta": "Movimiento artístico basado en el desarrollo lógico de lo absurdo."
            },
            {
                "t11pregunta": "En este género el autor evita usar anécdotas, signos de puntuación y elementos tipográficos."
            },
            {
                "t11pregunta": "Se considera un poema visual."
            },
            {
                "t11pregunta": "Son poemas de origen japonés de no más de tres versos."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.<br>"
        }
    },
    //Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "Aliteración",
                "t17correcta": "1",
                "numeroPregunta":"0"
            },
            {
                "t13respuesta": "Personificación",
                "t17correcta": "0",
                "numeroPregunta":"0"
            },
            {
                "t13respuesta": "Metáfora",
                "t17correcta": "0",
                "numeroPregunta":"0"
            },
            {
                "t13respuesta": "Personificación",
                "t17correcta": "1",
                "numeroPregunta":"1"
            },
            {
                "t13respuesta": "Asíndeton",
                "t17correcta": "0",
                "numeroPregunta":"1"
            },
            {
                "t13respuesta": "Comparación",
                "t17correcta": "0",
                "numeroPregunta":"1"
            },
            {
                "t13respuesta": "Asíndeton",
                "t17correcta": "1",
                "numeroPregunta":"2"
            },
            {
                "t13respuesta": "Comparación",
                "t17correcta": "0",
                "numeroPregunta":"2"
            },
            {
                "t13respuesta": "Aliteración",
                "t17correcta": "0",
                "numeroPregunta":"2"
            },
            {
                "t13respuesta": "Comparación",
                "t17correcta": "1",
                "numeroPregunta":"3"
            },
            {
                "t13respuesta": "Aliteración",
                "t17correcta": "0",
                "numeroPregunta":"3"
            },
            {
                "t13respuesta": "Asíndeton",
                "t17correcta": "0",
                "numeroPregunta":"3"
            }
        ],
        "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>“Mi mamá me mima”, es un ejemplo de:",
                         "<br>“El cielo llora de tristeza por tu ausencia”, es un ejemplo de:",
                         "<br>“Ríe, llora, sueña, ama, vive”, es un ejemplo de:",
                         "<br>“Tus dientes tan hermosos y blancos como perlas”, es un ejemplo de:"],
         "preguntasMultiples": true
        }
    },
    //Reactivo 6
     {
        "respuestas": [
            {
                "t13respuesta": "Solicitamos la colocación de aire acondicionado, debido a que en el comedor hace mucho calor.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Exigimos que se aumenten las medidas de higiene en el lugar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Los cocineros deberían utilizar una cofia para evitar los cabellos en la comida.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Pedimos que las personas del comedor pongan más atención a la higiene general.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Queremos felicitar al departamento de limpieza, ya que las instalaciones siempre están en excelentes condiciones.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Solicitamos que compren mesas nuevas, pues las que hay están muy sucias.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Solicitamos que nos den más días de descanso por enfermedad.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para la pregunta.<br><br>¿Qué enunciados deberían incluir los empleados en una carta de petición o reclamo?",
            "t11instruccion":"Las siguientes frases son parte del buzón de sugerencias del comedor de una fábrica.  <UL type=disk> <LI>El comedor es oscuro y poco iluminado, además a la hora de la comida hace mucho calor.<LI>¡Por favor pongan aire acondicionado! Es imposible estar en el comedor más de 20 minutos.<LI>He encontrado cabellos en mi comida en diferentes días. A varios compañeros les ha ocurrido lo mismo. Tengan más cuidado.<LI>Ayer cuando llegué a comer, estaban las mesas sucias y había muchos trastes encima de ellas. También estaba sucio el piso.<LI>¡Ya no vuelvo a comer en ese lugar! Encontré una cucaracha en los frijoles.<LI>Tuve que faltar tres días al trabajo porque la comida me hizo daño.<LI>En el comedor hace mucho calor y hay muchos insectos en el piso.</UL>"
            
        }
    },
    //Reactivo 7
     {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los alumnos deben exigir que el profesor sea expulsado.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La carta deberá estar dirigida a sus papás, para que ellos resuelvan la situación",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los alumnos pueden solicitar que se les evalúe nuevamente.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los alumnos deben argumentar que su bajo desempeño se debe a la ausencia del profesor y a su poca disponibilidad para apoyarlos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la carta deben exigir que a todos les pongan 10 de calificación sin tener que evaluarlos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La carta será enviada al final del ciclo escolar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los alumnos deben exponer lo complicado que es para ellos no contar con su profesor de manera presencial.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<br><br>El profesor de computación de la escuela “Héroes de la Nación”, tiene un negocio propio que atiende después de clases. Todos los lunes por la mañana, visita a unos clientes, por lo que falta a dar su clase.  Sus alumnos ya saben que él no asiste esos días y siempre trabajan de manera autodidacta.  <br>Sin embargo, tres cuartas partes del grupo reprobó la materia porque había temas en los que tenían dudas y el profesor no estaba disponible para resolverlas. Los alumnos están muy molestos y decidieron escribir una carta para solicitar apoyo en esta situación.<\/p>",
            "descripcion": "Aspectos a valorar",   
            "variante": "editable",
            "evaluable"  : true
        }        
    },
];
