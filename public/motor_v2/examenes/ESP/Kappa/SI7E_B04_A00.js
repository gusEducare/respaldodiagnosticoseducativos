json = [
    // 1
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Es un texto expositivo.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Explica el procedimiento de un experimento.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Posee descripciones de hechos empíricos y objetivos.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Emplea lenguaje especializado del tema en cuestión.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Utiliza lenguaje coloquial.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Relata hechos fantásticos.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Es un texto narrativo.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"Selecciona todas las respuestas correctas a la pregunta.<br><br>¿Qué características tiene un informe de investigación científica?"
        }
     },
     //2
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Introducción<\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>Desarrollo<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Premisas<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>Tesis<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 4"
            },
            {
                "t13respuesta": "<p>Argumentos<\/p>",
                "t17correcta": "4",
                etiqueta:"Paso 5"
            },
            {
                "t13respuesta": "<p>Conclusión<\/p>",
                "t17correcta": "5",
                etiqueta:"Paso 6"
            },
            {
                "t13respuesta": "<p>Bibliografía<\/p>",
                "t17correcta": "6",
                etiqueta:"Paso 7"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los elementos de un informe de investigación científica.",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>sin embargo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>y<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>o sea<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>o<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>pero<\/p>",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>El duende lanzó una maldición para la princesa, <\/p>"
            },
            {
                "t11pregunta": "<p>, no funcionó. La bruja enfureció al ver que la princesa era hermosa <\/p>"
            },
            {
                "t11pregunta": "<p> la envió al bosque. El hijo del rey, <\/p>"
            },
            {
                "t11pregunta": "<p> el príncipe, fue a rescatar a la princesa del castillo. La princesa tenía que elegir entre casarse con el príncipe <\/p>"
            },
            {
                "t11pregunta": "<p> con el duque. El príncipe estaba triste y decepcionado <\/p>"
            },
            {
                "t11pregunta": "<p> aún así, salió a cabalgar.<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen los enunciados.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":false
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "<p>,<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>(<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>:<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>¡<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>!<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>¿<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>?<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>...<\/p>",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><center><strong>Fin de semana en Suecia</center></strong>El verano pasado aprendí el significado de viaje rápido y de fin de semana.  Pero en esta ocasión fue un viaje especial. Estaba de vacaciones con mis papás en Helsinki<\/p>"
            },
            {
                "t11pregunta": "<p> Finlandia<\/p>"
            },
            {
                "t11pregunta": "<p>habíamos ido a visitar a un primo que vivía allá), cuando mi papá nos dio la noticia<\/p>"
            },
            {
                "t11pregunta": "<p><br>-<\/p>"
            },
            {
                "t11pregunta": "<p>Preparen una maleta pequeña, nos vamos de fin de semana a Estocolmo<\/p>"
            },
            {
                "t11pregunta": "<p><br>-<\/p>"
            },
            {
                "t11pregunta": "<p>A Estocolmo<\/p>"
            },
            {
                "t11pregunta": "<p> -preguntamos sorprendidos.<br>-Así es. Y el barco sale esta tarde -respondió mi padre.<br><br>De inmediato comenzamos a arreglar nuestro equipaje, estábamos ansiosos por iniciar esta nueva aventura.<br>Esta historia continuará<\/p>"
            },
            {
                "t11pregunta": "<p> <\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los signos de puntuación que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":false
        }
    },
    //5
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Una batalla en Zacatecas",
              "t17correcta":"1",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"Una reunión familiar en Zacatecas",
              "t17correcta":"0",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"La historia del estado de Zacatecas",
              "t17correcta":"0",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"El 23 de julio de 1914",
              "t17correcta":"1",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"El 14 de junio de 1923",
              "t17correcta":"0",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"Entre las 5 y las 6 de la tarde",
              "t17correcta":"0",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"Francisco Villa",
              "t17correcta":"1",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"Pánfilo Natera",
              "t17correcta":"0",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"El Grillo",
              "t17correcta":"0",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"El disparo de un cañón",
              "t17correcta":"1",
              "numeroPregunta": "3"
           },
           {  
              "t13respuesta":"La invasión",
              "t17correcta":"0",
              "numeroPregunta": "3"
           },
           {  
              "t13respuesta":"Cuando ondearon la bandera",
              "t17correcta":"0",
              "numeroPregunta": "3"
           }
        ],

        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Lee el texto y selecciona la respuesta correcta.<br><br>¿Cuál es el tema principal del corrido?",
                         "¿Qué día fue tomada Zacatecas?",
                         "¿Cuál es el nombre del líder del batallón?",
                         "¿Qué elemento detonó el combate?"],
                         "preguntasMultiples": true,
           "t11instruccion" : "<center><strong>De la toma de Zacatecas</center></strong>Voy a cantar estos versos, <br>de tinta tienen sus letras,<br> voy a cantarles a ustedes <br>la toma de Zacatecas.<br><br>Mil novecientos catorce, <br>mes de junio veintitrés, <br>fue tomado Zacatecas<br> entre las cinco y las seis.<br><br>Gritaba Francisco Villa <br>en la estación de Calera:<br> vamos a darle la mano<br> a don Pánfilo Natera.<br><br>Ya tenían algunos días<br> que se estaban agarrando, <br>cuando llega el general<br> a ver qué estaba pasando.<br><br>Cuando llega Pancho Villa <br>sus medidas fue tomando: <br>a cada quien en su puesto<br> los iba posesionando.<br><br>Les decía Francisco Villa <br>al frente del Batallón;<br> para empezar el combate<br> al disparo de un cañón.<br><br>Al disparo de un cañón, <br> como lo tenían de acuerdo, <br>empezó duro el combate<br> por el lado derecho e izquierdo.<br>(1914)"
           
        }
     },
     //6
     {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los temas principales en la lírica tradicional son anécdotas, hechos históricos o inconformidades.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las calaveras se caracterizan por ser textos en los que se plasma el temor a la muerte.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lenguaje en este la lírica tradicional mexicana es formal y poético.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las composiciones de la lírica tradicional mexicana son un reflejo de la sociedad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La mayoría de las composiciones de la lírica tradicional mexicana son de invención popular.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La lírica tradicional mexicana suele ser musicalizada.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false,
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Cada quién escoge el tamaño de la cebolla con la que va a llorar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Hay viene la huesuda pa llevarse a los glotones. Al fin y al cabo de ellos, están llenos los panteones.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "La vecina de allí enfrente, tiene una panadería; a los casados les vende… y a los solteros, les fía.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Siete Leguas el caballo que Villa más estimaba, cuando oía silbar los trenes se paraba y relinchaba.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Refrán"
            },
            {
                "t11pregunta": "Calavera"
            },
            {
                "t11pregunta": "Copla"
            },
            {
                "t11pregunta": "Canción"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12"
        }
    } ,
        //8
        {  
            "respuestas":[  
               {  
                  "t13respuesta":"Deportivo",
                  "t17correcta":"1",
                  "numeroPregunta": "0"
               },
               {  
                  "t13respuesta":"Dibujos animados",
                  "t17correcta":"0",
                  "numeroPregunta": "0"
               },
               {  
                  "t13respuesta":"Documental",
                  "t17correcta":"0",
                  "numeroPregunta": "0"
               },
               {  
                  "t13respuesta":"Documental",
                  "t17correcta":"1",
                  "numeroPregunta": "1"
               },
               {  
                  "t13respuesta":"Talk show",
                  "t17correcta":"0",
                  "numeroPregunta": "1"
               },
               {  
                  "t13respuesta":"Reality show",
                  "t17correcta":"0",
                  "numeroPregunta": "1"
               },
               {  
                  "t13respuesta":"Talk show",
                  "t17correcta":"1",
                  "numeroPregunta": "2"
               },
               {  
                  "t13respuesta":"Comedia",
                  "t17correcta":"0",
                  "numeroPregunta": "2"
               },
               {  
                  "t13respuesta":"Concurso",
                  "t17correcta":"0",
                  "numeroPregunta": "2"
               },
               {  
                  "t13respuesta":"Ficción",
                  "t17correcta":"1",
                  "numeroPregunta": "3"
               },
               {  
                  "t13respuesta":"Reality show",
                  "t17correcta":"0",
                  "numeroPregunta": "3"
               },
               {  
                  "t13respuesta":"Noticiero",
                  "t17correcta":"0",
                  "numeroPregunta": "3"
               },
               {  
                  "t13respuesta":"Comedia",
                  "t17correcta":"1",
                  "numeroPregunta": "4"
               },
               {  
                  "t13respuesta":"Deportivo",
                  "t17correcta":"0",
                  "numeroPregunta": "4"
               },
               {  
                  "t13respuesta":"Ficción",
                  "t17correcta":"0",
                  "numeroPregunta": "4"
               }
            ],
    
            "pregunta":{  
               "c03id_tipo_pregunta":"1",
               "t11pregunta":["Selecciona la respuesta correcta.<br><br>Tipo de programa que incluye reportajes, debates y noticias relacionadas con el deporte, así como transmisiones de juegos y torneos.",
                             "Programa que muestra el resultado de una investigación acerca de algún tema específico y que es de interés para la sociedad.",
                             "Género televisivo en el que se reúnen diferentes expertos o involucrados en algún tema y conversan acerca de él.",
                             "Un ejemplo de este género son las series y las películas.",
                             "Programas cuyo objetivo es hacer reir a los televidentes."
                            ],
                             "preguntasMultiples": true,
            }
         },
         //9
         {
            "respuestas": [
                {
                    "t13respuesta": "Programas aptos para adolescentes de 15 años.",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "Programas para adultos que se transmiten a partir de las 22 hrs.",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "Programas para todo público.",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "Programas transmitidos a partir de las 20 hrs.",
                    "t17correcta": "4"
                },
            ],
            "preguntas": [
                {
                    "t11pregunta": "Clasificación B15"
                },
                {
                    "t11pregunta": "Clasificación C" 
                },
                {
                    "t11pregunta": "Clasificación A"
                },
                {
                    "t11pregunta": "Clasificación B"
                },
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Relaciona las columnas según corresponda.",
                
                }
            },
            //10
            {
                "respuestas": [
                    {
                        "t13respuesta": "Falso",
                        "t17correcta": "0"
                    },
                    {
                        "t13respuesta": "Verdadero",
                        "t17correcta": "1"
                    }
                ],
                "preguntas" : [
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "Los programas televisivos únicamente tienen influencia en los niños y adolescentes.",
                        "correcta"  : "0"
                    },
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "Algunos programas televisivos cambian nuestra imagen del mundo y podemos ser manipulados si son nuestra única fuente de información.",
                        "correcta"  : "1"
                    },
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "Absolutamente todos los programas televisivos presentan información honesta.",
                        "correcta"  : "0"
                    },
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "Algunos de los programas pueden ser una influencia negativa en nuestras normas y valores.",
                        "correcta"  : "1"
                    },
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "Dedicar más de 5 horas de televisión al día te permite ser una persona informada.",
                        "correcta"  : "0"
                    },
                ],
                "pregunta": {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
                    "descripcion":"Reactivo",
                    "evaluable":false,
                    "evidencio": false,
                }
            },
];