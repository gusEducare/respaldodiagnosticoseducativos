json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Examen oral",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Examen escrito",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Examen de desarrollo",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Examen de opción múltiple",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "El examen de Ciencias consistió en tener una conversación con la profesora acerca de cómo llevar una vida saludable."
            },
            {
                "t11pregunta": "El profesor de Geografía nos entregó una hoja con preguntas a resolver."
            },
            {
                "t11pregunta": "El profesor de Historia nos pidió responder una sola pregunta en la que debíamos escribir toda la información que conociéramos acerca de Las Cruzadas."
            },
            {
                "t11pregunta": "El examen de Matemáticas contenía 10 preguntas con diferentes opciones para responder."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Preguntas cerradas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Preguntas abiertas",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calor es un tipo de energía.   Falso  o verdadero",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "¿Por qué es importante que la ciudadanía esté informada acerca de las acciones del gobierno?",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ordena del 1 al 3 las partes del ensayo:  Desarrollo Introducción Conclusiones",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El guión de radio se compone de <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</u> y acotaciones.<br><b><i>a.</i></b> diálogos <br><b><i>b.</i></b> comentarios<br><b><i>c.</i></b> descripciones",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "¿Cuáles son las partes de la célula?",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Explica tres acciones para reducir la contaminación ambiental. ",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Preguntas cerradas o Preguntas abiertas según corresponda.<\/p>",
            "descripcion": "Indicación",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "¿A qué edad comenzaste a ser bombero?",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "¿Cómo fue tu primera experiencia en un incendio?",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "¿Qué te motivó a elegir este trabajo?",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "¿En dónde te capacitaste para ser bombero?",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "¿Cuál es tu opinión acerca del calentamiento global?",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "¿Qué acciones realizas para evitar la contaminación del agua?",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "¿Cuál es el tipo de cambio de hoy?",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "¿Qué preguntas harías a un bombero para escribir su biografía?",
            "t11instruccion": "",
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Biografía",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Autobiografía",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Está escrito en tercera persona.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La fuente de información es quien escribe el texto.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuenta la historia de alguien más.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es necesario consultar distintas fuentes para recopilar información.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Está escrito en primera persona.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El autor cuenta su propia historia.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Biografía o Autobiografía según corresponda.<\/p>",
            "descripcion": "Características",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Antes de transmitir un programa de radio, se debe cuidar que la escena tenga una buena iluminación y maquillaje para los actores.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La radio es el medio de comunicación que se transmite vía ondas sonoras y combina el lenguaje oral con elementos musicales. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función de un guion es que los locutores tengan una guía del contenido y del programa que se transmitirá.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La radio tiene como función informar y entretener a través de imágenes impactantes para el espectador.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un guion de radio contiene la estructura de las secciones del programa, presenta contenido e incluye datos técnicos. ",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p> Elige falso (F) o verdadero (V) según corresponda.</p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Es la persona encargada de planear el programa de radio.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es la voz que está frente al micrófono durante el programa de radio.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Es quien se encarga de controlar los niveles de voz, la música y los efectos de sonido.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Elementos musicales que se utilizan para diferenciar las secciones de un programa.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Es la etapa de planeación de un programa de radio.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Productor"
            },
            {
                "t11pregunta": "Locutor"
            },
            {
                "t11pregunta": "Operador"
            },
            {
                "t11pregunta": "Cortinilla"
            },
            {
                "t11pregunta": "Preproducción"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El reportaje se difunde a través de periódicos, revistas, radio, televisión o internet.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El reportaje trata exclusivamente de temas académicos o culturales.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función del reportaje es dar a conocer un hecho o descubrimiento hecho a partir de un trabajo de investigación.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función del reportaje es dar a conocer un hecho  a partir de un trabajo de investigación.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p> Elige falso (F) o verdadero (V) según corresponda.</p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Su objetivo es dar a conocer información importante sobre un hecho o un descubrimiento de interés general.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es información importante recopilada por el investigador o reportero. Es muy útil para plasmar las respuestas de una entrevista.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Es un acto de comunicación que busca obtener información de expertos y/o testigos.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Son preguntas que no permiten expresar la opinión del entrevistado.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Son textos que se mencionan de manera exacta y fidedigna a lo dicho por la fuente de información de donde fue obtenida.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Es el uso de las palabras propias del autor para explicar lo que otros dijeron.",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "Son preguntas en las que el entrevistado puede opinar acerca del tema.",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Reportaje"
            },
            {
                "t11pregunta": "Notas"
            },
            {
                "t11pregunta": "Entrevista"
            },
            {
                "t11pregunta": "Preguntas cerradas"
            },
            {
                "t11pregunta": "Citas textuales"
            },
            {
                "t11pregunta": "Paráfrasis"
            },
            {
                "t11pregunta": "Preguntas abiertas"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Es una forma de narrar un cuento; determina el punto de vista o la perspectiva que se tendrá de este.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es la descripción del lugar en donde se desarrollan los hechos del cuento.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Es la duración de los hechos dentro del relato.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Los hechos del cuento suceden en torno a él o ella. ",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Es una forma de presentar los hechos de un cuento en orden. La integran la introducción, el desarrollo y el desenlace.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Es la voz que narra los sucesos dentro de la historia. El protagonista nos cuenta la historia del cuento.",
                "t17correcta": "6",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Voces narrativas"
            },
            {
                "t11pregunta": "Espacio"
            },
            {
                "t11pregunta": "Tiempo"
            },
            {
                "t11pregunta": "Personaje principal"
            },
            {
                "t11pregunta": "Estructura del cuento"
            },
            {
                "t11pregunta": "Voz en primera persona"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //10
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Una comparación mediante <br>un nexo para relacionar dos <br>elementos con cierta similitud.<\/p>",
                "t17correcta": "1",

            },
            {
                "t13respuesta": "<p>Tiene tantas manchas<br>como un dálmata.<\/p>",
                "t17correcta": "2"
                ,
            },
            {
                "t13respuesta": "<p>Asigna propiedades a otra <br>cosa aunque no tengan una <br>relación racional o literal.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Mi corazón se<br>detuvo al verla.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Describe a un sustantivo, le<br>atribuye características.<\/p>",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "<p>Rojo, hermoso,<br>grande, liso.<\/p>",
                "t17correcta": "6",

            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; }\n\
                                   </style><table class='table' style='margin-top:-40px;'>\n\
                                   <tr>\n\
                                       <td style='width:350px'>Figura retórica</td>\n\
                                       <td style='width:100px'>Definición</td>\n\
                                       <td style='width:100px'>Ejemplos</td>\n\
                                   </tr>\n\
                                   <tr>\n\
                                       <td ><b>Símil</b></td> <td >"
            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": " </td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "     </td></tr><tr>\n\
                                       <td><b>Metáfora</b></td> <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "       </td><td>"

            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td> </tr><tr>\n\ <td><b>Adjetivo</b></td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td>  <td>"

            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td>\n\
                                       </tr></table>"

            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "contieneDistractores": true,
            "anchoRespuestas": 20,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Indica acciones<br>del pasado que fueron<br>constantes o indefinidas.<\/p>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<p>Parecía, comía,<br>salía, corría.<\/p>",
                "t17correcta": "2",

            },
            {
                "t13respuesta": "<p>Describe acciones que pudieron<br>suceder pero no se realizaron,<br>o acciones que se pueden<br>realizar; están narradas<br>desde un tiempo pasado.<\/p>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<p>Podría, comería,<br>saldría, nadaría.<\/p>",
                "t17correcta": "4",

            },
            {
                "t13respuesta": "<p>Indica que la acción<br>ocurrió en el pasado y concluyó<br>de manera definitiva.<\/p>",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "<p>Durmió, comió,<br>raspó, calló.<\/p>",
                "t17correcta": "6",

            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; }\n\
                                   </style><table class='table' style='margin-top:-40px;'>\n\
                                   <tr>\n\
                                       <td style='width:350px'>Figura retórica</td>\n\
                                       <td style='width:100px'>Definición</td>\n\
                                       <td style='width:100px'>Ejemplos</td>\n\
                                   </tr>\n\
                                   <tr>\n\
                                       <td ><b>Pretérito<br>imperfecto</b></td> <td >"
            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": " </td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "     </td></tr><tr>\n\
                                       <td><b>Pospretérito</b></td> <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "       </td><td>"

            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td> </tr><tr>\n\ <td><b>Pretérito<br>simple</b></td><td>"
            },            
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td>  <td>"

            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td>\n\
                                       </tr></table>"

            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "contieneDistractores": true,
            "anchoRespuestas": 20,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    
];
