
json = [
   {
        "respuestas": [
            {
                "t13respuesta": "T1",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "T2",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El texto habla del declive de la industria discográfica.",
                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El texto habla de los cambios en la manera de comprar música.",
                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se menciona cómo una famosa banda de rock demandó a una empresa para que cerrara sus puertas.",
                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Explica cómo se solían comprar discos y cassettes hace algunos años.",
                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El texto expone una postura positiva ante la compra de música por internet.",
                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El texto menciona cómo la compra de música por internet ha afectado la economía de algunas. ",
                
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><b>Lee los siguientes textos y elige texto 1 (T1) o texto 2 (T2) según corresponda.</b><\/p>",
            "t11instruccion":"<center><b>Texto 1<br>La revolución musical de internet</b></center><br><br>Internet ha cambiado nuestra forma de escuchar música. Antes, la manera de adquirir la música era comprando Long Plays (LP, ahora conocidos como acetatos), cassettes o CD´s. La dinámica consistía en acudir a un centro comercial, comprar el producto y escuchar las 10 o 12 canciones que componía el material. Probablemente, solo una canción podía ser de tu agrado, pero aún así, tenías que adquirir todo el disco.<br><br>En la actualidad, no es necesario acudir a una tienda para comprar música y mucho menos se requiere comprar un disco para poder escuchar una sola canción.  Todo ha cambiado.  Desde tu computadora, tableta o teléfono puedes comprar la canción que te guste y descargarla en tu dispositivo.  ¡Ya no es necesario trasladarse a un centro comercial! Además, con un solo click, puedes acceder a música de diferentes géneros y de distintas partes del mundo.  Sin duda, internet nos ha acercado a todo tipo de música.<br><br><center><b>Texto 2<br>La decadencia de la industria discográfica</b></center><br><br>En el año 1999 comenzó el rumor acerca de un sitio de internet llamado Napster, que permitía descargar música de manera gratuita. Por supuesto, no tardó en popularizarse, finalmente, los usuarios podían escuchar toda su música sin tener que pagar grandes cantidades de dinero por un material discográfico.  Más tarde, la banda de rock Metallica interpuso una demanda contra esta página web para impedir que se descargara su música.  Con el paso del tiempo, esto provocó que Napster desapareciera y todo volviera a una normalidad aparente.<br><br>Poco a poco comenzaron a surgir diferentes formas de descargar música, ya sea gratuita o no.  Los más afectados con esta situación fueron las disqueras, quienes dejaron de vender aún más discos, pues ya padecían esta situación desde la aparición de la piratería.<br><br>En la actualidad, las disqueras siguen sufriendo de una crisis por la poca venta de discos. Aunque, por otro lado, han encontrado la manera de continuar vigentes.  Ahora es posible descargar discos completos en las diferentes plataformas de música, previo el pago de dicho producto.<br><br>Por último, las disqueras han tenido que buscar la manera de seguir funcionando y no irse a bancarrota, desafortunadamente, este tipo de prácticas de consumo ha provocado que muchas personas se queden sin trabajo.",
            "descripcion": "Reactivo",   
            "variante": "editable", 
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
     {
        "respuestas": [
            {
                "t13respuesta": "Entrevista a un empleado del zoológico.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Enciclopedia automotriz.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Folletos sobre sitios interesantes en Nueva York.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Instructivo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Recetario de cocina.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La vida de los animales del zoológico."
            },
            {
                "t11pregunta": "Funcionamiento de un automóvil."
            },
            {
                "t11pregunta": "Sitios turísticos en Nueva York."
            },
            {
                "t11pregunta": "Cómo armar un invernadero."
            },
            {
                "t11pregunta": "Cómo preparar chiles en nogada."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "<b>Relaciona las columnas según corresponda a la fuente de consulta para cada tema.</b>",
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>como<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>por esa razón<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Además<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p> a diferencia de<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>Es por eso que<\/p>",
                "t17correcta": "5"
            }, 
            {
                "t13respuesta": "<p> y ahora<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p> incluso<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            
            {
                "t11pregunta": "<p>La vida en los años cincuenta era muy diferente a <\/p>"
            },
            {
                "t11pregunta": "<p> la conocemos ahora.  Fue una época en la que surgieron diferentes productos electrónicos, <\/p>"
            },
            {
                "t11pregunta": "<p>, aumentó su consumo. <\/p>"
            },
            {
                "t11pregunta": "<p>, los diseñadores industriales de esa época se preocuparon por hacer productos funcionales, durables y bonitos; <\/p>"
            },
            {
                "t11pregunta": "<p> los productos actuales, que tienen un tiempo de vida útil más corto. <\/p>"
            },
            {
                "t11pregunta": "<p> todo aquello sigue vigente, <\/p>"
            },
            {
                "t11pregunta": "<p>  es muy común encontrar lugares decorados al estilo de aquellos años e <\/p>"
            },
            {
                "t11pregunta": "<p> ver personas que utilizan ropa de esa época, actualmente llamada <b>Rockabilly</b>. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<b>Arrastra las palabras para completar el párrafo.</b>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Tomar bebidas calientes, antigripales y descansar en casa.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Evitar comer dulces y alimentos azucarados.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Comer únicamente verduras y pollo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Evitar la comida chatarra.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Visitar al dentista.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Resfriado"
            },
            {
                "t11pregunta": "Diabetes"
            },
            {
                "t11pregunta": "Malestar estomacal"
            },
            {
                "t11pregunta": "Obesidad"
            },
            {
                "t11pregunta": "Dolor de muelas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "<b>Relaciona las columnas según corresponda.</b>",
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mambrú es el nombre de un agricultor.",
                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La canción trata de un hombre que muere en la guerra.",
                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La canción habla de cómo un hombre pelea durante una guerra.",
                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mambrú fue enterrado en una caja lujosa de terciopelo y cristal.",
                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La canción narra la tragedia de manera alegre y festiva.",
                
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><b>Lee el siguiente texto y elige falso (F) o verdadero (V) según corresponda.</b><\/p>",
            "t11instruccion":"MAMBRÚ SE FUE A LA GUERRA<br><br>Mambrú se fue a la guerra,<br>mire usted, mire usted, que pena.<br>Mambrú se fue a la guerra,<br>no sé cuándo vendrá.<br>Do-re-mi<br>do-re-fa.<br>No sé cuándo vendrá.<br><br>Si vendrá por la Pascua,<br>mire usted, mire usted, qué gracia.<br>Si vendrá por la Pascua<br>por la Trinidad.<br>Do-re-mi,<br>do-re-fa.<br>O por la Trinidad.<br><br>La Trinidad se pasa,<br>mire usted, mire usted, qué guasa.<br>La Trinidad se pasa.<br>Mambrú no viene ya,<br>Do-re-mi,<br>do-re-fa.<br>Mambrú no viene ya.<br><br>Por allí viene un paje,<br>¡qué dolor, qué dolor, qué traje!<br>por allí viene un paje,<br>¿qué noticias traerá?<br>Do-re-mi, do-re-fa,<br>¿qué noticias traerá?<br><br>Las noticias que traigo,<br>¡del dolor, del dolor me caigo!<br>las noticias que traigo<br>son tristes de contar,<br>Do-re-mi, do-re-fa,<br>son tristes de contar.<br>Que Mambrú ya se ha muerto,<br>¡qué dolor, qué dolor, qué entuerto!,<br>que Mambrú ya se ha muerto,<br>lo llevan a enterrar.<br>Do-re-mi,<br>do-re-fa,<br>lo llevan a enterrar.<br><br>En caja de terciopelo,<br>¡qué dolor, qué dolor, qué duelo!,<br>en caja de terciopelo,<br>y tapa de cristal.<br>Do-re-mi, do-re-fa,<br>y tapa de cristal.<br><br>Y detrás de la tumba,<br>¡qué dolor, qué dolor, qué turba!,<br>y detrás de la tumba,<br>tres pajaritos van.<br>Do-re-mi,<br>do-re-fa,<br>tres pajaritos van.<br><br>Cantando el pío-pío,<br>¡qué dolor, qué dolor, qué trío!,<br>cantando el pío-pío,<br>cantando el pío-pá.<br>Do-re-mi,<br>do-re-fa,<br>cantando el pío-pá",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    {  
        "respuestas":[  
           {  
              "t13respuesta":"El poema en náhuatl tiene rima.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"El poema en español pierde la rima.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Ambos poemas están escritos en forma de verso.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Ambos poemas están escritos en forma de prosa.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"El poema en náhuatl no tiene rima.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"El poema en español tiene rima.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"<b>Selecciona todas las respuestas correctas para la pregunta.</b><br><br>Después de leer el poema, elige las aseveraciones que sean correctas.",
           "t11instruccion":"<center><b>Poema en náhuatl<br>Nonantzin</b></center><br><br>Nonantzin ihcuac nimiquiz,<br>motlecuilpan xinechtoca<br>huan cuac tiaz titlaxcal chihuaz,<br>ompa nopampa xichoca.<br>Huan tla acah mitztlah tlaniz:<br>-Zoapille, ¿tleca tichoca?<br>xiquilhui xoxouhqui in cuahuitl,<br>techochcti ica popoca.<br>Netzahualcóyotl<br><br><center><b>Poema en español<br>Madrecita mía</b></center><br><br>Madrecita mía, cuando yo muera,<br>sepúltame junto al fogón<br>y cuando vayas a hacer las tortillas<br>allí por mí llora.<br>Y si alguien te preguntara:<br>-Señora, ¿por qué lloras?<br>dile que está verde la leña,<br>hace llorar con el humo."
        }
     },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Hola<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> Finlandia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>contento<\/p>",
                "t17correcta": "3"
            }, 
            {
                "t13respuesta": "<p>acá<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>bicicleta<\/p>",
                "t17correcta": "5"
            }, 
            {
                "t13respuesta": "<p>parque<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>mejoría<\/p>",
                "t17correcta": "7"
            }, 
            {
                "t13respuesta": "<p>escribir<\/p>",
                "t17correcta": "8"
            }, 
            {
                "t13respuesta": "<p> balcón<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>pronto<\/p>",
                "t17correcta": "10"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p> <\/p>"
            },
            {
                "t11pregunta": "<p> Fabián: <br>Desde que te fuiste a <\/p>"
            },
            {
                "t11pregunta": "<p>, no he tenido noticias tuyas.  Espero te encuentres muy  <\/p>"
            },
            {
                "t11pregunta": "<p> en tu nuevo trabajo y te estés adaptando al frío.<br>Por <\/p>"
            },
            {
                "t11pregunta": "<p> todo va muy bien. Carlita ya aprendió a caminar y Diego ya sabe andar en  <\/p>"
            },
            {
                "t11pregunta": "<p>, aunque extraña a su tío Fabi para que lo lleve al <\/p>"
            },
            {
                "t11pregunta": "<p>.<br>La tía Carmen ha mejorado mucho de su columna y cada vez camina mejor. Estoy segura de que si estuvieras aquí, te daría gusto ver su <\/p>"
            },
            {
                "t11pregunta": "<p>.<br>Yo estoy terminando de <\/p>"
            },
            {
                "t11pregunta": "<p> mi novela, una universidad aceptó publicarla y me siento muy feliz.  Te prometo que cuando esté publicada, te enviaré un ejemplar firmado para que lo leas en tu <\/p>"
            },
            {
                "t11pregunta": "<p>, mientras disfrutas la vista del lago que está frente a tu casa.<br>Me despido de ti y espero recibir tu respuesta <\/p>"
            },
            {
                "t11pregunta": "<p>, por favor no dejes de escribir.  Te queremos.<br>Ana<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<b>Arrastra las palabras que completen el párrafo.</b>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
     {
        "respuestas": [
            {
                "t13respuesta": "T",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "E",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ahí",
                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Recientemente",
                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Anoche",
                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Acá",
                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lejos",
                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Después ",
                
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><b>Elige tiempo (T) o espacio (E) según indique cada palabra.</b><\/p>",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
     
     {
        "respuestas": [
            {
                "t13respuesta": "<p><font size ='4'>Te quiero contar lo que me sucedió</font><\/p>",
                "t17correcta": "1,3,5,7,9"
            },
            {
                "t13respuesta": "<p><font size ='4'>Estimado delegado<\/p>",
                "t17correcta": "2,4,6,8,10"
            },
            {
                "t13respuesta": "<p><font size ='4'>Hola amigo<\/p>",
                "t17correcta": "1,3,5,7,9"
            },
             {
                "t13respuesta": "<p><font size ='4'>A quien corresponda<\/p>",
                "t17correcta": "2,4,6,8,10"
            },
            {
                "t13respuesta": "<p><font size ='4'>Querida tía<\/p>",
                "t17correcta": "1,3,5,7,9"
            },
            {
                "t13respuesta": "<p><font size ='4'>Me permito comentarle la situación</font><\/p>",
                "t17correcta": "2,4,6,8,10"
            },
             {
                "t13respuesta": "<p><font size ='4'>Te mando saludos<\/p>",
                "t17correcta": "1,3,5,7,9"
            },
            {
                "t13respuesta": "<p><font size ='4'>Me despido de usted y quedo a sus órdenes</font><\/p>",
                "t17correcta": "2,4,6,8,10"
            },
            {
                "t13respuesta": "<p><font size ='4'>Abrazos<\/p>",
                "t17correcta": "1,3,5,7,9"
            },
            {
                "t13respuesta": "<p>Saludos cordiales<\/p>",
                "t17correcta": "2,4,6,8,10"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",


                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'><td>Frases para escribirle a un amigo o familiar</td><td>Frases para escribirle a un destinatario desconocido</td>\n\
                                    <tr>\n\
                                      <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },
            
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                       <td>\n\
                                        "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },

             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                       <td>"
            },{
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            }, 
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                           <td>"

            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<b>Completa la tabla con los elementos de los recuadros que correspondan.</b>",
           "contieneDistractores":false,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            
            "ocultaPuntoFinal": true
        }
    },
    {
      "respuestas": [
          {
              "t13respuesta": "SI6E_B04_A11_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "SI6E_B04_A11_04.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "SI6E_B04_A11_01.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "SI6E_B04_A11_03.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "SI6E_B04_A11_05.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "SI6E_B04_A11_06.png",
              "t17correcta": "5",
              "columna":"1"
          },
          {
              "t13respuesta": "SI6E_B04_A11_07.png",
              "t17correcta": "6",
              "columna":"1"
          },
          {
              "t13respuesta": "SI6E_B04_A11_08.png",
              "t17correcta": "7",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p><b>Arrastra la imagen al espacio que corresponda.</b><br><br>Completa el siguiente formulario de inscripción a una carrera deportiva.<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"SI6E_B04_A11_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "115,60", "cuadrado", "118, 50", ".","transparent"]},
          {"Contenedor": ["", "170,7", "cuadrado", "118, 50", ".","transparent"]},
          {"Contenedor": ["", "78,464", "cuadrado", "118, 50", ".","transparent"]},
          {"Contenedor": ["", "115,319", "cuadrado", "118, 50", ".","transparent"]},
          {"Contenedor": ["", "169,519", "cuadrado", "118, 50", ".","transparent"]},
          {"Contenedor": ["", "260,268", "cuadrado", "118, 50", ".","transparent"]},
          {"Contenedor": ["", "316,464", "cuadrado", "118, 50", ".","transparent"]},
          {"Contenedor": ["", "404,133", "cuadrado", "118, 50", ".","transparent"]}
      ]
  },{  
        "respuestas":[  
           {  
              "t13respuesta":"Nombre completo del remitente y destinatario",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Dirección del remitente",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Dirección del destinatario",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Dirección de correo electrónico del remitente",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Nombre de usuario en redes sociales",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Edad del remitente y destinatario",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"<b>Selecciona todas las respuestas correctas para la pregunta.</b><br><br>¿Qué datos debe llevar un sobre para enviar una carta por correo postal?"
        }
     },

];