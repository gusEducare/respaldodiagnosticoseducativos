json=[
{
      "respuestas": [
          {
              "t13respuesta": "O",
              "t17correcta": "0"
          },
          {
              "t13respuesta": "C",
              "t17correcta": "1"
          }
      ],
      "preguntas" : [
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Las acciones de la historia están divididas en actos.",
              "correcta"  : "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Es un relato corto que narra una historia.",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Es un texto escrito para ser visto en un escenario.",
              "correcta"  : "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Es un relato breve que resuelve el conflicto rápidamente.",
              "correcta"  : "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "El texto incluye acotaciones.",
              "correcta"  : "0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "<p>Elige obra de teatro (O) o cuento (C) según corresponda.<\/p>",
          "descripcion":"Reactivo",
          "evaluable":false,
          "evidencio": false
      }
  },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1920<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>1930<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>500<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>1500<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Para la segunda década del siglo XX <\/p>"
            },
            {
                "t11pregunta": " ,toda Europa vivía una grave crisis económica, producto de la Primera Guerra Mundial. Una década después <\/p>"
            },
            {
                "t11pregunta": " , el sistema capitalista estadounidense sufrió los efectos de una crisis económica que elevó el desempleo de manera alarmante. <br> La Edad Media comprende mil largos años de historia, comienza en la mitad del primer milenio de nuestra era "
            },
            {
                "t11pregunta": " y termina en la mitad del segundo "
            },
            {
                "t11pregunta": "."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen los párrafos.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>inició<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> terminó<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>intervención<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>apellido<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>vandalismo <\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>barón <\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>indemnización<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>privilegios<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>La guerra de los pasteles <\/p>"
            },
            {
                "t11pregunta": "<p> el 16 de abril de 1838 y <\/p>"
            },
            {
                "t11pregunta": "<p> el 9 de marzo de 1839, y fue la primera <\/p>"
            },
            {
                "t11pregunta": "<p> francesa en México.<br><br> Este conflicto fue causado por un pastelero francés de <\/p>"
            },
            {
                "t11pregunta": "<p> Rammontel, quien sufrió de <\/p>"
            },
            {
                "t11pregunta": "<p> y exigió el pago de la mercancía y mobiliario de su local.  Él envió sus reclamos a París por medio del embajador francés, el <\/p>"
            },
            {
                "t11pregunta": "<p> Deffaudis, quien se encontraba en México realizando gestiones diplomáticas entre ambos países. <br><br> Al no llegar a un arreglo,  Deffaudis regresó a México el  21 de marzo de 1838 con barcos de guerra. Exigió  al gobierno mexicano el pago de la <\/p>"
            },
            {
                "t11pregunta": "<p> a los daños causados y además, pedía ciertos <\/p>"
            },
            {
                "t11pregunta": "<p> para los comerciantes franceses ubicados en México. <\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    {
       "respuestas": [
           {
               "t13respuesta": "<p>En 1920, Europa vivía una crisis económica, producto de la Primera Guerra Mundial.<\/p>",
               "t17correcta": "0",
               etiqueta:"paso 1"
           },
           {
               "t13respuesta": "<p>Una década después, Estados Unidos también sufrió una crisis económica y desempleo.<\/p>",
               "t17correcta": "1",
               etiqueta:"paso 2"
           },
           {
               "t13respuesta": "<p>Sin empleo, no había consumo y muchas empresas se fueron a la bancarrota.<\/p>",
               "t17correcta": "2",
               etiqueta:"paso 3"
           },
           {
               "t13respuesta": "<p>El cierre de las empresas provocó una depresión económica a nivel mundial.<\/p>",
               "t17correcta": "3",
               etiqueta:"paso 4"
           },
           {
               "t13respuesta": "<p>De ahí se deriva su nombre: “La Gran Depresión”.<\/p>",
               "t17correcta": "4",
               etiqueta:"paso 5"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "9",
           "t11pregunta": "<p>Ordena los siguientes hechos.<\/p>",
       }
   },
       {
       "respuestas": [
           {
               "t13respuesta": "<p>(Emocionado)<\/p>",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "<p>¿sabes qué es un tlacuache?<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p>(Sin dejar de ver su teléfono,<\/p>",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "<p> hace un silencio corto) <\/p>",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "<p>¡Wow!</font><\/p>",
               "t17correcta": "5"
           },
           {
               "t13respuesta": "<p> (Continúa viendo su teléfono)<\/p>",
               "t17correcta": "6"
           }
       ],
       "preguntas": [
           {
               "t11pregunta": " <br> Jorge: <\/p>"
           },
           {
               "t11pregunta": "<p> Hola Laura, tengo algo que platicarte. <br> Luisa: (Viendo su teléfono) Sí, dime. <br> Jorge: 	Tengo una nueva mascota, es un tlacuache, <\/p>"
           },
           {
               "t11pregunta": "<p> <br> Luisa: <\/p>"
           },
           {
               "t11pregunta": "<p><\/p>"
           },
           {
               "t11pregunta": "<p> Mmm… Es un mamífero    marsupial, parecido a la rata. Es nocturno, omnívoro y vive en América. Tiene una cola larga que utiliza para trasladar a sus crías y hocico puntiagudo. <br> Jorge: <\/p>"
           },
           {
               "t11pregunta": "<p> Tú sí que sabes sobre marsupiales. <br> Luisa: <\/p>"
           },
           {
               "t11pregunta": "<p> En internet encuentras todo. <\/p>"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "t11pregunta": "Arrastra los signos de admiración, de interrogación y paréntesis que completen el  párrafo. ",
           "respuestasLargas": true,
           "pintaUltimaCaja": false,
           "contieneDistractores":true,
           "t11instruccion":"<center><p>ACTO ÚNICO<br> Escena 1 </center> Luisa se encuentra sentada en una banca del parque viendo su teléfono. Jorge entra en escena y la saluda con un beso en la mejilla.<br> <br><\/p>"
       }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Estimado Director:",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Debido a las fuertes lluvias, tenemos goteras en el salón de clases y las bancas se mojan.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Solicitamos su apoyo para reparar el techo del salón 6B.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Esperemos contar con la solución lo antes posible.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "De antemano, gracias por su atención.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Saludos cordiales.",
                "t17correcta": "6 "
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Buenos días Director:"
            },
            {
                "t11pregunta": "¿Qué cree? Tenemos goteras en el salón y las bancas se mojan."
            },
            {
                "t11pregunta": "Necesitamos que arreglen el techo de nuestro salón."
            },
            {
                "t11pregunta": "¡Urge la reparación!"
            },
            {
                "t11pregunta": "Gracias."
            },
            {
                "t11pregunta": "Bye."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.",
            "t11instruccion":"Los alumnos de la escuela Héroes de la Nación, redactaron una carta a su director para solicitar la reparación de algunos desperfectos en su salón de clases.  Ahora, necesitan reescribirla utilizando el lenguaje adecuado para el destinatario."
        }
    },
    {
      "respuestas": [
          {
              "t13respuesta": "<p>5<\/p>",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "<p>3<\/p>",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "<p>1<\/p>",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "<p>4<\/p>",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "<p>2<\/p>",
              "t17correcta": "5"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Toluca, Estado de México; 14 de enero, 2015   <\/p>"
          },
          {
              "t11pregunta": "<p><br><br>Lic. Diana González Fernández <br>DIRECTORA DEL COLEGIO PAULO FREIRE <\/p>"
          },
          {
              "t11pregunta": "<p><br><br>Buenas tardes: <\/p>"
          },
          {
              "t11pregunta": "<p><br><br>   En los últimos días ha bajado mucho la temperatura en la ciudad, lo cual provoca que se sienta demasiado frío en la escuela. <\/p>"
          },
          {
              "t11pregunta": "<p><br><br> El reglamento escolar estipula que las niñas deben usar una falda debajo de la rodilla. Sin embargo, debido al frío ha habido muchas niñas con enfermedades de las vías respiratorias.  Por tal motivo, se solicita su autorización para que las niñas utilicen el pantalón del uniforme de educación física durante la temporada invernal. <br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Agradezco su atención, esperando contar con su aprobación. <br><br><center> Atentamente <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Danaé Martínez Rubio &nbsp;&nbsp;<\/p>"
          },
          {
              "t11pregunta": "<p><br> Jefa de Grupo 5A </center> <\/p>"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "8",
          "t11pregunta": "Arrastra el número al espacio que corresponda.",
          "respuestasLargas": true,
          "pintaUltimaCaja": false,
          "contieneDistractores":true
      }
  }
];
