
json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Símil",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aliteración",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Metáfora",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "En el Oriente se encendió esta guerracuyo anfiteatro es hoy toda la Tierra. Como el otro, este juego es infinito.<br>(Borges, J.)"
            },
            {
                "t11pregunta": "¡Se juntaron en una sus alas enemigas y anudaron el nudo de la muerte y la vida!<br>(Mistral, G.)"
            },
            {
                "t11pregunta": "Por eso vuelvo y me voy, vuelo y no vuelo pero canto: soy el pájaro furioso de la tempestad tranquila.<br>(Neruda, P.)"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Lee los siguientes fragmentos de poemas y relaciona las columnas según corresponda a cada ejemplo.",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Métrica",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sinalefa",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Versos de arte menor",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Versos de arte mayor",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Adjetivos",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Adverbios",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<font size=3>Número de sílabas métricas de cada verso cuya función es dar ritmo y entonación al verso."
            },
            {
                "t11pregunta": "<font size=3>Conjunto de sílabas que pertenecen a palabras diferentes y se cuentan como una sola."
            },
            {
                "t11pregunta": "<font size=3>Versos que tienen ocho sílabas o menos."
            },
            {
                "t11pregunta": "<font size=3>Versos que contienen nueve sílabas o más."
            },
            {
                "t11pregunta": "<font size=3>Son palabras que califican al sustantivo, ayudan a dar más significado a la poesía y crear imágenes."
            },
            {
                "t11pregunta": "<font size=3>Son las palabras que califican un verbo, un adjetivo o a otro adverbio en la oración. Ayudan a recrear las acciones y estados de ánimo."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Sinalefa ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verso",
                "t17correcta": "1"
            }
         
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una calle desierta imploré<br>que vinieras a verme.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando pares de hablar<br>No tendrás que callar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una tarde lluviosa<br>recorrí esa calle<br>que se hallaba fangosa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Redonda es la tierra<br>cuadrada es la mente<br>círculo es la historia<br>y lineal es la vida.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Esa sonrisa hermosa,<br>tan cálida e inspiradora.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "Elige sinalefa (S) o verso (V) según corresponda.", 
            "t11instruccion":"",
            "descripcion": "Reactivos",   
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable"  : true
        }        
        },
        //4
        {
            "respuestas": [
                {
                    "t13respuesta": "<p>Mensaje del director.<\/p>",
                    "t17correcta": "0",
                    etiqueta:"Paso 1"
                },
                {
                    "t13respuesta": "<p>Capítulos.<\/p>",
                    "t17correcta": "1",
                    etiqueta:"Paso 2"
                },
                {
                    "t13respuesta": "<p>Eventos.<\/p>",
                    "t17correcta": "2",
                    etiqueta:"Paso 3"
                },
                {
                    "t13respuesta": "<p>Fotografía de los eventos.<\/p>",
                    "t17correcta": "3",
                    etiqueta:"Paso 4"
                },
                {
                    "t13respuesta": "<p>Espacio para dedicatorias.<\/p>",
                    "t17correcta": "4",
                    etiqueta:"Paso 5"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "9",
                "t11pregunta": "Ordena los siguientes elementos.<br><br>Fuiste seleccionado para realizar el anuario de despedida del ciclo escolar. Ordena los elementos de su estructura.",
            }
        },
        //5
        {  
            "respuestas":[  
               {  
                  "t13respuesta":"Recopilar imágenes relevantes para el destinatario.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta":"Describir brevemente los eventos.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta":"Organizar la información a presentar.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta":"Ordenar los eventos cronológicamente.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta":"Evitar incluir anécdotas divertidas.",
                  "t17correcta":"0"
               },
               {  
                  "t13respuesta":"Presentar un diseño sobrio.",
                  "t17correcta":"0"
               }
            ],
            "pregunta":{  
               "c03id_tipo_pregunta":"2",
               "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br><br>¿Qué aspectos debes tomar en cuenta para elaborar un álbum?"
            }
         },
         //6
         {
            "respuestas": [
                {
                    "t13respuesta": "Eventos escolares",
                    "t17correcta": "0"
                },
                {
                    "t13respuesta": "Festividades",
                    "t17correcta": "1"
                }
            ],
            "preguntas" : [
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Intercambio de dulces del día del amor y la amistad.",
                    "correcta"  : "1"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Visita guiada al Museo de Historia Natural.",
                    "correcta"  : "0"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Concurso de oratoria a nivel nacional.",
                    "correcta"  : "0"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Festival del día de las madres.",
                    "correcta"  : "1"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Feria de Ciencias.",
                    "correcta"  : "0"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Posada y pastorela.",
                    "correcta"  : "1"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "Elige eventos escolares (E) o festividades (F) según corresponda para cada situación.", 
                "t11instruccion":"",
                "descripcion": "Reactivos",   
                "variante": "editable",
                "anchoColumnaPreguntas": 50,
                "evaluable"  : true
            }        
            },

];