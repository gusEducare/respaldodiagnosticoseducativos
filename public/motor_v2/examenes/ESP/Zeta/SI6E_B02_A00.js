json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Su difusión se hace a través de los periódicos, revistas, radio, televisión o internet.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Expresan exclusivamente la opinión del autor, sin consultar otras fuentes.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Pueden ser sobre información actual o investigaciones previas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Su función es dar a conocer un acontecimiento o un descubrimiento hecho a partir de un trabajo de investigación.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sus temas a tratar son exclusivamente sobre política.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona los enunciados que mencionen características de los reportajes."
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "Noticia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Reportaje",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Género periodístico que suele ser corto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su información es vigente a pesar del paso del tiempo.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Incluye opiniones de expertos e información recopilada en trabajos de investigación.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es un texto objetivo, que no incluye opiniones.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Género periodístico cuya vigencia es corta, puede dejar de interesar al día siguiente de ser escrito.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee los siguientes enunciados y selecciona si se trata de una característica de una noticia (N) o de un reportaje (R).",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función principal de la rueda de agua  es convertirse en un bien para la humanidad.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La rueda de agua es un artefacto que permitirá transportar agua sin el esfuerzo físico que implica cargarla en grandes distancias.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La rueda de agua es un invento para curar dolores de espalda, de cabeza y problemas de parto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El producto ha sido todo un éxito de ventas en la India.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La rueda de agua permite transportar 50 litros de agua en una sola carga.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee el siguiente texto:<br><br>Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false,
            "t11instruccion": "<center><b>Agua potable para todos</b></center><br>¡Qué fácil es abrir el grifo y que salga agua potable!  Sin embargo, no todos disfrutan de ese privilegio. Existen diferentes regiones en el mundo, como las comunidades rurales en América, Asia y África, donde no cuentan con agua potable ni tuberías para transportar este líquido vital.<br><br>En algunas comunidades de la India, mujeres y niños caminan diariamente hasta 8 kilómetros cargando 20 litros de agua en la cabeza para poderla abastecer a sus familias.  Con el paso del tiempo, esto les ha provocado diferentes problemas físicos, como dolores de espalda y cabeza, y a las mujeres, problemas en el parto.  Además del tiempo que les implica acarrear el agua todos los días. Como solución a esta problemática, algunas empresas han diseñado las llamadas “ruedas de agua”.<br><br>La “rueda de agua” es un contenedor que permite empujar hasta 50 litros de agua de forma rápida e higiénica. Está hecho de plástico de alta calidad, seguro para el consumo humano y resistente para todo tipo de terreno.  Tiene forma de matka, recipiente utilizado en la India para recoger agua y que significa “agua limpia”. Cuenta con un tapón hermético que impide la contaminación del agua durante el traslado.<br><br>Sin duda, este producto puede contribuir a mejorar su calidad de vida. Por un lado, les ahorraría tiempo y haría el acarreo más rápido y sencillo; además, contribuirá a disminuir sus malestares físicos y a erradicar los problemas estomacales como la diarrea, ocasionados por beber agua contaminada.<br><br>Este producto aún se encuentra en etapa de desarrollo. Hasta ahora, sólo se ha piloteado en algunas comunidades rurales de la India y se han entrevistado a más de 1,500 personas, para detectar las necesidades de posibles clientes.<br><br>Por el momento, el producto no se considera rentable, pero algunas empresas están trabajando para lanzarlo a nivel mundial. Actualmente, la rueda cuesta entre 20 y 30 dólares, precio que no es accesible para las comunidades lo necesitan.<br><br>FUENTE: Escorial, M. (2017). Opinión | ¿Una rueda de agua? ¡Cómo no se le ocurrió a nadie antes! [online] EL PAÍS. Disponible en: https://elpais.com/elpais/2014/10/09/planeta_futuro/1412856806_874256.html"
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "<p>matka<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>agua limpia<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>rueda<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>contaminación<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>30<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>50<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [{
                "t11pregunta": "El "
            },
            {
                "t11pregunta": " es el recipiente que se usa en la India para recoger agua y significa "
            },
            {
                "t11pregunta": ".<br>La"
            },
            {
                "t11pregunta": " contendrá un tapón para evitar la "
            },
            {
                "t11pregunta": " del agua.<br>El precio actual de la rueda es de entre 20 y "
            },
            {
                "t11pregunta": " dólares.<br>La rueda permite transportar hasta "
            },
            {
                "t11pregunta": " litros de agua en una sola carga."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde corresponda para completar el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los reportajes  son  textos informativos que desarrollan extensamente  un tema particular.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los reportajes abordan solamente  temas culturales.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para escribir un reportaje, se puede hacer una recopilación de información actual e investigaciones previas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Su difusión es  siempre a través de revistas especializadas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En su investigación recopilan información de distintas fuentes.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee el siguiente texto:<br><br>Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "Lo que a mí me parezca más importante es lo relevante.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lo irrelevante siempre es lo más aburrido.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lo relevante es la información que se repite en varias fuentes.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lo que responda a preguntas guía será información relevante (qué, cómo, dónde, cuándo, por qué).",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cómo puedes distinguir la información relevante de la irrelevante en una investigación?"
        }
    },
    // Reactivo 7
    {
        "respuestas": [{
                "t13respuesta": "Escribir lo más rápido posible, sin importar que la letra sea ilegible.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Utilizar lenguaje propio y sencillo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No es necesario escuchar atentamente toda la información.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Escribir sólo las ideas principales que se lean o escuchen",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Utilizar abreviaturas.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona de la siguiente lista todas las estrategias para hacer anotaciones al recopilar información."
        }
    },
    // Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "Entrevista",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Preguntas abiertas",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Preguntas cerradas",
                "t17correcta": "3"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Acto comunicativo entre una persona que cuestiona y otra que responde. Su finalidad es obtener información sobre un tema específico."
            },
            {
                "t11pregunta": "Tipo de preguntas que permiten ampliar información, ya que ofrecen la oportunidad de dar respuestas extensas."
            },
            {
                "t11pregunta": "Tipo de preguntas que limitan la extensión de la respuesta, ya que son más específicas y concretas."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada definición con la descripción que le corresponda."
        }
    },
    // Reactivo 9
    {
        "respuestas": [{
                "t13respuesta": "Género literario para generar emociones positivas.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Género literario escrito para crear tensión.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Está basado en hechos reales.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El autor  recrea un ambiente sobrenatural que genera sentimientos de misterio o terror.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Prevalecen elementos como sombras, aullidos y espacios cerrados.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Son muy extensos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se destacan temas lúgubres como la muerte.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Los personajes principales siempre sufren algún temor o peligro.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Todos los relatos ocurren en lugares fantásticos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se presenta una polarización entre el bien y el mal.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Los personajes son siempre racionales y poco emotivos.",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona de la siguiente lista todas las características de los cuentos de misterio y terror."
        }
    },
    // Reactivo 10
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cuentos de misterio evitan presentar toda la información al inicio de la narración.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los personajes pueden tener una pérdida de integridad física y psicológica.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Carecen de un villano en la historia.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cuentos de terror hablan de temas que evocan alegría y felicidad.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Este género hace referencia a elementos oscuros.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee el siguiente texto:<br><br>Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 11
    {
        "respuestas": [{
                "t13respuesta": "<p>omnisciente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>antagonista<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>protagonista<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>testigo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>personajes<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>ambiente<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [{
                "t11pregunta": "a. El narrador "
            },
            {
                "t11pregunta": " es el que narra desde afuera del relato. No participa activamente en la historia y conoce todo lo que pasa.<br>b. El "
            },
            {
                "t11pregunta": " es el personaje que se opone al principal, creando dificultades e impidiendo sus propósitos.<br>c. El "
            },
            {
                "t11pregunta": " es el personaje principal, los hechos suceden en torno a él.<br>d. Tipo de narrador que presencia los hechos. Conocemos sus emociones pero no las del resto de los personajes: "
            },
            {
                "t11pregunta": ".<br>e. Los "
            },
            {
                "t11pregunta": " son quienes realizan acciones y viven numerosas aventuras. Se describen sus características físicas y emocionales.<br>f. Lugares en que se desarrollan los hechos y en donde los personajes realizan las acciones: "
            },
            {
                "t11pregunta": ""
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la palabra que complete cada enunciado.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 12
    {
        "respuestas": [{
                "t13respuesta": "Psicología",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Física",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Características del personaje que sirven para comprender sus miedos, pensamientos, sentimientos y motivaciones.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Describen el aspecto del personaje.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A través de ellas, se puede saber si el personaje es un monstruo o  un príncipe.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estas cualidades determinarán las acciones de los personajes en el relato.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee los siguientes enunciados e identifica a qué tipo de característica pertenece. Elige (P) si corresponde a una psicológica o (F), si es física.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 13
    {
        "respuestas": [{
                "t13respuesta": "Pospretérito o condicional simple",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presente indicativo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Pasado o pretérito simple",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Pretérito imperfecto o copretérito de indicativo",
                "t17correcta": "4"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Describe acciones que pudieron haber pasado, pero no sucedieron. Por ejemplo: haría, sentiría."
            },
            {
                "t11pregunta": "Tiempo verbal utilizado para hablar del tiempo actual del relato. Por ejemplo: hago, leo, estoy."
            },
            {
                "t11pregunta": "Es el tiempo verbal que describe los hechos que sucedieron y finalizaron en el pasado. Por ejemplo: dijo, comió, leyó."
            },
            {
                "t11pregunta": "Indica acciones que fueron constantes e indefinidas en el pasado. Por ejemplo: ansiaba, clavaba, sentía."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada tiempo verbal con su descripción y ejemplos."
        }
    },
    // Reactivo 14
    {
        "respuestas": [{
                "t13respuesta": "<p>fue<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>heló<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>quería<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>volviendo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>echaba<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>hacía<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>amaba<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>conocer<\/p>",
                "t17correcta": "8"
            },
        ],
        "preguntas": [{
                "t11pregunta": "<center><b>El almohadón de plumas<br>Autor: Horacio Quiroga</b></center>Su luna de miel "
            },
            {
                "t11pregunta": " un largo escalofrío. Rubia, angelical y tímida, el carácter duro de su marido "
            },
            {
                "t11pregunta": " sus soñadas niñerías de novia.  Lo "
            },
            {
                "t11pregunta": " mucho, sin embargo, a veces con un ligero estremecimiento cuando "
            },
            {
                "t11pregunta": " de noche juntos por la calle, "
            },
            {
                "t11pregunta": " una furtiva mirada a la alta estatura de Jordán, mudo desde "
            },
            {
                "t11pregunta": " una hora. Él por su parte, la "
            },
            {
                "t11pregunta": " profundamente sin darlo a "
            },
            {
                "t11pregunta": ". (...)<br><br>Ciudadseva.com. (2017). El almohadón de plumas - Horacio Quiroga - Ciudad Seva - Luis López Nieves. [online] Available at: http://ciudadseva.com/texto/el-almohadon-de-plumas/ [Accessed 3 Nov. 2017]."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Lee el siguiente fragmento del cuento “El almohadón de plumas” de Horacio Quiroga y arrastra el verbo que corresponda para contar la historia.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 15
    {
        "respuestas": [{
                "t13respuesta": "<p>Recuerdo<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>vi<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>sentarse<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>encontraba<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>pude<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>le<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>noté<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>estaba<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>temblaba<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>pudo<\/p>",
                "t17correcta": "10"
            },
        ],
        "preguntas": [{
                "t11pregunta": ""
            },
            {
                "t11pregunta": " perfectamente aquella mañana cuando la "
            },
            {
                "t11pregunta": " llegar y "
            },
            {
                "t11pregunta": " en su escritorio. Era el primer día de clases y me "
            },
            {
                "t11pregunta": " nervioso, al igual que todos mis compañeros del salón.  No "
            },
            {
                "t11pregunta": " aguantar la curiosidad y "
            },
            {
                "t11pregunta": " pregunté su nombre. Fue cuando "
            },
            {
                "t11pregunta": " que ella "
            },
            {
                "t11pregunta": " igual o más nerviosa que yo, pues su voz "
            },
            {
                "t11pregunta": " y sólo "
            },
            {
                "t11pregunta": " decir entre dientes: “P a o l a”."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que correspondan para completar el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 16
    {
        "respuestas": [{
                "t13respuesta": "Cuando me fui a vivir  a Mérida, siempre iba a la plaza a ver comer a las aves.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Cuando me fui a vivir a Mérida, siempre vimos comer a las aves en la plaza.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Desde que llegaba a vivir a Mérida, siempre veía comer a las aves en la plaza.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Desde que iba a Mérida, siempre llegaba a la plaza a ver comer a las aves.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "En la universidad, nunca hubieron profesores tan comprometidos como los que yo tuve.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "En la universidad, nunca hubo profesores tan comprometidos como los que yo tuviera.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "En la universidad, nunca hubo profesores tan comprometidos como los que yo tuve.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "En la universidad, nunca hubieron profesores tan comprometidos como los que yo tuviera.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona de cada lista el enunciado que esté redactado correctamente.<br><br>1.", "2."],
            "preguntasMultiples": true
        }
    },
    // Reactivo 17
    {
        "respuestas": [{
                "t13respuesta": "Entonces",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "De repente",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Desde entonces",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "En ese momento",
                "t17correcta": "4"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Conector cuya función es dar continuidad a la historia y el lector se mantenga atento."
            },
            {
                "t11pregunta": "Conector que sirve para advertir al lector que está a punto de conocer información inesperada."
            },
            {
                "t11pregunta": "Sirve para ubicar al lector sobre una situación que comienza a partir de ese momento de la historia."
            },
            {
                "t11pregunta": "Conector que da referencia al lector sobre una situación que sucede de manera simultánea en la historia."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada conector con su descripción."
        }
    },
    // Reactivo 18
    {
        "respuestas": [{
                "t13respuesta": "<p>de repente<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>En aquel momento<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Entonces<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Desde ese momento<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Julia se encontraba en su cuarto viendo una película de terror, cuando "
            },
            {
                "t11pregunta": " escuchó un ruido que la estremeció.  Se paró de su silla inmediatamente y se asomó a la ventana. "
            },
            {
                "t11pregunta": ", vio a una mujer vestida de blanco y gritó desesperadamente. "
            },
            {
                "t11pregunta": ", se abrió la puerta de su recámara.  Era su mamá para explicarle que su vecina, la enfermera, había ido a visitarlas. "
            },
            {
                "t11pregunta": ", Julia prometió no ver películas de terror en la noche."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 19
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los números arábigos se utilizan para enumerar pasos o procesos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las viñetas sirven para indicar orden y coherencia.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un ejemplo de números arábigos son: 1, 2, 3…",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las viñetas sirven para clasificar elementos de una misma categoría.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee el siguiente texto:<br><br>Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 20
    {
        "respuestas": [{
                "t13respuesta": "Son una representación gráfica de un cuento.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Presentan información de una manera ordenada, clara y precisa.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Permiten enlazar relaciones entre algunos elementos con otros.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Hacen una narración extensa de un procedimiento.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Ayudan a detectar posibles problemas a futuro.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ayudan a entender todo el proceso completo por el que debe pasar el trabajo o actividad que se está realizando.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Resumen los pasos irrelevantes de un proceso.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona de la siguiente lista aquellas características que correspondan a los diagramas de flujo."
        }
    },
    // Reactivo 21
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los textos instructivos son aquellos que describen o relatan leyendas..",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los instructivos pueden estar acompañados de imágenes para que el lector comprenda mejor las instrucciones, procedimientos y materiales a utilizar.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sus procesos están enumerados o se emplean palabras que indican orden temporal",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los textos instructivos detallan hechos fantásticos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En un instructivo se utilizan verbos en modo imperativo e infinitivo",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El instructivo debe contener lenguaje técnico y especializado.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función de un instructivo es describirnos los procedimientos y reglas para realizar alguna actividad de forma detallada, clara y precisa.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee el siguiente texto:<br><br>Selecciona verdadero (V) o falso (F) según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 22
    {
        "respuestas": [{
                "t13respuesta": "<p>Doblar<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hacer<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>formar<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>doblarlo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>formando<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Tirar<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>están<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>está<\/p>",
                "t17correcta": "8"
            },
        ],
        "preguntas": [{
                "t11pregunta": "1. "
            },
            {
                "t11pregunta": " una hoja de papel a la mitad.<br>2. Doblar una sola parte por la mitad.<br>3. "
            },
            {
                "t11pregunta": " dobleces a la mitad en forma de triángulo.<br>4. Doblar las partes de abajo, de un lado a otro, hacia arriba y luego las puntas para "
            },
            {
                "t11pregunta": " un triángulo perfecto.<br>5. Abrir con cuidado por dentro y "
            },
            {
                "t11pregunta": " para formar un cuadrado.<br>6. Por los dos lados, doblar las puntas hacia afuera, "
            },
            {
                "t11pregunta": " otro triángulo perfecto.<br>7. Abrir y formar otro cuadrado.<br>8. "
            },
            {
                "t11pregunta": " de las dos puntas que "
            },
            {
                "t11pregunta": " juntas en el centro y en el alto, para los lados.<br>9. Ajustar el barco e intentar arreglar y fijar todos los dobleces.<br>10. El barco "
            },
            {
                "t11pregunta": "listo."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras al lugar que corresponda para completar el  instructivo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 23
    {
        "respuestas": [{
                "t13respuesta": "<center><b>Cuidado del medio ambiente</b></center>El planeta nos brinda todo lo que necesitamos para vivir.  En él encontramos agua, comida, aire y fuego.  Desde hace muchos años, el hombre ha dejado de cuidar los recursos naturales y ha terminado con ellos.  Por eso es de gran importancia que tomemos medidas para no continuar contaminando nuestro hogar.<br>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<center><b>La catástrofe ambiental</b></center>Estudios recientes de científicos de prestigiosas universidades, han concluido que los índices de contaminación del planeta están llevando al ser humano a una catástrofe inminente.  Los altos índices de bióxido de carbono  no permiten que la calidad del aire sea óptima para vivir.  Se especula que, aproximadamente en 30 lustros, la especie humana podría desaparecer.<br>",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál de los siguientes párrafos utiliza el lenguaje adecuado para alumnos de primaria?"
        }
    },
    // Reactivo 24
    {
        "respuestas": [{
                "t13respuesta": "SI6E_B02_A24_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "SI6E_B02_A24_03.png",
                "t17correcta": "1",
                "columna": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "SI6E_B02_A24_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "contenedores": [{
                "Contenedor": ["", "102,71", "cuadrado", "250, 60", ".", "transparent"]
            },
            {
                "Contenedor": ["", "102,323", "cuadrado", "250, 60", ".", "transparent"]
            },
        ]
    },
    // Reactivo 25
    {
        "respuestas": [{
                "t13respuesta": "<p>Elegir la tarjeta de juego.</p>",
                "t17correcta": "0",
                "etiqueta": "paso 1"
            },
            {
                "t13respuesta": "<p>Escuchar atentamente cada objeto mencionado por el “gritón”.<\/p>",
                "t17correcta": "1",
                "etiqueta": "paso 2"
            },
            {
                "t13respuesta": "<p>Colocar una ficha en cada objeto que es mencionado por el “gritón”.<\/p>",
                "t17correcta": "2",
                "etiqueta": "paso 3"
            },
            {
                "t13respuesta": "<p>Al colocar todas las fichas en la carta, gritar “Lotería”.<\/p>",
                "t17correcta": "3",
                "etiqueta": "paso 4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los pasos para jugar lotería.",
        }
    },
    // Reactivo 26
    {
        "respuestas": [{
                "t13respuesta": "SI6E_B02_A26_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "SI6E_B02_A26_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "SI6E_B02_A26_03.png",
                "t17correcta": "2",
                "columna": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "SI6E_B02_A26_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "contenedores": [{
                "Contenedor": ["", "210,21", "cuadrado", "134, 106", ".", "transparent"]
            },
            {
                "Contenedor": ["", "210,489", "cuadrado", "134, 106", ".", "transparent"]
            },
            {
                "Contenedor": ["", "369,488", "cuadrado", "135, 106", ".", "transparent"]
            },
        ]
    }
];