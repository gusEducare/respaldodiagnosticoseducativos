json=[
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las descripciones, la representación e ilustración de lo concreto se alcanza por medio del lenguaje.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las descripciones proporcionan información acerca de las principales características físicas y psicológicas de personajes únicamente reales. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se pueden hacer descripciones de los elementos y partes que conforman o clasifican algún objeto o procedimiento.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mediante la descripción es imposible conocer los detalles y componentes de algún lugar o suceso.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La descripción se logra mediante el uso de adjetivos, adverbios, verbos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las descripciones pueden ser objetivas apegadas a la realidad o subjetivas si se comunica la impresión o emoción que dicha persona produce desde una perspectiva personal.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda a cada frase.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
    //2
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Párrafos 3, 1, 4, 2",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Párrafos 3, 2, 1, 4",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Párrafos 3, 4, 2, 1",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Párrafos 1, 2, 3, 4",
              "t17correcta":"0"
           },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Lee los párrafos y selecciona la respuesta correcta.  <br><br>Coloca en orden los párrafos que describan al personaje de manera congruente y ordenada, incluido el título.",
           "t11instruccion": "(Párrafo 1):&nbsp;&nbsp;&nbsp;&nbsp;    El gato con botas es un personaje imaginario, que ha aparecido en cuentos, caricaturas y películas. Se le llama así porque es un gato que anda en dos patas, como si fuera un humano, y lleva botas para caminar.<br><br>(Párrafo 2):&nbsp;&nbsp;&nbsp;&nbsp;     Por último, el gato con botas tiene una personalidad ingeniosa, consigue lo que quiere mediante engaños y mucha astucia. Es muy esforzado y obstinado para conseguir lo que quiere pero no por eso deja de ser generoso y justo, es un ejemplo de virtud y astucia.<br><br>(Párrafo 3):&nbsp;&nbsp;&nbsp;&nbsp;     Descripción del gato con botas.<br><br>(Párrafo 4):&nbsp;&nbsp;&nbsp;&nbsp;     Su vestimenta es muy peculiar, ya que recuerda el estilo de un mosquetero del siglo XVII o XVIII. Sus botas son muy altas y de piel, además, lleva un sombrero muy grande que le proporciona mucha sombra y siempre carga una espada por si se presentan problemas."
        }
     },
    //3
    {  
        "respuestas":[  
           {  
              "t13respuesta":"La deforestación arrasa los bosques y las selvas de la Tierra de forma masiva causando un inmenso daño a la calidad de los suelos. En México los ritmos de deforestación son alarmantes, gran variedad de especies de flora y fauna se encuentran en un estado de emergencia, ya que dependen de los  ecosistemas que están siendo devastados. Aun cuando los bosques todavía cubren alrededor de 30 por ciento de las regiones del mundo es sumamente preocupante que franjas del tamaño de Panamá se pierdan cada año.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"La deforestación arrasa los bosques y las selvas de la Tierra de forma masiva causando un inmenso daño a la calidad de los suelos. Los bosques todavía cubren alrededor de 30 por ciento de las regiones del mundo. México no es la excepción, los ritmos de deforestación que sufre son alarmantes.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"La deforestación arrasa los bosques y las selvas de la Tierra de forma masiva causando un inmenso daño a la calidad de los suelos. Los bosques todavía cubren alrededor de 30 por ciento de las regiones del mundo, pero franjas del tamaño de Panamá se pierden indefectiblemente cada año. La acelerada destrucción de los bosques ha colocado en estado de emergencia a una gran variedad de especies de flora que dependen de ese ecosistema. Entre esas especies se encuentra la humana.",
              "t17correcta":"0"
           },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Lee los párrafos y selecciona la respuesta correcta. <br><br> ¿Qué párrafo, de los que se presentan a continuación, mantiene la coherencia y la integridad de la información?",
           "t11instruccion": "Los bosques cubren una superficie total de 30 por ciento del planeta, sin embargo, la integridad de los bosques está en peligro, ya que la deforestación masiva de bosques y selvas causa un gran daño a la calidad de los suelos, provocando la pérdida de grandes franjas de bosque como del tamaño de Panamá.<br><br>La gran diversidad de flora y fauna de los bosques y selvas peligra, y dentro de esa gran variedad,  nos encontramos los humanos. Esto se debe principalmente al gran aumento en la deforestación y destrucción de bosques y selvas, como sucede en México, lo que nos pone en una situación de emergencia medioambiental."
        }
     },
     //4
     {
        "respuestas": [
            {
                "t13respuesta": "SI5E_B05_A04_02.png",
                "t17correcta": "0,1",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A04_03.png",
                "t17correcta": "0,1",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A04_04.png",
                "t17correcta": "2,3",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A04_05.png",
                "t17correcta": "2,3",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A04_06.png",
                "t17correcta": "4,5",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A04_07.png",
                "t17correcta": "4,5",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A04_08.png",
                "t17correcta": "6,7",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A04_09.png",
                "t17correcta": "6,7",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra cada elemento donde corresponda.<br><br>Arrastra la definición y el ejemplo donde corresponda.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"SI5E_B05_A04_01.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":true
            
        },
        "contenedores": [
            {"Contenedor": ["", "39,190", "cuadrado", "211, 111", ".","transparent"]},
            {"Contenedor": ["", "39,402", "cuadrado", "211, 111", ".","transparent"]},
            {"Contenedor": ["", "151,190", "cuadrado", "211, 111", ".","transparent"]},
            {"Contenedor": ["", "151,402", "cuadrado", "211, 111", ".","transparent"]},

            {"Contenedor": ["", "263,190", "cuadrado", "211, 111", ".","transparent"]},
            {"Contenedor": ["", "263,402", "cuadrado", "211, 111", ".","transparent"]},
            {"Contenedor": ["", "375,190", "cuadrado", "211, 111", ".","transparent"]},
            {"Contenedor": ["", "375,402", "cuadrado", "211, 111", ".","transparent"]}
        ]
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "SI5E_B05_A05_02.png",
                "t17correcta": "0",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A05_03.png",
                "t17correcta": "1,6",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A05_04.png",
                "t17correcta": "5",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A05_05.png",
                "t17correcta": "2",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A05_06.png",
                "t17correcta": "3,4",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A05_03.png",
                "t17correcta": "1,6",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A05_06.png",
                "t17correcta": "3,4",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra cada elemento donde corresponda.<br><br>Coloca las características del tríptico donde corresponden.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"SI5E_B05_A05_01.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":false
        },
        "contenedores": [
            {"Contenedor": ["", "63,465", "cuadrado", "140, 40", ".","transparent"]},
            {"Contenedor": ["", "226,465", "cuadrado", "140, 40", ".","transparent"]},
            {"Contenedor": ["", "50,143", "cuadrado", "140, 40", ".","transparent"]},
            {"Contenedor": ["", "309,252", "cuadrado", "140, 40", ".","transparent"]},
            {"Contenedor": ["", "297,36", "cuadrado", "140, 40", ".","transparent"]},
            {"Contenedor": ["", "390,252", "cuadrado", "140, 40", ".","transparent"]},
            {"Contenedor": ["", "378,36", "cuadrado", "140, 40", ".","transparent"]}
        ]
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "SI5E_B05_A06_02.png",
                "t17correcta": "0",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A06_03.png",
                "t17correcta": "1",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A06_04.png",
                "t17correcta": "2",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A06_05.png",
                "t17correcta": "3",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A06_06.png",
                "t17correcta": "4",
                "columna":"0"
            },
            {
                "t13respuesta": "SI5E_B05_A06_07.png",
                "t17correcta": "5",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A06_08.png",
                "t17correcta": "6",
                "columna":"1"
            },
            {
                "t13respuesta": "SI5E_B05_A06_09.png",
                "t17correcta": "7",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra los elementos y completa la tabla.<br><br>Arrastra las fuentes necesarias para investigar cada tema y el tipo de información que podrías encontrar.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"SI5E_B05_A06_01.png",
            "respuestaImagen":true,
            "altoImagen":900, 
            "tamanyoReal":false,
            "bloques":false,
            "borde":false 
        },
        "contenedores": [
            {"Contenedor": ["", "82,217", "cuadrado", "201, 196", ".","transparent"]},
            {"Contenedor": ["", "82,426", "cuadrado", "201, 196", ".","transparent"]},
            {"Contenedor": ["", "284,217", "cuadrado", "201, 196", ".","transparent"]},
            {"Contenedor": ["", "284,426", "cuadrado", "201, 196", ".","transparent"]},

            {"Contenedor": ["", "487,217", "cuadrado", "201, 196", ".","transparent"]},
            {"Contenedor": ["", "487,426", "cuadrado", "201, 196", ".","transparent"]},
            {"Contenedor": ["", "690,217", "cuadrado", "201, 196", ".","transparent"]},
            {"Contenedor": ["", "690,426", "cuadrado", "201, 196", ".","transparent"]}
        ]
    },
    //7

     {  
        "respuestas":[  
           {  
              "t13respuesta":"Comparar precios de diferentes distribuidores y comprar una dentro del rango de precio y con la calidad necesaria, si está en oferta mejor.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Comprar la mejor máquina para hacer zapatos, aunque la tenga que importar de otro país y tarde mucho en llegar.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Comprar la máquina para hacer zapatos más cara, lo más caro siempre es lo mejor y va a poder presumir que compró la más cara.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Comprar la primera máquina que encuentre, no importa el tamaño ni el precio, lo que importa es tener la máquina lo más pronto posible.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Lee el texto y selecciona la respuesta correcta.<br><br>¿Qué decisión de las que se presentan a continuación es la mejor?",
           "t11instruccion": "Juan necesita una máquina para hacer zapatos, pero no tiene mucho dinero, en realidad no necesita una máquina demasiado especializada para hacer los mejores zapatos, los que fabrica son muy sencillos. <br><br>Juan tiene muchas opciones para comprar otra máquina pero le gustaría hacer la mejor compra de acuerdo a sus necesidades y posibilidades."
        }
     },
      //8
     {  
        "respuestas":[  
           {  
              "t13respuesta":"Informar a un maestro o directivo de la escuela que sus compañeros de clase lo molestan y que no puede hacer lo que más le gusta. Exigir el respeto que se merece así como él respeta a los demás.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Comenzar una pelea con los compañeros que lo molestan, aunque fueran más, él tiene que defenderse como hombre.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Molestar a sus compañeros igual que sus compañeros lo molestan, tiene que fijarse muy bien en los defectos de cada uno para hacerles daño con lo que a cada uno le afecta más.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Hacerles travesuras como ponerles tierra en sus mochilas, ponerles suciedad en sus butacas y aventarles cosas asquerosas sin que se den cuenta.              ",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Debe aguantarse y aprender que las personas son muy crueles, si quiere desahogarse podría escribir, pintar o tocar un instrumento musical.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Dejar de ir a la escuela o dejar de hacer lo que le gusta, debe aprender que esas actividades son de niñas y hay otras de niños que podría hacer.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Lee los párrafos y selecciona la respuesta correcta. <br><br> Analiza la situación y selecciona la mejor actitud posible. ¿Qué es lo que debe hacer Luis?",
           "t11instruccion": "A Luis le gusta mucho ir a la escuela, su parte favorita son los talleres artísticos al final del día. A él le gusta tomar clases de ballet, así que se inscribió al curso para aprender más y mejorar su técnica.<br><br>A pesar de que le gusta demasiado el ballet, se siente muy apenado pues hay un grupo de niños en la escuela que molestan a muchos compañeros, pero a Luis es al que más molestan de todos. Le dicen que está gordo y que es una niña porque le gusta bailar y que eso solo lo hacen las niñas.<br><br>A Luis lo hacen sentir muy mal, le da mucha tristeza todos los días, a veces llora pero no deja que nadie se de cuenta, nadie sabe cómo se siente, ni siquiera sus papás o su hermano mayor. Harto de esa situación ha tomado la decisión de ya no ir más a clases de ballet, y además, quiere cambiarse de escuela y dejar los pocos amigos que no lo molestan.<br><br>¿Qué actitud debería tomar Luis que fomente el respeto?"
        }
     },


];