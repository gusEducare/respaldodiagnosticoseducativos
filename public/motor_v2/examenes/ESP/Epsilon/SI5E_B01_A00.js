json = [

  {
    "respuestas": [
      {
        "t13respuesta": "Popol Wuj significa libro de la comunidad.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "El Popol Wuj fue escrito en el siglo XVI.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Los cuatro Pawantun sostienen el reino de los mortales.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Existen 9 inframundos en la mitología maya.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Las celebraciones y rituales mayas estaban determinados por las fechas y posición de los astros.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "El hombre fue creado a partir de maíz blanco y amarillo.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Los cuatro Chaac son dioses que controlan las tormentas.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "La guerra era común en el mundo maya.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Las mujeres tenían un papel fundamental en la guerra.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Las primeras inscripciones de escritura maya fueron encontradas en Petén.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "El año solar maya se calculó con mayor precisión que el calendario juliano.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      }, ////////////////////////////////////////////////

      {
        "t13respuesta": "Información acerca de la mitología maya.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Información breve acerca del Popol Wuj.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Información acerca del dios creador de los mayas.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Del maíz fue creado el hombre.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Los cuatro Chaac son dioses que controlan las tormentas.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Existen 9 inframundos en la mitología maya.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "La sangre humana se consideraba un alimento sagrado para las deidades.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Los mayas marcaban los solsticios y equinoccios mediante sus edificios.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "2",
      "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>¿Qué información puedes encontrar en los dos textos?", "¿Qué información se repite en los dos textos?"],
      "t11instruccion": "<center><b>Texto 1</b></center> <br>   <b>El mito maya de la creación del hombre</b><br> <br>   El Popol Wuj, que significa libro de la comunidad, relata la historia de la creación del hombre\n\
  según la cultura maya. El relato fue registrado y preservado en el siglo XVI bajo el mandato del reino colonial, debido a que la conquista amenazó con las costumbres y tradiciones de la cultura milenaria.<br><br>\n\
Según el relato, antes de que los hombres fueran creados, existían diferentes deidades, como Itzamna creadora de todo, el corazón del cielo, los Héroes Gemelos y la Serpiente emplumada. \n\
Las deidades se reunieron porque querían crear seres con corazón y mente que pudieran llevar la cuenta de los días, pero sus primeros intentos fracasaron. Cuando usaron maíz blanco y amarillo, el hombre surgió y \n\
vieron que podía hablar, entonces las deidades quedaron satisfechas con la creación y le otorgaron el maíz como alimento, a cambio de recibir numerosas ofrendas y sacrificios.\n\
<br><center><b>Texto 2</b></center> <br>   <b>Religión y mitología maya</b><br><br>  Los mayas tenían una estructura cosmológica muy compleja, identificaban diferentes deidades, cada una con una función específica, estas deidades estaban ligadas intrínsecamente a la\n\
 astronomía y su calendario, por eso las ofrendas se determinaban por las fechas y posiciones de los astros en el cielo.<br><br> Itzamna era el dios creador y tenía diferentes aspectos, se identificaba como la totalidad del cosmos, como el dios solar y también como el jaguar-noche\n\
  que recorría el inframundo durante la noche. Los cuatro Pawatun eran deidades que sostenían las cuatro esquinas del reino mortal; los cuatro Chaac eran dioses de las tormentas que dominaban las lluvias, tormentas, truenos, etc. Los Señores de la Noche reinaban en cada uno\n\
   de los 9 inframundos y la diosa Luna controlaba el crecimiento del maíz.<br><br> El Popol Wuj es uno de los relatos más importantes que tenemos para conocer la mitología maya, fue escrito en la época colonial. Narra la creación del hombre a partir del maíz, la historia de los Héroes\n\
   Gemelos y algunas deidades que incluye en el relato son Hun Hunahpu y la tríada de Tohil, Awilix y Jacawitz.",
      "preguntasMultiples": true
    }
  },
  {
    "respuestas": [
      {
        "t13respuesta": "posteriormente",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "previamente",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "Con motivo",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "Mientras tanto",
        "t17correcta": "4"
      },
      {
        "t13respuesta": "antes",
        "t17correcta": "5"
      },
      {
        "t13respuesta": "Finalmente",
        "t17correcta": "6"
      },
      {
        "t13respuesta": "Anteriormente",
        "t17correcta": "7"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "En 1492 Cristóbal Colón llegó a las islas del Caribe, particularmente la isla de Guanahaní, que "
      },
      {
        "t11pregunta": " bautizaron con el nombre de San Salvador. Colón pensaba que había llegado a India mediante otra ruta marítima, pero la posteridad descubrió que se trataba de otro continente del que no tenían noticia los europeos, por eso, para ellos fue un auténtico descubrimiento aunque "
      },
      {
        "t11pregunta": " diferentes culturas a lo largo de la historia ya habían llegado a esta parte del mundo.<br>"
      },
      {
        "t11pregunta": " de tal descubrimiento, la Corona española invadió y conquistó tierra azteca, tarea que no les fue nada fácil debido a la gran resistencia y poder que tenía el Imperio. "
      },
      {
        "t11pregunta": ", diferentes poblaciones se encontraban sometidas por los aztecas, por lo que Hernán Cortés buscó la alianza con ellas.<br>Fue esta alianza la que permitió concluir la conquista del nuevo territorio y que la Corona española estableciera una colonia que se conocería como La Nueva España, pero "
      },
      {
        "t11pregunta": " tenían que evangelizar y enseñar el español a lo largo y ancho del continente para crear una unidad social que estuviera al servicio de la Corona.<br>"
      },
      {
        "t11pregunta": ",  Hernán Cortés mandó demoler México-Tenochtitlán y edificar una ciudad nueva sobre los restos, una ciudad tal y como les gustaba a los españoles. "
      },
      {
        "t11pregunta": ", Hernán había dirigido una carta a Carlos V para sugerirle que aquel territorio debía llamarse Nueva España.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que completen el párrafo.<br>",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //3
  {
    "respuestas": [
      {
        "t13respuesta": "Narraciones que cuentan una historia o situación que al finalizar deja una reflexión.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Son frases breves que se dicen habitualmente dentro de una comunidad y buscan enseñar de manera sencilla e ingeniosa temas morales.",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "Son lecciones o enseñanzas que se pueden deducir de cuentos, fábulas o anécdotas.",
        "t17correcta": "3",
      },
      {
        "t13respuesta": "Es un recurso literario que le otorga características y personalidad a cosas o animales.",
        "t17correcta": "4",
      },
      {
        "t13respuesta": "Figura retórica por medio de la cual se relaciona una realidad o concepto con otros con los que guarda cierta semejanza.",
        "t17correcta": "5",
      },
      {
        "t13respuesta": "Exageración excesiva de un hecho, circunstancia o relato.",
        "t17correcta": "6",
      },
      {
        "t13respuesta": "Es un lenguaje que se usa habitualmente entre personas que se tienen confianza.",
        "t17correcta": "7",
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Fábulas"
      },
      {
        "t11pregunta": "Refranes"
      },
      {
        "t11pregunta": "Moralejas"
      },
      {
        "t11pregunta": "Personificación"
      },
      {
        "t11pregunta": "Metáfora"
      },
      {
        "t11pregunta": "Hipérbole"
      },
      {
        "t11pregunta": "Expresiones informales"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "altoImagen": "100px",
      "anchoImagen": "200px"
    }
  },

  /*{
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      }
    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Las referencias bibliográficas son resúmenes del texto que hemos usado para la investigación.",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "(2008) La Historia del Islam. Raymundo Bayer: Editorial Mayéutica: España. Es un ejemplo de una buena referencia bibliográfica.",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Las referencias bibliográficas deben contener: apellido, inicial del nombre del autor, año de publicación, título del libro, revista, artículo, etcétera, país en el que fue impreso y editorial; en ese orden.",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Cuando se hace una cita textual se puede colocar únicamente el apellido y la inicial del nombre del autor, seguido del año y la página que hemos citado. En la bibliografía se debe incluir la referencia completa.",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Si un escritor parafrasea el contenido de un texto y lo explica desde su propio punto de vista, podemos considerar que el escritor ha plagiado.",
        "correcta": "0"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 65,
      "evaluable": true
    }
  },*/
  //4
  {
    "respuestas": [
      {
        "t13respuesta": "Fábula",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Refrán",
        "t17correcta": "1",
      }
    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Al nopal solo se le arriman cuando tiene tunas.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El flojo y el mezquino, recorren dos veces el mismo camino.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "La tortuga y la liebre",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El niño mentiroso",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Más sabe el diablo por viejo, que por diablo.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "La gallina de los huevos de oro",
        "correcta": "0",
      },

    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Lee con atención cada enunciado y selecciona si es fábula o refrán.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },
  /*{
    "respuestas": [
      {
        "t13respuesta": "Narraciones que cuentan una historia o situación que al finalizar deja una reflexión.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Frases breves que se dicen habitualmente dentro de una comunidad y buscan enseñar de manera sencilla e ingeniosa temas morales.",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "Lecciones o enseñanzas que se pueden deducir de cuentos, fábulas o anécdotas.",
        "t17correcta": "3",
      },
      {
        "t13respuesta": "Recurso literario que le otorga características y personalidad a cosas o animales.",
        "t17correcta": "4",
      },
      {
        "t13respuesta": "Figura retórica por medio de la cual se relaciona una realidad o concepto con otros con los que guarda cierta semejanza.",
        "t17correcta": "5",
      },
      {
        "t13respuesta": "Exageración excesiva en un hecho, circunstancia o relato.",
        "t17correcta": "6",
      },
      {
        "t13respuesta": "Lenguaje que se usa habitualmente entre personas que se tienen confianza.",
        "t17correcta": "7",
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Fábulas"
      },
      {
        "t11pregunta": "Refranes"
      },
      {
        "t11pregunta": "Moralejas"
      },
      {
        "t11pregunta": "Personificación"
      },
      {
        "t11pregunta": "Metáfora"
      },
      {
        "t11pregunta": "Hipérbole"
      },
      {
        "t11pregunta": "Expresiones informales"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "t11instruccion": "",
      "altoImagen": "100px",
      "anchoImagen": "200px"
    }
  },*/
  //5
  {
    "respuestas": [
      {
        "t13respuesta": "Es un título que busca atrapar la atención del espectador.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Son imágenes y recursos gráficos que hacen más claro o atractivo el mensaje.",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "Son símbolos, imágenes o letras que conforman el nombre o imagen de una empresa.",
        "t17correcta": "3",
      },
      {
        "t13respuesta": "Es una invitación al público para la compra.",
        "t17correcta": "4",
      },
      {
        "t13respuesta": "Es la frase que identifica la marca o producto; debe ser fácil de recordar.",
        "t17correcta": "5",
      },
      {
        "t13respuesta": "Es el medio por el que los consumidores pueden adquirir los productos o pedir más informes.",
        "t17correcta": "6",
      },
      {
        "t13respuesta": "Muestran un beneficio que pueden adquirir los consumidores si adquieren el producto.",
        "t17correcta": "7",
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Encabezado"
      },
      {
        "t11pregunta": "Fotografía"
      },
      {
        "t11pregunta": "Logotipo"
      },
      {
        "t11pregunta": "Llamado a la acción"
      },
      {
        "t11pregunta": "Eslogan"
      },
      {
        "t11pregunta": "Contacto"
      },
      {
        "t11pregunta": "Motivador"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "altoImagen": "100px",
      "anchoImagen": "200px"
    }
  },
  //6
  {
    "respuestas": [
      {
        "t13respuesta": "Transmitir un mensaje.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Prevalecer en la memoria de los consumidores.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Persuadir la conciencia del público para que compren.",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Informar acerca de los efectos secundarios del producto.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Dar información extensa de un área del conocimiento.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Proponer una postura crítica frente al consumismo excesivo.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Rogar para que la gente regale dinero.",
        "t17correcta": "0",
        "numeroPregunta": "0"
      }, //////////////////////////////////////////////////////////////////////////////////

      {
        "t13respuesta": "Motivadores y ofertas especiales.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Recursos gráficos.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Eslóganes llamativos.",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Tablas comparativas de datos.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Información específica y extensa.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Un resumen de todas las actividades que la empresa ha realizado.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Instructivos.",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },/////////////////////////////////////////////////////////////////////////////////////

      {
        "t13respuesta": "Demuéstrale tu amor con Chocolates Ferreira.",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Llévate tres pares de tenis al precio de dos.",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Alarmas Espía cuidará de tu auto mientras no estás.",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Comunícate con el proveedor para dejar tus quejas y sugerencias.",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Todo lo que necesitas, todo lo que quieres y mucho más.",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Rapix, la solución a todos tus problemas.",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "2",
      "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>¿Cuál es el propósito de los anuncios publicitarios?", "¿Qué recursos utiliza la publicidad para ser más efectiva?", "De las siguientes oraciones,  ¿cuáles emplean un motivante de la publicidad?"],
      "t11instruccion": "",
      "preguntasMultiples": true
    }
  },
  /* {
     "respuestas": [
       {
         "t13respuesta": "I__otencia",
         "t17correcta": "1,4,7"
       },
       {
         "t13respuesta": "Te__rano",
         "t17correcta": "1,4,7"
       },
       {
         "t13respuesta": "E__ujar",
         "t17correcta": "1,4,7"
       },
       {
         "t13respuesta": "Cu__ia",
         "t17correcta": "2,5,8"
       },
       {
         "t13respuesta": "Ti__re",
         "t17correcta": "2,5,8"
       },
       {
         "t13respuesta": "Ha__re",
         "t17correcta": "2,5,8"
       },
       {
         "t13respuesta": "E__idia",
         "t17correcta": "3,6,9"
       },
       {
         "t13respuesta": "I__adir",
         "t17correcta": "3,6,9"
       },
       {
         "t13respuesta": "I__ernadero",
         "t17correcta": "3,6,9"
       },
     ],
     "preguntas": [
       {
         "t11pregunta": "<br><br><style>\n\
  .table img{height: 90px !important; width: auto !important; }\n\
  </style><table class='table' style='margin-top:-40px;'>\n\
  <tr>\n\
  <td style='width:150px'>mp</td>\n\
  <td style='width:100px'>mb</td>\n\
  <td style='width:100px'>nv</td>\n\
  </tr>\n\
  <tr><td>\n\ "},
       {
         "t11pregunta": "</td> <td>"
       },
       {
         "t11pregunta": "</td> <td>"
       },
       {
         "t11pregunta": "</td> </tr><tr><td>"
       },
       {
         "t11pregunta": "</td> <td>"
       },
       {
         "t11pregunta": "</td> <td>"
       },
       {
         "t11pregunta": "</td> </tr><tr><td>"
       },
       {
         "t11pregunta": "</td> <td>"
       },
       {
         "t11pregunta": "</td> <td>"
       },
       {
         "t11pregunta": "</td> </tr></table>"
       }
     ],
 
     "pregunta": {
       "c03id_tipo_pregunta": "8",
       "t11pregunta": "Arrastra los elementos y completa la tabla.",
       "t11instruccion": "",
       "respuestasLargas": true,
       "pintaUltimaCaja": false,
       "contieneDistractores": true
     }
   },*/
  //7
  {
    "respuestas": [
      {
        "t13respuesta": "3",
        "t17correcta": "1,7"
      },
      {
        "t13respuesta": "2",
        "t17correcta": "2,4,6"
      },
      {
        "t13respuesta": "1",
        "t17correcta": "3,5"
      },
      {
        "t13respuesta": "2",
        "t17correcta": "2,4,6"
      },
      {
        "t13respuesta": "1",
        "t17correcta": "3,5"
      },
      {
        "t13respuesta": "2",
        "t17correcta": "2,4,6"
      },
      {
        "t13respuesta": "3",
        "t17correcta": "1,7"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Arrastra las palabras que completen el párrafo.<br>Lee con atención las siguientes ideas centrales, posteriormente clasifica las oraciones de acuerdo al número que corresponda a partir de la idea central a la que pertenecen.<br><ol> <li>El hombre utiliza el mar para tirar su basura.<br><li>Efectos de la contaminación del agua.<br><li>Medidas para evitar la contaminación.<br></ol>"
      },
      {
        "t11pregunta": " Evitar tirar basura a las fuentes de agua.<br>"
      },
      {
        "t11pregunta": " Mal olor, cambio de color, fermentación y cambios de temperatura.<br>"
      },
      {
        "t11pregunta": " En algunas ciudades, el drenaje llega al agua del mar.<br>"
      },
      {
        "t11pregunta": " La muerte de plantas y animales acuáticos.<br>"
      },
      {
        "t11pregunta": " Los ecológos  están preocupados por cantidad de contaminantes que llegan a mares y océanos.<br>"
      },
      {
        "t11pregunta": " Producción de diversas enfermedades que afectan al hombre: fiebre tifoidea, hepatitis, etc.<br>"
      },
      {
        "t11pregunta": " Campañas que promuevan la conversación de este recurso natural.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //8
  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "En un texto expositivo el autor plasma su opinión.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Para realizar un texto expositivo, se necesita hacer una investigación sobre el tema en diversas fuentes.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Cuando se encuentra una fuente de información, se debe leer para conocer el contenido y extraer los datos más importantes.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Un texto expositivo presenta descripciones y sentimientos del autor.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Para determinar si un texto contiene la información que se está buscando, se deben identificar las palabras clave.",
        "correcta": "1",
      },

    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },
  //9
  {
    "respuestas": [
      {
        "t13respuesta": "Realidad",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Fantasía",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Relatan algo verdadero (situación, lugar o persona que existió).",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Pueden incluir rasgos característicos y únicos de una cultura, como una costumbre o tradición.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Relatan exageraciones en las que atribuyen poderes o características irreales a los personajes de los que se habla",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Suelen incluir personajes fantásticos.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Se ubican en un contexto histórico, es decir, parten de un hecho o época específica real.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Incluyen elementos sobrenaturales para exagerar la historia.",
        "correcta": "1",
      },

    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige Realidad (R) o Fantasía (F) según corresponda.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },
  //10
  {
    "respuestas": [
      {
        "t13respuesta": "Forman parte del folclore de una cultura.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Su función se limita a contar relatos.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Aun cuando tienen relación con hechos históricos, desarrollan algunos elementos fantásticos y sobrenaturales.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Tienen como propósito explicar un fenómeno sobrenatural.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Únicamente desarrolla elementos históricos.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "No sufren modificaciones a lo largo del tiempo.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Son explicaciones fidedignas sobre la época en la que se desarrollan.",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "2",
      "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta.<br><br>¿Cuáles son las características de la leyenda?",
      "t11instruccion": "",
    }
  },
  //11
  {
    "respuestas": [
      {
        "t13respuesta": "Flotaba en el aire.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Como una sombra se desvanecía.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Todos los días a mitad de la noche.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Hincada, daba el último angustioso lamento.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Recorría muchas calles de la ciudad dormida.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Continuaba con el paso lento y pausado hacia el mismo rumbo.",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "2",
      "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta.<br><br>\n\
      Todas los días a mitad de la noche, cuando todo se encontraba completamente oscuro, una mujer aparecía por los callejones del pueblo, vestía un traje blanquísimo y un velo cubría su rostro.\n\
      Una espesa neblina la acompañaba, flotaba en el aire y con paso lento recorría muchas calles de la ciudad dormida, cada noche distintas, aunque sin faltar una sola. Se le solía ver en la Plaza Mayor con mayor frecuencia, donde vuelto el velado rostro hacia el oriente, hincada, daba el último angustioso y languidísimo lamento. Después, se ponía de pie, continuaba con paso lento y pausado hacia el mismo rumbo. Al llegar a orillas del salobre lago, que en ese tiempo penetraba dentro de algunos barrios, como una sombra, se desvanecía.\n\
      <br><br>¿Qué frases denotan elementos fantásticos en el fragmento anterior?",
      "t11instruccion": "",
    }
  },
];

/*
  {
    "respuestas": [
      {
        "t13respuesta": "Título que busca atrapar la atención del espectador.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Imágenes y recursos gráficos que hacen más claro o atractivo el mensaje.",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "Símbolos, imágenes o letras que conforman el nombre o imagen de una empresa.",
        "t17correcta": "3",
      },
      {
        "t13respuesta": "Invitación al público para la compra.",
        "t17correcta": "4",
      },
      {
        "t13respuesta": "Frase que identifica a la marca o producto, debe ser fácil de recordar.",
        "t17correcta": "5",
      },
      {
        "t13respuesta": "Medio por el que los consumidores pueden adquirir los productos o pedir más informes.",
        "t17correcta": "6",
      },
      {
        "t13respuesta": "Muestran un beneficio que pueden adquirir los consumidores si compran el producto.",
        "t17correcta": "7",
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Encabezado"
      },
      {
        "t11pregunta": "Fotografía"
      },
      {
        "t11pregunta": "Logotipo"
      },
      {
        "t11pregunta": "Llamado a la acción"
      },
      {
        "t11pregunta": "Eslogan"
      },
      {
        "t11pregunta": "Contacto"
      },
      {
        "t11pregunta": "Motivador"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "t11instruccion": "",
      "altoImagen": "100px",
      "anchoImagen": "200px"
    }
  },*/



/*
 {
    "respuestas": [
      {
        "t13respuesta": "Propaganda",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Publicidad",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Chocolate Tía Chona, disfruta los mejores momentos.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Vota por el candidato del PYZ para que ganes más dinero.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Ayuda a los niños con cáncer, dona en el centro de acopio.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Catsup Tomatin, ponle a la comida lo mejor de ti.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Nosotros ponemos la ropa, tú pones el estilo: Jacketwearsuit.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Recuerda respetar las reglas de tránsito y mantener limpia la ciudad, con tu ayuda mejoramos todos.",
        "correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Elige Propaganda o Publicidad según corresponda.",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },*/