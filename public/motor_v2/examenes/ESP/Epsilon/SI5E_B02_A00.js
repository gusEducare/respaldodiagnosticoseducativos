json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Expone el contenido, desarrolla cada aspecto del tema a tratar, detalla los procesos y ejemplifica situaciones.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Plantea el tema y el objetivo del texto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se elabora una síntesis de lo más relevante del tema y se mencionan recomendaciones o invitaciones a profundizar en el mismo.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Desarrollo"
            },
            {
                "t11pregunta": "Introducción"
            },
            {
                "t11pregunta": "Conclusión"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las definiciones con el elemento del texto expositivo al que corresponde."
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "El hombre utiliza el mar para tirar su basura.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Efectos de la contaminación del agua",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Medidas para evitar la contaminación",
                "t17correcta": "2"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Evitar tirar basura a las fuentes de agua.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mal olor, cambio de color, fermentación y cambios de temperatura.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En algunas ciudades, el drenaje llega al agua del mar.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La muerte de plantas y animales acuáticos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ecólogos están muy preocupados por la elevada cantidad de contaminantes que llegan a mares y océanos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Producción de diversas enfermedades que afectan al hombre: fiebre tifoidea, hepatitis, etc.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Campañas que promuevan la conservación de este recurso natural.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Contaminantes como materiales radiactivos, derrames de petróleo, herbicidas, pesticidas, fertilizantes, etc.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la opción que relacione cada idea central con las oraciones.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "Causas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Rocas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Volcánica",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Zona",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Sismos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Montañas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Desplazamiento",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Deslizamientos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tectónica",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aguas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Explosiones atómicas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Fallas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Peso",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee con atención el siguiente texto.<br><br>Selecciona todas las palabras clave que aparecen en el texto anterior.",
            "t11instruccion": "<center><b>Causas de los sismos</b></center><br>Se dice que el movimiento de las placas tectónicas es una de las causas de los sismos.  Sin embargo, no es la única. Las causas más comunes son:<br><br>Tectónica: son los  que se originan por el desplazamiento de las placas tectónicas que conforman la corteza y afectan grandes extensiones.<br><br>Volcánica: es poco frecuente. Ocurre cuando un volcán tiene una erupción violenta y afecta a lugares cercanos.<br><br>Hundimiento: ocurre cuando al interior de la corteza terrestre se produce acción erosiva de las aguas subterráneas.<br><br>Deslizamientos: se debe al peso de las montañas, el cual las aplana y provoca sismos.<br><br>Explosiones atómicas: son provocadas por pruebas realizadas en la fabricación de armamento."
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En un texto expositivo, el autor plasma su opinión.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para realizar un texto expositivo, se necesita hacer una investigación sobre el tema en diversas fuentes.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando se encuentra una fuente de información, se debe leer para conocer el contenido y extraer los datos más importantes.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un texto expositivo presenta descripciones y sentimientos del autor.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para determinar si un texto contiene la información que se está buscando, se deben identificar las palabras clave.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "VARIOS, (1993) Secretos y misterios de la historia.  México, Reader´s Digest, pág. 18.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "NAVARRO, R. (1994) Historia de México I. México, McGraw-Hill, pág. 53.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "LOZANO, J.M. (2006) Historia del Arte. México, Compañía Editorial Continental, pág. 209.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Tesoros perdidos de los nazis<br>Cuando cayó Alemania, sus líderes fascistas trataron de ocultar 7.5 mil millones de dólares en oro e invaluables obras de arte robadas. Nunca se recuperó la mayor parte del botín, aunque, sorprendentemente, una parte fue hallada en 1990, en un pequeño pueblo de Texas."
            },
            {
                "t11pregunta": "Hacia el año 1215 aparecen en el valle los aztecas o mexicas, que habían intervenido en la destrucción de Tula. Procedían del noreste y habían atravesado Guanajuato, Querétaro, Hidalgo y México."
            },
            {
                "t11pregunta": "En la pintura podemos distinguir dos aspectos: la pintura mural y la de miniatura. La mayoría de las obras son anónimas."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada texto con su referencia bibliográfica."
        }
    },
    // Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "Revista",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Internet",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Libro",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Biblioteca",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Internet",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Libro",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿A qué fuente de consulta pertenece la ficha: KING, S. (1980). La niebla. Nueva York. Editorial: Viking Press.?", "¿A qué fuente de consulta pertenece la ficha: Günther, K. (2010) Etecnología: Saber cómo cuidar el medio ambiente. Recuperado el 20 de agosto de 2017 de https://goo.gl/hnajGy?"],
            "preguntasMultiples": true
        }
    },
    // Reactivo 7
    {
        "respuestas": [{
                "t13respuesta": "Siempre se debe colocar entre comillas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aporta seriedad y formalidad a nuestros trabajos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Puedes mencionar al autor, pero no es indispensable.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Debe incluir los datos del texto o publicación de la que fue extraída.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No es necesario colocarlas entre comillas.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se debe copiar de manera exacta cada palabra leída.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las características de una cita textual que aparezcan en la siguiente lista."
        }
    },
    // Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "Realidad",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Fantasía",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Relatan algo verdadero (situación, lugar o persona que existió).",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pueden incluir rasgos característicos y únicos de una cultura, como una costumbre o tradición.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Relatan exageraciones en las que les atribuyen poderes o características irreales a los personajes de los que se habla.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Suelen incluir personajes fantásticos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se ubican en un contexto histórico, es decir, parten de un hecho o época específica real.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Incluyen elementos sobrenaturales para exagerar la historia.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "De las siguientes oraciones, selecciona la letra R, si corresponde a una característica de narración apegada a la realidad o la letra F, si es de alguna basada en la fantasía.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 9
    {
        "respuestas": [{
                "t13respuesta": "Atribuye poderes o características irreales al personaje.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Incluye elementos sobrenaturales.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se ubica en un contexto histórico.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué característica de las leyendas se ejemplifica en el siguiente párrafo?",
            "t11instruccion": "Se dice que allá por el siglo XVI, después de la conquista, habitantes de la Ciudad de México aguardaban en su casas cuando sonaban las campanas de la Gran Catedral.  Ese repicar indicaba el inicio de la hora de queda y que al poco rato se escucharían los estridentes gemidos de una mujer afligida, por el dolor de haber perdido a uno de sus hijos."
        }
    },
    // Reactivo 10
    {
        "respuestas": [{
                "t13respuesta": "Tienen como propósito explicar algún fenómeno.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Forman parte del folclore de una cultura.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Únicamente desarrolla elementos históricos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Su función se limita a contar relatos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No sufren modificaciones a lo largo del tiempo.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Aún cuando tienen una base histórica desarrollan elementos fantásticos y sobrenaturales.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las características de las leyendas que estén presentes en la siguiente lista."
        }
    },
    // Reactivo 11
    {
        "respuestas": [{
                "t13respuesta": "Usaba un traje de una blancura como la nieve y un velo que cubría su rostro.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Cada noche recorría, una por una, las calles de la ciudad.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "A la orilla del lago, se podía ver su espíritu cruzar sobre el agua  y las pequeñas olas que su presencia formaban.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué fragmento del siguiente párrafo desarrolla los elementos fantásticos y sobrenaturales de una leyenda? ",
            "t11instruccion": "Usaba un traje de una blancura como la nieve y un velo que cubría su rostro. Cada noche recorría, una por una, las calles de la ciudad. Sólo se escuchaban sus amargos lamentos, mas solo era una sombra entre las paredes. A la orilla del lago, se podía ver su espíritu cruzar sobre el agua y las pequeñas olas que su presencia formaban."
        }
    },
    // Reactivo 12
    {
        "respuestas": [{
                "t13respuesta": "Se utiliza para mostrar relaciones de semejanza entre dos o más elementos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Consiste en la sustitución de un elemento por otro con el cual tiene similitud.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Es un recurso literario que consiste en exagerar las cualidades de una persona o cosa.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Comparación"
            },
            {
                "t11pregunta": "Metáfora"
            },
            {
                "t11pregunta": "Hipérbole"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada recurso literario con su definición."
        }
    },
    // Reactivo 13
    {
        "respuestas": [{
                "t13respuesta": "Metáfora",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Hipérbole",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Comparación",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué recurso literario se utiliza en el siguiente enunciado?<br><br>Usaba un traje de una blancura como la nieve y un velo que cubría su rostro."
        }
    },
    // Reactivo 14
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En los textos expositivos se presentan ideas aisladas para darle variedad al tema.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los textos expositivos están compuestos de párrafos y estos están integrados por oraciones.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las oraciones se clasifican únicamente en oraciones principales.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las oraciones de los textos expositivos tratan sobre un mismo tema y se clasifican en oraciones principales y secundarias.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las oraciones de un texto expositivo no obedecen una secuencia lógica.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las oraciones de un texto expositivo están encadenadas por nexos que las vinculan unas con otras.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 15
    {
        "respuestas": [{
                "t13respuesta": "<img src=\"SI5E_B02_A15_02.png\">",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src=\"SI5E_B02_A15_01.png\">",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src=\"SI5E_B02_A15_03.png\">",
                "t17correcta": "3"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Portada"
            },
            {
                "t11pregunta": "Índice"
            },
            {
                "t11pregunta": "Prólogo"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada palabra con la parte del libro que corresponda."
        }
    },
    // Reactivo 16
    {
        "respuestas": [{
                "t13respuesta": "Tiene el título del libro, el nombre completo del autor o autores, el lugar y el año en que se imprimió, el nombre de la editorial y el número de la colección.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tiene el título de la obra, el nombre del autor o autores y el sello editorial.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Escrito realizado o por el mismo autor o por alguna persona que sea especialista en el tema que se trata en el libro.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Es una lista que contiene separados cada uno de los capítulos del contenido del libro, en el que se indica en qué página específicamente de la numeración se encuentra cada uno de ellos.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Portada"
            },
            {
                "t11pregunta": "Portadilla"
            },
            {
                "t11pregunta": "Introducción"
            },
            {
                "t11pregunta": "Índice"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada recurso literario con su definición."
        }
    },
    // Reactivo 17
    {
        "respuestas": [{
                "t13respuesta": "La contaminación del canal por la basura.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La indiferencia del gobierno.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La sequía en la zona.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Cuál es la principal problemática que se presenta en el siguiente fragmento de noticia?",
            "t11instruccion": "La región de Comitán, Chiapas está sufriendo debido a que hace más de 30 días que no llueve en la zona.  Campesinos y agricultores se encuentran angustiados, ya que han perdido gran parte de sus cosechas debido a la sequía, las cuales ascienden a más de 2 millones de pesos.  Representantes del Gobierno Estatal acudieron a escuchar las peticiones de los habitantes. “No está en nuestras manos, no podemos controlar la naturaleza”, fueron las palabras del delegado."
        }
    },
    // Reactivo 18
    {
        "respuestas": [{
                "t13respuesta": "La delincuencia organizada.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Que no llueva en dos días.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Un cambio de presidente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Que una familia se cambie de domicilio.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El aumento al precio del transporte público.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona de la siguiente lista aquellos  acontecimientos que pueden ser relevantes para una comunidad."
        }
    },
    // Reactivo 19
    {
        "respuestas": [{
                "t13respuesta": "<p>boletines<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>público<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>grupo<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>intereses<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>atractivo<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>informativos<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>noticioso<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>publicidad<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>digital<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>mensual<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Los "
            },
            {
                "t11pregunta": " informativos están dirigidos a un "
            },
            {
                "t11pregunta": " específico y contienen información de interés para un "
            },
            {
                "t11pregunta": " de personas que comparte algunas características o "
            },
            {
                "t11pregunta": ". Deben tener un título que resulte "
            },
            {
                "t11pregunta": " para su audiencia. Son "
            },
            {
                "t11pregunta": " y pueden incluir contenido "
            },
            {
                "t11pregunta": ", comentarios o "
            },
            {
                "t11pregunta": ". Se puede distribuir en papel o en versión "
            },
            {
                "t11pregunta": "; de manera semanal, "
            },
            {
                "t11pregunta": ", bimensual, semestral o anual."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde corresponda para completar el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 20
    {
        "respuestas": [{
                "t13respuesta": "Un comercial de televisión",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Un espectacular",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Un boletín informativo",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Lee atentamente el siguiente caso:<br><br>En tu comunidad se está organizando el torneo anual de futbol varonil de la categoría jóvenes de 17 a 21 años. Necesitas comunicar dicho evento para que los equipos interesados se puedan registrar y se enteren de los requisitos, así como para que se den cita el 20 de octubre del 2017 en las canchas municipales. <br><br>¿Qué medio es más conveniente para difundir la información del evento?"
        }
    },
    // Reactivo 21
    {
        "respuestas": [{
                "t13respuesta": "<p>jóvenes<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>octubre<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>municipales<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Tener<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>médico<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>ropa<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>jueves<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>participación<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [{
                "t11pregunta": "<center><b>Atenta Invitación</b></center><center><img src=\"SI5E_B02_A21_01.png\" width=\"530\"></center><br>Se le invita a todos los "
            },
            {
                "t11pregunta": " a participar en el Torneo Anual de Futbol Varonil. La cita es el 20 de "
            },
            {
                "t11pregunta": " a las 10 am en las canchas "
            },
            {
                "t11pregunta": ".  Los requisitos son:<br>"
            },
            {
                "t11pregunta": "entre 17 y 21 años.<br>Llevar un certificado "
            },
            {
                "t11pregunta": ".<br>Presentarse con "
            },
            {
                "t11pregunta": " deportiva.<br>Contar con disponibilidad para entrenar los martes y"
            },
            {
                "t11pregunta": " por la tarde y jugar los partidos los fines de semana.<br><br><center>¡Esperamos contar con tu "
            },
            {
                "t11pregunta": "!</center>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde corresponda para completar el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    }
];