json=[
    //reactivo 1
     {
        "respuestas": [
            {
                "t13respuesta": "Argumento",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Dato",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cada año se producen 200 billones de libras de plástico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las botellas de agua de plástico producen 1.5 toneladas de basura tan solo en Estados unidos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las bolsas de plástico para el súper tardan más de 500 años en degradarse.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una persona desecha en promedio 238 bolsas de plástico, por eso es importante sustituirlas por bolsas de tela.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es importante reducir el uso de bolsas de plástico, ya que no son biodegradables.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se recomienda utilizar recipientes de vidrio para calentar en el microondas, ya que el plástico emite una sustancia llamada bisfenol que puede provocar cáncer.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige argumento (A) o dato (D) según corresponda a cada frase.",
            "descripcion": "Frase",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    //reactivo 2
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las citas y referencias bibliográficas no son necesarias en un trabajo de investigación.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Utilizar citas y referencias bibliográficas otorga validez y seriedad a los trabajos de investigación.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las citas son evidencia de la confiabilidad de la información que presenta el autor.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para escribir una cita se debe hacer uso de la paráfrasis.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una cita se deben escribir entre comillas “ ” las palabras dichas o escritas por el autor.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una cita se puede omitir el nombre del autor y los datos de la publicación de donde fue extraída.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda a cada frase.",
            "descripcion": "Frase",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
    //reactivo 3
    {  
        "respuestas":[  
           {  
              "t13respuesta":"En los años ochenta y noventa se usaban más bolsas de plástico que botellas.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"En la actualidad se usan más botellas de plástico que bolsas.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"El uso de plástico ha ido aumentando con el tiempo.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"En el año 2007 se utilizaron más botellas de plástico que bolsas.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"En el año 2017 se redujo el uso de plástico con respecto a 2007.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"En la actualidad se utilizan más bolsas de plástico que botellas.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"En 1987 se usó una gran cantidad de botellas de plástico.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"En 1997 disminuyó el uso de bolsas de plástico con respecto a 1987.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br><img src='SI5E_B04_A03_.png' width=800 height=400><br>¿Qué información presenta la gráfica anterior?",
        }
     },
    //reactivo 4
    {
        "respuestas": [
            {
                "t13respuesta": "<p>trama<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>diálogo<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>dramaturgos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>acotaciones<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>ambiente<\/p>",
                "t17correcta": "5"
            }, 
            {
                "t13respuesta": "<p>nudo<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>personajes<\/p>",
                "t17correcta": "7"
            }, 
            {
                "t13respuesta": "<p>actos<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>La <\/p>"
            },
            {
                "t11pregunta": "<p> de una obra se refiere a todos los sucesos que ocurren en ella, la cual seconoce a partir del <\/p>"
            },
            {
                "t11pregunta": "<p> de cada personaje y de sus acciones. En las obras de teatro, los <\/p>"
            },
            {
                "t11pregunta": "<p> utilizan incisos o <\/p>"
            },
            {
                "t11pregunta": "<p> para dar detalles acerca del <\/p>"
            },
            {
                "t11pregunta": "<p>, la vestimenta y las expresiones de los personajes. Una obra de teatro abarca tres momentos: Introducción, <\/p>"
            },
            {
                "t11pregunta": "<p> y desenlace. Se escribe a modo de guión teatral, debe contener información de cada uno de los <\/p>"
            },
            {
                "t11pregunta": "<p> y estar dividido en <\/p>"
            },
            {
                "t11pregunta": "<p> y escenas.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //reactivo 5
    {
        "respuestas": [
            {
                "t13respuesta": "<p>(<\/p>",
                "t17correcta": "1,7"
            },
            {
                "t13respuesta": "<p>)<\/p>",
                "t17correcta": "2,8"
            },
            {
                "t13respuesta": "<p>¡<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>!<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>¿<\/p>",
                "t17correcta": "5"
            }, 
            {
                "t13respuesta": "<p>?<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>(<\/p>",
                "t17correcta": "7,1"
            }, 
            {
                "t13respuesta": "<p>)<\/p>",
                "t17correcta": "8,2"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><\/p>"
            },
            {
                "t11pregunta": "<p>Es una noche oscura, se escuchan truenos y el agua de la lluvia caer.  Se abre el telón y se ve el escenario iluminado: la recámara de los niños, Luis y Favio.  Luis está jugando con un cubo Rubik y Favio hace un avión con una caja de pasta de dientes y palillos.<\/p>"
            },
            {
                "t11pregunta": "<p><br>FAVIO: <\/p>"
            },
            {
                "t11pregunta": "<p>Ya me aburrí<\/p>"
            },
            {
                "t11pregunta": "<p> Quiero salir a probar mi nuevo avión. (Se asoma a la ventana.)<br>LUIS: (Sin voltear a verlo)<\/p>"
            },
            {
                "t11pregunta": "<p> Ya vas a empezar a quejarte de nuevo<\/p>"
            },
            {
                "t11pregunta": "<p><br>FAVIO: ¡Mira, es Marisol! Está brincando en los charcos. (Señala hacia la ventana.)<br>LUIS: <\/p>"
            },
            {
                "t11pregunta": "<p>Continúa armando su cubo<\/p>"
            },
            {
                "t11pregunta": "<p>¡Seguro! Nunca se pierde una tormenta.<br>(Favio se pone a saltar en su recámara con los pies juntos, como si fuera de un charco a otro.)<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los signos de puntuación que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //reactivo 6
    {
        "respuestas": [
            {
                "t13respuesta": "Creatividad y energía para hacer cosas divertidas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Inteligencia para hacer cosas divertidas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Gusto por la naturaleza y por correr riesgos.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Favio"
            },
            {
                "t11pregunta": "Luis"
            },
            {
                "t11pregunta": "Marisol"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.<br><br>De acuerdo con el texto anterior, selecciona las características de cada personaje."
        }
    },
    //reactivo 7
    {
        "respuestas": [
            {
                "t13respuesta": "¿Qué sabores de helado prefieren los niños de la escuela?",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El objetivo de esta encuesta es conocer los sabores de helado favoritos de los niños de la escuela “Héroes de la Nación”.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se aplicó una encuesta de 10 preguntas en el patio de la escuela. En total se encuestaron  a 60 personas, 40 niños y 20 niñas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='SI5E_B04_A07.png'>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Los sabores favoritos de los niños de la escuela “Héroes de la Nación” son: limón y chocolate, seguidos por fresa y al último vainilla.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Título"
            },
            {
                "t11pregunta": "Introducción"
            },
            {
                "t11pregunta": "Desarrollo"
            },
            {
                "t11pregunta": "Resultados"
            },
            {
                "t11pregunta": "Conclusiones"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta":"Relaciona las columnas según corresponda.",
            "altoImagen": "150px",
            "anchoImagen":"230px"
        }
    },
    //reactivo 8
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Por un lado,<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Días después<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Primero<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>luego<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>Finalmente<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Una mañana, la maestra nos dijo que llevaríamos a cabo el “Proyecto Empresa” del año escolar, el cual consistía en vender helados a la hora del recreo.<br>La idea nos causó emoción y nerviosismo a la vez. <\/p>"
            },
            {
                "t11pregunta": "<p> teníamos que planear todas las actividades a realizar, y por otro, decidir quién las llevaría a cabo. <br><\/p>"
            },
            {
                "t11pregunta": "<p>, nos entró la duda sobre los sabores que deberíamos vender.<br>-Necesitan hacer una encuesta-, nos dijo la maestra. Así sabrán qué sabores prefieren sus clientes.<br><\/p>"
            },
            {
                "t11pregunta": "<p> redactamos las preguntas de la encuesta, después sacamos las copias suficientes y <\/p>"
            },
            {
                "t11pregunta": "<p>, fuimos a los salones a aplicarlas. Al terminar todo este proceso, hicimos el conteo e interpretación de los resultados obtenidos. <\/p>"
            },
            {
                "t11pregunta": "<p>, decidimos que venderíamos tres sabores: fresa, chocolate y limón.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
];