//reactivo 1

json=[
 {
        "respuestas": [
            {
                "t13respuesta": "Te quiero y sé que te quiero porque sé lo que quiero.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Tu ausencia deja mi corazón frío y seco como un bosque en invierno. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "El tiempo vuela cuando estoy contigo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "La calle irregular que va de afuera bajando desde el llano más escueto termina donde el gótico sujeto contrasta con el color de la cantera.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Tu ausencia deja mi corazòn frìo y seco recordando ese bosque en invierno. A tu regreso volverà la primavera y entonces mi alma estarà llena",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Repetición"
            },
            {
                "t11pregunta": "Símil"
            },
            {
                "t11pregunta": "Metáfora"
            },
            {
                "t11pregunta": "Rima consonante"
            },
            {
                "t11pregunta": "Rima asonante"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"<b>Relaciona las columnas según corresponda.</b>"
        }
    },
    
//reactivo 2

    {
        "respuestas": [
            {
                "t13respuesta": "<p>Persona que posee iniciativa,</p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>entusiasmo y determinación</p>",
                "t17correcta": "2"
            },
             {
                "t13respuesta": "<p>para iniciar una aventura.</p>",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "<p>Afronta retos, resuelve</p>",
                "t17correcta": "4"
            },
             {
                "t13respuesta": "<p>problemas y toma decisiones.</p>",
                "t17correcta": "5"
            },
             {
                "t13respuesta": "<p>Persona que crea un negocio.</p>",
                "t17correcta": "6"
            },
             {
                "t13respuesta": "<p>Liderazgo y habilidad</p>",
                "t17correcta": "7"
            },
             {
                "t13respuesta": "<p>para motivar a los trabajadores.</p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
           {
                "c03id_tipo_pregunta": "8",
                "t11pregunta":  "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td>Concepto</td>\n\
                                        <td>Definición</td>\n\
                                        <td>Características</td>\n\
                                        </tr><tr>\n\
                                        <td>Emprendedor</td>\n\
                                        <td>"
                
            },
             {
                "t11pregunta": " "
            },
             {
                "t11pregunta": " "
            },
            { //
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
             {
                "t11pregunta": " "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>Empresario</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
             {
                "t11pregunta": " "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr></table>"
                                                             
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<b>Lee el siguiente texto. Arrastra los elementos y completa la tabla.</b>",
            "t11instruccion":"<center><b>¿Cuál es la diferencia entre un emprendedor y un empresario?</b></center><br><br>Desde el siglo XVII, la palabra <b>Emprendedor</b> se utiliza para referirse a aquellas personas que, con entusiasmo, iniciativa y determinación, inician una aventura. En aquellos tiempos, los emprendedores eran aventureros que iniciaban el viaje de Europa al Nuevo Mundo, afrontando retos en busca de oportunidades.<br><br>Una persona que escala una montaña es una persona emprendedora, al igual que alguien que forma una familia o inicia un negocio. Una persona emprendedora afronta grandes retos, resuelve problemas y toma decisiones para alcanzar sus sueños.<br><br>Un empresario es una persona que crea o adquiere un negocio con fines de lucro, es decir, para obtener ganancias económicas; desarrolla y establece procesos para hacer funcionar eficiente e inteligentemente el negocio. El empresario debe ser el líder que induce y motiva a los trabajadores para que alcancen los objetivos de su empresa. Además, contribuye con la comunidad al proporcionar un bien o servicio útil a los consumidores, incentiva la economía, genera fuentes de empleo y crea nuevos puestos de trabajo.<br><br>Calao & Zepeda (2014)",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //reactivo 3}

    {
        "respuestas": [{
                "t13respuesta": "L",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "F",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las rosas se secan con el paso del tiempo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cielo llora de tristeza.  ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el otoño se caen las hojas de los árboles.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El árbol no quiere que sus hojas lo abandonen. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los buenos sentimientos hacen que florezca el campo más árido.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El calentamiento global está cambiando el clima.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b>Elige literal (L) o figurado (F) según corresponda al tipo de significado de cada frase.</b>",
            "descripcion": "Aspectos a valorar",
            "evaluable": false,
            "evidencio": false
        }
    },

    //reactivo 4
     {
        "respuestas": [{
                "t13respuesta": "Hospital, médicos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Manicomio, terapeutas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Teatro, actores",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Descubrimiento inesperado",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Ansiedad",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Miedo a los lugares cerrados",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Emoción",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Escritor",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Biblioteca",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Miedo a los espacios abiertos",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Miedo a la oscuridad",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Dolor de piernas",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Agradable",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Agresivo",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Deshonesto",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["<b>Selecciona la opción que corresponda al significado de las palabras que están resaltadas.</b><br><br>Después del accidente, el ingeniero sufrió lesiones en la columna vertebral y tuvo que ser atendido en el <b>nosocomio.</b> Ahí recibió la atención adecuada por parte de los <b>galenos</b>, así como las terapias necesarias para su pronta recuperación.",
                "Fui a la biblioteca a investigar acerca de los modelos atómicos para la tarea de Química y me encontré un libro muy interesante de Historia del arte moderno que me encantó. ¡Vaya <b>serendipia</b>!",
                "La última novela del autor me hizo entrar en <b>catarsis</b> mientras la leía.",
                "Pablo no puede jugar por mucho tiempo en el campo de fútbol, ya que sufre de <b>agorafobia</b>.",
                "La maestra de quinto grado tiene un trato <b>afable</b> con sus alumnos, por eso la quieren mucho."
            ],
            "preguntasMultiples": true
        }
    },
    //reactivo 5
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Elegir el tema a debatir.<\/p>",
                "t17correcta": "0",
                etiqueta:"paso 1"
            },
            {
                "t13respuesta": "<p>Recopilar información de diferentes fuentes acerca del tema.<\/p>",
                "t17correcta": "1",
                etiqueta:"paso 2"
            },
            {
                "t13respuesta": "<p>Elegir una postura ante el tema.<\/p>",
                "t17correcta": "2",
                etiqueta:"paso 3"
            },
            {
                "t13respuesta": "<p>Determinar los argumentos para defender la postura elegida.<\/p>",
                "t17correcta": "3",
                etiqueta:"paso 4"
            },
            {
                "t13respuesta": "<p>Fundamentar los argumentos a exponer.<\/p>",
                "t17correcta": "4",
                etiqueta:"paso 5"
            },
            {
                "t13respuesta": "<p>Exponer los argumentos cuando sea el turno para hablar.<\/p>",
                "t17correcta": "5",
                etiqueta:"paso 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<b>Ordena los siguientes elementos.<br><br>¿Cuál es el procedimiento para organizar un debate?</b>",
        }
    },
    //reactivo 6
      {
        "respuestas": [
            {
                "t13respuesta": "sin embargo, lo hacen de manera temporal.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "de modo que, han elevado los precios de los productos que contribuyen a tener una mejor alimentación. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "o sea que muestran personas que son felices por consumir productos bajos en grasa.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "de modo que se vuelva un hábito y un estilo de vida.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Algunas personas han cambiado sus hábitos para llevar una vida más saludable;"
            },
            {
                "t11pregunta": "Desafortunadamente, algunas empresas sacan provecho de esta nueva forma de vida,"
            },
            {
                "t11pregunta": "La publicidad y las empresas han puesto de moda llevar una vida saludable,"
            },
            {
                "t11pregunta": "Lo importante es tener buenos hábitos alimenticios y hacer ejercicio diariamente,"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"<b>Relaciona las columnas según corresponda para formar oraciones complejas</b>"
        }
    }
];


