json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Es la forma, tamaño y presentación de la revista.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Primera página de la  publicación, que sirve como presentación con los títulos más relevantes.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Parte posterior de la revista donde se colocan anuncios.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Diseño mediante ilustraciones, dibujos o tipografía especial, que dan la identidad al nombre de la revista.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Informa el número y fecha de publicación.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Lista de nombres y contactos de los colaboradores de la revista.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Frase que representa el objetivo e identidad de la revista.",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "Información con los títulos y páginas del contenido de la revista.",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "Información breve acerca del contenido general de la revista.",
                "t17correcta": "9"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Formato",
            },
            {
                "t11pregunta": "Portada"
            },
            {
                "t11pregunta": "Contraportada"
            },
            {
                "t11pregunta": "Logotipo"
            },
            {
                "t11pregunta": "Fechario"
            },
            {
                "t11pregunta": "Directorio"
            },
            {
                "t11pregunta": "Lema"
            },
            {
                "t11pregunta": "Índice"
            },
            {
                "t11pregunta": "Sumario"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",

            "t11pregunta": "Relaciona las columnas según corresponda.",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Argumento racional<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Descripción<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Argumento de hecho<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Argumento de ejemplificación<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Narración<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Argumento sentimental<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Todos los queretanos son mexicanos, todos los ciudadanos de México son libres, por lo tanto, los queretanos son libres. <\/p>"
            },
            {
                "t11pregunta": "<br><p> El koala es un marsupial que habita principalmente en Oceanía, es un animal noble, tranquilo y muy perezoso. Su dentadura cuenta con afilados dientes, tienen cinco dedos con fuertes garras para trepar por los árboles, pueden llegar a medir hasta 85 cm y pesar 15 kilogramos. &nbsp;&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<br><p> Más de 20 millones de niños viven en pobreza, de los cuales, cinco millones se encuentran en pobreza extrema. Estas cifras van en aumento y se deben a la desigualdad en la educación y a la centralización de los recursos  públicos, así como a inversiones privadas y públicas.&nbsp;&nbsp; <\/p>"
            },
            {
                "t11pregunta": "<br><p> En épocas pasadas, las personas no hacían uso de teléfonos, computadoras, internet, ni tabletas, por eso no deberíamos preocuparnos por nuestro estilo de vida si no las usamos de ahora en adelante.  &nbsp;&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<br><p>Alfredo Bustamante nos mostró en el monitor de su pantalla, las fotografías tomadas por el telescopio espacial Hubble, en realidad no podíamos entender nada de lo que veíamos porque las imágenes las tiene que interpretar un experto con la formación académica adecuada. &nbsp;&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<br><p> No pude hacer la tarea porque ayer mi perro se perdió, no tenía la concentración necesaria para hacer las cosas bien, por eso merezco otra oportunidad.&nbsp;&nbsp;<\/p>"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": true,
            "contieneDistractores": true
        }
    },

    //3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Palabras provenientes de<br> culturas precolombinas que se<br> han incorporado al español.<\/p>",
                "t17correcta": "1",

            },
            {
                "t13respuesta": "<p>Chapulín, chicle, cóndor, <br>apapacho.<\/p>",
                "t17correcta": "2"
                ,
            },
            {
                "t13respuesta": "<p>Son palabras incorporadas al<br> español usadas comúnmente<br> en una región específica.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Papalote, amolado,  agüitado,<br> gringo.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Son palabras que provienen<br> de otro idioma y que se han <br>incorporado al español.<\/p>",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "<p>Closet, flash, kayak, cúter.<\/p>",
                "t17correcta": "6",

            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; }\n\
                                   </style><table class='table' style='margin-top:-40px;'>\n\
                                   <tr>\n\
                                       <td style='width:350px'>Concepto</td>\n\
                                       <td style='width:100px'>Definición</td>\n\
                                       <td style='width:100px'>Ejemplos</td>\n\
                                   </tr>\n\
                                   <tr>\n\
                                       <td >Indigenismos </td> <td >"
            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": " </td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "     </td></tr><tr>\n\
                                       <td>Regionalismos</td> <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "       </td><td>"

            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td> </tr><tr>\n\ <td>Extranjerismos</td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td>  <td>"

            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td>\n\
                                       </tr></table>"

            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla. <br><br>Coloca los indigenismos, regionalismos, extranjerismo así como las definiciones en el lugar correspondiente.",
            "contieneDistractores": true,
            "anchoRespuestas": 90,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cuentos son narraciones que pueden incluir características  que de manera ficticia reflejan circunstancias sociales, políticas e ideológicas de una región.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Realismo Mágico hace uso de muchos indigenismos y regionalismos en sus narraciones.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Realismo Mágico se desarrolló en España durante el siglo XIX.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El idioma es inmodificable, la Real Academia de la Lengua Española se encarga de preservar la gramática sin alteraciones.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los paisajes representados en el Realismo Mágico suelen ser grandes palacios y personajes muy allegados a la burguesía.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una de las características más sobresalientes del Realismo Mágico es su manejo del tiempo no lineal o tiempo estático.",

                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": " Aspectos a valorar",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,
            "t11instruccion": ''
        }
    },
    //5
     {
        "respuestas": [
            {
                "t13respuesta": "Derecho a la intimidad, a que se respete el cuerpo, así como el espacio personal.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Derecho a votar y participar en las decisiones de la comunidad.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Derecho a tener una lengua propia y preservarla.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Derecho a gozar de un servicio de salud y justicia.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Derecho al trabajo y gozar de un salario justo.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Derecho a expresar opiniones, a pensar y a mantener o cambiar creencias.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Derechos individuales",
            },
            {
                "t11pregunta": "Derechos ciudadanos"
            },
            {
                "t11pregunta": "Derechos culturales"
            },
            {
                "t11pregunta": "Derechos sociales"
            },
            {
                "t11pregunta": "Derechos económicos"
            },
            {
                "t11pregunta": "Derechos de conciencia"
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12",

        }
    },
    //6
    
  

    {
        "respuestas": [
            {
                "t13respuesta": "No ser detenido arbitrariamente.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tener nacionalidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Libertad de pensamiento.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "No ser discriminado.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "No ser esclavizado.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Ser inocente hasta que se pruebe la culpabilidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Circular libremente por el país.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Gozar de protección legal y asesoría jurídica.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derecho a tener la ropa de moda.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a ser premiado por estudiar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a invadir propiedad privada.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a tener automóvil. ",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>De los siguientes, ¿cuáles se consideran derechos humanos fundamentales según la Asamblea General de los Derechos Humanos?",

            "t11instruccion": "",
        }
    },
 //7

    {
        "respuestas": [
            {
                "t13respuesta": "Elegir un tema de interés actual y colectivo.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Investigar a profundidad sobre el tema elegido.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Definir los roles a desempeñar: moderador, expositores y audiencia.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Planear el respaldo de los argumentos a exponer.",
                "t17correcta": "1",
            },
            
            {
                "t13respuesta": "Definir varios temas a tratar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Exponer solo con lo que se conozca sobre el tema.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "No asignar roles a desempeñar.",
                "t17correcta": "0",
            },
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las respuestas correctas.<br><br>¿Qué actividades se deben realizar para llevar a cabo una mesa redonda?",

            "t11instruccion": "",
        }
    },



    {
        "respuestas": [
            {
                "t13respuesta": "Investigar ampliamente el tema a exponer y definir el punto de vista a exponer.",
                "t17correcta": "0",
                etiqueta:"paso 1"
            },
            {
                "t13respuesta": "Realizar un guion que incluya: introducción, desarrollo (argumentos y posibles contraargumentos) y conclusiones.",
                "t17correcta": "1",
                etiqueta:"paso 2"
            },
            {
                "t13respuesta": "Comenzar la participación con una introducción al tema.",
                "t17correcta": "2",
                etiqueta:"paso 3"
            },
            {
                "t13respuesta": "Mencionar los datos o cifras que que apoyen la posición frente al tema.",
                "t17correcta": "3",
                etiqueta:"paso 4"
            },
            {
                "t13respuesta": "Finalizar con un comentario personal, una posible solución y las razones por las que la audiencia debería tomar esa postura.",
                "t17correcta": "4",
                etiqueta:"paso 5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos.<br><br>¿Cuáles son los pasos a seguir para participar en una mesa redonda?",
        }
    },//9 falso o verdadero
      {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cuento es una narración muy larga que tiene una estructura básica.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cuento es un texto que desarrolla una trama.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el desenlace del cuento es cuando los personajes enfrentan un problema que deben solucionar.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A la narración en primera persona se le conoce también como “protagonista”.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hechos que narra un cuento pueden ser en espacios reales o imaginarios.",

                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": " Aspectos a valorar",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,
            "t11instruccion": ''
        }
  
  },
  //10 ordena elementos
  
  
   {
        "respuestas": [
            {
                "t13respuesta": "<p><b>El espejo chino</b> <br>Cuento anónimo</p>",
                "t17correcta": "0",
                etiqueta:"paso 1"
            },
            {
                "t13respuesta": "Un campesino chino se fue a la ciudad para vender la cosecha de arroz y su mujer le encargó un peine.",
                "t17correcta": "1",
                etiqueta:"paso 2"
            },
            {
                "t13respuesta": "Después de vender su arroz el campesino se reunió con sus amigos para celebrar.",
                "t17correcta": "2",
                etiqueta:"paso 3"
            },
            {
                "t13respuesta": "Cuando iba a regresar a su casa, no recordaba qué le había pedido su mujer.",
                "t17correcta": "3",
                etiqueta:"paso 4"
            },
            {
                "t13respuesta": "Entonces, compró en una tienda un espejo y regresó al pueblo.",
                "t17correcta": "4",
                etiqueta:"paso 5"
            }
            ,
            {
                "t13respuesta": "Llegando a su casa, le entregó el regalo a su mujer y se marchó.",
                "t17correcta": "5",
                etiqueta:"paso 6"
            },
            {
                "t13respuesta": "Cuando la mujer se miró en el espejo comenzó a llorar y le dijo a su madre: “Mi marido ha traído a otra mujer, joven y hermosa.”",
                "t17correcta": "6",
                etiqueta:"paso 7"
            }
            ,
            {
                "t13respuesta": "La madre tomó el espejo y al verlo le dijo a su hija: ”No tienes de qué preocuparte, es una vieja.”",
                "t17correcta": "7",
                etiqueta:"paso 8"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos para formar el cuento.",
        }
    },

];

