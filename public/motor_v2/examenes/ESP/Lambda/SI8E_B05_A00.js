json = [
//1
{  
    "respuestas":[  
       {  
          "t13respuesta":"Narra los sucesos en orden cronológico.",
          "t17correcta":"1"
       },
       {  
          "t13respuesta":"El término crónica proviene del griego kronos que significa tiempo.",
          "t17correcta":"1"
       },
       {  
          "t13respuesta":"Describe lugares, sucesos, ambientes, antecedentes y participantes del evento.",
          "t17correcta":"1"
       },
       {  
          "t13respuesta":"Incluye comentarios e interpretaciones del cronista.",
          "t17correcta":"1"
       },
       {  
          "t13respuesta":"Existe solo un tipo de crónica.",
          "t17correcta":"0"
       },
       {  
          "t13respuesta":"Su extensión es específicamente de 3 cuartillas.",
          "t17correcta":"0"
       },
       {  
          "t13respuesta":"Es un texto objetivo y serio.",
          "t17correcta":"0"
       },
    ],
    "pregunta":{  
       "c03id_tipo_pregunta":"2",
       "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br><br>Cuáles son las características de una crónica."
    }
 },
 //2
 {
    "respuestas": [
        {
            "t13respuesta": "Tiempo",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Espacio",
            "t17correcta": "1"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Esa mañana...",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Como todas las navidades...",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Recuerdo cuánto disfrutaba ese bosque...",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Era un cuarto oscuro con olor a objetos antiguos...",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Exactamente a las 11:35 aparecieron en el escenario...",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La arena era suave como el talco...",
            "correcta"  : "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Elige tiempo (T) o espacio (E) según haga referencia cada frase.",
        "descripcion":"Reactivo",
        "evaluable":false,
        "evidencio": false,
    }
},
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>semana<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>simpático<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>chiflido<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>México<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>pijama<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>cuadra<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>atuendo<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>colorida<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>primera<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>música<\/p>",
                "t17correcta": "10"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><strong>Crónica en pijama</strong><br>Era una mañana como cualquier otra en mi fin de <\/p>"
            },
            {
                "t11pregunta": "<p>. Me desperté después de las 10 a.m. (porque antes de esa hora no soy muy <\/p>"
            },
            {
                "t11pregunta": "<p>) y puse la tetera en la estufa.  Mientras esperaba escuchar el <\/p>"
            },
            {
                "t11pregunta": "<p> avisando que mi agua ya estaba lista para prepararme mi té, prendí la radio.  En ese preciso momento estaban Pablo Cuevas y Martín Fabela, los locutores de 89.5 anunciando que regalarían boletos para el concierto de “The neighborhood” en <\/p>"
            },
            {
                "t11pregunta": "<p>. “Estamos en la esquina de Av. del rocío con Calzada de la soledad y regalaremos un boleto doble a la persona que venga en <\/p>"
            },
            {
                "t11pregunta": "<p> y cante una canción de la banda”.<br><br>¡Eso era a solo una <\/p>"
            },
            {
                "t11pregunta": "<p> de mi casa! No lo pensé dos veces, me levanté del sillón y salí corriendo. Al fin y al cabo, tenía el <\/p>"
            },
            {
                "t11pregunta": "<p> perfecto. Crucé la calle como si no pasaran autos (recuerdo que escuché algunos insultos, pero no me importó).  Al llegar al “crucero prometido” estaban los 2 locutores sentados dentro de una camioneta <\/p>"
            },
            {
                "t11pregunta": "<p> que tenía logotipos de la estación de radio. Tuve la suerte de ser el primero en llegar y cantar “Sweater weather”, ahora mi canción favorita, por supuesto.<br><br>Brincaba de felicidad al recibir mis boletos y saber que vería en <\/p>"
            },
            {
                "t11pregunta": "<p> fila a una de mis bandas favoritas. Ya me imaginaba... la gente, la <\/p>"
            },
            {
                "t11pregunta": "<p>, las luces, los chiflidos… ¡Oh no! La tetera.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Eran las 7 de la mañana cuando comenzaron los manifestantes a llegar. Traían pancartas y estaban vestidos de blanco.<\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>Para las 8:00 la Plaza de la Libertad ya estaba llena, no cabía un alfiler.<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Comenzaron a marchar a las 8:30, caminaron a lo largo de Avenida Independencia hasta llegar a la Plaza de la luz.<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>Ahí prendieron veladoras para recordar a las personas fallecidas en el terremoto. Exactamente a las 9:34 guardaron un minuto de silencio.<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 4"
            },
            {
                "t13respuesta": "<p>Finalmente, entregaron al Alcalde una carta solicitando ayuda a las familias damnificadas. El evento terminó a las 10 de la mañana.<\/p>",
                "t17correcta": "4",
                etiqueta:"Paso 5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos.<br><br>Elabora una crónica.",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La carta poder se utiliza para gestionar trámites administrativos formales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Puede ser redactada en lenguaje informal.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debe contener el nombre completo de los participantes.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es un documento formal y de carácter legal.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debe presentar información ambigua.",
                "correcta"  : "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Reactivo",
            "evidencio": false,
        }
    },
    //6
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Que Marcos utilice el número de teléfono de Daniel.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Que Daniel utilice el número de teléfono de Marcos.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Que la compañía telefónica le dé un nuevo equipo a Marcos.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Que la compañía telefónica le dé un nuevo equipo a Daniel.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Lee el texto de la imagen y selecciona la respuesta correcta.<br><center><img src='SI8E_B05_A06_01.png' width=600 height=400></center><br>¿Qué información presenta la gráfica anterior?",
        }
     },
     //7
     {  
        "respuestas":[  
           {  
              "t13respuesta":"Se debe contar con una firma estable.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Siempre hay que firmar de la misma forma.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"La firma debe ser personal e intransferible.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Se debe cambiar de firma constantemente.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"La firma debe ser fácil de falsificar.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"Selecciona todas las afirmaciones que sean correctas."
        }
     },
];
