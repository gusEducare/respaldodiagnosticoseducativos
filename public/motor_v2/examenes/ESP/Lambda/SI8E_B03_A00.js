json = [
    // Reactivo 1
   {  
      "respuestas":[
         {  
            "t13respuesta": "Ambos poemas hablan de la muerte.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Ambos autores hablan de resignación.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Los dos poemas hablan sobre juventud.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },   
         {  
            "t13respuesta": "Cuando él muera, todos deben seguir con su vida normal.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Ante su muerte, todo será tristeza y sufrimiento.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El autor teme a la muerte.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Es preferible morir joven y hermosa.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "La muerte llega en la vejez.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "No es posible predecir la muerte.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cuál es la semejanza entre ambos poemas?",
                         "¿Qué mensaje plantea García Lorca?","¿Qué mensaje plantea De la Cruz?"],
         "preguntasMultiples": true,
     "t11instruccion":"<center><strong>Despedida</strong></center><br>Si muero,<br>dejad el balcón abierto. <br><br>El niño come naranjas. <br>(Desde mi balcón lo veo). <br><br>El segador siega el trigo. <br>(Desde mi balcón lo siento). <br><br>¡Si muero, <br>dejad el balcón abierto!<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(García Lorca, 1931) <br><br><center><strong>Muestra se debe escoger antes de morir </strong></center><br>Miró Celia una rosa que en el prado<br> ostentaba feliz la pompa vana<br> y con afeites de carmín y grana<br> bañaba alegre el rostro delicado;<br><br>y dijo: Goza, sin temor del hado,<br>el curso breve de tu edad lozana,<br>pues no podrá la muerte de mañana<br>quitarte lo que hubieres hoy gozado.<br><br>Y aunque llega la muerte presurosa<br>y tu fragante vida se te aleja,<br>no sientas el morir tan bella y moza:<br><br>mira que la experiencia te aconseja<br>que es fortuna morirte siendo hermosa<br>y no ver el ultraje de ser vieja.<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(De la Cruz, 1692)"
      }
   },
    // Reactivo 2
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Se narra en primera persona.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Tiene como objetivo persuadir al lector.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Es un texto argumentativo de estilo personal.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Su objetivo es plantear puntos de vista de un tema específico.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Su objetivo es entretener al lector.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Incluye las opiniones del autor sin fundamentos teóricos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Se narra en tercera persona.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Es un texto informativo.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas a la pregunta.<br><br>¿Cuáles son las características de un ensayo literario?"
      }
   },
    // Reactivo 3
 {
        "respuestas": [
            {
                "t13respuesta": "Quiroga",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "célebre",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "marcada",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "ocasionado",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "ella",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "padrastro",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "accidentalmente",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "Lugones",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "él",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "momentáneamente",
                "t17correcta": "9"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Horacio Silvestre Quiroga Forteza nació el 31 de diciembre de 1878, en Salto, Uruguay. Sus padres eran Prudencio "
            },
            {
                "t11pregunta": " y Pastora Forteza.<br><br>La vida de este "
            },
            {
                "t11pregunta": " escritor estuvo "
            },
            {
                "t11pregunta": " por la tragedia. Por ello, no es de extrañar que muchas de sus obras fueran del mismo tema. Su padre murió a causa de un accidente, "
            },
            {
                "t11pregunta": " por un disparo de escopeta, frente a él y a su madre. En 1891, "
            },
            {
                "t11pregunta": " contrajo nupcias con Ascencio Barcos. Su "
            },
            {
                "t11pregunta": " sufrió un derrame cerebral que le impedía el habla y terminó suicidándose. En 1901, murieron sus 2 hermanos debido a fiebre tifoidea y años después, Horacio mató "
            },
            {
                "t11pregunta": " de un disparo, a su amigo Federico Ferrando. Posteriormente, su hija Eglé Quiroga, su amigo, el escritor Leopoldo Lugones y su hijo varón, Darío, se suicidaron."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },   
    
    // Reactivo 4
      {  
      "respuestas":[  
         {  
            "t13respuesta":     "Quiroga es el mayor representante del género extraño.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "<i>Cuentos de amor, de locura y de muerte</i> es una de sus principales obras.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Su primer libro de poesía se llamó <i>Los arrecifes de coral.</i>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Horacio Quiroga es uno de los grandes cuentistas latinoamericanos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Su primera esposa se llamaba María Esther.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Su segunda esposa era amiga de su hija.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Uno de sus pasatiempos fue la floricultura.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Quiroga fundó la <i>Revista del Salto.<i/>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br><br>¿Cuáles son los datos y sucesos más importantes de la vida de Horacio Quiroga?",
             "t11instruccion":"La trágica vida de Quiroga, considerado uno de los más grandes cuentistas de todos los tiempos, marcó sus obras, las cuales se caracterizaron por narrar sucesos extraños y misteriosos que se quedaban en el delgado límite de la fantasía y la realidad. Por eso se le considera el mayor representante del género extraño.<br>En 1898 conoció a María Esther Jurkovski, su primera esposa y quien fuera la inspiración para sus obras Las sacrificadas y Una estación de Amor. En ese mismo año, fundó la Revista del Salto, la cual representó un fracaso en su carrera. Más tarde, en 1901, publicó su primer libro de poesía Los arrecifes de coral.<br>Después de la muerte de su amigo Federico Ferrando, se mudó a Argentina donde escribió sus primeros cuentos.  En 1917 publicó Cuentos de amor, de locura y de muerte, una de sus obras más importantes, que incluye cuentos de terror y misterio.<br>En 1927 contrajo matrimonio por segunda ocasión con una amiga de su hija, de ese matrimonio nació una niña. En 1929 publicó la novela Pasado amor pero al no obtener buena respuesta, decidió dedicarse a la floricultura.<br>Años después, Horacio Quiroga  se enfermó de cáncer de próstata, enfermedad que lo llevó a tomar cianuro y suicidarse.<br>Entre sus obras más destacadas se encuentran Cuentos de amor, de locura y de muerte (1917), Cuentos de la selva (1918), El salvaje (1920) y Los desterrados (1926)."
      }
   },
    // Reactivo 5
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caricatura hace referencia al costo de la gasolina.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caricatura puede ser utilizada para informar acerca de las nuevas tecnologías para comprar combustibles.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caricatura mostrada utiliza globos para transmitir el mensaje.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caricatura habla del surgimiento de gasolineras para una clase social alta.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caricatura habla de la reducción del costo de la gasolina.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caricatura presenta mensajes explícitos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la caricatura y elige falso (F) o verdadero (V) según corresponda.<center><img src='SI8E_B03_A05_01.png' width= '300' height= '200'></center>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    }
];