json = [
//Reactivo 1
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las entrevistas permiten obtener información poco detallada.",               
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una entrevista permite obtener información y detalles acerca de un tema.",                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las entrevistas únicamente se obtienen opiniones. ",               
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las entrevistas se puede hacer uso tanto de preguntas cerradas como abiertas.",                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al adaptar el lenguaje en una entrevista se garantiza la comprensión de la misma.  ",                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La información recopilada durante la entrevista se utiliza para realizar un reporte.",                
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },
    //Reactivo 2
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "El Campeonato Juvenil Canadiense de Squash se realizará en Toronto, Canadá.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Habrá competidoras de India, Egipto, Nueva Zelanda, Pakistán, Inglaterra, Perú y Colombia.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Participarán 2 niñas mexicanas: una de Guanajuato y otra de Veracruz.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "El torneo será el 13 de diciembre de 2018.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Ana Fernanda Ojeda Mireles nació en Acámbaro, Guanajuato.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Sus papás y amigos le dicen Ana Fer, de cariño.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Todas las tardes come en casa de su abuelita antes de ir a entrenar.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Su papá juega squash con ella todos los domingos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br> ¿Qué información deberá contener un reporte de entrevista acerca del Campeonato Juvenil Canadiense de Squash?"
      }
   },
    //Reactivo 3
    {
        "respuestas": [
            {
                "t13respuesta": "I",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "D",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "“A veces es complicado combinar el deporte con la escuela. Es agotador, pero vale la pena.”",               
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "“Siempre me gustó hacer deporte, de hecho, juego basquetbol desde los seis años.”",                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "“Me llena de emoción poder ir tan lejos y representar a mi país, aunque también estoy muy nerviosa.”",               
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ana Fernanda nos platicó que siente un gran compromiso por representar a su país en el extranjero.",                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ana Fernanda comenzó a practicar squash cuando tenía  seis años.",                
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La entrevistada comentó que su padre siempre le ha dado todo su apoyo para ir a los torneos.",                
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige indirecto (I) o directo (D) según corresponda al estilo de discurso de cada frase.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },
    //Reactivo 4
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Es un texto breve.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Su objetivo es hacer una crítica literaria.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Busca despertar el interés de los lectores.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Incluye un análisis acerca del contexto, trama y personajes de una obra literaria.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Su objetivo es presentar un reporte científico.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Para escribir una reseña no es necesario haber leído la obra.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Es un texto extenso.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br> ¿Cuáles son las características de una reseña literaria?"
      }
   },
    //Reactivo 5
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Es un día muy frío.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Es un día como cualquier otro.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Es un día caluroso.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el siguiente fragmento y selecciona la respuesta correcta.<br>¿Qué está sucediendo en la ciudad de Edimburgo?",
          "t11instruccion":"[…] Las pequeñas calles de Edimburgo se metamorfosean. Las fuentes se transforman en jarrones helados que sujetan ramilletes de hielo. El viejo río se ha disfrazado de lago de azúcar glaseado y se extiende hasta el mar. Las olas resuenan como cristales rotos. […]<br><br><i>(Malzieu, 2007)</i>"
      }
   },
    //Racivo 6
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "La madre del protagonista es joven e inexperta.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "La madre del protagonista es una mujer madura.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "La protagonista quiere ser mamá.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el siguiente fragmento y selecciona la respuesta correcta.<br>¿Qué significa la frase anterior?",
          "t11instruccion":"“Mi madre es una niña que juega a tener un bebé.”<br><br><i>(Malzieu, 2007)</i>"
      }
   },
    //Reactivo 7
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una reseña literaria se pueden omitir los datos del autor.",               
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reseña literaria tiene la siguiente estructura: introducción, desarrollo, cierre y referencias bibliográficas.",                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La introducción de una reseña contiene la opinión final del autor acerca de la obra. ",               
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El desarrollo de la reseña contiene una descripción detallada de la trama y personajes de la obra.",                
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La referencia bibliográfica de una reseña incluye los datos de publicación de la obra.",                
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F)  o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Aspecto",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },
    //Reactivo 8
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Concientizar a la sociedad acerca de los efectos de la contaminación del aire.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Concientizar a la sociedad acerca de los efectos de la contaminación del agua.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Concientizar a la sociedad acerca de la muerte prematura.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el siguiente fragmento y selecciona la respuesta correcta.<br>De acuerdo con la frase anterior, ¿cuál es el propósito del reportaje?",
          "t11instruccion":"Realiza la lectura del siguiente fragmento del reportaje “La Tierra pide un respiro”  de Gómez y Muñoz (2012), publicado por El ideario.<br><br>(...)La contaminación atmosférica causa un gran daño al medio ambiente y la muerte prematura de 2.3 millones de personas en el mundo, según la Organización Meteorológica Mundial. Un estudio de la Unión Europa prevé que para 2050 haya un mayor deterioro del aire. (…)”<br><br><i>Gómez y Muñoz (2012)</i>"
      }
   },
     //Reactivo 9
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Tiene la siguiente estructura: título, entrada, desarrollo y conclusión.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Se apoya de diferentes géneros periodísticos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Puede incluir fotografías o infografías.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Aborda temas de interés para la comunidad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Utiliza lenguaje informal.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "No es necesario incluir resultados de la investigación realizada.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br>¿Qué características tiene un reportaje?",
          
      }
   },
    //Reactivo 10
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Entrevistas a especialistas en el tema.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Citas textuales con su referencia.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Fotografías",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Rumores",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Reportajes de otros autores sin citar la fuente.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas para la pregunta.<br>¿Qué tipo de textos se pueden incluir en los reportajes?",
          
      }
   },
];
