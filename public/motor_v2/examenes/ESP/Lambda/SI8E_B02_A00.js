json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Definir varios temas de tu interés.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Definir un tema de interés actual para el colectivo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Conocer a profundidad el tema o problemática.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No investigar respecto al tema, con lo que conozcas basta.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Conocer el rol activo  que desempeñarás; ya sea como moderador, expositor o audiencia.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No asignar los roles a desempeñar.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Planear el respaldo de tus argumentos.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee los siguientes enunciados y selecciona todos los que indiquen acciones que debes realizar antes de participar en una mesa redonda."
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando participas en una mesa redonda, debes dar tus puntos de vista aun cuando no los respaldes con investigación previa.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es indispensable que te prepares para las posibles preguntas de la audiencia.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No debes permitir preguntas de la audiencia.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es importante definir el orden de las intervenciones y la duración de cada una.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debes delimitar el tema que vas a exponer en la mesa redonda.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee con atención los siguientes enunciados y elige verdadero (V) o falso según corresponda (F).",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "Presentar información estadística.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Presentar semejanzas y diferencias entre dos hechos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Resumir las consecuencias actuales de los hechos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Ilustrar cuantitativamente las consecuencias de los hechos.",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Observa el siguiente cuadro comparativo y selecciona la respuesta correcta.<br><center><img src=\"SI8E_B02_A03_00.png\"></center><br>Durante el desarrollo de una mesa redonda, este cuadro comparativo puede ser de apoyo para:"
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "SI8E_B02_A04_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "SI8E_B02_A04_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "SI8E_B02_A04_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false

        },
        "contenedores": [{
                "Contenedor": ["", "47,90", "cuadrado", "156, 63", ".", "transparent"]
            },
            {
                "Contenedor": ["", "47,406", "cuadrado", "156, 63", ".", "transparent"]
            },
        ]
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "<p>Investiga de manera amplia el tema que se discutirá en la mesa redonda.</p>",
                "t17correcta": "0",
                "etiqueta": "paso 1"
            },
            {
                "t13respuesta": "<p>Define tu punto de vista sobre el tema.<\/p>",
                "t17correcta": "1",
                "etiqueta": "paso 2"
            },
            {
                "t13respuesta": "<p>Realiza un guion que incluya: introducción, desarrollo (argumentos y posibles contraargumentos) y conclusiones.<\/p>",
                "t17correcta": "2",
                "etiqueta": "paso 3"
            },
            {
                "t13respuesta": "<p>Comienza tu participación con una introducción sobre tu tema.<\/p>",
                "t17correcta": "3",
                "etiqueta": "paso 4"
            },
            {
                "t13respuesta": "<p>Menciona los datos o cifras que hayas investigado que apoyen tu posición frente al tema.<\/p>",
                "t17correcta": "4",
                "etiqueta": "paso 5"
            },
            {
                "t13respuesta": "<p>Finaliza con un comentario personal y plantea una posible solución e indica las razones de por qué los demás deberían apoyarte.<\/p>",
                "t17correcta": "5",
                "etiqueta": "paso 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Arrastra las oraciones para ordenar el proceso de participación en una mesa redonda.",
        }
    },
    // Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "La población tiene plena confianza en las acciones de los gobernantes.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El gobierno del estado de Morelos no supo cómo administrar y repartir los víveres a los damnificados del sismo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El 19 de septiembre hubo un incendio en el estado de Morelos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La desconfianza de la población, provocó que dejaran de enviar ayuda a Morelos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Para los gobernantes, es prioridad mostrar  en las redes sociales, que están atendiendo las necesidades del pueblo.",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee el siguiente texto y, en la lista posterior, subraya todos los enunciados que recuperen los puntos de vista del autor.",
            "t11instruccion": "<center><b>Confianza perdida en los escombros</b></center><br>Después del temblor que movió la Ciudad de México el pasado 19 de septiembre, la ciudadanía no tardó en organizarse para hacer llegar ayuda a los afectados en los diferentes estados de la República.   Sin embargo, a los pocos días comenzaron las denuncias, a través de las redes sociales, contra el gobierno de Morelos.<br><br>“No nos dejan pasar. Se quedan con los víveres y los medicamentos, para después repartirlos y publicar las fotos en la página del Estado.” fue el testimonio de un brigadista proveniente de Acámbaro, Guanajuato.<br><br>Estas publicaciones provocaron, con justa razón, la molestia tanto de los internautas nacionales, como de los  internacionales quienes dejaron de dar ayuda a este estado.  Algunos medios de comunicación se dieron a la tarea de entrevistar al gobernador Graco Ramírez: “No se está deteniendo la ayuda, solo la estamos recolectando para hacerla llegar de la manera más rápida y eficiente posible. Desgraciadamente, todo se salió de control.”, fueron sus palabras.<br><br>Sin duda, aquí el problema es la falta de confianza. ¿A quién le creemos? Tal vez sea cierto, pero por ahora, pagan justos por pecadores."
        }
    },
    // Reactivo 7
    {
        "respuestas": [{
                "t13respuesta": "Primera persona",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Segunda persona",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Tercera persona",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "En un patio",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "En una casa abandonada",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "En el espacio",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Mazzini-Ferraz",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Masserati Ferrari",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Mazzini y Berta",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "En hacer ruidos como un tranvía.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Estaban sentados en un banco del patio.",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "En ensuciarse para que su mamá los cuidara.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Lee el siguiente fragmento de cuento y selecciona la respuesta correcta.<br><br>¿En qué persona se encuentra el narrador?",
                "¿En qué escenario se desarrolla la historia?",
                "¿Cuáles son los apellidos de los padres de los niños?",
                "¿En qué se ocupaban los niños durante todo el día?"
            ],
            "preguntasMultiples": true,
            "t11instruccion": "<center><b>Confianza perdida en los escombros</b></center><br>Después del temblor que movió la Ciudad de México el pasado 19 de septiembre, la ciudadanía no tardó en organizarse para hacer llegar ayuda a los afectados en los diferentes estados de la República.   Sin embargo, a los pocos días comenzaron las denuncias, a través de las redes sociales, contra el gobierno de Morelos.<br><br>“No nos dejan pasar. Se quedan con los víveres y los medicamentos, para después repartirlos y publicar las fotos en la página del Estado.” fue el testimonio de un brigadista proveniente de Acámbaro, Guanajuato.<br><br>Estas publicaciones provocaron, con justa razón, la molestia tanto de los internautas nacionales, como de los  internacionales quienes dejaron de dar ayuda a este estado.  Algunos medios de comunicación se dieron a la tarea de entrevistar al gobernador Graco Ramírez: “No se está deteniendo la ayuda, solo la estamos recolectando para hacerla llegar de la manera más rápida y eficiente posible. Desgraciadamente, todo se salió de control.”, fueron sus palabras.<br><br>Sin duda, aquí el problema es la falta de confianza. ¿A quién le creemos? Tal vez sea cierto, pero por ahora, pagan justos por pecadores."
        }
    },
    // Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "Los cuatro hijos y sus padres.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "(...) Pero en el vigésimo mes sacudiéronlo una noche convulsiones terribles, y a la mañana siguiente no conocía más a sus padres. (...)",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "(...) Tenían la lengua entre los labios, los ojos estúpidos, y volvían la cabeza con la boca abierta. (...)",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Todo el día, sentados en el patio, en un banco estaban los cuatro hijos idiotas del matrimonio Mazzini-Ferraz. (...)",
                "t17correcta": "4"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Personajes"
            },
            {
                "t11pregunta": "Nudo"
            },
            {
                "t11pregunta": "Descripción de personajes"
            },
            {
                "t11pregunta": "Introducción"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada momento del cuento con el concepto que le corresponda."
        }
    },
    // Reactivo 9
    {
        "respuestas": [{
                "t13respuesta": "Desenlace",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Introducción",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Nudo",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Narrador",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Diálogos",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Secuencias de acción",
                "t17correcta": "6"
            },
        ],
        "preguntas": [{
                "t11pregunta": "En esta parte del relato se presenta la resolución de la situación planteada."
            },
            {
                "t11pregunta": "El autor plantea el ambiente, personajes y lugar donde se desarrollará la historia."
            },
            {
                "t11pregunta": "Indica la situación que los personajes deben enfrentar y solucionar."
            },
            {
                "t11pregunta": "Es quien que lleva la voz de la historia, todo relato es contado por uno de ellos."
            },
            {
                "t11pregunta": "Son las palabras que emplean los personajes para comunicarse."
            },
            {
                "t11pregunta": "Es el orden en que se mencionan los hechos en la obra."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada descripción con el elemento del cuento al que corresponda."
        }
    },
    // Reactivo 10
    {
        "respuestas": [{
                "t13respuesta": "Son narraciones breves.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Son narraciones extensas con un gran número de personajes.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Su origen puede ser  literario o popular.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Son textos que desarrollan una trama.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Son relatos fantásticos ubicados en lugares y momentos distintos a la realidad.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Los hechos que narran pueden ser reales o imaginarios.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Están redactados en forma de prosa.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sus temas son tan variados como la creatividad permita.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Están redactados en forma de verso.",
                "t17correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las características de un cuento."
        }
    },
    // Reactivo 11
    {
        "respuestas": [{
                "t13respuesta": "lejano",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "enorme",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "hija",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "viajeros",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "abrigo",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "rotundamente",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "súplicas",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "amada",
                "t17correcta": "8"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Se dice que en un lugar muy "
            },
            {
                "t11pregunta": ", un rey estaba paseando por su "
            },
            {
                "t11pregunta": " reino con su hermosa "
            },
            {
                "t11pregunta": ". Ambos disfrutaban la majestuosidad del bosque, cuando de repente se encontraron con un grupo de "
            },
            {
                "t11pregunta": " que necesitaban alimento, "
            },
            {
                "t11pregunta": " y un lugar donde pasar la noche.<br>Al principio, el rey se negó "
            },
            {
                "t11pregunta": "; sin embargo, sucumbió ante las "
            },
            {
                "t11pregunta": " de su "
            },
            {
                "t11pregunta": " hija, quien le pidió que ayudara a esos pobres viajeros."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el cuento.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 12
    {
        "respuestas": [{
                "t13respuesta": "<p><b>El espejo chino</b><br>Cuento anónimo</p>",
                "t17correcta": "0",
                "etiqueta": "1"
            },
            {
                "t13respuesta": "<p>Un campesino chino se fue a la ciudad para vender la cosecha de arroz y su mujer le encargó un peine.<\/p>",
                "t17correcta": "1",
                "etiqueta": "2"
            },
            {
                "t13respuesta": "<p>Después de vender su arroz el campesino se reunió con sus amigos para celebrar.<\/p>",
                "t17correcta": "2",
                "etiqueta": "3"
            },
            {
                "t13respuesta": "<p>Cuando iba a regresar a su casa, no recordaba qué le había pedido su mujer.<\/p>",
                "t17correcta": "3",
                "etiqueta": "4"
            },
            {
                "t13respuesta": "<p>Entonces, compró en una tienda un espejo y regresó al pueblo.<\/p>",
                "t17correcta": "4",
                "etiqueta": "5"
            },
            {
                "t13respuesta": "<p>Llegando a su casa, le entregó el regalo a su mujer y se marchó.<\/p>",
                "t17correcta": "5",
                "etiqueta": "6"
            },
            {
                "t13respuesta": "<p>Cuando la mujer se miró en el espejo comenzó a llorar y le dijo a su madre: “Mi marido ha traído a otra mujer, joven y hermosa”<\/p>",
                "t17correcta": "6",
                "etiqueta": "7"
            },
            {
                "t13respuesta": "<p>La madre tomó el espejo y al verlo le dijo a su hija: ”No tienes de qué preocuparte, es una vieja.”<\/p>",
                "t17correcta": "7",
                "etiqueta": "8"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Arrastra las siguientes frases para ordenar el cuento.<br><br>Bibliografía: Narrativa Breve. (2017). Cuento breve: El espejo chino | Un entrañable cuento anónimo. [online] Available at: https://narrativabreve.com/2013/10/cuento-breve-espejo-chino.html [Accessed 9 Nov. 2017].",
        }
    },
    // Reactivo 13
    {
        "respuestas": [{
                "t13respuesta": "Donas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Camote",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Piloncillo",
                "t17correcta": "3"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Picarones"
            },
            {
                "t11pregunta": "Batatas"
            },
            {
                "t11pregunta": "Chancaca"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Lee la receta para preparar “picarones” y relaciona cada palabra con su sinónimo.",
            "t11instruccion": "<center><b>Picarones</b></center><br><center><img src=\"SI8E_B02_A13_01.png\"></center><br>Ingredientes:<br>1 lb de batatas, sin pelar<br>1 cucharada de azúcar<br>4 tazas de harina<br>Aceite vegetal<br>2 trozos de chancaca<br><br>Pela y pica las batatas. Cocínalas hasta que se ablanden. Escúrrelas y aplástalas para formar un puré. Agrega la harina y media taza de agua hasta que la masa se sienta suave. Tápalo y déjalo reposar durante dos horas. Cuando la masa esté lista, calienta  aceite en una cacerola grande y fríe los picarones.<br><br><b>Para hacer el jarabe:</b><br>Ralla la chancaca, ponla en una cacerola con agua y cocina a fuego medio hasta que se disuelva y forme una miel espesa. Escurre y déjalo enfriar. Ponlo en un frasco para mojar los picarones.<br><br>Quericavida.com. (2017). Picarones. [online] Available at: http://www.quericavida.com/recetas/picarones/ab58648f-c005-4f89-8d7b-498af441ee01 [Accessed 9 Nov. 2017]."
        }
    },
    // Reactivo 14
    {
        "respuestas": [{
                "t13respuesta": "machete",
                "t17correcta": "1,6"
            },
            {
                "t13respuesta": "pavo",
                "t17correcta": "2,4"
            },
            {
                "t13respuesta": "fuente",
                "t17correcta": "3,7"
            },
            {
                "t13respuesta": "pavo",
                "t17correcta": "2,4"
            },
            {
                "t13respuesta": "banda",
                "t17correcta": "5,8"
            },
            {
                "t13respuesta": "machete",
                "t17correcta": "1,6"
            },
            {
                "t13respuesta": "fuente",
                "t17correcta": "3,7"
            },
            {
                "t13respuesta": "banda",
                "t17correcta": "5,8"
            },
        ],
        "preguntas": [{
                "t11pregunta": "1. El campesino cortó la hierba con el "
            },
            {
                "t11pregunta": ".<br> 2. En casa de mi abuelita, cada año cenamos "
            },
            {
                "t11pregunta": "en Nochebuena.<br>3. En la boda de mi prima había una "
            },
            {
                "t11pregunta": " de chocolate.<br>4. Mi primo es "
            },
            {
                "t11pregunta": " al hablar en público.<br>5. Cuando salgo a correr, me gusta usar una "
            },
            {
                "t11pregunta": " en la cabeza.<br>6. La maestra reprobó a Juan en el examen por tener un "
            },
            {
                "t11pregunta": ".<br>7. Tengo una noticia importante y de muy buena "
            },
            {
                "t11pregunta": " .<br>8. Los domingos por la tarde toca una "
            },
            {
                "t11pregunta": " en la plaza del pueblo."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la palabra que corresponda para completar la oración.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 15
    {
        "respuestas": [{
                "t13respuesta": "Ecuatoriano",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Venezolano",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Griego",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Argelino",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Beliceño",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Húngaro",
                "t17correcta": "6"
            },
        ],
        "preguntas": [{
                "t11pregunta": "Ecuador"
            },
            {
                "t11pregunta": "Venezuela"
            },
            {
                "t11pregunta": "Grecia"
            },
            {
                "t11pregunta": "Argelia"
            },
            {
                "t11pregunta": "Belice"
            },
            {
                "t11pregunta": "Hungría"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada gentilicio con el país que le corresponda."
        }
    },
    // Reactivo 16
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Jurisprudencia<\/p>",
                "t17correcta": "1,4,7,10"
            },
            {
                "t13respuesta": "<p>Artículos<\/p>",
                "t17correcta": "1,4,7,10"
            },
            {
                "t13respuesta": "<p>Ley<\/p>",
                "t17correcta": "1,4,7,10"
            },
            {
                "t13respuesta": "<p>Justicia<\/p>",
                "t17correcta": "1,4,7,10"
            },
            {
                "t13respuesta": "<p>Interfaz<\/p>",
                "t17correcta": "2,5,8,11"
            },
            {
                "t13respuesta": "<p>Página web<\/p>",
                "t17correcta": "2,5,8,11"
            },
            {
                "t13respuesta": "<p>Malware<\/p>",
                "t17correcta": "2,5,8,11"
            },
            {
                "t13respuesta": "<p>Skype<\/p>",
                "t17correcta": "2,5,8,11"
            },
            {
                "t13respuesta": "<p>Dependencia<\/p>",
                "t17correcta": "3,6,9,12"
            },
            {
                "t13respuesta": "<p>Enfermedad<\/p>",
                "t17correcta": "3,6,9,12"
            },
            {
                "t13respuesta": "<p>Sustancias<\/p>",
                "t17correcta": "3,6,9,12"
            },
            {
                "t13respuesta": "<p>Crónico<\/p>",
                "t17correcta": "3,6,9,12"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><table class='table' style='margin-top:-40px;'><tr><td colspan=\"3\">Campos semánticos</td></tr><tr><td>Derecho</td><td>Tecnología</td><td>Adicciones</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras a la casilla del campo semántico que correspondan.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true,
        }
    },
    // Reactivo 17
    {
        "respuestas": [{
                "t13respuesta": "El género del hablante",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El lugar de procedencia del hablante",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "La edad del hablante",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Los recursos económicos del hablante",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El nivel educativo del hablante",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Cuando nos referimos a variantes léxicas, hablamos de las distintas maneras de nombrar y expresar un mismo significante. Éstas pueden ser originadas por:"
        }
    },
    // Reactivo 18
    {
        "respuestas": [{
                "t13respuesta": "No estoy contento en la fiesta.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "No encuentro lugar en la fiesta.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "No tengo amigos en la fiesta.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Mis amigos asesinaron a una persona en el restaurante.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Mis amigos asesinaron a una persona en el restaurante.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Mis amigos se fueron del restaurante sin pagar.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "No olvides manejar con cuidado.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "No olvides ponerte un impermeable.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "No olvides llamarme cuando llegues.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [
                "Selecciona la opción de respuesta que corresponda al significado de cada frase.<br><br>No me hallo en la fiesta (Paraguay).",
                "Mis amigos dejaron un muerto en el restaurante porque no llevaban dinero (Uruguay).",
                "Está lloviendo muy fuerte, no olvides ponerte tu piloto (Argentina)."
            ],
            "preguntasMultiples": true
        }
    },
];