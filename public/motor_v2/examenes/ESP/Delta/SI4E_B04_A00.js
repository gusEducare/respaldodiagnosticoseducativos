json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Alfabético",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cronológico",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Temático",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Expositivo",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Enciclopedia",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Organización de temas en una enciclopedia conforme a las letras del abecedario."
            },
            {
                "t11pregunta": "Los temas se organizan basándose en la secuencia temporal de los hechos."
            },
            {
                "t11pregunta": "La información de la enciclopedia se hace por temas."
            },
            {
                "t11pregunta": "Tipos de texto que informan sobre un  tema de manera objetiva. Utilizan datos, ejemplos e imágenes."
            },
            {
                "t11pregunta": "Conjunto de artículos de gran variedad de temas y que pueden estar clasificados de diferentes maneras."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<b>Relaciona las columnas según corresponda.</b>"
        }
    },
    // Reactivo 2
     {
        "respuestas": [{
                "t13respuesta": "Título",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Párrafo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Subtítulo",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Recursos gráficos",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Fuente ",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Formación de un eclipse"
            },
            {
                "t11pregunta": "Los eclipses son fenómenos astronómicos en los que el Sol, la Luna y la Tierra se tapan entre ellos. Existen dos tipos de eclipses: el solar y el lunar."
            },
            {
                "t11pregunta": "Eclipse solar"
            },
            {
                "t11pregunta": "188918785"
            },
            {
                "t11pregunta": "Calao, R., Zepeda, C., Bueno, R. y Ambriz, G. (2016). Ciencias Naturales Interactivas 4. 1a ed. Querétaro, Bloque 4, Tema 4."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<b>Relaciona las columnas según corresponda.</b>",
            "t11instruccion":"<center><strong>Formación de un eclipse</strong></center><br><br>Los eclipses son fenómenos astronómicos en los que el Sol, la Luna y la Tierra se tapan entre ellos. Existen dos tipos de eclipses: el solar y el lunar.<br><br><strong>Eclipse solar</strong><br><br>Ocurre cuando la Luna se interpone entre el Sol y la Tierra. La sombra de la Luna se proyecta sobre la Tierra y en determinadas zonas del globo terráqueo se oscurece.  Solo puede suceder en días de Luna nueva ya que en esta posición el satélite se sitúa entre el Sol y la Tierra.<br><br>(Calao et al., 2016)<br><img src='SI4E_B04_A02.png'>"
        }
    },
    //Reactivo 3 
    {  
        "respuestas":[  
           {  
              "t13respuesta":"El dios Quetzalcóatl y un conejo",
              "t17correcta":"1",
              "numeroPregunta":"0"
           },
           {  
              "t13respuesta":"El conejo y la luna",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           {  
              "t13respuesta":"La hierba y el conejo",
              "t17correcta":"0",
              "numeroPregunta":"0"
           },
           { 
            "t13respuesta":"En un bosque", 
            "t17correcta":"1", 
            "numeroPregunta":"1"
            }, 
            { 
            "t13respuesta":"En el mar", 
            "t17correcta":"0", 
             "numeroPregunta":"1"
            }, 
            { 
            "t13respuesta":"En la ciudad", 
            "t17correcta":"0", 
             "numeroPregunta":"1"
            }, 
            { 
            "t13respuesta":"El dios Quetzalcóatl decide viajar por el mundo en forma de humano.", 
            "t17correcta":"1", 
             "numeroPregunta":"2"
            }, 
            { 
            "t13respuesta":"El conejo le ofrece un poco de hierba al dios Quetzalcóatl.", 
            "t17correcta":"0", 
            "numeroPregunta":"2"
            }, 
            { 
            "t13respuesta":"El dios se cansa de caminar y decide sentarse a descansar.", 
            "t17correcta":"0", 
            "numeroPregunta":"2"
            },
            { 
            "t13respuesta":"Quetzalcóatl puede morir de hambre porque no encuentra qué comer.", 
            "t17correcta":"1", 
            "numeroPregunta":"3"
            }, 
            { 
            "t13respuesta":"El conejo ve su imagen reflejada en la luna.", 
            "t17correcta":"0", 
            "numeroPregunta":"3"
            }, 
            { 
            "t13respuesta":"El dios se conmueve por el acto de nobleza del conejo.", 
            "t17correcta":"0", 
            "numeroPregunta":"3"
            },
            { 
            "t13respuesta":"La imagen del conejo queda plasmada en la luna para la eternidad.", 
            "t17correcta":"1", 
            "numeroPregunta":"4"
            }, 
            { 
            "t13respuesta":"El dios decide tomar forma humana para recorrer el mundo.", 
            "t17correcta":"0", 
            "numeroPregunta":"4"
            }, 
            { 
            "t13respuesta":"El conejo le ofrece un poco de hierba a Quetzalcóatl.", 
            "t17correcta":"0", 
            "numeroPregunta":"4"
            }    
        ],
        "pregunta":{   
           "c03id_tipo_pregunta":"1",
            "t11instruccion": "",
           "t11pregunta":["<b>Lee el siguiente texto y selecciona la respuesta correcta.</b><br><br>¿Quiénes son los personajes de la historia?","¿En qué lugar se desarrolla la historia?","¿Qué sucede en la introducción de la narración?","¿Cuál es el nudo o conflicto de la historia?","¿Cuál es el desenlace de la historia?"],
           "t11instruccion":"<center><strong>El conejo en la luna</strong></center><br><br>Se dice que hace muchos años el dios Quetzalcóatl, cuya imagen era de una serpiente emplumada, un día decidió tomar forma humana para pasear por el mundo.<br><br>Caminó por mucho tiempo por bosques, mares y montañas. Al caer la noche, se sentó a descansar en una roca para disfrutar de la tranquilidad de la naturaleza y de la luz de la luna.<br><br>De repente, vio a un conejito que comía en el campo.<br>¿Qué comes conejito? -preguntó.<br>Hierba fresca,  ¿quieres un poco? -le respondió el conejito.<br>Muchas gracias, pero en realidad no se me antoja.<br>¿Qué comerás entonces?, -preguntó el conejito-. Te ves cansado y hambriento.<br>Supongo que moriré de hambre. -contestó Quetzalcóatl.<br>Sé que solo soy un conejo pequeño, pero puedes comerme y así sobrevivir.<br><br>El dios se acercó al conejo y lo acarició. Le había conmovido que ofreciera su vida para salvarlo. Lo tomó en sus brazos y lo levantó de tal forma que su silueta quedara grabada en la superficie de la luna.<br><br>Gracias a tu bondad, ahora serás recordado para la eternidad.<br><br>El conejo pudo ver su imagen plasmada en la brillante y enorme luna.<br><br>(Mundo Primaria, 2018)<br><img src='SI4E_B04_A03.png'>",
           "preguntasMultiples": true
        }
     },
    {  //Reactivo 4 
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El efecto del engaño del lobo fue perder la vida.",
              
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lobo no dio causas para ser sacrificado.",
     
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La causa de que el lobo perdiera la vida, fue que quiso engañar al pastor.",
             
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La moraleja de la fábula es: “Cuando engañamos a alguien, nosotros recibimos el daño”",
              
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La moraleja de la fábula es: “Las malas acciones no tienen efecto alguno.",
            
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "<b>Lee la siguiente fábula y elige falso (F) o verdadero (V) según corresponda.</b>", 
            "t11instruccion":"<center><strong>El lobo con piel de oveja</strong></center><br><br>Érase una vez un lobo que tenía mucha hambre. A lo lejos se encontraba un rebaño con apetitosas ovejas, pero estaba custodiado por su pastor. Intentó acercarse, pero sus intentos fueron en vano.<br><br>Un día cuando iba caminando por el bosque, se encontró una piel de oveja. Entonces, se le ocurrió utilizarla para poder acercarse al rebaño sin ser visto y escoger la oveja más grande para cenar.<br><br>Esa noche, el pastor fue al corral a buscar una oveja para alimentar a su familia. En la oscuridad, no se percató del disfraz del lobo y lo sacrificó para cocinarlo.<br><br>(Cuentos infantiles, 2018)<br><img src='SI4E_B04_A04.png'>",
            "descripcion": "Reactivos",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
  
   { //Reactivo 6 
       

 
      "respuestas": [
          {
              "t13respuesta": "SI4E_B04_A06_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "SI4E_B04_A06_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "SI4E_B04_A06_03.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "SI4E_B04_A06_04.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "SI4E_B04_A06_05.png",
              "t17correcta": "4",
              "columna":"0"
          },{
              "t13respuesta": "SI4E_B04_A06_06.png",
              "t17correcta": "5",
              "columna":"0"
          },{
              "t13respuesta": "SI4E_B04_A06_07.png",
              "t17correcta": "6",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p><b>Arrastra la respuesta correcta en los espacios en blanco.</b> <br><br>¿Cuál es la manera correcta de llenar este formulario?<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"SI4E_B04_A06_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "85,58", "cuadrado", "120, 50", ".","transparent"]},
          {"Contenedor": ["", "172,258", "cuadrado", "120, 50", ".","transparent"]},
         {"Contenedor": ["", "226,5", "cuadrado", "120, 50", ".","transparent"]},
          {"Contenedor": ["", "226,194", "cuadrado", "120, 50", ".","transparent"]},
          {"Contenedor": ["", "270,413", "cuadrado", "120, 50", ".","transparent"]},
          {"Contenedor": ["", "307,281", "cuadrado", "120, 50", ".","transparent"]},
          {"Contenedor": ["", "420,262", "cuadrado", "120, 50", ".","transparent"]},
      ]
  },
    {  //Reactivo 5
      "respuestas":[  
         {  
            "t13respuesta":     "Escribir mi dirección correctamente para que me llegue la información.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Escribir mi nombre completo con apellidos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Llenarlo con letra legible.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Verificar que la información que escribo sea correcta.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Escribir correctamente el pie de página.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Escribir mi nombre utilizando abreviaturas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Agregar una conclusión al final del texto.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Incluir las referencias del autor.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"<b>Selecciona todas las respuestas correctas para la pregunta.</b><br><br>¿Qué debo tomar en cuenta al llenar un formulario?",
      
      }
   },
];
 

  