json = [
    
    //1
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Francia.",
              "t17correcta":"1",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"Italia.",
              "t17correcta":"0",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"España.",
              "t17correcta":"0",
              "numeroPregunta": "0"
           },
           {  
              "t13respuesta":"1828, 1905.",
              "t17correcta":"1",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"1905, 1828.",
              "t17correcta":"0",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"1828, 1864.",
              "t17correcta":"0",
              "numeroPregunta": "1"
           },
           {  
              "t13respuesta":"Cinco semanas en globo.",
              "t17correcta":"1",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"Cinco semanas en la Luna.",
              "t17correcta":"0",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"Viaje al centro de la Tierra.",
              "t17correcta":"0",
              "numeroPregunta": "2"
           },
           {  
              "t13respuesta":"Porque predijo la aparición de grandes inventos.",
              "t17correcta":"1",
              "numeroPregunta": "3"
           },
           {  
              "t13respuesta":"Porque sus viajes le sirvieron de inspiración.",
              "t17correcta":"0",
              "numeroPregunta": "3"
           },
            {  
              "t13respuesta":"Porque sus obras hablan de grandes romances.",
              "t17correcta":"0",
              "numeroPregunta": "3"
           },
           {  
              "t13respuesta":"Aventuras y fantasía.",
              "t17correcta":"1",
              "numeroPregunta": "4"
           },
           {  
              "t13respuesta":"Historias románticas.",
              "t17correcta":"0",
              "numeroPregunta": "4"
           },
           {  
              "t13respuesta":"Catástrofes ambientales.",
              "t17correcta":"0",
              "numeroPregunta": "4"
           }
        ],

        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Lee el siguiente texto y selecciona la respuesta correcta.<br><br>¿En qué país nació Julio Verne?",
                         "Julio Verne nació en el año<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>y murió en<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
                         "¿Cuál es el nombre de su primer novela?",
                         "¿Por qué es considerado el padre de la ciencia ficción?",
                        "¿Cuáles son los temas de sus obras?"],
                         "preguntasMultiples": true,
           "t11instruccion" : "<center><strong>Julio Verne</strong></center><br><br>Escritor francés nacido en Nantes, el 8 de febrero de 1828. Fue el mayor de cinco hermanos, de una familia que pertenecía a la burguesía.  Su papá era abogado y fue él quien lo convenció para estudiar la misma carrera profesional. Una vez que terminó el doctorado en derecho, inició sus estudios en letras.<br><br>Su primera obra exitosa fue “Cinco semanas en globo” (1863), de ahí siguió “Viaje al centro de la Tierra” (1864) y posteriormente, “De la Tierra a la Luna” (1865), por mencionar algunas.<br><br>Sus novelas fueron reconocidas por narrar historias de aventura, fantasía y ciencia ficción. Se dice que siempre fue aventurero y realizaba grandes travesías por el mundo, lo cual le sirvió como fuente de inspiración e investigación para sus obras.<br><br>Es considerado el padre de la literatura de ciencia ficción, ya que en muchas de sus creaciones predijo, de manera muy exacta, la aparición de algunos avances tecnológicos como el helicóptero, el submarino,  el tren, los rascacielos y las naves espaciales.  Además, de ahí han surgido ideas para grandes producciones cinematográficas.<br><br>Julio Verne falleció en Amiens, el 24 de marzo de 1905."
           
        }
     },
     //2
     {
        "respuestas": [
            {
                "t13respuesta": "<p>biografía<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>vida<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>persona<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>breves<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>muerte<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>educación<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>contexto<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>relevante<\/p>",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>La <\/p>"
            },
            {
                "t11pregunta": "<p> es un género literario que narra la <\/p>"
            },
            {
                "t11pregunta": "<p> y obra de una determinada <\/p>"
            },
            {
                "t11pregunta": "<p>, Suelen ser <\/p>"
            },
            {
                "t11pregunta": "<p> y comienzan desde el nacimiento del individuo hasta su <\/p>"
            },
            {
                "t11pregunta": "<p> destacando detalles sobre su <\/p>"
            },
            {
                "t11pregunta": "<p>, viajes, logros, aportaciones, fracasos,<\/p>"
            },
            {
                "t11pregunta": "<p> histórico y cualquier otro hecho <\/p>"
            },
            {
                "t11pregunta": "<p> de la vida de esa persona.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el recuadro.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Hay sequía.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Comitán, Chiapas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "8 de marzo, 2018.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Pérdidas por sequía.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Es la mayor sequía en los últimos 15 años.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "No ha llovido en 30 días.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "¿Qué sucedió?"
            },
            {
                "t11pregunta": "¿En dónde?"
            },
            {
                "t11pregunta": "¿Cuándo sucedió?"
            },
            {
                "t11pregunta": "Título"
            },
            {
                "t11pregunta": "Subtítulo"
            },
            {
                "t11pregunta": "¿Por qué?"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Lee la siguiente nota periodística y relaciona las columnas según corresponda.",
            "t11instruccion": "<center><strong>Pérdidas por sequía</strong></center><br>Comitán, Chiapas (8 de marzo, 2018)  La ciudad de Comitán, ubicada en el estado de Chiapas, está sufriendo debido a que no llueve en la zona desde hace más de 30 días. Campesinos y agricultores se encuentran angustiados, pues han perdido gran parte de sus cosechas por la sequía.  “Las pérdidas ascienden a más de dos millones de pesos” -declaró un campesino del lugar.<br><br><strong>Es la mayor sequía en los últimos 15 años</strong><br><br>La Comisión de Medio ambiente declaró que desde hace 15 años no se presentaba una sequía de esa magnitud. Representantes del gobierno estatal acudieron a escuchar las peticiones de sus habitantes.  “No está en nuestras manos, no podemos controlar la naturaleza”, fueron las palabras del delegado."
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "<p><strong>¡Asaltan un banco con armas de juguete!</strong><\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>La mañana del 5 de noviembre del año en curso, tres individuos ingresaron a una sucursal de Bancomex ubicada en Av. Constitución.<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Se estima que el monto robado es de 5 millones de pesos.<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>Debido al suceso, la Dirección de Seguridad Pública se comprometió a enviar personal para vigilar la sucursal.<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos.<br><br>¿Cuál es el orden de la siguiente nota periodística?",
        }
    },
    //5
    {
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1"
        }
     
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La nota periodística se desarrolla de lo general a lo específico.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El titular de la nota periodística debe ser corto y llamativo.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La nota periodística no debe llevar subtítulos.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La entrada es el primer párrafo en el que se responden las preguntas:  ¿qué?, ¿cuándo?, ¿por qué?, ¿cómo?, ¿dónde? y ¿quién?",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En el cuerpo de la noticia se narra el acontecimiento en orden de importancia o relevancia.",
            "correcta"  : "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.", 
        "t11instruccion":"",
        "descripcion": "Reactivos",   
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable"  : true
    }        
    },

];
 

  