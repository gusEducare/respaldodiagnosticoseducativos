json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "La Luna se originó hace aproximadamente 4.5 millones de años, después de que un cuerpo de tamaño similar al de Mercurio chocó con la Tierra. ",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La Luna se originó hace aproximadamente 3.5 millones de años, después de que un cuerpo de tamaño similar al de Marte chocó con la Tierra.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La Luna se originó hace aproximadamente 4.5 millones de años, después de que un cuerpo de tamaño similar al de Marte chocó con la Tierra.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Los escombros del choque formaron la Luna y, después de 500 millones de años, el magma fundido se cristalizó y se formó la corteza lunar.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Los escombros del choque formaron la Luna y, después de 100 millones de años, el magma fundido se cristalizó y se formó la corteza lunar.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Los escombros del choque se fundieron y el magma se cristalizó para formar la Luna.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Lee atentamente el siguiente texto.<br><br><center><img src=\"SI4E_B02_A01_01.png\"></center><br><br>Selecciona la opción que corresponda a la respuesta correcta.<br><br>¿Hace cuántos años fue el origen de la luna y qué acontecimiento lo provocó?", "¿Cómo se formó la Luna?"],
            "preguntasMultiples": true,
            "t11instruccion": "<center><b>La Luna, el satélite de la tierra</b></center><br>La Luna es el satélite natural de la Tierra, el único que posee. Es un cuerpo celeste rocoso sin anillos y sin lunas, por supuesto. Existen varias teorías para explicar su formación, pero la más aceptada sugiere que su origen tuvo lugar hace unos 4.5 millones de años después de que un cuerpo de tamaño similar al de Marte chocó con la Tierra. De los escombros se formó la Luna y después de 100 millones de años el magma fundido se cristalizó y se formó la corteza lunar.<br><br>La Luna se encuentra a una distancia de la Tierra de aproximadamente 384,400 kilómetros. Después del Sol, es el cuerpo más brillante visto desde la superficie terrestre, aunque su superficie es en realidad oscura. Realiza una órbita completa alrededor de la Tierra en 27 días terrestres (27.322  días o 655.73 horas) y realiza el movimiento de rotación a la misma velocidad. Como se encuentra en rotación síncrona con la Tierra, la Luna presenta la misma cara a ésta. Gracias a la tecnología actual, se sabe que la “cara oculta” presenta cráteres, depresiones denominadas talasoides y ausencia de mares.<br><br>La observación de la Luna es tan antigua como la humanidad. Su nombre está presente en numerosas civilizaciones y al mismo tiempo forma parte de la mitología de ellas. Ejerce una influencia vital en los ciclos terrestres: modera el movimiento del planeta sobre su eje, por lo que permite que el clima sea relativamente estable. Además, es la causa de las mareas de la Tierra. Éstas ocurren gracias a la fuerza de atracción de gravedad, que, mientras de un lado tira fuertemente del agua, por el otro la mueve lejos de ella, provocando mareas altas y mareas bajas.<br><br>Todo un hito histórico fue la llegada del hombre a la Luna el 20 de julio de 1969. Doce astronautas estadounidenses de las misiones Apolo caminaron sobre la superficie lunar durante el período 1969-1972. Después de unos años de pausa, la exploración del satélite se reanudó en la década de 1990.<br><br>Fuente:<br>Geoenciclopedia (n.d). La Luna, el satélite de la Tierra: Geoenciclopedia. Disponible en: http://www.geoenciclopedia.com/luna/"
        }
    },
    // Reactivo 2
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Luna realiza media órbita alrededor de la Tierra en 27 días terrestres (27.322  días o 655.73 horas) y lleva a cabo el movimiento de rotación a la misma velocidad.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La distancia de la Luna a la Tierra es de aproximadamente 384,400 kilómetros.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Luna no afecta la estabilidad del clima de la Tierra.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Después del Sol, la Luna es el cuerpo más brillante visto desde la superficie terrestre, aunque su superficie es en realidad oscura.",
                "correcta": "1"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "De acuerdo con el texto “La Luna, el satélite de la tierra”, selecciona falso o verdadero según corresponda.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false,
            "t11instruccion": "<center><b>La Luna, el satélite de la tierra</b></center><br>La Luna es el satélite natural de la Tierra, el único que posee. Es un cuerpo celeste rocoso sin anillos y sin lunas, por supuesto. Existen varias teorías para explicar su formación, pero la más aceptada sugiere que su origen tuvo lugar hace unos 4.5 millones de años después de que un cuerpo de tamaño similar al de Marte chocó con la Tierra. De los escombros se formó la Luna y después de 100 millones de años el magma fundido se cristalizó y se formó la corteza lunar.<br><br>La Luna se encuentra a una distancia de la Tierra de aproximadamente 384,400 kilómetros. Después del Sol, es el cuerpo más brillante visto desde la superficie terrestre, aunque su superficie es en realidad oscura. Realiza una órbita completa alrededor de la Tierra en 27 días terrestres (27.322  días o 655.73 horas) y realiza el movimiento de rotación a la misma velocidad. Como se encuentra en rotación síncrona con la Tierra, la Luna presenta la misma cara a ésta. Gracias a la tecnología actual, se sabe que la “cara oculta” presenta cráteres, depresiones denominadas talasoides y ausencia de mares.<br><br>La observación de la Luna es tan antigua como la humanidad. Su nombre está presente en numerosas civilizaciones y al mismo tiempo forma parte de la mitología de ellas. Ejerce una influencia vital en los ciclos terrestres: modera el movimiento del planeta sobre su eje, por lo que permite que el clima sea relativamente estable. Además, es la causa de las mareas de la Tierra. Éstas ocurren gracias a la fuerza de atracción de gravedad, que, mientras de un lado tira fuertemente del agua, por el otro la mueve lejos de ella, provocando mareas altas y mareas bajas.<br><br>Todo un hito histórico fue la llegada del hombre a la Luna el 20 de julio de 1969. Doce astronautas estadounidenses de las misiones Apolo caminaron sobre la superficie lunar durante el período 1969-1972. Después de unos años de pausa, la exploración del satélite se reanudó en la década de 1990.<br><br>Fuente:<br>Geoenciclopedia (n.d). La Luna, el satélite de la Tierra: Geoenciclopedia. Disponible en: http://www.geoenciclopedia.com/luna/"
        }
    },
    // Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "El tema está delimitado y es preciso. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Desarrolla aspectos poco relevantes sobre un tema.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Es objetivo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "La investigación está fundamentada con diferentes fuentes bibliográficas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El autor expresa su opinión sobre el tema.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La redacción contiene un lenguaje claro.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No es necesario citar las fuentes.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona de la siguiente lista las características que correspondan a un texto monográfico."
        }
    },
    // Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "<p>Cultura maya<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Ubicación geográfica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Organización social<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Religión<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Aportaciones<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Bibliografía<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [{
                "t11pregunta": "<center>"
            },
            {
                "t11pregunta": "</center><p>La cultura maya fue una civilización precolombina mesoamericana que surgió durante el periodo clásico (2000 a.C. a 250 d.C.).<\/p><br><br>"
            },
            {
                "t11pregunta": "<br><p>Los mayas vivieron en el sur y sureste de México (que corresponde a los estados de Yucatán, Campeche, tabasco, Quintana Roo y la zona oriental de Chiapas) y en partes de Guatemala, Honduras y Belice.<\/p><br><br>"
            },
            {
                "t11pregunta": "<br><p>Estaba dividida en tres clases sociales:<br>1.- La clase alta: conformada por los gobernantes y sus familiares, los funcionarios de alta jerarquía y los ricos comerciantes.<br>2.- Funcionarios públicos y los trabajadores especializados.<br>3.- Campesinos, obreros y prisioneros de guerra (esclavos).<\/p><br><br>"
            },
            {
                "t11pregunta": "<br><p>Se consideraba panteísta, ya que adoraban a la naturaleza y los fenómenos atmosféricos; y politeísta porque tenían varios dioses.<\/p><br><br>"
            },
            {
                "t11pregunta": "<br><p>En matemáticas, los mayas desarrollaron un sistema de numeración base 20 y crearon el concepto del cero. En arquitectura, construyeron grandes ciudades como Chichen Itzá, Tikal y Uxmal, que actualmente son sitios arqueológicos de gran importancia.<\/p><br><br>"
            },
            {
                "t11pregunta": "<br><p>Gonzales, A. (2010/ 01/ 01). Historia Universal. Cultura maya: Historia cultural. Disponible en:<br>http://www.historiacultural.com/2010/01/cultura-maya-precolombina-mesoamerica.html<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Lee el texto monográfico que se presenta a continuación y arrastra las palabras o frases que correspondan al título de cada párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "<p>indígena<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>palabras<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>ofensiva<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>hablan<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>náhuatl<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>idiomas<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>sur<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Hidalgo<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>región<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>Maíz<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [{
                "t11pregunta": "<b>Del nombre</b><br>Los pames se llaman a sí mismos Xi úi (o sus variantes locales), que significa \""
            },
            {
                "t11pregunta": "\"; término que se utiliza para referirse a toda persona descendiente de no-mestiza. Es por eso que, las "
            },
            {
                "t11pregunta": " pame y xi'úi no son gentilicios. Sólo cuando hablan en español emplean la palabra pame para autonombrarse.Sin embargo, en la región dicha palabra se considera "
            },
            {
                "t11pregunta": ", por eso tratan de evitarla.<br><br><b>Lengua</b><br>En la actual región Xi'úi, además del español, "
            },
            {
                "t11pregunta": " su propio idioma, llamado pame. Cerca de ellos, hacia el oriente, se encuentran grupos de habla "
            },
            {
                "t11pregunta": " y huasteca, además de la población indígena que ha emigrado a la región Xi'úi y que habla sus propias lenguas. Existen al menos dos "
            },
            {
                "t11pregunta": " pame: pame del norte y pame del "
            },
            {
                "t11pregunta": " , éste último aparentemente ha desaparecido a pesar de que a principios del siglo XX se hablaba en el estado de "
            },
            {
                "t11pregunta": ". El pame del norte, que es el que se habla actualmente en toda la "
            },
            {
                "t11pregunta": " Xi'úi, se divide en dos variantes: el de las áreas de Ciudad del "
            },
            {
                "t11pregunta": ", Alaquines y La Palma, y el del área de Santa María Acapulco.<br><br>Fuente:<br>Comisión Nacional para el Desarrollo de los Pueblos Indígenas (2017/06/14). Etnografía de los pames de San Luis Potosí. México: www.gob.mx. Disponible en: https://goo.gl/itD2ph"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Lee el siguiente fragmento del texto monográfico “Los Pames de San Luis Potosí (Xi úi)”y arrastra las palabras donde corresponda para completar los párrafos.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "<p>Título</p>",
                "t17correcta": "0",
                "etiqueta": "paso 1"
            },
            {
                "t13respuesta": "<p>Antecedentes históricos<\/p>",
                "t17correcta": "1",
                "etiqueta": "paso 2"
            },
            {
                "t13respuesta": "<p>Organización social<\/p>",
                "t17correcta": "2",
                "etiqueta": "paso 3"
            },
            {
                "t13respuesta": "<p>Conclusión<\/p>",
                "t17correcta": "3",
                "etiqueta": "paso 4"
            },
            {
                "t13respuesta": "<p>Bibliografía<\/p>",
                "t17correcta": "4",
                "etiqueta": "paso 5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos para formar la secuencia lógica de un texto monográfico.",
        }
    },
    // Reactivo 7
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los pueblos indígenas son motivo de orgullo, ya que son los representantes de los pobladores prehispánicos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los pueblos prehispánicos representan a las civilizaciones que dieron origen a grandes manifestaciones culturales.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los pueblos indígenas disfrutan de los beneficios sociales y económicos que el resto de la población.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los pueblos indígenas suelen sufrir discriminación.",
                "correcta": "1"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona falso o verdadero según corresponda",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "La cultura purépecha vivía al noroeste de Michoacán, en lo que ahora conocemos como Uruapan y Pátzcuaro.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se basaba en la agricultura, la caza y la recolección.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Orfebrería, arte lapidario y cerámica.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Construyeron estructuras piramidales  llamadas yácatas.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "El Cazonci o Irecha era su principal autoridad política.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Eran politeístas, adoraban al universo de tres partes: cielo, la tierra y el inframundo.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Localización"
            },
            {
                "t11pregunta": "Economía"
            },
            {
                "t11pregunta": "Artesanías"
            },
            {
                "t11pregunta": "Arquitectura"
            },
            {
                "t11pregunta": "Organización política"
            },
            {
                "t11pregunta": "Religión"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada concepto con la descripción que corresponda."
        }
    },
    // Reactivo 9
    {
        "respuestas": [{
                "t13respuesta": "<p>pocas palabras bastan.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>a aullar se enseña.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>hasta que se rompe.<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>y te diré de qué careces.<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>buena cara.<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>jamás su rama endereza.<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Al buen entendedor, "
            },
            {
                "t11pregunta": "<br>El que con lobos anda, "
            },
            {
                "t11pregunta": "<br>Tanto va el cántaro al agua, "
            },
            {
                "t11pregunta": "<br>Dime de qué presumes, "
            },
            {
                "t11pregunta": "<br>Al mal tiempo, "
            },
            {
                "t11pregunta": "<br>Árbol que nace torcido, "
            },
            {
                "t11pregunta": ""
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la frase que complete cada uno de los siguientes refranes.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 10
    {
        "respuestas": [{
                "t13respuesta": "Mantén la boca cerrada si hay moscas volando.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "En ciertas ocasiones, es mejor guardar silencio.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Siempre sé el primero en opinar.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Satisfacer el apetito nos hace sentir bien.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Si no comes, siempre estarás contento.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Es importante ser feliz.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Siempre debemos hacer el bien.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Todo lo bueno trae algo malo para compensarse.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Cuando algo malo sucede, siempre trae algo bueno.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Me cansé de bailar.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Aun cuando las consecuencias son negativas, lo que se disfrutó al momento las hace más llevaderas.",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Si vas a una fiesta, debes bailar toda la noche.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la oración que explique mejor cada uno de los siguientes refranes.<br><br>En boca cerrada no entran moscas.", "Barriga llena corazón contento", "No hay mal que por bien no venga.", "Lo bailado nadie me lo quita."],
            "preguntasMultiples": true
        }
    },
    // Reactivo 11
    {
        "respuestas": [{
                "t13respuesta": "Metáforas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Ironías",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Analogías",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Anáforas",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Metáforas",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Hipérboles",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Recurso literario basado en establecer relaciones basadas en las semejanzas o comparaciones entre dos ideas.", "Recurso literario en el que se comparan dos conceptos sin relación aparente y para hacerlo, se le transfieren propiedades de un elemento a otro."],
            "preguntasMultiples": true
        }
    },
    // Reactivo 12
    {
        "respuestas": [{
                "t13respuesta": "Metáfora",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Analogía",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A caballo regalado, no se le mira el diente.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando el río suena, es porque agua lleva.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El que con lobos anda, a aullar se enseña.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Más vale ser cabeza de ratón, que cola de león.",
                "correcta": "1"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona metáfora o analogía según el recurso literario usado en cada uno de los siguientes refranes.",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false
        }
    },
    // Reactivo 13
    {
        "respuestas": [{
                "t13respuesta": "directora",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "pequeña",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "grandes",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "bienvenida",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "asombroso",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "silencio",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "caluroso",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "perfectamente",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "extraño",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "asustada",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "nueva",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "platicó",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "azules",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "rasgados",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "hermosa",
                "t17correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee atentamente el siguiente texto.<br><br>Selecciona todos los adjetivos que aparezcan en el texto “Mi amiga de China”.",
            "t11instruccion": "<center><b>Mi amiga de China</b></center><br>Recuerdo que ese caluroso día de escuela estábamos estudiando sobre las grandes ballenas azules, cuando de pronto, la directora entró al salón.  La acompañaba una pequeña niña de ojos rasgados, que se veía un poco asustada.  La directora comenzó a hablar pausadamente y nos presentó a nuestra nueva compañera.  Todos volteamos a verla mientras permanecíamos en silencio. Ella se presentó y nos dijo su extraño nombre: Yuga. Hablaba español perfectamente y nos platicó sobre sobre su asombroso país y su hermosa cultura.  De inmediato, todos nos acercamos a platicar con ella, era la niña nueva y queríamos hacerla sentir bienvenida."
        }
    },
    // Reactivo 14
    {
        "respuestas": [{
                "t13respuesta": "<p>Anoche<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>mucho<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Últimamente<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>cerca<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>mejor<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "1. "
            },
            {
                "t11pregunta": " no pude dormir, había un mosquito en mi habitación.<br>2. Me espantaste "
            },
            {
                "t11pregunta": ", por favor no lo vuelvas a hacer.<br>3. "
            },
            {
                "t11pregunta": ", me da mucha hambre en la escuela.<br>4. El hotel está "
            },
            {
                "t11pregunta": " de la zona turística.<br>5. Hoy amanecí "
            },
            {
                "t11pregunta": " que ayer."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde corresponda para completar las oraciones.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 15
    {
        "respuestas": [{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El texto anterior es un instructivo, ya que cuenta todas las características de uno y muestra el procedimiento para realizar galletas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El texto no es un instructivo, ya que no presenta información precisa sobre el procedimiento.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee con atención la siguiente receta.<br><br>Selecciona falso o verdadero según corresponda",
            "descripcion": "",
            "evaluable": false,
            "evidencio": false,
            "t11instruccion": "<center><b>Galletas “besos de nuez”</b></center><br>Triturar las nueces. Aparte, en una batidora poner la mantequilla con azúcar glas y batir hasta conseguir una masa esponjosa. Agregar la harina, la vainilla, y la sal, y volver a batir hasta que todo esté mezclado. Incorporar el resto del harina y las nueces molidas. Formar una bola con la masa, meterla en una bolsa de plástico y dejarla en el refrigerador hasta que esté firme.<br><br>Calentar el horno. Hacer bolitas con la masa, colocarlas en una charola y ponerlas a hornear. Transcurrido el tiempo de cocción, dejarlas reposar y espolvorear azúcar  glas."
        }
    },
    // Reactivo 16
    {
        "respuestas": [{
                "t13respuesta": "El texto se apoya de imágenes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Los procedimientos están numerados.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Emplea tecnicismos para mayor especificidad.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Utiliza palabras específicas que indican los pasos a seguir.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Emplea el modo subjuntivo del verbo.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Utiliza verbos modo imperativo e infinitivo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No es necesaria la inclusión de imágenes.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El lenguaje es sencillo y directo.",
                "t17correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona de la siguiente lista todos los elementos que correspondan a los textos instructivos."
        }
    },
    // Reactivo 17
    {
        "respuestas": [{
                "t13respuesta": "Formar",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Ata",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Unir",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Amarra",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Obtener",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Pega",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Volar",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Marca",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Formar",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Átalas",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Obtener",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Une",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Volar",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Pega",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Amarrar",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Haz",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Cortas",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Subes",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Busca",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas de las siguientes preguntas.<br><br>Son verbos en infinitivo que aparecen en “Cómo armar un papalote”",
                "Son verbos en imperativo que aparecen en “Cómo armar un papalote”"
            ],
            "preguntasMultiples": true,
            "t11instruccion": "<center><b>Instructivo para armar un papalote</b></center><br><b>Materiales necesarios:</b><br>2 varillas de madera<br>Cintas de papel de 2 centímetros de ancho<br>Papel de china<br>Hilo<br>1 bolsa de plástico<br><br><b>Procedimiento:</b><br>1. Primero, marca en una de las varillas 1/3 de la longitud y la otra a la mitad y  átalas con hilo para formar una cruz.<br>2. Después, une cada extremo de los palos para formar un diamante.<br>3. Posteriormente, pega las varillas al papel con cinta adhesiva, y después coloca cinta para unir las puntas de las varillas por encima del hilo y así obtener un perímetro reforzado.<br>4. Para formar los tiros corta dos trozos de 1,5 metros de hilo y átalos de extremo a extremo de la varilla vertical. Haz lo mismo con la varilla horizontal.<br>5. Ata tiras de plástico para formar la cola y amárralas del extremo inferior del papalote.<br>6. Ata el hilo de vuelo en la intersección de ambos tiros.<br>7. Finalmente, busca el momento adecuado y el viento para volar tu papalote."
        }
    },
    // Reactivo 18
    {
        "respuestas": [{
                "t13respuesta": "<p>Ve al supermercado a comprar los ingredientes.</p>",
                "t17correcta": "0",
                "etiqueta": "paso 1"
            },
            {
                "t13respuesta": "<p>Mezcla los ingredientes.<\/p>",
                "t17correcta": "1",
                "etiqueta": "paso 2"
            },
            {
                "t13respuesta": "<p>Precalienta el horno a 200 ºC.<\/p>",
                "t17correcta": "2",
                "etiqueta": "paso 3"
            },
            {
                "t13respuesta": "<p>Hornea el pastel por 45 minutos.<\/p>",
                "t17correcta": "3",
                "etiqueta": "paso 4"
            },
            {
                "t13respuesta": "<p>Saca el pastel del molde.<\/p>",
                "t17correcta": "4",
                "etiqueta": "paso 5"
            },
            {
                "t13respuesta": "<p>Decora el pastel a tu gusto.<\/p>",
                "t17correcta": "5",
                "etiqueta": "paso 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los pasos a seguir para hacer un pastel.",
        }
    },
    // Reactivo 19
    {
        "respuestas": [{
                "t13respuesta": "<p>Primero<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Después<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>posteriormente<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Finalmente<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>por último<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": ""
            },
            {
                "t11pregunta": " consigue todos los ingredientes en el supermercado. "
            },
            {
                "t11pregunta": " haz la mezcla y "
            },
            {
                "t11pregunta": " mételo al horno por 45 minutos. "
            },
            {
                "t11pregunta": ", sácalo del molde y "
            },
            {
                "t11pregunta": ", decóralo a tu gusto."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras al lugar que corresponda para ordenar el siguiente instructivo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    // Reactivo 20
    {
        "respuestas": [{
                "t13respuesta": "centímetro",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "kilómetro",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "metro",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "litro",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "mililitro",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "cm"
            },
            {
                "t11pregunta": "km"
            },
            {
                "t11pregunta": "m"
            },
            {
                "t11pregunta": "l"
            },
            {
                "t11pregunta": "ml"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada palabra con el símbolo que la representa."
        }
    },
    // Reactivo 21
    {
        "respuestas": [{
                "t13respuesta": "<p>gramos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>toneladas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>litros<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>miligramos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>metros cúbicos<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Para hacer un panqué necesitas 350 "
            },
            {
                "t11pregunta": " de harina.<br>El camión pesa más de 2 "
            },
            {
                "t11pregunta": ".<br>Mi mamá me encargó comprar 2 "
            },
            {
                "t11pregunta": " de leche.<br>Una hormiga pesa aproximadamente 3 "
            },
            {
                "t11pregunta": ".<br>El tinaco tiene una capacidad de 10 "
            },
            {
                "t11pregunta": "."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la palabra donde corresponda para completar cada oración.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    }
];