json=[ 
    //reactivo 1  
    { 
        "respuestas": [ 
            { 
                "t13respuesta": "F", 
                "t17correcta": "0" 
            }, 
            { 
                "t13respuesta": "V", 
                "t17correcta": "1" 
            } 
        ], 
        "preguntas" : [ 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "La entrevista tiene como objetivo brindar información acerca del Campeonato Juvenil Canadiense de Squash.", 
                "correcta"  : "0" 
            }, 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "El texto es un ejemplo de entrevista psicológica.", 
                "correcta"  : "1" 
            }, 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "En esta entrevista se plantean preguntas abiertas.", 
                "correcta"  : "1" 
            }, 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "Es evidente que el entrevistador se preparó para la entrevista.", 
                "correcta"  : "1" 
            }, 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "Al planear una entrevista, no es necesario investigar acerca del tema.", 
                "correcta"  : "0" 
            }, 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "En la presentación de esta entrevista se narra el motivo por el cual se realizará y se brinda información de la entrevistada.", 
                "correcta"  : "1" 
            }, 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "El texto anterior no contiene el cuerpo de la entrevista.", 
                "correcta"  : "0" 
            }, 
            { 
                "c03id_tipo_pregunta": "13", 
                "t11pregunta": "Durante el cierre de la entrevista, se agradece al entrevistado y se hace una breve conclusión.", 
                "correcta"  : "1" 
            } 
        ], 
        "pregunta": { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "<p>Lee la siguiente entrevista y elige falso (F) o verdadero (V) según corresponda.<\/p>", 
            "t11instruccion": "<center><b>Una campeona muy joven</center></b><br>Ella es Ana Fernanda Ojeda Mireles. Nació en Acámbaro, Guanajuato, y a sus 10 años de edad es campeona nacional de squash.  El próximo 13 de diciembre viajará a la ciudad de Toronto, Canadá, para representar a México en el Campeonato Juvenil Canadiense de Squash. Ana Fer, como le dicen sus papás y amigos, nos platicará un poco acerca de esta interesante experiencia.<br></br><i>Buenos días Ana Fer, platícanos,  ¿cómo empezaste a jugar squash?</i><br><br>Hola. Siempre me gustó hacer deporte, de hecho, juego basquetbol desde los 6 años. Un día, durante mi entrenamiento en el deportivo de Acámbaro, vi un anuncio de clases de squash y me llamó la atención. Mi papá me inscribió, me gustó mucho y seguí practicando.<br><br><i>¿Cómo haces para integrar el deporte con tus responsabilidades de la escuela?</i><br><br>A veces no es sencillo. Voy en 4to de primaria y tengo que hacer tareas y estudiar para los exámenes. Siempre me ha gustado sacar buenas calificaciones. Cuando regreso de la escuela, voy a comer a casa de mi abuelita, hago mi tarea y me preparo para irme a entrenar. En la noche regreso a mi casa, muy cansada, a bañarme y a dormir. Prácticamente, así es toda la semana y los fines de semana son los partidos.<br><br><i>A esta competencia van únicamente dos mexicanas en tu categoría, ¿qué sientes al haber sido elegida para representar al país en un evento de esa magnitud?</i><br><br>Me llena de emoción poder ir tan lejos y representar a mi país, aunque también estoy muy nerviosa. La otra una niña, que es de Veracruz y yo, competiremos contra niñas de India, Egipto, Nueva Zelanda, Pakistán, Inglaterra, Perú, Colombia, etc.<br><br><i>Tus papás también practican deporte, ¿qué te dijeron cuando se enteraron que irías a Canadá a competir?</i><br><br>Les dio mucho gusto. Siempre me apoyan llevándome a los entrenamientos y partidos.  Mi papá va a viajar conmigo, él también juega squash y todos los domingos jugamos juntos.<br><br><i>Para finalizar, ¿qué aconsejarías a los niños que quisieran practicar algún deporte y competir fuera de México?</i><br><br>Que nunca se rindan. No es fácil y a veces te gana el cansancio, pero bien vale la pena. Y a los que no hacen deporte, les diría que dejen los celulares y videojuegos por un momento, se paren del sillón y se atrevan a hacerlo. Es una experiencia que te da grandes satisfacciones.<br><br><i>Excelente recomendación Ana Fer. Muchas gracias por tu tiempo.</i><br><br>Como podrán ver, Ana Fernanda Ojeda es un ejemplo de disciplina y constancia, tanto en el ámbito deportivo como escolar. Estamos seguros que será una gran representante de la juventud mexicana y que esta competencia significa el inicio de una brillante carrera deportiva. ¡Éxito Ana Fer!", 
            "descripcion": "",    
            "variante": "editable", 
            "anchoColumnaPreguntas": 50, 
            "evaluable"  : true 
        }         
    }, 
    //reactivo 2 
     { 
        "respuestas": [ 
            { 
                "t13respuesta": "Amistad", 
                "t17correcta": "1" 
            }, 
            { 
                "t13respuesta": "Admiración", 
                "t17correcta": "2" 
            }, 
            { 
                "t13respuesta": "Amor", 
                "t17correcta": "3" 
            }, 
            { 
                "t13respuesta": "Tristeza", 
                "t17correcta": "4" 
            } 
        ], 
        "preguntas": [ 
            { 
                "t11pregunta": "Un simple abrazo nos enternece el corazón; nos da la bienvenida y nos hace más llevadera la vida.(Neruda, 1959)" 
            }, 
            { 
                "t11pregunta": "Las mujeres tienen fuerzas que asombran a los hombres. Ellas cargan niños, penas, y cosas pesadas; sin embargo, tienen espacio para la felicidad, el amor y la alegría. (Mistral, 1950)" 
            }, 
            { 
                "t11pregunta": "Quiero querer con música. Y quiero que me quieran con tono verdadero Casi en azul y casi eternamente. (García Lorca,1945)" 
            }, 
            { 
                "t11pregunta": "Me doy cuenta de que me faltas y de que te busco entre las gentes, en el ruido, pero todo es inútil. Cuando me quedo solo me quedo más solo solo por todas partes y por ti y por mí. (Sabines, 1961)" 
            } 
        ], 
        "pregunta": { 
            "c03id_tipo_pregunta": "12", 
             "t11pregunta": "Relaciona cada sentimiento con el fragmento que lo expresa." 
             
        } 
    }, 
    //reactivo 3 
    {   
      "respuestas":[ 
         {   
            "t13respuesta": "El cielo", 
            "t17correcta": "1", 
            "numeroPregunta":"0" 
         }, 
         {   
            "t13respuesta": "El mar", 
            "t17correcta": "0", 
            "numeroPregunta":"0" 
         }, 
         {   
            "t13respuesta": "El bosque", 
            "t17correcta": "0", 
            "numeroPregunta":"0" 
         },  
         {   
            "t13respuesta": "La tierra", 
            "t17correcta": "0", 
            "numeroPregunta":"0" 
         }, 
         {   
            "t13respuesta": "Las estrellas", 
            "t17correcta": "1", 
            "numeroPregunta":"1" 
         }, 
         {   
            "t13respuesta": "Sus antepasados", 
            "t17correcta": "0", 
            "numeroPregunta":"1" 
         }, 
         {   
            "t13respuesta": "Un ejército", 
            "t17correcta": "0", 
            "numeroPregunta":"1" 
         }, 
         {   
            "t13respuesta": "Los árboles", 
            "t17correcta": "0", 
            "numeroPregunta":"1" 
         }, 
          {   
            "t13respuesta": "El autor se pregunta si el árbol lo verá cuando esté muerto.", 
            "t17correcta": "1", 
            "numeroPregunta":"2" 
         }, 
         {   
            "t13respuesta": "El autor piensa plantar un árbol.", 
            "t17correcta": "0", 
            "numeroPregunta":"2" 
         }, 
         {   
            "t13respuesta": "Su corazón siempre estará en esa tierra.", 
            "t17correcta": "0", 
            "numeroPregunta":"2" 
         },  
         {   
            "t13respuesta": "El árbol toca su corazón.", 
            "t17correcta": "0", 
            "numeroPregunta":"2" 
         }, 
         {   
            "t13respuesta": "Flechas", 
            "t17correcta": "1", 
            "numeroPregunta":"3" 
         }, 
         {   
            "t13respuesta": "Guerreros", 
            "t17correcta": "0", 
            "numeroPregunta":"3" 
         }, 
         {   
            "t13respuesta": "Estrellas", 
            "t17correcta": "0", 
            "numeroPregunta":"3" 
         }, 
         {   
            "t13respuesta": "Raíces", 
            "t17correcta": "0", 
            "numeroPregunta":"3" 
         }, 
         {   
            "t13respuesta": "Metáfora", 
            "t17correcta": "1", 
            "numeroPregunta":"4" 
         }, 
         {   
            "t13respuesta": "Analogía", 
            "t17correcta": "0", 
            "numeroPregunta":"4" 
         }, 
         {   
            "t13respuesta": "Alegoría", 
            "t17correcta": "0", 
            "numeroPregunta":"4" 
         }, 
         {   
            "t13respuesta": "Personificación", 
            "t17correcta": "0", 
            "numeroPregunta":"4" 
         } 
      ], 
      "pregunta":{   
         "c03id_tipo_pregunta":"1", 
         "t11pregunta": ["Lee el siguiente poema y selecciona la respuesta correcta.<br>En la oración: ¿Habéis sido flechas caídas del azul?, la frase “del azul”, se refiere a:","Cuando el autor habla de guerreros, se refiere a:","El enunciado “¿Conocerán vuestras raíces toscas mi corazón en tierra?” significa que:","El autor compara los árboles con:","¿Qué figura retórica utiliza el autor en el siguiente fragmento?<br>¡Árboles!<br>¿Habéis sido flechas <br>caídas del azul?"], 
         "preguntasMultiples": true, 
         "t11instruccion": "<center><b>Árboles<br></b>Federico García Lorca</center><br>¡Árboles!<br>¿Habéis sido flechas<br>caídas del azul?<br>¿Qué terribles guerreros os lanzaron?<br>¿Han sido las estrellas?<br><br>Vuestras músicas vienen del alma de los pájaros, <br>de los ojos de Dios,<br>de la pasión perfecta.<br>¡Árboles!<br>¿Conocerán vuestras raíces toscas<br>mi corazón en tierra?<br><br>Recuperado de: Ciudadseva.com. (2017). Árboles - Federico García Lorca - Ciudad Seva - Luis López Nieves. [online] Available at: http://ciudadseva.com/texto/arboles/ [Accessed 5 Dec. 2017]." 
      } 
   },
   //reactivo 4
    {
        "respuestas": [ 
            { 
                "t13respuesta": "<p><img src='SI4E_B03_A04_02.png'><\/p>", 
                "t17correcta": "0", 
                "etiqueta":"1" 
            }, 
            { 
                "t13respuesta": "<p><img src='SI4E_B03_A04_03.png'><\/p>", 
                "t17correcta": "1", 
                "etiqueta":"2" 
            }, 
            { 
                "t13respuesta": "<p><img src='SI4E_B03_A04_04.png'><\/p>", 
                "t17correcta": "2", 
                "etiqueta":"3" 
            }, 
            { 
                "t13respuesta": "<p><img src='SI4E_B03_A04_05.png'><\/p>", 
                "t17correcta": "3", 
                "etiqueta":"4" 
            }, 
            { 
                "t13respuesta": "<p><img src='SI4E_B03_A04_06.png'><\/p>", 
                "t17correcta": "4", 
                "etiqueta":"5" 
            } 
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra la imagen al espacio que corresponda.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url":"SI4E_B03_A04_01.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":false
            
        },
        "contenedores": [
            {"Contenedor": ["", "216,237", "cuadrado", "40, 40", ".","transparent"]},//1
            {"Contenedor": ["", "329,322", "cuadrado", "40, 40", ".","transparent"]},//
            {"Contenedor": ["", "226,332", "cuadrado", "40, 40", ".","transparent"]},//3
            {"Contenedor": ["", "52,157", "cuadrado", "40, 40", ".","transparent"]},//4
            {"Contenedor": ["", "226,562", "cuadrado", "40, 40", ".","transparent"]},//5
            
        ]
    },
    //reactivo 5
    {  
      "respuestas":[  
         {  
            "t13respuesta": "El consumo responsable nos ayuda a dejar de gastar en cosas innecesarias.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta": "Analizar los anuncios publicitarios ayuda a evaluar si la información que contienen es cierta o no.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Al consumir ciertos productos se puede contribuir al cuidado del medio ambiente.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Evaluar diferentes productos nos permite tomar mejores decisiones de compra.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"El consumo de productos ayuda a tener muchos amigos y a estar siempre a la moda.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Consumir productos caros contribuye a tener una mejor imagen ante los demás y en consecuencia más poder.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Para decidir qué producto comprar hay que identificar qué marca tiene más anuncios.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La fórmula para un consumo responsable consiste en siempre comprar los productos más costosos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"<p>Selecciona todas las ventajas de un consumo responsable.</p>"
      }
   }, 
    //reactivo 6 
    {   
      "respuestas":[ 
         {   
            "t13respuesta": "A jóvenes.", 
            "t17correcta": "1", 
            "numeroPregunta":"0" 
         }, 
         {   
            "t13respuesta": "A hombres de negocios.", 
            "t17correcta": "0", 
            "numeroPregunta":"0" 
         }, 
         {   
            "t13respuesta": "A familias que les gusta ir a la playa.", 
            "t17correcta": "0", 
            "numeroPregunta":"0" 
         }, 
         {   
            "t13respuesta": "Necesitas obtener Motophone para ser feliz.", 
            "t17correcta": "1", 
            "numeroPregunta":"1" 
         }, 
         {   
            "t13respuesta": "Lo más importante es poder comunicarse.", 
            "t17correcta": "0", 
            "numeroPregunta":"1" 
         }, 
         {   
            "t13respuesta": "Con ese teléfono llamarás la atención de todos.", 
            "t17correcta": "0", 
            "numeroPregunta":"1" 
         }, 
          {   
            "t13respuesta": "Analizar si cubre tus necesidades.", 
            "t17correcta": "1", 
            "numeroPregunta":"2" 
         }, 
         {   
            "t13respuesta": "Revisar si se trata de una oferta.", 
            "t17correcta": "0", 
            "numeroPregunta":"2" 
         }, 
         {   
            "t13respuesta": "Asegurarse de que esté a la moda.", 
            "t17correcta": "0", 
            "numeroPregunta":"2" 
         }, 
         {   
            "t13respuesta": "Con ese teléfono, llamarás la atención.", 
            "t17correcta": "1", 
            "numeroPregunta":"3" 
         }, 
         {   
            "t13respuesta": "El teléfono tiene todo al mejor precio.", 
            "t17correcta": "0", 
            "numeroPregunta":"3" 
         }, 
         {   
            "t13respuesta": "El teléfono posee tecnología de vanguardia.", 
            "t17correcta": "0", 
            "numeroPregunta":"3" 
         }, 
         {   
            "t13respuesta": "Porque tendría un teléfono resistente y con el que puede estar comunicada.", 
            "t17correcta": "1", 
            "numeroPregunta":"4" 
         }, 
         {   
            "t13respuesta": "Porque así tendría más amigos.", 
            "t17correcta": "0", 
            "numeroPregunta":"4" 
         }, 
         {   
            "t13respuesta": "Porque llamaría la atención de las personas.", 
            "t17correcta": "0", 
            "numeroPregunta":"4" 
         }, 
         {   
            "t13respuesta": "Porque tendría un teléfono con alta tecnología.", 
            "t17correcta": "1", 
            "numeroPregunta":"5" 
         }, 
         {   
            "t13respuesta": "Porque con ese teléfono podría ir a la playa con sus amigas.", 
            "t17correcta": "0", 
            "numeroPregunta":"5" 
         }, 
         {   
            "t13respuesta": "Porque tendría que pagar un costo extra para poder comunicarse.", 
            "t17correcta": "0", 
            "numeroPregunta":"5" 
         }
      ], 
      "pregunta":{   
         "c03id_tipo_pregunta":"1", 
         "t11pregunta": ["Observa los siguientes anuncios y selecciona la respuesta correcta.<br><center><img src='SI4E_B03_A06_01.png'><br><img src='SI4E_B03_A06_02.png'></center>¿A qué público está dirigido el anuncio de Motophone?","¿Qué mensaje transmite el eslogan de Motophone?","Antes de obtener un producto, es importante:","¿Qué mensaje transmite el eslogan de Cheaphone?","¿Por qué Ana debería comprar Cheaphone?","¿Por qué Ana debería comprar Motophone?"], 
         "preguntasMultiples": true, 
         "t11instruccion": "Ana tiene un teléfono que utiliza para comunicarse con su familia y amigos.  Un día, mientras jugaba durante el recreo, se le cayó el celular al piso y la pantalla se rompió.  Afortunadamente, tiene el dinero que ha ahorrado todo el año y puede comprarse un equipo nuevo. Cuando llegó a la tienda se encontró con dos anuncios que llamaron su atención. Ahora no sabe qué teléfono elegir." 
      } 
   }
]; 