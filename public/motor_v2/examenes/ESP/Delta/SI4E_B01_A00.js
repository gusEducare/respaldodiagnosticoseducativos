json=[
  {
    "respuestas": [        
        {
           "t13respuesta":  "que",
             "t17correcta": "1"
         },
        {
           "t13respuesta":  "qué",
             "t17correcta": "2"
         },
        {
           "t13respuesta":  "cuanto",
             "t17correcta": "3"
         },
        {
           "t13respuesta":  "dónde",
             "t17correcta": "4"
         },
        {
           "t13respuesta":  "donde",
             "t17correcta": "5"
         },
        

        {
           "t13respuesta":  "como",
             "t17correcta": "6"
         },
        {
           "t13respuesta":  "cuánto",
             "t17correcta": "7"
         },
 {
            "t13respuesta":  "cómo",
              "t17correcta": "0"
          },
        
    ],
    "preguntas": [
      {
      "t11pregunta": "-     Hola Fer. Me gustaría "
      },
      {
      "t11pregunta": " vinieras hoy a mi casa a jugar videojuegos, ¿a "
      },
      {
      "t11pregunta": "&nbsp;&nbsp;&nbsp;hora puedes?<br>-  Estoy haciendo la tarea, en "
      },
      {
      "t11pregunta": " acabe voy para allá. ¿En "
      },
      {
      "t11pregunta": " vives?<br>-   En la colonia "
      },
      {
      "t11pregunta": " hay unas torres de colores.<br>-  No recuerdo llegar, pero busco en un mapa.<br>-  ¡Excelente! Te veo "
      },
      
      {
      "t11pregunta": " a las 6 de la tarde.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que completen las oraciones.",
       "t11instruccion": "",
       "respuestasLargas": true,
       "pintaUltimaCaja": false,
       "contieneDistractores": true
     }
  } ,

 {
 "respuestas":[
{
 "t13respuesta":"Pueden ser infografías, mapas o fotografías.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"Frase breve que encabeza un texto.",
 "t17correcta":"2",
 },
{
 "t13respuesta":"Consiste en explicar un texto con palabras propias.",
 "t17correcta":"3",
 },
{
 "t13respuesta":"Texto que incluye las ideas principales y secundarias sin perder la intención del autor.",
 "t17correcta":"4",
 },
],
 "preguntas": [
 {
 "t11pregunta":"Fuente gráfica"
 },
{
 "t11pregunta":"Título"
 },
{
 "t11pregunta":"Paráfrasis"
 },
{
 "t11pregunta":"Resumen"
 },
 ],
 "pregunta":{
 "c03id_tipo_pregunta":"12",
 "t11instruccion": "",
 "t11pregunta":"Relaciona las columnas según corresponda.",
 "altoImagen":"100px",
 "anchoImagen":"200px"
 }
 } ,

 {
"respuestas":[
{
"t13respuesta":"F",
"t17correcta":"0",
},
{
"t13respuesta":"V",
"t17correcta":"1",
 },

], "preguntas" : [
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "El texto anterior es un ejemplo de trabalenguas.",
"correcta":"1",
},
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "El texto anterior carece de  sonidos semejantes y repetidos.",
"correcta":"0",
},
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "Las palabras pintor y pinta provienen de la misma familia de palabras.",
"correcta":"1",
},
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "Las palabras perpetuo, partir y París provienen de la misma familia de palabras.",
"correcta":"0",
},
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "Todas las palabras del texto comienzan con la misma letra para hacerlo fácil de pronunciar.",
"correcta":"0",
},
],
"pregunta":{
"c03id_tipo_pregunta":"13",
"t11pregunta": "<p>Lee el siguiente texto y elige falso (F) o verdadero (V) según corresponda.<br><br>Pedro Pérez Pita pintor perpetuo<br>\n\
pinta paisajes por poco precio<br>\n\
para poder partir<br>\n\
pronto para París</p>",
"t11instruccion": "",
"descripcion": "Reactivo",
 "variante": "editable",
"anchoColumnaPreguntas": 60,
"evaluable": true
}
} ,

 {
 "respuestas":[
{
 "t13respuesta":"árbol",
 "t17correcta":"1",
 },
{
 "t13respuesta":"lápiz",
 "t17correcta":"1",
 },
{
 "t13respuesta":"pentágono",
 "t17correcta":"1",
 },
{
 "t13respuesta":"francés",
 "t17correcta":"1",
 },
{
 "t13respuesta":"adiós",
 "t17correcta":"1",
 },
{
 "t13respuesta":"arbol",
 "t17correcta":"0",
 },
{
 "t13respuesta":"hipopotamo",
 "t17correcta":"0",
 },
{
 "t13respuesta":"album",
 "t17correcta":"0",
 },
{
 "t13respuesta":"caracteristica",
 "t17correcta":"0",
 },
],
 "pregunta":{
 "c03id_tipo_pregunta":"2",
 "t11pregunta": "Selecciona todas las respuestas correctas para la pregunta.<br><br>¿Qué palabras están acentuadas correctamente?",
 "t11instruccion": "",
 }
 },

  {
 "respuestas":[
{
 "t13respuesta":"Av.",
 "t17correcta":"1",
 },
{
 "t13respuesta":"INBA",
 "t17correcta":"2",
 },
{
 "t13respuesta":"UNAM",
 "t17correcta":"3",
 },
{
 "t13respuesta":"CDMX",
 "t17correcta":"4",
 },
{
 "t13respuesta":"Czda.",
 "t17correcta":"5",
 },
{
 "t13respuesta":"Cdad.",
 "t17correcta":"6",
 },
{
 "t13respuesta":"Edo.",
 "t17correcta":"7",
 },
],
 "preguntas": [
 {
 "t11pregunta":"Avenida"
 },
{
 "t11pregunta":"Instituto Nacional de Bellas Artes"
 },
{
 "t11pregunta":"Universidad Nacional Autónoma de México"
 },
{
 "t11pregunta":"Ciudad de México"
 },
{
 "t11pregunta":"Calzada"
 },
{
 "t11pregunta":"Ciudad"
 },
{
 "t11pregunta":"Estado"
 },
 ],
 "pregunta":{
 "c03id_tipo_pregunta":"12",
 "t11pregunta":"Relaciona las columnas según corresponda.",
 "t11instruccion": "",
 "altoImagen":"100px",
 "anchoImagen":"200px"
 }
 } ,
//reactivo 6
  {
 "respuestas":[
 {
 "t13respuesta":"Ignacio Allende con Francisco I. Madero",
 "t17correcta":"1",
  "numeroPregunta": "0"
 },
{
 "t13respuesta":"Av. Corregidora con Av. Constituyentes",
 "t17correcta":"0",
  "numeroPregunta": "0"
 },
{
 "t13respuesta":"Ignacio Allende con Av. 5 de febrero",
 "t17correcta":"0",
  "numeroPregunta": "0"
 },////////////////////////////////
  {
 "t13respuesta":"Entre las calles Ignacio Allende y Vicente Guerrero y las calles Francisco I. Madero y Pino Suárez.",
 "t17correcta":"1",
  "numeroPregunta": "1"
 },
{
 "t13respuesta":"Entre la calle 16 de septiembre y Francisco I. Madero y entre Av. Corregidora y Juárez.",
 "t17correcta":"0",
  "numeroPregunta": "1"
 },
{
 "t13respuesta":"Ignacio Allende con Av. 5 de febrero.",
 "t17correcta":"0",
  "numeroPregunta": "1"
 },  ///////////////////////////////////////
  {
 "t13respuesta":"Conducir por Calle 16 de septiembre, dar vuelta a la izquierda en Vicente Guerrero hasta Pino Suárez y ahí girar a la izquierda hasta la Plaza de la Constitución.",
 "t17correcta":"1",
  "numeroPregunta": "2"
 },
{
 "t13respuesta":"Conducir por Calle 16 de septiembre, dar vuelta a la izquierda en Vicente Guerrero hasta Pino Suárez y ahí girar a la derecha hasta la Plaza de la Constitución.",
 "t17correcta":"0",
  "numeroPregunta": "2"
 },
{
 "t13respuesta":"Conducir por Calle 16 de septiembre, dar vuelta a la derecha en Vicente Guerrero hasta Pino Suárez y ahí girar a la derecha hasta la Plaza de la Constitución.",
 "t17correcta":"0",
  "numeroPregunta": "2"
 },
],
 "pregunta":{
 "c03id_tipo_pregunta":"1",
 "t11pregunta": ["Selecciona la respuesta correcta.<br><img src='SI4E_B01_A00_00.png' width='800'><br>¿Qué intersecciones se pueden apreciar en el croquis?","¿Cuál es la ubicación del Museo de Arte?","¿Cuál es el trayecto a seguir para llegar en auto del Cineteatro Rosalío Solano a la Plaza de la Constitución?"],
 "t11instruccion": "",
   "preguntasMultiples": true
 }
 },
 // reactivo 7
 {
"respuestas":[
{
"t13respuesta":"F",
"t17correcta":"0",
},
{
"t13respuesta":"V",
"t17correcta":"1",
 },

], "preguntas" : [
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "El tema de una monografía debe ser extenso y abarcar toda la información posible sobre el tema.",
"correcta":"0",
},
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "La monografía resalta los aspectos más importantes sobre el tema.",
"correcta":"1",
},
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "El escritor de una monografía debe expresar su opinión personal.",
"correcta":"0",
},
{
"c03id_tipo_pregunta": "13",
"t11pregunta": "Para hacer una monografía podemos ayudarnos de fuentes bibliográficas.",
"correcta":"1",
},

],
"pregunta":{
"c03id_tipo_pregunta":"13",
"t11pregunta": "<p>Lee el siguiente texto y elige falso (F) o verdadero (V) según corresponda.",
"t11instruccion": "",
"descripcion": "Reactivo",
 "variante": "editable",
"anchoColumnaPreguntas": 60,
"evaluable": true
}
} ,
// reactivo 8
{
  "respuestas": [        
        {
           "t13respuesta":  "Cultura Maya",
             "t17correcta": "1"
         },
        {
           "t13respuesta":  "Ubicación Geográfica",
             "t17correcta": "2"
         },
        {
           "t13respuesta":  "Organización Social",
             "t17correcta": "3"
         },
        {
           "t13respuesta":  "Religión",
             "t17correcta": "4"
         },
        {
           "t13respuesta":  "Aportaciones",
             "t17correcta": "5"
         },
        {
           "t13respuesta":  "Bibliografía",
             "t17correcta": "6"
         },
    ],
    "preguntas": [
      {
      "t11pregunta": ""
      },
      {
      "t11pregunta": "<br>La cultura maya fue una civilización precolombina mesoamericana que surgió durante el periodo clásico (2000 a.C. al 250 d. C.).<br>"
      },
      {
      "t11pregunta": "<br>Los mayas vivieron en el sur y sureste de México (que corresponde a los estados de Yucatán, Campeche, Tabasco, Quintana Roo y la zona oriental de Chiapas) y en partes de Guatemala, Honduras y Belice.<br>"
      },
      {
      "t11pregunta": "<br>Estaba dividida en tres clases sociales:<br>1. La clase alta que estaba conformada por los gobernantes y sus familiares, los funcionarios de alta jerarquía y los ricos comerciantes.<br>2. Funcionarios públicos y los trabajadores especializados.<br>3. Campesinos, obreros y prisioneros de guerra (esclavos).<br>"
      },
      {
      "t11pregunta": "<br>Se consideraba panteísta, ya que adoraban a la naturaleza y los fenómenos atmosféricos; y politeísta, porque adoraban a varios dioses.<br>"
      },
      {
      "t11pregunta": "<br>En Matemáticas, los mayas desarrollaron un sistema de numeración base 20 y crearon el concepto del cero.  En arquitectura, construyeron grandes ciudades como Chichén Itzá, Tikal y Uxmal que actualmente son sitios arqueológicos de gran importancia.<br>"
      },
      {
      "t11pregunta": "<br>Historia Universal, cultura maya.  Recuperado de: http://www.historiacultural.com/2010/01/cultura-maya-precolombina-mesoamerica.html<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que correspondan al título de cada párrafo.",
       "t11instruccion": "",
       "respuestasLargas": true,
       "pintaUltimaCaja": false,
       "contieneDistractores": true
     }
},
// reactivo 9
{
"respuestas":[
{
"t13respuesta":"En ciertas ocasiones, es mejor guardar silencio.",
"t17correcta":"1",
"numeroPregunta": "0"
},
{
"t13respuesta":"Mantén la boca cerrada si hay moscas cerca.",
"t17correcta":"0",
"numeroPregunta": "0"
},
{
"t13respuesta":"Siempre sé el primero en opinar.",
"t17correcta":"0",
"numeroPregunta": "0"
},////////////////////////////////
{
"t13respuesta":"Satisfacer el apetito nos hace sentir bien.",
"t17correcta":"1",
"numeroPregunta": "1"
},
{
"t13respuesta":"Si no comes, siempre estarás contento.",
"t17correcta":"0",
"numeroPregunta": "1"
},
{
"t13respuesta":"Es importante ser feliz.",
"t17correcta":"0",
"numeroPregunta": "1"
},  ///////////////////////////////////////
{
"t13respuesta":"Cuando algo malo sucede, siempre trae algo bueno.",
"t17correcta":"1",
"numeroPregunta": "2"
},
{
"t13respuesta":"Siempre debemos hacer el bien.",
"t17correcta":"0",
"numeroPregunta": "2"
},
{
"t13respuesta":"Todo lo bueno trae algo malo para compensar.",
"t17correcta":"0",
"numeroPregunta": "2"
},
],
"pregunta":{
"c03id_tipo_pregunta":"1",
"t11pregunta": ["Lee cada refrán y selecciona la respuesta que corresponda al mensaje que transmite.<br><br>En boca cerrada no entran moscas.","Barriga llena, corazón contento.","No hay mal que por bien no venga."],
"t11instruccion": "",
 "preguntasMultiples": true
}
},
// reactivo 10
{
    "respuestas": [
        {
            "t13respuesta": "<p>pequeña<\/p>",
            "t17correcta": "1,3,5,7,8,9,10",

        },
        {
            "t13respuesta": "<p>grandes<\/p>",
            "t17correcta": "1,3,5,7,8,9,10"
            ,
        },
        {
            "t13respuesta": "<p>azules<\/p>",
            "t17correcta": "1,3,5,7,8,9,10"
        },
        {
            "t13respuesta": "<p>asombroso<\/p>",
            "t17correcta": "1,3,5,7,8,9,10"
        },
        {
            "t13respuesta": "<p>rasgados<\/p>",
            "t17correcta": "1,3,5,7,8,9,10",
        },
        {
            "t13respuesta": "<p>extraño<\/p>",
            "t17correcta": "1,3,5,7,8,9,10",

        },
        {
            "t13respuesta": "<p>hermosa<\/p>",
            "t17correcta": "1,3,5,7,8,9,10",

        },
        {
            "t13respuesta": "<p>pausadamente<\/p>",
            "t17correcta": "2,4,6",
        },
        {
            "t13respuesta": "<p>asustada<\/p>",
            "t17correcta": "2,4,6",

        },
        {
            "t13respuesta": "<p>perfectamente<\/p>",
            "t17correcta": "2,4,6",

        }

    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<br><style>\n\
                                   .table img{height: 90px !important; width: auto !important; }\n\
                               </style><table class='table' style='margin-top:-40px;'>\n\
                               <tr>\n\
                                   <td style='width:350px'>Adjetivos</td>\n\
                                   <td style='width:100px'>Adverbios</td>\n\
                               </tr>\n\
                               <tr>\n\
                                   <td >"
        },

        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": " </td><td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "     </td></tr><tr>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "       </td><td>"

        },

        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td> </tr><tr>\n\ <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "  </td>  <td>"

        },

        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "  </td>\n\
                                   </tr><tr><td>"

        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "  </td>  <td></td></tr><tr><td>"

        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "  </td>  <td></td></tr><tr><td>"

        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "  </td>  <td></td></tr><tr><td>"

        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "  </td>  <td></td></tr></table>"

        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "Arrastra los elementos y completa la tabla.",
        "contieneDistractores": true,
        "anchoRespuestas": 90,
        "soloTexto": true,
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "ocultaPuntoFinal": true,
        "t11instruccion":"<img src='SI4A_B01_R10.png'><br>Mi amiga de China <br>Recuerdo que ese caluroso día de escuela estábamos estudiando sobre las grandes ballenas azules, cuando de pronto, la directora entró al salón.  La acompañaba una pequeña niña de ojos rasgados, que se veía muy asustada. La directora comenzó a hablar pausadamente y nos presentó a nuestra nueva compañera.  Todos volteamos a verla mientras permanecíamos en silencio. Ella se presentó y nos dijo su extraño nombre: Yuga. Hablaba español perfectamente y nos platicó sobre su asombroso país y su hermosa cultura. De inmediato, todos nos acercamos a platicar con ella. era la niña nueva y queríamos hacerla sentir bienvenida."
    }
},

 ];
