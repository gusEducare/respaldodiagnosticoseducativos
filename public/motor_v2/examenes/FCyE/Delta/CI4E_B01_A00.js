json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las capacidades son talentos o destrezas que tienen las personas para realizar ciertas actividades.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con base en la genética, cada persona está predispuesta para realizar determinadas tareas. ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las habilidades no pueden ser desarrolladas, pues están marcadas por el contexto en el que se vive.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las habilidades pueden dividirse en dos clasificaciones: las motrices y las mentales.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Existen 8 tipos de inteligencias, y cada persona puede poseerlas todas en diferentes medidas y combinaciones.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Existe un tipo de inteligencia que es universal, es decir, todas las personas la poseen y solo pueden desarrollarla si practican diferentes actividades.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Las personas tienen diferentes formas de ser inteligentes. Todo ello es dado por la genética, por lo que no pueden desarrollar más habilidades que las que heredaron.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué afirmaba Howard Gardner en su &quot;Teoría de las inteligencias múltiples&quot;?",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Inteligencia musical.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Inteligencia verbal-lingüística.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Inteligencia interpersonal.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Inteligencia intrapersonal.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Inteligencia kinestésica.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Inteligencia lógico-matemática.",
                "t17correcta": "6",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Luis tiene una gran capacidad para escuchar, cantar y tocar instrumentos."
            },
            {
                "t11pregunta": "Lorena es profesora de español, tiene una gran capacidad para comprender el orden y significado de las palabras tanto en la lectura como en la escritura."
            },
            {
                "t11pregunta": "Samanta tiene habilidades y gusto por trabajar con las personas, así como también para ayudarlas a identificar y superar sus problemas."
            },
            {
                "t11pregunta": "Julio se plantea metas, y de cada una de ellas, analiza y evalúa las habilidades con las que cuenta para lograrlas."
            },
            {
                "t11pregunta": "Santiago es bailarín, desde niño disfrutaba y era bueno con actividades que requerían fuerza y coordinación."
            },
            {
                "t11pregunta": "La profesora de José notó su capacidad para calcular, y también para realizar razonamientos inductivos y deductivos."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Es la inteligencia que nos permite reconocer, manejar y gestionar la forma en que expresamos e interpretamos las emociones, tanto propias como ajenas.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Es la capacidad de reconocer nuestras emociones, definirlas y encontrar una forma de no expresarlas.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Es la inteligencia producida por un sentimiento muy intenso de alegría o tristeza provocado por una situación o hecho ajeno a nosotros.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué es la inteligencia emocional?",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },

    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Desarrollar la capacidad de trabajar en equipo con tus compañeros y aprender el sentido de camaradería para lograr una meta en común.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Conseguir desarrollar tus habilidades manuales y fomentar la parte creativa de tu cerebro.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Fomentar tus habilidades de imaginación, perder el miedo de compartir tus ideas con tus compañeros y conocer gente con ideas similares a las tuyas.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Aprender que existen lugares en los que puedes aprender además de la escuela, Así como lograr fomentar tus habilidades para socializar con tus compañeros.",
                "t17correcta": "4",

            },


        ],
        "preguntas": [
            {
                "t11pregunta": "Ser parte de un club deportivo..."
            },
            {
                "t11pregunta": "Participar en un curso de arte..."
            },
            {
                "t11pregunta": "Entrar a un taller de escritura creativa..."
            },
            {
                "t11pregunta": "Ir a excursiones con tus compañeros..."
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.<br><br>¿Qué impacto tendría cada actividad en tu desarrollo personal?",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Habilidades",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Capacidades",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Ambos",
                "t17correcta": "2",
            }
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son aptitudes que todos los seres humanos poseen.",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son las herramientas naturales con que las personas enfrentan diferentes retos.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Facilidad y rapidez que permite llevar a cabo una actividad.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son técnicas y prácticas específicas que se utilizan para resolver situaciones.",
                "correcta": "0",
            },

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Incluye un proceso que permite adquirir condiciones para aprender.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada característica.",
            "t11instruccion": "",
            "descripcion": "Características",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Son las reacciones que produce el cuerpo ante un estímulo del entorno.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Son respuestas intensas y automáticas a diversos estímulos.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Son reflejos automáticos que se producen al estar frente a situaciones únicamente felices.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Paula lloró al momento de no encontrar su mochila.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Pedro corrió por su chamarra cuando empezó a llover.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Jaime dice que ama tanto a su mamá que le escribirá una carta.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué son las sensaciones?",
                "¿Quién de los alumnos está expresando una emoción?",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen cinco emociones básicas: alegría, enojo, asco, tristeza y miedo.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones, las sensaciones y los sentimientos son diferentes para cada individuo y no cambian a lo largo de la vida.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Reconocer la emoción que se está viviendo permite tomar decisiones acertadas y sin dañar a nadie más.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se reacciona siempre de la misma forma, no importa la emoción que se está sintiendo.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Triste.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Enojado.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Feliz.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Sorprendido.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Asco.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Santiago perdió a su mascota y ahora él se siente..."
            },
            {
                "t11pregunta": "Juan llegará tarde a un compromiso por no salir a tiempo de casa y toparse con el tráfico, él se sintió..."
            },
            {
                "t11pregunta": "El equipo de fútbol de Norma, gano el campeonato estatal, ella se siente..."
            },
            {
                "t11pregunta": "A Toño le hicieron un examen para el que no había estudiado, por ello, se sintió..."
            },
            {
                "t11pregunta": "Al abrir el refrigerador, Natalia se encontró con un envase de comida caducada, al olerlo no pudo evitar sentir…"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "Desarrollo de empatía y comprensión.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Fomenta habilidades personales y colectivas.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Permite forjar una personalidad propia.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Mejora la imagen pública del contexto.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Beneficia el liderazgo y la capacidad de seguir instrucciones.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>¿Por qué es importante la pertenencia a un grupo?.",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //11
    {
         "respuestas": [
             {
                 "t13respuesta": "<p>Cercanía entre miembros.<\/p>",
                 "t17correcta": "1,3,5"
             },
             {
                 "t13respuesta": "<p>Relaciones íntimas y de larga duración.<\/p>",
                 "t17correcta": "1,3,5"
             },
             {
                 "t13respuesta": "<p>Determinan valores, actitudes y <br>creencias de las personas.<\/p>",
                 "t17correcta": "1,3,5"
             },
             {
                 "t13respuesta": "<p>Relación informal e intermitente.<\/p>",
                 "t17correcta": "2,4,6"
             },
             {
                 "t13respuesta": "<p>Pueden disolverse fácilmente.<\/p>",
                 "t17correcta": "2,4,6"
             },
             {
                 "t13respuesta": "<p>La escuela, los grupos religiosos o <br>de trabajo, son algunos ejemplos.<\/p>",
                 "t17correcta": "2,4,6"
             }
         ],
         "preguntas": [
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "<br><style>\n\
                                         .table img{height: 90px !important; width: auto !important; }\n\
                                     </style><table class='table' style='margin-top:-40px;'>\n\
                                     <tr>\n\
                                         <td style='width:250px'>Grupos primarios</td>\n\
                                         <td style='width:100px'>Grupos secundarios </td>\n\
                                     </tr>\n\
                                     <tr>\n\
                                         <td >"
             },

             {"c03id_tipo_pregunta": "8",
                 "t11pregunta": "  </td>\n\
                   <td>"
             },
             //
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "     </td></tr><tr>\n\
                                         <td>"
             },
             //
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "     </td>\n\
                                          <td>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "     </td></tr><tr>\n\
                                          <td>"
             },
             {
                 "c03id_tipo_pregunta": "8",
                 "t11pregunta": "       </td>\n\
                    <td> "
             },


              {"c03id_tipo_pregunta": "8",
                 "t11pregunta": "  </td>\n\
                                         </tr></table>"

  }
         ],
         "pregunta": {
             "c03id_tipo_pregunta": "8",
             "t11pregunta": "Arrastra cada elemento al sitio que complete el sentido de la oración.",
            "contieneDistractores":true,
             "anchoRespuestas": 90,
             "soloTexto": true,
             "respuestasLargas": true,
             "pintaUltimaCaja": false,
             "ocultaPuntoFinal": true
         }
     },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "Que debemos tener cuidado con las personas en las cuales depositamos nuestra confianza y tener claro cuáles son sus intenciones hacia nosotros.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Que debemos de confiar en cualquier persona solo porque comparte ciertas características con nosotros.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Que tenemos que ser desconfiados con las personas que tratan de abrir un espacio de confianza entre ambos.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br><center> <img style='height: 300px; display:block; ' src='CI4E_B01_01.png'> </center>¿Qué mensaje crees que nos quiere transmitir la imagen?",],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la Convención sobre los Derechos del Niño, creada en 1998, se describen los derechos fundamentales de todos los niños.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En México son considerados como niños todas las personas que tienen menos de 12 años.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Ley para la Protección de los Derechos de Niñas, Niños y Adolescentes, consta de 20 artículos, entre ellos, el derecho a vivir en familia y el derecho a una vida libre de violencia.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los adultos deben proveer a los niños de actividades que les permitan un desarrollo integral.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 0,
            "evaluable": true
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "Cuidados de alimentación, salud e higiene.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Fomento del desarrollo intelectual y la promoción de actividades benéficas en el tiempo libre.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Libertad de pensamiento y expresión, además de la posibilidad de participar en los asuntos de la sociedad.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Bienestar físico"
            },
            {
                "t11pregunta": "Bienestar mental"
            },
            {
                "t11pregunta": "Bienestar social "
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "<p><font size = 4>Fondo de las Naciones<BR>Unidas para la Infancia</font><\/p>",
                "t17correcta": "1,4,7"
            },
            {
                "t13respuesta": "<p><font size = 4>Sistema Nacional para el <br>Desarrollo Integral de la<br>Familia en México</font><\/p>",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "<p><font size = 4>Comisión Nacional de los<br>Derechos Humanos</font><\/p>",
                "t17correcta": "3,6"
            },
            {
                "t13respuesta": "<p><font size = 4>Promueve la protección de<br>los derechos de los niños.</font><\/p>",
                "t17correcta": "1,4,7"
            },
            {
                "t13respuesta": "<p><font size = 4>Encargada de proteger y<br>defender los derechos de<br>las familias mexicanas. </font><\/p>",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "<p><font size = 4>Organismo encargado de<br>la defensa y divulgación<br>de los derechos humanos. </font><\/p>",
                "t17correcta": "3,6"
            },
            {
                "t13respuesta": "<p><font size = 4>Apoya en la satisfacción de<br>las necesidades de los<br>niños, como la educación,<br>alimentación y vivienda.</font><\/p>",
                "t17correcta": "1,4,7"
            },
            {
                "t13respuesta": "<p><font size = 4>Realizan políticas públicas<br>y acciones sociales.</font><\/p>",
                "t17correcta": "2,5,8"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td style='width:150px'>UNICEF</td>\n\
                                        <td style='width:100px'>DIF</td>\n\
                                        <td style='width:100px'>CNDH</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr></table>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "¿Qué características tienen las instituciones encargadas de proteger los derechos de los niños y niñas?",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La libertad es la capacidad de tomar decisiones sin restricciones, sobre aquello que nos lleva a cumplir una meta.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La libertad es un derecho que las personas deben ganarse, pues de no contar con las habilidades necesarias, es mejor que las personas no puedan elegir.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al momento de actuar con libertad, se deben asumir las consecuencias de los actos.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La libertad no debe ser restringida ni limitada, cada persona puede actuar conforme a sus deseos sin importar qué consecuencias tienen sus actos.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //17
    /*{
        "respuestas": [
            {
                "t13respuesta": "función",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "asociación",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "enseñar",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "opinión",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "expresión",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "manifestar",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "elección",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "culto",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "elección",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "manifestación",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "elegir",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "libertad",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Existen diferentes tipos de libertad y cada una de ellas tiene una "
            },
            {
                "t11pregunta": " propia, algunas de ellas son:<br>La libertad de "
            },
            {
                "t11pregunta": " es la que nos permite agruparnos de acuerdo a nuestras ideologías.<br>La libertad académica permite  "
            },
            {
                "t11pregunta": " o estudiar cualquier tema que sea de nuestro interés.<br>Algunas se llegan a confundir, como la libertad de "
            },
            {
                "t11pregunta": " y la de "
            },
            {
                "t11pregunta": ", pues la primera hace referencia a dar nuestro punto de vista sin dañar a nadie y la segunda, en que las personas pueden "
            },
            {
                "t11pregunta": " lo que piensan.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
*/
// reactivo 18
{
    "respuestas": [
        {
            "t13respuesta": "Es la capacidad que se tiene para moderar lo que se piensa, siente y dice, para saber actuar de acuerdo con las situaciones.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Capacidad de las personas para resolver de forma positiva y pacífica los conflictos a los que se enfrentan.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Control voluntario de los movimientos del cuerpo y valoración de los conocimientos antes de actuar.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué es la autorregulación?",],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
},

// reactivo 19

{
    "respuestas": [
        {
            "t13respuesta": "Doña Rosa descubrió que su sobrino rompió sus macetas favoritas, por ello, antes de enojarse y regañarlo, se calmó y decidió hablar con él para que tuviera más cuidado al jugar.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Noelia quería que sus papás le compraran un videojuego; cuando sus papás le dijeron que no lo harían, comenzó a gritar que lo quería.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "El profesor Carlos se enojó porque sus alumnos no entregaron bien la tarea, y por esa razón fueron castigados.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Quién está poniendo en práctica la autorregulación?",],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
},

// reactivo 20

{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Una decisión racional es aquella en la cual solo se toman en cuenta las emociones.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Para tomar decisiones racionales debe existir equilibrio entre lo que sentimos, lo que pensamos y lo que queremos lograr.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Todas las personas tienen la capacidad de tomar decisiones, mientras estas respeten a los demás y sigan las normas y reglas establecidas.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Todas las decisiones que se toman tienen consecuencias positivas.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Antes de tomar una decisión, debemos preguntarnos qué queremos lograr y qué acción nos lleva a conseguirlo.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El desarrollo personal son todos aquellos comportamientos o actitudes que nos ayudan a lograr nuestros objetivos.",
            "correcta": "1",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Elige Falso o Verdadero según corresponda.",
        "t11instruccion": "",
        "descripcion": "Enunciados",
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable": true
    }
},

//reactivo 21
{
    "respuestas": [
        {
            "t13respuesta": "Ser libre significa que cada persona puede actuar según su voluntad, siempre y cuando respete la ley, además de los derechos de las demás personas.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "La libertad incluye que las personas decidan sobre sus acciones con la finalidad de no obtener beneficios de su toma de decisiones.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "La libertad es un derecho que tienen todas las personas; se adquiere al momento de nacer.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cuál es la relación entre libertad y toma de decisiones?",],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
},

// reactivo 22
{
    "respuestas": [
        {
            "t13respuesta": "Su influencia es a corto, mediano y largo plazo; es decir, las consecuencias que se obtienen de cada alternativa.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "El impacto que tendrá la decisión en otras actividades.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Si es una decisión planeada o aleatoria.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Tomar en cuenta si es una decisión que se toma regularmente o es un caso excepcional.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Tomar todas las decisiones en consenso con otra persona, nunca de forma individual.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Concentrar conocimientos, pensamientos y sensaciones.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Reducir conflictos en la comunicación e implicar un grado de compromiso.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "2",
        "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>¿Qué elementos influyen en la toma de decisiones? ",],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
},
// reactivo 23

{
    "respuestas": [
        {
            "t13respuesta": "Acción",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "Reacción",
            "t17correcta": "2",
        },

    ],
    "preguntas": [
        {
            "t11pregunta": "Realizar una actividad que implica movimiento, cambios físicos y mentales."
        },
        {
            "t11pregunta": "Se genera por impulso; puede ser física, emocional o psicológica."
        },

    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda.",
        "t11instruccion": "",
        "altoImagen": "100px",
        "anchoImagen": "200px"
    }
},
// reactivo 24
{
     "respuestas": [
         {
             "t13respuesta": "<p>Golpes<\/p>",
             "t17correcta": "1,4"
         },
         {
             "t13respuesta": "<p>Enojo<\/p>",
             "t17correcta": "2,5"
         },
         {
             "t13respuesta": "<p>Frustración<\/p>",
             "t17correcta": "3"
         },
         {
             "t13respuesta": "<p>Gritos<\/p>",
             "t17correcta": "1,4"
         },
         {
             "t13respuesta": "<p>Tristeza<\/p>",
             "t17correcta": "2,5"
         },

     ],
     "preguntas": [
         {
             "c03id_tipo_pregunta": "8",
             "t11pregunta": "<br><style>\n\
                                     .table img{height: 90px !important; width: auto !important; }\n\
                                 </style><table class='table' style='margin-top:-40px;'>\n\
                                 <tr>\n\
                                     <td style='width:250px'>Reacción física</td>\n\
                                     <td style='width:250px'>Reacción emocional</td>\n\
                                     <td style='width:250px'>Reacción psicológica</td>\n\
                                 </tr>\n\
                                 <tr>\n\
                                     <td >"
         },

         {"c03id_tipo_pregunta": "8",
             "t11pregunta": "  </td>\n\
               <td>"
         },
         //
         {
             "c03id_tipo_pregunta": "8",
             "t11pregunta": "     </td>\n\
                                     <td>"
         },
         //
         {
             "c03id_tipo_pregunta": "8",
             "t11pregunta": "     </td></tr><tr>\n\
                                     <td>"
         },
         //
         {
             "c03id_tipo_pregunta": "8",
             "t11pregunta": "     </td>\n\
                                      <td>"
         },
         //
         {
             "c03id_tipo_pregunta": "8",
             "t11pregunta": "     </td>\n\
                                      </tr></table>"
         },
         //





     ],
     "pregunta": {
         "c03id_tipo_pregunta": "8",
         "t11pregunta": "Arrastra cada elemento donde corresponda.<br><br>¿Qué tipo de reacciones tuvo Carlos a lo largo de su día?",
         "t11instruccion":"Carlos tuvo un altercado inesperado. El viaje que había planeado por meses fue cancelado sin previo aviso;  por ello, su primera reacción fue lanzarse a los golpes y dar gritos, pues trataba de hacer que la otra persona notara su frustración. Lo que le sucedió le hizo sentir enojo y posteriormente tristeza, por la forma en que actuó.",
        "contieneDistractores":true,
         "anchoRespuestas": 90,
         "soloTexto": true,
         "respuestasLargas": true,
         "pintaUltimaCaja": false,
         "ocultaPuntoFinal": true
     }
 },
 // reactivo 25
 {
     "respuestas": [
         {
             "t13respuesta": "Carlos debió tranquilizarse, respirar y revisar el porqué de la cancelación de su viaje; después, verificar cuándo se podría volver a agendar.",
             "t17correcta": "1",
             "numeroPregunta": "0"
         },
         {
             "t13respuesta": "Carlos, después de gritar y golpear, debía calmarse y respirar; de esta forma, hablaría claramente con las personas involucradas.",
             "t17correcta": "0",
             "numeroPregunta": "0"
         },
         {
             "t13respuesta": "Carlos tenía que hablar de forma enérgica con el encargado y después de eso, exigir su dinero de vuelta.",
             "t17correcta": "0",
             "numeroPregunta": "0"
         },
     ],
     "pregunta": {
         "c03id_tipo_pregunta": "1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cuál habría sido una forma positiva de actuar?",],
         "contieneDistractores": true,
         "preguntasMultiples": true,
         "evaluable": true,
         "t11instruccion": "Carlos tuvo un altercado inesperado. El viaje que había planeado por meses fue cancelado sin previo aviso; por ello, su primera reacción fue lanzarse a los golpes y dar gritos, pues trataba de hacer que la otra persona notara su frustración. Lo que le sucedió le hizo sentir enojo y posteriormente tristeza, por la forma en que actuó."
     }
 },
// reactivo 26
{
    "respuestas": [
        {
            "t13respuesta": "Para no actuar de manera impulsiva y poder meditar qué alternativa trae mayores beneficios, con base en lo que se quiere lograr.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Para tomar más tiempo del propuesto, de forma que se puede decidir más rápida y eficazmente.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Si se controlan las reacciones, la forma de actuar es tranquila y novedosa.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Por qué es importante controlar las reacciones antes de tomar una decisión?",],
        "contieneDistractores": true,
        "preguntasMultiples": true,
        "evaluable": true
    }
},


];
