json=[
{  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPara llegar a un acuerdo en el que se respeten nuestros intereses, pero también se respeten los intereses de los demás.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara defender nuestros intereses sin importar la opinión de los demás.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPara mediar nuestros conflictos, de forma que podamos exigir que se haga lo que nosotros creemos mejor. \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br><br>Cuando se nos presenta  un conflicto, ¿para qué nos sirve el diálogo?"
      }
   },/////////2
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Falta de interés por parte de los ciudadanos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "La comunicación no es clara y sincera por parte del gobierno.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Falta responsabilidad de las organizaciones que deben facilitar la comunicación.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Las organizaciones dan comunicación clara y ordenada.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Los ciudadanos no explican sus finanzas a los gobernantes.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Los gobernantes están en su derecho de no dar explicación de sus acciones.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>¿Cuáles son los motivos por los que no existe una buena comunicación entre gobernantes y ciudadanos?"
      }
   },//////////////3
   {
        "respuestas": [
            {
                "t13respuesta": "Abstenerse de cualquier acto u omisión que implique incumplimiento de cualquier disposición jurídica relacionada con el servicio público.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Proporcionar, en su caso, en tiempo y forma ante las dependencias competentes, la documentación comprobatoria de la aplicación  de recursos económicos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Abstenerse de desempeñar algún otro empleo, cargo o comisión oficial o particular.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<small>Abstenerse de nombrar, contratar o promover como servidores públicos a personas con quienes tenga parentesco consanguíneo, por afinidad o civil, y que por razón de su adscripción dependan jerárquicamente de la unidad administrativa de la que sea titular.</small>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Cumplir con la máxima diligencia (interés-dedicación) el servicio que le sea encomendado.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Legalidad"
            },
            {
                "t11pregunta": "Honradez"
            },
            {
                "t11pregunta": "Lealtad"
            },
            {
                "t11pregunta": "Imparcialidad"
            },
            {
                "t11pregunta": "Eficiencia"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion":"Los servidores públicos tienen una serie de obligaciones fundamentales para poder ejercer su cargo de la mejor manera, estas son: legalidad, honradez, lealtad, imparcialidad y eficiencia; para lograr que estas se cumplan se necesita una serie de obligaciones por parte de los servidores públicos. El artículo 42 de nuestra constitución señala una serie de 32 obligaciones que deben seguirse para salvaguardarlas.",
            "t11pregunta":"Relaciona las columnas según corresponda",
        }
    },////////4
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EQue los servidores públicos y gobernantes deben ocuparse de realizar de la mejor manera su trabajo, para infundir el sentimiento de crecimiento en las personas y puedan crear herederos que cuiden del país.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EQue los servidores públicos y gobernantes deben cuidar del pueblo y dejar la corrupción.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EQue los servidores públicos y gobernantes están cumpliendo con su deber al pie de la letra.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EQue los servidores públicos y gobernantes deben aferrarse al poder, además de tomar las decisiones acorde a lo que ellos crean mejor para el pueblo.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
          "t11instruccion":"“Un buen líder, es aquel capaz de generar otros líderes. Si un líder quiere sostener el liderazgo es un tirano”.<br><br><div style='text-align:right'><i>Papa Francisco I (20/09/2015)</i></div>",
         "t11pregunta":"Lee la frase y selecciona la respuesta correcta.<br><br>¿Qué mensaje puede tener la frase del Papa Francisco I hacia los gobernantes y servidores públicos de tu país?"
      }
   },/////////////5
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Permitir que los ciudadanos reclamen sus derechos y sean parte de la política de su país.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Demandar, crear y ayudar en un mejor desarrollo de nuestra sociedad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Trabajo en conjunto con los servidores públicos y gobernantes para promover un bienestar. ",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "No permitir que las autoridades realicen sus labores.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Cooperar con el Estado para lograr un mejor cuidado de sus habitantes.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Resolver únicamente problemáticas ambientales.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11instruccion":"Las Organizaciones de Sociedad Civil son muy importantes para la sociedad, se conforman de grupos de personas organizados en torno a determinados objetivos o temas de interés. <br>Una de las razones principales por las cuales nacen estas organizaciones, es que en ocasiones los países no cuentan con las herramientas necesarias para garantizar la seguridad de sus habitantes. Por lo tanto, se vuelven importantes para que podamos tener un peso en el desarrollo y cuidado de nuestra sociedad.",
         "t11pregunta":"Lee el texto y selecciona las respuestas correctas. <br><br>¿Qué papel tienen las OSC en nuestra sociedad?"
      }
   },/////////////6
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para que la comunicación entre gobierno y ciudadanos sea posible, es necesaria la participación ciudadana en actividades como: elección de gobernantes, recolección de basura y rendición de cuentas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los gobernantes deben informar a la población por medio de cartas, boletines informativos, juntas, servicios públicos, difusión de medios de comunicación masivos, entre otros.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La participación social democrática indica que, los ciudadanos tienen derecho y obligación de formar parte de los procesos, y hacer uso de vías para informar a las autoridades de sus necesidades.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },///////////7
     {
        "respuestas": [
            {
                "t13respuesta": "Comisión Federal de Electricidad (CFE).  Prestar el servicio público de energía eléctrica con criterios de suficiencia, competitividad y sustentabilidad, comprometidos con la satisfacción de los clientes, con el desarrollo del país y con la preservación del medio ambiente.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Grupo Educare. Desarrollar, a través de la investigación, productos y servicios innovadores para mejorar la educación en el mundo.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Fondo para la paz. Ayudar a que las familias en las regiones de mayor pobreza de nuestro país, mejoren su calidad de vida de forma progresiva y permanente, por medio de varios programas dentro de las áreas de mayor necesidad.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Organización pública."
            },
            {
                "t11pregunta": "Organización privada."
            },
            {
                "t11pregunta": "Organización social."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda<br><br>Lee la misión de cada una de las organizaciones para identificar a qué tipo corresponde.",
        }
    }
];
