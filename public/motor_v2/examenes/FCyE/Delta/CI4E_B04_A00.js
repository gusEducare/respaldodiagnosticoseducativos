json=[
   //1
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='CI4E_B04_A01_01.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='CI4E_B04_A01_02.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='CI4E_B04_A01_03.png'>",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Falta a la ley federal de sanidad animal; se sanciona con multa."
            },
            {
                "t11pregunta": "Falta a las normas y leyes  jurídicas; se sanciona con reclusión."
            },
            {
                "t11pregunta": "Falta a las leyes de tránsito; se sanciona con multa, dependiendo de la gravedad."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen":170,
            "anchoImagen":170
            
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Constitución es el conjunto de normas jurídicas encargadas de estructurar y organizar al Estado.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dentro del Código Penal se señalan los derechos fundamentales de los individuos frente al propio Estado.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Convención de los Derechos del Niño y la Convención acerca de la Eliminación de Todas las Formas de Discriminación contra la Mujer, son ejemplos de leyes internacionales que protegen los derechos de todos los seres humanos.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Situación",
            "evaluable":false,
            "evidencio": false,
        }
    },
   //3
   {
    "respuestas": [
            {
                "t13respuesta": "<img src='CI4E_B04_A03_01.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='CI4E_B04_A03_03.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='CI4E_B04_A03_02.png'>",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Promueve el desarrollo integral de las familias mexicanas y procura el bienestar y desarrollo de los niños."
            },
            {
                "t11pregunta": "Vigila la migración en territorio mexicano y se asegura que se respeten los tratados nacionales e internacionales que salvaguardan los  grupos vulnerables, como por ejemplo los niños."
            },
            {
                "t11pregunta": "Cuida de los derechos humanos en México y se asegura que los  niños sean protegidos por todos los acuerdos  nacionales e internacionales."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen":170,
            "anchoImagen":170
        }
        },
    //4
        {  
            "respuestas":[  
            {  
                "t13respuesta":"Promover la cultura de transparencia en la gestión pública y la rendición de cuentas del gobierno a la sociedad.",
                "t17correcta":"1"
            },
            {  
                "t13respuesta":"Salvaguardar a los ciudadanos en tratos de injusticia por parte de las autoridades.",
                "t17correcta":"1"
            },
            {  
                "t13respuesta":"Vigilar el desempeño de los servidores públicos.",
                "t17correcta":"1"
            },
            {  
                "t13respuesta":"Protección de los datos personales.",
                "t17correcta":"1"
            }
            ],
            "pregunta":{  
            "c03id_tipo_pregunta":"2",
            "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>¿Qué papel ejercen las instituciones señaladas en las imágenes?",
            "t11instruccion": "Hace unos 500 años, Thomas Hobbes, uno de los pensadores más importantes en la historia, decía que el Estado debe garantizar la seguridad y la convivencia sana de su gente; Hoy en día, es indispensable conocer a fondo quiénes son las figuras de autoridad que rigen nuestro país, porque ellos son los responsables de nuestra seguridad.<br><br><img src='CI4E_B04_A04_01.png'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='CI4E_B04_A04_02.png'/>",
            }
        },
        //5
        {  
            "respuestas":[  
            {  
                "t13respuesta":"Se utiliza la violencia para reprimir el diálogo constructivo y ninguna de las partes puede llegar a un acuerdo.",
                "t17correcta":"1"
            },
            {  
                "t13respuesta":"Ninguno, porque la violencia es necesaria para entablar un verdadero diálogo constructivo y equitativo.",
                "t17correcta":"0"
            },
            {  
                "t13respuesta":"Que se está utilizando el diálogo para una propuesta pacífica.",
                "t17correcta":"0"
            },
            {  
                "t13respuesta":"Ninguno, se está entablando un diálogo constructivo para llegar a un acuerdo mutuo.",
                "t17correcta":"0"
            }
            ],
            "pregunta":{  
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Selecciona la respuesta correcta. <br><br>¿Qué problema podemos observar en la primera imagen?",
            "t11instruccion": "Platón, uno de los primeros filósofos en la historia, escribió sus pensamientos en forma de diálogo, debido a que seguía la enseñanza de su maestro Sócrates, quien sostenía que un diálogo adecuado y constructivo era la mejor manera de desarrollar el conocimiento.<br>El diálogo ha formado parte de la convivencia de los hombres por más de 2000 años, es importante aprender la importancia del diálogo, ya que es una herramienta para fomentar el crecimiento personal y la sana convivencia con nuestros semejantes.<br><br>&nbsp;&nbsp;&nbsp;&nbsp;<img src='CI4E_B04_A05_01.png'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='CI4E_B04_A05_02.png'/>",
            }
        },
        //6
        {  
            "respuestas":[  
            {  
                "t13respuesta":"Porque parece que ambos respetan sus ideas, pero a la vez, tratan de construir un nuevo pensamiento a partir de sus diferencias.",
                "t17correcta":"1"
            },
            {  
                "t13respuesta":"Porque solo se están escuchando, pero sin debatir sobre sus ideas.",
                "t17correcta":"0"
            },
            {  
                "t13respuesta":"Porque ambos están creando ideas, pero a partir del conflicto.",
                "t17correcta":"0"
            }
            ],
            "pregunta":{  
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Selecciona la respuesta correcta. <br><br>¿Por qué la segunda imagen parece representar mejor lo que es un diálogo?",
            "t11instruccion": "Platón, uno de los primeros filósofos en la historia, escribió sus pensamientos en forma de diálogo, debido a que seguía la enseñanza de su maestro Sócrates, quien sostenía que un diálogo adecuado y constructivo era la mejor manera de desarrollar el conocimiento.El diálogo ha formado parte de la convivencia de los hombres por más de 2000 años, es importante aprender la importancia del diálogo, ya que es una herramienta para fomentar el crecimiento personal y la sana convivencia con nuestros semejantes.<br><br>&nbsp;&nbsp;&nbsp;&nbsp;<img src='CI4E_B04_A05_01.png'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='CI4E_B04_A05_02.png'/>",
            }
        },
        //7
        {
            "respuestas": [
                {
                    "t13respuesta": "Norma jurídica",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "Norma social",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "Norma de salud",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "Norma moral",
                    "t17correcta": "4"
                },
                            
            ],
            "preguntas": [
                {
                    "t11pregunta": "Vender bebidas alcohólicas a menores de edad."
                },
                {
                    "t11pregunta": "Aprovecharse de otras personas para lograr tus metas."
                },
                {
                    "t11pregunta": "No lavarse las manos después de ir al baño."
                },
                {
                    "t11pregunta": "Utilizar lenguaje obsceno o vulgar."
                },
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Relaciona las columnas según corresponda.<br><br>Relaciona las faltas con el tipo de ley o norma que esté quebrantando.",
              
                
            }
        },
        //8
        {  
            "respuestas":[  
               {  
                  "t13respuesta": "Lograr un ambiente adecuado para el desarrollo pleno de cada individuo.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta": "Administrar los recursos financieros, humanos y materiales en forma equitativa.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta": "Dar prioridad a los problemas más urgentes e importantes de cada comunidad.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta": "Respetar y hacer respetar las leyes.",
                  "t17correcta":"1"
               },
               {  
                  "t13respuesta": "Realizar acciones en beneficio de su bienestar.",
                  "t17correcta":"0"
               },
               {  
                  "t13respuesta": "Proponer y apoyar situaciones de corrupción.",
                  "t17correcta":"0"
               },
               {  
                  "t13respuesta": "Dictar la organización del país.",
                  "t17correcta":"0"
               },
               {  
                  "t13respuesta": "Construir y fomentar las desigualdades en la población.",
                  "t17correcta":"0"
               }
            ],
            "pregunta":{  
               "c03id_tipo_pregunta":"2",
               "t11pregunta":"Selecciona las cuatro funciones que deben cumplir las autoridades.",
            }
         },

];
