json = [
    // Reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Ayuda a comprender las necesidades de los pueblos indígenas, promoviendo la igualdad de condiciones para todos los habitantes del país.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Fomenta  una manera de discriminación cultural, al no tener los mismos estudios que otros candidatos. ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Se crea una preocupación ante las deficiencias de los pueblos, sin llevar a cabo acciones por ellos. ",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Lee el fragmento del siguiente artículo y selecciona la respuesta correcta.<br> <center><img style='width: 300px' src='CI4E_B03_A01_01.png'></center> <br> Según el artículo, ¿qué se puede lograr con involucrar a una  mujer indígena en la política?",
            "t11instruccion":"<h2>Siete frases para conocer a Marichuy, la indígena que aspira a ser presidenta de México</h2><strong>Redacción Animal Político<br>junio 20, 2017, 13:41<br></strong>“Ya es tiempo de que los pueblos hablen y se manifiesten”. Con esta frase se presentó María de Jesús Patricio, Marichuy, una mujer indígena, oriunda de Tuxpan, Jalisco, vocera del Congreso Nacional Indígena (CNI), organismo que reúne las voces de 44 pueblos originarios del país.<br>En entrevista con Carmen Aristegui, Marichuy destacó la importancia de que los pueblos indígenas participen, no con miras de llegar al poder y permanecer ahí, sino mantenerse y hacerlo desde abajo.<br>Por ello, Marichuy, será proclamada candidata independiente a la presidencia de México, convirtiéndola en la primera mujer indígena en aspirar a ese cargo."
        }
    },
    // Reactivo 2
   {
        "respuestas": [{
                "t13respuesta": "Porque los hombres y las mujeres son iguales, comparten los mismos derechos y responsabilidades.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Porque los hombres son mejores que las mujeres en realizar actividades.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Porque las mujeres pueden hacer cosas que los hombres no pueden. ",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Observa la imagen y selecciona la respuesta correcta.<br> <center><img style='width: 300px' src='CI4E_B03_A02_01.png'></center> <br> ¿Por qué la balanza está nivelada?",
           
        }
    },
    //Reactivo 3
    {
        "respuestas": [{
                "t13respuesta": "Reducir la cantidad de luz, agua y aerosoles que utilizamos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Reciclar y reutilizar la basura.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Comprar lo que realmente es necesario.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Llevar bolsas al súper para no generar basura con las bolsas plásticas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Utilizar más el carro y fomentar la producción de gases tóxicos de las grandes empresas.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Tirar toda la basura en un mismo bote.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Lavar el auto con el chorro de la llave del agua para mantenerlo limpio.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Imprimir muchos volantes con mensajes ecológicos.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee el siguiente texto y selecciona las respuestas correctas. <center><img style='width: 300px' src='CI4E_B03_A03_01.png'></center> <br>¿Cuáles de los siguientes incisos representan buenas medidas para detener la contaminación?",
           "t11instruccion":"En los últimos años hemos aumentado de manera peligrosa la cantidad de basura que desechamos en el mundo, debido, quizá, a que no se ha tenido conciencia de las limitaciones de nuestro hogar, el planeta Tierra. Por ello, es necesario que comencemos a crear un cambio en la manera en cómo tratamos a la naturaleza, y así poder garantizar un mejor futuro para todos."
        }
    },
    //Reactivo 4
     {
        "respuestas": [{
                "t13respuesta": "Crecimiento económico, intelectual, social, musical, gastronómico; además de tradiciones y costumbres propias, lo que hace al país único. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Lograr una diversidad gastronómica que nos permita ser reconocidos en el mundo.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Tener muchas playas, bosques, selvas que permiten el crecimiento turístico del país. ",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Lee el siguiente texto y selecciona la respuesta correcta. <br> <br>¿Cuál es la importancia de la diversidad cultural en el país?",
            "t11instruccion":"México es un país con siglos de tradición y de historia, donde el mestizaje ha tenido un papel primordial. <br>Existen costumbres, tradiciones, platillos gastronómicos, etc., conocidos como productos mexicanos, con “denominación de origen” en México,  es el caso del pulque, el mezcal o la talavera, y algunos otros que fueron producto de la conquista española, como la religión católica o la entrada de insumos, algunos de ellos son: el cerdo, el limón, la canela, etc. "
        }
    },
    //Reactivo 5
    {
        "respuestas": [{
                "t13respuesta": "Sus compañeros pueden ayudarle a comprender las palabras que no entiende. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Pedir que los libros que utilizan aparezcan en ambas lenguas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Compartir las palabras que sabe de náhuatl en su clase para que no se pierdan.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Debe adaptarse al nuevo entorno y aprender sólo el idioma español.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "El profesor debe pedir que todos tengan su libro únicamente  en náhuatl.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "No se puede hacer nada, la clase debe continuar en español porque así lo marcan los libros. ",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee el siguiente fragmento del cuento “El secreto de Cristina”  de CONAPRED, y selecciona las respuestas correctas. <br>¿Qué acciones se pueden realizar para  lograr su inclusión en el aula?",
           "t11instruccion":"...Cuando sonó el timbre todos se fueron. Yo me acerqué al profesor y le dije que ahí en Tihuiztlán se había quedado mi libro. <br>Sí, me dio otro. Pero no era igual. Estaba todo escrito en castilla, no como el mío, que viene en castilla y en náhuatl. Yo sé escribir y leer en los dos idiomas, porque mi escuela es bilingüe y enseñan las dos. Pero la abuela siempre me ponía más tarea en lengua, y también me hacía repetir muchas veces las lecturas en lengua. Por eso leo y escribo mejor en náhuatl. <br>¿Para qué me habrá acostumbrado mi abuela a tanto hablar en lengua, sí acá solamente estudian puro español? Era feo que no hubiera 10 páginas donde estuvieran las palabras con las que me hablaba mi abuela. Ella decía que eran las palabras de sus abuelos y de nuestro pueblo, que si no las hablábamos nosotros, se iban a perder. Tenía razón mi abuela: en el libro de la escuela de Kipatla esas palabras ya se habían perdido..."
        }
    },
    //Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "Ayuda a practicar la tolerancia, promoviendo una convivencia armoniosa.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Conocer y compartir ideas con personas diferentes, enriqueciendo nuestra cultura. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Fomenta un sentimiento de fraternidad en la sociedad. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Promueve la desigualdad social y los malos tratos. ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Permite tolerar las diferentes culturas y dar un trato discriminatorio.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Enriquece los conocimientos y la inteligencia. ",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee el texto y selecciona las respuestas correctas. <br>¿Cuál es la importancia del respeto entre diferentes grupos de personas?",
           "t11instruccion":"La dignidad es la cualidad que nos permite ver nuestro valor como personas, implica que nos comportemos con responsabilidad, seriedad, con respeto hacia nosotros y hacia los demás.<br>Por lo tanto, es necesario aprender a relacionarnos de la manera más respetuosa con las personas que nos rodean, a pesar de las múltiples diferencias que se puedan tener con ellos."
        }
    },
    //Reactivo 7
    {  
      "respuestas":[
         {  
            "t13respuesta": "Usar medios electrónicos para archivar información.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Reutilizar las hojas de papel.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Elaborar papel reciclado.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Utilizar solamente una vez el papel.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Comprar más papel cuando se acabe.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Regalar la comida a instituciones que lo permitan.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Utilizarlo como composta.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Preparar las cantidades exactas. ",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Tirar la comida sobrante en contenedores adecuados.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Usar una bolsa negra y colocar todos los sobrantes.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Denunciar las fugas a la comisión correspondiente.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Utilizar el agua que se tira para regar árboles y plantas.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Cerrar las llaves de agua cuando no se están utilizando. ",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Tratar de no pisar los charcos formados.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Jugar con el agua durante mucho tiempo. ",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Observa las imágenes y señala tres medidas que contribuyan a lograr un uso racional de los recursos naturales.<br><center><img style='width: 300px' src='CI4E_B03_A07_02.png'></center>",
                         "<center><img style='width: 300px' src='CI4E_B03_A07_03.png'></center><br>",
                         "<center><img style='width: 300px' src='CI4E_B03_A07_01.png'></center><br>"],
         "preguntasMultiples": true
      }
   },
    //Reactivo 8
    {
        "respuestas": [{
                "t13respuesta": "Karen es ingeniera civil y tiene a su cargo un grupo de hombres y mujeres, es líder de la construcción del nuevo centro comercial.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "José realiza las labores del hogar y lleva a sus hijos a la escuela, trabaja desde casa como diseñador editorial.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Carlos y su pequeño hijo abordan un transporte público al ingresar otra persona le ceden su lugar.  ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Una empresa tiene la filosofía de no contratar mujeres, porque pierden horas laborales por estar al pendiente de sus hijos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Mariana ha sido despedida de su trabajo como secretaria por quedar embarazada.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "A Edgar le prohibieron estudiar gastronomía, porque solo las mujeres deben cocinar. ",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee las situaciones y selecciona aquellas que son ejemplo de igualdad y equidad."
        }
    }
];
