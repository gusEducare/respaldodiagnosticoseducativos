json=[
  //1
  {  
      "respuestas":[  
         {  
            "t13respuesta":"Es una persona enojada.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que debemos de meditar nuestras opciones antes de reaccionar de manera agresiva a causa de nuestro enfado.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Que debemos sacar nuestra ira cuando se presente una situación que nos incomode en exceso, sin importar si afectamos a terceras personas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que detenernos a  reflexionar sobre nuestras emociones puede lastimarnos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa el texto y la imagen presionando \"+\", analiza y selecciona la respuesta correcta:",
         "t11instruccion":"Aristóteles, un sabio de una época antigua, dijo hace unos 2500 años que enojarse es algo muy sencillo, lo difícil es hacerlo en el momento adecuado, con la persona correcta y de modo apropiado. Por lo tanto, debemos aceptar el enojo como parte de nuestras emociones, pero tenemos que aprender a controlarlo y expresarlo de la mejor manera posible.<br><br><center><img src='CI4E_B02_A01.png'/><center/>"
      }
   },
   //2
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Golpearse y ver quién gana. ",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Gritarse y maldecir.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Turnarse la patineta.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Jugar juntos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Comprar otra patineta.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Calmarse y hablar de lo que pasa.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Decidir quién se queda con la patineta con un \“volado\”.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Observa la imagen presionando \"+\", analiza y selecciona las 3 mejores formas de solucionarlo:",
         "t11instruccion":"<center><img src='CI4E_B02_A02_01.png'/><center/>"
      }
   },
   //3
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Hablar todos al mismo tiempo. ",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Tomar un tiempo a solas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"“Explotar”, gritar y maldecir.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Correr al adolescente de la casa.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Hablar y escuchar tranquilamente.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Castigar al adolescente.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Compartir cómo se sienten al respecto. ",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Observa la imagen presionando \"+\", analiza y selecciona las 3 mejores formas de solucionarlo:",
         "t11instruccion":"<center><img src='CI4E_B02_A02_02.png'/><center/>"
      }
   },
   //4
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Que debemos ejercer nuestra libertad sin importar a quién o qué podamos perjudicar. Por ejemplo: golpear a mi amigo si voy perdiendo en un juego es una manera correcta de ejercer mi libertad.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"No tiene ningún mensaje.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que nuestra libertad siempre está ligada con la libertad de nuestros semejantes, por ejemplo: tengo la libertad de golpear a mi amigo si voy perdiendo en un juego, pero sé que mi acción perjudica a otra persona.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Que no podemos actuar libremente porque debemos obedecer las normas sociales, escolares,  y familiares para lograr una convivencia sana con los demás.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa el texto y la imagen presionando \"+\", analiza y selecciona la respuesta correcta:",
         "t11instruccion":"<b>Jean Paul Sartre, es uno de los pensadores más importantes del siglo pasado, él expuso que nuestra libertad siempre está ligada con la libertad de las otras personas, pues siempre estamos en contacto y en relación con otros.</b><br><br><center><img src='CI4E_B02_A03.png'/><center/>"
      }
   },
   //5
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El niño de la imagen es víctima de alguna injusticia.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la imagen presionando \"+\", analiza y responde verdadero (V) o falso (F):",
            "descripcion":"Situación",
            "evaluable":false,
            "evidencio": false,
            "t11instruccion":"<center><img src='CI4E_B02_A04.png'/><center/>"
        }
    },
    //6
     {  
      "respuestas":[  
         {  
            "t13respuesta":"No hacer nada y esperar que el mundo te dé las herramientas para cumplir tus metas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Dedicar esfuerzo, disciplina, dedicación y horas de trabajo con el fin de cumplir tus objetivos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Resignarte a que tus metas no pueden ser logradas y pensar en unas nuevas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Tener dinero y acceder a los medios necesarios para lograr tus objetivos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto presionando \"+\", reflexiona y selecciona la respuesta correcta:",
         "t11instruccion":"<b>Una meta es \“el fin al que se dirigen las acciones o deseos de una persona\” y es nuestra responsabilidad aprender cómo alcanzarlas. Nuestra libertad nos permite elegir el camino por el cual llegaremos a nuestro destino, sin embargo, a lo largo del trayecto, es normal que encuentres una serie de dificultades. Por eso necesitamos aprender estrategias que nos ayuden a sobreponernos a ellas.</b>"
      }
   },
   //7
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Gastar todo tu dinero en juguetes y golosinas, sin importar si tienes que pedir más dinero a tus padres.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Explotar los recursos del hogar: agua, luz y gas.",
            "t17correcta":""
         },
         {  
            "t13respuesta":"Ahorrar tu dinero de la escuela y otros incentivos de tus padres, evitando comprar comida chatarra.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Realizar actividades recreativas de bajo costo y gastar tu dinero sabiamente en cosas que te ayudarán a lograr tus metas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Proponer una meta para tus ahorros.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Tener dinero y acceder a los medios necesarios para lograr tus objetivos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Lee el texto presionando \"+\" y señala las 3 opciones que indican un buen manejo de tu dinero:",
         "t11instruccion":"<b>Una de las herramientas para lograr nuestras metas es el dinero. Como niño, tus recursos monetarios están ligados fuertemente a tus padres; por lo tanto, tus metas pueden estar conectadas a la disponibilidad de tu familia. Sin embargo, existen diferentes maneras en las que puedes ayudar en los gastos de tu hogar y en las que puedes ser dueño de tu propio dinero.</b>"
      }
   },
   //8
  {  
      "respuestas":[
         {  
            "t13respuesta": "La imagen 1 muestra una injusticia, pero la 2 no.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La imagen 1 no muestra una injusticia, pero la 2 sí.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Las dos imágenes muestran una injusticia.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Ninguna de las imágenes muestra injusticias.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Porque los niños están copiando ideas que no son suyas.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque copiar permite tener la misma información.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque recibirán un castigo.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque hay esfuerzo y trabajo.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa el texto y la imagen presionando \"+\", analiza lo expuesto y selecciona la respuesta correcta:<br><br>Después de observar ambas imágenes se puede asegurar que:",
                         "¿Por qué piensas que puede existir alguna clase de injusticia?"],
         "preguntasMultiples": true,
         "t11instruccion":"<b>La justicia es una virtud que debe ser fomentada constantemente en el aula, debido a que la institución cuenta con la responsabilidad de lograr que los niños se conviertan en buenos ciudadanos y que sean productivos para la sociedad. Por lo tanto, es importante impartir castigos a aquellos que están cometiendo alguna injusticia, además de orientarlos para que el alumno pueda aprender que la justicia es un hábito que se debe practicar para lograr una mejor convivencia.</b><br><br><center><img src='CI4E_B02_A08_01.png' style='width:400px; height:300px;'/><img src='CI4E_B02_A08_02.png' style='width:400px; height:300px;'/><center/>"
      }
   },
];
