json=[  
   {
        "respuestas": [
            {
                "t13respuesta": "<p>La escuela de Salamanca en España, declara que los indígenas tienen “derechos naturales”.<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>Guerras de independencia en América Latina, redacción de sus Constituciones.<\/p>",
                "t17correcta": "4",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Imperaba el esclavismo y la discriminación, en especial para las mujeres y los extranjeros.<\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>Se comienza a considerar a los esclavos como seres humanos.<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 4"
            },
            {
                "t13respuesta": "<p>Después de la Revolución Francesa y la independencia de E.U.A, se reconocen legalmente los derechos humanos y su carácter irrenunciable.<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 5"
            },
            {
                "t13respuesta": "<p>Creación de la ONU, y de múltiples declaraciones de derechos individuales, sociales y culturales.<\/p>",
                "t17correcta": "5",
                etiqueta:"Paso 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Ordena los siguientes elementos. <br /><br />¿Cuál es la historia de los Derechos Humanos?<\/p>",
            "random": true
        }
    },
    ////////////////////////////////////////11111111111111111111111111111111111111111!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    {
        "respuestas": [
            {
                "t13respuesta": "Basados en el principio de libertad.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Basados en el principio de igualdad.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Basados en el principio de solidaridad.",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho a un nivel de vida digno.",
                "valores": ['', '', ''],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho a la identidad nacional y cultural.",
                "valores": ['', '', ''],
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho a opinar y expresarse.",
                "valores": ['', '', ''],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho a formar sindicatos para la defensa de sus intereses laborales.",
                "valores": ['', '', ''],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho a elegir con quién casarse y el número de hijos que desea tener.",
                "valores": ['', '', ''],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho a la educación en todas sus modalidades.",
                "valores": ['', '', ''],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Derecho a la independencia política y económica.",
                "valores": ['', '', ''],
                "correcta"  : "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda, de acuerdo a si está basado en el principio de libertad, de igualdad o de solidaridad.<\/p>",
            "descripcion": "Derechos",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    ////////////////////////////////////////222222222222222222222222222222222222222222222222/////////////////////////////////////
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Todos somos partícipes de la democracia por medio de nuestro voto, y somos libres de votar por el o los candidatos que creamos capaces de gobernar.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Una de las manifestaciones de la democracia es el voto, sin embargo, en ocasiones existen situaciones  de engaño y corrupción que no permiten que este sea libre.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Al votar todos al mismo tiempo no hay un orden, por lo que es necesario que alguna autoridad detenga el paso de todos los votos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<center><img src='CI8E_B04_A03.png'></center><br/>¿Qué mensaje transmite la imagen? "
      }
   },
   /////////////////////////////////////////3333333333333333333333333333333//////////////////////////////////////////////////
    {  
      "respuestas":[
         {  
            "t13respuesta": "Principio de mayoría.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Pluralismo.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Legalidad",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         //////////////////////////////////
         {  
            "t13respuesta": "Principio de mayoría.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Principio de soberanía nacional.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Diálogo.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         /////////////////////////////////
         {  
            "t13respuesta": "Participación ciudadana.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Diálogo",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Principio de soberanía nacional.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta: <br /><br />Es la diversidad de intereses y de ideologías que se deben de negociar de forma pacífica en beneficio de la ciudadanía:", 
                         "<br>Se logra cuando hay una propuesta aceptada por un número mayor de personas y se establece al haber por lo menos un voto de diferencia a favor, respetando la decisión de la mayoría.",
                         "<br />Permite el intercambio de ideas y experiencias, siendo el principal ejercicio para llegar a un acuerdo."],
         "preguntasMultiples": true,
        
         "t11instruccion":"En la democracia representativa como la de nuestro país existen diferentes principios y  procedimientos, los cuales son necesarios, para establecer acuerdos y tomar decisiones en asuntos de interés colectivo."
      }
   },
   /////////////////////////////////////////////////////4444444444444444444444444444444444/////////////////////////////////
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia reconoce que la ciudadanía tiene participación en las decisiones políticas que se toman en un país.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia indirecta hace referencia a que las decisiones le pertenecen directamente al pueblo y se ejercen a través de asambleas en las que participan todos los ciudadanos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia directa o representativa es aquella en la que las decisiones y el gobierno se ejercen a través  de los cuales hacen valer su opinión y postura.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El objetivo de la democracia es la participación ciudadana y la convivencia pacífica.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "anchoColumnaPreguntas": 60,
            "evaluable":false,
            "evidencio": false
        }
    },
    ////////////////////////////////////////////////55555555555555555555555555555555555555////////////////////////////////
    {
        "respuestas": [
            {
                "t13respuesta": "Garantizan que las personas y comunidades tengan acceso a la cultura y de sus componentes en condiciones de igualdad, dignidad humana y no discriminación.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Aquellos que la Constitución le otorga a los ciudadanos al cumplir la mayoría de edad, para participar en los asuntos públicos del país.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Permiten que los ciudadanos tengan condiciones económicas y de acceso a bienes necesarios para una vida digna.",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Políticos"
            },
            {
                "t11pregunta": "Sociales"
            },
            {
                "t11pregunta": "Culturales"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.",
            "t11instruccion":"Como aprendiste durante el tema, la democracia no se trata únicamente de emitir un voto para elegir a nuestros gobernantes, si bien, es una de las características primordiales, también es necesario ejercer nuestros derechos y hacerlos valer."
        }
    },
    ////////////////////////////////////////////////////66666666666666666666666666666666666//////////////////////////////
    {
      "respuestas": [
          {
              "t13respuesta": "CI8E_B04_A07_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B04_A07_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B04_A07_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "CI8E_B04_A07_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "CI8E_B04_A07_08.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B04_A07_09.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B04_A07_06.png",
              "t17correcta": "6",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B04_A07_07.png",
              "t17correcta": "7",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra la imagen al espacio que corresponda. <br /><br />Completa el mapa conceptual de los mecanismos de representación de los ciudadanos en el gobierno democrático.<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"CI8E_B04_A07_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["0", "126,134", "cuadrado", "118, 79", ".","transparent"]},
          {"Contenedor": ["1", "126,262", "cuadrado", "118, 79", ".","transparent"]},
          {"Contenedor": ["2", "214,5", "cuadrado", "118, 79", ".","transparent"]},
          {"Contenedor": ["3", "214,134", "cuadrado", "118, 79", ".","transparent"]},
          {"Contenedor": ["4", "214,391", "cuadrado", "118, 79", ".","transparent"]},
          {"Contenedor": ["5", "214,520", "cuadrado", "118, 79", ".","transparent"]},
          {"Contenedor": ["6", "302,5", "cuadrado", "118, 79", ".","transparent"]},
          {"Contenedor": ["7", "302,262", "cuadrado", "118, 79", ".","transparent"]}
      ]
  },
  //////////////////////////////////////////////7777777777777777777777777///////////////////////////////////////////
   {
        "respuestas": [
            {
                "t13respuesta": "Rendición de cuentas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Transparencia.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Acceso a la información.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La transparencia y la rendición de cuentas son componentes esenciales que fundamentan el gobierno democrático."
            },
            {
                "t11pregunta": "La información es pública para aquellos interesados en revisarla, analizarla y en dado caso, utilizarla como un mecanismo para sancionar."
            },
            {
                "t11pregunta": "Derecho y garantía de conocer las ideas, opiniones, hechos o datos que produce el gobierno."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta":"Relaciona las columnas según corresponda."
        }
    }
    //////////////////////////////////(88888888888888888888888888888//////////////////////////////////7)
];