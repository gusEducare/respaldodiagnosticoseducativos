json=[
   //1  
   {
        "respuestas": [
            {
                "t13respuesta": "Favorecen",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Obstaculizan",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tener una casa y  alimentación básica.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser inmigrante y que no se acredite su nacionalidad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Contar con una familia que procure sus necesidades.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Vivir en un hogar donde sea violentado.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Escuelas donde sólo se aceptan alumnos de clase alta.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Vivir en una comunidad  de armonía y sana convivencia.",
                "correcta"  : "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Lee las situaciones y selecciona la opción correcta  de acuerdo a si favorecen u obstaculizan el desarrollo integral de los adolescentes:<\/p>",
            "descripcion":"Situaciones",
            "evaluable":false,
            "evidencio": false
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='CI8E_B02_A01_01.png' />  ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='CI8E_B02_A01_02.png'/>",
                "t17correcta": "1"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Principal entidad gubernamental encargada de promover y proteger los derechos humanos en principalmente ante abusos cometidos por las personas, funcionarios públicos o por el estado."
            },
            {
                "t11pregunta": "Su labor se guía por la Convención sobre los Derechos del Niño (CDN) de las Naciones Unidas. Se encargan de defender todos los derechos de la infancia para que todos los niños y niñas sean felices."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Lee el texto presionando \"+\" y relaciona la descripción de cada institución con su logo:",
            "t11instruccion":"Como adolescente tienes derechos y responsabilidades amparados por la ley.  Para velar por ellos, existen varias organizaciones tanto públicas como gubernamentales."
        }
    },
    //3
    {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Aceptar las opiniones y formas ser de las personas.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Dialogar asertivamente con los demás.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Decir mentiras en toda situación.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Estar dispuesto a negociar un conflicto buscando siempre tener razón.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Utilizar responsablemente las redes sociales.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Resolver las situaciones problemáticas con agresiones y malas palabras.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Formar parte de grupos que actúen positivamente en favor del respeto a los derechos humanos.<\/p>",
            "t17correcta":"1"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las 4 acciones que promueven una sana convivencia en los diversos contextos:"
      }
   },
   //4
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>No lograba estar en un grupo donde sus formas de pensar fueran similares a las de ella.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>El grupo al que pertenecía no le brindaba comodidad ni seguridad.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Su grupo de amigos no la apoyaba ante situaciones de conflicto o tristeza.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Era demasiado popular.<\/p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el siguiente fragmento del Diario de Ana Frank presionando \"+\" y selecciona la respuesta correcta:<br><br>¿Por qué Ana Frank no se sentía parte de un grupo de amigos?",
         "t11instruccion":"<b>He llegado al punto donde nace toda esta idea de escribir un diario: no tengo ninguna amiga. Para ser más clara tendré que añadir una explicación, porque nadie entenderá cómo una chica de trece años puede estar sola en el mundo. Es que tampoco es tan así: tengo unos padres muy buenos y una hermana de dieciséis, y tengo como treinta amigas en total, entre buenas y menos buenas. Tengo un montón de admiradores que tratan de que nuestras miradas se crucen o que, cuando no hay otra posibilidad, intentan mirarme durante la clase a través de un espejito roto. Tengo a mis parientes, a mis tías, que son muy buenas, y un buen hogar. Al parecer no me falta nada, salvo la amiga del alma. Con las chicas que conozco lo único que puedo hacer es divertirme y pasarlo bien. Nunca hablamos de otras cosas que no sean las cotidianas, nunca llegamos a hablar de cosas íntimas. Y ahí está justamente lo importante de la cuestión. Tal vez la falta de confidencialidad sea culpa mía, el asunto es que las cosas son como son y lamentablemente no se pueden cambiar. De ahí este diario.</b><br><br><i>Frank, Ana. (2017). El Diario de Ana Frank. México: Mirlo.</i>"
      }
   },
   //5
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Tener una  baja autoestima.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Poner límites y evitar chantajes.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Permitir que otros decidan por mi. <\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Ser firme en mis ideas y valores.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Tener seguridad y respeto por mí mismo.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Evadir las situaciones de presión.<\/p>",
            "t17correcta":"0"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Lee el texto presionando \"+\" y selecciona todos los elementos que te permiten ser asertivo:",
         "t11instruccion":"<b>Los adolescentes se enfrentan a diversas situaciones en su vida. Deben ser capaces de tomar decisiones por sí mismos, sin dejarse influenciar ni presionar por amigos o familiares y siempre bien informados; pues sólo así sabrán cómo y  cuándo decir no.</b>"
      }
   },
   //6
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Lo correcto es que las mujeres realicen únicamente labores  del hogar, mientras los hombres salen a trabajar por horas para ser el sustento.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Existen estereotipos que definen lo que una mujer o un hombre están destinados a ser y que solo tienen cualidades, actitudes y habilidades para llevar a cabo ciertas actividades.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Mujeres y hombres tienen igualdad de condiciones para realizar las actividades que les proporcionen bienestar.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Que las mujeres deben liberarse del estereotipo “ama de casa” y buscar un empleo.<\/p>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa la imagen presionando \"+\", analiza y selecciona la respuesta correcta:<br><br>¿Qué mensaje está transmitiendo la imagen?",
         "t11instruccion":"<br/><center><img style='height: 300px; width:500px;' src='CI8E_B02_A06.png'>"
      }
   },
   //7
   {
      "respuestas": [
          {
              "t13respuesta": "CI8E_B02_A08_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B02_A08_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B02_A08_04.png",
              "t17correcta": "2,3",
              "columna":"1"
          },
          {
              "t13respuesta": "CI8E_B02_A08_05.png",
              "t17correcta": "2,3",
              "columna":"1"
          },
          {
              "t13respuesta": "CI8E_B02_A08_06.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B02_A08_07.png",
              "t17correcta": "5,6",
              "columna":"0"
          },
          {
              "t13respuesta": "CI8E_B02_A08_09.png",
              "t17correcta": "5,6",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Completa el mapa conceptual, arrastrando los conceptos al lugar correcto:<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"CI8E_B02_A08_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":false,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "120,13", "cuadrado", "102, 60", ".","transparent"]},
          {"Contenedor": ["", "303,79", "cuadrado", "102, 60", ".","transparent"]},
          {"Contenedor": ["", "205,223", "cuadrado", "102, 60", ".","transparent"]},
          {"Contenedor": ["", "285,223", "cuadrado", "102, 60", ".","transparent"]},
          {"Contenedor": ["", "120,314", "cuadrado", "102, 60", ".","transparent"]},
          {"Contenedor": ["", "205,528", "cuadrado", "102, 60", ".","transparent"]},
          {"Contenedor": ["", "285,528", "cuadrado", "102, 60", ".","transparent"]}
      ]
  },
  //8
   {  
      "respuestas":[  
         {  
            "t13respuesta":"<p>Derecho a una sexualidad placentera y recreacional, independientemente de la reproducción.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Derecho a la libertad de expresión.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Derecho a los beneficios del progreso científico.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Derecho a no morir por causas evitables relacionadas con el embarazo o parto.<\/p>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<p>Derecho a la igualdad sustantiva.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Derecho a la vida, a la supervivencia y al desarrollo.<\/p>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<p>Derecho a la autonomía, integridad y seguridad del propio cuerpo.<\/p>",
            "t17correcta":"1"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todos los derechos sexuales y reproductivos:"
      }
   }
  
];