json=[  

{  
      "respuestas":[  
         {  
            "t13respuesta":     "Trabajo decente y crecimiento económico.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Paz, justicia e instituciones sólidas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Igualdad de género.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Educación de calidad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Proporcionar información de hechos financieros, económicos y sociales.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Fomentar el respeto de los derechos y libertades.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Garantizar el cumplimiento de los derechos de la infancia.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona las respuestas correctas. <br><br>¿Cuáles son  los objetivos del desarrollo sostenible que planteó la Organización de las Naciones Unidas? "
      }
   },/////2
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EResponsabilidad social.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAcción conjunta.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDesarrollo sostenible.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EDesarrollo sostenible.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAcción conjunta.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EResponsabilidad social.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         
         "t11pregunta":["Selecciona la respuesta correcta. <br><br>Compromiso u obligación de los miembros de una sociedad de contribuir para tener una sociedad más justa y proteger el ambiente.",
         "Idea de satisfacer las necesidades de la sociedad actual sin comprometer la estabilidad del futuro, manteniendo un equilibrio a fin de desarrollar estrategias en pro del bienestar del mundo."]
      }
   },//////////3

 {
        "respuestas": [
            {
                "t13respuesta": "<i>Rol</i>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<i>Estereotipo</i>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<i>Prejuicio</i>",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<i>Función que una persona desempeña en una situación o grupo.</i>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<i>“Los hombres no deben llorar”.</i>",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<i>Opinión sobre algo o alguien, a priori, comúnmente desfavorable y acerca de algo que no se conoce lo suficiente.</i>",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<i>Modelo que establece las cualidades ideales de un objeto, persona o conducta.</i>",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<i>“Ella será la líder del equipo”.</i>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<i>Sentimientos negativos hacia otras personas por su afiliación a un grupo determinado.</i>",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada a expresión. ",
            "descripcion": "<i>Frases</i>",   
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable"  : true
        }        
    },///////////4
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEs necesario que hombres y mujeres estén unidos para  hacer valer sus derechos y oportunidades, teniendo como base la igualdad y equidad.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENecesitamos de una pareja para lograr lo que nos proponemos.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ESi nos aprovechamos de las personas lograremos establecer un equilibrio que nos permita a todos desarrollarnos. \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa la imagen y selecciona la respuesta correcta. <br><center><img src='CI8E_B05_A04.png'/></center><br>¿Qué mensaje nos transmite la imagen?"
      }
   },////////////5
   {
        "respuestas": [
            {
                "t13respuesta": "Acosar con insistencia a una persona por medio de insultos, agresión verbal o física, violencia en todas sus modalidades.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Advertencia con malicia hacia otras personas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Aislar a una persona o grupo del lugar que ocupaba, o prescindir de él o ello.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Tratar de manera diferente o despectiva a una persona con el pretexto de sus características o diferencias.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Hostigamiento"
            },
            {
                "t11pregunta": "Amenaza"
            },
            {
                "t11pregunta": "Exclusión"
            },
            {
                "t11pregunta": "Discriminación"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },///////////6
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Capacidades y habilidades como: escuchar, aprender, comprender, analizar, etc., nos permiten lograr acuerdos y mantener el equilibrio.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El diálogo colaborativo, la argumentación, la conciliación y la exclusión, son herramientas que nos permiten resolver las diferencias y problemas que surjan en la comunidad escolar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las principales causas que provocan un conflicto son: falta de comunicación, ausencia de confianza, falta de respeto y violación del reglamento escolar.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.<br><br>En los grupos se suelen manifestar diferentes ideales o pensamientos, causando conflictos, controversia o incomprensión. Para eso, es necesario que se empleen acciones no violentas para solucionar los conflictos.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    }
];