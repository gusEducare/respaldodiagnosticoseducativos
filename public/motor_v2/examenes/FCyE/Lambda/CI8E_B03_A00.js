
json=[
    //reactivo 1
    {
        "respuestas": [
            {
                "t13respuesta": "Valoración de todo aquello que se capta a través de los sentidos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Guías de comportamiento que regulan la conducta de las personas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Actitudes y conductas que una determinada sociedad considera indispensables para la convivencia.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Conceptos, creencias y actividades  que forman parte de la expresión social.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Valorar todo aquello que es aplicable, adaptable y funcional para una situación.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Estéticos"
            },
            {
                "t11pregunta": "Éticos"
            },
            {
                "t11pregunta": "Morales"
            },
            {
                "t11pregunta": "Culturales"
            },
            {
                "t11pregunta": "Económicos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona  cada valor con su definición. <br><br>Los valores nos ayudan a convivir sanamente con nuestros semejantes, pues son la base para determinar si nuestras acciones son correctas."
        }
    },
    //Reactivo 2
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Reconocer nuestros valores.<\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>Evaluar la situación.<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Priorizar los valores morales sobre los valores personales.<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>Determinar las acciones más viables.<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Lee el texto y ordena los pasos del 1 al 4 para tomar buenas decisiones.<br><br>Cuando debemos tomar decisiones importantes o relevantes, es necesario reflexionar antes, esto nos permite resolver conflictos de forma razonada y con argumentos que indiquen que hemos tomado decisiones que nos favorecen, o bien, que favorecen a la mayoría.<\/p>",
        }
    },
    //Reactivo 3
    {
        "respuestas": [
            {
                "t13respuesta": "Dependencia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Independencia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Interdependencia",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Combinar los esfuerzos propios con los esfuerzos de otros para lograr un éxito mayor.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Necesitar de otros para conseguir un objetivo propio.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lograr una meta gracias al esfuerzo propio.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se logra después de alcanzar la independencia.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Frases como “Yo puedo hacerlo”, “Yo soy responsable”, “Yo lo voy a lograr” son características de esta fase.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fase en la cual al nacer, sin el cuidado de otras personas, no se podría sobrevivir.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hacerse cargo de su persona en todos los ámbitos: físico, mental, emocional y económico.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la fase que corresponde. <br><br>En el camino a la madurez, los adolescentes atraviesan por varias fases, en el proceso van adquiriendo un pensamiento propio y crítico, pues de no ser así, tendrían el riesgo de no asumir la responsabilidad de sus acciones y pensamientos.<\/p>",
            "descripcion": "Características",   
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable"  : true
        }        
    },
    //Reactivo 4
    {
        "respuestas": [{
                "t13respuesta": "Grupo de personas con rasgos culturales determinados y comunes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Acción conjunta para solicitar algún tipo de medida por parte del Estado en lo que respecta a un tema en particular.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Comparten normas, hábitos, intereses, etc.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "No presentan una necesidad de interacción.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Identidad común en diferentes contextos.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Viven en distintos territorios, por lo cual fomentan la discriminación.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Lee la frase y selecciona las respuestas correctas.<br><br>“Solamente una colectividad que se rija por el respeto a la vida humana, la tolerancia y la libertad puede sacar al mundo del cruel abismo en que los poderes políticos la conducen.” <br><strong>Aldous Huxley</strong><br><br>Selecciona las características de la colectividad. ",
           
        }
    },
    //Reactivo 5
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El género no hace referencia al hombre y la mujer, sino a lo masculino y lo femenino, esto es, a las cualidades y características que la sociedad atribuye a cada sexo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La igualdad de género existe cuando las mujeres y los hombres no gozan de los mismos derechos y oportunidades en la vida civil y política.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La equidad de género hace referencia a cualquier exclusión o restricción basadas en las funciones y las relaciones de género e impide que una persona disfrute plenamente de los derechos humanos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<br><br>El género son las ideas, creencias y atribuciones sociales construidas en cada cultura en un momento histórico; muchas veces, la sociedad toma como base estas creencias para fomentar estereotipos y discriminación, como el decir que las mujeres son más débiles que los hombres, o que estos no deben llorar.<\/p>",
            "descripcion": "Aspectos a valorar",   
            "variante": "editable",
            "evaluable"  : true
        }        
    },
    //Reactivo 6
    {
        "respuestas": [{
                "t13respuesta": "La discriminación se manifiesta de diferentes formas, siendo evidente la falta de juicio y valores de la sociedad, por lo cual, es necesario cambiar la forma de actuar y pensar de la misma, pues de no ser así, no se puede actuar para erradicarlas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Es necesario que se mantenga la cultura actual, pues de esta forma se podrán eliminar todas las formas de discriminación existentes.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "La sociedad cuenta con una sana convivencia, por lo que la frase se equivoca al decir que se debe cambiar, las razones de la discriminación es el respeto y la tolerancia.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Lee la frase y selecciona la respuesta correcta.<br><br>“Sexual, racial, violencia de género y otras formas de discriminación y violencia en una cultura no pueden ser eliminados sin cambiar la cultura”<br><strong>Charlotte Bunch</strong><br><br>Con base en lo que aprendiste durante el tema, ¿qué mensaje transmite la frase?",
           
        }
    },
    //Reactivo 7
    {
        "respuestas": [
            {
                "t13respuesta": "Conexión que existe entre nuestra vida y la de los demás, al necesitar de otros para lograr un objetivo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Intercambio sincero de ideas que permite a los individuos interactuar.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Innovación al enfrentar desafíos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Trabajo en equipo para un beneficio común y preparación para posibles riesgos posteriores.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Apoyo y ayuda a personas que se encuentran en una situación vulnerable.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Interdependencia"
            },
            {
                "t11pregunta": "Diálogo"
            },
            {
                "t11pregunta": "Divergencia"
            },
            {
                "t11pregunta": "Colaboración"
            },
            {
                "t11pregunta": "Solidaridad"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona  cada concepto con su definición."
        }
    },
    //Reactivo 8
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Manejo de emociones<\/p>",
                "t17correcta": "0",
                etiqueta:"Paso 1"
            },
            {
                "t13respuesta": "<p>Clarificar el problema<\/p>",
                "t17correcta": "1",
                etiqueta:"Paso 2"
            },
            {
                "t13respuesta": "<p>Diálogo<\/p>",
                "t17correcta": "2",
                etiqueta:"Paso 3"
            },
            {
                "t13respuesta": "<p>Negociación<\/p>",
                "t17correcta": "3",
                etiqueta:"Paso 4"
            },
            {
                "t13respuesta": "<p>Conciliación<\/p>",
                "t17correcta": "4",
                etiqueta:"Paso 5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Ordena los pasos del 1 al 5 para resolver un conflicto de una manera no violenta. <br><br>Al enfrentar un conflicto es importante no actuar con las emociones del momento, para esto, existe una serie de elementos que ayudan a  resolver los conflictos de manera no violenta, evitando los malos entendidos y la falta de respeto.<\/p>",
        }
    }
];