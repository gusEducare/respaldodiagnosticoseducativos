
json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Con la experiencia y las elecciones que realizamos.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Con nuestro nombre y dirección.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },{
                "t13respuesta": "Teniendo una buena autoestima.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Desarrollando el sentido de responsabilidad.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Tomando buenas decisiones.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },



            {
                "t13respuesta": "La forma en que cada uno se valora.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La forma en la que nos expresamos.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Las expectativas que desarrollamos.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La familia que tenemos.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cómo se construye la identidad personal?",
                "¿Qué es la autoestima?"],
            "t11instruccion": "",
            "preguntasMultiples": true,
            // "columnas":2
        }
    },
    //2 arrasctra corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p>autoestima <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>personas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p> cualidades<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>regalo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>maestras<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>ideas<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Una  <\/p>"
            },
            {
                "t11pregunta": "<p> positiva se desarrolla al convivir con otras <\/p>"
            },
            {
                "t11pregunta": "<p> , pues esa relación nos permite saber quiénes somos, cuáles son nuestras <\/p>"
            },
            {
                "t11pregunta": "<p> y defectos y qué valores son importantes para nosotros.<\/p>"
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el párrafo con las palabras del recuadro.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La identidad personal se conforma por nuestra personalidad, y por la dirección donde vivimos. ",
                "correcta": "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los adolescentes pueden dejar de lado los intereses personales con la finalidad de participar en proyectos con los grupos a los que pertenecen. Esto da pauta al aprendizaje de conductas y valores que les sirven para actuar en comunidad.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La conformación de la identidad, debe incluir la conservación de nuestra identidad.  ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uno de los pilares de la autoestima, es la relación con nuestra familia. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las expectativas irreales nos ayudan a saber de qué somos capaces y elevar nuestro autoestima. ",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,


        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La familia es un ejemplo de red social, que al unirse con otras, va formando una red cada vez mayor.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las redes sociales siempre cuentan con información verídica.",
                "correcta": "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen ciertos anzuelos en los mensajes, que hacen que las personas desconfíen de la información que se les proporciona.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La identidad y la autodefinición de los jóvenes, mantiene una estrecha relación con la cultura. ",
                "correcta": "1"
            }
            ,

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,


        }
    },
    //5 Selecciona la respuesta correcta
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EBuscan los modelos con los que definirse y aquellos que puedan imitar para lograr ser quienes son y quienes quieren ser. \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ENo existe una relación directa; al ser un espacio ficticio, no demuestran quiénes quieren ser, únicamente siguen páginas de moda. \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ELa cultura y las redes sociales van a definir a los adolescentes sin importar aquellos ideales que presenten. \u003C\/p\u003E",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "descripcion": " ",
           
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>¿Qué relación tiene el contenido que buscan los adolescentes en las redes sociales con la construcción de identidad?",
        }
    },
    //6 relaciona columnas
    {
        "respuestas": [
            {
                "t13respuesta": "Estado de bienestar en que el cuerpo funciona de manera óptima.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Los trastornos más comunes son depresión y ansiedad.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Efecto del entorno sobre el bienestar de una persona.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Está conformado por el bienestar físico, mental y social.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Salud física",
            },
            {
                "t11pregunta": "Salud mental"
            },
            {
                "t11pregunta": "Salud social"
            },
            {
                "t11pregunta": "Salud integral"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."

        }
    },
    //7 ordena elementos
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Se refiere a la reducción de riesgo.<\/p>",
                "t17correcta": "0",
                etiqueta: " 1"
            },
            {
                "t13respuesta": "<p>Se refiere a reducir la duración de la enfermedad.<\/p>",
                "t17correcta": "1",
                etiqueta: " 2"
            },
            {
                "t13respuesta": "<p>Se refiere a evitar complicaciones.<\/p>",
                "t17correcta": "2",
                etiqueta: " 3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Ordena los siguientes elementos dependiendo el nivel de salud que le corresponda.<br><\/p>",
        }
    },

    //8 respuesta multiple

    {
        "respuestas": [
            {
                "t13respuesta": "Diafragma",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "DIU de cobre",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Anillo vaginal",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Vasectomía",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Marihuana",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Videojuegos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Dificultad para dormir",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Pérdida de interés",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Cansancio",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Baja autoestima",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Ejercicio",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Bailar continuamente",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>Son ejemplos de métodos anticonceptivos.",
                "     Algunos síntomas de la depresión son:                "],
            "preguntasMultiples": true,

            "columnas": 1
        }
    },
    //9 opcion multiple
    {
        "respuestas": [
            {
                "t13respuesta": "Derechos que todo ser humano posee desde su nacimiento.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Responsabilidades de los niños.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Limitantes que pone el Estado.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Libertad de amar y sentir.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué son las garantías individuales?",
            "t11instruccion": ""
        }
    },
    //10 respuesta multiple

    {
        "respuestas": [
            {
                "t13respuesta": "Armar un plan de acción.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Llevar a cabo el plan.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Hacer modificaciones si es necesario.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cambiar de plan si uno no funciona.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Pedir que los demás hagan cosas por nosotros.",
                "t17correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son pasos que se deben seguir para cumplir un objetivo.",
        }
    },

    //11 falso verdadero
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las decisiones que tomamos en nuestra vida, nos ayudan a reafirmarnos como seres únicos.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tener una perspectiva limitada, ayuda a identificar aspiraciones y potencialidades. ",

                "correcta": "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Expresar nuestras opiniones, es un derecho que tenemos todas las personas.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los periodistas y medios de comunicación deben mantenernos informados con lo que ellos quieran, sea legítimo o no.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los periodistas son personas que corren peligro en el cumplimiento de su deber, pues sufren agresiones constantemente.",

                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": " Elige Falso o Verdadero según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,

        }
    },
    //matris editable
    {
        "respuestas": [
            {
                "t13respuesta": "Garantía individual",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Acciones del Estado para cumplir nuestras garantías",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fomentar el respeto a las decisiones y los procesos judiciales.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La educación es laica, gratuita y obligatoria hasta la secundaria.",

                "correcta": "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Establecer más órganos jurisdiccionales en el Tribunal Electoral.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aplicar acciones preventivas para evitar la violación de los Derechos Humanos.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nadie puede hacerse justicia por sí mismo.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las personas gozarán de libertad de pensamiento.",

                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda. ",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,

        }
    },
   
    //13 opcion multiple
     { 
 "respuestas":[ 
 { 
 "t13respuesta":"Es una construcción de competencias que permite a los adolescentes actuar de forma consciente, además de vivir su sexualidad de manera informada y responsable.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Es un derecho que tienen los adolescentes para vivir su sexualidad de forma libre, sin importar las consecuencias de sus actos.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Es la toma de decisiones que realizan los jóvenes al momento de iniciar su vida sexual, para que esta sea de forma informada y planeada.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué es la educación sexual?", 
 "t11instruccion": "", 
 } 
 },  //14 Respuesta múltiple
  { 
 "respuestas":[ 
{ 
 "t13respuesta":"Hablar con alguien de confianza para disipar las dudas.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Informarse sobre las ITS y los riesgos de contraer alguna de ellas.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Asistir con un ginecólogo que pueda recomendar el mejor método anticonceptivo, de acuerdo con las características.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Hacerlo por presión de la pareja o amistades.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Consultar únicamente información en redes sociales sobre el tema.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Apresurarse para vivir la experiencia antes que sus compañeros.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"2", 
 "t11pregunta": "Selecciona todas las respuestas correctas. <br><br>¿Qué se debe realizar antes de iniciar la vida sexual?", 
 "t11instruccion": "", 
 } 
 }, 
 //15Falso o Verdadero

 { 
 "respuestas":[ 
  {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
 ], 
 "preguntas": [ 
{ 
 "t11pregunta":"Existen muchas infecciones de transmisión sexual; algunas se deben a virus, otras a bacterias y otras a hongo" ,
  "correcta": "1",
 }, 
{ 
 "t11pregunta":"Las infecciones de transmisión sexual, o ITS, son infecciones que se transmiten exclusivamente por el contacto" ,
  "correcta": "0",
 }, 
{ 
 "t11pregunta":"Los riesgos de embarazo adolescente suelen asociarse con desinformación, desigualdad, pobreza e inequidad de género",
  "correcta": "1",
 }, 
{ 
 "t11pregunta":"Además de conocer los métodos anticonceptivos, es sumamente importante mantener una postura informada,que nos permita tomar decisiones acertadas y vivir nuestra vida sexual sin riesgos" ,
  "correcta": "1",
 }, 
 ],
 "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,

        }
 },
 //16 Arrastra Matriz Horizontal
  {
        "respuestas": [
            {
                "t13respuesta": "VIH",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Infecta las células<br> del sistema inmunológico<br> y las destruye.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Parásito",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Infecta células<br> de la vagina<br> y la uretra.",
                "t17correcta": "4"
            },
          
            {
                "t13respuesta": "Clamidia",
                "t17correcta": "5"
            }, {
                "t13respuesta": "Sífilis",
                "t17correcta": "6"
            },
             {
                "t13respuesta": "Bacterias",
                "t17correcta": "7"
            },
            
            
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td style='width:150px'> ITS </td><td style='width:150px'>Causada por…</td><td style='width:150px'>Consecuencias</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                  <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td><td>Virus</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr><td>Tricomoniasis</td>\n\
                                    <td>"
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },

  {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                         <td>"
            },
              
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td> <td>Bacterias</td><td>Dolor pélvico <br>crónico y esterilidad.</td> </tr><tr>\n\
                                         <td>"

                                   
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td><td>Daños al corazón,<br> cerebro, ojos y<br> otros órganos<br> internos.</td>\n\
                                       </tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Coloca los enunciados según corresponda a causas internas o externas de la independencia de México. ",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": true,
            "ocultaPuntoFinal": true
        }
    },
    
    //17 Falso o Verdadero
{ 
 "respuestas":[ 
  {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
 ], 
 "preguntas": [ 
{ 
 "t11pregunta":"La interculturalidad busca promover el reconocimiento de que hay personas que son mejores que otras." ,
  "correcta": "0",
 }, 
{ 
 "t11pregunta":"La riqueza cultural de México permite que todos pensemos igual aunque pertenezcamos a tribus diferentes." ,
  "correcta": "0",
 }, 
{ 
 "t11pregunta":"Para crear una cultura incluyente debemos desarrollar conciencia social.",
  "correcta": "1",
 }, 
{ 
 "t11pregunta":"Las relaciones interculturales están basadas en el respeto a la diversidad." ,
  "correcta": "1",
 }, { 
 "t11pregunta":"El diálogo nos permite crear conflictos entre culturas distintas." ,
  "correcta": "0",
 },
 ],
 "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,

        }
 },
 //18 Relaciona Columnas
 { 
 "respuestas":[ 
{ 
 "t13respuesta":"Buscar comprensión mutua y no despreciar a nadie.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Comprender las situaciones por las que pasa otra persona.", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"Aprender que las diferencias de los demás enriquecen la vida.", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"Propiciar el intercambio justo entre culturas e integración de las personas.", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"Comprender que existen diferentes maneras de valorar la vida.", 
 "t17correcta":"5", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Respeto" 
 }, 
{ 
 "t11pregunta":"Situarse en el lugar de otro." 
 }, 
{ 
 "t11pregunta":"Valorar las diferencias." 
 }, 
{ 
 "t11pregunta":"Horizontalidad" 
 }, 
{ 
 "t11pregunta":"Escala de valores." 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11pregunta": "Relaciona las columnas según corresponda.",
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } 
];
