json = [
   //1
   {  
    "respuestas":[  
       {  
          "t13respuesta":"Indica que gracias a la democracia ningún gobernante tiene poder si el pueblo lo rechaza.",
          "t17correcta":"1"
       },
       {  
          "t13respuesta":"Representa que los gobernantes tienen el poder absoluto en el pueblo que gobiernan.",
          "t17correcta":"0"
       },
       {  
          "t13respuesta":"Indica que gracias a la democracia, los gobernantes mantienen su posición aunque cuenten con el apoyo de pocas personas.",
          "t17correcta":"0"
       },
    ],
    "pregunta":{  
       "c03id_tipo_pregunta":"1",
       "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Qué mensaje quiere transmitir la imagen?",
       "t11instruccion":"<br><br><center><img src='CI5E_B04_A01.png'/><center/>",
    }
 },
 //2
 {
    "respuestas": [
        {
            "t13respuesta": "<img src='CI5E_B04_A02_01.png'>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<img src='CI5E_B04_A02_02.png'>",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "<img src='CI5E_B04_A02_03.png'>",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "<img src='CI5E_B04_A02_04.png'>",
            "t17correcta": "4"
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "Encargada de resolver conflictos jurídicos entre Estados que forman parte de las Naciones Unidas."
        },
        {
            "t11pregunta": "Organización encargada de mantener la paz, la seguridad internacional, siguiendo los principios de las Naciones Unidas."
        },
        {
            "t11pregunta": "Organización que juzga a las personas acusadas de cometer crímenes, por ejemplo: genocidio, guerra y lesa humanidad."
        },
        {
            "t11pregunta": "Supervisa la aplicación del Pacto Internacional de Derechos Civiles y Políticos. "
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda.",
        "altoImagen":170,
        "anchoImagen":170
        
        }
    },
    //3
    {  
        "respuestas":[  
           {  
              "t13respuesta":"El gobernante es el representante del pueblo,  debe conocer los intereses y necesidades de sus gobernados  para crear situaciones  que beneficien a todos.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Únicamente es el vocero de la voluntad del pueblo, sin tomar decisiones.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Dirige el pueblo logrando mayor seguridad, comodidad y buscando el bienestar, sin tomar en cuenta las características del pueblo ni sus necesidades.",
              "t17correcta":"0"
           },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Qué papel tienen los gobernantes frente a los ciudadanos, en un sistema democrático?",
        }
     },
     //4
     {  
        "respuestas":[  
           {  
              "t13respuesta":"Se priva a los ciudadanos de mejores prestaciones, debido a que no pagan la parte que les corresponde para garantizar los servicios públicos.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"No existen consecuencias, cada persona es libre de pagarlos y hacer lo que quieran con sus propiedades.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Las personas incrementan sus riquezas y apoyan a los que más lo necesitan.",
              "t17correcta":"0"
           },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Qué consecuencias tiene la evasión de impuestos?",
           "t11instruccion":"En Junio de 2016 se publicó el caso “Panamá Pappers”, el cual fue un trabajo periodístico para filtrar información confidencial de la junta de abogados panameña Mossack Fonseca, esta información reveló que diferentes jefes de Estado, líderes de la política internacional, actores, futbolistas, empresarios, etc., habían ocultado, propiedades, activos, empresas, etc., para realizar evasión fiscal, es decir, no pagar los impuestos que les corresponden, además de ocultar una serie de chantajes y delitos.",
        }
     },
     //5
     {
        "respuestas": [
            {
                "t13respuesta": "Faltas graves",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Faltas menores",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Faltas reiteradas de puntualidad o inasistencia sin justificación.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Daños leves a las instalaciones o bienes de la escuela.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Conducta agresiva contra otro miembro de la comunidad escolar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Falsificación de firmas o documentos oficiales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Falta de colaboración en las actividades escolares.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a una falta grave o a una menor, del reglamento escolar.",
            "descripcion":"Título preguntas",
            "evaluable":false,
            "evidencio": false,
        }
    },
    //6
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Convención de los Derechos del Niño.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Convención sobre la eliminación de todas las formas de Discriminación.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Declaración Universal de los Derechos Humanos.",
              "t17correcta":"1"
           },
           {  
            "t13respuesta":"Declaración Universal de los Derechos Animales.",
            "t17correcta":"0"
            },
            {  
                "t13respuesta":"Ley Nacional contra la Corrupción.",
                "t17correcta":"0"
            },
            {  
                "t13respuesta":"Ley Federal del Trabajo ",
                "t17correcta":"0"
            },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"Selecciona tdas las respuestas correctas.<br><br>¿Cuáles son las leyes internacionales que protegen los derechos de todos los seres humanos?",
        }
     },
     //7
     {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Independientemente de la edad, género, clase económica o cualquier otra distinción, todos los mexicanos gozan de los mismos derechos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los ciudadanos mexicanos comparten los mismos derechos que están expresados en acuerdos como: la Constitución Política, la Declaración Universal de Derechos Humanos y los acuerdos pactados en la Revolución Mexicana.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Constitución Política de los Estados Unidos Mexicanos establece que “Todos los seres humanos nacen libres e iguales en dignidad y derechos y, dotados como están de razón y conciencia, deben comportarse fraternalmente los unos con los otros”.",
                "correcta"  : "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false,
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Monarquía",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aristocracia",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Democracia",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Forma de gobierno en el que la jefatura del estado o cargo supremo es unipersonal, a este gobernante se le denomina monarca o rey."
            },
            {
                "t11pregunta": "El gobierno está manejado por un grupo de personas que toma decisiones en nombre de su pueblo, los cuales participan solo en ciertas ocasiones. Se transmite por herencia."
            },
            {
                "t11pregunta": "Se caracteriza por la división de poderes, la legalidad de la administración y la consagración constitucional."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
          
            
            }
        },

];
