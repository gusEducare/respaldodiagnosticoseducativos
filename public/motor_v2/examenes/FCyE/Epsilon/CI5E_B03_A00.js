json = [
  //1
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Interdependencia es depender e interactuar con otros para un bien común.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La interdependencia es un fenómeno que únicamente se da en niveles macro de la sociedad, es decir, en una comunidad, un estado, una comuna, etc.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ya que la interdependencia es depender del otro, se necesita  cooperación, equidad y justicia.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reciprocidad quiere decir que siempre debemos dar, sin esperar nada a cambio.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reciprocidad es un elemento fundamental para el bienestar de una sociedad.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Lee las siguientes oraciones y selecciona falso o verdadero según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //2
    {  
        "respuestas":[  
           {  
              "t13respuesta":     "Que nuestra naturaleza es amar a nuestros semejantes a pesar de sus diferencias.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":     "Que no debemos odiar lo diferente, que nos llega a enseñar la sociedad, en lugar de eso debemos ser más receptivos con los demás.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":     "Que debemos preservar nuestras diferencias, apartando a los demás de nosotros.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":     "Que debemos aprender a ser más tolerantes con las diferencias de los demás.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"2",
           "t11pregunta":"Selecciona todas las respuestas correctas, con base en la frase de Nelson Mandela.<br><br>¿Qué mensaje quiere transmitirnos Nelson Mandela?",
           "t11instruccion": "<strong>“Nadie nace odiando a otra persona por el color de su piel, su historia o religión. La gente aprende a odiar y, si pueden aprender a odiar, pueden aprender a amar, pues el amor le viene más natural al corazón del hombre”.</strong><br><br>Nelson Mandela"
        }
     },
     //3
     {  
        "respuestas":[  
           {  
              "t13respuesta":     "Porque eso demuestra que hay acciones que pueden estar por encima del bien de nuestro planeta.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":     "Porque es peligroso para el medio ambiente.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":     "Porque actúa sin importarle la seguridad del mundo.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Con base en el texto siguiente, selecciona la respuesta correcta.<br><br>¿Por qué crees que sería perjudicial que un país como Estados Unidos se retire del “Acuerdo de París?",
           "t11instruccion": "El 22 de abril de 2016 se firmó el “Acuerdo de París”, un documento que establece medidas para reducir gases y diferentes sustancias que contaminan el planeta;  96 países y toda la Unión Europea estuvieron de acuerdo con él y en acatar las medidas correspondientes.<br>Dicho acuerdo tiene un gran efecto en nuestro planeta, porque se trata de un trabajo en conjunto de  los líderes mundiales para salvaguardar la integridad de la Tierra. Sin embargo, el 1 de junio de 2017 el presidente de Estados Unidos, Donald Trump, decidió que su país dejaría de formar parte de estos acuerdos, debido a que aseguraba existían trabas económicas en el tratado para su nación."
        }
     },
     //4
     {  
        "respuestas":[  
           {  
              "t13respuesta":     "En el colegio “Isaac Newton”  se ha permitido el ingreso de tres alumnos con capacidades diferentes.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":     "En el hospital se le negó el acceso a una mujer embarazada por ser de origen étnico y no contar con seguro médico.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":     "En una empresa se le negó la oportunidad de tener una entrevista de trabajo a un joven por la foto que colocó en su solicitud de empleo.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Lee con atención las tres situaciones siguientes, y selecciona aquella que refleja la práctica de igualdad.",
           "t11instruccion": " "
        }
     },
     //5
     {
        "respuestas": [
            {
                "t13respuesta": "CI5E_B03_A05_02.png",
                "t17correcta": "0,2,4",
                "columna":"0"
            },
            {
                "t13respuesta": "CI5E_B03_A05_04.png",
                "t17correcta": "0,2,4",
                "columna":"0"
            },
            {
                "t13respuesta": "CI5E_B03_A05_01.png",
                "t17correcta": "0,2,4",
                "columna":"1"
            },
            {
                "t13respuesta": "CI5E_B03_A05_05.png",
                "t17correcta": "1,3,5",
                "columna":"1"
            },
            {
                "t13respuesta": "CI5E_B03_A05_06.png",
                "t17correcta": "1,3,5",
                "columna":"0"
            },
            {
                "t13respuesta": "CI5E_B03_A05_03.png",
                "t17correcta": "1,3,5",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra las imágenes a la casilla correcta, dependiendo si la imagen refiere a una acción para el bienestar social o el bienestar individual.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"CI5E_B03_A05_00.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":false
            
        },
        "contenedores": [
            {"Contenedor": ["", "109,121", "cuadrado", "200, 118", ".","transparent"]},
            {"Contenedor": ["", "109,321", "cuadrado", "200, 118", ".","transparent"]},
            {"Contenedor": ["", "229,121", "cuadrado", "200, 118", ".","transparent"]},
            {"Contenedor": ["", "229,321", "cuadrado", "200, 118", ".","transparent"]},
            {"Contenedor": ["", "349,121", "cuadrado", "200, 118", ".","transparent"]},
            {"Contenedor": ["", "349,321", "cuadrado", "200, 118", ".","transparent"]}
        ]
    },
    //6
    {  
        "respuestas":[  
           {  
              "t13respuesta":     "En la integración existe una separación de los estudiantes y en la inclusión todos trabajan juntos, con un principio de equidad, tomando en cuenta las capacidades de cada uno.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":     "Ninguna, en ambas existe una unión entre estudiantes normales y estudiantes con capacidades diferentes.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":     "Los niños con capacidades diferentes siguen separados de los demás, simplemente se encuentran en la misma institución.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Con base en los conocimientos que has adquirido a lo largo de esta lección, selecciona la respuesta correcta.<BR><center><img src='CI5E_B03_A06_01.png' width= '300' height= '200'></center><br><br>Analizando la imagen, ¿cuál es la principal diferencia entre integración e inclusión escolar?",
           "t11instruccion": " "
        }
     },
     //7
     {
        "respuestas": [
            {
                "t13respuesta": "Interdependencia.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Equidad.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Reciprocidad.",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "El equipo de fútbol escolar quiere llegar a la final del torneo estatal, y necesita de un entrenador para lograrlo."
            },
            {
                "t11pregunta": "El gobierno se ha encargado de la distribución gratuita de útiles escolares a todas las escuelas del país."
            },
            {
                "t11pregunta": "Sara fue elegida como jefa de grupo y gracias a esto pudo conseguir mejores pupitres para sus compañeros."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona cada situación con el principio que representa."
        }
    },
    //8
    {  
        
  "respuestas":[  
                   {  
                      "t13respuesta":     "Al asistir a un lugar protegido hay que informarse de las actividades permitidas y las que no.",
                      "t17correcta":"1"
                   },
                   {  
                      "t13respuesta":     "Investigar dentro de las leyes vigentes del país los derechos de los animales.",
                      "t17correcta":"1"
                   },
                   {  
                      "t13respuesta":     "Participar en actividades de voluntariado que promueven la protección y cuidado de la biodiversidad.",
                      "t17correcta":"0"
                   },
                   {  
                      "t13respuesta":     "Comprar un abrigo de piel de jaguar.",
                      "t17correcta":"0"
                   },
                   {  
                    "t13respuesta":     "Sembrar semillas de especies exóticas para tener en el jardín.",
                    "t17correcta":"0"
                 },
                 {  
                    "t13respuesta":     "Realizar actividades de turismo con cetáceos y delfines.",
                    "t17correcta":"0"
                 },
                   
                ],
                "pregunta":{  
                   "c03id_tipo_pregunta":"2",
                   "t11pregunta":"Selecciona las frases que representan acciones para cuidar a las especies en peligro de extinción.",
                }
             }
];
