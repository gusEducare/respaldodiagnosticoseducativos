json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los trastornos alimenticios son enfermedades que se originan por una alteración de la autoimagen corporal.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando nos agrada nuestro cuerpo e imagen, podemos llegar a tener un transtorno alimenticio.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una vida saludable requiere incluir todo tipo de alimentos en cantidades adecuadas en nuestra dieta diaria.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los trastornos alimenticios pueden llegar a ser saludables debido a que generan pérdida de peso y personas más bellas.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //2
   /*{
        "respuestas": [
            {
                "t13respuesta": "Apariencia delgada, saltarse las comidas, desmayos, cansancio, negarse a comer en público, inventar excusas para no comer.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Apariencia delgada, comer grandes cantidades de comida, amar su autoimagen, piel brillosa.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Apariencia obesa, comer grandes cantidades de comida, piel brillosa, comer en público.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>¿Cuáles son los principales síntomas y señales de que una persona padece anorexia?",
            "t11instruccion": "",
        }
    },*/
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Bulimia.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sobrepeso.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Anorexia.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Es un trastorno que se caracteriza por la ingesta descontrolada de alimentos en periodos cortos, que después se intentan compensar con conductas anómalas como vómitos, ayuno excesivo, dietas o uso de laxantes o diuréticos."
            },
            {
                "t11pregunta": "Es la acumulación excesiva de grasa en nuestro cuerpo, originado por el consumo excesivo de alimentos y una vida no activa."
            },
            {
                "t11pregunta": "Es un trastorno que se caracteriza por el miedo constante a subir de peso, generando que las personas se nieguen a comer."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Autoestima",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Bulimia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Anorexia",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando una persona se cuida y quiere tal y como es.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hacer ejercicio, motivarte, amarte y confiar en ti son acciones que aportan a lo siguiente.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los dolores de cabeza recurrentes, pérdida de esmalte dental, autoestima baja, tendencia a vomitar e  impulsividad son sus características.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es una enfermedad que afecta principalmente a jóvenes, se caracteriza por negarse a comer, sufrir cansancio y apariencia delgada.",
                "correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada concepto. <\/p>",
            "descripcion": "Características",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [

            {
                "t13respuesta": "Es una enfermedad que se caracteriza por la búsqueda y el consumo compulsivo de drogas, a pesar de sus consecuencias negativas.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Es el uso de sustancias dañinas para fines recreativos en periodos cortos.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Es una enfermedad que se caracteriza por la obsesión con la imagen corporal y la pérdida de peso.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Distorsiones en el pensamiento, pérdida de control sobre sí mismo, comportamiento agresivo.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Confianza en sí mismo, aumento de la autoestima, comportamiento agresivo.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Comportamiento amigable, distorsión del pensamiento, confianza en sí mismo.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Empatía, confianza en sí mismo y sociabilidad.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué son las adicciones?",
                "<br><br>Son características negativas de las adicciones:",

            ],
            "preguntasMultiples": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Uso de redes sociales.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Compras excesivas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Juego y videojuegos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Comer en exceso.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Alcohol.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Drogas.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Cafeína.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las respuestas correctas.<br><br>Son comportamientos que pueden derivar en una adicción conductual:",
            "t11instruccion": "",
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "De muchas formas, con consecuencias negativas en lo físico, emocional y social.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tienen consecuencias positivas, pueden ayudar a ser más productivo y feliz.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "No existen pruebas confiables de que existan consecuencias negativas.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Cómo afectan las adicciones a las personas?",
            "t11instruccion": "",
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una alimentación adecuada y hacer ejercicio son formas de autocuidado.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El autocuidado significa no contemplar a los otros y  solo pensar en mi bienestar.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La asertividad es la capacidad de decir no, y tomar nuestras propias decisiones.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser asertivo es rechazar el consumo de sustancias que pueden dañar física y mentalmente.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Social.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Emocional.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Física.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Cuando Juan reprueba materias en su escuela debido al consumo de drogas es una consecuencia..."
            },
            {
                "t11pregunta": "Cuando Lupita se comporta agresiva y le grita a sus amigos y familia debido al consumo excesivo de alcohol es una consecuencia…"
            },
            {
                "t11pregunta": "Lupita padece de trastornos de apetito debido al consumo de drogas, esto es una consecuencia..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "Relaciona las columnas en función de cada caso y el tipo de consecuencia de la que se trate.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "El trabajo infantil.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Violencia física.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Niños trabajando con materiales pesados y en constante peligro.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Insultos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tener obligaciones en el hogar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Hacer la tarea.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Tener reglas en casa.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las respuestas correctas.<br><br>Son acciones de abuso y explotación infantil:",
            "t11instruccion": "",
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Violencia psicologica.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Violencia fisica.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Derechos humanos.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Burlarse, gritar o insultar a una persona son formas de violencia..."
            },
            {
                "t11pregunta": "Empujar, golpear y amenazar con armas son formas de..."
            },
            {
                "t11pregunta": "Deben ser respetados sin importar el idioma, color de piel y nacionalidad..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El diálogo y la comunicación son herramientas para resolver nuestros problemas, quejas y dudas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reflexión es fundamental para construir una sociedad basada en el respeto y la paz.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No existen leyes que protejan los derechos de los niños y niñas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los niños y niñas no deben ser protegidos física y psicológicamente.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "Reconocer cuando algo o alguien nos hace daño, identificar las leyes que nos protegen cuando sufrimos violencia y acudir a personas de confianza.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Identificar las leyes que nos protegen cuando sufrimos violencia, golpear y hacer uso de la violencia, gritarle a mis compañeros para que me respeten.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Gritarle a mis compañeros para que me respeten, reconocer cuando alguien nos hace daño, hacer uso de la violencia física.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Son formas de prevenir el maltrato, la violencia y el abuso infantil:",
            "t11instruccion": "",
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "Ideología",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Lucha de Interés",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Circunstancias socioeconómicas",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Inclusión",
                "t17correcta": ""
            },
            {
                "t13respuesta": "Personalidad dócil",
                "t17correcta": ""
            },
            {
                "t13respuesta": "Color de piel",
                "t17correcta": ""
            },
            {
                "t13respuesta": "Miedo",
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Existen diferentes causas  para cometer discriminación, a continuación se muestran las principales:<br>"
            },
            {
                "t11pregunta": ": Cuando alguien tiene un pensamiento distinto al mío.<br>"
            },
            {
                "t11pregunta": ": Cuando no pueden resolverse los conflictos de manera pacífica.<br>"
            },
            {
                "t11pregunta": ": Discriminación debido al ingreso económico.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "Raza.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Género.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Apariencia física.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Discapacidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Empatía.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Amistad.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Empírica.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Socialista.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son los tipos de discriminación más comunes:",
            "t11instruccion": "",
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "Por su apariencia fisica.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Por género.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Por cuestiones religiosas.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "María se burló de Sofía por su peso; eso es discriminación..."
            },
            {
                "t11pregunta": "Diego le dijo a Lucía que no podía juntarse con ella por ser una niña; eso es discriminación..."
            },
            {
                "t11pregunta": "Leila tiene mucho miedo de ir a la escuela, sus compañeros la evitan por ser musulmana; esa es discriminación..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //17
    {
        "respuestas": [

            {
                "t13respuesta": "Burlarse de los demás por ser diferentes, no escuchar o dialogar con los otros.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Ser empático y apoyar a las personas sin importar sus características.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Escuchar, dialogar y estar abierto a conocer personas diferentes.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Incluir a personas diferentes en mi vida diaria, dialogar y escuchar a los demás.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Pensar que mi opinión es la única y la más importante, no tomar en cuenta lo que opinen los demás.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Burlarme de los demás porque son diferentes a mí, no los vuelve originales.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Son características de la discriminación:",
                "<br><br>Son acciones que fomentan la libertad y la inclusión: ",

            ],
            "preguntasMultiples": true
        }
    },

    //===================== Actualizacion  19 de septiembre

    //18
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Karl Rogers fue un psicólogo que creía que la manera de entender a los demás era abandonar temporalmente nuestras creencias y acercarnos a las personas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La empatía nos permite reconocer, interpretar y comprender a los demás. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un ejemplo de empatía es ignorar a las personas o juzgarlas cuando están pasando por una circunstancia difícil.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La convivencia no es necesaria para vivir en sociedad.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mundo moderno se caracteriza por ser tranquilo y crear lazos fuertes entre las personas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Vivir en comunidad implica compartir costumbres, sentimientos y responsabilidades.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    //19
    {
        "respuestas": [
            {
                "t13respuesta": "Roberto observó que había una persona ciega que quería cruzar la calle y se acercó para ofrecerle su mano.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Luis observó que María estaba triste y continuó jugando para no distraerla.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Victoria observó que Roberta necesitaba apoyo para hacer sus ejercicios de matemáticas y la ignoró.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>¿Quién está siendo empático?",
            "t11instruccion": "",
        }
    },
    //20
    {
        "respuestas": [
            {
                "t13respuesta": "Escuchar a las personas que te rodean.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Utilizar el diálogo como mediación y resolución de conflictos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Reflexionar sobre cómo te relacionas con los demás.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Pensar en el bien común.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Anteponer mis deseos a los de los demás.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Pensar que mi opinión es la única y la más importante.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "No escuchar a las personas que me rodean.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son acciones que permiten tener una sana convivencia:",
            "t11instruccion": "",
        }
    },
    //21
    {
        "respuestas": [
            {
                "t13respuesta": "Sistemas políticos",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Rutas de comercio, relaciones con otros países, sistema de salud",
                "t17correcta": "2",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Representan la forma en que un grupo determinado organiza los diferentes sectores de un país"
            },
            {
                "t11pregunta": "Son sectores de un país que forman parte de las decisiones de  organización política"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponde.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //22
    {
        "respuestas": [
            {
                "t13respuesta": "Principio de mayoría, principio de soberanía popular y principio de representación.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Principio de estatuto, principio de minoría, principio de declaración.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Principio de soberanía estatal, principio de mayoria, principio de legitimación.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br><br>Laura investigó sobre los diferentes principios de democracia; entre ellos, encontró que es fundamental considerar la opinión de la mayoría en la toma de decisiones del país y tener presente que el poder reside originalmente en el pueblo y de ellos emanan los poderes públicos.<br><br> ¿Cuáles son los principios de la democracia?",
            "t11instruccion": "",
        }
    },
    //23
    {
        "respuestas": [
            {
                "t13respuesta": "Fraternidad",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Libertad",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Diálogo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Diversidad",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Autoritarismo",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Individualidad",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Anarquía",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Amor",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las respuestas correctas.<br><br>¿Cuáles son los valores de la democracia?",
            "t11instruccion": "",
        }
    },
    //24
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia se inventó en Grecia con la finalidad de que las personas pudieran tomar decisiones por medio de asambleas ciudadanas. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las personas que habitaban Grecia eran consideradas ciudadanos y podían participar democráticamente.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la democracia, el poder reside en todos los miembros que conforman la sociedad por medio de participación directa o indirecta.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia viola los derechos humanos y la dignidad de los grupos sociales.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la democracia, todas las personas son iguales ante la ley y tienen el mismo derecho de participación.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La convivencia, la armonía y el bien común son fundamentales para la democracia.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    //25
    {
        "respuestas": [
            {
                "t13respuesta": "Directa, en la que todos los miembros contribuyen a tomar decisiones sobre los asuntos en común, y representativa, en la que se eligen representantes que pueden tomar decisiones en cuanto a las maneras de gobernar.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Directa, en la que se eligen representantes que toman las decisiones, e indirecta, en la que todos participan en la toma de decisiones.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Dialogal, en la que se eligen diputados que toman las decisiones nacionales, y parlamentaria, en la que los magistrados toman las decisiones políticas.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué formas de democracia existen?",
            "t11instruccion": "",
        }
    },
];