json = [
  //1
  {  
      "respuestas":[  
         {  
            "t13respuesta":"Que debemos actuar acorde a nuestros impulsos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Qué debemos olvidarnos de nuestras emociones al momento de actuar.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que debemos razonar nuestros actos y no dejarnos llevar sólo por nuestras emociones.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa la imagen presionando \"+\" y responde:<br><br>¿Qué mensaje transmite la siguiente imagen?",
         "t11instruccion":"<center><img src='CI5E_B02_A01.png'/><center/>"
      }
   },
   //2
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Porque el mundo es injusto con las personas y sus decisiones.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque la gente no ha aprendido a ser responsable de sus decisiones y sus consecuencias.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Porque la gente no tiene un plan para tomar sus decisiones.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa la imagen presionando \"+\" y responde:<br><br>¿Por qué la frase de la siguiente imagen es utilizada por muchas personas en nuestra sociedad?",
         "t11instruccion":"<center><img src='CI5E_B02_A02.png'/><center/>"
      }
   },
   //3
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Al no existir esclavitud en nuestro país, tenemos la libertad de realizar cualquier actividad sin preocuparnos por las consecuencias.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La libertad comprende más que el simple hecho de hacer lo que queramos, involucra el respeto a las leyes, así como a la dignidad de las demás personas, y procurar el bienestar a la sociedad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Ser libre no sólo es mirar por nosotros, sino también buscar el bienestar de los que nos rodean, pues es un derecho que nos  incluye a todos.  ",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Debemos buscar la libertad de todas las personas, sin importar que, al luchar por ello, nuestros derechos se vean violentados.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Observa la imagen presionando \"+\" y elige las dos opciones que correspondan al mensaje que transmite.",
         "t11instruccion":"<center><img src='CI5E_B02_A03.png'/><center/>"
      }
   },
   //4
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Porque existe un exceso de desigualdad de ganancias entre las empresas y los productores.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Porque las grandes empresas deben de desaparecer y eliminar todos los intermediarios.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque es beneficioso para la economía, otorgar productos con mejor calidad y mejores precios.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Porque los granjeros deben desaparecer para dar paso a que las grandes empresas se encarguen de todo el proceso.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"¿Por qué es importante terminar con el comercio tradicional y empezar a implementar el comercio justo? Selecciona las 2 respuestas correctas:",
      }
   },
   //5
   {  
      "respuestas":[  
         {  
            "t13respuesta":"La igualdad de ganancias que existe entre productores y distribuidores.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La desigualdad de ganancias entre las empresas distribuidoras y los agricultores que producen la materia prima.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"La extrema pobreza que viven los agricultores que producen los productos que nosotros consumimos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa la imagen presionando \"+\" y selecciona la respuesta correcta:<br><br>¿Qué características del comercio tradicional transmite la imagen?",
         "t11instruccion":"<center><img src='CI5E_B02_A05.png'/><center/>"
      }
   },
   //6
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Que todas mis acciones deben de servir de ejemplo para la acción de los demás.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Que soy libre de actuar como yo quiera, mientras piense que está bien.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que debo de actuar de acuerdo a mis conveniencias y olvidarme de los demás.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto presionando \"+\" y responde:<br><br>¿Qué quiere decir la afirmación?",
         "t11instruccion":"<b>Mis actos y mi libertad están estrechamente ligados a los demás, ya que somos animales sociales en constante relación con otros seres humanos. Por lo tanto, cada acción que tomemos tendrá un peso específico en la libertad de los demás.<br>El filósofo Immanuel Kant  sostenía que debemos actuar tomando a la persona como un fin y nunca como un medio. Es decir, nuestras acciones no deben tener el  fin de utilizar a los demás como objetos. Nuestro proceder, por el contrario, debe tomar en cuenta que cada quien tiene sus propios intereses y  metas.</b>"
      }
   },
   //7
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\“Automovilista detenido por ir a exceso de velocidad y causar un accidente\”.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\“Se le detuvo por la ropa sospechosa que vestía\”.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\“Se detiene joven por estar repartiendo volantes afuera del Palacio de Gobierno\”.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\“Se detuvo a persona que perpetró un robo a tienda comercial\”.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\“Durante la marcha pacífica del pasado martes se realizaron diez detenciones arbitrarias\”.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\“Un criminal de los más buscados ha sido detenido en la capital mexicana\”.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Lee los titulares de periódico y selecciona aquellos en los que una persona ha sido detenida injustamente:",
      }
   },
   //8
   {  
      "respuestas":[  
         {  
            "t13respuesta":"No debe hacer nada, pues siguen siendo sus amigos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Debe acudir con un adulto para contarle lo que está sucediendo y hagan algo al respecto.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Pedirles que lo inviten de vez en cuando.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Alejarse de ellos y no comentar la situación a nadie para que no los regañen.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el siguiente caso y responde:<br><br><b>Pedro se junta con un grupo de amigos que ha conocido en la escuela, en los últimos días ha notado que por las tardes ellos se juntan con personas que consumen alcohol y tabaco.</b><br><br>¿Qué debería hacer Pedro? Elige la respuesta correcta.",
      }
   },  
];
