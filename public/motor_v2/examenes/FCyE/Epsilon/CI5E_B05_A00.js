json = [
 
{
        "respuestas": [
            {
                "t13respuesta": "Comisión Mexicana de Defensa y Promoción  de los Derechos Humanos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Liga Mexicana de Defensa de los Derechos Humanos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Centro de Estudios Fronterizos y de Promoción de los Derechos Humanos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Red Nacional de Organismos Civiles de Derechos Humanos.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='CI5E_B05_A01_01.png'/>"
            },
            {
                "t11pregunta": "<img src='CI5E_B05_A01_02.png'/>"
            },
            {
                "t11pregunta": "<img src='CI5E_B05_A01_03.png'/>"
            },
            {
                "t11pregunta": "<img src='CI5E_B05_A01_04.png'/>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "altoImagen":180,
             "t11pregunta": "Relaciona las columnas según corresponda"
        }
    },/////////////2
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EA partir de nuestras diferencias podemos crear cosas y conocimientos nuevos, con una solución pacífica como lo es el diálogo. \u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENuestras diferencias son incompatibles, por lo tanto, no podemos resolver nuestras diferencias.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPodemos aprender de nuestras diferencias mientras las resolvamos a partir del conflicto y la discusión.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11instruccion":"Por lo general un conflicto nace por una diversidad de pensamientos, ideas o maneras de actuar; el problema  surge por la poca tolerancia que tenemos con las personas que deciden actuar o pensar de forma diferente a la nuestra, por eso, debemos aprender a resolver esos conflictos de la mejor manera posible, porque, incluso del conflicto, nos puede quedar una serie de experiencias muy enriquecedoras; además, nos permitirá compartir de forma constructiva nuestras diferencias con los demás.",
         "t11pregunta":"Lee el párrafo, observa la imagen  y selecciona la respuesta correcta. <br><center><img src='CI5E_B05_A02.png' width=420px height=420px/></center><br> ¿Qué mensaje da la imagen anterior?"
      }
   },////////////////3
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPorque necesitamos personas capaces de alzar la voz y oponerse a las injusticias disfrazadas de reglas.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque necesitamos gente que nos guíe al buen camino y nos digan cómo debemos actuar en sociedad.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENo son necesarias, el cambio está sobrevalorado, solo son personas rebeldes que buscan crear conflictos.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EPorque ejerció su rol como autoridad de cambio y cooperó con su comunidad para lograr llevarlos por un nuevo camino, buscando el bienestar de todos.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque estaba dispuesto a morir por sus ideales.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque siempre quiso ayudar a la gente de su país y encontró formas violentas de realizarlo.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11instruccion":"Martin Luther King Jr. fue una de las personas con mayor iniciativa en la historia reciente. Siendo un pastor de tez negra lideró el movimiento por los derechos civiles para terminar la discriminación racial hacia los afroestadounidenses, con una serie de protestas no violentas a lo largo del país.<br><br> Fue condecorado con el premio nobel de la paz en el año de 1964 y en 1965 la mayoría de los derechos que habían sido reclamados por el movimiento fueron aprobados, como la ley de derecho al voto. Fue asesinado en 1968 en Memphis, y se considera una de las pérdidas más grandes y lamentables del siglo pasado.\n\
         <br><br>----------------------------------------------------------------------------------<br><br>Nelson Rolihlahla Mandela es una de las figuras más representativas del siglo pasado, representa claramente los valores y cualidades necesarios para una iniciativa. Nació en Sudáfrica y dedicó la mayor parte de su vida a ser un activista contra el apartheid, que era un sistema de discriminación racial que dominaba en Sudáfrica, consistía en crear lugares separados, tanto de recreación como de estudio, para personas blancas y negras, además de quitar la facultad del voto a las personas de color, para convertirse en un país de superioridad blanca.\n\
         <br><br>Fue encarcelado en 1962 por sus constantes protestas contra el régimen de su país y se mantuvo ahí por 27 años, liberado justo después del término del régimen del apartheid en 1990. Lideró junto con Frederik de Klerk la abolición de todas las normas de discriminación y llamar a las primeras elecciones democráticas en su país en 1994, dichas elecciones fueron ganadas por el Congreso Nacional Africano, partido que él encabezaba, gobernó hasta 1998 y su gobierno se dedicó a eliminar las secuelas que había dejado el régimen de apartheid, a través del combate del racismo institucionalizado, la pobreza y la desigualdad social, y la promoción de la reconciliación social. Esta serie de logros le valieron el premio nobel de la paz en 1993.",
         "t11pregunta":["Lee el párrafo y selecciona la respuesta correcta. <br><br>¿Por qué es necesario que  las personas actúen con iniciativa en la sociedad?",
         "¿Por qué Nelson Mandela representa las cualidades y valores de una persona con iniciativa?"]
      }
   },//////////////4
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ENo, pues la base de la democracia es el poder que tiene el pueblo de participar en la toma de decisiones respecto al manejo de recursos y acciones que tienen impacto en el desarrollo del país.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPor supuesto, no es necesario que las personas participen, pues quien toma las decisiones es el gobierno.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPuede existir siempre y cuando solo participen en las decisiones que respecta a su comunidad.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>De no permitir la participación política y social, ¿la democracia seguiría existiendo? "
      }
   },////////////5
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPorque es un trabajo de cooperación, ya que él, es el encargado de representar nuestra voluntad y necesidades.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque necesitamos a alguien que nos guíe desde nuestro contexto social.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAl contrario se necesita un jefe, pues necesitamos a alguien que nos dirija desde lo alto como una figura respetable.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EComparten metas, recursos, esfuerzos, existe motivación, trabajo en equipo, comunicación, responsabilidad y cooperación.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EImposición de la autoridad, existe la autoevaluación, y el logro de objetivos en conjunto.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEsfuerzo individual, intereses personales, conveniencia de estar en el grupo.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAsignación de roles, amistad, dinámico, motivación, trabajo independiente.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         
         "t11pregunta":["Lee el párrafo y selecciona la respuesta correcta. <br><br>En nuestra sociedad, es necesario elegir personas que representen y ejerzan la voluntad de un colectivo, llámese presidente, gobernador o incluso jefe del aula, por ello es necesario que cuidemos nuestras decisiones y cultivemos nuestro criterio con el fin de seleccionar bien a las personas que nos puedan representar mejor.<br><br>¿Por qué necesitamos un líder como representante y no un jefe?",
         "Los gansos tienen una forma peculiar de volar, lo hacen formando una V en el cielo, y esto tiene una explicación. Esta formación permite que el movimiento que hace el ganso que va adelante ayude a avanzar al que va detrás. Además, cuando el ganso que va de líder llega a cansarse, cambia de puesto y otro ocupa su lugar en lo que recupera fuerzas. <br>\n\
         De igual forma, cuando alguno de los gansos cae enfermo o herido sale de la formación en compañía de otros dos gansos, los cuales se encargan de apoyarlo y protegerlo  hasta que se recupera o muere.<br><br>¿Qué características del trabajo colaborativo se ven reflejadas en la lectura?"]
      }
   }
]