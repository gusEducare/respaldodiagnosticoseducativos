json=[
   //1
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Que nuestros sentimientos son parte de nuestros sentidos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que somos capaces de decidir qué sentimos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que no podemos decidir qué sentimientos nos afectan, a diferencia que sí podemos decidir qué cosas queremos ver.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Analiza la siguiente frase y selecciona la respuesta que explique el mensaje:<br><br><b>\“Puedes cerrar tus ojos a las cosas que no quieres ver, pero no puedes cerrar tu corazón a las cosas que no quieres sentir\”</b><br><i>Johnny Depp</i>",
      }
   },
   //2
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Porque son la base de cómo nos vamos a comportar en nuestra sociedad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Porque nos permite vivir en una sociedad perfecta.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque nos dirigen hacia la salvación eterna.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque son las bases de nuestro camino hacia el bien supremo del hombre.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Lee el texto presionando \"+\" y  selecciona las respuestas que expliquen por qué es importante tener principios éticos:",
         "t11instruccion":"<b>Los principios éticos son la base de nuestras acciones, pues  con ellos delimitamos qué interpretamos como una acción buena o mala, qué es beneficioso o perjudicial para nosotros mismos, el otro y la sociedad. Por eso, es  importante regirnos con ciertos principios éticos fundamentales: como lo apunta la filosofía, la ética debe de tener como fin conducir al hombre a su fin último, (la felicidad).</b><br><br><i>\“La verdadera felicidad consiste en hacer el bien\”<br>(Aristóteles)</i>"
      }
   },
   //3
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Todo parece bastante justo para todos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque las riquezas se distribuyen en un solo sector de la sociedad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Porque existe  abuso por parte de las personas con mayor solvencia económica, hacia los menos favorecidos.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Observa el texto y la imagen presionando \"+\", analiza y responde:<br><br>¿Por qué la imagen representa falta de equidad social? Selecciona todas las respuestas correctas:",
         "t11instruccion":"<b>La equidad no quiere decir que todos vamos a recibir las mismas cosas sin importar nuestras capacidades, sino que todos deben tener las mismas oportunidades y se les deben de otorgar las herramientas necesarias para poder ejercer con normalidad sus actividades diarias. Además, denota que no debe haber favoritismo o, incluso, lástima, hacia aquellas personas que requieren herramientas adicionales para ejercer sus actividades.</b><br><center><img src='CI6E_B02_A03.png' style='width:400px; height:500px;'/><center/>"
      }
   },
   //4
   {  
      "respuestas":[  
         {  
            "t13respuesta":"La imagen solo transmite mutua confianza y respeto.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque parece que se está rompiendo un acuerdo.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque hay una persona con intenciones francas de estafar.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa el texto y la imagen presionando \"+\", analiza y responde:<br><br>¿Por qué la imagen transmite una sensación de desconfianza?",
         "t11instruccion":"<b>Kant dijo hace casi 300 años que siempre miremos a la persona como un fin y nunca como un medio. Es decir, que nunca utilicemos a los demás como meros objetos, pues  cada quien tiene sus propios conceptos y nadie merece ser tratado como cosa.<br>Bajo este principio moral, no debemos engañar a las personas para conseguir nuestros propios deseos, sino trabajar con ellos para poder lograr cada uno nuestros propios fines.</b><br><center><img src='CI6E_B02_A04.png' style='width:400px; height:300px;'/><center/>"
      }
   },
   //5
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Porque necesitamos investigadores de calidad que denuncien actos de corrupción por parte de los gobernantes de nuestro país.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"No son necesarios, ya que la gente es lo suficientemente capaz de denunciar esos delitos por ellos mismos.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque la gente es ignorante y tibia a la hora de denunciar a sus gobernantes.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto presionando \"+\" y selecciona la respuesta que explique por qué son importantes las investigaciones como la realizada por <i>Animal político</i>.",
         "t11instruccion":"<b>En mayo de 2014, Animal Político difundió que el Gobernador del Estado de Veracruz,  Javier Duarte,  había creado una serie de empresas fantasma. Es decir, empresas ficticias para desviar fondos públicos, asignándoles contratos para el bienestar social, como el combate a la pobreza. Se reveló que existían más de 131 contratos diferentes en las 69 empresas fantasma que habían salido a la luz, lo cual generó un desvío de 950 millones de pesos.<br>A raíz de estas investigaciones, se dictó un proceso judicial, que  culminó con una orden de aprehensión en su contra el 17 de octubre de 2016. Duarte se dio a la fuga, pero finalmente fue capturado en Guatemala  el 15 de Abril de 2017.</b>"
      }
   },
   //6
   {
        "respuestas": [
            {
                "t13respuesta": "Fomenta el juego sano",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Entorpece el juego sano",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Empujar y decir groserías a los compañeros.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Respetar  las reglas establecidas en el juego.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Practicar deporte en áreas verdes establecidas por la institución.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Practicar deporte en áreas poco seguras, donde es fácil lastimarse.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Clasifica las acciones. ¿Promueven el juego sano o lo entorpecen?",
            "descripcion":"Acciones",
            "evaluable":false,
            "evidencio": false
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Justicia",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Injusticia",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el salón de clases  se realizaron elecciones para jefe de grupo. Ofelia ganó porque les dio regalos a sus compañeros a cambio de  votar por ella.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Joaquín y su equipo tenían que preparar una exposición, así que cada uno realizó una parte de la investigación y presentación. Al final pudieron obtener un 10 de calificación. ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Arantza tiene que trabajar todas las mañanas en la comunidad más cercana pues tiene que llevar dinero para que su familia pueda comer.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Analiza con atención cada uno de los casos y selecciona si se refiere a una justicia o una injusticia:",
            "descripcion":"Casos",
            "evaluable":false,
            "evidencio": false
        }
    },
    //8
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Rogelio va por la estación del metro. Mientras sube las escaleras ve que una señora de avanzada edad va cargando bolsas de mandado, decide hacer como que no la ve y seguir su camino.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Al salir del supermercado, Rosa ve a un joven en situación de calle, así que decide alejarse porque piensa que puede asaltarla.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Mateo ha sido testigo de que molestan a uno de sus compañeros, por ello ha decidido acercarse a él y ayudarle a denunciarlo con los directores del colegio.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la situación donde se actuó con base en los principios éticos:"
      }
   },
];
