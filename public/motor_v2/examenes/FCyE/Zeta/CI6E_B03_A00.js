json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Desempleo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Violencia intrafamiliar.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Calentamiento global",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "Migración",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Trabajo informal.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='CI6E_B03_A01_02.png'>"
            },
            {
                "t11pregunta": "<img src='CI6E_B03_A01_03.png'>"
            },
            {
                "t11pregunta": "<img src='CI6E_B03_A01_04.png'>"
            },
            {
                "t11pregunta": "<img src='CI6E_B03_A01_05.png'>"
            },
            {
                "t11pregunta": "<img src='CI6E_B03_A01_01.png'>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta":"Relaciona cada una de las imágenes con la problemática actual que representa.",
            "altoImagen":"130px",
            "anchoImagen":"250px"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Porque es importante aprender de todas las culturas del mundo en que vivimos.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Porque a partir del diálogo intercultural puedes comprender mejor el mundo en el que vives y te desenvuelves.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"No es necesario, nosotros tenemos nuestras costumbres y ellos las suyas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Porque es el camino de la verdadera felicidad.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Con base en la lectura siguiente, selecciona todas las respuestas correctas. <br><br> Como ciudadanos del mundo ¿por qué es necesario el diálogo intercultural?",
         "t11instruccion":"Séneca afirmaba que nosotros somos ciudadanos del mundo y  los límites que existen en él, es decir, países o Estados, son creados por los mismos hombres. Por eso, aseguraba que el exilio no era un castigo en particular grave, simplemente era una imposición más de los hombres. Nosotros somos habitantes de un mundo, por lo tanto, nuestro lugar de nacimiento no determina nuestras acciones ni nuestra manera de ser."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Porque promueve el odio y la poca tolerancia, generando que las personas caigan en los estereotipos y el racismo.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"No causa daño, solamente se está encargando de que Estados Unidos siga siendo el mejor país del mundo.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Porque promueve la igualdad entre ciudadanos de diferentes regiones.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"No causa ningún problema, simplemente expresa su opinión acerca de otras personas.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Con base en el texto siguiente. Selecciona la respuesta correcta. <br><br> ¿Por qué crees que sea dañino tener este tipo de presidentes que fomentan con sus discursos la discriminación racial?",
         "t11instruccion":"El 8 de noviembre de 2016, Donald Trump fue elegido el  presidente número 45 de Estados Unidos, lo curioso de esta victoria es que fue conseguida con base en discursos de tono racista; su principal propuesta de campaña fue la de construir un gran muro fronterizo con México, pues dijo que los mexicanos mandábamos traficantes, ladrones y violadores a su país, por lo tanto, al construir un muro de 30 metros entre México y Estados Unidos evitaría esa situación. Además sostuvo en toda su campaña que no dejaría entrar a ningún ciudadano sirio que buscara asilo político por causa de la guerra civil que padecía su país, ya que no se arriesgaría a recibir terroristas en su nación."
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aumentar el uso de pesticidas perjudica el ambiente.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La única medida para recuperar el planeta es prohibir la tala excesiva de árboles.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Utilizar motos en lugar de vehículos evitará la contaminación.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Evitar y atender los incendios forestales mejorará la calidad del aire.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El uso indiscriminado de recursos naturales es una de las principales causas del daño ambiental.",
                "correcta"  : "1"
            },
             {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las campañas de reforestación de los bosques son medidas a corto plazo.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee  las acciones siguientes y elige falso o verdadero, dependiendo si benefician o perjudican al planeta.",  
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
   {
        "respuestas": [
            {
                "t13respuesta": "Energía eólica.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Energía hidráulica.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Energía solar.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='CI6E_B03_A05_03.png'>"
            },
            {
                "t11pregunta": "<img src='CI6E_B03_A05_01.png'>"
            },
            {
                "t11pregunta": "<img src='CI6E_B03_A05_02.png'>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona cada una de las imágenes con la energía limpia que representa.",
            "altoImagen":"130px",
            "anchoImagen":"250px"
        }
    },
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Que los estereotipos y prejuicios, pueden evolucionar hasta la discriminación.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Que los prejuicios y estereotipos, son la base de todos los males de la sociedad.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Que los estereotipos y prejuicios son la base de la economía de las empresas actuales.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Que es normal que una persona de color gane menos que las demás personas.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Con base en la siguiente imagen, selecciona la respuesta correcta. <br><br><center><img src='CI6E_B03_A06_01.png'></center> ¿Qué mensaje transmite la imagen?"
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Hay una falta de recursos económicos dentro de sus hogares. ",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Falta de oportunidades y discriminación en diversos sectores de la población.",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"A los niños les gusta ser responsables y ganar su propio dinero.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Las escuelas se encuentran muy alejadas de su comunidad.",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"Población con bajos niveles educativos.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Se impide que los niños y adolescentes alcancen un desarrollo pleno.",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Niños con ahorros para poder comprar juguetes.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Niños y adolescentes con sentido de responsabilidad y asistencia a la escuela.",
            "t17correcta":"0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":["Lee el siguiente caso y selecciona todas las respuestas correctas <br><br> ¿Cuáles son las posibles causas de dicha situación?"
         ,"¿Qué consecuencias puede desencadenar?"],
         "t11instruccion":"Julieta de 8 años  y Ramón de 10 años  se despiertan todos los días a las 5 de la mañana, pues deben caminar una hora para llegar a su lugar de trabajo, una fábrica de ladrillos.",
         "preguntasMultiples": true,
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Reconocer que las personas somos seres humanos iguales y sin distinción.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "En lugares tanto públicos o privados, nuestra responsabilidad es convivir con armonía y tolerancia.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Denunciar las situaciones a las autoridades correspondientes, e informarte sobre estos temas para poder enfrentar el problema.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Diseñar y promover acciones, programas y políticas públicas, que ayuden a la población a conocer las causas y consecuencias de actos excluyentes.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Las causas más comunes de la discriminación son: la pobreza, el color de piel y las preferencias sexuales."
            },
            {
                "t11pregunta": "Cerca de 32% reconoce que alguna vez fue discriminada en el trabajo, la calle, alguna institución pública, la escuela y el transporte público."
            },
            {
                "t11pregunta": "De las personas que reconocieron haber sido discriminadas, 46% no hizo nada al respecto."
            },  
            {
                "t11pregunta": "La Ciudad de México obtuvo una calificación de 7.2 (alta), donde 10 significa que existe mucha discriminación y 0 representa que no existe."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion":"Lee con atención algunos de los resultados de la Encuesta acerca de Discriminación en la Ciudad de México, realizada por el Consejo para Prevenir y Eliminar la Discriminación (COPRED).<br><br>Después escribe en el recuadro la letra que corresponda a los datos presentados, con la actitud tolerante y respetuosa que debe tener la comunidad para erradicar  los casos de discriminación."
        }
    }
];
