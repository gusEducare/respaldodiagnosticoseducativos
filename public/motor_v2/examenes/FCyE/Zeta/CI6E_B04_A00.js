json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Derechos",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Responsabilidades",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Votar en elecciones populares.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Alistarse a la guardia nacional.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Votar en las elecciones.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tomar parte en la política.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Desempeñar cargos concejales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Votar en consultas populares acerca de temas de trascendencia.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a un derecho o a una responsabilidad del ciudadano.",
            "descripcion": "Acciones",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
    //2
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Que las leyes solo sirven si todos las respetamos, porque decidimos que eran los mejores medios para una sana convivencia.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Que las leyes son el camino para la sociedad perfecta y la paz universal.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Que las leyes son prescindibles y no las necesitamos para tener una buena convivencia social.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
            "t11instruccion":"Las leyes son obligaciones que tenemos con el Estado para garantizar nuestra seguridad, si estas son violentadas de alguna manera existirá un castigo para el involucrado en medida de qué tan grave fue su ofensa. <br>Debemos tomar en cuenta estas penitencias a la hora de analizar nuestras acciones, pero estas no deben ser nuestra principal razón para cumplir las leyes. Debemos de seguirlas porque garantizan nuestra seguridad y bienestar social, por lo tanto, el cumplimiento de las leyes debe ir acorde a la búsqueda de la paz y el buen trato y no basado en el miedo a posibles castigos.<br><img src='CI6E_B04_A02.png' width=850 height=400 >",
           "t11pregunta":"Selecciona la respuesta correcta.<br><br>Con base en lo aprendido en la lección y el texto anterior, ¿qué mensaje crees que nos quiere transmitir?"
        }
     },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Poder Ejecutivo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Poder Legislativo.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Poder Judicial",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Se encarga de dirigir y administrar la gestión diaria de la nación, acorde a las normas y leyes estipuladas."
            },
            {
                "t11pregunta": "Realiza y modifica las leyes dependiendo de las necesidades; se representa con la figura                           del Congreso Nacional."
            },
            {
                "t11pregunta": "Se encarga de impartir justicia mediante la aplicación de normas y leyes jurídicas; se representa con la figura de la Corte Suprema de Justicia de la Nación."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.<br><br>Relaciona cada sección de los poderes de un sistema democrático con su función y principal representante."
        }
    },
    //4
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Que no existe una verdadera transparencia, sino que solo dan a conocer los datos más convenientes.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Que debemos exigir una total transparencia en los gastos públicos.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Que existe una total transparencia en los gastos públicos en el país.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la respuesta correcta. <br><br><center><img src='CI6E_B04_A04.png' width=800 height=400></center><br>Con base en lo aprendido en la lección, contesta la siguiente pregunta. ¿Qué mensaje nos quiere transmitir la imagen?"
        }
     },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Obstaculiza",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Promueve",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estar siempre informado.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tratar de involucrar a todos los ciudadanos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mostrar apatia hacia los problemas del país.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Conformarse con lo establecido.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Exigir nuestros derechos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fomentar la indiferencia.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca si la acción promueve u obstaculiza la presencia de los ciudadanos en la regulación de sus autoridades.",
            "descripcion": "Acciones",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
    //6
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Porque como representante de nuestro país, es el encargado de garantizar nuestra seguridad pública y social.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Para no elegirlo solo por ser el más guapo de los candidatos.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Porque es nuestro líder.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
            "t11instruccion":"Hobbes sostenía que una vez que creamos las leyes a partir de una serie de  acuerdos establecidos por los ciudadanos, necesitamos elegir a un representante que nos guíe y se encargue de gobernar el Estado. Decía que esa persona junto con el Estado, debían ser los encargados de salvaguardar nuestra integridad de amenazas exteriores y/o interiores, es decir, que nosotros les otorgamos nuestra libertad de protección.",
           "t11pregunta":"Selecciona la respuesta correcta.<br><br>Acorde a lo aprendido y al texto anterior. ¿Por qué es importante saber elegir a nuestros gobernantes?"
        }
     },
    //7
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Libertad de la población para involucrarse en la toma de  decisiones que beneficien más al país, comunicación entre gobernantes y ciudadanos para garantizar el bien común. ",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"El país puede contar con muchos gobernantes, la represión y la nula libertad de expresión.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Búsqueda del bien común, libertades personales y la libertad de votación por el único partido que está en el poder.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
            "t11instruccion":"Analiza los siguientes casos.<br><br>1. En el país donde vive Maline, tienen al mismo gobernante desde hace muchos años, este recurre a la represión para ejercer su voluntad en la población, es conservador y su prioridad es el orden cívico, limitando las libertades personales. <br><br> 2.  Rodrigo vive en un país donde existe una división de poderes, las elecciones son libres y se realizan periódicamente, hay participación ciudadana para la toma de decisiones y también diversos partidos políticos. ",
           "t11pregunta":"Selecciona la respuesta correcta.<br><br>Analizando  ambas situaciones, ¿qué fortalezas encuentras en el gobierno democrático?"
        }
     },
    //8
    {  
        "respuestas":[  
           {  
              "t13respuesta":"En la calle de Ramiro se han vivido situaciones de violencia, por eso, los vecinos realizaron una junta donde invitaron a las autoridades correspondientes para levantar la denuncia. ",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"En la escuela Miguel Hidalgo, renunció el maestro de educación física, por lo que los niños se quedaron sin clase, los padres de familia decidieron dejarlo así hasta que la directora lo resuelva.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"En la comunidad donde vive Matías, después de las lluvias se llenó de baches, por eso todos los habitantes salieron con material para repararlos.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Qué situación refleja la participación ciudadana?"
        }
     },
    
];