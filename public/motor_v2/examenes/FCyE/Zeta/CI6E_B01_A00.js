json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Periodo en el que ocurren cambios en hombres y mujeres que los preparan para alcanzar una madurez sexual.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Lapso en el que ocurren cambios físicos, psicológicos y emocionales en los hombres.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Proceso de transformación de niños a adultos donde ocurren cambios físicos.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué es la pubertad?",
            "t11instruccion": "",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Cambio psicológico.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Cambio físico.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Cambio emocional.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Cambio social.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "José quiere salir cada fin de semana con sus amigos, pues dice que solo ellos lo entienden."
            },
            {
                "t11pregunta": "Natalia ha notado que su cabello se ha vuelto más graso aún cuando se lo ha lavado en la mañana."
            },
            {
                "t11pregunta": "Antonio siempre pelea con su mamá por las reglas que hay en casa."
            },
            {
                "t11pregunta": "Rosaura se integró a un grupo cultural en el que todos los fines de semana se reúnen para dar desayunos a las personas."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        },
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Aparte de los cambios que se presentan, existe una búsqueda de identidad, que si no se construye adecuadamente, puede ceder ante diferentes presiones.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es una etapa de rebeldía en la que los adolescentes sólo toman malas decisiones que ponen en riesgo su integridad.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Es cuando los adolescentes dependen al 100% de sus padres o tutores para realizar sus actividades, y el momento donde se inicia la búsqueda de la escuela preparatoria.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Por qué la adolescencia es considerada una etapa de vulnerabilidad?",
            "t11instruccion": "",
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los deseos de independencia y la creación de ideales propios son parte de los cambios psicológicos. ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre los cambios físicos que ocurren para ambos sexos son: el aumento de peso y altura, el ensanchamiento de hombros y cadera; el engrosamiento de la voz, cambios de humor, entre otros.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la adolescencia se pueden ocasionar conflictos entre padres e hijos, como la imposición de límites.",
                "correcta": "1",
            },



        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },

    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La salud es un derecho fundamental de los ciudadanos, que se encuentra plasmado en el artículo 3° de la Constitución Mexicana de los Estados Unidos Mexicanos.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El deficiente acceso a los servicios de salud en la infancia, deteriora el desarrollo de los niños y aumenta la tasa de mortalidad infantil.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La salud integral implica el bienestar físico, social, emocional y mental.",
                "correcta": "1",
            }, {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunas de las instituciones que protegen a los adolescentes de los riesgos son: Instituto Mexicano de la Juventud y el  Instituto Nacional de la Economía Social. ",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las instituciones de salud deben atender a las personas de forma confidencial, atenta y respetuosa. ",
                "correcta": "1",
            },



        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige  Falso o Verdadero según corresponda.",
            "t11instruccion": " ",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Necesidad de consumir una o más sustancias nocivas para la salud que generan una sensación de placer.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Dependencia y consumo excesivo de bebidas alcohólicas.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Se generan por la preocupación excesiva respecto a la imagen y el peso, lo que provoca que las personas dejen de comer, o incluso coman de más.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Drogadicción"
            },
            {
                "t11pregunta": "Alcoholismo"
            },
            {
                "t11pregunta": "Trastornos alimenticios"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Negación de atención médica.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Falta de medicamentos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Falta de infraestructura para dar atención médica.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Discriminación.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Juicios injustos e irrespetuosos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Restringir la libertad de expresión.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Encarcelar a una persona por opinar diferente a los demás.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Cuáles de las siguientes acciones violan la salud de las personas?",
            "t11instruccion": "",
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "La contaminación produce diversas enfermedades, por ello se ve reducida la calidad de vida de las personas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "No existe como tal una relación, pues las consecuencias que existen de la contaminación, no producen ningún efecto en los seres vivos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Se relacionan en que a mayor contaminación, menores son las probabilidades de sufrir alguna enfermedad.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué relación tiene la contaminación con el daño a la salud?",
            "t11instruccion": "",
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Negligencia.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Abuso emocional.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Abuso físico.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Abuso sexual.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "No proveer comida o abrigo a los menores de edad."
            },
            {
                "t11pregunta": "Ignorar o no demostrar comprensión y afecto."
            },
            {
                "t11pregunta": "Proveer de golpes, empujones y ataduras."
            },
            {
                "t11pregunta": "Obligar a realizar cualquier tipo de acto sexual."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            }

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El abuso sexual es un acto que viola los derechos de los niños; coarta su infancia y disminuye su capacidad de desarrollo.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El abuso sexual únicamente ocurre cuando un desconocido obliga a un menor de edad a realizar una actividad en contra de su voluntad.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es mayor el número de víctimas de abuso sexual que callan el incidente, que las que lo denuncian ante las autoridades.",
                "correcta": "1",
            }, {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si uno de nuestros compañeros reacciona con temor a todo, es señal de que está sufriendo un abuso.",
                "correcta": "0",
            },




        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Se alejan de las personas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Poseen baja autoestima.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Reaccionan de forma violenta.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Aumenta sus calificaciones y concentración.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Sabe decir “no” ante situaciones que lo ponen incómodo.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Qué síntomas tiene una persona que ha sido víctima de abuso?",
            "t11instruccion": "",
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "Violencia emocional",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Violencia psicológica ",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Violencia verbal ",
                "t17correcta": "2",
            },

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se minimizan los sentimientos de la otra persona.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se realiza para obtener control sobre otra persona, siendo la consecuencia de la violencia emocional y verbal. ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se ridiculizan las creencias de las personas.",
                "correcta": "0",
            }, {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se basa en restricciones y amenazas.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lenguaje hablado o escrito se utiliza para realizar agresiones y dañar a otra persona. ",
                "correcta": "2",
            },



        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige el tipo de violencia ejercida en cada caso.",
            "t11instruccion": "",
            "descripcion": "Acciones",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    ///13
    {
        "respuestas": [
            {
                "t13respuesta": "Se daña intencionalmente, de manera física, a una persona, ya sea con golpes, jalones, etc.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Ocurre cuando se fuerza a la persona a realizar alguna actividad sexual.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Ocurre cuando la pareja controla el dinero del otro sin su consentimiento.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Uso de la tecnología para acosar, intimidar u hostigar a la personas.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Violencia física"
            },
            {
                "t11pregunta": "Violencia sexual"
            },
            {
                "t11pregunta": "Violencia económica"
            },
            {
                "t11pregunta": "Violencia digital"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "Contar con libertad de decisión y de acción.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Respetar las creencias e ideales de los demás.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Mantener una buena comunicación con las personas que nos rodean.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Evitar la crítica constructiva.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Comunicarnos con un tono de voz alto y malas palabras.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br>¿Qué acciones permiten construir relaciones con respeto y sin violencia?",
            "t11instruccion": "",
        }
    },
    //15 nueva
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Desecho que se genera por las actividades humanas y que se debe eliminar siguiendo ciertas pautas.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Residuos repugnantes y con mal olor que se desechan.", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Todo aquello que debe tirarse al exterior para no causar contaminación.", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cuál es la definición de “basura”?", 
        "t11instruccion": "", 
        } 
        }, 
    //16 nueva
    { 
        "respuestas":[ 
       { 
        "t13respuesta":"Residuos que provienen de vegetales o animales.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Residuos de productos sintéticos que fueron creados por el hombre.", 
        "t17correcta":"2", 
        }, 
       { 
        "t13respuesta":"Desechos que generan hospitales o fábricas y que causan daño a la salud.", 
        "t17correcta":"3", 
        }, 
       ], 
        "preguntas": [ 
        { 
        "t11pregunta":"Orgánicos" 
        }, 
       { 
        "t11pregunta":"Inorgánicos" 
        }, 
       { 
        "t11pregunta":"Peligrosos" 
        }, 
        ], 
        "pregunta":{ 
        "c03id_tipo_pregunta":"12", 
        "t11pregunta":"Relaciona las columnas según corresponde.",
        "t11instruccion": "", 
        "altoImagen":"100px", 
        "anchoImagen":"200px" 
        } 
        },
    //17 nueva
     {
            "respuestas": [        
                {
                   "t13respuesta":  "Reducir:",
                     "t17correcta": "1" 
                 },
                {
                   "t13respuesta":  "disminuir",
                     "t17correcta": "2" 
                 },
                {
                   "t13respuesta":  "energía",
                     "t17correcta": "3" 
                 },
                {
                   "t13respuesta":  "Reutilizar:",
                     "t17correcta": "4" 
                 },
                {
                   "t13respuesta":  "alargar",
                     "t17correcta": "5" 
                 },
                {
                   "t13respuesta":  "Reciclar:",
                     "t17correcta": "6" 
                 },
                {
                   "t13respuesta":  "deshacen",
                     "t17correcta": "7" 
                 },
                {
                   "t13respuesta":  "fabricarlos",
                     "t17correcta": "8" 
                 },
         {
                       "t13respuesta":  "aumentar",
                         "t17correcta": "9" 
                     },
             {
                           "t13respuesta":  "venderlos",
                             "t17correcta": "10" 
                         },
                 {
                               "t13respuesta":  "reusar",
                                 "t17correcta": "11" 
                             },
                     {
                                   "t13respuesta":  "comprarlos",
                                     "t17correcta": "12" 
                                 },
                         {
                                       "t13respuesta":  "utensilios",
                                         "t17correcta": "13" 
                                     },
            ],
            "preguntas": [ 
              {
              "t11pregunta": "<li>"
              },
              {
              "t11pregunta": " se refiere a "
              },
              {
              "t11pregunta": "  el consumo de productos y "
              },
              {
              "t11pregunta": ".<br><li>"
              },
              {
              "t11pregunta": " se refiere a volver a utilizar las cosas, "
              },
              {
              "t11pregunta": "  su vida y disminuir el volumen de la basura que producimos.<br><li>"
              },
              {
              "t11pregunta": " se refiere al proceso mediante el cual se "
              },
              {
              "t11pregunta": " ciertos materiales para darles un uso nuevo sin tener que "
              },
              {
              "t11pregunta": " nuevamente.<br>"
              },
            ],
            "pregunta": {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "Arrastra las palabras que completen el párrafo.", 
               "t11instruccion": "", 
               "respuestasLargas": true, 
               "pintaUltimaCaja": false, 
               "contieneDistractores": true 
             } 
          },
    //18 nueva
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al acto de despreciar a una persona por ser diferente a nosotros se le conoce como intolerancia.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La intolerancia promueve relaciones de cordialidad entre las personas sin violar sus derechos.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ejemplo menos común de intolerancia es el racismo.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El rechazo a todo aquello que es diverso conlleva una situación de conflictos constantes, pues cada cultura tiene sus propias características y todas deben ser aceptadas. ",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //19 nueva
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Patricio votará por primera vez en las elecciones; él está convencido del partido político por el que va a votar, por lo que en redes sociales se encarga de desprestigiar a los demás partidos y hacer bromas o chistes al respecto.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"José apoya a su partido político desde hace 20 años, confía en él y cree que es la mejor opción, habla de sus propuestas a las demás personas y confía en que se lograrán.", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Carmen anulará su voto al acudir a la casilla, pues siente que ninguno de los candidatos está preparado para ocupar el puesto.", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br><br>En México se realizan elecciones para presidente de la República y han surgido varias posturas por parte de los ciudadanos. <br><br>¿Cuál de los siguientes casos es un ejemplo de  intolerancia durante las elecciones?", 
        "t11instruccion": "", 
        } 
        },
    //20 nueva
    { 
        "respuestas":[ 
       { 
        "t13respuesta":"Igualdad de valores y condiciones.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Compartir puntos de vista diferentes.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Respeto a las costumbres y tradiciones.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Ignorar aquello que es diferente a nosotros.", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Criticar las prácticas de una cultura ajena a la nuestra.", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"2", 
        "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Qué acciones fomentan prácticas de inclusión?", 
        "t11instruccion": "", 
        } 
        },
    //21 nueva
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Variedad de costumbres, tradiciones, conocimientos, lenguas, etc., con las que conviven e interactúan las personas dentro de una misma zona geográfica.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Costumbres, tradiciones, valores y dialectos diferentes que solo son permitidos en un mismo lugar y no aceptan cambios.", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Diferentes culturas que interactúan entre sí, fomentando la cooperación y la creación de una cultura que los incluya a todos.", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué es la diversidad cultural?", 
        "t11instruccion": "", 
        } 
        },   
];
