json = [
 
  {
       "respuestas":[
          {
             "t13respuesta": "Entablar un diálogo.",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Saber escuchar y esperar el turno de contestar.",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Ser claro y conciso.",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Buscar una solución justa para ambas partes.",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Fomentar la hostilidad entre los involucrados.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Suponer siempre los hechos.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Ganar a toda costa.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Siempre dar la razón.",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },///////
          {
             "t13respuesta": "Detenerse a ayudar cuando alguien lo necesite.",
             "t17correcta": "1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Comprar productos a los vendedores locales.",
             "t17correcta": "1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Enseñar a los demás a reciclar su basura.",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Estar informado sobre tu comunidad.",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Discriminar a los desamparados.",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Iniciar tu propio negocio.",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Separar la basura en casa, pero tirar la basura en la calle.",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },///////////
          ],
       "pregunta":{
          "c03id_tipo_pregunta":"2",
          "t11pregunta": ["Selecciona todas las respuestas correctas. <br><br>¿Qué cualidades o capacidades sirven para resolver un conflicto de forma pacífica? ",
                          "¿Qué acciones traerán un beneficio a la comunidad?"],
          "preguntasMultiples": true
       }
     },///////////2
     {
        "respuestas": [
            {
                "t13respuesta": "Bomberos voluntarios.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Procuraduría Federal de Protección al Ambiente.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Sistema Estatal de Protección Cívil.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": " Brigada de Intervención en Crisis.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Sistema Nacional de Protección Civil.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='CI6E_B05_A02_01.png'/>"
            },
            {
                "t11pregunta": "<img src='CI6E_B05_A02_04.png'/>"
            },
            {
                "t11pregunta": "<img src='CI6E_B05_A02_03.png'/>"
            },
            {
                "t11pregunta": "<img src='CI6E_B05_A02_05.png'/>"
            },
            {
                "t11pregunta": "<img src='CI6E_B05_A02_02.png'/>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "altoImagen":140,
             "t11pregunta": "Relaciona las columnas según corresponda"
        }
    },///////////3
    {
        "respuestas": [
            {
                "t13respuesta": "Incendios",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Terremotos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Huracanes",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Alejarse de todo tipo de objetos pesados que puedan caerse.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Evitar caminar por lugares angostos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Palpar las puertas antes de tratar de salir.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dirigirse a los lugares de refugio.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Alejarse de ventanas y puertas de cristal.",
                "correcta"  : "1,2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Proteger la cara y las vías respiratorias.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No buscar refugio aislado, siempre buscar una salida.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada medida de prevención.",
            "descripcion": "Medidas de protección",   
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable"  : true
        }        
    },///////////4
     {
        "respuestas": [
            {
                "t13respuesta": "Promueve la cultura de paz.",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Obstaculiza la cultura de paz.",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gandhi : “No debemos perder la fe en la humanidad que es como el océano. No se ensucia porque algunas de sus gotas estén sucias”.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Adolph Hitler: “Ante Dios y el mundo el más fuerte tiene el derecho de hacer prevalecer su voluntad”.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Osama Bin Laden: “La guerra es entre nosotros y los judíos. Cualquier país que siga los pasos en la misma trinchera que los judíos, sólo se puede culpar a sí mismo”.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Francisco l: “Negociar la paz requiere mucho coraje, mucho más que hacer la guerra, el coraje de decir sí”.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Martin Luther King: “Hemos aprendido a volar como los pájaros, a nadar como los peces, pero no hemos aprendido el sencillo arte de vivir como humanos”.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige promueve  (P) u obstaculiza (O) según corresponda.<br><br>Las siguientes declaraciones fueron realizadas por diferentes figuras históricas. Clasifícalas con base en las instrucciones. ",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//////5
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPara observar las diferencias entre la información que se brinda en distintos medios de comunicación, así se puede analizar y conformar una visión crítica de las situaciones, descartando información dudosa.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENo funciona, pues solo la información proveniente de las instituciones y gobiernos es confiable y en la que debemos centrar la atención.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDe esta forma podemos encontrar información confiable en sitios web y conformar nuestro punto de vista de las situaciones.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee los siguientes artículos y selecciona la respuesta correcta. <br><center><img src='CI6E_B05_A05_01.png' width=420px height=400px/></center><br>¿Para qué nos sirve informarnos en fuentes y medios de comunicación diferentes?"
      }
   },////////6
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEn la escuela, al sonar la alarma, la maestra Lupita formó  a los alumnos, les dijo que mantuvieran la calma y salieran del salón para refugiarse en el punto de reunión.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa familia de Paco mantuvo la calma, se alejaron de los lugares que pudieran derrumbarse y salieron corriendo de su hogar.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa señora González  continuó manejando su auto y aumentó la velocidad para llegar rápidamente a su hogar y estar con su familia. \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el párrafo y selecciona la respuesta correcta. <br><br>El pasado viernes a las 07: 43 horas, se vivió un sismo en la comunidad de Santiago, durante el altercado los habitantes realizaron diferentes acciones. <br><br>¿Quién actuó de forma correcta? "
      }
   }
];