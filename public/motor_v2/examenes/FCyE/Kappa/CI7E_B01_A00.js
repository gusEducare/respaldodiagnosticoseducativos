json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Nuevos intereses, cambios físicos y hormonales, cambios conductuales, surgen muchas dudas y un interés por el amor y la sexualidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Inserción laboral, relaciones amorosas estables, nuevos intereses, cambios conductuales.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Relaciones amorosas estables, inserción laboral, cambios únicamente hormonales.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Son características de la pubertad:",
            "t11instruccion": "",
        }

    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Plan de vida",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Cambio físico.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Derechos sexuales.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Identidad.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Son las diferentes actividades coordinadas e interrelacionadas que buscan cumplir un objetivo específico o una meta."
            },
            {
                "t11pregunta": "Rosalia notó que su cuerpo empezó a cambiar, por ejemplo: crecimiento de vello púbico, aumento de los senos y  expansión de caderas."
            },
            {
                "t11pregunta": "Promueven, respetan y protegen a los adolescentes y jóvenes en temas de sexualidad de manera libre y saludable."
            },
            {
                "t11pregunta": "Conjunto de rasgos, características, capacidades, creencias y habilidades de una persona."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Identificar tus fortalezas y debilidades, enunciar los rasgo de tu personalidad que más te gustan, plantear metas y analizar la realidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Plantear metas, ser pesimista, identificar tus debilidades, quedarte en tu zona de confort.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Quedarte en tu zona de confort, plantear metas, ser apático.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Son acciones que sirven para crear un buen proyecto de vida: ",
            "t11instruccion": "",
        }

    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Decidir de forma libre, autónoma e informada sobre su cuerpo y sexualidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derecho a la educación integral en sexualidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derecho a los servicios de salud sexual y reproductiva.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derecho a la identidad sexual.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derecho a ser obligados a tener una vida sexual activa.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a prohibir la información sobre sexualidad.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a obligar a otra persona a tener relaciones sexuales.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son derechos sexuales de los adolescentes:",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El respeto a sí mismo y a los demás es uno de los valores más importantes en nuestra sociedad.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es fundamental respetar y aceptar la diversidad.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La opinión personal es lo más importante, criticar a los demás y ser intolerantes nos enriquece.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La escuela es un espacio para adquirir conocimiento, no aporta ningún significado a nuestra existencia o autoconcepto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Rocío, le gritó a Lourdes debido a que se pintó el cabello  de rosa y para ella eso es “naco” (de mal gusto o vulgar). Lo anterior  es un ejemplo de estereotipos e intolerancia.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,


        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Nos permite identificar nuestros gustos, potenciales, reacciones y errores con la finalidad de mejorar a lo largo de la vida.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Permite conocer nuestra conciencia, actos y estado de ánimo a través de una observación interna.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Resultado de los aspectos cognitivos, emocionales y motivacionales que caracterizan a cada persona.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Auto-concepción"
            },
            {
                "t11pregunta": "Instrospección"
            },
            {
                "t11pregunta": "Personalidad"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Reunirse para humillar a tribus urbanas que tienen ideas contrarias a mis creencias.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Criticar a las personas por su modo de vestir y pensar.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Excluir a las personas por la religión que profesan.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tener una personalidad y autoestima.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Generar un auto-concepto.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Fomentar reuniones para compartir creencias, opiniones y gustos.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son acciones negativas que no fomentan la aceptación de la diversidad:",
            "t11instruccion": "",
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Un conjunto de rasgos, los cuales se entienden como la tendencia a reaccionar de determinada manera a la realidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Un conjunto de rasgos negativos que determinan el proyecto de vida personal.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Un conjunto de emociones positivas y creencias religiosas que conducen la vida de las personas.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cómo define Raymond Cattell la personalidad?",
            "t11instruccion": "",
        }

    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es fundamental estar informados;decidir de manera autónoma y respetar los derechos de las personas para ejercer una sexualidad libre.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La identidad se construye de elementos negativos.",
                "correcta": "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debemos aceptarnos, valorarnos y querernos tal y como somos.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,


        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "Tratar a las personas como iguales, reconocer sus derechos y obligaciones. ",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Existen para salvaguardar las facultades que todos los individuos poseen por el hecho de ser personas.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Es una herramienta legal de nuestro país, encargada de definir los límites ciudadanos y salvaguardar nuestros derechos humanos.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Respeto"
            },
            {
                "t11pregunta": "Derechos humanos"
            },
            {
                "t11pregunta": "Constitución Política de los Estados Unidos Mexicanos"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Igualdad y no discriminación.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Libertad de expresión.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derechos sexuales y reproductivos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derecho a una vida sin violencia, y un trato respetuoso.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Derecho a una vida de lujos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a explotar a los demás en mi beneficio.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a maltratar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Derecho a ejercer un trato violento.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son derechos humanos universales: ",
            "t11instruccion": "",
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "Tortura a las personas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Dsicriminación.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Allanamiento de vivienda.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "El negar la libre expresión.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Hacer los deberes de la casa.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Respetar las reglas de mi hogar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Obligarme a ir a la escuela.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son ejemplos de violaciones a los derechos humanos:",
            "t11instruccion": "",
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El pensamiento crítico es racional, autónomo y reflexivo.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El pensamiento crítico nos permite formarnos como ciudadanos con conciencia social y tomar decisiones informadas.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La asertividad requiere ser influenciados por otros y hacer lo que ellos digan.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Laura tuvo un conflicto con Sofía, decidió arreglarlo dialogando y comunicándose de manera clara, lo cual fue una decisión asertiva.",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Emilio le dijo a Paolo que Josué es un chavo gordo y feo al cual no hay que hablarle. Emilio decidió creerle a Paolo sin conocer antes a Josué y por lo tanto eso lo hace una persona con pensamiento crítico.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,


        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "El pensamiento crítico",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "La comunicación correcta",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "los gestos correctos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Deserción",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Pensamiento laboral",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Pensamiento social",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Comunicación incorrecta",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "¿Cómo podemos tomar una decisión informada?<br>"
            },
            {
                "t11pregunta": " es fundamental,  nos permite analizar las cosas que la sociedad acepta sin dudarlo, para entonces formar nuestra opinión.<br>"
            },
            {
                "t11pregunta": ", nos permite tener claridad en las ideas que expresamos a través de mostrar "
            },
            {
                "t11pregunta": " y tomar en cuenta todas las perspectivas involucradas.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "Escuchar la opinión de las demás personas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Hacer que nuestra opinión sea respetada.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Claridad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Comunicación correcta.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Gritar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Considerar que mi opinión es más importante que otras.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "No escuchar a los demás.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son características de la asertividad: ",
            "t11instruccion": "",
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "Habilidad para expresar opiniones y hacer saber a las demás personas lo que deseamos, de manera respetuosa.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Mecanismo para pensar mejor; permite recolectar, interpretar y evaluar información para tomar decisiones.",
                "t17correcta": "2",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Asertividad"
            },
            {
                "t11pregunta": "Pensamiento crítico"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //17
    {
        "respuestas": [
            {
                "t13respuesta": "Los padres de Roberta le prestaron su auto para ir a una fiesta a la que ella decidió ir. Por esta razón eligió no tomar alcohol para evitar accidentes cuando conduzca de regreso a casa. ",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Roberta decidió ir a una fiesta, sus amigos la obligaron a tomar alcohol y ella aceptó para no quedar mal ante ellos.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Roberta decidió que se iría de campamento con Luis sin importar que el estuviera agresivo e ingiriendo alcohol.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Vestirse como uno elija, sin importar lo que piensen los demás y más allá de las modas.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Hacer lo que mis amigos me digan, para que no se enojen conmigo,  sin importar que no esté de acuerdo con ellos. ",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Criticar a las personas porque no piensan como yo.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Es un ejemplo de libertad:",
                "Es un ejemplo de autonomía:"],
            "t11instruccion": "",
            "preguntasMultiples": true,
        }

    },
    //18
    {
        "respuestas": [
            {
                "t13respuesta": "Los padres confunden la autonomía con rebeldía.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Empezar a tomar tus propias decisiones.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Mayor libertad para elegir por ti mismo. ",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Inserción laboral estable.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Matrimonio y formación de una familia.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Inicio temprano de la sexualidad.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son situaciones que viven comúnmente los jóvenes en la construcción de su autonomía:",
            "t11instruccion": "",
        }
    },
    //19
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las personas tienen derecho a la libertad, que se vincula directamente con la toma de decisiones. ",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Laura fue al super a comprar un pastel. No sabía si quería comprarlo de vainilla o chocolate, así que imagino que sabor le gustaría probar más. Para tomar esta decisión utilizó los factores internos. ",
                "correcta": "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Josué esquivó con rapidez una motocicleta que casi lo atropella, por lo tanto utilizó sus instintos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los instintos nos permiten reaccionar de manera lenta y utilizando la razón.",
                "correcta": "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las emociones no influyen en la toma de decisiones.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La autonomía, significa depender de otros para tomar decisiones.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true,


        }
    },
    //20
    {
        "respuestas": [
            {
                "t13respuesta": "Capacidad de elección, voluntad propia, pensamiento crítico.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Nivel personal y colectivo.",
                "t17correcta": "2",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Características de la libertad"
            },
            {
                "t11pregunta": "Repercusiones de la libertad"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //21
    {
        "respuestas": [
            {
                "t13respuesta": "Analizar ventajas y desventajas, repetir esquemas de pensamiento, contar con información objetiva.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tomar una decisión con rapidez y poca información, imponer mi autoridad, dejarme llevar por mis sentimientos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Contemplar mi individualidad, ignorar los argumentos de las demás personas, imponerse.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Son acciones que aportan a la toma de decisiones responsables:",
            "t11instruccion": "",
        }

    },

];