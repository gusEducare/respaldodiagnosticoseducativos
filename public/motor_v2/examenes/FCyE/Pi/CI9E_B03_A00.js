json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Todas las personas tienen derecho a la vida, la libertad y la seguridad.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ninguna persona puede ser sometida a tortura, ni a tratos crueles o inhumanos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Ninguna persona puede ser arbitrariamente detenida, presa ni desterrada.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Todas las personas tienen derecho a salir de cualquier país, y de regresar al mismo.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Ninguna persona puede ser arbitrariamente despojada de su propiedad.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Todas las personas tienen derecho al descanso,  al disfrute del ocio, a tener horas razonables de trabajo y vacaciones pagadas.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Art. 3"
            },
            {
                "t11pregunta": "Art. 5"
            },
            {
                "t11pregunta": "Art. 9"
            },
            {
                "t11pregunta": "Art. 13"
            },
            {
                "t11pregunta": "Art. 17"
            },
            {
                "t11pregunta": "Art. 24"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona cada artículo de la Declaración Universal de los Derechos Humanos, con su función en la sociedad."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos humanos son una serie de prerrogativas que las personas, instituciones y gobernantes están obligados a seguir.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos humanos son una serie de sugerencias hacia los gobiernos de todo el mundo, para solventar la dignidad de sus ciudadanos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los derechos humanos no garantizan la conservación de la dignidad humana.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cumplimiento de los derechos humanos nos garantiza una mejor convivencia entre semejantes en una sociedad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las autoridades que han violado los derechos humanos deben ser castigados.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee las siguientes oraciones y selecciona falso o verdadero de acuerdo a cada caso.",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"La belleza en la diversidad natural del país.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Un instrumento musical emblemático.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Orgullo de nacer en México y no querer desprenderse del país.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Un sistema democrático en los pueblos.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"El deseo de migrar a otro país.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"La importancia de los volcanes que aportan minerales.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Lee el fragmento de la canción “México lindo y querido”, escrita por el moreliano Chucho Mongue, y hecha famosa por el cantante Jorge Negrete <br><br><center><img src='CI9E_B03_A03_01.png'></center>¿Qué manifestaciones de  identidad y pertenencia nacional identificas en la canción? Selecciónalas.",
         "t11instruccion":"Voz de la guitarra mía,<br>al despertar la mañana<br>quiere cantar su alegría <br>a mi tierra mexicana.<br><br>Yo le canto a sus volcanes<br>a sus praderas y flores<br>que son como talismanes<br>del amor de mis amores.<br><br>México Lindo y Querido<br>si muero lejos de ti<br>que digan que estoy dormido<br>y que me traigan aquí."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Grupos de pertenencia.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Tradiciones y costumbres.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Historias compartidas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Instituciones sociales y políticas.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Puntualidad y respeto.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Planear metas y objetivos.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Buena infraestructura.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona los elementos que intervienen en la conformación de la identidad personal."
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La tolerancia respecto de otras creencias religiosas es un puente para la buena comunicación e interacción entre los hombres.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las guerras santas fueron el mejor método de inclusión social que se haya podido haber inventado.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La corrupción es una de las principales trabas para la estabilidad social.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El diálogo es la mejor herramienta para la solución de nuestras diferencias.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debemos pelear los unos con los otros por nuestras diferencias culturales y sociales.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee las siguientes oraciones y selecciona falso o verdadero de acuerdo a cada caso.",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Porque demostró que no era necesaria la violencia y que existen otros métodos para solucionar un problema.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Porque luchaba para alcanzar su felicidad.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Porque murió como un mártir.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Porque viene de un grupo marginado.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el siguiente texto y selecciona la respuesta correcta.<br><br><center><img src='CI9E_B03_A06_01.png'></center>Con base en lo aprendido en clase y con el texto anterior, responde, ¿por qué Gandhi es considerado un revolucionario en la resolución de conflictos?",
         "t11instruccion":"Mohandas Karamchand Gandhi es una de las figuras más representativas del siglo pasado, fue un abogado, pensador y político hindú que dedicó su vida a eliminar la fuerte desigualdad racial que se vivía en su país por parte del gobierno británico y lograr la independencia de su país.<br>Gandhi siempre predicó el áhimsa, término sánscrito (lengua clásica de India) que se refiere al concepto filosófico que aboga por la vida y la no violencia, por lo tanto, sus protestas promovieron la paz y rechazaban la violencia, como las múltiples huelgas de hambre que desarrolló a lo largo de su vida.<br>Su lucha fue recompensada el 15 de agosto de 1947 cuando se le concedió la independencia al imperio de India del gobierno británico, gracias a su protesta pacífica y sus constantes negociaciones con ellos, sin embargo, fue asesinado meses después, el 30 de enero de 1948 por Nathuram Godse, un radical hinduista perteneciente a la ultraderecha de su gobierno."
      }
   },
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Que compartimos más aspectos de lo que creemos con el resto del mundo, en nuestras creencias que más nos identifican.",
            "t17correcta":"1"
         },
          {  
            "t13respuesta":"Que nuestras diferencias pueden más que nuestras semejanzas.",
            "t17correcta":"0"
         },
          {  
            "t13respuesta":"Que la Biblia y el Corán son  medios para convivir con las personas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Que no debemos seguir otro libro sagrado o mesías, como el Corán y Mahoma.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee la frase y selecciona la respuesta correcta. <br><br>Con base en lo que aprendiste durante la lección, ¿qué mensaje transmite la frase del Papa Francisco?",
         "t11instruccion":"“El Corán y la Biblia son lo mismo, Jesucristo, Mahoma, Jehová, Alá. Estos son todos los nombres utilizados para describir una entidad que claramente es la misma en todo el mundo”. <br><b>Papa Francisco</b>"
      }
   },
   {
        "respuestas": [
            {
                "t13respuesta": "Organización de las Naciones Unidas para  la Educación, la Ciencia y la Cultura.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Fondo Monetario Internacional.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Organización de las Naciones Unidas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Organización Mundial de la Salud.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='CI9E_B03_A08_01.png'>"
            },
            {
                "t11pregunta": "<img src='CI9E_B03_A08_02.png'>"
            },
            {
                "t11pregunta": "<img src='CI9E_B03_A08_05.png'>"
            },
            {
                "t11pregunta": "<img src='CI9E_B03_A08_06.png'>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona el logo con el nombre de las instituciones mundiales que se encargan de asegurar la protección de los derechos humanos.",
            "altoImagen":"150px",
            "anchoImagen":"200px"
        }
    }
];
