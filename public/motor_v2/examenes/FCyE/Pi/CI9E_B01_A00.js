json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Ser clara y estar bien definida, tener un límite de tiempo y ser realista.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "No tener un límite de tiempo, ser utópica y poco clara.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Ser realista, no tener un límite de tiempo, ajustarse a las necesidades.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué características debe tener una meta?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Objetivo que surge por la necesidad que una persona tiene de cumplir un propósito.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Pueden ser naturales o natas, nos ayudan a desarrollar habilidades y fortalezas.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Se obtiene mediante el alcance de los objetivos que nos planteamos en la vida.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Plan para organizar nuestros sueños y metas a corto, mediano y largo plazo.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Meta"
            },
            {
                "t11pregunta": "Capacidades"
            },
            {
                "t11pregunta": "Realización personal"
            },
            {
                "t11pregunta": "Proyecto de vida"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "No querer hacer nada en la vida, poca motivación, ocio, falta de oportunidad de empleo, flojera.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Oportunidades laborales, ocio, proyectos de vida, motivación.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Ingresos económicos altos, autoestima alta, motivación y ocio.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>Son razones para que las personas sean <i>ninis</i>:",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Personas que terminan sus estudios, pero no aceptan trabajos que no cumplan con sus expectativas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Personas que no tienen estudios y no encuentran trabajo debido a la falta de empleo.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Personas que no tienen deseos de estudiar ni trabajar.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Personas que consiguen empleo al terminar de estudiar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Personas que estudian y trabajan al mismo tiempo.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Personas con ganas de realizarse y seguir estudiando.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas. <br><br>Son los tipos de ninis:",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En México, el 22% de adolescentes, que equivale a  40 millones de jóvenes, no estudian ni trabajan.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En nuestro país hay una baja inversión del gobierno en la educación, lo cual dificulta que los jóvenes continúen estudiando.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ninis son jóvenes que se dedican a estudiar y trabajar.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Según la OMS, la salud es solamente la ausencia de enfermedad.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La salud integral, depende de la individualidad. Para lograrla cada persona debe hacer conciencia del cuidado de sí mismo.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ir al psicólogo cuando tengo problemas emocionales es una forma de aportar a mi salud.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tener una vida sedentaria es fundamental para tener una buena salud integral.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cuidado de las emociones, como recordar los aspectos que te gustan de ti y realizar actividades que te hacen feliz, es parte de una salud integral.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<br></p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "OMS y ONU.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "IMSS, ISSSTE, IMJUVE.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Alimentos y bebidas azucaradas.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Son organismos internacionales que protegen nuestra salud integral."
            },
            {
                "t11pregunta": "Son instituciones gubernamentales encargadas de cuidar la salud integral."
            },
            {
                "t11pregunta": "Su consumo descontrolado provoca diversos padecimientos, como sobrepeso y diabetes."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Hacer actividades que disfrutes, realizar ejercicio, leer un libro y comer saludable.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Realizar ejercicio, ingerir alcohol, ignorar mis emociones.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Expresar mis emociones, ingerir alcohol, ser sedentario.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Son acciones que aportan a tu salud integral:",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los principales medios de comunicación de los jóvenes son las redes sociales y el internet.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La información en internet, puede estar plagada de estereotipos y prejuicios que reflejan la sociedad en la que vivimos.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los adolescentes no pueden llegar a tener un desarrollo integral.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los jóvenes pasan poco tiempo utilizando sus celulares o el internet, prefieren el deporte y la lectura.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una autoestima alta, es es fundamental para un desarrollo pleno.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Consumismo.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Autoestima.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Violencia.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "La publicidad que generalmente manipula al público para adquirir ciertos  productos, puede llevar a una actitud de..."
            },
            {
                "t11pregunta": "Los estereotipos poco realistas que se promueven en los medios de comunicación, pueden llegar a generar en lo jóvenes problemas de..."
            },
            {
                "t11pregunta": "Algunos programas o contenidos, pueden generar conductas de..."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Género.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Religiosos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Políticos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Raciales.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Positivos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Realistas.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Autoestima.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las respuestas correctas.<br><br>¿Qué tipos de estereotipos existen?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Desarrollo de las capacidades del individuo.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Vivir en un entorno favorable.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Autoestima alta.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Autoestima baja.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Crisis emocionales.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona las respuestas correctas.<br><br>¿Qué implica un desarrollo integral?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La Unesco se encarga de asesorar a sus Estados miembros para proteger y promover el derecho a la libertad de expresión.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En México existen casos en que el gobierno ha intentado limitar la libertad de expresión de sus periodistas mediante amenazas.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Rocío realizó una investigación sobre la contaminación de los ríos debido a las descargas industriales. Las empresas involucradas se molestaron y prohibieron que Rocío publicara su investigación. Este es un ejemplo de libertad de expresión.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ejercer la libertad de expresión, implica buscar información, analizarla y compararla.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La libertad de expresión te permite denigrar a otras personas.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una de las principales necesidades que tenemos como seres humanos es el relacionarnos.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Cualidad del ser humano por establecer parámetros de igualdad para todos, que permiten lograr el bien común.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Trato negativo y perjudicial que se le da a una persona por motivos de raza, género, religión, etc.",
                "t17correcta": "2",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Justicia"
            },
            {
                "t11pregunta": "Discriminación"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Impedir injusticias, mejorar la relación con los otros, poder trabajar en equipo y dar nuestra opinión.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Mejorar la relación con los otros, timidez, inseguridad.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Inseguridad, capacidad de escucha, agresividad.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué nos aporta el ser capaces de expresarnos?",
            "t11instruccion": "",
        }
    },

    // reactivo 16
    {
        "respuestas": [
            {
                "t13respuesta": "En libertad individual, que es aquella que corresponde a un individuo, y  en libertad colectiva, aquella que corresponde a un grupo de personas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "En libertad gubernamental, aquella que impone el Estado, y libertad colectiva, aquella que corresponde a las asociaciones.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "En libertad individual, aquella que corresponde a un individuo, y libertad gubernamental, aquella que impone el Estado.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cómo se puede dividir la libertad?",
            "t11instruccion": "",
        }
    },
    // reactivo 17
    {
        "respuestas": [
            {
                "t13respuesta": "Cruz Roja Internacional, Organización Mundial de la Salud, Unesco y ONU.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "WWF, Greenpeace, ONU y Organización Mundial de la Salud.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Greenpeace, Sikanda, ONU y Organización Mundial de la Salud.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué instituciones mundiales garantizan el ejercicio de los derechos humanos?",
            "t11instruccion": "",
        }
    },

    // reactivo 18
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un ejemplo de libertad individual es la libertad de expresión, opinión o de religión.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un ejemplo de libertad colectiva es la libertad de asociación o reunión pacífica.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las instituciones que garantizan los derechos humanos se dividen en organismos de la sociedad civil, organismos privados e instituciones.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El artículo 4 de la Constitución mexicana establece que en nuestro territorio queda prohibida la esclavitud.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No existen instituciones gubernamentales encargadas de proteger los derechos humanos.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El artículo 9 de la Constitución mexicana establece que no se podrá coartar el derecho a asociarse o reunirse pacíficamente.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
// reactivo 19
{
    "respuestas": [
        {
            "t13respuesta": "Son derechos fundamentales, como la libertad, seguridad e igualdad, que se encuentran en la Constitución Política mexicana.",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "Son derechos que tienen las personas de clase alta.",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Son derechos que se encuentran en las organizaciones sociales que trabajan temas de igualdad.",
            "t17correcta": "0",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué son las garantías individuales?",
        "t11instruccion": "",
    }
},
// reactivo 20
{
  "respuestas":[
  {
   "t13respuesta":"Pensamiento positivo, neutro o negativo que una persona tiene acerca de otra por las características que posee.",
   "t17correcta":"1",
   },
  {
   "t13respuesta":"Metas o expectativas que se tienen para cada género.",
   "t17correcta":"2",
   },
  ],
   "preguntas": [
   {
   "t11pregunta":"Estereotipo"
   },
  {
   "t11pregunta":"Estereotipo de género"
   },
   ],
   "pregunta":{
   "c03id_tipo_pregunta":"12",
   "t11pregunta":"Relaciona las columnas según corresponda.",
   "t11instruccion": "",
   "altoImagen":"100px",
   "anchoImagen":"200px"
   }
},
// reactivo 21
{
    "respuestas": [
        {
            "t13respuesta": "A través de las normas, creencias y valores de la cultura donde nacimos.",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "No se adquieren, es extraño formarse en estereotipos de género.",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Solamente a través de películas y libros.",
            "t17correcta": "0",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cómo se adquieren los estereotipos de género?",
        "t11instruccion": "",
    }
},

// reactivo 22
{
    "respuestas": [
        {
            "t13respuesta": "F",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "V",
            "t17correcta": "1",
        },
    ], "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Una de las características de los estereotipos es que muchos de ellos los hemos adquirido de forma inconsciente.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Asignar el color azul a los niños y el color rosa a las niñas al nacer es un estereotipo de género.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El estereotipo de género es poco común, se presenta de forma extraordinaria en la sociedad.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los estereotipos son beneficiosos, no importa que perjudiquen a una persona.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Todas las personas, incluyendo hombres y mujeres, tienen derecho a expresar sus sentimientos, emociones y llanto.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los estereotipos pueden derivar en violencia y daño psicológico.",
            "correcta": "1",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
        "t11instruccion": "",
        "descripcion": "Reactivo",
        "variante": "editable",
        "anchoColumnaPreguntas": 60,
        "evaluable": true
    }
},


];
