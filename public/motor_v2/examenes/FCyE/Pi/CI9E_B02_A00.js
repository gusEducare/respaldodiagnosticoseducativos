json=[
  //1
    {
        "respuestas":[
            {
                "t13respuesta": "Capacidad",
                "t17correcta": "1",
                "numeroPregunta":"0"
            },
            {
                "t13respuesta": "Habilidad",
                "t17correcta": "0",
                "numeroPregunta":"0"
            },
            {
                "t13respuesta": "Potencial humano",
                "t17correcta": "0",
                "numeroPregunta":"0"
            },
            {
                "t13respuesta": "Expectativas",
                "t17correcta": "0",
                "numeroPregunta":"1"
            },
            {
                "t13respuesta": "Aspiración personal",
                "t17correcta": "1",
                "numeroPregunta":"1"
            },
            {
                "t13respuesta": "Realización personal",
                "t17correcta": "0",
                "numeroPregunta":"1"
            },
            {
                "t13respuesta": "Potencial humano",
                "t17correcta": "0",
                "numeroPregunta":"2"
            },
            {
                "t13respuesta": "Aspiración personal",
                "t17correcta": "0",
                "numeroPregunta":"2"
            },
            {
                "t13respuesta": "Realización personal",
                "t17correcta": "1",
                "numeroPregunta":"2"
            },
            {
                "t13respuesta": "Expectativa",
                "t17correcta": "1",
                "numeroPregunta":"3"
            },
            {
                "t13respuesta": "Habilidad",
                "t17correcta": "0",
                "numeroPregunta":"3"
            },
            {
                "t13respuesta": "Aspiración personal",
                "t17correcta": "0",
                "numeroPregunta":"3"
            }
        ],
        "pregunta":{
            "c03id_tipo_pregunta":"1",
            "t11pregunta": [
                "Selecciona la respuesta correcta. <br>Conjunto de recursos y aptitudes que posee un individuo para desempeñar una determinada tarea. ",
                "Objetivos a cumplir que se plantea una persona, metas que se marca de acuerdo a sus inquietudes personales y su modo de ser.",
                "Anhelo por alcanzar la felicidad, conseguir la plenitud y la satisfacción con la vida que tenemos. ",
                "Esperanza que tiene un individuo o grupo de personas de realizar o conseguir algo específico. "
            ],
            "preguntasMultiples": true
        }
    },
    //2
    {
        "respuestas":[
            {
                "t13respuesta": "Enfocarse en la decisión que se va a tomar.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Saber quién soy ahora mismo.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Encontrar opciones o posibilidades para resolver un conflicto.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Idealizar el futuro.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Pedir ayuda y dejar que otros tomen la decisión.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Analizar las ventajas y desventajas de cada opción.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Enfocarse solo en el objetivo personal.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Identificar las consecuencias y estar dispuesto a asumirlas.",
                "t17correcta":"1"
            }
        ],
        "pregunta":{
            "c03id_tipo_pregunta":"2",
            "t11pregunta":"<p>La vida está guiada por las decisiones que tomamos, ya sea que lo hagamos consciente o inconscientemente, y que estas tengan consecuencias agradables o desagradables. Por eso, al momento de decidir es necesario tomar en cuenta ciertas medidas.<br><br>Selecciona las acciones que permiten tomar buenas decisiones.</p>"
        }
    },
    //3
    {
        "respuestas": [{
                "t13respuesta": "Estudio",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Recreación",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Trabajo",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Expresión",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Afectivo",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Satisface las necesidades de conocimiento creatividad y desarrollo intelectual."
            },
            {
                "t11pregunta": "Satisface necesidades de ocio, afecto, creatividad, pertenencia, e identidad."
            },
            {
                "t11pregunta": "Permite obtener satisfacciones en lo intelectual, social y emocional."
            },
            {
                "t11pregunta": "Ejercicio del derecho de pensamiento y libertad."
            },
            {
                "t11pregunta": "Satisface necesidades emocionales, de protección, pertenencia, sexuales y reproductivos."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Todas las personas interactuamos en diferentes contextos, pues para desarrollarnos de manera integral y lograr nuestra realización personal, es necesario que alcancemos los objetivos que nos planteamos en los diferentes ámbitos de nuestra vida, como por ejemplo: hacer ejercicio, leer, aceptar opiniones diferentes, expresar afecto o sentirnos agradecidos.<br><br>Después de leer el texto anterior, relaciona cada concepto con su definición."
        }
    },
    //4
    {
        "respuestas": [{
                "t13respuesta": "Estudio",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Trabajo",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Expresión",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Recreación",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Afectivo",
                "t17correcta": "5"
            }
        ],
        "preguntas": [{
                "t11pregunta": "Se relaciona con el paso por la escuela  y la certificación."
            },
            {
                "t11pregunta": "Esfuerzo para producir o comercializar un bien o servicio, por el cual se recibe un pago."
            },
            {
                "t11pregunta": "Satisface necesidades de participación, creatividad, desarrollo intelectual, integración y participación."
            },
            {
                "t11pregunta": "Pueden ser actividades físicas, intelectuales, artísticas, etc., que provean gozo y placer."
            },
            {
                "t11pregunta": "Vínculos o lazos emocionales que compartimos con otras personas."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Según el texto anterior, relaciona cada concepto con su definición."
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para ser un ciudadano mexicano necesitas reunir las siguientes características: tener la nacionalidad mexicana, haber cumplido 18 años y tener un modo honesto de vivir.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para ejercer la ciudadanía es fundamental la educación cívica, estar informados, entender la realidad social y política, además de estimular la reflexión crítica de las condiciones en las que se lleva a cabo la acción ciudadana.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para que la sociedad pueda convivir en un ambiente democrático es necesario que únicamente participen las autoridades en la toma de decisiones en aspectos políticos, sociales y de los derechos civiles.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona selecciona falso o verdadero según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }
    },
    //6
    {
        "respuestas":[
            {
                "t13respuesta": "Tomar decisiones y asumir las consecuencias.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Respetar algunas normas y reglas de convivencia.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Causar revuelo en los mítines políticos.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Evitar la apatía.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Ejercer el derecho a la información. ",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Discriminar a los que son diferentes.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Cuestionar el trabajo de las instituciones públicas.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Creer fielmente en la información propuesta por las televisoras nacionales.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Respetar la dignidad, los derechos y las responsabilidades propias y ajenas.",
                "t17correcta":"1"
            }
        ],
        "pregunta":{
            "c03id_tipo_pregunta":"2",
            "t11pregunta":"<p>Selecciona aquellas acciones que permiten a los adolescentes tener participación en asuntos de interés público y fortalecer la democracia.</p>"
        }
    },
    //7
    {
        "respuestas":[
            {
                "t13respuesta": "Mejorar la toma de decisiones de la gestión pública.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Dar prioridad a recursos para el desarrollo ambiental.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Poner bases sólidas a los valores. ",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Fortalecer el Estado para que sea eficiente, transparente y descentralizado.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Mejorar el desempeño agrario.",
                "t17correcta":"0"
            },
            {
                "t13respuesta": "Tener mejores organizaciones sociales para la defensa de sus intereses.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Propiciar la construcción de un liderazgo más democrático.",
                "t17correcta":"1"
            },
            {
                "t13respuesta": "Planificar mejor las familias, completando su educación.",
                "t17correcta":"0"
            }
        ],
        "pregunta":{
            "c03id_tipo_pregunta":"2",
            "t11pregunta":"<p>La participación ciudadana es un derecho que todos tenemos, pues así podemos intervenir en los asuntos de interés colectivo.<br><br>Selecciona los beneficios de la participación ciudadana.</p>"
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Intercambio argumentado de ideas con la finalidad de llegar a un acuerdo. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Capacidad de reconocer, valorar, respetar y aceptar formas de ser, ideas, opiniones o creencias diferentes a las nuestras y convivir con ello.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Espacio de reconocimiento y manifestación  de distintas posturas ante situaciones cotidianas, problemas sociales, etc.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Canal de comunicación por el cual los ciudadanos saben con claridad que se está haciendo con los recursos públicos, beneficios y avances del país.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Diálogo"
            },
            {
                "t11pregunta": "Tolerancia"
            },
            {
                "t11pregunta": "Debate plural"
            },
            {
                "t11pregunta": "Rendición de cuentas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada concepto con su definición."
        }
    },
    //9
    {
        "respuestas":[
            {
                "t13respuesta":"La juventud tiene la energía y las posibilidades de cambiar su contexto, sin embargo muchas veces, gasta esa energía en cosas que no le proveen un cambio o un beneficio, quedándose  en un estado de confort.",
                "t17correcta":"1"
            },
            {
                "t13respuesta":"La juventud se encuentra en un dilema, ante transgredir el sistema o seguir las reglas que se les imponen, las cuales no les traen un beneficio.",
                "t17correcta":"0"
            },
            {
                "t13respuesta":"Los jóvenes están llenos de problemas que nadie entiende y no pueden hacer nada al respecto.",
                "t17correcta":"0"
            }
        ],
        "pregunta":{
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Selecciona la opción que mejor explique el mensaje de la siguiente frase.<br><br><b>“Los jóvenes siempre han tenido el mismo problema; cómo ser rebelde y conformarse al mismo tiempo.”</b><br><br><i>Quentin Crisp (Escritor británico)</i>"
        }
    }
];
