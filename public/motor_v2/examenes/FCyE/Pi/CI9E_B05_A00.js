 json=[
 
 {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las redes sociales son un medio de comunicación bastante útil para mantener contacto con amigos, hacer negocios o realizar alguna compra.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las redes sociales son fácilmente manipulables, debemos actuar con cautela cuando estamos en ellas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Necesitamos aprender a no respetar a las demás personas en las redes sociales por su orientación sexual, preferencia religiosa, su etnia, nacionalidad, etcétera.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debemos utilizar las redes sociales como una herramienta y no como un estilo de vida.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debemos tratar de fomentar los estereotipos construidos en las redes sociales para mejorar nuestras interacciones sociales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Debemos promover un mensaje de odio y poca tolerancia en las redes sociales.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },////2
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EPorque nos permite mantener un mejor trato con nuestros semejantes.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENo es importante, las redes sociales son un medio para expresarnos libremente y sin ataduras.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque solo mediante el respeto y tolerancia podemos llegar a una sociedad perfecta y diversa. \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el párrafo y selecciona la respuesta correcta. <br><br>Las redes sociales son un medio bastante útil para expresar libremente nuestros pensamientos, frustraciones, puntos de vista, etc. Sin embargo, es importante tomar en cuenta que existen personas de diferente etnia, preferencia sexual o nacionalidad que también utilizan estos medios de comunicación y pueden encontrar ofensivos o agresivos algunos de nuestros comentarios, por eso, es necesario actuar con el mismo respeto que esperaríamos recibir para nosotros mismos.<br><br>¿Por qué es importante fomentar una cultura de respeto en la redes sociales?"
      }
   },///////3
   {
        "respuestas": [
            {
                "t13respuesta": "Reconocer que se está siendo violentada, establecer límites y denunciar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Relacionarse con personas cuyas actividades no dañen su integridad, reconocer las consecuencias de los actos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Comunicarlo a un adulto inmediatamente, para que éste pueda intervenir al respecto.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='CI9E_B05_A03_01.png'/>"
            },
            {
                "t11pregunta": "<img src='CI9E_B05_A03_02.png'/>"
            },
            {
                "t11pregunta": "<img src='CI9E_B05_A03_03.png'/>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda <br><br>Durante el tema aprendiste que todos somos constructores del grado de bienestar tanto nuestro como de la sociedad, sin embargo, existen diferentes problemáticas que lo ponen en riesgo.\n\
             <br><br>Observa con atención las imágenes y selecciona  la alternativa que da solución al conflicto.  "
        }
    },/////4
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Alejarse de los malos ejemplos en la familia, escuela o círculo de amigos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Controlar mis emociones.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Admitir cuando me equivoco.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Trazar un límite de conducta de mi parte hacia los demás y viceversa.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Fomentar la desigualdad y la indiferencia.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Indignarme si alguien apunta a un error que pude haber cometido.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Aferrarse a aquellos lazos afectivos que nos incitan hacia el camino de los excesos y la irresponsabilidad.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas. <br><br>¿Qué acciones fomentan el desarrollo de nuestras relaciones afectivas? "
      }
   },//////5
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ECulturales, sociales, políticos y económicos.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ERacistas, sociales y políticos.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ECulturales, químicos y económicos.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11instruccion":"Malala Yousafzai es una estudiante y activista paquistaní, quien ha luchado por el derecho a la educación de las mujeres que viven dentro del territorio dominado por el régimen talibán, donde se prohíbe la asistencia a las escuelas y que además se ordenó el cierre de todas las escuelas privadas.\n\
         <br><br>Malala se dedicaba a promover los debates entre jóvenes de las escuelas de su región, con la finalidad de crear conciencia de sus derechos, además, se dedicó a escribir un blog para la BBC. Por estas acciones, resultó víctima de un atentado al ir en su autobús escolar, fue atacada por un grupo terrorista que le disparó hiriéndola de gravedad.",
         "t11pregunta":"Lee el párrafo y selecciona la respuesta correcta. <br><br>¿Qué tipos de problemas son los que refleja la sociedad donde vivía Malala? "
      }
   },//////6
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Porque reafirman que los jóvenes también tienen un peso en la sociedad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Porque nos enseñan caminos por los cuales los jóvenes pueden moverse para lograr un cambio en la sociedad.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Porque los jóvenes deben ser los nuevos gobernantes de nuestra sociedad actual.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Porque es necesario un cambio generacional a la brevedad.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "No son importantes y solo obstaculizan el desarrollo correcto de las leyes.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona las respuestas correctas. <br><br>¿Por qué es importante que existan jóvenes con iniciativa por el cambio social como Malala?"
      }
   }
]; 


 