 json=[
    { //reactivo 1
        "respuestas": [
            {
                "t13respuesta": "Son aquellos que tienen todas las personas que viven dentro de una sociedad.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Señala que no debe imponerse ninguna doctrina religiosa, dejando que cada ciudadano profese aquella con la que se identifique.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Todas las personas deben estar registradas  y respetar la legalidad de su país.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Hace referencia a que todas las personas  deben tener el mismo trato y oportunidad frente a la política del país.",
                "t17correcta": "4" 
            } 
        ],
        "preguntas": [
            {
                "t11pregunta": "Derechos políticos"
            },
            {
                "t11pregunta": "Laico"
            },
            {
                "t11pregunta": "Civil"
            },  
            {
                "t11pregunta": "Democrático"
            } 
        ], 
        "pregunta": {
            "c03id_tipo_pregunta": "12", 
            "t11pregunta":"<b>Relaciona las columnas según corresponda.</b>", 
            "t11instruccion":"", 
            "altoImagen":"100px", 
            "anchoImagen":"200px" 
        }
    }, 
    {  //reactivo 2
"respuestas":[ 
{ 
"t13respuesta":"Evitar la corrupción.", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Actuar con honestidad y transparencia. ", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Aumentar la participación ciudadana. ", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Siendo responsables de los servicios públicos. ", 
"t17correcta":"1",  
}, 
{ 
"t13respuesta":"Fomentar la violencia y no tolerancia. ", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"Aprovecharse de sus compañeros. ", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"Evadir impuestos. ", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"Fomentar la desigualdad social. ", 
"t17correcta":"0", 
}, 
],
"pregunta":{ 
"c03id_tipo_pregunta":"2",  
"t11pregunta": "<b>Selecciona todas las respuestas correctas.</b><br><br> ¿Qué acciones nos permiten enfrentar los retos de la democracia? ", 
"t11instruccion": "", 
} 
},  
{  //reactivo 3
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            } 
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La conformación de un estado incluye cuatro elementos: población, gobierno, territorio y su Constitución. ", 
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen dos tipos de democracia representativa, la presidencial y la parlamentaria.", 
                "correcta"  : "1"
            }, 
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ser un Estado soberano significa que no reconocemos a ningún poder por encima del poder político.", 
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia representativa se utiliza cuando se toman unilateralmente decisiones importantes.", 
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La democracia parlamentaria es cuando se elige al presidente mediante el voto popular.", 
                "correcta"  : "0"  
            } 
        ],
        "pregunta": { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b>Elige falso (F) o verdadero (V) según corresponda.</b>",
            "descripcion": "Título preguntas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 0,
            "evaluable"  : true
        }        
    }, 
{  //reactivo 4
"respuestas":[ 
{ 
"t13respuesta":"Exigir que se respeten los derechos de todas las personas. ", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Resolver los problemas de la comunidad realizando juntas y propuestas. ", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Cumplir con las obligaciones que dicta la Constitución. ", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Verificar que lo que promete el gobierno se cumpla. ", 
"t17correcta":"1",  
}, 
{ 
"t13respuesta":"Indiferencia ante las acciones del gobierno. ", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"Conformismo ante las situaciones de violencia. ", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"No mostrar interés por ejercer los derechos. ", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"Fomentar la discriminación y desigualdad. ", 
"t17correcta":"0", 
}, 
],
"pregunta":{ 
"c03id_tipo_pregunta":"2", 
"t11pregunta": "<b>Selecciona todas las respuestas correctas.</b><br><br> ¿Qué acciones son ejemplo de la participación ciudadana? ", 
"t11instruccion": "", 
} 
},
{  //reactivo 5
        "respuestas": [
            {
                "t13respuesta": "Espontáneo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Informal",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Formal",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Votar para las elecciones de Presidente de la República.", 
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Huelgas para defender los derechos de la Universidad.", 
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Participar como jurado.", 
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Realizar alguna colecta de bienes tras un desastre natural.", 
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Realizar sindicatos de trabajadores para la educación al servicio del pueblo.", 
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Recaudación de firmas para arreglar baches  de su comunidad.", 
                "correcta"  : "0"
            },
            { 
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Marchas por el cese de la violencia.", 
                "correcta"  : "1" 
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b>Observa la siguiente matriz y marca la opción que clasifique la participación ciudadana.</b>",
            "descripcion": "Título preguntas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 0,
            "evaluable"  : true
        }        
    },
    {  //reactivo 6
"respuestas":[ 
{ 
"t13respuesta":"Surgen problemas mayores, pues al no haber una mayoría y un consenso, no se encuentran soluciones a las situaciones políticas y sociales. ", 
"t17correcta":"1",  
}, 
{ 
"t13respuesta":"Se denota ignorancia por parte de los ciudadanos, creando una democracia sin errores. ", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"El gobierno queda mal al no proponer soluciones que dejen sin beneficio a los involucrados. ", 
"t17correcta":"0", 
}, 
],
"pregunta":{ 
"c03id_tipo_pregunta":"1", 
"t11pregunta": "<b>Selecciona la respuesta correcta.</b><br><br>¿Qué consecuencia puede haber cuando los ciudadanos no participan de forma responsable en las decisiones que los incluyen?", 
"t11instruccion": "En 2016 el gobierno de Reino Unido solicitó un Referendo a los ciudadanos para decidir si permanecían o salían de la Unión Europea, el resultado fue un 51% a favor contra un 49% en contra, la razón principal de este resultado se debe a la poca participación del sector joven a la hora de la votación y el dominio de los grupos conservadores o gente mayor que anhelaba un gobierno como en sus épocas.",  
} 
},
{  //reactivo 7
"respuestas":[ 
{ 
"t13respuesta":"Garantizar el derecho a las personas a la información pública gubernamental. ", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Proteger los datos personales que están en manos del gobierno federal, como de los particulares.", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Facilitar el acceso de tus datos personales a las grandes corporaciones.", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"Contribuir en el desarrollo y análisis de normas jurídicas que nos permitan salvaguardar nuestra información privada.", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"Guardar la información de los gobernantes acerca de los gastos públicos.", 
"t17correcta":"0", 
}, 
],
"pregunta":{ 
"c03id_tipo_pregunta":"2", 
"t11pregunta": "<b>Selecciona todas las respuestas correctas.</b><br><br>¿De qué se encarga el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales (INAI)?", 
"t11instruccion": "",  
} 
},
{  //reactivo 8
"respuestas":[ 
{ 
"t13respuesta":"Demuestra un nulo respeto por la privacidad por parte del gobierno a sus ciudadanos.", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Demuestra que el gobierno está más interesado en callar las críticas que en encerrar a los criminales.", 
"t17correcta":"1", 
}, 
{ 
"t13respuesta":"Demuestra el poder tan grande que ha alcanzado la tecnología.", 
"t17correcta":"0", 
}, 
{ 
"t13respuesta":"No son graves, siempre y cuando sea con el fin de garantizar nuestra seguridad.", 
"t17correcta":"0", 
}, 
],
"pregunta":{ 
"c03id_tipo_pregunta":"2",  
"t11pregunta": "<b>Selecciona las respuesta correctas.</b><br><br>Independientemente de la culpabilidad o no del gobierno mexicano, ¿por qué son tan graves dichas acusaciones?", 
"t11instruccion": "El 19 de Junio de 2017 el diario The New York Times entre otros medios, reveló que ciertos periodistas y activistas por los derechos humanos de México como, Carmen Aristegui, Carlos Loret de Mola, Salvador Camarena, de Mexicanos Contra la Corrupción y la Impunidad, entre otros, fueron víctimas de espionaje por el gobierno mexicano con un software llamado Pegasus, el cual permitía revisar las llamadas, mensajes y correos de cualquier celular que se deseara, además, de poder intervenir en el micrófono y la cámara del mismo.", 
} 
}  
]; 


 