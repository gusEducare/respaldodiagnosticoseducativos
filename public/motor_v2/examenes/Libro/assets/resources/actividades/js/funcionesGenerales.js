var transitionEnd = "DOMSubtreeModified webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend";
var letrasArr = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
var letra = letrasArr.split("");
var preguntaActual=0;
var strEvaluacion = "";
var libro = "ALM";
var resultados;
var lastResort;
var primerVuelta = true;
var direccionCambio="-100";//define la direccion que tomara al cambiar de pregunta en la actividad
var finalizaActividad = true;
var textoFinaliza = false;
//variables de evaluacion
var aciertos = 0;
var totalPreguntas=0;
var intentosRestantes=0;
var esUltima = false;
var evaluable = true;
var incisos=false; //variable arrastra corta incisos
var conRestantes;//variable de agrupa contenedor+
var strEvaluacion="";
var objCadenas=[];//almacena todas las cadenas generadas a lo largo de la evaluacion
var arrCorrectas=[];//almacena los aciertos, 1 por reactivo
var maxWidth=0;

function fGenerales(){
    //restaura las variables de evalaucion
    if(evaluacion && strAmbiente === "PROD"){
        objCadenas = parentData.respuestaUsuario;
        objCadenas = objCadenas === null ? [] : objCadenas.split("¬");
        arrCorrectas = parentData.correcta.split("");
        if(objCadenas.length !== json.length){
            cargarObjCadenas();
        }
    }
    //añade los sonidos al dom
    $("body").append('<audio id="snd_clic" src="sounds/click.mp3"></audio><audio id="snd_correcta" src="sounds/campana.mp3"></audio><audio id="snd_incorrecta" src="sounds/corneta.mp3"></audio>');
    if(strAmbiente === "PROD"){
        libro = setProfMode ? "PROF" : "ALM";//(location.href.split("/assets/resources")[0].split("/").pop()).charAt(3) === "P" ? "PROF" : "ALM";
        strEvaluacion = evaluacion ?  "/motor_v2/":"";
    }else{
        $("body").css({boxShadow:"4px 4px 12px 3px rgba(0,0,0, 0.2)"});
        if(window.self === window.top){ !evaluacion ? $("body").css({margin: "30px auto"}) : $("body, html").css({margin: "0px auto", height:"100%"}); }
    }
    esUltima = preguntaActual === json.length -1;
    strAmbiente === "DEV" ? cargarObjCadenas() : "";
    json !== undefined && json !== null ? importResources() : "";
}
function importResources(){
        //obtiene el tipo de actividad y los recursos necesarios
        var numeroVariante = obtenerTipoActividad(preguntaActual);
        var numeroPregunta = json[preguntaActual].pregunta.c03id_tipo_pregunta;
        var recurso = tiposActividad[numeroPregunta].variantes[numeroVariante];
        //carga de los recursos de la actividad
        if(recurso !== lastResort){
            lastResort = recurso;
            importCss(strEvaluacion+"css/actividades/", "estilos"+recurso, "Actividad", true, function(){//importa recursos de actividad
                limpiarEventos();
                iniciarActividad();
                lastResort !== "ArrastraCorta" && $(".respuestaDrag").length>0 ? dragAndDrop() : "";
                if(primerVuelta){
                    primerVuelta = false;
                    if(evaluacion && libro !== "PROF"){ 
                        importCss(strEvaluacion+"css/", "estilosEval", "Eval", true, function(){ initEval(); }); 
                    }else{
                        if(libro === "PROF"){
                            importCss(strEvaluacion+"css/", "estilosProfesor", "prof", true,function(){ 
                                resolverActividades();
                                importCss(strEvaluacion+"css/", "estilosAvance", "barraAvance", true, function(){ barraAvance(); });
                            });
                        }else{
                            importCss(strEvaluacion+"css/", "estilosAvance", "barraAvance", true, function(){ barraAvance(); });
                        }
                    }
                    setTimeout(function(){//oculta preview
                        $("aside .btn.btn_start").removeClass("loading");
                        window.clearInterval(loadingInterval); window.clearInterval(loadingBarInterval);
                        !evaluacion ? setTimeout(function(){ document.querySelector("#previewActividad").style.left = "-102%"; }, 350) : "";
                    }, 500);
                }
            });
        }else{
            iniciarActividad();
            libro === "PROF" ? $("#contenidoActividad").on(transitionEnd, function(){
                $("#contenidoActividad").off(transitionEnd);
                resolverActividades();
                $("#contenidoActividad").on(transitionEnd);
            })
            : lastResort !== "ArrastraCorta" && $(".respuestaDrag").length>0 ? dragAndDrop() : "";
        }
}
function limpiarEventos(){//apaga los eventos basura olvidados
    //resetea valores
    evaluacion ? maxWidth = 0 : "";
    //arrastra corta
    $("#contenidoActividad").off("mouseup touchend");
    $("#contenidoActividad").off("scroll");
}
////////////////funcione cque se pueden compartir para todas las actividades////
function hexToRgb(hex) {//cambia hexadecimal a rgb
    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;
    return r + "," + g + "," + b;
}
function sndClick(){ sndRestart("snd_clic"); }
function sndCorrecto() { sndRestart("snd_correcta"); }
function sndIncorrecto() { sndRestart("snd_incorrecta"); }
function sndRestart(element){
    element = document.getElementById(element);
    element.pause();
    element.currentTime = 0;
    element.play();
}
function cambiarRutaImagen(element){
    var rLibro = "", raiz = "/";
    if(strAmbiente === "PROD" && evaluacion){
        rLibro = ""; rutaLibro = ""; raiz = "";
    }else{
        rLibro = idActividad.charAt(5) === "B" && idActividad.charAt(9) === "N" ? "../../../../EVAL" : rutaLibro;
    }
    element = eliminarP(element);//elimina etiquetas p
    if(element.search("<img")!==-1){
        var url, elements;
        elements = element.match(/<img.*?src='(.*?)'/g);
        elements === null ? elements = element.match(/<img.*?src="(.*?)"/g) : "";
        $.each(elements, function(i, e){
            e.search("src='")!==-1 ? url = e.match(/<img.*?src='(.*?)'/)[1] : "";
            e.search('src="')!==-1 ? url = e.match(/<img.*?src="(.*?)"/)[1] : "";
            url = url === undefined ? (e.split(/<img.*?src=/)[1].split(".png")[0])+".png" : url;
            element = element.replace(e.split(/<img.*?src=/)[1], "'"+rLibro+raiz+url+"'");
        });
    }else if(element.search(".png")!==-1){
        element = rLibro+raiz+element;
        element = "url('"+element+"')";
    }
    return element;
}
function eliminarP(element){
    element = element.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
    return element;
}
function eliminarAcentos(element) {
    var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
    var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
    for (var i=0; i<acentos.length; i++) {
        element = element.replace(acentos.charAt(i), original.charAt(i));
    }
    return element;
}
function cartasRandom(cadena){//aplica random a las posiciones de los elementos
    if(!evaluacion){
        cadena = cadena.slice(0, -1);//se quita la coma al final
        cadena = cadena.split(",").sort(function(){return 0.75-Math.random();});//se divide en elementos
        $.each(cadena, function(index, element){//se reposicionan los elementos
            if(index+"" !== element+""){
                $(".respuestaDrag:nth("+index+"):not(.contenedor .respuestaDrag)").insertBefore($(".respuestaDrag:nth("+element+"):not(.contenedor .respuestaDrag)"));
            }
        });
    }
}
var coloresTarjeta = ["51c3af", "f8b559", "accb69", "61c2d6", "3e82c1", "9a9a9a", "e16fa6",
    "fecb2f", "e65c73", "ce92bc", "b6cd4f", "b664a4", "eb7c8f", "639bcd", "ed7d21"];
function coloresRandom(selector) {
    coloresTarjeta.sort();
    var contador = parseInt(Math.random()*coloresTarjeta.length);
    $(selector).each(function(index, element){
        $(element).css("background-color", "#"+coloresTarjeta[contador]);
        contador === coloresTarjeta.length-1 ? contador=0 : contador++;
    });
}
function sigPregunta(){//determina si hay mas preguntas o se finaliza la evaluacion
    $("#contenidoActividad").addClass("lock");
    libro === "PROF" ? nextA() : setTimeout(function(){ nextA(); }, 750);
    function nextA(){
        intentosRestantes=0;
        esUltima && !evaluacion && finalizaActividad && !setProfMode ? finalizarActividad() : siguientePregunta();
        evaluacion && strAmbiente === "PROD" ? parent.window.preguntaActual = window.preguntaActual : "";
        $("#contenidoActividad").trigger("cambioDePregunta");
    }
}
function siguientePregunta(){
    $("#contenidoActividad").trigger("siguiente");
    preguntaActual++;
    $("#contenidoActividad").css({left:direccionCambio+"%", "transition":"left 1s"});
    $("#contenidoActividad").on(transitionEnd, function(){
        $("#contenidoActividad").off(transitionEnd).removeClass("lock").css({left:(direccionCambio*-1)+"%", "transition":"none"});
        importResources();
        esUltima = preguntaActual === json.length -1;
        setTimeout(function(){
            $("#contenidoActividad").css({left:"0%", "transition":"left 1s"}).on(transitionEnd, function(){
                $(this).off(transitionEnd).trigger("transitionEnded");
            });
        }, 20);
    });
}
function finalizarActividad(){
    try{
        if(noEvaluable){ totalPreguntas=1; aciertos=1; }
        var obj = {};
        obj.id = idActividad;           obj.score = aciertos;
        obj.score_max = totalPreguntas; obj.attempts = 0;
        window.top.postMessage({saveProgress:true, obj:obj}, "*");
    }catch(e){ console.log(e); }
        var texto = {}; hideProgress();
        $("#modal").removeClass("hidden").css("background","rgba(255,255,255, 0.85)");
        $("#modalContent").css({width:"100%", height:"100%"});
        ////textos al finalizar la actividad español e ingles
        texto = evaluable === false ?  
            {top:{es:"", en:""}, bottom:{es:"Autoevaluación finalizada", en:"Self-assessment completed"}} 
            : {top:{es:"Has obtenido", en:"You got"}, bottom:{es:"aciertos", en:"hits!"}};
        !finalizaActividad || textoFinaliza ? texto = 
            {top:{es:"", en:""}, bottom:{es:"¡Actividad finalizada!", en:"Activity completed!"}} 
            : "";
        ////se añade el contenido al modal y se ejecutan las animaciones
        $("#modalContent").append("<div id='trofeo' class='hideText' top='"+texto.top[lang]+"' bottom='"+texto.bottom[lang]+"'>"+(evaluable ? aciertos:"")+"</div>");
        setTimeout(function(){
            $("#trofeo").css({width:"350px", height:"350px", opacity:"1"});
            setTimeout(function(){
                $("#trofeo").css({width:"300px", height:"300px", opacity:"1"});
                setTimeout(function(){$("#trofeo").removeClass("hideText");}, 250);
                setTimeout(function(){
                    $("#trofeo").addClass("hideText");
                    setTimeout(function(){
                        $("#modal").css("background","transparent");
                        $("#trofeo").css({width:"60px", height:"60px", top:"35px", right:"110px"});
                    }, 250);
                }, 4000);
            }, 350);
        }, 350);
    ////añade boton reiniciar
    $('<img src="img/flecha.png" id="reload" alt=""/>').appendTo("#modalContent").on("click touchend", function(){//Reinicia la actividad
        $(this).css({"transform":"scale(1.25, 1.25)", transition:"transform 0.15s"}).fadeOut(150);
        setTimeout(function(){ location.reload(); }, 500);
    });
}
////////////////////via alterna para enventos del tipo elementFromPoint/////////
function elementsFromPoint(x,y){//obtiene todos los elementos del dom respecto a un punto de la pantalla
    var elements = [], previousPointerEvents = [], current, i, d;
    while ((current = $.elementFromPoint(x,y)) && elements.indexOf(current)===-1 && current != null) {
            elements.push(current);
            previousPointerEvents.push({
            value: current.style.getPropertyValue('pointer-events'),
            priority: current.style.getPropertyPriority('pointer-events')
        });
            current.style.setProperty('pointer-events', 'none', 'important'); 
    }
    for(i = previousPointerEvents.length; d=previousPointerEvents[--i]; ) {
            elements[i].style.setProperty('pointer-events', d.value?d.value:'', d.priority); 
    }
    return elements;
}
(function ($){//$.elementFromPoint(x, y) //obtiene elemento html respecto a pocision el la pantalla
  var check=false, isRelative=true;
  $.elementFromPoint = function(x,y)
  {
    if(!document.elementFromPoint) return null;
    if(!check){
      var sl;
      if((sl = $(document).scrollTop()) >0){
        isRelative = (document.elementFromPoint(0, sl + $(window).height() -1) == null);
      }else if((sl = $(document).scrollLeft()) >0){
        isRelative = (document.elementFromPoint(sl + $(window).width() -1, 0) == null);
      }
      check = (sl>0);
    }
    if(!isRelative) { x += $(document).scrollLeft(); y += $(document).scrollTop(); }
    return document.elementFromPoint(x,y);
  }
})(jQuery);
///////////////guarda las respuestas de los usuarios en actividades no evaluables o de evaluacion/////////////////////
function cargarObjCadenas() {
//    if (objCadenas.length === 0) {
//        objCadenas = localStorage.getItem("avencesActividad:"+idActividad);
//        if(objCadenas !== null){
//            objCadenas = JSON.parse(objCadenas);
//            restaurarAvace();
//        }else{objCadenas = [];
//        }
//    } else{ restaurarAvace(); }
    while (json.length > objCadenas.length) {
        objCadenas.push(null);
    }
}
function guardarAvance(){
    var respuestaUsuario=[];
    switch(lastResort){
        case "Graficas":
            var selected;
            $(".categoria").each(function(index, element){
                selected = $("tr td:nth-child("+($(element).index()+1)+")").filter("td.selected");
                if(selected.length>0){
                    respuestaUsuario.push($(selected).parent().index()+"-"+$(selected).index());//tr,td
                }else{
                    respuestaUsuario.push("x-x");
                }
            });
            respuestaUsuario = respuestaUsuario.join(",");
            break
        case "MapasDeBits":
            $("td:not(#cellEmpty)").each(function(index, element){
                if(element.className.indexOf("pincel")!==-1){
                    respuestaUsuario.push(index+"-"+element.className.split("pincel")[1]);
                }
            });
            respuestaUsuario = respuestaUsuario.join(",");
            break
    }
    objCadenas[preguntaActual] = respuestaUsuario;
    evaluacion ? guardarCadenaUsuario(respuestaUsuario) : "";
    !evaluacion ? localStorage.setItem("avencesActividad:"+idActividad, JSON.stringify(objCadenas)) : "";
    return respuestaUsuario;
}
function restaurarAvace(){
    if(objCadenas[0]!==undefined && objCadenas[preguntaActual]!==null){
        var cadena = objCadenas[preguntaActual].split(",");
        switch(lastResort){
            case "Graficas":
                var element, serie;
                $.each(cadena, function(i, e){
                    if(e.indexOf("x")===-1){
                        element = $("tr").eq(e.split("-")[0]).find("td").eq(e.split("-")[1]).addClass("selected");
                        serie = $(".serie").eq(i).addClass("selected");
                        calcularAltura(serie, element);
                    }
                });
                break;
            case "MapasDeBits":
                $.each(cadena, function(i, e){
                    $("td:not(#cellEmpty)").eq(e.split(",")[0]).addClass("pincel"+e.split(",")[1])
                });
                break
        }
    }
}
///////////////Funciones compartidas de tipo drag and drop/////////////////////
function dragAndDrop(){
    document.getElementById("contenidoActividad").addEventListener("mousemove", {passive:false});
    document.getElementById("contenidoActividad").addEventListener("touchmove", {passive:false});
    $("#contenidoActividad #respuestas").append("<style> .respuestaDrag.correctAnswer{ margin: "+$(".contenedor:first").css("border-width")+"; } </style>");
    var respuestaDrag, contenedor, esCorrecta;
    $(".respuestaDrag:first").removeClass("resph");
    var top, left, sTop;
    $(".respuestaDrag").draggable({//evento drag
        scroll:true,
        scrollSensitivity: 150,
        scrollSpeed: 14,
        start:function(){
            sndClick();
            esCorrecta = false;
            sTop = $("#contenidoActividad").scrollTop();
            $(".ui-draggable-highlight").css({zIndex:900});
            respuestaDrag = $(this).css({transition:"none", zIndex:"1000"}).addClass("ui-draggable-highlight");
            $("#contenidoActividad").on("mousemove touchmove", function(event){ event.preventDefault(); });
            top = respuestaDrag[0].style.top;
            left = respuestaDrag[0].style.left;
        },
        drag: function(event, ui){
            ui.position.top += $("#contenidoActividad").scrollTop() - sTop;
        },
        stop: function(){
            dragg = false;
            $("#contenidoActividad").off("mousemove touchmove").on("mousemove touchmove");
            !esCorrecta ? respuestaDrag.css({top: top, left: left, transition:"top 1s, left 1s"}) :"";
            respuestaDrag.on(transitionEnd, function(){ 
                $(this).css({zIndex:0}).off(transitionEnd).removeClass("ui-draggable-highlight");
            });
        }
    });
    $(".contenedor:not(.mostrar)").droppable({//evento drop
        hoverClass:"ui-state-highlight",
        tolerance:"intersect",
        drop:function(){
            if(evaluacion && respuestaDrag.attr("t11") !== undefined){
                $(".contenedor").eq(respuestaDrag.attr("t11")).droppable("option", "disabled", false).removeClass("disabled");
            }
            contenedor = $(this);
            var t17 = respuestaDrag.attr("t17");
            var t11 = contenedor.attr("t11");
            esCorrecta = t17.split(",").indexOf(t11) !== -1 || evaluacion;
            var grupos = json[preguntaActual].pregunta.agrupaContenedores === true;
            if(grupos){//define en arrastra grupos si el contenedor ya esta ocupado por algun grupo y evalua si son identicos
                if(contenedor.attr("grupo") !== undefined){//contenedor ocupado
                    esCorrecta = contenedor.attr("grupo") === respuestaDrag.attr("grupo");
                }else{//contenedor desocupado
                    esCorrecta = $(".contenedor[grupo='"+respuestaDrag.attr("grupo")+"']").length === 0;
                }
            }
            //eventos correcta/incorrecta
            if(esCorrecta){
                contenedor.addClass("disabled");
                var parent = $("#fondoPregunta, #contenido, #contenedores");
                if(contenedor[0].style.top !== ""){
                    var t, l;
                    t = contenedor.position().top;
                    l = contenedor.position().left;
                    respuestaDrag.css({position:"absolute", //fija la posicion de la respuesta
                        top:Math.round(t + (contenedor.css("border-top-width").replace("px","")*1)) + "px",
                        left:Math.round(l + (contenedor.css("border-left-width").replace("px","")*1)) + "px"
                    }).addClass("correctAnswer").appendTo(parent);
                }else{
                    respuestaDrag.css({position:"absolute", //fija la posicion de la respuesta
                        top: Math.round(contenedor.offset().top - parent.offset().top + parseInt(contenedor.css("border-top-width"))) + "px",
                        left:Math.round(contenedor.offset().left - parent.offset().left + parseInt(contenedor.css("border-left-width"))) + "px"
                    }).addClass("correctAnswer").appendTo(parent);
                }
                $(".respuestaDrag:not(.correctAnswer, .noVisible):last").removeClass("resph");
                respuestaDrag.attr("grupo") !== undefined ? contenedor.attr("grupo", respuestaDrag.attr("grupo")) : "";
                if($(".contenedor.noHeight").length>0){
                    if(respuestaDrag.attr("t11") === undefined){
                        var respHidden = respuestaDrag.clone().css({top:0, left:0, position:"relative"}).addClass("noVisible lock").removeClass("correctAnswer");
                        $(respHidden).insertBefore(respuestaDrag);
                        setTimeout(function(){
                            respHidden.addClass("noWidth noHeight");
                            $(".contenedor:not(.disabled):not(.noHeight)").length === 0 ?
                            $(".contenedor.noHeight:first").removeClass("noHeight") : "";
                            respHidden.on(transitionEnd, function(){ $(this).remove(); });
                        }, 20);
                    }
                }else if(respuestaDrag.attr("t11")===undefined){ reubicarCartas(); }
                if(evaluacion){
                    contenedor.removeAttr("t17");
                    respuestaDrag.attr("t11",t11);
                    
                }else{
                    sndCorrecto();
                    intentosRestantes > 0 ? aciertos++ : "";
                    respuestaDrag.draggable("disable");
                    if(grupos){//siguiente pregunta
                        $(".correctAnswer").length === conRestantes ? sigPregunta() : "";
                    }else{ $(".respuestaDrag.correctAnswer").length === $(".contenedor:not(.mostrar)").length ? sigPregunta() : ""; }
                }
                !grupos || $(".respuestaDrag[grupo='"+respuestaDrag.attr("grupo")+"']:not(.correctAnswer)").length===0 ? contenedor.attr("t17", respuestaDrag.attr("t17")).droppable("option", "disabled", true) : "";
            }else{ sndIncorrecto(); }
            intentosRestantes--;
            function reubicarCartas(){
                var respHidden = respuestaDrag.clone().css({top:0, left:0, position:"relative"}).addClass("noVisible lock").removeClass("correctAnswer");
                $(respHidden).insertBefore(respuestaDrag);
                if($(".respuestaDrag:not(.correctAnswer, .noVisible)").length > 1){
                    var element = $(".respuestaDrag:not(.correctAnswer, .noVisible):last");
                    if(element.index() > respuestaDrag.index()){
                        var top, left, offsetRd, offsetEl;//variables que definen la nueva posicion al reacomodar la ultima tarjeta
                        offsetEl = element.attr("offset")!==undefined ? element.attr("offset") : "0-0";
                        offsetRd = respuestaDrag.attr("offset")!==undefined ? respuestaDrag.attr("offset") : "0-0";
                        top = respHidden.offset().top + (offsetEl.split("-")[1]*1 - offsetRd.split("-")[1]*1);
                        left = respHidden.offset().left + (offsetEl.split("-")[0]*1 - offsetRd.split("-")[0]*1);
                        element.addClass("lock").css("transition","0.75s").offset({top:top, left:left});
                        element.on(transitionEnd, function(){
                            element.off(transitionEnd); element.insertBefore(respHidden); respHidden.remove();
                            element.removeClass("lock").css({position: "relative", top:0, left:0, transition:"none"});
                        });
                    }else{ respHidden.remove(); }
                }else if($(".respuestaDrag:not(.correctAnswer, .noVisible)").length === 1){
                    setTimeout(function(){
                        var rHTop = respHidden.offset().top + respHidden.css("margin-top").replace("px","")*1;
                        var rDTop = $(".respuestaDrag:not(.correctAnswer, .noVisible)");
                        rDTop = rDTop.offset().top + rDTop.css("margin-top").replace("px","")*1;
                        rHTop !== rDTop ? respHidden.addClass("noHeight").css("display","block") : respHidden.addClass("noWidth");
                        respHidden.on(transitionEnd, function(){ respHidden.on(transitionEnd); respHidden.remove(); });
                    }, 20);
                }
            }
        }
    });
    if(evaluacion){
        $("#respuestas").droppable({
            drop: function(){
                if(respuestaDrag.attr("t11")!==undefined){
                    $(".contenedor").eq(respuestaDrag.attr("t11")).droppable("option", "disabled", false)
                                    .removeAttr("t17").removeClass("disabled");
                    revertResp(respuestaDrag.removeAttr("t11").css("color","transparent"), "appendTo");
                    top = 0; left = 0;
                    if(lastResort === "OrdenaElementos"){
                        while($(".contenedor:not(.noHeight):not(.disabled)").length>1){
                            $(".contenedor:not(.noHeight):last").addClass("noHeight");
                        }
                    }
                }
                return false;
            }
        });
    }
}
function revertResp(element, action){//revierte la respuesta a su padre original
    element[action]($("#respuestas")).draggable({revert:false}).addClass("noWidth");
    element.removeClass("correctAnswer");
    element[0].style.removeProperty("top");
    element[0].style.removeProperty("left");
    element[0].style.removeProperty("position");
    element[0].style.removeProperty("transition");
    setTimeout(function(){
        element.css({transition:"0.5s", pointerEvents:"none"}).removeClass("noWidth");
        setTimeout(function(){
            element[0].style.removeProperty("transition");
            element[0].style.removeProperty("color");
            element[0].style.removeProperty("pointer-events");
        }, 500);
    }, 20);
}