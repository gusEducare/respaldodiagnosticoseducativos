var freeInterval, arrowInterval, dragInterval, inDrag=false;
function iniciarActividad(){
    $("#contenidoActividad").bind("cambioDePregunta", function(){
        clearInterval(freeInterval);
        clearInterval(arrowInterval);
        clearInterval(dragInterval);
    });
    //se genera la estructura html de la actividad
    $("#contenidoActividad").html("<div id='fondoPregunta'></div><div id='colRespuestas'></div>");
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $("#fondoPregunta").css("background-image", cambiarRutaImagen(json[preguntaActual].pregunta.url));
    //se añaden contenedores
    $.each(json[preguntaActual].contenedores, function(index, element){
        $("#fondoPregunta").append("<div t11='"+index+"' class='contenedor free'></div>");
        if(json[preguntaActual].pregunta.direcciones !== false){//habilita bandera que oculta flechas de direccion
            //se compara la direccion de la flecha
            var direccion = false;
            direccion = !direccion && element.Contenedor[2] === "izquierda" ? "left" : direccion;
            direccion = !direccion && element.Contenedor[2] === "derecha" ? "right" : direccion;
            direccion = !direccion && element.Contenedor[2] === "arriba" ? "up" : direccion;
            direccion = !direccion && element.Contenedor[2] === "abajo" ? "down" : direccion;
            direccion = !direccion  ? "left" : direccion;

            $(".contenedor:last").append("<div class='arrow "+direccion+"'></div>");//se añade la felecha
        }
        var top = element.Contenedor[1].split(",")[0],
        left = element.Contenedor[1].split(",")[1],
        background = element.Contenedor[3].split(",")[1];
        background === undefined ? background = "white" : "";
        $(".contenedor:last").css({top:top+"px", left:left+"px", background: background});
    });
    //se añaden las respuestas
    var preguntas="", cadena="";
    $.each(json[preguntaActual].respuestas, function(index, element){
        cadena += index+",";
        //habilita la bandera rotar
        element.rotar !== undefined ? $(".contenedor:nth("+element.t17correcta+")").css("transform","rotate("+element.rotar+"deg)") : "";
        //se añaden elementos de respuesta y etiquetas
        preguntas += "<div class='respuesta'><div class='etiqueta'>"+cambiarRutaImagen(element.etiqueta)+"</div></div>";
    });
//    +cambiarRutaImagen(element.t13respuesta)
//    t17 = '"+element.t17correcta+"'
    $("#colRespuestas").html(preguntas);    
    
    //añade contenido a las respuestas
    cadena = cadena.slice(0, -1);//se quita la coma al final
    cadena = cadena.split(",");//se separan los elementos
    json[preguntaActual].pregunta.random !== false && !evaluacion ? cadena = cadena.sort(function(){return 0.75-Math.random()}) : "";//aplica bandera random
    $.each(json[preguntaActual].respuestas, function(index, element){
        $(".respuesta:nth("+cadena[index]+")").append(cambiarRutaImagen(element.t13respuesta)).find(".etiqueta").attr("t17", element.t17correcta);
    });
    
//aplica bandera para cambiar forma
    if(json[preguntaActual].pregunta.forma !== undefined && json[preguntaActual].pregunta.forma === "circulo"){
        $(".contenedor, .etiqueta").css({borderRadius:"50%"});
    }
//aplica bandera para respuestas transparentes
    json[preguntaActual].pregunta.colorFondo === false ? $(".etiqueta").css("background","transparent") : "";
//aplica bandera tamnyoReal
    var tamañoReal = json[preguntaActual].pregunta.tamanyoReal === true;
    tamañoReal ? $("#fondoPregunta").css("background-size","auto auto") : "";
//aplica bandera anchoImagen
    var imgWidth = json[preguntaActual].pregunta.anchoImagen !== undefined ? json[preguntaActual].pregunta.anchoImagen : 0;
    imgWidth > 0 ? ($("#fondoPregunta").css("width", imgWidth+"%"), $("#colRespuestas").css("width", (100 - imgWidth)+"%")) : "";
    
    setTimeout(function(){
        //animaciones para los contenedores libres
        $(".free").css({boxShadow:"0 0 2px rgba(0,0,0, 0.1)"});
        setTimeout(function(){
            $(".free").css({boxShadow:"0 0 7px rgba(0,0,0, 1)"});
        },1600);
        freeInterval = setInterval(function(){
            $(".free").css({boxShadow:"0 0 2px rgba(0,0,0, 0.1)"});
            setTimeout(function(){
                $(".free").css({boxShadow:"0 0 7px rgba(0,0,0, 1)"});
            }, 1600);
        }, 3200);
        //animacion para las flechas de los contenedores libres
        $(".free .arrow").hide();        
        $(".free .arrow").fadeIn(350);
        setTimeout(function(){
            $(".free .arrow").fadeOut(350);
        },1600);
        arrowInterval = setInterval(function(){//intervalo de animacion
            $(".free .arrow").fadeIn(350);
            setTimeout(function(){
                $(".free .arrow").fadeOut(350);
            },1600);            
        }, 2000);
    }, 100);
    
    //se inician variables de evaluacion 
    intentosRestantes = $(".contenedor").length;
    totalPreguntas+=intentosRestantes;    
    
    //eventos drag and drop
    document.getElementById("contenidoActividad").addEventListener("mousemove",{passive:false});
    document.getElementById("contenidoActividad").addEventListener("touchmove",{passive:false});
    var posEnd=[];
    $(".respuesta").each(function(index, element){
        element.addEventListener("mousedown", function(event){
            dragStart(event, this);
        }, true);
    });
    function dragStart(event, respuesta){
        sndClick();
        clearInterval(dragInterval);
        inDrag=true;
        $("*").css("cursor","pointer");
       var nuevaEtiqueta;
       nuevaEtiqueta = $(respuesta).find(".etiqueta").prop("outerHTML");
       $("#contenidoActividad").append(nuevaEtiqueta);
       while($("#contenidoActividad>.etiqueta").length>1){
           $("#contenidoActividad>.etiqueta:last").remove();
       }
       var etiqueta = $("#contenidoActividad>.etiqueta");
        //se pocisiona la etiqueta
        var bodyLeft = $("body").offset().left, bodyTop = $("body").offset().top;
        if(event.pageX !== undefined){
            posEnd[0]=event.pageX; 
            posEnd[1]=event.pageY;
        }
       $(etiqueta).css({top:(posEnd[1]-bodyTop-20)+"px", left:(posEnd[0]-bodyLeft-20)+"px"});
       //se bloquea la generacion de nuevas etiquetas
       $(".respuesta").addClass("lock");
       //intervalo de arrastre
       $(etiqueta).css({transform:"scale(1, 1)", opacity:"1"});
       dragInterval = setInterval(function(){
           $(etiqueta).css({top:(posEnd[1]-bodyTop-20)+"px", left:(posEnd[0]-bodyLeft-20)+"px"});
       }, 10);
        $("#contenidoActividad").on("mousemove touchmove", function(event){
            event.preventDefault();
        });
    }
    document.body.addEventListener("mousemove", function(event){
        if(inDrag && event.pageX !== undefined){
            posEnd[0]=event.pageX;
            posEnd[1]=event.pageY;
        }
        event.preventDefault();
    }, true);
    document.body.addEventListener("mouseup", function(event){
        $("#contenidoActividad").off("mousemove touchmove");
        $("#contenidoActividad").on("mousemove touchmove");
        if(inDrag){
            inDrag=false;
            clearInterval(dragInterval);
            $("*:not(.respuesta, .etiqueta)").css("cursor","initial");
            //se obtiene el elemento dropeado
            var elements = elementsFromPoint(event.pageX, event.pageY);
            var dropElement = $(elements).filter(".contenedor");
            if(dropElement[0]!==undefined){//verifica que sea un contenedor
                //se evalua la respuesta
                var esCorrecta=false;
                var dragElement = $("#contenidoActividad>.etiqueta");
                var t11;
                t11 = json[preguntaActual].pregunta.columnas === true ? dropElement.parent(".columna").attr("t11") : dropElement.index();
                var t17 = dragElement.attr("t17");
                if(t17.search(",")!==-1){
                    $.each(t17.split(","), function(i, e){
                        if(t11*1 === (e*1)){
                            esCorrecta = true;
                            return false;
                        }
                    });
                }else{esCorrecta = t11*1 === (t17*1);}
                //eventos correcta/incorrecta
                if(esCorrecta || evaluacion){
                    if(!evaluacion){
                        sndCorrecto();
                        if(intentosRestantes > 0){
                            aciertos++;
                        }
                        $(".respuesta:has(.etiqueta[t17='"+dragElement.attr("t17")+"'])").addClass("solved");
                        dropElement.find(".arrow").fadeOut(100);//muestra la flecha indicadora
                        setTimeout(function(){dropElement.find(".arrow").fadeIn(250);}, 100);
                        var color = dragElement.css("background-color");
                        dropElement.css({background:color, borderColor:color}).removeClass("free").addClass("lock");
                    }else{
                        dropElement.find(".etiqueta").remove();
                        $(".contenedor .etiqueta[t17='"+dragElement.attr("t17")+"']").remove();
                    }
                    dragElement.css({top:0, left:0, margin: 0, paddin: 0});
                    dropElement.append(dragElement);
                    $(".respuesta").removeClass("lock");
                    !evaluacion && $(".contenedor .etiqueta").length === $(".contenedor").length ? (sigPregunta(), 
                                clearInterval(freeInterval), clearInterval(arrowInterval), clearInterval(dragInterval)) : "";
                    $("#contenidoActividad>.etiqueta").remove();
                }else{
                    sndIncorrecto();
                    $("#contenidoActividad>.etiqueta:first").css({background:"#e1385a"})
                    setTimeout(eliminarEtiqueta, 200);
                }
                //ACTUALIZA MARCADOR
                if(intentosRestantes > 0){
                    intentosRestantes--;
                }                
            }else{
                eliminarEtiqueta();
            }
        }
    }, true);
}

function eliminarEtiqueta(){
    $("#contenidoActividad>.etiqueta:first").css({transform:"scale(0.1, 0.1)", opacity:"0", transition:"transform 0.35s, opacity 0.35s"});
    setTimeout(function(){
        $("#contenidoActividad>.etiqueta").remove();
        $(".respuesta").removeClass("lock");
    },350);
}
///////repertorio de banderas nuevas
//  "forma":        define la forma de la respuesta circulo o cuadrado(predefinido)
//  "rotar":        define la rotacion del contenedor   //la bandera se añade en respuestas