var grupos = false;
function iniciarActividad(){
    var tamañoReal = json[preguntaActual].pregunta.tamanyoReal === true;
    $("#contenidoActividad").html("<div id='contenedor'><div id='fondoPregunta'></div><div id='respuestas'></div></div>");
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $("#fondoPregunta").css("background-image", cambiarRutaImagen(json[preguntaActual].pregunta.url));
    evaluacion ? $("#fondoPregunta").css("height","525px") : "";
    //se añaden contenedores
    var maxHeight = 0;//variables de tamaño real
    maxWidth = 0;
    $.each(json[preguntaActual].contenedores, function(index, element){
        $("#fondoPregunta").append("<div t11='"+index+"' class='contenedor'></div>");
        var top = element.Contenedor[1].split(",")[0],
        left = element.Contenedor[1].split(",")[1],
        width = element.Contenedor[3].split(",")[0]*1,
        height = element.Contenedor[3].split(",")[1]*1,
        color = element.Contenedor[5]===undefined ? "white" : element.Contenedor[5];
        color = color===undefined ? "none" : color;
        $(".contenedor:last").css({top:(top*1)+"px", left:(left*1)+"px", width:(width*1)+"px", height: (height*1)+"px", background: color});
        maxHeight = maxHeight < height ? height : maxHeight;
        maxWidth = maxWidth < width ? width : maxWidth;
    });
    
    //se añaden cartas
    var cadena="", contenido, color, colorRespuesta, contador=0;
    $.each(json[preguntaActual].respuestas, function(index, element){
        cadena += index+",";
        if(json[preguntaActual].contenedores[element.t17correcta.split(",")[0]]!==undefined){
            color = json[preguntaActual].contenedores[element.t17correcta.split(",")[0]].Contenedor[5] !== undefined ? json[preguntaActual].contenedores[element.t17correcta.split(",")[0]].Contenedor[5] : "white";
            colorRespuesta = json[preguntaActual].contenedores[element.t17correcta.split(",")[0]].Contenedor[6] === true;
            colorRespuesta ? contador++ : "";
        }
        $("#respuestas").append("<div t17='"+element.t17correcta+"' index='"+index+"' class='respuestaDrag'></div>");
        element.grupoContenedor !== undefined ? $(".respuestaDrag:last").attr("grupo", element.grupoContenedor) : "";
        contenido = cambiarRutaImagen(element.t13respuesta);
        contenido = contenido.search(".png") === -1 ? "<p>"+contenido+"</p>" : contenido;
        tamañoReal ? $(".respuestaDrag:last").html(contenido.replace("url('","<img src='").replace(")","/>")) : "";
        !tamañoReal ? contenido.indexOf("url('")!==-1 ? $(".respuestaDrag:last").css({backgroundImage:contenido, backgroundSize:"100% 100%"}) : $(".respuestaDrag:last").html(contenido) : "";
        colorRespuesta ? $(".respuestaDrag:last").css({backgroundColor:color}) : "";
    });
    contador < json[preguntaActual].contenedores.length/2 ? coloresRandom($(".respuestaDrag")) : "";//aplica colores random
    var width, height, existe, esImagen, img;
    //aplica bandera tamnyoReal
    if(tamañoReal){
        !evaluacion ? $("#fondoPregunta").css("background-size","contain") : $("#fondoPregunta").css("background-size", "auto");
        width = maxWidth; height = maxHeight;
    }
    //aplica la bandera forma
    json[preguntaActual].pregunta.forma === "circulo" ? $(".respuestaDrag, .contenedor").css("border-radius","50%") : "";
    //aplica bandera anchoRespuestas/altoRespuestas
    var anchoRespuestas = json[preguntaActual].pregunta.anchoRespuestas !== undefined ? json[preguntaActual].pregunta.anchoRespuestas : undefined;
    var altoRespuestas = json[preguntaActual].pregunta.altoRespuestas !== undefined ? json[preguntaActual].pregunta.altoRespuestas : undefined;
    if(anchoRespuestas === "iguales"){//valida bandera anchoRespuestas
        width = maxWidth;
    }else if(anchoRespuestas !== undefined && anchoRespuestas*1+"" !== "NaN"){
        width = anchoRespuestas;
    }
    if(altoRespuestas === "iguales"){//valida bandera altoRespuestas
        height = maxHeight;
    }else if(altoRespuestas !== undefined && altoRespuestas*1+"" !== "NaN"){
        height = altoRespuestas;
    }
    $(".respuestaDrag").each(function(index, element){
        existe = $(".contenedor[t11='"+$(element).attr("t17").split(",")[0]+"']")[0]!==undefined;
        esImagen = $(element).find("img")[0]!==undefined;
        width = anchoRespuestas!==undefined ? width : existe ? $(".contenedor[t11='"+$(element).attr("t17").split(",")[0]+"']").width() : $(".contenedor:last").width();
        height = altoRespuestas!==undefined ? height : existe ? $(".contenedor[t11='"+$(element).attr("t17").split(",")[0]+"']").height() : $(".contenedor:last").height();
        $(element).css({width:width+"px", height:height+"px", marginBottom:"10px"});
    });
    $(".respuestaDrag img").css({width:"100%", height:"100%"});
    //aplica bandera sombra
    json[preguntaActual].pregunta.sombras === false ? $(".respuestaDrag").css("box-shadow","none") : "";
    //aplica la bandera bloques
    json[preguntaActual].pregunta.bloque === true ? $(".respuestaDrag").addClass("block") : "";
    //aplica la bandera centrar fondo
    json[preguntaActual].pregunta.centrarFondo === true ? $("#fondoPregunta").css({backgroundPosition:"center"}) : "";
    //aplica la bandera random
    if(json[preguntaActual].pregunta.random !== false){
        cartasRandom(cadena);
    }
    //aplica bandera anchoImagen
    var imgWidth = json[preguntaActual].pregunta.anchoImagen !== undefined ? json[preguntaActual].pregunta.anchoImagen : 0;
    imgWidth > 0 ? ($("#fondoPregunta").css("width", imgWidth+"%"), $("#respuestas").css("width", (100 - imgWidth)+"%")) : "";
    var imgHeight = json[preguntaActual].pregunta.altoImagen !== undefined ? json[preguntaActual].pregunta.altoImagen : 0;
    imgHeight > 0 ? $("#contenedor").css("height", imgHeight+"px") : "";
    //aplica bandera borde
    json[preguntaActual].pregunta.borde === false ? $(".contenedor").css({border:"none"}) : "";
    json[preguntaActual].pregunta.radius !== undefined ? $(".contenedor, .respuestaDrag").css("border-radius", json[preguntaActual].pregunta.radius+"px") : "";
    //oculta elementos fuera de pantalla
    var top, height, maxHeight=0, maxWidth=0;
    $(".respuestaDrag").each(function(index, element){
        top = element.getBoundingClientRect().top - document.body.getBoundingClientRect().top;
        height = element.scrollHeight;
        maxHeight = maxHeight < element.scrollHeight ? element.scrollHeight : maxHeight;
        maxWidth = maxWidth < element.scrollWidth ? element.scrollWidth : maxWidth;
        if(!evaluacion){
            if(top+height > $("#respuestas").height()){
                $(element).addClass("resph");
            }
        }
    });
    //agrupa contenedor
    if(json[preguntaActual].pregunta.agrupaContenedores === true){
        grupos = true; conRestantes = 0;
        var elementos=0, grupos="";
        $.each(json[preguntaActual].respuestas, function(index, element){
            element.grupoContenedor !== undefined ? (elementos++, grupos += element.grupoContenedor+",") : "";
        });
        //elimina elementos repetidos
        Array.prototype.unique=function(a){
          return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
        });
        //se obtienen los contenedoes que seran agrupados
        grupos = grupos.slice(0, -1).split(",").unique();
        conRestantes = $(".contenedor").length - grupos.length + elementos;
        intentosRestantes = conRestantes;
    }else{//arrastra imagen
        intentosRestantes = $(".contenedor").length;
    }
    //generico
    totalPreguntas += intentosRestantes;
    //iguala dimensiones para elementos de distintos tamaños
    var marginX, marginY, element;
    $(".respuestaDrag").each(function(i, e){
        element = $(e);
        if(element.width() < maxWidth || element.height() < maxHeight){
            marginX = Math.round((maxWidth - element.width()) / 2);
            marginY = Math.round((maxHeight - element.height()) / 2);
            element.css({
                marginTop: "+="+marginY,
                marginBottom: "+="+marginY,
                marginLeft: "+="+marginX,
                marginRight: "+="+marginX
            }).attr("offset", marginX+"-"+marginY);
        }
    });
}

// "sombras":       // true/false     //determina si los elementos arrastrables tendran sombra
//anchoRespuestas   //"iguales", 0-9    //al recibir el parametro "iguales", todas las respuestas se adaptan al tamaño del elemento mas grande. SI se intreoduce un valor numerico, todas tomaran el valor introducido
//altoRespuestas   //"iguales", 0-9    //al recibir el parametro "iguales", todas las respuestas se adaptan al tamaño del elemento mas grande. SI se intreoduce un valor numerico, todas tomaran el valor introducido