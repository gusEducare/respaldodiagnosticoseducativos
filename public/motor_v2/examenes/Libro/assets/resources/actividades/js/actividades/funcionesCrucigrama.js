var customDir, increment;
var correctas, write = true;
var caracteres = "0123456789"+letrasArr;
function iniciarActividad(){
    json[preguntaActual].pocisiones !== undefined ? parseJson() : "";
    correctas = 0;
    intentosRestantes=json[preguntaActual].respuestas.length;
    totalPreguntas+=intentosRestantes;
    $("#contenidoActividad").html(//añade la estructura inicial del crucigrama
        "<div class='panel preguntas'>"
            +"<div class='contenedor v' type='Verticales'>"
                +"<hr class='btn back'><hr class='btn next'>"
            +"</div>"
            +"<div class='contenedor h' type='Horizontales'>"
                +"<hr class='btn back'><hr class='btn next'>"
            +"</div>"
        +"</div>"
        +"<div class='panel cuadricula'>"
        +"</div>"
    );
    corregirIndices();
    $(".btn").click(function(){
        var element = $(this).parent().find(".pregunta.reveal");
        var next = $(this).hasClass("next") ? element.next(".pregunta") : element.prev(".pregunta");
        if(next[0]!==undefined){
            element.removeClass("reveal");
            next.addClass("reveal");
            $(this).addClass("selected");
            var cell = $("td.number[dir*='"+$(next).attr("dir")+"'][nPreg='"+next.attr("nPreg")+"']");
            cell.trigger("click");
        }
        return false;
    });
    $(".contenedor").click(function(){
        customDir = true;
        $(".contenedor").removeClass("selected");
        $(this).addClass("selected");
        var dir = $(this).hasClass("h") ? "h" : "v";
        var cell =  $("td.number[nPreg"+dir+"='"+$(this).find(".pregunta.reveal").attr("nPreg")+"']");
        cell.trigger("click");
    });
}
function corregirIndices(){
    var jsonPA = json[preguntaActual];
    var cruzados = [];
    $.each(jsonPA.preguntas, function(i, e){
        $.each(jsonPA.preguntas, function(index, element){
            if(//extrae las posiciones que se cruzan en un mismo punto
                index !== i &&
                cruzados.indexOf(element.posicion.replace(" ","")) === -1 &&
                e.posicion.replace(" ","") === element.posicion.replace(" ","")
            ){
                cruzados.push(element.posicion);
            }
        });
    });
    añadirContenido(jsonPA, cruzados);
}
var palabras;
function añadirContenido(jsonPA, cruzados){
    palabras = [];
    $.each(jsonPA.respuestas, function(i, e){ palabras.push(eliminarAcentos(eliminarP(e.t13respuesta))); });
    crearGrid(jsonPA.preguntas);
    //agrega las preguntas
    var pos, dir, nPreg;
    //añade palabras intersectadas (inician en el mismo punto)
    $.each(jsonPA.preguntas, function(i,e){
        pos = e.posicion.replace(" ","");
        dir=e.direccion;
        nPreg = $(".contenedor."+dir+" .pregunta").length+1;
        if(cruzados.indexOf(pos) !== -1){
            if($(".pregunta[pos='"+pos+"']").length>0){
                $(".contenedor."+dir).append("<div t17='"+i+"' class='pregunta' dir='"+dir+"' nPreg='"+$(".pregunta[pos='"+pos+"']").attr("nPreg")+"' pos='"+pos+"'>"+cambiarRutaImagen(e.t11pregunta)+"</div>");
            }else{
                $(".contenedor."+dir).append("<div t17='"+i+"' class='pregunta' dir='"+dir+"' nPreg='"+nPreg+"' pos='"+pos+"'>"+cambiarRutaImagen(e.t11pregunta)+"</div>");
            }
            acomodaPalabra(nPreg, pos, dir, palabras[i]);
        }
    });
    //añade el resto de palabras
    $.each(jsonPA.preguntas, function(i,e){
        pos = e.posicion.replace(" ","");
        dir=e.direccion;
        nPreg = $(".contenedor."+dir+" .pregunta").length+1;
        if(cruzados.indexOf(pos) === -1){
            $(".contenedor."+dir).append("<div t17='"+i+"' class='pregunta' dir='"+dir+"' nPreg='"+nPreg+"' pos='"+pos+"'>"+cambiarRutaImagen(e.t11pregunta)+"</div>");
            acomodaPalabra(nPreg, pos, dir, palabras[i]);
        }
    });
    eventosCrucigrama();
    $(".contenedor.v .pregunta:first, .contenedor.h .pregunta:first").addClass("reveal");
}
function crearGrid(preguntas){
    //define las dimenciones maximas de la cuadricula
    var maxLength=0, maxHeight=0;
    var length, height;
    $.each(preguntas, function(i,e){
        if(e.direccion.toLocaleLowerCase() === "v"){
            height = (e.posicion.split(",")[1]*1+palabras[i].length);
            maxHeight = maxHeight > height ? maxHeight : height ;
        }else{
            length = (e.posicion.split(",")[0]*1+palabras[i].length);
            maxLength = maxLength > length ? maxLength : length;
        }
    });
    //añade la estructura html
    $(".panel.cuadricula").html("<table class='grid'></table>");
    for(var i=0; i < maxHeight; i++){ $(".grid").append("<tr></tr>"); }
    for(var i=0; i < maxLength; i++){ $(".grid tr").append("<td></td>"); }
    //fija dimensiones
    var width = $(".grid td:first").width();
    var height = $(".grid td:first").height();
    $(".grid td").css({
        width:width, maxWidth:width, minWidth:width,
        height:height, maxHeight:height, minHeight:height
    });
}
var input = "<input  autocomplete='false' autocorrect='off' autocapitalize='off' spellcheck='false' contenteditable='true' maxlength='1' type='text'>";
function acomodaPalabra(nPreg, pos,dir, palabra){
    pos = pos.split(",");
    var selector;
    $.each(palabra.split(""), function(i,e){
        selector = $("tr:nth("+pos[1]+") td:nth("+pos[0]+")");
        i === 0 ? selector.addClass("number") : "";
        selector.attr("nPreg")!==undefined ? selector.attr("nPreg"+dir,nPreg) : "";
        selector.find("input").length === 0 ? selector.append(input) : "";
        selector.addClass("cell").attr({
            dir: selector.attr("dir")===undefined ? dir : dir+selector.attr("dir"),
            nPreg:nPreg,
            maxlength:1
        }).attr("nPreg"+dir,nPreg);
        dir==="v" ? pos[1]++ : pos[0]++;
    });
}
//////////////////////////////////////////////////////////////////////////////
function eventosCrucigrama(){
    $("td.cell").on("click", setDirection).find("input")
    .keydown(function(){
        var cell = $(this).parent();
        var isGreen = cell.hasClass("green");
        isGreen ? navegaSiguiente($(this).parent()) : $(this).val("");
        if(event.keyCode === 13 || !write){ return false; }
        var kCode = event.keyCode;
        increment = true;
        if(kCode === 8 || kCode === 37 || kCode === 38){//teclas backspace arriba e izquierda
            increment = false;
            navegaSiguiente(cell);
            return false;
        }else if(kCode === 39 || kCode === 40){//teclas derecha y abajo
            navegaSiguiente(cell);
            return false;
        }else if(isGreen){ return false; }else{ $(this).val(""); }
    }).on("input", function(){
        var cell = $(this).parent();
        if(!write){ return false; }
        var str = eliminarAcentos($(this).val()).toUpperCase();
        if(caracteres.indexOf(str) !== -1){
            navegaSiguiente(cell);
            evaluaPalabra(cell, direccion);;
        }else{ $(this).val(""); }
    })
    .keypress(keyValidation).keyup(keyValidation).on("paste", function(){ $(this).val(""); return false; });
    function keyValidation(){
        if($(this).hasClass("green")){ return false; }
        return write;
    }
    var direccion;
    function setDirection(event){
        $(".focus").removeClass("focus");
        $(this).addClass("focus").find("input").focus();
        var isTrigger = event.type === "click2";
        if(!customDir){
            direccion = $(this).attr("dir");
            if(direccion === "vh" || direccion === "hv"){
                direccion = parseInt(Math.random() * 2);
                direccion = direccion === 1 ? "v" : "h";
            }
            if(!isTrigger){
                $(".contenedor.h, .contenedor.v").removeClass("selected");
                $(".contenedor."+direccion).addClass("selected");
            }
            $(".contenedor."+direccion).find(".pregunta").removeClass("reveal");
            $(".contenedor."+direccion+" .pregunta[nPreg='"+$(this).attr("nPreg"+direccion)+"']").addClass("reveal");
        }else{
            customDir = false;
            direccion = $(".contenedor.selected").hasClass("h") ? "h" : "v";
        }
        var cells, green = true;
        cells = $(".cell[npreg"+direccion+"='"+$(this).attr("npreg")+"']");
        cells.each(function(){ if(!$(this).hasClass("green")){ green = false; return false; } });
        cells.length === 0 ? green=false : "";
        green ? $(".contenedor.selected").addClass("green") : $(".contenedor.selected").removeClass("green"); 
    }
    function navegaSiguiente(element){
        var x = element.index(),
            y = element.parent().index();
        increment ? (direccion === "v" ? y++ : x++) : (direccion === "v" ? y-- : x--);
        var selector = $("tr:nth("+y+") td:nth("+x+")");
        if(selector.length !== 0){
            element.blur();
            $(".focus").removeClass("focus");
            selector.addClass("focus").find("input").focus();
        }
    }
    function evaluaPalabra(element, dir){
        var nPreg = $(element).attr("npreg"+dir);
        var t17 = $(".pregunta[dir='"+dir+"'][npreg='"+nPreg+"']").attr("t17");
        var palabra = "";
        $(".cell[npreg"+dir+"='"+nPreg+"'] input").each(function(){ palabra+=this.value; });
        palabra = eliminarAcentos(palabra).toUpperCase();;
        if(palabra.length === palabras[t17].length){
            if(palabra === palabras[t17].toUpperCase()){
                sndCorrecto();
                $(".contenedor.selected").addClass("green");
                $(".cell[npreg"+dir+"='"+nPreg+"']").addClass("green");
                intentosRestantes > 0 ? aciertos++ : "";
                correctas++;
                element.find("input").blur();
            }else{
                write = false;
                sndIncorrecto();
                var cells = $(".cell[npreg"+dir+"='"+nPreg+"']").addClass("red");
                setTimeout(function(){
                    cells.removeClass("red").not(".green").addClass("white").filter(".number").on(transitionEnd, function(){
                        $(this).off(transitionEnd);
                        cells.removeClass("white");
                        cells.not(".green").find("input").val("");
                        $(this).trigger("click");
                        write = true;
                        return false;
                    });
                }, 500);
            }
            intentosRestantes --;
            correctas === palabras.length ? sigPregunta() : "";
        }
    }
}
////ciorrige la estructura del json
function parseJson(){
    var jsonPA = {
        respuestas:[],
        preguntas:[]
    };
    jpa = json[preguntaActual];
    $.each(jpa.respuestas, function(i,e){
        jsonPA.respuestas.push({});
        jsonPA.preguntas.push({});
        jsonPA.respuestas[i].t13respuesta = e.t13respuesta;
        jsonPA.preguntas[i].t11pregunta = jpa.preguntas[i].t11pregunta;
        jsonPA.preguntas[i].direccion = jpa.pocisiones[i].direccion*1 === 0 ? "h" : "v";
        jsonPA.preguntas[i].posicion = (jpa.pocisiones[i].datoX-1) + "," + (jpa.pocisiones[i].datoY-1);
    });
    json[preguntaActual] = jsonPA;
}