function iniciarActividad() {
    var cadena="";
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $.each(json[preguntaActual].contenedores, function (index, element) {
        $("#contenedores").append('<table class="columna"><tr class="encabezado"><td>'+element+'</td></tr></table>');
    });
    $.each(json[preguntaActual].respuestas, function (index, element) {
        cadena+=index+",";
        $("#respuestas").append('<div index="'+index+'" t17="'+element.t17correcta+(evaluacion? "="+index : "")+'" class="respuestaDrag"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
        $(".columna").eq(parseInt(element.t17correcta)).append('<tr><td  class="contenedor" t11='+parseInt(element.t17correcta)+'></td></tr>');
    });
    coloresRandom(".respuestaDrag");
    cartasRandom(cadena);
    if($("#contenidoActividad img").length>0){
        $("img").on("load error", function(){
            fijarDimensiones();
        });
    }else{
        fijarDimensiones();
    }
    function fijarDimensiones(){
        $(".columna").css("height", "calc("+(100 / $(".columna").length)+"% - 6px)");
        var headerHg = 0;
        $(".encabezado").each(function(i, e){ headerHg = headerHg < e.scrollHeight ? e.scrollHeight : headerHg; }).find("td").css("height",parseInt(headerHg)+"px");
        var maxHeight = 0;
        $(".respuestaDrag").each(function(i,e){ maxHeight = maxHeight < e.scrollHeight ? e.scrollHeight : maxHeight;});
        $(".columna").css("height", "auto")
        $(".respuestaDrag, .contenedor").css("height",parseInt(maxHeight)+"px");
    }
}