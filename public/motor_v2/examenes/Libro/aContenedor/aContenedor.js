var rutaBase = "../../";
var materia="", grado="", ruta = "";
var json, idActividad;
$(document).ready(function(){
    buscarArchivos();
    $(".btn.materia").click(function(){ $(".btn.materia").removeClass("selected"); $(this).addClass("selected"); buscarArchivos(); });
    $(".btn.grado").click(function(){ $(".btn.grado").removeClass("selected"); $(this).addClass("selected"); buscarArchivos(); });
    $("#save").click(function(){ window.frames[0].saveJson(); });
    $("input[type='range']").on("input", function(){
        $(this).attr("value", $(this).val());
        return false;
    }).change(aplyFilters);
    $("#axis").change(aplyFilters);
    $("#Resizable").click(function(){
        $(this).toggleClass("resize");
        aplyFilters();
    });
    $(document).on('keydown', function(e){
    if(e.ctrlKey && e.which === 83){ // Check for the Ctrl key being pressed, and if the key = [S] (83)
        e.preventDefault();
        $("#save").trigger("click");
        return false;
    }
});
});
function buscarArchivos(){
    materia = $(".btn.materia.selected").attr("type");
    grado = $(".btn.grado.selected").attr("type");
    ruta = rutaBase + "/" + materia + "/" + grado;
    $("section .file-item").remove();
    $("body").css("cursor", "wait");
    setTimeout(function(){
        $.ajax({
            url:ruta,
            async:false,
            success:function(data){
                $(data).find("a[href*='.js']").each(function(ind, ele){
                    if($(ele).attr("href").length >= 12){
                        $("section").append("<div class='file-item' src='"+$(ele).attr("href")+"'>"+$(ele).attr("href").replace(".js","")+"</div>");
                    }
                });
                $("body").removeAttr("style");
                fileItemEvents();
                $(".file-item:first").trigger("click");
            },
            error: function(){ $("body").removeAttr("style"); }
        });
    }, 50);
}
//function filtrar(ruta){
//    var tipo;
//    $.ajaxSetup({async:false});
//    $.getScript(ruta, function(data){ tipo = json[0].pregunta.c03id_tipo_pregunta; });
//    return (tipo == "5" || tipo == "2") && obtenerTipoActividad();
//}
function fileItemEvents(){
    $(".file-item").click(function(){
        $(".disabled").removeClass("disabled");
        $(".file-item").removeClass("selected");
        var element = $(this).addClass("selected");
        idActividad = element.text();
        $("iframe").attr("src", "motor/index_rev.html?id="+idActividad);
    });
}
var grid=[1,1], axis="x,y", resizable=true;
function aplyFilters(){
    grid = $("#grid").val();
    axis = $("#axis").val();
    resizable = $("#Resizable.resize").length>0;
    window.frames[0].updateUI(grid, axis, resizable);
}
function obtenerTipoActividad(){
    var numPregunta = 0;
    var numActividad = json[numPregunta].pregunta.c03id_tipo_pregunta;
    var tipoActividad=false;
    if(numActividad+""==="5"){//se obtine el subtipo Arratra contenedor
        tipoActividad = json[numPregunta].pregunta.tipo === "vertical" ? "5.1" : tipoActividad;//Arrastra vertical
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "horizontal" ? "5.2" : tipoActividad;//Arrastra Horizontal
        tipoActividad = tipoActividad===false && json[numPregunta].contenedores !== undefined && json[numPregunta].contenedoresFilas !== undefined && json[numPregunta].pregunta.tipo === undefined ? "5.3" : tipoActividad;//Arrastra matriz
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "matrizHorizontal" ? "5.4" : tipoActividad;//Arrastra matriz horizontal
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.respuestaUnicaMultiple === true ? "5.9" : tipoActividad;//Arrastra cerdito
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].contenedores.length === 1 ? "5.8" : tipoActividad;//Arrastra acontenedor unico
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].pregunta.imagen === true && json[numPregunta].pregunta.respuestaImagen === true ? "5.7" : tipoActividad;//Arrastra imagenes /Arrastra grupos
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" && json[numPregunta].pregunta.imagen === true ? "5.6" : tipoActividad;//Arrastra a imagen
        tipoActividad = tipoActividad===false && json[numPregunta].pregunta.tipo === "ordenar" ? "5.5" : tipoActividad;//Arrastra ordenar
    }else if(numActividad+""==="1"){
        tipoActividad =  json[numPregunta].pregunta.test === true ? "1.2" : tipoActividad;//opcion multiple TEST
        tipoActividad =  tipoActividad===false ? "1.1" : tipoActividad;//opcion multiple basico
    }else if(numActividad+""==="2"){
        tipoActividad = tipoActividad===false && json[numPregunta].respuestas[0].coordenadas !== undefined ? "2.2":tipoActividad;//respuesta multiple imagen
        tipoActividad = tipoActividad===false && tipoActividad === false ? "2.1":tipoActividad;//opcion multiple basico
    }else if(!isNaN(numActividad*1)){
        tipoActividad = numActividad+"";
    }
    var recursoValido = ["2.2","5.8","5.7","5.6"].indexOf(tipoActividad) !== -1;
    return recursoValido;
}