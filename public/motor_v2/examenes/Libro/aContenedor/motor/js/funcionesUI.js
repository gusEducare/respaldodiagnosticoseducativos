var selector;
function setUi(){
    ////////////////////////////////////////////////////////////////////////////////
    $(".contenedor.ui-droppable").droppable( "disable" );
    selector = ".contenedor";

    $(".parent:has(>.contenedor)").length>0 ? selector=".parent:has(>.contenedor)" : "";
    $(".contenedores:has(>.contenedor)").length>0 ? selector=".contenedores:has(>.contenedor)" : "";
    if(lastResort === "RespuestaMultipleFondo"){
        selector=".respuesta";
        $(selector).resizable({
            aspectRatio: true,
            resize:function(){
                $(selector).not($(this)).width($(this).width()).height($(this).height());
            }
        });
    }
    $(".respuesta").off("click");
    $(selector).on("click mouseenter", function(){
        $(".repHover").removeClass("repHover");
        var contenedor = $(this).addClass("repHover");
        $(".respuestaDrag").each(function(){
            var t17 = $(this).attr("t17").split(",");
            t17.indexOf(contenedor.attr("t11")) !== -1 ? $(this).addClass("repHover") : "";
        });
    });
    $(selector).css({position:"absolute", background:"rgba(0,0,0, 0.45)"}).resizable({
        stop:function(){
            updateJson();
        }
    }).draggable({
        stop:function(){
            updateJson();
        }
    });
    window.top.aplyFilters();
}
function updateUI(grid, axis, resizable){
    $(selector).draggable({
        grid:[grid, grid],
        axis:axis
    });
    $(selector).resizable( resizable ? "enable" : "disable" );
    $(selector).resizable({ grid:[grid, grid] });
}
///////////////////////////////////////////////////////////////////////////////
function updateJson(){
    var contenedor, position;
    if(lastResort === "RespuestaMultipleFondo"){
        $.each(json[preguntaActual].respuestas, function(i,e){
            contenedor = $(selector).eq(i);
            position = contenedor.position();
            e.coordenadas = position.top+","+position.left;
        });
        if($(selector+":first").width() !== 40){
            json[preguntaActual].pregunta.radio = $(selector+":first").width();
        }
    }else{
        $.each(json[preguntaActual].contenedores, function(i, e){
            contenedor = $(selector).eq(i);
            position = contenedor.position();
            e.Contenedor[1] = Math.round(position.top)+","+Math.round(position.left);
            e.Contenedor[3] = Math.round(contenedor.width())+","+Math.round(contenedor.height());
        });
    }
}
function saveJson(){
    var file = parent.ruta.replace("../..//","")+"/"+parent.idActividad+".js";
    var data = {ruta:file, txt: "json="+JSON.stringify(json,null,2)+";"};
    var guardar = json[preguntaActual].pregunta.c03id_tipo_pregunta;
    guardar = (guardar+"" === "5" || guardar+"" === "2") && $(selector).length!==0;
    if(guardar){
        $.ajax({
            type:"POST",
            url: "../php/saveFile.php",
            async:false,
            data:data,
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $(parent.document.body).find("#save").toggleClass("green");
                setTimeout(function(){
                    $(parent.document.body).find("#save").toggleClass("green");
                }, 35);
            }
        });
    }
}