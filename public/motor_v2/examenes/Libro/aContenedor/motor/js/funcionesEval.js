function initEval(){
    $("body").append("<nav id='preguntas'><hr id='lastNav' class='hidden'><div id='contenedorNav'></div><hr id='nextNav'></nav>");
    barraEvaluaciones();
    $("#contenidoActividad").addClass("actividadEvaluacion");
    $("#previewActividad, #progressBar").remove();
    if(strAmbiente === "PROD"){
        parent.$('body').css({overflow:"hidden"});
        parent.$('body').find(".contenedor_body iframe").css({//cambia la altura al iframe de la actividad
            height:"calc(100vh - "+(parent.$('body').find(".contenedor_body")[0].scrollHeight-27)+"px)"
        });
        $("html, body").css({width:"100%"});
//        $(".inst_exa").css({margin:0});
//        $("footer#slideShow").append("<div id='time'></div>");
//        window.time = parent.window.tiempo;
//        if(time !== "0" && time !==null){
//            $('#time').chrony({
//                hour: parseInt(time[0]),
//                minute: parseInt(time[1]),
//                second: parseInt(time[2]),
//                finish: function () {
//                    finalizarExamen();
//                }
//            });
//        }
    }
}
function barraEvaluaciones(){
    $.each(json, function(i){
        $("nav#preguntas>#contenedorNav").append("<div class='nPregunta'>"+(i+1)+"</div>");
    });
    if($("#contenedorNav")[0].scrollWidth > 565){
        npTime = setTimeout(function(){},1);
        $(".nPregunta").on(transitionEnd, function(){
            clearTimeout(npTime);
            npTime = setTimeout(function(){
                if($("#contenedorNav")[0].scrollWidth <= 460){
                    $("#nextNav").addClass("hidden");
                }else if($(".nPregunta.noWidth").length === 0){
                    $("#lastNav").addClass("hidden");
                }
                $("#preguntas hr").removeClass("lock");
            }, 100);
        });
    }else{
        $("nav#preguntas hr").remove();
    }
    $("nav#preguntas .nPregunta:first").addClass("selected");
    $("nav#preguntas .nPregunta").on("click", function(){
        var element = $(this);
        if(element.hasClass("selected")){
            element.hasClass("flag") ? element.removeClass("flag") : element.addClass("flag");
        }else{
            $("nav#preguntas .nPregunta").removeClass("selected");
            element.addClass("selected");
            preguntaActual = element.index()-1;
            obtenerRespuestaUsuario();
            siguientePregunta();
        }
    });
    $("#preguntas hr").on("click", function(){
        var contador; $("#preguntas hr").addClass("lock");
        var direccion = $(this).attr("id") === "lastNav";
        if(direccion){//anteriores
            contador = 10;
            while(contador>0){
                setTimeout(function(){
                    $(".nPregunta.noWidth:last").removeClass("noWidth");
                }, (10-contador)*50);
                contador--;
            }
            $("#nextNav").removeClass("hidden");
        }else{//siguientes
            contador = 0;
            while(contador<10){
                setTimeout(function(){
                    $(".nPregunta:not(.noWidth):first").addClass("noWidth");
                }, contador*50);
                contador++;
            }
            $("#lastNav").removeClass("hidden");
        }
    });
}
var timeToRestore;
function restore(){
    console.log("Holi restore");
    var time = lastResort === "RelacionaLineas" ? 1000 : 700;
    $("#contenidoActividad").bind("DOMSubtreeModified", 
    function(){//se restauran las respuestas del usuario, una vez cargada la actividad
        $(".respuestaDrag").css({opacity:0, transition:"none"});
        $("#actividadOrdena").css({opacity:0});
        clearTimeout(timeToRestore);
        timeToRestore = setTimeout(function(){
            restaurarActividad();
        }, time*1);
    });
}
function obtenerRespuestaUsuario(){
    var respuestaUsuario = "";
    var arrRespuestas = [];
    switch (lastResort) {
        case "ArrastraVertical":
        case "ArrastraHorizontal":
        case "ArrastraMatriz":
        case "ArrastraMatrizHorizontal":
        case "ArrastraOrdenar":
        case "ArrastraImagenes":
        case "ArrastraContenedorUnico":
        case "ArrastraCorta":
        case "ArrastraImagen":
        case "OrdenaElementos":
            var iContenedor, iRespuesta, selector;
            lastResort === "ArrastraImagen" ? selector="etiqueta" : selector="respuestaDrag";
            $(".contenedor:not(.mostrar)").each(function(index, element){
                iContenedor = element.getAttribute('t11');
                iRespuesta = element.getElementsByClassName(selector);
                    iRespuesta = iRespuesta.length > 0 ? iRespuesta[0].getAttribute('t17') : "x";
                arrRespuestas.push(iContenedor+"-"+iRespuesta);
            });
            respuestaUsuario = arrRespuestas.join("_");
            break;
        case "Matriz":
            $(".check").each(function (index, element) {
                element.className.indexOf("checked") !== -1 ? arrRespuestas.push("1") : arrRespuestas.push("0");
            });
            respuestaUsuario = arrRespuestas.join("");
            break;
        case "OpcionMultiple":
        case "RespuestaMultipleFondo":
            var clase;
            clase = lastResort==="OpcionMultiple" ? "OMSelectedEval" : "selected";
            $(".respuesta").each(function(index, element){
                element.className.indexOf(clase) !== -1 ? arrRespuestas.push("1") : arrRespuestas.push("0");
            });
            respuestaUsuario = arrRespuestas.join("");
            break;
        case "SopaLetrasV2":
            var cadenaSopa = $("#espacioSopa td").text();
            var dimensionesSopa = $("tr").length + "-" + $("tr:first td").length;
            $(".palabraResuelta").each(function(index, element){
                arrRespuestas.push(element.getAttribute('coords'));
            });
            var palabrasMarcadas="";
            $("#espacioPalabras .palabra").each(function(){
                palabrasMarcadas += $(this).hasClass("mark") ? "1" : "0";
            });
                                // tamaño "x-x"        //letras        //respuestas de usuario
            respuestaUsuario = dimensionesSopa+"[CS]"+cadenaSopa+"[PM]"+palabrasMarcadas+"[RU]"+arrRespuestas.join("=");
            break;
        case "RelacionaLineas":
            $("#contenedorIzquierda .columnas").each(function(){
                arrRespuestas.push($(this).attr("relacion"));
            });
            respuestaUsuario = arrRespuestas.join(",");
            break
        case "ListasDesplegables":
            $(".dropList").each(function(index, element){
                if(!$(element).find("p:first").hasClass("hidden")){
                    arrRespuestas.push($(element).find("p:first").text());
                }else{
                    arrRespuestas.push("[empty]");
                }
            });
            respuestaUsuario = arrRespuestas.join(",");
            break
        case "ArrastraEtiquetas":
            var selector = json[preguntaActual].pregunta.columnas===true ? ".columna" : ".contenedor";
                $(selector).each(function(index, element){
                    iContenedor = index;
                    iRespuesta = element.getElementsByClassName('etiqueta');
                        iRespuesta = iRespuesta.length > 0 ? iRespuesta[0].getAttribute('t17') : "x";
                    arrRespuestas.push(iContenedor+"-"+iRespuesta);
                });
            respuestaUsuario = arrRespuestas.join("_");
            break
        case "Crucigrama":
            var palabra = "";
            $.each(horizontales, function(index){
                $("td[pregh='"+(index+1)+"'] input").each(function(i, element){
                    palabra += element.value === "" ? "_" : element.value;
                });
                arrRespuestas.push("[pregh='"+(index+1)+"']-"+palabra);
                palabra = "";
            });
            $.each(verticales, function(index){
                $("td[pregv='"+(index+1)+"'] input").each(function(i, element){
                    palabra += element.value === "" ? "_" : element.value;
                });
                arrRespuestas.push("[pregv='"+(index+1)+"']-"+palabra);
                palabra = "";
            });
            respuestaUsuario = arrRespuestas.join(",");
            break
        case "Graficas":
            guardarAvance();//definido en funciones generales   //aplicado tambien cuando no es evaluable el tipo de actividad
            break
        default:
            respuestaUsuario = null;
    }
   guardarCadenaUsuario(respuestaUsuario);
}
function guardarCadenaUsuario(respuestaUsuario){
    var conectarBD = true;
    if(respuestaUsuario!=="" && respuestaUsuario !== objCadenas[preguntaActual]){
        objCadenas[preguntaActual] = respuestaUsuario;
    }else{
        conectarBD = false;
    }
    if(conectarBD && strAmbiente==="PROD"){//se actualizan los registros en la base de datos
        actualizaRespuesta(respuestaUsuario, json[preguntaActual].pregunta.t11id_pregunta);
    }
}
function restaurarActividad(){
    $("#contenidoActividad").unbind("DOMSubtreeModified");
    if(objCadenas[preguntaActual] !== null && objCadenas[preguntaActual] !== undefined){
        switch (lastResort) {
            case "ArrastraVertical":
            case "ArrastraHorizontal":
            case "ArrastraMatriz":
            case "ArrastraMatrizHorizontal":
            case "ArrastraOrdenar":
            case "ArrastraImagenes":
            case "ArrastraContenedorUnico":
            case "ArrastraCorta":
            case "ArrastraImagen":
            case "OrdenaElementos":
                var t11, t17, selector, contenedor, respuesta;
                lastResort === "ArrastraImagen" ? selector="etiqueta" : selector="respuestaDrag";
                $.each(objCadenas[preguntaActual].split("_"), function(index, element){
                    if(element.indexOf("x")===-1){
                        t11 = element.split("-")[0];
                        t17 = element.split("-")[1];
                        contenedor = $(".contenedor[t11='"+t11+"']:not(.ocupado):last");
                        respuesta = $("."+selector+"[t17='"+t17+"']:not(.correctAnswer):first");
                        lastResort === "ArrastraImagen" ? respuesta = respuesta.prop("outerHTML") : "";
                        $(respuesta).addClass("correctAnswer").appendTo(contenedor);
                        contenedor.addClass("ocupado");
                    }
                });
                //desbloquea contenedores y respuestas necesarios u ocupadas
                $(".contenedor:has(.respuestaDrag)").removeClass("noHeight");
                $(".contenedor.noHeight:first").removeClass("noHeight");
                $(".resph").removeClass("resph");
                $("#actividadOrdena").css({opacity:1});
                //aplica estilos adicionales
                $(".contenedor .respuestaDrag, .contenedor .etiqueta").css({top:0, left:0, margin:0});
                fijarCartas($(".respuestaDrag:not(.contenedor .respuestaDrag)"));
                break
            case "OpcionMultiple":
            case "RespuestaMultipleFondo":
                var clase;
                clase = lastResort==="OpcionMultiple" ? "OMSelectedEval" : "selected";
                $.each(objCadenas[preguntaActual].split(""), function(index, element){
                    element === "1" ? $(".respuesta:nth("+index+")").addClass(clase) : "";
                });
                break;
            case "Matriz":
                $.each(objCadenas[preguntaActual].split(""), function(index, element){
                    element === "1" ? $($(".check")[index]).css("background-image","url('img/opcion_multiple_palomita.png')").addClass("checked") : "";
                });
                break
            case "RelacionaLineas"://0-1,1-0,2-2,3-3,4-5,5-4
                $("svg").hide();
                obtenerPosicionesLineas();
                function obtenerPosicionesLineas(){
                    lineasCorrectas=[];
                    $("#contenidoActividad").scrollTop(0);
                    var x1, x2, y1, y2, circuloActual;
                    var l, r, lineas="";
                    $.each(objCadenas[preguntaActual].split(","), function(i, e){
                        if(e!==""){
                            l = e.split("-")[0];
                            r = e.split("-")[1];
                            circuloActual = $("#contenedorIzquierda .columnas:has(div[t11='"+l+"'])").addClass("correctAnswer").attr("relacion", e).find(".circuloLineas");
                            x1 = parseInt($(circuloActual).offset().left + 22);
                            y1 = parseInt($(circuloActual).offset().top + 22);
                            circuloActual = $("#contenedorDerecha .columnas:has(div[t17='"+r+"'])").addClass("correctAnswer").attr("relacion", e).find(".circuloLineas");
                            x2 = parseInt($(circuloActual).offset().left + 22);
                            y2 = parseInt($(circuloActual).offset().top + 22);
                            lineasCorrectas.push({x1: x1, y1: y1, x2: x2, y2: y2});
                            lineas += "<line x1='"+x1+"' x2='"+x2+"' y1='"+y1+"' y2='"+y2+"' class='correctLine' relacion='"+e+"'></line>";
                        }
                    });
                    lineas+="<line></line>";
                    $("#svgLine").html(lineas);
                    $("line").length===1 ? $("svg").show() : "";
                }
                var circulosTimeout;
                $(".circuloLineas").bind("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend", function(){
                    clearTimeout(circulosTimeout);
                    circulosTimeout = setTimeout(function(){
                        $(".circuloLineas").unbind("webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend");
                        obtenerPosicionesLineas();
                        $("svg").show();
                    }, 100);
                });
                break
            case "SopaLetrasV2":
                var palabrasMarcadas = objCadenas[preguntaActual].split("[PM]")[1].split("[RU]")[0];
                $.each(palabrasMarcadas.split(""), function(index, element){
                    element === "1" ? $("#espacioPalabras .palabra").eq(index).addClass("mark") : "";
                });
                break
            case "ListasDesplegables":
                var dropList, listElement, index;
                $.each(objCadenas[preguntaActual].split(","), function(index, element){
                    if(element!=="[empty]"){
                        dropList = $(".dropList").eq(index);
                        listElement = dropList.find("p:contains('"+element+"')");
                        index = listElement.index();
                        if(index>1){
                            listElement.insertBefore(dropList.find("p:first"));
                        }
                        listElement.addClass("selected").removeClass("hidden");
                        dropList.addClass("selected");
                    }
                });
                break
            case "Crucigrama":
                var selector, word;
                $.each(objCadenas[preguntaActual].split(","), function(index, element){
                    selector = element.split("-")[0];
                    word = element.split("-")[1];
                    $.each(word.split(""), function(i, e){
                        if(e!=="_"){
                            $(".cell"+selector).eq(i).find("input").val(e);
                        }
                    });
                });
                break
                case "ArrastraEtiquetas":
                    var selector = json[preguntaActual].pregunta.columnas===true ? ".columna" : ".contenedor";
                    var iContenedor, iRespuesta, contenedor, respuesta;
                        $.each(objCadenas[preguntaActual].split("_"), function(index, element){
                            iContenedor = element.split("-")[0];
                            iRespuesta = element.split("-")[1];
                            contenedor = $(".contenedor").eq(iContenedor);
                            respuesta = $(".etiqueta[t17='"+iRespuesta+"']").prop("outerHTML");
                            $(respuesta).appendTo(contenedor);
                            $(".contenedor .etiqueta").css({top:0, left:0, margin:0});
                        });
                    break
                case "Graficas":
                    restaurarAvace();
                    break
        }
    }
    $(".button, #actSelect").removeClass("lock");
    return false;
}
function actualizaRespuesta(strRespUsuario, idPregunta) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: "/preguntas/preguntatodo/"+idPregunta,
        data: {
            'objRespuestas': strRespUsuario,
            'idPregunta': idPregunta
        },
        beforeSend: function () {

        },
        success: function (json) {
            console.log("success!");
        },
        error: function (json) {
            console.log("Hubo un error al procesar los datos: " + json);
        }
    });
    if(localStorage.getItem("respuestasUsuario")=== null){
        localStorage.setItem('respuestasUsuario',  strRespUsuario);
    }else{
        var respuestasLocal = localStorage.getItem("respuestasUsuario");
        localStorage.setItem('respuestasUsuario',  respuestasLocal+","+strRespUsuario);
    }
}
function finalizarExamen(){
    $.ajax({
        type:'POST',
        dataType:'json',            
        url:  '/examenes/finalizaexamenajax',
        data:{ 
            },
        beforeSend: function() {
        },
        success:function(json){
                if( json ){
                    window.parent.calificacion = json.calificacion;
                    parent.$('body').trigger('finalizarExamen');
                }else{
                }
        },
        error: function(json){
             console.log("Hubo un error al procesar los datos: " + json);
        }
    });
}
function evaluaActividad(){
    if(window.parent.demo == true){
        correctas = window.parent.correctas;
        var calificacion = 0;
        var ponderacion = 100/correctas.length;
        $.each(objCadenas, function(index, element){
            if(element === correctas[index]){
                calificacion += ponderacion;
            }else if(correctas[index].search("demo")!==-1 && element !== null){
                calificacion += ponderacion;
            }
        });
        window.parent.calificacion = calificacion;
        parent.$('body').trigger('finalizarExamen');
    }else{
        finalizarExamen();
    }
}
