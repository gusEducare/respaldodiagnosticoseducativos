function iniciarActividad(){
    var cadena = "";
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $("#contenedores").append('<table class="columna"><tr class="encabezado"><td class="blank"></td></tr></table>');
    $.each(json[preguntaActual].contenedores, function (index, element) {//añade columnas
        $(".encabezado").append('<td class="textos">'+cambiarRutaImagen(element)+'</td>');
    });
    $.each(json[preguntaActual].contenedoresFilas,function(index, element){//añade filas
        $(".columna").append('<tr><td class="textoFilas textos">'+cambiarRutaImagen(element)+'</td></tr>');
        while($("tr:last .contenedor").length < $("tr:first > .textos").length){//se añaden contenedores
            $("tr:last").append('<td class="contenedor" t11="'+$("tr:last .contenedor").length+","+($("tr:last").index()-1)+'"></td>');
        }
    });        
    var t17="";
    $.each(json[preguntaActual].respuestas, function (index, element) {//se añaden respuestas
        cadena += index+",";
        t17 = element.t17correcta;
        if(Object.prototype.toString.call(element.t17correcta)==="[object Array]"){
            t17 = t17.join("-");
        }
        $("#respuestas").append('<div t17="'+t17+'" class="respuestaDrag resph"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
    });
    var vuelta = 0;
    cartasRandom(cadena);
    coloresRandom($(".respuestaDrag"));
    if($("#contenidoActividad img").length>0){
        $("img").on("load error", function(){
            fijarDimensiones();
        });
    }else{
        fijarDimensiones();
    }
    function fijarDimensiones(){
        $("p:has(img)").css({margin:0, padding:0, width:"100%", height:"100%"})
        var width = ($(".columna").width() - $("td:first").width()) / $("tr:last .contenedor").length;
        $(".contenedor").css({minWidth:width, maxWidth:width, width:width});
        $(".respuestaDrag").width($(".contenedor:first").width());
        var indexRespuesta = "", maxHeight = 0;
        t17 = "";
        $(".respuestaDrag").each(function(index, element){
            indexRespuesta = "";
            maxHeight = maxHeight < element.scrollHeight ? element.scrollHeight : maxHeight;
            if(vuelta === 0){
                t17 = $(element).attr("t17").split("-");
                $.each(t17, function(index, element){//se obtiene t17
                    indexRespuesta += $(".contenedor").index($(".contenedor[t11='"+element+"']"))+",";
                });
                $(element).attr("t17", indexRespuesta.slice(0, -1));//se determina t17
            }
        });
        $(".contenedor").each(function(index, element){
            maxHeight = maxHeight < element.scrollHeight ? element.scrollHeight : maxHeight;
            $(element).attr("t11", index);//se determina t11
        });
        $(".textoFilas").css({width:"auto"});
        $(".contenedor, .respuestaDrag, #respuestas").css("height",maxHeight+"px");
        vuelta = 1;
    }
}