var operadores = [
    {simbolo:"÷", operador:"division",       class:"inner"},
    {simbolo:"×", operador:"multiplicacion", class:""},
    {simbolo:"-", operador:"resta",          class:""},
    {simbolo:"+", operador:"suma",           class:""}
];
var operadoresAvanzados = [
//    {simbolo:"( )", operador:"parentesis"},
    {simbolo:"√",  operador:"raiz",         class:"inner"},
    {simbolo:"^",  operador:"potencia",     class:"inner"},
    {simbolo:"%",  operador:"porcentaje",   class:"inner"}
];
var avanzado;
var numeros = "1234567890.";
var textArea = "<span contenteditable='true'>0</span>";//caja de texto base
function iniciarActividad(){
    totalPreguntas++;
    textoFinaliza = true; evaluable = false;
    avanzado = json[preguntaActual].pregunta.operadoresAvanzados === true;
    $("#contenidoActividad").html("<div id='contenido'></div>");
    $("#contenido").append("<div class='resultado'>0</div>\n\
            <div class='respuesta'>"+json[preguntaActual].respuestas[0].t17correcta+"</div>\n\
            <div class='pizarra'></div>");
//    se añaden los operadores//botones
    var right = 10, bottom = 10;
    $.each(operadores, function(i, e){
        $("#contenido").append("<div tipo='"+e.operador+"' style='right:"+right+"px;' class='button operador bg "+e.operador+" "+e.class+"'>"+e.simbolo+"</div>");
        right+=50;
    });
    if(avanzado){//añade operadores avanzados
        bottom = 55; right = 10;
        $.each(operadoresAvanzados, function(i, e){
            $("#contenido").append("<div tipo='"+e.operador+"' style='right:"+right+"px; bottom:"+bottom+"px;' class='button operador bg "+e.operador+" "+e.class+"'>"+e.simbolo+"</div>");
            right+=50;
        });
    }
    //añade elemento para eliminar
    $("#contenido").append("<div style='right:"+right+"px; bottom:"+bottom+"px;' class='button operador bg eliminar'></div>");
    //se añade primer elemento de edicion
    newElement("suma", $("#contenido .pizarra"));
    ////////////////////////////////////eventos/////////////////////////////////
    $(".button.operador.eliminar").on("click touch", function(){//elimina elementos
        if($(".pizarra>.element").length > 1 || getLastChild().parent().hasClass("element")){
            var elementRemove = getLastChild();
            var parent = elementRemove.parent();
            if(
                "potencia division porcentaje".indexOf(parent.attr("tipo")) !== -1 ||
                parent.find(">.element").length < 2 && parent.hasClass("element")
                ){
                    elementRemove.replaceWith(TextAreaEvents($(textArea)));
                    setTimeout(function(){ parent.find(">span").focus(); }, 10);
            }else{
                elementRemove.remove();
            }
            obtenerResultado();
            $(".element:last").trigger("click");
        }else{
            $(".element").addClass("selected").find(">span").text("0");
            $(".resultado").text("0");
        }
        return false;
    });
    $(".pizarra").on("click touch", function(){//dehabilita elementos
        $(".element").removeClass("selected");
        $(".button.inner").addClass("lock");
        $("span").blur();
    });
    ///////eventos botones de operaciones
    $(".button.operador:not(.eliminar)").on("click touch", function(){
        var tipo = $(this).attr("tipo");//tipo de elemento
        var element = getLastChild();//elemeto al cual se agrega una nueva operacion
        var replace = element.length > 0;//determina si es necesario reemplazar un elemento
        !replace || (!avanzado && tipo !== "division") ? element = $(".pizarra") : "";
        if(!avanzado && !element.hasClass("division")){
            newElement(tipo, element);
        }else if(avanzado){
            newElement(tipo, element);
        }
    });
}
function getLastChild(){
    var element = $(".pizarra>.element.selected");
    while(element.find(">.element.selected").length>0){
        element = element.find(">.element.selected").eq(0);
    }
    return element;
}
function newElement(tipo, parent){
    var newElement;
    switch (tipo){//define la estructura del nuevo elemento
        case "suma":
            newElement = "<div tipo='"+tipo+"' class='element "+tipo+"'>"+textArea+"</div>";
            break;
        case "resta":
            newElement = "<div tipo='"+tipo+"' class='element "+tipo+"'>"+textArea+"</div>";
            break;
        case "multiplicacion":
            newElement = "<div tipo='"+tipo+"' class='element "+tipo+"'>"+textArea+"</div>";
            break;
        case "division":
            newElement = "<div tipo='"+tipo+"' class='element "+tipo+"'>"+textArea+textArea+"</div>";
            break;
        case "porcentaje":
            newElement = "<div tipo='"+tipo+"' tipo='"+tipo+"' class='element "+tipo+"'>"+textArea+"<p>de</p>"+textArea+"</div>";
            break;
        case "potencia":
            newElement = "<div tipo='"+tipo+"' tipo='"+tipo+"' class='element "+tipo+"'>"+textArea+textArea+"</div>";
            break;
        case "raiz":
            newElement = "<div tipo='"+tipo+"' class='element "+tipo+"'>"+textArea+"</div>";
            break;
    }
    //añade evento click al nuevo elemento
    newElement = $(newElement);
    newElement.on("click touch", function(){//al seleccionar un elemento
        $(".element.selected").removeClass("selected");
        if($(this).find(">.element").length === 0 || $(this).hasClass("division") ||$(this).hasClass("porcentaje")){
            $(".button.inner").removeClass("lock");
        }else{
            $(".button.inner").addClass("lock");
        }
        $(this).addClass("selected").parents(".element").addClass("selected");
        return false;
    });
    TextAreaEvents(newElement.find(">span"));
    //define si algun elemento sera eliminado
    var creaNuevo = true;
    var delElement = ">span";
    var reemplaza = false;
    if("potencia division porcentaje".indexOf(parent.attr("tipo")) !== -1){
        reemplaza = true;
        parent.attr("tipo") === "potencia" && parent.find(">span").length === 1 ? creaNuevo = false : "";
        "division porcentaje".indexOf(parent.attr("tipo")) !== -1 && parent.find(">span").length === 0 ? creaNuevo = false : "";
        delElement = parent.find(">spanActive");
        delElement.length === 0 ? delElement = ">span:first" : "";
    }
    if(creaNuevo){
        //añade nuevo elemento
        if(reemplaza){
            parent.find(delElement).replaceWith(newElement);
            newElement.trigger("click").find(">span:first").focus();
        }else{
            parent.find(delElement).remove();
            newElement.appendTo(parent);
            if($("#contenidoActividad").hasClass("lock")){
                $("#contenidoActividad").on(transitionEnd, function(){
                    $("#contenidoActividad").off(transitionEnd);
                    newElement.trigger("click").find(">span:first").focus();
                });
            }else{
                newElement.trigger("click").find(">span:first").focus();
            }
        }
    }
}
function TextAreaEvents(element){
    var keyUpTimeout = setTimeout(function(){}, 10);
    element.on("click touch", function(){//eventos para elementos <span>
        $(".spanActive").removeClass("spanActive");
        $(this).addClass("spanActive");
    }).on("paste", function(){//evita que se pueda pegar texto
        return false;
    }).on("DOMSubtreeModified", function(){
        clearTimeout(keyUpTimeout);
        var span = $(this);
        keyUpTimeout = setTimeout(function(){
            if($(span).text() === ""){
                $(span).text("0");
            }else{
                obtenerResultado();
            }
        }, 200);
    }).on("keyup", function(e){//valida que se escriban valores numericos y que no se escriba mas de un "." por caja de texto
        if(e.which !==8 && e.which !==46 && "1234567890.".indexOf(String.fromCharCode(e.which))===-1){
            return false;
        }
    });
    return element;
}
function obtenerResultado(){
    $(".resultado").text("");
    var resultado = calcularResultado($(".pizarra>.element"));
    setTimeout(function(){
        resultado = Math.round(eval(resultado) * 10000) / 10000;
        $(".resultado").text(resultado);
        if(resultado === Math.round( eval($(".respuesta").text()) * 10000) / 10000 && $(".element").length>1 && $("span").filter(function() { return $(this).text().replace(/0/g, "") === ""; }).length === 0){
            sndCorrecto();
            $(".respuesta, .resultado").addClass("lock");
            aciertos ++;
            sigPregunta();
        }
    }, 150);
}
function calcularResultado(elements){
    elements = elements.get().reverse();
    var resultado="", element;
    var e1, e2;
    $.each(elements, function(i, e){
        element = $(e);
        if(element.find(">.element").length > 0){
            switch (element.attr("tipo")){
                case "suma":
                    resultado = "+("+calcularResultado(element.find(">.element"))+")"+resultado;
                    break;
                case "resta":
                    resultado = "-("+calcularResultado(element.find(">.element"))+")"+resultado;
                    break;
                case "multiplicacion":
                    if(element.prev().length === 0){
                        resultado = "1*("+calcularResultado(element.find(">.element"))+")"+resultado;
                    }else{
                        resultado = "*("+calcularResultado(element.find(">.element"))+")"+resultado;
                    }
                    break;
                case "division":
                    e1 = element.find(">*:first").hasClass("element") ? calcularResultado(element.find(">.element:first")) : element.find(">span:first").text()*1;
                    e2 = element.find(">*:last").hasClass("element") ? calcularResultado(element.find(">.element:last")) : element.find(">span:last").text()*1;
                    resultado = "("+ e1 + "/" + e2 +")"+resultado;
                    break;
                case "porcentaje":
                    e1 = element.find(">*:first").hasClass("element") ? calcularResultado(element.find(">.element:first")) : element.find(">span:first").text()*1;
                    e2 = element.find(">*:last").hasClass("element") ? calcularResultado(element.find(">.element:last")) : element.find(">span:last").text()*1;
                    resultado = "(("+ e1 + "/100)*" + e2 +")"+resultado;
                    break;
                case "potencia":
                    e1 = element.find(">*:first").hasClass("element") ? calcularResultado(element.find(">.element:first")) : element.find(">span:first").text()*1;
                    e2 = element.find(">*:last").hasClass("element") ? calcularResultado(element.find(">.element:last")) : element.find(">span:last").text()*1;
                    resultado = "(Math.pow("+ e1 + ", " + e2 +"))"+resultado;
                    break;
                case "raiz":
                    resultado = resultado = "(Math.sqrt("+calcularResultado(element.find(">.element")) +"))"+resultado;
                    break;
            }
        }else{
            switch (element.attr("tipo")){
                case "suma":
                    resultado = "+("+element.find(">span").text()*1+")"+resultado;
                    break;
                case "resta":
                    resultado = "-("+element.find(">span").text()*1+")"+resultado;
                    break;
                case "multiplicacion":
                    if(element.prev().length === 0){
                        resultado = "1*("+element.find(">span").text()*1+")"+resultado;
                    }else{
                        resultado = "*("+element.find(">span").text()*1+")"+resultado;
                    }
                    break;
                case "division":
                    resultado = "("+element.find(">span:first").text()*1 + "/" + element.find(">span:last").text()*1 +")"+resultado;
                    break;
                case "porcentaje":
                    resultado = "(("+element.find(">span:first").text()*1 + "/100)*" + element.find(">span:last").text()*1 +")"+resultado;
                    break;
                case "potencia":
                    resultado = "(Math.pow("+element.find(">span:first").text()*1 + ", " + element.find(">span:last").text()*1 +"))"+resultado;
                    break;
                case "raiz":
                    resultado = "(Math.sqrt("+element.find(">span").text()*1 +"))"+resultado;
                    break;
            }
        }
    });
    return resultado;
}