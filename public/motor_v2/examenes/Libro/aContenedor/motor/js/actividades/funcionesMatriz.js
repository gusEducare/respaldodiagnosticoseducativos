var coloresLineas=['#ea5653','#3d82c3','#30b39f','#f07e07','#e26ea9'];
var coloresColumnas=['#fab651','#7c7bb9','#ee7775','#30B39F', '#B37330', '#309BB3', '#81B330'];
var evaluable = true;
var totalAciertos;
function iniciarActividad(){
json[preguntaActual].pregunta.evaluable === false || json[preguntaActual].pregunta.evidencio === false ? evaluable=false : evaluable=true;//define si es autoevaluacion
var descripcion = json[preguntaActual].pregunta.descripcion === undefined ? "Aspectos a valorar" : json[preguntaActual].pregunta.descripcion;
descripcion = descripcion === "" || descripcion === " " ? "Aspectos a valorar" : cambiarRutaImagen(descripcion);
$("#contenidoActividad").html("<table><tr class='header'><td style='background: #5dc2d7;'>"+descripcion+"</td></tr></table>");
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
 //se añaden preguntas
 var indexColor= 4;
 $.each(json[preguntaActual].preguntas, function(index, element){
     if(evaluable){
         //define el total de intentos
         if(element.correcta.search(",")!==-1){
             intentosRestantes += element.correcta.split(",").length;
         }else{
             element.correcta.length>0 ? intentosRestantes++ : "";
         }
     }
     //lena contenido
     indexColor === coloresLineas.length-1 ? indexColor = 0 : indexColor++;
    $("table").append("<tr  "+(element.correcta!=="" ? "t17='"+element.correcta+"'" : "")+" class='row' style='background: "+coloresLineas[indexColor]+";'><td>"+cambiarRutaImagen(element.t11pregunta)+"</td></tr>");
    $.each(json[preguntaActual].respuestas, function(i, e){
        $("tr:last").append("<td class='answer' style='background: rgba(255, 255, 255, 0.3);'></td>");
        if(element.valores!==undefined){
            $(".answer:last").append("<p>"+cambiarRutaImagen(element.valores[i])+"</p>");
        }
        $(".answer:last").append("<div class='check'></div>");
    });  
 });
 
 totalPreguntas += intentosRestantes;
 totalAciertos = intentosRestantes;
 //se elimina cuadro de estadisticas si es autoevaluacion
 !evaluable ? $("#buttonsNav p").remove() : "";
 //se añaden respuestas
 $.each(json[preguntaActual].respuestas, function(index, element){
     $("tr:first").append("<td style='background: "+coloresColumnas[index]+";'>"+cambiarRutaImagen(element.t13respuesta)+"</td>");
 });
 //aplica bandera anchoColumnaPreguntas
 if(json[preguntaActual].pregunta.anchoColumnaPreguntas!==undefined){
     $("tr>td:nth-child(1)").css("width", json[preguntaActual].pregunta.anchoColumnaPreguntas+"%");
 }
 //aplica bandera altoMatriz
    json[preguntaActual].pregunta.alturaMatriz!==undefined ? $("table").css({height:json[preguntaActual].pregunta.alturaMatriz+"px"}) : "";
    //si hay 5 filas, entonces la tabla se expande al tamaño de la actividad
    !evaluacion && $("tr").length>3 ? $("table").css("height", "100%") : "";

/*** LightBox ***/
    if(json[preguntaActual].pregunta.box !== null && json[preguntaActual].pregunta.box !== undefined && json[preguntaActual].pregunta.box != ''){
        // $('#contenidoActividad').prepend('<div class="btn-box"><img id="box" src="icon-' + json[preguntaActual].pregunta.box_type + '.png"></div>');
        $('#preguntaActividad').append('<div class="btn-box"><img id="box" src="icon-' + json[preguntaActual].pregunta.box_type + '.png"></div>');
        $('#preguntaActividad').children('#cuestionMarker').next().css({
            "display": "inline-block",
            "vertical-align": "top",
            "width": "90%"
        });
        $('#box').click(function() {
            $('#contenidoActividad').append('<div class="box"><div class="internal-box"><div class="cerrar"><span id="cerrar">x</span></div><img src="' + json[preguntaActual].pregunta.box + '"><div></div>');
            $('#cerrar').click(function() {
                $('.box > .internal-box').addClass('remove');
                $('.box').addClass('remove');
                setTimeout(function() {
                    $('.box').remove();
                }, 1000);
            });
        });
    }else{
    }
    /*** Fin LightBox ***/
    
 //calcula el ancho de las columnas de preguntas para que todas tengan las mismas dimenciones
    var anchoColumnas = ($("table").width() - $("tr:first>td:first").width()) / $("tr:first>td").length-1;
    anchoColumnas = anchoColumnas-10;
    if(json[preguntaActual].pregunta.columnasFlexibles === true){
        $("tr>td:not(td:first)").css("width", anchoColumnas+"px");
    }else{
        $("tr>td:not(td:first)").css({minWidth: anchoColumnas+"px", maxWidth: anchoColumnas+"px"});
    }
    
    //se evalua la respuesta seleccionada
    var dosOpciones = $("tr[t17]").length === $(".check").length/2;//se considera uicamente una respuesta correcta si solo hay dos opciones para responder por columna
    $(".answer").on("click touchend", function(){
        var element = $(this).find(".check"), esCorrecta=false;
        if(!evaluable || evaluacion){//si es autoevaluacion
            sndClick();
            if($(element).hasClass("checked")){
                $(element).css("background-image","none").removeClass("checked");
            }else{
                !evaluacion || dosOpciones ? $(element).parents("tr").find(".check").css("background-image","none").removeClass("checked") : "";
                $(element).css("background-image","url('img/opcion_multiple_palomita.png')").addClass("checked");
                !evaluacion && $(".checked").length === json[preguntaActual].preguntas.length ? sigPregunta() : "";
            }
        }else{
            var index = ($(element).parent().index()-1)+"";
            var t17 = $(element).parents("tr").attr("t17");
            if(t17!==undefined){
                esCorrecta = t17.split(",").indexOf(index) !== -1;
            }
            $(element).parent("td").addClass("lock");//bloquea el elemento seleccionado
            if(esCorrecta){//eventos correcta
                sndCorrecto();
                completo = true;
                $(element).css("background-image","url('img/opcion_multiple_palomita.png')").addClass("checked");
                $(element).addClass("correctAnswer");
                if(intentosRestantes > 0){
                    aciertos++;
                }
                $(".correctAnswer").length === totalAciertos ? sigPregunta() : "";
            }else{//eventos incorrecta
                sndIncorrecto();
                $(element).addClass("badAnswer");
                $(element).css("background-image","url('img/opcion_multiple_tache.png')").addClass("checked");
                setTimeout(function(){
                    $(element).css({opacity:"0.35", transition:"opacity 0.5s"});
                }, 500);
            }
        }
        intentosRestantes--;
        animarCheck(element);
        return false;
    });
}

function animarCheck(element){
    $(element).css({transform:"scale(0.1, 0.1)", opacity:"0", transition:"none"});
    setTimeout(function(){
        $(element).css({transform:"scale(1, 1)", opacity:"1", transition:"transform 0.35s, opacity0.35s"});
    }, 10)
}

//nuevas banderas disponibles
// "columnasFlexibles":true  //permite que las columnas de preguntas y respuestas se ajusten al contenido