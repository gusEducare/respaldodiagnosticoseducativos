function iniciarActividad(){
    finalizaActividad = false;
    $("#contenidoActividad").html("<div id='pincels'><div class='pincel transparent selected' rgb='white' rgba='white'>&nbsp</div></div><div id='map'></div><style id='colores'></style>");
    var color, rgbaColor, pincel;
    $.each(json[preguntaActual].pinceles, function(index, element){
        pincel = $("<div class='pincel'>&nbsp</div>");
        color = element.color;
        if(color.indexOf("rgba")!==-1){
            color = color.replace("rgba","rgba").replace(color.split(",").pop().replace(")",""), "1");
        }
        pincel.css({borderColor:color}).attr("pincel", "pincel"+index);
        $("#pincels").append(pincel);
        rgbaColor = $(".pincel:last").css("border-color").replace("rgb","rgba").replace(")",", 0.7)");
        $("#colores").append(".pincel[pincel='pincel"+index+"']{\n\
                border-color:"+color+";\n\
                box-shadow: 3px 3px 6px rgba(0,0,0, 0.45), inset 1px 1px 6px 1px "+rgbaColor+";\n\
                background: white;\n\
            }\n\
            td.pincel"+index+"{\n\
                background: "+rgbaColor+" !important;\n\
                box-shadow: inset 0 0 2px 1px "+color+" !important;\n\
            }");
    });
    $("#map").append("<table id='grid'></table>");//se añade el grid
    var alto=11, ancho=11;
    var custom = json[preguntaActual].pregunta.dimensiones!==undefined;
    if(custom && (alto * ancho)<200){//dimensiones personalizadas
        var dimensiones = json[preguntaActual].pregunta.dimensiones;
        alto=dimensiones.split(",")[0]*1;
        alto = alto > 8 ? alto : 8;//se define un minimo de 8 celdas
        ancho=dimensiones.split(",")[1]*1;
        ancho = ancho > 8 ? ancho : 8;//se define un minimo de 8 celdas
    }else{ alto = 11; ancho = 11; }
    //aplica la imagen de fondo en caso de que exista
    var imagenFondo = json[preguntaActual].pregunta.url !== undefined;
    imagenFondo ? $("#grid").css("background-image", cambiarRutaImagen(json[preguntaActual].pregunta.url)) : "";
    //se añaden las celdas
    while($("#grid tr").length<alto){
        $("#grid").append("<tr></tr>");
    }
    while($("#grid tr:first td").length<ancho){
        $("#grid tr").append("<td></td>");
    }
    $("td").width($("td:first").height());
    $(".pincel").on("click touchend", function(){//cambio de pincel
        $(".pincel.selected").removeClass("selected");
        $(this).addClass("selected");
        return false;
    });
    //se comienza a trazar sobre el lienzo
    var onDrag=false;
    $("td").on("mousedown touchstart", function(){
        onDrag = true;
        $(this).removeAttr("class").addClass($(".pincel.selected").attr("pincel"));
    });
    $("table").on("mousemove touchmove", function(event){
        if(onDrag && event.pageX !== undefined){
            $(elementsFromPoint(event.pageX, event.pageY)).filter("td").removeAttr("class").addClass($(".pincel.selected").attr("pincel"));
        }
    });
    $("body").on("mouseup touchcancel", function(){
        onDrag ? onDrag = false : "";
    });
    //eventos de guardar, avanzar y restaurar actividad
    !finalizaActividad ? restaurarAvace() : "";
    $("tr:first").append("<td id='cellEmpty'> <div id='guardarRespuesta' title='Guardar avance' class='button'></div><div id='next' title='Siguiente pregunta' class='button'></div><div id='back' title='Pregunta anterior' class='button'></div><div id='reiniciar' title='Reiniciar actividad' class='button'></div> </td>");
    $(".button").on("click touchend", function(){
        var element = $(this);
        element.addClass("lock").css({transform:"scale(0.5,0.5)", transition:"transform 0.15s linear"});
        setTimeout(function(){
            element.removeClass("lock").css({transform:"scale(1,1)"});
        }, 150);
        switch (element.attr("id")){
            case"reiniciar":
                localStorage.removeItem("avencesActividad:"+idActividad);
                $(".button").on(transitionEnd, function(){ location.reload(); });
                break
            case"guardarRespuesta":
                guardarAvance();
                break;
            case"next":
                 preguntaActual = preguntaActual===json.length-1 ? -1 : preguntaActual;
                sigPregunta();
                break;
            case"back":
                preguntaActual = preguntaActual===0 ? json.length-2 : preguntaActual-2;
                sigPregunta();
                break;
        }
        return false;
    });
}