var siguiente, preguntasMultiples, columnas;
function iniciarActividad(){
    $("#contenidoActividad").html("");
    preguntasMultiples = json[preguntaActual].pregunta.preguntasMultiples===true;
    siguiente = false;
    var pregunta = json[preguntaActual].pregunta.t11pregunta;
    if(pregunta.length > 0){
        if( Object.prototype.toString.call(pregunta) === '[object Array]' ) {
            $.each(pregunta, function(index, element){
                $("#contenidoActividad").append("<div class='grupoRespuestas'></div>");
                $(".grupoRespuestas:last").html("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(element)+"</div></div><div class='respuestasGrupo'></div>");
            });
        }else{
            $("#contenidoActividad").html("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(pregunta)+"</div></div><div id='respuestasActividad'></div>");
        }
    }else{
        $("#contenidoActividad").html("<div id='respuestasActividad'></div>");
    }
    var cadena="";
    $.each(json[preguntaActual].respuestas, function(index, element){
        cadena += index+",";
        var letraCss="", preguntaCss;
        var elemento = cambiarRutaImagen(element.t13respuesta);
            //aplica estilos si hay imagen como respuesta (optimiza espacio)
            elemento.search("<img") !== -1 ? letraCss="style='position: absolute !important; z-index: 3 !important; top: 15px !important;'" : "";
            elemento.search("<img") !== -1 ? preguntaCss="style='width: calc(100% - 2px) !important;'" : "";
            var selectorPregunta = preguntasMultiples ? ".grupoRespuestas:nth("+element.numeroPregunta+") .respuestasGrupo" : "#respuestasActividad";
            $(selectorPregunta).append("<div class='respuesta' t17='"+element.t17correcta+"'><div class='inciso' "+letraCss+"></div><div "+preguntaCss+">"+elemento+"</div><div class='eventoClick'></div></div>");
    });
    json[preguntaActual].pregunta.c03id_tipo_pregunta+"" === "2" ? $(".inciso").addClass("noRadius") : "";
    //centra las respuestas de imagen
    $(".respuesta:has(div img)").css("text-align","center");
    //activa la bandera columnas
    columnas=json[preguntaActual].pregunta.columnas;
    if(columnas !== undefined){
        if(!isNaN(columnas*1) && columnas>1){
            $("#respuestasActividad").addClass("columnas");
            for(var i=0; i<columnas; i++){
                var selectorColumnas = preguntasMultiples ? "#contenidoActividad" : "#respuestasActividad";
                $(selectorColumnas).append("<div class='columnaPreguntas'></div>");
            }
            $(".columnaPreguntas").css({display:"inline-block", width:100/columnas+"%", height: "100%", verticalAlign:"top"});

            var columna=0, selector = preguntasMultiples ? ".grupoRespuestas" : ".respuesta";
            $(selector).each(function(index, element){
                $(element).appendTo($(".columnaPreguntas").eq(columna));
                columna*1 === columnas*1-1 ? columna=0 : columna++;
            });    
            $("#respuestasActividad>.respuesta").appendTo($(".columnaPreguntas:last"));
            randomColumnas();
        }
    }
    //se igualan las respuestas al de mayor tamaño
    if($("#contenidoActividad img").length>0){
        $("#contenidoActividad img").on("load", function(){
            igualarTamaños();
        });
    }
    //activa bandera dosColumnas
    if(!preguntasMultiples && json[preguntaActual].pregunta.dosColumnas === true){
        $("#contenidoActividad").addClass("dosColumnas");
    }else{
        $("#contenidoActividad").removeClass("dosColumnas");
    }
    //inserta la letra para cada inciso
    if(!evaluacion){
        //aplica el random a los elementos
        if(preguntasMultiples){
            $(".respuestasGrupo").each(function(i, e){ respRandom($(e)); });
        }else if(columnas !== undefined && !isNaN(columnas*1) && columnas>1){
            $(".columnaPreguntas").each(function(i, e){ respRandom($(e)); });
        }else{
            respRandom($("#respuestasActividad"));
        }
        var contador, vuelta=-1, letraActual;
        !preguntasMultiples ? añadirIncisos($("#respuestasActividad")) : $(".respuestasGrupo").each(function(i, e){ añadirIncisos($(e)); });
        function añadirIncisos(parent){
            contador = 0;
            parent.find(".inciso").each(function(i, e){
                letraActual = vuelta===-1 ? letra[contador] : letra[vuelta]+(contador);
                contador++;
                vuelta = contador === letra.length ? vuelta+1 : vuelta;
                contador = contador === letra.length ? 0 : contador;
                $(e).html(letraActual);
            });
        }
    }
    /*** LightBox ***/
    if(json[preguntaActual].pregunta.box !== null && json[preguntaActual].pregunta.box !== undefined && json[preguntaActual].pregunta.box != ''){
        // $('#contenidoActividad').prepend('<div class="btn-box"><img id="box" src="icon-' + json[preguntaActual].pregunta.box_type + '.png"></div>');
        $('#preguntaActividad').append('<div class="btn-box"><img id="box" src="icon-' + json[preguntaActual].pregunta.box_type + '.png"></div>');
        $('#preguntaActividad').children('#cuestionMarker').next().css({
            "display": "inline-block",
            "vertical-align": "top",
            "width": "90%"
        });
        $('#box').click(function() {
            $('#contenidoActividad').append('<div class="box"><div class="internal-box"><div class="cerrar"><span id="cerrar">x</span></div><img src="' + json[preguntaActual].pregunta.box + '"><div></div>');
            $('#cerrar').click(function() {
                $('.box > .internal-box').addClass('remove');
                $('.box').addClass('remove');
                setTimeout(function() {
                    $('.box').remove();
                }, 1000);
            });
        });
    }else{
    }
    /*** Fin LightBox ***/

    //determina el total de respuestas corrrectas
    var respuestaMultiple=false;
    var respuestas=$("div[t17='1']").length;
    var tipoPregunta = json[preguntaActual].pregunta.c03id_tipo_pregunta+"";
    if(tipoPregunta === "1"){
        if(preguntasMultiples){
            intentosRestantes=$(".grupoRespuestas").length; totalPreguntas+=intentosRestantes;
        }else{
            intentosRestantes++; totalPreguntas++;
        }
    }else if(tipoPregunta === "2"){
        intentosRestantes+=respuestas; totalPreguntas+=respuestas;
        respuestaMultiple = true;
    }
    //se evalua la actividad
    var contador = 0;
    $(".respuesta").on("click touchend", function(){
        if(!evaluacion){
            contador ++; var esCorrecta=false;
            var element = this;
            if($(element).attr("t17")==="1"){
                esCorrecta=true; sndCorrecto();
                evaluaOpcion(element, true);
                if(preguntasMultiples){
                    if(tipoPregunta === "1"){
                        $(element).parents(".respuestasGrupo").addClass("lock").find(".respuesta:not(.correctAnswer)").css("opacity", "0.3");
                    }else{
                        if($(element).parents(".respuestasGrupo").find(".respuesta[t17='1']").length === $(element).parents(".respuestasGrupo").find(".correctAnswer").length){
                            $(element).parents(".respuestasGrupo").addClass("lock").find(".respuesta:not(.correctAnswer)").css("opacity", "0.3");
                        }
                    }
                }
                $(".correctAnswer").length === respuestas ? siguiente=true : "";
            }else{           
                sndIncorrecto();
                evaluaOpcion(this, false);
            }
            if(intentosRestantes > 0){
                if(esCorrecta){
                    aciertos++;
                }
                intentosRestantes--;
            } 
        }else{
            sndClick();
            if($(this).hasClass("OMSelectedEval")){
                $(this).removeClass("OMSelectedEval");
            }else{
                if(!respuestaMultiple){
                    if(preguntasMultiples){
                       $(this).parents(".grupoRespuestas").find(".OMSelectedEval").removeClass("OMSelectedEval");
                    }else{
                        $(".OMSelectedEval").removeClass("OMSelectedEval")
                    }
                }
                $(this).addClass("OMSelectedEval");
                animacionInciso(this);
            }
        }
        return false;
    });
}
function igualarTamaños(){
        var maxHeight = 0, height;
        $(".respuesta:has(img)").each(function(index, element){
            height = $(element)[0].scrollHeight;
            maxHeight = height > maxHeight ? height : maxHeight;
        });
        $(".respuesta:has(img)").css({"height":(maxHeight-12)+"px"});
}
function evaluaOpcion(element, correcta){
    var color;
    animacionInciso(element);
    $(".respuesta").addClass("lock");
    if(correcta){
        $(element).addClass("correctAnswer");
        color = "#A4CD6D";
    }else{
        $(element).addClass("badAnswer");
        color = "#e1385a";
    }
    if($(element).find("img").length !== 0){
        $(element).find("img").css({opacity:"0.5"});
        $(element).find(".eventoClick").css({"box-shadow":"inset 0 0 24px "+color});
    }
    setTimeout(function(){
        $(element).hasClass("badAnswer") ? $(element).css("opacity", "0.3") : "";
        siguiente ? sigPregunta() : $(".lock:not(.respuestasGrupo)").removeClass("lock");
    }, 50);
}
function animacionInciso(element){
    var inciso = $($(element).find(".inciso"));
    inciso.css({transform:"scale(0, 0)", transition:"none"});
    !evaluacion ? inciso.html("") : "";
    setTimeout(function(){
        $(inciso).css({transform:"scale(1, 1)", transition:"transform 0.35s"});
    }, 30);
}

function respRandom(parent){//aplica random a las posiciones de los elementos
    var cadena = "";
    parent.find(".respuesta").each(function(i){
        cadena+=i+",";
    });
    cadena = cadena.slice(0, -1);//se quita la coma al final
    cadena = cadena.split(",").sort(function(){return 0.75-Math.random();});//se divide en elementos
    $.each(cadena, function(index, element){//se reposicionan los elementos
        if(index+"" !== element+""){
            parent.find(".respuesta:nth("+index+")").insertBefore(parent.find(".respuesta:nth("+element+")"));
        }
    });
}
function randomColumnas(){
    var cadena = "";
    $(".columnaPreguntas").each(function(i){
        cadena+=i+",";
    });
    cadena = cadena.slice(0, -1);//se quita la coma al final
    cadena = cadena.split(",").sort(function(){return 0.75-Math.random();});//se divide en elementos
    $.each(cadena, function(index, element){//se reposicionan los elementos
        if(index+"" !== element+""){
            $(".columnaPreguntas:nth("+index+")").insertBefore($(".columnaPreguntas:nth("+element+")"));
        }
    });
}