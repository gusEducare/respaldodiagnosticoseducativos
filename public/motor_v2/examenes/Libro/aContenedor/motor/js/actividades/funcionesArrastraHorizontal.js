function iniciarActividad() {
    var cadena="";
    intentosRestantes = json[preguntaActual].respuestas.length;
    totalPreguntas += intentosRestantes;
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $.each(json[preguntaActual].contenedores, function (index, element) {//añade contenedores
        $("#contenedores").append('<table class="columna"><tr class="encabezado"><td>'+cambiarRutaImagen(element)+'</td></tr></table>');
    });
    $.each(json[preguntaActual].respuestas, function (index, element) {//añade respuestas
        cadena+=index+",";
        $("#respuestas").append('<div t17="'+element.t17correcta+'" class="respuestaDrag resph"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
        $(".columna").eq(parseInt(element.t17correcta)).append('<tr><td class="contenedor" t11='+parseInt(element.t17correcta)+'></td></tr>');
    });
    cartasRandom(cadena);
    coloresRandom($(".respuestaDrag"));
    if($(".respuestaDrag img").length>0){
        $("img").on("load error", fijarDimensiones);
    }else{
        fijarDimensiones();
    }
    function fijarDimensiones(){
        $(".respuestaDrag p:has(img)").addClass("img");
        $(".columna").css("width", "calc("+(100 / $(".columna").length)+"% - 10px)");
        $(".respuestaDrag").css("width", $(".contenedor:first")[0].scrollWidth);
        var maxHeight = 0;
        $(".respuestaDrag").each(function(i, e){
            maxHeight = maxHeight < e.scrollHeight ? e.scrollHeight : maxHeight;
        });
        $(".respuestaDrag, .contenedor").css("height", maxHeight);
        $("#respuestas").css("height", maxHeight+10);
    }
}