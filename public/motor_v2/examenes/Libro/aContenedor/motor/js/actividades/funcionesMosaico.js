function iniciarActividad(){
    $("#contenidoActividad").html("<div id='fondoActividad'></div><div id='colores'></div><div id='colorPicker' class='noWidth'>\n\
            <table id='colorsTable'>\n\
                <tr></tr><tr></tr><tr></tr><tr></tr>\n\
                <tr></tr><tr></tr><tr></tr><tr></tr>\n\
            </table>\n\
            <div id='lastUsed'></div></div>");
    $("#fondoActividad").css("background-image",cambiarRutaImagen(json[preguntaActual].pregunta.url));
    añadirColores();
    //añade la paleta de colores
    var coloresFijos = json[preguntaActual].pregunta.colores !== undefined && json[preguntaActual].pregunta.evaluable !== false;
    textoFinaliza = !coloresFijos;
    evaluable = coloresFijos;
    if(coloresFijos){
        $.each(json[preguntaActual].pregunta.colores, function(i,e){
            $("#colores").append("<div color='"+e+"' class='color' style='background-color:"+e+";'></input>");
        });
        $(".color:last").addClass("selected");
    }else{
        $("#colores").append("<div color='transparent' class='color na selected'></input>");
        $("#colores").append("<div color='#a6acaf' class='color' style='background-color:#a6acaf;'></input>");
        $("#colores").append("<div color='#a6acaf' class='color' style='background-color:#a6acaf;'></input>");
    }
    //añade los bloques
    $.each(json[preguntaActual].respuestas, function(i, e){
        $("#fondoActividad").append("<div class='block' \n\
                style='width: "+e.size.split(",")[0]+"px;\n\
                    height: "+e.size.split(",")[1]+"px;\n\
                    top: "+e.coords.split(",")[0]+"px;\n\
                    left: "+e.coords.split(",")[1]+"px;\n\
            ' t17='"+e.t17correcta+"'></div>")
        if(e.t17correcta*1 > 0){
            totalPreguntas++; intentosRestantes++;
        }
        if(e.url !== undefined){
            $(".block:last").addClass("mask").css("-webkit-mask-image",cambiarRutaImagen(e.url));
        }
    });
    //aplica bandera tamañoReal
    var tamañoReal = json[preguntaActual].pregunta.tamañoReal === true;
    tamañoReal ? $("#fondoActividad").addClass("tamañoReal") : "";
    //aplica la bandera blendMode
    json[preguntaActual].pregunta.blendMode === "multiply" ? $(".block").addClass("multiply") : "";
    if(strAmbiente === "DEV"){
        $(".block").addClass("dev");
    }
    //añade los eventos para los botones de color
    $(".color").on("click touch", function(){
        if(!$(this).hasClass("selected")){
            $(".selected").removeClass("selected");
            $(this).addClass("selected");
        }else if(!coloresFijos && !$(this).hasClass("na")){
            $("#colorPicker").removeClass("noWidth");
            $(".hexColor[color='"+$(this).attr("color")+"']").addClass("selected");
        }
        return false;
    }).change(function(){
        $(this).css({borderColor:$(this).val()});
    });
    //añade los eventos para los bloques a colorear
    $(".block").on("click touch", function(){
        var element = $(this);
        var color = $(".selected").attr("color");
        color === undefined ? color = "transparent" : "";
        element.attr("color", color).css({backgroundColor:color, transition:"none"}).addClass("hasColor");
        if(color === "transparent"){element.removeAttr("color").removeClass("hasColor");}
        if(coloresFijos && color === json[preguntaActual].pregunta.colores[element.attr("t17")-1]){
            if(intentosRestantes>0){
                aciertos++;
            }
            element.addClass("lock");
            sndCorrecto();
            if($(".block.hasColor").length === $(".block:not(.block[t17='0'])").length){
                sigPregunta();
            }
        }else if(!coloresFijos){
            sndClick();
            if($(".block.hasColor").length === $(".block").length){
                sigPregunta();
            }
        }else{
            sndIncorrecto();
            $(element).removeClass("hasColor");
            setTimeout(function(){
                element.css({backgroundColor:"", transition:"background 1s"});
            }, 500);
        }
        intentosRestantes--;
        return false;
    });
    //añade los eventos del color picker
    $("#colorPicker .hexColor").on("click touch", function(){
        $(".hexColor.selected").removeClass("selected");
        $(this).addClass("selected");
        $("#colorPicker").addClass("noWidth");
        $(".color.selected").attr("color", $(this).attr("color")).css({backgroundColor:$(this).attr("color")});
    });
}
function añadirColores(){
    var elements = [
        "#e6b0aa","#d98880","#cd6155","#c0392b","#a93226","#922b21","#7b241c","#641e16",
        "#f5b7b1","#f1948a","#ec7063","#e74c3c","#cb4335","#b03a2e","#943126","#78281f",
        "#d7bde2","#c39bd3","#af7ac5","#9b59b6","#884ea0","#76448a","#633974","#512e5f",
        "#d2b4de","#bb8fce","#a569bd","#8e44ad","#7d3c98","#6c3483","#5b2c6f","#4a235a",
        "#a9cce3","#7fb3d5","#5499c7","#2980b9","#2471a3","#1f618d","#1a5276","#154360",
        "#aed6f1","#85c1e9","#5dade2","#3498db","#2e86c1","#2874a6","#21618c","#1b4f72",
        "#a3e4d7","#76d7c4","#48c9b0","#1abc9c","#17a589","#148f77","#117864","#0e6251",
        "#a2d9ce","#73c6b6","#45b39d","#16a085","#138d75","#117a65","#0e6655","#0b5345",
        "#a9dfbf","#7dcea0","#52be80","#27ae60","#229954","#1e8449","#196f3d","#145a32",
        "#abebc6","#82e0aa","#58d68d","#2ecc71","#28b463","#239b56","#1d8348","#186a3b",
        "#f9e79f","#f7dc6f","#f4d03f","#f1c40f","#d4ac0d","#b7950b","#9a7d0a","#7d6608",
        "#fad7a0","#f8c471","#f5b041","#f39c12","#d68910","#b9770e","#9c640c","#7e5109",
        "#f5cba7","#f0b27a","#eb984e","#e67e22","#ca6f1e","#af601a","#935116","#784212",
        "#edbb99","#e59866","#dc7633","#d35400","#ba4a00","#a04000","#873600","#6e2c00",
        "#e5e7e9","#d7dbdd","#cacfd2","#bdc3c7","#a6acaf","#909497","#797d7f","#626567",
        "#ccd1d1","#b2babb","#99a3a4","#7f8c8d","#707b7c","#616a6b","#515a5a","#424949",
        "#aeb6bf","#85929e","#5d6d7e","#34495e","#2e4053","#283747","#212f3c","#1b2631",
        "#abb2b9","#808b96","#566573","#2c3e50","#273746","#212f3d","#1c2833","#17202a"
    ];
    var contador = 0;
    $.each(elements, function(i,e){
        contador ++;
        $("#colorsTable tr:nth-child("+contador+")").append("<td class='hexColor' color='"+e+"' style='background-color: "+e+";'></td>");
        contador === 8 ? contador = 0 :  "";
    });
}