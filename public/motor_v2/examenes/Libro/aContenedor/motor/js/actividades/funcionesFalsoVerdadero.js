var t11=1;
function iniciarActividad(){
    var fvCustom = true;
    var fvRandom = json[preguntaActual].pregunta.random !== false;
    var align = json[preguntaActual].pregunta.alinearImagenes;
    align!==undefined ? align = align.toUpperCase() === "DERECHA" ? "right" : "left" : "right";
    $("#contenidoActividad").html("<div id='fvBacground'><ul id='fvContainer'></ul></div>");
    var respuestas, t17, btn1="", btn2="", cadena="";
    $.each(json[preguntaActual].preguntas, function(i, e){
        intentosRestantes++; totalPreguntas++;
        cadena+=i+",";
        respuestas = json[preguntaActual].respuestas[i].t13respuesta;
        t17 = json[preguntaActual].respuestas[i].t17correcta;
        fvCustom = fvCustom && json[preguntaActual].respuestas[i].t13respuesta !== undefined;
        if(fvCustom){
            btn1 = cambiarRutaImagen(json[preguntaActual].respuestas[i].t13respuesta[0]);
            btn2 = cambiarRutaImagen(json[preguntaActual].respuestas[i].t13respuesta[1]);
        }
        $("#fvContainer").append("<li>"+cambiarRutaImagen(e.t11pregunta)+"</li>");
        var lastLi = $("#fvContainer li:last"), loadTimeOut=setTimeout(function(){}, 1);
        lastLi.find("img").on("load error", function(){
            clearTimeout(loadTimeOut);
            loadTimeOut = setTimeout(function(){
                lastLi.height(lastLi[0].scrollHeight - 10);
            }, 50);
        });
        lastLi.append("<div class='fvButtons'>\n\
                                        <div class='fvBtn fvBtn1'>\n\
                                            <div class='respuesta' t17='"+(t17+"" === "0" ? "1" :"0")+"'>F</div><p>"+btn1+"</p>\n\
                                        </div><div class='fvBtn fvBtn2'>\n\
                                            <div class='respuesta' t17='"+(t17+"" === "1" ? "1" :"0")+"'>"+(lang==="en" ? "T" : "V")+"</div><p>"+btn2+"</p>\n\
                                        </div></div>");
        lastLi.has(".fvButtons img").addClass("hasImg");
    });
    align!==undefined ? $("li>img:not(.fvButtons img)").css("float", align) : "";
    fvCustom ? $(".fvButtons").addClass("custom") : "";
    fvRandom && !evaluacion ? repRandom( cadena.slice(0,-1).split(",").sort(function(){return 0.5-Math.random()}) ) :"";
    $("#fvContainer li").each(function(i,e){
        $(e).attr("t11", t11++);
    });
    //aplica bandera align
//eventos click-touch
    $(".fvBtn").on("click touchend", function(){
        var element = $(this);
        var resp = element.find(".respuesta");
        var esCorrecta = false;
        resp.css({transform:"scale(0.45,0.45)"});//emulate click event on touch
        setTimeout(function(){
            resp.css({transform:"scale(1,1)"});
        }, 150);
        //evalua respuesta
        if(resp.attr("t17")==="1"){
            sndCorrecto();
            esCorrecta=true;
            element.addClass("correct lock");
            if($(".fvBtn.correct").length === $("#fvContainer li").length){
                $(".fvButtons").addClass("lock");
                sigPregunta();
            }
        }else{
            sndIncorrecto();
            element.addClass("incorrect lock");
        }
        if(intentosRestantes > 0 && element.parent().find(".lock").length === 1){
            if(esCorrecta){
                aciertos++;
            }
            intentosRestantes--;
        }
        return false;
    });
}

function repRandom(cadena){
    $.each(cadena, function(index, element){
        if(index+"" !== element+""){
            $("#fvContainer li:nth("+element+")").insertBefore($("#fvContainer li:nth("+index+")"));
        }
    });
}