function iniciarActividad(){
    evaluable = json[preguntaActual].pregunta.evaluable!==false;
    finalizaActividad = evaluable;
    $("#contenidoActividad").html("<div id='contenedorGrafica'></div>");
    var pregunta = json[preguntaActual].pregunta.t11pregunta !== undefined;
    if(pregunta){
        pregunta = json[preguntaActual].pregunta.t11pregunta;
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker'></div>"+cambiarRutaImagen(pregunta)+"</div>");
    }
    //se añade el grid
    var longitud = json[preguntaActual].respuestas.length+1;
    var altitud = json[preguntaActual].preguntas.length+1;
    if(evaluable){
        intentosRestantes = longitud-1; totalPreguntas+=intentosRestantes;
    }else{
        $("#buttonsNav p").remove();
    }
    $("#contenedorGrafica").append("<table id='grafica'></table>");
    while($("tr").length<altitud){
        $("#grafica").append("<tr></tr>");
    }
    while($("tr:first td").length<longitud){
        $("#grafica tr").append("<td></td>");
    }
    //añade boton de guasrdar cuando no sea evaluable la actividad
    $("tr:last td:first").attr("id","emptyCell");
    !evaluable && !evaluacion ? $("#emptyCell").html("<div id='guardarRespuesta' title='Guardar avance'></div><div id='next' title='Siguiente pregunta'></div><div id='back' title='Pregunta anterior'></div><div id='reiniciar' title='Reiniciar actividad'></div>") : "";
    $("#emptyCell>div").addClass("button");
    $(".button").on("click", function(){
        var element = $(this);
        element.addClass("lock").css({transform:"scale(0.5,0.5)", transition:"transform 0.15s linear"});
        setTimeout(function(){
            element.removeClass("lock").css({transform:"scale(1,1)"});
        }, 150);
    });
    $("#reiniciar").on("click", function(){//reinicia la actividad
        localStorage.removeItem("avencesActividad:"+idActividad);
        setTimeout(function(){
            location.reload();
        }, 150);
        return false;
    });
    $("#guardarRespuesta").on("click", function(){//boton guardar
        guardarAvance();
        return false;
    });
    $("#next").on("click", function(){//boton siguiente pregunta
         preguntaActual = preguntaActual===json.length-1 ? -1 : preguntaActual;
        sigPregunta();
        return false;
    });
    $("#back").on("click", function(){//boton pregunta anterior
        preguntaActual = preguntaActual===0 ? json.length-2 : preguntaActual-2;
        sigPregunta();
        return false;
    });
    //se insertan categorias y frecuencias
    var frecuencia = "";
    var listaFrecuencias = json[preguntaActual].preguntas.reverse();
    $("tr").find("td:first").each(function(index, element){
        if(index<listaFrecuencias.length){
            frecuencia = listaFrecuencias[index].t11pregunta;
            $(element).append(cambiarRutaImagen(frecuencia)).addClass("frecuencia");
        }
    });
    var t17;
    $.each(json[preguntaActual].respuestas, function(index, element){
        $("tr:last td:nth-child("+(index+2)+")").append(cambiarRutaImagen(element.t13respuesta)).addClass("categoria");
        t17 = (altitud-1) - element.t17correcta;
        !evaluacion && evaluable ? $("tr:nth-child("+t17+") td:nth-child("+(index+2)+")").attr("t17", "1") : "";
    });
    $("td:not(td[t17='1'], .categoria, .frecuencia)").attr("t17","0");
    $("#emptyCell").removeAttr("t17").css({position:"static"});
    $("td[t17]").append("<div></div>");
    //aplica bandera espacios series
    var espacioSeries = json[preguntaActual].pregunta.espacioSeries !==undefined;
    var tamañoEspacio = espacioSeries ? json[preguntaActual].pregunta.espacioSeries : 0;
    if(espacioSeries){
        $("#grafica td[t17]").css("padding-left",tamañoEspacio+"px");
        $(".categoria").css("padding-left",(tamañoEspacio+15)+"px");
    }
    //aplica el mismo ancho a las categorias
    var maxWidth=0;
    $(".categoria").each(function(){
        maxWidth = maxWidth < $(this).width() ? $(this).width() : maxWidth;
        $(this).append("<div class='serie'></div>");
    });
    //aplica bandera anchoColumnas y altoColumnas
    if(json[preguntaActual].pregunta.anchoColumnas!==undefined){
        maxWidth = json[preguntaActual].pregunta.anchoColumnas;
    }
    var maxHeight="auto";
    if(json[preguntaActual].pregunta.altoColumnas!==undefined){
        maxHeight = json[preguntaActual].pregunta.altoColumnas;
    }
    $(".frecuencia").css("height",maxHeight);
    $(".categoria").css({width:maxWidth+"px", minWidth:maxWidth+"px", maxWidth:maxWidth+"px"});
    $("td[t17] div").css({width:$("td[t17]:first").width()+"px", height:($("td[t17]:first")[0].scrollHeight)+"px"});
    $(".serie").css({width:$("td[t17]:first").width()+"px"});
    //se restaura la actividad
    !finalizaActividad ? restaurarAvace() : "";
    //eventos click/touch
    var esCorrecta, serie;
    $("td[t17]").on("click", function(event){
        var element = $(this);
        $("tr td:nth-child("+(element.index()+1)+")").addClass("lock");
        serie = $(".serie:nth("+(element.index()-1)+")");
        !evaluable || evaluacion ? serie.css({height:0, top:"-2px", opacity:1, transition:"none"}) : "";
        calcularAltura(serie, element);
        if(element.attr("t17")==="1" || !evaluable || evaluacion){
            if(evaluable && !evaluacion){
                serie.addClass("correctAnswer");
                !setProfMode ? sndCorrecto() : "";
                esCorrecta = true;
            }else{
                sndClick();
                serie.addClass("selected");
                $("tr td:nth-child("+(element.index()+1)+")").removeClass("lock selected");
                element.addClass("selected");
            }
            if(!setProfMode){
                if($(".correctAnswer").length === $(".categoria").length){
                    $("#contenidoActividad").addClass("lock");
                    sigPregunta();
                }
            }
        }else{
            serie.addClass("badAnswer");
            sndIncorrecto();
            esCorrecta = false;
            setTimeout(function(){
                serie.css({opacity:0, transition:"0.4s linear"});
                setTimeout(function(){
                    $("tr td:nth-child("+(element.index()+1)+")").removeClass("lock");
                    serie.css({height:0, top:"-2px", opacity:1, transition:"none"});
                    serie.removeClass("badAnswer");
                    serie.show();
                }, 400);
            }, 500);
        }
        if(intentosRestantes > 0){
            if(esCorrecta){
                aciertos++;
            }
            intentosRestantes--;
        }
        return false;
    });
}
function calcularAltura(serie, element){
    var posIn, altoSerie, top;
        top = element[0].getBoundingClientRect().top;
        posIn = serie[0].getBoundingClientRect().top;
        altoSerie = posIn - top;
        serie.css({opacity:1, top:-altoSerie-2, height:altoSerie, transition:"0.3s"});
}