function iniciarActividad(){
    $("#contenidoActividad").html('<div id="contenedores"></div><div id="respuestas"></div>');
    if(evaluacion && json[preguntaActual].pregunta.t11pregunta !== undefined && json[preguntaActual].pregunta.t11pregunta !== null && json[preguntaActual].pregunta.t11pregunta !== ""){
        $("#contenidoActividad").prepend("<div id='preguntaActividad'><div id='cuestionMarker' class='bg_bookColor'></div><div>"+cambiarRutaImagen(json[preguntaActual].pregunta.t11pregunta)+"</div></div>")
    }
    $("#contenedores").append('<table><tr class="encabezado"><td class="textos blank">&nbsp;</td></tr></table>');
    $.each(json[preguntaActual].contenedoresFilas, function (index, element) {
        $(".encabezado").append('<td class="textos">'+cambiarRutaImagen(element)+'</td>');
    });
    $.each(json[preguntaActual].contenedores,function(index, element){
        $("table").append('<tr id="fila'+index+'"><td class="textoFilas textos">'+cambiarRutaImagen(element)+'</td></tr>');
    });        
    var fila="", t17="", t11 = "", mostrar="", classMostrar="";
    
    var cadena="";
    //se insertan las celdas
    $.each(json[preguntaActual].respuestas, function (index, element) {
        classMostrar = element.mostrar ? "mostrar":"";
        if(Object.prototype.toString.call(element.t17correcta)==="[object Array]"){ 
            t17 = element.t17correcta.join("-");
            fila = t17.split("-")[0].split(",")[0];
            t11 = t17.split("-")[0];
        }else{ 
            t17 = element.t17correcta;
            fila = t17.split(",")[0];
            t11 = t17;
            
        }
        mostrar = element.mostrar === undefined ? "":'<div>'+cambiarRutaImagen(element.t13respuesta)+'</div>';
        if(element.mostrar !== true){
            cadena+=index+",";
            $("#respuestas").append('<div t17="'+t17+'" class="respuestaDrag resph '+mostrar+'"><p>' + cambiarRutaImagen(element.t13respuesta) + '</p></div>');
            
        }
        $("#fila"+fila).append('<td class="contenedor '+classMostrar+'" t11="'+t11+'" indice='+index+'>'+mostrar+'</td>');
    });
    //variables de evaluacion
    intentosRestantes = $(".contenedor:not(.mostrar)").length;
    totalPreguntas += intentosRestantes;
    //cartas random
    cartasRandom(cadena);
    //determina el maximo alto de las filas
    var indexRespuesta = ""; t17 = "";
    $(".respuestaDrag").each(function(){
        indexRespuesta = ""; t17 = $(this).attr("t17").split("-");
        $.each(t17, function(index, element){
            indexRespuesta += $(".contenedor[t11='"+element+"']").attr("indice")+",";
        });
        indexRespuesta = indexRespuesta.slice(0, -1);
        $(this).attr("t17", indexRespuesta);
        $(".contenedor[t11='"+t17[0]+"']").attr("t11", $(".contenedor[t11='"+t17[0]+"']").attr("indice"));
    });
    $(".blank").append(json[preguntaActual].pregunta.descripcion ? json[preguntaActual].pregunta.descripcion : "Tema");
    if($("#contenidoActividad img").length>0){
        $("img").on("load error", fijarDimensiones);
    }else{ fijarDimensiones(); }
    function fijarDimensiones(){
        var anchoContenedor = (100 -25) / json[preguntaActual].contenedoresFilas.length;
        $(".contenedor").css({width:anchoContenedor+"%"});//define el alto para las celdas
        var maxHeight=0;
        $(".contenedor, .respuestaDrag").each(function(i,e){
            maxHeight = maxHeight < e.scrollHeight ? e.scrollHeight : maxHeight;
        });
        maxHeight > 100 ? 100 : maxHeight;
        $("tr:not(tr:first) td, .respuestaDrag").css({maxHeight:maxHeight+"px", minHeight:maxHeight+"px", height:maxHeight+"px"}); //fija el alto de las filas
        $("#respuestas").css("height", maxHeight+10);
        $(".respuestaDrag").css({width: $(".contenedor:last").width()+"px"});//define las dimenciones para las respuestas
        coloresRandom("tr:not(tr:first)");
    }
}