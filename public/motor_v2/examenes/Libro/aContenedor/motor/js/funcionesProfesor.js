var inicioProf = true;
function resolverActividades(){
    if(inicioProf){
        eventosSwipe();
    }
    var t17, agrupar, contenedor, grupo, insertar;
    switch(lastResort){
        case "ArrastraVertical":
        case "ArrastraHorizontal":
        case "ArrastraMatriz":
        case "ArrastraMatrizHorizontal":
        case "ArrastraOrdenar":
        case "ArrastraImagenes":
        case "ArrastraContenedorUnico":
        case "ArrastraCorta":
        case "OrdenaElementos":
            var numPregunta = incisos ? 0 : preguntaActual;
            $(".textoContenedor").remove();
            $("#cartas").css("opacity","0");
            if(incisos){//eventos para arrastra corta incisos
                $("#cartas").html("");
                var clacesCarta = json[numPregunta].pregunta.soloTexto === true ? "" : "iconDrag";
                $.each(json[numPregunta].respuestas, function(index, element){
                    $("#cartas").append("<div class='respuestaDrag "+clacesCarta+"' t17='"+element.t17correcta+"'><p>"+cambiarRutaImagen(element.t13respuesta)+"</p></div>");
                });
            }
            grupos = json[numPregunta].pregunta.agrupaContenedores === true;
            $(".respuestaDrag").each(function(index, element){
                insertar=false;
                t17=$(element).attr("t17").split(",");
                $.each(t17, function(i, e){
                    agrupar = $(element).attr("grupo") !== undefined;
                    if(!agrupar){
                        contenedor = $(".contenedor[t11='"+e+"']:not(.ocupado):first");
                        if(contenedor.length>0 && !contenedor.hasClass("ocupado")){
                            insertar = true;
                        }
                    }else{//variable de agrupa contenedores
                        contenedor = $(".contenedor[t11='"+e+"']");
                        grupo = $(contenedor.find(".respuestaDrag"));
                        if(grupo.length === 0 || grupo.first().attr("grupo")*1 === $(element).attr("grupo")*1){
                            insertar = true;
                        }
                    }
                    if(insertar){
                        contenedor.addClass("ocupado");
                        $(element).addClass("respuestaColocada").appendTo(contenedor);
                        return false;
                    }
                });
            });
        $(".contenedor .respuestaDrag").css({border:0, "background-color":"white", color:"magenta"});
        $(".contenedor .respuestaDrag:has(img)").css({"background-color":"Magenta", color:"white"});
        $(".contenedor .respuestaDrag[style *= 'background-image'], .respuestaDrag[style*='transparent']").css({"background-color":"transparent", color:"magenta"});
        $(".resph, .noHeight").removeClass("resph noHeight");
        break;
        //
        case "RelacionaLineas":
            function solveActivity(){
                var lineas = "";
                var CI, CD;
                var x1=0, x2=0, y1=0, y2=0;
                var baseCoords = $("#contenidoActividad").offset();
                $(".col.child.left").each(function(index, element){
                    CI = $(element).find(".circle"); CD = $(".col.child.right[t17='"+$(element).attr("t11")+"'] .circle");
                    x1 = CI.offset().left + 25 - baseCoords.left;
                    x2 = CD.offset().left + 25 - baseCoords.left;
                    y1 = CI.offset().top + 25 - baseCoords.top;
                    y2 = CD.offset().top + 25 - baseCoords.top;
                    lineas += "<line x1='"+x1+"' x2='"+x2+"' y1='"+y1+"' y2='"+y2+"'></line>";
                });
                $("svg").html(lineas);
                $("img").off("load error");
            }
            setTimeout(function(){
                solveActivity();
                $("img").on("load error", function(){
                    solveActivity();
                });
            }, 200);
        break;
        //
        case "ListasDesplegables":
            $(".dropList").each(function(inidex, element){
                $(element).attr("t17")*1 === 0 ? "" : $(element).find("p:nth("+$(element).attr("t17")+")").insertBefore($(element).find("p:first"));
                $(element).find("p:first").addClass("optionSolved").removeClass("hidden");
            });
        break;
        //
        case "OpcionMultiple":
            $(".respuesta[t17='1']").addClass("OMSolved");
        break;
        //
        case "RespuestaMultipleFondo":
            $(".respuesta[t17='1']").addClass("RMFSolved");
        break;
        //
        case "FalsoVerdadero":
            $(".respuesta").css({borderColor:"silver", color:"silver"});
            $(".respuesta[t17='1']").css({borderColor:"magenta", color:"magenta"});
            $(".respuesta[t17='1']").has("img").css({backgroundColor:"magenta"});
        break;
        //
        case "Matriz":
            if(json[preguntaActual].pregunta.evaluable !== false){
                $(".row[t17]").each(function(index, element){
                    $.each($(element).attr("t17").split(","), function(i, e){
                        $(element).find(".answer:nth("+e+") .check").addClass("checkSolved");
                    });
                });
            }
        break;
        //
        case "Crucigrama":
            $(".pregunta").each(function(i,e){
                var palabra = json[preguntaActual].respuestas[$(e).attr("t17")].t13respuesta;
                palabra = eliminarP(palabra);
                $(".cell[nPreg"+$(this).attr("dir")+"='"+$(this).attr("npreg")+"']").find("input").each(function(ind, ele){
                    $(this).val(palabra.charAt(ind));
                });
            });
            $("input").off("keydown keypress keyup");
        break;
        //
        case "ArrastraImagen":
        case "ArrastraEtiquetas":
        var columnas = json[preguntaActual].pregunta.columnas === true;
        var selector, answer, t17;
            $(".etiqueta").each(function(index, element){
                t17 = $(element).attr("t17").split(",");
                answer = element.outerHTML;
                $.each(t17, function(i, e){
                    selector = columnas ? ".columna:nth("+e+") .contenedor" : ".contenedor:nth("+e+")";
                    $(answer).appendTo(selector);
                    $(selector).css("border-color", $(answer).css("background-color"));
                });
            });
            lastResort === "ArrastraImagen" ? $(".contenedor .etiqueta").css({backgroundColor:"magenta"})  : "";
        break;
        //
        case "ArrastraRespuestaMultiple":
            $(".columna").each(function(index, element){
                while($(element).find(".respuesta").length>1){
                    $(element).find(".respuesta:last").remove();
                }
                $(element).append("<p class='xCount'>x"+$(element).find(".respuesta").attr("cntdr")+"</p>");
            });
            $(".respuesta").css("right","80px");
        break;
        case "Mosaico":
            $(".color").each(function(i,e){ $(".block[t17='"+($(e).index()+1)+"']").css("background-color",$(e).attr("color")); });
        break;
        case "Graficas":
            $("td[t17='1']").trigger("click");
        break;
        case "PreguntaAbierta":
            $.each(json[preguntaActual].respuestas, function(i, e){ $("textarea.area").eq(i).text(e.t17correcta); })
        break;
    }
}

function eventosSwipe(){
    var posIn, posEnd, dPos;
    $("#contenidoActividad")[0].addEventListener("mousemove", {passive:false});
    $("#contenidoActividad")[0].addEventListener("touchmove", {passive:false});
    $("#contenidoActividad").draggable({axis:"x", 
        start:function(event){
            posIn = event.pageX;
            $("#contenidoActividad").css({transition:"none"}).on("mousemove touchmove", 
            function(event){
                event.preventDefault();
            });
        },
        stop: function(event){
            posEnd = event.pageX;
            dPos = posIn - posEnd;
            direccionCambio = dPos < 0 ? "100" : "-100";
            dPos < 0 ? dPos = dPos*-1 : "";
            if(dPos > 300){//cambio de pregunta
                var cambiaPregunta;
                if(direccionCambio === "100"){//pregunta anterior
                    preguntaActual-=2;
                }
                cambiaPregunta = preguntaActual>-2 && preguntaActual<json.length-1;
                if(cambiaPregunta){
                    incisos === true ? siguienteCorta() : sigPregunta();
                }else{
                    preguntaActual < -1 ? preguntaActual = 0 : "";
                    $("#contenidoActividad").css({left:0, transition:"left 0.35s"});
                }
            }else{//se regresa a la posicion origginal
                $("#contenidoActividad").css({left:0, transition:"left 0.35s"});
            }
            $("#contenidoActividad *").off("mousemove touchmove");
            $("#contenidoActividad *").on("mousemove touchmove");
        }
    });
}