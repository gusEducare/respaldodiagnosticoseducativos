var cssBottom = "5px", progressTimeout = setTimeout(hideProgress, 3000);
function barraAvance(){
    for(var i = 1; i<=json.length; i++){
        $("#progressBar").append("<div class='mark unsolved bg_bookColor2'><hr class='bar bg_bookColor2'><p class='num'>"+i+"</p></div>");
    }
    $("#progressBar .mark:last .bar").remove();
    $("#progressBar .mark").eq(preguntaActual).addClass("current").removeClass("unsolved");
    if(libroProf){
        $("<img src='img/flecha.png' id='reload'>").on("click", function(){ location.reload(); }).appendTo("body");
    }
    if(setProfMode){
        if(libro === "PROF"){
            $("#progressBar .mark").on("click touch", function(){
                showProgress();
                if(preguntaActual !== $(this).index()){
                    actualizaProgreso($(this));
                    preguntaActual = $(this).index() - 1;
                    incisos ? siguienteCorta() : sigPregunta();
                }
            });
        }
    }else{
        cssBottom = "-40px";
        $("#progressBar").on("click touch", hideProgress);
    }
    $("#contenidoActividad").on("cambioDePregunta", function(){
        if(!esUltima){
            showProgress();
        }else if (libro === "PROF" && esUltima) {
            showProgress();
        }
        setTimeout(function(){
            actualizaProgreso($("#progressBar .mark").eq(preguntaActual));
        }, 500);
    });
}
function hideProgress(){
    $("#progressBar").css("bottom", cssBottom);
}
function showProgress(){
    clearTimeout(progressTimeout);
    $("#progressBar").css("bottom", "5px");
    progressTimeout = setTimeout(hideProgress, 6000);
}
function actualizaProgreso(element){
    $("#progressBar .mark").addClass("unsolved").removeClass("solved current").each(function(i,e){
        if(i === element.index()){
            $(e).addClass("current").removeClass("unsolved");
            return false;
        }else{
            $(e).addClass("solved").removeClass("unsolved");
        }
    });
}

var selector;
function setUi(){
    ////////////////////////////////////////////////////////////////////////////////
    $(".contenedor.ui-droppable").droppable( "disable" );
    selector = ".contenedor";

    $(".parent:has(>.contenedor)").length>0 ? selector=".parent:has(>.contenedor)" : "";
    $(".contenedores:has(>.contenedor)").length>0 ? selector=".contenedores:has(>.contenedor)" : "";
    if(lastResort === "RespuestaMultipleFondo"){
        selector=".respuesta";
        $(selector).resizable({
            aspectRatio: true,
            resize:function(){
                $(selector).not($(this)).width($(this).width()).height($(this).height());
            }
        });
    }
    $(".respuesta").off("click");
    $(selector).on("click mouseenter", function(){
        $(".repHover").removeClass("repHover");
        var contenedor = $(this).addClass("repHover");
        $(".respuestaDrag").each(function(){
            var t17 = $(this).attr("t17").split(",");
            t17.indexOf(contenedor.attr("t11")) !== -1 ? $(this).addClass("repHover") : "";
        });
    });
    $(selector).css({position:"absolute", background:"rgba(0,0,0, 0.45)"}).resizable({
        stop:function(){
            updateJson();
        }
    }).draggable({
        stop:function(){
            updateJson();
        }
    });
    window.top.aplyFilters();
}
function updateUI(grid, axis, resizable){
    $(selector).draggable({
        grid:[grid, grid],
        axis:axis
    });
    $(selector).resizable( resizable ? "enable" : "disable" );
    $(selector).resizable({ grid:[grid, grid] });
}
///////////////////////////////////////////////////////////////////////////////
function updateJson(){
    var contenedor, position;
    if(lastResort === "RespuestaMultipleFondo"){
        $.each(json[preguntaActual].respuestas, function(i,e){
            contenedor = $(selector).eq(i);
            position = contenedor.position();
            e.coordenadas = position.top+","+position.left;
        });
        if($(selector+":first").width() !== 40){
            json[preguntaActual].pregunta.radio = $(selector+":first").width();
        }
    }else{
        $.each(json[preguntaActual].contenedores, function(i, e){
            contenedor = $(selector).eq(i);
            position = contenedor.position();
            e.Contenedor[1] = position.top+","+position.left;
            e.Contenedor[3] = contenedor.width()+","+contenedor.height();
        });
    }
}
function saveJson(){
    var file = parent.ruta.split("paquetes-json/").pop()+"/"+parent.idActividad+".js";
    var data = {ruta:file, txt: "json="+JSON.stringify(json,null,2)};
    $.ajax({
        type:"POST",
        url: "../php/saveFile.php",
        async:false,
        data:data,
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            $(parent.document.body).find("#save").toggleClass("green");
            setTimeout(function(){
                $(parent.document.body).find("#save").toggleClass("green");
            }, 35);
        }
    });
}