json = [
    {
        "respuestas": [
            {
                "t13respuesta": "refused",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "lend",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "to help",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "apologize",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "dad",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "borrow",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "mom",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "remembered",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Carla was so tired that she  "
            },
            {
                "t11pregunta": " to help her mom with the dishes.<br>Ulises did not want to "
            },
            {
                "t11pregunta": " his laptop to his father.<br>Seo-Jun refused "
            },
            {
                "t11pregunta": " his friend with the science project.<br>Carla plans to "
            },
            {
                "t11pregunta": " to her mother.<br>Ulises will do the same with his "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the correct option an place it in the blank to complete the sentences.",
            "t11instruccion": "<b>Listening </b><br><br>\n\
            Carla and Ulises are chatting during the recess at school. Seo-Jun, a Korean classmate, joins them.<br><br>\n\
            <b>Carla:</b> I think I made a big mistake at home last night.<br><br>\n\
            <b>Ulises:</b> Really? What did you do?<br><br>\n\
            <b>Carla:</b> I was so tired that when my mom asked me to help with the dishes, I just snapped at her and said I couldn’t. I could see by her face that she was quite upset, but she didn’t say anything. I really shouldn’t have responded like that.<br><br>\n\
            <b>Ulises:</b> Oh dear. That wasn’t very cool. But I think we all have done bad things like that at one time or another. I did something like that last week.<br><br>\n\
            <b>Seo-Jun:</b> Hi guys! Can I sit here with you two?<br><br>\n\
            <b>Ulises:</b> Sure, Seo-Jun! Here, sit here next to me. We were just talking about some mistakes we have made recently.<br><br>\n\
            <b>Seo-Jun:</b> Oh!… that’s interesting. What did you do, Ulises? <br><br>\n\
            <b>Ulises:</b> Well… I hate to admit it, but my father wanted to borrow my laptop as his had frozen, but I said no. I was worried he might accidently erase my homework files or something. He isn’t very good with computers. But his expression made me regret it immediately. I could’ve lent it to him and helped him. I felt really bad.<br><br>\n\
            <b>Seo-Jun:</b> Yes… you shouldn’t have been so mean. But I can’t talk! Only yesterday one of my friends wanted some help with his science project. I could’ve helped him, my next class wasn’t for another half hour, but I said no as I was feeling too lazy. He had to go and get help from someone else. I felt a bit guilty after, so when I saw him again I apologized and offered to help him this weekend.<br><br>\n\
            <b>Carla:</b> Well... at least you apologized, that’s good. I didn’t, but now hearing that you did, I will talk to my mom tonight. I hope she accepts my apology.<br><br>\n\
            <b>Ulises:</b> Oh... I am sure she will. Good for you, Carla! I’ll talk to my dad, too.<br><br>\n\
            ",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "She was upset.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "She felt guilty.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "She was very tired.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Because his had frozen.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Because he might erase his homework files.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Because he isn’t very good with computers.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Because he was in his lazy moment.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Because he needed help with his science project.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Because he could get help from someone else.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Apologize to her mother.",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Offer help this weekend.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Sit next to her.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Because he refused to help someone as well.",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Because he didn’t have class for another hour.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Because he apologized to his friend.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "None of them were helpful to someone.",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "They didn’t want to help their parents.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Their actions were incorrect towards their friends.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Listen again. Choose the option that best answers the question.<br>How did Carla’s mom feel after she refused to help with the dishes?",
                "Why did Ulises’s father need to borrow his laptop?",
                "Why did Seo-Jun refused to help his friend?",
                "What does Carla plan to do about her feeling guilty?",
                "Why can’t Seo-Jun talk?",
                "What do the three kids have in common?"
            ],
            "t11instruccion": "<b>Listening </b><br><br>\n\
            Carla and Ulises are chatting during the recess at school. Seo-Jun, a Korean classmate, joins them.<br><br>\n\
            <b>Carla:</b> I think I made a big mistake at home last night.<br><br>\n\
            <b>Ulises:</b> Really? What did you do?<br><br>\n\
            <b>Carla:</b> I was so tired that when my mom asked me to help with the dishes, I just snapped at her and said I couldn’t. I could see by her face that she was quite upset, but she didn’t say anything. I really shouldn’t have responded like that.<br><br>\n\
            <b>Ulises:</b> Oh dear. That wasn’t very cool. But I think we all have done bad things like that at one time or another. I did something like that last week.<br><br>\n\
            <b>Seo-Jun:</b> Hi guys! Can I sit here with you two?<br><br>\n\
            <b>Ulises:</b> Sure, Seo-Jun! Here, sit here next to me. We were just talking about some mistakes we have made recently.<br><br>\n\
            <b>Seo-Jun:</b> Oh!… that’s interesting. What did you do, Ulises? <br><br>\n\
            <b>Ulises:</b> Well… I hate to admit it, but my father wanted to borrow my laptop as his had frozen, but I said no. I was worried he might accidently erase my homework files or something. He isn’t very good with computers. But his expression made me regret it immediately. I could’ve lent it to him and helped him. I felt really bad.<br><br>\n\
            <b>Seo-Jun:</b> Yes… you shouldn’t have been so mean. But I can’t talk! Only yesterday one of my friends wanted some help with his science project. I could’ve helped him, my next class wasn’t for another half hour, but I said no as I was feeling too lazy. He had to go and get help from someone else. I felt a bit guilty after, so when I saw him again I apologized and offered to help him this weekend.<br><br>\n\
            <b>Carla:</b> Well... at least you apologized, that’s good. I didn’t, but now hearing that you did, I will talk to my mom tonight. I hope she accepts my apology.<br><br>\n\
            <b>Ulises:</b> Oh... I am sure she will. Good for you, Carla! I’ll talk to my dad, too.<br><br>\n\
            ",
            "preguntasMultiples": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Only take those items that you really need.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Take a tent that is big enough for the number of occupants.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Take enough food if you don’t plan to get your own.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Take enough fuel and supplies to cook.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Take the appropriate wear.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Take the right camping gear.",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "Take a first aid kit and other supplies.",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Tip #1"
            },
            {
                "t11pregunta": "Tip #2"
            },
            {
                "t11pregunta": "Tip #3"
            },
            {
                "t11pregunta": "Tip #4"
            },
            {
                "t11pregunta": "Tip #5"
            },
            {
                "t11pregunta": "Tip #6"
            },
            {
                "t11pregunta": "Tip #7"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the correct tip number to what it said.",
            "t11instruccion": "<b>Reading</b><br><br>\n\
            Read this advice to campers.<br><br>\n\
            If you are thinking of going camping any time soon, these tips will help you get the most out of your trip.<br><br>\n\
            Tip number 1<br><br>\n\
            Take only what is necessary. Most campers take far too much gear and regret it when they have to haul their rucksacks around all day.<br><br>\n\
            Tip number 2<br><br>\n\
            To allow for a good night’s sleep every night, it is advisable to make sure your tent is a good size for the number of people required. Today one can buy very lightweight tents that can be put up in no time at all and that will provide good shelter from storms. A comfortable sleeping bag is also highly recommended, but choose one that suits the weather conditions you will be facing.<br><br>\n\
            Tip number 3<br><br>\n\
            Make sure that, unless you intend to catch your own food, you take the right type and amount of food necessary for three meals a day. Choose food that is filling and energy giving such as packets of dry soup, tins of beans, power bars, sugar and tea or instant coffee. Take plenty of water!<br><br>\n\
            Tip number 4<br><br>\n\
            If you plan to heat water or cook, make sure you have plenty of fuel like wood or charcoal and lighters. Newspaper or, in some cases, a magnifying glass can be used to start fires. If not, take a small stove and lots of gas canisters. \n\
            Tip number 5<br><br>\n\
            Make sure you take the appropriate clothes. It is not advisable to wear denim jeans or shirts as these get very heavy and uncomfortable when wet. Better take light cotton clothes that can dry quickly. Comfortable, light but strong boots are a must, these will help prevent accidents while walking, and don’t forget plenty of thick socks. A waterproof jacket and warm sweater will also be needed.<br><br>\n\
            Tip number 6<br><br>\n\
            Take the tools and gadgets that you will need. These include maps, a flashlight and spare batteries, a good knife, an axe, a small spade and a cell phone with a spare battery.<br><br>\n\
            Tip number 7<br><br>\n\
            For your safety and hygiene, make sure you pack a good medical kit, toilet paper, soap, shampoo and plastic bags to keep the trash in.<br><br>\n\
            Final tip<br><br>\n\
            Remember, always leave the campsite in better condition than you found it!\n\
            ",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "far too",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "sleeping bag",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "filling",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "to wear",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "area",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "tent",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "very",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "shorts",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Most campers take "
            },
            {
                "t11pregunta": " much gear and regret it.<br>A comfortable "
            },
            {
                "t11pregunta": " is also highly recommended.<br>Choose food that is "
            },
            {
                "t11pregunta": " and energy giving.<br>It is not advisable "
            },
            {
                "t11pregunta": " denim jeans.<br>Always leave the "
            },
            {
                "t11pregunta": " in better condition than you found it!<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the option to the right place to complete the sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "has been released",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "were releasing",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "wasn’t released",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "was designed",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "were designing",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "will design",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "will be awarded",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "is awarding",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "were awarded",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "have been trusted",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "were trusting",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "will trust",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "was elected",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "is electing",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "elects",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Read and choose the option which best completes the sentence.<br>The new album <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> since last month. ",
                "The admission exam <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> by the school teachers. ",
                "A special recognition <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> to the firemen of the neighborhood next weekend.",
                "I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> with a very special secret. I must keep it.",
                "My brother <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> as captain of the team this morning. ",
            ],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "us do",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "make us",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "we will",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "brother take out",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "will make",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Next week will be a very strange week in my house: Mom will be away visiting our aunt and she will have "
            },
            {
                "t11pregunta": " some adjustments to our chores, but it’s weird. First, Mom will "
            },
            {
                "t11pregunta": " wake up Dad, so he can take us to school. Then, "
            },
            {
                "t11pregunta": " let Dad fix breakfast and lunch for us. Therefore, Dad will have my "
            },
            {
                "t11pregunta": " the trash and my brother "
            },
            {
                "t11pregunta": " me get, both, mine and his uniform ready for the following day. I wish Mom comes back soon!<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose and drag the options that best complete the sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "into an old friend whom I had not seen in a long time.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "with your neighbors, but you must find the way.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "out with patience.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "off the grill party and make a home dinner.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "after my siblings.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Yesterday I ran"
            },
            {
                "t11pregunta": "Sometimes it is difficult to deal"
            },
            {
                "t11pregunta": "It was a complicated situation which we finally sorted"
            },
            {
                "t11pregunta": "It rained so hard that we had to call"
            },
            {
                "t11pregunta": "Our parents taught me to always look"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the columns to complete the sentences.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "must",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "can",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "will",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "might",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "can",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "should",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "can’t ",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "will",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "should",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "might",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "can’t",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "must",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "must",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "can",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "should",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",//&#9135;&#9135;&#9135;&#9135;  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> para guión bajo
            "t11pregunta": ["Choose the best option to complete the sentences.<br>She is not picking up her phone, she <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> be busy. ",
                "I’d rather buy the tickets now because they <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>  not have them later.  ",
                "Well, Anne is visiting the museum with my sister, so it <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> be her riding that bike. ",
                "It is a little cloudy, do you think it  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> rain?",
                "That’s the most expensive rate, it <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> be the ‘all inclusive’ price.  ",
            ],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "loves",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "have",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "should have",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "had",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "faster",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "have won",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "couldn’t",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "taken",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "to get",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "could",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "got",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "have been",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "I know she "
            },
            {
                "t11pregunta": " you; all this must "
            },
            {
                "t11pregunta": " been a misunderstanding.<br>They "
            },
            {
                "t11pregunta": " arrived by now, they might have "
            },
            {
                "t11pregunta": " a setback.<br>If they had run "
            },
            {
                "t11pregunta": ", they could "
            },
            {
                "t11pregunta": " the first place.<br>The play was excellent! It "
            },
            {
                "t11pregunta": "  have been better.<br>It may have "
            },
            {
                "t11pregunta": " longer for them "
            },
            {
                "t11pregunta": " there with the rain.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the words and drop them in the blanks to complete the sentences. There are three options that you will not need to use.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "So",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Such",
                "t17correcta": "1",
            },

        ], "preguntas": [
            {//<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> 
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mary has been <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>  a supportive friend, I’m glad to know her. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My uncle was <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> excited because his team won, that he took us out for dinner. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "It’s been <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> difficult to climb the mountain that all the climbers are exhausted. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My friend is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> an intelligent student that he got a scholarship.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I had thought it was <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> an impossible task to do, that I had postponed it for years",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose the correct option to complete the sentences.",
            "t11instruccion": "",
            "descripcion": "Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "believed",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "reported",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "was",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "have",
                "t17correcta": "4"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "It is commonly "
            },
            {
                "t11pregunta": " that being skinny is healthy, but doctors have "
            },
            {
                "t11pregunta": " that sometimes being so thin might be a sign of a poor diet and thus, of a poor nutrition. Just as some years ago it "
            },
            {
                "t11pregunta": " claimed that eating much was a sign of being well nourished. Doctors "
            },
            {
                "t11pregunta": " advised to get a general check up at least once a year.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose the correct options to complete the sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //12
    {
        "respuestas": [
            {
                "t17correcta": "was walking"
            },
            {
                "t17correcta": "heard"
            },
            {
                "t17correcta": "had"
            },
            {
                "t17correcta": "decided"
            },
            {
                "t17correcta": "investigate"
            },
            {
                "t17correcta": "came"
            },
            {
                "t17correcta": "was"
            },
            {
                "t17correcta": "have"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Last night, I  "
            },
            {
                "t11pregunta": "  (walk)  to my house when I   "
            },
            {
                "t11pregunta": " (hear)  a strange noise. I  "
            },
            {
                "t11pregunta": "(have) never heard such a strange noise, so I "
            },
            {
                "t11pregunta": "(decide) to  "
            },
            {
                "t11pregunta": " (investigate)  where it  "
            },
            {
                "t11pregunta": " (come) from. It  "
            },
            {
                "t11pregunta": " (be) a frog! Now I  "
            },
            {
                "t11pregunta": " (have)  a new pet. <br><br>\n\
                In your notebook, write a narrative paragraph about the funniest, weirdest, or most ridiculous moment of your life. Your paragraph must include:<br>\n\
                <li> A catchy line referring to that moment\n\
                <li> A description of the situation\n\
                <li> The people who was there\n\
                <li> How you felt\n\
                <li> How people reacted\n\
                <li> What you learned, if you learned something from the experience\n\
                "
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Fill the narrative paragraph with the correct form of the verb in parentheses.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: false
        }
    },
    //13
    { 
        "respuestas":[ 
       { 
        "t13respuesta":"which made them miss the next class.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"which made him feel ignored.", 
        "t17correcta":"2", 
        }, 
       { 
        "t13respuesta":"which totally surprised me because he loved it.", 
        "t17correcta":"3", 
        }, 
       { 
        "t13respuesta":"which got me thinking about buying her a new mixer.", 
        "t17correcta":"4", 
        }, 
       { 
        "t13respuesta":"which turned out to be really useful for my brain.", 
        "t17correcta":"5", 
        }, 
       ], 
        "preguntas": [ 
        { 
        "t11pregunta":"Sara’s team had to clean the laboratory," 
        }, 
       { 
        "t11pregunta":"Everyone seemed to have forgotten his birthday," 
        }, 
       { 
        "t11pregunta":"My brother told me he got rid of the videogame," 
        }, 
       { 
        "t11pregunta":"My mom enrolled in a new cooking class," 
        }, 
       { 
        "t11pregunta":"When I was a small girl I took up knitting," 
        }, 
        ], 
        "pregunta":{ 
        "c03id_tipo_pregunta":"12", 
        "t11pregunta":"Match the columns to complete the ideas.",
        "t11instruccion": "", 
        "altoImagen":"100px", 
        "anchoImagen":"200px" 
        } 
        },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "Having",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "After",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Has",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "published",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "publishes",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "publishing",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Walking",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Walked",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Walks",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "holding",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "holds",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "held",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "reading",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "read",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "reads",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Select the correct option to complete the statement.<br><br><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> excused himself, the teacher left the classroom. ",
                "Having <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> her most recent investigation, the scientist took a break.",
                "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> around in circles, he seemed really worried.",
                "The man <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> the stick is my neighbor.",
                "After <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> the letter, the committee met again. ",
            ],
            "t11instruccion": " ",
            "preguntasMultiples": true
        }
    },
    //15
     {
            "respuestas": [        
                {
                   "t13respuesta":  "couldn’t",
                     "t17correcta": "1" 
                 },
                {
                   "t13respuesta":  "might",
                     "t17correcta": "2" 
                 },
                {
                   "t13respuesta":  "must",
                     "t17correcta": "3" 
                 },
                {
                   "t13respuesta":  "may",
                     "t17correcta": "4" 
                 },
                {
                   "t13respuesta":  "must",
                     "t17correcta": "5" 
                 },
            ],
            "preguntas": [ 
              {
              "t11pregunta": "Staring at the suspect, the chief inspector asked the witness to give his statement one more time.<br><b>Witness:</b> “I can’t say for sure, it was very dark and I "
              },
              {
              "t11pregunta": " see the thief’s face.”<br><b>Chief inspector:</b> “But the height and complexion of the suspect do match the man you saw …”<br><b>Witness:</b> “Yes, they do… errr, it "
              },
              {
              "t11pregunta": " be him, he is as tall as the person I saw and that cut behind his back "
              },
              {
              "t11pregunta": " have been made by a heavy object, like the one the victim used to defend himself.”<br><b>Chief inspector:</b> “We need you to confirm your declaration; try to recall every detail. It "
              },
              {
              "t11pregunta": " seem difficult for you, but you are doing an excellent job. We "
              },
              {
              "t11pregunta": " have missed a detail, try again!”<br>The witness closed his eyes and a drop of sweat rolled down his forehead.<br>"
              },
            ],
            "pregunta": {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "Using what you know about deduction and possibility, drag and drop the words to complete the paragraph.", 
               "t11instruccion": "", 
               "respuestasLargas": true, 
               "pintaUltimaCaja": false, 
               "contieneDistractores": true 
             } 
          } 
];