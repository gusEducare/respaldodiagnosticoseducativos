json = [
    {
        "respuestas": [
            {
                "t13respuesta": "False",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "True",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mark is doing some spring cleaning.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mariana is waxing the floors.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mariana is telling jokes.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mom is cleaning the bedroom.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The sister is mopping the kitchen floor.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose (T) for true or (F) for false for the following statements.",
            "t11instruccion": "",
            "descripcion": "Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "waxing the floor.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "working outside the house.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "laughing her head off.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "spring cleaning.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "imitating my movements.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "I know nothing about"
            },
            {
                "t11pregunta": "My father prefers"
            },
            {
                "t11pregunta": "My mom saw me and is"
            },
            {
                "t11pregunta": "I don’t come near the kitchen during"
            },
            {
                "t11pregunta": "My sister is mopping the kitchen floor while"
            },
        ],
        "pregunta": {
            "t11pregunta": "Match the halves to make sentences. ",
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "light",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "don’t have to",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "has to",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "hottest",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "coldest",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "public",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "private",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "teams",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "pairs",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Two",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Three",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Choose the option that best completes the sentence.<br><br>Readers are encouraged to wear <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> clothes.",
                "The first week back in school will be the <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
                "Authorities plan to distribute shots of hydrating fluid in <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> schools.",
                "To play Guess the Song they need to divide the class in <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
                "The second game is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>  Truths and a Lie."],
            "t11instruccion": "<center><b>Reading</b></center><br>\n\
            Next week, children from all over the country will finally go back to school after a long summer in what experts think will be the hottest week since 1980. Hopefully, many schools will have air conditioning or ceiling fans in every classroom. Authorities plan to distribute shots of hydrating fluid in public schools. At the same time, schools are asked to refrain from holding any outdoor activities.<br><br>\n\
            It doesn’t sound like much fun, does it? In GED Magazine we spent some time finding fun indoor activities that students can do at school given the heat wave we face. Scroll down and see what our team suggests you do to have fun inside your classroom during recess.<br><br>\n\
            <br><b>Guess the Song!</b><br>\n\
            1. Divide the class into teams of four to five members.<br>\n\
            2. The first team will start singing one or two verses from a popular song while the other teams try to guess the song.<br>\n\
            3. The team that guesses first will continue singing the song from that point on. It will now be the turn of that team to start singing a new song that other teams will try to guess.<br>\n\
            The team that guesses more songs will be the winner.<br>\n\
            <b>Two Truths and a Lie!</b><br>\n\
            To play this game, one student comes to the front and says three things about himself. Two of those are true and one is a lie. The other students have to guess which one is the lie. The student who guesses right, gets a point. It will be his turn, then, to come to the front next. If, after two tryouts no one guesses, the point goes to the student who said the sentences originally.<br>\n\
            To play any competition game, you will have to come up with a strategy to determine who goes first in the games. We suggest you use a selection system based on a fact about the students. Use a fact no one can argue with; for example, the oldest student or the youngest one, the one whose hair is longest, the student whose birthday is coming up, the first or last in the list, etc.<br><br>\n\
            Go to our blog to check out more ideas, suggestions, and recipes for this fall.<br><br>\n\
            Keep in mind that it’s important to wear lightweight, light-colored clothing. Wear sunscreen at all times. Don’t wait until you’re thirsty to drink water and, as much as possible, stay inside a ventilated room, preferably one that has air-conditioning.\n\
            ",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Children from all over the country will finally go back to school.</p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>Authorities plan to distribute shots of hydrating fluid in public schools.</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>Here are some indoor activities you can do.</p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
            {
                "t13respuesta": "<p>Go to our blog to check out more ideas, suggestions, and recipes for this fall.</p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p>Wear sunscreen at all times.</p>",
                "t17correcta": "4",
                etiqueta: "5"
            },
            {
                "t13respuesta": "<p>Drink lots of liquids.</p>",
                "t17correcta": "5",
                etiqueta: "6"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Arrange the following events in the order they are mentioned in the text.",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "came",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "had",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "gardener",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "said",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "wash the dishes",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "cleaning up",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "brought",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "sang",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "didn’t",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "ordered",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "cook",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "plumber",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "do",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "come",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Hi!<br>My friends "
            },
            {
                "t11pregunta": " over yesterday and we "
            },
            {
                "t11pregunta": " a picnic in my backyard. The "
            },
            {
                "t11pregunta": " just cut the grass and the smell was just perfect. My parents "
            },
            {
                "t11pregunta": " I could invite my friends with the condition that we clean everything after that. All my friends volunteered to "
            },
            {
                "t11pregunta": " and help with the "
            },
            {
                "t11pregunta": " afterwards. Tom "
            },
            {
                "t11pregunta": " a guitar and we all "
            },
            {
                "t11pregunta": " and had fun. We "
            },
            {
                "t11pregunta": " cook; we "
            },
            {
                "t11pregunta": " some pizza instead. My dad prepared some salad and Sally brought the drinks.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Look at the image. Drag the correct word and place it in the space provided to complete the text.<center> <img style='height: 180px; display:block; ' src='EI8A_B1_R01.png'></center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "I hate to brush my hair.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I enjoy brushing my hair any time.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I like to style my hair in the mornings.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Look at the picture and choose the sentence that best describes it.<br><br><center> <img style='height: 300px; display:block; ' src='EI8A_B1_R02.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "I love Christmas gifts.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I don’t like to get presents.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I hate lying on the floor. ",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Look at the picture and choose the sentence that best describes it.<br><br><center> <img style='height: 400px; display:block; ' src='EI8A_B1_R03.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "I don’t like eating vegetables. ",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I like salads. ",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I love broccoli. ",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Look at the picture and choose the sentence that best describes it.<br><br><center> <img style='height: 400px; display:block; ' src='EI8A_B1_R04.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "I don’t like tidying my room.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I love to clean my bedroom.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I like to order the things in my room.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Look at the picture and choose the sentence that best describes it.<br><br><center> <img style='height: 400px; display:block; ' src='EI8A_B1_R05.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "I enjoy nature.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I don’t like flowers.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I hate the grass. ",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Look at the picture and choose the sentence that best describes it.<br><br><center> <img style='height: 400px; display:block; ' src='EI8A_B1_R06.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "I like cars.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "I don’t like toys.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Robots are my favorite.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": [" Look at the picture and choose the sentence that best describes it.<br><br><center> <img style='height: 400px; display:block; ' src='EI8A_B1_R07.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "False",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "True",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I have got one brother.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I haven’t got any headsets.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My brother has got a green T-shirt.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I have got a tablet. ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My brother hasn’t got a cell phone.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Look at the picture and choose (T) for true of (F) for false according to what you see.<br><br><center> <img style='height: 400px; display:block; ' src='EI8A_B1_R08.png'> </center>",
            "t11instruccion": "",
            "descripcion": " Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "himself",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "ourselves",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "myself",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the picture and choose the correct word to complete the sentence.<br><br>Reagan cannot walk by <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> yet.<center> <img style='height: 400px; display:block; ' src='EI8A_B1_R09.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "myself",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "yourself",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "herself",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the picture and choose the correct word to complete the sentence.<br><br>I cannot cut my hair by<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.<center> <img style='height: 400px; display:block; ' src='EI8A_B1_R10.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "herself",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "myself",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "themselves",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the picture and choose the correct word to complete the sentence.<br><br>Tanya brushes her teeth by<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.<center> <img style='height: 400px; display:block; ' src='EI8A_B1_R11.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "ourselves",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "themselves",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "yourselves",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the picture and choose the correct word to complete the sentence.<br><br>We prepare our lunch<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.<center> <img style='height: 400px; display:block; ' src='EI8A_B1_R12.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //17
    {
        "respuestas": [
            {
                "t13respuesta": "themselves",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "ourselves",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "herself",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the picture and choose the correct word to complete the sentence.<br><br>The cheerleaders design their routines<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.<center> <img style='height: 400px; display:block; ' src='EI8A_B1_R13.png'> </center>"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //18
    {
        "respuestas": [
            {
                "t13respuesta": "myself",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "was",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "heard",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "were",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "came",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "opened",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "screamed",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "saw",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "told",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "sleep",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "yourself",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "listen",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "say",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Last night I was by "
            },
            {
                "t11pregunta": " watching a horror movie. It "
            },
            {
                "t11pregunta": " really scary.<br>I "
            },
            {
                "t11pregunta": " some noises in the window. Two cats "
            },
            {
                "t11pregunta": " in the backyard, I think.<br>Suddenly, my parents "
            },
            {
                "t11pregunta": " and "
            },
            {
                "t11pregunta": " the door. I "
            },
            {
                "t11pregunta": " .<br>When Mom "
            },
            {
                "t11pregunta": " how scared I was, she "
            },
            {
                "t11pregunta": " me to stop watching that.<br>I didn’t "
            },
            {
                "t11pregunta": " well. I guess I was too scared.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag and place the option that best completes the sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //19
    {
        "respuestas": [
            {
                "t13respuesta": "did you go last weekend?",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "did you do in your birthday?",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "was the girl you were talking to?",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "did you visit your grandfather?",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "didn’t you go to the party?",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "did you fix your cell phone?",
                "t17correcta": "6",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Where"
            },
            {
                "t11pregunta": "What "
            },
            {
                "t11pregunta": "Who"
            },
            {
                "t11pregunta": "When "
            },
            {
                "t11pregunta": "Why"
            },
            {
                "t11pregunta": "How"
            },
        ],
        "pregunta": {
            "t11pregunta": "Match the columns and complete the questions.",
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //20
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Hello, Daniel! Did you visit the city last weekend?</p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>Hi, Carla. Yes, we did.</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>You don’t seem so happy. Did you go to the amusement park?</p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
            {
                "t13respuesta": "<p>No, we didn’t.</p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p>Why, did your parents prefer to visit the museum?</p>",
                "t17correcta": "4",
                etiqueta: "5"
            },
            {
                "t13respuesta": "<p>Yes, they did.</p>",
                "t17correcta": "5",
                etiqueta: "6"
            },
            {
                "t13respuesta": "<p>Don’t worry. You can come with me next Saturday.</p>",
                "t17correcta": "6",
                etiqueta: "7"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Read the sentences and arrange the conversation.",
            "t11instruccion": "",
        }
    },
    //21
    {
        "respuestas": [
            {
                "t13respuesta": "visited",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "lives",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "was",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "woke up",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "helped",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "learned",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "wake",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "visits",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "learn",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Last summer, we "
            },
            {
                "t11pregunta": " my aunt’s farm. She "
            },
            {
                "t11pregunta": " in the country.  It "
            },
            {
                "t11pregunta": " a wonderful experience. We "
            },
            {
                "t11pregunta": " very early and "
            },
            {
                "t11pregunta": " my aunt milk the cows. Then we "
            },
            {
                "t11pregunta": " how to make some cheese, cream, and butter. I don’t usually "
            },
            {
                "t11pregunta": " up that early! It was an extraordinary experience!<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //==============================0   Actualizacion actividades 14 de septiembre
    //22
    {
        "respuestas": [
            {
                "t13respuesta": "cheese from the farm?",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "didn’t.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "with the animals?",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "did!",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "try fresh milk?",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Did you bring some"
            },
            {
                "t11pregunta": "No, we"
            },
            {
                "t11pregunta": "Did you like to work"
            },
            {
                "t11pregunta": "Yes, I"
            },
            {
                "t11pregunta": "Did she like to"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the columns to make questions and answers.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //23
    {
        "respuestas": [
            {
                "t13respuesta": "If",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Why",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Do",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option to complete the statement. <br><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> you don’t feel well, you need to go to the doctor.",
            "t11instruccion": "",
        }
    },
    //24
    {
        "respuestas": [
            {
                "t13respuesta": "worries",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "worry",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "worried",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option to complete the statement. <br>If I don’t get home early, Mom <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> a lot.",
            "t11instruccion": "",
        }
    },
    //25
    {
        "respuestas": [
            {
                "t13respuesta": "put",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "made",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "puts",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option to complete the statement. <br>When you are in the movies, you <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> your cellphone in silent mode.",
            "t11instruccion": "",
        }
    },
    //26
    {
        "respuestas": [
            {
                "t13respuesta": "could",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "do",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "if",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option to complete the statement. <br> Excuse me, <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> you tell me how to get to the bank, please?",
            "t11instruccion": "",
        }
    },
    //27
    {
        "respuestas": [
            {
                "t13respuesta": "couldn’t",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "could",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "cannot",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option to complete the statement. <br> We didn’t go to the party because we <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> leave our dogs by themselves.",
            "t11instruccion": "",
        }
    },


    //===================== Terminacion de actualizacion

    {
        "respuestas": [
            {
                "t13respuesta": "are",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "am",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "write",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "put on",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "drives",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "don’t",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "doesn’t",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "ends",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "do",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "have",
                "t17correcta": "10"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Dear Sergei,<br>I hope you "
            },
            {
                "t11pregunta": " well. My name is Daniel. I "
            },
            {
                "t11pregunta": " your host for the students’ exchange programme.<br>I "
            },
            {
                "t11pregunta": " to welcome you and to let you know a little bit of our daily routine.<br>We wake up at 6:30 in the mornings to take a shower. Then we "
            },
            {
                "t11pregunta": " our school uniform and get ready.<br>We have breakfast at 7:15 and Mom "
            },
            {
                "t11pregunta": " us to school. We "
            },
            {
                "t11pregunta": " take the bus because Mom likes to have breakfast with us and chat but she "
            },
            {
                "t11pregunta": " pick us up; in the afternoon, we walk home.<br>School "
            },
            {
                "t11pregunta": " at 2:00 and we have lunch at the school’s canteen and "
            },
            {
                "t11pregunta": " homework from 3:00 to 4:00. We have football training from 4:00 to 5:00 and finally we take a shower.<br>We get home by 6:00 and we "
            },
            {
                "t11pregunta": " dinner at 6:30 with all my family. After that, we have time to rest or play. We usually go to bed by 9:30.<br>Please feel free to tell me if there is any food that you like, or a place you want to visit.<br>I hope to see you soon.<br>Regards,<br>Daniel<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the words to the space provided to complete the email.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "EI8A_B1_R14_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "EI8A_B1_R14_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "EI8A_B1_R14_03.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "EI8A_B1_R14_04.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "EI8A_B1_R14_05.png",
                "t17correcta": "4",
                "columna": "0"
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrange the fragments of the email on the correct place.",
            "tipo": "ordenar",
            "imagen": true,
            "url": "EI8A_B1_R14.png",
            "evaluable": true,
            "respuestaImagen": true,
            "tamanyoReal": false,
            // "anchoImagen": 60,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["", "15,161", "cuadrado", "398, 34", ".", "transparent"] },
            { "Contenedor": ["", "89,161", "cuadrado", "398, 34", ".", "transparent"] },
            { "Contenedor": ["", "154,72", "cuadrado", "396, 96", ".", "transparent"] },
            { "Contenedor": ["", "266,72", "cuadrado", "396, 96", ".", "transparent"] },
            { "Contenedor": ["", "393,111", "cuadrado", "396,96", ".", "transparent"] },



        ]
    },

];
