
json = [
    //1 relacionar columnas
    {
        "respuestas": [
            {
                "t13respuesta": "Ulises will rehearse all day. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Ulises will audition for a musical. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Ulises will compete in chess.  ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Ulises will go to dinner with his aunt. ",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Ulises will play football. ",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Ulises will know if he got the part.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Ulises will play chess a second time.  ",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Sunday",
            },
            {
                "t11pregunta": "Monday",
            },
            {
                "t11pregunta": "Tuesday ",
            },
            {
                "t11pregunta": "Wednesday",
            },
            {
                "t11pregunta": "Thursday",
            },
            {
                "t11pregunta": "Friday ",
            },
            {
                "t11pregunta": "Saturday ",
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the columns to show what Ulises will do on the week.",
        }
    },
    //2ordena elementos
    {
        "respuestas": [
            {
                "t13respuesta": "<p>I’m auditioning for the school musical.<\/p>",
                "t17correcta": "0",
                etiqueta: "paso 1"
            },
            {
                "t13respuesta": "<p>I’m competing in a chess tournament in another school.<\/p>",
                "t17correcta": "1",
                etiqueta: "paso 2"
            },
            {
                "t13respuesta": "<p>It’s my aunt’s birthday and I’m taking her to dinner.<\/p>",
                "t17correcta": "2",
                etiqueta: "paso 3"
            },
            {
                "t13respuesta": "<p>I’m playing football with friends at 4pm.<\/p>",
                "t17correcta": "3",
                etiqueta: "paso 4"
            }
            ,
            {
                "t13respuesta": "<p>I’m going to find out the results of the audition for the school musical.<\/p>",
                "t17correcta": "4",
                etiqueta: "paso 5"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Arrange the following sentences in the order they were mentioned in the text.<\/p>",
        }
    },
    //3 opcion multiple
    {
        "respuestas": [
            {
                "t13respuesta": "1864",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "1888",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "1837",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },


            {
                "t13respuesta": "1769",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "1837",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "1864",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },


            {
                "t13respuesta": "1807",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "1837",
                "t17correcta": "1",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "1888",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },


            {
                "t13respuesta": "1807",
                "t17correcta": "1",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "1864",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "1769",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },



            {
                "t13respuesta": "1886",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "1837",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "1807",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },



        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Choose the correct option to complete each sentence.<br><br>Mr. Marcus started working in his car in &#9135;&#9135;&#9135;&#9135.",
                "Nicolas J. Cugnot worked on a steam powered car in &#9135;&#9135;&#9135;&#9135.",
                "Davidson from Scotland designed an electrical traction in &#9135;&#9135;&#9135;&#9135 .",
                "Francois Isaac de Rivaz made a hydrogen engine in &#9135;&#9135;&#9135;&#9135.",
                "Karl F. Benz presented an internal combustion engine in &#9135;&#9135;&#9135;&#9135.",],
                "t11instruccion":"<strong>Carla:</strong> Remember to take notes that we can use in our essay. I’ll read about Siegfried Samuel Marcus… mmm, it says here that Mr. Marcus started worked on building a car in 1864. After he finally built his first prototype he dismantled it because he realized that it was too difficult to maneuver. This picture here is a car he built in 1888; it can be seen at the Technological Museum of Vienna and it is one of the oldest cars in the world. Again, it didn’t succeed because it was exhausting to operate.<br><br><strong>Daniel:</strong> Wow, I never realized how much effort was behind cars. This plate is from Nicolas Joseph Cugnot, who in 1769, worked on a steam powered car which was not successful due to the difficulty involved in carrying a huge external combustion engine.<br><br><strong>Ulises:</strong> Well, what about this? Robert Davidson from Scotland was the first person to design electrical traction in 1837, but its building process was very expensive, so it couldn’t succeed. How awful must that have been!<br><br><strong>Carla:</strong> And listen to this! Francois Isaac de Rivaz made a hydrogen engine in 1807, but its design was not successful either. It must have been fascinating to live in those days, among so many inventors transforming the world.<br><br><strong>Ulises:</strong> Sure!<br><br><strong>Daniel:</strong> Finally Karl Friedrich Benz presented an internal combustion engine in 1886 which later developed into the four wheel car we know today.<br><br><strong>Daniel and Ulises:</strong> That’s why we know his name!",
            "preguntasMultiples": true,
            
        }
    },
    //4 VERDADERO O FALSO
    {
        "respuestas": [
            {
                "t13respuesta": "False",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "True",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Siegfried Samuel Marcus dismantled his car prototype.  ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mr. Marcus prototype can be seen in a museum in Spain. ",

                "correcta": "0"
            },

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nicolas Joseph Cugnot’s steam powered car was very successful.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Building Robert Davidson’s design was very expensive.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Karl Friedrich Benz presented an internal combustion engine.",

                "correcta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Determine if the following statements are true or false.",
            "descripcion": "Aspects",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,
        }
    },
    //5  respuesta habierta
    {
        "respuestas": [
            {
                "t17correcta": "packing"
            },
            {
                "t17correcta": "to do"
            },
            {
                "t17correcta": "to pack"
            },
            {
                "t17correcta": "to say"
            },
            {
                "t17correcta": "to learn"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "My mother thinks it is easier to pack our bags three days in advance.<br>  My mom thinks that"
            },
            {
                "t11pregunta": "our backs three days in advance is easier.  <br> But my brother and I prefer doing it the night before.<br> But for my brother and me, it is enough"
            },
            {
                "t11pregunta": "  it the night before. <br>Packing things in the last minute is not very convenient. <br> It is very inconvenient "
            },
            {
                "t11pregunta": "things in the last minute.<br>Saying that my mom is very organized makes me proud. <br> I am proud"
            },
            {
                "t11pregunta": " that Mom is very organized. <br>Learning from someone like her is very easy. <br> In fact, it is very easy "
            },
            {
                "t11pregunta": " from someone like her.  "
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Type one or two words to make the second sentence carry the same meaning as the first one.",
            "evaluable": true,
            "pintaUltimaCaja": false,
            "acentos":true,
            "mayusculas":true
        }
    },

    //6 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Smoking<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>To drive <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Exercising <\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p> having <\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>To swim <\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Smoke<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Swim<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Drives<\/p>",
                "t17correcta": "8"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": " is injurious to the smoker and to those around him. <br>"
            },
            {
                "t11pregunta": "fast in rainy weather can cause accidents.<br>"
            },
            {
                "t11pregunta": "and"
            },
            {
                "t11pregunta": "a balanced diet are basic if you pursue a healthy lifestyle. "
            },
            {
                "t11pregunta": "when the tide is high is really dangerous."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose the correct word to complete the sentences.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    //7 relaciona columnas
    {
        "respuestas": [
            {
                "t13respuesta": "to play video games with me. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "me to clean my room. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "to let us publish a new school magazine. ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "user to log in every day. ",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "enroll in the excursion to the mountain.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "I will ask Mike ",
            },
            {
                "t11pregunta": "Last week my mom forced ",
            },
            {
                "t11pregunta": "We finally convinced the principal ",
            },
            {
                "t11pregunta": "This app requires the ",
            },
            {
                "t11pregunta": "Dad did not allow us to",
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the columns to complete the sentences.",
        }
    },
    //
    //8 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p> finished <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "stood",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>have been<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p> has won<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>studied <\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>finishing<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>has been<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>winning <\/p>",
                "t17correcta": "8"
            }

        ],
        "preguntas": [
            {
                "t11pregunta": "We finally "
            },
            {
                "t11pregunta": " our project for the science fair. <br> The new student felt nervous when he "
            },
            {
                "t11pregunta": "in front of the class. <br>We  "
            },
            {
                "t11pregunta": "planning this summer camp for months.<br> Our team "
            },
            {
                "t11pregunta": " this tournament for three years in a row.  <br>I "
            },
            {
                "t11pregunta": "hard for the final test.  "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose the correct options to complete the sentences.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //9 resopuesta habierta
    {
        "respuestas": [
            {
                "t17correcta": "am"
            },
            {
                "t17correcta": "Friday"
            },
            {
                "t17correcta": "at"
            },
            {
                "t17correcta": "is"
            },
            {
                "t17correcta": "from"
            },
            {
                "t17correcta": "last"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "I  "
            },
            {
                "t11pregunta": " going to have several tests this week.  <br> We are going to have English on "
            },
            {
                "t11pregunta": " .<br>This week we are going to have recess "
            },
            {
                "t11pregunta": "10:15.<br>Literature"
            },
            {
                "t11pregunta": " going to be the first test. <br>Geography is going to be  "
            },
            {
                "t11pregunta": "  9:30 to 10:15.<br> So, it will "
            },
            {
                "t11pregunta": " 45 minutes. "
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Read the table and complete the sentences by typing just one word.",
            evaluable: true,
            "pintaUltimaCaja": false,
            "t11instruccion": '<img src="EIAE_B01_A00_01.png" >'
        }
    },

    //10 opcion multiple
    {
        "respuestas": [
            {
                "t13respuesta": "good",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "amazing",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "best",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },


            {
                "t13respuesta": "amazing",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "nice",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "beautiful",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },


            {
                "t13respuesta": "hungry",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "starving",
                "t17correcta": "1",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "satisfied",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },


            {
                "t13respuesta": "tiny",
                "t17correcta": "1",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "little bit",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "smaller",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },



            {
                "t13respuesta": "happy",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "thrilled",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "fantastic",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },



        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Choose the option that best completes the sentence.<br><br>Oh, that’s a &#9135;&#9135;&#9135;&#9135 idea!",
                "She painted an &#9135;&#9135;&#9135;&#9135 landscape and it was immediately sold. ",
                "I didn’t have dinner last night nor breakfast this morning, so I’m &#9135;&#9135;&#9135;&#9135.",
                "Standing at the top of the mountain, you realize how &#9135;&#9135;&#9135;&#9135 the human being is. ",
                "I was very &#9135;&#9135;&#9135;&#9135 to know I did a good exam. ",],
            "preguntasMultiples": true,
           
        }
    },
    // 11VERDADERO O FALSO
    {
        "respuestas": [
            {
                "t13respuesta": "might",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "will",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tony ____ be thrilled when he knows we are going to the beach. He loves it! ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "She doesn’t pick up the phone; she ____ be busy with something else.",

                "correcta": "0"
            },

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The teacher _____ not accept our project if we don’t provide enough basis to back it up. He warned us.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I can’t find my notebook, I ______ have left it in the library but I’m not sure.",

                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I prefer to buy the tickets now because they ____ sell them all today.",

                "correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Mark the correct option to complete the sentence.",
            "descripcion": "Aspects",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,
            "t11instruccion": ""
        }
    },

    //12 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p> have to <\/p>",
                "t17correcta": ["1", "5"]
            },
            {
                "t13respuesta": "<p> must<\/p>",
                "t17correcta": ["2", "3"]
            },
            {
                "t13respuesta": "<p>must  <\/p>",
                "t17correcta": ["3", "2"]
            },
            {
                "t13respuesta": "<p>don’t have to <\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>have to <\/p>",
                "t17correcta": ["5", "1"]
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "We"
            },
            {
                "t11pregunta": " wear tennis shoes to enter the basketball court. That’s the rule.  <br> I  "
            },
            {
                "t11pregunta": "get first place in the spelling bee this year, I really want to. <br> Oh, I insist, you"
            },
            {
                "t11pregunta": "try this cake; it’s delicious!<br> You "
            },
            {
                "t11pregunta": "come if you don’t want to, but I’m sure you will enjoy it. <br>The teacher said we "
            },
            {
                "t11pregunta": "follow the instructions carefully.  "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose the correct options to complete the sentences.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
//13 Arrastra corta  
{
        "respuestas": [
            {
                "t13respuesta": "<p>tried<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>doing <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Have <\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p> been <\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>was<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>try<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>does<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Did<\/p>",
                "t17correcta": "8"
            }, {
                "t13respuesta": "<p>be<\/p>",
                "t17correcta": "8"
            }, {
                "t13respuesta": "<p>is<\/p>",
                "t17correcta": "8"
            }, 

        ],
        "preguntas": [
            {
                "t11pregunta": "I have never "
            },
            {
                "t11pregunta": " banana water. All I know is they make it in Quintana Roo, Mexico.<br>She was "
            },
            {
                "t11pregunta": "her homework when I called.<br>"
            },
            {
                "t11pregunta": "you visited the new movie theatre?<br>I have "
            },
            {
                "t11pregunta": "looking for you all morning! <br>My dad "
            },
            {
                "t11pregunta": "taking a shower when you called."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag and drop the correct words to complete the statements.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //14 nrelaciona lineas
     { 
 "respuestas":[ 
{ 
 "t13respuesta":"should get a second opinion.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"are not entirely satisfied with the product, you can always get a refund.", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"better do exactly as the doctor said.", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"clean up your room by Friday.", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"to listen to the students before making a decision.", 
 "t17correcta":"5", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Mr. Evans, I think you" 
 }, 
{ 
 "t11pregunta":"Dear customer, if you" 
 }, 
{ 
 "t11pregunta":"She’d" 
 }, 
{ 
 "t11pregunta":"You ought to" 
 }, 
{ 
 "t11pregunta":"The school Committee ought" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
  "t11pregunta": "Using what you know, match the columns to complete the statements.",
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } ,
 //15 Opcion multiple

{  
      "respuestas":[
         {  "t13respuesta":"too", 
            "t17correcta":"1", 
            "numeroPregunta":"0"
         },
         {  "t13respuesta":"enough", 
             "t17correcta":"0", 
            "numeroPregunta":"0"
         },
         {  
             
            "t13respuesta":"such", 
             "t17correcta":"0", 
            "numeroPregunta":"0"
         }, 
       


         {  
            "t13respuesta":"so", 
           "t17correcta":"1",     
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"too", 
         "t17correcta":"0", 
            "numeroPregunta":"1"
         },
         {  
             "t13respuesta":"enough", 
        "t17correcta":"0", 
            "numeroPregunta":"1"
         },




          {  
            "t13respuesta":"too", 
            "t17correcta":"1", 
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"enough", 
            "t17correcta":"0", 
            "numeroPregunta":"2"
         },
         {"t13respuesta":"such", 
             "t17correcta":"0", 
            "numeroPregunta":"2"
         },
         



         {  
           
 "t13respuesta":"such", 
 "t17correcta":"1", 
            "numeroPregunta":"3"
         }, {   "t13respuesta":"too", 
 "t17correcta":"0", 
            "numeroPregunta":"3"
         }, { 
 "t13respuesta":"so", 
 "t17correcta":"0", 
            "numeroPregunta":"3"
         },





         {  
 "t13respuesta":"enough", 
 "t17correcta":"1", 
            "numeroPregunta":"4"
         }, {  
 "t13respuesta":"such", 
 "t17correcta":"0", 
            "numeroPregunta":"4"
         }, { 
 "t13respuesta":"so", 
 "t17correcta":"0", 
            "numeroPregunta":"4"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Choose the best option to complete the statement.<br><br>That T-shirt is &#9135;&#9135;&#9135;&#9135 big for you, it looks like a dress.", 
                         "The party was great! We had &#9135;&#9135;&#9135;&#9135 much fun.",
                         "Oh, I have a stomach ache, I guess I had &#9135;&#9135;&#9135;&#9135 many hot dogs.",
                         "Wow! Congratulations! I didn’t know you were &#9135;&#9135;&#9135;&#9135 a good singer!",
                       "Please taste the water, is it sweet &#9135;&#9135;&#9135;&#9135 ?"

                         ],
         "preguntasMultiples": true,

      }
   },



 //20 repuesta Unica
  {
        "respuestas": [
            {
                "t17correcta": "quickly"
            },
            {
                "t17correcta": "fast"
            },
            {
                "t17correcta": "carefully"
            },
            {
                "t17correcta": "patiently"
            },
            {
                "t17correcta": "grateful"
            },
           
        ],
        "preguntas": [
            {
                "t11pregunta": "During an evacuation drill, people must leave &nbsp"
            },
            {
                "t11pregunta": "(quick, quickly)<br>How &nbsp"
            },
            {
                "t11pregunta": "can you run?   (fast, fastly)<br>She is &nbsp"
            },
            {
                "t11pregunta": "following the instructions.   (careful, carefully)<br>The teacher is &nbsp"
            },
            {
                "t11pregunta": "observing the students solve the quiz.  (patient, patiently)<br>The winner is &nbsp"
            },
            {
                "t11pregunta": "to receive the medal.   (grateful, gratefully)"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
             "t11pregunta": "Choose the best option to complete the statement.<br><br>Please taste the water, is it sweet &#9135;&#9135;&#9135;&#9135 ?", 

            "pintaUltimaCaja": false,
              "acentos":true,
            evaluable:true
        }
    }
];
