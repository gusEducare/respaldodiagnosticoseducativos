
json = [
    //1 opcion multiple
    {
        "respuestas": [
            {
                "t13respuesta": "Safety Measures in Case of Natural Disasters",
                "t17correcta": "1",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "Strategies to Prevent Panic in Natural Disasters",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, {
                "t13respuesta": "How Can Natural Disasters Be Predicted",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Contact information.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Their parents’ full name.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "A mobile phone.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "A first aid kit.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "A booklet with safety procedures.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "Contact information.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Nothing.",
                "t17correcta": "1",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "The value of your safety.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "Food and water.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Scientists.",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "Staff and teachers.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "School coordinators.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },



        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Listen to the audio and choose the option that answers the question best.<br><br>What is a good name for this article?",
                "What do students need to keep at all times?",
                "What should schools keep at hand?",
                "What should students take with them in case of an emergency?",
                "Who can predict natural disasters?",],
            "t11instruccion": "",
            "preguntasMultiples": true,
            evaluable: true,

        }
    },


    //2 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p> save<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>safety <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>places <\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>procedures<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>predictions<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>risks<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>kits<\/p>",
                "t17correcta": "0"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Planning helps"
            },
            {
                "t11pregunta": " lives. <br>Students need to become familiar with"
            },
            {
                "t11pregunta": "procedures.<br>Systematic emergency operations help reduce or prevent risks.<br>Schools need to identify safe "
            },
            {
                "t11pregunta": "of refuge.<br> Students should take emergency situations seriously, stay calm and follow"
            },
            {
                "t11pregunta": "."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen again and choose the option that completes the sentences correctly. There are three extra options that you will not need to use.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    //3
    //VERDADERO O FALSO
    {
        "respuestas": [
            {
                "t13respuesta": "False",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "True",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The Sun Empire Exhibition showed Mayan history. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aztecs’ early “postal system” was huge and complex.",

                "correcta": "1"
            },

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "They received almost all of their food from local trade.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There were many types of markets, and they served also as a socialization place.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Merchants enjoyed a favorable position in society.",

                "correcta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose “T” if the statement is True, or “F” if it’s False .",
            "t11instruccion": "<center><b> My Experience of “The Sun Empire” Exhibition<b></center><br>\n\
            I visited “The Sun Empire” this past weekend and was impressed both by how big the exhibition was and by the new information I learned about the Aztec civilization. I knew a bit about the Aztecs, but I knew only the most basic facts. I thought this was because I’m not a Mexican, but as I shared my experience with locals, I was surprised to know it was a shared feeling.\n\
            <br><br>This exhibition helped me understand what took place in Mexico before the Spaniards arrived. I saw the typical Aztec artifacts that I was already familiar with, like the Aztec calendar and other stone sculptures. However, I through the galleries dedicated to the commercial aspect of Aztec life were the most interesting.\n\
            <br><br>Aztecs’ early “postal system” was huge and complex, and many areas of their society depended on it. They traded and transported both goods and information. They received almost all of their food from foreign trade. They also traded specific luxury products such as precious stones, metals and cacao beans for the higher classes. Messengers usually delivered messages orally and sometimes goods were carried. There were many types of markets and markets served also as a socialization place.\n\
            <br><br>Merchants enjoyed a favorable position in society. There were those who oversaw all trades, decided where and to whom to trade, and supervised that prices were fair, among other things. They were usually the oldest merchants who no longer travelled but who had acquired that higher rank though many years of experience. These higher-ranked merchants in turn decided which merchants should travel on trade journeys, and of course chose their best merchants for trading on long dangerous  journeys.\n\
            The last group of merchants served a little like spies. They travelled disguised in order to avoid robberies while transporting valuable products, but also to gather information incognito that could be valuable to the military.\n\
            <br><br>I would definitely recommend a visit to the museum before the exhibition closes, as the saying says: “Those who do not learn history are doomed to repeat it.",
            "descripcion": "Aspects",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,
        }
    },
    //4 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p> Mexico <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aztec",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>goods<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p> orally<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>spies<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Argentina <\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>food<\/p>",
                "t17correcta": "0"
            }, {
                "t13respuesta": "<p>in paper<\/p>",
                "t17correcta": "0"
            }, {
                "t13respuesta": "<p>assistants <\/p>",
                "t17correcta": "0"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "This exhibition helped me understand what took place in "
            },
            {
                "t11pregunta": " before the Spaniards arrived. <br>I saw the typical "
            },
            {
                "t11pregunta": " artifacts that I was already familiar with, like the Aztec calendar.<br>They traded and transported both "
            },
            {
                "t11pregunta": " and information. <br> Messengers usually delivered messages "
            },
            {
                "t11pregunta": " and sometimes goods were carried. <br>The last group of merchants served a little like "
            },
            {
                "t11pregunta": ". "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag and drop the option that best completes the sentence. There are some extra options that you will not need to use.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    //5 OPCION MULTIPLE
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EThe bus hasn’t left yet.\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EThe bus has already left.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EThe bus is leaving at this moment.\u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option according to what you see in the picture.",
            "t11instruccion": '<img src="EI6E_B01_A00_01.png" >',
        }
    },
    //6 OPCION MULTIPLE
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EWhen he arrived, the bus had already left. \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EWhen he arrived, the bus was about to leave.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EThe bus hadn’t left yet when he arrived.\u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option according to what you see in the picture.",
            "t11instruccion": '<img src="EI6E_B01_A00_02.png" >',
        }
    },
    //7 OPCION MULTIPLE
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EThe movie has already started. \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EThe movie has already finished.\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EThe movie hasn’t started yet\u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option according to what you see in the picture.",
            "t11instruccion": '<img src="EI6E_B01_A00_03.png" >'

        }
    },
    //8 OPCION MULTIPLE
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EThe movie has already finished.  \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EThe movie has already started. \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EThe movie hasn’t finished yet. \u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option according to what you see in the picture.",
            "t11instruccion": '<img src="EI6E_B01_A00_04.png" >'

        }
    },
    //9 OPCION MULTIPLE
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EHave you met our new classmates yet?   \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EHave you seen my new bike lately?\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EHave they already finished their homework? \u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Choose the correct option according to what you see in the picture.",
            "t11instruccion": '<img src="EI6E_B01_A00_05.png" >'
        }
    },
    //10
    //VERDADERO O FALSO
    {
        "respuestas": [
            {
                "t13respuesta": "False",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "True",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The family still needs to set up the  tent. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The girl needs to put her marshmallow closer to the fire.",

                "correcta": "1"
            },

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The boys don’t want to sit by the campfire.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The backpacks need to be placed inside the tent to keep them away from any animal.",

                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The family is getting ready to go to bed. It’s not so late!",

                "correcta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Look at the picture. Read the sentences and mark them as true or false according to what you see.",
            "descripcion": "Aspects",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true,
            "t11instruccion": '<img src="EI6E_B01_A00_06.png" >'
        }
    },
    //11 ordena elementos
    {
        "respuestas": [
            {
                "t13respuesta": "<p>I enjoy Sundays, first of all, because I don’t have to get up early.<\/p>",
                "t17correcta": "0",
                etiqueta: "Step 1"
            },
            {
                "t13respuesta": "<p>Second, I have the chance to have breakfast with my family.<\/p>",
                "t17correcta": "1",
                etiqueta: "Step 2"
            },
            {
                "t13respuesta": "<p>After breakfast, we generally take a shower and get ready to go out.<\/p>",
                "t17correcta": "2",
                etiqueta: "Step 3"
            },
            {
                "t13respuesta": "<p>Once we are ready, we grab our bikes and start our journey to the park.<\/p>",
                "t17correcta": "3",
                etiqueta: "Step 4"
            }
            ,
            {
                "t13respuesta": "<p>When we get to the park, we join several friends to begin a bike route.<\/p>",
                "t17correcta": "4",
                etiqueta: "Step 5"
            }
            ,
            {
                "t13respuesta": "<p>By the end of the route, we meet again and decide where to have lunch.<\/p>",
                "t17correcta": "5",
                etiqueta: "Step 6"
            }
            ,
            {
                "t13respuesta": "<p>Finally, we enjoy lunch and have fun all together.<\/p>",
                "t17correcta": "6",
                etiqueta: "Step 7"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Read the sentences and arrange the text in the correct order.<\/p>",
        }
    },
    //12relacionar columnas
    {
        "respuestas": [
            {
                "t13respuesta": "it is freezing.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "if you have an exam the next morning.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "in your everyday diet.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "spend more than one hour a day doing it.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "at least eight hours a day.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "skip breakfast.",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "thirty minutes a day.",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "You should cover yourself to go out;"
            },
            {
                "t11pregunta": "You shouldn’t go to bed late"
            },
            {
                "t11pregunta": "You should include vegetables"
            },
            {
                "t11pregunta": "It’s ok to play video games, but you shouldn’t"
            },
            {
                "t11pregunta": "Everybody should sleep"
            },
            {
                "t11pregunta": "You shouldn’t"
            },
            {
                "t11pregunta": "We should exercise at least"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Choose the option from the right to complete the sentence on the left.",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            evaluable: true
        }
    },
    //13 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p> Before<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> since <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>When <\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>while<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>As soon as <\/p>",
                "t17correcta": "5"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": " the basketball match, the players were warming up. <br> The team had been training "
            },
            {
                "t11pregunta": " the beginning of the term for this match.<br> "
            },
            {
                "t11pregunta": " the teams were playing, one of the players got a muscle tear.<br> The referee stopped the game "
            },
            {
                "t11pregunta": " the paramedics came in with the stretcher.<br> "
            },
            {
                "t11pregunta": " they left the court, the game continued. "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag and drop the correct time expression to complete the text.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            evaluable: true
        }
    },
    //14 respuesta abierta
    {
        "respuestas": [
            {
                "t17correcta": "went"
            },
            {
                "t17correcta": "wore"
            },
            {
                "t17correcta": "rode"
            },
            {
                "t17correcta": "ordered"
            },
            {
                "t17correcta": "played"
            },
            {
                "t17correcta": "were"
            },
            {
                "t17correcta": "watched"
            },
            {
                "t17correcta": "had"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>Yesterday, my family and I &nbsp  "
            },
            {
                "t11pregunta": " to the park.  (go)<br> My sister  &nbsp her favorite pink jacket."
            },
            {
                "t11pregunta": " her favorite pink jacket. (wear)<br>We  &nbsp"
            },
            {
                "t11pregunta": " our bikes all morning. (ride)<br>We &nbsp"
            },
            {
                "t11pregunta": " pizza for lunch.  (order) <br>Then we &nbsp "
            },
            {
                "t11pregunta": " a board game. (play)<br>We &nbsp  "
            },
            {
                "t11pregunta": " at the dining room for about three hours. (be) <br>After the game, my parents&nbsp   "
            },
            {
                "t11pregunta": " a movie and we went to bed.  (watch)<br>We &nbsp    "
            },
            {
                "t11pregunta": " a really good time. (have)"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Type the correct form of the verb in parentheses.",
            evaluable: true,
            "pintaUltimaCaja": false,
        }
    }
    ,
    //15 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p>  once a week<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> once a year <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>three times a day<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p> twice<\/p>",
                "t17correcta": "4"
            },


        ],
        "preguntas": [
            {
                "t11pregunta": "My family and I have ridden our bikes "
            },
            {
                "t11pregunta": " since I was 7; we do it every Sunday. <br> My school has always held the Spelling Bee competition "
            },
            {
                "t11pregunta": ", in March.<br> Mom has always made sure we eat at least  "
            },
            {
                "t11pregunta": ", that is breakfast, lunch, and dinner.<br> Our team has won the Science Fair "
            },
            {
                "t11pregunta": " in a row; last year and the year before that. We want to win again. <br> "
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose the correct option to complete the sentence.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },


    //16 respuesta habierta
    {
        "respuestas": [
            {
                "t17correcta": "since"
            },
            {
                "t17correcta": "for"
            },
            {
                "t17correcta": "for"
            },
            {
                "t17correcta": "since"
            },
            {
                "t17correcta": "since"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "I have attended this school "
            },
            {
                "t11pregunta": "first grade. <br> We have been training"
            },
            {
                "t11pregunta": "six months now.<br>I haven’t been to the beach "
            },
            {
                "t11pregunta": "a year. <br>Teacher Bernardo looks happier"
            },
            {
                "t11pregunta": " we won the Science Fair prize.<br>We have known each other "
            },
            {
                "t11pregunta": " kindergarten.   "
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Complete the sentence with for or since.",
            evaluable: true,
            "pintaUltimaCaja": false,
        }
    }
    ,//17 Opcion Multiple
    {
      "respuestas": [
          {
              "t13respuesta": "was captured",
              "t17correcta": "1",
              "numeroPregunta": "0"
          }, {
              "t13respuesta": "he is captured",
              "t17correcta": "0",
              "numeroPregunta": "0"
          }, {
              "t13respuesta": "did capture",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "was revealed",
              "t17correcta": "1",
              "numeroPregunta": "1"
          }, {
              "t13respuesta": "revealed",
              "t17correcta": "0",
              "numeroPregunta": "1"
          }, {
              "t13respuesta": "were identified.",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "was imprisoned",
              "t17correcta": "1",
              "numeroPregunta": "2"
          }, {
              "t13respuesta": "imprisoned",
              "t17correcta": "0",
              "numeroPregunta": "2"
          }, {
              "t13respuesta": "was imprison",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "is pronounced",
              "t17correcta": "1",
              "numeroPregunta": "3"
          }, {
              "t13respuesta": "were pronounced",
              "t17correcta": "0",
              "numeroPregunta": "3"
          }, {
              "t13respuesta": "pronounced",
              "t17correcta": "0",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "were published",
              "t17correcta": "1",
              "numeroPregunta": "4"
          }, {
              "t13respuesta": "was published",
              "t17correcta": "0",
              "numeroPregunta": "4"
          }, {
              "t13respuesta": "it published",
              "t17correcta": "0",
              "numeroPregunta": "4"
          },



      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["<img src='EI6A_B01_A00_15.png' height='400px' ><br>Look at the newspaper announcement and complete the sentences that might appear on the article.<br><br>Today, a highly dangerous thief ________________.",
              "The police identified the thief and other suspects, after a set of fingerprints  ______.",
              "The crime perpetrator  __________ in the city jail to await his trial.",
              "The thief will remain at the city jail until a sentence  ______ by the judge.",
              "Many pictures of this thief ________ during the five years the police looked for him.",],
          "t11instruccion": "",
          "preguntasMultiples": true,
          evaluable: true,

      }
    },
//arrastra corta
    {
      "respuestas": [        
        {
           "t13respuesta":  "did",
             "t17correcta": "1"
         },
        {
           "t13respuesta":  "didn’t",
             "t17correcta": "2"
         },
        {
           "t13respuesta":  "asked",
             "t17correcta": "3"
         },
        {
           "t13respuesta":  "First,",
             "t17correcta": "4"
         },
        {
           "t13respuesta":  "and",
             "t17correcta": "5"
         },
    ],
    "preguntas": [
      {
      "t11pregunta": "<b>Peter:</b> Mom, "
      },
      {
      "t11pregunta": " you see my black sneakers?<br><b>Mom:</b> No dear, I "
      },
      {
      "t11pregunta": ".<br><b>Peter:</b> But last time I saw them you "
      },
      {
      "t11pregunta": " me to put them away.<br><b>Mom:</b> And did you?<br><b>Peter:</b> No, but they’re not here either.<br><b>Mom:</b> Well, then this is what  you should do: "
      },
      {
      "t11pregunta": " ask your sister if she put them away, "
      },
      {
      "t11pregunta": " thank her if she did.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Drag and drop the words where they best complete the following dialogue.",
       "t11instruccion": "",
       "respuestasLargas": true,
       "pintaUltimaCaja": false,
       "contieneDistractores": true
     }
    },
//relaciona columnas
{
  "respuestas":[
 {
  "t13respuesta":"we can win the tournament.",
  "t17correcta":"1",
  },
 {
  "t13respuesta":"a bit longer every afternoon.",
  "t17correcta":"2",
  },
 {
  "t13respuesta":"to win, but we scored at the last minute.",
  "t17correcta":"3",
  },
 {
  "t13respuesta":"persevering people.",
  "t17correcta":"4",
  },
 {
  "t13respuesta":"ready to win the next match.",
  "t17correcta":"5",
  },
 ],
  "preguntas": [
  {
  "t11pregunta":"If each of us trains hard,"
  },
 {
  "t11pregunta":"I think we should stay and train"
  },
 {
  "t11pregunta":"The match was quite difficult"
  },
 {
  "t11pregunta":"Athletes are very disciplined and"
  },
 {
  "t11pregunta":"It seems our team is"
  },
  ],
  "pregunta":{
  "c03id_tipo_pregunta":"12",
  "t11instruccion": "",
  "t11pregunta":"Match the columns to complete the sentences.",
  "altoImagen":"100px",
  "anchoImagen":"200px"
  }

},

    {
        "respuestas": [
            {
                "t13respuesta": "This is when someone feels very happy",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Spotless",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Ravenous",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "This is when someone is very rich",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "This is when someone feels very tired",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "This is when someone feels very nervous",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "This is when time is very short",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": '<img src="EI6E_B01_A00_07.png" >',
            },
            {
                "t11pregunta": '<img src="EI6E_B01_A00_08.png" >',
            },
            {
                "t11pregunta": '<img src="EI6E_B01_A00_09.png" >',
            },
            {
                "t11pregunta": '<img src="EI6E_B01_A00_10.png" >',
            },
            {
                "t11pregunta": '<img src="EI6E_B01_A00_11.png" >',
            },
            {
                "t11pregunta": '<img src="EI6E_B01_A00_12.png" >',
            },
            {
                "t11pregunta": '<img src="EI6E_B01_A00_13.png" >',
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the word in the picture to an equivalent expression.",
            "altoImagen": "100px",
            "anchoImagen": "200px"

        }
    },

    //18 Arrastra y corta
    {
        "respuestas": [
            {
                "t13respuesta": "<p>  jubilant <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> wealthy  <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>spotless <\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>anxious<\/p>",
                "t17correcta": "4"
            }, {
                "t13respuesta": "<p>brief <\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>exhausted! <\/p>",
                "t17correcta": "6"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Last weekend, my older sister celebrated her birthday. She was "
            },
            {
                "t11pregunta": " with all the preparations, thinking about a famous D.J. to play the music. She thought of a pool party, a bar of drinks and food, and waiters to attend the guests. I guess she suddenly felt as "
            },
            {
                "t11pregunta": " as Bill Gates. But Mom imposed a condition for her to hold the party: the whole house had to be "
            },
            {
                "t11pregunta": " by the weekend and this made my sister "
            },
            {
                "t11pregunta": ". She had a really "
            },
            {
                "t11pregunta": " time to do it all since, as you may have already guessed, we are not at all related to Mr. Gates. I had to help her out because there was too much to do and we all ended up "
            },
            {
                "t11pregunta": " There were not a D.J., nor a pool, but as you can see in the picture, my sister held her party as cool as any famous star."
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose the correct option to complete the sentence.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "t11instruccion": '<img src="EI6E_B01_A00_14.png" >',
        }
    },

    //19 opcion multiple
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EMy Sister’s Party Preparations \u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EMy Sister’s First Pool Party\u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EMy Sister’s Best Party Ever\u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "The best title for this paragraph is<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>."
        }
    },







];
