json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "False",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "True",
                "t17correcta": "1",
            },

        ], "preguntas": [

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Not many people visit Paris.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Countries like Japan and Korea honor death.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nico’s family loves traveling.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Anne’s parents aren’t  American.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pilar’s best friend is from Russia.",
                "correcta": "0",
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Listen to the audio and choose True or False for the following statements.",
            "t11instruccion": "",
            "descripcion": "Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }


    },

    //2

    {
        "respuestas": [
            {
                "t13respuesta": "Spain",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "French",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Italian",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "American",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Chinese",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "My name is Pilar Moya and I am from "
            },
            {
                "t11pregunta": ".<br>I am Alexandre Aguillard. I live in Mexico now, but I am "
            },
            {
                "t11pregunta": ".<br>My name is Nico Rossi and I am "
            },
            {
                "t11pregunta": ".<br>I am Anne Liang. I am "
            },
            {
                "t11pregunta": ".<br>My parents are "
            },
            {
                "t11pregunta": ", but they live in the United States too.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen again and drag the words in the boxes to complete the following sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true,
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Simon’s father left his glasses at home.</p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>Simon’s father asked for the toilet paper’s price.</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>Simon’s father asked for the sunglasses’ price.</p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
            {
                "t13respuesta": "<p>The sales clerk offered a more economic pair of sunglasses.</p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p>The sales clerk said the total was $22.</p>",
                "t17correcta": "4",
                etiqueta: "5"
            },
            {
                "t13respuesta": "<p>The sales clerk asked for an alternative payment method.</p>",
                "t17correcta": "5",
                etiqueta: "6"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Read the conversation. Put the following events in the order they ocurred.",
            "t11instruccion": "<b>Simon’s father:</b> Good morning! <br>\n\
            <b>Sales clerk:</b> Good morning! Can I help you? <br>\n\
            <b>Simon’s father:</b> Yes please, how much is that package of toilet paper? <br>\n\
            <b>Sales clerk:</b> We have packs with four and with six rolls. Which one do you need? <br>\n\
            <b>Simon’s father:</b> Four rolls, please. And how much are those sunglasses? I forgot mine. <br>\n\
            <b>Sales clerk:</b> The toilet paper is six dollars and the sunglasses are a hundred. <br>\n\
            <b>Simon’s father:</b> That’s ok. I will take the toilet paper. <br>\n\
            <b>Sales clerk:</b> We have these very cheap sunglasses. They are half the price. <br>\n\
            <b>Simon’s father:</b> No, that’s ok. How much are those chocolate bars? <br>\n\
            <b>Sales clerk:</b> They are two dollars each; how many do you want? <br>\n\
            <b>Simon’s father:</b> Let’s see, we are five, but the twins don’t like chocolate, so that will be three chocolate bars, a pack of toilet paper, two bottles of water and, I think, I will take the sunglasses after all. How much are they again? <br>\n\
            <b>Sales clerk:</b> They are ten dollars; your total adds up to twenty two dollars. <br>\n\
            <b>Simon’s father:</b> Here’s my credit card. <br>\n\
            <b>Sales clerk:</b> Sorry, our connection is down. Can you pay cash?<br>\n\
            ",
            "evaluable": true,

        }
    },
    //4
    {
        "respuestas": [
            {
                "t17correcta": "4"
            },
            {
                "t17correcta": "6"
            },
            {
                "t17correcta": "100"
            },
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "22"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "How many rolls of toilet paper does Simon’s father buy? "
            },
            {
                "t11pregunta": ". <br>How much does the toilet paper cost? "
            },
            {
                "t11pregunta": " dollars. <br>How much are the sunglasses that Simon’s father pointed at?  "
            },
            {
                "t11pregunta": " dollars. <br>How much are the chocolate bars? "
            },
            {
                "t11pregunta": " dollars. <br>How much will Simon’s father pay? "
            },
            {
                "t11pregunta": " dollars. <br>"
            },
            {
                "t11pregunta": " <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Type in the correct number.",
            "t11instruccion": "<b>Simon’s father:</b> Good morning! <br>\n\
            <b>Sales clerk:</b> Good morning! Can I help you? <br>\n\
            <b>Simon’s father:</b> Yes please, how much is that package of toilet paper? <br>\n\
            <b>Sales clerk:</b> We have packs with four and with six rolls. Which one do you need? <br>\n\
            <b>Simon’s father:</b> Four rolls, please. And how much are those sunglasses? I forgot mine. <br>\n\
            <b>Sales clerk:</b> The toilet paper is six dollars and the sunglasses are a hundred. <br>\n\
            <b>Simon’s father:</b> That’s ok. I will take the toilet paper. <br>\n\
            <b>Sales clerk:</b> We have these very cheap sunglasses. They are half the price. <br>\n\
            <b>Simon’s father:</b> No, that’s ok. How much are those chocolate bars? <br>\n\
            <b>Sales clerk:</b> They are two dollars each; how many do you want? <br>\n\
            <b>Simon’s father:</b> Let’s see, we are five, but the twins don’t like chocolate, so that will be three chocolate bars, a pack of toilet paper, two bottles of water and, I think, I will take the sunglasses after all. How much are they again? <br>\n\
            <b>Sales clerk:</b> They are ten dollars; your total adds up to twenty two dollars. <br>\n\
            <b>Simon’s father:</b> Here’s my credit card. <br>\n\
            <b>Sales clerk:</b> Sorry, our connection is down. Can you pay cash?<br>\n\
            ",
            evaluable: true,
            pintaUltimaCaja: false,
            "evaluable": true,
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "My",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "I",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "his",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "our",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "They",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "their",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "we",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "its",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": " name is Theo, today is the first day of school. "
            },
            {
                "t11pregunta": " am studying secondary school.<br>Our Math teacher is very strict but "
            },
            {
                "t11pregunta": " classes are very good.<br>Daniel and I are best friends and "
            },
            {
                "t11pregunta": " favorite sport is basketball.<br>Carla and Ulises love chess. "
            },
            {
                "t11pregunta": "’re getting ready to start "
            },
            {
                "t11pregunta": " practices for this year’s contest.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the correct option to complete the sentences. There are some extra options that you will not need to use.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true,
        }
    },
    //6
    {
        "respuestas": [
            {
                "t17correcta": "are"
            },
            {
                "t17correcta": "Are"
            },
            {
                "t17correcta": "are"
            },
            {
                "t17correcta": "is"
            },
            {
                "t17correcta": "am"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "My brother and sister "
            },
            {
                "t11pregunta": " very good at sports. <br>"
            },
            {
                "t11pregunta": " Ulises and Carla good in chess? <br>My best friend and I "
            },
            {
                "t11pregunta": " in class today. <br>My father "
            },
            {
                "t11pregunta": " a very good writer. He writes for a newspaper. <br>I "
            },
            {
                "t11pregunta": " in Mr. Jensen’s class. <br>"
            },
            {
                "t11pregunta": " <br>"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Complete the sentences with the correct form of verb be.",
            "t11instruccion": "",
            "evaluable": true,
            "pintaUltimaCaja": false,
            mayusculas: true, 
            acentos: true,
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "this",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "these",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "those",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "that",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "these",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "those",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },

            {
                "t13respuesta": "these",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "that",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "this",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },

            {
                "t13respuesta": "Those",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "These",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "That",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },

            {
                "t13respuesta": "this",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "these",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "those",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Choose the correct option to complete the sentence.<br><br>Write your name down in<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> survey and give it back to me.",
                "How much is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> poster on the wall?",
                "Daniel, can you put<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> notebooks on my desk, please?",
                "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> students over there are from Canada.",
                "The contest begins<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> week."],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "How much is",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "How much are",
                "t17correcta": "1",
            },

        ], "preguntas": [

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>those glasses?",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>the map?",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>this camera?",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>the batteries?",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>it for everything?",
                "correcta": "0",
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose the correct form to start these questions.",
            "t11instruccion": "",
            "descripcion": " Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }

    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "is your best friend?",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "old are you?",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "is her name?",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "is your favorite class?",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "is your birthday?",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Who"
            },
            {
                "t11pregunta": "How"
            },
            {
                "t11pregunta": "What"
            },
            {
                "t11pregunta": "Which"
            },
            {
                "t11pregunta": "When"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the columns to complete the questions.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true,
        }

    },
    //10
    {
        "respuestas": [
            {
                "t17correcta": "studying"
            },
            {
                "t17correcta": "training"
            },
            {
                "t17correcta": "raining"
            },
            {
                "t17correcta": "having"
            },
            {
                "t17correcta": "marking"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "I am "
            },
            {
                "t11pregunta": " because I have an exam tomorrow.    (study) <br>Is she "
            },
            {
                "t11pregunta": " for the next contest?    (train) <br>It is not "
            },
            {
                "t11pregunta": " today. You don’t need the umbrella.   (rain) <br>The students are "
            },
            {
                "t11pregunta": " lunch at the cafeteria.   (have) <br>Our teacher is "
            },
            {
                "t11pregunta": " our exams at the teacher’s room.    (mark) <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Type the correct form of the verb in parentheses to complete the sentences.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true,
            "acentos": false,
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Correct",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "1",
            },

        ], "preguntas": [

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Paul’s Mary’s husband",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tom’s Beth’s daughter",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Kate’s Tom’s brother",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "John’s Kate’s son",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Jay’s Jane’s cousin",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Joe’s Jane’s cousin",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Determine if the following statements are correct or incorrect.",
            "t11instruccion": "<img src='EI7E_B01_A00_00.png' height='700' >",
            "descripcion": " Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "in the hallway.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "voice in the library.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "the shore because you can fall.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "things, the class is over.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "before you have lunch.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Do not run"
            },
            {
                "t11pregunta": "Keep a low"
            },
            {
                "t11pregunta": "Stay away from"
            },
            {
                "t11pregunta": "Put away your"
            },
            {
                "t11pregunta": "Wash your hands"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match the columns to complete the sentences.",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true,
        }

    },
    //13
     {
            "respuestas": [        
                {
                   "t13respuesta":  "have",
                     "t17correcta": "1" 
                 },
                {
                   "t13respuesta":  "say",
                     "t17correcta": "2" 
                 },
                {
                   "t13respuesta":  "are not",
                     "t17correcta": "3" 
                 },
                {
                   "t13respuesta":  "feel",
                     "t17correcta": "4" 
                 },
                {
                   "t13respuesta":  "explains",
                     "t17correcta": "5" 
                 },
         {
                       "t13respuesta":  "saying",
                         "t17correcta": "6" 
                     },
             {
                           "t13respuesta":  "explain",
                             "t17correcta": "7" 
                         },
                 {
                               "t13respuesta":  "feels",
                                 "t17correcta": "8" 
                             },
    ],
    "preguntas": [ 
      {
      "t11pregunta": "Math is my favorite subject. We "
      },
      {
      "t11pregunta": " math lessons on Tuesday and Thursday morning. Teachers "
      },
      {
      "t11pregunta": " that this timing is perfect for us because we "
      },
      {
      "t11pregunta": " tired then, but I always "
      },
      {
      "t11pregunta": " sleepy at that time, anyway. Our teacher is very good. He "
      },
      {
      "t11pregunta": " patiently as many times as we need.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Drag the words to complete the paragraph. There are some words you don’t need.", 
       "t11instruccion": "", 
       "respuestasLargas": true, 
       "pintaUltimaCaja": false, 
       "contieneDistractores": true 
     } 
  },
//14
{ 
    "respuestas":[ 
   { 
    "t13respuesta":"I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> take a shower in the mornings or I can’t wake up!", 
    "t17correcta":"1", 
    }, 
   { 
    "t13respuesta":"I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> wake up at 6:00 in the morning, except on weekends.", 
    "t17correcta":"2", 
    }, 
   { 
    "t13respuesta":"I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> have cereal for breakfast or scrambled eggs, it depends on what Mom cooks.", 
    "t17correcta":"3", 
    }, 
   { 
    "t13respuesta":"I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> have rough eggs. I don’t like them at all.", 
    "t17correcta":"4", 
    }, 
   ], 
    "preguntas": [ 
    { 
    "t11pregunta":"always" 
    }, 
   { 
    "t11pregunta":"usually" 
    }, 
   { 
    "t11pregunta":"sometimes" 
    }, 
   { 
    "t11pregunta":"never" 
    }, 
    ], 
    "pregunta":{ 
    "c03id_tipo_pregunta":"12",
    "t11pregunta":"Match a frequency adverb to a sentence", 
    "t11instruccion": "", 
    "altoImagen":"100px", 
    "anchoImagen":"200px" 
    } 
    },
//15
    {
        "respuestas": [
            {
                "t13respuesta": "salt and sugar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "eggs",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "olive",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "bowl",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Pour the mix",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "cook",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Turn over",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "milk",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "cooking",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "turning over",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Ingredients: Two cups of flour, "
            },
            {
                "t11pregunta": " to taste, two "
            },
            {
                "t11pregunta": ", a teaspoon of baking soda, two tablespoons of "
            },
            {
                "t11pregunta": " oil, half a glass of warm water and a glass of milk.<br>Procedure: Mix all ingredients inside a "
            },
            {
                "t11pregunta": ". Knead the mixture thoroughly. Brush a nonstick saucepan and heat over medium heat. "
            },
            {
                "t11pregunta": " into a heated saucepan and "
            },
            {
                "t11pregunta": " until the top is full of bubbles. "
            },
            {
                "t11pregunta": " and cook until golden, about two minutes.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the correct option to complete the recipe.<br><br>",
            "t11instruccion": "<center><img src='EI7E_B01_A00_01.png' height='700'  width='700'></center>",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true,
        }
    },


];
