json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Dana",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sheppard",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "456",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "885",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "thebigcity",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Diane",
                "t17correcta": ""
            },
            {
                "t13respuesta": "Sheffield",
                "t17correcta": ""
            },
            {
                "t13respuesta": "645",
                "t17correcta": ""
            },
            {
                "t13respuesta": "inmycity",
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<center><br>Name: "
            },
            {
                "t11pregunta": " James<br>Address: 25 "
            },
            {
                "t11pregunta": " Street.<br>Postal code: STF "
            },
            {
                "t11pregunta": "<br>Phone: 204-"
            },
            {
                "t11pregunta": "-907<br>email: dana123@"
            },
            {
                "t11pregunta": ".com<br></center>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen to Daniel and Dana and drag the options to fill out the form with the missing information. There are some options that you will not need to use.",
            "t11instruccion": "<b>Daniel:</b> Hey, Dana! Good morning, my name is Daniel and I am a member of the welcome committee. This is Carla.<br> \n\
            <b>Carla:</b> Hello Dana, it is so good to have you here. Welcome to Brooklyn Junior High!<br> \n\
            <b>Daniel:</b> We’re here to guide you and help you through your first day. I am sure you will love the school and your classmates. We have to give you your new material, but first we need to fill in a questionnaire to label your new stuff. What is your last name?<br> \n\
            <b>Dana:</b> My last name is James. J- A-M-E-S<br> \n\
            <b>Daniel:</b> Ok, and what is your address, Dana?<br> \n\
            <b>Dana:</b> I live at 25 Sheppard Street. My postal code is STF456.<br> \n\
            <b>Daniel:</b> Great! And what is your telephone number and email?<br> \n\
            <b>Dana:</b>  My phone number is 204 - 885 - 907.<br> \n\
            <b>Daniel:</b> Sorry, I didn’t understand. Can you repeat that, please?<br> \n\
            <b>Dana:</b> Oh, sorry! I… I…. I always speak fast when I am anxious. 204 - 885 - 907. My email is dana123@thebigcity.com<br> \n\
            <b>Carla:</b> That’s all Dana. Let me print you a student card and please take your backpack, your books and notebooks from this box. You can find pencils and pens inside every classroom. Please, go with Daniel so that he can give you your tablet and show you the campus.<br> \n\
            ",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "No",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Yes",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Doesn’t say",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Daniel is in charge of welcoming Carla.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "It is Dana’s first day in Brooklyn Junior High.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dana lives in London.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dana is anxious at the moment.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Carla is going to print Dana’s student card.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dana needs to buy pens and pencils for every class.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Listen again. Choose “Yes,” “No” or “Doesn’t say” if there isn’t enough information to determine if the statement is true or false.",
            "descripcion": "Statement",
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "that",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "the",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "quality",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "her",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "to",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "such",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "on",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "techniques",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "teaching",
                "t17correcta": "9,10"
            },
            {
                "t13respuesta": "teaching",
                "t17correcta": "9,10"
            },
            {
                "t13respuesta": "who",
                "t17correcta": ""
            },
            {
                "t13respuesta": "quantity",
                "t17correcta": ""
            },
            {
                "t13respuesta": "enormous",
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Studies show "
            },
            {
                "t11pregunta": " a very important factor that determines "
            },
            {
                "t11pregunta": " quality of the education a child receives is the "
            },
            {
                "t11pregunta": " of his or "
            },
            {
                "t11pregunta": " teacher. Great teachers are not easy "
            },
            {
                "t11pregunta": " find, and they must have many important qualities, "
            },
            {
                "t11pregunta": " as great knowledge "
            },
            {
                "t11pregunta": " a variety of topics, enthusiasm, a positive attitude, classroom management "
            },
            {
                "t11pregunta": ", and, of course, a huge love for learning and "
            },
            {
                "t11pregunta": ", as well as a "
            },
            {
                "t11pregunta": " desire to make a difference in the lives of his or her students.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Read the text and drag the options to the blanks.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "decided without much consideration.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "was forced to do.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "opposed to at first.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "the damage meat producing does to the environment.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "the art used in the documentary she saw.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "the benefits it brings to her health.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "requires land and water.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "is cruel.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "requires a lot of time and effort.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "the animals digestive system.",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "greenhouse gasses.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "CO2.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "More than half",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "A half",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "1.5 degrees",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "contributes to global warming.",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "is healthy.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "is a difficult thing to do.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Read the text carefully and choose the option that best completes the sentences below.<br><br>Becoming a vegetarian is something Jane",
                "The main reason for Jane to become a vegetarian is",
                "Jane’s main argument is that breeding cattle",
                "Methane is produced by",
                "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> of greenhouse gasses come from meat production.",
                "Jane is convinced that eating meat",],
            "t11instruccion": "In this week’s showcase interview, we interview Jane Hollingworth, self-confessed hamburger addict, to find out why she became a veggie.<br><br><strong>Showcase:</strong> So, Jane, why did you decide to turn vegetarian?<br><br><strong>Jane:</strong> Well, really, it wasn’t something I had thought of doing before. I actually loved meat. I probably ate hamburgers about three times a week. But then I saw a documentary on the Internet about the damage that eating meat does to the environment.<br><br><strong>Showcase:</strong> Oh, really? How does meat damage the environment?<br><br><strong>Jane:</strong> It affects the environment in many ways. Firstly, land is cleared to make way for cattle and other animals. Much of the Amazon rainforest that has disappeared has been cut down for this very reason. These animals need a lot of space in which to graze. Secondly, livestock produce a lot of methane gas from their digestive systems. Methane is a very powerful greenhouse gas, much more potent than CO2. In this way, when we eat meat, we are contributing to global warming because we are encouraging farmers to raise more animals and the methane gas they produce goes into the atmosphere and makes the planet warmer. Another way in which raising livestock affects the environment is in the amount of water used. These farms use a lot of water because the animals need to drink, and it is also used in other processes of meat production. And these meat processing plants heavily pollute nearby rivers and lakes. Finally, the transportation of the animals and the meat coming out of processing plants contributes to global warming as trucks and airplanes travel long distances, sometimes internationally, and the combustion engines used produce carbon dioxide.<br><br><strong>Showcase:</strong> And just how big is this problem?<br><br><strong>Jane:</strong> Well, according to research carried out in 2016 by the Oxford Martin School in England, meat production accounts for 63% of greenhouse gas emissions coming from our food. And food production accounts for about half of total greenhouse emissions worldwide. So, eating meat is an enormous contributor to global warming. If we all ate less meat, we would be helping our countries reach our targets under the Paris Climate agreement to make sure the global average temperature does not rise 1.5 degrees above pre-industrial levels.<br><br><strong>Showcase:</strong> And was becoming vegetarian difficult?<br><br><strong>Jane:</strong> Actually, it was really easy. There are so many delicious dishes available for vegetarians. This has improved a lot in recent years. I normally have a lot of fruit and cereals for breakfast. Then for lunch I often have a pasta dish with lots of different types of vegetables. Or I might have a soup or a salad. Then, for dinner, I have a rice dish with lots of different kinds of vegetables, or maybe something more elaborate, like a vegetarian lasagne. ",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "have to",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "want to",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "like to",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "don’t have to",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "don’t want to",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "doesn’t like to",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "have to",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "prefers to",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "forgot to",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "have to stop",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "prefer to go",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "wants to come",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "have to",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "liked to",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "choose to",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the options and choose the one that completes the sentence.<br><br>To enter the contest we <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> subscribe on the webpage.",
                "We <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> pay the fee until the following month.",
                "To complete the project, we <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> write a conclusion.",
                "When the traffic light is red, you <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
                "We <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> be three hours early at the airport.",],
                "t11instruccion": "In this week’s showcase interview, we interview Jane Hollingworth, self-confessed hamburger addict, to find out why she became a veggie.<br><br><strong>Showcase:</strong> So, Jane, why did you decide to turn vegetarian?<br><br><strong>Jane:</strong> Well, really, it wasn’t something I had thought of doing before. I actually loved meat. I probably ate hamburgers about three times a week. But then I saw a documentary on the Internet about the damage that eating meat does to the environment.<br><br><strong>Showcase:</strong> Oh, really? How does meat damage the environment?<br><br><strong>Jane:</strong> It affects the environment in many ways. Firstly, land is cleared to make way for cattle and other animals. Much of the Amazon rainforest that has disappeared has been cut down for this very reason. These animals need a lot of space in which to graze. Secondly, livestock produce a lot of methane gas from their digestive systems. Methane is a very powerful greenhouse gas, much more potent than CO2. In this way, when we eat meat, we are contributing to global warming because we are encouraging farmers to raise more animals and the methane gas they produce goes into the atmosphere and makes the planet warmer. Another way in which raising livestock affects the environment is in the amount of water used. These farms use a lot of water because the animals need to drink, and it is also used in other processes of meat production. And these meat processing plants heavily pollute nearby rivers and lakes. Finally, the transportation of the animals and the meat coming out of processing plants contributes to global warming as trucks and airplanes travel long distances, sometimes internationally, and the combustion engines used produce carbon dioxide.<br><br><strong>Showcase:</strong> And just how big is this problem?<br><br><strong>Jane:</strong> Well, according to research carried out in 2016 by the Oxford Martin School in England, meat production accounts for 63% of greenhouse gas emissions coming from our food. And food production accounts for about half of total greenhouse emissions worldwide. So, eating meat is an enormous contributor to global warming. If we all ate less meat, we would be helping our countries reach our targets under the Paris Climate agreement to make sure the global average temperature does not rise 1.5 degrees above pre-industrial levels.<br><br><strong>Showcase:</strong> And was becoming vegetarian difficult?<br><br><strong>Jane:</strong> Actually, it was really easy. There are so many delicious dishes available for vegetarians. This has improved a lot in recent years. I normally have a lot of fruit and cereals for breakfast. Then for lunch I often have a pasta dish with lots of different types of vegetables. Or I might have a soup or a salad. Then, for dinner, I have a rice dish with lots of different kinds of vegetables, or maybe something more elaborate, like a vegetarian lasagne. ",
                "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "because",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "in spite of",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "and",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "but",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "due to",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "or",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Since",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "an",
                "t17correcta": ""
            },
            {
                "t13respuesta": "with",
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "I couldn’t go to the party "
            },
            {
                "t11pregunta": " I wasn’t in the city.<br>The visitors couldn’t defeat our team "
            },
            {
                "t11pregunta": " their effort.<br>I love to have ice cream "
            },
            {
                "t11pregunta": " apple pie in the same serving.<br>They want to visit the National Museum "
            },
            {
                "t11pregunta": " it is closed.<br>The museum is closed "
            },
            {
                "t11pregunta": " a remodeling phase.<br>Would you like to have water "
            },
            {
                "t11pregunta": " tea?<br>"
            },
            {
                "t11pregunta": " she was sick, she had to stay in bed all day.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Read the sentences and drag the words to complete them.  There are two extra options which you will not need to use.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "if they had arrived earlier.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "I would have gotten a place in the concert.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "if he hadn’t felt ashamed of his answer.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "I would’ve waited for you.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "if he had performed in that big stage.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "They wouldn’t have missed the flight"
            },
            {
                "t11pregunta": "If I had reserved a ticket in advance,"
            },
            {
                "t11pregunta": "He would have wanted to try again"
            },
            {
                "t11pregunta": "If I’d known you were coming,"
            },
            {
                "t11pregunta": "He would have been very nervous"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "Match the columns to complete the sentences.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "who",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "such",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "which",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "so",
                "t17correcta": "4"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "The man "
            },
            {
                "t11pregunta": " is standing behind my mother is my uncle.<br>I never thought you had "
            },
            {
                "t11pregunta": " patience with children.<br>The movie "
            },
            {
                "t11pregunta": " will be released is the beginning of a thrilogy.<br>The rescued dog was "
            },
            {
                "t11pregunta": " tired, he slept all day.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Add the words to their correct place and complete the sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "who speaks",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "as easy",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "such",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "who is",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "so shy that",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "so",
                "t17correcta": ""
            },
            {
                "t13respuesta": "that speak",
                "t17correcta": ""
            },
            {
                "t13respuesta": "so easy",
                "t17correcta": ""
            },
            {
                "t13respuesta": "which is",
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Anne, "
            },
            {
                "t11pregunta": " German, is translating for the new student.<br>This task isn’t  "
            },
            {
                "t11pregunta": "  as it might look.<br>It has been "
            },
            {
                "t11pregunta": " a long time since she last used German.<br>Her new roommate, "
            },
            {
                "t11pregunta": " Austrian, has recently come to the United States.<br>She is  "
            },
            {
                "t11pregunta": " she is afraid to use English.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the options to their correct place and complete the sentences. There are four options that you will not need to use.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "to go",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "going",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "go",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "to postpone",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "postponed",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "postponing",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "to go",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "goes",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "went",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "to study",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "studying",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "studied",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "to be",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "is",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "am",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Read the sentence and choose the correct option to complete it.<br><br>We wanted <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> to the amusement park. ",
                "Our parents preferred <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> the trip due to the bad weather. ",
                "We are planning <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> to the beach next summer. ",
                "Our teacher reminded us <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> for the test. ",
                "The doctor advised me <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> careful with my cast. ",],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "to start",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "glad",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "about",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "to touch",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "on",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "happy",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "losing",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "of",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Are you ready "
            },
            {
                "t11pregunta": " the exam?<br>I’m "
            },
            {
                "t11pregunta": " to see you again.<br>You apologized "
            },
            {
                "t11pregunta": " being late.<br>She is afraid "
            },
            {
                "t11pregunta": " the lab mouse.<br>We insisted "
            },
            {
                "t11pregunta": " talking to the coach about the next game.<br>My mother is "
            },
            {
                "t11pregunta": " to see my sister dance.<br>They are afraid of "
            },
            {
                "t11pregunta": " the final contest.<br>I am tired "
            },
            {
                "t11pregunta": " going from one building to the other.<br>"
            },
            {
                "t11pregunta": "follow the instructions carefully. <br><br> Writing:<br>\n\
                <p>In your notebook, write an entry for an international student blog. You have to describe your city because you want students to visit it in summer. \n\
                <br>Your entry must include:\n\
                <li>A catchy title referring to your city</li>\n\
                <li>A description of your city</li>\n\
                <li>A list of places of interest</li>\n\
                <li>Some activities you can do</li>\n\
                <li> A convincing statement to visit your city</li> \n\ </p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the words to their correct place to complete the sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //12
    {
        "respuestas": [
          {
              "t13respuesta": "announced",
              "t17correcta": "1",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "announcing",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "announce",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
            {
                "t13respuesta": "had been",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "was",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "going to be",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "acknowledged",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "acknowledges",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "acknowledge",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "that",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "what",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "for",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "confirmed",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "was confirm",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "assure",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["<img src='EIBA_B01_A00_01.png' height='400'><br>The City Mayor ______ that the families were rescued.","The Fire Department confirmed that several pets ________ reunited with their families.",
            "The media was ______ for the great support provided to the community.","The chief of the Police Department reported ____ some robberies have been registered in the District.",
            "The staff in the community hospital _____ that there were enough resources to attend the victims."],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //reactivo 13
    {
      "respuestas": [
          {
              "t13respuesta": "do",
              "t17correcta": "0"
          },
          {
              "t13respuesta": "make",
              "t17correcta": "1"
          }

      ],
      "preguntas": [
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Could you ____ me a favor?",
              "correcta": "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "My mother is really strict. I have to _____ my bed on a daily basis.",
              "correcta": "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "I know I have to _____ an effort to bring my grades up this term.",
              "correcta": "1"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "What do you want to ____ on your birthday?",
              "correcta": "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "You look fit! Do you ____ Zumba or anything?",
              "correcta": "0"
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "What can I _____ for you?",
              "correcta": "0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "Select the correct verb (make or do) to fill in the space in the sentences.",
          "descripcion": "Statement",
          "variante": "editable",
          "anchoColumnaPreguntas": 40,
          "evaluable": true
      }
    },

];
