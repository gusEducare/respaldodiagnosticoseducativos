json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='EI3A_B1_R01_01.png'/>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='EI3A_B1_R01_02.png'/>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='EI3A_B1_R01_03.png'/>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='EI3A_B1_R01_04.png'/>",
                "t17correcta": "4,5",
            },
            {
                "t13respuesta": "<img src='EI3A_B1_R01_05.png'/>",
                "t17correcta": "4,5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Speaker 1: Lucy"
            },
            {
                "t11pregunta": "Speaker 2: Mary"
            },
            {
                "t11pregunta": "Speaker 3: Tommy"
            },
            {
                "t11pregunta": "Speaker 4: Andrew"
            },
            {
                "t11pregunta": "Speaker 4: Andrew"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Listen and match the image with the speaker.",
            // "t11instruccion": "<b>PART 1:</b><br>\n\
            // Girl 1: Lucy<br>\n\
            // Girl 2: Mary<br>\n\
            // Boy 1: Tommy<br>\n\
            // Boy 2: Andrew<br>",
            "altoImagen": "150px",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Leo",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Daniel",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Aurora",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, /////////////////////////////////////
            {
                "t13respuesta": "He is 4 years old.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "He is 10 years old.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "He is 8 years old.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, ///////////////////////
            {
                "t13respuesta": "He’s 7 years old.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "He’s 8 years old",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "He’s 10 years old.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, ////////////////////////////////////////
            {
                "t13respuesta": "He has no one.",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "He has 3.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "He has 1.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, /////////////////////////////////////
            {
                "t13respuesta": "Their family.",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Their toys.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Their pets.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Listen again and choose the option that best answers the question. \n\
 <br><br>What name does Lucy’s father like?", "How old is Tony?", "How old is Andrew?\n\
 ", "How many brothers does Tommy have?", "What are the kids describing?"],
            "t11instruccion": "",
            "preguntasMultiples": true,
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Playground",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "locker",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Flamingoes",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Sunday",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Brownies",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Hopscotch",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "You play on it during  recess.  "
            },
            {
                "t11pregunta": "<br>You put your things  in a  "
            },
            {
                "t11pregunta": " at school.<br>They have bright pink feathers on their bodies.  "
            },
            {
                "t11pregunta": "<br>This is a day when people rest. "
            },
            {
                "t11pregunta": ".<br>You can eat them with a hot chocolate.  "
            },
            {
                "t11pregunta": "<br>You draw it on the floor and jump on it.  "
            },
            {
                "t11pregunta": "<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the words to the description.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Miriam:  I’m drawing a flower.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Miriam:  I’m fine thanks.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Miriam: I’m in the backyard.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, /////////////////////////////
            {
                "t13respuesta": "Miguel:  All kinds of animals.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Miguel:  I like to eat pizza.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Miguel:  I like your drawing.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, ///////////////////////////////////////////
            {
                "t13respuesta": "Miriam: My mom did.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Miriam: I have to.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Miriam: In school.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, /////////////////////////////////////////////////
            {
                "t13respuesta": "Miguel: Red and blue",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Miguel:  Yes, it's over there, thanks.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Miguel: Mine is orange.",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, ///////////////////////////////////////////////////////
            {
                "t13respuesta": "Miriam: Yes, please.",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Miriam: Yes, it's over there, thanks.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Miriam: Yes, it was.",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Read the question and choose the best response.<br><br> \n\
 Miguel: What are you drawing?\n\
 ", "Miriam: What do you like drawing?\n\
 ", "Miguel: Who taught you how to draw?", "Miriam:  What are your favorite colors?\n\
 ", "Miguel:  Do you need help with your drawing?"],
            "t11instruccion": "<center><img src='EI3A_B1_R03.png' width=480px height=280px/></center>",
            "preguntasMultiples": true,
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Yes",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "No",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, ////////////////////////
            {
                "t13respuesta": "No",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Yes",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, ////////////////////////
            {
                "t13respuesta": "No",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Yes",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },/////////////////////
            {
                "t13respuesta": "No",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Yes",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },////////////////////////
            {
                "t13respuesta": "No",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Yes",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the picture and choose “Yes” or “No”.  <br><br>  It’s a sunny day.\n\
 ", "The woman on the pink mat is sleeping \n\
 ", "The girl is riding a black bike.  \n\
 ", "The boy near the lake is flying a kite.\n\
 ", "The boy playing with the ball is wearing a blue cap. "],
            "t11instruccion": "<center><img src='EI3A_B1_R08.png' width=600px height=480px/></center>",
            "preguntasMultiples": true,
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "EI3A_B1_R09_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "EI3A_B1_R09_03.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "EI3A_B1_R09_04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "EI3A_B1_R09_05.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "EI3A_B1_R09_06.png",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "EI3A_B1_R09_07.png",
                "t17correcta": "5",
                "columna": "0"
            },
            {
                "t13respuesta": "EI3A_B1_R09_08.png",
                "t17correcta": "6",
                "columna": "0"
            },
            {
                "t13respuesta": "EI3A_B1_R09_09.png",
                "t17correcta": "7",
                "columna": "0"
            },
            {
                "t13respuesta": "EI3A_B1_R09_10.png",
                "t17correcta": "8",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": " Drag the word to the corresponding image.",
            "tipo": "ordenar",
            "imagen": true,
            "url": "EI3A_B1_R09_01.png",
            "respuestaImagen": true,
            "tamanyoReal": false,
            "anchoImagen": 60,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["", "140,37", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "140,211", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "140,400", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "295,37", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "295,211", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "295,400", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "470,37", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "470,211", "cuadrado", "110, 40", ".", "transparent"] },
            { "Contenedor": ["", "470,400", "cuadrado", "110, 40", ".", "transparent"] }
        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "m",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "o",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "t",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "h",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "e",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "r",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "w",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "n",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center>"
            },
            {
                "t11pregunta": " "
            },
            {
                "t11pregunta": " "
            },
            {
                "t11pregunta": " "
            },
            {
                "t11pregunta": " "
            },
            {
                "t11pregunta": " "
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Unscramble the letters to form words.\n\
 There are two extra letters which you will not need to use.\n\
 <br><br><center><img src='EI3A_B1_R10.png' width=200px height=200px/></center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "m",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "o",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "u",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "t",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "h",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "e",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "d",
                "t17correcta": "7"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Unscramble the letters to form words.\n\
 There are two extra letters which you will not need to use.\n\
 <br><br><center><img src='EI3A_B1_R11.png' width=200px height=200px/></center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "w",
                "t17correcta": "1,6"
            },
            {
                "t13respuesta": "i",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "n",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "d",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "o",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "w",
                "t17correcta": "6,1"
            },
            {
                "t13respuesta": "a",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "v",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Unscramble the letters to form words.\n\
 There are two extra letters which you will not need to use.\n\
 <br><br><center><img src='EI3A_B1_R12.png' width=200px height=200px/></center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "p",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "o",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "s",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "t",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "e",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "r",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "c",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "u",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Unscramble the letters to form words.\n\
 There are two extra letters which you will not need to use.\n\
 <br><br><center><img src='EI3A_B1_R13.png' width=200px height=200px/></center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "S",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "o",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "c",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "k",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "s",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "u",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "h",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Unscramble the letters to form words.\n\
 There are two extra letters which you will not need to use.\n\
 <br><br><center><img src='EI3A_B1_R14.png' width=200px height=200px/></center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "chameleon",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "colors",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "tongue",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "crickets",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "egg",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "trees",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "I live in Africa. They call me "
            },
            {
                "t11pregunta": ".  When we are five months old, we can change "
            },
            {
                "t11pregunta": ". I turn dark brown or black when stressed. But if I am happy, my skin becomes lighter.<br><br>I have a long and sticky "
            },
            {
                "t11pregunta": " that helps me catch my food.<br>I eat "
            },
            {
                "t11pregunta": ", grasshoppers, mantids, and even insects, among others.<br><br>Some of us are born from a mother, not from an "
            },
            {
                "t11pregunta": ". We make good pets if we live in captivity, but in the wild, we live in "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Read the story. Drag a word from the options given and put it in the space provided. Next, choose the best name for the story. ",
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "What You Need to Know About Chameleons",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Things to Consider About Choosing a Pet",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Wild Chameleons Can Be Great Pets",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Select the best name of the previous text.",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "do",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "is",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "eat",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, /////////////////////////////////////
            {
                "t13respuesta": "Who",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "What",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "How",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, /////////////////////////////////////////
            {
                "t13respuesta": "like",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "be",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "look",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, //////////////////////////////////////
            {
                "t13respuesta": "is",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "are",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "am",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, ///////////////////////////////////
            {
                "t13respuesta": "are",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "is",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "have",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Read the texts. Choose the right word to complete the sentences from the options given. \n\
   <br><br>What kind of food <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>\n\
    you like? Perhaps you love pizza or pasta.", "<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> cooks for you at home? Can you imagine a droid preparing the food for you? \n\
    ", "How would you <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> \n\
    to have your food prepared by a droid? Well,\n\
     maybe not prepared but delivered.", "This <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> no science fiction anymore. Droids actually do the work for you.\n\
     ", "There <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> some companies that have food delivery services. You can say goodbye to the pizza boy and hello to droids!"],
            "preguntasMultiples": true,
        }
    },


    {
        "respuestas": [
            {
                "t13respuesta": "second",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "first",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "third",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, ///////////////////////////////////////////

            {
                "t13respuesta": "closed",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "open",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "far",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, ////////////////////////////////////////////////
            {
                "t13respuesta": "elephants",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "cubs",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "photographs",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, //////////////////////////////////////
            {
                "t13respuesta": "tusks",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "cubs",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "manes",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, ////////////////////////////////////
            {
                "t13respuesta": "sunny",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "hot",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "amazing",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Look at the pictures and read the story. Choose the missing word(s) to fill in the spaces.\n\
   <br><br> The children are in <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> grade.\n\
   ", "When the kids get to the zoo it is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.\n\
   ", "They saw the <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> and then the lions.\n\
   ", "The elephant big teeth are called <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.\n\
   ", "The kids are wearing caps because the day is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>."],
            "t11instruccion": "<center><img src='EI3A_B1_R21.png' width=480 height=300px/></center>Today is a special day for the children in second grade. \n\
 It’s the day they go to the zoo. Miss Julia wants them to know more about the animals. They arrive right before the zoo opens. \n\
 They are ready to see the animals and live this marvelous experience. They make two lines, boys and girls, and they are wearing caps because it’s a sunny day. \n\
 First, they go see the elephants. What do you call those big teeth elephants have? Those are the elephants’ tusks.\n\
  Lucy and her friends like the elephants. The teacher takes some photographs with the elephants at the back. These photos are amazing.\n\
   Then, they go to see the lions. Lucy is curious, there is only one lion with mane. Why is that? Miss Julia explains.\n\
    Those other lions are not male, they are female lions. And how do you call those baby lions? Those are cubs. This lion family is big!",
            "preguntasMultiples": true,
        }
    },
];
