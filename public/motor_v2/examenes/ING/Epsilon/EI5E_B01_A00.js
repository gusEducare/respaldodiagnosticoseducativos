json = [
   {
        "respuestas": [
            {
                "t13respuesta": "1900s",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "1970s",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "2010s",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "afro",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "century",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Men in the "
            },
            {
                "t11pregunta": " wore their hair slick and short.<br>People in the "
            },
            {
                "t11pregunta": " were influenced by the hippie culture.<br>People on the "
            },
            {
                "t11pregunta": " don’t follow a specific style.<br>An "
            },
            {
                "t11pregunta": " look is having curly hair.<br>Hairstyles have changed in the last "
            },
            {
                "t11pregunta": " .<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen to the audio and drag and drop the option that best completes the sentence.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<p>The bob haircut becomes popular in woman. Men cut their mustaches short and thin.</p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>Ponytails are used by women and the pompadour among men.</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>Men and women start wearing their hair longer and more natural looking.</p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
            {
                "t13respuesta": "<p>Women seem to turn to many short and long styles from the past.</p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p>Men and women start getting used to the idea of having no “fashion rules” to follow.</p>",
                "t17correcta": "4",
                etiqueta: "5"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Listen again and put the following events in the order they occurred.",
            "t11instruccion": "",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "False",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "True",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "All jobs are dangerous.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lumberjacks have the most dangerous job in America.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Deep sea fishermen are exposed to freezing temperatures.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hitting your pinky toe is not hurtful.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Electric shocks make you light up as a Christmas tree.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose True or False for the following statements.",
            "t11instruccion": "",
            "descripcion": "Statement",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "lung diseases and other long-term illnesses.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "road accidents and exhaustion.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "courageous people who put their lives on the line every day for others.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "is not common in adults.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "fall from high altitudes.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Miners can get"
            },
            {
                "t11pregunta": "Truck drivers are exposed to"
            },
            {
                "t11pregunta": "Firemen are"
            },
            {
                "t11pregunta": "Choking on food"
            },
            {
                "t11pregunta": "Roofers may"
            },
        ],
        "pregunta": {
            "t11pregunta": "Match the halves to make sentences. ",
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //5 
    {
        "respuestas": [
            {
                "t13respuesta": "am",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "is",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "are",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "I "
            },
            {
                "t11pregunta": " a little boy.<br>My mom "
            },
            {
                "t11pregunta": " very careful.<br>You "
            },
            {
                "t11pregunta": " my best friend.<br>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the words where they belong.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Correct",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My father are a tall man.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I am a little boy.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "They is my best friends.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My teacher is very good.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My cousins are very funny.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Select whether the sentence is correct or incorrect.",
            "t11instruccion": "",
            "descripcion": "Statement",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "don't",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "doesn't",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My cat &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 like pizza.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My little cousin &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 have a piano.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lilia and Mario &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 go to the park after 7.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tadeo &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 hear pop music.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 want to play football.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Elephants &#9135;&#9135;&#9135;&#9135;&#9135;&#9135 eat watermelon.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Mark the choice that completes the sentence correctly.",
            "t11instruccion": "",
            "descripcion": "Statement",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t17correcta": "wrote"
            },
            {
                "t17correcta": "flew"
            },
            {
                "t17correcta": "went"
            },
            {
                "t17correcta": "ran"
            },
            {
                "t17correcta": "came"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Yesterday, my grandmother "
            },
            {
                "t11pregunta": " a letter. (write) <br>Peter "
            },
            {
                "t11pregunta": " to London last week. (fly)<br>My sister "
            },
            {
                "t11pregunta": " to the park this morning. (go)<br>The girls "
            },
            {
                "t11pregunta": " to catch the ice cream bus. (run)<br>Daniela and Francisco "
            },
            {
                "t11pregunta": " to my house to watch a movie. (come)<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Type the correct form of the verb in parentheses to complete the sentence.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "The children were writing a letter.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "The girl is dancing.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "The children were watching TV.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "My mom was cooking dinner.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Look at the picture and choose the right option to describe it.<br><center> <img style='height: 300px; display:block; ' src='EI5A_B1_R01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "My mom was cooking dinner.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "My mom was playing with dolls.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Angélica was flying to Germany.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "My dad was having breakfast.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Look at the picture and choose the right option to describe it.<br><center> <img style='height: 400px; display:block; ' src='EI5A_B1_R02.png'> </center>",
            "t11instruccion": "",
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "John was playing football.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "John was skating.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Mariana was drinking water.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "John was doing his homework.",
                "t17correcta": "0",
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Look at the picture and choose the right option to describe it.<br><center> <img style='height: 400px; display:block; ' src='EI5A_B1_R03.png'> </center>",
            "t11instruccion": "",
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "The airplane was flying in the sky.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "The airplane was landing.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Rodrigo was running.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Laura was playing with her dog.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Look at the picture and choose the right option to describe it.<br><center> <img style='height: 400px; display:block; ' src='EI5A_B1_R04.png'> </center>",
            "t11instruccion": "",
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "Valeria was taking a shower.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Valeria was dancing.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Valeria was taking her math class.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Valeria was taking an exam.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Look at the picture and choose the right option to describe it.<br><center> <img style='height: 400px; display:block; ' src='EI5A_B1_R05.png'> </center>",
            "t11instruccion": "",
        }
    },
    //14
    {
        "respuestas": [
            {
                "t17correcta": "while"
            },
            {
                "t17correcta": "when"
            },
            {
                "t17correcta": "when"
            },
            {
                "t17correcta": "while"
            },
            {
                "t17correcta": "while"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Mom was cooking dinner "
            },
            {
                "t11pregunta": " I was doing my homework. <br>My sister learned karate "
            },
            {
                "t11pregunta": " she went to London. <br>The girl saw a dinosaur "
            },
            {
                "t11pregunta": " she visited the museum. <br>Hugo was watching TV "
            },
            {
                "t11pregunta": " his brother was taking a shower. <br>My parents were decorating for my party "
            },
            {
                "t11pregunta": " I was having lunch at Grandma’s. <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Type &quot;while&quot; or &quot;when&quot; where they belong.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "Badly",
                "t17correcta": "1,3,5"
            },
            {
                "t13respuesta": "Eventually",
                "t17correcta": "2,4,6"
            },
            {
                "t13respuesta": "Easily",
                "t17correcta": "3,1,5"
            },
            {
                "t13respuesta": "At this point",
                "t17correcta": "4,2,6"
            },
            {
                "t13respuesta": "Quickly",
                "t17correcta": "5,1,3"
            },
            {
                "t13respuesta": "Finally",
                "t17correcta": "6,2,4"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                            .table img{height: 90px !important; width: auto !important; }\n\
                            </style><table class='table' style='margin-top:-10px;'>\n\
                            <tr><td>Adverbs of manner</td><td>Sequence linkers</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the words to the column they belong.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "myself",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "himself",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "ourselves",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "themselves",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "yourselves",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "herself",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "itself",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "I washed the car "
            },
            {
                "t11pregunta": ".<br>The baby boy is eating by "
            },
            {
                "t11pregunta": ".<br>We painted the trailer "
            },
            {
                "t11pregunta": ".<br>The children made holiday decorations by "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose and complete the sentence with a reflexive pronoun.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //17
    {
        "respuestas": [
            {
                "t13respuesta": "Habits",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Customs",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Becoming familiar with",
                "t17correcta": "2",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "He is used to horses.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My mom is getting used to waking up early.",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "I am used to big cities.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "My teacher used to play with us.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Our dog used to eat in the mornings.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Liliana is getting used to her new school.",
                "correcta": "2",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose the option that best classifies the action.",
            "t11instruccion": "",
            "descripcion": "Statement",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //18
{ 
 "respuestas":[ 
 { 
 "t13respuesta":"seven hundred and fifty dollars.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"seven thousand dollars, fifty cents.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"seventy five hundred dollars.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"seven thousand five hundred dollars.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Look  at the invoice and choose the option that completes the sentence.<br><br>The amount for the Destination Charge is", 
 "t11instruccion": "<img src='EI5A_B1_R07.png'> ", 
 } 
 }, 
//20 Opcion multiple pendiente
 { 
 "respuestas":[ 
 { 
 "t13respuesta":"two thousand seven hundred and ninety dollars.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"two hundred and seventy nine dollars.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"twenty seven hundred dollars, ninety cents.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"twenty thousand seven hundred and ninety dollars.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Look  at the invoice and choose the option that completes the sentence. <br><br> The amount for the Total  Options is", 
 "t11instruccion": "<img src='EI5A_B1_R07.png'> ", 
 } 
 }, 
//21 Opcion multiple pendiente
 { 
 "respuestas":[ 
 { 
 "t13respuesta":"twenty nine thousand and thirty five dollars.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"twenty nine hundred dollars,  thirty five cents.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"two hundred and ninety dollars, thirty five cents.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"two thousand nine hundred and thirty five dollars.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Look  at the invoice and choose the option that completes the sentence.<br><br>The amount for the Total Vehicle & Options is", 
 "t11instruccion": "<img src='EI5A_B1_R07.png'> ", 
 } 
 }, 
//22 Opcion multiple pendiente
 { 
 "respuestas":[ 
 { 
 "t13respuesta":"you must not use your cellphone.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"you have to use your cellphone.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"you don’t have to use your cellphone.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Choose the option that expresses the message on the sign.<br><br>This sign says that ...", 
 "t11instruccion": "<img src='EI5A_B1_R10.png'>", 
 } 
 },
//23 Opcion multiple pendiente
 { 
 "respuestas":[ 
 { 
 "t13respuesta":"there is a camera recording.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"you don’t have to bring a camera to the premises.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"you must bring a camera.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Choose the option that expresses the message on the sign.<br><br>This sign says that ...", 
 "t11instruccion": "<img src='EI5A_B1_R11.png'>", 
 } 
 }, 
//24 Opcion multiple pendiente
{ 
 "respuestas":[ 
 { 
 "t13respuesta":"you are not allowed to bring food into the premises.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"you don't have to bring food inside.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"you have to eat before you get there.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Choose the option that expresses the message on the sign.<br><br>This sign means that ...", 
 "t11instruccion": "<img src='EI5A_B1_R12.png'>", 
 } 
 }, 
//25 Opcion multiple pendiente
{ 
 "respuestas":[ 
 { 
 "t13respuesta":"you must slow down.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"you have to speed up.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"you don't have to drive fast.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Choose the option that expresses the message on the sign.<br><br>This sign means that ...", 
 "t11instruccion": "<img src='EI5A_B1_R13.png'>", 
 } 
 }, 



//26 Arrastra y corta 
 
{
        "respuestas": [
            {
                "t13respuesta": "<p> Are you<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>are going<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>’m going<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>am not going<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>do<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "You are",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>going<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><b>Daniel:</b> <\/p>"
            },
            {
                "t11pregunta": "going to celebrate your birthday this weekend? <br><b>Ulises:</b> No, we"
            },
            {
                "t11pregunta": "to go to the beach this Friday afternoon, but I think I"
            },
            {
                "t11pregunta": "to hold a grill party when we come back. I"
            },
            {
                "t11pregunta": "to miss the chance to celebrate with my friends this year! <br><b>Daniel:</b> Cool! What"
            },
            {
                "t11pregunta": "we have to bring? ..."
            },
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag and drop the correct option to complete the dialogue.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
},
//27 opcion multiple
 {  
      "respuestas":[
         {  
            "t13respuesta": "had to",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "has to",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "have to",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 



         

         {  
            "t13respuesta": "had to take",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "has to take",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "have to take",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },




          {  
            "t13respuesta": "had to go",
            "t17correcta": "1",
            "numeroPregunta":"2"
         }, {  
            "t13respuesta": "have to go",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "has to go",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
        




      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Select the best option to complete the sentence.<br><br>Last week, the fire alarm went off at school. We <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> evacuate as we did in the drills. I’m glad everybody knew what to do.",
                         "Since no one knew what had ignited the alarm, the teacher <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> us out of the school while the firemen arrived. ",
                         "There was smoke coming out of one of the classrooms. The firemen <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> in and check. It turned out to be an air conditioning regulator. "],
         "preguntasMultiples": true,
      }
   },
 {
        "respuestas": [
            {
                "t13respuesta": "visited",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "first",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "elevated",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "brother",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "immediately",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "wasn’t used to",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "he",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "did he",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "insisted",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "ground",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "visit",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "after",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "immediate",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "didn’t use to",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "he did",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "small",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Yesterday we "
            },
            {
                "t11pregunta": " the new amusement park. The "
            },
            {
                "t11pregunta": " thing we saw was an "
            },
            {
                "t11pregunta": " ferris wheel which my little "
            },
            {
                "t11pregunta": " spotted "
            },
            {
                "t11pregunta": ". He wanted to get a lift to the top so we got in line for that attraction. Once at the top, we realized he "
            },
            {
                "t11pregunta": " ferris wheels because "
            },
            {
                "t11pregunta": " got dizzy. But, "
            },
            {
                "t11pregunta": " enjoy the ride? He did! He  "
            },
            {
                "t11pregunta": " on going up again as soon as we touched the "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Help the author complete the paragraph about one trip to an amusement park. Describe people, objects, and actions. Use verbs in accordance to the time you are using.<center> <img style='height: 200px; ' src='EI5A_B1_R06.png'> </center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

];