json=[
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "EI1A_B1_R01_02.png",
                "t17correcta": "0",
                "columna":"0"
            },
            {
                "t13respuesta": "EI1A_B1_R01_03.png",
                "t17correcta": "1",
                "columna":"0"
            },
            {
                "t13respuesta": "EI1A_B1_R01_04.png",
                "t17correcta": "2",
                "columna":"1"
            },
            {
                "t13respuesta": "EI1A_B1_R01_05.png",
                "t17correcta": "3",
                "columna":"1"
            },
            {
                "t13respuesta": "EI1A_B1_R01_06.png",
                "t17correcta": "4",
                "columna":"0"
            },
            {
                "t13respuesta": "EI1A_B1_R01_07.png",
                "t17correcta": "5",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Listen to your teacher. Drag the name of the kid in the space provided.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"EI1A_B1_R01_01.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":false
           
            
        },
        "contenedores": [
            {"Contenedor": ["", "437,302", "cuadrado", "150, 60", ".","transparent"]},
            {"Contenedor": ["", "456,472", "cuadrado", "150, 60", ".","transparent"]},
            {"Contenedor": ["", "116,412", "cuadrado", "150, 60", ".","transparent"]},
            {"Contenedor": ["", "116,224", "cuadrado", "150, 60", ".","transparent"]},
            {"Contenedor": ["", "152,63", "cuadrado", "150, 60", ".","transparent"]},
            {"Contenedor": ["", "401,15", "cuadrado", "150, 60", ".","transparent"]}
        ]
    },
     //2
    {
        "respuestas":[
           {
              "t13respuesta":     "<img src='EI1A_B1_R02_01.png' width=150 height=150px/>",
              "t17correcta":"1",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R02_02.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R02_03.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"0"
           },


           {
              "t13respuesta":     "<img src='EI1A_B1_R03_01.png' width=150 height=150px/>",
              "t17correcta":"1",
            "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R03_02.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R03_03.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"1"
           },


           {
              "t13respuesta":     "<img src='EI1A_B1_R04_01.png' width=150 height=150px/>",
              "t17correcta":"1",
            "numeroPregunta":"2"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R04_02.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"2"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R04_03.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"2"
           },


           {
              "t13respuesta":     "<img src='EI1A_B1_R05_01.png' width=150 height=150px/>",
              "t17correcta":"1",
            "numeroPregunta":"3"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R05_02.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"3"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R05_03.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"3"
           },



           {
              "t13respuesta":     "<img src='EI1A_B1_R06_01.png' width=150 height=150px/>",
              "t17correcta":"1",
            "numeroPregunta":"4"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R06_02.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"4"
           },
           {
              "t13respuesta":     "<img src='EI1A_B1_R06_03.png' width=150 height=150px/>",
              "t17correcta":"0",
            "numeroPregunta":"4"
           }
        ],
        "pregunta":{
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Listen to your teacher. Choose the right option.<br><br>1",
                          "<br><br><br>2",
                          "<br><br>3",
                          "<br><br>4",
                          "<br><br>5",
                        ],
         "preguntasMultiples": true,
       
        }
      },
      //3
      {
        "respuestas":[
           {
              "t13respuesta":     "Correct",
              "t17correcta":"1",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Incorrect",
              "t17correcta":"0",
            "numeroPregunta":"0"
           } ,{
            "t13respuesta":     "Correct",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Incorrect",
            "t17correcta":"1",
          "numeroPregunta":"1"
         } ,{
            "t13respuesta":     "Correct",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Incorrect",
            "t17correcta":"1",
          "numeroPregunta":"2"
         } ,{
            "t13respuesta":     "Correct",
            "t17correcta":"1",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "Incorrect",
            "t17correcta":"0",
          "numeroPregunta":"3"
         } ,{
            "t13respuesta":     "Correct",
            "t17correcta":"1",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "Incorrect",
            "t17correcta":"0",
          "numeroPregunta":"4"
         } ,{
            "t13respuesta":     "Correct",
            "t17correcta":"0",
          "numeroPregunta":"5"
         },
         {
            "t13respuesta":     "Incorrect",
            "t17correcta":"1",
          "numeroPregunta":"5"
         }
        ],
        "pregunta":{
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Decide if the following is correct or incorrect.<br><br>These are curtains.<br> <img src='EI1A_B1_R07.png' width=150 height=150px/>",
                          "<br><br>This is a carrot. <br><img src='EI1A_B1_R08.png' width=150 height=150px/>",
                          "<br><br>These are shorts. <br><img src='EI1A_B1_R09.png' width=150 height=150px/>",
                          "<br><br>This is an eraser. <br><img src='EI1A_B1_R10.png' width=150 height=150px/>",
                          "<br><br>This is a hat. <br><img src='EI1A_B1_R11.png' width=150 height=150px/>",
                          "<br><br>This is a shirt. <br><img src='EI1A_B1_R12.png' width=150 height=150px/>"
                        ],
         "preguntasMultiples": true,
        }
      },
      //4
      {
        "respuestas": [
            {
                "t13respuesta": "No",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Yes",
                "t17correcta": "1"
            }
           ], 
            "preguntas": [ 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"There are six kids in class.",
            "correcta"  : "1"
            }, 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"The kids are wearing a uniform.",
            "correcta"  : "1"
            }, 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"The clock is under the wall.",
            "correcta"  : "0"
            }, 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"There are three windows.",
            "correcta"  : "0"
            }, 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"The backpacks are on the floor.",
            "correcta"  : "0"
            }, 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"The teacher has a green book in her hand.",
            "correcta"  : "1"
            }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "13",
                "t11instruccion": "<center><img src='EI1A_B1_R13.png' width=472 height=412px/></center>",
                "t11pregunta": "<p>Choose “Yes” or “No” based on the picture.<\/p>",
                "descripcion": "Statement",   
                "variante": "editable",
                "anchoColumnaPreguntas": 50,
                "evaluable"  : true
            }        
        },
        //5
         {
                "respuestas": [        
                    {
                       "t13respuesta":  "m",
                         "t17correcta": "1" 
                     },
                    {
                       "t13respuesta":  "o",
                         "t17correcta": "2" 
                     },
                    {
                       "t13respuesta":  "t",
                         "t17correcta": "3" 
                     },
                    {
                       "t13respuesta":  "h",
                         "t17correcta": "4" 
                     },
                    {
                       "t13respuesta":  "e",
                         "t17correcta": "5" 
                     },
                    {
                       "t13respuesta":  "r",
                         "t17correcta": "6" 
                     },
                ],
                "preguntas": [ 
                  {
                  "t11pregunta": "<br><br><img src='EI1A_B1_R14.png' width=250 height=250px/>"
                  },
                  {
                  "t11pregunta": " "
                  },
                  {
                  "t11pregunta": " "
                  },
                  {
                  "t11pregunta": " "
                  },
                  {
                  "t11pregunta": " "
                  },
                  {
                  "t11pregunta": " "
                  },
                  {
                  "t11pregunta": "<br>"
                  },
                ],
                "pregunta": {
                  "c03id_tipo_pregunta": "8",
                  "t11pregunta": "Drag the letters to make words according to the picture.", 
                   "t11instruccion": "", 
                   "respuestasLargas": true, 
                   "pintaUltimaCaja": false, 
                   "contieneDistractores": true 
                 } 
              },
            //6
             {
                    "respuestas": [        
                        {
                           "t13respuesta":  "y",
                             "t17correcta": "1" 
                         },
                        {
                           "t13respuesta":  "e",
                             "t17correcta": "2" 
                         },
                        {
                           "t13respuesta":  "l",
                             "t17correcta": "3,4" 
                         },
                        {
                           "t13respuesta":  "l",
                             "t17correcta": "3,4" 
                         },
                        {
                           "t13respuesta":  "o",
                             "t17correcta": "5" 
                         },
                        {
                           "t13respuesta":  "w",
                             "t17correcta": "6" 
                         },
                    ],
                    "preguntas": [ 
                      {
                      "t11pregunta": "<br><br><img src='EI1A_B1_R15.png' width=250 height=250px/>"
                      },
                      {
                      "t11pregunta": " "
                      },
                      {
                      "t11pregunta": " "
                      },
                      {
                      "t11pregunta": " "
                      },
                      {
                      "t11pregunta": " "
                      },
                      {
                      "t11pregunta": " "
                      },
                      {
                      "t11pregunta": "<br>"
                      },
                    ],
                    "pregunta": {
                      "c03id_tipo_pregunta": "8",
                      "t11pregunta": "Drag the letters to make words according to the picture.", 
                       "t11instruccion": "", 
                       "respuestasLargas": true, 
                       "pintaUltimaCaja": false, 
                       "contieneDistractores": true 
                     } 
                  },
                //7
                 {
                        "respuestas": [        
                            {
                               "t13respuesta":  "s",
                                 "t17correcta": "1,3" 
                             },
                            {
                               "t13respuesta":  "i",
                                 "t17correcta": "2" 
                             },
                            {
                               "t13respuesta":  "s",
                                 "t17correcta": "1,3" 
                             },
                            {
                               "t13respuesta":  "t",
                                 "t17correcta": "4" 
                             },
                            {
                               "t13respuesta":  "e",
                                 "t17correcta": "5" 
                             },
                            {
                               "t13respuesta":  "r",
                                 "t17correcta": "6" 
                             },
                        ],
                        "preguntas": [ 
                          {
                          "t11pregunta": "<br><br><img src='EI1A_B1_R16.png' width=250 height=250px/>"
                          },
                          {
                          "t11pregunta": " "
                          },
                          {
                          "t11pregunta": " "
                          },
                          {
                          "t11pregunta": " "
                          },
                          {
                          "t11pregunta": " "
                          },
                          {
                          "t11pregunta": " "
                          },
                          {
                          "t11pregunta": "<br>"
                          },
                        ],
                        "pregunta": {
                          "c03id_tipo_pregunta": "8",
                          "t11pregunta": "Drag the letters to make words according to the picture.", 
                           "t11instruccion": "", 
                           "respuestasLargas": true, 
                           "pintaUltimaCaja": false, 
                           "contieneDistractores": true 
                         } 
                      },
                    //8
                     {
                            "respuestas": [        
                                {
                                   "t13respuesta":  "f",
                                     "t17correcta": "1" 
                                 },
                                {
                                   "t13respuesta":  "r",
                                     "t17correcta": "2" 
                                 },
                                {
                                   "t13respuesta":  "i",
                                     "t17correcta": "3" 
                                 },
                                {
                                   "t13respuesta":  "e",
                                     "t17correcta": "4" 
                                 },
                                {
                                   "t13respuesta":  "n",
                                     "t17correcta": "5" 
                                 },
                                {
                                   "t13respuesta":  "d",
                                     "t17correcta": "6" 
                                 },
                            ],
                            "preguntas": [ 
                              {
                              "t11pregunta": "<br><br><img src='EI1A_B1_R17.png' width=250 height=250px/>"
                              },
                              {
                              "t11pregunta": " "
                              },
                              {
                              "t11pregunta": " "
                              },
                              {
                              "t11pregunta": " "
                              },
                              {
                              "t11pregunta": " "
                              },
                              {
                              "t11pregunta": " "
                              },
                              {
                              "t11pregunta": "<br>"
                              },
                            ],
                            "pregunta": {
                              "c03id_tipo_pregunta": "8",
                              "t11pregunta": "Drag the letters to make words according to the picture.", 
                               "t11instruccion": "", 
                               "respuestasLargas": true, 
                               "pintaUltimaCaja": false, 
                               "contieneDistractores": true 
                             } 
                          },
                        //9
                         {
                                "respuestas": [        
                                    {
                                       "t13respuesta":  "m",
                                         "t17correcta": "1" 
                                     },
                                    {
                                       "t13respuesta":  "o",
                                         "t17correcta": "2" 
                                     },
                                    {
                                       "t13respuesta":  "u",
                                         "t17correcta": "3" 
                                     },
                                    {
                                       "t13respuesta":  "t",
                                         "t17correcta": "4" 
                                     },
                                    {
                                       "t13respuesta":  "h",
                                         "t17correcta": "5" 
                                     },
                                ],
                                "preguntas": [ 
                                  {
                                  "t11pregunta": "<br><br><img src='EI1A_B1_R18.png' width=250 height=250px/>"
                                  },
                                  {
                                  "t11pregunta": " "
                                  },
                                  {
                                  "t11pregunta": " "
                                  },
                                  {
                                  "t11pregunta": " "
                                  },
                                  {
                                  "t11pregunta": " "
                                  },
                                  {
                                  "t11pregunta": "<br>"
                                  },
                                ],
                                "pregunta": {
                                  "c03id_tipo_pregunta": "8",
                                  "t11pregunta": "Drag the letters to make words according to the picture.", 
                                   "t11instruccion": "", 
                                   "respuestasLargas": true, 
                                   "pintaUltimaCaja": false, 
                                   "contieneDistractores": true 
                                 } 
                              } 


];