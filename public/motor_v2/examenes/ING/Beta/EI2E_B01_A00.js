json = [
    {
        "respuestas": [
            {
                "t13respuesta": "K",
                "t17correcta": "1",

            },
            {
                "t13respuesta": "i",
                "t17correcta": "2",

            },
            {
                "t13respuesta": "n",
                "t17correcta": "3",

            },
            {
                "t13respuesta": "s",
                "t17correcta": "4",

            },
            {
                "t13respuesta": "e",
                "t17correcta": "5",

            },
            {
                "t13respuesta": "y",
                "t17correcta": "6",

            },
            {
                "t13respuesta": "q",
                "t17correcta": "0",

            },
            {
                "t13respuesta": "a",
                "t17correcta": "0",

            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Name: Jane "
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen to your teacher and drag the correct option in the form.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
        },

    },
    //1.2
    {
        "respuestas": [
            {
                "t13respuesta": "Fifth",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Third",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Fourth",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Address: 9 Manchester. "
            },
            {
                "t11pregunta": " floor. London<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": " Listen to your teacher and drag the correct option in the form.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        },
        //1.3
    },
    {
        "respuestas": [
            {
                "t13respuesta": "7",
                "t17correcta": "1"
            }, {
                "t13respuesta": "9",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "5",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Members in the family: "
            },
            {
                "t11pregunta": "<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen to your teacher and drag the correct information.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //1.4
    {
        "respuestas": [
            {
                "t13respuesta": "dog",
                "t17correcta": "1"
            }, {
                "t13respuesta": "cat",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "fish",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Pets: "
            },
            {
                "t11pregunta": " and parrot.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen to your teacher and drag the correct information.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //1.5
    {
        "respuestas": [
            {
                "t13respuesta": "C",
                "t17correcta": "1"
            }, {
                "t13respuesta": "D",
                "t17correcta": "0"
            }, {
                "t13respuesta": "B",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Second Grade room: "
            },
            {
                "t11pregunta": "<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Listen to your teacher and drag the correct information.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //2
    {
        "respuestas": [
            {
            "t13respuesta": "EI2A_B1_R01_02.png",
            "t17correcta": "5",
            "columna": "0"
            },
            {
            "t13respuesta": "EI2A_B1_R01_03.png",
            "t17correcta": "4",
            "columna": "0"
            },
            {
            "t13respuesta": "EI2A_B1_R01_04.png",
            "t17correcta": "3",
            "columna": "1"
            },
            {
            "t13respuesta": "EI2A_B1_R01_05.png",
            "t17correcta": "2",
            "columna": "1"
            },
            {
            "t13respuesta": "EI2A_B1_R01_06.png",
            "t17correcta": "0",
            "columna": "0"
            },
            {
            "t13respuesta": "EI2A_B1_R01_07.png",
            "t17correcta": "1",
            "columna": "0"
            },
           
            ],
            "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Listen to your teacher and drag the name of the kid.",
            "tipo": "ordenar",
            "imagen": true,
            "url": "EI2A_B1_R01_01.png",
            "respuestaImagen": true,
            "tamanyoReal": false,
           // "anchoImagen": 60,
            "bloques": false,
            "borde": false
            
            },
            "contenedores": [
                { "Contenedor": ["", "10,130", "cuadrado", "115, 34", ".", "transparent"] },
                { "Contenedor": ["", "10,272", "cuadrado", "115, 34", ".", "transparent"] },
                { "Contenedor": ["", "10,453", "cuadrado", "115, 34", ".", "transparent"] },
                { "Contenedor": ["", "477,57", "cuadrado", "115, 34", ".", "transparent"] },
                { "Contenedor": ["", "479,257", "cuadrado", "115,34", ".", "transparent"] },
                { "Contenedor": ["", "478,488", "cuadrado", "115,34", ".", "transparent"] },
                
               
            ]
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Correct",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Correct",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Correct",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Correct",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Correct",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Correct",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Incorrect",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Decide if the following is correct or incorrect.<br><br>This is red a star.<br><center><img src='EI2A_B1_R02.png' height='100' width='100'></center>",
                "This is a heart.<br><center><img src='EI2A_B1_R03.png' height='100' width='100'></center>",
                "These are chicks.<br><center><img src='EI2A_B1_R04.png' height='100' width='100'></center>",
                "These are sheep.<br><center><img src='EI2A_B1_R05.png' height='130' width='130'></center>",
                "Mark has got curly hair.<br><center><img src='EI2A_B1_R06.png' height='100' width='100'></center>",
                "This is a bed.<br><center><img src='EI2A_B1_R07.png' height='100' width='100'></center>",],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        },
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "No",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Yes",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The man with white hair is reading a newspaper.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There is a girl running.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The boy wearing a hat is riding a green bicycle.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There are five people riding a bike.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There are red and green flowers.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The man riding a bike is wearing a blue shirt.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose “Yes” or “No” based on the picture.",
            "t11instruccion": "<center><img src='EI2A_B1_R08.png' height='80%' ></center>",
            "descripcion": "Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "p",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "e",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "n",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "c",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "i",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "l",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center><img src='EI2A_B1_R09.png' height='400px'></center><br><center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the letters to make words according to the picture.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "t",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "r",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "i",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "a",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "n",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "g",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "l",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "e",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center><img src='EI2A_B1_R10.png' height='400px'></center><br><center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the letters to make words according to the picture.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "s",
                "t17correcta": "1,3"
            },
            {
                "t13respuesta": "i",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "s",
                "t17correcta": "1,3"
            },
            {
                "t13respuesta": "t",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "e",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "r",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center><img src='EI2A_B1_R11.png' height='400px'></center><br><center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the letters to make words according to the picture.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "c",
                "t17correcta": "1,4"
            },
            {
                "t13respuesta": "i",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "r",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "c",
                "t17correcta": "4,1"
            },
            {
                "t13respuesta": "l",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "e",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center><img src='EI2A_B1_R12.png' height='400px'></center><br><center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the letters to make words according to the picture.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "f",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "i",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "s",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "h",
                "t17correcta": "4"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<center><img src='EI2A_B1_R13.png' height='400px'></center><br><center>"
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": "</center><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag the letters to make words according to the picture.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "school",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "play",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Science",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "paint",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "English class",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "read",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "playground",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "friends",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "A "
            },
            {
                "t11pregunta": " is a place where kids learn and "
            },
            {
                "t11pregunta": ".<br>Kids take different subjects, like Math and "
            },
            {
                "t11pregunta": ".<br>In the Art class, kids draw and "
            },
            {
                "t11pregunta": ".<br>In the "
            },
            {
                "t11pregunta": ", students listen and "
            },
            {
                "t11pregunta": " words in English.<br>The school’s "
            },
            {
                "t11pregunta": " is a good place to make "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Look at the picture and read the text. Drag the missing words.<br><center><img src='EI2A_B1_R14.png' height='220px'></center><br><center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "bed",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "red",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "shelf",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "nightstand",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Three",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Where is the book? It’s on the  "
            },
            {
                "t11pregunta": ".<br>What color are the curtains? They are "
            },
            {
                "t11pregunta": ".<br>Where is the Teddy bear? It’s on the "
            },
            {
                "t11pregunta": ".<br>Where is the lamp?  It’s on the "
            },
            {
                "t11pregunta": ".<br>How many boxes can you see?  "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Look at the picture and read the questions. Drag the answers. <br><center><img src='EI2A_B1_R15.png' height='275px'></center><br><center>",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    }
];