json = [
  //1
  {
    "respuestas": [
      {
        "t13respuesta": "False",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "True",
        "t17correcta": "1"
      }
    ],
    "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Toltec Indians originally inhabited San Miguel a long time ago. ",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Salesians came immediately after the Spanish conquered Mexico.",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Toru knows exactly where the name San Miguel de Allende comes from.",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Many Americans retire in San Miguel de Allende.",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "“Magic Towns” only exist in fairytales. ",
        "correcta": "0"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",

      "t11pregunta": "<p>Listen to the audio and choose &quot;T&quot; for true or &quot;F&quot; for false for the following statements.<\/p>",
      "descripcion": "Aspects",
      "variante": "editable",
      "anchoColumnaPreguntas": 30,
      "evaluable": true
    }
  },
  //2
  {
    "respuestas": [
      {
        "t13respuesta": "pyramids",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "Spanish",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "Uruapan",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "San Miguel de Allende",
        "t17correcta": "4"
      },
      {
        "t13respuesta": "Mexico",
        "t17correcta": "5"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "They built "
      },
      {
        "t11pregunta": " like the ones in the archeological zone called Cañada de la Virgen.<br>Franciscans came immediately after the "
      },
      {
        "t11pregunta": " conquered Mexico.<br>Franciscans established missions in a nearby town called "
      },
      {
        "t11pregunta": ".<br>"
      },
      {
        "t11pregunta": " eventually became a home for many retired Americans.<br>  I found this in a web page called “Visit "
      },
      {
        "t11pregunta": ".”<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Listen again; drag and drop the option that best completes the sentence. ",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //3
  {
    "respuestas": [
      {
        "t13respuesta": "<p>The class starts.</p>",
        "t17correcta": "0",
        etiqueta: "1"
      },
      {
        "t13respuesta": "<p>The teacher introduces the topic for the day.</p>",
        "t17correcta": "1",
        etiqueta: "2"
      },
      {
        "t13respuesta": "<p>Julia answers a question.</p>",
        "t17correcta": "2",
        etiqueta: "3"
      },
      {
        "t13respuesta": "<p>Eric, wrongfully, answers a question.</p>",
        "t17correcta": "3",
        etiqueta: "4"
      },
      {
        "t13respuesta": "<p>Julia corrects Eric.</p>",
        "t17correcta": "4",
        etiqueta: "5"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "9",
      "t11pregunta": "Read the following conversation and answer the questions thereafter.<br>Put the following events in the order they occurred.",
      "t11instruccion": "<center><b>Reading</b></center><br>\n\
      Read the following conversation and answer the questions thereafter:<br>\n\
      <b>Teacher:</b> Good morning class! Today we are going to study the people who lived in this continent before the 16th century. As you probably know, the Aztecs were the most dominant group among the Mesoamerican civilizations. What else do you know about them?<br>\n\
      <b>Julia:</b> I know that they were warriors. They built pyramids to honor their gods. They established their city on an island surrounded by Lake Texcoco and they also studied the stars.<br>\n\
      <b>Teacher:</b> You are correct, Julia. They were warriors; they fought nearby groups and also made alliances. Does anyone know the name of their city? <br>\n\
      <b>Eric:</b> I think it was called Teotihuacan. It had two giant pyramids, The Pyramid of the Moon and The Pyramid of the Sun.<br>\n\
      <b>Julia:</b> Eric, I think you’re mixed up. The city was called Tenochtitlan. Teotihuacan is a nearby city, though. <br>\n\
      <b>Teacher:</b> That’s correct! Teotihuacan was a city built long before Tenoch found an eagle perched on a cactus as a signal from their gods to build their city there. Well, I think you are going to love this topic. And, to make it even more interesting, we are going \n\
      on a field trip to the Anthropology Museum in Mexico City. For that, we need to get your parents’ permission, but I will tell you more about that later. Now, as we were saying…",
    }
  },
  //4
  {
    "respuestas": [
      {
        "t13respuesta": "False",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "True",
        "t17correcta": "1"
      }
    ],
    "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "The Aztecs were the most dominant group among the Mesoamerican civilizations. ",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "They established their city on a mountain surrounded by Lake Texcoco",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "The Aztecs were warriors; they fought nearby groups.",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "The first Aztec city was called Teotihuacan.",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "The kids are going on a field trip to the Anthropology Museum in Mexico City.",
        "correcta": "1"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",

      "t11pregunta": "<p>Choose  True or False for the following statements.",
      "descripcion": "Aspects ",
      "variante": "editable",
      "anchoColumnaPreguntas": 30,
      "evaluable": true
    }
  },
  //5
  {
    "respuestas": [
      {
        "t13respuesta": "wake up",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "wakes up",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "waking up",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "have",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "has",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "doing",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "cooks",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "cook",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "cooking",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "brush",
        "t17correcta": "1",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "brushes",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "brushing",
        "t17correcta": "0",
        "numeroPregunta": "3"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": ["Read the following sentences. Use what you know to complete the description of My week routine. Simple Present and routines.<br><br> I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> at 6:00 am every morning.",
        "<br><br>At home, we <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> breakfast at 6:40.",
        "<br><br>My dad generally <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> dinner and I sometimes help him. ",
        "<br><br>I always <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> my teeth before I go to bed."
      ],
      "preguntasMultiples": true
    }
  },
  //6
  {
    "respuestas": [
      {
        "t13respuesta": "How",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "is",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "like",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "prefers",
        "t17correcta": "4"
      },
      {
        "t13respuesta": "Who",
        "t17correcta": "5"
      },
      {
        "t13respuesta": "likes",
        "t17correcta": "6"
      },
      {
        "t13respuesta": "preferred",
        "t17correcta": "7"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Hello, Tony! "
      },
      {
        "t11pregunta": " are you?<br>This "
      },
      {
        "t11pregunta": " Yoshi, the new student.<br>Yoshi doesn’t "
      },
      {
        "t11pregunta": " math class as much as I do.<br>Yoshi "
      },
      {
        "t11pregunta": " sports class.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Read the following conversation about the new student and choose the words that complete the statements.",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //7
  {
    "respuestas": [
      {
        "t13respuesta": "are you from?",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "is your name?",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "old are you?",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "is your birthday?",
        "t17correcta": "4"
      }
    ],
    "preguntas": [
      {
        "t11pregunta": "Where "
      },
      {
        "t11pregunta": "What "
      },
      {
        "t11pregunta": "How"
      },
      {
        "t11pregunta": "When "
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Match the columns to complete the questions.",
      "altoImagen": "150",
      evaluable: true
    }
  },
  //8
  {
    "respuestas": [
      {
        "t17correcta": "firebug"
      },
      {
        "t17correcta": "frog"
      },
      {
        "t17correcta": "pencil"
      },
      {
        "t17correcta": "books"
      }
    ],
    "preguntas": [
      {
        "t11pregunta": "Steve caught a firebug at the summer camp. "
      },
      {
        "t11pregunta": " <br>Fanny found a little frog in the garden. "
      },
      {
        "t11pregunta": " <br>I grabbed my pencil to copy."
      },
      {
        "t11pregunta": " <br>They put away the books to go to recess."
      },
      {
        "t11pregunta": " <br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "6",
      "t11pregunta": "Read the sentences and decide which is the direct object. Type only one word to represent it.",
      "t11instruccion": "",
      evaluable: true,
      pintaUltimaCaja: false
    }
  },
  //9
  {
    "respuestas": [
      {
        "t13respuesta": "friend",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "brother",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "email",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Monica",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "cake",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "mother",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "me",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "book",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Andrea",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "mothers",
        "t17correcta": "1",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "card",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "children",
        "t17correcta": "0",
        "numeroPregunta": "3"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": ["Decide which is the indirect object in the sentences below.<br><br> My brother sent an email to his friend.",
        "<br><br>My mother baked a cake for Monica on her birthday.",
        "<br><br>Andrea lent me her math book. ",
        "<br><br>The children made a card for their mothers. "
      ],
      "preguntasMultiples": true
    }
  },
  //10
  {
    "respuestas": [
      {
        "t13respuesta": "Did",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "did",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "saw",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "bought",
        "t17correcta": "4"
      },
      {
        "t13respuesta": "Who",
        "t17correcta": "5"
      },
      {
        "t13respuesta": "didn’t ",
        "t17correcta": "6"
      },
      {
        "t13respuesta": "seeing",
        "t17correcta": "7"
      },
      {
        "t13respuesta": "buys",
        "t17correcta": "8"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": ""
      },
      {
        "t11pregunta": " you go to the movies yesterday?<br>Yes, we "
      },
      {
        "t11pregunta": ".<br>We "
      },
      {
        "t11pregunta": " the new film about space.<br>Dad "
      },
      {
        "t11pregunta": " some popcorn and soda before the movie began.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Choose and drag the correct option to complete the sentences.",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //11
  {
    "respuestas": [
      {
        "t13respuesta": "There is",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "there are",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "There are",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "there is",
        "t17correcta": "4"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": ""
      },
      {
        "t11pregunta": " a big TV screen in the classroom.<br>Yes, I saw it. And "
      },
      {
        "t11pregunta": " many new items too, like the speakers and the blinds.<br>"
      },
      {
        "t11pregunta": " many children in the band this year.<br>Everyone is excited because "
      },
      {
        "t11pregunta": " an excursion next month.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Choose and drag the correct option to complete the sentences.",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //12
  {
    "respuestas": [
      {
        "t13respuesta": "perfectly",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "thoughtfully",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "typically",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "normally",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "instantly",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "carefully",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "suddenly",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "slowly",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "peacefully",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "kindly",
        "t17correcta": "1",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "traditionally",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "eventually",
        "t17correcta": "0",
        "numeroPregunta": "3"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": ["Choose the correct word to complete the sentence. <br><br> 3D movies are cool because you can <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> see the objects coming out of the screen.",
        "<br><br>We <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> go to school from Monday to Friday.",
        "<br><br>My little sister got scared when the bug <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> appeared in the movie scene.  ",
        "<br><br>My mom always advice us to act <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> with new students. "
      ],
      "preguntasMultiples": true
    }
  },
  //13
  {
    "respuestas": [
      {
        "t13respuesta": "was watching",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "while",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "finished",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "helped",
        "t17correcta": "4"
      },
      {
        "t13respuesta": "watched",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "helping",
        "t17correcta": "0"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Yesterday I "
      },
      {
        "t11pregunta": " the TV when the electricity went off.<br>My mom was holding a lamp "
      },
      {
        "t11pregunta": " Dad was trying to fix the fuse box.<br>I "
      },
      {
        "t11pregunta": " my dinner at the light of a candle.<br>I also "
      },
      {
        "t11pregunta": " my little brother put on his pajamas.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Drag the correct word to complete the sentence. ",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //====================== Nueva integracion  14 septiembre
  //14
  {
    "respuestas": [
      {
        "t13respuesta": "couldn’t",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "could",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "cannot",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "can",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "Mark the correct word to complete the statement.<br><br>When I was 4 years old, I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>  swim. I learned until I was 6.",
      "t11instruccion": "",
    }
  },
  //15
  {
    "respuestas": [
      {
        "t13respuesta": "could",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "can",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "cannot",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "couldn’t",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "Mark the correct word to complete the statement.<br>My Dad <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> skate very well when he was 10. He won many prizes and now he is teaching me.",
      "t11instruccion": "",
    }
  },
  //16
  {
    "respuestas": [
      {
        "t13respuesta": "couldn’t",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "could",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "can’t",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "can",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "When we attended elementary school, my brother <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> play the saxophone as well as he does now.",
      "t11instruccion": "",
    }
  },
  //17
  {
    "respuestas": [
      {
        "t13respuesta": "<p>First, read the entire recipe to see what you need.</p>",
        "t17correcta": "0",
        etiqueta: "1"
      },
      {
        "t13respuesta": "<p>Second, preheat the oven to 180C.</p>",
        "t17correcta": "1",
        etiqueta: "2"
      },
      {
        "t13respuesta": "<p>After that, mix the ingredients as requested.</p>",
        "t17correcta": "2",
        etiqueta: "3"
      },
      {
        "t13respuesta": "<p>Next, pour the mixture into a greased cake tin.</p>",
        "t17correcta": "3",
        etiqueta: "4"
      },
      {
        "t13respuesta": "<p>Bake for 35 minutes, until well risen.</p>",
        "t17correcta": "4",
        etiqueta: "5"
      },
      {
        "t13respuesta": "<p>Let it cool, unfold and decorate.</p>",
        "t17correcta": "5",
        etiqueta: "6"
      },
      {
        "t13respuesta": "<p>It’s better accompanied by vanilla ice-cream.</p>",
        "t17correcta": "6",
        etiqueta: "7"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "9",
      "t11pregunta": "Arrange the steps to bake a cake.",
      "t11instruccion": "",
    }
  },
  //18
  {
    "respuestas": [
      {
        "t13respuesta": "were singing",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "was singing",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "singing",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "Choose the option that best completes the statement.<br> Mom and Dad  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> when we got home.",
      "t11instruccion": "",
    }
  },
  //19
  {
    "respuestas": [
      {
        "t13respuesta": "wasn’t paying",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "were not paying",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "isn’t paying",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "Choose the option that best completes the statement. Sorry, I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> attention.",
      "t11instruccion": "",
    }
  },
  //20
  {
    "respuestas": [
      {
        "t13respuesta": "Were",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Was",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Are",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "Choose the option that best completes the statement.  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> you  listening to music while you took a shower?",
      "t11instruccion": "",
    }
  },
  //21
  {
    "respuestas": [
      {
        "t13respuesta": "orange, blue, purple, pink, and yellow",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "orange blue purple pink and yellow",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "orange, blue purple pink, and yellow",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "My favorite colors are <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
      "t11instruccion": "",
    }
  },
  //22
  {
    "respuestas": [
      {
        "t13respuesta": "swimming, water polo, soccer, baseball, basketball, and volleyball.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "swimming; water polo: soccer, baseball; basketball, and volleyball.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "swimming water polo soccer baseball basketball and volleyball.",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "The sports we will practice this year are <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
      "t11instruccion": "",
    }
  },
  //23
  {
    "respuestas": [
      {
        "t13respuesta": "family",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "weather",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "castles",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "kites",
        "t17correcta": "4"
      },
      {
        "t13respuesta": "surfing",
        "t17correcta": "5"
      },
      {
        "t13respuesta": "sport",
        "t17correcta": "6"
      },
      {
        "t13respuesta": "volleyball",
        "t17correcta": "7"
      },
      {
        "t13respuesta": "read",
        "t17correcta": "8"
      },
      {
        "t13respuesta": "snorkeling",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "game",
        "t17correcta": "10"
      },
      {
        "t13respuesta": "basketball",
        "t17correcta": "11"
      },
      {
        "t13respuesta": "teachers",
        "t17correcta": "12"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "I love the beach. All my "
      },
      {
        "t11pregunta": " does. We enjoy the warm "
      },
      {
        "t11pregunta": " and relax by the shore. My little siblings like making "
      },
      {
        "t11pregunta": " with the sand while my cousin and I enjoy flying "
      },
      {
        "t11pregunta": ". One girl is "
      },
      {
        "t11pregunta": ". Surfing is the "
      },
      {
        "t11pregunta": " I want to try this summer! You can also swim or play "
      },
      {
        "t11pregunta": ". Some people even "
      },
      {
        "t11pregunta": " a book.<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Look at the picture and complete the paragraph with words from the box. There are words you don’t need to use. ",
      "t11instruccion": "<br><img src='EI4A_B1_R14.png'>",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //24
  {
    "respuestas": [
      {
        "t13respuesta": "There is no place like the beach!",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "It is very hot at the beach!",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "A lot of people go to the beach!",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "Choose a final sentence for your paragraph.",
      "t11instruccion": "",
    }
  },
];