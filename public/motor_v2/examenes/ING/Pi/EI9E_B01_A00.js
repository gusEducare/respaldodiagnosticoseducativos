json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Mike Barr had an accident while doing his job.</p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>Mike was left out of action for three months.</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>The kids invited Mike to an interview.</p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
            {
                "t13respuesta": "<p>Daniel asked Mike about his job.</p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p>Mike mentioned he doesn’t like to be set on fire.</p>",
                "t17correcta": "4",
                etiqueta: "5"
            },
            {
                "t13respuesta": "<p>Daniel thanked Mike for talking to them.</p>",
                "t17correcta": "5",
                etiqueta: "6"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Arrange the following events in the order they occurred.",
            "t11instruccion": "",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "T",
                "t17correcta": "1",
            },

        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mike Barr job’s requires a lot of exercise.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mike’s job is like all others.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mike loves being set on fire.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mike´s face has to look exactly like the actor’s.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mike is injured and can’t work for three months.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Choose T for true or F for false for the following statements.",
            "t11instruccion": "",
            "descripcion": "Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "A",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "B",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "C",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "D",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "A near century of Oscars"
            },
            {
                "t11pregunta": "Extraordinary winners and losers"
            },
            {
                "t11pregunta": "No, thanks!"
            },
            {
                "t11pregunta": "Never too young, never too old"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Choose the best heading for each paragraph.",
            "t11instruccion": "A<br>There have been about 3000 statuettes given since the first Academy Award Ceremony in 1929. Since then, we have witnessed many memorable moments, surprises and an occasional reject. Here are some curious things about the Oscars you may not know.<br>B<br>In 1934, a five-year-old girl was given an Oscar. She has been the youngest actress to receive an award; her name, Shirley Temple. This was an honorary award for children. Forty years later, Tatum O’Neal, aged 10, received the statuette for best actress in a supporting role.<br>Christopher Plummer won the statuette at the age of 82 in 2012 for his performance in the movie Beginners. His award was for best supporting actor. In the role of best actor, the oldest winner has been Henry Fonda, he was 76.<br>Do you remember the Titanic? The actress playing Old Rose has been the oldest nominee ever, she was 87.<br>C<br>There have been just three movies which have won 11 Oscars: The Return of the King (Lord of the Rings), Titanic and Ben-Hur, but only The Return of the King won all the categories for which it was nominated.<br>Two very famous celebrities have been nominated many times. Meryl Streep has been nominated 20 times. Jack Nicholson, 12 times, but they have both only won two awards.<br>D<br>One of the most famous and iconic films in history is The Godfather, starring Marlon Brando, who was the best actor nominee in 1973. He didn’t go to the ceremony and declined the award as a protest against the mistreatment of Native Americans.<br>George C. Scott also rejected his Oscar in 1971. The night of the Ceremony he was home in New York. He thought the ceremony was just a show for economic purposes. The producer received the award for him.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "1929",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "1934",
                "t17correcta": "2"
            },

            {
                "t13respuesta": "1973",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1971",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "30",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "1791",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "The first Academy Award Ceremony was in "
            },
            {
                "t11pregunta": ".<br>In "
            },
            {
                "t11pregunta": ", a five-year-old girl was given an Oscar.<br>Marlon Brando was a best actor nominee in "
            },
            {
                "t11pregunta": ".<br>George C. Scott rejected his Oscar in "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Drag and place the option that best completes the sentences.",
                        "t11instruccion": "A<br>There have been about 3000 statuettes given since the first Academy Award Ceremony in 1929. Since then, we have witnessed many memorable moments, surprises and an occasional reject. Here are some curious things about the Oscars you may not know.<br>B<br>In 1934, a five-year-old girl was given an Oscar. She has been the youngest actress to receive an award; her name, Shirley Temple. This was an honorary award for children. Forty years later, Tatum O’Neal, aged 10, received the statuette for best actress in a supporting role.<br>Christopher Plummer won the statuette at the age of 82 in 2012 for his performance in the movie Beginners. His award was for best supporting actor. In the role of best actor, the oldest winner has been Henry Fonda, he was 76.<br>Do you remember the Titanic? The actress playing Old Rose has been the oldest nominee ever, she was 87.<br>C<br>There have been just three movies which have won 11 Oscars: The Return of the King (Lord of the Rings), Titanic and Ben-Hur, but only The Return of the King won all the categories for which it was nominated.<br>Two very famous celebrities have been nominated many times. Meryl Streep has been nominated 20 times. Jack Nicholson, 12 times, but they have both only won two awards.<br>D<br>One of the most famous and iconic films in history is The Godfather, starring Marlon Brando, who was the best actor nominee in 1973. He didn’t go to the ceremony and declined the award as a protest against the mistreatment of Native Americans.<br>George C. Scott also rejected his Oscar in 1971. The night of the Ceremony he was home in New York. He thought the ceremony was just a show for economic purposes. The producer received the award for him.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "T",
                "t17correcta": "1",
            },

        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There are some pancakes on the table. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "The boy wearing a red cap is eating something.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There are two boys and one girl in the room. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There are many chairs in the room.",
                "correcta": "0"
            },

            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "All children look very much alike.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "There is fish and chicken for dinner.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Look at the picture and choose “True” or “False” based on what you see.",
            "t11instruccion": "<center><img src='EI9A_B1_R01.png'></center>",
            "descripcion": "Aspect",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Like you",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "like your",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "look like",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "like home",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "seems like",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "looks like",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "likes home",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": ""
            },
            {
                "t11pregunta": ", I prefer to have a light breakfast.<br>You look a lot "
            },
            {
                "t11pregunta": " grandfather in this picture.<br>The wax statues "
            },
            {
                "t11pregunta": " real people.<br>This hotel room really feels "
            },
            {
                "t11pregunta": ".<br>It "
            },
            {
                "t11pregunta": " yesterday, but it was three years ago.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Choose the correct option to complete the sentences.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "have to",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "don’t have to",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "has to",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "don’t have to",
                "t17correcta": "1",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "have to",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "has to",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "don’t have to",
                "t17correcta": "1",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "have",
                "t17correcta": "0",
                "numeroPregunta": "2",

            },
            {
                "t13respuesta": "have to",
                "t17correcta": "0",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "have to",
                "t17correcta": "1",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "has to",
                "t17correcta": "0",
                "numeroPregunta": "3",

            },
            {
                "t13respuesta": "don’t have to",
                "t17correcta": "0",
                "numeroPregunta": "3",

            },
            {
                "t13respuesta": "don’t have to",
                "t17correcta": "1",
                "numeroPregunta": "4",
            },
            {
                "t13respuesta": "has to",
                "t17correcta": "0",
                "numeroPregunta": "4",

            },
            {
                "t13respuesta": "have to",
                "t17correcta": "0",
                "numeroPregunta": "4",

            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Choose the option that best completes the sentence. <br><br>To play a football match we <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> wear the team uniform.",
                "You <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> get a passport to travel inside your country.",
                "Mom will drive us to school so we <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> to walk.",
                "For the annual group picture you <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> wear the formal uniform.",
                "I  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> study for the math test because I was exempted."
            ],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "I was taking a shower, so I couldn’t answer.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "I’m doing my homework.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "while Dad is taking out the trash.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "when the airplane landed.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "when the coach and the principal came in.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "When the phone rang"
            },
            {
                "t11pregunta": "I am studying while"
            },
            {
                "t11pregunta": "We are helping Mom with the kitchen"
            },
            {
                "t11pregunta": "They were waiting at the airport"
            },
            {
                "t11pregunta": "Max was in the front of the class explaining his project"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Match columns A and B to make sentences.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "was able to",
                "t17correcta": "1",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "were able",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "be able",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "were able to",
                "t17correcta": "1",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "was able to",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "wasn’t able to",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "wasn’t able to",
                "t17correcta": "1",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "were not able to",
                "t17correcta": "0",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "is able to",
                "t17correcta": "0",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "were able",
                "t17correcta": "1",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "were able to",
                "t17correcta": "0",
                "numeroPregunta": "3",

            },
            {
                "t13respuesta": "wasn’t able",
                "t17correcta": "0",
                "numeroPregunta": "3",

            },
            {
                "t13respuesta": "was able to",
                "t17correcta": "1",
                "numeroPregunta": "4",
            },
            {
                "t13respuesta": "were able to",
                "t17correcta": "0",
                "numeroPregunta": "4",

            },
            {
                "t13respuesta": "wasn’t able",
                "t17correcta": "0",
                "numeroPregunta": "4",

            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Choose the option that best completes the sentence.<br>When I was 16 years old I <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> jump over the bushes with my roller skates.",
                "My siblings <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>  ride a sliding board downhill. ",
                "When she started practising, Emily <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> play the violin as good as she does now.",
                "Mom and Dad <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> to climb high mountains as teenagers.",
                "Martin  <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> swim very fast when he was nine. He won many competitions.",
            ],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "hear",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "are having",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "likes",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "is wearing",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "lighter",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "like",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "think",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "doing",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "write",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "has",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "do",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Hi, Sandra. It’s so nice to "
            },
            {
                "t11pregunta": " from you!<br>This is my family. We "
            },
            {
                "t11pregunta": " lunch right now. My brother "
            },
            {
                "t11pregunta": " dark colors. He "
            },
            {
                "t11pregunta": " a navy T-shirt in this photo. My mom and I prefer "
            },
            {
                "t11pregunta": " colors. People say my brother looks "
            },
            {
                "t11pregunta": " my dad. Do you "
            },
            {
                "t11pregunta": " so?<br>Anyway, how are you "
            },
            {
                "t11pregunta": "? Please "
            },
            {
                "t11pregunta": " to me soon.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11instruccion": "<center><img src='EI9A_B1_R02.png' height='420px' >",
            "t11pregunta": "Drag the word to fill in the space provided.<br>",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "better",
                "t17correcta": "1",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "good",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "a good player",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "longer",
                "t17correcta": "1",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "large",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "long",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "the best",
                "t17correcta": "1",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "better",
                "t17correcta": "0",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "the most",
                "t17correcta": "0",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "farther",
                "t17correcta": "1",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "far",
                "t17correcta": "0",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "more far",
                "t17correcta": "0",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "worst",
                "t17correcta": "1",
                "numeroPregunta": "4",
            },
            {
                "t13respuesta": "bad",
                "t17correcta": "0",
                "numeroPregunta": "4",
            },
            {
                "t13respuesta": "better",
                "t17correcta": "0",
                "numeroPregunta": "4",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Choose the option that best completes the sentence.<br>Many people think that Messi is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> than Maradona.",
                "What is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>, a meter or a kilometer?",
                "The Avengers is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> movie in the world.",
                "Which house is <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> from school, yours or mine?",
                "The <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> thing that can happen to me this week is to lose my place in the premiere."
            ],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },
];
