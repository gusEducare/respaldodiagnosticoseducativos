json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Responsabilidad contigo mismo",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Responsabilidad con la familia",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Responsabilidad con la comunidad",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Responsabilidad con la comunidad",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Responsabilidad contigo mismo",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Responsabilidad con la familia",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Responsabilidad con la familia",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Responsabilidad contigo mismo",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Responsabilidad con la comunidad",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Responsabilidad contigo mismo",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Responsabilidad con la familia",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Responsabilidad con la comunidad",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Responsabilidad contigo mismo",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Responsabilidad con la familia",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Responsabilidad con la comunidad",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Responsabilidad con la comunidad",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Responsabilidad con la familia",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Responsabilidad contigo mismo",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Responsabilidad con la familia",
                "t17correcta": "1",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Responsabilidad contigo mismo",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Responsabilidad con la comunidad",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Evitar responder de manera impulsiva y violenta cuando un comentario me molesta.",
                "Citar al autor de la información e imágenes que utilizo de internet.", "Pedir autorización para compartir información de fotos, videos o ubicación de familiares y amigos.",
                "Registrar únicamente los datos necesarios, proteger mis datos personales como dirección, colegio, número telefónico y ubicación.",
                "Configurar la privacidad de cada sitio en internet a donde ingreso para protegerme.",
                "Participar en campañas para prevenir, detectar y solucionar acoso digital.",
                "Evitar asignar perfiles de parentesco familiar como: primos, tíos, hermanos, papás, etc.",
            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }


    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Derechos de autor o <i>copyright<i>",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Plagio",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Derechos de internet",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Plagio",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Piratería",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Derechos de autor",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Piratería",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Plagio",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Derechos de autor",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Son leyes que protegen los intereses de los creadores de obras como: canciones, fotos, textos, artículos, libros, programas, películas, juegos y varias más.",
                "Sucede cuando una persona se apropia de la creación de otro autor.", "Sucede cuando se realiza una copia ilegal, no autorizada por el autor de una obra."

            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": " Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al citar a un autor, se debe escribir el nombre, seguido por el apellido y el año de la publicación.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al citar a un autor de un texto de internet, se debe escribir la URL al final de la cita.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al citar a un autor, lo primero es escribir sus datos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una cita bibliográfica se debe incluir el año de nacimiento del autor.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando se trata de un mapa o ilustración, no es necesario incluir el nombre de la obra en la cita bibliográfica.",
                "correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Si la obra genera beneficios, parte de estos deben de ir al autor.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "La obra no se puede modificar o distribuir sin consentimiento del autor.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Cada vez que la obra se vea, debe mostrarse el nombre del autor.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "La obra no puede ser modificada sin consentimiento del vendedor.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Cada vez que la obra se vea, no es necesario mostrar el nombre del autor.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta.<br><br>¿Cuáles son los tres derechos que todo autor tiene sobre su obra?",
            "t11instruccion": "",
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Video educativo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Video documental",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Video de entretenimiento",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Video promocional",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Video informativo",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Pueden ser tutoriales, conferencias, demostraciones o clases."
            },
            {
                "t11pregunta": "Presenta aspectos de la vida real, empleando narraciones de los acontecimientos y entrevistas con los involucrados. Lleva una secuencia lógica de tiempo."
            },
            {
                "t11pregunta": "Su objetivo es divertir a la audiencia, existen de todo tipo."
            },
            {
                "t11pregunta": "Van dirigidos a los consumidores o usuarios, presentan campañas de nuevos productos o servicios."
            },
            {
                "t11pregunta": "Su contenido se basa en reportajes, testimoniales, entrevistas y noticias."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Producción",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Preproducción",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Postproducción",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Postproducción",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Preproducción",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Producción",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Preproducción",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Producción",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Postproducción",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br><br>En esta fase se graban las escenas del video de acuerdo al plan establecido previamente.",
                "En esta fase se realiza la edición de la película o video, mediante un software especial donde se reúnen los archivos de audio e imagen para formar el producto final.", "En esta etapa se define el objetivo, mensaje y tipo de video que se va a realizar; también se especifican la duración, el guion y los recursos."],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Escena",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Guion",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Diálogo",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Descripción de la acción",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Especifica dónde y cuándo ocurren los hechos."
            },
            {
                "t11pregunta": "Es el documento donde se describen todas las acciones que serán filmadas, incluye escenas, acciones y diálogos."
            },
            {
                "t11pregunta": "Son las frases y palabras exactas que los actores o narradores deben decir durante la grabación."
            },
            {
                "t11pregunta": "Son los detalles de los sucesos que deben ocurrir, incluye los nombres de los personajes que intervienen con mayúsculas."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },
    //////8
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITZ_B01_R08_01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='PITZ_B01_R08_02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='PITZ_B01_R08_03.png' >",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='PITZ_B01_R08_04.png' >",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Diálogo"
            },
            {
                "t11pregunta": "Escena"
            },
            {
                "t11pregunta": "Descripción de la acción"
            },
            {
                "t11pregunta": "Guion"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "250px",
            "anchoImagen": "300px",
            "evaluable": true
        }
    },
    ///////9
    /*{
      "respuestas": [
                  {
                         "t13respuesta": "GeoGebra",
              "t17correcta": "1"
          },
                  {
                         "t13respuesta": "Earth Now",
              "t17correcta": "2,5"
          },
                  {
                         "t13respuesta": "YouTube Edu",
              "t17correcta": "3"
          },
                  {
                         "t13respuesta": "Scratch",
              "t17correcta": "4"
          },
                  {
                         "t13respuesta": "NASA",
              "t17correcta": "2,5"
          },
                  {
                         "t13respuesta": "Paint",
              "t17correcta": "0"
          },
          {
                         "t13respuesta": "Audicity",
              "t17correcta": "0"
          },
                
          ],
          "preguntas": [
                {
                    "t11pregunta": "<br><style>\n\
              .table img{height: 90px !important; width: auto !important; }\n\
              </style><table class='table' style='margin-top:-10px;'>\n\
              <tr><td>Matemáticas</td><td>Programación</td><td>Geografía y Ciencias</td><td>Canales de video educativos</td></tr><tr><td>"
                },
                {
                    "t11pregunta": "</td><td>Pocket Code</td><td>"
                },
                {
                    "t11pregunta": "</td><td>"
                },
                {
                   "t11pregunta": "</td></tr><tr><td>Tocamates</td><td>"
                },
                
                {
                    "t11pregunta": "</td><td>"
                },
                {
                    "t11pregunta": "</td><td>TED</td></tr></table>"
                },
               
               
                
          ],
          "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Se puede perder información por tres motivos.<br>Arrastra cada elemento a donde corresponda.",
          "t11instruccion": "",
          "respuestasLargas": true,
          "pintaUltimaCaja": false,
          "contieneDistractores": true
      }
    },*/
    ///10

    //11
    {
        "respuestas": [

            {
                "t13respuesta": "PITZ_B01_R09_01.png",
                "t17correcta": "0,4",
                "columna": "1"
            },
            {
                "t13respuesta": "PITZ_B01_R09_02.png",
                "t17correcta": "0,4",
                "columna": "1"
            },
            {
                "t13respuesta": "PITZ_B01_R09_03.png",
                "t17correcta": "1,5",
                "columna": "1"
            },
            {
                "t13respuesta": "PITZ_B01_R09_04.png",
                "t17correcta": "1,5",
                "columna": "1"
            },
            {
                "t13respuesta": "PITZ_B01_R09_05.png",
                "t17correcta": "2,6",
                "columna": "0"
            },
            {
                "t13respuesta": "PITZ_B01_R09_06.png",
                "t17correcta": "2,6",
                "columna": "0"
            },
            {
                "t13respuesta": "PITZ_B01_R09_07.png",
                "t17correcta": "3,7",
                "columna": "0"
            },
            {
                "t13respuesta": "PITZ_B01_R09_08.png",
                "t17correcta": "3,7",
                "columna": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra la imagen al espacio que corresponda.",
            "tipo": "ordenar",
            "imagen": true,
            "url": "PITZ_B01_R09_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "evaluable": true

        },
        "contenedores": [
            { "Contenedor": ["", "130,20", "cuadrado", "139, 140", ".", "transparent"] },//h   0  0,1,4,5
            { "Contenedor": ["", "130,173", "cuadrado", "139, 140", ".", "transparent"] },//h  1  2,3,6,7
            { "Contenedor": ["", "130,327", "cuadrado", "139, 140", ".", "transparent"] },//s  2
            { "Contenedor": ["", "130,481", "cuadrado", "139, 140", ".", "transparent"] },//s  3
            { "Contenedor": ["", "302,20", "cuadrado", "139, 140", ".", "transparent"] },//h   4
            { "Contenedor": ["", "302,173", "cuadrado", "139, 140", ".", "transparent"] },//h  5
            { "Contenedor": ["", "302,327", "cuadrado", "139, 140", ".", "transparent"] },//s  6
            { "Contenedor": ["", "302,481", "cuadrado", "139, 140", ".", "transparent"] },//s  7

        ]
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": " Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Puedes utilizar los dispositivos en las horas de comida.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Desconectarse por completo un día a la semana.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al irse a dormir por la noche, se deben apagar los dispositivos y alejarlos. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No es necesario limitar el tiempo de uso de los dispositivos o conexión a internet.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Evitar uso de dispositivos en reuniones, conversaciones y relaciones familiares o de amistad.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mientras buscas un tema, puedes desviarte viendo otras cosas que tu dispositivo e internet te ofrecen.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Marca si las medidas para prevenir la ciberadicción son falsas o verdaderas.",
            "descripcion": "Medidas",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Software de edición de video",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Toma de video",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Clip de video",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Toma",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Escena",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Claqueta",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Clip",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Toma",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Escena",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Editar o edición",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Duración",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Tema",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Duración",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Editar o edición",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Tema",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Se utiliza para unir varios clips de video en una sola película.",
                "Es cada una de las grabaciones de una escena para video.",
                 "Es un conjunto formado por audio, video, imágenes y texto.",
                 "En esta fase de la producción se agregar los efectos, transiciones, subtítulos y otras acciones al video.",
                 "En esta fase de la producción del video puedes recortar, acercar, alejar, rotar, copiar el clip entre otras cosas."
               ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
// reactivo 12

{
    "respuestas": [
        {
            "t13respuesta": "Efectos",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "Transición",
            "t17correcta": "2",
        },
        {
            "t13respuesta": "Corte directo",
            "t17correcta": "3",
        },
        {
            "t13respuesta": "Titulaje",
            "t17correcta": "4",
        },
        {
            "t13respuesta": "Edición de audio",
            "t17correcta": "5",
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "Efectos especiales que le dan una apariencia distinta a un clip de video."
        },
        {
            "t11pregunta": "Es un cambio temporal al pasar de un clip a otro."
        },
        {
            "t11pregunta": "Sucede cuando dos clips de video se colocan uno después del otro."
        },
        {
            "t11pregunta": "Son los cambios de estilos de texto, color, fuente de los clips de video."
        },
        {
            "t11pregunta": "Es la aplicación de música multimedia, efectos de sonido en los clips de video."
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda.",
        "t11instruccion": "",
        "altoImagen": "100px",
        "anchoImagen": "200px",
        "evaluable": true
    }
},
//reactivo 13
{
    "respuestas": [
        {
            "t13respuesta": "Navegador GPS",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Internet",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Satélite",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },

        {
            "t13respuesta": "Mapas",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "Smartphone",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "Navegador",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "Calles, puntos de interés, publicidad comercial y ubicación actual",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "Ubicación actual, centros comerciales y datos personales",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "Calles, puntos de interés, direcciones y nombres de las personas",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "PIN",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Satélite",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Ubicación",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },



    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Permite buscar un destino a través de mapas satelitales, es conocido como Sistema de Posicionamiento Global.",
            "Es el elemento fundamental para que el navegador GPS funcione.",
             "Es la información que se puede identificar con el navegador GPS.",
             "Es el punto indicado en un mapa."
           ],
        "t11instruccion": "",
        "contieneDistractores": true,
        "preguntasMultiples": true,
    }
},
{
    "respuestas": [
        {
            "t13respuesta": "Earth y Map",
            "t17correcta": "1",
        },
        {
            "t13respuesta": "Medir distancias",
            "t17correcta": "2",
        },
        {
            "t13respuesta": "Progreso de viaje",
            "t17correcta": "3",
        },
        {
            "t13respuesta": "La mejor ruta",
            "t17correcta": "4",
        },
        {
            "t13respuesta": "Tráfico",
            "t17correcta": "5",
        },
        {
            "t13respuesta": "Tus sitios",
            "t17correcta": "6",
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "Vistas básicas de Google Maps."
        },
        {
            "t11pregunta": "Estimación aproximada de la distancia mientras te trasladas de un punto A a un punto B."
        },
        {
            "t11pregunta": "Permite compartir ubicación y posición con otras personas mientras tomas una ruta hasta que llegas a tu destino."
        },
        {
            "t11pregunta": "Permite ver y comparar varios medios de transporte para encontrar el  mejor camino hacia el lugar indicado."
        },
        {
            "t11pregunta": "Muestra el flujo de vehículos en las rutas como: accidentes, construcciones, cierres de rutas, otros incidentes."
        },
        {
            "t11pregunta": "Permite personalizar los sitios que frecuentas y son de tu preferencia."
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda.",
        "t11instruccion": "",
        "altoImagen": "100px",
        "anchoImagen": "200px",
        "evaluable": true
    }
},


];
