json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Vedadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la Web 2.0, cualquier persona puede crear y modificar el contenido que está en línea en cualquier momento. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunos ejemplos de la Web 2.0 son las redes sociales y los sitios de intercambio de video. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunos ejemplos de la Web 2.0 son los correos electrónicos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una ventaja de la Web 2.0 es que al trabajar en equipo y a distancia, podemos crear trabajos con mayor calidad. ",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",

            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Redes sociales",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Repositorio de videos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Videoconferencias",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Mapas",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Estos espacios de la web 2.0 sirven para comunicar a personas, organizaciones y comunidades con intereses en común. "
            },
            {
                "t11pregunta": "Esta herramienta de la web 2.0 se usa para archivar videos que comunican o enseñan algo a millones de personas. "
            },
            {
                "t11pregunta": "Para acercar amigos y familiares la web 2.0 usa esta herramienta para mantener una conversación virtual a distancia por medio de video y audio. "
            },
            {
                "t11pregunta": "Estas aplicaciones de la web 2.0 se usan para encontrar direcciones o para hacer recorridos virtuales."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "150",
            evaluable: true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Ciberbullying",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Grooming",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Malware",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Phishing",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Sucede cuando una persona molesta, humilla o persigue a otra por medio de mensajes en redes sociales, páginas de internet, videos o fotos. "
            },
            {
                "t11pregunta": "Cuando un adulto busca ganarse la confianza de un menor para después pedirle que se conozcan en persona o que realice actividades inapropiadas. "
            },
            {
                "t11pregunta": "Sucede cuando nuestro dispositivo está en peligro de ser dañado por un software malicioso o virus. "
            },
            {
                "t11pregunta": "Cuando alguien más trata de obtener datos bancarios, contraseñas o información personal haciéndose pasar por instituciones de confianza."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "150",
            evaluable: true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Ciberacoso",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Phishing",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Grooming ",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Phishing",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Ciberacoso",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Scam",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Grooming",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Ciberacoso",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Phishing",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Scam",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Phishing",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Grooming ",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Paty recibe por redes sociales mensajes de desconocidos, donde la atacan y la humillan, haciéndola sentir triste y sola.",
                "<br><br>A la señora Andrea, le han llegado correos donde le avisan que se ganó un premio y para entregárselo debe dar sus datos personales y los de su cuenta bancaria.",
                "<br><br>Luis tiene un amigo con quien platica de fútbol por medio del chat, aunque nunca se han visto en persona el amigo le pide que le mande fotos de su familia y los lugares que visitan los fines de semana.",
                "<br><br>A Toño le llegan muchos mensajes cada semana, donde le piden que done algún dinero para una fundación de niños abandonados y a cambio le darán un premio."
            ],
            "preguntasMultiples": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Anti-ético",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Ético",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mandar mensajes desde una cuenta falsa, para hacerse pasar por otra persona.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tomar fotos de una persona y publicarlas sin su autorización. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No escribir nada en las redes sociales de otras personas aunque dejen sus sesiones abiertas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Copiar la tarea de otra persona y entregarla como tuya.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Siempre que utilizamos información de internet para una tarea se debe citar al autor. ",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",

            "t11pregunta": "<p>Elige si los actos de la lista son éticos o anti-éticos.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITD_B01_R06_01.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R06_02.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R06_03.png'>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R06_04.png'>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": ".jpg "
            },
            {
                "t11pregunta": ".mp3"
            },
            {
                "t11pregunta": ".avi / .mp4 "
            },
            {
                "t11pregunta": ".doc"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "150"
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITD_B01_R07_01.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R07_02.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R07_03.png'>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R07_04.png'>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R07_05.png'>",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><br><style>\n\
.table img{height: 90px !important; width: auto !important; }\n\
</style><table class='table' style='margin-top:-40px;'>\n\
<tr>\n\
<td style='width:150px'>Archivos de texto</td>\n\
<td style='width:100px'>Archivos de video</td>\n\
<td style='width:100px'>Archivos de imagen</td>\n\
<td style='width:100px'>Presentaciones con diapositivas</td>\n\
<td style='width:100px'>Hojas de cálculo</td>\n\
</tr>\n\
<tr><td>\n\ "},
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> </tr></table>"
            }
        ],

        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Barra de herramientas",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Área del documento",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Menú Inicio ",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Procesador de texto",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Programa de presentaciones",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Hoja de cálculo",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Área del documento",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Barra de herramientas",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Menú Inicio",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Programa de presentaciones",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Procesador de texto",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Procesador de texto",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Formato",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Edición",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Estilos ",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Edición",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Formato",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Estilos",
                "t17correcta": "0",
                "numeroPregunta": "5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br> Contiene las opciones más usadas para editar un texto y la encuentras en la interfaz de usuario.",
                "<br><br>Es un programa que utilizas para crear y dar formato a documentos de texto.",
                "<br><br>Es la parte donde puedes escribir en el documento, agregar imágenes y todo el texto que desees.",
                "<br><br>Es un programa que utilizas para crear presentaciones multimedia con diapositivas.",
                "<br><br>Te ayuda a darle presentación al texto de un documento como la fuente, tamaño y color.",
                "<br><br>Es la modificación del contenido de un documento (eliminar texto, cortar o mover párrafos)."
            ],
            "preguntasMultiples": true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Archivo",
                "t17correcta": "1,4,7"
            },
            {
                "t13respuesta": "Insertar",
                "t17correcta": "1,4,7"
            },
            {
                "t13respuesta": "Formato",
                "t17correcta": "1,4,7"
            },
            {
                "t13respuesta": "Ortografía",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "Tipo de Fuente",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "Alinear",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "Arial",
                "t17correcta": "3,6,9"
            },
            {
                "t13respuesta": "Calibri",
                "t17correcta": "3,6,9"
            },
            {
                "t13respuesta": "Comic Sans MS",
                "t17correcta": "3,6,9"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><br><style>\n\
.table img{height: 90px !important; width: auto !important; }\n\
</style><table class='table' style='margin-top:-40px;'>\n\
<tr>\n\
<td style='width:150px'>Títulos de menús:</td>\n\
<td style='width:100px'>Nombres de herramientas:</td>\n\
<td style='width:100px'>Nombres de fuentes:</td>\n\
</tr>\n\
<tr><td>\n\ "},
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> </tr><tr><td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> </tr><tr><td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td> </tr></table>"
            }
        ],

        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": true,
            "contieneDistractores": true
        }
    },
    //10
    {
        "respuestas": [
            {
                "t17correcta": "1"
            },
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "4"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Barra de menús "
            },
            {
                "t11pregunta": " <br>Área de diapositivas activa "
            },
            {
                "t11pregunta": " <br>Barra de herramientas "
            },
            {
                "t11pregunta": " <br>Miniaturas de diapositivas "
            },
            {
                "t11pregunta": " <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Observa la siguiente imagen y escribe el número que corresponda a cada elemento de la interfaz.<br><img src='PITD_B01_R10_00.png'>",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITD_B01_R11_01.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R11_02.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R11_03.png'>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R11_04.png'>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R11_05.png'>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Texto en cursiva"
            },
            {
                "t11pregunta": "Texto en negrita"
            },
            {
                "t11pregunta": "Lista numerada"
            },
            {
                "t11pregunta": "Color de fuente"
            },
            {
                "t11pregunta": "Lista con viñetas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "150",
            evaluable: true
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Vedadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El procesador de textos te ayuda a dar formato a los documentos de texto.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El formato de un documento de texto, sirve para cambiar su contenido.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un programa de presentaciones no te ayuda a crear diapositivas, solo sirve para trabajar con documentos de texto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La edición de un documento te ayuda a eliminar texto, cortar y mover párrafos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los elementos multimedia pueden ser: fotografías, ilustraciones, audio y videos.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",

            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable": true
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITD_B01_R13_01.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R13_02.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R13_03.png'>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R13_04.png'>",
                "t17correcta": "4"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Son herramientas que te permiten editar el color de relleno y contorno de las formas."
            },
            {
                "t11pregunta": "Es la herramienta con la que se pueden insertar imágenes desde el equipo."
            },
            {
                "t11pregunta": "Es la herramienta con la que se pueden insertar formas predefinidas."
            },
            {
                "t11pregunta": "Es la herramienta con la que se puede insertar un cuadro de texto."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "150",
            evaluable: true
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Vedadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para insertar una imagen es necesario tener conexión a internet.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las formas pueden contener mensajes de texto.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El procesador de textos no permite incluir imágenes",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre los elementos que se pueden insertar, hay formas, imágenes, video y cuadro de texto.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Agregar formas e imágenes te ayuda a enriquecer el contenido de un documento o presentación.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No se puede modificar la apariencia y posición de las formas insertadas.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",

            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable": true
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "Bots",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Navegadores",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Internet",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Análisis, memoria y toma de decisiones",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Memoria, repetición y rapidez",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Toma de decisiones, rapidez y repetición",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Inteligencia artificial",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Computación",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Internet",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Son programas informáticos autónomos que tienen la capacidad de realizar tareas concretas, repetitivas de forma rápida y eficiente, incluso imitar el comportamiento humano.",
                "<br><br>Un objeto, es “inteligente” cuando realiza los procesos siguientes:",
                "<br><br>Es un agente racional que observa el entorno y lleva a cabo acciones que aumentan la posibilidad de éxito para alcanzar un objetivo."
            ],
            "preguntasMultiples": true
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITD_B01_R16_02.png'>",
                "t17correcta": "0",
                "columna":"0"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R16_01.png'>",
                "t17correcta": "1",
                "columna":"0"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R16_05.png'>",
                "t17correcta": "2",
                "columna":"1"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R16_04.png'>",
                "t17correcta": "3",
                "columna":"1"
            },
            {
                "t13respuesta": "<img src='PITD_B01_R16_03.png'>",
                "t17correcta": "4",
                "columna":"0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Observa la siguiente imagen y escribe el número que corresponda a cada elemento de la interfaz.<br><br><table >\n\
            <tr><td>1. Columnas</td>\n\
            <tr><td>2. Filas</td>\n\
            <tr><td>3. Celda</td>\n\
            <tr><td>4. Hojas</td>\n\
            <tr><td>5. Barra de fórmulas</td></table>",
            "t11instruccion": "",
            "tipo": "ordenar",
            "imagen": true,
            "url":"PITD_B01_R16_00.png",
            "respuestaImagen":true,
            "tamanyoReal":true,
            "bloques":false,
            "borde":false


        },
        "contenedores": [
            {"Contenedor": ["", "340,54", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "145,255", "cuadrado", "50, 51", ".","transparent"]},
            {"Contenedor": ["", "225,359", "cuadrado", "50, 51", ".","transparent"]},
            {"Contenedor": ["", "403,97", "cuadrado", "50, 51", ".","transparent"]},
            {"Contenedor": ["", "341,452", "cuadrado", "50, 51", ".","transparent"]}
        ]
    },
    //17
     {
            "respuestas": [        
                {
                   "t13respuesta":  "libros",
                     "t17correcta": "1" 
                 },
                {
                   "t13respuesta":  "filas",
                     "t17correcta": "2" 
                 },
                {
                   "t13respuesta":  "columnas",
                     "t17correcta": "3" 
                 },
                {
                   "t13respuesta":  "celdas",
                     "t17correcta": "4" 
                 },
                {
                   "t13respuesta":  "tablas",
                     "t17correcta": "5" 
                },
             {
                       "t13respuesta":  "fórmulas",
                         "t17correcta": "6" 
                    },
                 {
                           "t13respuesta":  "opciones",
                             "t17correcta": "7" 
                        },
            ],
            "preguntas": [ 
              {
              "t11pregunta": "Los archivos de la hoja de cálculo de llaman "
              },
              {
              "t11pregunta": ". Un libro puede contener varias hojas, las cuales tienen "
              },
              {
              "t11pregunta": " horizontales identificadas por números y "
              },
              {
              "t11pregunta": " verticales identificadas por letras, las cuales forman muchas "
              },
              {
              "t11pregunta": ".<br>"
              },
            ],
            "pregunta": {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "Completa el párrafo con las palabras del recuadro.", 
               "t11instruccion": "", 
               "respuestasLargas": true, 
               "pintaUltimaCaja": false, 
               "contieneDistractores": true 
             } 
          } 
];