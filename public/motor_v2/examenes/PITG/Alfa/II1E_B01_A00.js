json = [
    //arrasta imagen
    
   {
        "respuestas": [
            {
                "t13respuesta": "1",
                "t17correcta": "0",
                "columna":"0"
            },
            {
                "t13respuesta": "2",
                "t17correcta": "1",
                "columna":"0"
            },
            {
                "t13respuesta": "3",
                "t17correcta": "2",
                "columna":"1"
            },
            {
                "t13respuesta": "4",
                "t17correcta": "3",
                "columna":"1"
            },
            {
                "t13respuesta": "5",
                "t17correcta": "4",
                "columna":"0"
            },
            {
                "t13respuesta": "6",
                "t17correcta": "5",
                "columna":"0"
            },
            {
                "t13respuesta": "7",
                "t17correcta": "6",
                "columna":"0"
            },
            {
                "t13respuesta": "8",
                "t17correcta": "7",
                "columna":"1"
            },
            {
                "t13respuesta": "9",
                "t17correcta": "8",
                "columna":"1"
            },
            {
                "t13respuesta": "10",
                "t17correcta": "9",
                "columna":"0"
            },
            {
                "t13respuesta": "11",
                "t17correcta": "10",
                "columna":"0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra cada elemento donde corresponda.<br><\/p><br><table border=1 cellspacing=0 cellpadding=0 width='100%' >\n\
        <tr><td>1. Pantalla táctil</td><td>7. Lente de la cámara trasera</td></tr>\n\
        <tr><td>2. Botón encendido/apagado</td><td>8. Bocinas o altavoz</td></tr>\n\
        <tr><td>3. Botón de volumen</td><td>9. Micrófono</td></tr>\n\
        <tr><td>4. Boton de inicio</td><td>10. Salida de audio para auriculares </td></tr>\n\
        <tr><td>5. Tarjeta de memoria</td><td rowspan='2'>11. Conector cargador</td></tr>\n\
        <tr><td>6. Cámara frontal</td></tr>\n\</table><br>",
            "t11instruccion": "",
            "tipo": "ordenar",
            "imagen": true,
            "url":"PITA_B01_R01_01.png",
            "respuestaImagen":true,
            "tamanyoReal":true,
            "bloques":false,
            "borde":false


        },
        "contenedores": [
            {"Contenedor": ["", "297,15", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "102,263", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "239,262", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "410,56", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "164,541", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "121,115", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "103,431", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "374,539", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "386,226", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "303,578", "cuadrado", "50, 50", ".","transparent"]},
            {"Contenedor": ["", "52,180", "cuadrado", "50, 50", ".","transparent"]}
        ]
    },
    //repuesta multiple
     { 
 "respuestas":[ 
{ 
 "t13respuesta":"Gabinete", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Pantalla", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Teclado", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Mouse", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Bocinas", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Micrófono", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Impresora multifuncional", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Internet", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"2", 
 "t11pregunta": "Selecciona todas las respuestas correctas.<br><br> ¿Cuáles son las cuatro partes básicas de la computadora?", 
 "t11instruccion": "", 
 } 
 }, 



    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con un dispositivo digital puedo jugar.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con un dispositivo digital puedo buscar información.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Con un dispositivo digital puedo imprimir trabajos.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Puedo darle uso rudo a un dispositivo digital.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Puedo visitar sitios por internet con un dispositivo digital",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Puedo experimentar desarmando un dispositivo digital.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Pregunta",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //3 arrastra imagen
    {
        "respuestas": [
            {
                "t13respuesta": "Postura correcta al<br>utilizar una tablet.",
                "t17correcta": "0",
                "columna":"0"
            },
            {
                "t13respuesta": "Postura correcta al utilizar una computadora.",
                "t17correcta": "1",
                "columna":"0"
            },
            {
                "t13respuesta": "Postura incorrecta al<br>utilizar una tablet. ",
                "t17correcta": "2",
                "columna":"1"
            },
            {
                "t13respuesta": "Postura incorrecta al utilizar una computadora.",
                "t17correcta": "3",
                "columna":"1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra la imagen al espacio que corresponda.<br>Determina las posturas correctas e incorrectas al utilizar una computadora o una tablet.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url":"PITA_B01_R03.png",
            "respuestaImagen":true,
            "tamanyoReal":true,
            "bloques":false,
            "borde":false

        },
        "contenedores": [
            {"Contenedor": ["", "205,30", "cuadrado", "300, 50", ".","transparent"]},
            {"Contenedor": ["", "430,5", "cuadrado", "310, 68", ".","transparent"]},
            {"Contenedor": ["", "205,350", "cuadrado", "300, 50", ".","transparent"]},
            {"Contenedor": ["", "430,355", "cuadrado", "310, 68", ".","transparent"]}
        ]
    },


    // opcion multiple
 { 
 "respuestas":[ 
 { 
 "t13respuesta":"Cuidar nuestra postura.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Elegir un buen lugar y cuidar la computadora.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Elegir un buen escritorio.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Elegir la mejor computadora o tableta.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Selecciona la respuesta correcta. <br><br>Para cuidar nuestra salud cuando utilizamos la computadora o tableta, lo más importante es:", 
 "t11instruccion": "", 
 } 
 },


    //4
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITA_B01_R04_01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='PITA_B01_R04_02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='PITA_B01_R04_03.png'>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='PITA_B01_R04_04.png'>",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Arrastrar"
            },
            {
                "t11pregunta": "Apuntar"
            },
            {
                "t11pregunta": "Clic(tocar)"
            },
            {
                "t11pregunta": "Doble clic(tocar dos veces)"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "200px",
            "anchoImagen": "200px"
        }
    },
    // relaciona columna
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITA_B01_R04_a.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='PITA_B01_R04_b.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='PITA_B01_R04_c.png'>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='PITA_B01_R04_d.png'>",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Arrastrar (deslizar)"
            },
            {
                "t11pregunta": "Doble toque"
            },
            {
                "t11pregunta": "Tocar (apuntar)"
            },
            {
                "t11pregunta": "Agrandar"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Escribe en el recuadro la letra que corresponda.",
            "t11instruccion": "",
            "altoImagen": "200px",
            "anchoImagen": "200px"
        }
    },
    // opcion multiple

    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Teclas especiales, numéricas y alfanuméricas.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Teclas numéricas, con signos y de espacios.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Teclas del abecedario, de signos y espacios.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>Un teclado se divide en las siguientes secciones: ",],
            "t11instruccion": "",
            "contienDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Teclas numéricas",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Teclas especiales",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Teclas alfanuméricas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Teclas alfanuméricas",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Teclas especiales",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Teclas numéricas",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["¿A qué sección del teclado corresponden las teclas? Selecciona la respuesta correcta.<br><br><br><center><img src='PITA_B01_R06_01.png'></center><br>",
                "<br><br><center><img src='PITA_B01_R06_02.png'></center><br>",],
            "t11instruccion": "",
            "contienDistractores": true,
            "preguntasMultiples": true,
        }
    },


    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Para separar las palabras con espacios se debe usar esta tecla.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Cuando por error escribiste una letra y quieres borrarla, debes utilizar esta tecla. ",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Cuando solo quieres escribir una letra en mayúscula, esta tecla te puede servir.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Cuando quieres iniciar un nuevo párrafo, cuando pones un “punto y aparte” o para saltar al siguiente renglón debes usar esta tecla.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='PITA_B01_R07_01.png'>"
            },
            {
                "t11pregunta": "<img src='PITA_B01_R07_02.png'>"
            },
            {
                "t11pregunta": "<img src='PITA_B01_R07_03.png'>"
            },
            {
                "t11pregunta": "<img src='PITA_B01_R07_04.png'>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "anchoImagen": "200px"
        }
    },


    //8 pendiente de imagenes

    {
      "respuestas": [
           {
               "t13respuesta": "<img src='PITA_B01_R08_05.png'>",
               "t17correcta": "0",
               "etiqueta":""
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_01.png'>",
               "t17correcta": "1",
               "etiqueta":""
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_02.png'>",
               "t17correcta": "2",
               "etiqueta":""
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_08.png'>",
               "t17correcta": "3",
               "etiqueta":"5"
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_03.png'>",
               "t17correcta": "4",
               "etiqueta":"6"
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_07.png'>",
               "t17correcta": "5",
               "etiqueta":"7"
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_04.png'>",
               "t17correcta": "6",
               "etiqueta":"8"
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_06.png'>",//n
               "t17correcta": "7",
               "etiqueta":"8"
           },
           {
               "t13respuesta": "<img src='PITA_B01_R08_09.png'>",//m
               "t17correcta": "8",
               "etiqueta":"8"
           }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra cada elemento donde corresponda.",
          "tipo": "ordenar",
          "imagen": true,
          
          "url":"PITA_B01_R08_00.png",
         // "altoImagen":1,
          //"anchoImagen":20,
         "respuestaImagen":true, 
          "tamanyoReal":false,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "120,145", "cuadrado", "40, 80", ".","transparent"]},
          {"Contenedor": ["", "215,68", "cuadrado", "40, 80", ".","transparent"]},
          {"Contenedor": ["", "215,112", "cuadrado", "40, 80", ".","transparent"]},
          {"Contenedor": ["", "215,425", "cuadrado", "40, 80", ".","transparent"]},
           {"Contenedor": ["", "120,368", "cuadrado", "40, 80", ".","transparent"]},
          {"Contenedor": ["", "120,412", "cuadrado", "40, 80", ".","transparent"]},
          {"Contenedor": ["", "120,457", "cuadrado", "40, 80", ".","transparent"]},
         {"Contenedor": ["", "310,380", "cuadrado", "40, 80", ".","transparent"]},//n,
          {"Contenedor": ["", "310,335", "cuadrado", "40, 80", ".","transparent"]}//m
      ]
  }
,



    //9falso o verdadero
      {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se le llama Pantalla de Inicio a la pantalla principal de trabajo del sistema operativo.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La pantalla de inicio se creó con la intención de acceder de una forma más fácil y rápida a los programas que tiene un dispositivo.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La pantalla de inicio de tu dispositivo siempre es la misma, aunque el sistema operativo sea diferente.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los íconos de la pantalla de inicio contienen: aplicaciones, carpetas de archivos, programas, papelera, etcétera.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La pantalla de inicio del dispositivo no puede personalizarse.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la pantalla de inicio se encuentran los íconos de los programas que más utilizas",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "t11instruccion": "",
            "descripcion": "Pregunta",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //relaciona lineas

 { 
 "respuestas":[ 
{ 
 "t13respuesta":"<img src='PITA_B01_R10_01.png'>", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R10_02.png'>", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R10_03.png'>", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R10_04.png'>", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R10_05.png'>", 
 "t17correcta":"5", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R10_06.png'>", 
 "t17correcta":"6", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Bandeja de reciclaje" 
 }, 
{ 
 "t11pregunta":"Navegador" 
 }, 
{ 
 "t11pregunta":"Archivos" 
 }, 
{ 
 "t11pregunta":"Imágenes" 
 }, 
{ 
 "t11pregunta":"Calculadora" 
 }, 
{ 
 "t11pregunta":"Calendario" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11instruccion": "", 
   "t11pregunta": "Relaciona las columnas según corresponda.",
 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } 
,

    //11 Opcion multiple 
  
    {  
      "respuestas":[
         {  
            "t13respuesta": "Un rectángulo que se despliega cuando abres una carpeta o una aplicación en tu dispositivo",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Una carpeta o una aplicación en tu dispositivo",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Un menú que despliega opciones",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
       


         {  
            "t13respuesta": "Maximizar, minimizar y cerrar",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Deformar, eliminar y cerrar",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Enfatizar, achicar y ver",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },




          {  
            "t13respuesta": "Barra de título",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Barra de desplazamiento",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Botones de Control",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         



         {  
            "t13respuesta": "Botones de Control",
            "t17correcta": "1",
            "numeroPregunta":"3"
         }, {  
            "t13respuesta": "Botones anterior y siguiente",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }, {  
            "t13respuesta": "Barra de desplazamiento",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Una ventana en la computadora es:",
                         "Las ventanas de la computadora se pueden:",
                         "Muestra el nombre del documento y te permite arrastrar la ventana para moverla:",
                         "Te permiten maximizar, minimizar y cerrar las ventanas:"
                         ],
         "preguntasMultiples": true,
         
      }
   },

//12 relaciona lineas
{
        "respuestas": [
            {
                "t13respuesta": "Guardar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Abrir",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Carpetas",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Documento",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Usamos esta opción para no perder la información que tenemos en un documento, en algunas aplicaciones funciona de manera automática.",
            },
            {
                "t11pregunta": "Con esta opción podemos visualizar un documento guardado anteriormente."
            },
            {
                "t11pregunta": "Nos permite organizar los documentos."
            },
            {
                "t11pregunta": "Se le llama así a un trabajo realizado en una aplicación, por ejemplo; texto, imagen, podcast, video, presentación con diapositivas, hoja de cálculo, etcétera."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."

        }
    },
    //13 Relsciona lineas 
    { 
 "respuestas":[ 
{ 
 "t13respuesta":"<img src='PITA_B01_R13_01.png'>", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R13_02.png'>", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R13_03.png'>", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R13_04.png'>", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"<img src='PITA_B01_R13_05.png'>", 
 "t17correcta":"5", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Video" 
 }, 
{ 
 "t11pregunta":"Audio" 
 }, 
{ 
 "t11pregunta":"Texto" 
 }, 
{ 
 "t11pregunta":"Presentaciones" 
 }, 
{ 
 "t11pregunta":"Hoja de cálculo" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
   "t11pregunta": "Relaciona las columnas según corresponda.",
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } 







]
