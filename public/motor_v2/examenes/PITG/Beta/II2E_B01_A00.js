
json = [
    //1 RELACIONAR COLUMNAS
    {
        "respuestas": [
            {
                "t13respuesta": "Dispositivo móvil multimedia",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Phablet",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Laptop",
                "t17correcta": "3"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Vicky necesita un dispositivo que le permita escuchar música y tener una gran variedad de canciones cuando va al supermercado con su madre.",
            },
            {
                "t11pregunta": "Mario, de 10 años, solo quiere que su dispositivo tenga una pantalla de tamaño regular pero que pueda hacer llamadas, escuchar algunas canciones y ver algunos videos."
            },
            {
                "t11pregunta": "Ricardo estudia en secundaria y necesita un dispositivo poderoso con teclado completo, pero sobre todo con acceso a internet y pantalla grande porque va a trabajar con videos."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."

        }
    },

    //2 OPCION MULTIPLE
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E32 Gb\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E8 Gb \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E250 Gb  \u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Beto va a salir de vacaciones y quiere llevar 20 juegos en su dispositivo. ¿Qué cantidad de Gb necesita almacenar?"
        }
    },

    //3 OPCION MULTIPLE
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E250 Gb\u003C\/p\u003E",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E32 Gb \u003C\/p\u003E",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E16 Gb \u003C\/p\u003E",
                "t17correcta": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Anita necesita hacer un trabajo en la escuela con texto, imágenes, videos y acceso a internet, ella necesita un dispositivo con un almacenamiento de:"
        }
    },
    //4 CRELACIONA COLUMNAS
    {
        "respuestas": [
            {
                "t13respuesta": "Se usan para llevar un control de pagos de inscripciones, colegiaturas y como herramienta para aprender.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se usan para monitorear los despegues y aterrizajes.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se usan para realizar animaciones y editar los programas.",
                "t17correcta": "3"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Escuela",
            },
            {
                "t11pregunta": "Torre de control aéreo"
            },
            {
                "t11pregunta": "Estudio de TV"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda con las funciones que realizan las computadoras en cada lugar."

        }
    },
    // //5 RELACIONA COLUMNAS
    {
        "respuestas": [
            {
                "t13respuesta": "Evitar encorvar la espalda y utilizar el dispositivo a la altura de los ojos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Evitar sostener el dispositivo con una sola mano.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Evitar poner el brillo muy alto y además descansar la vista durante 5 minutos cada media hora.",
                "t17correcta": "3"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Dolor de espalda",
            },
            {
                "t11pregunta": "Dolor de muñeca"
            },
            {
                "t11pregunta": "Vista cansada"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda con  las recomendaciones para cuidar tu salud al utilizar una tableta o computadora."

        }
    },
    // 6 CRELACIONA COLUMNAS
    {
        "respuestas": [
            {
                "t13respuesta": '<img src="PITB_B01_R06_01.png" >',
                "t17correcta": "1"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R06_02.png" >',
                "t17correcta": "2"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R06_03.png" >',
                "t17correcta": "3"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": " 08:45",
            },
            {
                "t11pregunta": "01:15"
            },
            {
                "t11pregunta": "11:30"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda con la hora que representa cada reloj digital.",
            "altoImagen": 100,
            "anchoImagen": 100

        }
    },
    // 7 CRELACIONA COLUMNAS
    {
        "respuestas": [
            {
                "t13respuesta": " Ver videos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Leer artículos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Escuchar audios",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Resolver ejercicios",
                "t17correcta": "4"
            }

        ],
        "preguntas": [
            {
                "t11pregunta": '<img src="PITB_B01_R07_01.png" >',

            },
            {
                "t11pregunta": '<img src="PITB_B01_R07_02.png" >'
            },
            {
                "t11pregunta": '<img src="PITB_B01_R07_03.png" >'
            },
            {
                "t11pregunta": '<img src="PITB_B01_R07_04.png" >'
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona los íconos con los recursos tecnológicos que te ayudan a aprender en todas las materias.<br> Relaciona las columnas según corresponda.",
            "altoImagen": 100,
            "anchoImagen": 100

        }
    },
    //8 ordenar elementos
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Minuto<\/p>",
                "t17correcta": "0",
                etiqueta: "menor"
            },
            {
                "t13respuesta": "<p>Hora<\/p>",
                "t17correcta": "1",

            },
            {
                "t13respuesta": "<p>Día<\/p>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<p>Semana<\/p>",
                "t17correcta": "3",

            },
            {
                "t13respuesta": "<p>Mes<\/p>",
                "t17correcta": "4",
                etiqueta: "mayor"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena las unidades de tiempo de menor a mayor.",

        }
    },

    // 9 CRELACIONA COLUMNAS
    {
        "respuestas": [
            {
                "t13respuesta": '<img src="PITB_B01_R10_01.png" >',
                "t17correcta": "1"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R10_02.png" >',
                "t17correcta": "2"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R10_03.png" >',
                "t17correcta": "3"
            },


        ],
        "preguntas": [
            {
                "t11pregunta": '<img src="PITB_B01_R10_05.png" >',

            },
            {
                "t11pregunta": '<img src="PITB_B01_R10_04.png" >'
            },
            {
                "t11pregunta": '<img src="PITB_B01_R10_06.png" >'
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada mensaje de la ventana con el ícono.",
            "altoImagen": 150,
            "anchoImagen": 150

        }
    },
    // 10 CRELACIONA COLUMNAS
    {
        "respuestas": [
            {
                "t13respuesta": '<img src="PITB_B01_R11_05.png" >',
                "t17correcta": "1"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R11_06.png" >',
                "t17correcta": "2"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R11_02.png" >',
                "t17correcta": "3"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R11_04.png" >',
                "t17correcta": "4"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R11_03.png" >',
                "t17correcta": "5"
            },
            {
                "t13respuesta": '<img src="PITB_B01_R11_01.png" >',
                "t17correcta": "6"
            },


        ],
        "preguntas": [
            {
                "t11pregunta": "Información y consejos",

            },
            {
                "t11pregunta": "Éxito"
            },
            {
                "t11pregunta": "Trabajar sin conexión"
            },
            {
                "t11pregunta": "Error"
            },
            {
                "t11pregunta": "Advertencia y seguridad"
            },
            {
                "t11pregunta": "Notificaciones"
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada ícono con el tipo de mensaje que le corresponda.",
            "altoImagen": "80px",
            "anchoImagen": "80px"

        }
    },
  //reactivo11


  {
      "respuestas": [
          {
              "t13respuesta": "Es una serie ordenada de pasos, que hay que seguir para lograr una meta.",
              "t17correcta": "1",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Una serie de pasos sin orden para realizar alguna actividad.",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Una lista de instrucciones para realizar un trabajo y lograr una meta.",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Modificar la apariencia de los párrafos de un texto, como color y tamaño de fuente.",
              "t17correcta": "1",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Modificar la forma del documento",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Modificar los márgenes y tamaño del documento",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },

      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Un algoritmo es:",
              "El formato de texto o fuente consiste en:",
          ],
          "preguntasMultiples": true
      }
  },
  // 12 CRELACIONA COLUMNAS
  {
      "respuestas": [
          {
              "t13respuesta": '<img src="PITB_B01_R12_01.png" >',
              "t17correcta": "1"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R12_02.png" >',
              "t17correcta": "2"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R12_03.png" >',
              "t17correcta": "3"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R12_04.png" >',
              "t17correcta": "4"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R12_05.png" >',
              "t17correcta": "5"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R12_06.png" >',
              "t17correcta": "6"
          },


      ],
      "preguntas": [
          {
              "t11pregunta": "Marcos",

          },
          {
              "t11pregunta": "Texto"
          },
          {
              "t11pregunta": "Sellos"
          },
          {
              "t11pregunta": "Formas"
          },
          {
              "t11pregunta": "Recortar"
          },
          {
              "t11pregunta": "Escalar"
          },


      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta": "Relaciona las columnas según corresponda a cada herramienta de Doodle Draw.",
          "altoImagen": "80px",
          "anchoImagen": "80px"

      }
  },
  //Reactivo 13
  {
      "respuestas": [
          {
              "t13respuesta": '<img src="PITB_B01_R13_01.png" >',
              "t17correcta": "1"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R13_02.png" >',
              "t17correcta": "2"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R13_03.png" >',
              "t17correcta": "3"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R13_04.png" >',
              "t17correcta": "4"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R13_05.png" >',
              "t17correcta": "5"
          },
          {
              "t13respuesta": '<img src="PITB_B01_R13_06.png" >',
              "t17correcta": "6"
          },


      ],
      "preguntas": [
          {
              "t11pregunta": "Alinear a la izquierda",

          },
          {
              "t11pregunta": "Cambiar el tamaño de la letra"
          },
          {
              "t11pregunta": "Centrar"
          },
          {
              "t11pregunta": "Alinear a la derecha "
          },
          {
              "t11pregunta": "Justificado"
          },
          {
              "t11pregunta": "Cambiar el color de la letra"
          },


      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta": "Relaciona las columnas según corresponda a cada herramienta del Procesador de Textos.",
          "altoImagen": "80px",
          "anchoImagen": "80px"

      }
  },
  //Reactivo 14



  {
      "respuestas": [
          {
              "t13respuesta": "Introducción, desarrollo y desenlace",
              "t17correcta": "1",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Lugar de la historia, desarrollo y desenlace",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Inicio, desarrollo y conocer al personaje",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "¿Qué y cómo sucede?",
              "t17correcta": "1",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "¿Quiénes son los principales personajes?",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "¿Qué hacen los principales personajes?",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "¿Quiénes son los principales personajes y qué hacen?",
              "t17correcta": "1",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "¿Qué y cómo sucede?",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "El lugar donde se desarrolla la historia",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },

      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br>El patrón de la narración de un cuento es:",
              "El patrón que se lee en el desarrollo de un cuento es:",
              "El patrón que se lee en la introducción de un cuento es:"
          ],
          "preguntasMultiples": true
      }
  },

  // reactivo 15
  {
      "respuestas": [

          {
              "t13respuesta": "PITB_B01_R15_01.png",
              "t17correcta": "0",
              "columna": "1"
          },
          {
              "t13respuesta": "PITB_B01_R15_02.png",
              "t17correcta": "1",
              "columna": "1"
          },
          {
              "t13respuesta": "PITB_B01_R15_03.png",
              "t17correcta": "2",
              "columna": "1"
          },
          {
              "t13respuesta": "PITB_B01_R15_04.png",
              "t17correcta": "3",
              "columna": "1"
          },

      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la imagen al espacio que corresponda.",
          "tipo": "ordenar",
          "imagen": true,
          "url": "PITB_B01_R15_00.png",
          "respuestaImagen": true,
          "tamanyoReal": true,
          "bloques": false,
          "borde": true,
          "evaluable": true

      },
      "contenedores": [
          { "Contenedor": ["", "135,332", "cuadrado", "49, 49", ".", "transparent"] },//h   0  0,1,4,5
          { "Contenedor": ["", "77,419", "cuadrado", "49, 49", ".", "transparent"] },//h  1  2,3,6,7
          { "Contenedor": ["", "383,342", "cuadrado", "49, 49", ".", "transparent"] },//s  2
          { "Contenedor": ["", "238,114", "cuadrado", "49, 49", ".", "transparent"] },//s  3


      ]
  },

// reactivo 16












];
