json=[
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
           ], 
            "preguntas": [ 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"La palabra infoxicación significa que la información cambia tan rápido, que los conceptos que hoy conocemos pueden cambiar en un tiempo relativamente corto.",
            "correcta"  : "0"
            }, 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"Cuando nos referimos a una sociedad líquida, decimos que se debe a que la cantidad de información sobre un tema es demasiada que es imposible revisar todo.",
            "correcta"  : "0"
            }, 
           { 
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"Reducir las fuentes de información y actualizarlas periódicamente es una forma de ser consumidor sano de la información.",
            "correcta"  : "1"
            }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
                "descripcion": "Reactivo",   
                "variante": "editable",
                "anchoColumnaPreguntas": 50,
                "evaluable"  : true
            }        
        },
        //2
        {
            "respuestas":[
               {
                  "t13respuesta":     "Muestra páginas sobre las funciones que puede hacer un director cinematográfico.",
                  "t17correcta":"1",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "Muestra páginas sobre los directores cinematográficos antiguos.",
                  "t17correcta":"0",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "Muestra páginas sobre cómo se desarrolla la dirección cinematográfica.",
                  "t17correcta":"0",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "Muestra páginas con ofertas de televisores con precios entre $4,000.00 y $7,000.00",
                  "t17correcta":"1",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "Muestra páginas que tratan de precios de televisores.",
                  "t17correcta":"0",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "Muestra páginas que tratan de grandes empresas televisoras.",
                  "t17correcta":"0",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "Muestra el resultado de convertir 41º Celsius a grados Fahrenheit.",
                  "t17correcta":"1",
                "numeroPregunta":"2"
               },
               {
                  "t13respuesta":     "Muestra páginas con el contenido escrito “41 C a F”.",
                  "t17correcta":"0",
                "numeroPregunta":"2"
               },
               {
                  "t13respuesta":     "Muestra páginas con los antecedentes de conversión de grados Fahrenheit.",
                  "t17correcta":"0",
                "numeroPregunta":"2"
               },
               {
                  "t13respuesta":     "Muestra páginas cuyo encabezado o título contiene el término “inteligencia artificial”.",
                  "t17correcta":"1",
                "numeroPregunta":"3"
               },
               {
                  "t13respuesta":     "Muestra páginas que traten del tema inteligencia en español.",
                  "t17correcta":"0",
                "numeroPregunta":"3"
               },
               {
                  "t13respuesta":     "Muestra páginas cuyo contenido sea el de ventajas y riesgos de la inteligencia artificial.",
                  "t17correcta":"0",
                "numeroPregunta":"3"
               }
            ],
            "pregunta":{
               "c03id_tipo_pregunta":"1",
               "t11pregunta":["Selecciona la respuesta correcta.<br><br>Se escribe en el buscador el siguiente enunciado: “El director de una película”.",
                              "<br><br>Se escribe en el buscador el enunciado: “Televisión $4000...$7000”.",
                              "<br><br>Se escribe en el buscador el enunciado: “41 C a F”",
                              "<br><br>Se escribe en el buscador el enunciado: intitle “inteligencia artificial” tanto en idioma español, como en inglés."
                            ],
             "preguntasMultiples": true
            }
          },
          //3
          {
            "respuestas":[
               {
                  "t13respuesta":     "Imágenes",
                  "t17correcta":"1",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "Texto",
                  "t17correcta":"0",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "Números",
                  "t17correcta":"0",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "Clarificar o complementar el tema.",
                  "t17correcta":"1",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "Exponer el tema.",
                  "t17correcta":"0",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "Extender los conocimientos del tema.",
                  "t17correcta":"0",
                "numeroPregunta":"1"
               }
            ],
            "pregunta":{
               "c03id_tipo_pregunta":"1",
               "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cuál es el elemento principal en una infografía?",
                              "<br><br>¿Cuál es la función del texto en una infografía?"
                            ],
             "preguntasMultiples": true
            }
          },
          //4
          { 
            "respuestas":[ 
           { 
            "t13respuesta":"Imágenes", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"Gráficos", 
            "t17correcta":"2", 
            }, 
           { 
            "t13respuesta":"Tablas", 
            "t17correcta":"3", 
            }, 
           { 
            "t13respuesta":"Diagramas", 
            "t17correcta":"4", 
            }, 
           ], 
            "preguntas": [ 
            { 
            "t11pregunta":"Son dibujos o ilustraciones representativas del tema en una infografía." 
            }, 
           { 
            "t11pregunta":"Sirven para ilustrar información estadística y numérica en una infografía." 
            }, 
           { 
            "t11pregunta":"Se usan para representar datos descriptivos de forma ordenada en una infografía." 
            }, 
           { 
            "t11pregunta":"Se utilizan para mostrar el funcionamiento de un proceso y relaciones de causa y efecto." 
            }, 
            ], 
            "pregunta":{ 
            "c03id_tipo_pregunta":"12", 
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "", 
            "altoImagen":"100px", 
            "anchoImagen":"200px" 
            } 
            },
            //5
            { 
                "respuestas":[ 
               { 
                "t13respuesta":"Contener imágenes adecuadas que puedan ahorrar descripciones innecesarias y complementen el texto.", 
                "t17correcta":"1", 
                }, 
               { 
                "t13respuesta":"Utilizar palabras clave o imágenes para asociar conceptos o descripciones.", 
                "t17correcta":"1", 
                }, 
               { 
                "t13respuesta":"Uso de colores con estrategia para grabarlas en la mente del espectador.", 
                "t17correcta":"1", 
                }, 
               { 
                "t13respuesta":"Usar tantas imágenes como sea posiblee para no aburrir al espectador.", 
                "t17correcta":"0", 
                }, 
               { 
                "t13respuesta":"Usar todos los efectos posibles de transición, animación y sonido para entretener al espectador.", 
                "t17correcta":"0", 
                }, 
               ],
                "pregunta":{ 
                "c03id_tipo_pregunta":"2", 
                "t11pregunta": "Selecciona todas las respuestas correctas. <br><br>De las siguientes opciones, selecciona 3 que sean útiles para lograr una excelente presentación con diapositivas.", 
                "t11instruccion": "", 
                } 
                }, 
                //6
                {
                    "respuestas": [
                        {
                            "t13respuesta": "F",
                            "t17correcta": "0"
                        },
                        {
                            "t13respuesta": "V",
                            "t17correcta": "1"
                        }
                       ], 
                        "preguntas": [ 
                       { 
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta":"Ya que la <strong>Introducción</strong> es la oportunidad para captar la atención del público, se recomienda iniciar con un texto para explicar de qué tratará el tema.",
                        "correcta"  : "0"
                        }, 
                       { 
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta":"Las diapositivas de una presentación son útiles como refuerzo a un discurso o exposición.",
                        "correcta"  : "1"
                        }, 
                       { 
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta":"Es aconsejable que primero se realicen los diseños de cada diapositiva y al final los textos que llevarán.",
                        "correcta"  : "0"
                        }, 
                        { 
                         "c03id_tipo_pregunta": "13",
                         "t11pregunta":"Las <strong>Notas del orador</strong> sirven para leer acerca de lo que trata el tema de cada diapositiva.",
                         "correcta"  : "1"
                         }, 
                         { 
                          "c03id_tipo_pregunta": "13",
                          "t11pregunta":"La diapositiva de cierre debe invitar a una reflexión y retomar la idea planteada en la introducción.",
                          "correcta"  : "1"
                          }
                        ],
                        "pregunta": {
                            "c03id_tipo_pregunta": "13",
                            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
                            "descripcion": "Reactivo",   
                            "variante": "editable",
                            "anchoColumnaPreguntas": 50,
                            "evaluable"  : true
                        }        
                    },
                    //7
                    { 
                        "respuestas":[ 
                       { 
                        "t13respuesta":"Puede contextualizar, narrando brevemente experiencias de la vida diaria.", 
                        "t17correcta":"1", 
                        }, 
                       { 
                        "t13respuesta":"Ilustra con metáforas algunos conceptos para clarificar.", 
                        "t17correcta":"1", 
                        }, 
                       { 
                        "t13respuesta":"Es dinámico, cambia su tono de voz, hace que los espectadores se rían.", 
                        "t17correcta":"1", 
                        }, 
                       { 
                        "t13respuesta":"Lee con claridad con el mismo tono de voz toda la exposición.", 
                        "t17correcta":"0", 
                        }, 
                       { 
                        "t13respuesta":"Narra anécdotas personales sin importar que no estén relacionadas con el tema que va a exponer.", 
                        "t17correcta":"0", 
                        }, 
                       ],
                        "pregunta":{ 
                        "c03id_tipo_pregunta":"2", 
                        "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Selecciona 3 habilidades que debe tener un buen expositor:", 
                        "t11instruccion": "", 
                        } 
                        }, 
                        //8
                        {
                            "respuestas": [
                                {
                                    "t13respuesta": "<p>Sensores<\/p>",
                                    "t17correcta": "1,3,5,7,9"
                                },
                                {
                                    "t13respuesta": "<p>Teléfonos y controles de <br>dispositivos musicales<\/p>",
                                    "t17correcta": "1,3,5,7,9"
                                },
                                {
                                    "t13respuesta": "<p>Cámaras de seguridad<\/p>",
                                    "t17correcta": "1,3,5,7,9"
                                },
                                {
                                    "t13respuesta": "<p>Calefactores, ventilación<br>y aire acondicionado<\/p>",
                                    "t17correcta": "1,3,5,7,9"
                                },
                                {
                                    "t13respuesta": "<p>Ahorro en consumo energético<\/p>",
                                    "t17correcta": "1,3,5,7,9"
                                },
                                {
                                    "t13respuesta": "<p>Luz, agua y gas<\/p>",
                                    "t17correcta": "2,4,6,8,10"
                                },
                                {
                                    "t13respuesta": "<p>Programas<\/p>",
                                    "t17correcta": "2,4,6,8,10"
                                },
                                {
                                    "t13respuesta": "<p>Controles de electrodomésticos<\/p>",
                                    "t17correcta": "2,4,6,8,10"
                                },
                                {
                                    "t13respuesta": "<p>Belleza y limpieza<\/p>",
                                    "t17correcta": "2,4,6,8,10"
                                },
                                {
                                    "t13respuesta": "<p>Iluminar toda la casa<\/p>",
                                    "t17correcta": "2,4,6,8,10"
                                }
                            ],
                            "preguntas": [
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "<br><style>\n\
                                                            .table img{height: 90px !important; width: auto !important; }\n\
                                                        </style><table class='table' style='margin-top:-40px;'>\n\
                                                        <tr>\n\
                                                            <td style='width:150px'>Elementos importantes de un edificio inteligente.</td>\n\
                                                            <td style='width:100px'>Elementos no importantes de un edificio inteligente.</td>\n\
                                                        </tr>\n\
                                                        <tr>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                        </tr><tr>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                        </tr><tr>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                        </tr><tr>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                        </tr><tr>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                            <td>"
                                },
                                {
                                    "c03id_tipo_pregunta": "8",
                                    "t11pregunta": "        </td>\n\
                                                            </tr></table>"
                                }
                            ],
                            "pregunta": {
                                "c03id_tipo_pregunta": "8",
                                "t11pregunta": "Arrastra cada elemento donde corresponda.<br><br>",
                               "contieneDistractores":true,
                                "anchoRespuestas": 70,
                                "soloTexto": true,
                                "respuestasLargas": true,
                                "pintaUltimaCaja": false,
                                "ocultaPuntoFinal": true
                            }
                        },
                        //9
                        { 
                            "respuestas":[ 
                           { 
                            "t13respuesta":"Transiciones", 
                            "t17correcta":"1", 
                            }, 
                           { 
                            "t13respuesta":"Hipervínculos", 
                            "t17correcta":"2", 
                            }, 
                           { 
                            "t13respuesta":"Animación", 
                            "t17correcta":"3", 
                            }, 
                           { 
                            "t13respuesta":"Regla del 5", 
                            "t17correcta":"4", 
                            }, 
                           { 
                            "t13respuesta":"site:", 
                            "t17correcta":"5", 
                            }, 
                           { 
                            "t13respuesta":"@", 
                            "t17correcta":"6", 
                            }, 
                           ], 
                            "preguntas": [ 
                            { 
                            "t11pregunta":"Son los efectos de movimiento que ocurren al cambiar de una a otra diapositiva." 
                            }, 
                           { 
                            "t11pregunta":"Son ligas o enlaces que transportan de un documento a otro o a la web con solo un clic." 
                            }, 
                           { 
                            "t11pregunta":"Son cambios en los objetos de las diapositivas que le proporcionan dinamismo." 
                            }, 
                           { 
                            "t11pregunta":"Se refiere al número máximo de palabras y frases que se deben usar  por diapositiva." 
                            }, 
                           { 
                            "t11pregunta":"En un motor de búsqueda te ayuda a encontrar únicamente sitios web específicos." 
                            }, 
                           { 
                            "t11pregunta":"En un motor de búsqueda te ayuda a encontrar únicamente etiquetas en redes sociales." 
                            }, 
                            ], 
                            "pregunta":{ 
                            "c03id_tipo_pregunta":"12", 
                            "t11pregunta": "Relaciona las columnas según corresponda. ",
                            "t11instruccion": "", 
                            "altoImagen":"100px", 
                            "anchoImagen":"200px" 
                            } 
                            },
    //10
     {
            "respuestas": [        
                {
                   "t13respuesta":  "almacenamiento",
                     "t17correcta": "1" 
                 },
                {
                   "t13respuesta":  "internet",
                     "t17correcta": "2" 
                 },
                {
                   "t13respuesta":  "archivos",
                     "t17correcta": "3" 
                 },
                {
                   "t13respuesta":  "nube",
                     "t17correcta": "4" 
                 },
                {
                  "t13respuesta":  "información",
                    "t17correcta": "5" 
                },
                {
                 "t13respuesta":  "aplicaciones",
                   "t17correcta": "6" 
                },
            ],
            "preguntas": [ 
              {
              "t11pregunta": "La nube es un espacio virtual de "
              },
              {
              "t11pregunta": " al que se puede ingresar con una conexión a "
              },
              {
              "t11pregunta": ". Se usa con diferentes servicios como: correo electrónico, mensajes y envío de "
              },
              {
              "t11pregunta": ". Dropbox, Google Drive y iCloud son algunos ejemplos de aplicaciones que funcionan en la "
              },
              {
              "t11pregunta": ".<br>"
              },
            ],
            "pregunta": {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "Arrastra las palabras que completen el párrafo.", 
               "t11instruccion": "", 
               "respuestasLargas": true, 
               "pintaUltimaCaja": false, 
               "contieneDistractores": true 
             } 
          } , 

//11
{
    "respuestas":[
       {
          "t13respuesta":     "Espacio",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Compatibilidad",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Confiabilidad",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Compatibilidad",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Confiabilidad",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Espacio",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Confiabilidad",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Compatibilidad",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Espacio",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
      
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta.<br><br>Esta característica puede aumentar o ser limitada y está relacionada directamente con el costo del servicio de la nube.",
                      "<br><br>Esta característica de la nube se relaciona con la funcionalidad de los sistemas operativos y dispositivos.",
                      "<br><br>Es la característica más importante de la nube, ya que nos garantiza el respaldo y disposición de nuestros archivos.",
                    ],
     "preguntasMultiples": true
    }
  },
  
  //12
  {
    "respuestas": [
        {
            "t13respuesta": "F",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "V",
            "t17correcta": "1"
        }
       ], 
        "preguntas": [ 
       { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Una selfie no tiene impacto en nuestro entorno social.",
        "correcta"  : "0"
        }, 
       { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Un selfie adicto es aquel que en promedio se toma fotos de manera incontrolable y realiza más de 6 publicaciones al día.",
        "correcta"  : "1"
        }, 
       { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Las selfies no ponen en riesgo a las personas",
        "correcta"  : "0"
        },
        { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Algunas selfies pueden ser utilizadas para crear campañas que aporten algo positivo al resto de las personas.",
        "correcta"  : "1"
        }, 
        { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Nuestro rastro digital en la web se forma por las interacciones que realizamos en internet como la información, fotos y otros datos que compartimos.",
        "correcta"  : "1"
        }, 
        { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Publicar, etiquetar lugares y hacer comentarios en internet no deja ningún rastro digital.",
        "correcta"  : "0"
        },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },

//13
{
    "respuestas": [
        {
            "t13respuesta": "Clonado",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Capa",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "Pincel",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "Lazo magnético",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "PNG",
            "t17correcta": "5"
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "Copia una sección de la imagen."
        },
        {
            "t11pregunta": "Es una copia de protección de la imagen original."
        },
        {
            "t11pregunta": "Herramienta básica que permite realizar diferentes trazos."
        },
        {
            "t11pregunta": "Crea una selección, dibujándola a mano alzada con el puntero."
        },
        {
            "t11pregunta": "Formato en el que se guarda una firma creada con el editor de imagen."
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta":"Relaciona las columnas según corresponda."
    }
},
//14
{
    "respuestas": [
        {
            "t13respuesta": "F",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "V",
            "t17correcta": "1"
        }
       ], 
        "preguntas": [ 
       { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Un ejemplo de compresión de archivos de texto es cuando un algoritmo analiza el texto y determina si hay alguna secuencia de caracteres que se encuentre repetida, es decir, busca patrones.",
        "correcta"  : "1"
        }, 
       { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"La compresión de archivos es útil para optimizar el espacio en disco duro y para transmitir de forma inalámbrica y rápida archivos muy grandes.",
        "correcta"  : "1"
        }, 
       { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta":"Al comprimir un archivo, la calidad y la integridad de los datos se pierde y  disminuye cuando se descomprime.",
        "correcta"  : "0"
        },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
//15
 {
        "respuestas": [        
            {
               "t13respuesta":  "seguridad",
                 "t17correcta": "1" 
             },
            {
               "t13respuesta":  "industria",
                 "t17correcta": "2" 
             },
            {
               "t13respuesta":  "medicina",
                 "t17correcta": "3" 
             },
            {
               "t13respuesta":  "comercio",
                 "t17correcta": "4" 
             },
             {
               "t13respuesta":  "vigilancia",
                 "t17correcta": "5" 
             },
        ],
        "preguntas": [ 
          {
          "t11pregunta": "Algunas aplicaciones de la visión artificial pueden ser: en la "
          },
          {
          "t11pregunta": ", donde a través de la visión artificial se investiga a fondo hasta localizar personas, lugares y objetos. En la "
          },
          {
          "t11pregunta": ", al detectar daños en las piezas y realizarse automáticamente comparaciones con registros de una pieza en buen estado. En la "
          },
          {
          "t11pregunta": ", al comparar imágenes de órganos sanos y enfermos para obtener información de apoyo, diagnosticar y definir un tratamiento.<br>"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "8",
          "t11pregunta": "Arrastra las palabras que completen el párrafo.", 
           "t11instruccion": "", 
           "respuestasLargas": true, 
           "pintaUltimaCaja": false, 
           "contieneDistractores": true 
         } 
      } 
];