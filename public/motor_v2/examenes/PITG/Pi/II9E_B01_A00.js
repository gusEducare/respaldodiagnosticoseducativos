json = [
    {

        "respuestas": [
            {
                "t13respuesta": "Las videoconferencias y<br> archivos compartidos en tiempo real.",
                "t17correcta": "1,3,5,7"
            },
            {
                "t13respuesta": "Mucha información de la<br> web carece de fuentes verídicas.",
                "t17correcta": "2,4,6,8"
            },
            {
                "t13respuesta": "El acceso a una gran cantidad<br> de información de forma <br>rápida y gratuita.",
                "t17correcta": "1,3,5,7"
            },
            {
                "t13respuesta": "Ciberbullying, robo de<br> identidad, grooming y sexting.",
                "t17correcta": "2,4,6,8"
            },
            {
                "t13respuesta": "El uso de aplicaciones<br> de geolocalización.",
                "t17correcta": "1,3,5,7"
            },
            {
                "t13respuesta": "Hábitos que afectan la postura<br> y la salud al usar dispositivos digitales.",
                "t17correcta": "2,4,6,8"
            },
            {
                "t13respuesta": "La creación de aplicaciones<br> para controlar edificios<br> inteligentes o domótica.",
                "t17correcta": "1,3,5,7"
            },
            {
                "t13respuesta": "Exponer tus datos personales<br> al mantener un perfil<br> en sitios de internet.",
                "t17correcta": "2,4,6,8"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
.table img{height: 90px !important; width: auto !important; }\n\
</style><table class='table' style='margin-top:-20px;'>\n\
<tr>\n\
<td style='width:150px'>Ventajas</td>\n\
<td style='width:100px'>Desventajas</td>\n\
</tr>\n\
<tr><td>\n\ "},
            {
                "t11pregunta": "</td> <td>"
            },
            {
                "t11pregunta": "</td></tr><td>"
            },
            {
                "t11pregunta": "<td>"
            },
            {
                "t11pregunta": "</td></tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td> <td>"
            },

            {
                "t11pregunta": "</td> </tr></table>"
            }
        ],

        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": false,
        }

    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Comunidades",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Web celeb",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Cookies",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Cookies",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Comunidades",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Web celeb",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Web celeb",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Cookies",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Comunidades",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Comunidades",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Web celeb",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Cookies",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Comunidades",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Web celeb",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Cookies",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Web celeb",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Cookies",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Comunidades",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Comunidades",
                "t17correcta": "1",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Web celeb",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },
            {
                "t13respuesta": "Cookies",
                "t17correcta": "0",
                "numeroPregunta": "6"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Una desventaja de este fenómeno de internet, sucede cuando las personas muestran perfiles falsos con cualidades y virtudes inexistentes.",
                "Las empresas usan estos archivos para personalizar los servicios que ofrecen a los usuarios.", "Fenómeno de internet donde un solo usuario puede tener millones de seguidores alrededor del mundo y puede llegar a influir en las decisiones de la audiencia.",
                "Fenómeno de internet que tiene como ventajas poder compartir archivos e información en tiempo real y permitir que un gran número de personas puedan conectarse simultáneamente desde cualquier lugar del planeta.",
                "Una ventaja de este fenómeno de internet es la posibilidad de crear grupos cerrados para compartir información, por ejemplo; profesores, alumnos, directores y padres de familia.",
                "Una ventaja de este fenómeno de internet sucede cuando los usuarios se convierten en creadores de contenido original y aprenden acerca de los derechos de autor.",
                "Una desventaja de este fenómeno de internet sucede al proporcionar datos personales a desconocidos, los cuales pueden crear situaciones peligrosas como, acoso y poner en riesgo la integridad del usuario.",
            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Grooming",
                "t17correcta": "0"
            },
            {
                "t13respuesta": " Sexting",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando un adulto establece una relación amistosa con un menor para pedirle algo indebido.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando se pierde el control de las imágenes y videos compartidos y terminan formando parte de sitios dedicados a la pornografía sin autorización del protagonista.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Consiste en compartir material de contenido sexual por medio de internet o redes sociales.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No se conoce la identidad verdadera de la persona que bajo un perfil creado a su conveniencia puede manipular a un menor.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para evitarlo, se debe cuidar el contenido de los archivos que se comparten y pensar si alguno de ellos pueden dañar tu imagen.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para prevenirlo, es importante recordar que nadie puede obligarte a hacer algo indebido y pedir ayuda de un adulto de tu confianza.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada situación de riesgo en internet.",
            "descripcion": "Título preguntas",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "CONTAR.SI(B2:P2;1000)",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "CONTAR.SI(H2,L2,M2)",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "CONTAR.SI(A2:Q2;1000)",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "CONTAR.SI(B3:P3;5)",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "CONTAR.SI(A3:Q3;5)",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "CONTAR.SI(5;B3:P3)",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Observa la siguiente imagen y selecciona la respuesta correcta.<br><br><img src='II9E_B01_A00_00.png' width='800'><br><br>Para conocer cuántas personas tienen 1000 contactos, la función correcta es:",
                "Para conocer cuántas personas tienen 5 aplicaciones de redes sociales, la función correcta es:"

            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true,

        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "CONTAR.SI(B2:P2;”Facebook”)",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "CONTAR.SI(A2:Q2;Facebook)",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "CONTAR.SI(”Facebook”; B2:P2)",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "CONTAR.SI(B3:P3;”Video”)",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "CONTAR.SI(A3:Q3;Video)",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "CONTAR.SI(”Video”; B2:P2)",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Observa la siguiente imagen y selecciona la respuesta correcta.<br><br><img src='II9E_B01_A00_01.png' width='800'><br><br>Para conocer cuántas personas usan Facebook, la función correcta es:",
                "Para conocer cuántas personas comparten Video, la función correcta es:"

            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Filtrar por valores Facebook",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Filtrar por valores 20",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Ordenar A → Z",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Filtrar por valores Facebook y filtrar por valores 20",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Ordenar A → Z y filtrar por valores 20",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Filtrar por condición el texto es exactamente 20 y filtrar por condición Facebook",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Observa la siguiente imagen y selecciona la respuesta correcta.<br><br><img src='II9E_B01_A00_02.png' width='400'><br>Para ver únicamente los usuarios de Facebook, el filtro correcto es:",
                "Para ver únicamente los usuarios que tienen 20 accesos diarios a Facebook, los filtros correctos son:"

            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Moda",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Mediana",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Media",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Contar",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Es una función que la hoja de cálculo usa para encontrar un valor que se repite con frecuencia en un rango de celdas."
            },
            {
                "t11pregunta": "Es una función que la hoja de cálculo usa para encontrar el dato que está a la mitad de un conjunto de datos."
            },
            {
                "t11pregunta": "Es una función que la hoja de cálculo usa para mostrar el promedio de un conjunto de datos determinado."
            },
            {
                "t11pregunta": "Es una función que la hoja de cálculo usa para contar las celdas que contienen números."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },
    //////8
    {
        "respuestas": [
            {
                "t13respuesta": "500 y 3",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "1000 y 5",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "250 y 4",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "400 y 3",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "250 y 4",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "500 y 5",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "15",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "12",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "10",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Observa la imagen y usa tu aplicación de hoja de cálculo para seleccionar la respuesta correcta.<br><br><img src='II9E_B01_A00_03.png' width='800'><br>La moda de los datos anteriores es:",
                "La mediana de los datos anteriores es:",
                "La función =CONTAR(B4:P4) devuelve el valor:"

            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }

    },
    ///////9
    {
        "respuestas": [
            {
                "t13respuesta": "60",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "50",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "40",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "454.667",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "500.66",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "400.667",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "8",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "10",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "12",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Observa la imagen y usa tu aplicación de hoja de cálculo para seleccionar la respuesta correcta.<br><br><img src='II9E_B01_A00_04.png' width='800'><br><br>La función  =CONTAR(B2:P5)  devuelve el valor:",
                "La función  =PROMEDIO(B2:P2) devuelve el valor:",
                "La función  =MODA(B4:P4)  devuelve el valor:"

            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    ///10
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='II9E_B01_A00_05.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='II9E_B01_A00_06.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='II9E_B01_A00_07.png'>",
                "t17correcta": "3",
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Este tipo de gráficas se usan para comparar datos."
            },
            {
                "t11pregunta": "Este tipo de gráficas se usan para mostrar el cambio de una variable en el tiempo."
            },
            {
                "t11pregunta": "Este tipo de gráficas se usan para representar porcentajes de un total."
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "200px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Identificar el artículo comprado (una raqueta de tenis).",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Buscar en el catálogo artículos relacionados con el tenis (pelotas, ropa deportiva).",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Mostrar sugerencias con los artículos relacionados.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Si en 15 días el cliente no ha comprado nada, enviarle un correo electrónico con publicidad y ofertas en los artículos relacionados.",
                "t17correcta": "4",
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Paso 1"
            },
            {
                "t11pregunta": "Paso 2"
            },
            {
                "t11pregunta": "Paso 3"
            },
            {
                "t11pregunta": "Paso 4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },

    //=============== Actualizacion nuevos reactivos 5/10
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hiperconectividad significa estar constantemente conectados a los dispositivos tecnológicos y la interacción permanente entre los objetos, bases de datos y sistemas de información.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando una persona pierde el control del tiempo al usar la tecnología, no se considera una ciberadicción.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La ciberadicción se da cuando el abuso de la tecnología afecta la vida diaria de una persona, generando aislamiento, agresividad, alteraciones de la conducta, fracaso escolar y problemas de salud. ",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Reducir horas y calidad de sueño y sentir ansiedad cuando se está desconectado no son señales de ciberadicción.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Realizar actividades que no impliquen el uso de tecnología es un hábito que ayuda a evitar la ciberadicción.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 65,
            "evaluable": true
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Utilizar redes sociales sin la correcta configuración pone en riesgo tu integridad personal.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Establecer contraseñas débiles no representa ningún riesgo para nuestros datos personales.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No se dispersan archivos de software dañino al descargar archivos por torrents.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ejecutar programas de análisis periódicamente, como el antivirus, no ayuda a disminuir riesgos de contagio por malware.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Conectarse a cualquier red WiFi o redes públicas pone en riesgo los datos que compartes.",
                "correcta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 65,
            "evaluable": true
        },
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITP_B01_R14-01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='PITP_B01_R14-02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='PITP_B01_R14-03.png'>",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Recta"
            },
            {
                "t11pregunta": "Punto"
            },
            {
                "t11pregunta": "Deslizador"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",

            "anchoImagen": "150px"
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "Es la repetición de una imagen o un patrón, ya sea de forma idéntica o en diferentes versiones.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es la asociación de un artículo con un personaje famoso, un tema musical pegadizo o alguna emoción, para crear una conexión psicológica con el consumidor.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Es la creación de expectativas en el cliente potencial. En muchos casos, estas afirmaciones funcionan, aunque algunas pueden ser falsas o exageradas.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Se trata de convencer al cliente de que consuma un servicio o producto porque si no, se quedará fuera de algún beneficio innovador o de moda.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Repetición"
            },
            {
                "t11pregunta": "Asociación"
            },
            {
                "t11pregunta": "Afirmación"
            },
            {
                "t11pregunta": "Moda"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda según el tipo de publicidad visual.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "Internet de las cosas",
                "t17correcta": "1",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "Realidad virtual",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "Realidad aumentada",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "Realidad virtual",
                "t17correcta": "1",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "Internet de las cosas",
                "t17correcta": "0",
                "numeroPregunta": "1",

            },
            {
                "t13respuesta": "Realidad aumentada",
                "t17correcta": "0",
                "numeroPregunta": "1",

            },
            {
                "t13respuesta": "Realidad aumentada",
                "t17correcta": "1",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "Internet de las cosas",
                "t17correcta": "0",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "Realidad virtual",
                "t17correcta": "0",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "Wearables",
                "t17correcta": "1",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "Realidad aumentada",
                "t17correcta": "0",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "Realidad virtual",
                "t17correcta": "0",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "Big data",
                "t17correcta": "1",
                "numeroPregunta": "4",
            },
            {
                "t13respuesta": "Sixth sense",
                "t17correcta": "0",
                "numeroPregunta": "4",
            },
            {
                "t13respuesta": "Realidad virtual",
                "t17correcta": "0",
                "numeroPregunta": "4",
            },
            {
                "t13respuesta": "Sixth sense",
                "t17correcta": "1",
                "numeroPregunta": "5",
            },
            {
                "t13respuesta": "Realidad aumentada",
                "t17correcta": "0",
                "numeroPregunta": "5",
            },
            {
                "t13respuesta": "Big data",
                "t17correcta": "0",
                "numeroPregunta": "5",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Es la conexión en red de objetos comunes que pueden controlarse de forma remota por internet y enviar datos a un servidor.",
                "Es un entorno creado por computadora. Puede ser muy parecido a la vida real o totalmente imaginado por los programadores y diseñadores. Se usan lentes especiales, guantes, controles e incluso trajes con chips en todo el cuerpo.",
                "Consiste en agregar elementos digitales a la realidad que percibimos a través de cámaras, lentes u otros dispositivos. Por ejemplo, si a través de unos lentes estás observando un museo, los elementos de la exposición pueden mostrar datos relevantes e incluso tener movimiento.",
                "Son complementos tecnológicos que se incorporan a la ropa o accesorios que usamos a diario, como relojes, bolsos, lentes, pantalones o zapatos que a través de aplicaciones pueden medir pulsaciones, pasos y otras cosas.",
                "Es el término que se da al conjunto de datos prácticamente infinito y en constante crecimiento que genera la web a través de las conexiones de todos los dispositivos.",
                "Es un sistema informático que se ha basado en los gestos. Está constituido por un colgante en el cuello que contiene un proyector y una cámara. El sistema usa la cámara como un ojo digital y el proyector como una pantalla virtual, para que puedas usar muros y objetos como una interfaz que se controla con el movimiento de las manos.",
            ],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },

];
