json = [
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITG_B01_R01_01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R01_02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R01_03.png'>",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Se usa para enviar y recibir mensajes que viajan por un cable."
            },
            {
                "t11pregunta": "Tiene muchas piezas mecánicas móviles y se usa para escribir textos."
            },
            {
                "t11pregunta": "Se conecta a la televisión por medio de cables y sus cartuchos permiten jugar."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITG_B01_R02_01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R02_02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R02_03.png'>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R02_04.png'>",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Pueden funcionar como tabletas separando el teclado."
            },
            {
                "t11pregunta": "Es un espacio en la red de internet que permite almacenar archivos."
            },
            {
                "t11pregunta": "Permite ver un ambiente o entorno simulado por una computadora."
            },
            {
                "t11pregunta": "Permite ver el mundo real con elementos adicionales generados por una computadora."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //3
    {
        "respuestas": [

            {
                "t13respuesta": "PITG_B01_R03_01.png",
                "t17correcta": "0,1,4,5",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R03_02.png",
                "t17correcta": "0,1,4,5",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R03_03.png",
                "t17correcta": "0,1,4,5",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R03_04.png",
                "t17correcta": "0,1,4,5",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R03_05.png",
                "t17correcta": "2,3,6,7",
                "columna": "0"
            },
            {
                "t13respuesta": "PITG_B01_R03_06.png",
                "t17correcta": "2,3,6,7",
                "columna": "0"
            },
            {
                "t13respuesta": "PITG_B01_R03_07.png",
                "t17correcta": "2,3,6,7",
                "columna": "0"
            },
            {
                "t13respuesta": "PITG_B01_R03_08.png",
                "t17correcta": "2,3,6,7",
                "columna": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra los elementos y completa la tabla.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "PITG_B01_R03_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "evaluable": true

        },
        "contenedores": [
            { "Contenedor": ["", "190,35", "cuadrado", "110, 110", ".", "transparent"] },//h   0  0,1,4,5
            { "Contenedor": ["", "190,176", "cuadrado", "110, 110", ".", "transparent"] },//h  1  2,3,6,7 
            { "Contenedor": ["", "190,355", "cuadrado", "110, 110", ".", "transparent"] },//s  2
            { "Contenedor": ["", "190,495", "cuadrado", "110, 110", ".", "transparent"] },//s  3   
            { "Contenedor": ["", "330,35", "cuadrado", "110, 110", ".", "transparent"] },//h   4
            { "Contenedor": ["", "330,176", "cuadrado", "110, 110", ".", "transparent"] },//h  5
            { "Contenedor": ["", "330,355", "cuadrado", "110, 110", ".", "transparent"] },//s  6
            { "Contenedor": ["", "330,495", "cuadrado", "110, 110", ".", "transparent"] },//s  7

        ]
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Hardware",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Sistema Operativo",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Software",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Software",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Hardware",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Teclado",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "Sistema operativo",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Software",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Hardware",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "<img src='PITG_B01_R04_01.png' height='60' width='60'>",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<img src='PITG_B01_R04_02.png' height='60' width='60'>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<img src='PITG_B01_R04_03.png' height='60' width='60'>",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "<img src='PITG_B01_R04_04.png' height='60' width='60'>",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<img src='PITG_B01_R04_05.png' height='60' width='60'>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "<img src='PITG_B01_R04_06.png' height='60' width='60'>",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Conforman la parte tangible de la computadora, es decir, la parte que se puede tocar.",
                "Conforman la parte que no se puede tocar de una computadora, sirve para procesar la información.",
                "Coordina las tareas de hardware y software; además sirve de interface entre el usuario y el dispositivo.",
                "¿Cuál de estos íconos es de un Sistema Operativo?", "¿Cuál de estos íconos no es un Sistema Operativo?"],
            "t11instruccion": "",
            "preguntasMultiples": true,

        }
    },
    //5 
    {
        "respuestas": [

            {
                "t13respuesta": "PITG_B01_R05_01.png",
                "t17correcta": "0,3,6",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R05_02.png",
                "t17correcta": "0,3,6",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R05_03.png",
                "t17correcta": "0,3,6",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R05_04.png",
                "t17correcta": "1,4,7",
                "columna": "1"
            },
            {
                "t13respuesta": "PITG_B01_R05_05.png",
                "t17correcta": "1,4,7",
                "columna": "0"
            },
            {
                "t13respuesta": "PITG_B01_R05_06.png",
                "t17correcta": "1,4,7",
                "columna": "0"
            },
            {
                "t13respuesta": "PITG_B01_R05_07.png",
                "t17correcta": "2,5,8",
                "columna": "0"
            },
            {
                "t13respuesta": "PITG_B01_R05_08.png",
                "t17correcta": "2,5,8",
                "columna": "0"
            }, {
                "t13respuesta": "PITG_B01_R05_09.png",
                "t17correcta": "2,5,8",
                "columna": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra los elementos y completa la tabla que clasifica las aplicaciones.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "PITG_B01_R05_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "evaluable": true

        },
        "contenedores": [
            { "Contenedor": ["", "134,50", "cuadrado", "110, 110", ".", "transparent"] },//A 0
            { "Contenedor": ["", "134,265", "cuadrado", "110, 110", ".", "transparent"] },//C 1
            { "Contenedor": ["", "134,480", "cuadrado", "110, 110", ".", "transparent"] },//T 2
            { "Contenedor": ["", "262,50", "cuadrado", "110, 110", ".", "transparent"] },//A 3
            { "Contenedor": ["", "262,265", "cuadrado", "110, 110", ".", "transparent"] },//C 4
            { "Contenedor": ["", "262,480", "cuadrado", "110, 110", ".", "transparent"] },//T 5
            { "Contenedor": ["", "390,50", "cuadrado", "110, 110", ".", "transparent"] },//A 6
            { "Contenedor": ["", "390,265", "cuadrado", "110, 110", ".", "transparent"] },//C 7
            { "Contenedor": ["", "390,480", "cuadrado", "110, 110", ".", "transparent"] },//T 8


        ]
    },
    //6 
    {
        "respuestas": [

            {
                "t13respuesta": "1",
                "t17correcta": "0",
                "columna": "1"
            },
            {
                "t13respuesta": "2",
                "t17correcta": "1",
                "columna": "1"
            },
            {
                "t13respuesta": "3",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "4",
                "t17correcta": "3",
                "columna": "0"
            },
            {
                "t13respuesta": "5",
                "t17correcta": "4",
                "columna": "0"

            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra cada elemento donde corresponda.<br><\/p><br><table border=1 cellspacing=0 cellpadding=10 width='100%' >\n\
            <tr><td>1</td><td>Suma el número en pantalla con otro guardado en la memoria.</td></tr>\n\
            <tr><td>2</td><td>Significa multiplicar.</td></tr>\n\
            <tr><td>3</td><td>Muestra el resultado de tus operaciones. </td></tr>\n\
            <tr><td>4</td><td>Se usa para expresar números con decimales.</td></tr>\n\
            <tr><td>5</td><td>Elimina el último número sin deshacer todo el cálculo.</td></tr></table><br>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "PITG_B01_R06_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "evaluable": true

        },
        "contenedores": [
            { "Contenedor": ["", "205,533", "cuadrado", "80, 80", ".", "transparent"] },
            { "Contenedor": ["", "82,533", "cuadrado", "80, 80", ".", "transparent"] },
            { "Contenedor": ["", "343,533", "cuadrado", "80, 80", ".", "transparent"] },
            { "Contenedor": ["", "343,29", "cuadrado", "80, 80", ".", "transparent"] },
            { "Contenedor": ["", "147,29", "cuadrado", "80, 80", ".", "transparent"] },



        ]
    },
    //7 
    {
        "respuestas": [
            {
                "t13respuesta": "55",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "45",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "38",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "75",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "85",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "100",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta. Usa tu calculadora. <br><br> Brenda compró chocolates y los repartió en el colegio, a Ana le dió 11, a María 16, a Diego 13 y a Luis 15. ¿Cuántos chocolates compró Brenda?",
                "En un estacionamiento caben 25 coches en cada sección. Si hay tres secciones, ¿cuántos coches caben en total? ",],
            "t11instruccion": "",
            "contienDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //8 
    {
        "respuestas": [
            {
                "t13respuesta": "Te permite alinear el texto al centro.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Desplaza una línea o todo el párrafo a la derecha.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Aplica al texto el color que seleccionaste.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Sirve para crear una lista numerada.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Se utiliza para resaltar un título o un texto dentro de un párrafo.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='PITG_B01_R08_01.png'>",
            },
            {
                "t11pregunta": "<img src='PITG_B01_R08_02.png'>",
            },
            {
                "t11pregunta": "<img src='PITG_B01_R08_03.png'>",
            },
            {
                "t11pregunta": "<img src='PITG_B01_R08_04.png'>",
            },
            {
                "t11pregunta": "<img src='PITG_B01_R08_05.png'>",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //9 
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las imágenes que encuentras en internet, son propiedad de cualquier persona.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Citar una imagen, es poner debajo de ella el nombre del autor y el sitio de donde la obtuviste.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El autor o creador de cualquier imagen de internet, puede autorizar su uso o su venta.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando usas una imagen de internet, no es necesario citar al autor o creador.",
                "correcta": "0",
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //10 
    {
        "respuestas": [
            {
                "t13respuesta": "Este menú se utiliza para colocar una imagen en un texto.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Este comando se usa para copiar una imagen desde el navegador de internet.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Este comando se usa para colocar una imagen que copiaste.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Insertar"
            },
            {
                "t11pregunta": "Copiar"
            },
            {
                "t11pregunta": "Pegar"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //11 
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para usar un archivo que está en la nube, se necesita conexión a internet.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una App es una aplicación de software diseñada para ayudarnos a realizar una tarea específica.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No se necesita instalar una App para usarla.",
                "correcta": "0",
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITG_B01_R12_01.png' height='50'>",
                "t17correcta": "1",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R12_02.png' height='50'>",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R12_03.png' height='50'>",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "Insertar",
                "t17correcta": "1",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "Formato",
                "t17correcta": "0",
                "numeroPregunta": "1",

            },
            {
                "t13respuesta": "Inicio",
                "t17correcta": "0",
                "numeroPregunta": "1",

            },
            {
                "t13respuesta": "Columnas y Filas",
                "t17correcta": "1",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "Datos y Números",
                "t17correcta": "0",
                "numeroPregunta": "2",

            },
            {
                "t13respuesta": "Filtros y Herramientas",
                "t17correcta": "0",
                "numeroPregunta": "2",

            },
            {
                "t13respuesta": "Combinar celdas",
                "t17correcta": "1",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "Separar celdas",
                "t17correcta": "0",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "Distribuir celdas",
                "t17correcta": "0",
                "numeroPregunta": "3",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Con esta herramienta podemos insertar una tabla en un documento del procesador de textos.",
                "Este comando se usa para integrar una tabla al texto.",
                "En informática, las tablas están formadas por:",
                "Es la opción con la que convertimos varias celdas en una sola.",
            ],
            "t11instruccion": "",
            "contienDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //13
    {
        "respuestas": [
            
            {
                "t13respuesta": "-",
                "t17correcta": "1",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "“”",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": ":",
                "t17correcta": "0",
                "numeroPregunta": "0",
            },
            {
                "t13respuesta": "“”",
                "t17correcta": "1",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "-",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": ":",
                "t17correcta": "0",
                "numeroPregunta": "1",
            },
            {
                "t13respuesta": "filetype:",
                "t17correcta": "1",
                "numeroPregunta": "2",
            },
            {
                "t13respuesta": "define:",
                "t17correcta": "0",
                "numeroPregunta": "2",

            },
            {
                "t13respuesta": "site:",
                "t17correcta": "0",
                "numeroPregunta": "2",

            },
            {
                "t13respuesta": "define:",
                "t17correcta": "1",
                "numeroPregunta": "3",
            },
            {
                "t13respuesta": "filetype:",
                "t17correcta": "0",
                "numeroPregunta": "3",

            },
            {
                "t13respuesta": "site:",
                "t17correcta": "0",
                "numeroPregunta": "3",

            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Se utiliza para excluir una palabra en la búsqueda.",
                "Se utiliza cuando queremos que el buscador arroje un resultado textual.",
                "Se utiliza para buscar un archivo con extensión o formato determinado.",
                "Se utiliza para buscar una definición exacta.",
            ],
            "t11instruccion": "",
            "contienDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='PITG_B01_R14_01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R14_02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R14_03.png'>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='PITG_B01_R14_04.png'>",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Rama"
            },
            {
                "t11pregunta": "Opciones"
            },
            {
                "t11pregunta": "Nuevo Mind Map"
            },
            {
                "t11pregunta": "Agregar imagen"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "Un esquema gráfico de un tema que sirve para organizar ideas o conceptos",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Una gráfica de varios temas y sus definiciones",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Una representación de diferentes temas y subtemas",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Un mapa conceptual es:",
            "t11instruccion": "",
        }
    },
];