json = [
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El acceso a internet es un derecho internacional.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Contar con un dispositivo conectado a internet, hace que el propietario demuestre mayor inteligencia.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mantenerse actualizado sobre las innovaciones tecnológicas es propio de quien mejora la cultura digital.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Google, Yahoo, Bing.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Los autores comparten información a través de ellos. <br>Los usuarios dejan comentarios en los posts.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Calculadora, programa de diseño y otros que se usan sin instalar en el dispositivo.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "En ellos se exponen temas muy específicos, sirven como fuente de información.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Buscadores"
            },
            {
                "t11pregunta": "Blogs"
            },
            {
                "t11pregunta": "Aplicaciones en línea"
            },
            {
                "t11pregunta": "Foros"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11instruccion": "",
            "t11pregunta": "Relaciona las columnas según corresponda de acuerdo con el internet como recurso de aprendizaje.",
            "altoImagen": "300px",
            "anchoImagen": "300px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "1000110",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "0111001",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "0110001",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, //////////////////////////////////////////
            {
                "t13respuesta": "1010010",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "0101101",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "0100101",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, ///////////////////////////////////////
            {
                "t13respuesta": "1011010",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "0100101",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "0101101",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br> A continuación te presentamos 3 caracteres con su número decimal en la tabla ASCII, determina la secuencia binaria que le corresponde.\n\
 <br><br> Caracter F, valor ASCII decimal 70:", "Caracter R, valor ASCII decimal 82:", "Caracter Z, valor ASCII decimal 90:"],
            "t11instruccion": "",
            "preguntasMultiples": true,
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "<p>Delimitación del tema</p>",
                "t17correcta": "0",
                etiqueta: "Paso 1"
            },
            {
                "t13respuesta": "<p>Buscar la información</p>",
                "t17correcta": "1",
                etiqueta: "Paso 2"
            },
            {
                "t13respuesta": "<p>Leer y tomar notas</p>",
                "t17correcta": "2",
                etiqueta: "Paso 3"
            },
            {
                "t13respuesta": "<p>Organizar la información</p>",
                "t17correcta": "3",
                etiqueta: "Paso 4"
            },
            {
                "t13respuesta": "<p>Escribir el reporte</p>",
                "t17correcta": "4",
                etiqueta: "Paso 5"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos de la metodología para investigar en internet.",
            "t11instruccion": "",
        }
    },

    {
            "respuestas": [
                    {
                           "t13respuesta": "Virus ",
                "t17correcta": "1,4,7,10,12"
            },
                    {
                           "t13respuesta": "Worms",
                "t17correcta": "1,4,7,10,12"
            },
                    {
                           "t13respuesta": "Adware",
                "t17correcta": "1,4,7,10,12"
            },
                    {
                           "t13respuesta": "Caballo de Troya",
                "t17correcta": "1,4,7,10,12"
            },
                    {
                           "t13respuesta": "Spyware",
                "t17correcta": "1,4,7,10,12"
            },
                    {
                           "t13respuesta": "Hacking",
                "t17correcta": "2,5,8"
            },
                    {
                           "t13respuesta": "Cracking",
                "t17correcta": "2,5,8"
            },
                    {
                           "t13respuesta": "Phishing",
                "t17correcta": "2,5,8"
            },
                    {
                           "t13respuesta": "Robo de dispositivo",
                "t17correcta": "3,6,9,11"
            },
                    {
                           "t13respuesta": "Accidente",
                "t17correcta": "3,6,9,11"
            },
                    {
                           "t13respuesta": "Desastre natural",
                "t17correcta": "3,6,9,11"
            },
                    {
                           "t13respuesta": "Hardware dañado",
                "t17correcta": "3,6,9,11"
            },
            ],
            "preguntas": [
                  {
                      "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Ejemplos de malware</td><td>Ejemplos de ciberataques</td><td>Ejemplos de causa mayor</td></tr><tr><td>"
                  },
                  {
                      "t11pregunta": "</td><td>"
                  },
                  {
                      "t11pregunta": "</td><td>"
                  },
                  {
                      "t11pregunta": "</td></tr><tr><td>"
                  },
                  {
                      "t11pregunta": "</td><td>"
                  },
                  {
                      "t11pregunta": "</td><td>"
                  },
                  {
                      "t11pregunta": "</td></tr><tr><td>"
                  },
                  {
                      "t11pregunta": "</td><td>"
                  },
                  {
                      "t11pregunta": "</td><td>"
                  },
                  {
                      "t11pregunta": "</td></tr><tr><td>"
                  },
                  {
                      "t11pregunta": "</td><td></td><td>"
                  },
                  {
                      "t11pregunta": "</td></tr><tr><td>"
                  },
                  {
                      "t11pregunta": "</td><td></td><td></td></tr></table>"
                  },
            ],
            "pregunta": {
                  "c03id_tipo_pregunta": "8",
                  "t11pregunta": "Se puede perder información por tres motivos.<br>Arrastra cada elemento a donde corresponda.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
      },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },

        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término de seguridad digital se refiere a que nadie puede robarnos el dinero de la banca electrónica.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El malware es un software que usan los programadores para proteger la información contra ataques cibernéticos",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ciberataques solo pueden ser realizados por expertos en la intrusión de sistemas.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Respaldar la información periódicamente asegura su integridad.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Mantenerse informado de los nuevos virus y formas de ciberataque es suficiente para reducir las posibilidades de perder la información.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los virus se copian a sí mismos, se autoinstalan en los dispositivos.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Acerca de la importancia de la integridad de la información.  <br>Elige falso (F) o verdadero (V) según corresponda.",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Windows",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "MacOS",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Unix",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Chrome OS",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Android",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "iOS",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, ////////////////////////////////////////////
            {
                "t13respuesta": "MacOs",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Windows",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Unix",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Chrome OS",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Android",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "iOS",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, //////////////////////////////////////////////////////
            {
                "t13respuesta": "Unix",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Windows",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "MacOs",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Chrome OS",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Android",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "iOS",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            ////////////////////////////////////////////////////////////
            {
                "t13respuesta": "Chrome OS",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Windows",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "MacOs",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Unix",
                "t17correcta": "0",
                "numeroPregunta": "3"

            },
            {
                "t13respuesta": "Android",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "iOS",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
             ////////////////////////////////////////////////
            {
                "t13respuesta": "Android",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Windows",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "MacOs",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Unix",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Chrome OS",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "iOS",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
             //////////////////////////////////////////
            {
                "t13respuesta": "iOS",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Windows",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "MacOs",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Unix",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Chrome OS",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Android",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta según la característica que describa al sistema  operativo. \n\
 <br><br>Es un sistema operativo muy vulnerable a malware y ciberataque", "Es un sistema operativo prácticamente\n\
  libre de amenaza de virus", "Es un sistema operativo no apto para principiantes, pues no es de interfaz gráfica\n\
  ", "Sistema operativo cuya característica principal es que las aplicaciones no se instalan en el disco duro físico\n\
  ", "Es un sistema operativo que se encuentra instalado en casi todas las marcas de dispositivos móviles\n\
  ", "Es un sistema operativo para dispositivos móviles y que requiere de iTunes para sincronizar las aplicaciones y música"],
            "t11instruccion": "",
            "preguntasMultiples": true,
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Identificación por huella dactilar",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Identificación facial",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Identificación por voz",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, ////////////////////////////////////////////////
            {
                "t13respuesta": "Identificación facial",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Identificación por voz",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Identificación por huella dactilar",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, /////////////////////////////////////////////////
            {
                "t13respuesta": "Identificación por voz",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Identificación por huella dactilar",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Identificación facial",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, /////////////////////////////////////////////////////


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Reconoce en la acción el tipo de biometría informática y selecciona la respuesta correcta. \n\
 <br><br>Una persona pone el dedo índice en un lector óptico para registrar el momento de su llegada al trabajo\n\
 ", "Una persona se para frente a una cámara un par de segundos para que la puerta de acceso se abra\n\
 ", "Una persona tiene que decir su nombre y rango frente a una puerta de alta seguridad"],
            "t11instruccion": "",
            "preguntasMultiples": true,
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "IObit Applock-Face Lock",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "AppLock By Face",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "FaceCrypt",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Facebook Pro",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Retrica",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Identifica las aplicaciones para autenticación facial de entre las siguientes:",
            "t11instruccion": "",
        }
    },
    // 10
    {
      "respuestas": [
          {
              "t13respuesta": "Descarga programas de sitios oficiales",
              "t17correcta": "1",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Cuida lo que compartes",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Toma precauciones al comprar por internet",
              "t17correcta": "0",
              "numeroPregunta": "0"
          }, ////////////////////////////////////////////////
          {
              "t13respuesta": "Cuida lo que compartes",
              "t17correcta": "1",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Evita sitios sospechosos",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Descarga programas de sitios oficiales",
              "t17correcta": "0",
              "numeroPregunta": "1"
          }, /////////////////////////////////////////////////
          {
              "t13respuesta": "Sé un ciudadano digital",
              "t17correcta": "1",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "Protege tu información personal",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "Evita sitios sospechosos",
              "t17correcta": "0",
              "numeroPregunta": "2"
          }, /////////////////////////////////////////////////////
          {
              "t13respuesta": "Toma precauciones al comprar por internet",
              "t17correcta": "1",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "Evita sitios sospechosos",
              "t17correcta": "0",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "Descarga programas de sitios oficiales",
              "t17correcta": "0",
              "numeroPregunta": "3"
          }, /////////////////////////////////////////////////////


      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.\n\
<br><br>Esta norma te ayuda a evitar algún código malicioso que puede afectar tu equipo y tu información al descargar programas de cualquier sitio en internet, donde el software pudiera estar alterado.\n\
", "Esta norma te recuerda que no debes ingresar datos de contacto o personales en cualquier formulario. Te ayudará a establecer límites.\n\
", "Esta norma te da sugerencias para ser respetuoso con las otras personas, denunciar acciones inadecuadas y mantenerte seguro al convivir en espacios virtuales.",
"Esta norma sugiere que, antes de ingresar datos de tarjetas, verifiques que la aplicación o sitio web donde vas a comprar sea de confianza."],
          "t11instruccion": "",
          "preguntasMultiples": true,
      }
    },
    //10
    {
      "respuestas":[
 {
  "t13respuesta":"Acoso o chantaje por parte de un adulto hacia un menor, por medio de la tecnología y el internet",
  "t17correcta":"1",
  },
 {
  "t13respuesta":"Intercambio o envío de mensajes, fotos o videos con contenido sexual",
  "t17correcta":"2",
  },
 {
  "t13respuesta":"Acoso que una persona o grupo hace a alguien más, a través de la tecnología y el internet",
  "t17correcta":"3",
  },
 {
  "t13respuesta":"Sucede cuando una persona se hace pasar por alguien más bajo un perfil falso en la web",
  "t17correcta":"4",
  },
 ],
  "preguntas": [
  {
  "t11pregunta":"Grooming"
  },
 {
  "t11pregunta":"Sexting"
  },
 {
  "t11pregunta":"Ciberbullying"
  },
 {
  "t11pregunta":"Suplantación de identidad"
  },
  ],
  "pregunta":{
  "c03id_tipo_pregunta":"12",
  "t11instruccion": "",
  "t11pregunta":"Relaciona las columnas según corresponda.",
  "altoImagen":"100px",
  "anchoImagen":"200px"
  }
    },
    //12
    {
      "respuestas": [
          {
              "t13respuesta": "Márgenes",
              "t17correcta": "1",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Columnas",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Orientación de página",
              "t17correcta": "0",
              "numeroPregunta": "0"
          }, ////////////////////////////////////////////////
          {
              "t13respuesta": "Plantillas",
              "t17correcta": "1",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Formatos",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Informes",
              "t17correcta": "0",
              "numeroPregunta": "1"
          }, /////////////////////////////////////////////////
          {
              "t13respuesta": "Orientación de página",
              "t17correcta": "1",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "Márgenes",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "Columnas",
              "t17correcta": "0",
              "numeroPregunta": "2"
          }, /////////////////////////////////////////////////////
          {
              "t13respuesta": "Columnas",
              "t17correcta": "1",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "Orientación de página",
              "t17correcta": "0",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "Márgenes",
              "t17correcta": "0",
              "numeroPregunta": "3"
          }, /////////////////////////////////////////////////////


      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.\n\
<br><br>Establece el espacio entre el borde de página y el texto.\n\
", "Formatos prediseñados, de uso frecuente en un procesador de textos.\n\
", "Aplica a las páginas un diseño horizontal o vertical.",
"Divide el texto en dos o más columnas."],
          "t11instruccion": "",
          "preguntasMultiples": true,
      }
    },
    //13
    {
      "respuestas":[
{
"t13respuesta":"<img src='PITK_B01_R11_01.png'>",
"t17correcta":"1",
},
{
"t13respuesta":"<img src='PITK_B01_R11_02.png'>",
"t17correcta":"2",
},
{
"t13respuesta":"<img src='PITK_B01_R11_03.png'>",
"t17correcta":"3",
},
{
"t13respuesta":"<img src='PITK_B01_R11_04.png'>",
"t17correcta":"4",
},
{
"t13respuesta":"<img src='PITK_B01_R11_05.png'>",
"t17correcta":"5",
},
],
"preguntas": [
{
"t11pregunta":"Interlineado"
},
{
"t11pregunta":"Justificar"
},
{
"t11pregunta":"Aumentar sangría"
},
{
"t11pregunta":"Lista con viñetas"
},
{
"t11pregunta":"Color de fuente/ Color de texto"
},
],
"pregunta":{
"c03id_tipo_pregunta":"12",
"t11instruccion": "",
"altoImagen":"100px",
"anchoImagen":"200px"
}
    },
    //14
    {
      "respuestas": [
          {
              "t13respuesta": "F",
              "t17correcta": "0",
          },
          {
              "t13respuesta": "V",
              "t17correcta": "1",
          },

      ], "preguntas": [
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En un artículo de revista el formato del texto es cambiante y se requieren varias imágenes.",
              "correcta": "1",
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "No es importante aplicar la revisión de ortografía y gramática al darle formato a un texto.",
              "correcta": "0",
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Para una entrada de blog, el texto debe cumplir con el formato de alineación: justificar",
              "correcta": "0",
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "En una columna periodística; la sugerencia es trabajar el texto a dos columnas.",
              "correcta": "1",
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "El formato se puede aplicar a la fuente o caracteres, pero no al texto y párrafos.",
              "correcta": "0",
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "La edición consiste en realizar acciones con el texto, como: copiar, pegar, cortar, deshacer o suprimir.",
              "correcta": "1",
          },
          {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "Los estilos son formatos predefinidos de fuente y párrafo que se aplican en un solo paso.",
              "correcta": "1",
          },
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
          "t11instruccion": "",
          "descripcion": "Reactivo",
          "variante": "editable",
          "anchoColumnaPreguntas": 60,
          "evaluable": true
      }
    },
    //15
    {
      "respuestas":[
 {
  "t13respuesta":"Tabla de contenidos",
  "t17correcta":"1",
  },
 {
  "t13respuesta":"Referencia cruzada",
  "t17correcta":"2",
  },
 {
  "t13respuesta":"Marcador",
  "t17correcta":"3",
  },
 {
  "t13respuesta":"Hipervínculo",
  "t17correcta":"4",
  },
 {
  "t13respuesta":"Normas APA",
  "t17correcta":"5",
  },
 {
  "t13respuesta":"Citas y bibliografía",
  "t17correcta":"6",
  },
 ],
  "preguntas": [
  {
  "t11pregunta":"Es un índice temático que nos permite ver la estructura de un documento extenso y navegarlo fácilmente."
  },
 {
  "t11pregunta":"Es un enlace que direcciona hacia algún marcador previamente creado en alguna parte del mismo texto."
  },
 {
  "t11pregunta":"Se usa para identificar una posición del texto dentro de un documento."
  },
 {
  "t11pregunta":"Es un enlace dentro o fuera del documento a otros archivos o a páginas web."
  },
 {
  "t11pregunta":"Son un conjunto de estándares de publicación que tienen por objetivo lograr que los textos científicos y académicos cumplan con un mismo formato a nivel mundial."
  },
 {
  "t11pregunta":"Ambas se agregan cuando referenciamos nuestras fuentes."
  },
  ],
  "pregunta":{
  "c03id_tipo_pregunta":"12",
  "t11instruccion": "",
  "altoImagen":"100px",
  "anchoImagen":"200px"
  }
},
    //16
    {
      "respuestas": [
          {
              "t13respuesta": "Título, autores, hipótesis y bibliografía",
              "t17correcta": "1",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Motivación, curiosidad, lectura y escritura",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "Título, curiosidad, hipótesis, escritura y lectura",
              "t17correcta": "0",
              "numeroPregunta": "0"
          }, ////////////////////////////////////////////////
          {
              "t13respuesta": "Motivación, curiosidad, lectura y escritura",
              "t17correcta": "1",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Título, autores, hipótesis y bibliografía",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "Título, curiosidad, hipótesis, escritura y lectura",
              "t17correcta": "0",
              "numeroPregunta": "1"
          }, /////////////////////////////////////////////////
          {
              "t13respuesta": "Poner al alcance de diversos públicos los conocimientos y avances de la ciencia",
              "t17correcta": "1",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "Motivación, curiosidad, lectura y escritura",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "Título, autores, hipótesis y bibliografía",
              "t17correcta": "0",
              "numeroPregunta": "2"
          }, /////////////////////////////////////////////////////
          {
              "t13respuesta": "Borrador",
              "t17correcta": "1",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "Bibliografía",
              "t17correcta": "0",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "Hipótesis",
              "t17correcta": "0",
              "numeroPregunta": "3"
          }, /////////////////////////////////////////////////////


      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.\n\
<br><br>Es la estructura básica de un artículo científico.\n\
", "Son los elementos que debe tener un periodista científico:\n\
", "Es la meta principal del periodismo científico:",
"Es un esquema o guion escrito previo del artículo."],
          "t11instruccion": "",
          "preguntasMultiples": true,
      }
    },
];
