json = [

  {
    "respuestas": [
      {
        "t13respuesta": "Big Data",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Servidor",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Internet",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Servidor",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Big Data",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Internet",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Big Data",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Servidor",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Internet",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Comerciales, financieras, estadísticas o científicas",
        "t17correcta": "1",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Almacenamiento y documentación",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Ventas y cifrado de datos",
        "t17correcta": "0",
        "numeroPregunta": "3"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Es el conjunto de enormes cantidades de información, organizadas, almacenadas y analizadas con ayuda de herramientas de cómputo. ",
        "<br><br>Es un almacén de información remoto que provee los datos solicitados por cualquier usuario de internet.",
        "<br><br>Recolecta toda la información y datos que generan los usuarios de internet alrededor del mundo.",
        "Son las razones por las que Big Data estudia el comportamiento de mercados y tendencias de los usuarios."
      ],
      "preguntasMultiples": true
    }
  },
  //2
  {
    "respuestas": [
      {
        "t13respuesta": "Amazon",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Netflix",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "Nike",
        "t17correcta": "3",
      },
      {
        "t13respuesta": "Spotify",
        "t17correcta": "4",
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Este sitio web usa Big Data para analizar las características del usuario, sus intereses, tendencias del momento, para ofrecer una serie de productos sugeridos o relacionados con compras que el cliente ya ha hecho o se ha planteado hacer."
      },
      {
        "t11pregunta": "El éxito de este sitio, consiste en observar a detalle los hábitos de consumo y preferencias del usuario para descubrir lo que quiere ver a continuación con base en patrones predictivos que proporciona Big Data."
      },
      {
        "t11pregunta": "A través de sus dispositivos electrónicos colecciona una cantidad masiva de información de sus usuarios, lo que le sirve para crear nuevos productos donde sus usuarios sean fieles seguidores y consumidores."
      },
      {
        "t11pregunta": "Esta compañía utiliza Big Data para obtener los datos más llamativos, curiosos y raros del comportamiento de sus usuarios y hacer su publicidad más atractiva y conseguir más consumidores."
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11instruccion": "",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "altoImagen": "100px",
      "anchoImagen": "200px"
    }
  },
  //3
  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0"
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1"
      }
    ],
    "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Una red social es un espacio virtual que permite a las personas conectarse, estar comunicados e interactuar.",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Estos íconos pertenecen a redes sociales.<br><img src='PITE_B01_R03_01.png' width=50px height=50px/><img src='PITE_B01_R03_02.png' width=50px height=50px/>",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Tus acciones en internet (como buscar información, visitar sitios web, responder encuestas, realizar publicaciones, comentarios, compartir fotos, videos y archivos) dejan un rastro o evidencia.",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Tu rastro en internet es el resultado de tus interacciones del último mes o semana.",
        "correcta": "0"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Las características más importantes de las redes sociales son: crear comunidades, interactuar y conocer más contactos.",
        "correcta": "1"
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Estos íconos pertenecen a redes sociales.<br><img src='PITE_B01_R03_03.png' width=50px height=50px/><img src='PITE_B01_R03_04.png' width=50px height=50px/>",
        "correcta": "1"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 50,
      "evaluable": true
    }
  },
  //4
  {
    "respuestas": [
      {
        "t13respuesta": "Definición de ciberbullying",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Características del ciberbullying",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "Consecuencias del ciberbullying",
        "t17correcta": "3",
      },
      {
        "t13respuesta": "Prevención del ciberbullying",
        "t17correcta": "4",
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "Es el acoso, amenaza  intimidación, humillación, insulto, molestia mediante cualquier medio digital o virtual."
      },
      {
        "t11pregunta": "El anonimato de quien lo practica, la repetición de esas prácticas por medio de uno o varios medios virtuales."
      },
      {
        "t11pregunta": "Estrés, ansiedad, sentimientos de impotencia, ira, fatiga, desánimo, baja autoestima y culpa."
      },
      {
        "t11pregunta": "Consiste en denunciar, bloquear, guardar evidencia y buscar ayuda de un adulto."
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "t11instruccion": "",
      "altoImagen": "100px",
      "anchoImagen": "200px"
    }
  },
  //5
  {
    "respuestas": [
      {
        "t13respuesta": "No responder",
        "t17correcta": "1"
      },
      {
        "t13respuesta": "Guardar la evidencia",
        "t17correcta": "2"
      },
      {
        "t13respuesta": "Bloquear",
        "t17correcta": "3"
      },
      {
        "t13respuesta": "Habla con un adulto de confianza",
        "t17correcta": "4"
      },
      {
        "t13respuesta": "Denunciar al agresor",
        "t17correcta": "5"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": ""
      },
      {
        "t11pregunta": " cuando un compañero te agrede o acosa haciendo comentarios en redes sociales.<br><br>Si otra persona te acosa, molesta o humilla por medio de mensajes de texto debes "
      },
      {
        "t11pregunta": " como: capturas de pantalla y mensajes, para pedir ayuda con un adulto de tu confianza.<br><br>Puedes "
      },
      {
        "t11pregunta": " al agresor utilizando las opciones de privacidad para detener los mensajes agresivos.<br><br>Cuando se requiera la intervención de alguien más, "
      },
      {
        "t11pregunta": " para detener el problema.<br><br>Si sabes que alguien es molestado es tu deber actuar y ser un amigo, no un testigo; debes "
      },
      {
        "t11pregunta": ".<br>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que completen el párrafo.<br><br>",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },
  //6
  {
    "respuestas": [
      {
        "t13respuesta": "Contar",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Moda",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Promedio",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Moda",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Contar",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Promedio",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Promedio",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Moda",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Contar",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Filtros",
        "t17correcta": "1",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Promedio",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Moda",
        "t17correcta": "0",
        "numeroPregunta": "3"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Es una función que la hoja de cálculo usa para contar las celdas que contienen números.",
        "<br><br>Es una función que la hoja de cálculo usa para encontrar un valor estadístico que se repite con frecuencia en un rango de celdas.",
        "<br><br>Es una función que la hoja de cálculo usa para mostrar el resultado de dividir la suma de las cantidades que hay en un rango de celdas, entre el total de celdas determinado.",
        "En una hoja de cálculo, sirven para mostrar la información que cumple ciertos criterios, ocultando el resto."
      ],
      "preguntasMultiples": true
    }
  },
  //7
  {
    "respuestas": [
      {
        "t13respuesta": "8",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "12",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "5",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "9.2",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "9",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "10.5",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "15",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "12",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "10",
        "t17correcta": "0",
        "numeroPregunta": "2"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11instruccion": "",
      "t11pregunta": ["Observa la imagen y usa tu aplicación de hoja de cálculo para seleccionar la respuesta correcta.<br><center><img src='PITE_B01_R07.png'/></center><br>La moda de los datos anteriores es:",
        "<br><br>El promedio de los datos anteriores es:",
        "<br><br>La función =CONTAR(B2:P2) devuelve el valor:"
      ],
      "preguntasMultiples": true
    }
  },
  //8
  {
    "respuestas": [
      {
        "t13respuesta": "Ordenar A → Z",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Ordenar Z → A",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "El texto comienza por",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Ordenar Ascendente",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Ordenar Descendente",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Mayor que",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Mayor que 10",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Mayor o igual que 10",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Es igual a 10",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Es igual a 8",
        "t17correcta": "1",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Mayor que 7",
        "t17correcta": "0",
        "numeroPregunta": "3"
      },
      {
        "t13respuesta": "Mayor o igual que 8",
        "t17correcta": "0",
        "numeroPregunta": "3"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11instruccion": "",
      "t11pregunta": ["Observa la siguiente imagen y selecciona la respuesta correcta.<br><center><img src='PITE_B01_R08.png'/></center><br>Para ordenar la columna “ENCUESTADOS” alfabéticamente, el filtro correcto es:",
        "<br><br>Para ordenar la columna “Accesos diarios” de menor a mayor, el filtro correcto es:",
        "<br><br>Para mostrar únicamente los valores mayores a 10 en la columna “Accesos diarios”, el filtro correcto es:",
        "<br><br>Para mostrar únicamente los valores 8 en la columna “Accesos diarios”, el filtro correcto es:"
      ],
      "preguntasMultiples": true,
      "altoImagen":"200"
    }
  },
  //9
  
  {
    "respuestas": [
      {
        "t13respuesta": "<img src='PITE_B01_R09_01.png'/>",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "<img src='PITE_B01_R09_02.png'/>",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "<img src='PITE_B01_R09_03.png'/>",
        "t17correcta": "3",
      }
    ],
    "preguntas": [
      {
        "t11pregunta": "Cuando un filtro de la hoja de cálculo detecta caracteres de texto muestra estas opciones."
      },
      {
        "t11pregunta": "Cuando un filtro de la hoja de cálculo detecta caracteres numéricos muestra estas opciones."
      },
      {
        "t11pregunta": "Cuando un filtro de la hoja de cálculo detecta formato de fecha muestra estas opciones."
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "t11instruccion": "",
      "altoImagen": "170px"
    }
  },

//10 pendiente
 { 
 "respuestas":[ 
{ 
 "t13respuesta":"<img src='PITE_B01_R10_01.png'/>", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R10_02.png'/>", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R10_03.png'/>", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R10_04.png'/>", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R10_05.png'/>", 
 "t17correcta":"5", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Gráficos de columna" 
 }, 
{ 
 "t11pregunta":"Gráficos de líneas" 
 }, 
{ 
 "t11pregunta":"Gráficos circulares" 
 }, 
{ 
 "t11pregunta":"Gráficos de barras" 
 }, 
{ 
 "t11pregunta":"Gráficos de área" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } ,




//11 falso o verdadero
 {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para crear un gráfico, primero se deben capturar los datos en una hoja de cálculo.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para insertar un gráfico se deben seleccionar los datos y luego ",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No es necesario seleccionar los datos para seleccionar el tipo de gráfico que se desea.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al desplegar la opción “Insertar Gráfico” no se seleccionan los tipos de gráfico existentes.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Después de seleccionar el gráfico se deben especificar las opciones que complementen el gráfico.",
                "correcta": "1",
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "t11instruccion": "",
            "descripcion": "Pregunta",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },

//12 opcion multiple
 {  
      "respuestas":[
         {  
            "t13respuesta": "Estilos",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Encabezados",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Diseños",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
       


         {  
            "t13respuesta": "Filtros",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Estilos",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Fórmulas",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },




          {  
            "t13respuesta": "Color de relleno",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Color de texto",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Color de resaltado",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         


      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Son formatos predefinidos que se aplican a las celdas, contienen tipo de fuente, color, bordes, relleno, etcétera.",
                         "Sirve para visualizar únicamente la información que necesitas .",
                         "Sirve para colocar un color a una o varias celdas."
                         
                         ],
         "preguntasMultiples": true,
         
      }
   },


//13 opcion multiple
 {  
      "respuestas":[
         {  
            "t13respuesta": "Bluetooth",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Tecnología 4G",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "WiFi",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
          {  
            "t13respuesta": "Tecnología NFC",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
       


         {  
            "t13respuesta": "WiFi",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Tecnología NFC",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Bluetooth",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },{  
            "t13respuesta": "Tecnología 4G",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },




          {  
            "t13respuesta": "Tecnología NFC",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Bluetooth",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Tecnología 4G",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "WiFi",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         



         {  
            "t13respuesta": "Tecnología 4G",
            "t17correcta": "1",
            "numeroPregunta":"3"
         }, {  
            "t13respuesta": "Tecnología NFC",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },{  
            "t13respuesta": "Bluetooth",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },{  
            "t13respuesta": "WiFi",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         


      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Permite conectar 2 dispositivos a una distancia de 10 metros, usada para transmitir audio y video a teléfonos, estéreos y bocinas.",
                         "Esta tecnología permite conectarse de forma inalámbrica a redes locales con una distancia hasta de 20 metros.",
                         "Sirve para conectar dispositivos móviles a una distancia de 4 centímetros o menos, se usa para realizar pagos en una terminal con tu smartphone. ",
                          "Su principal característica es la velocidad de transmisión de datos entre  100 y 1000 megabits por segundo, actualmente se desarrolla una generación más avanzada de esta tecnología. "
                         
                         ],
         "preguntasMultiples": true,
         
      }
   },




//relaciona columans

 { 
 "respuestas":[ 
{ 
 "t13respuesta":"<img src='PITE_B01_R14_01.png'/>", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R14_02.png'/>", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R14_03.png'/>", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R14_04.png'/>", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"<img src='PITE_B01_R14_05.png'/>", 
 "t17correcta":"5", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Reparar" 
 }, 
{ 
 "t11pregunta":"Ojos rojos" 
 }, 
{ 
 "t11pregunta":"Recortar" 
 }, 
{ 
 "t11pregunta":"Desenfoque circular" 
 }, 
{ 
 "t11pregunta":"Rotar" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } 
,

  //cion multiple
 {  
      "respuestas":[
         {  
            "t13respuesta": "Saturación",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Luminosidad",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Temperatura",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
          {  
            "t13respuesta": "Resalte",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
       


         {  
            "t13respuesta": "Luminosidad",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Saturación",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Sombras",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },{  
            "t13respuesta": "Brillo",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },




          {  
            "t13respuesta": "Brillo",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Intensidad",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Saturación",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },{  
            "t13respuesta": "Exposición",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
            
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Este ícono <img src='PITE_B01_R15_01.png'/> corresponde a la herramienta:",
                         "Este ícono <img src='PITE_B01_R15_02.png'/> corresponde a la herramienta:",
                         "Este ícono <img src='PITE_B01_R15_03.png'/> corresponde a la herramienta:",
                         ""
                         
                         ],
         "preguntasMultiples": true,
         
      }
   },

];