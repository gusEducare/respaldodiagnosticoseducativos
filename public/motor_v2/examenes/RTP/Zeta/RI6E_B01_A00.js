json = [
    {
        "respuestas": [
            {
                "t13respuesta": "acelerómetro",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "transistor",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "fotorresistencia",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "<p>Selecciona la respuesta correcta.  <br>Al sensor de posición también se le conoce como<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
            "t11instruccion": "",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Iniciar un programa",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Terminar un programa",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Detener un programa",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<br>¿Para qué sirve este bloque?<br><br><center><img src='RTPZ_B01_R02_01.png'></center> ",
            "t11instruccion": " ",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Telégrafo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Celular",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Computadora",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<br>¿Cómo se llama el dispositivo antiguo que es capaz de transmitir mensajes a larga distancia, los cuales se denominan telegramas?",
            "t11instruccion": "",
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Líneas y puntos",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Letras griegas",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Cuadros",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Triángulos",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<br>¿Qué símbolos utiliza el código Morse?",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Para amplificar la corriente/sonido",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Para reproducir música",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Para prender un foco",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Para qué sirven los transistores PNP y NPN en este telégrafo?<center><img src='RTPZ_B01_R05_01.png' width='550' height='550'></center>",
            "t11instruccion": "",
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Horizontal - Boca abajo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Acostado - Boca arriba",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Inclinado arriba y abajo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "De costado izquierda y derecha",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Flotando",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para la pregunta.<br>Pedro quiere utilizar el sensor de posición del Birdy. ¿Qué posiciones podría reconocer el robot?",

            "t11instruccion": "",
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "alámbrico / inalámbrico",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "inalámbrico / alámbrico",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "alámbrico / antiguo",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona la respuesta correcta.<br>Diana quiere usar un telégrafo para comunicarse con una amiga que vive muy lejos, ella tiene dos opciones: usar un telégrafo<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>en el que es necesario usar un cable o usar un telégrafo<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>en el que se transmite el mensaje a través de ondas electromagnéticas.",

            "t11instruccion": "",
        }
    },
];