json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Motor",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Interruptor",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Portabaterías",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué elemento es este?<br><center> <img style='height: 200px; display:block; ' src='RTPD_B01_R01_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Portabaterías",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Resistencia",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Conector azul",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>En el siguiente circuito eléctrico, ¿qué elemento tendrías que colocar para aumentar la velocidad del motor?<br><center> <img style='height: 300px; display:block; ' src='RTPD_B01_R02_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Cerrado",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Abierto",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Apagado",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>La continuidad eléctrica se presenta cuando todos los elementos de un circuito están conectados, de esta manera el circuito se encuentra:",
            "t11instruccion": "",
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Disminuir la energía que pasa",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Concentrar la energía que pasa",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Repartir la energía que pasa",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Una resistencia sirve para:",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Sensor de luz",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sensor de sonido",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Resistencia",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br> ¿Qué elemento bloquea el paso de energía cuando lo tapas con el dedo?<br><center> <img style='height: 250px; display:block; ' src='RTPD_B01_R05_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Abierto",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Cerrado",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Medio",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>José utilizó su laboratorio de electrónica para construir el circuito de la imagen. ¿Qué tipo de circuito es?<br><center> <img style='height: 250px; display:block; ' src='RTPD_B01_R06_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Saldrá disparada hacia arriba",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Se quedará quieta",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Se moverá pero no saldrá disparada",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Si el movimiento de las aspas de una hélice empuja el aire hacia abajo, la hélice:",
            "t11instruccion": "",
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Sensor de sonido",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sensor de luz",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Sensor de contacto",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>El maestro le pidió a Mariana que construyera un circuito que se active cuando ella aplauda. ¿Qué sensor debería agregar?",
            "t11instruccion": "",
        }
    },
    ];