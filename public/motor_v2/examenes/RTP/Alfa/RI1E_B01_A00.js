json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Primer grado",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Segundo grado",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Tercer grado",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "¿Qué grado de palanca es una balanza?<br><br>Selecciona la respuesta correcta. <br><center> <img src='RTPA_B01_R01_01.png'></center>",
            "t11instruccion": "",
        }
    },

    //2

    {
        "respuestas": [
            {
                "t13respuesta": "5 kg",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "2 kg",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "6 kg",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "10 kg",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>Una balanza con brazos del mismo tamaño, tiene un objeto de 5 kg en un extremo. ¿Cuánta masa debe tener el objeto que necesitas colocar en el otro extremo para equilibrar la balanza?",
            "t11instruccion": "",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Punto de apoyo",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Fuerza",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Carga",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Velocidad",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Batería",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>Escoge los elementos que debe llevar una balanza.",
            "t11instruccion": "",
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Ejes y ruedas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Arrastrarlo por el suelo",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Palos",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Cuerdas",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>Quieres desplazar un objeto de un lado a otro. ¿Qué debes hacer o usar?",
            "t11instruccion": " ",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='RTPA_B01_R05_01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='RTPA_B01_R05_02.png'>",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "<img src='RTPA_B01_R05_03.png'>",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Qué carrito llegaría más lejos si lo dejas sobre la pendiente?",
            "t11instruccion": "",
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "No, aunque haya una pendiente, las ruedas deben ser redondas.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sí, el carrito se podría mover porque está sobre una pendiente.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "No, las ruedas deben ser triangulares.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>Te pidieron construir un carrito con las ruedas cuadradas como el de la imagen. ¿El carrito se podría mover? <br> <center><img src='RTPA_B01_R06_01.png'>",
            "t11instruccion": "",
        }
    },//7
    {
        "respuestas": [
            {
                "t13respuesta": "8 kg",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "7 kg",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "3 kg",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>Diego quiere levantar una roca de 8 kg  y usará una palanca. ¿Cuántos kg de plastilina debe colocar en el otro extremo de la palanca para equilibrar el peso?",
            "t11instruccion": "",
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Arriba",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Abajo",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "En medio",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "A la derecha",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>Si haces ruedas de plastilina, ¿en qué posición deberías colocar los ejes en las ruedas?",
            "t11instruccion": "",
        }
    },

];
