json = [
    {
        "respuestas": [
            {
                "t13respuesta": "fricción",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "aceleración",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "posición",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>La &#9135;&#9135;&#9135;&#9135;&#9135; se entiende como una fuerza de rozamiento que se genera cuando dos superficies tienen contacto directo y ambas se oponen al movimiento entre sí.",
            "t11instruccion": "",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Puerto 1, el motor se mueve hacia atrás.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Puerto 1, el motor se mueve hacia adelante.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Puerto 2, el motor se mueve hacia atrás.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿En qué puerto y qué movimiento realiza el motor conectado a la tarjeta Hummingbird según el siguiente bloque?<br><center> <img style='height: 80px; display:block; ' src='RTPL_B01_R02_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "0° - 180°",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "90° - 180°",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "No tiene límite en el movimiento.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué ángulo/distancia puede recorrer un servomotor? <br><center> <img style='height: 200px; display:block; ' src='RTPL_B01_R03_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Almacena la distancia desde el sensor de distancia hasta un objeto.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Almacena la distancia recorrida por el robot.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Almacena la distancia que los motores recorrieron.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Para qué sirve este bloque? <br><center> <img style='height: 100px; display:block; ' src='RTPL_B01_R04_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Puerto 1, el servomotor se coloca en 150°.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Puerto 1, el servomotor se coloca en -150°.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Puerto 2, el servomotor se coloca en 75°.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿En qué puerto y a qué ángulo se mueve el servomotor conectado a la tarjeta Hummingbird según el siguiente bloque?  <br><center> <img style='height: 100px; display:block; ' src='RTPL_B01_R05_01.png'> </center>",
            "t11instruccion": "",
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Orugas mecánicas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Ligas",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Llanta loca",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Tania quiere que su vehículo pueda subir escalones. ¿Qué debe colocar en su robot en lugar de ruedas?",
            "t11instruccion": "",
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Sensor de distancia",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sensor de paredes",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Sensor de luz",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Pablo necesita construir un robot que no choque con las paredes. ¿Qué sensor debe usar su robot para lograr evadir obstáculos?",
            "t11instruccion": "",
        }
    },
]; 