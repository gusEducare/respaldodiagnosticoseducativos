json=[
//1 OPCION MULTIPLE
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003Eondas\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Epartículas\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eluz  \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"El sonido es un fenómeno físico que se transmite en forma de ________."
      }
   },//2 RESPUESTA MULTIPLE
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003Edel agua\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Edel aire\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Edel espacio \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"El sonido puede viajar a través  ________."
      }
   },
   //3OPCION MULTIPLE
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELED\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EResistencia\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EMotor \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":" ¿Qué elemento es este?<br><br><center> <img style='height: 100px; display:block; ' src='RTPG_B01_R03_01.png'> </center>",
        "t11instruccion":''
      }
   },
   //4 OPCION MULTIPLE
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ESal\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EMiel\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAzúcar \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué elemento debes agregar a un vaso con agua para que sea un buen conductor eléctrico?",
        "t11instruccion":"Selecciona la respuesta correcta.  "
      }
   },
   //5 OPCION MULTIPLE
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAgua de mar\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua embotellada\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua de la llave \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué tipo de agua es el mejor conductor eléctrico?",
        "t11instruccion":"Selecciona la respuesta correcta.  "
      }
   },//6 OPCION MULTIPLE
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAgua de mar\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua embotellada\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua de la llave \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Daniel está usando la bocina de su laboratorio de electrónica y quiere agregar agua para poder encenderla y apagarla. ¿Qué tipo de agua tendría que usar?",
        "t11instruccion":"Selecciona la respuesta correcta.  "
      }
   },
   //7 OPCION MULTIPLE
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAgua destilada\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua potable\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua con sal \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Juan está usando agua para tener conductividad en su circuito, pero no prende el LED. ¿Qué tipo de agua no conduce la electricidad?",
        "t11instruccion":"Selecciona la respuesta correcta.  "
      }
   },
];
