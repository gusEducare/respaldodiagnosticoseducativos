json=[
//1 OPCION MULTIPLE
   {  
     "respuestas":[  
        {  
           "t13respuesta":"\u003Cp\u003Elos colores de la nariz del Birdy\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003Elos motores del Birdy\u003C\/p\u003E",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"\u003Cp\u003Elo que escucha el Birdy\u003C\/p\u003E",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"1",
        "t11pregunta":"Este bloque de programación te permite controlar __________.<br><br><center> <img style='height: 100px; display:block; ' src='RTPE_B01_R01_01.png'> </center>",
        "t11instruccion": ''     }
  },//2 RESPUESTA MULTIPLE
  {  
     "respuestas":[  
        {  
           "t13respuesta":"\u003Cp\u003E60 / 60\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003E40 / 20\u003C\/p\u003E",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"\u003Cp\u003E-20 / 50 \u003C\/p\u003E",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"1",
        "t11pregunta":"¿Qué combinación usarías para mover a Birdy hacia adelante?<br><br><center> <img style='height: 100px; display:block; ' src='RTPE_B01_R02_01.png'> </center>",
         "t11instruccion": ''   
     }
  },
  //3 OPCION MULTIPLE
  {  
     "respuestas":[  
        {  
           "t13respuesta":"\u003Cp\u003ESensor de luz\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003ESensor de sonido\u003C\/p\u003E",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"\u003Cp\u003EResistencia \u003C\/p\u003E",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"1",
        "t11pregunta":" El elemento anterior bloquea el paso de energía cuando lo tapas con el dedo. ¿Qué elemento es?<br><br><center> <img style='height: 200px; display:block; ' src='RTPE_B01_R03_01.png'> </center>",
       "t11instruccion":''
     }
  },//4 OPCION MULTIPLE
  {  
     "respuestas":[  
        {  
           "t13respuesta":"\u003Cp\u003Emáximo\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003Ecorrecto\u003C\/p\u003E",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"\u003Cp\u003Emínimo\u003C\/p\u003E",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"\u003Cp\u003Emedio\u003C\/p\u003E",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"1",
        "t11pregunta":" Cuando el resistor variable tiene la perilla recorrida a la derecha, significa que se encuentra en su valor _________ de resistencia.",
       
     }
  },
  //5 OPCION MULTIPLE
   
 //5 OPCION MULTIPLE
  {  
     "respuestas":[  
        {  
           "t13respuesta":"\u003Cp\u003EColector\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003EBase\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003EEmisor \u003C\/p\u003E",
           "t17correcta":"1"
        }, 
        {  
           "t13respuesta":"\u003Cp\u003EResistencia \u003C\/p\u003E",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"2",
        "t11pregunta":"¿De qué elementos está compuesto un transistor?<br><br><center> <img style='height: 200px; display:block; ' src='RTPE_B01_R05_01.png'> </center>",
       "t11instruccion":"Selecciona la respuesta correcta.  ",
        "t11instruccion":''
     }
  },
  //6 OPCION MULTIPLE
    {  
     "respuestas":[  
        {  
           "t13respuesta":"\u003Cp\u003ESensor de obstáculos\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003ESensor de sonido\u003C\/p\u003E",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"\u003Cp\u003ESensor de luz \u003C\/p\u003E",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"1",
        "t11pregunta":"Pablo quiere programar su Birdy para que no choque con las paredes. ¿Qué sensor debe usar?",
       "t11instruccion":"Selecciona la respuesta correcta.  ",
     }
  },
   //7 OPCION MULTIPLE
    {  
     "respuestas":[  
        {  
           "t13respuesta":"\u003Cp\u003ETransistor\u003C\/p\u003E",
           "t17correcta":"1"
        },
        {  
           "t13respuesta":"\u003Cp\u003EResistencia\u003C\/p\u003E",
           "t17correcta":"0"
        },
        {  
           "t13respuesta":"\u003Cp\u003ELED\u003C\/p\u003E",
           "t17correcta":"0"
        }
     ],
     "pregunta":{  
        "c03id_tipo_pregunta":"1",
        "t11pregunta":"¿Qué elemento funciona como interruptor; es decir, que permite o no el paso de la señal eléctrica y sirve como amplificador de señales?",
       "t11instruccion":"Selecciona la respuesta correcta.  ",
    
     }
  }
];
