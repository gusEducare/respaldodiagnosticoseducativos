json = [
    //1
    {
        "respuestas":[
            {
            "t13respuesta":"Transmitir la energía de un punto a otro.",
            "t17correcta":"1",
            },
           {
            "t13respuesta":"Detectar la luz azul del ambiente.",
            "t17correcta":"0",
            },
           {
            "t13respuesta":"Mantener la energía.",
            "t17correcta":"0",
            },
           ],
            "pregunta":{
            "c03id_tipo_pregunta":"1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué utilidad tienen las piezas azules del laboratorio de electrónica?",
            "t11instruccion": "  ",
            }
    },

    //2

    {
      "respuestas":[
          {
          "t13respuesta":"frecuencia",
          "t17correcta":"1",
          },
         {
          "t13respuesta":"repeticiones",
          "t17correcta":"0",
          },
         {
          "t13respuesta":"continuidad",
          "t17correcta":"0",
          },
         ],
          "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": "Selecciona la respuesta correcta.<br><br>En electrónica se le llama <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> al número de veces que se repite cierta acción en un tiempo determinado.",
          "t11instruccion": "  ",
          }
    },
    //3
    {
      "respuestas":[
          {
          "t13respuesta":"1 MHz",
          "t17correcta":"1",
          },
         {
          "t13respuesta":"10 MHz",
          "t17correcta":"0",
          },
         {
          "t13respuesta":"1 GHz",
          "t17correcta":"0",
          },
         ],
          "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿A cuánto equivalen 1,000 kilohertz en megahertz?",
          "t11instruccion": "  ",
          }
    },
    //4
    {
      "respuestas":[
          {
          "t13respuesta":"Detecta la luz en el ambiente y dependiendo de la intensidad deja pasar energía.",
          "t17correcta":"1",
          },
         {
          "t13respuesta":"Detecta el sonido del ambiente y dependiendo de la intensidad deja pasar energía.",
          "t17correcta":"0",
          },
         {
          "t13respuesta":"Enciende la bombilla de 6 V.",
          "t17correcta":"0",
          },
         ],
          "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": "Selecciona la respuesta correcta.<br><br><center><img src='RI9E_B01_A00_01.png'></center><br>¿Para qué sirve esta pieza?",
          "t11instruccion": "  ",
          }
    },
    //5
    {
      "respuestas":[
          {
          "t13respuesta":"Magnético",
          "t17correcta":"1",
          },
         {
          "t13respuesta":"De presión ",
          "t17correcta":"0",
          },
         {
          "t13respuesta":"Deslizable",
          "t17correcta":"0",
          },
         ],
          "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": "Selecciona la respuesta correcta.<br><br><center><img src='RI9E_B01_A00_02.png'></center><br>¿Qué tipo de interruptor es este?",
          "t11instruccion": "  ",
          }
    },
    //6
    {
      "respuestas":[
          {
          "t13respuesta":"Polo positivo y polo negativo ",
          "t17correcta":"1",
          },
         {
          "t13respuesta":"Polo magnético y polo positivo ",
          "t17correcta":"0",
          },
         {
          "t13respuesta":"Polo norte y polo sur",
          "t17correcta":"0",
          },
         ],
          "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": "Selecciona la respuesta correcta.<br><br>Carlos está realizando la conexión de un LED pero no sabe cómo. ¿Qué polos tiene un LED?",
          "t11instruccion": ""
          }
    },
    //7
    {
      "respuestas":[
          {
          "t13respuesta":"5",
          "t17correcta":"1",
          },
         {
          "t13respuesta":"2",
          "t17correcta":"0",
          },
         {
          "t13respuesta":"10",
          "t17correcta":"0",
          },
         ],
          "pregunta":{
          "c03id_tipo_pregunta":"1",
          "t11pregunta": "Selecciona la respuesta correcta.<br><br>Ariadna quiere usar la pantalla digital de siete segmentos del laboratorio de electrónica para mostrar el número 2. ¿Cuántos LEDs necesita encender?",
          "t11instruccion": ""
          }
    }
    //8

    //9

    //10


    //11

];
