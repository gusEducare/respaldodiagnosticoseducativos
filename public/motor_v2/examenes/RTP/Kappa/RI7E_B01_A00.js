

json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Inicia el programa al presionar la tecla espacio.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Finaliza el programa al presionar la tecla espacio.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Mueve el robot en círculos al presionar la tecla espacio.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br><center><img src='RTPK_B01_R01_01.png' width=350px height=130px/></center><br>¿Para qué sirve este bloque de programación?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Derecha.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Adelante.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Atrás.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Izquierda.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br><center><img src='RTPK_B01_R02_01.png' width=360px height=80px/></center><br>El bloque de movimiento indica la dirección del robot mediante las velocidades que coloques para cada rueda. ¿Hacia dónde girará el robot si tienes este bloque?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "La posición del Birdy.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "La intensidad de luz del Birdy.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "El volumen de un sonido de Birdy.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br> El acelerómetro que tiene el Birdy lo utilizas para detectar <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Repite el programa ilimitadamente.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Repite el programa 3 veces.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Repite el programa hasta detectar movimiento.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br> <center><img src='RTPK_B01_R04_01.png' width=280px height=130px/></center><br>¿Qué hace este bloque de programación?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Parar los motores del Birdy.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Apagar los leds del Birdy.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Detener la lectura de los sensores.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br><center><img src='RTPK_B01_R05_01.png' width=280px height=130px/></center><br>El bloque <b>Stop Finch</b> sirve para <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Sensor de luz.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sensor de sonido.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Sensor de distancia.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br> Carla quiere programar su robot Birdy para que se mueva en cuanto detecte la luz de la lámpara. ¿Qué sensor debería usar?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "<img src='RTPK_B01_R07_01.png' width=380px height=120px/>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='RTPK_B01_R07_02.png' width=380px height=120px/>",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "<img src='RTPK_B01_R07_03.png' width=380px height=120px/>",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br> Luis necesita que el Birdy que está programando encienda la nariz de un color. ¿Qué bloque debe usar?",
            "t11instruccion": "",
        }
    },
];