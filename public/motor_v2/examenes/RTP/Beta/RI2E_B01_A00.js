json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Primer grado",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Segundo grado",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Tercer grado",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>¿Qué grado de palanca es una catapulta?",
            "t11instruccion": "",
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "8",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "3",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "5",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "1",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br> Si quieres lanzar un sacapuntas, ¿cuántas bolitas de plastilina te ayudarían a que el objeto salga disparado con más fuerza?",
            "t11instruccion": "<center> <img style='height: 500px; display:block; ' src='RTPB_B01_R02_01.png'> </center>",
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "A mayor inclinación, mayor velocidad",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "A mayor inclinación, menor velocidad",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "La velocidad no tiene nada que ver con la inclinación",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>Si tu vehículo lo dejas rodar sobre un plano inclinado, ¿cuál es la mejor opción para que el vehículo recorra una distancia más larga?",
            "t11instruccion": "",
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Cuña",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Hélice",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Rueda",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Polea",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>¿En cuáles de estos objetos encuentras planos inclinados?",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Si una hélice tiene un plano inclinado mayor, empujará más aire.",
                "t17correcta": "1",
                
            },
            {
                "t13respuesta": "Si una hélice tiene un plano inclinado menor, empujará más aire.",
                "t17correcta": "0",
                
            },
            {
                "t13respuesta": "Si una hélice no tiene plano inclinado, empujará más aire.",
                "t17correcta": "0",
                
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.",
            "contieneDistractores": true,
            
            "evaluable": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Punto de apoyo",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Contrapeso o carga",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Canastilla o fuerza",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Ruedas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Engranes",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>Diego quiere crear una catapulta, ¿qué partes son necesarias para que una catapulta funcione?",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Plano inclinado",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Palanca",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Ruedas y ejes",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>Pablo está usando una rampa para que su vehículo recorra más distancia, ¿qué tipo de máquina simple es una rampa?",
            "t11instruccion": "",
        }
    },
];