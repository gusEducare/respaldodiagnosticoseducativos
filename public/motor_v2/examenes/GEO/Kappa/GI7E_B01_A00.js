json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Bienestar físico",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Bienestar material",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Bienestar social",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Bienestar emocional",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Desarrollo",
                "t17correcta": "4",
            }
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ingresos",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Inteligencia emocional",
                "correcta": "3",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pertenencias",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Salud",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Autoestima",
                "correcta": "3",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Relaciones personales",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Seguridad",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Productividad",
                "correcta": "4",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Familia",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Espiritualidad",
                "correcta": "3",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Educación",
                "correcta": "4",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Marca la opción que corresponda a cada componente de la <b>calidad de vida.</b> ",
            "t11instruccion": "",
            "descripcion": "Componente",
            "variante": "editable",
            "anchoColumnaPreguntas": 20,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El espacio geográfico se compone solamente de elementos naturales.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los componentes naturales del espacio son las formas de relieve, como montañas, mesetas, llanuras, cuerpos de agua (ríos, lagos, lagunas, mares), flora, fauna, etc.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La organización de una población, sus características específicas y la distribución sobre el terreno que habitan, son componentes sociales.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son componentes económicos las distintas manifestaciones que hay en las sociedades y las caracterizan, como el idioma, la religión, las costumbres y las tradiciones, etc.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los movimientos migratorios y la cantidad de población urbana o rural, son componentes políticos del espacio.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La manera como se organizan los seres humanos, por ejemplo, las leyes que  se establecen sobre un territorio, son componentes políticos.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Para ubicar elementos geográficos-espaciales.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Para mostrar información sobre desastres naturales.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Para mostrar la ubicación de una persona en un espacio geográfico.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Para calcular las coordenadas sobre la superficie terrestre a partir de cualquier punto.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Para qué sirve un mapa?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Muestra una <br>representación plana<br> de la Tierra.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Muestra los continentes<br> con  mejor proporción.",
                "t17correcta": "3,5"
            },
            {
                "t13respuesta": "Representa con  mayor<br> fidelidad una parte<br> del hemisferio. ",
                "t17correcta": "5,3"
            },
            {
                "t13respuesta": "Muestra a la Tierra<br> sobre un elipse dividido<br> en varios segmentos.",
                "t17correcta": "2,4"
            },
            {
                "t13respuesta": "Presenta las proporciones<br> terrestres de norte a<br> sur con gran exactitud.",
                "t17correcta": "4,2"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Proyección cilíndrica o de Mercator.</td><td>Proyección cónica.</td><td>Proyección homalosenoidal de Goode.</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>Muestra la representación<br> de la Tierra por medio<br> de un cono.</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Muestra paralelos<br> y meridianos.</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td></td><td>"
            },
            {
                "t11pregunta": "</td><td></td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que correspondan a cada tipo de proyección cartográfica.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "homalosenoidal de Goode",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "cónica",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "cilíndrica o de Mercator",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br><center><img src='GI7E_B01_T2_R02.png' width=420px height=250px/></center><br>Es una proyección cartográfica de tipo <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Educación",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Salud",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Vivienda",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Alimentación",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Comunicación",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Transporte",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Jugar",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Escuchar música",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son necesidades básicas:",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },

        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "GPS significa Sistema de Posicionamiento Global.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El GPS nos permite saber sobre el clima de nuestra localidad.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El GPS es un sistema que calcula las coordenadas sobre la superficie terrestre a partir de cualquier punto.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los satélites comunican a una persona con otra y son fuentes de consulta de noticias diarias.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al coordinar una serie de satélites en diferentes órbitas se logra obtener datos para determinar la ubicación de una persona.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El SIG es un software que integra múltiples bases de datos con información geográfica.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El SIG es un instrumento que permite observar entes invisibles al ojo humano.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "el hemisferio norte del continente americano.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "la Sierra Madre Occidental y la Sierra Madre Oriental.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "extensión territorial.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "valles, cañones, cadenas montañosas, planicies costeras, altiplanicies y depresiones.",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "México se ubica en"
            },
            {
                "t11pregunta": "Son los elementos más sobresalientes del relieve en el territorio mexicano:"
            },
            {
                "t11pregunta": "México ocupa, a nivel mundial, el 5º lugar en"
            },
            {
                "t11pregunta": "Algunos ejemplos de variaciones de relieve en México son:"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Deformaciones de cualquier porción de tierra y del fondo de los mares y de los océanos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Capa gruesa y rocosa.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Es una capa del modelo dinámico y se encuentra fragmentada por las placas tectónicas.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Capa delgada que está debajo de las aguas.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Se da cuando dos placas se deslizan horizontalmente entre sí.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Es una capa del modelo estático y se encuentra en el centro de la Tierra.",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "Aquí nacen aproximadamente el 90% de los terremotos del mundo.",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Relieve"
            },
            {
                "t11pregunta": "Corteza continental"
            },
            {
                "t11pregunta": "Litosfera"
            },
            {
                "t11pregunta": "Corteza océanica"
            },
            {
                "t11pregunta": "Límite transformante"
            },
            {
                "t11pregunta": "Núcleo"
            },
            {
                "t11pregunta": "Cinturón de Fuego del Pacífico"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Biológicos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Atmosféricos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Hidrológicos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Sismos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Volcanes",
                "t17correcta": "0",
                "numeroPregunta": "0"
            }, //////////////////////////////////////////
            {
                "t13respuesta": "Meseta",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Cordillera",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Valle",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Sierra",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Bahía",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Estrecho",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }, //////////////////////////////////////////////////

            {
                "t13respuesta": "Playa",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Península",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Archipiélago",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Golfo",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Valle",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Cordillera",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>Son factores externos que intervienen en el cambio del relieve.", "Ejemplos de relieves terrestres.", "Ejemplos de relieves de tipo costero."],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "geodinámica",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "cambio",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "terrestre",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "relieves",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "biología",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "retroceso",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "marina",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "fauna",
                "t17correcta": "8"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "La "
            },
            {
                "t11pregunta": " es una rama de la geología que estudia los procesos que provocan un "
            },
            {
                "t11pregunta": " en la estructura y forma de la corteza "
            },
            {
                "t11pregunta": ", para eso, es necesario conocer acerca de las capas de la Tierra, de los "
            },
            {
                "t11pregunta": " y de factores que causan estos cambios.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Aguas de ríos, lagos,<br> subterráneos y glaciares.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Su movimiento produce olas,<br> mareas y corrientes marinas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "La tala de árboles evita su <br>filtración al subsuelo terrestre.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Aguas de mares y océanos.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Líquido vital de <br>plantas y animales.",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Aguas oceánicas</td><td>Aguas continentales</td></tr><tr><td>Contienen gran cantidad de minerales.</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>La mayor parte se encuentra en glaciares.</td></tr><tr><td>Regulan el clima.</td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra cada elemento a donde corresponda.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            },

        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El 70% de la superficie terrestre está compuesto por agua, de la cual, el 83% se encuentra en los océanos.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Sol filtra el agua de la Tierra, convirtiéndola en vapor, el cual se condensa y forma nubes.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las aguas del planeta se dividen en oceánicas y marítimas.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La hidrósfera es la capa de agua que rodea a la Tierra.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El agua puede tomar diferentes estados físicos, dependiendo de el lugar en donde cae.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Ecuador.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Trópico de Cáncer.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Meridiano de Greenwich.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Solsticio de invierno.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Trópico de Capricornio.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br><center><img src='GI7E_B01_T6_R01.png' width=250px height=250px/></center><br>Nombre de la línea que divide la Tierra en hemisferio norte y hemisferio sur.",
            "t11instruccion": "",
        }
    },


    {
        "respuestas": [
            {
                "t13respuesta": "<img src='GI7E_B01_T6_R02_01.png' width=200px height=150px/>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='GI7E_B01_T6_R02_02.png' width=200px height=150px/>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='GI7E_B01_T6_R02_03.png' width=200px height=150px/>",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Precipitaciones</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Temperatura</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Viento</td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Relaciona cada imagen con el concepto que corresponda.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Las plantas y los árboles de un lugar influyen en el clima de este.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Peso que tiene el aire sobre una parte de la superficie terrestre.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "El mar es un regulador de temperatura natural, puesto que calienta y enfría lentamente las costas.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Distancia de un punto con respecto al Ecuador.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Las corrientes frías causan precipitaciones, mientras que las cálidas aumentan la temperatura.",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Permite que exista diversidad de flora, generando un clima determinado.",
                "t17correcta": "6",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Vegetación"
            },
            {
                "t11pregunta": "Presión atmosférica"
            },
            {
                "t11pregunta": "Proximidad con los mares"
            },
            {
                "t11pregunta": "Latitud"
            },
            {
                "t11pregunta": "Corrientes oceánicas"
            },
            {
                "t11pregunta": "Influencia del suelo"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
/*
    {
        "respuestas": [
            {
                "t13respuesta": "Es la variedad de formas de vida que existen en el planeta.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es tener varios animales juntos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "La oportunidad de ser diferentes.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Los distintos países en el mundo.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "La variedad de cactáceas que viven en el desierto mexicano.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>¿Qué es la diversidad biológica?",
            "t11instruccion": "",
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Abundante vegetación y fauna.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Escasez de lluvias.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Altas temperaturas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Temperaturas por debajo de 0°.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "La temperatura no varía en todo el año.",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Zona cálida</td><td>Zona fría</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>Días y noches que duran 6 meses.</td></tr><tr><td>El suelo no permite la agricultura.</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>Nula humedad.</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra cada elemento donde corresponda.<br><br>¿Cuáles características pertenecen a la zona cálida y cuáles a la zona fría?",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "Relieve",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Movimiento del viento.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Inclinación de la Tierra",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Movimiento de traslación",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Vegetación",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Animales",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Placas tectónicas",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Condiciones geográficas que intervienen en el clima.",
            "t11instruccion": "",
        }
    },


    {
        "respuestas": [
            {
                "t13respuesta": "Zona fría",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Zona templada",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Zona cálida",
                "t17correcta": "2",
            }
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Matorrales, juncos y musgos.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Subclima oceánico.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Subclima de alta montaña.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El clima en verano es cálido y seco.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Subclima ecuatorial.",
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Animales como elefantes, camellos, cocodrilos y monos.",
                "correcta": "2",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada zona.</p>",
            "t11instruccion": "",
            "descripcion": "Zona",
            "variante": "editable",
            "anchoColumnaPreguntas": 45,
            "evaluable": true
        }
    }*/
];