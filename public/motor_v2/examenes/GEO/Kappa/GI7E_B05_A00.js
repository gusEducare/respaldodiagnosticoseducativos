json=[
    //1
    {
      "respuestas": [
          {
              "t13respuesta": "<p>supervivencia<\/p>",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "<p>naturaleza<\/p>",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "<p>necesidades<\/p>",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "<p>sociedades urbanas<\/p>",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "<p>sobrepoblación<\/p>",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "<p>contaminación<\/p>",
              "t17correcta": "6"
          }, 
          {
              "t13respuesta": "<p>desigual<\/p>",
              "t17correcta": "7"
          },
          {
              "t13respuesta": "<p>insatisfechas<\/p>",
              "t17correcta": "8"
          },
          {
              "t13respuesta": "<p>calidad de vida<\/p>",
              "t17correcta": "9"
          },
          {
              "t13respuesta": "<p>educación<\/p>",
              "t17correcta": "10"
          },
          {
              "t13respuesta": "<p>salud<\/p>",
              "t17correcta": "11"
          },
          {
              "t13respuesta": "<p>empleo<\/p>",
              "t17correcta": "12"
          },
          {
              "t13respuesta": "<p>bienestar social<\/p>",
              "t17correcta": "13"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "<p>Vivir dentro de una sociedad ya no implica solo <\/p>"
          },
          {
              "t11pregunta": "<p> como en el caso de nuestros antepasados, hace unos cuantos miles de años. Si bien es cierto que hay quien dice que las sociedades surgen para que los hombres logren protegerse del ímpetu de la <\/p>"
          },
          {
              "t11pregunta": "<p> y tengan una vida más fácil donde puedan cubrir todas sus <\/p>"
          },
          {
              "t11pregunta": "<p>, también es cierto que hoy día es difícil satisfacer todas y cada una de esas necesidades de manera fácil. En cierta forma, vivir en  <\/p>"
          },
          {
              "t11pregunta": "<p>, como lo hacemos hoy en día, genera una serie de problemas que necesitan ser igualmente resueltos, por ejemplo, la <\/p>"
          },
          {
              "t11pregunta": "<p> o la <\/p>"
          },
          {
              "t11pregunta": "<p>. El hecho de que tantas personas vivamos juntas en un espacio relativamente reducido implica que la situación sea bastante <\/p>"
          },
          {
              "t11pregunta": "<p>, por una parte; por otra, ocasiona también que las personas comiencen a sentirse <\/p>"
          },
          {
              "t11pregunta": "<p> pues no pueden cubrir sus necesidades de una manera fácil.<br>El término <\/p>"
          },
          {
              "t11pregunta": "<p> denota entonces la búsqueda que hace la sociedad para colocarse en una situación donde la vida sea más fácil, cómoda y placentera. Como hemos visto anteriormente la calidad de vida depende de varios factores como la <\/p>"
          },
          {
              "t11pregunta": "<p>, la <\/p>"
          },
          {
              "t11pregunta": "<p>, el <\/p>"
          },
          {
              "t11pregunta": "<p>, la estabilidad económica y el <\/p>"
          },
          {
              "t11pregunta": "<p>. Todos y cada uno son aspectos importantes para impulsar la sociedad a un mejor desarrollo y una mejor situación donde sus habitantes puedan sentirse satisfechos viviendo en ella. Sin embargo, la calidad de vida es un concepto que parte de cada uno de nosotros.<\/p>"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "8",
          "t11pregunta": "Completa el párrafo con las palabras de los recuadros que correspondan.",
          "respuestasLargas": true,
          "pintaUltimaCaja": false,
          "contieneDistractores":true
      }
  },
    //2
    {
      "respuestas": [{
          "t13respuesta": "Falso",
          "t17correcta": "0"
        },
        {
          "t13respuesta": "Verdadero",
          "t17correcta": "1"
        }
      ],
      "preguntas": [{
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "La calidad de vida implica un concepto meramente subjetivo.",
          "correcta": "1"
        },
        {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "Los factores para alcanzar cierta calidad de vida son los mismos siempre para cualquier país.",
          "correcta": "0"
        },
        {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "En los países que no tienen un fácil acceso a la educación, a la salud, una buena conservación del medio ambiente e ingresos altos para cubrir los gastos de entretenimiento, podemos hablar de que tienen una óptima calidad de vida.",
          "correcta": "0"
        },
        {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "México forma parte de la OCDE, cuyas siglas significan Organización Común para el Desarrollo Empresarial.",
          "correcta": "0"
        },
        {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "Para lograr un desarrollo económico y alcanzar cierta calidad de vida, los países explotan sus recursos naturales.",
          "correcta": "1"
        },
        {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "México cuenta con una gran biodiversidad conservada principalmente por la explotación de los recursos.",
          "correcta": "0"
        },
      ],
      "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
        "descripcion": "Reactivo",
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable": true
      }
    },
    //3
    {
      "respuestas": [
          {
              "t13respuesta": "Programa de las Naciones Unidas para el Medio Ambiente (PNUMA)",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "Protocolo de Montreal",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "Protocolo de Kyoto",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "Ley Federal para Prevenir y Controlar la Contaminación Ambiental",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "Ley General de Vida Silvestre",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "Programa 21",
              "t17correcta": "6"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "Evalúa el estado del medio ambiente, diseña estrategias para fomentar el desarrollo sustentable y se asegura del cumplimiento de normas ambientales."
          },
          {
              "t11pregunta": "Establece las medidas para el uso de sustancias que dañan la capa de ozono (1987)."
          },
          {
              "t11pregunta": "Fue firmado por 160 países, se establecieron metas para disminuir el efecto invernadero y en él surgió el concepto de desarrollo sustentable."
          },
          {
              "t11pregunta": "Su enfoque está dirigido a considerar las consecuencias de la contaminación del agua en la salud en México."
          },
          {
              "t11pregunta": "Tiene como propósito la conservación de la vida silvestre y el aprovechamiento sustentable de los recursos forestales en México."
          },
          {
              "t11pregunta": "Establece programas a nivel mundial para el desarrollo sustentable (1992)."
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta": "Relaciona las columnas según corresponda."
      },

  },
    //4
    {
        "respuestas": [{
            "t13respuesta": "SEMARNAT",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "CONAFOR",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "CONANP",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "SEDESOL",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "SECTUR",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Integrar el desarrollo y la conservación.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Conservar la integridad ecológica del planeta.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Satisfacer las necesidades de la población.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Desarrollar tecnología e innovación.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Aumentar la industrialización.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Quintana Roo.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Yucatán.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Chiapas.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Querétaro.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Ley de Aguas Nacionales.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Ley Federal de Sanidad Animal.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Ley General de Desarrollo Forestal Sustentable.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Convención de Lucha contra la desertificación.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Convenio sobre la Diversidad Biológica.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          }
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "2",
          "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>Las instituciones que se encargan de preservar el medio ambiente en nuestro país son:",
          "Son los requisitos establecidos en la Conferencia de Ottawa para conseguir un desarrollo sustentable.",
          "Son Estados con un alto grado de deforestación.",
          "Son algunas de las Leyes para el cuidado del medio ambiente  y el desarrollo sustentable en México.",],
      
          "preguntasMultiples": true
        }
      },
      //5
      {
        "respuestas": [{
            "t13respuesta": "Área de manejo de hábitats/especies",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Conservación de paisajes terrestres y marítimos",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Área Natural Silvestre",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Conservación y protección del ecosistema Parque Nacional",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Monumento Natural",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Área Protegida Manejada",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Monumento Natural",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Área Natural Silvestre",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Conservación y protección del ecosistema Parque Naciona",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Área de manejo de hábitats/especies",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Reserva Natural Estricta",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Monumento Natural",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Área Protegida Manejada",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Conservación de paisajes terrestres y marítimos",
            "t17correcta": "1",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Reserva Natural Estricta",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Área Protegida Manejada",
            "t17correcta": "1",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Conservación de paisajes terrestres y marítimos",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Monumento Natural",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Reserva Natural Estricta",
            "t17correcta": "1",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Área de manejo de hábitats/especies",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Monumento Natural",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Área de gran extensión que favorece las condiciones de los ecosistemas y la protección de las especies (es de protección estricta).",
          "Área de gran extensión que favorece las condiciones de los ecosistemas y la recreación.",
          "Área pequeña que contiene una o más cualidades específicas naturales o culturales únicas, con valor estético y cultural de alto interés para los visitantes.",
          "Áreas pequeñas que protegen hábitats o especies particulares.",
          "Áreas grandes de tierra, con costa o mar apropiados; con valor ecológico, biológico cultural y escénico.",
          "Área grande que mantiene la biodiversidad biológica y provee de productos y servicios naturales a las comunidades humanas.","Área de tierra o mar de extensión pequeña destinada principalmente a la investigación científica,  monitoreo ambiental. (Es de protección estricta)."],
      
          "preguntasMultiples": true
        }
      },
    //6
    {
      "respuestas": [{
          "t13respuesta": "Es aquella que alberga una vegetación o fauna en particular, diversidad biológica o interés cultural, y en la que se establecen restricciones para preservarla.",
          "t17correcta": "1",
          "numeroPregunta": "0"
        },
        {
          "t13respuesta": "Es aquella que se encuentra lejos de un medio urbano.",
          "t17correcta": "0",
          "numeroPregunta": "0"
        },
        {
          "t13respuesta": "Es aquella protegida militarmente en aras de producción de recursos naturales.",
          "t17correcta": "0",
          "numeroPregunta": "0"
        },
        {
          "t13respuesta": "Es aquella que garantiza la supervivencia de una población por la cantidad de recursos naturales que alberga, es perfecta para su explotación.",
          "t17correcta": "0",
          "numeroPregunta": "0"
        },
        {
          "t13respuesta": "Un 15.4% de la superficie de la tierra se encuentra protegida.",
          "t17correcta": "1",
          "numeroPregunta": "1"
        },
        {
          "t13respuesta": "Un 74.8% de la superficie terrestres se encuentra protegida.",
          "t17correcta": "0",
          "numeroPregunta": "1"
        },
        {
          "t13respuesta": "Un 36.4% de la superficie terrestres se encuentra protegida.",
          "t17correcta": "0",
          "numeroPregunta": "1"
        },
        {
          "t13respuesta": "Un 7.7% de la superficie de la tierra se encuentra protegida.",
          "t17correcta": "0",
          "numeroPregunta": "1"
        },
        {
          "t13respuesta": "Parque Natural del Valle del Río en Somalia.",
          "t17correcta": "1",
          "numeroPregunta": "2"
        },
        {
          "t13respuesta": "Parque Nacional de la Tijuca en Brasil.",
          "t17correcta": "0",
          "numeroPregunta": "2"
        },
        {
          "t13respuesta": "La ciudadela Inca de Machu Picchu en Perú.",
          "t17correcta": "0",
          "numeroPregunta": "2"
        },
        {
          "t13respuesta": "Parque Nacional Uluru-Kata Tjuta en Australia.",
          "t17correcta": "0",
          "numeroPregunta": "2"
        },
        {
          "t13respuesta": "Alrededor de 10% de la biodiversidad mundial.",
          "t17correcta": "1",
          "numeroPregunta": "3"
        },
        {
          "t13respuesta": "Alrededor de 30% de la biodiversidad mundial.",
          "t17correcta": "0",
          "numeroPregunta": "3"
        },
        {
          "t13respuesta": "Alrededor de 15% de la biodiversidad mundial.",
          "t17correcta": "0",
          "numeroPregunta": "3"
        },
        {
          "t13respuesta": "Alrededor de 5% de la biodiversidad mundial.",
          "t17correcta": "0",
          "numeroPregunta": "3"
        },
        {
          "t13respuesta": "Comisión Nacional de Áreas Naturales Protegidas.",
          "t17correcta": "1",
          "numeroPregunta": "4"
        },
        {
          "t13respuesta": "Colegio Nacional de Áreas Naturales Protegidas.",
          "t17correcta": "0",
          "numeroPregunta": "4"
        },
        {
          "t13respuesta": "Comisión Natural de Áreas Nacionales de Precipitación.",
          "t17correcta": "0",
          "numeroPregunta": "4"
        },
        {
          "t13respuesta": "Colegio Natural de Áreas Nacionales Protegidas.",
          "t17correcta": "0",
          "numeroPregunta": "4"
        },
        {
          "t13respuesta": "5 Monumentos naturales y 18 Santuarios.",
          "t17correcta": "1",
          "numeroPregunta": "5"
        },
        {
          "t13respuesta": "16 Monumentos Naturales y 42 Lagos.",
          "t17correcta": "0",
          "numeroPregunta": "5"
        },
        {
          "t13respuesta": "32 Sierras y 25 Santuarios.",
          "t17correcta": "0",
          "numeroPregunta": "5"
        },
        {
          "t13respuesta": "18 Sierras y 5 Lagos.",
          "t17correcta": "0",
          "numeroPregunta": "5"
        },
        {
          "t13respuesta": "Son procesos ecológicos de los ecosistemas naturales que suministran a la humanidad una gran e importante gama de servicios gratuitos de los que dependemos. Su utilidad radica en la manutención de la biósfera y la vida que habita en ella. Estos procesos pueden ser, por ejemplo, la fotosíntesis. Están relacionados con la actividad humana también.",
          "t17correcta": "1",
          "numeroPregunta": "6"
        },
        {
          "t13respuesta": "Son procesos humanos que se llevan a cabo en lugares que han sido contaminados o dañados por otros procesos humanos. La importancia de estos servicios radica en la dedicación que le debemos al ambiente. Un ejemplo puede ser la limpieza de un río contaminado.",
          "t17correcta": "0",
          "numeroPregunta": "6"
        },
        {
          "t13respuesta": "Son procesos industriales que se llevan a cabo para adaptar el lugar en el que se va a trabajar. Su importancia es principalmente para evitar contaminar esa parte del ambiente. Un ejemplo de estos servicios puede ser la tala de árboles de un sector para construir una planta maderera.",
          "t17correcta": "0",
          "numeroPregunta": "6"
        },
        {
          "t13respuesta": "Son procesos de la biósfera que surgen con la intervención del hombre y para beneficio del mismo. Su utilidad va siempre en aras de construir un ecosistema administrado por el hombre y del que dependerá él mismo. Por ejemplo, la plantación de maíz en una parcela vacía.",
          "t17correcta": "0",
          "numeroPregunta": "6"
        },
        {
          "t13respuesta": "Proveyendo capacitación o infraestructura para alguna actividad económica sustentable.",
          "t17correcta": "1",
          "numeroPregunta": "7"
        },
        {
          "t13respuesta": "Proveyendo personal humano capacitado para la realización de actividades económicas primarias.",
          "t17correcta": "0",
          "numeroPregunta": "7"
        },
        {
          "t13respuesta": "Proveyendo vigilancia y certificaciones a quienes llevan a cabo actividades económicas secundarias.",
          "t17correcta": "0",
          "numeroPregunta": "7"
        },
        {
          "t13respuesta": "Proveyendo capital para la conservación de santuarios y nichos ecológicos que puedan tener especies en peligro de extinción.",
          "t17correcta": "0",
          "numeroPregunta": "7"
        },
        {
          "t13respuesta": "Son tecnologías que aprovechan los recursos y energías renovables.",
          "t17correcta": "1",
          "numeroPregunta": "8"
        },
        {
          "t13respuesta": "Son técnicas de aprendizaje geográfico para ubicar espacios naturales protegidos.",
          "t17correcta": "0",
          "numeroPregunta": "8"
        },
        {
          "t13respuesta": "Son situaciones específicas que se llevan a cabo con el fin de preservar un área.",
          "t17correcta": "0",
          "numeroPregunta": "8"
        },
        {
          "t13respuesta": "Son avances científicos que se dedican a estudiar ciertas áreas protegidas.",
          "t17correcta": "0",
          "numeroPregunta": "8"
        },
        {
          "t13respuesta": "Combustibles fósiles.",
          "t17correcta": "1",
          "numeroPregunta": "9"
        },
        {
          "t13respuesta": "Sol.",
          "t17correcta": "0",
          "numeroPregunta": "9"
        },
        {
          "t13respuesta": "Aire.",
          "t17correcta": "0",
          "numeroPregunta": "9"
        },
        {
          "t13respuesta": "Agua.",
          "t17correcta": "0",
          "numeroPregunta": "9"
        },
        {
          "t13respuesta": "Porque sin ellas, los humanos podrían abusar de los recursos naturales y crearían un gran daño al ecosistema.",
          "t17correcta": "1",
          "numeroPregunta": "10"
        },
        {
          "t13respuesta": "Debido a que son una gran inversión a futuro, por los turistas.",
          "t17correcta": "0",
          "numeroPregunta": "10"
        },
        {
          "t13respuesta": "Porque sin ellas perderíamos una gran parte de terreno útil para la explotación.",
          "t17correcta": "0",
          "numeroPregunta": "10"
        },
        {
          "t13respuesta": "No hay razón alguna para esta situación.",
          "t17correcta": "0",
          "numeroPregunta": "10"
        }
      ],
      "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué es un Área Natural Protegida?",
        "¿Qué cantidad de superficie terrestre alberga las Áreas Naturales Protegidas?",
        "¿Cuál de los siguientes no es un espacio protegido?",
        "¿Qué cantidad porcentual de la biodiversidad mundial se encuentra en México?",
        "En México, existen 177 Áreas Naturales Protegidas que se agrupan según diversas características. ¿Quién es el organismo que se encarga de administrar estos espacios?",
        "Las áreas protegidas que se encuentran en México son 41 Reservas de la biosfera, 39 Áreas de protección de flora y fauna, 66 Parques nacionales, 8 Áreas de protección de recursos naturales… ¿Cuáles de las siguientes deben incluirse en el enunciado anterior?",
        "¿Qué son y para qué sirven los servicios ambientales?",
        "La naturaleza nos provee de lo que necesitamos para subsistir, por ello, debemos ser precavidos de no dañarla y de conservarla lo mejor que se pueda. ¿Cómo logran esto los gobiernos?",
        "El desarrollo sustentable ha implementado como solución las ecotecnias, ¿qué son las ecotecnias?",
        "¿Cuál de las siguientes no es un recurso generador de energía renovable?",
        "¿Por qué es importante la existencia de Áreas Naturales Protegidas?"],
    
        "preguntasMultiples": true
      }
    },
    //7
    {
      "respuestas": [
          {
              "t13respuesta": "...Geológicos, Hidrometeorológicos y Antrópicos.",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "...son originados por el movimiento de las placas tectónicas.",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "...son causados por el movimiento de la atmósfera y el agua.",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "...son producto de la acción humana.",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "...las placas tectónicas, por lo cual son riesgos geológicos.",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "...dependen de la acción humana enteramente.",
              "t17correcta": "6"
          },
          {
              "t13respuesta": "...existe una constante posibilidad de que ocurra un fenómeno natural o un accidente creado por el hombre.",
              "t17correcta": "7"
          },
          {
              "t13respuesta": "...esta se manifiesta en sismos, erupciones volcánicas, lluvias torrenciales, etcétera.",
              "t17correcta": "8"
          },
          {
              "t13respuesta": "...se encuentra expuesta a riesgos y no cuenta con una cultura preventiva.",
              "t17correcta": "9"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "Existen en el mundo 3 tipos de  riesgos..."
          },
          {
              "t11pregunta": "Los riesgos geológicos..."
          },
          {
              "t11pregunta": "Los riesgos hidrometeorológicos..."
          },
          {
              "t11pregunta": "Los riesgos antrópicos..."
          },
          {
              "t11pregunta": "Los sismos, tsunamis, volcanes deslizamientos y hundimientos del suelo dependen de..."
          },
          {
              "t11pregunta": "Un incendio o una explosión como la de Chernobyl son riesgos antrópicos porque..."
          },
          {
              "t11pregunta": "Se dice que una población está en riesgo cuando..."
          },
          {
              "t11pregunta": "La tierra está en una constante liberación de energía que se acumula en su interior..."
          },
          {
              "t11pregunta": "Una población vulnerable es aquella que..."
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta": "Relaciona las columnas según corresponda."
      }
  },
  //8
  {
    "respuestas": [{
            "t13respuesta": "Falso",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1"
        },
    ],
    "preguntas": [{
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las condiciones socioeconómicas, culturales y políticas no tienden a incrementar la vulnerabilidad de las poblaciones ante los riesgos.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Una población es más vulnerable si no tiene una buena calidad de vida con acceso a la salud, la vivienda, servicios básicos, educación, etcétera.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los riesgos que incrementan la vulnerabilidad de la población son exclusivamente socioeconómicos, políticos y culturales.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las poblaciones que se asientan cerca de cerros, montañas, volcanes, ríos o mar, son poblaciones más vulnerables ante los desastres.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Según el Banco Mundial, 70% de las poblaciones vulnerables se encuentran en Asia.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las poblaciones con recursos económicos más bajos son las poblaciones más vulnerables a sufrir daños por parte de algún fenómeno natural.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En México las zonas más vulnerables son aquellas que también son las más pobres y marginadas, se encuentran en los estados de Chiapas, Guerrero y Oaxaca.",
            "correcta": "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Selecciona la casilla correcta según corresponda a la descripción de las Áreas Naturales Protegidas.",
        "descripcion": "Descripción",
        "variante": "editable",
        "anchoColumnaPreguntas": 70,
        "evaluable": true
    }
  },
    //9
    {
        "respuestas": [{
            "t13respuesta": "La ONU.",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "La OTAN.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "La UNESCO.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "La FIFA.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "No, supera al decenio de los ochenta.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Sí, un 50% al menos.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "No sabemos.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "No importa, de hecho no hay razón para alarmarse.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Para incrementar la resiliencia de las naciones ante los desastres naturales.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Para reducir la creación, producción y utilización de energía nuclear con fines bélicos.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Para reducir la emisión de gases invernadero y tóxicos a la atmósfera.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Para firmar un acuerdo de paz y un armisticio.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "A nivel nacional realiza actividades de investigación, capacitación, instrumentación y difusión de fenómenos que pueden originar situaciones de desastre.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "A nivel nacional realiza campañas para el conocimiento de los desastres ambientales del siglo XXI.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "A nivel internacional realiza actividades de investigación, capacitación, instrumentación y difusión de fenómenos que pueden originar situaciones de desastre.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Involucrarse de manera activa en capacitaciones, campañas y acciones que permitan la reflexión sobre la vulnerabilidad ante los desastres y su prevención.",
            "t17correcta": "1",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Involucrarse en las instituciones mediante cargos políticos que permitan la toma de decisiones para la prevención de desastres.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Participar únicamente en las votaciones de manera democrática para respaldar las acciones gubernamentales.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Ley General de Asentamientos Humanos, Ley General del Equilibrio Ecológico y la protección del medio ambiente, Ley General de Protección Civil.",
            "t17correcta": "1",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Sistema Nacional de Protección Civil, Ley de Residuos Terapéuticos, Ley General del Equilibrio Ecológico y la protección del medio ambiente.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Instituto Nacional de Antropología e Historia, Ley General de Asentamientos Humanos, Instituto Nacional de Estadística y Geografía, Ley General de Protección Civil.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          }
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cuál de los siguientes organismos ha dicho que en el periodo comprendido entre 2000 y 2010 el daño económico causado por los desastres naturales había ascendido a mil billones de dólares?",
          "¿Durante los últimos veinte años las pérdidas humanas ocasionadas por los desastres naturales han disminuido?",
          "En 2005 se celebró la Conferencia Mundial sobre la Reducción de Desastres en Hyogo, en ella 168 países firmaron el Marco de Acción de Hyogo, ¿para qué?",
          "¿Que función tiene el Centro Nacional de Prevención de Desastres (CENAPRED)?",
          "¿Cuál es el papel de la ciudadanía en las acciones gubernamentales para la prevención de desastres?",
          "¿Qué leyes existen en nuestro país con el fin de garantizar la prevención y disminución de riesgos?"],
      
          "preguntasMultiples": true
        }
      },
      //10
      {
        "respuestas": [
            {
                "t13respuesta": "...movimientos migratorios, desarrollo de epidemias y problemas con el control sanitario.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...contaminación, destrucción de la biodiversidad, pérdidas ecológicas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...pérdida de infraestructura, impacto negativo en el  funcionamiento de los servicios públicos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...la reducción de la vulnerabilidad de las poblaciones expuestas.",
                "t17correcta": "4"
            },
            {
              "t13respuesta": "Incendios",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "Huracanes",
              "t17correcta": "6"
          },
        ],
        "preguntas": [
            {
                "t11pregunta": "Un desastre natural o antrópico ocasiona a nivel social..."
            },
            {
                "t11pregunta": "Un desastre natural o antrópico ocasiona a nivel ambiental..."
            },
            {
                "t11pregunta": "Un desastre  natural o antrópico ocasiona a nivel económico..."
            },
            {
                "t11pregunta": "Los programas gubernamentales e instituciones que enseñan medidas de prevención ante desastres permiten..."
            },
            {
              "t11pregunta": "Suelen ocurrir debido a acciones irresponsables humanas como dejar vidrios o metales que reflejan los rayos del sol, quemar hierbas."
          },
          {
              "t11pregunta": "Se pueden predecir por medio del uso de sistemas meteorológicos de monitoreo y sistema de alarma."
          },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    //11
    {
      "respuestas": [
          
          {
              "t13respuesta": "Inundaciones y deslaves",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "Sismos",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "Antes, durante y después de una situación de riesgo",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "La prevención y participación constante de todos los sectores de la sociedad",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "Mejoran la calidad de vida, mitigan en gran proporción las pérdidas económicas y humanas",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "Medidas que pueden implementarse en las construcciones arquitectónicas para hacerlas resistentes a los fenómenos expuestos",
              "t17correcta": "6"
          },
          {
              "t13respuesta": "Programas de educación y prevención",
              "t17correcta": "7"
          }
      ],
      "preguntas": [
          
          {
              "t11pregunta": "Se pueden prevenir evitando los asentamientos humanos en depresiones, laderas, cerros y las orillas de los ríos"
          },
          {
              "t11pregunta": "Se producen a raíz de los movimientos de las placas tectónicas y no pueden preverse."
          },
          {
              "t11pregunta": "Los planes de prevención deben contemplar las siguientes etapas:"
          },
          {
              "t11pregunta": "Es la clave del éxito para mitigar las consecuencias  de los riesgos naturales o antrópicos."
          },
          {
              "t11pregunta": "Son ventajas de la inversión en prevención  y capacitación ante desastres."
          },
          {
              "t11pregunta": "Medidas estructurales."
          },
          {
              "t11pregunta": "Medidas no estructurales."
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta": "Relaciona las columnas según corresponda."
      }
  },
     
];
