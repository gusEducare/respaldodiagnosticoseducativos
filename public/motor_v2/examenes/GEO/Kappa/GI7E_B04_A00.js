json=[
    //1
    {
        "respuestas": [{
            "t13respuesta": "Agricultura de plantación.",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Agricultura  intensiva.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Agricultura comercial.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Agricultura de subsistencia.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Agricultura de plantación.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Agricultura comercial.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Agricultura comercial.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Agricultura mercantil.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Agricultura de plantación.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Estados Unidos, Rusia, Brasil e India, dentro del país Sonora y Nuevo León.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "México, Rusia, Estados unidos, Canadá y China, dentro del país Sonora y Chihuahua.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "India, Estados Unidos, Brasil y Rusia, dentro del país Sonora y Chihuahua.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Establecerse en áreas de terreno delimitadas, el ganado se alimenta de pastos naturales o cultivados.",
            "t17correcta": "1",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Criar ganado en establos y corrales, el ganado se alimenta de pastos naturales o cultivados.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Establecerse en áreas de terreno delimitadas, el ganado se alimenta con distintos productos existentes en el mercado.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Ganadería de pastoreo nómada.",
            "t17correcta": "1",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Ganadería extensiva.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Ganadería intensiva.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Bovino, como fuente de trabajo en actividades agrícolas; porcino, para la fabricación de cueros, y ovino, se utiliza su lana.",
            "t17correcta": "1",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Bovino, se utiliza su lana; porcino, como fuente de trabajo en actividades agrícolas, y ovino, para la fabricación de pieles.",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Bovino, como fuente de actividades agrícolas; porcino y ovino se utilizan para hacer productos lácteos, y ovino, para el pastoreo.",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Combustibles para hacer leña, carbón, resinas, fibras, medicinas.",
            "t17correcta": "1",
            "numeroPregunta": "7"
          },
          {
            "t13respuesta": "Combustibles eólicos, resinas, medicinas, productos maderables.",
            "t17correcta": "0",
            "numeroPregunta": "7"
          },
          {
            "t13respuesta": "Combustibles fósiles, medicinas, resinas, cauchos y  energía solar.",
            "t17correcta": "0",
            "numeroPregunta": "7"
          },
          {
            "t13respuesta": "Altura, se practica en alta mar con altas embarcaciones, y Cabotaje, se practica cerca del litoral con pequeñas embarcaciones.",
            "t17correcta": "1",
            "numeroPregunta": "8"
          },
          {
            "t13respuesta": "Altura, se practica en alta mar con altas embarcaciones, y Baja, se practica cerca del litoral con pequeñas embarcaciones.",
            "t17correcta": "0",
            "numeroPregunta": "8"
          },
          {
            "t13respuesta": "Cabotaje, se practica en aguas bajas y con pequeñas embarcaciones, y Alta, se practica en mar con barcos.",
            "t17correcta": "0",
            "numeroPregunta": "8"
          },
          {
            "t13respuesta": "Sardina y atún, Perú, China, Japón, Estados Unidos e India.",
            "t17correcta": "1",
            "numeroPregunta": "9"
          },
          {
            "t13respuesta": "Crustáceos y atún, China, México, Estados Unidos y Brasil",
            "t17correcta": "0",
            "numeroPregunta": "9"
          },
          {
            "t13respuesta": "Sardina y Mojarra, China, México, Estados unidos e India.",
            "t17correcta": "0",
            "numeroPregunta": "9"
          }
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Sus cultivos más comunes son el cacao, el plátano y el café. Su finalidad es abastecer grandes mercados ubicados en países desarrollados de zonas templadas.",
          "Se caracteriza por tener poca producción pues depende  de la fuerza humana, del trabajo animal y de la época de lluvia para las plantaciones.",
          "Tiene el propósito de obtener ganancias con menores costos de producción. Utiliza maquinarias y técnicas modernas.",
          "La actividad ganadera se caracteriza  por ser amplia, y se practica en casi todo el mundo. ¿Cuáles son los países con mayor producción a nivel mundial y, específicamente, qué estados en México?",
          "La ganadería extensiva se distingue por:",
          "Se diferencia por el desplazamiento continuo del ganado hacia donde se encuentren pastos disponibles.",
          "Son algunos de los diferentes usos de la cría de ganado.",
          "De los bosques es posible obtener:",
          "La mayor actividad pesquera se concentra en las aguas oceánicas, donde se producen los siguientes tipos de pesca:",
          "Son los países en donde se desarrolla el mayor volumen de pesca y se obtiene principalmente lo siguiente:"
          ],
      
          "preguntasMultiples": true
        }
      },
      //2
      {
        "respuestas": [
            {
                "t13respuesta": "el tipo de manejo ganadero, agricultura, pesquero y forestal.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Recursos naturales.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Suelo fértil, disposición de agua, temperatura y humedad adecuada.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Ganadería.",
                "t17correcta": "4"
            },
            {
              "t13respuesta": "Espacios forestales.",
              "t17correcta": "5"
            },
            {
                "t13respuesta": "Clima.",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Dependen de la incorporación de la tecnología que puede agilizar la producción y determinan..."
            },
            {
                "t11pregunta": "Determinan los asentamientos de grupos humanos en un lugar específico y las actividades económicas que se pueden llevar  a cabo..."
            },
            {
                "t11pregunta": "La agricultura necesita..."
            },
            {
                "t11pregunta": "Depende de la topografía, el acceso al agua y el clima..."
            },
            {
                "t11pregunta": "Dependen de la altitud, latitud, humedad y relieve…"
            },
            {
                "t11pregunta": "Determina el tipo de vegetación que crece en un lugar..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
      },
      //3
      {
        "respuestas": [{
            "t13respuesta": "Minerales no metálicos.",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Minerales metálicos.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Minerales energéticos.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Metálicos brillosos, se usan en aleaciones y no ferrosos; energéticos, incluyen al carbón, el petróleo y el gas natural;  y  no metálicos que son buenos aislantes, industriales y tierras raras.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "No metálicos que se caracterizan por ser brillosos, no ferrosos y de aleación; metálicos que incluyen aislantes y tierras raras;  y  energéticos como el carbón, petróleo y tierras raras.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Energéticos como el gas natural y el carbón, los cuales son buenos aislantes; metálicos que incluyen metales preciosos, de aleación, no ferrosos y tierras raras; y no metálicos que se caracterizan por ser no ferrosos, y estratégicos.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Energéticos, metálicos, preciosos.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Zinc, oro y plata.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Esmeralda, rubí y oro.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Magnesio, uranio y plata.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Asia, Australia, Europa y América del Norte.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "América del Norte.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Irak, Grecia, Canadá y Chile.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Medio Oriente, el sur de Rusia y  Venezuela.",
            "t17correcta": "1",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Venezuela, México y Estados Unidos.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Medio Oriente, el norte de Rusia y México.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Minería subterránea.",
            "t17correcta": "1",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Minería dragado.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Minería de superficie o explotaciones a cielo abierto.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Minería por pozos de perforación.",
            "t17correcta": "1",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Minería subterránea.",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Minería a cielo abierto.",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Esmeralda, rubí, asbesto.",
            "t17correcta": "1",
            "numeroPregunta": "7"
          },
          {
            "t13respuesta": "Esmeralda, uranio, cobre.",
            "t17correcta": "0",
            "numeroPregunta": "7"
          },
          {
            "t13respuesta": "Magnesio, azufre, grafito.",
            "t17correcta": "0",
            "numeroPregunta": "7"
          },
          {
            "t13respuesta": "Constituye una de las fuentes principales de ingreso para distintos países, otros utilizan los productos derivados de estos para la comercialización.",
            "t17correcta": "1",
            "numeroPregunta": "8"
          },
          {
            "t13respuesta": "Sirve para la fabricación de alimentos primarios que se distribuyen en el mundo y aportan al flujo de la economía.",
            "t17correcta": "0",
            "numeroPregunta": "8"
          },
          {
            "t13respuesta": "Constituye una fuente principal para la fabricación de aparatos electrónicos en países de primer mundo, además de ser utilizados para creación de alimentos sintéticos que se comercializan.",
            "t17correcta": "0",
            "numeroPregunta": "8"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Se encuentran en grandes volúmenes y su costo es más económico.",
          "La clasificación de los minerales es la siguiente:",
          "Son ejemplos de minerales metálicos.",
          "Son los depósitos más abundantes de recursos minerales en el mundo.",
          "Principales reservas de petróleo del mundo.",
          "Se realiza a profundidad mediante el uso de dinamita. Es el método para extraer el carbón.",
          "Se emplea para la obtención de combustibles como el petróleo y el gas natural.",
          "Son ejemplos de minerales no metálicos.",
          "La importancia de los recursos minerales y energéticos para la economía mundial se puede resumir en lo siguiente:",
          ],
      
          "preguntasMultiples": true
        }
      },
      //4
      {
        "respuestas": [
            {
                "t13respuesta": "<p>industrialización<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>desarrollo económico<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>distribución<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>mano de obra<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>transformar<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>mineral<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>espacio geográfico<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>consumo directo<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>alimentos<\/p>",
                "t17correcta": "9,10,11"
            },
            {
                "t13respuesta": "<p>medicamentos<\/p>",
                "t17correcta": "10,9,11"
            },
            {
              "t13respuesta": "<p>ropa<\/p>",
              "t17correcta": "11,9,10"
            },
            {
              "t13respuesta": "<p>40% del Producto<\/p>",
              "t17correcta": "12"
            },
            {
              "t13respuesta": "<p>Interno Bruto<\/p>",
              "t17correcta": "13"
            },
            {
              "t13respuesta": "<p>autosuficiente<\/p>",
              "t17correcta": "0"
            },
            {
              "t13respuesta": "<p>tecnológico<\/p>",
              "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>¿Sabías que el nivel de <\/p>"
            },
            {
                "t11pregunta": "<p> muestra el grado de <\/p>"
            },
            {
                "t11pregunta": "<p> de un país? Este permite la producción y <\/p>"
            },
            {
                "t11pregunta": "<p> masiva de los productos fabricados y al requerir de <\/p>"
            },
            {
                "t11pregunta": "<p>, genera empleos. Esta se divide en dos grandes categorías, una se encarga de <\/p>"
            },
            {
                "t11pregunta": "<p> los recursos naturales, por ejemplo, de origen <\/p>"
            },
            {
                "t11pregunta": "<p>, y modifica el <\/p>"
            },
            {
                "t11pregunta": "<p>; la segunda categoría realiza procesos de transformación de la industria para su <\/p>"
            },
            {
                "t11pregunta": "<p> como <\/p>"
            },
            {
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "t11pregunta": "<p>, <\/p>"
            },
            {
                "t11pregunta": "<p>, calzado, autos, entre otros. Aportando <\/p>"
            },
            {
                "t11pregunta": "<p> <\/p>"
            },
            {
                "t11pregunta": "<p> y del que muchos países dependen económicamente, los cuales sirven para incentivar el desarrollo tecnológico y nuevos sectores.<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
      },
      //5
      {
        "respuestas": [{
            "t13respuesta": "Disponibilidad de mano de obra calificada, proximidad a los centros urbanos, proximidad a otras industrias y facilidad de transporte y comunicaciones.",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Disponibilidad de mano de obra, establecimiento en  zonas territoriales extensas rurales, facilidad de transporte y comunicaciones.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Facilidad de transporte y comunicaciones, disponibilidad de materias primas en el lugar, proximidad a espacios rurales que doten de zonas territoriales extensas.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "California, Bogotá, Baikal y Corea del Sur.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Houston, Sao Paulo, Ucrania y Sudáfrica.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Moscú, Buenos aires, Vancouver, Seattle y Europa Occidental.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Baja California, Chihuahua, Sonora y Nuevo León.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Tamaulipas, Baja California, Estado de México y Puebla.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Sonora, Guanajuato, Nuevo León y Estado de México.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Productos alimenticios como bebidas y tabaco, derivados del petróleo y maquinaria y equipo.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Industrias metálicas, maquinaria y equipo e industrias textiles.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Derivados del petróleo, industrias metálicas, fabricación de muebles.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>La localización de las industrias se da por los siguientes factores.",
          "De acuerdo al mapa que muestra las áreas industriales en el mundo y lo aprendido en la lección, estas son algunas de las ciudades más importantes en la industrialización.",
          "De acuerdo al mapa que muestra las principales entidades con parques industriales, selecciona las cuatro entidades con mayor número de parques en México.",
          "Selecciona las tres principales actividades industriales que se realizan en México.",
          ],
          "preguntasMultiples": true,
          "t11instruccion": "<br><br><img src='GI7E_B04_A05_01.png' style='max-width: 850px; max-height: 1000px' ><br><br><img src='GI7E_B04_A05_02.png' style='max-width: 850px; max-height: 1000px'>",
        }
      },
      //6
      {
        "respuestas": [
            {
                "t13respuesta": "<p>materiales<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>terciarias<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>comercio<\/p>",
                "t17correcta": "3,7,4"
            },
            {
                "t13respuesta": "<p>transporte <\/p>",
                "t17correcta": "4,3"
            },
            {
                "t13respuesta": "<p>servicios<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>necesidades<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>comercio<\/p>",
                "t17correcta": "7,3,4"
            },
            {
                "t13respuesta": "<p>interno<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>comercio internacional<\/p>",
                "t17correcta": "9,12"
            },
            {
                "t13respuesta": "<p>balanza comercial<\/p>",
                "t17correcta": "10"
            },
            {
              "t13respuesta": "<p>mayor<\/p>",
              "t17correcta": "11"
            },
            {
              "t13respuesta": "<p>comercio internacional<\/p>",
              "t17correcta": "12,9"
            },
            {
              "t13respuesta": "<p>bloques económicos<\/p>",
              "t17correcta": "13"
            },
            {
              "t13respuesta": "<p>tasas preferenciales<\/p>",
              "t17correcta": "14"
            },
            {
              "t13respuesta": "<p>menor<\/p>",
              "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>¿Sabías que las actividades económicas que no producen bienes <\/p>"
            },
            {
                "t11pregunta": "<p>, sino que reciben los productos elaborados en la industria para su venta, se llaman <\/p>"
            },
            {
                "t11pregunta": "<p>? Estas incluyen el <\/p>"
            },
            {
                "t11pregunta": "<p>, el <\/p>"
            },
            {
                "t11pregunta": "<p> y los <\/p>"
            },
            {
                "t11pregunta": "<p>, también tienen como objetivo satisfacer las <\/p>"
            },
            {
                "t11pregunta": "<p> de la población. Precisamente gracias al <\/p>"
            },
            {
                "t11pregunta": "<p> podemos obtener productos de cualquier parte del mundo. Existen dos tipos, el que ocurre dentro del mismo país conocido como <\/p>"
            },
            {
                "t11pregunta": "<p> y el <\/p>"
            },
            {
                "t11pregunta": "<p>. La diferencia entre estas dos actividades se conoce como <\/p>"
            },
            {
                "t11pregunta": "<p>, la cual es positiva cuando lo que un país vende, es <\/p>"
            },
            {
                "t11pregunta": "<p> que lo que compra.<br>Para incrementar y agilizar el <\/p>"
            },
            {
                "t11pregunta": "<p>, los países han formado <\/p>"
            },
            {
                "t11pregunta": "<p> que hacen uso de estrategias como la eliminación de los impuestos y <\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
      },
      //7
      {
        "respuestas": [
            {
                "t13respuesta": "Banco Mundial (FMI).",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Empresas transnacionales.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Organismos internacionales.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Organización Mundial del Comercio (OMC).",
                "t17correcta": "4"
            },
            {
              "t13respuesta": "Ciudades alfa.",
              "t17correcta": "5"
            },
            {
                "t13respuesta": "Sistema financiero.",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Brinda asistencia financiera a los países con agudas crisis económicas..."
            },
            {
                "t11pregunta": "Son aquellas que operan fuera de sus países de origen, utilizando las mismas estrategias y etapas de sus procesos industriales y comerciales..."
            },
            {
                "t11pregunta": "Tienen como propósito coordinar la economía y lograr un desarrollo económico y humano equitativo entre todos los países..."
            },
            {
                "t11pregunta": "Tiene como función regular las normas  de comercio entre países..."
            },
            {
                "t11pregunta": "Son sedes de empresas transnacionales y de las actividades financieras más importantes del mundo..."
            },
            {
                "t11pregunta": "Regula el flujo de dinero derivado de las actividades económicas a nivel global, administra las inversiones, efectúa operaciones financieras, etc..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
      },
      //8
      {
        "respuestas": [{
            "t13respuesta": "Turismo rural.",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Turismo en espacios naturales.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Turismo litoral.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Turismo urbano.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Turismo nacional.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Turismo citadino.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "La muralla China, Madrid, París, Nueva York.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "París, El Coliseo de Roma, Venecia, Puerta de Brandemburgo en Berlín.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Londres, París, Nueva York, La catedral de san Basilio.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Cancún, Ciudad de México, Los Cabos y Acapulco.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Ciudad de méxico, Acapulco, Monterrey y Cancún.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Ciudad de México, Cancún, Querétaro y Monterrey.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Turismo en espacios naturales.",
            "t17correcta": "1",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Turismo rural.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Turismo litoral.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Turismo litoral.",
            "t17correcta": "1",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Turismo en paisajes naturales.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Turismo acuático.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Crecimiento de las ciudades, creación de vías de comunicación, habilitación de infraestructura de servicios básicos.",
            "t17correcta": "1",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Reducción de las ciudades, creación de vías de comunicación.",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
          {
            "t13respuesta": "Creación de vías de comunicación marítimas, habilitación de infraestructura de servicios básicos, reducción de los ingresos.",
            "t17correcta": "0",
            "numeroPregunta": "6"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Promueve la conservación del ambiente, se pueden realizar visitas a granjas, caballerías y campos.",
          "Se realizan actividades como recorridos gastronómicos, visitas a museos, ferias, entre otros. ",
          "Son los  cuatro lugares más visitados turísticamente a nivel mundial.",
          "Son los cuatro lugares más visitados turísticamente dentro de México.",
          "Se realizan actividades deportivas y se disfruta del atractivo del paisaje natural.",
          "Estimula el comercio y los servicios de hoteles, vuelos, entre otros. Se realizan actividades de playa.",
          "¿Qué aspectos promueve el turismo?",
          ],
      
          "preguntasMultiples": true,
          "t11instruccion": "<br><br><img src='GI7E_B04_A08.png' style='max-width: 850px; max-height: 1000px'>"
        }
      },
      //9
      {
        "respuestas": [{
            "t13respuesta": "Falso",
            "t17correcta": "0"
          },
          {
            "t13respuesta": "Verdadero",
            "t17correcta": "1"
          }
        ],
        "preguntas": [{
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "México es un país con una carente diversidad natural y cultural, lo cual genera poca atracción turística internacional.",
            "correcta": "0"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El turismo es una fuente de ingresos, empleos y de inversión en infraestructura.",
            "correcta": "1"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En México, el sector turístico es una fuente de divisas secundaria.",
            "correcta": "0"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Algunas de las amenazas del turismo en el país son fenómenos naturales, inestabilidad económica e inseguridad.",
            "correcta": "0"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El turismo es un recurso económico y un recurso social vinculado a la calidad de vida.",
            "correcta": "1"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
          "descripcion": "Reactivo",
          "variante": "editable",
          "anchoColumnaPreguntas": 30,
          "evaluable": true
        }
      },
      //10
      {
        "respuestas": [{
            "t13respuesta": "Salud, educación e ingreso.",
            "t17correcta": "1",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Vivienda, empleo, educación, salud y medio ambiente.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Empleo, educación, satisfacción.",
            "t17correcta": "0",
            "numeroPregunta": "0"
          },
          {
            "t13respuesta": "Para evaluar y analizar a los países con el propósito de asistirlos y apoyarlos con políticas asistenciales.",
            "t17correcta": "1",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Para evaluar el grado de desarrollo económico con el propósito de identificar la distribución de los fondos monetarios.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Para analizar la distribución económica y sus posibles alianzas para la firma de tratados.",
            "t17correcta": "0",
            "numeroPregunta": "1"
          },
          {
            "t13respuesta": "Peor: Guerrero, Chiapas, Oaxaca. Mejor: Nuevo León, Sonora, Ciudad de México.",
            "t17correcta": "1",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Peor: Oaxaca, Puebla, Chiapas.  Mejor: Guanajuato, Guadalajara, Ciudad de México.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Mejor: Ciudad de México, Saltillo, Sinaloa. Peor: Veracruz, Querétaro, Chiapas.",
            "t17correcta": "0",
            "numeroPregunta": "2"
          },
          {
            "t13respuesta": "Países centrales.",
            "t17correcta": "1",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Países periféricos.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Países desarrollados.",
            "t17correcta": "0",
            "numeroPregunta": "3"
          },
          {
            "t13respuesta": "Países semiperiféricos.",
            "t17correcta": "1",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Países periféricos.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Países con una tasa media de calidad de vida.",
            "t17correcta": "0",
            "numeroPregunta": "4"
          },
          {
            "t13respuesta": "Periférico.",
            "t17correcta": "1",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Central.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
          {
            "t13respuesta": "Semiperiférico.",
            "t17correcta": "0",
            "numeroPregunta": "5"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>El Índice de Desarrollo Humano (PNUD) está integrado por las siguientes dimensiones:",
          "¿Qué utilidad tiene el Índice de Desarrollo Humano?",
          "Según el Índice de Desarrollo Humano, ¿qué estados tienen la mejor y la peor calidad de vida en México?",
          "Se caracterizan por exportar tecnología, inversiones de capital, ofertas de empleo y salarios altos, además de gozar de una calidad de vida alta.",
          "Se caracterizan por tener un alto desarrollo industrial y tecnológico, son importadores  de productos de los países centrales.",
          "México es un país categorizado como:",
          ],
      
          "preguntasMultiples": true
        }
      },
      //11
      {
        "respuestas": [{
            "t13respuesta": "Falso",
            "t17correcta": "0"
          },
          {
            "t13respuesta": "Verdadero",
            "t17correcta": "1"
          }
        ],
        "preguntas": [{
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Existe una relación entre la concentración de poblados indígenas en los estados de México y una baja calidad de vida en México.",
            "correcta": "1"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los conflictos bélicos, las epidemias y desastres naturales son factores que afectan el nivel de calidad de vida de los países.",
            "correcta": "1"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Ser un país catalogado como central garantiza un alto grado de desarrollo humano y calidad de vida.",
            "correcta": "0"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En México, los recursos económicos, naturales y sociales están distribuidos equitativamente.",
            "correcta": "0"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Rusia, Australia y Canadá son países centrales.",
            "correcta": "1"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "México, China y Perú son países periféricos. ",
            "correcta": "0"
          },
          {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La denominación de países centrales, periféricos y semiperiféricos sirve para identificar diferencias entre las dimensiones de salud, ingresos y bienestar social.",
            "correcta": "0"
          },
        ],
        "pregunta": {
          "c03id_tipo_pregunta": "13",
          "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
          "t11instruccion": "<img src='GI7E_B04_A11.png' style='max-width: 850px; max-height: 1000px'> ",
          "descripcion": "Reactivo",
          "variante": "editable",
          "anchoColumnaPreguntas": 50,
          "evaluable": true
        }
      },


];
