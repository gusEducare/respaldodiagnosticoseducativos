json=[
  //1
    {
        "respuestas": [
            {
                "t13respuesta": "Norteamericana, Sudamericana, Africana y Euroasiática",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Movimientos deslizantes",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Desplazamiento de continentes, volcanes y terremotos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Capas tectónicas",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Movimientos convergentes",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Placas tectónicas",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Principales capas tectónicas de la Tierra"
            },
            {
                "t11pregunta": "Pueden generar fallas que deriven en terremotos"
            },
            {
                "t11pregunta": "Son procesos internos que se manifiestan en la superficie de los que el núcleo es responsable"
            },
            {
                "t11pregunta": "Son grandes capas en las que está fragmentada la litosfera"
            },
            {
                "t11pregunta": "Provocan que se formen las cadenas montañosas"
            },
            {
                "t11pregunta": "Dan cuenta de la distribución de zonas sísmicas y volcánicas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //7
     {  
      "respuestas":[
         {  
            "t13respuesta": "El Cinturón de Fuego del Pacífico y Círculo Norte",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Círculo Mediterráneo y Cinturón del Pacífico",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Círculo Mediterráneo y Cinturón de Fuego del Pacífico",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Ritcher y Mercalli",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Ritcher y Mercator",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Epicentro y Ritcher",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Foco",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Superficial",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Epicentro",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Incendios forestales",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Terremotos",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tsunamis",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Corteza",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Manto",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Mesosfera",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa el mapa presionando \"+\", analiza y selecciona la respuesta correcta:<br><br>Son zonas con alta sismicidad: ",
                         "Los sismos pueden medirse en las siguientes escalas:",
                         "Si un sismo se origina  sobre la superficie terrestre se denomina:",
                         "Los movimientos de las placas tectónicas acumulan energía que se libera en:",
                         "Es la capa tectónica donde se manifiestan los procesos internos y externos como la erosión:"],
         "preguntasMultiples": true,
         "t11instruccion":"<center/><img src='GI7E_B02_A02.png'/>"
      }
   },
   //3
    {  
      "respuestas":[
         {  
            "t13respuesta": "Montaña",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Volcán",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Ladera",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Meseta Norte y Meseta Sur",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Meseta Central y Meseta Norte",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Meseta Central y Meseta Sur",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Cordillera",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Llanura",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Sierra",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Sistema volcánico convergente",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Relieves montañosos",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Sistema Volcánico Transversal",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Montañas",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Mesetas",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Cordilleras",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Colina",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Mesetas",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Planicies",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Depresión",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Valle",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Cerro",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Cadenas o cinturones de montañas",
            "t17correcta": "1",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Montes y cinturones",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Sistema montañoso",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Erosión eólica, erosión pluvial, erosión kárstica, erosión incendial",
            "t17correcta": "",
            "numeroPregunta":"8"
         },
         {  
            "t13respuesta": "Erosión eólica, erosión pluvial, erosión kárstica, erosión marina",
            "t17correcta": "1",
            "numeroPregunta":"8"
         },
         {  
            "t13respuesta": "Erosión eólica, erosión pluvial, erosión kárstica, erosión enérgica",
            "t17correcta": "0",
            "numeroPregunta":"8"
         },
         {  
            "t13respuesta": "Meseta",
            "t17correcta": "0",
            "numeroPregunta":"9"
         },
         {  
            "t13respuesta": "Cerro",
            "t17correcta": "0",
            "numeroPregunta":"9"
         },
         {  
            "t13respuesta": "Llanura",
            "t17correcta": "1",
            "numeroPregunta":"9"
         },
         {  
            "t13respuesta": "Llanuras, mesetas y cadenas montañosas",
            "t17correcta": "1",
            "numeroPregunta":"10"
         },
         {  
            "t13respuesta": "Montañas, Llanuras, hidrografía",
            "t17correcta": "0",
            "numeroPregunta":"10"
         },
         {  
            "t13respuesta": "Llanuras, Hidrografía, cadenas  montañosas",
            "t17correcta": "0",
            "numeroPregunta":"10"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa el mapa presionando \"+\", analiza y selecciona la respuesta correcta:<br><br>El número 1 de la imagen refiere a:",
                         "Las mesetas mexicanas están divididas en:",
                         "El número 8 de la imagen nos muestra:",
                         "Su formación se debe a la erupción de volcanes activos cuya magma rellena las cuencas, depresiones y valles de las zonas circundantes:",
                         "Son elevaciones que sobresalen de la superficie terrestre, favorecen la presencia de yacimientos minerales:",
                         "Si observamos la imagen, el número 4 nos muestra:",
                         "Son elevaciones que no sobrepasan los 100 m de altura:",
                         "La distribución del relieve en el mundo se presenta mayormente en:",
                         "Podemos distinguir varios tipos de erosión según los agentes que la provocan, ¿cuáles son?:",
                         "El número tres de la imagen nos muestra: ",
                         "Debido a que México se encuentra ubicado sobre varias capas tectónicas, su actividad geológica ocasiona:"],
         "preguntasMultiples": true,
         "t11instruccion":"<center/><img src='GI7E_B02_A03.png' style='width:900px;'/>"
      }
   },
   //4
    {
        "respuestas": [
            {
                "t13respuesta": "Sistema montañoso Mediterráneo-Himalaya",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Relieve Oceánico",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Dorsales",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Fosas submarinas",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Llanura abisal",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "En este sistema se localizan los Pirineos, Alpes, Cárpatos, Hinda Kush"
            },
            {
                "t11pregunta": "Es producto de los mismos procesos internos y externos del relieve continental"
            },
            {
                "t11pregunta": "Corresponden a cordilleras volcánicas submarinas; pueden alcanzar una altura de 3,000 metros"
            },
            {
                "t11pregunta": "Son las mayores depresiones de la corteza terrestre"
            },
            {
                "t11pregunta": "Son zonas de pendiente muy suave del fondo de la cuenca oceánica profunda."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Pacífico<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Atlántico<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Índico<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Ártico<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>petróleo<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>salinidad<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>dimensión<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>golfos<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>presión<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>corrientes<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>frías<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>cálidas<\/p>",
                "t17correcta": "12"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br><br>Existen diferentes océanos en nuestro planeta: el<\/p>"
            },
            {
                "t11pregunta": "<p> es el más grande en la Tierra; le siguen el <\/p>"
            },
            {
                "t11pregunta": "<p> e <\/p>"
            },
            {
                "t11pregunta": "<p>; por último está el más pequeño y frío, nombrado océano <\/p>"
            },
            {
                "t11pregunta": "<p>. En ellos se encuentran yacimientos de <\/p>"
            },
            {
                "t11pregunta": ".<br>En cuanto a su composición, las aguas oceánicas se caracterizan por su <\/p>"
            },
            {
                "t11pregunta": "<p> donde existen cuerpos de agua de menor <\/p>"
            },
            {
                "t11pregunta": "<p> como los mares, <\/p>"
            },
            {
                "t11pregunta": "<p> y bahías.<br>Las aguas oceánicas están  en constante movimiento, donde intervienen la <\/p>"
            },
            {
                "t11pregunta": "<p>y la temperatura del mar. Entre estos movimientos  están las <\/p>"
            },
            {
                "t11pregunta": "<p> marinas, que a su vez se dividen en <\/p>"
            },
            {
                "t11pregunta": "<p> (que generan aridez) y <\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el siguiente texto:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Olas",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Mareas",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Aguas continentales",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Ríos, lagos, lagunas y depósitos subterráneos como grutas y cenotes",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Lagos, lagunas y depósitos subterráneos",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Vertiente del Atlántico, vertiente del Pacífico y vertiente interior",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Se producen por la acción del viento"
            },
            {
                "t11pregunta": "Son el ascenso y descenso periódico del nivel del mar, sus movimientos se originan por la fuerza de atracción gravitacional de la Luna y el Sol sobre la Tierra"
            },
            {
                "t11pregunta": "Se caracterizan por presentar una baja salinidad, lo que la hace consumible para el ser humano, la fauna y la vegetación"
            },
            {
                "t11pregunta": "Son ejemplos de distribución de las aguas continentales"
            },
            {
                "t11pregunta": "En México, ubicado en zona tropical, se favorece la existencia de estos depósitos de agua continental"
            },
            {
                "t11pregunta": "Los cuerpos de agua continental en nuestro país se distribuyen en estas zonas"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //7
     {  
      "respuestas":[
         {  
            "t13respuesta": "Ofrecen un hábitat adecuado para la flora y la fauna de la zona",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Regular los componentes bioquímicos del subsuelo",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Fuente de sustento y desarrollo social, cultural y económico",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Enriquecen y riegan los suelos",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Zona de resolución",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Zona de cabecera",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Zona de captación",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Zona intermedia",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Grandes cuencas",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Macrocuencas",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Cuencas intermedias",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Microcuencas",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Chiapas",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Veracruz",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Oaxaca",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tabasco",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Cuenca del Amazonas",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Cuenca de la Plata",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Cuenca de América",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Cuenca Caspio",
            "t17correcta": "1",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las respuestas correctas para cada pregunta:<br><br>La captación del agua de las cuencas hídricas tiene varias funciones, entre ellas:",
                         "Las cuencas se dividen en las siguientes zonas:",
                         "Según su tamaño, las cuencas se clasifican:",
                         "En las siguientes regiones de México se encuentran las cuencas con los ríos más caudalosos:",
                         "Principales cuencas en el mundo:"],
         "preguntasMultiples": true,
      }
   },
   //8
   {
        "respuestas": [
            {
                "t13respuesta": "Regiones cercanas al ecuador en donde el clima es cálido y lluvioso",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Regiones de climas secos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cuenca",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Desembocadura",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...minería, demanda de energía eléctrica, crecimiento demográfico",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Las cuencas más grandes se localizan en estas zonas"
            },
            {
                "t11pregunta": "Son las zonas donde están los caudales de las cuencas que disminuyen y aprovechan el agua subterránea"
            },
            {
                "t11pregunta": "Es la porción de la superficie terrestre donde los escurrimientos de agua confluyen en un punto de acumulación como un río principal, lago o mar"
            },
            {
                "t11pregunta": "Es el elemento principal en la clasificación de las cuencas en México"
            },
            {
                "t11pregunta": "Muchas cuencas son sobreexplotadas debido a la..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Precipitación, presión atmosférica, viento, humedad y temperatura",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Latitud, altitud, relieve, continentalidad y corrientes marinas",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Grupos climáticos",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "El clima",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Corrientes Marinas",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Elementos principales del clima"
            },
            {
                "t11pregunta": "Factores que intervienen en el clima"
            },
            {
                "t11pregunta": "Están determinados por su temperatura promedio. Se identifican con las primeras cinco letras del abecedario en mayúsculas"
            },
            {
                "t11pregunta": "Promueve el desarrollo de ciertos tipos de flora y fauna según la región. Incide en las actividades humanas"
            },
            {
                "t11pregunta": "Transportan calor y humedad; también pueden impedir la evaporación y por lo tanto la precipitación en muchos lugares"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //10.1
    {  
      "respuestas":[
         {  
            "t13respuesta": "La mesosfera",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La atmósfera",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "El Círculo polar ártico",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Escala Celsius",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Radiación",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Temperatura",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Viento",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Presión atmosférica.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Humedad",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Temperatura",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Humedad",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Viento",
            "t17correcta": "1",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br><br>Regula la cantidad de radiación solar que llega a nuestro planeta y en este ocurren los fenómenos meteorológicos que determinan los climas del mundo:",
                         "Se refiere a la medida de calor que tiene el aire y que es causada por la incidencia de los rayos del Sol:",
                         "Se mide con un barómetro y tiene varias escalas de medida:",
                         "Es el aire en movimiento y está determinado por la presión atmosférica, se mide con un anemómetro:"],
         "preguntasMultiples": true,
      }
   },
   //10.2
   {  
      "respuestas":[
         {  
            "t13respuesta": "Latitud",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Temperatura",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Relieve",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Distancia",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Ubicación",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Altitud",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Climas helados",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Climas fríos",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Clima templado",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br><br>La altitud hace que la temperatura varíe. A mayor altitud menor:",
                         "Si una localidad se encuentra cerca del mar, su humedad será más alta de la de un lugar alejado del mar, que será más seco y menos húmedo. Esto se debe a:",
                         "Está presente en las zonas de cumbres altas de las montañas mexicanas:"],
         "preguntasMultiples": true,
      }
   },
   //11
   {  
      "respuestas":[
         {  
            "t13respuesta": "Zonas climáticas",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Biomas",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Pradera",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Bosque o selva tropical",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Bosque templado",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Región templada",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Región seca",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Taiga",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Polar",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Fría",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Helada",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Zonas climáticas",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Cambio climático",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Regiones naturales",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Atmosfera, troposfera, hidrosfera",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "La posición del sol y el grado de inclinación lunar",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Atmósfera, la hidrosfera y la litosfera",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "México, Colombia, Brasil",
            "t17correcta": "1",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "México, Chile, India",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Brasil, Perú, Islandia",
            "t17correcta": "0",
            "numeroPregunta":"7"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa las imagenes presionando \"+\", analiza y selecciona la respuesta correcta:<br><br> Las áreas que comparten similitudes climáticas constantes se les denomina regiones naturales o:",
                         "Es la región natural con mayor diversidad en el planeta, se caracteriza por su humedad y su cercanía al ecuador:",
                         "La imagen 4 nos muestra la zona climática llamada:",
                         "Es una región natural propicia para el desarrollo de la ganadería y la agricultura:",
                         "La imagen 2 nos muestra una zona climática:",
                         "Surgen a raíz de la combinación de diversos factores como la ubicación, hidrografía, clima, vegetación, relieve, entre otros:",
                         "La vida en la Tierra es posible gracias a distintos factores, entre ellos la interacción de:",
                         "Debido a las distintas regiones naturales, existe una diversidad biológica en el planeta. Entre los países megadiversos están:"],
         "preguntasMultiples": true,
         "t11instruccion":"<center/><img src='GI7E_B02_A11.png' style='width:900px;'/>"
      }
   },
];
