json=[
  //1
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término composición de la población se refiere a las características sociales de un determinado espacio geográfico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La composición de la población está relacionada con el número de habitantes, edad, sexo, actividades económicas, las creencias y la lengua.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las causas del crecimiento de la población son 4: natalidad, mortalidad, movimiento migratorio  e índice de desarrollo humano.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la tasa de natalidad no inciden factores como la edad, el estado civil, el nivel educativo, etc.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La manera de medir la tasa de mortalidad es contar las muertes que ocurren en una población por cada mil habitantes en un periodo determinado.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La densidad poblacional se refiere a la cantidad de personas que viven en un kilómetro cuadrado y equivale nacionalmente a la población relativa.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><strong>Elige  falso (F) o verdadero (V) según corresponda.</strong><\/p> ",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },
    //2
    {
     "respuestas":[
        {
           "t13respuesta":"La alta concentración de población.",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"El tamaño  de los municipios del norte, por ejemplo, en Chihuahua cabe España.",
           "t17correcta":"0",
            "numeroPregunta":"0"
        },
        {
           "t13respuesta":"Los habitantes del mundo.",
           "t17correcta":"0",
            "numeroPregunta":"0"
        },
        {
           "t13respuesta":"La concentración mínima por kilómetro cuadrado.",
           "t17correcta":"0",
            "numeroPregunta":"0"
        },
        {
           "t13respuesta":"Urbano",
           "t17correcta":"1",
            "numeroPregunta":"1"
        },
        {
           "t13respuesta":"De primer mundo",
           "t17correcta":"0",
            "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Rural",
           "t17correcta":"0",
            "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Marginal",
           "t17correcta":"0",
            "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Rural",
           "t17correcta":"1",
            "numeroPregunta":"2"
        },
        {
           "t13respuesta":"De primer mundo",
           "t17correcta":"0",
            "numeroPregunta":"2"
        },
        {
           "t13respuesta":"Urbano",
           "t17correcta":"0",
            "numeroPregunta":"2"
        },
        {
           "t13respuesta":"Marginal",
           "t17correcta":"0",
            "numeroPregunta":"2"
        },
        {
           "t13respuesta":"Más de un 50% de la población.",
           "t17correcta":"1",
            "numeroPregunta":"3"
        },
        {
           "t13respuesta":"Un 50% de la población.",
           "t17correcta":"0",
            "numeroPregunta":"3"
        },
        {
           "t13respuesta":"Cerca de un 50% de la población.",
           "t17correcta":"0",
            "numeroPregunta":"3"
        },
        {
           "t13respuesta":"Al menos un 20% de la población.",
           "t17correcta":"0",
            "numeroPregunta":"3"
        },

        {
           "t13respuesta":"Un proceso de urbanización.",
           "t17correcta":"1",
            "numeroPregunta":"4"
        },
        {
           "t13respuesta":"Un proceso migratorio de la ciudad  al campo.",
           "t17correcta":"0",
            "numeroPregunta":"4"
        },
        {
           "t13respuesta":"La venta de latifundios y la subasta de servicios.",
           "t17correcta":"0",
            "numeroPregunta":"4"
        },
        {
           "t13respuesta":"Un proceso de recesión económica.",
           "t17correcta":"0",
            "numeroPregunta":"4"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11pregunta":["<strong>Selecciona la respuesta correcta.</strong><br></br>Una de las características que conforman los espacios urbanos es:",
                        "Una comunidad que cuenta con servicios básicos, médicos, servicios educativos y una eficiente infraestructura se define como un medio:",
                        "La agricultura, ganadería y la silvicultura son actividades de un medio:",
                        "Ante las estimaciones mencionadas en tu libro, ¿qué cantidad de la población lleva una vida urbana?",
                        "Acondicionar un espacio para una gran concentración de población y ofrecer gran cantidad de bienes y servicios implica:"],
        "preguntasMultiples": true
     }
  },
  //3
  {
       "respuestas": [
           {
               "t13respuesta": "la incapacidad de cubrir necesidades básicas como alimentación, salud, educación y  vivienda.",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "una situación de discriminación y represión contra cierto sector de la sociedad.",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "Cuando el ingreso de las familias es menor a 2 dólares o cuando las personas sobreviven con menos de 1.25 dólares al día.",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "795 millones de personas en el mundo no gozan con los alimentos necesarios.",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "menos del 20% de la población en situación de pobreza.",
               "t17correcta": "5"
           }
       ],
       "preguntas": [
           {
               "t11pregunta": "La pobreza se refiere a:"
           },
           {
               "t11pregunta": "La marginación es:"
           },
           {
               "t11pregunta": "Según la ONU, ¿cuáles son los dos indicadores que miden la pobreza? "
           },
           {
               "t11pregunta": "De acuerdo con la información de la ONU:"
           },
           {
               "t11pregunta": "México tiene: "
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "12",
           "t11pregunta":"<strong>Relaciona las columnas según corresponda.</strong>"
       }
   },
   //4
   {
        "respuestas": [
            {
                "t13respuesta": "de personas de un lugar a otro en aras de una mejor calidad de vida.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "mientras que en el proceso de inmigración una persona llega a un nuevo lugar.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "cuando el movimiento o desplazamiento se hace de un estado a otro dentro de un mismo país.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "puede ser legal o ilegal, según se  cuente o no  con los requisitos migratorios solicitados.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La migración es un desplazamiento..."
            },
            {
                "t11pregunta": "Durante el proceso de emigración, una persona abandona su lugar de origen..."
            },
            {
                "t11pregunta": "Se denomina migración nacional... "
            },
            {
                "t11pregunta": "El proceso de migración internacional"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"<strong>Relaciona las columnas según corresponda.</strong>"
        }
    },
    //5
    {
      "respuestas":[
         {
            "t13respuesta":     "Asia",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "América del Norte",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "Australia",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "África",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },

         {
            "t13respuesta":     "Turquía",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Marruecos",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Alemania",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "Brazil",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "De Sudamérica hacia Norteamérica.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "De África central y Sudáfrica hacia el viejo continente.",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "De Australia hacia África.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "De América para Asia.",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":["<strong>Selecciona todas las respuestas correctas para cada pregunta.</strong><br></br>¿Cuáles son los continentes con mayor recepción de inmigrantes?",
                        "Durante la década de 1950 la alta demanda de mano de obra en Europa Central provocó el flujo migratorio, ¿de qué países provenían?",
                        "Los principales flujos o rutas migratorias en el mundo son:"],
         "preguntasMultiples": true
      }
   },
  //6
  {
       "respuestas": [
           {
               "t13respuesta": "Cultura tradicional",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Cultura contemporánea",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "Cultura emergente",
               "t17correcta": "2"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Se les suele nombrar cosmopolitas a sociedades como las de Nueva York, Tokio, Amsterdam y Berlín.",
               "valores": ['', '', ''],
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Se observa en comunidades que comparten un antecedente étnico, religioso o lengua.",
               "valores": ['', '', ''],
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Los chicanos, menonitas, gitanos y hasta darketos son ejemplos de:",
               "valores": ['', '', ''],
               "correcta"  : "2"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Las personas tienen medios para cambiar su modo de vida.",
               "valores": ['', '', ''],
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Son resultado de migraciones, desplazamientos y el desarrollo de nuevas tecnologías.",
               "valores": ['', '', ''],
               "correcta"  : "2"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La guelaguetza de Oaxaca, el festival de Cheung Chau, en Malasia y  el día del Pisco Sour en Perú.",
               "valores": ['', '', ''],
               "correcta"  : "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<p><strong>Observa la siguiente matriz y marca la opción que corresponda a cada tipo de cultura.</strong><\/p>",
           "descripcion": "Título                                        preguntas",
           "variante": "editable",
           "anchoColumnaPreguntas": 30,
           "evaluable"  : true
       }
   },
   //7
   {
       "respuestas": [
           {
               "t13respuesta": "Falso",
               "t17correcta": "0"
           },
           {
               "t13respuesta": "Verdadero",
               "t17correcta": "1"
           }
       ],
       "preguntas" : [
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "El acceso a internet nos permite tener información en el momento en que  ocurren algunos hechos, así como conocer más sobre otras culturas.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "Los medios de comunicación no impactan la cultura ni a la sociedad.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La cultura global surge cuando las  sociedades adoptan rasgos de otras, y en algunos casos, al desechar los que les caracterizaban.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La cultura dominante se impone por medio de la moda, la comida y en colectivos que son ajenos a esos elementos.",
               "correcta"  : "1"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La cultura residual mantiene sus tradiciones, pero las mezcla e interactúa con otras.",
               "correcta"  : "0"
           },
           {
               "c03id_tipo_pregunta": "13",
               "t11pregunta": "La cultura híbrida conserva su tradición de generación en generación.",
               "correcta"  : "0"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "13",
           "t11pregunta": "<p><strong>Elige  falso (F) o verdadero (V) según corresponda.</strong><\/p>",
           "descripcion":"Reactivo",
           "evaluable":false,
           "evidencio": false
       }
   }
];
