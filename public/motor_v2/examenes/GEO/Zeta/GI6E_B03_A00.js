json = [
   //1
      
    {  
      "respuestas":[
         {  
            "t13respuesta": "El más poblado es Asia y el menos poblado es Oceanía.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "El más poblado es América y el menos poblado es África.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "El más poblado es Asia y el menos poblado es África.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Sedentarismo, desarrollo de la agricultura, disminución de la mortalidad y aumento progresivo de la esperanza de vida.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Desarrollo de la agricultura, aumento de la mortalidad, nomadismo y aumento en la esperanza de vida.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Aumento progresivo de la esperanza de vida, aumento de la mortalidad, revolución verde y sedentarismo.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los datos más relevantes son edad y género.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los datos más relevantes son sexo e ingreso económico.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         } ,
         {  
            "t13respuesta": "Los datos más relevantes son género y religión.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Se creó un gráfico piramidal que agrupa a la población por edades,  en periodos de cinco años, y los separa en hombres y mujeres.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Se creó un gráfico piramidal que clasifica a la población en grupos de edad que abarcan periodos de cinco años y los separa por ingresos económicos.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         } ,
         {  
            "t13respuesta": "Se creó un gráfico pastel que clasifica a la población en grupos de edad que abarcan periodos de cinco años y los separa en hombres y mujeres.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Detener la contaminación ambiental, generar mecanismos para el cuidado del agua, reducir la tasa de natalidad.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Aumentar la tasa de natalidad, proteger los recursos naturales, generar tratamiento del agua y descargar los residuos industriales en cuerpos de agua.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         } ,
         {  
            "t13respuesta": "Generar mecanismos para el cuidado del agua, aumentar los permisos para descargar los residuos industriales en zonas urbanas, avanzar en materia de producción de alimentos y educación.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Distribución poblacional",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Distribución piramidal",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Distribución de natalidad",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }, 
         {  
            "t13respuesta": "Diferentes condiciones geográficas y medioambientales que facilitan el desarrollo de la vida y las actividades económicas.",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Al desarrollo agrícola que facilita el desarrollo de la vida.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         }, 
         {  
            "t13respuesta": "Las condiciones económicas que facilitan el desarrollo de la agricultura y la ganadería. ",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<div  style='text-align:center;'><img style='height: 360px' src='GI6E_B03_A01.png'><br>  </div> <strong>Selecciona la respuesta correcta</strong> <br>De acuerdo con el mapa, ¿cúal es el continente más poblado y cuál tiene menor población?","La historia de la humanidad ha tenido distintos incrementos en la población debido a los siguientes acontecimientos:","¿Qué datos se necesitan para conocer la composición poblacional?","¿Qué sistema se creó para visualizar de manera rápida y exacta a la población y cómo funciona?","¿Qué retos y problemáticas plantea el crecimiento de la población a nivel mundial?","Se refiere a los lugares donde se encuentra repartida la población del mundo o de algún país:","¿Qué factores influyen en que la distribución de la población se concentre más en algunas zonas que en otras?"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },
   
   //2
   {
        //1
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La contaminación y destrucción del medio ambiente tiene como una de sus causas el crecimiento de las ciudades.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El crecimiento desmedido de las ciudades ocasiona daños en nuestra salud y modo de vida debido a la contaminación del aire, suelo y agua.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El desarrollo de las ciudades implica crecimiento económico y combate a la pobreza debido al aumento de empleos con alta remuneración.",
                "correcta"  : "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La ciudad se caracteriza por tener una población culturalmente homogénea.",
                "correcta"  : "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El crecimiento de las ciudades es una causa de la disminución de la fuerza laboral que hay en las zonas rurales.",
                "correcta"  : "1"
            } ,
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><strong>Selecciona verdadero (V) o falso (F) según corresponda:</strong><\/p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        },
    },
    
    //3
     {
        "respuestas": [
            {
                "t13respuesta": " Rusia, Arabia Saudita, Alemania, Estados Unidos y Canadá.",
                
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Turquía, Filipinas, Reino Unido, India y Rusia.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Christian nació en Querétaro, luego vivió en Guanajuato y estudia en Veracruz.",
                
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Finlandia y su estrategia de integración multicultural de los migrantes.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Debido a la migración, en algunas comunidades solo quedan mujeres, niños y adultos mayores para el trabajo.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "En la composición y distribución de la población.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Son los países con mayor atracción del fenómeno migratorio en el mundo."
            },
            {
                "t11pregunta": "Son los países con mayor índice de emigración."
            },
            {
                "t11pregunta": "Es un ejemplo de migración nacional."
            },
            {
                "t11pregunta": "Es una consecuencia política de la emigración."
            },
            {
                "t11pregunta": "Es una consecuencia social de la emigración."
            },
            {
                "t11pregunta": "¿En qué influye la migración?"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "<strong>Relaciona las columnas según corresponda</strong>"
             
            
        }
    },
        
    //4
     {
        "respuestas": [
            {
                "t13respuesta": " México ocupa el 6° lugar a nivel mundial por la cantidad de Patrimonios de la Humanidad",
                
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Hay 1031 patrimonios mundiales, de los cuales 802 son culturales, 197 naturales y 32 mixtos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Chichén Itzá, la ciudad fortificada de Campeche y Teotihuacán.",
                
                "t17correcta": "3"
            },
            {
                "t13respuesta": "La pirekua, el mariachi y los voladores de Papantla.",
                "t17correcta": "4"
            },
           
        ],
        "preguntas": [
            {
                "t11pregunta": "Según la Unesco:"
            },
            {
                "t11pregunta": "En la lista de Patrim:"
            },
            {
                "t11pregunta": "Son ejemplos de patrimonio cultural tangible:"
            },
            {
                "t11pregunta": "Son ejemplos de patrimonio cultural intangible:"
            },
            
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "<b>Relaciona las columnas según corresponda</b>"
             
            
        }
    },
   
    
];
