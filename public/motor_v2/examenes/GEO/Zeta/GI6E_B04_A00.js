json=[

    {  
      "respuestas":[
         {  
            "t13respuesta": "Geografía económica",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Geografía política",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Geografía orográfica",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Geografía hidrográfica",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
          {  
            "t13respuesta": "Son productos procesados que tienen una utilidad para nosotros, como las piñas enlatadas.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Son virtudes deseadas en todos que deben ponerse en práctica, como la honestidad. ",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Son utilidades heredadas por los padres, abuelos o familiares mayores, como un inmueble.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Son acciones realizadas que ayudan a alguien, por ejemplo, reciclar la basura.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
           {  
            "t13respuesta": "Son las atenciones y actividades que se brindan a otros seres humanos, como una consulta médica o una consulta odontológica.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Se trata de una situación de empleo de fuerza laboral en ciertos sectores sociales.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Son actividades sin remuneración que prestan atenciones especiales a quien las solicita.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
         {  
            "t13respuesta": "Se trata de actividades de corte común que cualquiera puede realizar esperando una remuneración módica.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
            {  
            "t13respuesta": "Uganda",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Estados Unidos de América",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Alemania",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }, 
         {  
            "t13respuesta": "Taiwán",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
          {  
            "t13respuesta": "La distribución y comercialización.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "La innovación e invención.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "La exportación y reconocimiento.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }, 
         {  
            "t13respuesta": "La importación y endeudamiento.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
            {  
            "t13respuesta": "Importación, cuando un producto es traido de otro país; exportación, cuando un producto es enviado a otro país.",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Introducción, cuando un producto es traido de otro país; excursión, cuando un producto es enviado a otro país.",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Inmigración, cuando un producto es traido de otro país; emigración, cuando un producto es enviado a otro país.",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }, 
         {  
            "t13respuesta": "Libre venta, cuando un producto es traido de otro país; tráfico, cuando un producto es enviado a otro país.",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
            {  
            "t13respuesta": "El transporte marítimo, ya que pueden enviarse cargas inconmensurables a cualquier parte del mundo, además de que es relativamente barato.",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "El transporte terrestre, debido a que es más fácil su acceso a otros lugares.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "El transporte ferroviario, porque es práctico y útil para transportar grandes mercancías.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         }, 
         {  
            "t13respuesta": "Ninguno, no hay producto o materia tan grande como para no ser transportada.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         }

        
      ],
      "pregunta":{  
         
         "c03id_tipo_pregunta":"1",
         
         "t11pregunta": ["<b>Selecciona la respuesta correcta.</b><br><br>Es el estudio del proceso de transformación de materia prima en bienes o mercancías, así como la forma en la que se distribuyen entre la población que los consume.",
          "¿Qué son los bienes?","¿Qué son los servicios?",
          "¿Cuál de los siguiente países no está industrializado o tiene menos desarrollo?","El proceso que sigue después de la producción y transformación de los productos es…","El intercambio comercial en el mundo se presenta de dos maneras, dependiendo del origen y destino de los productos, por ello se utilizan los términos:",
          "Cuando un producto o materia es excesivamente grande para ser transportado en avión a su destino, ¿qué transporte se utiliza y por qué?"],

         "preguntasMultiples": true,

         "columnas":1
         
      }
   },
   {
   	 "respuestas": [{
            "t13respuesta": "<p>trueques<\/p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>comercio<\/p>",
            "t17correcta": "2,3"
        },
        {
            "t13respuesta": "<p>mercado<\/p>",
            "t17correcta": "2,3"
        },
        {
            "t13respuesta": "<p>producción<\/p>",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "<p>bienes<\/p>",
            "t17correcta": "5,6"
        },
        {
            "t13respuesta": "<p>servicios<\/p>",
            "t17correcta": "5,6"
        },
        {
            "t13respuesta": "<p>agua embotellada<\/p>",
            "t17correcta": "7"
        },
        {
            "t13respuesta": "<p>tela<\/p>",
            "t17correcta": "8"
        },
        {
            "t13respuesta": "<p>confeccionar<\/p>",
            "t17correcta": "9"
        },
        {
            "t13respuesta": "<p>producto<\/p>",
            "t17correcta": "10"
        },
        {
            "t13respuesta": "<p>venta<\/p>",
            "t17correcta": "11"
        },
        {
            "t13respuesta": "<p>compra<\/p>",
            "t17correcta": "12"
        },
        {
            "t13respuesta": "<p>exportación<\/p>",
            "t17correcta": "13"
        },
        {
            "t13respuesta": "<p>importación<\/p>",
            "t17correcta": "14"
        }
    ],
    "preguntas": [{
            "t11pregunta": "<p>La civilización ha avanzado mucho, desde los primeros pobladores y sus </p>"
        },
        {
            "t11pregunta": "<p> en las estepas de los viejos continentes, hasta el comercio electrónico por medio de monedas intangibles y números impresos en pedazos de plástico. El <\/p>"
        },
        {
            "t11pregunta": "<p>, el <\/p>"
        },
        {
            "t11pregunta": "<p> y la <\/p>"
        },
        {
            "t11pregunta": "<p> de <\/p>"
        },
        {
            "t11pregunta": "<p> y <\/p>"
        },
        {
            "t11pregunta": "<p> para consumo humano han evolucionado junto con la humanidad para convertirse en bases fundamentales de nuestra sociedad. Hemos recorrido un largo camino que ha hecho posible nuestra existencia de la manera en que la experimentamos hoy. El hecho de tener <\/p>"
        },
        {
            "t11pregunta": "<p> de la que podamos disponer a placer; la ropa que compramos, sin preocuparnos por hacer la <\/p>"
        },
        {
            "t11pregunta": "<p> y <\/p>"
        },
        {
            "t11pregunta": "<p> la pieza; el tener entre nuestras manos cualquier <\/p>"
        },
        {
            "t11pregunta": "<p> que hayan pasado antes por otras manos humanas que lo fabricaron, eso nos hace, hoy, lo que somos y la sociedad en que vivimos.<\/p>"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "<b>Arrastra las palabras que completen el párrafo.</b>",
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "contieneDistractores": true
    }
},
{
    "respuestas": [{
            "t13respuesta": "...produce alimentos y materia prima que serán aprovechados por el ser humano.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "…  o un 28.5% de la población se dedica a la agricultura.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "...la ganadería, de la que también proceden derivados como leche, queso, mantequilla, huevos, cuero y lana.",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "...recursos maderables y no maderables a partir de selvas y bosques.",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "...zacates, resinas, tierra de hoja y celulosa.",
            "t17correcta": "5"
        },
        {
            "t13respuesta": "...pesqueras dependen de condiciones geográficas muy específicas.",
            "t17correcta": "6"
        },
        {
            "t13respuesta": "...corrientes marinas frías que fomentan la existencia de abundante alimento para peces.",
            "t17correcta": "7"
        }
    ],
    "preguntas": [{
            "t11pregunta": "La agricultura es de gran importancia mundial ya que..."
        },
        {
            "t11pregunta": "Aproximadamente dos mil millones de personas..."
        },
        {
            "t11pregunta": "Al igual que de la agricultura, otra actividad importante de la que depende la subsistencia humana es…"
        },
        {
            "t11pregunta": "La actividad forestal se encarga de obtener..."
        },
        {
            "t11pregunta": "De la actividad forestal no sólo se obtienen maderas, sino además.."
        },
        {
            "t11pregunta": "La pesca se realiza en aguas continentales o en el mar, ya que las zonas..."
        },
        {
            "t11pregunta": "En las zonas pesqueras se presentan..."
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "<b>Relaciona las columnas según corresponda.</b>"
    }
},
{
    "respuestas": [{
            "t13respuesta": "China",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Rusia",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
         {
            "t13respuesta": "Australia",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Uganda",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Taiwán",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        
        {
            "t13respuesta": "De la existencia de minerales en un lugar determinado.",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "De la abundancia de rocas en la tierra.",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "De la disponibilidad de tecnología necesaria para la extracción.",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "De la cantidad de dinero necesaria para la inversión.",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "De la demanda social que haya con respecto a estos productos.",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "El carbón",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "La madera",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "El gas",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "El agua",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
       
        
        {
            "t13respuesta": "Kuwait",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "México",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Arabia Saudita",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "Taiwán",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        
        {
            "t13respuesta": "Estados Unidos",
            "t17correcta": "1",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "China",
            "t17correcta": "1",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "Japón",
            "t17correcta": "1",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "México",
            "t17correcta": "0",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "España",
            "t17correcta": "0",
            "numeroPregunta": "4"
        },
        {
            "t13respuesta": "Estados Unidos",
            "t17correcta": "1",
            "numeroPregunta": "5"
        },
        {
            "t13respuesta": "Alemania",
            "t17correcta": "1",
            "numeroPregunta": "5"
        },
        {
            "t13respuesta": "México",
            "t17correcta": "0",
            "numeroPregunta": "5"
        },

        {
            "t13respuesta": "Japón",
            "t17correcta": "0",
            "numeroPregunta": "5"
        },
        {
            "t13respuesta": "India",
            "t17correcta": "0",
            "numeroPregunta": "5"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "2",
        "t11pregunta": [
            "<b>Selecciona todas las respuestas correctas para cada pregunta.</b><br><br>¿Cuáles son algunos de los países más importantes en la producción minera a nivel mundial?",
            "¿De qué depende la producción minera?",
            "¿Cuáles son los principales productos energéticos?",
            "¿Cuáles de los siguientes países tienen una producción de petróleo importante a nivel mundial?",
            "¿Cuáles de los siguientes países destacan en la producción industrial?",
            "¿Cuáles son los dos países que más importaciones hacen?"
        ],
        "preguntasMultiples": true
    }
},
{
    "respuestas": [{
            "t13respuesta": "<p>producción<\/p>",
            "t17correcta": "1,2,3"
        },
        {
            "t13respuesta": "<p>distribución<\/p>",
            "t17correcta": "1,2,3"
        },
        {
            "t13respuesta": "<p>venta<\/p>",
            "t17correcta": "1,2,3"
        },
        {
            "t13respuesta": "<p>revolución industrial<\/p>",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "<p>manera artesanal<\/p>",
            "t17correcta": "5"
        },
        {
            "t13respuesta": "<p>bienes<\/p>",
            "t17correcta": "6"
        }
    ],
    "preguntas": [{
            "t11pregunta": "<p>La </p>"
        },
        {
            "t11pregunta": "<p>, <\/p>"
        },
        {
            "t11pregunta": "<p> y <\/p>"
        },
        {
            "t11pregunta": "<p> de productos de manera especializada es una innovación muy reciente en la civilización. Antes de la <\/p>"
        },
        {
            "t11pregunta": "<p>  y el boom de las fábricas, todos los productos para consumo humano eran realizados de <\/p>"
        },
        {
            "t11pregunta": "<p>. El oficio de zapatero, de tejedor e incluso de carpintero eran los que se encargaban de producir todos los <\/p>"
        },
        {
            "t11pregunta": "<p> que las personas necesitaban. <\/p>"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "<b>Arrastra las palabras que completen el párrafo.</b>",
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "contieneDistractores": true
    }
},
{
    "respuestas": [{
            "t13respuesta": "F",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "V",
            "t17correcta": "1"
        }
    ],
    "preguntas": [{
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las necesidades básicas de la población son aquellas que todos los humanos tenemos que satisfacer para poder vivir.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Algunas de las necesidades básicas son la vivienda, el alimento y el vestido.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las necesidades básicas también incluyen la adquisición de productos complejos como computadoras, celulares, televisores o tablets.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La pirámide de Maslow es una representación de los deseos del hombre, fue diseñada por James Maslow, un físico canadiense.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La pirámide de Maslow está dividida en seis estratos: Fisiología, Seguridad, Afiliación, Reconocimiento, Autorrealización y Autoconocimiento.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En el estrato correspondiente a las necesidades fisiológicas de la pirámide de Maslow se encuentran necesidades como alimento, descanso y respiración.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En esencia, un deseo es una necesidad básica aumentada.",
            "correcta": "0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p><b>Elige falso (F) o verdadero (V) según corresponda.</b><\/p>",
        "descripcion": "",
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable": true
    }
}
,
{
    "respuestas": [{
            "t13respuesta": "Es el dinero generado por la producción de bienes y servicios de un país durante un año.",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Es el dinero generado por la producción de bienes y servicios de un país durante un mes.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Es el dinero generado por la producción de bienes y servicios de un país en un día.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Es el dinero gastado por la producción de bienes y servicios de un país en un año.",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "Es un factor utilizado para conocer las condiciones socioeconómicas de un país.",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "Es un factor utilizado para proyectar las condiciones socioeconómicas de un estado.",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "Es un factor utilizado para proyectar las condiciones salubres de un país.",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        
        {
            "t13respuesta": "Es el número de años que en promedio se espera que viva una persona, una esperanza de vida alta indica un mejor desarrollo económico y social en la población.",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "Se refiere a la cantidad de ingresos que una persona tiene, y sirve para saber a qué clase social pertenece.",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "Hablamos del número de bienes y servicios que puede contratar una persona, mientras más bienes y servicios tiene, mayor es su poder adquisitivo.",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "Se trata del poder adquisitivo de una persona y su capacidad para mantener una calidad de vida, que se mide por medio del mismo poder.",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "PIB per cápita, Empleo, Educación y Salud.",
            "t17correcta": "1",
            "numeroPregunta": "3"
        },
       
        {
            "t13respuesta": "PIB per cápita, Escolaridad, Salubridad y Desarrollo.",
            "t17correcta": "0",
            "numeroPregunta": "3"
        },
        {
            "t13respuesta": "PIB per cápita, Empleo, Educación y Tecnología.",
            "t17correcta": "0",
            "numeroPregunta": "3"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": [
            "<b>Selecciona la respuesta correcta. </b><br><br>¿Qué es el Producto Interno Bruto (PIB)?",
            "¿Para qué es utilizado el PIB per cápita?",
            "¿Qué es la esperanza de vida y cuál es su importancia?",
            "¿Cuáles son los cuatro factores que se toman en cuenta para evaluar las condiciones socioeconómicas de un país?"
        ],
        "preguntasMultiples": true
    }
}






]