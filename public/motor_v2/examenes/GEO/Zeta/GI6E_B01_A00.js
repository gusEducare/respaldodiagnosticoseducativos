
json=[
   {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003ESon espacios en donde convive lo físico y lo social, están organizados por los grupos culturales.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003ESon espacios culturales y sociales de un determinado grupo de personas. \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003ESon espacios que comprenden el territorio urbano y el área urbana.  \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003ESon territorios que no se encuentran modificados por el hombre.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>¿Qué son los espacios geográficos?"
      }
   },//2
   {
      "respuestas": [
          {
              "t13respuesta": "Característica del espacio, en el que todo espacio puede ser representado por medio de mapas.",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "Característica perceptible a la mirada humana. ",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "Es una característica que tiene relación entre los efectos propios de la naturaleza, como el cambio de relieve o el efecto de las <br>plantas en el suelo, así como también los humanos inciden en estos.",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "Ciencia que se ocupa de las interacciones que ocurren entre el hombre y el espacio, y cómo el espacio influencia las actividades de los hombres y los hombres <br>en el espacio, en una relación conjunta.",
              "t17correcta": "4"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "Cartografiable",
          },
          {
              "t11pregunta": "Visible"
          },
          {
              "t11pregunta": "Cambiante"
          },
          {
              "t11pregunta": "Geografía"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta":"Relaciona las columnas según corresponda."

      }
       },
       //3
        {
 "respuestas":[
{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
],
 "preguntas": [
  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Cracovia tiene un clima moderado, con veranos secos y calurosos e inviernos fríos y con temperaturas bajo cero. " ,
                "correcta"  : "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Belo Horizonte tiene un clima tropical muy templado, altas temperaturas y lluvias durante el verano. " ,

                "correcta"  : "1",

            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Cracovia es un importante centro comercial, por lo que está casi totalmente urbanizado y es complicado observar flora característica. " ,

                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"Cracovia tiene especies de árboles únicas en Europa. " ,

                "correcta"  : "1"
            },
             {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Belo Horizonte es una exótica y poblada ciudad con un clima muy cálido y húmedo. "  ,

                "correcta"  : "0"
            },
             {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Cracovia está ubicada al nivel del mar y tiene constantes lluvias, lo que propicia a la inundaciones."  ,

                "correcta"  : "0"
            },
             {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Entre la fauna de Yakarta se encuentran especies como el orangután, el tigre, el tapir, el elefante y una gran cantidad de aves, reptiles y anfibios. "  ,

                "correcta"  : "1"
            }
 ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige Falso o Verdadero según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true,
            "t11instruccion":"",
        }    
 },
 //4
 {
 "respuestas":[
{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
],
 "preguntas": [
  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El clima y el tipo de relieve son ejemplos de características naturales.  " ,
                "correcta"  : "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Las características sociales son aquellas que forman parte de la naturaleza y que no son producidas por el hombre. " ,
                "correcta"  : "0",

            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Las características naturales son aquellas creadas y modificadas por el hombre.  " ,
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"Las normas de convivencia y  los modelos económicos son ejemplos de características sociales.  " ,
                "correcta"  : "1"
            }
 ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true,


        }
 },
 //5
 {
        "respuestas": [
            {
                "t13respuesta": "<p>valores<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p> iguales<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>transportador<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>circunferencia<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>medidas<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>equitativas<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>regla<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>figura geométrica<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>La palabra grado, designa <\/p>"
            },
            {
                "t11pregunta": "<p> , por ejemplo en, geometría se usa para designar el valor de las partes<\/p>"
            },
            {
                "t11pregunta": "<p>  en las que se puede dividir un círculo. El instrumento que se usa para medir los grados, se llama<\/p>"
            },
            {
                "t11pregunta": "<p> . La medición en grados se puede aplicar a cualquier<\/p>"
            },
            {
                "t11pregunta": "<p> .<\/p>"
            },
            

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //6
      {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003ETrópico de Cáncer\u003C\/p\u003E",
             "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EMeridiano de Greenwich \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EEl paralelo del Ecuador\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>Línea imaginaria que divide a la Tierra en Hemisferio Norte y Hemisferio Sur. "
      }
   },
   //7
    {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003EEl Paralelo del Ecuador\u003C\/p\u003E",
             "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003ECírculo Polar Ártico \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EMeridiano de Greenwich\u003C\/p\u003E",
            "t17correcta":"1"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>Divide a la Tierra en Hemisferio Este y Hemisferio Oeste."
      }
   },
   //8
{
  "respuestas": [
  //tabla 1
  {
  "t17correcta": "4"
  },
  {
  "t17correcta": "3"
  },
  {
  "t17correcta": "1"
  },
  {
  "t17correcta": "7"
  },
  {
  "t17correcta": "2"
  },
  {
  "t17correcta": "6"
  },
   {
  "t17correcta": "5"
  },

  //tabla 2

  ],
  "preguntas": [
  {
  "t11pregunta": "<style>\n\ .table img{height: 90px !important; width: auto !important; }\n\ </style><br><table class='table' style= 'margin-top:-20px;'>\n\
  <col width='100'>\n\
  <col width='100'>\n\
  <col width='100'>\n\
  <col width='100'>\n\
  <col width='100'>\n\
                   <tr> \n\ <td> Trópico de Cáncer </td>\n\ <td> \n\ " },
 {
  "t11pregunta": "</td>\n\<td> Polo Norte</td>\n\ <td> \n\ "
  },
  {
  "t11pregunta": "</td></tr>\n\ <td> Ecuador <td>  \n\  "
  },
   {
  "t11pregunta": "</td>\n\<td>  Polo Sur</td>\n\ <td> \n\ "
  },
  {
  "t11pregunta": "</td></tr>\n\ <td> Círculo Polar Ártico <td>  \n\ "
  },
  {
  "t11pregunta": "</td>\n\ <td>Círculo Polar Antártico</td>\n\ <td> \n\ "
  },
  {
  "t11pregunta": "</td></tr>\n\ <td>Trópico de Capricornio<td> "
  },


  {
    "t11pregunta": "</td>\n\ </tr></table>"
  },




  ],
  "pregunta": {
  "t11pregunta": "Observa atentamente la imagen y escribe el número que corresponde de acuerdo a la zona climática. ",
  "c03id_tipo_pregunta": "6",
  "pintaUltimaCaja": false,
 "t11instruccion":'<img src="GI6E_B01_R08.png" >'
  }
  },
    //9
    {
 "respuestas":[
{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
],
 "preguntas": [
  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Una de las representaciones de la Tierra más antiguas, está tallada sobre arcilla y se le atribuye a los egipcios. " ,
                "correcta"  : "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Las representaciones de la Tierra pueden ser planas o en globo terráqueo." ,

                "correcta"  : "1",

            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Los paralelos son líneas horizontales  imaginarias que se trazan a partir del Paralelo del Ecuador hacia el norte y hacia el sur. " ,

                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"Se le conoce como latitud a la línea que divide a la Tierra en dos hemisferios.  " ,

                "correcta"  : "0"
            }
            ,
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"Se les conoce como meridianos a las líneas verticales imaginarias que se trazan desde el Polo Norte hasta el Polo Sur. " ,

                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"La longitud es la distancia entre un punto y el Meridiano de Greenwich. " ,

                "correcta"  : "1"
            },
              {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":" Al juntar meridianos y el paralelo del Ecuador se forma la red de coordenadas. " ,

                "correcta"  : "0"
            }



           
 ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true,
            "t11instruccion":""
        }
 },//10
  {
      "respuestas": [
          {
              "t13respuesta": "Expresa el fin que tiene el mapa, el área o zona geográfica que representa. ",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "Son los símbolos que explican el contenido del mapa, regularmente se ubican en una esquina del mapa.  ",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "Muestra la temporalidad en que se recolectaron los datos y se hizo  el mapa.  ",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "Permite hacer relaciones de distancias, determinar áreas y hacer comparaciones entre distintos objetos que son parte del mapa.",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "Son elementos que deben tener los mapas:",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "Representaciones planas en las que se reproducen superficies y expresan el espacio por medio de escalas.",
              "t17correcta": "6"
          },
          {
              "t13respuesta": "Formas en las que se puede expresar una escala:",
              "t17correcta": "7"
          }
          ,
          {
              "t13respuesta": "Tipo de escala que es una línea recta dividida en segmentos. ",
              "t17correcta": "8"
          }
      ],
      "preguntas": [

          {
              "t11pregunta": "Título",
          },
          {
              "t11pregunta": "Leyenda"
          },
          {
              "t11pregunta": "Fecha de elaboración "
          },
          {
              "t11pregunta": "Escala "
          },
          {
              "t11pregunta": "Título, leyenda, fecha de elaboración, escala"
          }
          ,
          {
              "t11pregunta": "Mapa"
          }  ,
          {
              "t11pregunta": "Escala numérica y escala gráfica."
          }  ,
          {
              "t11pregunta": "Escala gráfica"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta":"Relaciona las columnas según corresponda."

      }
       },
       //11
  {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003EMapas a gran escala. \u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003EMapas en escala pequeña. \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EMapa topográfico.  \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>Tipo de mapa que representa un territorio pequeño y es utilizado para representar mapas estatales y municipales. ",
         "t11instruccion":'<img src="GI6E_B01_R11.png" >'
      }
   },
       //12
  {
      "respuestas":[
         {
            "t13respuesta":"\u003Cp\u003EMapa en escala grande.  \u003C\/p\u003E",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"\u003Cp\u003EMapas en escala pequeña. \u003C\/p\u003E",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"\u003Cp\u003EMapa hidrológico.  \u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>Tipo de mapa que abarca un territorio extenso y representa a territorios mayores.  ",
         "t11instruccion":'<img src="GI6E_B01_R12.png" >'
      }
   },
   //13 opcion multiple
   { 
 "respuestas":[ 
 { 
 "t13respuesta":"Es un punto imaginario sobre el cual se sostiene la Tierra.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Línea imaginaria que atraviesa la Tierra y la divide en dos hemisferios, el norte y sur.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Línea imaginaria que permite que la Tierra gire en el movimiento de traslación.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Es un punto imaginario que divide al mundo en dos semicircunferencias.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Qué es el eje terrestre?", 
 "t11instruccion": "", 
 } 
 }, 
//14// falso o verdadero
 {
 "respuestas":[
{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
],
 "preguntas": [
  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El movimiento de rotación es el movimiento que la Tierra realiza sobre el eje terrestre. " ,
                "correcta"  : "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El movimiento de rotación tarda 365 días en que la Tierra de la vuelta completa. " ,

                "correcta"  : "0",

            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"El movimiento de traslación es el movimiento que la Tierra realiza alrededor de la luna.  " ,

                "correcta"  : "0",
            },
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"El movimiento de traslación se realiza en dirección opuesta a las manecillas del reloj. " ,

                "correcta"  : "1"
            }
            ,
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"El año que tiene 366 días se le conoce como año del sol." ,

                "correcta"  : "0",
            },
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"El movimiento de rotación provoca fenómenos como el día y la noche." ,

                "correcta"  : "1"
            },
              {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"El achatamiento de los polos ocurre por la fuerza centrífuga que se genera cuando la Tierra gira sobre su propio eje. " ,

                "correcta"  : "1"
            },
              {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"Los vientos causados por la rotación de la Tierra y la distancia del Sol provocan corrientes marinas." ,

                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"Las diferencias horarias son consecuencia de la rotación.  " ,

                "correcta"  : "1"
            },
              {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"Los puntos cardinales nos permiten ubicarnos.  " ,

                "correcta"  : "1"
            },
              {
                "c03id_tipo_pregunta": "13",
              "t11pregunta":"El norte es por donde sale el Sol. " ,

                "correcta"  : "0"
            }
 ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true,
            "t11instruccion":""
        }
 },
//15 relacion alineas
 { 
 "respuestas":[ 
{ 
 "t13respuesta":"Estación en la que  los días son más largos que las noches, las temperaturas son más cálidas y abunda más vegetación.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Estación con temperaturas elevadas y cálidas que en cualquiera de las otras estaciones.", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"Días frescos, lluviosos y con vientos. Los vientos comienzan a tirar las hojas y su color verde cambia a marrón o amarillo.", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"Días más cortos y noches más largas, temperaturas frías y con lluvias.", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"Ocurren  a causa de la inclinación del eje terrestre y a que la Tierra es casi esférica.", 
 "t17correcta":"5", 
 }, 
{ 
 "t13respuesta":"Las zonas térmicas o de insolación se dividen en:", 
 "t17correcta":"6", 
 }, 
{ 
 "t13respuesta":"Tiempo en que tarda la tierra en darle una vuelta al Sol.", 
 "t17correcta":"7", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Primavera" 
 }, 
{ 
 "t11pregunta":"Verano" 
 }, 
{ 
 "t11pregunta":"Otoño" 
 }, 
{ 
 "t11pregunta":"Invierno" 
 }, 
{ 
 "t11pregunta":"Zonas térmicas o de insolación" 
 }, 
{ 
 "t11pregunta":"Dos zonas polares, dos zonas templadas y una cálida." 
 }, 
{ 
 "t11pregunta":"364 días más seis horas" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11pregunta": "Relaciona las columnas según corresponda.",
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 },
 //16 opcion multiple
 { 
 "respuestas":[ 
 { 
 "t13respuesta":"Son líneas verticales imaginarias que seccionan al planeta.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Son líneas horizontales imaginarias que seccionan al planeta.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Líneas verticales que dividen al planeta en dos hemisferios con diferentes horarios.", 
 "t17correcta":"0", 
 }, 
{ 
 "t13respuesta":"Son  la forma de saber el tiempo que transcurre en el planeta.", 
 "t17correcta":"0", 
 }, 
],
 "pregunta":{ 
 "c03id_tipo_pregunta":"1", 
 "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Qué son los husos horarios?", 
 "t11instruccion": "", 
 } 
 }, 
//17 Falso verdadero 
{
 "respuestas":[
{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
],
 "preguntas": [
  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Las placas tectónicas son porciones de la tierra debajo de la corteza terrestre.  " ,
                "correcta"  : "1",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"La Tierras de divide en tres capas principales, el núcleo, el manto y la estratosfera. " ,

                "correcta"  : "0",

            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"La litosfera es la capa de la Tierra formada por el manto subyacente y la corteza.  " ,

                "correcta"  : "1",
            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"En el núcleo se originan la mayoría de los fenómenos que pueden afectar a la superficie de la Tierra.  " ,

                "correcta"  : "0",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"La litosfera se divide en 7 grandes placas tectónicas que son la Norteamericana, la Sudamericana, la Africana, la Euroasiática, la Indo-Australiana, la Polar y la del Pacífico. " ,

                "correcta"  : "0",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Los fenómenos naturales tales como terremotos o tsunamis se producen por el movimiento de las placas tectónicas.  " ,

                "correcta"  : "1",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"En nuestro planeta existen 36 placas tectónicas.  " ,

                "correcta"  : "0",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Debido a su tamaño, las placas tectónicas se clasifican en placas primarias y secundarias. " ,

                "correcta"  : "0",
            },
           
 ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true,
            "t11instruccion":""
        }
 },
//18 relacion a columnas
{ 
 "respuestas":[ 
{ 
 "t13respuesta":"Se encuentra en el continente europeo y se extiende hasta el oeste continente asiático.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Es la más grande de todas las placas, cuenta con aproximadamente 70 millones de kilómetros cuadrados.", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"Se extiende por toda Australia, África y la India.", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"Esta placa colinda con la placa de Filipinas y la del Pacífico, abarca el continentes asiático.", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"Situada en el océano Pacífico, cuenta con el mayor número de volcanes activos de todo el planeta.", 
 "t17correcta":"5", 
 }, 
{ 
 "t13respuesta":"Abarca los países de la India, Nueva Zelanda, Australia y alguna parte del océano índico. Esta placa tiene una convergencia con Filipinas lo que facilita el surgimiento de las islas.", 
 "t17correcta":"6", 
 }, 
{ 
 "t13respuesta":"Abarca la zona situada en el océano pacífico oriental. Cada año, choca con la placa Sudamericana, el choque de estas dos placas es lo que forma los Andes.", 
 "t17correcta":"7", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Placa Euroasiática del Oeste" 
 }, 
{ 
 "t11pregunta":"Placa del Pacífico" 
 }, 
{ 
 "t11pregunta":"Placa Antártica" 
 }, 
{ 
 "t11pregunta":"Placa Euroasiática del Este" 
 }, 
{ 
 "t11pregunta":"Placa Filipina" 
 }, 
{ 
 "t11pregunta":"Placa Indo-Australiana" 
 }, 
{ 
 "t11pregunta":"Placa Nazca" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11pregunta": "Relaciona las columnas según corresponda. ",
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } ,
 //19 Flaso verdadero 
 {
 "respuestas":[
{
                "t13respuesta": "Opción 1",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opción 2",
                "t17correcta": "1"
            }
],
 "preguntas": [
  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Se pueden  observar en la formación de cordilleras y erupciones volcánicas. " ,
                 "valores": ['Límites divergentes', 'Límites convergentes'],
                "correcta"  : "1",

            },{
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Este movimiento es provocado por la separación de placas." ,
            "valores": ['Límites divergentes', 'Límites convergentes'],
                "correcta"  : "1",

            }, {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Se le llama así a cuando chocan las placas tectónicas y se incrusta una en la otra generando la expansión." ,
              "valores": ['Subducción', 'Obducción'],
                "correcta"  : "1",
            }, {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Es el hundimiento de una placa oceánica debajo de una continental.  " ,
           "valores": ['Subducción', 'Obducción'],
                "correcta"  : "1",
            }, {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Movimiento muy brusco de la Tierra con distintas intensidades. " ,
          "valores": ['Erupción volcánica', 'Sismos'],
                "correcta"  : "1",
            }, {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Fenómeno natural que inunda las zonas costeras. " ,
                "valores": ['Terremoto', 'Tsunami'],
                "correcta"  : "1",
            }, {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Emisión de lava de alguna otra materia que procede desde el interior de nuestro globo terráqueo. " ,
                 "valores": ['Magma', 'Erupción volcánica'],
                "correcta"  : "1",
            },  {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Abertura o grieta de la corteza terrestre conectada a una cámara magmática que va desde el interior de la Tierra.  " ,
                 "valores": ['Montaña', 'Volcán'],
                "correcta"  : "1",
            }, {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Chimenea encargada de expulsar los materiales incandescentes, el vapor y cierto tipo de gases. " ,
                 "valores": ['Magma', 'Cráter'],
                "correcta"  : "1",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Roca fundida que se encuentra debajo de la Tierra." ,
                 "valores": ['Lava', 'Magma'],
                "correcta"  : "1",
            },  {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Líquido extremadamente caliente que sale a causa de una erupción volcánica. " ,
                 "valores": ['Magma', 'Lava'],
                "correcta"  : "1",
            },  {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"Cuando dos placas tectónicas chocan y se separan surgen los..." ,
                 "valores": ['Sismos', 'Volcanes'],
                "correcta"  : "1",
            },       {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"El leve choque de dos placas tectónicas generan los… " ,
                 "valores": ['Volcanes', 'Sismos'],
                "correcta"  : "1",
            },           
 ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la respuesta correcta según corresponda. ",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true,
            "t11instruccion":""
        }
 },
//20 falso verdadero
{
 "respuestas":[
{
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
],
 "preguntas": [
  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El 75% de la superficie del planeta es agua. " ,
                "correcta"  : "1",

            },
             {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El 50% de agua del planeta es salada y no es apta para consumo humano." ,

                "correcta"  : "0",

            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"2% del agua del planeta es dulce y apta para consumo humano." ,

                "correcta"  : "1",

            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"La mayor cantidad del recurso hídrico en ríos del planeta se ubica en América y Sudamérica ." ,

                "correcta"  : "1",
            },
            {
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"El principal uso de agua a nivel mundial se destina al ganado.  " ,

                "correcta"  : "0",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"La calidad del agua ha disminuido por el crecimiento poblacional y las actividades humanas. " ,

                "correcta"  : "0",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"El cambio climático es la variación que presenta la superficie de la Tierra.  " ,

                "correcta"  : "1",
            },{
                "c03id_tipo_pregunta": "13",
               "t11pregunta":"El efecto invernadero se refiere al  calor acumulado en la atmósfera de la Tierra por la capa de gases que se almacenan en esta. " ,

                "correcta"  : "0",
            },
           
 ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true,
            "t11instruccion":""
        }
 },
 //21 Arrastra corta
 {
        "respuestas": [
            {
                "t13respuesta": "Disminución de los<br> caudales de ríos y lagos. ",
                "t17correcta": "1,3,6,7"
            },
            {
                "t13respuesta": "Afectación en los ecosistemas y biomas. ",
                "t17correcta": "1,3,6,7"
            },
            {
                "t13respuesta": "Problemas de erosión de <br>la tierra.",
                "t17correcta": "1,3,6,7"
            }, {
                "t13respuesta": "Derretimiento de los polos.  ",
                "t17correcta": "1,3,6,7"
            },
            {
                "t13respuesta": " Derretimiento de los polos.  ",
                "t17correcta": "1,3,6,7"
            },
            {
                "t13respuesta": "Fenómeno migratorios con<br> mayor frecuencia.",
                "t17correcta": "2,4,5"
            },{
                "t13respuesta": "Incrementos en el<br> costo de  alimentos.",
                "t17correcta": "2,4,5"
            },
          
            {
                "t13respuesta": "Nuevas enfermedades y malnutrición.  ",
                "t17correcta": "2,4,5"
            },
            {
                "t13respuesta": "Disminución de las temperaturas. ",
                "t17correcta": "7"
            },
             {
                "t13respuesta": "Mejores niveles de educación. ",
                "t17correcta": "7"
            },
             {
                "t13respuesta": "Divorcios. ",
                "t17correcta": "7"
            },
            
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td style='width:150px'>Afectaciones en los recursos hidrológicos</td><td style='width:150px'>Afectaciones económico-sociales</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                  <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td> <td>Destrucción de ciudades, puertos, pueblos, etc.</td> \n\
                                        </tr>\n\
                                    <tr><td>Variaciones en los niveles de precipitación.</td>\n\
                                  <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                    <td>"
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },

  {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td><br><br><br>Aumento de temperaturas<br><br><br></td> <td>\n\
                                       "
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr><tr> <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td> </td><td>\n\
                                        </tr><tr> <td>"
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td><td></td>\n\
                                        </tr> </table>"
            },
              
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Coloca los enunciados según corresponda a causas internas o externas de la independencia de México. ",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },




];
