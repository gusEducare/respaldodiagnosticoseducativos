json=[
   {
        "respuestas": [
            {
                "t13respuesta": "<p>día<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>noche<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>rotación<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Tierra<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>geoide<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>este<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Sol<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>estrellas<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>24 horas<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>planeta<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>eje<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>1,700 km/h<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>velocidad constante<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>terrestre<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>polo<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>23°27’<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>rayos<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>tarde<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>esférico<\/p>",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "<p>365<\/p>",
                "t17correcta": "20"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>¿Alguna vez te has preguntado por qué  existen el día y la noche? Bueno, como debes saber, el <\/p>"
            },
            {
                "t11pregunta": "<p> y la <\/p>"
            },
            {
                "t11pregunta": "<p> son fenómenos que ocurren debido a la <\/p>"
            },
            {
                "t11pregunta": "<p> de la <\/p>"
            },
            {
                "t11pregunta": "<p>. La Tierra, nuestra nave en este cosmos, es un <\/p>"
            },
            {
                "t11pregunta": "<p> gigante que gira de oeste a <\/p>"
            },
            {
                "t11pregunta": "<p>, esto provoca que el <\/p>"
            },
            {
                "t11pregunta": "<p> y las demás <\/p>"
            },
            {
                "t11pregunta": "<p> salgan por el este y se pongan por el oeste. El ciclo de rotación de la Tierra tiene una duración de unas <\/p>"
            },
            {
                "t11pregunta": "<p> ㅡsegundos más, segundos menosㅡ, mismas que componen un día en el <\/p>"
            },
            {
                "t11pregunta": "<p>. Sabemos, por buena fuente, que la Tierra gira en su propio <\/p>"
            },
            {
                "t11pregunta": "<p> a una velocidad aproximada de unos <\/p>"
            },
            {
                "t11pregunta": "<p>, sin embargo nosotros no logramos percibir el movimiento debido a que esta se trata de una <\/p>"
            },
            {
                "t11pregunta": "<p>, es decir, no acelera o desacelera, por lo cual no es posible sentir su movimiento. De igual manera, es muy común pensar que el eje <\/p>"
            },
            {
                "t11pregunta": "<p> es algo parecido a un palito insertado justo a la mitad del planeta, de polo a <\/p>"
            },
            {
                "t11pregunta": "<p> y que la Tierra está siempre en la misma posición, mas esto no es del todo verdad.Si bien es cierto que el eje terrestre va de polo a polo, no se trata de una línea recta vertical, sino más bien de una línea recta con una ligera inclinación de <\/p>"
            },
            {
                "t11pregunta": "<p>. Por ese motivo, el calor emitido por los <\/p>"
            },
            {
                "t11pregunta": "<p> del Sol es heterogéneo sobre la faz de la Tierra, provocando así las distintas temperaturas en las distintas partes del mundo.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //2
    {  
      "respuestas":[
         {  
            "t13respuesta": "8758 horas o 365 días",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "8759 horas o 365 días",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "8760 horas o 365 días",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "8761 horas o 365 días",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "8762 horas o 365 días",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Sí, es siempre la misma porque la Tierra es estática.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "No, cambia constantemente porque la Tierra se mueve alrededor del Sol.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Sí, es siempre la misma, porque la Tierra es plana.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "No, cambia constantemente porque los astros se mueven alrededor de la Tierra, incluido el Sol.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Nadie lo sabe a ciencia cierta, a veces es estática, otras veces no.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "La Tierra está estática y el universo gira alrededor de ella. A esto se le conoce como teoría clásica de la estática.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "La Tierra gira alrededor del Sol, completando una vuelta aproximadamente cada 365 días, a esto se le conoce como traslación.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "La Tierra es parte de una alineación del sistema planetario, en donde cada estrella y elemento se alinea una vez con ella cada 365 días.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "La Tierra viaja libre en el universo cambiando constantemente de estrella, migrando cada 24 horas, produciendo el día y la noche.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Ninguna de las anteriores.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Un evento que ocurre cuando los rayos solares caen verticalmente sobre el ecuador, de manera que el día y la noche duran 12 horas.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Un evento que ocurre cuando los rayos solares caen en una posición de 45° con respecto al ecuador, de manera que el día y la noche duran 12 horas.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Un evento que ocurre cuando los rayos solares caen verticalmente sobre los trópicos, de manera que el día y la noche duran 12 horas.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Un evento que ocurre cuando los rayos solares caen en una posición de 20° con respecto al ecuador sobre los trópicos, de manera que el día y la noche duran 12 horas.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Un evento en el que los rayos del Sol caen verticalmente en los dos trópicos una vez al año.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Un evento donde los rayos caen en una posición de 30° en uno de los trópicos.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Un evento donde los rayos del sol caen verticalmente en uno de los trópicos dependiendo de la estación.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Un evento donde los rayos del sol se disipan en la atmósfera creando la auroras boreales.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cuánto tiempo es la duración de un año?",
                         "¿La bóveda celeste (la posición de las estrellas) es siempre la misma o cambia constantemente?, ¿por qué?",
                         "El hecho de que, durante el tiempo de un año, la posición de las estrellas esté cambiando constantemente quiere decir que…",
                         "¿Qué es un equinoccio?",
                         "¿Qué es un solsticio?"],
         "preguntasMultiples": true,
      }
   },
   //3
    {
        "respuestas": [
            {
                "t13respuesta": "...tres capas: corteza, manto y núcleo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...capas internas, la corteza se presenta como capa externa.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...corteza, que es una capa externa de unos 70 km de grosor.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...consecuencia directa de las placas de la tierra y su constante movimiento.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...tres direcciones: se alejan, chocan entre sí y se deslizan horizontalmente.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...tres tipos de zonas: de separación o divergencia, de contacto o convergencia y de deslizamiento lateral o transcurrentes.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La tierra está dividida en..."
            },
            {
                "t11pregunta": "Mientras que el manto y el núcleo se pueden clasificar como..."
            },
            {
                "t11pregunta": "La placas tectónicas corresponden a la..."
            },
            {
                "t11pregunta": "La sismicidad y el vulcanismo son..."
            },
            {
                "t11pregunta": "Las placas tectónicas pueden moverse en..."
            },
            {
                "t11pregunta": "Como consecuencia de estos tres tipos de movimiento surgen..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //4
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Sismo<\/p>",
                "t17correcta": "1,3,5"
            },
            {
                "t13respuesta": "<p>Volcán<\/p>",
                "t17correcta": "2,6,8"
            },
            {
                "t13respuesta": "<p>Sismo<\/p>",
                "t17correcta": "1,3,5"
            },
            {
                "t13respuesta": "<p>Depresiones<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Sismo<\/p>",
                "t17correcta": "1,3,5"
            },
            {
                "t13respuesta": "<p>Volcán<\/p>",
                "t17correcta": "2,6,8"
            },
            {
                "t13respuesta": "<p>Relieve oceánico<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Volcán<\/p>",
                "t17correcta": "2,6,8"
            },
            {
                "t13respuesta": "<p>Sismicidad<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>Vulcanismo<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<ol><li>Se genera por la colisión de las placas tectónicas:</li>"
            },
            {
                "t11pregunta": "<li>Es un fenómeno geológico que se observa en la superficie de la Tierra como una elevación:</li>"
            },
            {
                "t11pregunta": "<li>Se propaga a través de diferentes tipos de ondas, que pueden atravesar diferentes materiales:</li>"
            },
            {
                "t11pregunta": "<li>Pueden ser de dos tipos, absolutas y relativas. Las relativas están situadas sobre el nivel del mar:</li>"
            },
            {
                "t11pregunta": "<li>Es un movimiento vibratorio de la corteza terrestre:</li>"
            },
            {
                "t11pregunta": "<li>Algunos de sus elementos son el cráter, la chimenea y la cámara magmática:</li>"
            },
            {
                "t11pregunta": "<li>Está debajo de los cero metros, es decir, debajo del mar:</li>"
            },
            {
                "t11pregunta": "<li>Surge cuando el magma está entre grietas o fisuras y sale en forma de lava:</li>"
            },
            {
                "t11pregunta": "<li>Es la relación entre sismos de una región y un área definida:</li>"
            },
            {
                "t11pregunta": "<li>Se refiere a los fenómenos y procesos relacionados con los volcanes, también está relacionado con el movimiento de las placas tectónicas:</li>"
            },
            {
                "t11pregunta":"</ol>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra cada concepto a su definición:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //5
      {  
      "respuestas":[
         {  
            "t13respuesta": "Mar de Bering",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Océano Ártico",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Mar de Filipinas",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Océano Pacífico",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Océano Mediterráneo",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Océano Atlántico",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Mar Arábigo",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Océano Ártico",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Mar Caribe",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Mar de Filipinas",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Océano Ártico",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Océano Pacífico",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Mar Arábigo",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Océano Antártico",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Mar Caribe",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Océano Índico",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Mar de Filipinas",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Mar Mediterráneo",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Mar Arábigo",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Océano Antártico",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Observa el mapa presionando \"+\" y selecciona las respuestas correctas<br><br>¿Cuáles de los siguientes son nombres de algunos de los océanos del planeta?",
                         "¿Cuales de los siguientes mares u océanos colindan con Asia?",
                         "¿Con qué océanos o mares colinda el Océano Atlántico?",
                         "Si estuvieras en la isla de Australia y quisieras ir a Alemania, ¿qué mares y océanos deberías cruzar?"],
         "preguntasMultiples": true,
         "t11instruccion":"<center/><img src='GI6E_B02_A05_01.png' style='width:900px;'/>"
      }
   },
   //6
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La parte de la tierra cubierta por agua se conoce como litosfera.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los océanos y mares del planeta se encuentran siempre estáticos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al hecho de que el agua baje y suba por causa de la fuerza de atracción de la Luna y el Sol se le conoce como marea.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En un periodo de 48 horas hay dos mareas altas y dos mareas bajas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si estuvieras en la playa y hubiese marea alta, deberías esperar cerca de unas seis  horas para que cambie la marea y puedas meterte al mar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La circulación de grandes masas de agua en la superficie de los océanos es conocida como corrientes marinas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las corrientes marinas pueden ser frías, cálidas, templadas y tropicales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las corrientes marinas cálidas parten de la línea del ecuador hacia los polos, mientras que las frías lo hacen a la inversa.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La mayoría de las corrientes cálidas se concentran en el océano Atlántico, mientras que las frías lo hacen en el océano Pacífico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las corrientes marinas permiten que la vida en los océanos sea fructífera, pues transportan peces para que sirvan de alimento o para que encuentren los lugares más propicios para desovar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las corrientes marinas de igual modo sirven a los barcos durante su navegación, pues la ruta que trazan para llegar a su destino se basa en ellas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "...los trópicos, el ecuador y los círculos polares.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...definidas por el eje de la inclinación de la Tierra y la forma en que inciden los rayos solares sobre la superficie del planeta.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...calientan más la superficie.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...calientan menos la superficie.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...son 5, dos templadas, dos polares y una tropical.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...en los climas del planeta y con ello indirectamente en la distribución de la flora y fauna.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Los paralelos más importantes en la Tierra son..."
            },
            {
                "t11pregunta": "Las zonas térmicas de la Tierra están..."
            },
            {
                "t11pregunta": "Los rayos que caen en el ecuador..."
            },
            {
                "t11pregunta": "Los rayos que caen sobre los polos..."
            },
            {
                "t11pregunta": "Las zonas térmicas que existen en la Tierra..."
            },
            {
                "t11pregunta": "Las zonas térmicas tienen una gran influencia..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    }
];
