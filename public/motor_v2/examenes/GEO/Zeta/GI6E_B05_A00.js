json=[

    {  
      "respuestas":[
         {  
            "t13respuesta": "Calidad de vida.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Bienestar individual.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Bienestar social.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
          {  
            "t13respuesta": "Bienestar.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Calidad de vida. ",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Paz.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
           {  
            "t13respuesta": "Bienestar físico.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Bienestar social.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Bienestar mental.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
            {  
            "t13respuesta": "Índice de Desarrollo Humano.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tasa de Desarrollo Humano.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Índice de Calidad de Vida.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }, 
          {  
            "t13respuesta": "Bienestar social.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Bienestar mental.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Bienestar individual.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }, 
            {  
            "t13respuesta": "Australia.",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Estados Unidos.",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Suecia.",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }, 
            {  
            "t13respuesta": "Estados Unidos.",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Suecia.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Noruega.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Bienestar mental.",
            "t17correcta": "1",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Bienestar individual.",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Bienestar social.",
            "t17correcta": "0",
            "numeroPregunta":"7"
         }

        
      ],
      "pregunta":{  
         
         "c03id_tipo_pregunta":"1",
         
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Es una medición del bienestar de las personas en relación al lugar en donde viven.",
          "Es el estado de la persona cuyas condiciones físicas y mentales le proporcionan un sentimiento de satisfacción y tranquilidad.",
          "Este aspecto del bienestar  involucra factores como la salud, buena alimentación, actividad física, buenos servicios sanitarios, entre otros.",
          "Medición de la calidad de vida desarrollado por la Organización de las Naciones Unidas.",
          "Este aspecto del bienestar incluye factores como la seguridad, la paz social, tiempo libre y acceso a servicios necesarios.",
          "Según el OECD,  ¿qué país tiene el mayor índice de calidad de vida?",
          "Según el OECD, ¿qué país tiene la mejor vivienda?",
          "Este aspecto del bienestar es el equilibro entre la mente y aquello que nos rodea, es decir, cómo nos relacionamos con la sociedad."],

         "preguntasMultiples": true,
         
      }
   },
   //2
   {
    "respuestas": [
        {
            "t13respuesta": "Índice de Desarrollo Humano (IDH).",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Índice para una Vida Mejor- OECD.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "Comunidad.",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "Seguridad.",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "Balance vida-trabajo.",
            "t17correcta": "5"
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "Tipo de medición de la calidad de vida que contempla solamente la esperanza de vida, el nivel educativo y el ingreso por habitante."
        },
        {
            "t11pregunta": "Tipo de medición que contempla once factores para medir la calidad de vida."
        },
        {
            "t11pregunta": "Según la OECD la medición del porcentaje de personas que confiarán en amigos o parientes en caso de necesidad se refiere al factor."
        },
        {
            "t11pregunta": "Según la OECD la medición de la cantidad de homicidios y el promedio de asaltos se refiere al factor."
        },
        {
            "t11pregunta": "Según la OECD la medición del tiempo destinado al ocio, el cuidado personal de quienes trabajan se refiere al factor."
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda."
    }
},
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>recursos naturales<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>sustentable del agua<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>2.5%<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>consumo<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>distribuida uniformemente<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>carecen<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>aprovechamiento responsable<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>derecho<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>necesidades<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>deforestación<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>vegetación<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>erosión del suelo<\/p>",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "<p>equilibrio<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>uso inadecuado del agua<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>actividad ganadera<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>desertificación<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>mitigar<\/p>",
                "t17correcta": "17"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Nuestro estilo de vida es posible gracias a los diferentes <\/p>"
            },
            {
                "t11pregunta": "<p>, los cuales nos brindan las condiciones necesarias para cultivar comida, bañarnos, tomar agua, e incluso poder utilizar la tecnología. Por esta razón, el aprovechamiento <\/p>"
            },
            {
                "t11pregunta": "<p> es vital, pues sin este no existiría la vida en el planeta, aún más importante, el agua se encuentra en una situación de escasez, tan solo <\/p>"
            },
            {
                "t11pregunta": "<p> de toda la existente es apta para <\/p>"
            },
            {
                "t11pregunta": "<p>. Otra problemática que enfrentamos es que no está <\/p>"
            },
            {
                "t11pregunta": "<p>, hay gente que tiene más agua de la que necesita y otros en cambio <\/p>"
            },
            {
                "t11pregunta": "<p> de ella. Para combatir  estas dificultades es necesario generar un <\/p>"
            },
            {
                "t11pregunta": "<p> del agua que implica satisfacer las necesidades de las generaciones presentes sin comprometer el <\/p>"
            },
            {
                "t11pregunta": "<p> de las generaciones futuras de satisfacer sus propias <\/p>"
            },
            {
                "t11pregunta": "<p>. A la par, se encuentra un problema ambiental llamado <\/p>"
            },
            {
                "t11pregunta": "<p>, resultado de la destrucción de la <\/p>"
            },
            {
                "t11pregunta": "<p> de un lugar o ecosistema. Esto tiene como repercusiones  la <\/p>"
            },
            {
                "t11pregunta": "<p> y variaciones climáticas que impiden el <\/p>"
            },
            {
                "t11pregunta": "<p> en el ecosistema y la obtención de diversos productos como madera, alimentos y combustibles. Aunque no lo creas, esta problemática se debe principalmente al <\/p>"
            },
            {
                "t11pregunta": "<p>, incendios forestales, tala incontrolada de árboles y la <\/p>"
            },
            {
                "t11pregunta": "<p> y agrícola intensiva. Y bueno, como el medio ambiente está altamente relacionado, la <\/p>"
            },
            {
                "t11pregunta": "<p> del suelo es una consecuencia de la deforestación y consiste en la degradación del suelo fértil hasta dejarlo inservible para cualquier actividad productiva.<br><br>Afortunadamente aún es posible generar acciones cotidianas, medianas y grandes para revertir o <\/p>"
            },
            {
                "t11pregunta": "<p> estos efectos y problemáticas.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el párrafo con las palabras de los recuadros según corresponda.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //4
    {  
        "respuestas":[
           {  
              "t13respuesta": "Desastres naturales.",
              "t17correcta": "1",
              "numeroPregunta":"0"
           },
           {  
              "t13respuesta": "Desastres socioambientales.",
              "t17correcta": "0",
              "numeroPregunta":"0"
           },
           {  
              "t13respuesta": "Desastres provocados por el hombre.",
              "t17correcta": "0",
              "numeroPregunta":"0"
           }, 
            {  
              "t13respuesta": "Desastres.",
              "t17correcta": "1",
              "numeroPregunta":"1"
           },
           {  
              "t13respuesta": "Desastres provocados por el hombre.",
              "t17correcta": "0",
              "numeroPregunta":"1"
           },
           {  
              "t13respuesta": "Crisis ambiental.",
              "t17correcta": "0",
              "numeroPregunta":"1"
           }, 
             {  
              "t13respuesta": "Desastres provocados por el hombre.",
              "t17correcta": "1",
              "numeroPregunta":"2"
           },
           {  
              "t13respuesta": "Desastres artificiales.",
              "t17correcta": "0",
              "numeroPregunta":"2"
           },
           {  
              "t13respuesta": "Desastres naturales.",
              "t17correcta": "0",
              "numeroPregunta":"2"
           }, 
              {  
              "t13respuesta": "Condiciones sociales.",
              "t17correcta": "1",
              "numeroPregunta":"3"
           },
           {  
              "t13respuesta": "Condiciones de infraestructura.",
              "t17correcta": "0",
              "numeroPregunta":"3"
           },
           {  
              "t13respuesta": "Condiciones educativas.",
              "t17correcta": "0",
              "numeroPregunta":"3"
           }, 
            {  
              "t13respuesta": "Condiciones económicas.",
              "t17correcta": "1",
              "numeroPregunta":"4"
           },
           {  
              "t13respuesta": "Condiciones sociales.",
              "t17correcta": "0",
              "numeroPregunta":"4"
           },
           {  
              "t13respuesta": "Condiciones de infraestructura.",
              "t17correcta": "0",
              "numeroPregunta":"4"
           }, 
              {  
              "t13respuesta": "La destrucción del medio ambiente debido a la actividad humana puede llegar a influir, empeorar y ocasionar los distintos tipos de desastres.",
              "t17correcta": "1",
              "numeroPregunta":"5"
           },
           {  
              "t13respuesta": "La actividad humana está relacionada con los desastres ocasionados por los humanos  y no tiene relación con lo ambiental.",
              "t17correcta": "0",
              "numeroPregunta":"5"
           },
           {  
              "t13respuesta": "No existe una relación directa.",
              "t17correcta": "0",
              "numeroPregunta":"5"
           }
        ],
        "pregunta":{  
           
           "c03id_tipo_pregunta":"1",
           
           "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Las erupciones volcánicas, los terremotos y las inundaciones son ejemplos de:",
            "Hace referencia a las enormes pérdidas humanas y materiales que ocasionan sucesos devastadores como terremotos, deforestación, contaminación ambiental y otros.",
            "Las inundaciones causadas indirectamente por el cambio climático, la explosión de una fábrica y el derrame de petróleo en el mar, son ejemplos de:",
            "Hace referencia a las condiciones que afectan a la población ante un desastre debido a una mala planeación o falta de información entre los afectados.",
            "Hace referencia a las condiciones que permiten satisfacer las necesidades humanas básicas  como trabajo e ingresos económicos, educación, salud o vivienda adecuada para afrontar los posibles desastres.",
            "¿Qué relación existe entre el medio ambiente, la actividad humana  y los desastres ambientales?"],
  
           "preguntasMultiples": true,
           
        }
     },
]