json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las montañas son terrenos con muy poca pendiente, casi planos y  que además se encuentran muy elevados.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los valles son porciones de terreno cuya forma es la de una “V”, normalmente son formados por el paso de un río.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las mesetas son terrenos de grande extensión, planos y que se encuentran casi al nivel del mar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las llanuras son elevaciones muy altas, normalmente escarpadas. Mientras más larga sea la llanura tiene la posibilidad de convertirse en una sierra.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En México existen al menos cinco sistemas montañosos importantes: la sierra madre oriental, la sierra madre occidental, la sierra madre de  chiapas, la sierra de baja california y el sistema volcánico transversal.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunos de los estados mexicanos que comparten terreno con la sierra madre oriental son: Querétaro, San Luis Potosí y Guanajuato.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La sierra madre occidental corre a través de los estados de Chihuahua, Sonora, Sinaloa, Durango y Nayarit.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //2
    {  
      "respuestas":[
         {  
            "t13respuesta": "Una estructura geológica por medio de la cual emerge el magma a la superficie.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La montaña más alta en una sierra o cordillera.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Un sistema escarpado de difícil acceso.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Una estructura geológica formada por rocas, tierra y roca fundida",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Los volcanes inactivos rara vez hacen erupción, los activos comúnmente sí.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los volcanes activos están en constante erosión, por ello terminan siendo valles.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los volcanes inactivos no tienen flora ni fauna, los activos la tienen.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los volcanes activos son más predecibles, los inactivos menos.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los volcanes serenos",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los volcanes extintos",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los volcanes climáticos",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los volcanes desérticos",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Guadalajara",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Baja California Sur",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "San Luis Potosí",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Colima",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "San Luis Potosí",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Puebla",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Durango",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Yucatán",
            "t17correcta": "1",
            "numeroPregunta":"4"
         }

         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las respuestas correctas:<br/><br/>¿Qué es un volcán?",
                         "<br/><br/>¿Cuáles son las diferencias entre un volcán activo frente a un inactivo?",
                         "Además de los volcanes inactivos existe una tercera clase de volcanes, ¿cuál es?",
                         "¿En cuáles de los siguientes estados se pueden encontrar volcanes inactivos?",
                         "¿En cuáles de los siguientes estados NO se encuentran volcanes?"],
         "preguntasMultiples": true,
      }
   },
   //3
   {
        "respuestas": [
            {
                "t13respuesta": "conecta con otro cuerpo de agua. Normalmente nacen en las montañas y desembocan en el mar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "normalmente se acumula en una depresión y tiene escaso movimiento.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "se acumula en una depresión, tiene poca profundidad y puede ser de agua dulce o salada.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "se encuentra encerrada entre dos puntas de tierra.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "una masa de agua salada de gran tamaño.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Un río es un cuerpo de agua que se mantiene en constante movimiento y..."
            },
            {
                "t11pregunta": "Un lago es un cuerpo de agua dulce que..."
            },
            {
                "t11pregunta": "Una laguna es un cuerpo de agua que..."
            },
            {
                "t11pregunta": "Un golfo es una gran parte de océano que..."
            },
            {
                "t11pregunta": "Un mar es..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //4
    {  
      "respuestas":[
         {  
            "t13respuesta": "Río Bravo, Río Suchiate y Río Hondo.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Río Balsas, Río Bravo y Río Usumacinta.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Río Bravo, Río Nueces, y Río Hondo.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Río Hondo, Río  Suchiate y Río Santiago.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Río Santiago, Río Usumacinta y Río Grande.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Río Mayo, Río Yaqui y Río Papagayo.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Río Tamesí, Río San Fernando y Río Sabinas.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Río Piaxtla, Río Río Grijalva y Río Balsas.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Río Pánuco, su vertiente está en el Golfo de México.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Río Colorado, su vertiente está en el Golfo de California.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Río Lerma, su vertiente está en el lago de Chapala.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Río Coatzacoalcos, su vertiente está en el lago Catemaco.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Río Usumacinta, su caudal fluye por el estado de Tabasco.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Río Lerma, su caudal fluye por el estado de México, Querétaro y Jalisco.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Río Pánuco, su caudal fluye por el estado de San Luis Potosí, Tamaulipas y Veracruz.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Río Sabinas, su caudal fluye por los estados de Chihuahua y Nuevo León.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         } 
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa la imagen presionando \"+\" y selecciona la respuesta correcta:<br/><br/>¿Cuáles son los nombres de algunos de los ríos que además pueden ser ubicados como fronteras naturales en México?",
                         "¿Cuáles son algunos de los ríos cuya vertiente queda sobre el pacífico?",
                         "¿Cuál de los siguientes ríos es el más extenso de México y dónde desemboca?",
                         "¿Cuál de los siguientes ríos es el más caudaloso de México y de qué estado ocupa mayor extensión?"],
         "preguntasMultiples": true,
         "t11instruccion":"<center/><img src='GI4E_B02_A04_01.png'/>",
      }
   },
   //5
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El estado del tiempo no tiene nada que ver con las condiciones de la atmósfera de un lugar.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La palabra atmósfera que viene del griego atmós que significa vapor y sfaira que significa a esfera, por lo tanto la palabra atmósfera se refiere a la capa de aire que nos rodea.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La temperatura, los vientos, la humedad, las precipitaciones y la presión atmosférica son características propias del clima.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El clima se refiere al estado de la atmósfera de un lugar, es decir, el hecho de que se mantengan estables las condiciones meteorológicas de un lugar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Clima y estado del tiempo son esencialmente una y la misma cosa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "México está dividido en tres grandes grupos climáticos.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona Verdadero (V) o Falso (F) según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //6
    {  
      "respuestas":[
         {  
            "t13respuesta": "La distribución y masificación del ecosistema.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La distribución geográfica de montes y cordilleras.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La latitud, el relieve y la circulación de los vientos.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "El viento, la longitud y la hidrografía.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "En el norte se asemeja más a un clima cálido y húmedo, mientras que en el sur es más tropical.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "En el norte se asemeja más a un clima seco, mientras que en sur es un clima tropical.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "En el norte representa más un clima frío y desértico, mientras que en el sur un clima templado es el que predomina.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "En el norte el clima es más bien cálido y seco, mientras que en el sur es húmedo y desértico.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Son 3: Tropical, Templado y Húmedo.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Son 4: Tropical, Templado, Seco y Frío.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Son 5: Tropical, Templado, Frío, Húmedo y Desértico.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Son 6: Tropical, Templado, Húmedo, Frío, Seco y Desértico.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Tropical húmedo",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tropical subhúmedo",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Seco",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Templado",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tropical húmedo",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Tropical subhúmedo",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Seco",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Templado",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Tropical húmedo",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Tropical subhúmedo",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Seco",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Templado",
            "t17correcta": "1",
            "numeroPregunta":"5"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>En México, ¿qué es lo que determina los climas?",
                         "En cierta forma podríamos decir que México está dividido en dos por el trópico de Cáncer.<br/>¿Cuáles son los climas que se presentan al norte y al sur de este meridiano?",
                         "¿Cuántos y cuáles son los grandes grupos climáticos de México?",
                         "¿Cuál es el clima de la península de Baja California?",
                         "¿Cuál es el clima del estado de Yucatán?",
                         "¿Cuál es el clima que predomina en el estado de Jalisco?"],
         "preguntasMultiples": true,
      }
   },
   //7
   {
        "respuestas": [
            {
                "t13respuesta": "Selva húmeda",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Selva seca",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Bosque templado",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Bosques de neblina",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Pastizal",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Matorral xerófilo",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Manglar",
                "t17correcta": "6"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algas, lirios, ninfas, tule, mangle y sauce, entre otras.",
                "correcta"  : "6"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Borrego cimarrón, coyote, mapache y murciélagos.",
                "correcta"  : "5"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Roedores, pastos y las gramíneas.",
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pino, encino y helechos arborescentes.",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Coníferas, mapaches, venados, armadillos, tejones y pumas.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Caoba, helechos, epífitas. Jaguar, mono araña, tucán y anfibios.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Armadillo, ocelote, iguana, lagarto y tortugas.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la casilla sobre la flora o fauna que coincida con su región natural:",
            "descripcion":"",
            "evaluable":false,
            "evidencio": false,
            "anchoColumnaPreguntas":17 
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "...la variedad de organismos vivos que existen en un lugar.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...son aquellos que tiene un clima tropical, como las selvas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...por lo cual es considerado un país megadiverso.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "... medicinas, resinas, aceites, fibras, maderas, pieles, etc.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...que es ella la que nos alimenta y cubre nuestras necesidades básicas.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El término biodiversidad se refiere a..."
            },
            {
                "t11pregunta": "Usualmente los lugares en que existe una mayor concentración de especies..."
            },
            {
                "t11pregunta": "En México se encuentra alrededor del 12% de todas las especies del planeta..."
            },
            {
                "t11pregunta": "Gracia a la biodiversidad existente en el país es posible obtener..."
            },
            {
                "t11pregunta": "La importancia de cuidar la biodiversidad del país radica en..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    }
];
