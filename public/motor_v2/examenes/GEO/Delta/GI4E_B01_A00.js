json = [
  {
    "respuestas": [
      {
        "t17correcta": "5"
      },
      {
        "t17correcta": "2"
      },
      {
        "t17correcta": "11"
      },
      {
        "t17correcta": "4"
      },
      {
        "t17correcta": "8"
      },
      {
        "t17correcta": "10"
      },
      {
        "t17correcta": "1"
      },
      {
        "t17correcta": "7"
      },
      {
        "t17correcta": "3"
      },
      {
        "t17correcta": "9"
      },
      {
        "t17correcta": "6"
      }
    ],
    "preguntas": [
      {
        "t11pregunta": "&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Antártida&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Pacífico <br>&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Antártico&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " América <br>&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Índico&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Ártico <br>&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Oceanía&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Europa <br>&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Asia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " África <br>&nbsp;&nbsp;"
      },
      {
        "t11pregunta": " Atlántico"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "6",
      "t11pregunta": "Observa atentamente la imagen y escribe el número que corresponde con el continente u océano.<br><br><center><img src='GI4E_B01_R01.png' width=520px height=350px/></center>",
      "t11instruccion": "",
      evaluable: true,
      pintaUltimaCaja: false
    }
  },

  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },
    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Centroamérica  abarca  la parte norte del continente.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Norteamérica está integrada por Canadá y Estados Unidos.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Centroamérica, al estar entre América del Sur y América del Norte, se encuentra en la latitud norte.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El planeta ha sido dividido en latitud norte y sur, gracias a una línea imaginaria llamada Ecuador.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El Ecuador rodea al planeta en su circunferencia de forma horizontal.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Belice, Guatemala, Honduras, El Salvador, Panamá, Nicaragua y Costa Rica, y algunas islas como Haití y Jamaica, integran Centroamérica.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Norteamérica también es conocida como América Latina.",
        "correcta": "0",
      },
      { 
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Sudamérica está conformada por los países de Colombia, Perú, Brasil, Argentina, Bolivia, Ecuador, Uruguay, Paraguay, Chile y Venezuela.",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },
  //3
  /*
  {
    "respuestas": [
      {
        "t13respuesta": "Gran variedad de <br>flora y fauna.",
        "t17correcta": "1,3"
      },
      {
        "t13respuesta": "Predomina el clima <br>tropical.",
        "t17correcta": "3,1"
      },
      {
        "t13respuesta": "Gran variedad de bosques<br> templados y fríos.",
        "t17correcta": "4,5"
      },
      {
        "t13respuesta": "Bosques tropicales <br>y selvas.",
        "t17correcta": "5,4"
      },
      {
        "t13respuesta": "Climas variados, tropicales,<br> fríos y templados.",
        "t17correcta": "2,6"
      },
      {
        "t13respuesta": "Planicies y altas<br>montañas.",
        "t17correcta": "6,2"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Centroamérica</td><td>Norteamérica</td><td>Sudamérica</td></tr><tr><td>"
      },
      {
        "t11pregunta": "</td><td>Zonas geográficas y <br>climas muy variados.</td><td>"
      },
      {
        "t11pregunta": "</td></tr><tr><td>"
      },
      {
        "t11pregunta": "</td><td>"
      },
      {
        "t11pregunta": "</td><td>Selvas y  llanuras.</td></tr><tr><td></td><td>"
      },
      {
        "t11pregunta": "</td><td>"
      },
      {
        "t11pregunta": "</td></tr></table>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que correspondan a cada región.",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": false
    }
  },*/

  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El río que separa a México de Guatemala en el Sur se llama Grijalva ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El río que separa a México de Estados Unidos en el norte se llama Bravo.",
        
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El Borde que se establece entre las naciones y determina la extensión territorial que pertenece a cada país se llama latitud .",
        
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El golfo es una gran parte de mar encerrado por puntas de tierra.",
       
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Las penínsulas son extensiones de tierra rodeadas por mar, pero que se mantienen unidas a la gran masa continental.",
       
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Selecciona falso o verdadero según corresponde .</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 50,
      "evaluable": true
    }
  },

  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },
    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "México es un país con 33 entidades: 32 estados y  1 capital.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "La capital de nuestro país es Ciudad de México y es su ciudad más poblada.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Chihuahua es el estado con mayor extensión territorial.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El estado  más pequeño es Tlaxcala.",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },

  {
    "respuestas": [
      {
        "t13respuesta": "Agricultura",
        "t17correcta": "1,5"
      },
      {
        "t13respuesta": "Pesca",
        "t17correcta": "5,1"
      },
      {
        "t13respuesta": "Industrias manufactureras",
        "t17correcta": "2,6"
      },
      {
        "t13respuesta": "Industrias automotrices",
        "t17correcta": "6,2"
      },
      {
        "t13respuesta": "Restaurantes",
        "t17correcta": "3,4"
      },
      {
        "t13respuesta": "Servicios turísticos",
        "t17correcta": "4,3"
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Primarias</td><td>Secundarias</td><td>Terciarias</td></tr><tr><td>"
      },
      {
        "t11pregunta": "</td><td>"
      },
      {
        "t11pregunta": "</td><td>"
      },
      {
        "t11pregunta": "</td></tr><tr><td>Ganadería</td><td>Industrias alimenticias</td><td>"
      },
      {
        "t11pregunta": "</td></tr><tr><td>"
      },
      {
        "t11pregunta": "</td><td>"
      },
      {
        "t11pregunta": "</td><td>Transporte</td></tr><tr><td></td><td></td><td>Almacenamiento</td></tr></table>"
      }
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las actividades económicas donde correspondan.",
      "t11instruccion": "",
      "respuestasLargas": true,
      "pintaUltimaCaja": false,
      "contieneDistractores": true
    }
  },

  {
    "respuestas": [
      {
        "t13respuesta": "Baja California",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Sonora",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Chihuahua",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Coahuila",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Nuevo León",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Tamaulipas",
        "t17correcta": "1",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Baja California Sur",
        "t17correcta": "0",
        "numeroPregunta": "0"
      },
      {
        "t13respuesta": "Sinaloa",
        "t17correcta": "0",
        "numeroPregunta": "0"
      }, ///////////////////////////////////////////////////////
      {
        "t13respuesta": "Quintana Roo",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Campeche",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Tabasco",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Chiapas",
        "t17correcta": "1",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Yucatán",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Tamaulipas",
        "t17correcta": "0",
        "numeroPregunta": "1"
      },
      {
        "t13respuesta": "Oaxaca",
        "t17correcta": "0",
        "numeroPregunta": "1"
      }, ///////////////////////////////////////////////////
      {
        "t13respuesta": "Tamaulipas",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Veracruz",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Tabasco",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Campeche",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Yucatán",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Quintana Roo",
        "t17correcta": "1",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Nuevo León",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
      {
        "t13respuesta": "Sonora",
        "t17correcta": "0",
        "numeroPregunta": "2"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "2",
      "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.\n\
 <br><br><center><img src='GI4E_B01_R07_01.png' width=480px height=200px/></center><br>\n\
 ¿Cuáles son los estados fronterizos al norte de México?" , "<center><img src='GI4E_B01_R07_02.png' width=480px height=200px/></center><br>\n\
 ¿Cuáles son los estados fronterizos al sur de México?", "¿Cuáles son los estados que colindan con el Golfo de México?"],
      "t11instruccion": "",
      "preguntasMultiples": true
    }
  },

  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },
    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Quintana Roo es el único estado que colinda con el mar Caribe.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Estado de México y Morelos rodean a la Ciudad de México.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Durango, Zacatecas, San Luis Potosí, Aguascalientes y Guanajuato, no tienen de vecino a algún país, no están al lado de la Ciudad de México pero tienen mar.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Querétaro, Hidalgo y Tlaxcala  no tienen mar, no están cerca de otro país y tampoco están al lado de la Ciudad de México.",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<P>Elige falso (F) o verdadero (V) según corresponda.</P>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },

  {
    "respuestas": [
      {
        "t13respuesta": "Disciplina científica que se especializa en la elaboración de mapas, a través de las formas y dimensiones que presenta un territorio determinado.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Se basa en las ciencias geográficas y en las matemáticas.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Logra hacer cálculos entre las distancias, medir elevaciones o la extensión de un lago.",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Se basa en ciencias matemáticas únicamente.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Disciplina científica que se especializa en la creación de dimensiones que presenta un territorio determinado.",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Puede determinar algunos componentes del espacio como el clima, la dinámica hidrológica, flora y fauna.",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "2",
      "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Cuáles de las siguientes frases están relacionadas con la cartografía?",
      "t11instruccion": "",
    }
  },

  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      }

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "En el mapa del clima se puede observar las zonas boscosas.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Existen mapas que muestran el clima, los bosques, las ciudades más importantes y la localización de volcanes.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El título indica la información que contiene un mapa.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El título aborda el tema de forma indirecta.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El título es amplio para describir detalladamente el tema.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Los símbolos son pequeños dibujos que representan información.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Para interpretar un mapa es necesario ver la orientación o la rosa de los vientos.",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },
  //11
  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El sureste es el punto que se encuentra entre el norte y el sur.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El sureste es el punto que se encuentra entre el sur y el este.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El noroeste es el punto que se encuentra entre el norte y el oeste.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El suroeste es el  punto que se encuentra entre el sur y el oeste",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },
  //=============================== Integracion nuevas act 19 septiembre
  //12
  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Un paisaje está formado por todos los objetos y fenómenos que ocupan ciertos espacios.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El paisaje hace referencia a varios elementos inertes que juntos forman un escenario determinado.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "La humedad es un fenómeno inerte. ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Los objetos  inertes son elementos estáticos como rocas, montañas, tierra y arena.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Los fenómenos son acciones propias del entorno natural.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Existen fenómenos visibles como un río e invisibles como el calor.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Cultura es la forma en la que vive un grupo humano, caracterizado por conocimientos, tradiciones, ideas, costumbres, etc.",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 60,
      "evaluable": true
    }
  },
  //13
  {
    "respuestas": [
      {
        "t13respuesta": "Natural",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Cultural o humanizado ",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Sucesos ecológicos",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Sombrillas",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Interacción de seres vivos con objetos naturales ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Palapas",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Ecosistemas",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Matorrales",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Caminos",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Bosque",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Carreteras",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Arrecifes",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Acción del hombre ",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Marca la opción que corresponda a cada tipo de paisaje.</p>",
      "t11instruccion": "",
      "descripcion": "Elementos",
      "variante": "editable",
      "anchoColumnaPreguntas": 50,
      "evaluable": true
    }
  },
  //14
  {
    "respuestas": [
      {
        "t13respuesta": "F",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "V",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Algunos ejemplos de relieve son las montañas y edificios de nuestra ciudad. ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El relieve se compone  del suelo y las superficies de diferentes formas. ",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "En el  territorio mexicano se conjuntan cinco placas tectónicas: la placa de Norteamérica, la de Cocos, la del Pacífico, la de Rivera y la del Atlántico.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Las mesetas son parecidas a las montañas pero  en la parte superior tienen forma piramidal. ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Los valles son una parte de la superficie de la tierra que es plano en comparación con el terreno que lo rodea. ",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Las cuencas son una parte de terreno que se encuentra más arriba en comparación de la superficie que la rodea. ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Las llanuras son grandes extensiones de superficie plana. ",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El relieve mexicano se divide en: mesetas, altiplanos, montañas, depresiones y valles.",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "El Cañón del Sumidero es un ejemplo de una depresión.",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "La Sierra Madre Occidental y el Sistema Volcánico Transversal son  ejemplos de  montañas.",
        "correcta": "1",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.</p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 50,
      "evaluable": true
    }
  },
  //15
  {
    "respuestas": [
      {
        "t13respuesta": "el ciclo del agua",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "el ciclo de filtración",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "ciclo de condensación",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "precipitación",
        "t17correcta": "0",
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "1",
      "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Cómo se le llama al  proceso en el que el  agua pasa de una forma a otra?",
      "t11instruccion": "",
    }
  },
  //16
  {
    "respuestas": [
      {
        "t13respuesta": "Precipitación",
        "t17correcta": "1",
      },
      {
        "t13respuesta": "Estado gaseoso del agua",
        "t17correcta": "2",
      },
      {
        "t13respuesta": "Condensación",
        "t17correcta": "3",
      },
      {
        "t13respuesta": "Estado sólido del agua",
        "t17correcta": "4",
      },
    ],
    "preguntas": [
      {
        "t11pregunta": "<img src='GI4E_B01_R15_01.png'>"
      },
      {
        "t11pregunta": "<img src='GI4E_B01_R15_02.png'>"
      },
      {
        "t11pregunta": "<img src='GI4E_B01_R15_03.png'>"
      },
      {
        "t11pregunta": "<img src='GI4E_B01_R15_04.png'>"
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "12",
      "t11pregunta": "Relaciona las columnas según corresponda.",
      "altoImagen": "100px",
      "anchoImagen": "200px"
    }
  },
  //17
  {
    "respuestas": [
      {
        "t13respuesta": "Agua oceánica",
        "t17correcta": "0",
      },
      {
        "t13respuesta": "Agua continental",
        "t17correcta": "1",
      },

    ], "preguntas": [
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Golfo de México ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Océano Atlántico ",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Río Bravo",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Lago de Chapala ",
        "correcta": "1",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Océano Pacífico",
        "correcta": "0",
      },
      {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Río Pánuco ",
        "correcta": "1",
      },
      
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "13",
      "t11pregunta": "<p>Elige agua oceánica o agua continental según corresponda. </p>",
      "t11instruccion": "",
      "descripcion": "Reactivo",
      "variante": "editable",
      "anchoColumnaPreguntas": 50,
      "evaluable": true
    }
  },
];
