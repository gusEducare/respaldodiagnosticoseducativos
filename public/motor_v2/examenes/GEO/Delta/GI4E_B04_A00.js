json = [
    {
        "respuestas": [
            {
                "t13respuesta": "...elementos de la naturaleza que abundan en ella y pueden ser utilizados por el hombre para satisfacer sus necesidades.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "…generar riqueza económica, aunque si se sobreexplotan pueden agotarse.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...las actividades como la ganadería, agricultura, minería, etcétera.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...la producción de bienes.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...las actividades que brindan servicios.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...la producción de alimentos para la población.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "...la agricultura, tanto por la manera en que debe alimentarse el ganado, como por el hecho de que también es parte del consumo alimenticio.",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "...maderables, de los cuales se fabrican muebles o hasta papel; o recursos no maderables, como resinas, hule, anteriormente chicle.",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Los recursos naturales son..."
            },
            {
                "t11pregunta": "A partir de los recursos naturales se puede..."
            },
            {
                "t11pregunta": "Actividades económicas se refiere a..."
            },
            {
                "t11pregunta": "Las actividades primarias y secundarias se refieren a..."
            },
            {
                "t11pregunta": "Las actividades terciarias se refieren a..."
            },
            {
                "t11pregunta": "Los espacios agrícolas son fundamentales para..."
            },
            {
                "t11pregunta": "La actividad ganadera está relacionada estrechamente con..."
            },
            {
                "t11pregunta": "Los espacios forestales son importantes pues a partir de ellos se obtienen recursos..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los espacios agrícolas son más eficientes en zonas áridas y no en áreas templadas, debido a la concentración de metales en sus suelos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la zona del Bajío existe una importante actividad agrícola, debido al clima y a los minerales del suelo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las zonas áridas existe la ganadería extensiva, en las templadas una producción menor y en zonas cálidas como Coatzacoalcos y Veracruz no existe ganadería en absoluto.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La presencia de fauna marina se ve favorecida por las corrientes marinas, como la del Golfo, que es cálida, y la de California que es fría. Además, también le favorece la extensión litoral de aproximadamente 11 122 km.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las entidades federativas de México con mayor cantidad de recursos forestales son Michoacán, Jalisco y el Estado de México.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la industria forestal se da la explotación de maderas finas utilizadas en la industria mueblera, extracción de goma, petróleo y combustibles fósiles.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : false
        }        
    },
   //3
   {
    "respuestas": [
        {
            "t13respuesta": "Minerales",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Metálicos",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "No metálicos",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "Energéticos",
            "t17correcta": "3"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Estos no forman rocas, pero cuando se extraen salen unidos a ellas. Son brillantes y buenos conductores de calor y electricidad.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Se utilizan mayormente como fuentes energéticas en el desarrollo industrial.",
            "correcta"  : "3"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Son materiales naturales formados por un solo componente que se encuentran en la corteza de la tierra.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Son compuestos que no brillan, frágiles y además no conducen electricidad.",
            "correcta"  : "2"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada recurso: mineral, metálico, no metálico o energético.",
        "descripcion": "",   
        "variante": "editable",
        "anchoColumnaPreguntas": 30,
        "evaluable"  : false
    }        
},
//4
{  
    "respuestas":[
       {  
          "t13respuesta": "Zacatecas",
          "t17correcta": "1",
          "numeroPregunta":"0"
       },
       {  
          "t13respuesta": "Chihuahua",
          "t17correcta": "1",
          "numeroPregunta":"0"
       },
       {  
          "t13respuesta": "San Luis Potosí",
          "t17correcta": "1",
          "numeroPregunta":"0"
       }, 
       {  
          "t13respuesta": "Morelos",
          "t17correcta": "0",
          "numeroPregunta":"0"
       }, 
       {  
          "t13respuesta": "Chiapas",
          "t17correcta": "0",
          "numeroPregunta":"0"
       },
       {  
          "t13respuesta": "Plata",
          "t17correcta": "1",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Fluorita",
          "t17correcta": "1",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Yeso",
          "t17correcta": "1",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Oro",
          "t17correcta": "0",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Niquel",
          "t17correcta": "0",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "La industria",
          "t17correcta": "1",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "El transporte",
          "t17correcta": "1",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "La calefacción",
          "t17correcta": "0",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "La manufactura de papel",
          "t17correcta": "0",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "La venta internacional",
          "t17correcta": "0",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "San Luis Potosí",
          "t17correcta": "1",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "Puebla",
          "t17correcta": "1",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "Tamaulipas",
          "t17correcta": "1",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "Jalisco",
          "t17correcta": "0",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "California",
          "t17correcta": "0",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "La transformación de materias primas",
          "t17correcta": "1",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "La elaboración de bienes de consumo",
          "t17correcta": "1",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "Creación de herramientas para el desarrollo de la industria",
          "t17correcta": "1",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "Utilización de calefacción en la nave industrial",
          "t17correcta": "0",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "Extracción de materias primas",
          "t17correcta": "0",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "Coahuila",
          "t17correcta": "1",
          "numeroPregunta":"5"
       },
       {  
          "t13respuesta": "Puebla",
          "t17correcta": "1",
          "numeroPregunta":"5"
       },
       {  
          "t13respuesta": "Guanajuato",
          "t17correcta": "1",
          "numeroPregunta":"5"
       },
       {  
          "t13respuesta": "Estado de México",
          "t17correcta": "0",
          "numeroPregunta":"5"
       },
       {  
          "t13respuesta": "Tlaxcala",
          "t17correcta": "0",
          "numeroPregunta":"5"
       }
    ],
    "pregunta":{  
       "c03id_tipo_pregunta":"2",
       "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br>¿Cuáles son algunos de los estados en México con mayor producción minera?",
                       "¿Cuáles son algunos de los minerales que produce México?",
                       "El recurso energético más explotado en México es el petróleo, que es utilizado para:",
                       "¿Cuáles son algunos de los estados en México productores de gas natural?",
                       "Los recursos energéticos son empleados en la industria para:",
                       "¿Cuáles son algunos de los estados de la República sede de la industria manufacturera?",],
       "preguntasMultiples": true
    }
 },
 //5
 {  
    "respuestas":[
       {  
          "t13respuesta": "Se encargan de transportar y distribuir cada uno de los productos y materias que después serán consumidos por nosotros.",
          "t17correcta": "1",
          "numeroPregunta":"0"
       },
       {  
          "t13respuesta": "Se encargan de producir materiales y productos para el consumo humano, como los enlatados, el papel, los textiles, etcétera.",
          "t17correcta": "0",
          "numeroPregunta":"0"
       }, 
       {  
          "t13respuesta": "Se encargan de explotar el espacio geográfico para obtener las materias primas, como madera, oro, petróleo o cereales.",
          "t17correcta": "0",
          "numeroPregunta":"0"
       },
       {  
        "t13respuesta": "Son la mano de obra que trabaja en condiciones infrahumanas y gracias a ellos obtenemos muchos de los productos de uso diario como ropa.",
        "t17correcta": "0",
        "numeroPregunta":"0"
       },
       {  
          "t13respuesta": "Externo e interno.",
          "t17correcta": "1",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Local, nacional e internacional.",
          "t17correcta": "0",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Externo, local, interno e industrial.",
          "t17correcta": "0",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Libre mercado.",
          "t17correcta": "0",
          "numeroPregunta":"1"
       },
       {  
          "t13respuesta": "Comercio externo",
          "t17correcta": "1",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "Comercio mercantil",
          "t17correcta": "0",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "Comercio interno",
          "t17correcta": "0",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "Comercio simple",
          "t17correcta": "0",
          "numeroPregunta":"2"
       },
       {  
          "t13respuesta": "Importación, cuando los productos son de procedencia extranjera; exportación, cuando los productos nacionales se venden al extranjero.",
          "t17correcta": "1",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "Inmigración, cuando el producto se va a otro lugar; emigración, cuando el vendedor es de otro lugar.",
          "t17correcta": "0",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "Explotación, cuando se trabaja más de ocho horas seguidas y sin buenas condiciones laborales; nepotismo; cuando las personas consiguen un buen puesto de trabajo por sus relaciones e influencias y no por sus capacidades.",
          "t17correcta": "0",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "Exportación, cuando los productos son de procedencia extranjera; importación, cuando los productos nacionales se venden al extranjero.",
          "t17correcta": "0",
          "numeroPregunta":"3"
       },
       {  
          "t13respuesta": "Sí, porque es un conjunto de servicios que se brindan a las personas y no depende de una materia prima.",
          "t17correcta": "1",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "No, porque se utilizan muchas materias primas para satisfacer al turista.",
          "t17correcta": "0",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "No sé, el turismo no fue mencionado en mi libro.",
          "t17correcta": "0",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "El turismo no es un servicio es un derecho de todo habitante del planeta.",
          "t17correcta": "0",
          "numeroPregunta":"4"
       },
       {  
          "t13respuesta": "Es una actividad económica que aprovecha los recursos turísticos, naturales o culturales. Puede afectar el ambiente.",
          "t17correcta": "1",
          "numeroPregunta":"5"
       },
       {  
          "t13respuesta": "Es un concepto económico para cobrar a las personas por visitar lugares no comunes. Puede resultar costoso.",
          "t17correcta": "0",
          "numeroPregunta":"5"
       },
       {  
          "t13respuesta": "Es una actividad humanista practicada por las personas de clase social alta. Puede resultar problemático.",
          "t17correcta": "0",
          "numeroPregunta":"5"
       },
       {  
          "t13respuesta": "Es un concepto económico empleado para promover el desarrollo de una nación. Puede resultar contraproducente.",
          "t17correcta": "0",
          "numeroPregunta":"5"
       }
    ],
    "pregunta":{  
       "c03id_tipo_pregunta":"1",
       "t11pregunta": ["Selecciona la respuesta correcta. <br>¿Qué son las actividades terciarias?",
                       "¿Cuántos tipos de comercio existen?",
                       "Este proceso se da cuando vendemos o compramos productos a otros países:",
                       "En el caso del comercio externo existen dos subprocesos dependiendo del origen y el destino de la mercancía vendida, ¿cuáles son?",
                       "¿Puede el turismo ser considerado una actividad terciaria?",
                       "¿Qué es el turismo?",],
       "preguntasMultiples": true
    }
 },
 //6
 {
    "respuestas": [
        {
            "t13respuesta": "...el desarrollo de todas las actividades económicas de México, es decir, que cuente con carreteras, vías férreas, puertos, aeropuertos, etc.; que conecten al país.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "...de carreteras, de las cuales se dividen  a su vez en carreteras principales, de cuota y caminos secundarios.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "...son utilizados como transporte de carga y comunican a  las poblaciones más importantes.",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "...están dedicados a actividades como exportación e importación de mercancías, turismo, pesca y petrolera.",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "...se encuentran el de la Ciudad de México, Cancún, Guadalajara, Monterrey y Tijuana.",
            "t17correcta": "5"
        }
    ],
    "preguntas": [
        {
            "t11pregunta": "Contar con un sistema de comunicación en tres zonas del país a nivel interno y externo es esencial para..."
        },
        {
            "t11pregunta": "México cuenta con 355 796 km..."
        },
        {
            "t11pregunta": "Actualmente en México los ferrocarriles..."
        },
        {
            "t11pregunta": "Los puertos son una parte importante de la infraestructura del país..."
        },
        {
            "t11pregunta": "En México existen 95 aeropuertos, 63 son internacionales, entre los más importantes..."
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta":"Relaciona las columnas según corresponda."
    }
},
//7
{
    "respuestas": [
        {
            "t13respuesta": "F",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "V",
            "t17correcta": "1"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En México las actividades económicas realizadas son minería, agricultura, ganadería, pesca y actividades forestales.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los estados de Baja California Norte y Sur se dedican principalmente a la minería, agricultura, pesca, comercio y turismo.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En el Estado de México se llevan a cabo actividades como embarco de mercancía, pesca, actividades forestales y agrícolas.",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El estado de San Luis Potosí solo se dedica a la agricultura y la manufactura.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El estado de Querétaro lleva a cabo actividades de Ganadería, manufactura, comercio y turismo.",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El estado de Hidalgo se dedica principalmente a la pesca.",
            "correcta"  : "0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
        "descripcion": "",   
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable"  : false
    }        
},
//8
{
    "respuestas": [
        {
            "t13respuesta": "...4.2% del producto interno bruto.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "..la industria y la manufactura en las actividades secundarias.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "...las actividades terciarias, que aportan un 62.5% del producto interno bruto.",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "...Campeche con un 10.5% de las actividades.",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "...Jalisco con un 12.6% de las actividades.",
            "t17correcta": "5"
        },
        {
            "t13respuesta": "...la CDMX (anteriormente llamado DF) con un 23.5% de las actividades.",
            "t17correcta": "6"
        }
    ],
    "preguntas": [
        {
            "t11pregunta": "En México las actividades primarias generan alrededor de..."
        },
        {
            "t11pregunta": "Aproximadamente ⅓ del total del PIB en México es generado por..."
        },
        {
            "t11pregunta": "La mayor parte del PIB en México es producido por..."
        },
        {
            "t11pregunta": "En México el estado con mayor producción de actividades primarias es..."
        },
        {
            "t11pregunta": "El estado con la mayor producción de actividades secundarias es..."
        },
        {
            "t11pregunta": "La entidad federativa con mayor producción de actividades terciarias es..."
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta":"Relaciona las columnas según corresponda."
    }
}

];
