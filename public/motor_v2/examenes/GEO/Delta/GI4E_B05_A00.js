json = [
    {  
        "respuestas":[  
            {
                "t13respuesta":     "La capacidad que tiene una persona para desarrollar sus valores, aspiraciones y expectativas personales, en un ambiente sano y en condiciones materiales favorables.",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "La capacidad que tiene una persona de desarrollarse plenamente en un ambiente estresante y plano, además de la capacidad de hacer frente al temor.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "La capacidad que tiene una persona de desenvolverse laboralmente en cualquier parte del mundo.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "La capacidad que se tiene de mantener una familia en condiciones favorables y económicamente estable.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Determinar el nivel de calidad de vida de una población.",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Ver la capacidad de esa persona para sortear dificultades ante la vida.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Medir la calidad de vida familiar y laboral, con estadística.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Encontrar una relación entre insatisfacción laboral y la felicidad.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Subjetivos de la calidad de vida.",
                "t17correcta":"1",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Objetivos de la calidad de vida.",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Personales de la calidad de vida.",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Comunes de la calidad de vida.",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Los aspectos objetivos que ayudan a establecer la calidad de vida de una población.",
                "t17correcta":"1",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Los aspectos personales que ayudan a establecer la calidad de vida de una población.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Los aspectos subjetivos que ayudan a establecer la calidad de vida de una población.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Los aspectos comunes que ayudan a establecer la calidad de vida de una población.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Sentido de pertenencia.",
                "t17correcta":"1",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Sentido de seguridad.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Participación en la toma de decisiones.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Educación.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Los ingresos.",
                "t17correcta":"1",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Los préstamos.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Los beneficios.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Las prestaciones.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "La posibilidad de obtener atención en los servicios médicos y medicamentos, además de la esperanza de vida.",
                "t17correcta":"1",
              "numeroPregunta":"6"
             },
             {
                "t13respuesta":     "La posibilidad de caer enfermo y la capacidad de recuperarse de tal percance.",
                "t17correcta":"0",
              "numeroPregunta":"6"
             },
             {
                "t13respuesta":     "La posibilidad de adquirir una enfermedad y la capacidad de contagio de la misma.",
                "t17correcta":"0",
              "numeroPregunta":"6"
             },
             {
                "t13respuesta":     "La posibilidad de ingresar a los hospitales y adquirir medicinas, además del número de muertes infantiles y de adultos que hay al año.",
                "t17correcta":"0",
              "numeroPregunta":"6"
             }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Selecciona la respuesta correcta:<br><br>La calidad de vida se refiere al nivel de bienestar de las personas, por lo tanto, una buena calidad de vida es:",
            "A partir de datos estadísticos, conocidos como aspectos objetivos, y según algunos aspectos subjetivos, que parten de la apreciación personal de un humano, es posible:",
            "La participación en la toma de decisiones, el sentido de seguridad y el sentido de pertenencia, son algunos de los aspectos:",
            "Los ingresos, salud, educación, servicios básicos y calidad de ambiente, son:",
            "Cuando las personas se sienten aceptadas y valoradas en la comunidad adquieren confianza y seguridad, esto es:",
            " ...se refieren al dinero que se percibe por un trabajo hecho. Para tener una buena calidad de vida estos deben ser suficientes para cubrir las necesidades de quien los percibe y su familia.",
            "Para medir la calidad de vida con base en la salud se toma en cuenta:"],
           "preguntasMultiples": true
        }
     },
     //2
     {
        "respuestas": [
            {
                "t13respuesta": "...objetivos y subjetivos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...el sentido de seguridad, el sentido de pertenencia, la participación en la toma de decisiones y la satisfacción con la vida que se tiene.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...los ingresos, educación, salud, servicios básicos y calidad del ambiente.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...una menor calidad de vida, esto por la falta de empleos, las escasas oportunidades educativas y la falta de servicios.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...problemas como la contaminación, la inseguridad y la falta de equilibrio entre el trabajo y la vida personal.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...la esperanza de vida, la tasa de alfabetización de adultos y el PIB per cápita, son algunos aspectos que se toman en cuenta.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "...tiene el mayor PIB per cápita, contando con 23 029 dólares al año.",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "...tiene mayor tasa de alfabetización en adultos (98.27%) y esperanza de vida al nacer con 75.61 años.",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "La calidad de vida se mide por aspectos..."
            },
            {
                "t11pregunta": "Algunos de los aspectos subjetivos son..."
            },
            {
                "t11pregunta": "Algunos de los aspectos objetivos son..."
            },
            {
                "t11pregunta": "Por lo general las poblaciones rurales tienen..."
            },
            {
                "t11pregunta": "Las ciudades tienen una mejor calidad de vida aunque en ellas encontramos..."
            },
            {
                "t11pregunta": "En México cada estado tiene indicadores para evaluar la calidad de vida de los ciudadanos..."
            },
            {
                "t11pregunta": "Al ser la capital del país, la Ciudad de México..."
            },
            {
                "t11pregunta": "Baja California es el estado que..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    //3
    {  
        "respuestas":[  
            {
                "t13respuesta":     "Contaminación del aire.",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Contaminación del agua.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Contaminación del suelo.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Contaminación del suelo.",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Contaminación del agua.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Contaminación del aire.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Contaminación del agua.",
                "t17correcta":"1",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Contaminación del aire.",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Contaminación del suelo.",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Contaminación del suelo.",
                "t17correcta":"1",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Contaminación del aire.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Contaminación del agua.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Contaminación del agua.",
                "t17correcta":"1",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Contaminación del suelo.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Contaminación del aire.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Contaminación del aire.",
                "t17correcta":"1",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Contaminación del agua.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Contaminación del suelo.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Selecciona la respuesta correcta:<br><br>Es la presencia de humo, gases, polvo, cenizas, bacterias y residuos, que al ponerse en contacto con la atmósfera alteran su composición.",
            "Los principales problemas que acarrea esta contaminación son la erosión, la desertificación, la compactación y la pérdida de las zonas fértiles.",
            "Se presenta al introducir basura, desechos orgánicos y químicos a cuerpos de agua, volviéndose de mala calidad y  no aptos para el consumo. Puede provocar problemas de salud en humanos, animales y plantas, además de que aumenta la escasez.",
            "Consiste en el desequilibrio químico, físico o biológico, debido a la infiltración de sustancias químicas como los fertilizantes y a la erosión provocada por la tala indiscriminada.",
            "En México los espacios que más sufren de este tipo de contaminación son el lago de Chapala y el Río Lerma-Santiago.",
            "Originada principalmente por humo y gases de automóviles y fábricas, la producción de energía eléctrica, los incendios forestales y la deforestación. La CDMX, es la entidad en el país con más problemas por este tipo de contaminación.",],
           "preguntasMultiples": true
        }
     },
     //Pendiente
     {  
        "respuestas":[  
            {
                "t13respuesta":     "“Toda persona tiene derecho a un ambiente sano para su desarrollo y bienestar. El Estado garantizará el respeto a este derecho. El daño y deterioro ambiental generará responsabilidad para quien lo provoque en términos de lo dispuesto por la ley”.",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "“Toda persona tiene derecho a ser libre para disfrutar de su vida en un contexto sano y sin problemas”.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "“Todo humano perteneciente a la República Mexicana goza del fundamental derecho a vivir una vida plena en un ambiente controlado y que no presente problemas”.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "“Es un derecho fundamental para todo mexicano el poder disfrutar de actividades laborales y de esparcimiento en un ambiente libre de contaminación, que será garantizado por el Estado. Se sancionará a quien modifique, altere o dañe el paisaje y el ambiente”.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Ley General de Equilibrio Ecológico y la Protección del Ambiente, que regula las acciones en materia de protección del ambiente, conservación y manejo de los recursos naturales.",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Ley General del Balance Ecológico y la Protección del País, que regula las acciones en materia de protección del ambiente, conservación y manejo de los recursos naturales.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Ley General de Equilibrio Ambiental y la Protección del Estado, que regula las acciones en materia de protección del ambiente, conservación y manejo de los recursos naturales.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Ley Federal de Balance Ambiental y la Protección del Ambiente, que regula las acciones en materia de protección del ambiente, conservación y manejo de los recursos naturales.",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "Secretaría de Medio Ambiente y Recursos Naturales (SEMARNAT).",
                "t17correcta":"1",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Secretaría de Comunicaciones y Transportes (SCT).",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Secretaría Nacional de Ambiente y Ecología (SNAE).",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Secretaría Federal de Recursos Naturales, Ambiente y Ecología (SEFRENAE).",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "Conservar la vegetación y la fauna silvestre, preservar los paisajes naturales, crear oportunidades de recreación, generar espacios para la educación ambiental y sirven como laboratorios para la investigación científica.",
                "t17correcta":"1",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Promover ambientes intactos por el hombre, terrenos con flora y fauna oriunda sin peligros latentes, espacios de turismo y recreación a los que todos pueden asistir.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "La naturalización de ambientes y lugares que pueden ser útiles para la  extracción de recursos naturales, la caza deportiva y la exposición de especies únicas al público en general.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Modificar áreas naturales para la utilización humana sin afectar la flora y fauna del lugar, es la famosa arquitectura ecológica que evita dañar el ambiente.",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "Archipiélago de Cortés.",
                "t17correcta":"1",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Mapimí.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Valle de los Cirios.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "El Vizcaíno.",
                "t17correcta":"0",
              "numeroPregunta":"4"
             },
             {
                "t13respuesta":     "Proteger, preservar y restaurar zonas silvestres donde existe flora y fauna que puede estar en peligro de extinción y preservar la diversidad de especies.",
                "t17correcta":"1",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Promover, restaurar y capitalizar zonas de interés turístico donde se puedan apreciar otros tipos de flora y fauna.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Proteger, preservar y restaurar zonas urbanas donde existe flora y fauna en peligro de extinción y preservar la diversidad de especies.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
             {
                "t13respuesta":     "Promover, restaurar y capitalizar zonas de interés científico donde se puedan apreciar otros tipos de fenómenos de interés académico.",
                "t17correcta":"0",
              "numeroPregunta":"5"
             },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Selecciona la respuesta correcta:<br><br>¿Qué indica el artículo 4° de la Constitución Mexicana?",
            "Para asegurar que se cumpla lo establecido en la Constitución, referente al artículo 4°, en 1988 se promulgó la:",
            "¿Cuál de la siguientes secretarías es la que se encarga de vigilar que se cumpla la ley anterior?",
            "¿Cuál es la función de las Áreas Naturales Protegidas?",
            "¿Cuál de las siguientes no es una Reserva de la biósfera: Parque nacional, Área de protección de flora y fauna o Área de protección de los recursos naturales de México?",
            "La creación de áreas naturales se hace con la finalidad de:",],
           "preguntasMultiples": true
        }
     },
     //4
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los desastres pueden ser naturales o causados por el hombre.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los desastres naturales pueden originarse por  derrames de sustancias tóxicas, explosiones de fábricas, derrumbes en minas, etcétera.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los temblores, inundaciones, huracanes, sequías, deslaves y erupciones volcánicas son desastres naturales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Uno de los desastres más trágicos ocurridos en México en el último siglo fue el terremoto del 19 de septiembre de 1985.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Otro desastre reciente en México puede ser la explosión de uno de los reactores de la planta de Polietileno en la petroquímica La Cangrejera en Coatzacoalcos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los riesgos y desastres naturales son completamente predecibles para cualquier caso.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para prevenir los desastres naturales debemos primeramente saber cuáles son las amenazas y riesgos y posteriormente saber qué hacer, es decir, tener un plan de contingencia.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : false
        }        
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "sismo del 85",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Sumatra-Andamán",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "desastres naturales",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "placas tectónicas",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "responsabilidad",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "desastres",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "derrames petroleros",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "incendios forestales",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "prevención de accidentes",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "respeto",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "interés",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "plan de contingencia",
                "t17correcta": "12"
            },
            {
                "t13respuesta": "simulacros",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "alertas",
                "t17correcta": "14"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Nuestra nave flotante en el espacio, llamada Tierra, no está exenta de tragedias. En ella pueden ocurrir todo tipo de percances naturales como el <\/p>"
            },
            {
                "t11pregunta": "<p> en la Ciudad de México o el terremoto de <\/p>"
            },
            {
                "t11pregunta": "<p> en 2004 que fue devastador. Los <\/p>"
            },
            {
                "t11pregunta": "<p> son producto de los movimientos y transformaciones de nuestro planeta; el desplazamiento de las <\/p>"
            },
            {
                "t11pregunta": "<p>, la erupción de un volcán o incluso un maremoto son solo situaciones que dependen de la naturaleza, sin embargo, toman una gran relevancia cuando afectan a los humanos, pues ¿si no hubiese existido la Ciudad de México en 1985, a quién habría afectado el terremoto? Si nadie lo hubiese sentido, habría sido un desastre, ¿para quién?<br>Al contrario de los desastres naturales, la poca <\/p>"
            },
            {
                "t11pregunta": "<p> de los hombres sí puede causar <\/p>"
            },
            {
                "t11pregunta": "<p> que afecten no solo al hombre sino al lugar en donde habita, e incluso más allá. Los <\/p>"
            },
            {
                "t11pregunta": "<p>, los <\/p>"
            },
            {
                "t11pregunta": "<p>, los accidentes de plantas nucleares o el mismo desastre de Bhopal no son producto de movimientos o transformaciones de la Tierra, sino producto de la poca conciencia, la nula empatía y el excesivo egoísmo de los hombres. Siendo más graves los hechos de este último tipo.<br>En todo caso, la <\/p>"
            },
            {
                "t11pregunta": "<p> debería iniciarse al crear una conciencia de <\/p>"
            },
            {
                "t11pregunta": "<p> e <\/p>"
            },
            {
                "t11pregunta": "<p> por el mundo en el que vivimos, después educar a la población para que comprendan que los llamados desastres naturales pueden ocurrir en cualquier momento y, finalmente, idear un <\/p>"
            },
            {
                "t11pregunta": "<p> para minimizar enormemente los estragos que estos ocasionen. Los <\/p>"
            },
            {
                "t11pregunta": "<p> de sismos, las <\/p>"
            },
            {
                "t11pregunta": "<p> de erupción y las acciones posteriores que se toman cuando ocurren estas situaciones son nuestra única defensa ante la naturaleza, así que debemos pensarlo bien y prepararnos para estas contingencias.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el siguiente p&aacute;rrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
];
