json = [
    {
        //1 Verdadero o falso
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La distribución poblacional en México es homogénea.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El INEGI se encarga de contabilizar a la población de México.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las políticas gubernamentales se elaboran de acuerdo a la composición poblacional realizada  por la CONAPO.",
                "correcta"  : "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La composición de la población se representa en gráficas.",
                "correcta"  : "1"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La densidad poblacional es el número de habitante respecto a sus creencias religiosas y culturales.",
                "correcta"  : "0"
            } ,
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El sexo es una variable importante de la composición poblacional.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><b>Selecciona verdadero (V) o falso (F) según corresponda:</b><\/p>",
            "t11instruccion": "¿Sabías que nuestro país es el onceavo más poblado del mundo? ¡Si!, y es precisamente el Instituto Nacional de Estadística y Geografía (INEGI) el encargado de contar a los habitantes de nuestro país. Pero, te has preguntado ¿para qué nos sirve saber esto? bueno pues las políticas gubernamentales se elaboran de acuerdo a estos datos y las necesidades de la población. Por ejemplo: el estado más poblado es  el Estado de México con 15.18 millones habitantes y el menos poblado es Colima  con 650,555 habitantes. La composición de la población se mide con variables como el estado civil, el tamaño de la familia, las actividades económicas, la edad y la cantidad de hombres y mujeres que hay, es decir el sexo. Esto tiene repercusión sobre lo que requiere el país respecto a viviendas, hospitales, educación, etcétera. Por otro lado, la distribución de la población en México no es homogénea, hay lugares donde viven muchas personas y otros donde viven pocas, a esto se le llama densidad de población. Gráficamente, esto se representa  mediante pirámides que nos ayudan a entender la situación de la población en el pasado y pronosticar su comportamiento en el futuro.",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        },
    },
        //2 Opcion multiple
        {  
      "respuestas":[
         {  
            "t13respuesta": "Implementación de políticas gubernamentales para el aumento de las actividades industriales en las ciudades.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Implementación de tecnologías verdes para el campo.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Implementación de políticas gubernamentales de apoyo a la migración rural.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Ciudades grandes, con más de un millón de habitantes; ciudades medianas, entre cien mil y un millón; y ciudades pequeñas, entre quince mil y cien mil habitantes.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Ciudades grandes con más de 3 millones de habitantes, ciudades medianas con un millón de habitantes y ciudades pequeñas entre quince mil y cien mil habitantes.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Ciudades grandes con más de 2  millón de habitantes, ciudades medianas entre cien mil y dos millones de habitantes y ciudades pequeñas entre cincuenta mil y cien mil habitantes.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Ventajas: Servicios públicos, servicios de salud, educación especializada.<br>Desventajas: contaminación del aire, destrucción del medio ambiente, generación de grandes cantidades de basura.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Ventajas: Oferta laboral diversa, productos agrícolas del lugar, servicios de salud.<br> Desventajas:  contaminación, carencia de servicios financieros,  tránsito vehicular",
            "t17correcta": "0",
            "numeroPregunta":"2"
         } ,
         {  
            "t13respuesta": "Ventajas: Servicios recreativos, calidad del aire, flujo del tránsito vehicular.<br>  Desventajas: generación de mucha basura, destrucción del medio ambiente, carencia de ofertas laborales.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Ausencia de políticas gubernamentales de desarrollo rural y actividades agropecuarias.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Falta de acceso a los servicios básicos de salud y carencia de apoyo comunitario para las actividades agropecuarias.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         } ,
         {  
            "t13respuesta": " Falta de ganado y espacios agrícolas en las comunidades.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Marginación, pobreza, reduce la calidad de los servicios públicos.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Fomenta la producción de autoconsumo, fomenta el crecimiento económico y eleva los costos del transporte.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         } ,
         {  
            "t13respuesta": "Aumento en los costos de transporte, carencia de alimentos, crecimiento económico.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<b>Selecciona la respuesta correcta.</b> <br> ¿A qué se debió el aumento de población urbana en México en 1940? ","¿Cómo se jerarquiza el sistema de ciudades en México?","¿Cuáles son las ventajas y desventajas más características de una ciudad?","¿A qué se debe el descenso de población rural a lo largo de los años en México?","¿Qué consecuencias tiene la elevada dispersión de la población rural?"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },
   
   //3 relaciona columnas
    {
        "respuestas": [
            {
                "t13respuesta": " se generan las actividades económicas variadas, el medio de trabajo es artificial",
                
                "t17correcta": "1"
            },
            {
                "t13respuesta": "tratan con seres vivos y se relacionan con la naturaleza dependiendo de esta.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Provienen las materias primas para elaborar productos de",
                
                "t17correcta": "3"
            },
            {
                "t13respuesta": "se caracterizan por tener grandes concentraciones de población",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Inmigrantes de diferentes lugares y la mezcla de sus costumbres y tradiciones",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Desarrollo de las personas y una  salud favorable",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Del Trabajo de ciudad"
            },
            {
                "t11pregunta": "En el trabajo de campo"
            },
            {
                "t11pregunta": "De las poblaciones rurales"
            },
            {
                "t11pregunta": "Las urbes y ciudades"
            },
            {
                "t11pregunta": "Los grupos de personas de las ciudades están conformados por"
            },
            {
                "t11pregunta": "La falta de aglomeraciones produce"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "<b>Relaciona las columnas según corresponda</b>"
             
            
        }
    },
    
    //4 falso o verdadero
    
    {
        
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La migración es únicamente voluntaria",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un emigrante es una persona que decide dejar su lugar de residencia para ir a otro",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La migración sucede cuando una persona reside en su país de origen",
                "correcta"  : "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los conflictos armados ocasionan una migración voluntaria",
                "correcta"  : "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La migración tiene categorías de interna y externa dentro de un mismo país ",
                "correcta"  : "1"
            } ,
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p><b>Selecciona verdadero (V) o falso (F) según corresponda:</b><\/p>",
            "t11instruccion": "Las ciudades y los lugares en donde vivimos están conformados por una diversidad de personas. ¿Te has preguntado si han vivido siempre ahí?, ¿de dónde vienen y a dónde van? La migración sucede cuando las personas se mudan del lugar donde viven hacia otra parte, dentro de su país (migración interna) o fuera de él (migración externa). Entonces, un emigrante es aquel que decide dejar el lugar en donde vive para irse a otro. Este fenómeno se da porque las personas buscan mejores oportunidades de vida, empleo, estudio y salud (migración voluntaria). Cuando las personas son forzadas a huir debido a algún conflicto armado o inseguridad en su lugar de origen, se habla de migración involuntaria.",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        },
    },
    
    //5 opcion multiple
    
        {  
      "respuestas":[
         {  
            "t13respuesta": "Interna y externa",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Voluntaria e involuntaria",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Temporal y permanente",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Temporal y permanente",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Permanente y continua",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Continua y Temporal",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Consecuencias políticas, económicas y sociales tanto en lugares de origen como  de destino,",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Solo hay consecuencias en lo social y cultural, tanto en el lugar de origen como de destino",
            "t17correcta": "0",
            "numeroPregunta":"2"
         } ,
         {  
            "t13respuesta": "Lugares de origen, consecuencias sociales y políticas pero no económicas.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Migración ilegal",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Migración forzada",
            "t17correcta": "0",
            "numeroPregunta":"3"
         } ,
         {  
            "t13respuesta": "Migración involuntaria",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Componentes sociales",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Componentes culturales",
            "t17correcta": "0",
            "numeroPregunta":"4"
         } ,
         {  
            "t13respuesta": "Componentes políticos",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Componente cultural",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Componente económico",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Componente social",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }, 
         {  
            "t13respuesta": "Componente político",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Componente económico ",
            "t17correcta": "0",
            "numeroPregunta":"6"
         }, 
         {  
            "t13respuesta": "Componente social",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<b>Selecciona la respuesta correcta</b> <br> De acuerdo al destino, ¿Cómo se clasifica la migración?","Según el tiempo de permanencia, la migración se clasifica en:","¿Cuáles son las consecuencias de que una persona emigre?","¿Cómo se le llama al acto en donde las personas no cuentan con autorización del gobierno del lugar de destino?","Se relacionan directamente con la composición de la población","Los inmigrantes se adaptan a otras costumbres y formas de vida, que se mezclan con sus propias tradiciones.","La creación de acciones más restrictivas para frenar la migración o el establecimiento de acuerdos migratorios responde a"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },
   
   //6 opcion multiple
   
    {  
      "respuestas":[
         {  
            "t13respuesta": "Son el grupo más numeroso del país: descendientes de la unión entre indígenas e inmigrantes europeos durante la colonia.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Son el grupo más numeroso del país: descendientes de la unión entre españoles y criollos durante la colonia",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Son el segundo grupo más numeroso del país: descendientes de la unión entre indígenas e inmigrantes europeos durante la colonia.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Son el segundo grupo más numeroso del país: descendientes de los pueblos originarios de México, conservan algunas de sus instituciones sociales, económicas y culturales.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Son el segundo menos numeroso del país: descendientes de los pueblos originarios de México, conservan su lengua únicamente.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         {  
            "t13respuesta": "Son el quinto grupo más numeroso del país: descendientes de los pueblos originarios de México, conservan algunas de sus instituciones sociales, económicas y culturales.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "7 millones 382 mil 785 personas y 72 grupos etnolingüísticos, se encuentran mayoritariamente en los estados del centro y del sur.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "1 millón 282 mil 785 personas y 10 grupos etnolingüísticos, se encuentran mayoritariamente en los estados del  norte y del sur.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         } ,
         {  
            "t13respuesta": "5 millones 382 mil 785 personas y 50 grupos etnolingüísticos, se encuentran mayoritariamente en los estados del centro y del sur",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Maya, Mixteco, Nahuatl, Zapoteco",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Triqui, Mazahua, Tarahumara, Maya",
            "t17correcta": "0",
            "numeroPregunta":"3"
         } ,
         {  
            "t13respuesta": "Otomí, Triqui, Mazateco, Zapoteco",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Españoles. italianos, judíos y libaneses",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Chinos, mazahuas, libaneses y españoles",
            "t17correcta": "0",
            "numeroPregunta":"4"
         } ,
         {  
            "t13respuesta": "Italianos, triquis, españoles y judíos",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "INEGI Y CONAPO",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "CONAPO y  SEDESU",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "INEGI Y SEDESU",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }, 
         {  
            "t13respuesta": "Vestimenta, comida, lengua, tradiciones, pensamiento y modo de vida.",
            "t17correcta": "1",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Población total, vestimenta y pensamiento.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         }, 
         {  
            "t13respuesta": " Modo de vida, número de hablantes, población total y vestimenta.",
            "t17correcta": "0",
            "numeroPregunta":"6"
         },
         {  
            "t13respuesta": "Comida e idioma",
            "t17correcta": "1",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Danzas, comida, rebozo",
            "t17correcta": "0",
            "numeroPregunta":"7"
         },
         {  
            "t13respuesta": "Huipil, rebozo, comida",
            "t17correcta": "0",
            "numeroPregunta":"7"
         }, 
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["<div  style='text-align:center;'><img style='height: 610px' src='GI4E_B03_A06.png'><br><p> Lenguas indígenas habladas en México </p></div><br> <b>Observa el mapa y selecciona la respuesta correcta.</b> <br> ¿Cuáles son las características de los mestizos?","Son características de los grupos indígenas","¿En dónde se concentran mayoritariamente los grupos indígenas mexicanos y cuanto es el total de su población?","¿Cuáles son las lenguas indígenas más habladas del país?","Son grupos minoritarios no indígenas que habitan nuestro país","¿Qué instituciones se encargan de contabilizar la población indígena en México?","¿De qué elementos está conformada la cosmovisión? ","Son aportes y manifestaciones que combinan las costumbres originarias y las extranjeras en México:"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },
   
    
];
