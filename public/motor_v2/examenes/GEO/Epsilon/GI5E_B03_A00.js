json=[
  //1
   {
      "respuestas":[
         {
            "t13respuesta":"China",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"India",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"Filipinas",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":"Ciudad del Vaticano",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Jamaica",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Mónaco",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":"Asia",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"América",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"Europa",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":"Oceanía",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta":"África",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
         {
            "t13respuesta":"Europa",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11instruccion":"El siguiente mapa está hecho de manera que las proporciones territoriales estén relacionadas al tamaño de su población y no a su extensión geográfica. Obsérvalo y selecciona la respuesta correcta. ",
        "t11pregunta": ["<b>Selecciona la respuesta correcta:</b><br></br><img src='GI5E_B03_A01.png' style=' margin-left:-27px;'/> <br></br>¿Cuál de los países en el mapa es el más poblado?",
                        "¿Cuál es el país con menos población a nivel mundial?",
                        "¿Cuál de los seis continentes mantiene el mayor número de habitantes?",
                        "¿Cual es el continente con una población fija con el menor número de habitantes?"],
        "preguntasMultiples": true

      }
   },
   //2
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La composición de la población de todo el mundo es igual, es decir, la misma cantidad de niños y adultos, hombres y mujeres.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Es posible medir la composición de la población a partir de un gráfico, como la pirámide poblacional.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A pesar de ser el continente más grande, Asia tiene la menor población continental a nivel mundial.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las zonas urbanas son ocupadas por la población porque en ellas  pueden saciar sus necesidades básicas",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se estima que en los próximos años disminuirá drásticamente la población mundial.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El aumento desmedido de la población provoca escasez de recursos, contaminación del ambiente y atención sanitaria y educativa deficiente.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<b><p>Elige  falso (F) o verdadero (V) según corresponda.</b><\/p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "la destrucción del medio ambiente que rodea las ciudades.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "se debe al crecimiento de las ciudades y la tala de árboles para la construcción.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "genera problemas sociales como la pobreza y el aumento del crimen por falta de oportunidades.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "hay una población existente de más de 2,500 habitantes.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "en las ciudades hay mejores servicios de salud, educación y oportunidades laborales.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Una de la implicaciones que trae el crecimiento urbano es..."
            },
            {
                "t11pregunta": "El aumento de la contaminación en el aire..."
            },
            {
                "t11pregunta": "La sobrepoblación de las urbes..."
            },
            {
                "t11pregunta": "Por definición, se considera que una población es urbana cuando..."
            },
            {
                "t11pregunta": "Las personas en el mundo prefieren la vida urbana debido a que..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p><b>Relaciona las columnas según corresponda.</b><\/p>"
        }
    },
    //4
    {
     "respuestas":[
        {
           "t13respuesta":"Es el desplazamiento geográfico (de manera temporal o definitiva) de individuos o grupos.",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"Cuando una persona, de manera definitiva, se mantiene lejos de su residencia.",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"Cuando una persona establece su residencia en el mismo lugar donde nació.",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":"El emigrante es el que deja un lugar, mientras que el inmigrante es quien llega a un lugar.",
           "t17correcta":"1",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"No hay diferencia, ambos son migrantes.",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"El emigrante es quien llega a un  lugar, mientras que el inmigrante deja un lugar.",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"Ambos dejan un lugar, pero el emigrante se dirige al norte y el inmigrante al sur.",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":"En países de bajo desarrollo, cuando las personas quieren obtener mejores oportunidades y calidad de vida.",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":"En países ricos, cuando sus habitantes buscan más comodidades.",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":"En países ricos, cuando existen crisis económicas.",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":"Cuando el número de personas que salen de un país es mayor de las que llegan.",
           "t17correcta":"1",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":"Cuando el número de la población aumenta drásticamente.",
           "t17correcta":"0",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":"Cuando el número de los habitantes disminuye drásticamente.",
           "t17correcta":"0",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":"Cuando el número de personas que entran a un país es mayor de las que salen.",
           "t17correcta":"0",
           "numeroPregunta":"3"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11pregunta":["<b>Selecciona la respuesta correcta.</b><br></br>¿Qué es la migración?",
                      "¿Cuales son las diferencias entre emigrante e inmigrante?",
                      "Normalmente, ¿dónde se da mayor migración?",
                      "¿Qué es la migración positiva?"],
        "preguntasMultiples": true
     }
  },
  //5
  {
      "respuestas": [
          {
              "t13respuesta": "social",
              "t17correcta": "1"
          },
          {
              "t13respuesta": " cultural",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "económica",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "política",
              "t17correcta": "4"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "Cuando la vida de las familias se ve afectada en el lugar de donde emigran se habla de una consecuencia..."
          },
          {
              "t11pregunta": "Cuando los migrantes llegan a otro lugar llevan consigo sus creencias. Esto genera una expansión de la tradición del lugar al que llegan y es una consecuencia..."
          },
          {
              "t11pregunta": "A los lugares de los que emigran normalmente llegan remesas y dinero que mejora la calidad de vida de quienes reciben este beneficio. Sin embargo, no hay muchas personas que trabajen en esa área, lo que genera un déficit laboral y es una consecuencia..."
          },
          {
              "t11pregunta": "Los países que reciben población migrante deberán implementar acciones para proteger la calidad de vida de sus habitantes, esto es una consecuencia..."
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta":"<b>Relaciona las columnas según corresponda.</b>"
      }
  },
  //6
  {
     "respuestas":[
        {
           "t13respuesta":     "Cristianismo",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "Islam",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "Budismo",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "Judaísmo",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "Hinduismo",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
      {
        "t13respuesta":     "Inglés",
        "t17correcta":"1",
        "numeroPregunta":"1"

      },
      {
        "t13respuesta":     "Francés",
        "t17correcta":"1",
        "numeroPregunta":"1"

      },
      {
        "t13respuesta":     "Español",
        "t17correcta":"1",
        "numeroPregunta":"1"

      },
      {
        "t13respuesta":     "Italiano",
        "t17correcta":"0",
        "numeroPregunta":"1"

      },
      {
        "t13respuesta":     "Japonés",
        "t17correcta":"0",
        "numeroPregunta":"1"

      },
        {
           "t13respuesta":     "La arquitectura",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "La gastronomía",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "Las tradiciones",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "La política",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "La ciencia",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "Conserva los recursos naturales.",
           "t17correcta":"1",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":     "La diversidad enriquece a las poblaciones.",
           "t17correcta":"1",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":     "Favorece el intercambio de conocimientos.",
           "t17correcta":"1",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":     "Genera racismo y xenofobia.",
           "t17correcta":"0",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":     "Eleva los índices de crimen y desplaza la identidad cultural.",
           "t17correcta":"0",
           "numeroPregunta":"3"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"2",
        "dosColumnas": false,
        "t11pregunta":["<b>Selecciona todas las respuestas correctas para cada pregunta.</b><br></br>¿Cuáles son las tres religiones con más seguidores en el mundo?",
                      "¿Cuáles son algunas de las lenguas más importantes del mundo por la cantidad de hablantes que tienen?",
                      "¿Cuáles son algunas de la manifestaciones culturales más importantes del mundo?",
                      "La diversidad cultural es importante debido a que: "],
        "preguntasMultiples": true
     }
  }
];
