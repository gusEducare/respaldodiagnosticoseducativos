json = [
    //1
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Factores objetivos de la calidad de vida.", 
        "t17correcta":"1",
        "numeroPregunta": "0"
        }, 
        { 
        "t13respuesta":"Factores subjetivos de la calidad de vida.", 
        "t17correcta":"0",
        "numeroPregunta": "0"
        }, 
        { 
        "t13respuesta":"Factores variables  del estilo de vida.", 
        "t17correcta":"0",
        "numeroPregunta": "0"
        }, 
        { 
        "t13respuesta":"Para medir el factor salud se utiliza la esperanza de vida, en el factor empleo se utiliza la tasa de empleo, en el factor educación el indicador es el promedio de la cantidad de años de estudio de una población determinada y en el factor ambiente se mide la calidad del ambiente.", 
        "t17correcta":"1",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"Para medir el factor empleo se utiliza la tasa de ingreso, en el factor salud se usa la calidad hospitalaria, en el factor educación es el promedio de la cantidad de años de estudio de una población determinada  y en el factor ambiente se mide únicamente el nivel de concentración de CO<sub>2</sub>.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"Para medir el factor medio ambiente se utiliza el nivel de concentración de CO<sub>2</sub>, para el factor empleo se usa la tasa de empleo, el factor educación utiliza la tasa escolar y el factor salud la esperanza de vida.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"La esperanza de vida, el PIB per cápita, el porcentaje de alfabetización y la emisión de gases invernadero.", 
        "t17correcta":"1",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Calidad hospitalaria, el PIB per cápita, concentración de CO<sub>2</sub> y la tasa de ingreso.", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Tasa de calificaciones educativas, calidad hospitalario, emisión de gases invernadero y esperanza de vida.", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Europa.", 
        "t17correcta":"1",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"América del norte.", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"Francia.", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        },
        { 
        "t13respuesta":"América Latina.", 
        "t17correcta":"1",
        "numeroPregunta": "4"
        }, 
        { 
        "t13respuesta":"América del norte.", 
        "t17correcta":"0",
        "numeroPregunta": "4"
        }, 
        { 
        "t13respuesta":"Oceanía.", 
        "t17correcta":"0",
        "numeroPregunta": "4"
        },
        { 
        "t13respuesta":"África.", 
        "t17correcta":"1",
        "numeroPregunta": "5"
        }, 
        { 
        "t13respuesta":"Oceanía.", 
        "t17correcta":"0",
        "numeroPregunta": "5"
        }, 
        { 
        "t13respuesta":"Haití.", 
        "t17correcta":"0",
        "numeroPregunta": "5"
        },
        { 
            "t13respuesta":"Los desechos líquidos y sólidos, como basura y envases, descargas de aguas residuales, descargas industriales.", 
            "t17correcta":"1",
            "numeroPregunta": "6"
            }, 
            { 
            "t13respuesta":"Contaminación y uso del agua en el hogar, descargas residuales de las milpas y los desechos como basura y envases.", 
            "t17correcta":"0",
            "numeroPregunta": "6"
            }, 
            { 
            "t13respuesta":"Contaminación y uso del agua en el hogar, descargas de aguas industriales y los desechos como basura y envases.", 
            "t17correcta":"0",
            "numeroPregunta": "6"
            }, 
            { 
            "t13respuesta":"Las descargas de aguas residuales desechan sustancias químicas como detergentes y materia orgánica, mientras que las industriales están compuestas por desechos de productos químicos altamente venenosos.", 
            "t17correcta":"1",
            "numeroPregunta": "7"
            }, 
            { 
            "t13respuesta":"La cantidad de sustancia descargada en los cuerpos de agua, las descargas de aguas residuales desechan una cantidad mínima relacionada a sustancias orgánicas fertilizantes, mientras que las descargas industriales desechan altas cantidades compuestas por detergentes y aceites.", 
            "t17correcta":"0",
            "numeroPregunta": "7"
            }, 
            { 
            "t13respuesta":"Las descargas de aguas residuales desechan sustancias compuestas por aceites y detergentes mientras que las descargas de aguas residuales contienen sustancias que sirven como  elemento natural para el tratamiento del agua.", 
            "t17correcta":"0",
            "numeroPregunta": "7"
            }, 
            { 
            "t13respuesta":"América, con el río Missisipi, debido a desechos industriales, y el río Lerma por desechos domésticos,  y Asia, con el río Yangtzé, por desechos agrícolas.", 
            "t17correcta":"1",
            "numeroPregunta": "8"
            }, 
            { 
            "t13respuesta":"África, con el río Danubio, debido a desechos domésticos,  y América, con  con el río Missisipi, debido a desechos industriales y  el río Lerma por desechos agrícolas.", 
            "t17correcta":"0",
            "numeroPregunta": "8"
            }, 
            { 
            "t13respuesta":"Asia, con el río Yangtzé, por desechos industriales, y África, con el río Nilo, debido a desechos de drenaje.", 
            "t17correcta":"0",
            "numeroPregunta": "8"
            }, 
            { 
            "t13respuesta":"Los principales factores son la  actividad industrial,  la obtención de energía a partir de combustibles fósiles y la ganadería.", 
            "t17correcta":"1",
            "numeroPregunta": "9"
            }, 
            { 
            "t13respuesta":"Los principales factores son la contaminación de los cuerpos de agua, la agricultura y la actividad industrial.", 
            "t17correcta":"0",
            "numeroPregunta": "9"
            }, 
            { 
            "t13respuesta":"Los principales factores son la  desertificación, la actividad industrial y la agricultura.", 
            "t17correcta":"0",
            "numeroPregunta": "9"
            },
            { 
            "t13respuesta":"Rellenos sanitarios o de basura que se infiltran en el suelo, uso de sustancias químicas en la agricultura y derrames de desechos industriales.", 
            "t17correcta":"1",
            "numeroPregunta": "10"
            }, 
            { 
            "t13respuesta":"Derrames de desechos domésticos, uso de materia orgánica en la agricultura y actividad ganadera.", 
            "t17correcta":"0",
            "numeroPregunta": "10"
            }, 
            { 
            "t13respuesta":"Rellenos sanitarios o de basura, uso de materia orgánica en  la agricultura y la obtención de energía a partir de combustibles fósiles.", 
            "t17correcta":"0",
            "numeroPregunta": "10"
            },
            { 
            "t13respuesta":"La degradación del suelo tiene como resultado que no se puedan sostener los organismos vivos que dependen de él y  la contaminación del aire genera calentamiento global.", 
            "t17correcta":"1",
            "numeroPregunta": "11"
            }, 
            { 
            "t13respuesta":"La contaminación del aire genera infertilidad de los nutrientes de los suelos.", 
            "t17correcta":"0",
            "numeroPregunta": "11"
            }, 
            { 
            "t13respuesta":"La contaminación del aire genera calentamiento global.", 
            "t17correcta":"0",
            "numeroPregunta": "11"
            }    
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta":["Selecciona la respuesta correcta.<br><br>El ingreso, empleo, salud, educación y ambiente son:",
                         "Son los respectivos sistemas que se utilizan para medir  los factores de la calidad de vida:",
                         "¿Qué datos estadísticos nos permiten comparar la calidad de vida en cada continente?",
                         "¿Qué continente tiene la calidad de vida más alta?",
                         "¿Qué parte del continente americano tiene una calidad de vida media?",
                         "¿Qué continente tiene una calidad de vida baja?",
                         "La contaminación de los cuerpos de agua se debe principalmente a:",
                         "¿Cuál es la diferencia entre las descargas de aguas residuales y las descargas industriales?",
                         "¿Qué  continentes  tienen los mayores problemas de contaminación en sus ríos y debido a qué razones?",
                         "¿Cuáles son los principales factores de la contaminación del aire y sus consecuencias?",
                         "¿Qué elementos ocasionan la contaminación del suelo?",
                         "¿Qué repercusiones tienen los distintos problemas ambientales?"],
        "preguntasMultiples": true,
        } 
        },
        //2
        {
            "respuestas": [
                {
                    "t13respuesta": "Calentamiento global.",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "Desertificación.",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "Mitigación.",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "Consumo responsable, no desperdiciar agua, hacer uso de las energías alternativas.",
                    "t17correcta": "4"
                },
                {
                    "t13respuesta": "Medidas que minimizan el impacto negativo",
                    "t17correcta": "5"
                },
                {
                    "t13respuesta": "Organismos internacionales.",
                    "t17correcta": "6"
                }
            ],
            "preguntas": [
                {
                    "t11pregunta": "Emisión de gases efecto invernadero que ocasionan el aumento de temperatura en la atmósfera."
                },
                {
                    "t11pregunta": "Aumento de las regiones secas y semisecas debido a la deforestación y degradación del suelo."
                },
                {
                    "t11pregunta": "Tiene la finalidad  de evitar o disminuir los efectos adversos de los problemas ambientales."
                },
                {
                    "t11pregunta": "Aminoran nuestro impacto sobre el medio ambiente."
                },
                {
                    "t11pregunta": "Programas de reciclaje de desechos, limpieza de playas."
                },
                {
                    "t11pregunta": "Se aseguran de crear acuerdos que regulen en todos los países la responsabilidad de cada uno  con el impacto ambiental."
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "12",
                "t11pregunta": "Relaciona las columnas según corresponda."
            }
        },
        //3
        {
            "respuestas": [{
                    "t13respuesta": "Birmania y Myanmar",
                    "t17correcta": "0"
                },
                {
                    "t13respuesta": "Chernóbil, Ucrania",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "Golfo de México",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "Japón",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "Nepal",
                    "t17correcta": "4"
                },
                {
                    "t13respuesta": "Ontake, Japón",
                    "t17correcta": "5"
                },
                {
                    "t13respuesta": "Estados Unidos, N. Orleans",
                    "t17correcta": "6"
                }
            ],
            "preguntas": [{
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Terremoto y Tsunami, 2011",
                    "correcta": "3"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Huracán Nargis, 2008",
                    "correcta": "0"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Huracán Katrina, 2005",
                    "correcta": "6"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Explosión de un reactor nuclear, 1986",
                    "correcta": "1"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Derrame de petróleo, 2010",
                    "correcta": "2"
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Erupción volcánica, 2014",
                    "correcta": "5"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda al suceso de acuerdo con el lugar en donde ocurrió.",
                "descripcion": "Sucesos",
                "variante": "editable",
                "anchoColumnaPreguntas": 20,
                "evaluable": true
            }
        },
        //4
        {
            "respuestas": [
                {
                    "t13respuesta": "<p>prevenidos<\/p>",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "<p>simulacros<\/p>",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "<p>seguros<\/p>",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "<p>localidad<\/p>",
                    "t17correcta": "4"
                },
                {
                    "t13respuesta": "<p>rutas de evacuación<\/p>",
                    "t17correcta": "5"
                },
                {
                    "t13respuesta": "<p>mapa de riesgos<\/p>",
                    "t17correcta": "6"
                }, 
                {
                    "t13respuesta": "<p>fenómenos naturales<\/p>",
                    "t17correcta": "7"
                },
                {
                    "t13respuesta": "<p>artículos de emergencia<\/p>",
                    "t17correcta": "8"
                },
                {
                    "t13respuesta": "<p>autoridades competentes<\/p>",
                    "t17correcta": "9"
                },
                {
                    "t13respuesta": "<p>evitar<\/p>",
                    "t17correcta": "10"
                },
                {
                    "t13respuesta": "<p>reparación de daños<\/p>",
                    "t17correcta": "11"
                },
                {
                    "t13respuesta": "<p>electricidad<\/p>",
                    "t17correcta": "12"
                },
                {
                    "t13respuesta": "<p>fugas<\/p>",
                    "t17correcta": "13"
                }
            ],
            "preguntas": [
                {
                    "t11pregunta": "<p>Imagina que nos estamos preparando para estar <\/p>"
                },
                {
                    "t11pregunta": "<p> antes, durante y después de un desastre, ¿qué deberíamos hacer?, podemos, por ejemplo, participar en <\/p>"
                },
                {
                    "t11pregunta": "<p> y brigadas de auxilio, ubicar los lugares más <\/p>"
                },
                {
                    "t11pregunta": "<p> de nuestra casa y <\/p>"
                },
                {
                    "t11pregunta": "<p>, trazar <\/p>"
                },
                {
                    "t11pregunta": "<p> para desalojar la casa o comunidad, también se puede diseñar en conjunto con tus maestros un <\/p>"
                },
                {
                    "t11pregunta": "<p> y promover la protección de la naturaleza para disminuir los riesgos. Todo esto nos servirá para saber cómo reaccionar ante los distintos desastres ya sean <\/p>"
                },
                {
                    "t11pregunta": "<p> o provocados por humanos.<br>Ahora, mientras ocurre el desastre, se pueden hacer diferentes acciones como tener a la mano <\/p>"
                },
                {
                    "t11pregunta": "<p>, seguir las instrucciones de las <\/p>"
                },
                {
                    "t11pregunta": "<p> y conservar la calma.<br>Por último, las acciones a realizar después de un desastre tienen como finalidad <\/p>"
                },
                {
                    "t11pregunta": "<p> actividades que podrían ocasionar daños mayores, algunas de estas pueden ser, colaborar con los vecinos para la <\/p>"
                },
                {
                    "t11pregunta": "<p>, no utilizar la <\/p>"
                },
                {
                    "t11pregunta": "<p>, el gas y el agua hasta asegurarse de que no hay <\/p>"
                },
                {
                    "t11pregunta": "<p> y reportar si hay heridos.<\/p>"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Selecciona las palabras de los recuadros que completen el párrafo.<br>",
                "respuestasLargas": true,
                "pintaUltimaCaja": false,
                "contieneDistractores":true
            }
        },
];