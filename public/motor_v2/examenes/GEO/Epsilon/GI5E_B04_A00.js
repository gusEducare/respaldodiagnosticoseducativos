json = [
    //reactivo 1
    {
        "respuestas": [{
                "t13respuesta": "Espacios agrícolas",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Espacios Ganaderos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Espacios forestales",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Espacios Pesqueros",
                "t17correcta": "3"
            }
        ],
        "preguntas": [{
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Necesita amplias extensiones de llanuras, suelo fértil, disponibilidad y cercanía con cuerpos de agua,  su clima ideal es de zonas templadas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "De preferencia que sean zonas de llanura, con climas templados y fríos, además necesitan disponibilidad y accesibilidad a cuerpos de agua.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dependiendo del clima, el terreno puede ser montaña o llanura; a mayor altitud los troncos son más delgados; se desarrollan mejor en latitudes altas; los climas templados y fríos son aptos para el desarrollo de bosques, y los cálidos para selvas.",
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las corrientes marinas frías transportan nutrientes, las aguas que se encuentran más cerca de la plataforma continental son más ricas en fauna.",
                "correcta": "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada tipo de espacio.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable": true
        }
    },
    //reactivo 2
    {
        "respuestas": [
            {
                "t13respuesta": "<p>civilización<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>agricultura<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>cereal<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>arroz<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>maíz<\/p>",
                "t17correcta": "5"
            }, 
            {
                "t13respuesta": "<p>trigo<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>domesticación de animales<\/p>",
                "t17correcta": "7"
            }, 
            {
                "t13respuesta": "<p>Historia<\/p>",
                "t17correcta": "8"
            }, 
            {
                "t13respuesta": "<p>Gandería<\/p>",
                "t17correcta": "8"
            }, 
            {
                "t13respuesta": "<p>Perú<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El inicio de la <\/p>"
            },
            {
                "t11pregunta": "<p> se da directamente con el descubrimiento de la <\/p>"
            },
            {
                "t11pregunta": "<p> y el paso de ser  nómadas a sedentarios. La agricultura está relacionada con los orígenes de la civilización, tanto que cada civilización es partidaria en especial de un <\/p>"
            },
            {
                "t11pregunta": "<p>, por ejemplo, en China el <\/p>"
            },
            {
                "t11pregunta": "<p>, en México el <\/p>"
            },
            {
                "t11pregunta": "<p> y los pueblos mediterráneos el <\/p>"
            },
            {
                "t11pregunta": "<p>. Además de la agricultura, la <\/p>"
            },
            {
                "t11pregunta": "<p> permitió al hombre el desarrollo de nuevas formas de sustento como la ganadería o la pesca.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //reactivo 3
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los minerales se han formado a partir de la composición polar de la tierra.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los minerales se encuentran presentes en nuestra vida diaria en forma de acero, sal u otras formas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La minería ha sido una actividad necesaria e importante para el desarrollo de la humanidad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La minería puede producir recursos minerales, como el oro y la plata, o recursos energéticos como el gas y el carbón.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En México se producen principalmente plata y cobre.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El carbón, el gas y el petróleo son recursos minerales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estados Unidos y Rusia son altos productores de gas natural, como México lo es de petróleo.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "",   
            "variante": "editable",
            //"anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    //reactivo 4
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"...de procesar las materias primas para convertirlas en bienes y servicios que serán utilizados para satisfacer las necesidades humanas.", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"...en los servicios como el comercio y el transporte, que se encargan de distribuir los productos terminados.", 
        "t17correcta":"2", 
        }, 
        { 
        "t13respuesta":"...industria básica e industria ligera o de manufactura.", 
        "t17correcta":"3", 
        }, 
        { 
        "t13respuesta":"...genera productos para el consumidor directo. No requiere tanta materia prima ni energía. Ejemplo de ella puede ser la industria textil, alimenticia, de calzado, etcétera.", 
        "t17correcta":"4", 
        }, 
        { 
        "t13respuesta":"...por ejemplo, la industria siderúrgica, que produce acero que después será utilizado para producir otros bienes como los automóviles.", 
        "t17correcta":"5", 
        }, 
        ], 
        "preguntas": [ 
        { 
        "t11pregunta":"La industria es una actividad económica que se encarga..." 
        }, 
        { 
        "t11pregunta":"El complemento de la industria está..." 
        }, 
        { 
        "t11pregunta":"Por lo general la industria se divide en..." 
        }, 
        { 
        "t11pregunta":"La industria de manufactura o ligera es la que..." 
        }, 
        { 
        "t11pregunta":"La industria básica es la que se encarga de producir bienes de gran tamaño..." 
        }, 
        ], 
        "pregunta":{ 
        "c03id_tipo_pregunta":"12", 
        "t11pregunta": "Relaciona las columnas según corresponda.", 
        } 
        },
    //reactivo 5
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Aquellas que se encargan de distribuir en el espacio los bienes producidos por la industria.", 
        "t17correcta":"1",
        "numeroPregunta": "0"
        }, 
        { 
        "t13respuesta":"Aquellas que se encargan de extraer los recursos minerales, forestales, pesqueros y ganaderos.", 
        "t17correcta":"0",
        "numeroPregunta": "0"
        }, 
        { 
        "t13respuesta":"Aquellas que se encargan de procesar y transformar en bienes las materias primas.", 
        "t17correcta":"0",
        "numeroPregunta": "0"
        }, 
        { 
        "t13respuesta":"Aquellas que se encargan de reglamentar las materias utilizadas y tener un control de calidad.", 
        "t17correcta":"0",
        "numeroPregunta": "0"
        },
        { 
        "t13respuesta":"Medios de transporte, redes de carreteras, vías férreas, rutas marítimas y rutas aéreas.", 
        "t17correcta":"1",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"Personal capacitado que transporte la mercancía de manera segura.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"Vehículos automotores de baja densidad para mayor velocidad.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"Información útil de cómo llegar a tal lugar, como los mapas.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        },
        { 
        "t13respuesta":"Terrestres", 
        "t17correcta":"1",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Aéreos", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Ferroviarios", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Marítimos", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        },
        { 
        "t13respuesta":"Marítimo", 
        "t17correcta":"1",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"Aéreo", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"Terrestre", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"Submarino", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        },   
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Qué son las actividades terciarias?",
                         "Para poder moverse, comunicarse y distribuir productos el humano ha creado…",
                         "¿Cuáles son los medios de transporte más utilizados?",
                         "¿Cuál es el medio de transporte más utilizado para trasladar grandes cantidades de mercancía?"],
        "preguntasMultiples": true,
        } 
        },
    //reactivo 6 
    { 
        "respuestas":[ 
            { 
            "t13respuesta":"F", 
            "t17correcta":"0", 
            }, 
            { 
            "t13respuesta":"T", 
            "t17correcta":"1", 
            }
            ],
        "preguntas" : [ 
            { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "El comercio de mercancías se realiza en las ciudades más grandes y con mayor población, aun cuando no tengan un puerto o una vía de comunicación.", 
            "correcta":"0", 
            }, 
            { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "El comercio puede presentarse de manera nacional o internacional.", 
            "correcta":"1", 
            }, 
            { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "La importancia del comercio internacional es que permite el intercambio de productos y materias que no siempre se encuentran en algunos países.", 
            "correcta":"1", 
            }, 
            { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "La importación es la acción de llevar productos a otro lugar, mientras que la exportación es la acción de traer productos de otro lugar.", 
            "correcta":"0", 
            }, 
            { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "Los puertos marítimos y los aeropuertos están ubicados estratégicamente.", 
            "correcta":"1", 
            }, 
            { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "El turismo es la actividad que consiste en viajar a otros lugares con fines de recreación, culturales, de negocios o académicos.", 
            "correcta":"1", 
            }, 
            { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "México recibe aproximadamente entre 10 y 20 millones de turistas al año.", 
            "correcta":"0", 
            }, 
            ],
         "pregunta":{ 
            "c03id_tipo_pregunta":"13", 
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.", 
            "t11instruccion": "", 
            "descripcion": "", 
            "variante": "editable", 
            "anchoColumnaPreguntas": 70, 
            "evaluable": true 
            } 
    },
    //reactivo 7
    {
    "respuestas": [        
        {
           "t13respuesta":  "globalización",
            "t17correcta": "1" 
        },
        {
           "t13respuesta":  "producción",
            "t17correcta": "2" 
        },
        {
           "t13respuesta":  "explotación de los recursos",
            "t17correcta": "3" 
        },
        {
           "t13respuesta":  "minería",
            "t17correcta": "4" 
        },
        {
           "t13respuesta":  "tala",
            "t17correcta": "5" 
        },
        {
           "t13respuesta":  "agricultura",
            "t17correcta": "6" 
        },
        {
           "t13respuesta":  "actividades primarias",
            "t17correcta": "7" 
        },
        {
           "t13respuesta":  "Comercialización ",
            "t17correcta": "8" 
        },
        {
           "t13respuesta":  "naturaleza",
            "t17correcta": "9" 
        }
    ],
    "preguntas": [ 
      {
      "t11pregunta": "  La mecánica del mundo actual se basa en la convivencia de distintos pueblos y países en un proceso de "
      },
      {
      "t11pregunta": " iniciado hace ya más de una década. Dos de los aspectos más valorados a nivel internacional son el mercado y la economía. Estos van estrechamente relacionados a la "
      },
      {
      "t11pregunta": " que, en mayor o menor medida, depende de la "
      },
      {
      "t11pregunta": ". La "
      },
      {
      "t11pregunta": ", la "
      },
      {
      "t11pregunta": ", la "
      },
      {
      "t11pregunta": ", etc.; son actividades que fungen como base de este sistema económico, y son designadas como "
      },
      {
      "t11pregunta": "."
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "Arrastra las palabras que completen el párrafo.", 
      "t11instruccion": "", 
      "respuestasLargas": true, 
      "pintaUltimaCaja": false, 
      "contieneDistractores": true 
    } 
  },
    //reactivo 8
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Producto Interno Bruto", 
        "t17correcta":"1", 
        "numeroPregunta": "0"

        }, 
        { 
        "t13respuesta":"Productos Internacionales y Bienes", 
        "t17correcta":"0",
        "numeroPregunta": "0"
        }, 
        { 
        "t13respuesta":"Producto Interior Base", 
        "t17correcta":"0",
        "numeroPregunta": "0"

        }, 
        { 
        "t13respuesta":"Producto Ígneo Básico", 
        "t17correcta":"0",
        "numeroPregunta": "0"
        },
        { 
        "t13respuesta":"Es el dinero generado por la producción de bienes y servicios de un país en un año.", 
        "t17correcta":"1",
        "numeroPregunta": "1"
        },
        { 
        "t13respuesta":"Son los servicios que se generan dentro de un país y su valor estimado.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"Es la estadística anual de la producción industrial a nivel estatal.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"Son los bienes y servicios consumidos por un país en un lapso de un mes.", 
        "t17correcta":"0",
        "numeroPregunta": "1"
        }, 
        { 
        "t13respuesta":"China", 
        "t17correcta":"1",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Japón", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Vietnam", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        }, 
        { 
        "t13respuesta":"Mongolia", 
        "t17correcta":"0",
        "numeroPregunta": "2"
        },
        { 
        "t13respuesta":"Dominica", 
        "t17correcta":"1",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"México", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"Uruguay", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        }, 
        { 
        "t13respuesta":"Venezuela", 
        "t17correcta":"0",
        "numeroPregunta": "3"
        },
        { 
        "t13respuesta":"Estados Unidos de América", 
        "t17correcta":"1",
        "numeroPregunta": "4"
        }, 
        { 
        "t13respuesta":"Alemania", 
        "t17correcta":"0",
        "numeroPregunta": "4"
        }, 
        { 
        "t13respuesta":"China", 
        "t17correcta":"0",
        "numeroPregunta": "4"
        }, 
        { 
        "t13respuesta":"Japón", 
        "t17correcta":"0",
        "numeroPregunta": "4"
        },
        { 
        "t13respuesta":"En actividades terciarias.", 
        "t17correcta":"1",
        "numeroPregunta": "5"
        }, 
        { 
        "t13respuesta":"En actividades primarias.", 
        "t17correcta":"0",
        "numeroPregunta": "5"
        }, 
        { 
        "t13respuesta":"En actividades secundarias.", 
        "t17correcta":"0",
        "numeroPregunta": "5"
        }, 
        { 
        "t13respuesta":"En las tres actividades.", 
        "t17correcta":"0",
        "numeroPregunta": "5"
        },
        { 
        "t13respuesta":"En actividades primarias y secundarias.", 
        "t17correcta":"1", 
        "numeroPregunta": "6"
        }, 
        { 
        "t13respuesta":"En actividades primarias.", 
        "t17correcta":"0",
        "numeroPregunta": "6"
        }, 
        { 
        "t13respuesta":"En actividades secundarias.", 
        "t17correcta":"0",
        "numeroPregunta": "6"
        }, 
        { 
        "t13respuesta":"En actividades terciarias y secundarias.", 
        "t17correcta":"0",
        "numeroPregunta": "6"
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Qué significan las siglas PIB?",
                         "¿Qué es el PIB?",
                         "¿Cuál es el país asiático con el mayor PIB?",
                         "¿Cuál es el país americano con el menor PIB?",
                         "¿Cuál es el país que tiene el mayor PIB?",
                         "¿Los países más desarrollados en qué tipo de actividades basan su economía?",
                         "¿Los países menos desarrollados en qué tipo de actividades basan su economía?"],
        "preguntasMultiples": true,
        } 
    } 
];