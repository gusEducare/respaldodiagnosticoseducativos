json = [
    //1
    {  
      "respuestas":[
         {  
            "t13respuesta": "Los Montes Urales",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Los Montes Apalaches",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Los Himalaya",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "El Kilimanjaro",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La Ossa",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Kilimanjaro",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Popocatepetl",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Everest",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "La Ossa",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los Alpes",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los Alpes",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Montes Cárpatos",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los Pirineos",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Montes Urales",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los Alpes del Sur",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Monte Vesubio",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Mont Blanc",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Monte Everest",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Nanga Parbat",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Monte Camerún",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>¿Qué cordilleras pertenecen al continente americano?",
                         "¿Cuál de las siguientes elevaciones pertenece a Asia?",
                         "¿Cuál de las siguientes cordilleras no pertenece a Europa?",
                         "¿Cuál de los siguientes nombres pertenece a un volcán?"],
         "preguntasMultiples": true,
      }
   },
   //2
   {
        "respuestas": [
            {
                "t13respuesta": "<p>volcán<\/p>",
                "t17correcta": "25"
            },
            {
                "t13respuesta": "<p>monte<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>distribución geográfica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>geografía<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>planeta<\/p>",
                "t17correcta": "4"
            }, 
            {
                "t13respuesta": "<p>qué es<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>continentes<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>Marie Tharp<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>dorsal mesoatlántica<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>sistema montañoso<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>dinámica interna<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>Himalaya<\/p>",
                "t17correcta": "11"
            },
            {
                "t13respuesta": "<p>Asia<\/p>",
                "t17correcta": "12"
            }, 
            {
                "t13respuesta": "<p>cordillera<\/p>",
                "t17correcta": "13"
            },
            {
                "t13respuesta": "<p>humedad<\/p>",
                "t17correcta": "14"
            },
            {
                "t13respuesta": "<p>clima desértico<\/p>",
                "t17correcta": "15"
            },
            {
                "t13respuesta": "<p>sismos<\/p>",
                "t17correcta": "16"
            },
            {
                "t13respuesta": "<p>placas tectónicas<\/p>",
                "t17correcta": "17"
            },
            {
                "t13respuesta": "<p>orografía<\/p>",
                "t17correcta": "18"
            },
            {
                "t13respuesta": "<p>volcanes<\/p>",
                "t17correcta": "19"
            },
            {
                "t13respuesta": "<p>activos<\/p>",
                "t17correcta": "20"
            },
            {
                "t13respuesta": "<p>79<\/p>",
                "t17correcta": "21"
            },
            {
                "t13respuesta": "<p>Vesubio<\/p>",
                "t17correcta": "22"
            },
            {
                "t13respuesta": "<p>península itálica<\/p>",
                "t17correcta": "23"
            },
            {
                "t13respuesta": "<p>Kilimanjaro<\/p>",
                "t17correcta": "24"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>En algún punto puede parecer innecesario aprender qué es una montaña, un <\/p>"
            },
            {
                "t11pregunta": "<p>, un volcán o cuál es su <\/p>"
            },
            {
                "t11pregunta": "<p>, pues se puede pensar que no tiene utilidad alguna aprender estas cosas. Sin embargo, en un primer momento es importante recordar que gracias a la <\/p>"
            },
            {
                "t11pregunta": "<p> los hombres aprendieron cómo funcionaba la dinámica de su <\/p>"
            },
            {
                "t11pregunta": "<p> Una pregunta como \“¿<\/p>"
            },
            {
                "t11pregunta": "<p> una montaña o por qué existe una montaña?\” puede tomar una relevancia científica impresionante, tanto que a causa de esto fue que los hombres descubrieron que los <\/p>"
            },
            {
                "t11pregunta": "<p> se movían.<br/>En realidad quien lo descubrió fue una mujer: <\/p>"
            },
            {
                "t11pregunta": "<p>, una oceanógrafa y geóloga estadounidense. Ella descubrió, después de una exhaustiva recopilación de datos, la <\/p>"
            },
            {
                "t11pregunta": "<p> —un <\/p>"
            },
            {
                "t11pregunta": "<p> marítimo— que fue el punto de quiebre para que se descubriera que la Tierra tiene una <\/p>"
            },
            {
                "t11pregunta": "<p>.<br/>Saber que los <\/p>"
            },
            {
                "t11pregunta": "<p> se encuentran en <\/p>"
            },
            {
                "t11pregunta": "<p> no parece ser muy útil. No obstante, saber que el desierto de Gobi existe porque es precisamente la <\/p>"
            },
            {
                "t11pregunta": "<p> del Himalaya la que bloquea el paso de la mayor parte de la <\/p>"
            },
            {
                "t11pregunta": "<p>de la región —y que causa el <\/p>"
            },
            {
                "t11pregunta": "<p>—  es una cosa distinta. O, en el caso de los <\/p>"
            },
            {
                "t11pregunta": "<p>, saber que son producidos por el movimiento de las <\/p>"
            },
            {
                "t11pregunta": "<p> nos permite designar cuáles son las zonas más propensas a sufrir uno. Sin embargo, es algo a lo que sólo se puede llegar si antes se ha estudiado bien la <\/p>"
            },
            {
                "t11pregunta": "<p> local. Por otra parte, el saber que los <\/p>"
            },
            {
                "t11pregunta": "<p> distribuidos alrededor del mundo son <\/p>"
            },
            {
                "t11pregunta": "<p> o inactivos nos brinda un mayor margen de prevención para que no no ocurra lo que a los pompeyanos en el año <\/p>"
            },
            {
                "t11pregunta": "<p>, cuando el monte <\/p>"
            },
            {
                "t11pregunta": "<p>, ubicado en la <\/p>"
            },
            {
                "t11pregunta": "<p>, hizo erupción.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra el concepto al lugar que corresponda:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "...cubierta por agua.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...es agua dulce, el resto es agua salada.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...a su longitud o motivos históricos y culturales.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...el Mississippi-Missouri, el río Amazonas, el río Yukón y el Río de Plata.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...el río Danubio, el río Rin, el río Ebro y el río Volga.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...el río Lena, el río Yeniséi, el río Obi, el río Indo, el río Ganges y el río Mekong.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "...el río Nilo, el río Congo y el río Níger.",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "...el río Darling y el río Murray.",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El 75% de la superficie terrestre está..."
            },
            {
                "t11pregunta": "Sólo el 3% de toda el agua del planeta..."
            },
            {
                "t11pregunta": "Un río puede ser importante debido a la cantidad de agua que fluye,..."
            },
            {
                "t11pregunta": "Algunos de los principales ríos de América son..."
            },
            {
                "t11pregunta": "Algunos de los principales ríos de Europa son..."
            },
            {
                "t11pregunta": "Algunos de los principales ríos de Asia son..."
            },
            {
                "t11pregunta": "Algunos de los principales ríos de África son..."
            },
            {
                "t11pregunta": "Algunos de los principales ríos de Oceanía son..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda:"
        }
    },
    //4
    {  
      "respuestas":[
         {  
            "t13respuesta": "Para los continentes y su desarrollo hidro y orográfico.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Para las primeras civilizaciones, ellas se desarrollaron a las orillas de los ríos.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Para el planeta, son fluyentes que despejan un paisaje.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Para el ecosistema, todo un mismo ecosistema subsiste de un río por siglos.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Porque de él se puede obtener agua, alimento y hasta energía.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque es parte del ecosistema donde vivimos.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque es parte de un paisaje y sería un crimen destruirlo.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "En realidad no tienen importancia alguna.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Sí, toda el agua de los ríos es agua dulce.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "No, el agua de los ríos no es potable, sólo podemos beber de los mares.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Sí, aunque hay que hervirla para evitar enfermedades.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "No, pueden existir ríos de agua salada cuyo consumo no sería apto para los humanos.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br><br>¿Para quiénes fueron históricamente importantes los ríos?",
                         "¿Por qué es importante un río para los humanos?",
                         "¿El agua de los ríos es apta para el consumo humano?"],
         "preguntasMultiples": true,
      }
   },
   //5
   {  
      "respuestas":[
         {  
            "t13respuesta": "Troposfera",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Estratosfera",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Mesosfera",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Ternosfera",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Demosfera",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Se basa en la temperatura.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Se refiere a los cambios en la atmósfera.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Normalmente los cambios se dan en la troposfera.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Se basa en la temperatura diaria.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Tiene sus bases en la altitud.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El tiempo atmosférico es un proceso de constante cambio, mientras que el clima es constante.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "El tiempo atmosférico incluye las precipitaciones diarias, el clima no.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "El tiempo atmosférico es constante, el clima es un proceso diario de continuo cambio.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "El clima se refiere a la temperatura y las precipitaciones, el tiempo atmosférico no.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Son completamente lo mismo, no hay motivo para separar estos conceptos.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "La rotación",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "La latitud",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "La profundidad",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "La altitud",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "La gravedad",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Son 3: frío, templado y tropical.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Son 4: frío, templado, seco y tropical.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Son 5: polar, frío, templado, seco y tropical.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Son 6: polar, ártico, frío, templado, seco y tropical.",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Troposfera",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Exosfera",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Demosfera",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Atmósfera",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Termosfera",
            "t17correcta": "1",
            "numeroPregunta":"5"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las respuestas correctas para cada pregunta.<br><br>¿Cuáles de las siguientes son capas de la atmósfera?",
                         "¿Cuáles son algunas de las características del tiempo atmosférico?",
                         "¿Cuáles son algunas de las diferencias entre tiempo atmosférico y clima?",
                         "¿Cuáles son los dos factores que modifican el clima?",
                         "¿Cuántos y cuáles no son los climas de la tierra?",
                         "¿Cuáles no son nombres de la capa que permite mantener las condiciones térmicas para el desarrollo de la vida como la conocemos?"],
         "preguntasMultiples": true,
      }
   },
   //6
   {  
      "respuestas":[
         {  
            "t13respuesta": "Polar",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Seco",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Frío",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Templado",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Tropical",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Seco",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Polar",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Frío",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Tropical",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Templado",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Existen 5: polar, tropical, seco, templado y frío.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Existen 4: polar, frío, tropical y templado.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Existen 3: polar, templado y frío.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Existen 2: frío y templado.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Existen 3: frío, polar y seco.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "En América",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "En Europa",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "En África",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "En Asia",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "En Oceanía",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Europa",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Asia y Europa",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Asia",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "América",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "América y Asia",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa a imagen presionando \"+\" y selecciona la respuesta correcta:<br><br>¿Qué clima es predominante en en el continente asiático?",
                         "¿Qué clima es el que predomina en Brasil?",
                         "¿Cuántos tipos de clima distintos existen en Europa?",
                         "¿En qué continente existe una mayor diversidad climática?",
                         "¿Qué continente o continentes abarcan la mayor área de clima templado en el mundo?"],
         "preguntasMultiples": true,
         "t11instruccion":"<center/><img src='GI5E_B02_A06_01.png'/>"
      }
   },
   //7
   {  
      "respuestas":[
         {  
            "t13respuesta": "Taiga",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Bosque",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Selva",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Desierto",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Taiga",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Bosque",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Selva",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Desierto",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Taiga",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Bosque",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Selva",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Desierto",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Taiga",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Bosque",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Selva",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Desierto",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Taiga",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Bosque.",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Selva",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Desierto",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "Taiga",
            "t17correcta": "1",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Tundra",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Bosque",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Selva",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Sabana",
            "t17correcta": "0",
            "numeroPregunta":"5"
         },
         {  
            "t13respuesta": "Desierto",
            "t17correcta": "0",
            "numeroPregunta":"5"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona  el clima al que le corresponde su flora y fauna:<br><br>Algunas de la especies que habitan esta región son los musgos y los líquenes. En cuanto a la fauna, son oriundos el reno, el lobo y el alce.",
                         "Oriundos de esta región son la ardilla voladora, el tecolote, el lince rojo, el zorro, etc. Entre la fauna local se encuentra el enebro, el arce  y el plágano.",
                         "Pertenecientes a la flora de esta región están el cactus, la yuca, los lirios del desierto, el ocotillo y el mezquite. En cuanto a la fauna, se encuentran el dromedario, la mangosta, el coyote, el berrendo y los escorpiones.",
                         "Pertenecientes a esta región se encuentran el oso hormiguero, el perezoso, el pájaro, la anaconda, el puma y la nutria. Su flora se compone principalmente de plantas trepadoras, orquídea, jacinto acuático, frutos y flores.",
                         "De esta región son oriundos los árboles de caucho, de cacao, las palmeras, la caoba, los helechos y los líquenes. En cuanto a su fauna, en ella habitan elefantes, jirafas, cebras, leones, chacales, buitres, hienas, gacelas y cocodrilos.",
                         "La especies animales que habitan esta región son el oso pardo, el lince, el zorro, el gato montés, el ciervo, el halcón y el búho. En cuanto a su flora, está compuesta principalmente de líquenes, musgos, pinos, abedules y abetos."],
         "preguntasMultiples": true,
      }
   },
   //8
   {
        "respuestas": [
            {
                "t13respuesta": "...el bosque tropical o selva, el bosque templado y el desierto.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "...las regiones naturales de tundra, pradera de alta montaña y hielos perpetuos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "...la región de la sabana, aunque existen también regiones como la selva o la estepa.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "...regiones naturales como la pradera y el bosque templado.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "...además de la selva tropical y el bosque templado.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "...aunque el desierto, la sabana y la pradera también ocupan un área importante.",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "En México existen al menos tres regiones naturales, que son..."
            },
            {
                "t11pregunta": "Al clima polar le corresponden..."
            },
            {
                "t11pregunta": "En el África subsahariana abunda..."
            },
            {
                "t11pregunta": "En Australia existen, además de la estepa y el desierto..."
            },
            {
                "t11pregunta": "En China existe un área de región natural perteneciente al clima polar..."
            },
            {
                "t11pregunta": "En la mayor parte de Sudamérica predominan las regiones de selva y bosque templado..."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Observa a imagen presionando \"+\" y relaciona las columnas",
            "t11instruccion":"<center/><img src='GI5E_B02_A06_01.png'/>"
        }
    } 
];
