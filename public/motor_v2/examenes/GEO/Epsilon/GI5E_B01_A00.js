json=[
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
           ],
            "preguntas": [
           {
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"La Pangea surgió a partir de los movimientos de la Tierra, este proceso duró más de  300 millones de años.",
            "correcta"  : "1"
            },
           {
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"Pangea significa enorme superficie de tierra.",
            "correcta"  : "0"
            },
           {
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"Las capas de la Tierra son núcleo, manto y corteza.",
            "correcta"  : "1"
            },
           {
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"En la era Paleozoica, la Pangea comenzó a separarse dando forma a los continentes.",
            "correcta"  : "0"
            },
           {
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"Las placas tectónicas son responsables del cambio climático de la Tierra.",
            "correcta"  : "0"
            },
           {
            "c03id_tipo_pregunta": "13",
             "t11pregunta":"Los continentes son grandes porciones de tierra separados por océanos o por algún accidente geográfico.",
            "correcta"  : "1"
            },
           {
            "c03id_tipo_pregunta": "13",
            "t11pregunta":"Antártida no es considerada un continente más debido a que es una extensión muy pequeña de la Tierra.",
            "correcta"  : "0"
            },
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
                "descripcion": "Reactivo",
                "variante": "editable",
                "anchoColumnaPreguntas": 60,
                "evaluable"  : true
            }
        },
        //2
        {
            "respuestas": [
                {
                    "t13respuesta": "<p>Es un desierto debido<br> a sus condiciones<br> geográficas. <\/p>",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "<p><br>Conformado por<br> 54 países. <\/p>",
                    "t17correcta": "2,4"
                },
                {
                    "t13respuesta": "<p><br>Conformado por<br> 35 países. <\/p>",
                    "t17correcta": "3,5"
                },
                {
                    "t13respuesta": "<p><br>Ecosistemas: sabanas,<br>selvas y desiertos.<\/p>",
                    "t17correcta": "4,2"
                },
                {
                    "t13respuesta": "<p>Sus principales cordilleras<br> son las Montañas<br>Rocosas y Cordillera de<br>los Andes.<\/p>",
                    "t17correcta": "5,3"
                }
            ],
            "preguntas": [
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<br><style>\n\
                                            .table img{height: 90px !important; width: auto !important; }\n\
                                        </style><table class='table' style='margin-top:-40px;'>\n\
                                        <tr>\n\
                                            <td style='width:350px'>África</td>\n\
                                            <td style='width:100px'>América</td>\n\
                                            <td style='width:100px'>Antártida</td>\n\
                                        </tr>\n\
                                        <tr>\n\
                                            <td style='width:350px'>Sus principales ríos son Nilo, Senegal, Niger y Congo.</td>\n\
                                            <td style='width:100px'>Considerado pluricultural. </td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td></tr><tr>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td><td>Continente menos habitado.</td>\n\
                                        </tr><tr>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                     <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td><td>Es el continente más frío del planeta.</td>\n\
                                            </tr></table>"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Arrastra las características que correspondan a cada continente.",
               "contieneDistractores":true,
                "anchoRespuestas": 90,
                "soloTexto": true,
                "respuestasLargas": true,
                "pintaUltimaCaja": false,
                "ocultaPuntoFinal": true
            }
        },
    
    
        //3
        {
            "respuestas": [
                {
                    "t13respuesta": "Opción 1",
                    "t17correcta": "0"
                },
                {
                    "t13respuesta": "Opción 2",
                    "t17correcta": "1"
                }
               ],
                "preguntas": [
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Continente más grande y poblado del planeta.",
                "valores": ['África', 'Asia', ],
                "correcta"  : "1"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Es el continente más pequeño.",
                "valores": ['Oceanía', 'Europa', ],
                "correcta"  : "0"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Es conocido como el viejo continente.",
                "valores": ['Europa', 'América', ],
                "correcta"  : "0"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Limita al norte con el océano Glacial Ártico, al sur el Índico, al este el Pacífico y en el oeste colinda con el continente europeo.",
                "valores": ['Oceanía', 'Asia', ],
                "correcta"  : "1"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Sus principales cordilleras son los Alpes, Carpatos y montes Escandinavos.",
                "valores": ['Antártida', 'Europa', ],
                "correcta"  : "1"
                },
              /* {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Se compone de varias islas (archipiélago).",
                "valores": ['Oceanía', 'Asia', ],
                "correcta"  : "0"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"Sus principales ríos son el Obi, Indo y Ganges.",
                "valores": ['Asia', 'Europa', ],
                "correcta"  : "0"
                },
                {
                 "c03id_tipo_pregunta": "13",
                 "t11pregunta":"Se caracteriza por poseer especies de animales endémicas como el canguro, el koala y el demonio de Tasmania.",
                 "valores": ['Oceanía', 'América', ],
                 "correcta"  : "0"
                 },
                 {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta":"Se distinguen tres grupos étnicos principales: nórdicos, germanos y eslavos. ",
                  "valores": ['Asia', 'Europa', ],
                  "correcta"  : "1"
                  },
                  {
                   "c03id_tipo_pregunta": "13",
                   "t11pregunta":"Está formado por 14 países. ",
                   "valores": ['Antártida', 'Oceanía', ],
                   "correcta"  : "1"
                   }*/
                ],
                "pregunta": {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "<p>Elige el continente según corresponda. <\/p>",
                    "descripcion": "Reactivo",
                    "variante": "editable",
                    "anchoColumnaPreguntas": 60,
                    "evaluable"  : true
                }
            },
            //4
             {
            "respuestas": [
                {
                    "t13respuesta": "<p>Gran variedad de flora y <br>fauna. <br>Predomina el clima tropical.<\/p>",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "<p>Climas variados, tropicales, <br> fríos y templados.<\/p>",
                    "t17correcta": "2,5"
                },
                {
                    "t13respuesta": "<p><br>Gran variedad de bosques <br> templados y fríos. <\/p>",
                    "t17correcta": "3,4"
                },
                {
                    "t13respuesta": "<p>Bosques tropicales y selvas.<\/p>",
                    "t17correcta": "4,3"
                },
                {
                    "t13respuesta": "<p>Planicies y altas montañas.<\/p>",
                    "t17correcta": "5,2"
                }
            ],
            "preguntas": [
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<br><style>\n\
                                            .table img{height: 90px !important; width: auto !important; }\n\
                                        </style><table class='table' style='margin-top:-40px;'>\n\
                                        <tr>\n\
                                            <td style='width:350px'>Centroamérica</td>\n\
                                            <td style='width:100px'>Norteamérica</td>\n\
                                            <td style='width:100px'>Sudamérica</td>\n\
                                        </tr>\n\
                                        <tr>\n\
                                            <td >"
                },
    
                {"c03id_tipo_pregunta": "8",
                    "t11pregunta": "  </td><td><style='width:350px'>Zonas geográficas y climas muy variados.</td>\n\
                      <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "     </td></tr><tr>\n\
                                            <td></td> <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "       </td><td>Selvas y  llanuras.</tr><tr>\n\
                     <td> </td>  <td> "
                },
    
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td><td>"
                },
                 {"c03id_tipo_pregunta": "8",
                    "t11pregunta": "  </td>\n\
                                            </tr></table>"
    
    }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Arrastra las palabras que correspondan a cada región.",
               "contieneDistractores":true,
                "anchoRespuestas": 90,
                "soloTexto": true,
                "respuestasLargas": true,
                "pintaUltimaCaja": false,
                "ocultaPuntoFinal": true
            }
        },
        //5
            {
                "respuestas":[
                {
                "t13respuesta":"norte, sur, este y oeste.",
                "t17correcta":"1",
                },
               {
                "t13respuesta":"norte, sur, este y oriente.",
                "t17correcta":"0",
                },
               {
                "t13respuesta":"oriente, occidente, oeste y sur.",
                "t17correcta":"0",
                },
               {
                "t13respuesta":"oeste, sur, norte y occidente.",
                "t17correcta":"0",
                },
               ],
                "pregunta":{
                "c03id_tipo_pregunta":"1",
                "t11pregunta": "Selecciona la respuesta correcta. <br><br>Los puntos cardinales son...",
    
                }
                },
                //6
                {
                    "respuestas": [
                        {
                            "t13respuesta": "Oeste",
                            "t17correcta": "0",
                            "columna":"0"
                        },
                        {
                            "t13respuesta": "Norte",
                            "t17correcta": "1",
                            "columna":"0"
                        },
                        {
                            "t13respuesta": "Este",
                            "t17correcta": "2",
                            "columna":"1"
                        },
                        {
                            "t13respuesta": "Sur",
                            "t17correcta": "3",
                            "columna":"1"
                        }
                    ],
                    "pregunta": {
                        "c03id_tipo_pregunta": "5",
                        "t11pregunta": "<p>Observa atentamente la imagen y arrastra el punto cardinal al número que corresponda.<br><\/p>",
                        "tipo": "ordenar",
                        "imagen": true,
                        "url":"GI5E_B01_R05.png",
                        "respuestaImagen":true,
                        "tamanyoReal":true,
                        "bloques":false,
                        "borde":true
    
                    },
                    "contenedores": [
                        {"Contenedor": ["", "234,15", "cuadrado", "100, 50", ".","transparent"]},
                        {"Contenedor": ["", "0,268", "cuadrado", "100, 50", ".","transparent"]},
                        {"Contenedor": ["", "234,530", "cuadrado", "100, 50", ".","transparent"]},
                        {"Contenedor": ["", "473,270", "cuadrado", "100, 50", ".","transparent"]},
    
                    ]
                },
                //6
              /*  {
                    "respuestas":[
                    {
                    "t13respuesta":"una representación de los puntos cardinales; sirve para ubicar distintos puntos.",
                    "t17correcta":"1",
                    },
                   {
                    "t13respuesta":"una representación de los puntos cardinales;  sirve para representar un espacio terrestre.",
                    "t17correcta":"0",
                    },
                   {
                    "t13respuesta":"una representación plana para buscar ubicaciones.",
                    "t17correcta":"0",
                    },
                   {
                    "t13respuesta":"una representación de la Tierra.",
                    "t17correcta":"0",
                    },
                   ],
                    "pregunta":{
                    "c03id_tipo_pregunta":"1",
                    "t11pregunta": "Selecciona la respuesta correcta. <br><br>La rosa de los vientos es….",
                    "t11instruccion": "",
                    }
                },*/
                    //7
                    {
                        "respuestas":[
                       {
                        "t13respuesta":"<img src='GI5E_B01_R06_01.png'/>",
                        "t17correcta":"1",
                        },
                       {
                        "t13respuesta":"<img src='GI5E_B01_R06_02.png'/>",
                        "t17correcta":"2",
                        },
                       {
                        "t13respuesta":"<img src='GI5E_B01_R06_03.png'/>",
                        "t17correcta":"3",
                        },
                       {
                        "t13respuesta":"<img src='GI5E_B01_R06_04.png'/>",
                        "t17correcta":"4",
                        },
                       {
                        "t13respuesta":"<img src='GI5E_B01_R06_05.png'/>",
                        "t17correcta":"5",
                        },
                        {
                         "t13respuesta":"<img src='GI5E_B01_R06_06.png'/>",
                         "t17correcta":"6",
                         },
                         {
                          "t13respuesta":"<img src='GI5E_B01_R06_07.png'/>",
                          "t17correcta":"7",
                          }
                       ],
                        "preguntas": [
                        {
                        "t11pregunta":"Globo terráqueo."
                        },
                       {
                        "t11pregunta":"Mapa de escala de ampliación."
                        },
                        {
                         "t11pregunta":"Plano."
                         },
                       {
                        "t11pregunta":"Fotografías aéreas."
                        },
                       {
                        "t11pregunta":"Mapa de escala."
                        },
                        {
                         "t11pregunta":"Imágenes satelitales."
                         },
                         {
                          "t11pregunta":"Mapa temático."
                          }
                        ],
                        "pregunta":{
                        "c03id_tipo_pregunta":"12",
                        "t11pregunta": "Relaciona las columnas según corresponda.",
                        "t11instruccion": "",
                        "altoImagen":"100px",
                        "anchoImagen":"200px"
                        }
                        },
                        //8
                         {
                                "respuestas": [        
                                    {
                                       "t17correcta":  "4"
                                     },
                                    {
                                       "t17correcta":  "5"
                                     },
                                    {
                                       "t17correcta":  "7"
                                     },
                                    {
                                       "t17correcta":  "9"
                                     },
                                    {
                                       "t17correcta":  "1"
                                     },
                                    {
                                       "t17correcta":  "8"
                                     },
                                    {
                                       "t17correcta":  "3"
                                     },
                                    {
                                       "t17correcta":  "2"
                                     },
                                    {
                                       "t17correcta":  "6"
                                     },
                                    {
                                       "t17correcta":""
                                     }
                                ],
                                "preguntas": [
                                 
    
                                      {
                                          "t11pregunta": ""
                                          },  
                                {
                                  "t11pregunta": "Brasil <br>"
                                  },
                                 {
                                  "t11pregunta": " Perú <br>"
                                  },
                                 {
                                  "t11pregunta": " Bolivia <br>"
                                  },
                                 {
                                  "t11pregunta": " Ecuador <br>"
                                  },
                                 {
                                  "t11pregunta": " Canadá <br>"
                                  },
                                 {
                                  "t11pregunta": " Argentina <br>"
                                  },
                                 {
                                  "t11pregunta": " México <br>"
                                  },
                                 {
                                  "t11pregunta": " Estados Unidos <br>"
                                  },
                                 {
                                  "t11pregunta": " Colombia <br>"
                                  },
                               ],
                                "pregunta": {
                                  "c03id_tipo_pregunta": "6",
                                  "t11pregunta": "Observa atentamente la imagen y escribe el número que corresponde con el país.",
                                  "t11instruccion":"<center><img src='GI5E_B01_R08.png' width = 500 height = 700px/></center>",
                                   evaluable:true,
                                   pintaUltimaCaja:true
                                }
                        },
                        //9
                        /*{
                                "respuestas": [        
                                    {
                                       "t13respuesta":  "35",
                                         "t17correcta": "1"
                                     },
                                    {
                                       "t13respuesta":  "Océano Pacífico",
                                         "t17correcta": "2"
                                     },
                                    {
                                       "t13respuesta":  "inglés",
                                         "t17correcta": "3"
                                     },
                                    {
                                       "t13respuesta":  "España",
                                         "t17correcta": "4"
                                     },
                                     {
                                        "t13respuesta":  "50",
                                          "t17correcta": ""
                                      },
                                      {
                                         "t13respuesta":  "Océano Índico",
                                           "t17correcta": ""
                                       },
                                       {
                                          "t13respuesta":  "francés",
                                            "t17correcta": ""
                                        },
                                        {
                                           "t13respuesta":  "Francia",
                                             "t17correcta": ""
                                         }
                                ],
                                "preguntas": [
                                  {
                                  "t11pregunta": "El Continente Americano tiene "
                                  },
                                  {
                                  "t11pregunta": " países y  se caracteriza por estar rodeado de 3 océanos: el Océano Glaciar Ártico, el Océano Atlántico y el "
                                  },
                                  {
                                  "t11pregunta": ". Los idiomas que predominan son "
                                  },
                                  {
                                  "t11pregunta": " y español; los países en donde predomina el español fueron conquistados y colonizados por "
                                  },
                                  {
                                  "t11pregunta": " durante el siglo XVI, XVII Y XVIII, es decir, de los siglos de 1500 a 1800, en términos generales.<br>"
                                  },
                                ],
                                "pregunta": {
                                  "c03id_tipo_pregunta": "8",
                                  "t11pregunta": "",
                                   "t11instruccion": "",
                                   "respuestasLargas": true,
                                   "pintaUltimaCaja": false,
                                   "contieneDistractores": true
                                 }
                              },*/
                                //10
                                {
                                    "respuestas": [
                                        {
                                            "t13respuesta": "Opción 1",
                                            "t17correcta": "0"
                                        },
                                        {
                                            "t13respuesta": "Opción 2",
                                            "t17correcta": "1"
                                        }
                                       ],
                                        "preguntas": [
                                       {
                                        "c03id_tipo_pregunta": "13",
                                        "t11pregunta":"Las Cataratas del Niágara, la isla Manitoulin y la Bahía de Fundy son algunos de sus  lugares de interés. ",
                                        "valores": ['Canadá', 'Estados Unidos', ],
                                        "correcta"  : "0"
                                        },
                                       {
                                        "c03id_tipo_pregunta": "13",
                                        "t11pregunta":"Limita al norte con el mar Caribe, al sur con el océano Pacífico, al este con Colombia y al oeste con Costa Rica. ",
                                        "valores": ['Bolivia', 'Panamá', ],
                                        "correcta"  : "1"
                                        },
                                       {
                                        "c03id_tipo_pregunta": "13",
                                        "t11pregunta":"La capital de este país es Quito y su ciudad con más población es Guayaquil.",
                                        "valores": ['Venezuela', 'Ecuador', ],
                                        "correcta"  : "1"
                                        },
                                       {
                                        "c03id_tipo_pregunta": "13",
                                        "t11pregunta":"Alberga el sitio arqueológico conocido como Machu Picchu, el cual está declarado patrimonio cultural de la humanidad.",
                                        "valores": ['Colombia', 'Perú', ],
                                        "correcta"  : "1"
                                        },
                                       {
                                        "c03id_tipo_pregunta": "13",
                                        "t11pregunta":"Su principal elevación es el Nevado Sajama, su principal río es el Mamoré y su principal lago es el Titicaca.",
                                        "valores": ['Bolivia', 'Brasil', ],
                                        "correcta"  : "0"
                                        },
                                       {
                                        "c03id_tipo_pregunta": "13",
                                        "t11pregunta":"Es considerado estado libre asociado de Estados Unidos de América.",
                                        "valores": ['Puerto Rico', 'Costa Rica', ],
                                        "correcta"  : "0"
                                        }
                                        ],
                                        "pregunta": {
                                            "c03id_tipo_pregunta": "13",
                                            "t11pregunta": "<p>Elige el país que corresponde.<\/p>",
                                            "descripcion": "Reactivo",
                                            "variante": "editable",
                                            "anchoColumnaPreguntas": 60,
                                            "evaluable"  : true
                                        }
                                    },
                                    //11
                                    {
                                        "respuestas":[
                                       {
                                        "t13respuesta":"<img src='GI5E_B01_R10_01.png'/>",
                                        "t17correcta":"1",
                                        },
                                       {
                                        "t13respuesta":"<img src='GI5E_B01_R10_02.png'/>",
                                        "t17correcta":"2",
                                        },
                                       {
                                        "t13respuesta":"<img src='GI5E_B01_R10_03.png'/>",
                                        "t17correcta":"3",
                                        },
                                       {
                                        "t13respuesta":"<img src='GI5E_B01_R10_04.png'/>",
                                        "t17correcta":"4",
                                        },
                                       {
                                        "t13respuesta":"<img src='GI5E_B01_R10_05.png'/>",
                                        "t17correcta":"5",
                                        },
                                        {
                                         "t13respuesta":"<img src='GI5E_B01_R10_06.png'/>",
                                         "t17correcta":"6",
                                         },
                                         {
                                          "t13respuesta":"<img src='GI5E_B01_R10_07.png'/>",
                                          "t17correcta":"7",
                                          }
                                       ],
                                        "preguntas": [
                                        {
                                        "t11pregunta":"Uruguay "
                                        },
                                       {
                                        "t11pregunta":"Chile"
                                        },
                                        {
                                         "t11pregunta":"Colombia"
                                         },
                                       {
                                        "t11pregunta":"República Dominicana"
                                        },
                                       {
                                        "t11pregunta":"Jamaica"
                                        },
                                        {
                                         "t11pregunta":"Cuba"
                                         },
                                         {
                                          "t11pregunta":"Costa Rica"
                                          }
                                        ],
                                        "pregunta":{
                                        "c03id_tipo_pregunta":"12",
                                        "t11pregunta": "Relaciona las columnas según corresponda.",
                                        "t11instruccion": "",
                                        "altoImagen":"100px",
                                        "anchoImagen":"200px"
                                        }
                                        },
    //12 nueva
    { 
        "respuestas":[ 
       { 
        "t13respuesta":"Cordilleras", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Ejemplo de una cordillera", 
        "t17correcta":"2", 
        }, 
       { 
        "t13respuesta":"Monte Aconcagua", 
        "t17correcta":"3", 
        }, 
       { 
        "t13respuesta":"Llanos/llanuras", 
        "t17correcta":"4", 
        }, 
       { 
        "t13respuesta":"La Pradera Canadiense", 
        "t17correcta":"5", 
        }, 
       { 
        "t13respuesta":"Mesetas", 
        "t17correcta":"6", 
        }, 
       { 
        "t13respuesta":"Macizo Patagónico", 
        "t17correcta":"7", 
        }, 
       ], 
        "preguntas": [ 
        { 
        "t11pregunta":"Forma accidentada de la superficie terrestre, formada por montañas." 
        }, 
       { 
        "t11pregunta":"Los Andes" 
        }, 
       { 
        "t11pregunta":"Es la montaña más alta de América..." 
        }, 
       { 
        "t11pregunta":"Grandes extensiones de superficies planas, que pueden tener ligeras ondulaciones, conformadas por algunos valles o cuencas." 
        }, 
       { 
        "t11pregunta":"Es un ejemplo de llanura..." 
        }, 
       { 
        "t11pregunta":"Altiplanicies de una altura promedio, que en la cúspide pueden ser planas." 
        }, 
       { 
        "t11pregunta":"Es un ejemplo de meseta..." 
        }, 
        ], 
        "pregunta":{ 
        "c03id_tipo_pregunta":"12", 
        "t11pregunta": "Relaciona las columnas según corresponda.",
        "t11instruccion": "", 
        "altoImagen":"100px", 
        "anchoImagen":"200px" 
        } 
        },
        //13 nueva
        { 
            "respuestas":[ 
            { 
            "t13respuesta":"La placa Norteamericana, la del Caribe, la de Cocos, la de Nazca y la Sudamericana.", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"La placa norteamericana, la del Caribe, la de Cocos, la de Rivera y la Filipina.", 
            "t17correcta":"0", 
            }, 
           { 
            "t13respuesta":"La placa norteamericana, la Antártica, la de Cocos, la de Nazca y la Sudamericana.", 
            "t17correcta":"0", 
            }, 
           { 
            "t13respuesta":"La placa norteamericana, la del Caribe, la de Cocos, la Filipina y la Sudamericana.", 
            "t17correcta":"0", 
            }, 
           ],
            "pregunta":{ 
            "c03id_tipo_pregunta":"1", 
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cuáles son las principales placas tectónicas del continente americano?", 
            "t11instruccion": "", 
            } 
            }, 
        //14 nueva imagen
        { 
            "respuestas":[ 
            { 
            "t13respuesta":"En el océano Pacífico", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"En el océano Atlántico", 
            "t17correcta":"0", 
            }, 
           { 
            "t13respuesta":"En el océano Índico", 
            "t17correcta":"0", 
            }, 
           ],
            "pregunta":{ 
            "c03id_tipo_pregunta":"1", 
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿En qué océano se encuentra la mayor concentración de volcanes y de zonas sísmicas en el mundo?", 
            "t11instruccion": "<img src='GI5E_B01_R11.png' width = 900 height = 550px />", 
            } 
            }, 
        //15 nueva
        { 
            "respuestas":[ 
            { 
            "t13respuesta":"Cinturón de fuego", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"Falla de San Andrés", 
            "t17correcta":"0", 
            }, 
           { 
            "t13respuesta":"Laguna sísmica", 
            "t17correcta":"0", 
            }, 
           ],
            "pregunta":{ 
            "c03id_tipo_pregunta":"1", 
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cómo se le llama a la zona sísmica que atraviesa las aguas del Pacífico y sus costas?", 
            "t11instruccion": "", 
            } 
            }, 
        //16 nueva
        {
            "respuestas": [
                {
                    "t13respuesta": "F",
                    "t17correcta": "0"
                },
                {
                    "t13respuesta": "V",
                    "t17correcta": "1"
                }
               ],
                "preguntas": [
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El volcán de Fuego es uno de los más importantes de Chile.",
                "correcta"  : "0"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El volcán Popocatépetl es uno de los volcanes más importantes de América.",
                "correcta"  : "1"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El volcán Calbuco es de importancia turística para Chile.",
                "correcta"  : "1"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"El volcán Wolf se ubica en Estados Unidos al norte de Yellowstone.",
                "correcta"  : "0"
                },
               {
                "c03id_tipo_pregunta": "13",
                "t11pregunta":"La actividad del  volcán Wolf amenazó la población de especies únicas como las iguanas rosadas y las tortugas gigantes.",
                "correcta"  : "1"
                },
                ],
                "pregunta": {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
                    "descripcion": "Reactivo",
                    "variante": "editable",
                    "anchoColumnaPreguntas": 60,
                    "evaluable"  : true
                }
            },
        //17 nueva
        { 
            "respuestas":[ 
            { 
            "t13respuesta":"Son espacios específicos delimitados que presentan características como  situación geográfica, clima, relieve, etc.", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"Son zonas o lugares donde interactúa la naturaleza y el ser humano.", 
            "t17correcta":"0", 
            }, 
           { 
            "t13respuesta":"Son espacios geográficos designados únicamente para la protección de la fauna del planeta.", 
            "t17correcta":"0", 
            }, 
           { 
            "t13respuesta":"Son espacios desconocidos por el ser humano, jamás han sido explorados.", 
            "t17correcta":"0", 
            }, 
           ],
            "pregunta":{ 
            "c03id_tipo_pregunta":"1", 
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Qué son las regiones naturales?", 
            "t11instruccion": "", 
            } 
            },
        //18 nueva
        { 
            "respuestas":[ 
           { 
            "t13respuesta":"Su vegetación incluye matorrales, hierbas y árboles. Hay  dos estaciones, una húmeda y una seca, además, tiene largos periodos de sequía.", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"Áreas con árboles agrupados.", 
            "t17correcta":"2", 
            }, 
           { 
            "t13respuesta":"Lugar con poca agua, puede ser muy caliente o muy frío. La fauna y flora es muy escasa.", 
            "t17correcta":"3", 
            }, 
           { 
            "t13respuesta":"Áreas con temperaturas muy frías por lo que no crece abundante vegetación y  las especies que le habitan se han adaptado al clima.", 
            "t17correcta":"4", 
            }, 
           { 
            "t13respuesta":"Áreas planas y abiertas.", 
            "t17correcta":"5", 
            }, 
           { 
            "t13respuesta":"Área tropical en la que los árboles crecen muy alto, con altos registros de precipitación.", 
            "t17correcta":"6", 
            }, 
           { 
            "t13respuesta":"Zonas de clima húmedo caliente durante todo el año, podemos encontrar gorilas, perezosos, hipopótamos, etc.", 
            "t17correcta":"7", 
            }, 
           { 
            "t13respuesta":"Es el área más grande e incluye países de Europa, Asia, África y América.", 
            "t17correcta":"8", 
            }, 
           ], 
            "preguntas": [ 
            { 
            "t11pregunta":"Sabana" 
            }, 
           { 
            "t11pregunta":"Bosque" 
            }, 
           { 
            "t11pregunta":"Desierto" 
            }, 
           { 
            "t11pregunta":"Tundra" 
            }, 
           { 
            "t11pregunta":"Praderas" 
            }, 
           { 
            "t11pregunta":"Selva" 
            }, 
           { 
            "t11pregunta":"Región Ecuatorial" 
            }, 
           { 
            "t11pregunta":"Región Mediterránea" 
            }, 
            ], 
            "pregunta":{ 
            "c03id_tipo_pregunta":"12", 
            "t11pregunta":"Relaciona la columna según corresponda.",
            "t11instruccion": "", 
            "altoImagen":"100px", 
            "anchoImagen":"200px" 
            } 
            },
        //19 nueva
         /*{
                "respuestas": [        
                    {
                       "t13respuesta":  "Suelo escaso y poroso.<br>Los cactus son su principal flora.",
                         "t17correcta": "1"
                     },
                    {
                       "t13respuesta":  "Principalmente en<br>México y Estados Unidos",
                         "t17correcta": "5"
                     },
                    {
                       "t13respuesta":  "Industrias manufactureras",
                         "t17correcta": "2,6"
                     },
                    {
                       "t13respuesta":  "Industrias automotrices",
                         "t17correcta": "6,2"
                     },
                    {
                       "t13respuesta":  "Restaurantes",
                         "t17correcta": "3,4"
                     },
                    {
                       "t13respuesta":  "Servicios turísticos",
                         "t17correcta": "4,3"
                     },
                ],
                "preguntas": [
                  {
                  "t11pregunta": "<br><style>\n\
                            .table img{height: 90px !important; width: auto !important; }\n\
                            </style><table class='table' style='margin-top:-10px;'>\n\
                            <tr><td>Primarias</td><td>Secundarias</td><td>Terciarias</td></tr><tr><td>"
                  },
                  {
                  "t11pregunta": "</td><td>"
                  },
                  {
                  "t11pregunta": "</td><td>"
                  },
                  {
                  "t11pregunta": "</td></tr><tr><td>Ganadería</td><td>Industrias alimenticias</td><td>"
                  },
                  {
                  "t11pregunta": "</td></tr><tr><td>"
                  },
                  {
                  "t11pregunta": "</td><td>"
                  },
                  {
                  "t11pregunta": "</td><td>Transporte</td></tr><tr><td></td><td></td><td>Almacenamiento</td></tr></table>"
                  }
                ],
                "pregunta": {
                  "c03id_tipo_pregunta": "8",
                  "t11pregunta": "Arrastra las actividades económicas donde correspondan.",
                   "t11instruccion": "",
                   "respuestasLargas": true,
                   "pintaUltimaCaja": false,
                   "contieneDistractores": true
                 }
              } ,*/
        //tabla    
      /*  {
        "respuestas": [
            {
                "t13respuesta": "<sup>Suelo escaso y poroso.<br>Los cactus son su<br>principal flora.</sup>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<sup>Principalmente en México <br>y Estados Unidos.</sup>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<sup>Zorros, linces, osos, búhos,<br>renos, patos, insectos e<br>invertebrados.</sup>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<sup>Alaska, Canadá y<br>Estados Unidos.</sup>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<sup>Suelo alcalino<br>difícil de <br>cultivar.</sup>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<sup>En el sur,<br>comadrejas,<br>liebres y aves.</sup>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<sup>América del Norte<br>en E.U. y en<br>la Pampa Argentina.</sup>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<sup>Clima:<br>húmedo y muy<br>cálido, abundantes<br>precipitaciones.</sup>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<sup> Parte central del<br>Caribe y el Amazonas.</sup>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<sup> Costa Atlántica de<br>Colombia, Venezuela,<br>Brasil, Bolivia,<br>Paraguay y Argentina.</sup>",
                "t17correcta": "9"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td><b>Desierto</b></td><td><sup>Clima: seco.</sup><br>"
            },
            {
                "t11pregunta": "</td><td><sup>Fauna:<br>serpientes, halcones,<br>lagartijas, buitres,<br>algunos insectos<br>y roedores.</sup></td><td><sup>Ubicación:</sup><br>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Tundra</b></td><td><sup>Clima:<br>Frío extremo.<br>Suelo delgado y subsuelo<br>congelado.En zonas<br>pegadas al polo Norte.</sup><br><td><sup>Fauna:</sup><br>"
            },
            {
                "t11pregunta": "</td><td><sup>Ubicación:</sup><br>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Pradera</b></td><td><sup>Clima: cálidos-calientes</sup><br>"
            },
            {
                "t11pregunta": "<td><sup>Fauna:<br>En el norte zorros,<br>bisontes, venados<br>y caballos.</sup><br>"
            },
            {
                "t11pregunta": "</td><td><sup>Ubicación:</sup><br>"
            },
            {
                "t11pregunta": "</td><td><sup>Flora:<br>Trébol, girasoles<br>y juncales. </sup></td></tr><tr><td><b>Selva</b></td><td>"
            },
            {
                "t11pregunta": "<br><sup>Suelo ácido con<br>vegetación abundante.</sup></td><td><sup>Fauna:<br>Insectos, tapires,<br>tortugas, monos, aves y<br>mamíferos de pequeña altura.</sup></td><td><sup>Ubicación:</sup><br>"
            },
            {
                "t11pregunta": "</td><td><sup>Flora:<br>Caoba.</sup></td></tr><tr><td><b>Sabana</b></td><td><sup>Clima:<br>tropical.<br>Suelo arcilloso.</sup></td><td><sup>Fauna:<br>oso hormiguero, jaguar,<br>puma, nutria, simios,<br>anaconda, boa, serpiente<br>de cascabel. </sup></td><td><sup>Ubicación:</sup><br>"
            },
            {
                "t11pregunta": "</td><td><sup>Flora:<br> cedros, palmas<br<}>y orquídeas.</sup></td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },*/
     //18 tabla parte 1
{
    "respuestas": [
        {
            "t13respuesta": "Clima:<br>seco.",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Los cactus son su<br>principal flora.",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "Flora:<br>escaza",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "Zorros, linces, osos,<br>búhos, renos, patos,<br>insectos e<br>invertebrados.",
            "t17correcta": "4"
        },
        {
            "t13respuesta": "Clima:<br>cálidos-calientes",
            "t17correcta": "5"
        },
        {
            "t13respuesta": "En el norte zorros,<br>bisontes, venados<br>y caballos.",
            "t17correcta": "6"
        },
       /* {
            "t13respuesta": "<sup>En el sur,<br>comadrejas,<br>liebres y aves.</sup>",
            "t17correcta": "6"
        },
        {
            "t13respuesta": "<sup>América del Norte<br>en E.U. y en<br>la Pampa Argentina.</sup>",
            "t17correcta": "7"
        },*/
    ],
    "preguntas": [
        {
            "t11pregunta": "<br><style>\n\
            .table img{height: 90px !important; width: auto !important; }\n\
            </style><table class='table' style='margin-top:-10px;'>\n\
            <tr><td><b>Desierto</b></td><td><br>"
        },
        {
            "t11pregunta": "</td><td>Flora:<br>"
        },
        {
            "t11pregunta": "<td>Fauna:<br>serpientes, halcones,<br>lagartijas, buitres,<br>algunos insectos<br>y roedores.</td></tr><tr><td><b>Tundra</b></td><td><b>Clima:<br>Frío extremo,<br>congelado.</b><br><td>"
        },
        {
            "t11pregunta": "</td><td><b>Fauna:</b><br>"
        },
        {
            "t11pregunta": "</td></tr><tr><td><b>Pradera</b></td><td><br>"
        },
        {
            "t11pregunta": "<td><b>Flora:<br>Trébol, girasoles<br>y juncales.</b><td><b<Fauna:</b><br>"
        },
        {
            "t11pregunta": "</td></tr></table>"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "Arrastra los elementos y completa la tabla.",
        "t11instruccion": "",
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "contieneDistractores": true
    }
},
//19 tabla aprte 2
{
    "respuestas": [
       
        {
            "t13respuesta": "Árboles maderables y<br>abundantes helechos</sup>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "Oso hormiguero, jaguar,<br>puma, nutria,<br>simios, anaconda.</sup>",
            "t17correcta": "2"
        },
    ],
    "preguntas": [
        {
            "t11pregunta": "<br><style>\n\
            .table img{height: 90px !important; width: auto !important; }\n\
            </style><table class='table' style='margin-top:-10px;'>\n\
            <tr><td><b>Selva</b></td><td>Clima:<br>húmedo y muy cálido, abundantes precipitaciones. <br><td>Flora:<br>"
        },
        {
            "t11pregunta": "</td><td>Fauna:<br>Insectos, tapires,<br>tortugas, monos, aves y<br>mamíferos de pequeña altura.</sup></td></tr><tr><td><b>Sabana</b></td><td><b>Clima:<br>tropical.</b></td><td><b>Flora:<br>cedros, palmas<br>y orquídeas.</b></td><td><b>Fauna:</b><br>"
        },
        {
            "t11pregunta": "</td></tr></table>"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "Arrastra los elementos y completa la tabla.",
        "t11instruccion": "",
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "contieneDistractores": true
    }
},
    //20 nueva
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Se refiere a la variedad de especies que se puede encontrar en un paisaje y ecosistema determinado.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Se refiere a la variedad de espacios geográficos en el planeta.", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Se refiere a las plantas y animales que viven en una región o ecosistemas determinados.", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué es la biodiversidad?", 
        "t11instruccion": "", 
        } 
    }, 
   
]