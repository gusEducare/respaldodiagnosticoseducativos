
json=[  
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EIsaac Newton\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAntoine Lavoiser\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EGalileo Galilei\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDemtri Mendeleiev\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br /> <br />¿Cómo se llama el científico que descubrió la ley de la gravedad?"
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "Un objeto se mantendrá en reposo o movimiento a menos que una fuerza externa actúe sobre el. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "La aceleración es en dirección a la fuerza, proporcionada a la intensidad e inversamente proporcional a la masa del objeto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cuando el objeto A ejerce una fuerza sobre un objeto B, el objeto B ejercerá una fuerza igual pero en sentido opuesto. ",
                "t17correcta": "3"
            }

        ],
        "preguntas": [
            {
                "t11pregunta": "Primera Ley de Newton"
            },
            {
                "t11pregunta": "Segunda Ley de Newton"
            },
            {
                "t11pregunta": "Tercera Ley de Newton"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>resistencia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>contacto<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>opuesta<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>calor<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>fuerza<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>Fricción:<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;causada por dos objetos en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y actúa en dirección&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;al movimiento.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='NI4E_B04_A04_01.png'/>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='NI4E_B04_A04_02.png'/>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='NI4E_B04_A04_03.png'/>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='NI4E_B04_A04_04.png'/>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Primera Ley de Newton"
            },
            {
                "t11pregunta": "Segunda Ley de Newton"
            },
            {
                "t11pregunta": "Tercera Ley de Newton"
            },
            {
                "t11pregunta": "Fricción"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "anchoImagen":"250",
            "altoImagen":"150",
             "t11pregunta": "Arrastra la imagen al espacio que corresponda. <br /><br />Relaciona los conceptos con la imagen que les corresponda."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La fuerza de fricción disminuye con la rugosidad de las superficies y aumenta con superficies lisas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La fuerza de fricción depende del material con el que están hechos los cuerpos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Podríamos caminar o andar en bici aunque no existiera la fricción.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los lubricante son sustancias que facilitan el movimiento al hacer las superficies más parejas o lisas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La fricción transforma el calor en energía cinética, suponiendo una aceleración de los objetos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EElectrones\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EProtones\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ENeutrones\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.  <br /><br />¿Qué partículas subatómicas producen la electricidad al moverse?"
      }
   },
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Frotamiento",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Contacto",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Calentamiento",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Reacciones químicas",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona las respuestas correctas.   <br /><br />Son dos formas de producir electricidad estática."
      }
   },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEs el flujo constante de electrones a lo largo de un circuito.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEs la interrupción del flujo de electrones para producir energía térmica.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEs una medida que sirve para cuantificar el número de electrones que pasan por un circuito.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.  <br /><br />¿Qué es la electricidad?"
      }
   },
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La luz es radiación electromagnética.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El fotón crea un campo eléctrico y magnético que se mueve creando ondas de diferentes longitudes que forman el espectro electromagnético.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ojo humano puede ver todo el espectro electromagnético.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La luz blanca está formada por la radiación de todos los colores.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La refracción es el cambio de dirección y velocidad que experimenta una onda al pasar de un medio a otro con diferente índice refractivo.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>opacos<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>luz<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>transparentes<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>translúcidos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>penumbra<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>Un fenómeno muy común es el de las sombras. Este se produce debido a que los materiales&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;no permiten el paso de la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, de modo que cuando un rayo incide sobre ellos se proyecta su sombra del mismo lado de donde recibe el rayo de luz. En cambio cuando la luz incide en materiales&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;estos dejan pasar la luz y posibilitan ver los objetos existentes al otro lado. Los materiales&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;permiten el paso de luz pero de manera difusa por lo que los objetos ubicados del otro lado no se ven con claridad. Cuando un lugar se encuentra en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;significa que la luz que entra a este es muy poca, pero no alcanza a estar oscuro por completo.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='NI4E_B04_A11_01.png'/>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='NI4E_B04_A11_02.png'/>",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Reflexión"
            },
            {
                "t11pregunta": "Refracción"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Arrastra la imagen al espacio que corresponda."
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "<p>arcoiris<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>refracción<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>dirección<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>siete<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>El&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;es un fenómeno natural ocasionado por la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de la luz al pasar por las gotas de agua de la lluvia, del rocío o de la niebla. Estas gotas además de cambiar la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de la luz, separan cada color que forman los rayos del Sol, lo que crea la clásica figura arqueada de los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;colores del arcoiris.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
   {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Eclipse proviene del griego “ekleipsis” que significa aparición o encuentro.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El eclipse se da cuando la luz que procede de un cuerpo celeste es bloqueada por un cuerpo eclipsante.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En nuestra Tierra existen más de tres tipos de eclipses.  ",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para ver un eclipse de Sol se requiere utilizar lentes especiales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cada punto de la superficie de la Tierra tiene en promedio un eclipse total de Sol cada 360 años.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    }
];