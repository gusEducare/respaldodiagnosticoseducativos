json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Son alimentos que puedes consumir sin límite de cantidad.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Son alimentos cuyo consumo debes moderar.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Son alimentos que casi no tienes que consumir.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>En el plato del buen comer, la fracción del color verde indica que:",
            "t11instruccion": "",
        }
    },

    //2

    {
        "respuestas": [
            {
                "t13respuesta": "Carne de res, carne de cerdo, leche, queso.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Manzana, lechuga, espinacas, durazno, uvas.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Maíz, trigo, pan de caja, tostadas, tortillas.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Son alimentos de la zona roja en el plato del buen comer."
            },
            {
                "t11pregunta": "Son alimentos de la zona verde en el plato del buen comer."
            },
            {
                "t11pregunta": "Son alimentos de la zona amarilla en el plato del buen comer."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }

    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Son la principal fuente<br> de energía para el<br> funcionamiento del cuerpo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Son necesarias para<br> el crecimiento y<br> reparación del tejido.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Son los encargados<br> de mantener el funcionamiento<br>de los órganos.",
                "t17correcta": "3"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Nutrientes</td><td>¿Por qué son necesarios?</td></tr><tr><td><b>Carbohidratos</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Proteínas</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Vitaminas</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Sin olor",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sin sabor",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sin color",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sin microorganismos",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "A temperatura ambiente",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Sin minerales",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>Son las características del agua potable.",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
          
            {
                "t13respuesta": "70%",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "50%",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "90%",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Llevar un registro de las vacunas más importantes hasta que cumplimos 12 años.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Llevar un registro de las vacunas más importantes hasta que cumplimos 3 años.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Llevar un registro de las vacunas más importantes hasta que cumplimos 18 años.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
           /* {
                "t13respuesta": "Inocuos",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Potables",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Desinfectados",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
              /*{
                "t13respuesta": "Alimentos chatarra",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Alimentos de origen animal",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Alimentos inocuos",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué porcentaje del cuerpo está constituido por agua?", "Es una de las funciones de la cartilla de vacunación:", /*"Los alimentos que están libres de microorganismos y sustancias que nos puedan dañar se dicen que son:"*//*¿Cómo se les conoce a los alimentos que aportan pocos nutrientes y altos contenidos de azúcar, grasa y sal que pueden dañar la salud?"*/],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //6 nueva
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Antígenos", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Hormonas", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Virus", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cómo se llaman en general  los organismos microscópicos externos que causan enfermedades en nuestro cuerpo?", 
        "t11instruccion": "", 
        } 
        }, 
    //7
    { 
        "respuestas":[ 
       { 
        "t13respuesta":"Se encargan de exterminar a los antígenos.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Encargadas de  producir anticuerpos.", 
        "t17correcta":"2", 
        }, 
       { 
        "t13respuesta":"Son los encargados de proteger y viajar por todo el sistema circulatorio hasta cada uno de los órganos, protegiendo especialmente en donde se llevan cabo secreciones de nuestro cuerpo.", 
        "t17correcta":"3", 
        }, 
       { 
        "t13respuesta":"Se encarga de avisarles a las células B si se necesita mayor número de anticuerpos.", 
        "t17correcta":"4", 
        }, 
       { 
        "t13respuesta":"Son las encargadas de mandar la señal a las células B de parar la producción de anticuerpos.", 
        "t17correcta":"5", 
        }, 
       { 
        "t13respuesta":"Se alimentan de microorganismos.", 
        "t17correcta":"6", 
        }, 
       ], 
        "preguntas": [ 
        { 
        "t11pregunta":"Células T: destructoras" 
        }, 
       { 
        "t11pregunta":"Células B" 
        }, 
       { 
        "t11pregunta":"Anticuerpos" 
        }, 
       { 
        "t11pregunta":"Células T: colaboradoras" 
        }, 
       { 
        "t11pregunta":"Células T: reguladoras" 
        }, 
       { 
        "t11pregunta":"Fagocitos" 
        }, 
        ], 
        "pregunta":{ 
        "c03id_tipo_pregunta":"12", 
        "t11pregunta":"Relaciona las columnas según corresponda.",
        "t11instruccion": "", 
        "altoImagen":"100px", 
        "anchoImagen":"200px" 
        } 
        } ,
  /*  //6
    {
        "respuestas": [

            {
                "t13respuesta": "Difteria",
                "t17correcta": "1,4,7,9,10"
            },
            {
                "t13respuesta": "Sarampión",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "Virus del <br>papiloma humano",
                "t17correcta": "3,6"
            },
            {
                "t13respuesta": "Tosferina",
                "t17correcta": "1,4,7,9,10"
            },
            {
                "t13respuesta": "Rubéola",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "Protege contra<br> el cáncer",
                "t17correcta": "3,6"
            },
            {
                "t13respuesta": "Tétanos",
                "t17correcta": "1,4,7,9,10"
            },
            {
                "t13respuesta": "Parotiditis",
                "t17correcta": "2,5,8"
            },
            {
                "t13respuesta": "Poliomielitis",
                "t17correcta": "1,4,7,9,10"
            },
            {
                "t13respuesta": "Infección por<br> <i>Haemophilus<br> influenzae</i> tipo B",
                "t17correcta": "1,4,7,9,10"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Pentavalente Acelular</td><td>SRP (triple viral)</td><td>VPH</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td></td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td></td><td></td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td></td><td></td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla. <br><br>Elige las enfermedades contra las que protegen las siguientes vacunas:",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Antígenos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Hormonas",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Virus",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Tétanos",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Viruela",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Influenza",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿Cómo se llaman los organismos externos que causan enfermedades en nuestro cuerpo?",
                "Es una enfermedad ocasionada por bacterias. Provoca babeo, fiebre, bloqueo de la mandíbula para abrir la boca y deglutir, micción o defecación incontrolables."],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },*/
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Mantiene la integridad<br> de las mucosas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Aumenta respuesta<br> inmunológica.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Producción de interferón<br> que previene infecciones.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Antioxidantes",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Proliferación celular y<br> la respuesta inmunológica.",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Nutrientes</td><td>Función</td></tr><tr><td><b>Vitamina A</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Vitamina E</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Vitamina C</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Flavonoides</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td><b>Hierro</b></td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
   /* //9
    {
        "respuestas": [
            {
                "t13respuesta": "pubertad",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "adolescentes",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "cuerpo",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "niñez",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "adultos",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "cabello",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "La "
            },
            {
                "t11pregunta": " es la fase cuando dejamos de ser niños para convertirnos en "
            },
            {
                "t11pregunta": ". Durante esta fase, el "
            },
            {
                "t11pregunta": " de niñas y niños experimenta varios cambios.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }

    },*/
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "Testículos",
                "t17correcta": "1,3,5"
            },
            {
                "t13respuesta": "Vello facial",
                "t17correcta": "2,4,6"
            },
            {
                "t13respuesta": "Vulva",
                "t17correcta": "3,1,5"
            },
            {
                "t13respuesta": "Ensanchamiento de caderas",
                "t17correcta": "4,2,6"
            },
            {
                "t13respuesta": "Pene",
                "t17correcta": "5,1,3"
            },
            {
                "t13respuesta": "Crecen los senos",
                "t17correcta": "6,2,4"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Caracteres sexuales primarios</td><td>Caracteres sexuales secundarios</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos para completar la tabla.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "Testosterona",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Progesterona",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Estrógenos",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Prolactina",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Endorfina",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Cuáles son las hormonas responsables de los cambios físicos durante la adolescencia?",
            "t11instruccion": "",
        }
    },
   /* //12
    {
        "respuestas": [
            {
                "t13respuesta": "Testículos",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Glande",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Escroto",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Uretra",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Ovario",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Vejiga",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Ovario",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Uretra",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Próstata",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿En qué parte se producen los espermatozoides?",
                "¿Qué forma parte exclusivamente del órgano interno masculino?", "¿Qué forma parte del órgano interno femenino?"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },*/
    //11 nueva
    { 
        "respuestas":[ 
       { 
        "t13respuesta":"Nos brinda diferentes recursos como: alimentos, medicinas, materiales para elaborar vestimentas, etc.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Contribuye a la formación de la identidad cultural.", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Nos ayuda a tener más cosas en la casa.", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Sin ella no habría razón de ir de vacaciones a diferentes partes.", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"2", 
        "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Por qué es importante la biodiversidad?", 
        "t11instruccion": "", 
        } 
        }, 
        //12 nueva
        { 
            "respuestas":[ 
            { 
            "t13respuesta":"Porque alberga entre el 60 y 70% de las especies conocidas en el planeta.", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"Porque tiene una gran variedad de alimentos.", 
            "t17correcta":"0", 
            }, 
           { 
            "t13respuesta":"Porque alberga entre el 60 y 70% de la riqueza de las especies en el planeta.", 
            "t17correcta":"0", 
            }, 
           ],
            "pregunta":{ 
            "c03id_tipo_pregunta":"1", 
            "t11pregunta": "Selecciona la respuesta correcta. <br><br>¿Por qué México se considera un país megadiverso?", 
            "t11instruccion": "", 
            } 
            }, 
        //13 nueva
        { 
            "respuestas":[ 
           { 
            "t13respuesta":"Es en el que se agrupan las bacterias.", 
            "t17correcta":"1", 
            }, 
           { 
            "t13respuesta":"Es en el que se agrupan los protozoarios como las amibas", 
            "t17correcta":"2", 
            }, 
           { 
            "t13respuesta":"Es en el que se agrupan los hongos.", 
            "t17correcta":"3", 
            }, 
           ], 
            "preguntas": [ 
            { 
            "t11pregunta":"Reino Monera" 
            }, 
           { 
            "t11pregunta":"Reino Protista" 
            }, 
           { 
            "t11pregunta":"Reino Fungi" 
            }, 
            ], 
            "pregunta":{ 
            "c03id_tipo_pregunta":"12", 
            "t11pregunta":"Relaciona las columnas según corresponda.",
            "t11instruccion": "", 
            "altoImagen":"100px", 
            "anchoImagen":"200px" 
            } 
            },
    //14 nueva
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la reproducción asexual participan dos sexos: uno masculino y otro femenino.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la reproducción sexual un solo ser vivo por sí mismo puede generar otro idéntico a él.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las plantas pueden reproducirse de forma sexual y asexual.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La polinización es el proceso por el cual el polen llega al pistilo para llevar a cabo la fecundación.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los elementos naturales como el viento y el agua ayudan a facilitar el logro de la polinización.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p> Elige falso (F) o verdadero (V) según corresponda.</p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //15 nueva
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"Ovíparos y vivíparos", 
        "t17correcta":"1", 
        }, 
       { 
        "t13respuesta":"Autótrofos y heterótrofos", 
        "t17correcta":"0", 
        }, 
       { 
        "t13respuesta":"Marsupiales y omnívoros", 
        "t17correcta":"0", 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta. <br><br>De acuerdo con la forma en que nacen, los animales se pueden clasificar en:", 
        "t11instruccion": "", 
        } 
        }, 
];
