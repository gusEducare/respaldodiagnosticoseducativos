 json=[ 
  
 {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAlimentos chatarra.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAlimentos inocuos\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAlimentos de origen animal\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },///////////////////1
         {  
            "t13respuesta":"\u003Cp\u003E70%\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E50%\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E90%\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },////////////////////////////2
          {  
            "t13respuesta":"\u003Cp\u003EAgua potable\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua dulce\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAgua entubada\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },/////////////////////3
          ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cómo se le conoce a los alimentos que aportan pocos nutrientes y altos contenidos de azúcar, grasa y sal, y que además pueden dañar la salud?",
         "¿Qué porcentaje del cuerpo está constituido por agua?",
         "El agua que no tiene color, sabor, olor y  está libre de microorganismos, se conoce como..."]
      }
   },/////////2
   {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B05_A02_01.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A02_02.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A02_03.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B05_A02_04.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B05_A02_05.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A02_06.png",
              "t17correcta": "5,6,7",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A02_07.png",
              "t17correcta": "6,5,7",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A02_08.png",
              "t17correcta": "7,6,5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A02_09.png",
              "t17correcta": "8,9",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A02_10.png",
              "t17correcta": "9,8",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra los elementos y completa la tabla.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B05_A02_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "163,3", "cuadrado", "211, 49", ".","transparent"]},
          {"Contenedor": ["", "213,3", "cuadrado", "211, 49", ".","transparent"]},
          {"Contenedor": ["", "263,3", "cuadrado", "211, 49", ".","transparent"]},
          {"Contenedor": ["", "313,3", "cuadrado", "211, 49", ".","transparent"]},
          {"Contenedor": ["", "363,3", "cuadrado", "211, 49", ".","transparent"]},
          {"Contenedor": ["", "163,216", "cuadrado", "212, 49", ".","transparent"]},
          {"Contenedor": ["", "213,216", "cuadrado", "212, 49", ".","transparent"]},
          {"Contenedor": ["", "263,216", "cuadrado", "212, 49", ".","transparent"]},
          {"Contenedor": ["", "163,430", "cuadrado", "211, 49", ".","transparent"]},
          {"Contenedor": ["", "213,430", "cuadrado", "211, 49", ".","transparent"]}
      ]
  }, //////////3
   {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B05_A03_01.png",
              "t17correcta": "0,1,2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A03_02.png",
              "t17correcta": "0,1,2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A03_03.png",
              "t17correcta": "0,1,2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B05_A03_04.png",
              "t17correcta": "3,4,5",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B05_A03_05.png",
              "t17correcta": "3,4,5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B05_A03_06.png",
              "t17correcta": "3,4,5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
           "t11pregunta": "Arrastra los elementos y completa la tabla.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B05_A03_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "214,3", "cuadrado", "318, 48", ".","transparent"]},
          {"Contenedor": ["", "264,3", "cuadrado", "318, 48", ".","transparent"]},
          {"Contenedor": ["", "314,3", "cuadrado", "318, 48", ".","transparent"]},
          {"Contenedor": ["", "214,323", "cuadrado", "318, 48", ".","transparent"]},
          {"Contenedor": ["", "264,323", "cuadrado", "318, 48", ".","transparent"]},
          {"Contenedor": ["", "314,323", "cuadrado", "318, 48", ".","transparent"]}
      ]
  }, //////////4
     {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reproducción asexual es más rápida que la reproducción sexual.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las plantas tienen la capacidad de reproducirse de manera sexual y asexual.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los estambres se consideran el aparato femenino de la planta.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El polen se produce en los estambres y tiene que pasar al pistilo para reproducirse.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Animales como abejas y aves ayudan al proceso de polinización.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },/////////////5
     {
        "respuestas": [
            {
                "t13respuesta": "<p>crecimiento<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>nutrición<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>parásitos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>saprófitos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>simbióticos<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Al igual que cualquier planta o animal, los hongos aumentan de tamaño a lo largo de su vida, proceso conocido como&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Para crecer, un hongo requiere absorber nutrientes que encuentra en su medio. De acuerdo con su tipo de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, los hongos se clasifican en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;(todos aquellos que dañan o enferman a otros organismos, como el pie de atleta en el ser humano),&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>(&nbsp;todos aquellos que descomponen a las plantas y animales que mueren) y&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;(aquellos hongos que se asocian con otros organismos beneficiándose mutuamente).<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },////////////////6
     {
        "respuestas": [
            {
                "t13respuesta": "Enfermedades causadas por microorganismos presentes en los alimentos. ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Previenen el desarrollo de microorganismos en los alimentos. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Representan un método para eliminar los microorganismos de los alimentos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Habitan en cualquier ambiente, pueden tener consecuencias negativas para la salud y causar enfermedades. ",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Cólera, tifoidea y amebiasis"
            },
            {
                "t11pregunta": "Temperaturas bajas"
            },
            {
                "t11pregunta": "Temperaturas altas "
            },
            {
                "t11pregunta": "Microorganismos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona con líneas las columnas según corresponda."
        }
    },//////////////7
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ETransferencia de energía térmica entre dos cuerpos de diferente temperatura.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEs una cualidad de la materia.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEs un proceso que implica el desgaste de sus elementos.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Cuál es la definición más completa de calor?"
      }
   },/////////////////8
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Fricción",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Contacto",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Reproducción térmica",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Termogénesis",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>Son formas de generar calor."
      }
   },/////////////////9
   {
        "respuestas": [
            {
                "t13respuesta": "<p>arcoiris<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>refracción<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>dirección <\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>siete<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;es un fenómeno natural ocasionado por la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de la luz al pasar por las gotas de agua de la lluvia, del rocío o de la niebla. Estas gotas además de cambiar la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de la luz, separan cada color que forman los rayos del Sol, creando la clásica figura arqueada de los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;colores del arcoiris.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },///////10
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Eclipse proviene del griego “ekleipsis” que significa aparición o encuentro.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El eclipse se da cuando la luz que procede de un cuerpo celeste es bloqueada por un cuerpo eclipsante.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En nuestra Tierra existen más de tres tipos de eclipses.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para ver un eclipse de Sol se requiere utilizar lentes especiales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cada punto de la superficie de la Tierra tiene en promedio un eclipse total de Sol cada 360 años.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    }
   ];