json = [
    {
        //1 Verdadero o falso
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La materia puede definirse como todo aquello que tiene masa; por lo tanto, no ocupa un lugar en el espacio.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los factores que diferencian cada estado son el movimiento de las partículas y su fluidez.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el estado sólido, la materia no tiene forma ni volumen constante.",
                "correcta"  : "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el estado líquido, la materia tiene un cuerpo fluido que adopta la forma del recipiente que lo contiene.",
                "correcta"  : "1"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el estado gaseoso, la materia tiene forma y volumen fijo.",
                "correcta"  : "0"
            } ,
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existe un cuarto estado de la materia llamado plasma.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero (V) o falso (F) según corresponda:<\/p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        },
    },
    
    //2 Arrastra imagen 
    {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B03_A02_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B03_A02_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B03_A02_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          
         
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra la imagen al espacio que corresponda <br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B03_A02_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "234,56", "cuadrado", "110, 110", ".","transparent"]},
          {"Contenedor": ["", "234,266", "cuadrado", "110, 110", ".","transparent"]},
          {"Contenedor": ["", "234,466", "cuadrado", "110, 110", ".","transparent"]}
      ]
  },
  //3 arrastra corta
  
   {
        "respuestas": [
            {
                "t13respuesta": "<p>evaporación<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>condensación <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>precipitación<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>transpiración<\/p>",
                "t17correcta": "4,5"
            },
            {
                "t13respuesta": "<p>respiración.<\/p>",
                "t17correcta": "5,4"
            },
            
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br><br>La <\/p>"
            },
            {
                "t11pregunta": "<p> consiste en el cambio de líquido a vapor por el aumento de la temperatura. Por el contrario, en la <\/p>"
            },
            {
                "t11pregunta": "<p> el vapor se transforma en líquido cuando disminuye la temperatura en el ambiente. Por otro lado, la <\/p>"
            },
            {
                "t11pregunta": "<p> se refiere a la caída del agua en forma de lluvia, granizo o nieve. Además, el agua también pasa a los seres vivos de forma líquida, pero estos la regresan al ambiente por medio de la <\/p>"
            },
            {
                "t11pregunta": "<p> y <\/p>"
            },
             {
                "t11pregunta": "<p> . <\/p>"
            },
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    
    //4
     {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B03_A04_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B03_A04_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B03_A04_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B03_A04_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B03_A04_06.png",
              "t17correcta": "4",
              "columna":"1"
          },
          
          
         
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra la imagen al espacio que corresponda. <br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B03_A04_10.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "123,55", "cuadrado", "120, 120", ".","transparent"]},
          {"Contenedor": ["", "123,258", "cuadrado", "120, 120", ".","transparent"]},
          {"Contenedor": ["", "123,461", "cuadrado", "120, 120", ".","transparent"]},
          {"Contenedor": ["", "367,109", "cuadrado", "120, 120", ".","transparent"]},
          {"Contenedor": ["", "367,410", "cuadrado", "120, 120", ".","transparent"]}
      ]
  },
  
  //5
  {
        "respuestas": [
            {
                "t13respuesta": "<p>limitada<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>salada<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>dulce<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>sólida<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>líquida<\/p>",
                "t17correcta": "5"
            },
            
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br><br>La disponibilidad de agua para los seres vivos que habitamos el planeta es<\/p>"
            },
            {
                "t11pregunta": "<p> , ya que el 97% de la totalidad de este recurso es <\/p>"
            },
            {
                "t11pregunta": "<p> y no podemos consumirla. Solo 3% del agua es <\/p>"
            },
            {
                "t11pregunta": "<p> y de ella depende la mayoría de organismos que viven en la Tierra. De este pequeño porcentaje de agua dulce, 2% se halla en forma <\/p>"
            },
            {
                "t11pregunta": "<p> , como hielo. Entonces, solo el 1% del agua que se encuentra de manera <\/p>"
            },
             {
                "t11pregunta": "<p> puede aprovecharse. <\/p>"
            },
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    
    // 6 ordena elementos 
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Lavar correctamente las manos, utensilios e ingredientes.<\/p>",
                "t17correcta": "0",
                etiqueta:"paso 1"
            },
            {
                "t13respuesta": "<p>Separar los alimentos crudos de los cocidos.<\/p>",
                "t17correcta": "1",
                etiqueta:"paso 2"
            },
            {
                "t13respuesta": "<p>Cocinar muy bien alimentos como carne, pollo y huevo.<\/p>",
                "t17correcta": "2",
                etiqueta:"paso 3"
            },
            {
                "t13respuesta": "<p>Guardar los alimentos en el refrigerador.<\/p>",
                "t17correcta": "3",
                etiqueta:"paso 4"
            },
            {
                "t13respuesta": "<p>Descongelar adecuadamente los alimentos.<\/p>",
                "t17correcta": "4",
                etiqueta:"paso 5"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Ordena los pasos para lograr un manejo higiénico de los alimentos.<br><\/p>",
        }
    }, 
    
    //7 verdadero falso 

    {
       
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cocción se refiere al proceso de la aplicación de calor para que los alimentos sean comestibles.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cocción hace más difícil la digestión de los alimentos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cocinar por más tiempo del requerido un alimento no produce pérdida de sus propiedades nutrimentales.",
                "correcta"  : "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A través de la cocción se eliminan microorganismos que pueden afectar la salud. ",
                "correcta"  : "1"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se requiere más tiempo de cocción para un huevo que para un pedazo de carne de res.",
                "correcta"  : "0"
            } ,
            
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero (V) o falso (F) según corresponda:<\/p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        },
    },
    
    //8 Relaciona columnas
    
     {
        "respuestas": [
            {
                "t13respuesta": " Enfermedades causadas por microorganismos presentes en los alimentos. ",
                
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Previenen el desarrollo de microorganismos en los alimentos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Representan un método para eliminar los microorganismos de los alimentos.",
                
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Habitan en cualquier ambiente, pueden tener consecuencias negativas para la salud y causar enfermedades. ",
                "t17correcta": "4"
            },
            
        ],
        "preguntas": [
            {
                "t11pregunta": "Cólera, tifoidea y amebiasis"
            },
            {
                "t11pregunta": "Temperaturas bajas"
            },
            {
                "t11pregunta": "Temperaturas altas"
            },
            {
                "t11pregunta": "Microorganismos"
            },
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda"
             
            
        }
    },
    
    // 9 opcion multiple
    
    {  
      "respuestas":[
         {  
            "t13respuesta": "Transferencia de energía térmica entre dos cuerpos de diferente temperatura.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Es una cualidad de la materia.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Es un proceso que implica el desgaste de sus elementos.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta <br> ¿Cuál es la definición más completa de calor?"],
         "preguntasMultiples": true,
         "columnas":1,
          
        
      }
   },
   
   // 10 respuesta multiple
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Fricción",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Contacto",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Reproducción térmica",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Termogénesis",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Son formas de generar calor."
      }
   },

    
    
        
   
    
];
