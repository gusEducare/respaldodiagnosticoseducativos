json=[
  ////1
   {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B02_A01_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A01_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A01_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A01_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A01_06.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A01_07.png",
              "t17correcta": "5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra los elementos que completen la información de la tabla:<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B02_A01_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "213,217", "cuadrado", "210, 50", ".","transparent"]},
          {"Contenedor": ["", "262,217", "cuadrado", "210, 50", ".","transparent"]},
          {"Contenedor": ["", "313,217", "cuadrado", "210, 50", ".","transparent"]},
          {"Contenedor": ["", "213,427", "cuadrado", "210, 50", ".","transparent"]},
          {"Contenedor": ["", "262,427", "cuadrado", "210, 50", ".","transparent"]},
          {"Contenedor": ["", "313,427", "cuadrado", "210, 50", ".","transparent"]}
      ]
  },
  //2
   {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B02_A02_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A02_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A02_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A02_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A02_08.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A02_09.png",
              "t17correcta": "6",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A02_10.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A02_11.png",
              "t17correcta": "7",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": " Arrastra las palabras a la etapa que corresponda:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B02_A02_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":true
          
      },
      "contenedores": [
          {"Contenedor": ["", "154,141", "cuadrado", "115, 51", ".","transparent"]},
          {"Contenedor": ["", "154,264", "cuadrado", "115, 51", ".","transparent"]},
          {"Contenedor": ["", "154,388", "cuadrado", "115, 51", ".","transparent"]},
          {"Contenedor": ["", "154,510", "cuadrado", "115, 51", ".","transparent"]},
          {"Contenedor": ["", "259,141", "cuadrado", "115, 51", ".","transparent"]},
          {"Contenedor": ["", "259,388", "cuadrado", "115, 51", ".","transparent"]},
          {"Contenedor": ["", "321,141", "cuadrado", "115, 51", ".","transparent"]},
          {"Contenedor": ["", "321,388", "cuadrado", "115, 51", ".","transparent"]}
      ]
  },
  //3
  {  
      "respuestas":[  
         {  
            "t13respuesta":"Generar un ser similar a sus padres para que la especie se conserve.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Producir gametos que puedan ser fertilizados.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Dar lugar a organismos con baja adaptación a su medio.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Conservar la especie sin modificaciones a sus características.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta:<br/><br/> ¿Cuál es el resultado de la reproducción?"
      }
   },
   //4
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La reproducción asexual es más rápida que la reproducción sexual.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las plantas tienen la capacidad de reproducirse de manera sexual y asexual.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los estambres se consideran el aparato femenino de la planta.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El polen se produce en los estambres y tiene que pasar al pistilo para reproducirse.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Animales como abejas y aves ayudan al proceso de polinización.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda:",
            "descripcion":"",
            "evaluable":false,
            "evidencio": false
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Los animales se impregnan de polen y lo depositan en otra flor.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El viento transporta el polen, es frecuente en árboles.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se realiza a través del agua.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "La flor se fertiliza con su propio polen.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Polinización zoófila."
            },
            {
                "t11pregunta": "Polinización anemófila."
            },
            {
                "t11pregunta": "Polinización hidrófila."
            },
            {
                "t11pregunta": "Autopolinización."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las definiciones con el concepto que les corresponda:<br/><br/>"
        }
    },
    //6
    {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B02_A06_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A06_03.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A06_04.png",
              "t17correcta": "1",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A06_05.png",
              "t17correcta": "4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A06_06.png",
              "t17correcta": "2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A06_07.png",
              "t17correcta": "5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": " Arrastra los elementos que completen la información de la tabla:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B02_A06_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "183,218", "cuadrado", "208, 69", ".","transparent"]},
          {"Contenedor": ["", "253,218", "cuadrado", "208, 69", ".","transparent"]},
          {"Contenedor": ["", "323,218", "cuadrado", "208, 69", ".","transparent"]},
          {"Contenedor": ["", "183,428", "cuadrado", "208, 69", ".","transparent"]},
          {"Contenedor": ["", "253,428", "cuadrado", "208, 69", ".","transparent"]},
          {"Contenedor": ["", "323,428", "cuadrado", "208, 69", ".","transparent"]}
      ]
  },
  //7
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La característica principal que diferencia a los hongos de otros seres vivos es que el proceso de digestión se realiza fuera de su cuerpo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los hongos son grandes y se ven a simple vista.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando las hifas se agrupan forman una estructura que se denomina micelio.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hongos producen su propio alimento al igual que las plantas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cuerpo fructífero es sólo una parte productiva que pertenece al hongo.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "<p>crecimiento<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>parásitos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>simbióticos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>nutrición<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>saprófitos <\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>Al igual que cualquier planta o animal, los hongos aumentan de tamaño a lo largo de su vida, proceso conocido como <\/p>"
            },
            {
                "t11pregunta": "<p>. Para crecer, un hongo requiere absorber nutrientes que encuentra en su medio. De acuerdo con su tipo de <\/p>"
            },
            {
                "t11pregunta": "<p>, los hongos se clasifican en <\/p>"
            },
            {
                "t11pregunta": "<p>(todos aquellos que dañan o enferman a otros organismos, como el pie de atleta en el ser humano),  <\/p>"
            },
            {
                "t11pregunta": "<p>(todos aquellos que descomponen a las plantas y animales que mueren) y <\/p>"
            },
            {
                "t11pregunta": "<p>, asi como para favorecer el aprovechamiento de sus nutrientes y eliminar <\/p>"
            },
            {
                "t11pregunta": "<p>(aquellos hongos que se asocian con otros organismos beneficiándose mutuamente).<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde corresponda:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen algunas bacterias que pueden verse a simple vista, sin necesidad de utilizar un microscopio.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las bacterias son microorganismos unicelulares procariotas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dentro de nuestro cuerpo existen bacterias que favorecen procesos de digestión.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La función de las bacterias puede ser tanto benéfica como dañina.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una de las principales características de las bacterias es su amplia resistencia en todos los ambientes.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //10
    {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B02_A10_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A10_03.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A10_04.png",
              "t17correcta": "1",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A10_05.png",
              "t17correcta": "5",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A10_06.png",
              "t17correcta": "2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A10_07.png",
              "t17correcta": "6",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A10_08.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A10_09.png",
              "t17correcta": "7",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B02_A10_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "149,218", "cuadrado", "209, 69", ".","transparent"]},
          {"Contenedor": ["", "219,218", "cuadrado", "209, 69", ".","transparent"]},
          {"Contenedor": ["", "289,218", "cuadrado", "209, 69", ".","transparent"]},
          {"Contenedor": ["", "359,218", "cuadrado", "209, 69", ".","transparent"]},
          {"Contenedor": ["", "149,428", "cuadrado", "209, 69", ".","transparent"]},
          {"Contenedor": ["", "219,428", "cuadrado", "209, 69", ".","transparent"]},
          {"Contenedor": ["", "289,428", "cuadrado", "209, 69", ".","transparent"]},
          {"Contenedor": ["", "359,428", "cuadrado", "209, 69", ".","transparent"]}
      ]
  },
  //11
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Medio físico",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Bacterias",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Seres vivos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Oxígeno",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las respuestas correctas:<br/><br/>Son los componentes de un ecosistema:"
      }
   },
   //12
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Clima",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Animales",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Paisajes",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Relieve",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las respuestas correctas:<br/><br/>Son factores que componen el medio físico:"
      }
   },
   //13
    {
        "respuestas": [
            {
                "t13respuesta": "<p>reptiles<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>osos polares<\/p>",
                "t17correcta": ""
            },
            {
                "t13respuesta": "<p>tundra<\/p>",
                "t17correcta": ""
            },
            {
                "t13respuesta": "<p>desierto<\/p>",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br><br>En el ecosistema de <\/p>"
            },
            {
                "t11pregunta": "<p>, donde el clima es cálido, son comunes animales como los <\/p>"
            },
            {
                "t11pregunta": "<p> los cuales requieren del calor del ambiente para sobrevivir.<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": " Arrastra las palabras donde corresponda:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //14
    {
      "respuestas": [
          {
              "t13respuesta": "NI4E_B02_A03_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A03_03.png",
              "t17correcta": "2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A03_04.png",
              "t17correcta": "4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A03_05.png",
              "t17correcta": "6",
              "columna":"1"
          },
          {
              "t13respuesta": "NI4E_B02_A03_06.png",
              "t17correcta": "7",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A03_07.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A03_08.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI4E_B02_A03_09.png",
              "t17correcta": "5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra los elementos que completen la información de la tabla:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI4E_B02_A03_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "128,89", "cuadrado", "201, 40", ".","transparent"]},
          {"Contenedor": ["", "128,353", "cuadrado", "201, 40", ".","transparent"]},
          {"Contenedor": ["", "218,51", "cuadrado", "201, 40", ".","transparent"]},
          {"Contenedor": ["", "218,391", "cuadrado", "201, 40", ".","transparent"]},
          {"Contenedor": ["", "258,51", "cuadrado", "201, 40", ".","transparent"]},
          {"Contenedor": ["", "258,391", "cuadrado", "201, 40", ".","transparent"]},
          {"Contenedor": ["", "350,221", "cuadrado", "201, 40", ".","transparent"]},
          {"Contenedor": ["", "390,221", "cuadrado", "201, 40", ".","transparent"]}
      ]
  },
  //15
  {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las relaciones alimentarias son importantes para el mantenimiento del equilibrio de los ecosistemas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los organismos productores necesitan una fuente de alimento externa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los organismos desintegradores producen su propio alimento.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El equilibrio de la cadena alimentaria consiste en evitar el crecimiento desmedido de las poblaciones y la extinción de las diferentes especies.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las relaciones alimentarias hacen que el ecosistema tenga los elementos que deben estar.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    } 
];
