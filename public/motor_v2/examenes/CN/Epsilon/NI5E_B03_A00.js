
json=[
  //reactivo 1
    {
        "respuestas": [
            {
                "t13respuesta": "<p>materia<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>propiedades físicas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>volumen<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>masa<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>litros<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>kilogramos<\/p>",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>La <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;se refiere a todo aquello que ocupa un lugar en el espacio y tiene masa. Las dos <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;más importantes son el <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;y la<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;. Generalmente, la cantidad de materia se mide con<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;, para los líquidos y <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;, para los sólidos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //reactivo 2
    {
        "respuestas": [
            {
                "t13respuesta": "(F)",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "(V)",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los términos peso y masa pueden usarse como sinónimos, ya que se refieren a lo mismo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El peso siempre está en función de la fuerza de gravedad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El volumen de un cuerpo puede verse afectado por un cambio de temperatura.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La densidad indica la cantidad de masa contenida en un volumen específico.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La densidad de un sólido generalmente es menor que la densidad de un líquido.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },
    //reactivo 3
    {
        "respuestas": [
            {
                "t13respuesta": "Masa",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Volumen",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Densidad",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Peso",
                "t17correcta": "3"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Relación masa-volumen",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Acción de la gravedad",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Espacio que ocupa la materia",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cantidad de materia",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada concepto.<\/p>",
            "descripcion": "Título preguntas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    //reactivo 4
    {
        "respuestas": [
            {
                "t13respuesta": "(F)",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "(V)",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una sustancia pura es una clase de materia que tiene propiedades específicas y una composición definida.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las sustancias puras pueden clasificarse en elementos y mezclas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los términos compuesto y mezcla son sinónimos, pues se refieren a lo mismo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La composición de una mezcla homogénea es uniforme.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dentro de las mezclas heterogéneas es imposible reconocer cada uno de sus componentes.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },
    //reactivo 5
    {
        "respuestas": [
            {
                "t13respuesta": "Compuesto",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Elemento",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Mezcla homogénea",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Mezcla heterogénea",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Sal (NaCl)"
            },
            {
                "t11pregunta": "Cloro (Cl)"
            },
            {
                "t11pregunta": "Agua de limón"
            },
            {
                "t11pregunta": "Petróleo en el océano"
            }
        ],
        "pregunta": {
             "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
    //reactivo 6
    {  
      "respuestas":[  
         {
            "t13respuesta":"Sabor, olor, color y apariencia o textura.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"La cantidad y calidad de la materia que contiene la mezcla.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Al realizar una mezcla no se transforma ninguna propiedad de la materia.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿Cuáles son las propiedades físicas de la materia que cambian al formar una mezcla?"
      }
   },
    //REACTIVO 7
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Tamizado<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Decantación<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Filtración<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td>Mezcla</td>\n\
                                        <td>Arena-cemento</td>\n\
                                        <td>Agua-aceite</td>\n\
                                        <td>Tierra-agua</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td>Método de separación</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr></table>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    //reactivo 8
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Nitrógeno",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Argón",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Dióxido de carbono",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿Qué gas se encuentra en mayor proporción dentro del aire?"
      }
   },
    //reactivo 9
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Compuesto",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Elemento",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Sustancia",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿A qué categoría de la materia pertenece el dióxido de carbono?"
      }
   },
    //reactivo 10
    {  
      "respuestas":[  
         {  
            "t13respuesta":"De acuerdo al tamaño de las partículas que los forman.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"De acuerdo al nivel de toxicidad que presuponen para el ser humano.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"De acuerdo al grado de enlaces que forman con los gases del aire.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿Cómo se clasifican los contaminantes del aire?"
      }
   },
    //reactivo 11
    {
        "respuestas": [
            {
                "t13respuesta": "<p>calor<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>conducción<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>convección<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>radiación<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>El <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;se refiere a la energía transferida de un objeto de mayor temperatura hacia otro de menor temperatura. Existen tres formas de transferir el calor: la <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;, donde el calor se transmite por el contacto directo entre los objetos; la <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;, donde el movimiento de moléculas es el que permite la transferencia de calor y la <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;, donde el calor se transmite a través de ondas electromagnéticas.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
]
