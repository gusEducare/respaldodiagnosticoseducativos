json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "dieta",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "alimentos",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "equilibrada",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "comida",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "chatarra",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "excesiva",
                "t17correcta": "6"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Una "
            },
            {
                "t11pregunta": " correcta se refiere al consumo de "
            },
            {
                "t11pregunta": " de forma completa, "
            },
            {
                "t11pregunta": ", sana, variada y suficiente.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Comemos un alimento de cada grupo.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Comemos muy bien un alimento.",
                "t17correcta": "0",
                "numeroPregunta": "0"

            },
            {
                "t13respuesta": "Comemos bastante y quedamos satisfechos.",
                "t17correcta": "0",
                "numeroPregunta": "0"

            },
            {
                "t13respuesta": "En el sobrepeso el IMC es igual o superior a 25, mientras que en la obesidad el IMC es mayor a 30.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "La obesidad es controlable con medicamentos, mientras que el sobrepeso no.",
                "t17correcta": "0",
                "numeroPregunta": "1"

            },
            {
                "t13respuesta": "Ninguna, son términos que se pueden utilizar como sinónimos.",
                "t17correcta": "0",
                "numeroPregunta": "1"

            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cuándo se dice que una alimentación es completa?",
                "¿Cuál es la diferencia entre sobrepeso y obesidad?"],
            "t11instruccion": "",
            "preguntasMultiples": true
        }
    },//3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>De 6 a 11 porciones <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>De 5 a 6 porciones <\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>De 2 a 3 porciones<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
.table img{height: 90px !important; width: auto !important; }\n\
</style><table class='table' style='margin-top:-40px;'>\n\
<tr>\n\
<td style='width:350px'>Mucha</td>\n\
<td style='width:100px'>Suficiente</td>\n\
<td style='width:100px'>Poca</td>\n\
</tr>\n\
<tr>\n\
<td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": " </td>\n\
<td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": " </td>\n\
<td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "</td></tr></table>"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa la tabla con los elementos de los recuadros que correspondan.",
            "contieneDistractores": true,
            "anchoRespuestas": 90,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },//4
    /*{
        "respuestas": [
            {
                "t13respuesta": "Papa",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Pan",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Avena",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Frijol",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Lenteja",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta. <br><br>¿Cuáles son ejemplos del grupo de alimento de los cereales?",
            "t11instruccion": "",
        }
    },//5
    {
        "respuestas": [
            {
                "t13respuesta": "Cereales",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Semillas, mantequilla",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Frutas y verduras",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Productos de origen animal",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Carbohidratos"
            },
            {
                "t11pregunta": "Lípidos"
            },
            {
                "t11pregunta": "Vitaminas"
            },
            {
                "t11pregunta": "Proteínas"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },//6
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003EAlimentación \u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDieta\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EComida\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003EDieta\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EAlimentación \u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EComida\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br/><br/>¿Cómo se llama el conjunto de alimentos que consumimos?",
                "¿Cómo se llama a la cantidad y variedad de alimentos?"],
            "preguntasMultiples": true
        }
    },//7
     {
        "respuestas": [
            {
                "t13respuesta": "Infancia",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Adolescencia",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Madurez",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Vejez",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Niñez",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Ser mayor",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Cuáles son las etapas de desarrollo de los humanos?",
            "t11instruccion": "",
        }
    },//8*/
    {
        "respuestas": [
            {
                "t13respuesta": "Cereales",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Semillas, mantequilla",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Frutas y verduras",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Productos de origen animal",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Carbohidratos"
            },
            {
                "t11pregunta": "Lípidos"
            },
            {
                "t11pregunta": "Vitaminas"
            },
            {
                "t11pregunta": "Proteínas"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003E8 años \u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E9 años\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003E7 años\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
           /* {
                "t13respuesta": "\u003Cp\u003ENos alimenta, educa y cuida con cariño.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ENos alimenta y compra lo que queramos. \u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ENos da educación, alimento y casa.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },*/
            {
                "t13respuesta": "\u003Cp\u003EA partir de los 20 años\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EA partir de los 17 años \u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EA partir de los 15 años\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003E60 años\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003E55 años\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003E50 años\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br/><br/>¿Hasta qué edad termina la infancia?",
               // "¿Por qué es tan importante la familia en la infancia?",
                "¿En qué edad empieza la madurez?",
                "¿A partir de que edad se considera la vejez?"],
            "preguntasMultiples": true
        }
    },//9
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            }
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La adolescencia es el fin de la infancia.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En la adolescencia disminuye la producción de hormonas, por eso se dan los cambios físicos.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La testosterona y progesterona son las hormonas responsables de los cambios físicos en mujeres y hombres.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige Falso o Verdadero según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },//10
    {
        "respuestas": [
            {
                "t13respuesta": "A la producción de hormonas por las glándulas reproductoras.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "A la producción de hormonas por los riñones.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "A la producción de glándulas por las hormonas sexuales.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿A qué se debe la presencia de las características sexuales secundarias?",
            "t11instruccion": "",
        }
    },//11
    {
        "respuestas": [
            {
                "t13respuesta": "NI5E_B01_A00_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "NI5E_B01_A00_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "NI5E_B01_A00_03.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "NI5E_B01_A00_04.png",
                "t17correcta": "3",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra la imagen al espacio que corresponda.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "NI5E_B01_A00.png",
            "respuestaImagen": true,
            "tamanyoReal": 10,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["", "108,336", "cuadrado", "119, 34", ".", "transparent"] },
            { "Contenedor": ["", "280,408", "cuadrado", "119, 34", ".", "transparent"] },
            { "Contenedor": ["", "392,281", "cuadrado", "119, 34", ".", "transparent"] },
            { "Contenedor": ["", "246,104", "cuadrado", "119, 34", ".", "transparent"] },
        ]
    },//12
    {
        "respuestas": [
            // {
            //     "t13respuesta": "Dolor de cabeza",
            //     "t17correcta": "1",
            //     "numeroPregunta": "0"
            // },
            // {
            //     "t13respuesta": "Altibajos de estados de ánimo",
            //     "t17correcta": "1",
            //     "numeroPregunta": "0"
            // },
            // {
            //     "t13respuesta": "Irritabilidad",
            //     "t17correcta": "1",
            //     "numeroPregunta": "0"
            // },
            // {
            //     "t13respuesta": "Mucha sed",
            //     "t17correcta": "0",
            //     "numeroPregunta": "0"
            // },
            {
                "t13respuesta": "Bañarse diariamente",
                "t17correcta": "1",

            },
            {
                "t13respuesta": "Cambiarse de ropa interior",
                "t17correcta": "1",

            },
            {
                "t13respuesta": "Usar ropa interior de algodón",
                "t17correcta": "1",

            },
            {
                "t13respuesta": "Bañarse cuando sea necesario",
                "t17correcta": "0",

            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Cuáles son algunas medidas de higiene general tanto para mujeres como hombres?",
            //"preguntasMultiples": true
        }
    },//13
    /*{
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            }
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al producir óvulos y espermatozoides, es hora de tener un hijo.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tener un hijo en la adolescencia afecta nuestra vida a partir de ese momento.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El conocimiento de nuestro cuerpo y la responsabilidad, evitan un embarazo en la adolescencia.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El embarazo adolescente es responsabilidad de la mujer.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p> Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },*/
    {
        "respuestas": [
            {
                "t13respuesta": "Seres vivos que conforman el ecosistema.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Elementos del ecosistema que no poseen vida, pero determinan la presencia, crecimiento y desarrollo de los seres vivos.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Área geográfica donde habitan e interactúan en conjunto seres vivos y un medio físico.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Factores bióticos"
            },
            {
                "t11pregunta": "Factores abióticos"
            },
            {
                "t11pregunta": "Ecosistema"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Temperatura.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Cantidad de precipitación.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tipo de suelo.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Calidad del aire.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Qué factores físicos determinan la vegetación y las características propias de un ecosistema?",
            "t11instruccion": "",
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            }
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aproximadamente el 70% del agua del planeta está congelada en glaciares.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No existen enfermedades que se asocien a la calidad deficiente del agua.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tirar aceite de cocina al drenaje no contamina el agua. ",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El acuacultura se refiere al grado de responsabilidad social que se adquiere en el cuidado del agua.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Más del 50% de las aguas superficiales están contaminadas.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p> Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //13


    {
        "respuestas": [
            {
                "t13respuesta": "hábitats",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "sobreexplotación",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "contaminación",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "invasoras",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "climático",
                "t17correcta": "5"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<ol type='a'><li> La pérdida de "
            },
            {
                "t11pregunta": " se refiere a la desaparición del lugar donde viven las plantas o animales de toda una comunidad y es la principal amenaza de la pérdida de biodiversidad.<br><li>La "
            },
            {
                "t11pregunta": " de recursos es el uso indiscriminado de elementos que son los más probables de desaparecer.<br><li>La "
            },
            {
                "t11pregunta": " es una de las amenazas más graves de la biodiversidad, que es la               transmisión de sustancias perjudiciales o tóxicas al ambiente.<br><li>Las especies "
            },
            {
                "t11pregunta": " son las que llegan a vivir a un lugar y destruyen el equilibrio dentro de ese ecosistema .<br><li>El cambio "
            },
            {
                "t11pregunta": " es la modificación del clima en un área geográfica en un periodo de veinte años.<br></ol>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1",
            }
        ], "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen especies de hongos que son parásitos",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hongos no producen su propio alimento",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los hongos presentan flor y fruto",
                "correcta": "0",
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p> Elige falso (F) o verdadero (V) según corresponda.</p>",
            "t11instruccion": "",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //==============Terminada
];
