  json=[ 
    { 
        "respuestas":[ 
        { 
        "t13respuesta":"F", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"V", 
        "t17correcta":"1", 
        } 
        ], "preguntas" : [ 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "La rapidez y la velocidad son términos que pueden utilizarse como sinónimos pues hacen referencia al mismo concepto.", 
        "correcta":"0", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "La rapidez se define como la distancia que recorre un objeto en un tiempo determinado.", 
        "correcta":"1", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "La velocidad tiene en cuenta conceptos como rapidez, dirección y trayectoria.", 
        "correcta":"1", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "De acuerdo al Sistema Internacional de Unidades la velocidad puede cuantificarse únicamente a través de kilómetros por hora.", 
        "correcta":"0", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "Es una ley que durante el trayecto, el objeto en movimiento siempre mantiene la misma rapidez.", 
        "correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"13", 
        "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.", 
        "t11instruccion": "",   
        "descripcion": "Aspectos", 
        "variante": "editable", 
        "anchoColumnaPreguntas": 50, 
        "evaluable": true 
        } 
    }, 
{ //Reactivo 2 
      "respuestas": [
          {
              "t13respuesta": "NI5E_B04_A02_02.png",
              "t17correcta": "0",
              "columna":"0"
          }, 
          {
              "t13respuesta": "NI5E_B04_A02_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B04_A02_04.png",
              "t17correcta": "2",
              "columna":"0"  
          },
          {
              "t13respuesta": "NI5E_B04_A02_05.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B04_A02_06.png",
              "t17correcta": "4",
              "columna":"0"
          },
           { 
              "t13respuesta": "NI5E_B04_A02_07.png",
              "t17correcta": "5",
              "columna":"0"  
          },
           {
              "t13respuesta": "NI5E_B04_A02_08.png",
              "t17correcta": "6",
              "columna":"0"
          }  
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la imagen al espacio que corresponda.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI5E_B04_A02_01.png",
          "respuestaImagen":true, 
          "anchoImagen":60,
          "tamanyoReal":false,
          "bloques":false,
          "borde":false
           
      },
      "contenedores": [
          {"Contenedor": ["", "22,140", "cuadrado", "380, 60", ".","transparent"]},
          {"Contenedor": ["", "107,140", "cuadrado", "380, 60", ".","transparent"]},
          {"Contenedor": ["", "167,140", "cuadrado", "380, 60", ".","transparent"]},
          {"Contenedor": ["", "244,140", "cuadrado", "380, 60", ".","transparent"]},
          {"Contenedor": ["", "304,140", "cuadrado", "380, 60", ".","transparent"]},
          {"Contenedor": ["", "364,140", "cuadrado", "380, 60", ".","transparent"]},
          {"Contenedor": ["", "444,140", "cuadrado", "380, 60", ".","transparent"]}
      ]   
  }, 
  {  //Reactivo 3
        "respuestas":[  
        { 
        "t13respuesta":"Tiempo y dirección", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Rapidez y dirección", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"Espacio y tiempo", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br><center><img src='NI5E_B04_A03.png'></center><br>¿Qué elementos básicos del movimiento faltaron especificar en el esquema?", 
        "t11instruccion": "", 
        } 
    },  
    { //Reactivo 4
        "respuestas":[ 
        { 
        "t13respuesta":"La dirección describe el sentido del movimiento de acuerdo a los punto cardinales.", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"La trayectoria describe el sentido del movimiento de acuerdo a los punto cardinales.", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"La trayectoria se mide únicamente a través de metros o kilómetros.", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué diferencia hay entre la dirección y la trayectoria del objeto en movimiento?", 
        "t11instruccion": "", 
        } 
    }, 
    { //Reactivo 5 
        "respuestas":[ 
        { 
        "t13respuesta":"Conjunto de vibraciones que se propaga en un medio elástico.", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Movimientos rápidos en forma de onda.", 
        "t17correcta":"2", 
        }, 
        { 
        "t13respuesta":"Ondas que se propagan generalmente por medio del aire y se producen a través del movimiento de un objeto que genera un sonido.", 
        "t17correcta":"3", 
        }, 
        { 
        "t13respuesta":"Es la distancia entre el punto más alejado de una onda y el punto de equilibrio o medio.", 
        "t17correcta":"4", 
        }, 
        { 
        "t13respuesta":"Distancia que recorre una onda en un determinado periodo.", 
        "t17correcta":"5", 
        }, 
        { 
        "t13respuesta":"Es el punto de la onda más separado de su posición de reposo.", 
        "t17correcta":"6", 
        }, 
        ], 
        "preguntas": [ 
        { 
        "t11pregunta":"Sonido" 
        }, 
        { 
        "t11pregunta":"Vibraciones" 
        }, 
        { 
        "t11pregunta":"Ondas sonoras" 
        }, 
        { 
        "t11pregunta":"Amplitud" 
        }, 
        { 
        "t11pregunta":"Longitud de onda" 
        }, 
        { 
        "t11pregunta":"Cresta" 
        }, 
        ], 
        "pregunta":{ 
        "c03id_tipo_pregunta":"12", 
        "t11pregunta":"Relaciona las columnas según corresponda.",
        "t11instruccion": "",
        "altoImagen":"100px", 
        "anchoImagen":"200px" 
        } 
    }, 
    { //Reactivo 6
        "respuestas":[ 
        { 
        "t13respuesta":"A la característica del medio que facilita el desplazamiento de las ondas sonoras.", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"A la capacidad que tiene la voz para adaptarse a su medio.", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"Es una propiedad intrínseca de las ondas sonoras que les permite viajar por cualquier medio.", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿A qué se refiere el término “elástico” dentro de la propagación del sonido?", 
        "t11instruccion": "", 
        } 
     },  
     { //Reactivo 7 
        "respuestas":[ 
        { 
        "t13respuesta":"Hierro", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Cobre", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"Plomo", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cuál es el material sólido donde el sonido se propaga a mayor velocidad?", 
        "t11instruccion": "", 
        } 
    }, 
    { //Reactivo 8
        "respuestas":[ 
        { 
        "t13respuesta":"Agua de mar", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Aceite", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"Agua pura", 
        "t17correcta":"0", 
        }
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cuál es el material líquido donde el sonido se propaga a mayor velocidad?", 
        "t11instruccion": "", 
        } 
    }, 
    { //Reactivo 9 
        "respuestas":[ 
        { 
        "t13respuesta":"Hidrógeno", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Helio", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"Oxígeno", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cuál es el material gaseoso donde el sonido se propaga a mayor velocidad?", 
        "t11instruccion": "", 
        } 
    }, 
    {//Reactivo 10
      "respuestas": [
          {
              "t13respuesta": "NI5E_B04_A10_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B04_A10_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B04_A10_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI5E_B04_A10_06.png",
              "t17correcta": "3",
              "columna":"1"
          }, 
          {
              "t13respuesta": "NI5E_B04_A10_05.png",
              "t17correcta": "4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la imagen al espacio que corresponda.",
          "tipo": "ordenar", 
          "imagen": true,
          "url":"NI5E_B04_A10_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true, 
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [ 
          {"Contenedor": ["", "289,154", "cuadrado", "155, 43", ".","transparent"]},
          {"Contenedor": ["", "415,175", "cuadrado", "155, 43", ".","transparent"]},
          {"Contenedor": ["", "440,450", "cuadrado", "155, 43", ".","transparent"]},
          {"Contenedor": ["", "168,482", "cuadrado", "155, 43", ".","transparent"]},
          {"Contenedor": ["", "100,451", "cuadrado", "155, 43", ".","transparent"]}
      ] 
  },
  { //Reactivo 11
    "respuestas": [        
        {
           "t13respuesta":  "sonido",
            "t17correcta": "1" 
        },
        {
           "t13respuesta":  "ruido",
            "t17correcta": "2" 
        },
        {
           "t13respuesta":  "intensidad",
            "t17correcta": "3" 
        },
        {
           "t13respuesta":  "decibeles",
            "t17correcta": "4"  
        },
    ],
    "preguntas": [ 
      {
      "t11pregunta": "Arrastra las palabras que completen el párrafo.<br><br>El "
      },
      {
      "t11pregunta": " es el conjunto de ondas sonoras que se transmiten por un medio elástico, el "
      },
      {
      "t11pregunta": " en cambio, es un sonido que no tiene armonía y, por lo tanto, es molesto, lo cual es importante ya que perjudica la salud, escuchar sonidos de alta "
      },
      {
      "t11pregunta": " puede dañar nuestro sistema auditivo. Para conocer la intensidad del sonido, esta se puede medir en "
      },
      { 
      "t11pregunta": "."
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "8",
      "t11pregunta": "",  
      "respuestasLargas": true, 
      "pintaUltimaCaja": false, 
      "contieneDistractores": true 
    } 
  },
  { //Reactivo 12
        "respuestas":[ 
        { 
        "t13respuesta":"70 db", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"180 db", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"120 db", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cuál es el nivel máximo de decibeles que el ser humano tolera?", 
        "t11instruccion": "", 
        } 
    },
    { //Reactivo 13
        "respuestas":[ 
        { 
        "t13respuesta":"90 db", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"180 db", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"120 db", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿A cuántos decibeles se encuentra el umbral de dolor?", 
        "t11instruccion": "", 
        } 
    },
    { //Reactivo 14
        "respuestas":[ 
        { 
        "t13respuesta":"F", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"V", 
        "t17correcta":"1", 
        }
        ], "preguntas" : [ 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "Estar expuesto a sonidos de gran intensidad es un factor de riesgo para acelerar la pérdida auditiva.", 
        "correcta":"1", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "Escuchar música en dispositivos electrónicos puede hacerse de manera ilimitada.", 
        "correcta":"0", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "Una exposición a 85 db por ocho horas es el límite sin riesgos.", 
        "correcta":"1", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "Conforme crecemos hay una pérdida auditiva normal.", 
        "correcta":"1", 
        }, 
        { 
        "c03id_tipo_pregunta": "13", 
        "t11pregunta": "Los reproductores musicales emiten sonidos con una intensidad menor a los 100 db.", 
        "correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"13", 
        "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.", 
        "t11instruccion": "",  
        "descripcion": "Aspectos", 
        "variante": "editable", 
        "anchoColumnaPreguntas": 50, 
        "evaluable": true 
        } 
    },
    { //Reactivo 15 
        "respuestas":[ 
        { 
        "t13respuesta":"Flujo de electrones que viaja de átomo en átomo en una dirección.", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Unidad de medida para cuantificar la corriente eléctrica.", 
        "t17correcta":"2", 
        }, 
        { 
        "t13respuesta":"Característica de la batería donde existe un polo negativo y otro positivo que permite el movimiento de cargas eléctricas.", 
        "t17correcta":"3", 
        }, 
        { 
        "t13respuesta":"Cuando dos cuerpos tienen la misma carga eléctrica se rechazan pero cuando tienen carga diferente se atraen.", 
        "t17correcta":"4", 
        }, 
        { 
        "t13respuesta":"Los electrones se mueven en un mismo sentido y con un mismo voltaje.", 
        "t17correcta":"5", 
        }, 
        { 
        "t13respuesta":"La corriente fluye en un sentido durante un tiempo y luego se invierte al opuesto.", 
        "t17correcta":"6", 
        }, 
        { 
        "t13respuesta":"Conjunto de elementos conectados por los que fluye una corriente eléctrica.", 
        "t17correcta":"7", 
        }, 
        ], 
        "preguntas": [ 
        { 
        "t11pregunta":"Corriente eléctrica" 
        }, 
        { 
        "t11pregunta":"Volts" 
        }, 
        { 
        "t11pregunta":"Diferencia de potencial" 
        }, 
        { 
        "t11pregunta":"Ley de Coulomb" 
        }, 
        { 
        "t11pregunta":"Corriente continua" 
        }, 
        { 
        "t11pregunta":"Corriente alterna" 
        }, 
        { 
        "t11pregunta":"Circuito eléctrico" 
        }, 
        ], 
        "pregunta":{ 
        "c03id_tipo_pregunta":"12",
        "t11pregunta":"Relaciona las columnas según corresponda.",
        "t11instruccion": "", 
        "altoImagen":"100px", 
        "anchoImagen":"200px" 
        } 
    },
    { //Reactivo 16 
        "respuestas":[ 
        { 
        "t13respuesta":"Unidad astronómica", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Angstrom", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"Unidad millonaria", 
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué unidad se utiliza para medir las distancias dentro del sistema Solar?", 
        "t11instruccion": "", 
        } 
    }, 
    {  //Reactivo 17 
        "respuestas":[ 
        { 
        "t13respuesta":"Después de Neptuno", 
        "t17correcta":"1", 
        }, 
        { 
        "t13respuesta":"Entre Venus y la Tierra", 
        "t17correcta":"0", 
        }, 
        { 
        "t13respuesta":"Entre Urano y Neptuno",  
        "t17correcta":"0", 
        }, 
        ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"1", 
        "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Dónde se localiza el cinturón de Kuiper?", 
        "t11instruccion": "", 
        } 
    }  
];   