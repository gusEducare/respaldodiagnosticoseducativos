
json=[

    {
        "respuestas": [
            {
                "t13respuesta": "Incluye todos los grupos de alimentos del plato del buen comer y considera <br>la jarra del buen beber.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Debe incluir diferentes alimentos de los tres grupos del plato del buen comer.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Los alimentos y los nutrimentos están en la proporción adecuada. ",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Los alimentos no deben producir daño, no deben estar sucios, ni en mal estado.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Aporta la cantidad necesaria de nutrimentos para cada persona.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Dieta completa"
            },
            {
                "t11pregunta": "Dieta variada"
            },
            {
                "t11pregunta": "Dieta equilibrada"
            },
            {
                "t11pregunta": "Dieta inocua"
            },
            {
                "t11pregunta": "Dieta suficiente"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },////////////2
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Alimentación se refiere al conjunto de alimentos que se consume.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dieta es la cantidad y variedad de alimentos que consume una persona.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La dieta debe ser la misma tanto para adultos como para niños.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La nutrición es el proceso en donde el cuerpo absorbe los nutrientes presentes en los alimentos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las proteínas se consideran las principales fuentes de energía del organismo.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },/////////3
     {
        "respuestas": [
            {
                "t13respuesta": "<p>depresiva<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>desinhibición<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>problemas de visión<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>permanentes<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>riñones<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El alcohol es una sustancia adictiva&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;con efectos principalmente en el Sistema Nervioso. Los efectos transitorios del alcohol son&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, dificultad en el control de movimientos y el equilibrio,&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, pérdida de reflejos, entre otras.<br><br>El consumo excesivo y frecuente de alcohol provoca daños&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en órganos como el hígado,&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, estómago y corazón.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },///////4
    {
        "respuestas": [
            {
                "t13respuesta": "Individual",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Familiar",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Social",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Baja autoestima",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Falta de atención",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Compañeros que se frecuentan",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Desintegración/Violencia",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Bullying",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Discriminación",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada situación.",
            "descripcion": "Situación",   
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable"  : true
        }        
    },//////////5
     {
        "respuestas": [
            {
                "t13respuesta": "<p>biodiversidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>vida<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>genéticas<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>ambiente<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>identidad<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>60 y 70%<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>mamíferos marinos<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>plantas <\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El término&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se refiere a la variedad de formas en las que se manifiesta la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en la Tierra. En ella se describe la cantidad de plantas, animales, hongos y microorganismos distintos que habitan un determinado espacio. Abarca también las diferencias&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;entre organismos y las diversas relaciones entre las especies y el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<br><br>La biodiversidad nos brinda muchos recursos y contribuye a la formación de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;cultural. México es uno de los cuatro países que albergan entre&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;de  las especies conocidas en el planeta. También es el país con el mayor número de especies de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y se encuentra dentro de los cinco países con la mayor cantidad de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;vasculares.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },///////////////6
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EA todas las especies que tienen una distribución restringida a un territorio.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EA las especies que no requieren de cuidados para su crecimiento.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EA las especies que requieren de un cuidado intensivo para su desarrollo.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EPorque representan una forma de vida única que ha logrado sobrevivir y adaptarse al medio que habitan.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque generan un ecosistema que puede aprovecharse a través de la deforestación.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EPorque pueden producir un beneficio económico a la población de la región.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿A qué se refiere el término especie endémica?",
         "¿Por qué son importantes las especies endémicas?"]
      }
   },//////////7
  {
        "respuestas": [
            {
                "t13respuesta": "<p>Tamizado<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Decantación<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Filtración<\/p>",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>Mezcla</td><td>&nbsp;Arena-cemento&nbsp;</td><td>&nbsp;Agua-aceite&nbsp;</td><td>&nbsp;Tierra-agua&nbsp;</td></tr><tr><td><b>Método de separación</b></td><td>\n\<\/p>"
            },
            {
                "t11pregunta": "<p></td><td><\/p>"
            },
            {
                "t11pregunta": "<p></td><td><\/p>"
            },
            {
                "t11pregunta": "<p></td></tr></table><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },/////////////8
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ENitrógeno\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EArgón\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EDióxido de carbono\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EHidrógeno\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EHelio\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EOxígeno\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cuál gas se encuentra en mayor proporción dentro del aire?",
         "¿Cuál es el material gaseoso donde el sonido se propaga a mayor velocidad?"]
      }
   },/////////////////9
    {
      "respuestas": [
          {
              "t13respuesta": "NI5E_B05_A00_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B05_A00_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B05_A00_03.png",
              "t17correcta": "2",
              "columna":"1"
          },
         
          {
              "t13respuesta": "NI5E_B05_A00_04.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "NI5E_B05_A00_05.png",
              "t17correcta": "4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la imagen al espacio que corresponda.<br><br>\n\
          a) Conducto auditivo<br>\n\
          b) Tímpano<br>\n\
          c) Trompa de Eustaquio<br>\n\
          d) Nervio auditivo<br>\n\
          e) Conductos semicirculares<br>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI5E_B05_A00_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "196,170", "cuadrado", "80, 50", ".","transparent"]},
          {"Contenedor": ["", "415,223", "cuadrado", "80, 50", ".","transparent"]},
          {"Contenedor": ["", "432,500", "cuadrado", "80, 50", ".","transparent"]},
          {"Contenedor": ["", "157,530", "cuadrado", "80, 50", ".","transparent"]},
          {"Contenedor": ["", "91,482", "cuadrado", "80, 50", ".","transparent"]}
      ]
  },
   ///10
  {
        "respuestas": [
            {
                "t13respuesta": "<p>sonido<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>ruido<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>intensidad<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>decibeles<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>El&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;es el conjunto de ondas sonoras que se transmiten por un medio elástico, el&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;es un sonido que no tiene armonía y, por lo tanto, es molesto, es importante evitarlo  ya que perjudica la salud. Escuchar sonidos de alta&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;puede dañar nuestro sistema auditivo. Para conocer la intensidad del sonido, esta se puede medir en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
  ];