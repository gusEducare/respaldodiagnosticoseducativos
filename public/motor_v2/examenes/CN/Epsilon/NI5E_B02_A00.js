json=[
    //1
     {
        "respuestas": [
            {
                "t13respuesta": "<p>60 y 70%<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>genéticas<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>biodiversidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>identidad<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>mamíferos marinos<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>plantas<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>ambiente<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>vida<\/p>",
                "t17correcta": "2"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>El término <\/p>"
            },
            {
                "t11pregunta": "<p> se refiere a la variedad de formas en las que se manifiesta la <\/p>"
            },
            {
                "t11pregunta": "<p> en la Tierra. En ella se describen la variedad de plantas, animales, hongos y microorganismos que habitan un determinado espacio. Abarca también las diferencias  <\/p>"
            },
            {
                "t11pregunta": "<p> entre organismos y las diversas relaciones entre las especies y el  <\/p>"
            },
            {
                "t11pregunta": "<p>.<br/>La biodiversidad nos brinda muchos recursos y contribuye a la formación de <\/p>"
            },
            {
                "t11pregunta": "<p> cultural. México es uno de los cuatro países que alberga entre el  <\/p>"
            },
            {
                "t11pregunta": "<p> de las especies conocidas en el planeta. Es el país con el mayor número de especies de <\/p>"
            },
            {
                "t11pregunta": "<p> y se encuentra dentro de los cinco países con mayor número de  <\/p>"
            },
            {
                "t11pregunta": "<p>  vasculares.<\/p>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde correspondan:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
   //2
    {
        "respuestas": [
            {
                "t13respuesta": "Monera",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Fungi",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Protista",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Animal",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Vegetal",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Amiba",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Bacteria del cólera",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hongo del pie de atleta",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Violeta",
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pingüino",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Bacteria <i>E. Coli</i>",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Protozoarios",
                "correcta"  : "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona el reino al que pertenecen cada uno de los siguientes organismos:<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El reino es la categoría más específica de agrupamiento.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La categoría más pequeña de agrupamiento es la especie.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dentro de la especie se comparten rasgos físicos, los integrantes se pueden reproducir entre sí y dejar descendencia fértil.",
                "correcta"  : "0"
            }
            ,
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El ser humano pertenece a la especie Homo sapiens.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los patos que existen en el mundo pertenecen a la misma especie.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Invertebrados",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Peces",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Reptiles",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Anfibios",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Aves",
                "t17correcta": "4"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Pulpo",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tiburón",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cocodrilo",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Ballena",
                "correcta"  : ""
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Águila",
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Rana",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Libélula",
                "correcta"  : ""
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Avestruz",
                "correcta"  : "4"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Estrella de mar",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Lagartijas",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Delfín",
                "correcta"  : ""
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Salamandra",
                "correcta"  : "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la categoría de cada ser vivo:",
            "descripcion":"",
            "evaluable":false,
            "evidencio": false,
            "anchoColumnaPreguntas":20
        }
    },
    //5
    {  
      "respuestas":[
         {  
            "t13respuesta": "A todas aquellas especies que provienen del exterior de una región.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "A las especies que requieren de un cuidado intensivo para su desarrollo.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "A todas las especies que tienen una distribución restringida a un territorio.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "A las especies que no requieren de cuidados para su crecimiento.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Porque hablan de las situaciones adversas que vive cierta región.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque representan una forma de vida única que ha logrado sobrevivir y adaptarse al medio que habitan.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque generan un ecosistema que puede aprovecharse a través de la deforestación.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque pueden producir un beneficio económico a la población de la región.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>¿A qué se refiere el término especie endémica?",
                         "<br/><br/>¿Por qué son importantes las especies endémicas?"],
         "preguntasMultiples": true,
      }
   },
   //6
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Ajolote",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Nopal",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Perrito de la pradera",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Flor de cempasúchil",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las respuestas correctas:<br/><br/>¿Qué especies son endémicas de México?"
      }
   },
   //7
   {
        "respuestas": [
            {
                "t13respuesta": "<p>incompatible<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>ecosistema<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>recursos<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>especie<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>desaparición<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>actividad humana<\/p>",
                "t17correcta": "1"
            }, 
            {
                "t13respuesta": "<p>exóticas<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br/>La principal causa de pérdida de especies es la <\/p>"
            },
            {
                "t11pregunta": "<p>, porque el estilo de vida que sostiene el hombre es <\/p>"
            },
            {
                "t11pregunta": "<p>  con el cuidado de la naturaleza ya que se utilizan y desperdician  muchos <\/p>"
            },
            {
                "t11pregunta": "<p> para satisfacer las necesidades del hombre.<br>Otra causa de pérdida de biodiversidad es la introducción de especies <\/p>"
            },
            {
                "t11pregunta": "<p>,es decir, que no son originarias del <\/p>"
            },
            {
                "t11pregunta": "<p> y compiten con las especies originarias, pudiendo llegar a desplazarlas.<br/>La extinción se puede definir como la <\/p>"
            },
            {
                "t11pregunta": "<p> de todos los miembros de una especie. Si una <\/p>"
            },
            {
                "t11pregunta": "<p> endémica se extingue, desaparece de la faz de la tierra sin tener la mínima probabilidad de recuperarse.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde corresponda:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Seres vivos que conforman el ecosistema.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Elementos del ecosistema que no poseen vida, pero determinan la presencia, crecimiento y desarrollo de los seres vivos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Área geográfica donde habitan e interactúan en conjunto seres vivos y un medio físico.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Factores bióticos"
            },
            {
                "t11pregunta": "Factores abióticos"
            },
            {
                "t11pregunta": "Ecosistema"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las definiciones con el concepto que les corresponda:"
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La milpa de maíz es un ecosistema terrestre natural presente en México.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los jardines públicos, campos agrícolas y estanques para la acuacultura son ejemplos de ecosistemas artificiales.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La deforestación y la contaminación impactan negativamente en los ecosistemas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ecosistemas en México están resguardados adecuadamente, por lo que no existe el riesgo de extinción.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cambio climático antropogénico se refiere a aquel causado por el hombre.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero o falso según corresponda.<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //10
    {  
      "respuestas":[
         {  
            "t13respuesta": "Terrestres y manglares.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Terrestres y acuáticos.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Acuáticos y selváticos.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Acuáticos y bosques.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Temperatura.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Calidad del aire.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Cantidad de precipitación.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Tipo de suelo.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "En México no está presente este tipo de ecosistema.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Principalmente al norte del país en la región del altiplano.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Al sur, desde el norte de Veracruz hasta la península de Yucatán y Chiapas.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Está en los picos de altas montañas como en los volcanes.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Desierto.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Matorral.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Arenas movedizas.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Dunas.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona la respuesta correcta.<br/><br/>¿En qué categorías generales se dividen los ecosistemas?",
                         "<br/><br/>¿Qué factores físicos determinan la vegetación y las características propias de un ecosistema?",
                         "¿En dónde se localiza el ecosistema de la tundra en México?",
                         "Son extensiones de arena que tienen como vegetación principal a plantas que crecen en suelos con alto contenido de sales:"],
         "preguntasMultiples": true,
      }
   },
   //11
    {
        "respuestas": [
            {
                "t13respuesta": "Ubicado en zonas frías, su vegetación típica se compone de musgos, líquenes y pastos de talla baja.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Localizado al norte del país en la región del altiplano, tiene pastos cortos y zacates amacollados como principal vegetación.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Se encuentra en zonas montañosas y está formado por árboles como pinos y encinos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Localizado en montañas altas con árboles de talla mayor a 20 m como el oyamel.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Está en la zona templada tropical; predomina el pino, encino y líquidambar.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Ubicada en la península de Yucatán y Chiapas, se caracteriza por tener árboles de gran altura como el Palo mulato y maderas preciosas como el cedro rojo y la caoba.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Se extiende desde Sinaloa hasta Chiapas. Su vegetación está compuesta por leguminosas y la mayor diversidad de herbáceas trepadoras.",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "Ecosistema con mayor distribución en México, ocupa el 40% de la superficie del país. Tiene baja precipitación y temperaturas extremas.",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "Presentan extensiones de arena con suelos con alto contenido en sales. Las herbáceas son su principal vegetación (chaiso, verdolaga de playa y bejuco).",
                "t17correcta": "9"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Tundra."
            },
            {
                "t11pregunta": "Pastizal."
            },
            {
                "t11pregunta": "Bosque templado."
            },
            {
                "t11pregunta": "Bosque boreal."
            },
            {
                "t11pregunta": "Bosque de niebla."
            },
            {
                "t11pregunta": "Selva húmeda."
            },
            {
                "t11pregunta": "Selva seca."
            },
            {
                "t11pregunta": "Matorrales."
            },
            {
                "t11pregunta": "Dunas."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12"
        }
    },
    //!2
    {
      "respuestas": [
          {
              "t13respuesta": "NI5E_B02_A17_01.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B02_A17_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B02_A17_03.png",
              "t17correcta": "4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI5E_B02_A17_04.png",
              "t17correcta": "6",
              "columna":"1"
          },
          {
              "t13respuesta": "NI5E_B02_A17_05.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B02_A17_06.png",
              "t17correcta": "2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B02_A17_07.png",
              "t17correcta": "7",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B02_A17_08.png",
              "t17correcta": "0",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra la imagen al nombre del ecosistema que representa:<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI5E_B02_A17_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "150,23", "cuadrado", "147, 111", ".","transparent"]},
          {"Contenedor": ["", "150,173", "cuadrado", "147, 111", ".","transparent"]},
          {"Contenedor": ["", "150,323", "cuadrado", "147, 111", ".","transparent"]},
          {"Contenedor": ["", "150,473", "cuadrado", "147, 111", ".","transparent"]},
          {"Contenedor": ["", "344,23", "cuadrado", "147, 111", ".","transparent"]},
          {"Contenedor": ["", "344,173", "cuadrado", "147, 111", ".","transparent"]},
          {"Contenedor": ["", "344,323", "cuadrado", "147, 111", ".","transparent"]},
          {"Contenedor": ["", "344,473", "cuadrado", "147, 111", ".","transparent"]}
      ]
  },
  //13
  {
      "respuestas": [
          {
              "t13respuesta": "NI5E_B02_A18_02.png",
              "t17correcta": "2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B02_A18_03.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI5E_B02_A18_04.png",
              "t17correcta": "1",
              "columna":"1"
          },
          {
              "t13respuesta": "NI5E_B02_A18_05.png",
              "t17correcta": "3",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra los nombres de los ecosistemas a la región de México que les corresponda.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI5E_B02_A18_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "48,333", "cuadrado", "200, 39", ".","transparent"]},
          {"Contenedor": ["", "213,433", "cuadrado", "200, 39", ".","transparent"]},
          {"Contenedor": ["", "308,09", "cuadrado", "200, 39", ".","transparent"]},
          {"Contenedor": ["", "441,139", "cuadrado", "200, 39", ".","transparent"]}
      ]
  },
  //14
   {  
      "respuestas":[
         {  
            "t13respuesta": "Ecosistemas marinos.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Humedales.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Ecosistemas de precipitación.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Ecosistemas fluidos.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Porque son centros de recarga para ellos.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque proporcionan una fuente de alimento y hábitat.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque son barreras contra las tormentas.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Porque regulan el clima local.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>¿Cómo se denomina de manera genérica a los ecosistemas acuáticos?",
                         "<br/><br/>¿Por qué son importantes los ecosistemas acuáticos para los mantos acuíferos?"],
         "preguntasMultiples": true,
      }
   },
   //15
    {
        "respuestas": [
            {
                "t13respuesta": "Recolección - caza",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Agrícola",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Industrial",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Relación no invasiva con la naturaleza",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Deforestación de zonas boscosas",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Relación destructiva con la naturaleza",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Nomadismo",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sedentarismo",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Sociedad consumista",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Domesticación de plantas y animales",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Relaciona las diferentes etapas del desarrollo de la humanidad con las consecuencias ambientales que les correspondan:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "<p>hábitats<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>sobrexplotación<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>invasoras<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>ecosistema<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>amenaza<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>contaminación<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>modificación<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>transmisión<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><b>Causas de la pérdida de la biodiversidad:</b><br/><br/><b>a.</b> La pérdida de <\/p>"
            },
            {
                "t11pregunta": "<p> se refiere a la desaparición del lugar donde viven las plantas o animales de toda una comunidad y es la principal <\/p>"
            },
            {
                "t11pregunta": "<p>de la pérdida de biodiversidad.<br/><b>b</b>. La <\/p>"
            },
            {
                "t11pregunta": "<p> de recursos es el uso indiscriminado de elementos que son los más probables de desaparecer.<br/><b>c.</b> La <\/p>"
            },
            {
                "t11pregunta": "<p> es una de las amenazas más graves de la biodiversidad, que es la <\/p>"
            },
            {
                "t11pregunta": "<p> de sustancias perjudiciales o tóxicas al ambiente.<br/><b>d.</b> Las especies <\/p>"
            },
            {
                "t11pregunta": "<p> son las que llegan a vivir a un lugar y destruyen el equilibrio dentro de ese <\/p>"
            },
            {
                "t11pregunta": "<p>.<br/><b>e.</b> El cambio climático es la <\/p>"
            },
            {
                "t11pregunta": "<p> del clima en un área geográfica en un periodo de veinte años.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras donde corresponda para completar el párrafo:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //17
     {
        "respuestas": [
            {
                "t13respuesta": "Desechos industriales",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Desechos domésticos",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Basura",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Residuos que se vierten en los ríos, lagos o lagunas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tirar directamente al mar o playa los desechos.",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Desechos tapan alcantarillas o drenajes.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Relaciona las actividades humanas con las consecuencias ambientales que provocan:<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //18
     {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aproximadamente el 70% del agua del planeta está congelada en glaciares.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No existen enfermedades que se asocien a la calidad deficiente del agua.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tirar aceite de cocina al drenaje no contamina el agua.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El acuacultura se refiere al grado de responsabilidad social que se adquiere en el cuidado del agua.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Más del 50% de las aguas superficiales están contaminadas.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona verdadero o falso según corresponda:<\/p>",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    }
];
