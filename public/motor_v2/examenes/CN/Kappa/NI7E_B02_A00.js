json=[
    //1
     {
        "respuestas": [
            {
                "t13respuesta": "<p>A través de la nutrición garantizamos que nuestro cuerpo reciba la proporción adecuada de nutrientes para su buen funcionamiento.<\/p>",
                "t17correcta": "0",
                etiqueta:"paso 1"
            },
            {
                "t13respuesta": "<p>La nutrición es el proceso a través del cual los alimentos complejos se descomponen para transformarse en sustancias simples que el cuerpo pueda utilizar para obtener energía y fortalecer a sus células y tejidos.<\/p>",
                "t17correcta": "1",
                etiqueta:"paso 2"
            },
            {
                "t13respuesta": "<p>En primer lugar, los nutrientes pasan a través de las membranas que rodean a cada célula viviente.<\/p>",
                "t17correcta": "2",
                etiqueta:"paso 3"
            },
            {
                "t13respuesta": "<p>Después, los nutrientes absorbidos se transportan a cada célula del cuerpo.<\/p>",
                "t17correcta": "3",
                etiqueta:"paso 4"
            },
            {
                "t13respuesta": "<p>A continuación, los azúcares simples, aminoácidos, ácidos grasos, vitaminas, minerales, fitonutrientes, etc., pasan a través del recubrimiento de las paredes intestinales y son transportados directamente al sistema circulatorio.<\/p>",
                "t17correcta": "4",
                etiqueta:"paso 5"
            },
            {
                "t13respuesta": "<p>Finalmente, el cuerpo elimina los desechos. <\/p>",
                "t17correcta": "5",
                etiqueta:"paso 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "<p>Ordena las etapas de la nutrición:<\/p>",
        }
    },
    //2
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Alimentación y nutrición es lo mismo, ya que en ambas obtenemos energía.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Alimentación y nutrición no son la misma cosa, ya que alimentación es el acto de consumir alimentos y nutrición es el proceso en el que a partir de los alimentos se obtienen la energía.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Alimentación y nutrición no es lo mismo, ya que la alimentación da energía a los humanos y la nutrición a las células.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto presionando \"+\" y seleciona la respuesta correcta:",
         "t11instruccion":"<b>El cuerpo necesita reemplazar las uñas que te cortas, el cabello que se cae, las células de piel muerta que te quitas al bañarte. El cuerpo diariamente está construyendo células para reemplazar las que perdemos.  Este proceso de construcción requiere energía, además de la que se ocupa en cada una de las actividades que realizamos como caminar, parpadear, soñar, etcétera. Los materiales para la construcción de células y la obtención de energía son los nutrientes que obtenemos de los alimentos por medio del proceso de nutrición.</b>"
      }
   },
   //3
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Buena, ya que consumían poca carne y muchas semillas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Malo, ya que consumían muchas semillas y poca carne.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Bueno, ya que consumían mucho maíz y legumbres cocidas.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Malo, ya que consumían cacao, chía y legumbres cocidas.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto presionando \"+\" y seleciona la respuesta correcta:<br><br>¿Cómo era la alimentación de nuestros antepasados?",
         "t11instruccion":"<b>\“La base del alimento era el maíz, preparado ya en pan, ya en atole, ya en diversas bebidas a la manera de los mexica. Usaban también el cacao, con el cual confeccionaban líquidos sabrosos y refrigerantes. Conocían legumbres de diversas clases, y aunque carne comían poca, consumían la de los venados y aves de monteses que tomaban de la caza, y de las aves domésticas que criaban muchas. Comían los hombres apartados de las mujeres, lavándose al concluir manos y boca. Hacían del maíz bebidas fermentadas para sus bailes y regocijos.\”</b><br><br><i>Diego de Landa. (2017). Relación de las cosas de Yucatán. España: Alianza editorial.</i>"
      }
   },
   //4
    {
        "respuestas": [
            {
                "t13respuesta": "Frutas y verduras",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Legumbres",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Origen animal",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cereales y/o semillas",
                "t17correcta": "3"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "jitomate",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "frijol",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "venado",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "aves",
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "chía",
                "correcta"  : ""
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "calabaza",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "cacao",
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "aguacate",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "maíz",
                "correcta"  : "3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Marca en la tabla el grupo al que pertenece cada alimento:<\/p>",
            "descripcion": "Alimentos",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Néctar de Flor Ohia e insectos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Desprende la corteza para buscar escarabajos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Tiene un pico fuerte para triturar las semillas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Se alimenta de néctar.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='NI7E_B02_A01_03.png'/>"
            },
            {
                "t11pregunta": "<img src='NI7E_B02_A01_05.png'/>"
            },
            {
                "t11pregunta": "<img src='NI7E_B02_A01_04.png'/>"
            },
            {
                "t11pregunta": "<img src='NI7E_B02_A01_02.png'/>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona el tipo de alimentación que corresponde a cada especie de ave endémica de la isla de Hawaii:"
        }
    },
    //6
      {  
      "respuestas":[
         {  
            "t13respuesta": "ojos y nariz",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "mimetismo y camuflaje",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "temperatura y altura",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "ovíparos y vivíparos",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Ayuda a que se puedan reproducir varios de la misma especie y que no se extingan.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Cada especie puede asegurar su sobrevivencia en el planeta.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Así otras especies pueden tener más posibilidades de adaptación.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Ayuda a que una especie sobreviva para poder alimentar a otra especie y así contribuir a las redes tróficas.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br><br>Son ejemplos de adaptación de los animales relacionadas con la nutrición:",
                         "La adaptación de las especies es importante porque:"],
         "preguntasMultiples": true,
      }
   },
   //7
   {
      "respuestas": [
          {
              "t13respuesta": "NI7E_B02_A04_02.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_03.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_04.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_05.png",
              "t17correcta": "6",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_06.png",
              "t17correcta": "7",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_07.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_08.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_09.png",
              "t17correcta": "8",
              "columna":"0"
          },
          {
              "t13respuesta": "NI7E_B02_A04_10.png",
              "t17correcta": "2",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI7E_B02_A04_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "251,02", "cuadrado", "100, 80", ".","transparent"]},
          {"Contenedor": ["", "251,144", "cuadrado", "100, 80", ".","transparent"]},
          {"Contenedor": ["", "172,271", "cuadrado", "100, 80", ".","transparent"]},
          {"Contenedor": ["", "251,398", "cuadrado", "100, 80", ".","transparent"]},
          {"Contenedor": ["", "251,540", "cuadrado", "100, 80", ".","transparent"]}
      ]
  },
  //8
  {  
      "respuestas":[
         {  
            "t13respuesta": "Construir sociedades democráticas que sean justas, participativas, sostenibles y pacíficas.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Construir sociedades con calles pavimentadas, luz y agua.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Construir sociedades en la que los gobernantes respeten las leyes y escuchen a los ciudadanos.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Construir sociedades divididas en clases sociales, los que tienen más recursos y los que tienen menos recursos.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Asegurar que todos tengan un trabajo, con prestaciones y bonos por productividad.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Asegurar la equidad de género como prerrequisito para el desarrollo sostenible y asegurar el acceso universal a la educación, el cuidado de la salud y la oportunidad económica.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Asegurar la igualdad de género en el trabajo, que se dividan las mujeres en las áreas que son para mujeres y los hombres en las áreas que son para los hombres.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Asegurar que la mujer sólo se dedique al hogar y al cuidado de los hijos.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa las imágenes y selecciona el párrafo que tenga la solución más acertada a cada problema:<br><br><center><img src='NI7E_B02_A05_01.png' style='width:250px; height:200px;'/><center/>",
                         "<center><img src='NI7E_B02_A05_02.png' style='width:250px; height:200px;'/><center/>"],
         "preguntasMultiples": true,
      }
   },
];