 json=[
 {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caza no controlada del pato rabudo provocaría la sobrepoblación de Ninfas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una de las principales causas de la desecación del lago de Almoloya de Juárez son las altas temperaturas en la zona.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las actividades agrícolas e industriales entre otras, son las principales causas de la contaminación del río. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La actividad económica de la zona no es un factor que provoque la contaminación de la ciénega.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },///////////2
     {
        "respuestas": [
            {
                "t13respuesta": "<p>respiración<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>gases<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>energía<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>sexual<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>asexual<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>aerobio<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>anaerobio<\/p>",
                "t17correcta": "7"
            }, 
            {
                "t13respuesta": "<p>branquial<\/p>",
                "t17correcta": "8"
            }, 
            {
                "t13respuesta": "<p>heterosexual<\/p>",
                "t17correcta": "9"
            }, 
            {
                "t13respuesta": "<p>pulmonar<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>-La&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;también forma parte de las características del metabolismo.<br>-Hay dos procesos importantes en la respiración: el intercambio de&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;entre &nbsp;&nbsp;ambiente y seres vivos y la respiración celular, ya que se obtiene&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en el interior &nbsp;&nbsp;de las células. <br>-Los seres vivos se pueden reproducir de dos formas:&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;o&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<br>-La respiración en los seres vivos puede ser de tipo:&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;o&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//////3
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAlimentación y nutrición no son la misma cosa, alimentación es el acto de consumir alimentos y nutrición es el proceso en el cual a partir de los alimentos se obtiene la energía.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAlimentación y nutrición es lo mismo, ya que en ambas obtenemos energía.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAlimentación y nutrición no es lo mismo, ya que la alimentación da energía a los humanos y la nutrición a las células.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto y selecciona la respuesta correcta.",
         "t11instruccion":"<i>El cuerpo necesita reemplazar las uñas que te cortas, el cabello que se cae, las células de piel muerta que te quitas al bañarte, por eso, diariamente produce  células para reemplazar las que perdemos.  Este proceso requiere de energía, además de la que se ocupa en cada una de las actividades que realizamos como caminar, parpadear, soñar, etcétera. Los materiales necesarios para la producción de células y la obtención de energía son los nutrientes que obtenemos de los alimentos por medio de la nutrición.</i>",
      }
   },///////4

{  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAyuda a que una especie sobreviva para poder alimentar a otra y así contribuir a las redes tróficas.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAyuda a que se puedan reproducir varios de la misma especie y que no se extingan.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAsí otras especies pueden tener más posibilidades de adaptación.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br><br>La adaptación de las especies es importante porque:",
         "t11instruccion":"",
      }
   },////////////////5
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EAsegurar la equidad de género como prerrequisito para el desarrollo sostenible y asegurar el acceso universal a la educación, el cuidado de la salud y la oportunidad económica.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAsegurar la igualdad de género en el trabajo, que se dividan las mujeres en las áreas que son para mujeres y los hombres en las áreas que son para los hombres.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EAsegurar que la mujer solo se dedique al hogar y al cuidado de los hijos.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa la imagen y selecciona la opción más acertada al problema.<br><center><img src='NI7E_B05_A05_01.png' width=480px height=480px/></center>",
         "t11instruccion":"",
      }
   },/////6
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003ELa respiración pulmonar\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa respiración aerobia\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa respiración anaerobia\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003ELa respiración celular aerobia, que toma glucosa o azúcar de los alimentos y oxígeno de la respiración pulmonar para transformarlo en ATP.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa respiración celular aerobia, que toma glucosa o azúcar proveniente de los alimentos y dióxido de carbono de la respiración pulmonar para transformarlo en ATP.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa respiración celular anaerobia, que toma glucosa o azúcar proveniente de los alimentos y oxígeno de la respiración pulmonar para transformarlo en ATP.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>La primera etapa de la respiración humana corresponde a...",
         "La energía que necesitan las células se obtiene a través de... "]
      }
   },////////7
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "La actividad volcánica",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Las corrientes oceánicas",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "La quema de gasolina",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "El ruido",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>Son causas del efecto invernadero."
      }
   }
];