json=[
   {
      "respuestas":[
         {
            "t13respuesta":"La respiración pulmonar",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"La respiración aerobia",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"La respiración anaerobia",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br> La primera etapa de la respiración humana corresponde a:"
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"La respiración celular",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"La respiración aerobia",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"La respiración pulmonar",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br> La segunda etapa de la respiración humana corresponde a:"
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"La respiración celular aerobia, que toma glucosa o azúcar de los alimentos y oxígeno de la respiración pulmonar para transformarlo en ATP.",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"La respiración celular aerobia, que toma glucosa o azúcar proveniente de los alimentos y dióxido de carbono de la respiración pulmonar para transformarlo en ATP.",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"En la respiración celular anaerobia, que toma glucosa o azúcar proveniente de los alimentos y oxígeno de la respiración pulmonar para transformarlo en ATP.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br> La energía que necesitan las células se obtiene a través de:"
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"Tabaquismo",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"Contaminación",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"Aire frío",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br> Es la principal causa de cáncer de pulmón."
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"Aplicar vacuna antineumocócica.",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"Taparse la boca al toser.",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"No tomar cosas frías.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br> Es una medida de prevención de la neumonía."
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"Grandes grupos de ballenas encalladas en playas.",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"Reptiles que viven en madrigueras y se alimentan de roedores pequeños.",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"Migración de la mariposa monarca de Canadá a la República Mexicana.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto y selecciona la respuesta correcta. <br><br> El aumento en la temperatura de la atmósfera terrestre es provocada por los cambios drásticos de los ciclos de nutrientes, donde los elementos químicos, como el carbono, nitrógeno, hidrógeno, potasio y otros, sufren alteraciones en los niveles de concentración que se encuentran en los distintos ecosistemas. El equilibrio de los elementos es esencial para la sobrevivencia de los organismos vivos.<br><br>Es el ejemplo de un cambio en la función y composición de los ecosistemas."
      }
   },
   {
      "respuestas":[
         {
            "t13respuesta":"Porque el cambio climático altera los ciclos naturales (agua, carbono, nitrógeno, etc) y dichos cambios modifican las actividades agrícolas.",
            "t17correcta":"1"
         },
         {
            "t13respuesta":"Porque el cambio de temperatura afecta al humano en sus actividades cotidianas y provoca menos hambre en las personas.",
            "t17correcta":"0"
         },
         {
            "t13respuesta":"Porque la temperatura en invierno es más fría y en verano más caliente.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Lee el texto y selecciona la respuesta correcta. <br><br> Las comunidades rurales en situaciones de alta marginación son las que presentan mayor riesgo de inseguridad alimentaria debido a que los episodios climáticos extremos modifican los ciclos naturales y provocan pérdidas en la actividad agrícola.<br><br>¿Por qué el texto dice que el cambio climático es una amenaza para la seguridad alimentaria?"
      }
   },
   {
     "respuestas":[
        {
           "t13respuesta":"La actividad volcánica",
           "t17correcta":"1"
        },
        {
           "t13respuesta":"Las corrientes oceánicas",
           "t17correcta":"1"
        },
        {
           "t13respuesta":"La quema de gasolina",
           "t17correcta":"1"
        },
        {
           "t13respuesta":"El ruido",
           "t17correcta":"0"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"2",
        "tresColumnas": true,
        "t11pregunta":"Selecciona todas las respuestas correctas.<br><br> Son causas del efecto invernadero."
     }
  }
];
