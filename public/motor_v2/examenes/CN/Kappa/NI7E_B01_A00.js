json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Organismos a los que  llamó “animáculos”",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "La orina",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Una nueva forma de ver el cielo",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "A Hooke, quien fue el primero en observar un tipo de celda en el corcho.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "A Janssen, quien fue el primero en observar un tipo de celda en el corcho.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "A Galileo, quien fue el primero en observar un tipo de celda en el corcho.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué descubrió Leeuwenhoek?",
                "<br><br>¿A quién se debe el nombre de célula?"
            ],
            "preguntasMultiples": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El microscopio  posibilitó a la ciencia la capacidad de  profundizar más en el conocimiento de los seres vivos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gracias a Zacarías Janssen se desarrolló el microscopio compuesto.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Se puede decir que la célula no es importante para algunos sistemas o estructuras de los seres vivos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La célula es la unidad mínima de un organismo. ",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Cuando las diferentes partes se benefician de la relación, pero esta no es estrictamente necesaria para la supervivencia, aunque mejora su aptitud biológica.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cuando una especie u organismo (parásito o huésped) obtiene beneficio directo y a costa de otro (anfitrión), perjudicándole normalmente en el proceso.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cuando solo una parte de las especies implicadas en la relación obtienen beneficio de la misma, sin perjudicar a la otra parte por ello",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Mutualismo"
            },
            {
                "t11pregunta": "Parasitismo"
            },
            {
                "t11pregunta": "Comensalismo"
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El equilibrio ecológico se da gracias a la interacción depredador-presa.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La comunidad ecológica es la relación de los diferentes nichos ecológicos.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los factores abióticos no son indispensables en los nichos ecológicos. ",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Lípidos",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Proteínas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Minerales",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿De que está formada la membrana celular?",
            "t11instruccion": "",
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Es la parte de la célula donde se encuentran los organelos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es la membrana que cubre a la célula.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Es en la parte central de la célula donde está la información genética.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Qué es el citoplasma?",
            "t11instruccion": "",
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Los cloroplastos",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Aparato de Golgi",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Membrana celular",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>¿Cuál es una de las diferencias entre la célula animal y la vegetal?",
            "t11instruccion": "",
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La respiración celular es el proceso por medio del cual todos los seres vivos registran la información en el cerebro. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La respiración celular aeróbica ocurre en cada una de las células del organismo.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las células necesitan glucosa y oxígeno para producir energía.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los seres vivos respiramos dióxido de carbono y producimos oxígeno. ",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable": true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "NI7E_B01_R05_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "NI7E_B01_R05_02.png",
                "t17correcta": "1,2,3",
                "columna": "0"
            },
            {
                "t13respuesta": "NI7E_B01_R05_03.png",
                "t17correcta": "2,3,1",
                "columna": "1"
            },
            {
                "t13respuesta": "NI7E_B01_R05_04.png",
                "t17correcta": "3,1,2",
                "columna": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p> Arrastra la imagen al espacio que corresponda.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "NI7E_B01_R05_00.png",
            "anchoImnagen": 30,
            "respuestaImagen": true,
            "tamanyoReal": false,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["", "138,6", "cuadrado", "210, 60", ".", "transparent"] },
            { "Contenedor": ["", "377,392", "cuadrado", "210, 60", ".", "transparent"] },
            { "Contenedor": ["", "35,411", "cuadrado", "215, 60", ".", "transparent"] },
            { "Contenedor": ["", "29,124", "cuadrado", "205, 60", ".", "transparent"] }
        ]
    },
    //10
    {
        "respuestas": [
          
            {
                "t13respuesta": "Debido a la interacción que existe entre las neuronas motoras, sensitivas  e interneuronas junto con el S.N. central y S.N. Periférico",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Debido a la interacción que existe entre las neuronas motoras y sensitivas.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Debido a que somos capaces de usar el hemisferio derecho y el hemisferio izquierdo del cerebro.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Porque es un tipo de movimiento involuntario controlado por el sistema nervioso autónomo.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Porque es un tipo de movimiento involuntario controlado por el sistema nervioso somático.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Porque es un tipo de movimiento voluntario controlado por el sistema nervioso somático.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cómo podemos responder a los estímulos?", "La razón por la que el corazón sigue latiendo aunque estemos dormidos es:"],
            "t11instruccion": "",
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "dendrita",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "axón",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "sinapsis",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "núcleo",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Schwann",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "interneurona",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Motora",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Extensión de una neurona que se encuentra ramificada y es capaz de recibir estímulos de otras neuronas."
            },
            {
                "t11pregunta": "Extensión larga de una neurona que se encarga de transmitir un impulso nervioso a grandes distancias."
            },
            {
                "t11pregunta": "Es la unión que se llevaba a cabo entre dos neuronas, lo que desencadena que una actúe  igual a la otra."
            },
            {
                "t11pregunta": "Es la parte de la neurona que se encuentra rodeada por una membrana."
            },
            {
                "t11pregunta": "Son aquellas células que alimentan y proveen de  iones a las neuronas."
            },
            {
                "t11pregunta": "Son aquellas neuronas con axones cortos transmiten señales en el Sistema Nervioso. Central."
            },
            {
                "t11pregunta": "Son aquellas neuronas que  presentan axones largos  y transmiten la señal al sistema nervioso central."
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El sistema nervioso central está compuesto por cerebro, cerebelo y médula espinal.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El sistema nervioso autónomo  controla  los  movimientos involuntarios.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El sistema nervioso periférico está compuesto por ganglios,  nervis , receptores y la médula espinal.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando se decide golpear una pelota de voleibol se está utilizando al sistema nervioso  somático",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "digestión",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "absorción",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "asimilación",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "excreción",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "expulsión",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "indigestión",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta<br><br>¿Cuáles son los procesos que implica la nutrición?",
            "t11instruccion": "",
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "equilibrada",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "inocua",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "variada",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "suficiente",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "abundante",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "que tenga un precio justo",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta<br><br>¿Cuáles son algunas de las características necesarias para llevar a cabo  una dieta correcta?",
            "t11instruccion": "",
        }
    },
    //15
    {
        "respuestas": [
            {
                "t13respuesta": "La acumulación de altas cantidades de glucosa en la sangre",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Fatiga continua y sed constante",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Daña  los vasos sanguíneos, los ojos, los riñones y el corazón",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Reduce la cantidad de glóbulos rojos en sangre",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Reduce la mielina en los axones de las neuronas lo que va deteriorando la coordinación del cuerpo",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta<br><br>La diabetes es una enfermedad que provoca:",
            "t11instruccion": "",
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "Cualquier alimento debe ser  lavado y desinfectado",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Los ingredientes  de la dieta deben estar libres de microorganismos y en buen estado.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Exclusiva  de un alimento en particular",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Que tenga todo tipo de alimentos con aportes suficientes de carbohidratos, lípidos, proteínas, etc.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Que sea suficiente dependiendo del tipo de actividad física y edad.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas para cada pregunta<br><br>En la frase: <i>“Hay que llevar a cabo una dieta <b>inocua</b>...”</i>, la palabra inocua quiere decir:",
            "t11instruccion": "",
        }
    },
    //17
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cuerpo está  compuesto 60% de agua.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Algunas de  las funciones del agua son termorregular,  ayudar en la digestión y transporte de nutrientes.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Solo se debe beber un 1 ½ litros de agua al día.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La glucosa es un lípido que puede ser dañino o benéfico según los requerimientos del cuerpo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El índice de masa corporal (IMC) se calcula dividiendo: Peso (kg) /estatura (m), de tal manera que si el resultado es &ge;25, será considerado como sobrepeso.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //18
    { 
        "respuestas":[ 
       { 
        "t13respuesta":"<p>Ingesta de la proporción adecuada de nutrientes para el buen funcionamiento de nuestro cuerpo.</p>", 
        "t17correcta":"0", 
        etiqueta:"1" 
        }, 
       { 
        "t13respuesta":"<p>Proceso a través del cual los alimentos complejos son descompuestos para transformarlos en sustancias simples que el cuerpo pueda utilizar para obtener energía y fortalecer a sus células y tejidos.</p>", 
        "t17correcta":"1", 
        etiqueta:"2" 
        }, 
       { 
        "t13respuesta":"<p>Proceso en el cual los nutrientes son hechos pasar a través de las membranas que rodean a cada célula viviente.</p>", 
        "t17correcta":"2", 
        etiqueta:"3" 
        }, 
       { 
        "t13respuesta":"<p>Transportar los nutrientes absorbidos a cada célula del cuerpo.</p>", 
        "t17correcta":"3", 
        etiqueta:"4" 
        }, 
       { 
        "t13respuesta":"<p>Proceso a través del cual los azúcares simples, aminoácidos, ácidos grasos, vitaminas, minerales, fitonutrientes, etc. pasan a través del recubrimiento de las paredes intestinales y son transportado directamente al sistema circulatorio.</p>", 
        "t17correcta":"4", 
        etiqueta:"5" 
        }, 
       { 
        "t13respuesta":"<p>Eliminación eficiente de los desechos por parte del cuerpo.</p>", 
        "t17correcta":"5", 
        etiqueta:"6" 
        }, 
       ],
        "pregunta":{ 
        "c03id_tipo_pregunta":"9", 
        "t11pregunta":"Ordena las siguientes  etapas de la nutrición<br><br>¿Cómo se lleva a cabo la nutrición en nuestro cuerpo?", 
        "t11instruccion": "", 
            } 
        },
    //19
    {
        "respuestas": [
            {
                "t13respuesta": " compuestos por células o por segregaciones de las mismas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": " no surgen de manera espontánea, sino que proceden de otras anteriores.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": " es un sistema abierto que intercambia materia y energía con su medio.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": " el material hereditario y también son una unidad genética. Esto permite la transmisión hereditaria de generación a generación.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Todos los seres vivos están..."
            },
            {
                "t11pregunta": "Las células..."
            },
            {
                "t11pregunta": "Cada célula..."
            },
            {
                "t11pregunta": "Las células contienen..."
            }
        ],
        "pregunta": {
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "c03id_tipo_pregunta": "12"
        }
    },
    //4
   /* {
        "respuestas": [
            {
                "t13respuesta": "De todos los elementos bióticos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "De todos los elementos abióticos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "De elementos bióticos y abióticos.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br>¿De qué está compuesta una comunidad ecológica?",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Relaciones de los seres vivos con una especie.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Relación de los factores abióticos con una especie.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Lagos y ríos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Orientación del Sol con relación a la especie.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br>¿Cuáles son los componentes de un nicho ecológico?",
            "t11instruccion": "",
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Regulan el tamaño de las poblaciones.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Regulan la intensidad de los factores abióticos.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Ninguna de las dos.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br>¿Qué beneficia la competencia intraespecífica en los ecosistemas?",
            "t11instruccion": "",
        }
    },*/
   
   
   
   
];

