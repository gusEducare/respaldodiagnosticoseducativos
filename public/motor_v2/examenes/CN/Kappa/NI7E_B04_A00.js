json=[
//1
   {  
      "respuestas":[  
         {  
            "t13respuesta":"...la suma de los atributos físicos, funcionales y psicológicos que se expresan a través de la identidad y la conducta sexual en todo lo que somos, sentimos, pensamos y hacemos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"...un tema relacionado con el aparato reproductor femenino y masculino.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"...una construcción social para dar a conocer las funciones de los aparatos reproductores.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br />La sexualidad es..."
      }
   },
   //2
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Género",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Erotismo",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Reproducción",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.  <br />Es el reconocimiento que una persona hace de sí misma como hombre o mujer con respecto a los genitales con los que nació, es decir, de acuerdo con su sexo."
      }
   },
   //3
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Reproducción",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Genitales",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Mujer",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br />Es una de las potencialidades de la sexualidad."
      }
   },
   //4
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Solo las mujeres cuidan a los hijos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"El estrato generacional.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La familia es lo más importante.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br />Es un estereotipo de género."
      }
   },
   //5
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Se cree, que por tener diferencias anatómicas, la mujer y el hombre son distintos en su inteligencia, forma de ser y valores.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Los hombres y las mujeres pueden tener diferencias pero son iguales.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Las mujeres y los hombres son diferentes en todos los aspectos.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br />El fragmento que dice: “...las mujeres y hombres tienen diferencias anatómicas, por lo tanto, se cree que su intelecto, valores, aptitudes y actitudes también lo son…”"
      }
   },
   //6
   {
        "respuestas": [
            {
                "t13respuesta": "<img src='NI7E_B04_A06_01.png'/>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='NI7E_B04_A06_03.png'/>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='NI7E_B04_A06_04.png'/>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='NI7E_B04_A06_05.png'/>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<img src='NI7E_B04_A06_06.png'/>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Se coloca en la vagina una vez por mes y se deja puesto durante tres semanas para que libere hormonas sintéticas que impiden la ovulación."
            },
            {
                "t11pregunta": "Actúa liberando hormonas similares a las que produce la mujer, de esta manera inhibe la ovulación y hace más espeso el moco del cuello del útero"
            },
            {
                "t11pregunta": "Son sustancias que alteran la movilidad de los espermatozoides o los matan; existen diversas presentaciones como espumas, geles, óvulos y cremas."
            },
            {
                "t11pregunta": "Son comprimidos de hormonas sintéticas semejantes a las que produce la mujer naturalmente; su función es evitar la ovulación."
            },
            {
                "t11pregunta": "Oclusión tubaria bilateral y vasectomía."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda.",
            "anchoImagen":"300"
        }
    },
   //7
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El dimorfismo sexual es la diferencia morfológica entre sexos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cortejo es cuando dos animales buscan alimento para invierno.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La polinización de algunas flores la realizan: aves, murciélagos y algunos insectos. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El proceso de la fusión de los gametos, femenino y femenino se la llama fecundación.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Son ejemplos de tipos de reproducción sexual: tubérculos, esporas y gemación.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    },
   //8
   {
        "respuestas": [
            
            {
                "t13respuesta": "Contras -",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Pros +",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cultivos más resistentes a las plagas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cambios en la genética por polinización cruzada: entre cultivos convencionales y los transgénicos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Incremento de la contaminación en los alimentos por un mayor uso de productos químicos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cultivos con más calidad de color, sabor y nutrientes.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Erradicar el hambre mundial.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cambios en el uso de los recursos alimenticios.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona la respuesta que describa mejor cada aspecto. <br />Señala los pros o los contras de los transgénicos. ",
            "descripcion": "Título preguntas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 50,
            "evaluable"  : true
        }        
    }
];