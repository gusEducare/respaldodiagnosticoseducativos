json=[
   //1
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Es la capacidad de un objeto para realizar un trabajo.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Cantidad de masa muscular que se requiere para mover un objeto.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Tipo de energía nuclear que permite la desintegración atómica.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br />¿Cómo se puede definir el término “Fuerza”?"
      }
   },
    //2
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Palanca",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Plano inclinado",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Polea",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Silla de ruedas",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Computadora",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas. <br />Son ejemplos de máquinas simples."
      }
   },
//3
    {
      "respuestas": [
          {
              "t13respuesta": "NI6E_B04_A03_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B04_A03_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B04_A03_02.png",
              "t17correcta": "2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la imagen al espacio que corresponda. <br />Completa la tabla de acuerdo a los géneros de palanca.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI6E_B04_A03_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":true
          
      },
      "contenedores": [
          {"Contenedor": ["", "102,310", "cuadrado", "249, 117", ".","transparent"]},
          {"Contenedor": ["", "229,310", "cuadrado", "249, 117", ".","transparent"]},
          {"Contenedor": ["", "355,310", "cuadrado", "249, 117", ".","transparent"]}
      ]
  },
    //4   
   {  
      "respuestas":[  
         {  
            "t13respuesta":"El microscopio",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"El foco",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"El telescopio",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br />¿Cuál fue el invento de Antoine van Leewenhoek?"
      }
   },
//5
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La luz es energía que viaja en ondas y puede alcanzar una velocidad de 300 000 km/s.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La óptica es una rama de la química que estudia a la luz.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La metrología tiene objeto de estudio las pesas y los sistemas de medición.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término “imagen virtual” significa que refleja el tamaño real del objeto.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La miopía es una condición que impide visualizar objetos lejanos.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Instrumento óptico simple que aumenta el tamaño de las imágenes.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Lentes paralelas que mejoran la visión del ser humano.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Instrumento óptico bi-ocular formado por lentes prismáticas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Instrumento óptico formado por lentes, platina y una base, entre otros.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Captura imágenes para conservar una imagen real.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Lupa"
            },
            {
                "t11pregunta": "Lentes"
            },
            {
                "t11pregunta": "Binoculares"
            },
            {
                "t11pregunta": "Microscopio"
            },
            {
                "t11pregunta": "Cámara fotográfica"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Escribe en el recuadro la letra que corresponda. <br />Relaciona las columnas según corresponda."
        }
    },
    //7 
  {  
      "respuestas":[  
         {  
            "t13respuesta":     "La presencia de luz",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Alcance",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "El campo de visión",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Reflexión y refracción",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas. <br />Son las principales limitaciones de los instrumentos ópticos."
      }
   },
//8
 {  
      "respuestas":[  
         {  
            "t13respuesta":"Energía eléctrica",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Energía electrónica",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Energía hidráulica",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué tipo de energía utiliza combustibles fósiles y se transforma en electricidad?"
      }
   },
//9
{  
      "respuestas":[  
         {  
            "t13respuesta":"Energía magnética",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Energía nuclear",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Energía cinética",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué tipo de energía se manifiesta a través de la atracción de un imán hacia otro objeto?"
      }
   },
   //10
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Energía mecánica",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Energía nucleoide",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Energía mecatrónica",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué energía se manifiesta a través del movimiento?"
      }
   },
   //11
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Energía nuclear",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Energía térmica",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Energía protónica",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"¿Qué energía se obtiene al desintegrar las partículas subatómicas?"
      }
   },
   //12
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Energía Luminosa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Energía Eléctrica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Energía Química<\/p>",
                "t17correcta": "3,5"
            },
            {
                "t13respuesta": "<p>Energía Térmica<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Energía Química<\/p>",
                "t17correcta": "3,5"
            },
            {
                "t13respuesta": "<p>Energía Mecánica<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br />A través de los paneles solares la  "
            },
            {
                "t11pregunta": "  se transforma en <br />"
            },
            {
                "t11pregunta": ". <br />La "
            },
            {
                "t11pregunta": " del gas se transforma en "
            },
            {
                "t11pregunta": " por el calentador de agua. <br />La "
            },
            {
                "t11pregunta": " se transforma en "
            },
            {
                "t11pregunta": " dentro de un automóvil."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Completa el párrafo con las palabras del recuadro. <br />Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
   //13
   {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Universo, hasta donde se sabe, es infinito.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Noventa por ciento del Universo está compuesto por materia visible y comprobable.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Universo es estático, es decir, no existe movimiento dentro de él.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La galaxia es una agrupación de cuerpos celestes que interactúan entre sí.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cúmulos de galaxias son agrupaciones de estrellas dentro de la galaxia.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las estrellas más jóvenes de la Vía Láctea se encuentran en los brazos de la galaxia.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
    },
   //14
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Andrómeda",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Nubes de Magallanes",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Brazo de Orión",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Brazo de Sagitario",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas. <br />¿Cuáles son las galaxias más cercanas a la Vía Láctea?"
      }
   },
   //15
   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Helio",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Hidrógeno",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Argón",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Metano",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas. <br />¿Cuáles gases son principalmente los que forman las estrellas?"
      }
   },
   //16
   {
        "respuestas": [
            {
                "t13respuesta": "Cuerpo esférico que no emite luz propia y gira sobre una estrella.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presentan un diámetro grande y son de naturaleza gaseosa.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cuerpo celeste que orbita alrededor de un planeta.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Astros formados por polvo, hielo y rocas que orbitan en torno al Sol.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Masa de gases que emite luz y radiación.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Planeta"
            },
            {
                "t11pregunta": "Planeta joviano"
            },
            {
                "t11pregunta": "Satélite "
            },
            {
                "t11pregunta": "Cometa"
            },
            {
                "t11pregunta": "Estrella"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    }
];
