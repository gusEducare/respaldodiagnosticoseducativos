json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Tener hábitos nutricionales que mejoran el aporte de nutrientes al cuerpo.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Seguir un horario estricto para llevar a cabo todas las comidas del día.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Realizar un plan de alimentación para consumir únicamente lípidos y carbohidratos.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué significa llevar un régimen alimenticio?",
               
            ],
            "preguntasMultiples": true
        }
    },
    //2 falso verdadero
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las calorías que aportan las bebidas no se acumulan en el cuerpo como las que generan los alimentos sólidos.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cantidad diaria de calorías necesarias no presenta cambios durante la vida del individuo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El agua no aporta ninguna caloría y satisface las necesidades de hidratación del cuerpo.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Así como tomar agua, también se recomienda tomar leche descremada, té y café en cantidades pequeñas.",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //3

    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cada órgano del cuerpo trabaja de manera aislada a los demás.",
                "correcta": "0"
            },
            
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Existen alrededor de 50 tipos de células dentro del cuerpo humano. ",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El nivel de organización de las células en tejidos y órganos es un complejo sistema del ser humano.	",
                "correcta": "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //4 respuesta multiple
   

    {
        "respuestas": [
            {
                "t13respuesta": "Transportar nutrientes y oxígeno a los tejidos.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Eliminar los desechos que los tejidos ya no requieren.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Formar una base para el movimiento del organismo.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Transportar el impulso nervioso a través de las células.",
                "t17correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": "Selecciona todas las respuestas correctas.<br><br>¿Cuáles son las principales funciones del sistema circulatorio?",
            "t11instruccion": "",

        }
    },
    //5 arrastra corta 
{
        "respuestas": [
            {
                "t13respuesta": "tabaquismo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "adicción",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "la nicotina",
                "t17correcta": "3"
            },
            {
                "t13respuesta": " la boca ",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "sistema nervioso ",
                "t17correcta": "5"
            },
            {
                "t13respuesta": " depresión",
                "t17correcta": "6,7"
            },
            {
                "t13respuesta": "fatiga",
                "t17correcta": "7,6"
            },
            {
                "t13respuesta": "transmisión de impulsos",
                "t17correcta": "8"
            },
              {
                "t13respuesta": "alcoholismo",
                "t17correcta": "10"
            },{
                "t13respuesta": "la nariz",
                "t17correcta": "10"
            },{
                "t13respuesta": "sistema digestivo",
                "t17correcta": "10"
            },{
                "t13respuesta": "Alegría",
                "t17correcta": "10"
            },{
                "t13respuesta": "alucinaciones",
                "t17correcta": "10"
            },{
                "t13respuesta": "la dopamina",
                "t17correcta": "10"
            },

           
        ],
        "preguntas": [
            {
                "t11pregunta": "El "
            },
            {
                "t11pregunta": " es considerado una enfermedad ya que crea una "
            },
            {
                "t11pregunta": ", una de las razones es debido a que el componente principal "
            },
            {
                "t11pregunta": " es muy fácil de absorber por la piel, mucosas y pulmones. Debido a que la manera de consumirlo es mediante "
            },
            {
                "t11pregunta": " llega  al "
            },
            {
                "t11pregunta": " en segundos, en donde lo que proporciona son efectos rápidos y placenteros en tan sólo 7 segundos.  Sin embargo, el estímulo inicial  pueden ir seguidos de "
            },
            {
                "t11pregunta": "  y "
            },
            {
                "t11pregunta": " . Cuando es consumido en grandes dosis paraliza el sistema nervioso autónomo, impidiendo la "
            },
            {
                "t11pregunta": ", que incluso pueden desencadenar convulsiones y la muerte."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
//6 Opción múltiple falta imagen
 {
        "respuestas": [
            {
                "t13respuesta": "Al cruzar dos plantas de chícharos con diferentes alturas, la  planta hija sería  alta debido a que éste era un carácter dominante, al menos en la primer generación...",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Al cruzar una planta de chícharo alta con una planta de chícharo de estatura baja, el resultado era una planta de estatura mediana al menos en la primer generación...",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Al cruzar dos plantas de chícharos con diferentes alturas, la  planta hija sería una planta baja debido a que éste era un carácter recesivo, al menos en la primer generación...",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.<br><br>Lo que Gregorio Mendel, padre de la genética, descubrió fue que…",
            "t11instruccion":"<img src='NI6E_B01_R18_01.png'> <img src='NI6E_B01_R18_02.png'><br><br><img src='NI6E_B01_R18_03.png'> <img src='NI6E_B01_R18_04.png'>"
        }
    },
    //7 Falso o verdadero
 {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Realizar una mastografía debe hacerse hasta que la mujer cumpla 50 años.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una actitud responsable ante la sexualidad comienza con estar informado con fuentes confiables.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los síntomas clínicos del VIH se manifiestan al siguiente día del contagio.",
                "correcta": "0"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //8 Respuesta múltiple
 {
        "respuestas": [
            {
                "t13respuesta": "Por contacto sexual sin protección.",
                "t17correcta": "1",
                  "numeroPregunta": "0"
            }, {
                "t13respuesta": "Por uso de agujas entre personas infectadas.",
                "t17correcta": "1",
                  "numeroPregunta": "0"
            },  {
                "t13respuesta": "A través de contacto físico como un abrazo o un beso.",
                "t17correcta": "0",
                  "numeroPregunta": "0"
            }, {
                "t13respuesta": "Comer cerca de la persona infectada.",
                "t17correcta": "0",
                 "numeroPregunta": "0"
            },



             {
                "t13respuesta": "Óvulo",
                "t17correcta": "1",
                 "numeroPregunta": "1"
            },{
                "t13respuesta": "Espermatozoide",
                "t17correcta": "1",
                 "numeroPregunta": "1"
            }, {
                "t13respuesta": "Miocito",
                "t17correcta": "0",
                 "numeroPregunta": "1"
            }, {
                "t13respuesta": "Neurona",
                "t17correcta": "0",
                 "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>Selecciona las maneras de contagio del VIH.",
            "Selecciona a las células sexuales haploides."
            ],
            "t11instruccion": "",
            "preguntasMultiples": true,

        }
    },
    //9 Arrastra corta 
    {
        "respuestas": [
            {
                "t13respuesta": "genes",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "cromosomas",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "núcleo",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "46",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "padre",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "23",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "XX",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "XY",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "Gametos",
                "t17correcta": ""
            },
            {
                "t13respuesta": "48",
                "t17correcta": ""
            }, {
                "t13respuesta": "24",
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Los "
            },
            {
                "t11pregunta": " son segmentos de ADN que están formados por "
            },
            {
                "t11pregunta": " que pueden distinguirse dentro del "
            },
            {
                "t11pregunta": " celular. A su vez, cada célula tiene "
            },
            {
                "t11pregunta": " cromosomas organizados en 23 pares; unos provienen de la madre y otro del "
            },
            {
                "t11pregunta": ". Es el par número "
            },
            {
                "t11pregunta": " el que determina el sexo biológico del individuo: si es mujer será "
            },
            {
                "t11pregunta": " mientras que si es hombre será "
            },
            {
                "t11pregunta": ".<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },
//10 relaciona lineas
 { 
 "respuestas":[ 
{ 
 "t13respuesta":"Serie de transformaciones graduales que las especies han experimentado para adaptarse al medio y sobrevivir.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Género  primitivo extinto considerado el eslabón del origen de todas las aves.", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"Supercontinente formado por la actividad volcánica.", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"Precámbrico, Cámbrico, Silúrico, Jurásico.", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"Vestigio de un organismo o huella que se transforma en roca por procesos químicos y se conserva a través del tiempo.", 
 "t17correcta":"5", 
 }, 
{ 
 "t13respuesta":"Proceso mediante el cual una especie desaparece de la tierra.", 
 "t17correcta":"6", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Evolución" 
 }, 
{ 
 "t11pregunta":"Archaeopteryx" 
 }, 
{ 
 "t11pregunta":"Pangea" 
 }, 
{ 
 "t11pregunta":"Eras geológicas" 
 }, 
{ 
 "t11pregunta":"Fósil" 
 }, 
{ 
 "t11pregunta":"Extinción" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11pregunta": "Relaciona las columnas según corresponda.",
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } 
,
//11 verdadero o falso
 {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la era Azoica las condiciones de la atmósfera eran las adecuadas para que surgiera la vida.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las extinciones masivas sólo pudieron darse durante la prehistoria.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "México es considerado un país megadiverso solo por la cantidad de cultura que tiene.",
                "correcta": "0"
            },
             {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "México ocupa el 5° lugar de los países megadiversos porque forma parte de los países que reúnen el 70% de biodiversidad mundial.",
                "correcta": "1"
            },  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Congo y México presentan el mayor número de reptiles del mundo.",
                "correcta": "0"
            },  {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "México ocupa el 5° lugar en especies de plantas vasculares",
                "correcta": "1"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //12 relaciona columnas
{ 
 "respuestas":[ 
{ 
 "t13respuesta":"Se refiere a la diversidad de especies vegetales,  animales y microorganismos  que viven en un espacio determinado.", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"Se refiere a la gran cantidad y diversidad de especies animales, vegetales y ecosistemas que hay en una región o país.", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"Se refiere al sistema biológico constituido por una comunidad de seres vivos y el medio natural en que viven.", 
 "t17correcta":"3", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"biodiversidad" 
 }, 
{ 
 "t11pregunta":"megadiversidad" 
 }, 
{ 
 "t11pregunta":"Ecosistema" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11instruccion": "", 
 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } ,



];