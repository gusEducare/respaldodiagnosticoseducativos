
json=[
    {
        "respuestas": [
            {
                "t13respuesta": "Fibra nerviosa que conduce el impulso desde el cuerpo celular hasta otra neurona.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Compuesto por encéfalo, cerebelo y médula espinal.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Formado por los nervios que salen del SNC.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Elemento que al entrar al cuerpo puede causar daño o producir una enfermedad.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Células que se encargan de defender al cuerpo de antígenos.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Proteínas que neutralizan a los antígenos.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Pequeña dosis de un antígeno debilitado con el objetivo de generar anticuerpos.",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Axón"
            },
            {
                "t11pregunta": "Sistema Nervioso Central"
            },
            {
                "t11pregunta": "Sistema Nervioso Periférico"
            },
            {
                "t11pregunta": "Agente patógeno"
            },
            {
                "t11pregunta": "Linfocitos"
            },
            {
                "t11pregunta": "Anticuerpos"
            },
            {
                "t11pregunta": "Vacuna"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },/////2
    {  
      "respuestas":[  
         {  
            "t13respuesta":     "Estómago",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Intestino delgado",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Branquias",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Diafragma",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>Son partes del sistema digestivo."
      }
   },//////3
   {
        "respuestas": [
            {
                "t13respuesta": "<p>genes<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>cromosomas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>núcleo<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>46<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>padre<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>23<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>XX<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>XY<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Los&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;son segmentos de ADN que se integran formando&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;que pueden distinguirse dentro del&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;celular. A su vez, cada célula tiene&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;cromosomas organizados en 23 pares, de los cuales uno proviene de la madre y otro del&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>. Es el par número&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;el que determina el sexo biológico del individuo: si es mujer será&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp; mientras que si es hombre será&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//////4
    {
        "respuestas": [
            {
                "t13respuesta": "Serie de transformaciones graduales que las especies han experimentado para adaptarse al medio y sobrevivir.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Especie primitiva considerada el origen de todas las aves.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Supercontinente formado por la actividad volcánica.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Precámbrico, Cámbrico, Silúrico, Jurásico.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Vestigio de un organismo o huella que se transforma en roca por procesos químicos y se conserva a través del tiempo.",
                "t17correcta": "5"
            }  
            
        ],
        "preguntas": [
            {
                "t11pregunta": "Evolución"
            },
            {
                "t11pregunta": "Archaeopteryx"
            },
            {
                "t11pregunta": "Pangea"
            },
            {
                "t11pregunta": "Eras geológicas"
            },
            {
                "t11pregunta": "Fósil"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda"
        }
    },////////5
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la era Azoica las condiciones de la atmósfera eran las adecuadas para que surgiera la vida.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La edad de los fósiles depende del estrato subterráneo donde se encuentren.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término extinción se utiliza cuando una especie desaparece de la Tierra.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A partir de estudios se ha demostrado que el hombre no causó la extinción masiva del Pleistoceno.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las extinciones masivas solo pudieron darse durante la prehistoria.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },////////6
    {
      "respuestas": [
          {
              "t13respuesta": "NI6E_B05_A06_I02.png",
              "t17correcta": "0,1,2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I03.png",
              "t17correcta": "1,0,2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I04.png",
              "t17correcta": "2,1,0",
              "columna":"1"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I05.png",
              "t17correcta": "3,4,5",
              "columna":"1"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I06.png",
              "t17correcta": "4,3,5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I07.png",
              "t17correcta": "5,4,3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I08.png",
              "t17correcta": "6,7,8",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I09.png",
              "t17correcta": "7,6,8",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B05_A06_I10.png",
              "t17correcta": "8,7,6",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra los elementos y completa la tabla.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI6E_B05_A06_I01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "182,19", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "246,19", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "310,19", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "182,222", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "246,222", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "310,222", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "182,425", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "246,425", "cuadrado", "201, 62", ".","transparent"]},
          {"Contenedor": ["", "310,425", "cuadrado", "201, 62", ".","transparent"]}
      ]
  }
   ,/////////7
    {
        "respuestas": [
            {
                "t13respuesta": "Medida para cuantificar la cantidad de tierra que una persona requiere para cubrir sus necesidades y desechar lo que ya no necesita.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Desechos que surgen a partir de compuestos de plantas o animales.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Desechos que se producen a partir de recursos naturales que no son seres vivos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Cada ser vivo está en interacción directa con su ambiente y sus decisiones tienen consecuencias en él.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Huella ecológica"
            },
            {
                "t11pregunta": "Residuos orgánicos"
            },
            {
                "t11pregunta": "Residuos inorgánicos"
            },
            {
                "t11pregunta": "Interdependencia"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda"
        }
    },/////////8
    {
        "respuestas": [
           
            {
                "t13respuesta": "Utilizar el residuo en otro proceso distinto al que fue producido.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Oposición al consumo excesivo e innecesario de recursos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Volver a usar los objetos antes de considerarlos como desecho.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Disminuir el consumo de bienes y energía.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Crear un objeto útil a partir de un desecho.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
           
            {
                "t11pregunta": "Recuperar"
            },
            {
                "t11pregunta": "Rechazar"
            },
            {
                "t11pregunta": "Reutilizar"
            },
            {
                "t11pregunta": "Reducir"
            },
            {
                "t11pregunta": "Reciclar"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda"
        }
    },///////9
    {  
      "respuestas":[  
         
         {  
            "t13respuesta":     "Combustibles fósiles",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Luz solar",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Energía geotérmica",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Respiración anaerobia",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>Son las principales fuentes de energía térmica."
      }
   },/////10
   {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003EEs la que utiliza recursos inagotables.\u003C\/p\u003E",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEs la que utiliza combustibles fósiles como el petróleo.\u003C\/p\u003E",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEs la que requiere mayor cantidad de combustible para generar energía.\u003C\/p\u003E",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿A qué se refiere el término energía renovable?"
      }
   },//////////////11
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Energía Luminosa<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Energía Eléctrica<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Energía Química<\/p>",
                "t17correcta": "3,5"
            },
            {
                "t13respuesta": "<p>Energía Térmica<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Energía Química<\/p>",
                "t17correcta": "5,3"
            },
            {
                "t13respuesta": "<p>Energía Mecánica<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>A través de los paneles solares la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se transforma en<br><\/p>"
            },
            {
                "t11pregunta": "<p>.<br>La&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;del gas se transforma en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;por el calentador de agua.<br>La&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se transforma en&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;dentro de un automóvil.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//////////12
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Universo, hasta donde se sabe, es infinito.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Noventa por ciento del Universo está compuesto por materia visible y comprobable.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Universo es estático, es decir, no existe movimiento dentro de él.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La galaxia es una agrupación de cuerpos celestes que interactúan entre sí.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los cúmulos de galaxias son agrupaciones de estrellas dentro de la galaxia.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las estrellas más jóvenes de la Vía Láctea se encuentran en los brazos de la galaxia.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },////////////13
     {
       "respuestas":[
          {
             "t13respuesta": "Andrómeda",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Nubes de Magallanes",
             "t17correcta": "1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Brazo de Orión",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta": "Brazo de Sagitario",
             "t17correcta": "0",
             "numeroPregunta":"0"
          },/////////////////////////////1
          {
             "t13respuesta": "Helio",
             "t17correcta": "1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Hidrógeno",
             "t17correcta": "1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Argón",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta": "Metano",
             "t17correcta": "0",
             "numeroPregunta":"1"
          },////////////////////2
          ],
       "pregunta":{
          "c03id_tipo_pregunta":"2",
          "t11pregunta": ["Selecciona todas las respuestas correctas.<br><br>¿Cuáles son las galaxias más cercanas a la Vía Láctea?",
                          "¿Cuáles gases son principalmente los que forman las estrellas?"],
          "preguntasMultiples": true
       }
     },////////14
     {
        "respuestas": [
            {
                "t13respuesta": "Cuerpo esférico que no emite luz propia y gira alrededor de una estrella.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Presentan un diámetro grande y son de naturaleza gaseosa.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cuerpo celeste que orbita alrededor de un planeta.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Astros formados por polvo, hielo y rocas que orbitan en torno al Sol.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Masa de gases que emite luz y radiación.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Planeta"
            },
            {
                "t11pregunta": "Planeta joviano"
            },
            {
                "t11pregunta": "Satélite"
            },
            {
                "t11pregunta": "Cometa"
            },
            {
                "t11pregunta": "Estrella"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda."
        }
    }
];