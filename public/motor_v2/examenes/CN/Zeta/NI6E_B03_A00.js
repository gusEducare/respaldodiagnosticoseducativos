json=[
   //1
   {
        "respuestas": [
            {
                "t13respuesta": "Capacidad de un material de doblarse fácilmente sin que exista peligro de que se rompa.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Resistencia de los materiales a las deformaciones o alteraciones.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Capacidad de un material de  permitir que un líquido o gas lo atraviesen sin que estos alteren su estructura.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Flexibilidad"
            },
            {
                "t11pregunta": "Dureza"
            },
            {
                "t11pregunta": "Permeabilidad"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
                "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    //2
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Flexibilidad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Liga de plástico<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Permeabilidad<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Trapo de cocina<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Dureza<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Madera<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta":  "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td>Necesidad</td>\n\
                                        <td>Propiedad</td>\n\
                                        <td>Material</td>\n\
                                        </tr><tr>\n\
                                        <td>Cerrar un empaque de pan de caja</td>\n\
                                        <td>"
                
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>Limpiar un líquido derramado</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr><tr>\n\
                                        <td>Construir un mueble para guardar ropa</td>\n\
                                        <td>"
                                        
                                        
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
                                        
                                        
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr></table>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
//3
    {
        "respuestas": [
            {
                "t13respuesta": "Es utilizar el residuo en otro proceso distinto<br> al que fue producido.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Oposición al consumo excesivo e innecesario de recursos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Volver a usar los objetos antes de considerarlos como desecho.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Disminuir el consumo de bienes y energía.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Crear un objeto útil a partir de un desecho.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Recupera"
            },
            {
                "t11pregunta": "Rechazar"
            },
            {
                "t11pregunta": "Reutilizar"
            },
            {
                "t11pregunta": "Reducir"
            },
            {
                "t11pregunta": "Reciclar"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    //4   
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Ley de conservación de la materia",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Ley del movimiento de Newton",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Ley de acción y reacción",
            "t17correcta":"0"
         }
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta. <br><br>¿A qué ley se refiere el enunciado “La materia no se crea ni se destruye, solo se transforma”? "
      }
   },
//5
   {
       "respuestas": [
           {
               "t13respuesta": "NI6E_B03_A05_02.png",
               "t17correcta": "0",
               "etiqueta":"1"
           },
           {
               "t13respuesta": "NI6E_B03_A05_03.png",
               "t17correcta": "1",
               "etiqueta":"2"
           },
           {
               "t13respuesta": "NI6E_B03_A05_04.png",
               "t17correcta": "2",
               "etiqueta":"3"
           },
           {
               "t13respuesta": "NI6E_B03_A05_05.png",
               "t17correcta": "3",
               "etiqueta":"4"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "5",
           "t11pregunta": "Arrastra la palabra al espacio que corresponda.",
           "tipo": "ordenar",
           "imagen": true,
           "url":"NI6E_B03_A05_01.png",
           "orientacion":"vertical",
           "respuestaImagen":true,
           "tamanyoReal":true
           
       },
       "contenedores": [
           {"Contenedor": ["", "94,127", "izquierda", "128, 46", "."]},
           {"Contenedor": ["", "94,378", "abajo", "128, 46", "."]},
           {"Contenedor": ["", "384,127", "izquierda", "128, 46", "."]},
           {"Contenedor": ["", "384,378", "izquierda", "128, 46", "."]}
       ],
        "css":{
            "espacioRespuestas":110,
            "anchoRespuestas":250
        }
   },
    //6
    

    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El cambio climático afecta las transformaciones del estado de la materia que ocurren dentro del ciclo del agua.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La temporalidad de las transformaciones del agua es lo que permite la fluidez del ciclo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los incendios naturales permiten renovar el suelo para que germinen más plantas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La oxidación y la combustión de materiales son sinónimos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un material se oxida cuando está en contacto prolongadamente con nitrógeno.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //7 
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Cuando las moléculas de la materia son agitadas y este movimiento genera calor.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"A través del aprovechamiento del flujo de una corriente de agua natural.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Gracias al proceso de desintegración de los átomos de algún elemento.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿Cómo se produce la energía térmica?"
      }
   },
//8
 {  
      "respuestas":[  
         {  
            "t13respuesta":"IMECA",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"INDEX",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"ICAMEX",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿Qué índice se utiliza para medir la calidad del aire?"
      }
   },
//9

   {  
      "respuestas":[  
         {  
            "t13respuesta":     "Combustibles fósiles",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Luz solar",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Energía geotérmica",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Respiración anaerobia",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona todas las respuestas correctas.<br>Son las principales fuentes de energía térmica:"
      }
   },
    //10
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Es la que utiliza recursos inagotables.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Es la que utiliza combustibles fósiles como el petróleo.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Es la que requiere mayor cantidad de combustible para generar energía.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br>¿A qué se refiere el término energía renovable?"
      }
   }


];
