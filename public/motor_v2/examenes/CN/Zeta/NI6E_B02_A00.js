json=[
   //1
   {
        "respuestas": [
            {
                "t13respuesta": "Serie de transformaciones graduales que las especies han experimentado para adaptarse al medio y sobrevivir.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Especie primitiva considerada el origen de todas las aves.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Supercontinente formado por la actividad volcánica.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Precámbrico, Cámbrico, Silúrico, Jurásico.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Vestigio de un organismo o huella que se transforma en roca por procesos químicos y se conserva a través del tiempo...",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Evolución"
            },
            {
                "t11pregunta": "Archaeopteryx"
            },
            {
                "t11pregunta": "Pangea"
            },
            {
                "t11pregunta": "Eras geológicas"
            },
            {
                "t11pregunta": "Fósil"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona  las definiciones con el concepto que les corresponda:"
        }
    },
   //2
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la era Azoica las condiciones de la atmósfera eran las adecuadas para que surgiera la vida.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La edad de los fósiles depende del estrato subterráneo donde se encuentren.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término extinción se refiere al proceso mediante el cual una especie desaparece de la tierra.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A través de estudios se ha demostrado que el hombre no causó la extinción masiva del Pleistoceno.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las extinciones masivas sólo pudieron darse durante la prehistoria.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //3
    {
      "respuestas": [
          {
              "t13respuesta": "NI6E_B02_A03_01.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A03_02.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A03_03.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A03_04.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A03_05.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A03_06.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A03_07.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A03_08.png",
              "t17correcta": "5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra a la siguiente tabla las acciones para cuidar a los seres vivos:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI6E_B02_A03_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "63,220", "cuadrado", "202, 80", ".","transparent"]},
          {"Contenedor": ["", "143,220", "cuadrado", "202, 80", ".","transparent"]},
          {"Contenedor": ["", "223,220", "cuadrado", "202, 80", ".","transparent"]},
          {"Contenedor": ["", "303,220", "cuadrado", "202, 80", ".","transparent"]},
          {"Contenedor": ["", "383,220", "cuadrado", "202, 80", ".","transparent"]}

      ]
    },
    //4
     {  
      "respuestas":[
         {  
            "t13respuesta": "A que no puede realizar acciones para cuidar de este.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "A que todas las especies deben de ser utilizadas únicamente para el beneficio del hombre.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "A que tiene la capacidad para generar estrategias que protejan el ambiente y a los seres vivos que lo habitan.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "A que es el único protagonista de la evolución.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Aquellos que pueden generar un daño a las especies que viven en él.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "A los elementos que determinan la extinción masiva de las especies.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Elementos inertes que propician la vida dentro del ecosistema.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "A los factores que permiten diferentes tipos de mutaciones en las especies.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>¿A qué se refiere la frase \“el ser humano ocupa un papel primario dentro del medio ambiente\”?",
                         "<br/><br/>¿Qué son los factores abióticos del medio ambiente?"],
         "preguntasMultiples": true,
      }
   },
   //5
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Cultural",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Religioso",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Político",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Económico",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las respuestas correctas:<br/><br/>Son los componentes del elemento social:"
      }
   },
   //6
   {
        "respuestas": [
            {
                "t13respuesta": "Los usos y costumbres, religión y tradiciones.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Las formas de gobierno, así como la promulgación de leyes y/o normas.Las formas de gobierno, así como la promulgación de leyes y/o normas.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Las industrias, los sistemas de producción, transporte y comercialización de bienes y servicios.",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Cultural"
            },
            {
                "t11pregunta": "Político"
            },
            {
                "t11pregunta": "Económico"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las definiciones con el concepto que les corresponda:"
        }
    },
   //7
   {
      "respuestas": [
          {
              "t13respuesta": "NI6E_B02_A08_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A08_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A08_03.png",
              "t17correcta": "2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra los elementos que completen la información de la tabla:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI6E_B02_A08_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "168,22", "cuadrado", "200, 80", ".","transparent"]},
          {"Contenedor": ["", "247,222", "cuadrado", "200, 80", ".","transparent"]},
          {"Contenedor": ["", "327,422", "cuadrado", "200, 80", ".","transparent"]}
      ]
  },
  //8
   {
      "respuestas": [
          {
              "t13respuesta": "NI6E_B02_A09_01.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A09_02.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A09_03.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI6E_B02_A09_04.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI6E_B02_A09_05.png",
              "t17correcta": "0,1,2,3,4",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A09_06.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A09_07.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A09_08.png",
              "t17correcta": "5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra a la tabla las acciones que promueven un aprovechamiento sustentable de los recursos:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI6E_B02_A09_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "211,02", "cuadrado", "127, 101", ".","transparent"]},
          {"Contenedor": ["", "211,130", "cuadrado", "127, 101", ".","transparent"]},
          {"Contenedor": ["", "211,258", "cuadrado", "127, 101", ".","transparent"]},
          {"Contenedor": ["", "211,386", "cuadrado", "127, 101", ".","transparent"]},
          {"Contenedor": ["", "211,514", "cuadrado", "127, 101", ".","transparent"]}
      ]
  },
  //9
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Únicamente un elemento natural.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Únicamente un elemento social.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Es un elemento cultural y político.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Un elemento natural y social.",
            "t17correcta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta:<br/><br/>¿Qué clase de elemento es el ser humano dentro del medio ambiente?"
      }
   },
   //10
   {
      "respuestas": [
          {
              "t13respuesta": "NI6E_B02_A11_01.png",
              "t17correcta": "0,1,2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A11_02.png",
              "t17correcta": "3,4,5",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A11_03_01.png",
              "t17correcta": "0,1,2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI6E_B02_A11_04.png",
              "t17correcta": "3,4,5",
              "columna":"1"
          },
          {
              "t13respuesta": "NI6E_B02_A11_05.png",
              "t17correcta": "0,1,2",
              "columna":"0"
          },
          {
              "t13respuesta": "NI6E_B02_A11_06.png",
              "t17correcta": "3,5,5",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra las acciones del consumismo o del consumo sustentable según corresponda:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI6E_B02_A11_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "168,121", "cuadrado", "200, 80", ".","transparent"]},
          {"Contenedor": ["", "248,121", "cuadrado", "200, 80", ".","transparent"]},
          {"Contenedor": ["", "328,121", "cuadrado", "200, 80", ".","transparent"]},
          {"Contenedor": ["", "168,321", "cuadrado", "200, 80", ".","transparent"]},
          {"Contenedor": ["", "248,321", "cuadrado", "200, 80", ".","transparent"]},
          {"Contenedor": ["", "328,321", "cuadrado", "200, 80", ".","transparent"]}
       ] 
   },
   //11
   {
        "respuestas": [
            {
                "t13respuesta": "Medida para cuantificar la cantidad de tierra que una persona necesita para cubrir sus necesidades y desechar lo que ya no necesita.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Desechos que surgen a partir de compuestos de plantas o animales.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Desechos que se producen a partir de recursos naturales que no son seres vivos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Cada ser vivo está en interacción directa con su ambiente y sus decisiones tienen consecuencias en él.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Huella ecológica"
            },
            {
                "t11pregunta": "Residuos orgánicos"
            },
            {
                "t11pregunta": "Residuos inorgánicos"
            },
            {
                "t11pregunta": "Interdependencia"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las definiciones con el concepto que les corresponda:"
        }
    },
    //12
    {  
      "respuestas":[
         {  
            "t13respuesta": "Aumento de las temperaturas globales",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Disminución de gases de efecto invernadero",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Derretimiento de glaciares",
            "t17correcta": "1",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Escasez de alimento",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "CFC (Clorofluorocarbonos)",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Argón",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Metano",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Monóxido de carbono",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona las respuestas correctas:<br/><br/>Son  efectos del cambio climático sobre la tierra:",
                         "Selecciona las sustancias que con mayor frecuencia contaminan el ambiente:"],
         "preguntasMultiples": true,
      }
   },
   //13
   {  
      "respuestas":[
         {  
            "t13respuesta": "A la conservación de distintas especies vegetales.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "A el proceso de retener la radiación térmica a través de varios gases que se encuentran dentro de la atmósfera.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "A las consecuencias que tiene el uso desmedido de recursos naturales.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "Es un término que puede utilizarse como sinónimo del cambio climático.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
        {  
            "t13respuesta": "Cadmio.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Helio.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Nitrógeno.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Monóxido de Carbono", 
            "t17correcta": "1",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta:<br/><br/>¿A qué se refiere el efecto invernadero?",
                         "La exposición a concentraciones superiores a 10% de este gas puede producir la muerte:"],
         "preguntasMultiples": true,
      }
   },	     
];
