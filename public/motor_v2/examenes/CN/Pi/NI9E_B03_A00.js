
json=[
    //Reactivo 1
    {
        "respuestas": [
            {
                "t13respuesta": "Sustancia encargada de acelerar o retardar las reacciones químicas.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Proteínas catalizadoras de las reacciones químicas que ocurren dentro de nuestro organismo.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Enzima que se encuentra en la saliva y  ayuda en el proceso digestivo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Su principal función es absorber los nutrientes obtenidos a través de los alimentos.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Nutrientes que son la principal fuente de energía del cuerpo.",
                "t17correcta": "5"
            }
            
        ],
        "preguntas": [
            {
                "t11pregunta": "Catalizador"
            },
            {
                "t11pregunta": "Enzimas"
            },
            {
                "t11pregunta": "Amilasa"
            },
            {
                "t11pregunta": "Intestino delgado"
            },
            {
                "t11pregunta": "Carbohidratos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
//Reactivo 2
   {
        "respuestas": [
            {
                "t13respuesta": "<p>Oxígeno<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Hidróxido<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Ácido<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Anhídrido<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td>Metal</td>\n\
                                        <td>+</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>→</td>\n\
                                        <td>Óxido básico</td>\n\
                                    </tr><tr>\n\
                                        <td>Metal</td>\n\
                                        <td>+</td>\n\
                                        <td>Agua </td>\n\
                                        <td>→</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>No metal</td>\n\
                                        <td>+</td>\n\
                                        <td>Agua</td>\n\
                                        <td>→</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>No metal</td>\n\
                                        <td>+</td>\n\
                                        <td>Oxígeno</td>\n\
                                        <td>→</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </table>"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
//Reativo 3
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Elementos más comunes dentro de los organismos vivos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Conjunto de proteínas que actúan como enzimas en procesos vitales.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Conjunto de catalizadores que se utilizan en el laboratorio.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br><br>¿A qué se refieren las siglas C H O N S P?<div  style='text-align:center;'> </div></p>"
         
      }
   },
//Reactivo 4
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Compuesto que está conformado principalmente por carbono y los enlaces de este con otros elementos.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Compuestos que pueden reutilizarse por su naturaleza inerte.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Compuesto que se localiza únicamente dentro de planetas como Júpiter y Saturno.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br><br>¿Qué es un compuesto orgánico?<div  style='text-align:center;'> </div></p>"
         
      }
   },
//Reactivo 5
     {  
      "respuestas":[  
          {  
            "t13respuesta":     "Cloruro de sodio (NaCl<sub>2</sub>)",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Agua (H<sub>2</sub>O)",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Ácido nítrico (HNO<sub>3</sub>)",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Azúcar (C<sub>12</sub>H<sub>22</sub>O<sub>11</sub>)",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona todas las respuestas correctas.<br><br>Son compuestos inorgánicos.<div  style='text-align:center;'> </div></p>"
         
      }
   },
//Reactivo 6
    {
        "respuestas": [
            {
                "t13respuesta": "Átomos o compuestos simples se unen para formar otros más complejos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Símbolo que representa la acción del calor en la reacción química.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Los compuestos se transforman en otros más sencillos.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Existe un intercambio de elementos de acuerdo a su afinidad química.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Hay un intercambio de iones entre dos compuestos.",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "Tipo de reacción que libera energía.",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "Tipo de reacción que absorbe energía.",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Reacción de síntesis"
            },
            {
                "t11pregunta": "<img src='NI9E_B03_A06_06.png'>"
            },
            {
                "t11pregunta": "Reacción de descomposición"
            },
            {
                "t11pregunta": "Reacción de sustitución"
            },
            {
                "t11pregunta": "Reacción de doble sustitución"
            },
            {
                "t11pregunta": "Reacción exotérmica"
            },
            {
                "t11pregunta": "Reacción endotérmica"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
//Reactivo 7
    {
        "respuestas": [
            {
                "t13respuesta": "(F)",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "(V)",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La caloría es una unidad de medida de la energía térmica requerida para elevar 1g de agua a 1°C.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El consumo calórico debe ser el mismo para todos los seres humanos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término dieta es sinónimo de régimen alimenticio.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La comida mexicana ha sido considerada patrimonio de la humanidad.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La inclusión de la comida rápida en la dieta ha mejorado la salud de la población.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un IMC de 23 significa que la persona padece sobrepeso.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    },
//Reactivo 8
   {  
      "respuestas":[  
         {  
            "t13respuesta":"A la capacidad de un átomo para atraer electrones y formar así un enlace.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Al grado en que los elementos se repelen entre sí.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Es el tipo de afinidad entre elementos negativos de la tabla periódica.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br><br>¿A qué se refiere el término “electronegatividad” de Pauling?<div  style='text-align:center;'> </div></p>"
         
      }
   },
//Reactivo 9
    {  
      "respuestas":[  
         {  
            "t13respuesta":"En las mismas condiciones de presión y temperatura, el volumen de cualquier gas posee el mismo número de moléculas.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Cuando se eleva la temperatura de un gas, la dinámica de las moléculas permanece estable.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"La cantidad de materia presente dentro de un gas aumenta al elevarse la presión del mismo.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br><br>Define la Ley de Avogadro<div  style='text-align:center;'> </div></p>"
         
      }
   },
//Reactivo 10
    {
        "respuestas": [
            {
                "t13respuesta": "(F)",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "(V)",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mol es una unidad de medida específica para átomos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mol es la cantidad de cualquier sustancia que posee la misma cantidad de unidades elementales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El número de Avogadro equivale a 6.023x10<sup>23</sup> moléculas, iones o átomos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si tenemos la cantidad de masa de una muestra podemos conocer el número de moles presente en la misma.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un mol de agua no contiene la misma cantidad de átomos que un mol de Fe.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Reactivo",
            "evaluable":false,
            "evidencio": false
        }
    }

];