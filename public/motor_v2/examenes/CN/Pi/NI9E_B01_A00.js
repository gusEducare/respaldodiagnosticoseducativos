json = [
    {
        "respuestas": [
            {
                "t13respuesta": "\u003Cp\u003ETodos aquellos cambios que sean internos de la materia.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ETodos aquellos cambios que no alcanzamos a ver a simple vista y su relación con la materia.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ETodos los cambios.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "\u003Cp\u003ETodas las reacciones.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },


            {
                "t13respuesta": "\u003Cp\u003EObtendremos un sólido frágil.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003EDebido al cambio de presión, obtendremos un líquido.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ENo obtendremos nada, ya que el gas tiende a evaporarse fácilmente.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "\u003Cp\u003ENinguna de las anteriores.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },


            {
                "t13respuesta": "\u003Cp\u003EUn cambio de estado de la materia.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003EUna reacción.\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003EUna liberación de la energía.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003ENinguna de las anteriores.\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "\u003Cp\u003EReactivo 1 + Reactivo 2------------------------> Producto 1 + Producto 2\u003C\/p\u003E",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003EProducto 1 + Producto 2------------------------> Reactivo 1 + Reactivo 2\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003EFactor 1 + Factor 2---------------------------> Producto 1 + Producto 2\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "\u003Cp\u003EProducto 1 + Producto 2------------------------> Factor 1 + Factor 2\u003C\/p\u003E",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona la(s) respuesta(s) correcta(s):<br/><br/>¿Qué estudia la química?",
                "¿Qué pasa cuando tratamos de expulsar el gas de un extintor de CO<sub>2</sub> durante 10 segundos para que se mantenga en una bolsa de tela? ",
                "¿Qué se puede deducir de la pregunta anterior? ", "Las partes que conforman una reacción son: "],
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "bioquímica",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "petroquímica",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "estequiometría",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Parte de la química que estudia las funciones de los seres vivos."
            },
            {
                "t11pregunta": "Se encarga del crecimiento y desarrollo de las industrias, como la textil, automotriz, electrónica, etcétera, para la elaboración de productos."
            },
            {
                "t11pregunta": "Parte de la química que determina la cantidad de producto y reactivos que se generan en una reacción."
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },

    ///
    {
        "respuestas": [
            {
                "t13respuesta": "NI9E_B01_A00_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "NI9E_B01_A00_02.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "NI9E_B01_A00_03.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "NI9E_B01_A00_04.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "NI9E_B01_A00_05.png",
                "t17correcta": "4",
                "columna": "0"
            },
            {
                "t13respuesta": "NI9E_B01_A00_06.png",
                "t17correcta": "5",
                "columna": "0"
            },
            {
                "t13respuesta": "NI9E_B01_A00_07.png",
                "t17correcta": "6",
                "columna": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "¿Cuáles son los estados de la materia de las siguientes sustancias o elementos y cómo se llaman sus diversas transformaciones?<br>Arrastra los recuadros al espacio que correspondan.",
            "tipo": "ordenar",
            "imagen": true,
            "tamanyoReal": true,
            "url": "NI9E_B01_A00_00.png",
            "respuestaImagen": true,
            "tamanyoReal": false,
            "anchoImagen": 80,
            "bloques": false,
            "borde": false,
            "evaluable": true

        },
        "contenedores": [
            { "Contenedor": ["", "353,55", "cuadrado", "67, 30", ".", "transparent"] },
            { "Contenedor": ["", "285,146", "cuadrado", "67, 30", ".", "transparent"] },
            { "Contenedor": ["", "353,146", "cuadrado", "67, 30", ".", "transparent"] },
            { "Contenedor": ["", "285,329", "cuadrado", "67, 30", ".", "transparent"] },
            { "Contenedor": ["", "353,329", "cuadrado", "67, 30", ".", "transparent"] },
            { "Contenedor": ["", "353,238", "cuadrado", "67, 30", ".", "transparent"] },
            { "Contenedor": ["", "353,420", "cuadrado", "67, 30", ".", "transparent"] }
        ]

    },
    ////
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En las mezclas homogéneas se distinguen dos o más componentes.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un coloide es un tipo de mezcla heterogénea.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las mezclas homogéneas no pueden separarse por filtración.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La fase dispersante es también llamado solvente.",
                "correcta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda:<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true,
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Cuando el soluto se encuentra en menor proporción que el solvente.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Cuando el soluto es de mayor proporción que el solvente.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Concentración mayor en el solvente y se observa la cristalización.",
                "t17correcta": "3",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Diluidas"
            },
            {
                "t11pregunta": "Concentradas"
            },
            {
                "t11pregunta": "Sobresaturada"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },
    //6
    /*{
        "respuestas": [
            {
                "t13respuesta": "<p>Consiste en dejar reposar la mezcla<br> y el componente más denso se va hacia<br> el fondo. De forma manual, se separan<br> las fases que se observan.<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Consiste en calentar la mezcla hasta<br> el punto de ebullición de cada sustancia <br>y posteriormente, pasar por un <br>refrigerante que condensa los vapores,<br> obteniendo así el líquido.<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Se utiliza para separar sólido-sólido.<\/p>",
                "t17correcta": "3"
            },

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                      .table img{height: 90px !important; width: auto !important; }\n\
                                  </style><table class='table' style='margin-top:-40px;'>\n\
                                  <tr>\n\
                                      <td style='width:350px'>Método de separación </td>\n\
                                      <td style='width:300px'>¿En qué consiste?</td>\n\
                                  </tr>\n\
                                  <tr>\n\
                                      <td >Embudo deseparación</td><td>"
            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td></tr><tr>\n\
                <td>Destilación</td><td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "     </td></tr><tr>\n\
                                      <td>Tamización</td><td>"
            },

            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "  </td>\n\
                                      </tr></table>"

            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "contieneDistractores": true,
            "anchoRespuestas": 90,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true,
            "evaluable": true
        }
    },*/
    //////7
    {
        "respuestas": [
            {
                "t13respuesta": "A la relación del volumen de soluto, entre el volumen de soluto y solvente.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "A la relación del volumen de soluto y solvente.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "A ninguna de las dos.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "%masa/volumen= (soluto (g)/ solución (ml) ) 100",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "%masa/masa= (soluto (g)/ solución (ml) ) 100",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "%masa/volumen= (solución (ml)/ solución (ml) ) 100",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿A qué se refiere el porcentaje en volumen?",
                "¿Qué fórmula se debe utilizar para calcular el porcentaje de volumen cuando hablamos de un sólido disuelto en un líquido?",
            ],
            "preguntasMultiples": true,
            "evaluable": true
        }
    },
    ///////
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "ppm es igual a partes por millón",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La fórmula para calcular las ppm es:  ppm = masa del soluto/volumen del solvente",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Arriba de 150 IMECAs, la calidad del aire es buena",
                "correcta": "0"
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona Verdadero o Falso según corresponda:<\/p>",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true,
        }
    },
    /////////////////////
    {
        "respuestas": [
            {
                "t13respuesta": "166 ml",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "661 ml",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "161 ml",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "38.64 g/ml",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "3.86 g/ml",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "3864 g/ml",
                "t17correcta": "0",
            },

             {
                "t13respuesta": "Huella de carbono",
                "t17correcta": "1",
             "numeroPregunta": "2"

            },
            {
                "t13respuesta": "ppm",
                "t17correcta": "0",
                             "numeroPregunta": "2"

            },
            {
                "t13respuesta": "ml",
                "t17correcta": "0",
                             "numeroPregunta": "2"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿Cuántos ml de disolución son necesarios para que esta tenga un 4,6% m/m, si contiene 9,1 g de soluto? (densidad de disolución = 1,192 g/m).",
                "Si a 100 ml de un líquido (densidad = 1,9 g/ml) se le vierten 43 g de NaCl, ¿cuál será el porcentaje masa/volumen de la disolución si su densidad final fue de 2,1 g/ml?",
             "¿Cuál es una medición aproximada de los gases de efecto invernadero causados directa o indirectamente por algún evento a lo largo de su ciclo de vida?",
            ],
            "preguntasMultiples": true
        }
    },
    ////////////////////
  
    ///////////////
    {
        "respuestas": [
            {
                "t13respuesta": "Antoine-Laurent de Lavoisier",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Louis Pasteur",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Jean-Augustin Barral",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Descubrió el hidrógeno",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Creó el primer microscopio ",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Investigó sobre la genética humana",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Flogisto",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Membrana combustible",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Espectro invisible",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Conservación de la energía",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "La electronegatividad",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Conservación de las especies",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿Quién es considerado el padre de la química moderna?",
                "¿Cuál es una aportación de Lavoisier?", "¿Cuál es la sustancia invisible que supuestamente existía en todas las cosas materiales y explicaba su combustión, antes del descubrimiento del oxígeno?",
                "¿Cuál es la ley que dice: “En toda reacción química la masa total de los reactivos es igual a la masa total de los productos”?"

            ],
            "preguntasMultiples": true
        }
    },

    //////////////
{
        "respuestas": [
            {
                "t13respuesta": "Se caracterizan por tener la misma composición química que es constante y definida.",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Se caracterizan por tener diferencias en la composición química.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Se caracterizan por tener la misma composición química aunque no es constante y ni definida.",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },

            {
                "t13respuesta": "Es un conjunto de sustancias que podemos separar por métodos físicos.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Es un conjunto de sustancias que no podemos separar por métodos físicos. ",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Es un conjunto de sustancias que algunas se pueden separar podemos separar por métodos físicos y otras no.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.  <br><br>¿Qué es una sustancia pura?",
                "¿Qué es una mezcla?",
            ],
            "preguntasMultiples": true
        }
    },

 


//falso verdadero
 {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las mezclas están divididas en homogeneas y heterogeneas.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una característica de las mezclas es que solo se pueden separar por algún método químico.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un compuesto es igual a un elemento.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un elemento es una sustancia pura que no se puede separar en otra más sencilla",
                "correcta": "1"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "",
            "variante": "editable",
            "anchoColumnaPreguntas": 70,
            "evaluable": true,
        }
    },


    //Arrastra imagen
     {
      "respuestas": [
          {
              "t13respuesta": "NI9E_B01_R41-43_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI9E_B01_R41-43_02.png",
              "t17correcta": "1,2,3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI9E_B01_R41-43_03.png",
              "t17correcta": "2,3,1",
              "columna":"1"
          },
        
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Ordena&nbsp;los pasos del m\u00e9todo de resoluci\u00f3n de problemas que aprendiste en esta sesi\u00f3n:<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI9E_B01_R41-43_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":true
          
      },
      "contenedores": [
          {"Contenedor": ["", "262,98", "cuadrado", "147, 147", ".","transparent"]},
          {"Contenedor": ["", "262,247", "cuadrado", "147, 147", ".","transparent"]},
          {"Contenedor": ["", "262,397", "cuadrado", "147, 147", ".","transparent"]}
      ]
  }
,


//opcion multiple
{
        "respuestas": [
            {
                "t13respuesta": "una representación conceptual o física a escala de un proceso o sistema.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "una representación necesaria para el estudio de la química.",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "una representación imaginaria de un proceso o sistema.",
                "t17correcta": "0",
           
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": "Selecciona la respuesta correcta.  <br><br>Un modelo corpuscular es...",
              
        }
    },

];
