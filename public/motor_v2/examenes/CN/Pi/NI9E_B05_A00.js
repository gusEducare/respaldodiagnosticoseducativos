 json=[
 {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La filtración consiste en colocar una barrera con un material poroso para retener partículas sólidas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La destilación consiste en enfriar la mezcla hasta el punto de congelación para separar las sustancias.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A la separación de un sólido con un sólido a través de una superficie porosa de distintos diámetros se le llama: Tamización",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },/////////////2
    {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E38.64 g/ml\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E48.64 g/ml\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E3 864 g/ml\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003E166 ml\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E266 ml\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003E1.66 ml\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          {  
            "t13respuesta":"\u003Cp\u003EDensidad\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003ETemperatura\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EViscosidad\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
          
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>Si a 100 ml de un líquido (densidad = 1.9 g/ml) se le vierten 43 g de NaCl. ¿Cuál será el porcentaje masa/volumen de la disolución si su densidad final fue de 2.1 g/ml?",
         "¿Cuántos ml de disolución son necesarios para que esta tenga 4.6% m/m, si contiene 9.1 g de soluto? (densidad de disolución = 1 192 g/m).",
         "Es la masa que contiene una unidad de volumen."]
      }
   },////////3
   {
        "respuestas": [
            {
                "t17correcta": "111"
            },
            {
                "t17correcta": "218"
            },
            {
                "t17correcta": "99"
            },
            {
                "t17correcta": "42"
            },
            {
                "t17correcta": "x"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>&nbsp&nbspÓxido de selenio (IV) (SeO<sub>2</sub>)&nbsp"
            },
            {
                "t11pregunta": "&nbspuma<br>&nbsp&nbspÓxido de renio (IV) (ReO<sub>2</sub>)&nbsp"
            },
            {
                "t11pregunta": "&nbspuma<br>&nbsp&nbspHidróxido de zinc (Zn (OH)<sub>2</sub>)&nbsp"
            },
            {
                "t11pregunta": "&nbspuma<br>&nbsp&nbspCloruro de litio (LiCl)&nbsp"
            },
            {
                "t11pregunta": "&nbspuma"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Calcula y escribe la masa molecular de los siguientes compuestos.",
            "pintaUltimaCaja": false,
            evaluable:true
        }
    },/////////4
    {
        "respuestas": [
            {
                "t13respuesta": "<p>puras<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>físicas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>partículas<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>no<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>compuestas<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>sí<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>sustancias<\/p>",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>Las sustancias&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;tienen propiedades&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y químicas perfectamente determinadas; asimismo, todas sus&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;son iguales con una composición constante y definida, y&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;pueden obtenerse otras mediante métodos físicos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },///////5
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Cloruro de sodio (NaCl<sub>2</sub>)",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Agua (H<sub>2</sub>O)",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Ácido nítrico (HNO<sub>3</sub>)",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Azúcar (C<sub>12</sub>H<sub>22</sub>O<sub>11</sub>)",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>Son compuestos inorgánicos."
      }
   },////////6
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mol es una unidad de medida específica para átomos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El mol es la cantidad de cualquier sustancia que posee la misma cantidad de unidades elementales.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El número de Avogadro equivale a 6.023 x 10<sup>23</sup> moléculas, iones o átomos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si tenemos la cantidad de masa de una muestra podemos conocer el número de moles presente en la misma.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un mol de agua no contiene la misma cantidad de átomos que un mol de Fe.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },/////////7
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Ácido<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Base<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Sal<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Agua<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br><style>\n\
                .table img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-10px;'>\n\
                <tr><td>&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;</td>&nbsp;<td></td><td>&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;</td>&nbsp;<td></td><td>&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;</td>&nbsp;<td></td><td>&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;</td></tr>\n\
                <tr><td><b>HCl</b></td>\n\
                <td><b>+</b></td>\n\
                <td><b><b>NaOH</b></b></td>\n\
                <td><b>→</b></td>\n\
                <td><b>NaCl</b></td>\n\
                <td><b>+</b></td>\n\
                <td><b>H<sub>2</sub>O</b></td>\n\
                </tr></table><\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },//////////////8
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El agua en estado puro tiende a ser un compuesto más básico que ácido.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ácidos generalmente están formados por elementos no metálicos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gracias a ciertas sustancias ácidas, disminuye el pH de la boca, lo que desmineraliza el esmalte natural de los dientes.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ácidos no tienen un uso benéfico para el ser humano.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Las bases, por lo regular, se usan como productos de limpieza.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    }
]