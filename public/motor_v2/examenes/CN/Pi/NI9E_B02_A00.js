json=[
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "<p>puras<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>físicas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>partículas<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>no<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>características<\/p>",
                "t17correcta": ""
            },
            {
                "t13respuesta": "<p>compuestas<\/p>",
                "t17correcta": ""
            }, 
            {
                "t13respuesta": "<p>sí<\/p>",
                "t17correcta": ""
            },
            {
                "t13respuesta": "<p>naturales<\/p>",
                "t17correcta": ""
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><br>Las sustancias <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;tienen propiedades <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;y químicas perfectamente determinadas; asimismo, todas sus <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;son iguales con una composición constante y definida, y <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;pueden obtenerse otras mediante métodos físicos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras al espacio que corresponda para completar correctamente el texto:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    //2
    {  
        "respuestas":[  
           {  
              "t13respuesta":"\u003Cp\u003EEl helio es un compuesto, ya que sirve para hacer flotar a un globo.\u003C\/p\u003E",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"\u003Cp\u003EEl agua es un compuesto al estar conformada por una molécula de hidrógeno y dos de oxígeno.\u003C\/p\u003E",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"\u003Cp\u003EEl aceite en el agua es un compuesto aunque no se mezclen.\u003C\/p\u003E",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":" Lee el siguiente texto presionando \"+\" y selecciona la afirmación correcta.",
           "t11instruccion":"<b>Un compuesto es una sustancia formada por dos o más elementos unidos químicamente en proporciones fijas. La unión entre dos elementos se da por medio de un enlace, es decir, la fuerza de unión entre dos o más átomos que permite la estabilidad en los compuestos.</b>"
        }
     },
     //3
     {  
        "respuestas":[  
           {  
              "t13respuesta":"\u003Cp\u003ENo existe diferencia.\u003C\/p\u003E",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"\u003Cp\u003EEl método de separación.\u003C\/p\u003E",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"\u003Cp\u003ELa primera requiere movimientos rápidos y la segunda requiere calor para formarse.\u003C\/p\u003E",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la espuesta correcta:<br><br>¿Cuál es la diferencia entre una mezcla y un compuesto?"
        }
     },
     //4
     {
        "respuestas": [
            {
                "t13respuesta": "Bohr",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Rutherford",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No tiene órbitas definidas y sus electrones se mueven todos juntos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tiene órbitas y sus partículas negativas giran en diferentes niveles.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "No posee neutrones.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el núcleo posee neutrones y protones.",
                "correcta"  : "1"
           }     
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona el científico al que le pertennece la característica de su modelo:<\/p>",
            "descripcion":"características",
            "evaluable":false,
            "evidencio": false
        }
    },
   //5
    {
      "respuestas": [
          {
              "t13respuesta": "NI9E_B02_A05_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI9E_B02_A05_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI9E_B02_A05_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI9E_B02_A05_05.png",
              "t17correcta": "3",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra el nombre de cada parte de los elementos de la tabla periódica según corresponda:<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI9E_B02_A05_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "61,25", "cuadrado", "150, 80", ".","transparent"]},
          {"Contenedor": ["", "223,25", "cuadrado", "150, 80", ".","transparent"]},
          {"Contenedor": ["", "386,25", "cuadrado", "150, 80", ".","transparent"]},
          {"Contenedor": ["", "168,470", "cuadrado", "150, 80", ".","transparent"]},
      ]
  },
  //6
  {  
      "respuestas":[
         {  
            "t13respuesta": "La cantidad de protones del elemento.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La cantidad de electrones del elemento.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "La cantidad de órbitas en el átomo.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "El peso de los neutrones y electrones, no protones.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El peso dado por los protones y neutrones, no electrones.",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "El peso del átomo de un elemento con otro elemento.",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Señala la respuesta correcta. Puede ser más de una:<br><br>¿Qué es el número atómico?",
                         "¿Qué indica el peso atómico?"],
         "preguntasMultiples": true,
      }
   },
   //7
    {
        "respuestas": [
            {
                "t17correcta": "64"
            },
            {
                "t17correcta": "108"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><img src='NI9E_B02_A08_01.png'/> MM="
            },
            {
                "t11pregunta": "<br><img src='NI9E_B02_A08_02.png'/> MM="
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            evaluable:true,
            "t11pregunta":"Escribe la masa atómica o masa molecular de los siguientes elementos:"
        }
    },
    //8
     {  
      "respuestas":[  
         {  
            "t13respuesta":"<img src='NI9E_B02_A09_02.png' style='width:70px; height:70px;'/>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src='NI9E_B02_A09_03.png' style='width:70px; height:70px;'/>",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"<img src='NI9E_B02_A09_04.png' style='width:70px; height:70px;'/>",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"<img src='NI9E_B02_A09_05.png' style='width:70px; height:70px;'/>",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Observa la tabla periódica presionando \"+\", contesta la pregunta:<br><br>¿Cuál es la estructura de Lewis del Plomo?",
         "t11instruccion":"<img src='NI9E_B02_A09_01.png'/>"
      }
   },
   //9
   {
      "respuestas": [
          {
              "t13respuesta": "NI9E_B02_A10_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI9E_B02_A10_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI9E_B02_A10_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI9E_B02_A10_05.png",
              "t17correcta": "3",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra las propiedades de los metales y no metales según corresponda:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI9E_B02_A10_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "117,142", "cuadrado", "238, 160", ".","transparent"]},
          {"Contenedor": ["", "117,393", "cuadrado", "238, 160", ".","transparent"]},
          {"Contenedor": ["", "288,142", "cuadrado", "238, 160", ".","transparent"]},
          {"Contenedor": ["", "288,393", "cuadrado", "238, 160", ".","transparent"]},
      ]
  },
  //10
  {
        "respuestas": [
            {
                "t17correcta": "111"
            },
            {
                "t17correcta": "218"
            },
            {
                "t17correcta": "99"
            },
            {
                "t17correcta": "42"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>Óxido de selenio (IV) (SeO<sub>2</sub>): uma"
            },
            {
                "t11pregunta": "<br>Óxido de renio (IV) (ReO<sub>2</sub>):uma"
            },
            {
                "t11pregunta": "<br>Hidróxido de zinc (Zn (OH)<sub>2</sub>):uma"
            },
            {
                "t11pregunta": "<br>Cloruro de litio (LiCl):uma"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            evaluable:true,
            "t11pregunta":"Calcula la masa molecular de cada compuesto y escríbela en el espacio correspondiente:"
        }
    },
    //11
    {  
      "respuestas":[
         {  
            "t13respuesta": "iónico",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "covalente",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "metálico",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "iónico",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "covalente",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "metálico",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "iónico",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "covalente",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "metálico",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Calcula la electronegatividad de los siguientes compuestos para poder determinar el tipo de enlace al que pertenece.<br>Selecciona en cada caso la respuesta correcta:<br><br>Cloruro de Sodio (NaCl)",
                         " Agua (H<sub>2</sub>O)",
                         "Sulfuro de potasio (K<sub>2</sub>S)"],
         "preguntasMultiples": true,
      }
   },
   //12
   {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En un enlace iónico los electrones de valencia y los átomos se transfieren de un no metal a un metal.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En un enlace metálico los electrones de valencia, se desligan de los átomos y se comparten entre todos.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero (V) o falso (F) según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    }  
];