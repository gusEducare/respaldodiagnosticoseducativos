json=[
    //1
   
     //4
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Svante Arrhenius",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Antoine Lavoisier",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Thomas Lowry",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
            "t11instruccion": "",
           "t11pregunta":"<b>Selecciona la respuesta correcta.</b> <br><br>¿Quién utiliza por primera vez el término “base”?"
        }
     },
     //5
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Estómago",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Cerebro",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Músculos",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"<b>Selecciona la respuesta correcta. </b><br><br>En nuestro cuerpo ¿dónde encontramos al ácido clorhídrico?",
            "t11instruccion": "",
        }
     },
      {  
        "respuestas":[  
           {  
              "t13respuesta":"Ácido cítrico",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Ácido tartárico",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Ácido ascórbico",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
            "t11instruccion": "",
           "t11pregunta":"<b>Selecciona la respuesta correcta.</b> <br><br>¿Qué ácido contiene el limón?"
        }
     },
      {
        "respuestas": [
            {
                "t13respuesta": "<p>HCl<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>NaOH<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>NaCl<\/p>",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "<p>H<sub>2</sub>O<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
              


                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'><td>Ácido</td><td>&nbsp; &nbsp; &nbsp;</td><td>Base</td><td>&nbsp; &nbsp; &nbsp;</td><td>Sal</td><td>&nbsp; &nbsp; &nbsp;</td><td>Agua</td>\n\
                                    <tr>\n\
                                        <td>"
            },
            {
                
                "t11pregunta": "        </td>\n\
                                        <td><b>+</b></td>\n\
                                        <td>"
            },
            
            {
                
                "t11pregunta": "        </td>\n\
                                        <td><b> → </b></td>\n\
                                        <td>"
            },
            {
                
                "t11pregunta": "        </td>\n\
                                        <td><b>+</b></td>\n\
                                        <td>"
            },
             {
               
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td>"

            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<b>Arrastra los elementos y completa la tabla.",
           "contieneDistractores":false,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            
            "ocultaPuntoFinal": true
        }
    },
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Neutralización",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Oxidación",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Combustión",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
            "t11instruccion": "",
           "t11pregunta":"<b>Selecciona la respuesta correcta.</b> <br><br>¿Qué reacción se llevó a cabo en HCl+NaOH → NaClH<sub>2</sub>O?<br>"
        }
     },

    {
    "respuestas": [{
            "t13respuesta": "F",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "V",
            "t17correcta": "1"
        }
    ],
    "preguntas": [{
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El agua en estado puro tiende a ser un compuesto más básico que ácido.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los ácidos generalmente están formados por elementos no metálicos.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Gracias a ciertas sustancias ácidas disminuye el pH de la boca, lo que desmineraliza el esmalte natural de los dientes.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los ácidos no tienen un uso benéfico para el ser humano.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las bases generalmente se usan como productos de limpieza.",
            "correcta": "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p><b>Elige falso (F) o verdadero (V) según corresponda.</b><\/p>",
        "descripcion": "",
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable": true
    }
},
{  
        "respuestas":[  
           {  
              "t13respuesta":"Potencial de Hidrógeno",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Potencial de Hidróxido",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Potencial de Helio",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
            "t11instruccion": "",
           "t11pregunta":"<b>Selecciona la respuesta correcta.</b> <br><br>¿Qué significa “pH”?<br>"
        }
     },
     {
        "respuestas": [
            {
                "t13respuesta": "<p>Refresco<\/p>",
                "t17correcta": "1,4,6"
            },
            {
                "t13respuesta": "<p>Vinagre<\/p>",
                "t17correcta": "1,4,6"
            },
            {
                "t13respuesta": "<p>Comida rápida<\/p>",
                "t17correcta": "1,4,6"
            },
             {
                "t13respuesta": "<p>Agua pura<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Aguacate<\/p>",
                "t17correcta": "5,7,3"
            },
            {
                "t13respuesta": "<p>Plátano<\/p>",
                "t17correcta": "5,7,3"
            },
             {
                "t13respuesta": "<p>Chícharos<\/p>",
                "t17correcta": "5,7,3"
            }
            
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",


                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'><td>Alimentos ácidos</td><td>Alimentos neutros</td><td>Alimentos alcalinos</td>\n\
                                    <tr>\n\
                                         <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },
            
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                           <td>\n\
                                        "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "       </td>\n\
                                       </tr><tr>\n\
                                        <td> "
            },

             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "       </td>\n\
                                        <td>-</td>\n\
                                        <td>"
            },{
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                          </tr><tr>\n\
                                        <td>"
            }, 
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>-</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "           </td>\n\
                                  </tr> </table>"
            }
            
            
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<b>Arrastra los elementos y completa la tabla.</b>",
           "contieneDistractores":false,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            
            "ocultaPuntoFinal": true
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Permite la degradación de los alimentos para que sean absorbidos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se presenta cuando el ácido estomacal sube hacia el esófago.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Músculo que permite el paso de los alimentos hacia el estómago.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Bacteria que provoca gastritis y úlceras gástricas.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Se prefiere alimentos alcalinos como verduras y frutas.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Jugo gástrico"
            },
            {
                "t11pregunta": "Reflujo gastroesofágico"
            },
            {
                "t11pregunta": "Esfínter gastroesofágico"
            },
            {
                "t11pregunta": "Helycobacter pilori"
            },
            {
                "t11pregunta": "Dieta alcalina"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<b>Relaciona las columnas según corresponda.</b>",
            "t11instruccion": "",
        }
    },
    {
    "respuestas": [{
            "t13respuesta": "F",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "V",
            "t17correcta": "1"
        }
    ],
    "preguntas": [{
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Dentro del compuesto H<sub>2</sub>O, el dos es el número de oxidación del Hidrógeno.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La síntesis de glucosa en la fotosíntesis corresponde a una reacción redox.",
            "correcta": "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los metales oxidados conservan sus propiedades mecánicas.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En las reacciones redox se transfieren los protones entre dos átomos.",
            "correcta": "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La galvanoplastia es el recubrimiento de la estructura para protegerla de la oxidación. ",
            "correcta": "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p><b>Elige falso (F) o verdadero (V) según corresponda.</b><\/p>",
        "descripcion": "",
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable": true
    }
}

];