json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A través del lenguaje matemático surgió una simbolización universal.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El modelo matemático no permite predecir el futuro, solo describir y explicar los fenómenos ya existentes.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un modelo matemático solo se puede construir a partir de complicadas fórmulas científicas.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para entender la naturaleza es conveniente tener un patrón rígido de observación.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La imaginación no forma parte de las representaciones probables de un fenómeno natural.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //2
	{
        "respuestas": [
            {
                "t13respuesta": "Afirmó que la naturaleza era divisible hasta pequeñísimas partículas que llamó átomos.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Consideraba que cuatro elementos constituían la naturaleza: agua, fuego, aire y tierra.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Explicó el movimiento de un objeto en caída libre, y descubrió la aceleración debida a la gravedad entre otras cosas.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "“La materia está hecha de partículas que interactúan entre sí mediante fuerzas atractivas y repulsivas.”",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Dedujo que el calor se transmite siempre del cuerpo de mayor temperatura hacia el cuerpo de menor temperatura.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Demócrito"
            },
            {
                "t11pregunta": "Aristóteles"
            },
            {
                "t11pregunta": "Galileo Galilei"
            },
            {
                "t11pregunta": "Isaac Newton"
            },
            {
                "t11pregunta": "Rudolf Clausius"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    //3
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Ludwig Boltzmann",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Robert Hooke",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"Isaac Newton",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la respuesta correcta. <br><br>Es el científico que logró integrar la mecánica con la termodinámica."
        }
     },
     //4
     {  
         "respuestas":[  
            {  
               "t13respuesta":"James Clark Maxwell",
               "t17correcta":"1"
            },
            {  
               "t13respuesta":"Galileo Galilei",
               "t17correcta":"0"
            },
            {  
               "t13respuesta":"Max Planck",
               "t17correcta":"0"
            }
         ],
         "pregunta":{  
            "c03id_tipo_pregunta":"1",
            "t11pregunta":"Selecciona la respuesta correcta. <br><br>Es el científico que logró integrar la electricidad con el magnetismo."
         }
      },
      //5
      {  
          "respuestas":[  
             {  
                "t13respuesta":"Max Planck",
                "t17correcta":"1"
             },
             {  
                "t13respuesta":"Demócrito",
                "t17correcta":"0"
             },
             {  
                "t13respuesta":"Isaac Newton",
                "t17correcta":"0"
             }
          ],
          "pregunta":{  
             "c03id_tipo_pregunta":"1",
             "t11pregunta":"Selecciona la respuesta correcta. <br><br>Es el científico que sentó las bases de la mecánica cuántica."
          }
       },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El modelo atómico análogo al sistema planetario sirve para explicar los átomos de toda la tabla periódica.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si el átomo tiene el mismo número de electrones que de protones se dice que es neutro.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El hidrógeno posee un protón y un neutrón dentro de su núcleo.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A mayor cantidad de electrones aumenta la carga negativa para el átomo. ",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al perder un electrón, el átomo queda ionizado negativamente.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
     //7
	{
        "respuestas": [
            {
                "t13respuesta": "Cantidad de moléculas o materia contenida en un objeto.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cantidad de espacio que ocupa la materia del objeto.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Cantidad de materia contenida en un volumen determinado.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Unión de las moléculas de acuerdo a la fuerza de atracción molecular.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Es la fuerza de atracción entre partículas adyacentes dentro de un mismo cuerpo.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Masa"
            },
            {
                "t11pregunta": "Volumen"
            },
            {
                "t11pregunta": "Densidad"
            },
            {
                "t11pregunta": "Estado de agregación"
            },
            {
                "t11pregunta": "Cohesión"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
            
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los sólidos generalmente tienen un volumen poco definido.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La forma del líquido es determinada por el recipiente que lo contiene.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los gases poseen una fuerza de cohesión mayor a la de los líquidos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La letra griega rho se utiliza para representar la densidad de un objeto.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La densidad se obtiene al dividir el peso entre el volumen del objeto.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //9
    {  
        "respuestas":[  
           {  
              "t13respuesta":"Blaise Pascal",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"Isaac Newton",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"James Maxwell",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la respuesta correcta. <br><br>¿Qué científico estudió la presión en los líquidos y en su honor se nombra a la unidad de presión con su apellido?"
        }
     },
    //10
    {  
        "respuestas":[  
           {  
              "t13respuesta":"La presión tiene en cuenta el área donde se aplica la fuerza.",
              "t17correcta":"1"
           },
           {  
              "t13respuesta":"La fuerza tiene en cuenta el área donde se aplica la presión.",
              "t17correcta":"0"
           },
           {  
              "t13respuesta":"La presión tiene en cuenta el volumen del material.",
              "t17correcta":"0"
           }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":"Selecciona la respuesta correcta. <br><br>¿Qué diferencia hay entre presión y fuerza?"
        }
     },
     //11
     {
        "respuestas": [
            {
                "t13respuesta": "Medida relacionada con la energía cinética promedio de sus moléculas al moverse.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cantidad de energía que genera movimiento en las moléculas de los objetos.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Capacidad que tiene la materia para producir un trabajo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Toma como estándar el punto de ebullición y congelación del agua. ",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Propone que el punto más bajo es el cero absoluto.",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Temperatura"
            },
            {
                "t11pregunta": "Calor"
            },
            {
                "t11pregunta": "Energía"
            },
            {
                "t11pregunta": "Escala Celsius"
            },
            {
                "t11pregunta": "Escala Kelvin"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "NI8E_B03_A16_02.png",
                "t17correcta": "0",
                "columna":"0"
            },
            {
                "t13respuesta": "NI8E_B03_A16_03.png",
                "t17correcta": "1",
                "columna":"0"
            },
            {
                "t13respuesta": "NI8E_B03_A16_04.png",
                "t17correcta": "2",
                "columna":"1"
            },
          
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Arrastra la imagen al espacio que corresponda.",
            "tipo": "ordenar",
            "imagen": true,
            "url":"NI8E_B03_A16_01.png",
            "respuestaImagen":true, 
            "tamanyoReal":true,
            "bloques":false,
            "borde":false
            
        },
        "contenedores": [
            {"Contenedor": ["", "211,19", "cuadrado", "202, 163", ".","transparent"]},
            {"Contenedor": ["", "211,222", "cuadrado", "202, 163", ".","transparent"]},
            {"Contenedor": ["", "211,423", "cuadrado", "202, 163", ".","transparent"]}
            
        ]
    },




    //13
    {
        "respuestas": [
            {
                "t13respuesta": "energía",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "energía cinética",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "equilibrio térmico",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "calor",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>La "
            },
            {
                "t11pregunta": " no se crea ni se destruye solo se transforma. Dentro de nuestro entorno se manifiesta de diferentes maneras, sin embargo hay dos clasificaciones principales: la "
            },
            {
                "t11pregunta": " y la energía potencial.<br><br>Cuando dos objetos tienen o alcanzan temperaturas iguales se dice que están en "
            },
            {
                "t11pregunta": ".<br><br>El "
            },
            {
                "t11pregunta": " siempre se transfiere del cuerpo más caliente hacia el más frío, hasta que los dos alcancen la misma temperatura."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
];