json=[ 
    { 
      "respuestas": [ 
          { 
              "t13respuesta": "Falso", 
              "t17correcta": "0" 
          }, 
          { 
              "t13respuesta": "Verdadero", 
              "t17correcta": "1" 
          } 
      ], 
      "preguntas" : [ 
           
          { 
              "c03id_tipo_pregunta": "13", 
              "t11pregunta": "Tales de Mileto fue el primero en observar  científicamente el fenómeno de la electricidad.", 
              "correcta"  : "1" 
          }, 
          { 
              "c03id_tipo_pregunta": "13", 
              "t11pregunta": "El modelo atómico actual plantea que los protones giran alrededor del núcleo.", 
              "correcta"  : "0" 
          }, 
          { 
              "c03id_tipo_pregunta": "13", 
              "t11pregunta": "Conforme el átomo crece, los electrones se van acomodando en las capas superiores.", 
              "correcta"  : "1" 
          }, 
          { 
              "c03id_tipo_pregunta": "13", 
              "t11pregunta": "Es más fácil separar por un instante los electrones que se encuentran en los orbitales internos.", 
              "correcta"  : "0" 
          }, 
          { 
              "c03id_tipo_pregunta": "13", 
              "t11pregunta": "El movimiento de los electrones genera corriente eléctrica.", 
              "correcta"  : "1" 
          }, 
          { 
            "c03id_tipo_pregunta": "13", 
            "t11pregunta": "El Berilio (Be) es considerado el mejor conductor de electricidad. ", 
            "correcta"  : "0" 
        }, 
      ], 
      "pregunta": { 
          "c03id_tipo_pregunta": "13", 
          "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.", 
          "descripcion":"Aspectos a valorar", 
          "evaluable":false, 
          "evidencio": false 
      } 
  }, 
  //2 
  {   
    "respuestas":[   
       {   
          "t13respuesta":"Protones", 
          "t17correcta":"1" 
       }, 
       {   
          "t13respuesta":"Neutrones", 
          "t17correcta":"1" 
       }, 
       {   
          "t13respuesta":"Electrones", 
          "t17correcta":"0" 
       }, 
       {   
          "t13respuesta":"Enlaces", 
          "t17correcta":"0" 
       } 
    ], 
    "pregunta":{   
       "c03id_tipo_pregunta":"2", 
       "t11pregunta":"Selecciona todas las respuestas correctas.<br>¿Qué partículas subatómicas permanecen fijas dentro del núcleo?" 
    } 
  }, 
  //3 
  {   
    "respuestas":[   
       {   
          "t13respuesta":"Niels Bohr", 
          "t17correcta":"1" 
       }, 
       {   
          "t13respuesta":"Benjamin Franklin", 
          "t17correcta":"0" 
       }, 
       {   
          "t13respuesta":"William  Gilbert", 
          "t17correcta":"0" 
       }, 
       {   
          "t13respuesta":"Alesandro Volta", 
          "t17correcta":"0" 
       } 
    ], 
    "pregunta":{   
       "c03id_tipo_pregunta":"1", 
       "t11pregunta":"Selecciona la respuesta correcta.<br> ¿Quién de los científicos propuso que a partir de su modelo existen diferentes niveles de energía donde los electrones se mueven?" 
    } 
  }, 
  //4 
  { 
    "respuestas": [ 
        { 
            "t13respuesta": "nube electrónica", 
            "t17correcta": "1" 
        }, 
        { 
            "t13respuesta": "electrostática", 
            "t17correcta": "2" 
        }, 
        { 
            "t13respuesta": "electrones", 
            "t17correcta": "3" 
        }, 
        { 
            "t13respuesta": "núcleo", 
            "t17correcta": "4" 
        }, 
        { 
            "t13respuesta": "electrón", 
            "t17correcta": "5" 
        }, 
        { 
            "t13respuesta": "corriente eléctrica", 
            "t17correcta": "6" 
        } 
    ], 
    "preguntas": [ 
        { 
            "t11pregunta": "En algunos elementos como los metales, la " 
        }, 
        { 
            "t11pregunta": " está muy alejada del núcleo, por lo que la fuerza " 
        }, 
        { 
            "t11pregunta": " es menor. Esto permite que los últimos " 
        }, 
        { 
            "t11pregunta": " sean libres de moverse.<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;La poca fuerza de atracción hacia el " 
        }, 
        { 
            "t11pregunta": " permite que alguno de los últimos electrones salga de la nube. El espacio libre que deja es ocupado por otro " 
        }, 
        { 
            "t11pregunta": ". Este intercambio de electrones entre átomos produce " 
        }, 
        { 
            "t11pregunta": "." 
        } 
    ], 
    "pregunta": { 
        "c03id_tipo_pregunta": "8", 
        "t11pregunta": "Completa el párrafo con las palabras del recuadro.", 
        "respuestasLargas": true, 
        "pintaUltimaCaja": false, 
        "contieneDistractores":true 
    } 
  } 
    
  ];