json = [
    {
        "respuestas": [
            {
                "t13respuesta": "Origen",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Magnitud",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Dirección",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Sentido",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Eje Y",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "Eje X",
                "t17correcta": "6",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "a"
            },
            {
                "t11pregunta": "b"
            },
            {
                "t11pregunta": "c"
            },
            {
                "t11pregunta": "d"
            },
            {
                "t11pregunta": "e"
            },
            {
                "t11pregunta": "f"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda",
            "t11instruccion": "<center> <img style='height: 500px; display:block; ' src='NI8E_B01_R01.png'> </center>",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "Posición fija que se utiliza para observar y medir el movimiento de un objeto.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Objeto o señal que se toma como base para hacer mediciones de manera subjetiva.",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Cambio de posición con respecto a una referencia.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "La distancia y dirección de un objeto con respecto al origen de su trayectoria.",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Camino seguido en un cambio de posición desde el inicio hasta el final.",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Punto de referencia"
            },
            {
                "t11pregunta": "Marco de referencia"
            },
            {
                "t11pregunta": "Movimiento"
            },
            {
                "t11pregunta": "Posición"
            },
            {
                "t11pregunta": "Trayectoria"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Posición",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Desplazamiento",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Velocidad",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "Trayectoria",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "Aceleración",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Ubicación de un objeto o sujeto con respecto a un observador."
            },
            {
                "t11pregunta": "Se refiere al cambio de posición de un objeto."
            },
            {
                "t11pregunta": "Cambio de posición considerando el cambio en el tiempo."
            },
            {
                "t11pregunta": "Ruta que sigue el objeto desde el principio hasta el fin del movimiento."
            },
            {
                "t11pregunta": "Cambio de la velocidad dividido por el cambio en el tiempo"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Aristóteles planteó tres tipos de movimiento: natural, violento y simple.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El movimiento de tipo violento para Aristóteles era aquel originado por fuerzas externas al objeto.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Según el movimiento natural, todos los objetos tienen un lugar propio en la naturaleza y tienden a moverse hacia él.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término experimentar se refiere a observar repetidamente un fenómeno y recolectar información sobre este.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Galileo Galilei encontró que el material del que estaba hecho el objeto influía durante la caída libre del mismo. ",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso o verdadero según corresponda.",
            "t11instruccion": "",
            "descripcion": "Pregunta",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
        //5

    //inicio
    {
            "respuestas": [        
                {
                   "t13respuesta":  "fuerza",
                     "t17correcta": "1"
                 },
                {
                   "t13respuesta":  "intensidad",
                     "t17correcta": "2"
                 },
                {
                   "t13respuesta":  "sentido",
                     "t17correcta": "3"
                 },
                {
                   "t13respuesta":  "dirección",
                     "t17correcta": "4"
                 },
                {
                   "t13respuesta":  "fuerza de gravedad",
                     "t17correcta": "5"
                 },
                {
                    "t13respuesta":  "masa",
                      "t17correcta": "6"
                  },
                  {
                     "t13respuesta":  "materia",
                       "t17correcta": "7"
                   },
                    {
                      "t13respuesta":  "trabajo",
                        "t17correcta": "8"
                    },
            ],
            "preguntas": [
              {
              "t11pregunta": "La "
              },
              {
              "t11pregunta": " es una acción que genera un movimiento.<br>El tamaño del vector que representa a la fuerza se conoce como "
              },
              {
              "t11pregunta": ".<br>El  "
              },
              {
              "t11pregunta": " indica hacia donde se aplica o dirige la fuerza.<br>La "
              },
              {
              "t11pregunta": " se refiere a la línea sobre la cual actúa la fuerza.<br>La "
              },
              {
              "t11pregunta": " se determina multiplicando la masa por la aceleración.<br>"
              },
            ],
            "pregunta": {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "Arrastra las palabras que completen el párrafo.",
               "t11instruccion": "",
               "respuestasLargas": true,
               "pintaUltimaCaja": false,
               "contieneDistractores": true
             }
          },
        //fin
//6
{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La trayectoria recorrida puede ser mayor al desplazamiento de un objeto.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La fuerza resultante representa a una sola fuerza que genera el mismo efecto que todo el sistema de fuerzas.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Dos fuerzas iguales que actúan sobre el mismo eje en sentido contrario se neutralizan.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Dos fuerzas iguales que actúan sobre el mismo eje en el mismo sentido se neutralizan.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Dos fuerzas que actúan en el mismo sentido sobre el mismo eje se suman.",
            "correcta": "1",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Elige falso o verdadero según corresponda.",
        "t11instruccion": "",
        "descripcion": "Pregunta",
        "variante": "editable",
        "anchoColumnaPreguntas": 30,
        "evaluable": true
    }
},
// 7
{
  "respuestas":[
 {
  "t13respuesta":"Ley de la Inercia; un cuerpo permanecerá en movimiento rectilíneo uniforme indefinidamente si no actúa sobre él ningún otro cuerpo",
  "t17correcta":"1",
  },
 {
  "t13respuesta":"El cambio de movimiento es directamente proporcional a la fuerza motriz.",
  "t17correcta":"2",
  },
 {
  "t13respuesta":"A toda acción corresponde una reacción con la misma intensidad, pero en sentido contrario.",
  "t17correcta":"3",
  },
 {
  "t13respuesta":"Equivale a una velocidad cero.",
  "t17correcta":"4",
  },
 {
  "t13respuesta":"Se refiere a un cambio de velocidad con respecto al tiempo",
  "t17correcta":"5",
  },
 ],
  "preguntas": [
  {
  "t11pregunta":"Primera Ley de Newton"
  },
 {
  "t11pregunta":"Segunda Ley de Newton"
  },
 {
  "t11pregunta":"Tercera Ley de Newton"
  },
 {
  "t11pregunta":"Estado de reposo"
  },
 {
  "t11pregunta":"Aceleración"
  },
  ],
  "pregunta":{
  "c03id_tipo_pregunta":"12",
  "t11instruccion": "",
  "altoImagen":"100px",
  "anchoImagen":"200px"
  }
},
//8
{
  "respuestas": [        
          {
             "t13respuesta":  "gravedad",
               "t17correcta": "1"
           },
          {
             "t13respuesta":  "aerodinámico",
               "t17correcta": "2"
           },
          {
             "t13respuesta":  "inercia",
               "t17correcta": "3"
           },
          {
             "t13respuesta":  "energía cinética",
               "t17correcta": "4"
           },
          {
             "t13respuesta":  "energía química",
               "t17correcta": "5"
           },
          {
             "t13respuesta":  "energía potencial",
               "t17correcta": "6"
           },
   {
              "t13respuesta":  "masa",
                "t17correcta": "0"
            },
           {
              "t13respuesta":  "fuerza",
                "t17correcta": "0"
            },
           {
              "t13respuesta":  "energía calorífica",
                "t17correcta": "0"
            },
           {
              "t13respuesta":  "trabajo",
                "t17correcta": "0"
            },
           {
              "t13respuesta":  "Energií mecánica",
                "t17correcta": "0"
            },
      ],
      "preguntas": [
        {
        "t11pregunta": "a. La "
        },
        {
        "t11pregunta": " se refiere a la fuerza con que los objetos son atraídos hacia el centro de la Tierra.<br>b. El término "
        },
        {
        "t11pregunta": " se refiere a los objetos que tienen una forma adecuada para reducir la resistencia al aire.<br>c. La "
        },
        {
        "t11pregunta": " es la propiedad de los cuerpos de mantener su estado de reposo o movimiento.<br>d. La "
        },
        {
        "t11pregunta": " es aquella que un cuerpo posee por su movimiento.<br>e. La "
        },
        {
        "t11pregunta": " proviene de un cambio químico dentro del organismo.<br>f. La "
        },
        {
        "t11pregunta": "  es la capacidad que tiene un cuerpo para realizar un trabajo por su posición.<br>"
        },
      ],
      "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "",
         "t11instruccion": "",
         "respuestasLargas": true,
         "pintaUltimaCaja": false,
         "contieneDistractores": true
       }
},
//9 pendiente
{
  "respuestas": [
      {
          "t13respuesta": "NI8E_B01_R02_02.png",
          "t17correcta": "0",
          "columna": "0"
      },
      {
          "t13respuesta": "NI8E_B01_R02_03.png",
          "t17correcta": "1",
          "columna": "0"
      },
      {
          "t13respuesta": "NI8E_B01_R02_04.png",
          "t17correcta": "2",
          "columna": "0"
      },
      {
          "t13respuesta": "NI8E_B01_R02_05.png",
          "t17correcta": "3",
          "columna": "0"
      },
      {
          "t13respuesta": "NI8E_B01_R02_06.png",
          "t17correcta": "4",
          "columna": "0"
      },
      {
          "t13respuesta": "NI8E_B01_R02_07.png",
          "t17correcta": "6",
          "columna": "0"
      },
      {
          "t13respuesta": "NI8E_B01_R02_08.png",
          "t17correcta": "6",
          "columna": "1"
      },
      {
          "t13respuesta": "NI8E_B01_R02_09.png",
          "t17correcta": "6",
          "columna": "1"
      },
      {
          "t13respuesta": "NI8E_B01_R02_10.png",
          "t17correcta": "6",
          "columna": "1"
      }
  ],
  "pregunta": {
      "c03id_tipo_pregunta": "5",
      "t11pregunta": "<p> Arrastra la imagen al espacio que corresponda.<\/p>",
      "tipo": "ordenar",
      "imagen": true,
      "url": "NI8E_B01_R02_01.png",
      "anchoImnagen": 30,
      "respuestaImagen": true,
      "tamanyoReal": false,
      "bloques": false,
      "borde": false

  },
  "contenedores": [
      { "Contenedor": ["", "52,293", "cuadrado", "62, 40", ".", "transparent"] },
      { "Contenedor": ["", "52,388", "cuadrado", "62, 40", ".", "transparent"] },
      { "Contenedor": ["", "276,132", "cuadrado", "62, 40", ".", "transparent"] },
      { "Contenedor": ["", "276,495", "cuadrado", "62, 40", ".", "transparent"] },
      { "Contenedor": ["", "458,312", "cuadrado", "62, 40", ".", "transparent"] }
  ]
},
//10
{
  "respuestas": [
      {
          "t13respuesta": "Masa de los objetos",
          "t17correcta": "1",
          "numeroPregunta": "0"
      },
      {
          "t13respuesta": "Distancia entre los centros",
          "t17correcta": "1",
          "numeroPregunta": "0"
      },
      {
          "t13respuesta": "Aceleración del objeto de mayor masa",
          "t17correcta": "0",
          "numeroPregunta": "0"
      },
      {
          "t13respuesta": "Peso de los objetos",
          "t17correcta": "0",
          "numeroPregunta": "0"
      },

  ],
  "pregunta": {
      "c03id_tipo_pregunta": "2",
      "t11pregunta": ["Selecciona las respuestas correctas<br><br>¿De qué elementos depende la fuerza de gravitación?"
      ],
      "preguntasMultiples": true
  }
},
// 11
{
  "respuestas":[
  {
   "t13respuesta":"<img src='NI8E_B01_R04_01.png'>",
   "t17correcta":"1",
   },
   {
    "t13respuesta":"<img src='NI8E_B01_R04_02.png'>",
    "t17correcta":"2",
    },
  {
   "t13respuesta":"Vector con dirección vertical que es consecuencia de la atracción gravitacional entre el objeto y la persona.",
   "t17correcta":"3",
   },
  {
   "t13respuesta":"La fuerza que aplicada durante un segundo a una masa de 1 kg incrementa su velocidad en 1 m/s.",
   "t17correcta":"4",
   },
  {
   "t13respuesta":"6.6726 x10-11N",
   "t17correcta":"5",
   },
  ],
   "preguntas": [
   {
   "t11pregunta":"Fuerza gravitacional"
   },
   {
   "t11pregunta":"Campo gravitacional"
   },
  {
   "t11pregunta":"Peso"
   },
  {
   "t11pregunta":"Newton"
   },
  {
   "t11pregunta":"Constante de G"
   },
   ],
   "pregunta":{
   "c03id_tipo_pregunta":"12",
   "t11instruccion": "",
   "altoImagen":"100px",
   "anchoImagen":"200px"
   }
},
];
