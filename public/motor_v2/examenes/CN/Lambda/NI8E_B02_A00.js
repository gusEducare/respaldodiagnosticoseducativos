json = [
    //1
	{
        "respuestas": [
            {
                "t13respuesta": "Ley de la Inercia; un cuerpo permanecerá en movimiento rectilíneo uniforme indefinidamente si no actúa sobre él ningún otro cuerpo.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El cambio de movimiento es directamente proporcional a la fuerza motriz.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "A toda \"acción\" corresponde una reacción con la misma intensidad, pero en sentido contrario.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Equivale a una velocidad cero.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Se refiere a un cambio de velocidad con respecto al tiempo",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Primera Ley de Newton"
            },
            {
                "t11pregunta": "Segunda Ley de Newton"
            },
            {
                "t11pregunta": "Tercera Ley de Newton"
            },
            {
                "t11pregunta": "Estado de reposo"
            },
            {
                "t11pregunta": "Aceleración"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona los siguientes conceptos según corresponda:"
        }
	},
    //2
	{
        "respuestas": [
            {
                "t13respuesta": "<p>gravedad<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>aerodinámico<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>inercia<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>cinética<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>química<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>potencial<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<ol><li>La "
            },
            {
                "t11pregunta": " se refiere a la fuerza con que los objetos son atraídos hacia el centro de la Tierra.</li><li>El término "
            },
            {
                "t11pregunta": " se refiere a los objetos que tienen una forma adecuada para reducir la resistencia al aire.</li><li>La "
            },
            {
                "t11pregunta": " es la propiedad de los cuerpos de mantener su estado de reposo o movimiento.</li><li>La energía "
            },
            {
                "t11pregunta": " es aquella que un cuerpo posee por su movimiento.</li><li>La energía "
            },
            {
                "t11pregunta": " proviene de un cambio químico dentro del organismo.</li><li>La energía "
            },
            {
                "t11pregunta": "es la capacidad que tiene un cuerpo para realizar un trabajo por su posición.</li></ol>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los conceptos para completar los enunciados según corresponda:",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
	},
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todas las cosas que vemos rugosas a nivel microscópico presentan fricción.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término masa puede utilizarse como sinónimo del término <i>peso</i>.",
                "correcta"  : "1"
            },	
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El peso es consecuencia de la interacción de la fuerza de gravedad con los objetos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El término masivo se refiere a la cantidad de masa presente en el objeto.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "\“A mayor masa, menor inercia.\”",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": " Escribe V (verdadero) o F (falso) según corresponda:",
            "variante": "editable",
            "anchoColumnaPreguntas": 60,
            "evaluable"  : true
        }        
	},
    //4
	 {  
      "respuestas":[
         {  
            "t13respuesta": "Ninguna fuerza",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Fuerza de atracción",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Fuerza de gravedad",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Fuerza de repulsión",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Energía cinética",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Inercia",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Adaptabilidad",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Acción-reacción",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Desaceleración",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Aceleración",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Movimiento negativo",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Aceleración contraria",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Favorece la desaceleración normal.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Su diseño aerodinámico reduce la resistencia al aire.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Al disminuir la fuerza de acción al  tocar el piso, la fuerza de reacción será menor.",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "Evita que la aceleración aumente.",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Observa la imagen presionando \"+\" y selecciona la respuesta correcta:<br><br>Durante la caída libre, ¿qué fuerza actúa sobre la persona?",
                         "Según la primera ley de Newton, ¿qué propiedad intrínseca del individuo hace que siga cayendo?",
                         "Según la segunda ley de Newton, ¿qué pasa cuando se abre el paracaídas?",
                         "Según la tercera ley de Newton, ¿qué función cumple abrir el paracaídas?"],
         "t11instruccion":"<center><img src='NI8E_B02_A04_01.png'/><center/>",                
         "preguntasMultiples": true,
      }
   },
   //5
    {
      "respuestas": [
          {
              "t13respuesta": "NI8E_B02_A05_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI8E_B02_A05_03.png",
              "t17correcta": "3",
              "columna":"0"
          },
          {
              "t13respuesta": "NI8E_B02_A05_04.png",
              "t17correcta": "4",
              "columna":"1"
          },
          {
              "t13respuesta": "NI8E_B02_A05_05.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "NI8E_B02_A05_06.png",
              "t17correcta": "1",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la letra que corresponda a cada momento del diagrama sobre la fuerza gravitacional.<br><br>a: M= masa masiva<br>b: m= masa menos masiva<br>c: r= distancia<br>d: -F= fuerza<br>e: F= fuerza",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI8E_B02_A05_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "196,82", "cuadrado", "100,40 ", ".","transparent"]},
          {"Contenedor": ["", "59,222", "cuadrado", "100,40", ".","transparent"]},
          {"Contenedor": ["", "59,382", "cuadrado", "100,40", ".","transparent"]},
          {"Contenedor": ["", "196,492", "cuadrado", "100,40", ".","transparent"]},
          {"Contenedor": ["", "428,286", "cuadrado", "100,40", ".","transparent"]},
      ]
  },
  //6
   {  
      "respuestas":[  
         {  
            "t13respuesta":"Masa de los objetos",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Peso de los objetos",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Distancia entre los centros",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Aceleración del objeto de mayor masa",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "t11pregunta":"Selecciona las respuestas correctas:<br><br>¿De qué elementos depende la fuerza de gravitación?"
      }
   },
   //7
   {
        "respuestas": [
            {
                "t13respuesta": "<img src='NI8E_B02_A07_01.png' />",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='NI8E_B02_A07_02.png' />",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Vector con dirección vertical que es consecuencia de la atracción gravitacional entre el objeto y la persona.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "La fuerza que aplicada durante un segundo a una masa de 1 kg incrementa su velocidad en 1 m/s.",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "6.6726 x10-11N",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Fuerza gravitacional"
            },
            {
                "t11pregunta": "Campo gravitacional"
            },
            {
                "t11pregunta": "Peso"
            },
            {
                "t11pregunta": "Newton"
            },
            {
                "t11pregunta": "Valor de la constante de la gravitación universal."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona los siguientes conceptos según corresponda.",
            "anchoImagen":150,
            "altoImagen":150
        }
    },
    //8
     {  
      "respuestas":[  
         {  
            "t13respuesta":"Potencia",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Movimiento",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":"Energía",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":"Electricidad",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta:<br><br>¿Qué significa el adjetivo griego kinesis?"
      }
   },
   //9
    {
      "respuestas": [
          {
              "t13respuesta": "NI8E_B02_A09_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI8E_B02_A09_03.png",
              "t17correcta": "0",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra el tipo de energía que corresponde a cada imagen:",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI8E_B02_A09_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "438,10", "cuadrado", "311, 40", ".","transparent"]},
          {"Contenedor": ["", "438,323", "cuadrado", "311, 40", ".","transparent"]}
      ]
  },
  //10
  {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La energía potencial depende de la altura en la que se encuentren los objetos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los diferentes tipos de energía se pueden explicar a través un concepto: energía cinética.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al estar cayendo un objeto, la energía potencial disminuye en proporción a la altura.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Al estar cayendo un objeto, la energía cinética aumenta en proporción a la velocidad.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La energía potencial al inicio de la caída es superior a la energía cinética al final.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Selecciona verdadero o falso según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false
        }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Falso",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La energía potencial depende de la altura en la que se encuentren los objetos.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La energía potencial y la energía cinética nunca llegan a igualarse dentro de la trayectoria del péndulo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La energía cinética alcanza su menor valor cuando el péndulo llega al extremo de la curva.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La energía potencial alcanza su mayor valor cuando el péndulo se encuentra justo a la mitad del recorrido.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La suma de energía tanto potencial como cinética siempre da el mismo resultado.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente tabla del <i>Principio de conservación de la energía</i> presionando \"+\" y selecciona verdadero o falso según corresponda:",
            "descripcion":"Aspectos a valorar",
            "evaluable":false,
            "evidencio": false,
            "t11instruccion":"<center><img src='NI8E_B02_A11_01.png' style='width:800px;'/><center/>"
        }
    },

];