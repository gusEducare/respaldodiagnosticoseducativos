json=[ 

   {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El <b>punto de referencia</b> es la posición fija que se utiliza para observar y medir el movimiento de un objeto.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La <b>trayectoria</b> es la distancia y dirección de un objeto con respecto al origen de su posición.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El <b>movimiento</b> es el cambio de posición con respecto a un punto de referencia.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La <b>distancia</b> es el camino seguido en un cambio de posición desde el inicio hasta el final.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El <b>marco de referencia</b> es el objeto o señal que se toma como base para hacer mediciones de manera subjetiva.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },//////////2
     {
        "respuestas": [
            {
                "t13respuesta": "Distancia recorrida desde el punto inicial hasta el punto final de un movimiento.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Cambio de la velocidad dividido por el cambio en el tiempo. ",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Distancia recorrida en un cierto tiempo.",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Ubicación de un cuerpo con respecto a un marco de referencia.",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Desplazamiento"
            },
            {
                "t11pregunta": "Aceleración"
            },
            {
                "t11pregunta": "Velocidad"
            },
            {
                "t11pregunta": "Posición"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda"
        }
    },/////////3
      {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003E<i><big>F</big>=(m)(a)</i>\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<i><big>F</big>=<span class='fraction'><span class='top'>m</span><fraction class='black'><span class='bottom'>a</span></span></i>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003E<i><big>F</big>=<span class='fraction'><span class='top'>a</span><fraction class='black'><span class='bottom'>m</span></span></i>\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003EUna acción que genera un movimiento.\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003EEl tamaño del vector que representa al movimiento.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003ELa línea sobre la cual actúa el movimiento.\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          {  
            "t13respuesta":"\u003Cp\u003ENewtons\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EKilogramos\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"\u003Cp\u003EMetros\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },//
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cuál de las siguientes es la fórmula para obtener la fuerza ejercida por un cuerpo?",
         "¿Qué es la fuerza?",
         "¿Cuál es la unidad del sistema internacional para medir las fuerzas?"]
      }
   },//////////////4
     {  
      "respuestas":[  
         {  
            "t13respuesta":"\u003Cp\u003Eb\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Ea\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"\u003Cp\u003Ec\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },//
         {  
            "t13respuesta":"\u003Cp\u003Ec\u003C\/p\u003E",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Ea\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"\u003Cp\u003Eb\u003C\/p\u003E",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },//
          
         ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "preguntasMultiples":true,
         "t11pregunta":["Observa la imagen y selecciona la respuesta correcta.<br><img src='NI8E_B05_A04.png'/><br>¿Cuál letra representa la magnitud del vector?",
         "¿Cuál letra representa la dirección del vector?"]
      }
   },////////5
   {
        "respuestas": [
            {
                "t13respuesta": "Opción 1",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opción 2",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Opción 3",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Opción 4",
                "t17correcta": "3"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Durante la caída libre, ¿qué fuerza actúa sobre la persona?",
                "valores": ['Fuerza de gravedad', 'Ninguna fuerza', 'Fuerza de atracción','Fuerza de repulsión'],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Según la primera ley de Newton, ¿qué propiedad intrínseca del individuo hace que siga cayendo?",
                "valores": ['Energía cinética', 'Inercia', 'Acción-reacción','Adaptabilidad'],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Según la segunda ley de Newton, ¿qué pasa cuando se abre el paracaídas?",
                "valores": ['Movimiento negativo', 'Aceleración contraria', 'Aceleración', 'Desaceleración'],
                "correcta"  : "3"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Según la tercera ley de Newton, ¿qué función cumple abrir el paracaídas?",
                "valores": ['Favorece la desaceleración normal', 'Al disminuir la fuerza con la que se toca el piso, la fuerza de reacción será menor.', 'Su diseño aerodinámico reduce la resistencia al aire', 'Porque evita que la aceleración aumente.'],
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la imagen y en la siguiente matriz marca la opción que corresponda a cada pregunta.<br><center><img src='NI8E_B05_A05.png' width=400px height=250px/></center>",
            "descripcion": "Preguntas",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },/////////////////////6
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El Newton es la fuerza necesaria para acelerar a 1 m/s<sup>2</sup> un kilogramo de masa.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La constante G tiene un valor de 6.6726 x 10<sup>-11</sup><i><span class='fraction'><span class='top'>Nm<sup>2</sup></span><span class='bottom'>Kg<sup>2</sup></span></span></i>",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La ley de Gravitación universal dice que la fuerza gravitatoria aumenta si los cuerpos se empiezan a separar.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El peso es la fuerza de atracción que ejerce la Tierra sobre un cuerpo.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La unidad de medida del peso en el sistema internacional es el gramo.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },////////////////7
    {
      "respuestas": [
          {
              "t13respuesta": "NI8E_B05_A07_I02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI8E_B05_A07_I03.png",
              "t17correcta": "1",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Arrastra la imagen y completa el siguiente esquema para explicar la energía",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI8E_B05_A07_I01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "158,466", "cuadrado", "102, 50", ".","transparent"]},
          {"Contenedor": ["", "412,410", "cuadrado", "102, 50", ".","transparent"]}
      ]
  }, //////////////8
  {
        "respuestas": [
            {
                "t13respuesta": "<p>Aristóteles<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>Rudolf Clausius<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Galileo Galilei<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Isaac Newton<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Demócrito<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;decía que la naturaleza estaba conformada por cuatro elementos: agua, fuego, aire y tierra.<br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;dedujo que el calor se transmite siempre del cuerpo de mayor temperatura hacia el cuerpo de menor temperatura.<br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;explicó el movimiento de un objeto en caída libre, y descubrió la aceleración debida a la gravedad entre otras cosas.<br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;dijo: “La materia está hecha de partículas que interactúan entre sí mediante fuerzas atractivas y repulsivas.”<br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;afirmó que la naturaleza era divisible hasta pequeñísimas partículas que llamó átomos.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },///////9
     {
      "respuestas": [
          {
              "t13respuesta": "NI8E_B05_A09_I02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "NI8E_B05_A09_I03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "NI8E_B05_A09_I04.png",
              "t17correcta": "2",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Ordena los siguientes elementos de acuerdo a la numeración.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"NI8E_B05_A09_I01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "80,10", "cuadrado", "234, 64", ".","transparent"]},
          {"Contenedor": ["", "282,184", "cuadrado", "234, 64", ".","transparent"]},
          {"Contenedor": ["", "80,400", "cuadrado", "234, 64", ".","transparent"]}
      ]
  },////////10
    {
        "respuestas": [
            {
                "t13respuesta": "Se transforma en energía eléctrica por el uso de celdas solares.",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Se transforma en energía eléctrica por los molinos de viento que generan electricidad.",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Energía mecánica",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Energía luminosa",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Energía solar"
            },
            {
                "t11pregunta": "Energía eólica"
            },
            {
                "t11pregunta": "¿En qué se convierte la energía eólica cuando pasa a través de las aspas que se mueven por el viento?"
            },
            {
                "t11pregunta": "¿En qué se transforma la energía eléctrica al encender un foco?"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda",
        }
    },//////////////11
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Tales de Mileto descubrió que el tejido animal respondía a la electricidad.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Alessandro Volta descubrió que al frotar el ámbar con lana aparecía un fenómeno atrayente.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "André-Marie Ampere observó que las corrientes eléctricas producen campos magnéticos.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Charles Coulomb descubrió la fuerza de repulsión y de atracción a partir de las cargas eléctricas.",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Luigi Galvani inventó el electróforo, el estroscopio y la pila.",
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "descripcion": "Reactivo",   
            "variante": "editable",
            "anchoColumnaPreguntas": 55,
            "evaluable"  : true
        }        
    },////////////12
    {
        "respuestas": [
            {
                "t13respuesta": "<p>difracción<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>descompone<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>fotón<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>partícula<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>espectro visible<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>reflexión<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>compone<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>electrón<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>molécula<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>onda<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>De acuerdo a la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>, la luz blanca al refractar en el prisma se&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;en los colores que la integran.<br><br>El término&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;se refiere a la&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;elemental que compone a la luz.<br><br>A la gama de colores que el ojo humano puede ver se le conoce como&nbsp;<\/p>"
            },
            {
                "t11pregunta": "<p>.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.<br><center><img src='NI8E_B05_A12.png' width=380px height=260px/></center>",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },///////////////13
     {  
      "respuestas":[  
         {  
            "t13respuesta":     "Disminuir el consumo eléctrico favorece un desarrollo sustentable.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Las fotoceldas producen electricidad a partir de la energía solar.",
            "t17correcta":"1"
         },
         {  
            "t13respuesta":     "Alterar la naturaleza para un beneficio tecnológico es un proceso que puede darse de inmediato.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "La producción de fotoceldas y sus desechos no presentan riesgos para el ambiente.",
            "t17correcta":"0"
         },
         {  
            "t13respuesta":     "Aprovechar la naturaleza para obtener energía no conlleva un impacto negativo.",
            "t17correcta":"0"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"Selecciona todas las respuestas correctas.<br><br>Elige los enunciados que sean correctos."
      }
   }
  ];