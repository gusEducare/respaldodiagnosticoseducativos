

json=[
  //1
   
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<span class='fraction '><span class='top'>21</span><span class='bottom'>4</span></span>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "32",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "4",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "5.4",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            
            {
                "t11pregunta": "<span class='fraction '><span class='top'>1</span><span class='bottom'>4</span></span>,\n\
                                <span class='fraction '><span class='top'>5</span><span class='bottom'>4</span></span>,\n\
                                <span class='fraction '><span class='top'>9</span><span class='bottom'>4</span></span>,\n\
                                <span class='fraction '><span class='top'>13</span><span class='bottom'>4</span></span>,\n\
                                <span class='fraction '><span class='top'>17</span><span class='bottom'>4</span></span>…"
            },
            {
                "t11pregunta": "2,7,12,17,22,27..."
            },
            {
                "t11pregunta": "<span class='fraction '><span class='top'>2</span><span class='bottom'>2</span></span>,\n\
                                1<span class='fraction '><span class='top'>1</span><span class='bottom'>2</span></span>,\n\
                                <span class='fraction '><span class='top'>4</span><span class='bottom'>2</span></span>,\n\
                                <span class='fraction '><span class='top'>5</span><span class='bottom'>2</span></span>,\n\
                                3,<span class='fraction '><span class='top'>7</span><span class='bottom'>2</span></span>…"
            },
            {
                "t11pregunta": "0.6,1.4,2.2,3,3.8,4.6…"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
             "t11pregunta": "Relaciona las columnas según corresponda.",           
            "altoImagen":"150"
        }
    },
    //3
    {
      "respuestas":[
         {
            "t13respuesta":     "<span class='fraction'><span class='top'>1</span><fraction class='black'><span class='bottom'>8</span></span> de pizza",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "<span class='fraction'><span class='top'>2</span><fraction class='black'><span class='bottom'>4</span></span> de pizza",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "<span class='fraction'><span class='top'>1</span><fraction class='black'><span class='bottom'>2</span></span> de pizza",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "Dos rebanadas",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Tres rebanadas",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Una y media rebanadas",
            "t17correcta":"0",
          "numeroPregunta":"2"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "Catherine compró tres pizzas para el convivio de la escuela de sus once compañeros más ella, cada pizza está dividida en ocho rebanadas y todos comieron la misma cantidad de rebanadas.",
         "t11pregunta":["Selecciona la respuesta correcta.","¿Qué porción de pizza representa una rebanada?",
                        "¿Cuántas rebanadas de pizza le tocó a cada niño?"],
       "preguntasMultiples": true
      }
    },
    //4
     {
      "respuestas": [
          {
              "t13respuesta": "MI6E_B04_A04_01.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B04_A04_02.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B04_A04_03.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B04_A04_04.png",
              "t17correcta": "3",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra los elementos y completa la tabla.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"MI6E_B04_A04_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["0", "94,165", "cuadrado", "156, 193", ".","transparent"]},
          {"Contenedor": ["1", "94,481", "cuadrado", "157, 193", ".","transparent"]},
          {"Contenedor": ["2", "289,165", "cuadrado", "156, 193", ".","transparent"]},
          {"Contenedor": ["3", "289,481", "cuadrado", "157, 193", ".","transparent"]}
      ]
  },
    //5
    {
        "respuestas": [
            {
                "t17correcta": "31.4"
            },
            {
                "t17correcta": "78.5"
            },
            {
                "t17correcta": "x"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><center><img src='MI6E_B04_A05_01.png'><br><br>Perímetro: "
            },
            {
                "t11pregunta": " cm<br><br><br> Área:"
            },
            {
                "t11pregunta": " cm<sup>2</sup>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Calcula el perímetro y área del círculo. Utiliza una cifra decimal sin redondear.",
            evaluable:true,
            "pintaUltimaCaja":false
        }
    },
    //6
    {
        "respuestas": [
            
            {
                "t17correcta": "18"
            },
            {
                "t17correcta": "x"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><center><img src='MI6E_B04_A06_01.png'><br><br>Volumen<br> V="
            },
            {
                "t11pregunta": " cm<sup>3</sup>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
             "t11pregunta": "Calcula el volumen del prisma triangular.",
            evaluable:true,
            "pintaUltimaCaja":false
        }
    },
     {
      "respuestas":[
         {
            "t13respuesta":     "10",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "100",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "1000",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////7
         {
            "t13respuesta":     "10",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "100",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "1000",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         /////////////////////////////////////
         {
            "t13respuesta":     "10",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "100",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "1000",
            "t17correcta":"1",
          "numeroPregunta":"3"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "Elige el denominador que corresponde a la representación de la fracción en decimal.",
         "t11pregunta":["Selecciona la respuesta correcta.<br />",
                        "0.3 = <span class='fraction black'><span class='top'>3</span><span class='bottom'>?</span></span>",
                        "0.63 = <span class='fraction black'><span class='top'>63</span><span class='bottom'>?</span></span>",
                        "0.194 = <span class='fraction black'><span class='top'>194</span><span class='bottom'>?</span></span>"],
       "preguntasMultiples": true,

      }
    },
];