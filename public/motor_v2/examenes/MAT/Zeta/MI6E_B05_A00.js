json=[
  //1
    {
      "respuestas":[
         {
            "t13respuesta":     "5, 10, 15, 20, 25, 30..",
            "t17correcta":"1",
         },
         {
            "t13respuesta":     "10,15, 32, 25, 45, 27..",
            "t17correcta":"0",
         },
         {
            "t13respuesta":     "5,12, 24, 29, 36, 48..",
            "t17correcta":"0",
         },
         {
            "t13respuesta":     "5, 22,16, 48, 35, 75..",
            "t17correcta":"0",
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":"Selecciona la respuesta correcta.<br><br>¿Cuáles son los primeros cinco múltiplos de 5?",
                        
      }
    },
    //2
    {
      "respuestas":[
         {
            "t13respuesta":     "18",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "36",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "84",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "72",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "31",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "13",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "25",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "35",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "60",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "51",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "24",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "552",
            "t17correcta":"0",
          "numeroPregunta":"1"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11pregunta":["Selecciona todas las respuestas correctas.<br><br>Selecciona todos los múltiplos de 3.",
                        "Selecciona todos los múltiplos de 5."],
       "preguntasMultiples": true
      }
    },
    //3
   
      //4
        {
            "respuestas": [
                {
                    "t13respuesta": "MI6E_B05_A04_01.png",
                    "t17correcta": "0",
                    "columna":"0"
                },
                {
                    "t13respuesta": "MI6E_B05_A04_02.png",
                    "t17correcta": "1",
                    "columna":"0"
                },
                {
                    "t13respuesta": "MI6E_B05_A04_03.png",
                    "t17correcta": "2",
                    "columna":"1"
                },
                {
                    "t13respuesta": "MI6E_B05_A04_04.png",
                    "t17correcta": "3",
                    "columna":"1"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "5",
                "t11pregunta": "Arrastra la imagen según corresponda.",
                "tipo": "ordenar",
                "imagen": true,
                "url":"MI6E_B05_A04_00.png",
                "respuestaImagen":true, 
                "tamanyoReal":true,
                "bloques":false,
                "borde":false
                
            },
            "contenedores": [
                {"Contenedor": ["", "64,423", "cuadrado", "97, 98", ".","transparent"]},
                {"Contenedor": ["", "164,423", "cuadrado", "97, 98", ".","transparent"]},
                {"Contenedor": ["", "264,423", "cuadrado", "97, 98", ".","transparent"]},
                {"Contenedor": ["", "363,423", "cuadrado", "97, 98", ".","transparent"]},
            ]
        },
    
      //5
      
    //6
    {
      "respuestas": [
          {
              "t13respuesta": "MI6E_B05_A06_02.png",
              "t17correcta": "0,1,2,4",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B05_A06_02.png",
              "t17correcta": "0,1,2,4",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B05_A06_02.png",
              "t17correcta": "0,1,2,4",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B05_A06_01.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B05_A06_02.png",
              "t17correcta": "0,1,2,4",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra la imagen según corresponda.<\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"MI6E_B05_A06_00.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":true,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["", "137,290", "cuadrado", "50, 50", ".","transparent"]},
          {"Contenedor": ["", "187,290", "cuadrado", "50, 50", ".","transparent"]},
          {"Contenedor": ["", "237,290", "cuadrado", "50, 50", ".","transparent"]},
          {"Contenedor": ["", "287,290", "cuadrado", "50, 50", ".","transparent"]},
          {"Contenedor": ["", "337,290", "cuadrado", "50, 50", ".","transparent"]}
      ]
  }
  ,
  {
        "respuestas": [
            {
                "t17correcta": "48"
            },
            {
              "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<center><img src='MI6E_B05_A05_01.png'></center><br><br>Se va a colocar una malla alrededor de dos terrenos iguales, cada uno tiene forma de un triángulo isósceles, como el que se muestra en la siguiente imagen, ¿cuántos metros de malla se requieren?<br> &nbsp"
            },
            {
                "t11pregunta": "cm"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe el perímetro de la siguiente figura.",
            evaluable:true
        }

    },
     {
        "respuestas":[
           {
              "t13respuesta":     "Un sol",
              "t17correcta":"1",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "3 soles",
              "t17correcta":"0",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "2 soles",
              "t17correcta":"0",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "Sucesión aritmética.",
              "t17correcta":"1",
            "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Sucesión geométrica.",
              "t17correcta":"0",
            "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Sucesión especial.",
              "t17correcta":"0",
            "numeroPregunta":"1"
           },
           {
              "t13respuesta":     "Con sumas o restas.",
              "t17correcta":"1",
            "numeroPregunta":"2"
           },
           {
              "t13respuesta":     "Multiplicaciones o divisiones.",
              "t17correcta":"0",
            "numeroPregunta":"2"
           },
           {
              "t13respuesta":     "Combinando sumas y multiplicaciones.",
              "t17correcta":"0",
            "numeroPregunta":"2"
           }
        ],
        "pregunta":{
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Observa la sucesión y selecciona la respuesta correcta.<br><br><center><img src='MI6E_B05_A03_01.png' weigth = '100' height = '100'></center><br><br>¿Cuántos soles se aumentan en cada término de la sucesión?",
                          "¿Qué tipo de sucesión es?",
                          "¿Cómo se obtiene cada nuevo término de una sucesión aritmética?"],
         "preguntasMultiples": true
        }
      }
];
