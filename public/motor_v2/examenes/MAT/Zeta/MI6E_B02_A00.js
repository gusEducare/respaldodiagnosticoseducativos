json=[
  //1
  {
      "respuestas": [
          {
              "t13respuesta": "MI6E_B02_A01_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_08.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B02_A01_10.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B02_A01_06.png",
              "t17correcta": "4",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_16.png",
              "t17correcta": "5",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_14.png",
              "t17correcta": "6",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_12.png",
              "t17correcta": "7",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_03.png",
              "t17correcta": "8",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_05.png",
              "t17correcta": "9",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_11.png",
              "t17correcta": "10",
              "columna":"0"
          },
          {
              "t13respuesta": "MI6E_B02_A01_17.png",
              "t17correcta": "21",
              "columna":"0"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "<p>Arrastra las fracciones que completen la recta numérica.<br><\/p>",
          "tipo": "ordenar",
          "imagen": true,
          "url":"MI6E_B02_A01_01.png",
          "respuestaImagen":true,
          "tamanyoReal":true,
          "bloques":false,
          // "anchoImagen":"50",
          // altoImagen:900,
          "borde":false

      },
      "contenedores": [
          {"Contenedor": ["", "262,76", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,114", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,153", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,191", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,229", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,268", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,383", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,423", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,498", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,538", "cuadrado", "30, 60", ".","transparent"]},
          {"Contenedor": ["", "262,578", "cuadrado", "30, 60", ".","transparent"]}
      ]
  },
  //2
  {
      "respuestas": [
          {
              "t13respuesta": "<p>5000<\/p>",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "<p>100<\/p>",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "<p>50<\/p>",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "<p>0<\/p>",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "<p>200<\/p>",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "<p>10<\/p>",
              "t17correcta": "6"
          },
          {
              "t13respuesta": "<p>5<\/p>",
              "t17correcta": "7"
          },
          {
              "t13respuesta": "<p>800<\/p>",
              "t17correcta": "8"
          },
          {
              "t13respuesta": "<p>70<\/p>",
              "t17correcta": "9"
          },
          {
              "t13respuesta": "<p>4<\/p>",
              "t17correcta": "10"
          },
          {
              "t13respuesta": "<p>600<\/p>",
              "t17correcta": "11"
          },
          {
              "t13respuesta": "<p>90<\/p>",
              "t17correcta": "12"
          },
          {
              "t13respuesta": "<p>8<\/p>",
              "t17correcta": "13"
          }
      ],
      "preguntas": [
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "<br><style>\n\
                                      .table img{height: 90px !important; width: auto !important; }\n\
                                  </style><table class='table' style='margin-top:-40px;'>\n\
                                  <tr>\n\
                                      <td>Cantidad</td>\n\
                                      <td colspan='8'>Notación de desarrollada</td>\n\
                                  </tr>\n\
                                  <tr>\n\
                                      <td></td>\n\
                                      <td></td>\n\
                                      <td>Unidades de millar</td>\n\
                                      <td></td>\n\
                                      <td>Centenas</td>\n\
                                      <td></td>\n\
                                      <td>Decenas</td>\n\
                                      <td></td>\n\
                                      <td>Unidades</td>\n\
                                  </tr>\n\
                                  <tr>\n\
                                      <td>5150</td>\n\
                                      <td>=</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td>215</td>\n\
                                      <td>=</td>\n\
                                      <td colspan='2' rowspan='3'></td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td>874</td>\n\
                                      <td>=</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td>698</td>\n\
                                      <td>=</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td>+</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                  </tr></table>"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "8",
          "t11pregunta": "Arrastra los valores correctos en notación desarrollada para completar la igualdad de la operación.",
         "contieneDistractores":true,
          "anchoRespuestas": 70,
          "soloTexto": true,
          "respuestasLargas": true,
          "pintaUltimaCaja": false,
          "ocultaPuntoFinal": true
      }
  },
   //3
   {
        "respuestas": [
            {
                "t13respuesta": "<img src='MI6E_B02_A03_07.png'>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<img src='MI6E_B02_A03_08.png'>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<img src='MI6E_B02_A03_09.png'>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<img src='MI6E_B02_A03_10.png'>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<img src='MI6E_B02_A03_12.png'>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='MI6E_B02_A03_01.png'>"
            },
            {
                "t11pregunta": "<img src='MI6E_B02_A03_02.png'>"
            },
            {
                "t11pregunta": "<img src='MI6E_B02_A03_03.png'>"
            },
            {
                "t11pregunta": "<img src='MI6E_B02_A03_04.png'><img src='MI6E_B02_A03_05.png'>"
            },
            {
                "t11pregunta": "<img src='MI6E_B02_A03_06.png'>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona el prisma o pirámide con la figura que representa su base."
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Precio con descuento ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Precio con descuento ",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Precio con descuento ",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Blusa",
                "valores": ['$245', '$305', '$455'],
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Jeans",
                "valores": ['$345', '$126', '$234'],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Bolsa",
                "valores": ['$600', '$780', '$420'],
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Gafas",
                "valores": ['$175', '$350', '$232'],
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Lee el problema y subraya la opción que corresponda a los  precios con descuento de cada artículo o prenda. <br><br>La boutique “Always fashion” tiene rebajas de 65% en toda la tienda. Por ello, Karina fue a comprar varias prendas y artículos:<br><br><ul><li>Blusa $700</li><li>Pantalón de mezclilla $360</li><li>Bolsa $1200</li><li>Gafas $500</li></ul><\/p>",
            "descripcion": "Prendas y Artículos",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }
    },
    //5
    {
      "respuestas":[
         {
            "t13respuesta": "Agua carbonatada, azúcar, aromas naturales y colorantes.",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Agua carbonatada, aromas naturales y colorantes.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "Agua carbonatada, azúcar  y aromas naturales.",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "180 kJ/105 kcal",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "450 kJ/105 kcal",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "350 kJ/105 kcal",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "27 g",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "6.10 g",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "10.6 g",
            "t17correcta": "1",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Analiza la información de la etiqueta y selecciona la respuesta correcta.<br><br><img src='MI6E_B02_A05.png'/><br><br>¿Qué ingredientes usan para hacer la bebida?",
                         "¿Cuál es el valor energético de 250 ml?",
                         "¿Cuántos gramos de hidratos de carbono contienen 100 ml?"],
         "preguntasMultiples": true
      }
   }

];
