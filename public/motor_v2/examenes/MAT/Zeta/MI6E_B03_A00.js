json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<p>500 gr<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>750 gr<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>250 gr<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>125 gr<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>333 gr<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td>Cantidad en fracción. </td>\n\
                                        <td>Cantidad en gramos.</td>\n\
                                    </tr><tr>\n\
                                        <td>½ kg  </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>¾  de kg</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>¼  de kg</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>⅛ de kg</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>⅓ de kg</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </table>"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra los elementos y completa la tabla. <br> Arrastra las equivalencias en gramos de las siguientes cantidades.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },


    {
        "respuestas": [
            {
                "t13respuesta": "Múltiplo",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Múltiplo",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Múltiplo",
                "t17correcta": "2"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "35",
                "valores": ['5', '3.5', '8'],
                "correcta"  : "0,1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "81",
                "valores": ['10', '9', '8.1'],
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "144",
                "valores": ['8.1', '11', '12'],
                "correcta"  : "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "72",
                "valores": ['8', '7.3', '2'],
                "correcta"  : "2,0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "36",
                "valores": ['5', '6', '7.6'],
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada  múltiplo de los siguientes números.",
            "descripcion": "Cantidades",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }
   },


        {  
      "respuestas":[
         {  
            "t13respuesta": "(-3,4)",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "(-3,-4)",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "(3,4)",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "(3,-4)",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "Punto C",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Punto E",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Punto D",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Punto B",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "Los puntos A y E",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los puntos A y F",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los puntos F y E",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "Los puntos C y B",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta. <br /> Observa el plano cartesiano y selecciona las opciones correctas a las preguntas. <img src='MI6E_B03_A03.png'> <br /> ¿Cuáles son las coordenadas del punto A? <br />",
         "¿Cuál es el punto que tiene las coordenadas (1,-3)?<br />",  "¿Cuáles son los puntos que tienen valor 4 en eje y?"],
         "preguntasMultiples": true
      }
   },

        

   {
        "respuestas": [
            {
                "t13respuesta": "2.54 cm",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "30.48 cm",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "914 cm",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1609 m",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "463.9 g",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "28.35 g",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "3.78 l",
                "t17correcta": "7"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "1 Pulgada"
            },
            {
                "t11pregunta": "1 Pie"
            },
            {
                "t11pregunta": "1 Yarda"
            },
            {
                "t11pregunta": "1 Milla"
            },
            {
                "t11pregunta": "1 Libra"
            },
            {
                "t11pregunta": "1 Onza"
            },
            {
                "t11pregunta": "1 Galón"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },

    {
        "respuestas": [
            {
                "t17correcta": "891"
            },
            {
                "t17correcta": "144"
            },
            {
                "t17correcta": "747"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>El volumen del prisma cuadrangular es de &nbsp"
            },
            {
                "t11pregunta": "cm<sup>3</sup>. <br> El volumen de la pirámide cuadrangular es de &nbsp"
            },
            {
                "t11pregunta": "cm<sup>3</sup>. <br> La diferencia entre los dos volúmenes es de &nbsp"
            },
            {
                "t11pregunta": "cm<sup>3</sup>."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta":"Escribe las cantidades que contestan correctamente las siguientes preguntas. <br /> <img src='MI6E_B03_A05_01.png'> <br />",
            evaluable:true,
            "pintaUltimaCaja": false
        }
    },


    {  
      "respuestas":[
         {  
            "t13respuesta": "A",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "B",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "A",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "B",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "A",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "B",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta. <br /> ¿Cuál oferta hace que se pague menos por la compra? <br /> A: 7 lápices por $28 <br />B: 5 lápices por $25 <br />",
         "A: 6 dulces a $22 <br /> B: 12 dulces a $42 <br />",  "A: 8 manzanas a $12 <br /> B: 4 manzanas a $7"],
         "preguntasMultiples": true
      }
   },


   {
        "respuestas": [
            {
                "t13respuesta": "<p>media<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>los valores están muy cercanos<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>moda<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>mediana<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<table border='1' align='center'><tr><th scope='col'>Entidad</th><th scope='col'> % de población urbana</th><th scope='col'>Entidad</th><th scope='col'> % de población urbana</th></tr>\n\
             <tr><td>Aguascalientes</td><td align='center'>81</td><td>Morelos</td><td align='center'>84</td></tr>\n\
             <tr><td>Baja California Sur</td><td align='center'>86</td><td>Oaxaca</td><td align='center'>77</td></tr>\n\
             <tr><td>Chihuahua</td><td align='center'>85</td><td>Quintana Roo</td><td align='center'>88</td></tr>\n\
             <tr><td>Coahuila</td><td align='center'>90</td><td>Sonora</td><td align='center'>86</td></tr>\n\
             <tr><td>Colima</td><td align='center'>89</td><td>Tamaulipas</td><td align='center'>88</td></tr>\n\
             <tr><td>Jalisco</td><td align='center'>87</td><td>Tlaxcala</td><td align='center'>80</td></tr>\n\
             <tr><td>México</td><td align='center'>87</td><td>Yucatán</td><td align='center'>84</td></tr>\n\
             </table>\n\
                  \n\
                 <p><br>Para la tabla anterior, la medida de tendencia central más significativa es la  <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;, ya que <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true,
             
            
        }
    },
    
    {
        "respuestas": [
            {
                "t13respuesta": "<p> mediana<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>son muy dispersos<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>Moda<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Media<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>están muy cercanos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>se repiten<\/p>",
                "t17correcta": "6"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<table border='1' align='center' ><tr><th scope='col'>Entidad</th><th scope='col'>Población hablante de lengua indígena</th></tr>\n\
             <tr><td>Campeche</td><td align='center'>120</td>\n\
             <tr><td>Chiapas</td><td align='center'>270</td>\n\
             <tr><td>Durango</td><td align='center'>20</td>\n\
             <tr><td>Guanajuato</td><td align='center'>3</td>\n\
             <tr><td>Hidalgo</td><td align='center'>150</td>\n\
             <tr><td>Michoacán</td><td align='center'>30</td>\n\
             <tr><td>Nuevo León</td><td align='center'>10</td>\n\
             <tr><td>Querétaro</td><td align='center'>10</td>\n\
             </table>\n\
                  \n\
                 <p><br><br>Para la tabla anterior, es más representativa usar la <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;, ya que los datos <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;.<\/p>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }

   







];
