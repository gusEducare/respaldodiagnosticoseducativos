json = [
    //1
   {
        "respuestas": [
            {
                "t13respuesta": "Mayor que (>)",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Menor que (<)",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Igual (=)",
                "t17correcta": "2"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<span class='fraction '><span class='top'>4</span><span class='bottom'>5</span></span>◻ <span class='fraction '><span class='top'>8</span><span class='bottom'>11</span></span>",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<span class='fraction '><span class='top'>9</span><span class='bottom'>5</span></span>◻ <span class='fraction '><span class='top'>11</span><span class='bottom'>10</span></span>",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<span class='fraction '><span class='top'>7</span><span class='bottom'>8</span></span>◻ <span class='fraction '><span class='top'>11</span><span class='bottom'>12</span></span>",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<span class='fraction '><span class='top'>21</span><span class='bottom'>49</span></span>◻ <span class='fraction '><span class='top'>24</span><span class='bottom'>56</span></span>",
                "correcta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa las siguientes comparaciones de fracciones y marca la opción que corresponda al signo que debe ir en el recuadro del centro.<\/p>",
            "descripcion": "Fracciones",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<span class='fraction '><span class='top'>13</span><span class='bottom'>12</span></span>kg",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<span class='fraction '><span class='top'>3</span><span class='bottom'>4</span></span>kg",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<span class='fraction '><span class='top'>7</span><span class='bottom'>4</span></span>kg",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<span class='fraction '><span class='top'>5</span><span class='bottom'>2</span></span>kg",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<span class='fraction '><span class='top'>1</span><span class='bottom'>2</span></span>kg",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<span class='fraction '><span class='top'>11</span><span class='bottom'>12</span></span>kg",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<span class='fraction '><span class='top'>1</span><span class='bottom'>4</span></span>kg",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<span class='fraction '><span class='top'>3</span><span class='bottom'>2</span></span>kg",
                "t17correcta": "0"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Jamón = "
            },
            {
                "t11pregunta": "<br>Queso = "
            },
            {
                "t11pregunta": "<br>Papas = "
            },
            {
                "t11pregunta": "<br>Pan   = "
            },
            {
                "t11pregunta": "<br>Fruta = "
            },
            {
                "t11pregunta": "<br><br><style>\n\
.table img{height: 90px !important; width: auto !important; }\n\
</style><table class='table' style='margin-top:-40px;'>\n\
<tr>\n\
<td style='width:150px'>Alimento</td>\n\
<td style='width:100px'>Alejandra</td>\n\
<td style='width:100px'>Martín</td>\n\
</tr>\n\
<tr>\n\
<td style='width:100px'>Jamón</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>3</span><span class='bottom'>4</span></span>kg</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>1</span><span class='bottom'>3</span></span>kg</td>\n\
</tr>\n\
<tr>\n\
<td style='width:100px'>Queso</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>1</span><span class='bottom'>2</span></span>kg</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>1</span><span class='bottom'>4</span></span>kg</td>\n\
</tr>\n\
<tr>\n\
<td style='width:100px'>Papas</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>1</span><span class='bottom'>4</span></span>kg</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>3</span><span class='bottom'>2</span></span>kg</td>\n\
</tr>\n\
<tr>\n\
<td style='width:100px'>Pan</td>\n\
<td style='width:100px'>1kg</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>3</span><span class='bottom'>2</span></span>kg</td>\n\
</tr>\n\
<tr>\n\
<td style='width:100px'>Fruta</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>3</span><span class='bottom'>10</span></span>kg</td>\n\
<td style='width:100px'><span class='fraction '><span class='top'>1</span><span class='bottom'>5</span></span>kg</td>\n\
</tr>"


            },
        ],

        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Alejandra y Martín llevaron comida para un campamento, en la tabla se muestra la cantidad que cada uno tenía de cada alimento.  Arrastra la cantidad total que tienen de cada uno:",
            "t11instruccion": "",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores": true
        }
    },


    //3
    {
        "respuestas": [
            {
                "t17correcta": "2.45"
            },
            {
                "t17correcta": "2.4"
            },
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "10.8"
            },
            {
                "t17correcta": "15"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "7 botellas de agua, cada una de 0.35 litros dan "
            },
            {
                "t11pregunta": " litros. <br>12 bolsas de arroz, cada una de 0.2 kilogramos dan "
            },
            {
                "t11pregunta": " kilogramos. <br>10 bolsas de frijol, cada una de 0.3 kilogramos dan "
            },
            {
                "t11pregunta": " kilogramos. <br>9 botellas de agua, cada una de 1.2 litros dan "
            },
            {
                "t11pregunta": " litros. <br>25  cajas de galletas, cada una de 0.6 kilogramos dan "
            },
            {
                "t11pregunta": " kilogramos. <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Cintia recolectó donaciones en su escuela para llevar a un orfanato el fin de semana.<br>Escribe la cantidad total que corresponde a cada objeto.",
            evaluable: true
        }
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "1",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "3",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "2",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "0",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "4",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='MI6E_B01_R04-01.png'> "
            },
            {
                "t11pregunta": "<img src='MI6E_B01_R04-02.png'> "
            },
            {
                "t11pregunta": "<img src='MI6E_B01_R04-03.png'> "
            },
            {
                "t11pregunta": "<img src='MI6E_B01_R04-04.png'> "
            },
            {
                "t11pregunta": "<img src='MI6E_B01_R04-05.png'> "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona cada figura con la cantidad de ejes de simetría que posee.",
            "altoImagen": "150"
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "Rombo",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Luna",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Círculo",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Sol",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Corazón",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Triángulo",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Cara",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Luna",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Rombo",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Círculo",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Sol",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Corazón",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Triángulo",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Cara",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Círculo",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Luna",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Rombo",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Sol",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Corazón",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Triángulo",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Cara",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Círculo",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Luna",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Sol",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Corazón",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Triángulo",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Cara",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Círculo",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Luna",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Sol",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Corazón",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Triángulo",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "Cara",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué figura está en la ubicación e5? ",
                "<br><br>¿Qué figura está en la ubicación f2? ",
                "<br><br>¿Qué figura está en la ubicación g4? ",
                "<br><br>¿Qué figuras están en la columna g? ",
                "<br><br>¿Qué figuras están en la fila 7? "

            ],
            "t11instruccion": "<center><img src='MI6E_B01_R-07.png' height='700'><center>",
            "preguntasMultiples": true

        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Vedadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El punto A está a la misma distancia del aeropuerto que del hospital.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El punto B está más cerca de la gasolinera que el punto A de las canchas deportivas.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El punto C está más lejos del supermercado que el punto B del hospital.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El punto D está a la misma distancia de la gasolinera que del restaurante.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El punto B está más cerca de la gasolinera que A del supermercado.",
                "correcta": "0"

            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",

            "t11pregunta": "<p>Observa el mapa e identifica si las siguientes afirmaciones son verdaderas o falsas.<\/p>",
            "t11instruccion": "<img src='MI6E_B01_R10.png' width= 750px height=750px > ",
            "descripcion": "Enunciados",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t17correcta": "625"
            },
            {
                "t17correcta": "7.2"
            },
            {
                "t17correcta": "20"
            },
            {
                "t17correcta": "4500"
            },
            {
                "t17correcta": "4312"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "¿Cuál es el 25% de 2 500? "
            },
            {
                "t11pregunta": " <br>¿Qué porcentaje de 45 es 16? "
            },
            {
                "t11pregunta": " % <br>¿Qué porcentaje es 18 de 90? "
            },
            {
                "t11pregunta": "% <br>¿Cuál es el 120% de 3 750? "
            },
            {
                "t11pregunta": " <br>Un producto de $5 600 tiene un descuento de 23%. ¿Cuánto se paga por el producto? <br>$"
            },
            {
                "t11pregunta": " <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",

            "t11pregunta": "Escribe la cantidad correcta a cada operación.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: false
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Vedadero",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Hay más gente en el grupo B a la que le gusta las películas de terror que en el grupo A",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La cantidad de gente a la que le gusta el género de acción en el grupo B, es el triple que en el grupo A.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el género de fantasía, el grupo B es el triple que en el grupo A.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El género de misterio es mayor en el grupo B que en el grupo A.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El grupo A tiene mayores seguidores en el grupo A que en el grupo B.",
                "correcta": "0"

            }
        ],

        "pregunta": {
            "c03id_tipo_pregunta": "13",

            "t11pregunta": "<p>Se hizo una encuesta con los alumnos de los grupos A y B de sexto de primaria para saber a cuántas personas les gustaban distintos géneros de películas, estos son los resultados obtenidos.<\/p>",
            "descripcion": "",
            "t11instruccion": "<img src='I6E_B01_R08_00.png'>",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    // arrastra corta faltan imagenes

    {
      "respuestas": [
          {
              "t13respuesta": "MI6E_B01_R13_02.png",
              "t17correcta": "0",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B01_R13_03.png",
              "t17correcta": "1",
              "columna":"1"
          },
           {
              "t13respuesta": "MI6E_B01_R13_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B01_R13_05.png",
              "t17correcta": "3",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B01_R13_06.png",
              "t17correcta": "4",
              "columna":"1"
          },/*
          {
              "t13respuesta": "MI6E_B01_R13_07.png",
              "t17correcta": "5",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B01_R13_08.png",
              "t17correcta": "5",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B01_R13_09.png",
              "t17correcta": "5",
              "columna":"1"
          },
          {
              "t13respuesta": "MI6E_B01_R13_10.png",
              "t17correcta": "5",
              "columna":"1"
          }
          ,
          {
              "t13respuesta": "MI6E_B01_R13_11.png",
              "t17correcta": "5",
              "columna":"1"
          }*/
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Resuelve las sumas o restas de fracciones y arrastra la recta numérica con el resultado correcto en decimales  de las operaciones. ",
          "tipo": "ordenar",
          "imagen": true,
          "url":"MI6E_B01_R13_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":true
          
      },
      "contenedores": [
          {"Contenedor": ["", "30,260", "cuadrado", "300, 80", ".","transparent"]},
          {"Contenedor": ["", "125,260", "cuadrado", "300, 80", ".","transparent"]},
          {"Contenedor": ["", "220,260", "cuadrado", "300, 80", ".","transparent"]},
          {"Contenedor": ["", "315,260", "cuadrado", "300, 80", ".","transparent"]},
          {"Contenedor": ["", "410,260", "cuadrado", "300, 80", ".","transparent"]}
      ]
  }
  ,
  { 
 "respuestas":[ 
{ 
 "t13respuesta":"46500", 
 "t17correcta":"1", 
 }, 
{ 
 "t13respuesta":"65000", 
 "t17correcta":"2", 
 }, 
{ 
 "t13respuesta":"250", 
 "t17correcta":"3", 
 }, 
{ 
 "t13respuesta":"140", 
 "t17correcta":"4", 
 }, 
{ 
 "t13respuesta":"255000", 
 "t17correcta":"5", 
 }, 
], 
 "preguntas": [ 
 { 
 "t11pregunta":"Una bolsa tiene 100 paletas de diferentes sabores, ¿cuántas paletas habrá en 465 bolsas?" 
 }, 
{ 
 "t11pregunta":"Ana tiene 65 cajas con 1000 canicas en cada una, ¿cuántas canicas tiene en total?" 
 }, 
{ 
 "t11pregunta":"En una veterinaria hay 10 costales de 25 kg cada uno, ¿cuántos kilos de croquetas hay en total?" 
 }, 
{ 
 "t11pregunta":"Yaba el pastor alemán come 14 kilos de croquetas en una semana, ¿cuántos kilos de croquetas comerá en 10 semanas?" 
 }, 
{ 
 "t11pregunta":"Verónica tiene 100 paquetes con 2550 estampas cada uno. ¿Cuántas estampas tiene en total?" 
 }, 
 ], 
 "pregunta":{ 
 "c03id_tipo_pregunta":"12", 
 "t11instruccion": "", 
 "t11pregunta": "Relaciona  cada problema con su respuesta correcta.",

 "altoImagen":"100px", 
 "anchoImagen":"200px" 
 } 
 } ,
  {
        "respuestas": [
            {
                "t13respuesta": "Tiene 2 bases y su altura se puede medir por cualquier arista",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tiene 11 caras, 18 vértices y 27 aristas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es un prisma eneágonal",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Sus caras tienen forma de rectángulo",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Figura geométrica que tiene base<br> <img src='MI6E_B01_R15_02.png'>",
                "t17correcta": "1",
            },
             {
                "t13respuesta": "Tiene 3 bases y 27 caras.",
                "t17correcta": "0",
            },{
                "t13respuesta": "Tiene 30 vértices y 36 aristas",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Figura geométrica que tiene base<br> <img src='MI6E_B01_R15_03.png'>",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Sus caras tienen forma de cuadrado.",
                "t17correcta": "0",
            },      
        ],
        "pregunta": {
            "c03id_tipo_pregunta":"2",
            "t11pregunta": "Selecciona las características del prisma o pirámide.",
            "t11instruccion": "<center><img src='MI6E_B01_R15_01.png' height='700'><center>",
          
        }
    },



    {
        "respuestas": [
            {
                "t13respuesta": "Tiene 2 bases y su altura es equivalente a la medida de una arista.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tiene 10 caras, 16 vértices y 24 aristas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Sus caras tienen forma de un rectángulo.",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Es un prisma octagonal",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_05.png'>",
                "t17correcta": "1",
            },
             {
                "t13respuesta": "Tiene 2 bases y 16 caras.",
                "t17correcta": "0",
            },{
                "t13respuesta": "Tiene 8 vértices y 16 aristas",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_06.png'>",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Es un prisma hexagonal",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Sus caras tiene forma de cuadrado.",
                "t17correcta": "0",
            },     
        ],
        "pregunta": {
            "c03id_tipo_pregunta":"2",
            "t11pregunta": "Selecciona las características del prisma o pirámide.",
            "t11instruccion": "<center><img src='MI6E_B01_R15_04.png' height='700'><center>",
          
        }
    },





    {
        "respuestas": [
            {
                "t13respuesta": "Tiene una base y un ápice ",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tiene 7 caras, 7 vértices y 12 aristas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es una pirámide hexagonal",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_06.png'>",
                "t17correcta": "1",
            },
             {
                "t13respuesta": "Tiene 6 caras con forma de triángulo y una cara forma de hexágono.",
                "t17correcta": "1",
            },{
                "t13respuesta": "Tiene 2 bases y 7 caras",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Tiene 8 vértices y 13 aristas",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Es un prisma heptagonal",
                "t17correcta": "0",
            }, 
             {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_05.png'>",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Tiene 7 caras con forma de triángulo y una cara forma de heptágono",
                "t17correcta": "0",
            },   
        ],
        "pregunta": {
            "c03id_tipo_pregunta":"2",
            "t11pregunta": "Selecciona las características del prisma o pirámide.",
            "t11instruccion": "<center><img src='MI6E_B01_R15_07.png' height='700'><center>",
          
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Tiene una base y un ápice  ",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tiene 4 caras, 4 vértices y 6 aristas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es una pirámide triangular",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_11.png'>",
                "t17correcta": "1",
            },
             {
                "t13respuesta": "Tiene 4 caras con forma de triángulo",
                "t17correcta": "1",
            },{
                "t13respuesta": "Tiene 2 bases y 5 caras",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Tiene 4 vértices y 7 aristas",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Es una pirámide cuadrangular.",
                "t17correcta": "0",
            }, 
             {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_12.png'>",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Tiene 2 caras con forma de cuadrado.",
                "t17correcta": "0",
            },   
        ],
        "pregunta": {
            "c03id_tipo_pregunta":"2",
            "t11pregunta": "Selecciona las características del prisma o pirámide.",
            "t11instruccion": "<center><img src='MI6E_B01_R15_10.png' height='700'><center>",
          
        }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "Tiene una base y un ápice  ",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Tiene 11 caras, 11 vértices y 20 aristas",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Es una pirámide decagonal",
                "t17correcta": "1",
            }, {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_14.png'>",
                "t17correcta": "1",
            },
             {
                "t13respuesta": "Tiene 10  caras con forma de triángulo",
                "t17correcta": "1",
            },{
                "t13respuesta": "Tiene 2 bases y 2 ápices",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Tiene 12 vértices y  24 aristas",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Es una piramide dodecagonal",
                "t17correcta": "0",
            }, 
             {
                "t13respuesta": "Figura geométrica que tiene base <br> <img src='MI6E_B01_R15_15.png'>",
                "t17correcta": "0",
            }, {
                "t13respuesta": "Tiene 12  caras con forma de triángulo",
                "t17correcta": "0",
            },   
        ],
        "pregunta": {
            "c03id_tipo_pregunta":"2",
            "t11pregunta": "Selecciona las características del prisma o pirámide.",
            "t11instruccion": "<center><img src='MI6E_B01_R15_13.png' height='700'><center>",
          
        }
    },
];
