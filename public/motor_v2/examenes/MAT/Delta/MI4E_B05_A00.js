json=[
  //1
  {
    "respuestas": [
        {
            "t13respuesta": "<p>><\/p>",
            "t17correcta": "1,4"
        },
        {
            "t13respuesta": "<p><<\/p>",
            "t17correcta": "2,5"
        },
        {
            "t13respuesta": "<p>=<\/p>",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "<p>><\/p>",
            "t17correcta": "4,1"
        },
        {
            "t13respuesta": "<p><<\/p>",
            "t17correcta": "5,2"
        }
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<br><style>\n\
                                    .table img{height: 90px !important; width: auto !important; }\n\
                                </style><table class='table' style='margin-top:-40px;'>\n\
                                <tr>\n\
                                    <td style='width:150px'></td>\n\
                                    <td style='width:100px'></td>\n\
                                    <td style='width:100px'></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td style='width:100px'><span class='fraction black'><span class='top'>72</span><span class='bottom'>81</span></span></td>\n\
                              <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    <td style='width:100px'><span class='fraction black'><span class='top'>35</span><span class='bottom'>42</span></span></td>\n\
                                </tr><tr>\n\
                                    <td><span class='fraction black'><span class='top'>16</span><span class='bottom'>28</span></span></td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    <td><span class='fraction black'><span class='top'>48</span><span class='bottom'>56</span></span></td>\n\
                                </tr><tr>\n\
                                    <td><span class='fraction black'><span class='top'>4</span><span class='bottom'>8</span></span></td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    <td><span class='fraction black'><span class='top'>8</span><span class='bottom'>16</span></span></td>\n\
                                </tr><tr>\n\
                                    <td><span class='fraction black'><span class='top'>25</span><span class='bottom'>15</span></span></td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    <td><span class='fraction black'><span class='top'>25</span><span class='bottom'>20</span></span></td>\n\
                                    </tr><tr>\n\
                                    <td><span class='fraction black'><span class='top'>12</span><span class='bottom'>18</span></span></td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    <td><span class='fraction black'><span class='top'>30</span><span class='bottom'>24</span></span></td>\n\
                                </tr><tr>\n\
                                </td>\n\
                                </tr></table>"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "Arrastra la imagen al espacio que corresponda.",
       "contieneDistractores":true,
        "anchoRespuestas": 70,
        "soloTexto": true,
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "ocultaPuntoFinal": true
    }
},
  //2
  {
    "respuestas": [
        {
            "t13respuesta": "MI4E_B05_A02_I02.png",
            "t17correcta": "0",
            "columna":"0"
        },
        {
            "t13respuesta": "MI4E_B05_A02_I03.png",
            "t17correcta": "1",
            "columna":"0"
        },
        {
            "t13respuesta": "MI4E_B05_A02_I04.png",
            "t17correcta": "2",
            "columna":"1"
        },
        {
            "t13respuesta": "MI4E_B05_A02_I05.png",
            "t17correcta": "3",
            "columna":"1"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "5",
        "t11pregunta": "Arrastra los elementos y completa la tabla.",
        "tipo": "ordenar",
        "imagen": true,
        "url":"MI4E_B05_A02_I01.png",
        "respuestaImagen":true, 
        "tamanyoReal":true,
        "bloques":false,
        "borde":false
        
    },
    "contenedores": [
        {"Contenedor": ["", "75,482", "cuadrado", "93, 93", ".","transparent"]},
        {"Contenedor": ["", "178,482", "cuadrado", "93, 93", ".","transparent"]},
        {"Contenedor": ["", "282,482", "cuadrado", "93, 93", ".","transparent"]},
        {"Contenedor": ["", "385,482", "cuadrado", "93, 93", ".","transparent"]}
    ]
},
  //3
  {
    "respuestas": [
        {
            "t17correcta": "10"
        },
        {
            "t17correcta": "100"
        },
        {
            "t17correcta": "1500"
        },
        {
            "t17correcta": "10000"
        },
        {
            "t17correcta": ""
        }
    ],
    "preguntas": [
        {
            "t11pregunta": "<br>5 + 5 = &nbsp"
        },
        {
            "t11pregunta": "<br>25 x 4= &nbsp"
        },
        {
            "t11pregunta": "<br>&nbsp"
        },
        {
            "t11pregunta": "- 500 = 1000<br>20 000 ÷ &nbsp"
        },
        {
            "t11pregunta": "= 2"
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "6",
        "t11pregunta": "Escribe la cantidad correcta a cada operación.",
        evaluable:true,
        //pintaUltimaCaja: false
    }
},
  //4
  {
    "respuestas":[
       {
          "t13respuesta":     "17 cajas",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "19 cajas",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "18 cajas",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "9",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
        "t13respuesta":     "10",
           "t17correcta":"0",
         "numeroPregunta":"0"
      },
       {
          "t13respuesta":     "94 donas",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "124 donas",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "114 donas",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "144 donas",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
        "t13respuesta":     "104 donas",
        "t17correcta":"0",
      "numeroPregunta":"1"
     },
       {
          "t13respuesta":     "9 donas ",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "5 donas",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "7 donas",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "8 donas",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
        "t13respuesta":     "10 donas",
        "t17correcta":"0",
      "numeroPregunta":"2"
     }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Lee el problema y selecciona la respuesta correcta.<br><br>En la panadería “La casa de la  abuela”, se hizo un pedido de 568 donas para el desayuno del día del padre, 300 de chocolate, 154 con relleno de frambuesa y el resto de sabor rompope.<br>A Ana Sofía le tocó empacar las donas en cajas. En una caja caben 32 donas.<br><br>¿Cuántas cajas necesita Ana Sofía para empacar las donas sin que sobre ninguna dona?",
                      "¿Cuántas donas son de sabor rompope?",
                      "¿Cuántas donas faltarían para completar la última caja?"],
     "preguntasMultiples": true
    }
 },
 //5
 {
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Todos los garrafones mostrados tienen la misma capacidad. <img src='MI4E_B05_A05_I01.png'>",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los tres vasos están ocupados solo por la mitad de su capacidad. <img src='MI4E_B05_A05_I02.png'>",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Cada envase mostrado tiene distinta cantidad de leche. <img src='MI4E_B05_A05_I03.png'>",
            "correcta"  : "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
        "descripcion":"Reactivo",
        "evaluable":false,
        "evidencio": false
    }
  },
  //6
  {
    "respuestas":[
       {
          "t13respuesta":     "Vanilla y Chocolate",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Limón y Vainilla",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "Fresa y Chocolate",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "3",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "2",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "4",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "Cajeta",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Fresa",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Limón",
          "t17correcta":"0",
        "numeroPregunta":"2"
       }

    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Lee el texto y selecciona la respuesta correcta.  <br><br>¿Cuáles sabores de helado tienen mayor frecuencia?",
                      "¿Con qué frecuencia se pidió helado de limón el Martes?",
                      "¿Cuál es el helado pedido con menor frecuencia?"],
     "preguntasMultiples": true,
     "t11instruccion": "En la heladería “Corte de Santo Domingo” se llevó un registro de los helados que piden durante dos días. Analiza la siguiente tabla y selecciona la opción correcta a cada pregunta.<br><br><center><img src='MI4E_B05_A06.png'></center>"
    }
 }
]
