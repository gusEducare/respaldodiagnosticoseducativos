json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "800",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "8",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "8000",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "08",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "5000",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "50",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "500",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "5",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "8 x 10",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "8 x 100",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "8 x 1000",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "8 x 1",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "0.004",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "0.04",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "0.4",
                "t17correcta": "0",
                "numeroPregunta": "3"
            }, {
                "t13respuesta": "4",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "2 x 100",
                "t17correcta": "1",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "2 x 1000",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "2 x 10",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "2 x 1",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la opción que representa el valor posicional de la cifra marcada. <b>9<u>8</u>51<b> ",
                "Selecciona la opción que representa el valor posicional de la cifra marcada. <b><u>5</u>756<b> ",
                "Selecciona la opción que representa el valor posicional de la cifra marcada. <b>27<u>8</u>4<b> ",
                "Selecciona la opción que representa el valor posicional de la cifra marcada. <b>698.25<u>4</u>1<b> ",
                "Selecciona la opción que representa el valor posicional de la cifra marcada. <b>10<u>2</u>54<b> ",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },

    //2

    {
        "respuestas": [
            {
                "t13respuesta": "<img src='MI4E_B01_R02-01.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='MI4E_B01_R02-02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='MI4E_B01_R02-03.png'>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='MI4E_B01_R02-04.png'>",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "<img src='MI4E_B01_R02-05.png'>",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Ana dividió la figura en 4 partes  y  coloreó <span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span>:"
            },
            {
                "t11pregunta": "Brenda dividió en 6 partes una figura y coloreó <span class='fraction'><span class='top'>3</span><span class='bottom'>6</span></span>:"
            },
            {
                "t11pregunta": "Arturo dividió en 8 partes una figura y coloreó <span class='fraction'><span class='top'>5</span><span class='bottom'>8</span></span>:"
            },
            {
                "t11pregunta": "Juliana dividió una figura en 10 partes y coloreó <span class='fraction'><span class='top'>7</span><span class='bottom'>10</span></span>:"
            },
            {
                "t11pregunta": "Veronica dividió una figura en 8 partes y coloreó <span class='fraction'><span class='top'>3</span><span class='bottom'>8</span></span>:"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //3
    {
        "respuestas": [
            {
                "t17correcta": "12"
            }, {
                "t17correcta": "17"
            }, {
                "t17correcta": "20"
            }, {
                "t17correcta": "25"
            }, {
                "t17correcta": "30"
            }, {
                "t17correcta": "3"
            }, {
                "t17correcta": "2"
            }, {
                "t17correcta": "3"
            }, {
                "t17correcta": "23"
            }, {
                "t17correcta": "11"
            }
            , {
                "t17correcta": "4"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "Completa la sucesión con los datos faltantes: <br>2, 5, 7, 10,  "
            },
            {
                "t11pregunta": " , 15, "
            },
            {
                "t11pregunta": ","
            }, {
                "t11pregunta": " , 22, "
            }, {
                "t11pregunta": " , 27, "
            }, {
                "t11pregunta": "<br>¿Cuál es la cantidad que se debe sumar al primer término para llegar al segundo en la sucesión anterior? "
            },
            {
                "t11pregunta": "<br>¿Cuál es la cantidad que se debe sumar al segundo término para llegar al tercero en la sucesión?  "
            },
            {
                "t11pregunta": "<br>Para la secuencia 3, 6, 9, ¿qué cantidad se suma, según su regla de sucesión? "
            },
            {
                "t11pregunta": "<br>Completa la sucesión con los datos faltantes: <br> 31, 27,"
            },
            {
                "t11pregunta": " , 19, 15, "
            },
            {
                "t11pregunta": ", 7, 3 <br>¿Que cantidad hay que restar, según la regla de la sucesión anterior?  "
            },
            {
                "t11pregunta": " <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta.",

            evaluable: true,
            pintaUltimaCaja: false,

        }
    },
    //4
    {
        "respuestas": [
            {
                "t17correcta": "4.42"
            }, {
                "t17correcta": "66.77"
            }, {
                "t17correcta": "78.11"
            }, {
                "t17correcta": "134.07"
            },

        ],
        "preguntas": [
            {
                "t11pregunta": "56.81 - "
            },
            {
                "t11pregunta": " = 52.39 <br> 32.43 + 34.34 = "
            }, {
                "t11pregunta": "<br>"
            },
            {
                "t11pregunta": "- 50.28 = 27.83 <br> 65.93 + 68.14 = "
            },
            {
                "t11pregunta": "<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta.",

            evaluable: true,
            pintaUltimaCaja: false,

        }
    },
    //5
    {
        "respuestas": [
            {
                "t17correcta": "3888"
            },
            {
                "t17correcta": "504"
            },
            {
                "t17correcta": "4480"
            },
            {
                "t17correcta": "115"
            },
            {
                "t17correcta": "246"
            },
            {
                "t17correcta": "952"
            },
            {
                "t17correcta": "228"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "324 &#215 12="
            },
            {
                "t11pregunta": " <br>28 &#215 18="
            },
            {
                "t11pregunta": " <br>14 &#215 320="
            },
            {
                "t11pregunta": " <br>23 &#215 5="
            },
            {
                "t11pregunta": " <br>82 &#215 3="
            },
            {
                "t11pregunta": " <br>68 &#215 14="
            },
            {
                "t11pregunta": " <br>19 &#215 12="
            },
            {
                "t11pregunta": " <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": " Escribe la respuesta a las siguientes operaciones.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true,
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "<img style='height: 20px;' src='MI4E_B01_R06-02.png' >",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "<img style='height: 20px;' src='MI4E_B01_R06-03.png' >",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "<img style='height: 20px;' src='MI4E_B01_R06-04.png' >",
                "t17correcta": "2",
                "columna": "0"
            },
            {
                "t13respuesta": "<img style='height: 20px;' src='MI4E_B01_R06-05.png' >",
                "t17correcta": "3",
                "columna": "0"
            },

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra cada elemento donde corresponda.<br><\/p>",
            "t11instruccion": "<br> <center> <img style='height: 600px; display:block; ' src='MI4E_B01_R06-01.png'> </center>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "MI4E_B01_R06_00.png",
            "respuestaImagen": true,
            "tamanyoReal": false,
            "bloques": false,
            "borde": false

        },
        "contenedores": [
            { "Contenedor": ["", "70,425", "cuadrado", "110, 110", ".", "transparent"] },
            { "Contenedor": ["", "182,425", "cuadrado", "110, 110", ".", "transparent"] },
            { "Contenedor": ["", "291,425", "cuadrado", "110, 110", ".", "transparent"] },
            { "Contenedor": ["", "402,425", "cuadrado", "110, 110", ".", "transparent"] },

        ]


    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "<img  src='MI4E_B01_R07-01.png'  >",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img  src='MI4E_B01_R07-02.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img  src='MI4E_B01_R07-03.png'>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img  src='MI4E_B01_R07-04.png'>",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "<img  src='MI4E_B01_R07-05.png'>",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Triángulo equilátero"
            },
            {
                "t11pregunta": "Triángulo acutángulo y escaleno"
            },
            {
                "t11pregunta": "Triángulo rectángulo y escaleno"
            },
            {
                "t11pregunta": "Triángulo isósceles"
            },
            {
                "t11pregunta": "Triángulo obtusángulo"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "tamanyoReal": false,
            "altoImagen": "150px",
            "anchoImagen": "200px"
        }
    },
    //8
    {
        "respuestas": [
            {
                "t17correcta": "12"
            },
            {
                "t17correcta": "12"
            },
            {
                "t17correcta": "24"
            },
            {
                "t17correcta": "60"
            },
            {
                "t17correcta": "11"
            },
            {
                "t17correcta": "10"
            },
            {
                "t17correcta": "15"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Un reloj de manecillas muestra un ciclo de "
            },
            {
                "t11pregunta": " horas. <br>Los relojes digitales pueden formar ciclos de "
            },
            {
                "t11pregunta": " horas con las siglas am y pm o de "
            },
            {
                "t11pregunta": " horas. <br>Una hora tiene "
            },
            {
                "t11pregunta": " minutos. <br>Si un reloj digital presenta las 23:00, eso significa que son las "
            },
            {
                "t11pregunta": " pm. <br>Un reloj muestra las 10:15 am, faltan "
            },
            {
                "t11pregunta": " minutos para que sean las 10:25 am. <br>Un cuarto de hora son "
            },
            {
                "t11pregunta": " minutos.<br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Completa las oraciones correctamente.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true,
        }

    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },

        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La ficha técnica es acerca de un alimento.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si son 35 porciones, entonces hay un total de 35 kilos de producto.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El sodio y las vitaminas de la A a la B12, están medidas en miligramos.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El contenido energético viene expresado en miligramos.",
                "correcta": "0",
            },


        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": " Observa la imagen y elige Falso o Verdadero según corresponda.",
            "t11instruccion": "<br>  <img style='height: 750px' src='MI4E_B01_R09.png'> ",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable": true
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "6",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "35",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "22",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "27",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "7",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "40",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "30",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='MI4E_B01_R10_01.png'> "
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R10_02.png'> "
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R10_03.png'> "
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R10_04.png'> "
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R10_05.png'> "
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R10_06.png'> "
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R10_07.png'> "
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            
            "anchoImagen": "350px"
        }
    },
    //11
    {
        "respuestas": [
            
            {
                "t13respuesta": "MI4E_B01_R11_02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "MI4E_B01_R11_03.png",
                "t17correcta": "1",
                "columna": "1"
            },
            {
                "t13respuesta": "MI4E_B01_R11_04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "MI4E_B01_R11_05.png",
                "t17correcta": "3",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "Escribe la respuesta correcta a cada operación.",
            "tipo": "ordenar",
            "imagen": true,
            "url": "MI4E_B01_R11_01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": true

        },
        "contenedores": [
            { "Contenedor": ["", "42,85", "cuadrado", "200, 200", ".", "transparent"] },
            { "Contenedor": ["", "42,378", "cuadrado", "200, 200", ".", "transparent"] },
            { "Contenedor": ["", "287,86", "cuadrado", "200, 200", ".", "transparent"] },
            { "Contenedor": ["", "287,379", "cuadrado", "200, 200", ".", "transparent"] },
            
        ]
    },
    //12
    {
        "respuestas": [
            {
                "t17correcta": "28.5"
            },
            {
                "t17correcta": "18.10"
            },
            {
                "t17correcta": "14.9"
            },
            {
                "t17correcta": "65.28"
            },
            {
                "t17correcta": "43.30"
            },
            {
                "t17correcta": "20.7"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "12.5+16="
            },
            {
                "t11pregunta": " <br>32.60+"
            },
            {
                "t11pregunta": "=50.7 <br>9.18+"
            },
            {
                "t11pregunta": "=24.08 <br>"
            },
            {
                "t11pregunta": "-18.12=47.16 <br>"
            },
            {
                "t11pregunta": "-18.60=24.7 <br>45.38+"
            },
            {
                "t11pregunta": "=66.08 <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta a cada operación.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //13
    {
        "respuestas": [
            {
                "t13respuesta": "<img src='MI4E_B01_R13_05.png'>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<img src='MI4E_B01_R13_06.png'>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<img src='MI4E_B01_R13_07.png'>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<img src='MI4E_B01_R13_08.png'>",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='MI4E_B01_R13_01.png'>"
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R13_02.png'>"
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R13_03.png'>"
            },
            {
                "t11pregunta": "<img src='MI4E_B01_R13_04.png'>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda para relacionar el cuerpo geométrico con la cara de la base.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    }
];
