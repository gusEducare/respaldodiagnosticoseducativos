json=[
    //Reactivo 1
    {
        "respuestas": [
            {
                "t13respuesta": "Quince",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Diecisiete mil ochocientos noventa y cinco",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Quinientos ochenta y cuatro mil ochocientos veintidós",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Doscientos treinta y uno",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "Doscientos mil",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "15"
            },
            {
                "t11pregunta": "17,895"
            },
            {
                "t11pregunta": "584, 822"
            },
            {
                "t11pregunta": "231"
            },
            {
                "t11pregunta": "200,000"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
//Reactivo 2
     {  
      "respuestas": [  
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_08.png'> <img src='MI4E_B03_A02_09.png'> <img src='MI4E_B03_A02_10.png'><img src='MI4E_B03_A02_11.png'> <img src='MI4E_B03_A02_04.png'> <img src='MI4E_B03_A02_05.png'>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_06.png'> <img src='MI4E_B03_A02_12.png'> <img src='MI4E_B03_A02_07.png'> <img src='MI4E_B03_A02_02.png'> <img src='MI4E_B03_A02_03.png'>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_15.png'>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_06.png'> <img src='MI4E_B03_A02_04.png'> <img src='MI4E_B03_A02_04.png'><img src='MI4E_B03_A02_05.png'>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_05.png'> <img src='MI4E_B03_A02_05.png'> <img src='MI4E_B03_A02_05.png'>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_03.png'> <img src='MI4E_B03_A02_07.png'> <img src='MI4E_B03_A02_03.png'> <img src='MI4E_B03_A02_03.png'>",
            "t17correcta":"0",
            "numeroPregunta":"1"
             
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_14.png'> <img src='MI4E_B03_A02_14.png'> <img src='MI4E_B03_A02_15.png'> <img src='MI4E_B03_A02_09.png'><br><img src='MI4E_B03_A02_06.png'> <img src='MI4E_B03_A02_06.png'> <img src='MI4E_B03_A02_04.png'>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_14.png'> <img src='MI4E_B03_A02_14.png'> <img src='MI4E_B03_A02_14.png'> <img src='MI4E_B03_A02_14.png'> <br> <img src='MI4E_B03_A02_06.png'> <img src='MI4E_B03_A02_06.png'> <img src='MI4E_B03_A02_06.png'> <img src='MI4E_B03_A02_06.png'>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<img src='MI4E_B03_A02_10.png'> <img src='MI4E_B03_A02_10.png'> <img src='MI4E_B03_A02_10.png'> <img src='MI4E_B03_A02_10.png'> <br> <img src='MI4E_B03_A02_10.png'> <img src='MI4E_B03_A02_10.png'> <img src='MI4E_B03_A02_07.png'> <img src='MI4E_B03_A02_07.png'> <br> <img src='MI4E_B03_A02_07.png'> <img src='MI4E_B03_A02_07.png'> <img src='MI4E_B03_A02_07.png'>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
           "t11pregunta":["<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br><br>¿Cuál opción es equivalente a la siguiente cantidad?<div  style='text-align:center;'><img src='MI4E_B03_A02_15.png'> <img src='MI4E_B03_A02_06.png'></div></p>","<p style='margin-bottom: 5px;'><br>¿Cuál opción es equivalente a la siguiente cantidad?<div  style='text-align:center;'><img src='MI4E_B03_A02_13.png'></div></p>", "<p style='margin-bottom: 5px;'>¿Cuál opción es equivalente a la siguiente cantidad?<div  style='text-align:center;'> <img src='MI4E_B03_A02_15.png'> <img src='MI4E_B03_A02_16.png'> <img src='MI4E_B03_A02_08.png'> <img src='MI4E_B03_A02_09.png'> </div></p>"],
          "preguntasMultiples": true
          
      }
   },
//Reactivo 3
    
    {  
      "respuestas": [  
         {  
            "t13respuesta":"<fraction class='black'><span>3</span><span>4</span></fraction>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>5</span><span>6</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>2</span><span>8</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>4</span><span>3</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>24</span><span>72</span></fraction>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>1</span><span>4</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>16</span><span>24</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>18</span><span>48</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>25</span><span>75</span></fraction>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>1</span><span>5</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>10</span><span>35</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>2</span><span>3</span></fraction>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
           "t11pregunta":["<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br>¿Cuál de las siguientes fracciones es equivalente a <fraction class='black'><span>24</span><span>32</span></fraction> ? <div  style='text-align:center;'> </div></p>", "<p style='margin-bottom: 5px;'>¿Cuál de las siguientes fracciones es equivalente a <fraction class='black'><span>4</span><span>12</span></fraction> ? <div  style='text-align:center;'> </div></p>","<p style='margin-bottom: 5px;'>¿Cuál de las siguientes fracciones es equivalente a <fraction class='black'> <span>5</span><span>15</span></fraction> ? <div  style='text-align:center;'> </div></p>"],
         "preguntasMultiples": true,
         
      }
   },
    
//Reactivo 4
    {  
      "respuestas": [  
         {  
            "t13respuesta":"Brenda",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Juan",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"María",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>4</span><span>12</span></fraction>",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>12</span><span>5</span></fraction>",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>3</span><span>5</span></fraction>",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"<fraction class='black'><span>5</span><span>3</span></fraction>",
            "t17correcta":"0",
             "numeroPregunta":"1"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
           "t11pregunta":["<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br> <br> Doña Luisa es la abuela de María, Brenda y Juan. El domingo irán a visitarla y ella les dará una pequeña sorpresa, una caja de galletas, la cual contiene doce paquetes de cinco galletas cada uno.<br> Doña Luisa les dio cuatro paquetes a cada uno de sus nietos. Al pasar una hora, a María le quedaban dos paquetes llenos, Brenda tenía solamente un  paquete a la mitad y Juan tenía aún tres paquetes completos. <br> <div  style='text-align:left;'> <img src='MI4E_B03_A04.png'> <br> ¿Cuál de los nietos tiene menos galletas? </div></p>","<p style='margin-bottom: 5px;'>¿Cuál es la fracción que representa el número de paquetes que le tocó a cada nieto? <div  style='text-align:center;'> </div></p>"],
          "preguntasMultiples": true  
      }
        
   }, 
    //Reactivo 5
    
    {  
      "respuestas": [  
         {  
            "t13respuesta":"8",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"3",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"6",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"0",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },

          {  
            "t13respuesta":"1",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"0",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"2",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"4",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"3",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"5",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"7",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"8",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
          {  
            "t13respuesta":"6",
            "t17correcta":"1",
            "numeroPregunta":"3"
         },
          {  
            "t13respuesta":"4",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
          {  
            "t13respuesta":"8",
            "t17correcta":"0",
            "numeroPregunta":"3"
         },
          {  
            "t13respuesta":"5",
            "t17correcta":"0",
            "numeroPregunta":"3"
         }
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
           "t11pregunta":["<p style='margin-bottom: 5px;'>Selecciona la respuesta correcta.<br><br> ¿Cuál es la cifra que debe ir en el lugar señalado para hacer correcta la operación indicada? <br>  12 x 5 _ = 696 <div  style='text-align:center;'> </div></p>","<p style='margin-bottom: 5px;'>¿Cuál es la cifra que debe ir en el lugar señalado para hacer correcta la operación indicada? <br>   _ 8  x 28 = 504 <div  style='text-align:center;'> </div></p>","<p style='margin-bottom: 5px;'>¿Cuál es la cifra que debe ir en el lugar señalado para hacer correcta la operación indicada? <br>   25 X _0 = 750 <div  style='text-align:center;'> </div></p>","<p style='margin-bottom: 5px;'>¿Cuál es la cifra que debe ir en el lugar señalado para hacer correcta la operación indicada? <br>   17 x _ 3 = 1 071 <div  style='text-align:center;'> </div></p>"],
          "preguntasMultiples": true,
         
      }
   },
    // Reactivo 6
    
    {
        "respuestas": [
            {
                "t13respuesta": "<p>cuadrángulos <\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>cuatro lados<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>vértices<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Agudos<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Rectos<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Obtusos<\/p>",
                "t17correcta": "6"
            }, 
            {
                "t13respuesta": "<p>Llanos<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>Completos<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>triángulos<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>cinco lados<\/p>",
                "t17correcta": "10"
            },
            {
                "t13respuesta": "<p>caras<\/p>",
                "t17correcta": "11"
            },
        ],
        
        "preguntas": [
            {
                "t11pregunta": "<p><br>Los cuadriláteros son conocidos también por nombre de <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;o tetrágonos, su principal característica es que tienen <\/p>"
            },
            {
                "t11pregunta": "<p> ,&nbsp;&nbsp;los cuales son líneas o segmentos que se unen en puntos llamados <\/p>"
            },
            {
                "t11pregunta": "<p>. Como sabrás,  tienen cuatro de ellos. <br> Como podrás recordar, tienen cuatro ángulos, los cuales están formados por los lados, a su vez pueden ser interiores  y exteriores,  estos  ángulos, se clasifican según su medida en: <br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;: Los que miden menos de 90°. <br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;: Aquellos que miden exactamente 90°.<br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;: Los que miden entre 90° y 180°. <br> <\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;: Aquellos que miden exactamente 180°.<br><\/p>"
            },
            {
                "t11pregunta": "<p>&nbsp;&nbsp;: Los que miden 360°. <\/p>"
            },
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las palabras que completen el párrafo.",
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    
    //Reactivo 7
    
     {  
      "respuestas": [  
         {  
            "t13respuesta":"Yucatán",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Tabasco",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Campeche",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Oaxaca",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Quintana Roo, con 37.0%",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Tabasco, con 42.7%",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Tamaulipas, con 37.1%",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Baja California Sur, con 43.6%",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Cardiovasculares, cáncer, trastornos musculares e hipertensión.",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Diabetes, Sida, esquizofrenia y depresión.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Depresión, diabetes, leucemia y hepatítis.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"Esquizofrenia, Sida, trastornos óseos y leucemia.",
            "t17correcta":"0",
             "numeroPregunta":"2"
         }
         
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
           "t11pregunta":["<p style='margin-bottom: 5px;'>Analiza la siguiente imagen y selecciona la respuesta correcta.<br><br><div  style='text-align:left;'><img src='MI4E_B03_A07.png'> <br> ¿Cuál es el estado de México que tiene mayor porcentaje de obesidad? </div></p>", "<p style='margin-bottom: 5px;'><div  style='text-align:left;'><br> ¿Cuál es el porcentaje y estado con menor índice de obesidad? </div></p>", "<p style='margin-bottom: 5px;'><div  style='text-align:left;'><br>¿Cuáles son las enfermedades que están asociadas con el problema de la obesidad? </div></p>"],
          "preguntasMultiples": true
          
      }
          }
];