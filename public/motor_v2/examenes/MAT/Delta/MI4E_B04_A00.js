json=[
  //1
  {
    "respuestas":[
       {
          "t13respuesta":     "100",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "10",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "250",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "5",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "1000",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "500",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "500",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "5",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "1000",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "9",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "81",
          "t17correcta":"1",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "54",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "54",
          "t17correcta":"0",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "9",
          "t17correcta":"1",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "81",
          "t17correcta":"0",
        "numeroPregunta":"4"
       }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta.<br>25 es <span class='fraction black'><span class='top'>1</span><span class='bottom'>4</span></span> de:",
                      "50 es <span class='fraction black'><span class='top'>1</span><span class='bottom'>10</span></span> de:",
                      "<span class='fraction black'><span class='top'>1</span><span class='bottom'>10</span></span> de 50 es:",
                      "27 es <span class='fraction black'><span class='top'>1</span><span class='bottom'>3</span></span> de:",
                       "<span class='fraction black'><span class='top'>1</span><span class='bottom'>3</span></span> de 27 es:",],
     "preguntasMultiples": true
    }
 },
 //2
 {
     "respuestas": [
         {
             "t17correcta": "80"
         },
         {
             "t17correcta": "x"
         }
     ],
     "preguntas": [
         {
             "t11pregunta": "Lee el siguiente problema y escribe la respuesta correcta en el espacio correspondiente.<br><br>El año pasado, en el jardín botánico “El charco del ingenio” se observó que la población de la colección botánica de Cactaceae  de la biznaga redonda  correspondía a <span class='fraction black'><span class='top'>1</span><span class='bottom'>2</span></span>  del total de la población de la biznaga “barril de oro”. Si hay 40 ejemplares de biznaga redonda .<br>Por lo tanto, existen "
         },
         {
             "t11pregunta": " ejemplares de biznagas “barril de oro”. "
         }
     ],
     "pregunta": {
         "c03id_tipo_pregunta": "6",
         evaluable:true,
         "pintaUltimaCaja":false
     }
 },
 //3
 {
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0"
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1"
        }
    ],
    "preguntas" : [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<img src='MI4E_B04_A03_01.png'>",
            "correcta"  : "0"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<img src='MI4E_B04_A03_02.png'>",
            "correcta"  : "1"
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<img src='MI4E_B04_A03_03.png'>",
            "correcta"  : "1"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Selecciona Verdadero o Falso según corresponda: ",
        "descripcion":"Aspectos a valorar",
        "evaluable":false,
        "evidencio": false
    }
  },
  //4
  {
    "respuestas":[
       {
          "t13respuesta":     "15+15",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "10+25",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "12+18",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "15+28",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "250+50+150+10+15+25",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "200+100+50+50+20+50+10+5+5+10",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "100+15+50+25+100+50+150",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "10+10+5+5+5+20+20+50+200+100",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "5+2+3+5+10",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "15+10",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "5+10+2+3+2+3",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "10+10+5+5",
          "t17correcta":"0",
        "numeroPregunta":"2"
       }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"2",
       "t11pregunta":["Selecciona todas las operaciones correspondientes a la descomposición aditiva en cada caso. <br><br>30",
                      "500",
                      "25"],
     "preguntasMultiples": true
    }
  },
  //6
  {
      "respuestas": [
          {
              "t17correcta": "6"
          },
          {
              "t17correcta": "5"
          },
          {
            "t17correcta": "30"
            },
          {
            "t17correcta": "5"
          },
          {
            "t17correcta": "4"
          },
          {
            "t17correcta": "20"
          },
          {
            "t17correcta": "x"
          }
    

      ],
      "preguntas": [
          {
              "t11pregunta": "Escribe la respuesta correcta.<br><br>Don fernando vio un anuncio en un espectacular de compra de terrenos de 600 metros cuadrados, para lo que él decidió llamar y agendar una cita para ver el terreno, pero tiene un pequeño problema debe saber las medidas exactas del terreno de los lados  para poder poner su vivero. Si pondrá 3 filas de flores 5 metros, con 5 columnas de 4 metros de distintos tipos de flores y 3 filas de cactus y columnas con las mismas medidas. Sacar cuánto mide cada lado para lo que necesita don Fernando.<br>En total usará "
          },
          {
              "t11pregunta": " filas de "
          },
          {
              "t11pregunta": " metros = "
          },
          {
              "t11pregunta": " metros de ancho<br>Son "
          },
          {
              "t11pregunta": " columnas de "
          },
          {
              "t11pregunta": " metros = "
          },
          {
              "t11pregunta": " metros largo. "
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "6",
          evaluable:true,
          "pintaUltimaCaja":false
      }
  },
  //5
  {  
    "respuestas":[  
       {  
          "t13respuesta":"<img src='MI4E_B04_A05_02.png'>",
          "t17correcta":"1"
       },
       {  
          "t13respuesta":"<img src='MI4E_B04_A05_03.png'>",
          "t17correcta":"0"
       },
       {  
          "t13respuesta":"<img src='MI4E_B04_A05_04.png'>",
          "t17correcta":"0"
       }
    ],
    "pregunta":{  
       "c03id_tipo_pregunta":"1",
       "columnas":2,
       "t11pregunta":"Selecciona la respuesta correcta.<br>¿Cuál opción tiene un área igual a la de la siguiente figura?<br><img src='MI4E_B04_A05_01.png'>"
    }
 }
]
