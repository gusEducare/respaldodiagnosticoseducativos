json=[
  //1
    {
        "respuestas": [
            {
                "t13respuesta": "<p>8 bloques<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>6 bloques<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>5 bloques<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>9 bloques<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>3 bloques<\/p>",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "<p>4 bloques<\/p>",
                "t17correcta": "0"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td>Mecate:</td>\n\
                                        <td>Números de trozos:</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td>Amarillo</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>Blanco</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>Verde</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr></table>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Don Fernando necesita ir a la ferretería a comprar 24 metros de mecate amarillo,18 de mecate blanco y 15 de mecate verde, al llegar, el dueño de la ferretería le dijo que vende el mecate por bloques de tres metros.<br><br> Arrastra la cantidad de bloques que debe pedir don Fernando para completar los metros de cada color.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "<span class='fraction'><span class='top'>5</span><span class='bottom'>9</span></span>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>8</span><span class='bottom'>12</span></span>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>8</span><span class='bottom'>18</span></span>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>2</span><span class='bottom'>3</span></span>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<img src='MI4E_B02_A02_01.png'>"
            },
            {
                "t11pregunta": "<img src='MI4E_B02_A02_02.png'>"
            },
            {
                "t11pregunta": "<img src='MI4E_B02_A02_03.png'>"
            },
            {
                "t11pregunta": "<img src='MI4E_B02_A02_04.png'>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "altoImagen":"150",
            "t11pregunta":"Relaciona con una línea la parte sombreada de cada figura geométrica con la fracción que la representa."
        }
    },
    //3
    {
      "respuestas":[
         {
            "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_01_02.png'>",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_01_03.png'>",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_01_04.png'>",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_01_05.png'>",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_02_02.png'>",
            "t17correcta":"0",
          "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_02_03.png'>",
           "t17correcta":"0",
          "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_02_04.png'>",
           "t17correcta":"1",
          "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_02_05.png'>",
           "t17correcta":"0",
          "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_03_02.png'>",
           "t17correcta":"1",
          "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_03_03.png'>",
           "t17correcta":"0",
          "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_03_04.png'>",
           "t17correcta":"0",
          "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "<img style='height: 210px' src='MI4E_B02_A03_03_05.png'>",
           "t17correcta":"0",
          "numeroPregunta":"2"
        }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["A continuación se muestran casillas con ciertacantidad de monedas. Selecciona en cada caso  la opción que hace que se sumen 10 pesos.<br><br><p style='margin-bottom: 5px;'> <div  style='text-align:center;'><img style='height: 110px' src='MI4E_B02_A03_01_01.png'></div></p>",
         "<p style='margin-bottom: 5px;'> <div  style='text-align:center;'><img style='height: 110px' src='MI4E_B02_A03_02_01.png'></div></p>",
         "<p style='margin-bottom: 5px;'> <div  style='text-align:center;'><img style='height: 110px' src='MI4E_B02_A03_03_01.png'></div></p>"],
         "preguntasMultiples": true
      }
   },
  //4
      {
          "respuestas": [
              {
                  "t13respuesta": "<img src='MI4E_B02_A04_05.png'>",
                  "t17correcta": "1"
              },
              {
                  "t13respuesta": "<img src='MI4E_B02_A04_06.png'>",
                  "t17correcta": "2"
              },
              {
                  "t13respuesta": "<img src='MI4E_B02_A04_04.png'>",
                  "t17correcta": "3"
              }
          ],
          "preguntas": [
              {
                  "t11pregunta": "<img src='MI4E_B02_A04_01.png'>"
              },
              {
                  "t11pregunta": "<img src='MI4E_B02_A04_02.png'>"
              },
              {
                  "t11pregunta": "<img src='MI4E_B02_A04_03.png'>"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "12",
              "altoImagen":"150",
              "t11pregunta":"Relaciona cada figura geométrica con su representación plana."
          }
      },
      //5
        {
          "respuestas":[
             {
                "t13respuesta":     "Recto.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Agudo.",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Obtuso.",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Llano.",
                "t17correcta":"0",
              "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "Agudo.",
               "t17correcta":"0",
              "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "Recto.",
               "t17correcta":"1",
              "numeroPregunta":"1"
            },
            {
               "t13respuesta":     "Llano.",
               "t17correcta":"0",
              "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "Obtuso.",
               "t17correcta":"1",
              "numeroPregunta":"2"
            },
            {
               "t13respuesta":     "Recto.",
               "t17correcta":"0",
              "numeroPregunta":"2"
            }
          ],
          "pregunta":{
             "c03id_tipo_pregunta":"1",
             "t11pregunta":["Selecciona la respuesta correcta.<br><br><p style='margin-bottom: 5px;'>¿Qué tipo de ángulos tiene el siguiente triángulo? <div  style='text-align:center;'><img style='' src='MI4E_B02_A05_01.png'></div></p>",
             "<p style='margin-bottom: 5px;'>¿Qué tipos de ángulos tiene el cuadrado? <div  style='text-align:center;'><img style='' src='MI4E_B02_A05_02.png'></div></p>",
             "<p style='margin-bottom: 5px;'>¿Qué tipo de ángulos tiene el hexágono? <div  style='text-align:center;'><img style='' src='MI4E_B02_A05_03.png'></div></p>"],
             "preguntasMultiples": true
          }
       },
        //7
        {
          "respuestas": [
              {
                  "t13respuesta": "<p>Ciudad de México<\/p>",
                  "t17correcta": "0",
                  etiqueta:"1°"
              },
              {
                  "t13respuesta": "<p>Tlaxcala<\/p>",
                  "t17correcta": "1",
                  etiqueta:"2°"
              },
              {
                  "t13respuesta": "<p>Morelos<\/p>",
                  "t17correcta": "2",
                  etiqueta:"3°"
              },
              {
                  "t13respuesta": "<p>Aguascalientes<\/p>",
                  "t17correcta": "3",
                  etiqueta:"4°"
              },
              {
                  "t13respuesta": "<p>Colima<\/p>",
                  "t17correcta": "4",
                  etiqueta:"5°"
              },
              {
                  "t13respuesta": "<p>Querétaro<\/p>",
                  "t17correcta": "5",
                  etiqueta:"6°"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "9",
              "t11pregunta": "<p>Observa la siguiente imagen y ordena de <span style='color:red;'>menor a mayor</span> los seis estados más pequeños en superficie de México.<br><img src='MI4E_B02_A07_01.png' style='width: 94%;'><\/p>",
          }
      },
      //6
      {
        "respuestas":[
           {
              "t13respuesta":     "60°",
              "t17correcta":"1",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "90°",
              "t17correcta":"0",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "120º",
              "t17correcta":"0",
            "numeroPregunta":"0"
           },
           {
              "t13respuesta":     "110°",
              "t17correcta":"1",
            "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "90º",
             "t17correcta":"0",
            "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "70º",
             "t17correcta":"0",
            "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "150°",
             "t17correcta":"1",
            "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "90°",
             "t17correcta":"0",
            "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "30º",
             "t17correcta":"0",
            "numeroPregunta":"2"
          }
        ],
        "pregunta":{
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Selecciona la respuesta correcta.<br><br><p style='margin-bottom: 5px;'>¿Cuántos grados mide el ángulo del triángulo equilátero? <div  style='text-align:center;'><img style='' src='MI4E_B02_A06_01.png'></div></p>",
           "<p style='margin-bottom: 5px;'>¿Cuántos grados mide el ángulo del rombo? <div  style='text-align:center;'><img style='' src='MI4E_B02_A06_02.png'></div></p>",
           "<p style='margin-bottom: 5px;'>¿Cuántos grados mide el ángulo del dodecágono? <div  style='text-align:center;'><img style='' src='MI4E_B02_A06_03.png'></div></p>"],
           "preguntasMultiples": true
        }
     }
]
