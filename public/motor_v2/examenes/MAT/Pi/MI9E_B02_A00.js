json=[
    //1
    {
        "respuestas": [
            {
                "t17correcta": "6"
            },
            {
                "t17correcta": "-1"
            },
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "-4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>¿Cuál es la solución positiva a la siguiente ecuación?<br>x<sup>2</sup>-5x - 6 = 0<br>x = &nbsp"
            },
            {
                "t11pregunta": "<br>¿Cuál es la solución negativa a la siguiente ecuación?<br>x<sup>2</sup>-5x - 6 = 0<br>x = &nbsp"
            },
            {
                "t11pregunta": "<br>¿Cuál es la solución positiva a la siguiente ecuación?<br>6x<sup>2</sup>+6x - 72 = 0<br>x = &nbsp"
            },
            {
                "t11pregunta": "<br>¿Cuál es la solución negativa a la siguiente ecuación?<br>6x<sup>2</sup>+6x - 72 = 0<br>x = &nbsp"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            evaluable:true,
            "t11pregunta":"Escribe la respuesta correcta a las siguientes ecuaciones."
        }
    },
    //2
    {
       "respuestas":[
          {
             "t13respuesta":     "<img style='height: 180px' src='MI9E_B02_A02_01.png'>",
             "t17correcta":"0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "<img style='height: 180px' src='MI9E_B02_A02_02.png'>",
             "t17correcta":"0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "<img style='height: 180px' src='MI9E_B02_A02_03.png'>",
             "t17correcta":"1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "Rotación",
             "t17correcta":"0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "Traslación",
             "t17correcta":"1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "Reflexión",
             "t17correcta":"0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "<img style='height: 180px' src='MI9E_B02_A02_05.png'>",
             "t17correcta":"1",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "<img style='height: 180px' src='MI9E_B02_A02_06.png'>",
             "t17correcta":"0",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "<img style='height: 180px' src='MI9E_B02_A02_08.png'>",
             "t17correcta":"0",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "<img style='height: 180px' src='MI9E_B02_A02_09.png'>",
             "t17correcta":"0",
             "numeroPregunta":"2"
          }
 		  ],
       "pregunta":{
          "c03id_tipo_pregunta":"1",
          "dosColumnas": true,
          "t11pregunta":["<p style='margin-bottom: 5px;'>Selecciona la opción correcta a cada pregunta.<br>- ¿Cuál de las imágenes muestran una transformación de rotación?</p>",
          "<p style='margin-bottom: 5px;'>¿Qué tipo de transformación muestra la imagen?<br><img style='height: 180px' src='MI9E_B02_A02_04.png'></p>",
          "<p style='margin-bottom: 5px;'>¿Cuál de las imágenes no muestra una transformación de reflexión?</p>"],
        "preguntasMultiples": true
       }
    },
    //3
    {
       "respuestas":[
          {
             "t13respuesta":     "Rotación",
             "t17correcta":"0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "Reflexión",
             "t17correcta":"1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "Traslación",
             "t17correcta":"0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "Rotación",
             "t17correcta":"0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "Reflexión",
             "t17correcta":"1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "Traslación",
             "t17correcta":"0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "Rotación",
             "t17correcta":"1",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "Reflexión",
             "t17correcta":"0",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "Traslación",
             "t17correcta":"0",
             "numeroPregunta":"2"
          },
          {
             "t13respuesta":     "Rotación",
             "t17correcta":"0",
             "numeroPregunta":"3"
          },
          {
             "t13respuesta":     "Reflexión",
             "t17correcta":"0",
             "numeroPregunta":"3"
          },
          {
             "t13respuesta":     "Traslación",
             "t17correcta":"1",
             "numeroPregunta":"3"
          }
 		  ],
       "pregunta":{
          "c03id_tipo_pregunta":"1",
          "dosColumnas": true,
          "t11pregunta":["Selecciona el tipo de transformación usada. <br><img src='MI9E_B02_A03_01.png'/>",
          "<img src='MI9E_B02_A03_02.png'/>",
          "<img src='MI9E_B02_A03_03.png'/>",
          "<img src='MI9E_B02_A03_04.png'/>"],
        "preguntasMultiples": true
       }
    },
    //4
    {
       "respuestas":[
          {
             "t13respuesta":     "7 m",
             "t17correcta":"0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "8 m",
             "t17correcta":"1",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "9 m",
             "t17correcta":"0",
             "numeroPregunta":"0"
          },
          {
             "t13respuesta":     "5 m",
             "t17correcta":"1",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "7 m",
             "t17correcta":"0",
             "numeroPregunta":"1"
          },
          {
             "t13respuesta":     "3 m",
             "t17correcta":"0",
             "numeroPregunta":"1"
          }
 		  ],
       "pregunta":{
          "c03id_tipo_pregunta":"1",
          "dosColumnas": true,
          "t11pregunta":["Subraya el tipo de transformación usada.<br><img src='MI9E_B02_A04.png'/><br>Una escalera de 10 m de longitud está apoyada sobre la pared. El pie de la escalera está a 6 m de la pared.<br><img src='MI9A_B02_A04_01.png'/><br>¿Qué altura alcanza la escalera sobre la pared?",
          "Un pájaro está situado en la punta de un poste de 4 metros, ¿cuánto viajará en línea recta si se lanza por una manzana que está a 3 metros de distancia del poste?<br><img src='MI9A_B02_A04_02.png'/>"],
        "preguntasMultiples": true
       }
    },
    //5
    {
      "respuestas":[
         {
            "t13respuesta": "<span class='fraction black'><span class='top'>1</span><span class='bottom'>2</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "<span class='fraction black'><span class='top'>3</span><span class='bottom'>6</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "<span class='fraction black'><span class='top'>1</span><span class='bottom'>3</span></span>",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "<span class='fraction black'><span class='top'>1</span><span class='bottom'>6</span></span>",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "<span class='fraction black'><span class='top'>1</span><span class='bottom'>7</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "<span class='fraction black'><span class='top'>1</span><span class='bottom'>8</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "216",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
		   {
            "t13respuesta": "<span class='fraction black'><span class='top'>1</span><span class='bottom'>216</span></span>",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
		   {
            "t13respuesta": "<span class='fraction black'><span class='top'>217</span><span class='bottom'>5</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
		  {
            "t13respuesta": "0.35",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
		   {
            "t13respuesta": "0.25",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
		   {
            "t13respuesta": "0.05",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la opción correcta a cada pregunta.<br>¿Cuál es la probabilidad de obtener un número mayor a 3 al lanzar un dado?",
                         "Se lanzan dos dados al aire y se anota la suma de los puntos obtenidos.¿Cuál es la probabilidad de que salga el número siete?",
						 "Se lanzan tres dados. ¿Cúal es la probabilidad de que salga seis en los tres dados?",
						 "En una urna existen ocho bolas rojas, cinco amarillas y siete verdes. Se extrae una al azar, ¿Qué probabilidad hay de que sea amarilla?"],
         "preguntasMultiples": true,
      }
   }
];
