json=[  
   {  
      "respuestas": [
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "-1"
            },
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "-1"
            }
        ],
       "preguntas": [
            {
                "t11pregunta": "<br>x<sup>2</sup>-4x +4=0, x= ["
            },
            {
                "t11pregunta": "].<br>x<sup>2</sup>=2+x &nbsp &nbsp El valor de x positiva es ["
            },
            {
                "t11pregunta": "], el valor de x negativa es ["
            },
            {
                "t11pregunta": "].<br>x<sup>2</sup>-2x-3=0 &nbsp &nbsp El valor de x positiva es ["
            },
            {
                "t11pregunta": "], el valor de x negativa es ["
            },
            {
                "t11pregunta": "]."
            }
        ],
      "pregunta":{  
         "c03id_tipo_pregunta":"6", 
         "t11pregunta":"Escribe los valores de x de las siguientes ecuaciones.",
          evaluable:true,
          pintaUltimaCaja:false
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "Semejanza",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Congruencia",
                "t17correcta": "1"
            }
        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dos triángulos tienen dos lados proporcionales. Tres triángulos tienen dos  ángulos que miden 60°.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": " Dos triángulos tienen dos ángulos que miden 60°.",
                "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "<img src='MI9E_B03_A02.png'",
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Dos triángulos tienen tres lados proporcionales.",
                "correcta"  : "1"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda.<\/p>",
            "descripcion": "Instrucción",   
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"3 cm",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"2 cm",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"4 cm",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
          {  
            "t13respuesta":"a=5 cm y b=3 cm",
            "t17correcta":"0",
              "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"a=9 cm y b=7 cm",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
         {  
            "t13respuesta":"Ninguna de las anteriores",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"145.36 cm",
            "t17correcta":"0",
              "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"75.52 cm",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
         {  
            "t13respuesta":"51.75 cm",
            "t17correcta":"1",
             "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
          "t11pregunta":["Selecciona la respuesta correcta. <br><br> Si se sabe  que las rectas r, s y t son paralelas, ¿cuál es la longitud de x? <br> <img src='MI9E_B03_A03.png'>","Sabiendo que el segmento DE es paralelo a la base del triángulo, ¿Cuáles son las medidas de los segmentos a y b ? <br> <img src='MI9E_B03_A04.png'>","¿Cuál es la altura del montón de libros situado sobre el césped? <br> <img src='MI9E_B03_A03_03.png'>"],
          "preguntasMultiples":true
      }
   },
    {
        "respuestas": [
            {
                "t13respuesta": "El valor positivo de X<sub>1</sub> es -7, el valor X<sub>2</sub> es 12",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "El valor de X es 1",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "El valor X<sub>1</sub> es 1, el valor de X<sub>2</sub> es -4",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "El valor de X es <span class='fraction'><span class='top'>5</span><span class='bottom'>3</span></span>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "y=x<sup>2</sup>-5x+84"
            },
            {
                "t11pregunta": "y=x<sup>2</sup>-2x+1"
            },
            {
                "t11pregunta": "y=x<sup>2</sup>+3x-4"
            },
            {
                "t11pregunta": "y=x+8x-15"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según correspondan los valores de X con la representación gráfica y la ecuación. <br><br>"
        }
    },
    {  
      "respuestas":[  
         {  
            "t13respuesta":"Independientes ",
            "t17correcta":"1",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Excluyentes",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
         {  
            "t13respuesta":"Complementarios",
            "t17correcta":"0",
             "numeroPregunta":"0"
         },
           {  
            "t13respuesta":"Excluyentes",
            "t17correcta":"1",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Complementarios",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
          {  
            "t13respuesta":"Dependientes",
            "t17correcta":"0",
             "numeroPregunta":"1"
         },
           {  
            "t13respuesta":"Complementarios",
            "t17correcta":"1",
             "numeroPregunta":"2"
         },
          {  
            "t13respuesta":"Independientes",
            "t17correcta":"0",
             "numeroPregunta":"2"
         },
          {  
            "t13respuesta":"Evento complementario",
            "t17correcta":"0",
             "numeroPregunta":"2"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
          "t11pregunta":["Selecciona la respuesta que indica el tipo de evento que describe la situación. <br><br> Se decide la sede de un partido, el lugar será a quien gane 2 de 3 lanzamientos de una moneda. ¿Qué tipo de eventos son los lanzamientos?","En una bolsa se encuentran las cuatro esferas mostradas. ¿Cómo son los siguientes eventos entre ellos? <br> <br> Sacar una pelota verde con puntos <br> Sacar una pelota con rayas<br> <img src='MI9E_B03_A05_I01.png '","Al tirar un dado, ¿Cómo son los siguientes eventos?: <br><br> Obtener un número par <br>Obtener los números 1, 3 ó 5"],
          "preguntasMultiples":true
      }
   }
];