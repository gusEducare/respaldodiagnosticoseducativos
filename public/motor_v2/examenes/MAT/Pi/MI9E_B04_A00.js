json=[ 
 {                                 //Reactivo 1
        "respuestas": [
            {
                "t13respuesta": "169",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "196",
                "t17correcta": "2"
            },
             {
                "t13respuesta": "225",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "256",
                "t17correcta": "4"
            },
             {
                "t13respuesta": "289",
                "t17correcta": "5"
            },
             {
                "t13respuesta": "25",
                "t17correcta": "6"
            },
             {
                "t13respuesta": "27",
                "t17correcta": "7"
            },
             {
                "t13respuesta": "29",
                "t17correcta": "8"
            },
             {
                "t13respuesta": "31",
                "t17correcta": "9"
            }  
        ],
        "preguntas": [
            {
               "t11pregunta": "<br><style> .table\n\
                img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-20px;'>\n\
                                      <tr><td>Sucesión</td>\n\
                                          <td>144</td><td>"
            },
            {
                "t11pregunta": "</td><td>" 
            },
           {
                "t11pregunta": "</td><td>" 
            },
            {
                "t11pregunta": "</td><td>" 
            },
            {
                "t11pregunta": "</td><td>" 
            },
            {
                "t11pregunta": "</td></tr><tr><td>Nivel 1</td><td>" 
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>33</td><td></td></tr>\n\
                                        <tr><td>Nivel 2</td>\n\
                                        <td>2</td><td>2</td>\n\
                                        <td>2</td><td>2</td>\n\
                                        <td>2</td><td></td>\n\
                                        </tr></table>" 
            }  

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "En la siguiente tabla coloca los valores que correspondan a la diferencia entre los elementos consecutivos de la sucesión y arrástralos a la casilla correspondiente del primer nivel.",
            "t11instruccion":"",
            "respuestasLargas": true, 
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    },
    {  // Reactivo 2
        "respuestas": [
            {
                "t17correcta": "2309.076"
            },
            {
                "t17correcta": "2197"
            },
            {
                "t17correcta": "132.952"
            },
            {
                "t17correcta": "680"
            },
            {
                "t17correcta": ""  
            } 
        ],
        "preguntas": [
           {
               "t11pregunta": "<br><style> .table\n\
                img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-20px;'>\n\
                                      <tr><td></td><td>Cilindro</td>\n\
                                          <td>Cubo</td><td>Cilindro</td>\n\
                                          <td>Prisma</td></tr><tr>\n\
                                          <td></td><td>naranja</td>\n\
                                          <td></td><td>rojo</td>\n\
                                          <td></td></tr><tr>\n\
                                          <td>Fórmula</td>\n\
                                          <td><i>V = &pi;r<sup>2</sup>h</i></td>\n\
                                          <td><i>V = a<sup>3</sup></i></td>\n\
                                          <td><i>V = &pi;r<sup>2</sup>h</i></td>\n\
                                          <td><i>V = ABh</i></td>\n\
                                          </tr><tr><td>Volumen: (cm<sup>3</sup>)</td>\n\
                                          <td> &nbsp;"
            },
            {
                "t11pregunta": "</td><td> &nbsp;"
            },
            {
                "t11pregunta": "</td><td> &nbsp;"
            }, 
            {
                "t11pregunta": "</td><td> &nbsp;" 
            },
            {
                "t11pregunta": "</td></tr></table>"
            } 
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta":"Calcula y escribe el volumen de cada figura y el volumen total.<br>Recuerda utilizar =3.1416 y escribir el resultado con la primeras tres cifras decimales sin redondear<br><img src='MI9E_B04_A02_01.png'>",
            evaluable:true,  
             pintaUltimaCaja:true 
        }
    },
    {  // Reactivo 3 
        "respuestas": [
            {
                "t17correcta": "15"
            },
            {
                "t17correcta": "12.594"
            },
            {
                "t17correcta": "12.594"
            },
            {
                "t17correcta": ""
            }   
        ],
        "preguntas": [
           {
               "t11pregunta": "<br><style> .table\n\
                img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-20px;'>\n\
                                      <tr><td rowspan='2'>Tan 50 ° = </td>\n\
                                      <td> &nbsp;" 
            },
            {
                "t11pregunta": " m</td></tr><tr><td>Cateto adyacente</td>\n\
                				</tr><tr><td>Cateto adyacente =</td>\n\
                				<td><span class='fraction black'><span class='top'>15 <i>m</i>\n\
                				</span><span class='bottom'><i>Tan 50°</i></span></span></td>\n\
                				</tr><tr><td>Cateto adyacente =</td>\n\
                				<td><span class='fraction black'><span class='top'>15 <i>m</i> \n\
                				</span><span class='bottom'>1.191</span></span></td>\n\
                				</tr><tr><td>Cateto opuesto=</td><td> &nbsp;"
            }, 
            {
                "t11pregunta": " m</td></tr><tr><td colspan='2'>El auto se encuentra a "
            }, 
            {
                "t11pregunta": " m de distancia del edificio.</td></tr></table>" 
            }   
        ], 
        "pregunta": {
            "c03id_tipo_pregunta": "6", 
            "t11pregunta":"Escribe los términos que hacen faltan para completar el ejercicio.<br><br>Ana estaciona su automóvil fuera de un edificio, al subir al último piso que está a 15 m de altura, ve su auto con una inclinación de 50°, ¿a cuántos metros de distancia del edificio dejó su auto?, y ¿a qué distancia se ve desde el edificio?<br><img src='MI9E_B04_A03.png'>",
            evaluable:true,  
             pintaUltimaCaja:true 
        }
    },
    {  //Reactivo 4 
    "respuestas": [        
         { 
           "t17correcta":  "15" 
         },
         { 
           "t17correcta":  "225" 
         },
         { 
           "t17correcta":  "383.608" 
         },
         { 
           "t17correcta":  "19.585" 
         },
         { 
           "t17correcta":  "19.585" 
         },
         { 
           "t17correcta":"" 
         }
    ],
    "preguntas": [   
      {
      "t11pregunta": "  c<sup>2</sup>=a<sup>2</sup>+b<sup>2</sup> <br>c<sup>2</sup>="
      },
      {
      "t11pregunta": "<sup>2</sup>+12.594<sup>2</sup><br>c<sup>2</sup>= "
      },
      {
      "t11pregunta": " + 158.608 <br>c = &radic;"
      },
      {
      "t11pregunta": " <br>c = "  
      }, 
      {
      "t11pregunta": " m <br>La distancia que hay entre la persona situada arriba hasta el auto es de "
      },
      {
      "t11pregunta": " m."
      },
    ],
    "pregunta": {
      "c03id_tipo_pregunta": "6",
      "t11pregunta": "Escribe los términos que hacen faltan para completar el ejercicio.<br><br>Del problema anterior, ¿cuál es la medida del lado indicado?<br><img src='MI9E_B04_A04.png'>", 
      "t11instruccion":"",  
       evaluable:true,   
       pintaUltimaCaja:true 
    } 
  },
  {   //Reactivo 5
		"respuestas":[ 
		{ 
		"t13respuesta":"m= <span class='fraction black'><span class='top'>y<sub>2</sub>-y<sub>1</sub></span><span class='bottom'>x<sub>2</sub>-x<sub>1</sub></span></span>", 
		"t17correcta":"1",
		"numeroPregunta":"0"   
		}, 
		{ 
		"t13respuesta":"m= <span class='fraction black'><span class='top'>x<sub>2</sub>-x<sub>1</sub></span><span class='bottom'>y<sub>2</sub>-y<sub>1</sub></span></span>", 
		"t17correcta":"0",
		"numeroPregunta":"0"   
		}, 
		{ 
		"t13respuesta":"m=(y<sub>2</sub>-y<sub>1</sub>)(x<sub>2</sub>-x<sub>1</sub>)", 
		"t17correcta":"0", 
		"numeroPregunta":"0"
		},
		{ 
		"t13respuesta":"m= <span class='fraction black'><span class='top'>2</span><span class='bottom'>3</span></span>", 
		"t17correcta":"1", 
		"numeroPregunta":"1"
		}, 
		{ 
		"t13respuesta":"m=-<span class='fraction black'><span class='top'>2</span><span class='bottom'>3</span></span>", 
		"t17correcta":"0", 
		"numeroPregunta":"1"
		}, 
		{ 
		"t13respuesta":"m=-<span class='fraction black'><span class='top'>4</span><span class='bottom'>6</span></span>", 
		"t17correcta":"0",  
		"numeroPregunta":"1"  
		}
		],
		"pregunta":{  
		"c03id_tipo_pregunta":"1", 
		"t11pregunta": ["Selecciona la opción correcta a cada pregunta.<br><br>¿Cuál es la fórmula para calcular la pendiente?","¿Cuál es la pendiente de la siguiente recta?<br><img src='MI9E_B04_A05.png'>"], 
		"preguntasMultiples": true
		} 
	},
	{  //Reactivo 6
		"respuestas":[ 
		{ 
		"t13respuesta":"14 personas", 
		"t17correcta":"1",
		"numeroPregunta":"0" 
		}, 
		{ 
		"t13respuesta":"4 personas", 
		"t17correcta":"0",
		"numeroPregunta":"0" 
		},
		{ 
		"t13respuesta":"6 personas", 
		"t17correcta":"0", 
		"numeroPregunta":"0"
		}, 
		{ 
		"t13respuesta":"5 personas", 
		"t17correcta":"0", 
		"numeroPregunta":"0"
		},
		{ 
		"t13respuesta":"<span class='fraction black'><span class='top'>2(0)+3(1)+4(2)+5(3)</span><span class='bottom'>14</span></span>", 
		"t17correcta":"1", 
		"numeroPregunta":"1"
		}, 
		{ 
		"t13respuesta":"<span class='fraction black'><span class='top'>2+3+4+5</span><span class='bottom'>14</span></span>", 
		"t17correcta":"0", 
		"numeroPregunta":"1"
		}, 
		{ 
		"t13respuesta":"<span class='fraction black'><span class='top'>(2)(3)(4)(5)</span><span class='bottom'>14</span></span>", 
		"t17correcta":"0", 
		"numeroPregunta":"1" 
		}, 
		{ 
		"t13respuesta":"(2)(0)+(3)(1)+(4)(2)+(5)(3)", 
		"t17correcta":"0",
		"numeroPregunta":"1" 
		},
		{ 
		"t13respuesta":"1.857", 
		"t17correcta":"1", 
		"numeroPregunta":"2" 
		}, 
		{ 
		"t13respuesta":"26", 
		"t17correcta":"0", 
		"numeroPregunta":"2"
		}, 
		{ 
		"t13respuesta":"3.5", 
		"t17correcta":"0",
		"numeroPregunta":"2" 
		}, 
		{ 
		"t13respuesta":"4.33", 
		"t17correcta":"0", 
		"numeroPregunta":"2" 
		},
		{ 
		"t13respuesta":"5", 
		"t17correcta":"1",
		"numeroPregunta":"3"  
		}, 
		{ 
		"t13respuesta":"14", 
		"t17correcta":"0",
		"numeroPregunta":"3" 
		}, 
		{ 
		"t13respuesta":"9", 
		"t17correcta":"0",
		"numeroPregunta":"3" 
		}, 
		{ 
		"t13respuesta":"3", 
		"t17correcta":"0", 
		"numeroPregunta":"3" 
		},
		{ 
		"t13respuesta":"0", 
		"t17correcta":"1",
		"numeroPregunta":"4"  
		}, 
		{ 
		"t13respuesta":"1", 
		"t17correcta":"0", 
		"numeroPregunta":"4" 
		}, 
		{ 
		"t13respuesta":"5", 
		"t17correcta":"0", 
		"numeroPregunta":"4" 
		}, 
		{ 
		"t13respuesta":"2", 
		"t17correcta":"0", 
		"numeroPregunta":"4" 
		},
		{ 
		"t13respuesta":"3", 
		"t17correcta":"1",
		"numeroPregunta":"5"  
		}, 
		{ 
		"t13respuesta":"14", 
		"t17correcta":"0", 
		"numeroPregunta":"5" 
		}, 
		{ 
		"t13respuesta":"5", 
		"t17correcta":"0", 
		"numeroPregunta":"5" 
		}, 
		{ 
		"t13respuesta":"26", 
		"t17correcta":"0", 
		"numeroPregunta":"5"  
		}      
		],  
		"pregunta":{ 
		"c03id_tipo_pregunta":"1", 
		"t11pregunta": ["El profesor de Matemáticas le pidió a sus estudiantes que realizaran una encuesta acerca de los hábitos alimenticios para saber si la cantidad de azúcar que consumen las personas excede a la recomendada por día. Daniel sabe que el refresco contiene mucha azúcar, por eso decide preguntarle a sus compañeros qué tanto lo consumen. La información que obtuvo se presenta en la siguiente tabla:<br><br><center><img src='MI9E_B04_A06.png'></center><br>¿A cuántas personas entrevistó Daniel?","¿Con cuál de las siguientes expresiones se calcula el promedio de días que consumen refresco a la semana?","¿Cuál es la media de los datos ?","¿Cuál fue la frecuencia más alta?","¿Cuál fue la frecuencia más baja?","El rango es..."], 
		"preguntasMultiples": true
		}    
	}           
]; 