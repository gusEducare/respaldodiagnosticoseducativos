json=[ 
    {  
        "respuestas":[  
            {
                "t13respuesta":     "x = 3y",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "x = 3 + y",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "y = 3x",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "2x + 2y = 24",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "x + 3y = 24",
                "t17correcta":"",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "x + y = 24",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "9",
                "t17correcta":"1",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "3",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "6",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "3",
                "t17correcta":"1",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "9",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "7",
                "t17correcta":"0",
              "numeroPregunta":"3"
             }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Lee el problema y selecciona la respuesta correcta.<br><br>¿Cuál es la ecuación que representa la expresión:<br>“Su lado mayor mide el triple que su lado menor”?",
            "¿Cuál es la ecuación que representa la expresión: “Un rectángulo tiene su perímetro de 24”?",
            "¿Cuál es el valor de “x”?",
            "¿Cuál es el valor de “y”?"],
           "t11instruccion": "Un rectángulo tiene su perímetro de 24, su lado mayor mide el triple que su lado menor.<br>Si:<br>X = lado mayor<br>Y = lado menor",
           "preguntasMultiples": true
        }
     },
     //2
     {  
        "respuestas":[  
            {
                "t13respuesta":     "La mitad",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "La tercera parte",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "La cuarta parte",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "19.63 cm<sup>2</sup>",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "12.63 cm<sup>2</sup>",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "15.98 cm<sup>2</sup>",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "La mitad",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "La cuarta parte",
                "t17correcta":"1",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "La tercera parte",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Lee el problema, observa la imagen y selecciona o subraya la respuesta correcta.<br><br>¿Cuánto mide el radio del cono menor que se forma al realizar el corte?",
            "¿Cuánto medirá el área de la base del cono menor?",
            "¿Qué fracción representa el área de la base del cono menor con respecto del área de la base del cono mayor?"],
           "t11instruccion": "Un cono tiene su radio de 5 cm y una altura de 5 cm, se corta a la mitad con un plano horizontal, perpendicular a la base.<br><br><center><img src='MI9E_B05_A02_01.png'></center>",
           "preguntasMultiples": true
        }
     },
     //3
     {  
        "respuestas":[  
            {
                "t13respuesta":     "a<sup>3</sup>",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "a<sup>2</sup>",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "a",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>a<sup>3</sup></span><span class='bottom'>3</span></span>",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>a<sup>2</sup></span><span class='bottom'>2</span></span>",
                "t17correcta":"",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "a<sup>3</sup>",
                "t17correcta":"0",
              "numeroPregunta":"1"
             }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Observa la imagen y selecciona la opción correcta para cada pregunta.<br><br><center><img src='MI9E_B05_A03_01.png'></center><br><br>¿Cuál es el volumen del cubo?",
            "¿Cuál es el volumen de la pirámide?"],
           "preguntasMultiples": true
        }
     },
     //4
     {
        "respuestas": [
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": ""
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>Su altura es de &nbsp"
            },
            {
                "t11pregunta": ".03 cm"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta.<br><br><center><img src='MI9E_B05_A04.png' weigth ='200' height ='200'></center><br><br>Un cilindro con un volumen de 160 cm<sup>3</sup> tiene su base con un diámetro de 10 cm.",
            evaluable:true,
           
        }
    },
    //5
    {                               
        "respuestas": [
            {
                "t13respuesta": "273.15",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "373.15",
                "t17correcta": "2"
            },
             {
                "t13respuesta": "523.15",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "773.15",
                "t17correcta": "4"
            },
             {
                "t13respuesta": "1023.15",
                "t17correcta": "5"
            },
             {
                "t13respuesta": "32",
                "t17correcta": "6"
            },
             {
                "t13respuesta": "212",
                "t17correcta": "7"
            },
             {
                "t13respuesta": "482",
                "t17correcta": "8"
            },
             {
                "t13respuesta": "932",
                "t17correcta": "9"
            },
            {
               "t13respuesta": "1382",
               "t17correcta": "10"
           }
        ],
        "preguntas": [
            {
               "t11pregunta": "<br><style> .table\n\
                img{height: 90px !important; width: auto !important; }\n\
                </style><table class='table' style='margin-top:-20px;'>\n\
                                      <tr><td>Celsius</td>\n\
                                          <td>0</td>\n\
                                          <td>100</td>\n\
                                          <td>250</td>\n\
                                          <td>500</td>\n\
                                          <td>750</td></tr><tr><td>Kelvin</td><td>"
            },
            {
                "t11pregunta": "</td><td>" 
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Fahrenheit</td><td>" 
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td><td>"
            },
            {
                "t11pregunta": "</td></tr></table>"
            },
            

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra  las conversiones en Fahrenheit y Kelvin en la siguiente tabla.",
            "t11instruccion":"Recuerda que:<br><br>ºC = 5/9 (ºF - 32)<br>K= ºC + 273.15<br>ºF = 9/5 ºC + 32",
            "respuestasLargas": true, 
            "pintaUltimaCaja": false,
            //"contieneDistractores":true
        }
    },
    //6
    {  
        "respuestas":[  
            {
                "t13respuesta":     "<span class='fraction black'><span class='top'>1</sup></span><span class='bottom'>4</span></span>",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>1</span><span class='bottom'>200</span></span>",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>150</span><span class='bottom'>9</span></span>",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>8</span><span class='bottom'>25</span></span>",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>17</span><span class='bottom'>50</span></span>",
                "t17correcta":"",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>4</span><span class='bottom'>5</span></span>",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>4</span><span class='bottom'>11</span></span>",
                "t17correcta":"1",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>1</span><span class='bottom'>4</span></span>",
                "t17correcta":"",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "<span class='fraction black'><span class='top'>3</span><span class='bottom'>4</span></span>",
                "t17correcta":"0",
              "numeroPregunta":"2"
             }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Selecciona  la respuesta correcta para cada problema.<br><br>En una bolsa se han colocado 30 paletas de fresa, 30 paletas de limón, 30 paletas de mango y 30 paletas de vainilla. Si todas son del mismo tamaño, ¿Cuál es la probabilidad de sacar una paleta de limón al azar?",
            "En la escuela de Fernanda se va a seleccionar al azar un alumno por grupo para que vaya en verano a un intercambio estudiantil en Brasil. En su salón de clases hay 25 alumnos de los cuales ocho no hablan inglés. ¿Cuál es la probabilidad de que el alumno seleccionado no hable inglés?",
            "Jessica está preparando regalos sorpresa para sus compañeros, todos los regalos están envueltos en cajas del mismo tamaño y la misma forma. En total hay cuatro cajas, cada una con una blusa, otras cuatro cajas, cada una con galletas importadas, una caja con zapatos y dos más con libros. Si a Beatriz, la mejor amiga de Jessica, le gustan mucho las galletas, ¿cuál es la probabilidad de que le toquen las galletas?"],
           "preguntasMultiples": true
        }
     },
]; 