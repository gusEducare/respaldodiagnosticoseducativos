
json = [{

    "respuestas": [

        {
            "t13respuesta": "A = (5x) (3x) = 15x<sup>2</sup>",
            "t17correcta": "1",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "A = 5x × 3x = 15x<sup>2</sup>",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },
        {
            "t13respuesta": "A = (5x) (3x) = 15x",
            "t17correcta": "0",
            "numeroPregunta": "0"
        },

        {
            "t13respuesta": "(x + 3)(x)",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "(x + 3)(x - 4)",
            "t17correcta": "1",
            "numeroPregunta": "1"
        },
        {
            "t13respuesta": "(3)(x - 4)",
            "t17correcta": "0",
            "numeroPregunta": "1"
        },

        {
            "t13respuesta": "(x + 3)(x)",
            "t17correcta": "1",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "(x) + 3(x)",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },
        {
            "t13respuesta": "(3x)(x)",
            "t17correcta": "0",
            "numeroPregunta": "2"
        },

    ],
    "pregunta": {
        "c03id_tipo_pregunta": "1",
        "t11pregunta": ["Selecciona la respuesta correcta a cada pregunta.<br><br><center><img src='MI9E_B01_A00_09.png' width='300' height='200'></center><br><br>¿Cuál es la ecuación correspondiente para obtener el área de la figura?", "<center><img src='MI9E_B01_A00_010.png' width='200' height='300'></center><br><br>¿Cuál es la expresión que corresponde al área de B?"
            , "<center><img src='MI9E_B01_A00_011.png' width='200' height='300'></center><br><br>¿Cuál es la expresión del cuadro D?"],
        "t11instruccion": "",
        "preguntasMultiples": true,
        "evaluable": true,
    }


},
// 2
{
    "respuestas": [{
        "t13respuesta": "<img src='MI9E_B01_A00_02.png'>",
        "t17correcta": "1"
    },
    {
        "t13respuesta": "<img src='MI9E_B01_A00_03.png'>",
        "t17correcta": "2"
    },
    {
        "t13respuesta": "<img src='MI9E_B01_A00_04.png'>",
        "t17correcta": "3"
    },
    {
        "t13respuesta": "<img src='MI9E_B01_A00_06.png'>",
        "t17correcta": "4"
    },
    {
        "t13respuesta": "<img src='MI9E_B01_A00_05.png'>",
        "t17correcta": "5"
    },
    {
        "t13respuesta": "<img src='MI9E_B01_A00_07.png'>",
        "t17correcta": "6"
    },
    {
        "t13respuesta": "<img src='MI9E_B01_A00_08.png'>",
        "t17correcta": "7"
    }
    ],
    "preguntas": [{
        "t11pregunta": "<img src='MI9E_B01_A00_02_Im1.png'>"
    },
    {
        "t11pregunta": "<img src='MI9E_B01_A00_03_Im1.png'>"
    },
    {
        "t11pregunta": "<img src='MI9E_B01_A00_04_Im1.png'>"
    },
    {
        "t11pregunta": "<img src='MI9E_B01_A00_05_Im1.png'>"
    },
    {
        "t11pregunta": "<img src='MI9E_B01_A00_06_Im1.png'>"
    },
    {
        "t11pregunta": "<img src='MI9E_B01_A00_07_Im1.png'>"
    },
    {
        "t11pregunta": "<img src='MI9E_B01_A00_08_Im1.png'>"
    }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "12",
        "t11pregunta": "Relaciona las columnas según corresponda. ",
        "altoImagen": "100",
        "evaluable": true,

    }
},

// 3
{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "La suma de los ángulos internos de un  triángulo, siempre tendrá 180°.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Dos triángulos son congruentes si sus ángulos son iguales y sus lados son distintos. ",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las figuras congruentes pueden o no cambiar de posición.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Dos triángulos son semejantes si dos de sus ángulos internos son congruentes.",
            "correcta": "1",
        },

        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las figuras congruentes pueden estar rotadas, reflejadas, trasladadas o tener una combinación de ambas.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Dos triángulos son congruentes si son triángulos clasificados por sus lados.",
            "correcta": "0",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
        "t11instruccion": "",
        "descripcion": "Enunciado",
        "variante": "editable",
        "anchoColumnaPreguntas": 40,
        "evaluable": true
    }
},
//4
{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "De acuerdo con la gráfica, asistieron más hombres que mujeres en el año 1992.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Hubo mayor audiencia en el año 1995.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El género que asistió más a la conferencia fue el de las mujeres. ",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Asistió más gente en el año 1993 que en el año 1996.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En el año 1993 asistieron 24 hombres.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "En el año 1996 asistieron 85 mujeres.",
            "correcta": "1",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<center>La gráfica que se muestra anuncia la cantidad de personas que asistieron a una pequeña conferencia con el paso de los años.<br><img src='MI9E_B01_A00_01.png' width='600'></center><br>Elige falso (F) o verdadero (V) según corresponda.",
        "t11instruccion": "",
        "descripcion": "Enunciado",
        "variante": "editable",
        "anchoColumnaPreguntas": 60,
        "evaluable": true
    }
},
//numero 5
{
    "respuestas": [
        //tabla 1
        {
            "t17correcta": "-1"
        },
        {
            "t17correcta": "2"
        },
        {
            "t17correcta": "2"
        },
        {
            "t17correcta": "10"
        },

        //tabla 2

    ],
    "preguntas": [
        {
            "t11pregunta": "<style>\n\ .table img{height: 90px !important; width: auto !important; }\n\ </style><br><table class='table' style= 'margin-top:-20px;'>\n\
   <col width='100'>\n\
   <col width='100'>\n\
   <col width='100'>\n\
   <col width='100'>\n\
   <col width='100'>\n\
   <tr>\n\<td>X</td>\n\<td>-2\n\
   </td><td>\n\
   " },
        {
            "t11pregunta": "</td><td>0</td><td>1</td><td> "
        },
        {
            "t11pregunta": "</td></tr><tr>\n\<td>y</td><td>10</td>\n\<td>"
        },
        {
            "t11pregunta": "</td>\n\<td>1</td><td>2</td>\n\<td>"
        },
        {
            "t11pregunta": "</td>\n\ </tr></table>"
        },




    ],
    "pregunta": {
        "t11pregunta": "Escribe los valores correspondientes de la función: y = x<sup>2</sup> + 1",
        "c03id_tipo_pregunta": "6",
        "pintaUltimaCaja": false,
        evaluable: true
    }
},
//numero 6
{
    "respuestas": [
        {
            "t13respuesta": "Complementario",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Excluyente",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lanzar un dado y obtener cara o cruz.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Jurar frente a la corte y decir mentiras.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Obtener un caramelo azul en una máquina de golosinas con dulces de color: verde, azul y rojo.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Ganar un concurso de baile.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lanzar un dado y obtener un número par.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El sexo de un bebé en una persona embarazada.",
            "correcta": "1",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Selecciona los eventos que pueden ser excluyentes o complementarios.",
        "t11instruccion": "",
        "descripcion": "Pregunta",
        "variante": "editable",
        "anchoColumnaPreguntas": 30,
        "evaluable": true
    }
},
//reactivo7
{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "42 personas eligieron el sabor chabacano.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "13 personas eligieron el sabor chicle.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "68 personas en total, eligieron el sabor chabacano y galleta.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "El sabor zarzamora fue el menos elegido.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Una cantidad de 63  personas, votaron por el sabor frambuesa.",
            "correcta": "0",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "24 personas eligieron el sabor limón.",
            "correcta": "1",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "<center>Para conocer qué sabor era más popular, una heladería realizó una encuesta a 200 personas, los resultados fueron los siguientes:<br><img src='MI9E_B01_A00_012.png' width='600'></center><br><br>Elige Falso o Verdadero según corresponda.",
        "t11instruccion": "",
        "descripcion": "Enunciado",
        "variante": "editable",
        "anchoColumnaPreguntas": 50,
        "evaluable": true
    }
},
//8
  { 
        "respuestas": [        
            { 
               "t17correcta":  "7" 
             },
            { 
               "t17correcta":  "12" 
             },
            { 
               "t17correcta":  "4" 
             },
            { 
               "t17correcta":  "x" 
             },
            { 
               "t17correcta":  "55" 
             },
            { 
               "t17correcta":  "x" 
             },
            { 
               "t17correcta":  "25" 
             },
            { 
               "t17correcta":  "30" 
             }, 
        ],
        "preguntas": [ 
          { 
          "t11pregunta": "Un terreno rectangular tiene dos lados de una longitud de x+5 y x−3 .El área del terreno es de 48m<sup>2</sup><br><center><img src='MI9E_B01_R08_01.png'></center><br>¿Cuál es el valor de x? " 
          }, 
         { 
          "t11pregunta": " <br>¿Cuánto mide de ancho?  " 
          }, 
         { 
          "t11pregunta": "m <br>¿Cuánto mide de largo? " 
          }, 
         { 
          "t11pregunta": " <br><br><br><br><br>En un terreno rectangular de 750 m2 se va a colocar una cerca de 110 m.<br><center><img src='MI9E_B01_R08_02.png'></center><br>Ecuación: (" 
          }, 
        { 
          "t11pregunta": ")(" 
          },
        { 
         "t11pregunta": "-" 
         },
         { 
          "t11pregunta": ")= 750 <br>¿Cuáles son las medidas del terreno? " 
          },  
         { 
          "t11pregunta": " m y " 
          },
         { 
          "t11pregunta": " m " 
          }, 
       ],
        "pregunta": { 
          "c03id_tipo_pregunta": "6", 
          "t11pregunta": "Escribe las respuestas correctas a cada pregunta.", 
          "t11instruccion":"", 
           evaluable:true, 
           pintaUltimaCaja:false 
        } 
      }, 
    //9
     {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tipo de transformación es traslación.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los vectores son perpendiculares.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los ángulos en la figura ABCDE  miden lo mismo que en la figura A’B’C’D’E’.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La distancia  entre  los vértices de la figura ABCDE y los vértices   de la figura A’B’C’D’E’ es la misma. ",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa las siguientes figuras, selecciona verdadero o falso según los enunciados.<br><br><center><img src='MI9E_B01_R09_01.png'></center>",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable": true
            }
        },
        //10
        {
            "respuestas": [
                {
                    "t13respuesta": "Falso",
                    "t17correcta": "0",
                },
                {
                    "t13respuesta": "Verdadero",
                    "t17correcta": "1",
                },
            ],
            "preguntas": [
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "El ángulo AFE y el ángulo A’F’E’ miden 84 grados.",
                    "correcta": "0",
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Los ángulos de la figura ABCDEF y los ángulos de la figura A’B’C’D’E’F’ miden 90° y son rectos.",
                    "correcta": "1",
                },
                {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Los vectores son paralelos.",
                    "correcta": "0",
                },
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Observa las siguientes figuras, selecciona verdadero o falso según los enunciados.<br><br><center><img src='MI9E_B01_R09_02.png'></center>",
                "t11instruccion": "",
                "descripcion": "Enunciado",
                "variante": "editable",
                "anchoColumnaPreguntas": 40,
                "evaluable": true
            }
    },
    //11
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El tipo de movimiento es simetría axial  y es un movimiento inverso.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los vértices de la figura F y F’ están a diferente distancia del eje de simetría.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Los lados de las figuras F y F’ son simétricos. ",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa las siguientes figuras, selecciona verdadero o falso según los enunciados.<br><br><center><img src='MI9E_B01_R09_03.png'></center>",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 40,
            "evaluable": true
        }
},
//12
{
    "respuestas": [
        {
            "t13respuesta": "Falso",
            "t17correcta": "0",
        },
        {
            "t13respuesta": "Verdadero",
            "t17correcta": "1",
        },
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Los ángulos de las figuras ABC y A’B’C’ miden lo mismo.",
            "correcta": "1",
        },
        {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Las medidas de  las figuras ABC y A’B’C’ son diferentes. ",
            "correcta": "0",
        },
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "13",
        "t11pregunta": "Observa las siguientes figuras, selecciona verdadero o falso según los enunciados.<br><br><center><img src='MI9E_B01_R09_04.png'></center>",
        "t11instruccion": "",
        "descripcion": "Enunciado",
        "variante": "editable",
        "anchoColumnaPreguntas": 40,
        "evaluable": true
    }
},
//13
{
    "respuestas": [
        {
            "t13respuesta": "MI9E_B01_R10_02.png",
            "t17correcta": "0,3,6",
            "columna":"0"
        },
        {
            "t13respuesta": "MI9E_B01_R10_05.png",
            "t17correcta": "1,4",
            "columna":"0"
        },
        {
            "t13respuesta": "MI9E_B01_R10_07.png",
            "t17correcta": "2,5,7",
            "columna":"1"
        },
        {
            "t13respuesta": "MI9E_B01_R10_03.png",
            "t17correcta": "3,0,6",
            "columna":"1"
        },
        {
            "t13respuesta": "MI9E_B01_R10_06.png",
            "t17correcta": "4,1",
            "columna":"0"
        },
        {
            "t13respuesta": "MI9E_B01_R10_08.png",
            "t17correcta": "5,2,7",
            "columna":"1"
        },
        {
            "t13respuesta": "MI9E_B01_R10_04.png",
            "t17correcta": "6,0,3",
            "columna":"1"
        },
        {
            "t13respuesta": "MI9E_B01_R10_09.png",
            "t17correcta": "7,2,5",
            "columna":"0"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "5",
        "t11pregunta": "<p>Escribe en el recuadro la letra que corresponda si la imagen representa:<br>a)Rotación<br>b)Traslación<br>c)Reflexión<\/p>",
        "tipo": "ordenar",
        "imagen": true,
        "url":"MI9E_B01_R10_01.png",
        "respuestaImagen":true, 
        "tamanyoReal":true,
        "bloques":false,
        "borde":true
        
    },
    "contenedores": [
        {"Contenedor": ["", "54,94", "cuadrado", "150, 149", ".","transparent"]},
        {"Contenedor": ["", "54,246", "cuadrado", "150, 149", ".","transparent"]},
        {"Contenedor": ["", "54,398", "cuadrado", "150, 149", ".","transparent"]},
        {"Contenedor": ["", "206,94", "cuadrado", "150, 149", ".","transparent"]},
        {"Contenedor": ["", "206,246", "cuadrado", "150, 149", ".","transparent"]},
        {"Contenedor": ["", "206,398", "cuadrado", "150, 149", ".","transparent"]},//5
        {"Contenedor": ["", "358,94", "cuadrado", "150, 149", ".","transparent"]},//6
        {"Contenedor": ["", "358,398", "cuadrado", "150, 149", ".","transparent"]}//7
    ]
},
//14
 { 
        "respuestas": [        
            { 
               "t17correcta":  "9.43" 
             },
            { 
               "t17correcta":  "25" 
             },
            { 
               "t17correcta":  "64" 
             },
            { 
               "t17correcta":  "88.92" 
             },
            { 
               "t17correcta":"" 
             } 
        ],
        "preguntas": [ 
          { 
          "t11pregunta": "Utiliza el teorema de Pitágoras para obtener la medida de la hipotenusa. <br>Formula:<center><i>c</i><sup>2</sup> = <i>a</i><sup>2</sup> + <i>b</i><sup>2</sup></center> <br>¿Cuál es la medida de la hipotenusa?  " 
          }, 
         { 
          "t11pregunta": " cm <br>¿Cuál es el área del cuadrado A?  " 
          }, 
         { 
          "t11pregunta": "cm<sup>2</sup> <br>¿Cuál es el área del cuadrado B? " 
          }, 
         { 
          "t11pregunta": " cm<sup>2</sup> <br>¿Cuál es el área del cuadrado C? " 
          }, 
         { 
          "t11pregunta": " cm<sup>2</sup> <br>" 
          }, 
       ],
        "pregunta": { 
          "c03id_tipo_pregunta": "6", 
          "t11pregunta": "Calcula y escribe el valor de la hipotenusa con el Teorema de Pitágoras y las áreas de los cuadrados. Recuerda utilizar 2 números después del punto decimal.<br><center><img src='MI9E_B01_R11.png'></center>", 
          "t11instruccion":"", 
           evaluable:true, 
           pintaUltimaCaja:true 
        } 
      }, 
    //15
     { 
            "respuestas": [        
                { 
                   "t17correcta":  "a" 
                 },
                { 
                   "t17correcta":  "b" 
                 },
                { 
                   "t17correcta":  "a" 
                 },
                { 
                   "t17correcta":  "17.3" 
                 },
                { 
                   "t17correcta":  "7.6" 
                 },
                { 
                   "t17correcta":  "241.53" 
                 },
                { 
                   "t17correcta":  "15.54" 
                 },
                { 
                   "t17correcta":"" 
                 } 
            ],
            "preguntas": [ 
              { 
              "t11pregunta": "Utiliza la fórmula del Teorema de Pitágoras: c<sup>2</sup> =" 
              }, 
             { 
              "t11pregunta": "<sup>2</sup> +" 
              }, 
             { 
              "t11pregunta": "<sup>2</sup> <br>Despeja la fórmula : b =&#8730;c<sup>2</sup> -" 
              }, 
             { 
              "t11pregunta": "<sup>2</sup> <br>Sustituye los valores en la fórmula despejada : <br>b =&#8730;" 
              }, 
             { 
              "t11pregunta": "<sup>2</sup> -" 
              }, 
             { 
              "t11pregunta": "<sup>2</sup> <br>Eleva al cuadrado los valores y restalos, así que obtienes como resultado: <br>b =&#8730;" 
              }, 
             { 
              "t11pregunta": " <br>Saca la raíz cuadrada del resultado obtenido. <br>¿Cuál es la longitud del cateto B? <br>b =" 
              }, 
             { 
              "t11pregunta": " cm <br>" 
              }, 
           ],
            "pregunta": { 
              "c03id_tipo_pregunta": "6", 
              "t11pregunta": "Calcula y escribe la medida del cateto B. Recuerda tomar dos números después del punto decimal.<br><center><img src='MI9E_B01_R12_01.png'></center>", 
              "t11instruccion":"", 
               evaluable:true, 
               pintaUltimaCaja:true 
            } 
          } 
];

