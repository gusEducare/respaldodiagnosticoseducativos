
json=[
     {
    "respuestas": [
        {
          "t17correcta": "<"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": "<"
        },
        {
          "t17correcta": "<"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": "<"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": "<"
        }
    ],
    "preguntas": [
        {
          "t11pregunta": "<style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class='table' style='margin-top:-20px;'>\n\
          <col width='80'>\n\
          <col width='80'>\n\
          <col width='80'>\n\
           <tr>\n\  <td>-2</td>\n\<td>&nbsp"   },
        {
          "t11pregunta": "</td>\n\<td>12</td>\n\</tr><tr>\n\<td>8</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>6</td>\n\</tr><tr>\n\<td>-4</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>-7</td>\n\</tr><tr>\n\<td>-22</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>-3</td>\n\</tr><tr>\n\<td>-14</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>-10</td>\n\</tr><tr>\n\<td>6</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>-1</td>\n\</tr><tr>\n\<td>3</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>-9</td>\n\</tr><tr>\n\<td>-50</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>5</td>\n\</tr><tr>\n\<td>87</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>65</td>\n\</tr><tr>\n\<td>-9</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>-8</td><table>"
        }

    ],
    "pregunta": {
        "t11pregunta": "Responde las siguientes preguntas escribiendo el signo < o > según corresponda.",
        "c03id_tipo_pregunta": "6",
        "t11instruccion":"Compara las cantidades y escribe el signo > o < según corresponda.",
        "pintaUltimaCaja": false,
        evaluable:true
    }
  },
  {
      "respuestas":[
         {
            "t13respuesta":     "El segmento GH representa el radio de la circunferencia.",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "El segmento GH representa el diámetro de la circunferencia.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "El segmento GH representa la tangente de la circunferencia.",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "Infinidad de circunferencias",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Solo una circunferencia",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "Ninguna",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "2.5 cm",
            "t17correcta":"1",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "5 cm",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "5.2 cm",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "15.708 cm",
            "t17correcta":"1",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "7.853 cm",
            "t17correcta":"0",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "31.415 cm",
            "t17correcta":"0",
          "numeroPregunta":"4"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "78.54 cm<sup>2</sup>",
            "t17correcta":"1",
          "numeroPregunta":"5"
         },
         {
            "t13respuesta":     "19.634 cm<sup>2</sup>",
            "t17correcta":"0",
          "numeroPregunta":"5"
         },
         {
            "t13respuesta":     "314.159 cm<sup>2</sup>",
            "t17correcta":"0",
          "numeroPregunta":"5"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "",
         "t11pregunta":["Selecciona la respuesta correcta. <br /><br /><center><img src='MI7E_B04_A02_01.png'><br /><br />","¿Qué relación hay entre el punto G y el punto H?",
                        "¿Cuántas circunferencias se pueden trazar con el mismo radio y que pasen por el punto H?",
                        "¿Cuánto mide el radio de la circunferencia?",
                        "¿Cuánto mide el perímetro de la circunferencia?",
                        "¿Cuál es el área de la circunferencia?"],
       "preguntasMultiples": true
      }
    },
  {
      "respuestas":[
         {
            "t13respuesta":     "50.265 cm",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "25.132 cm",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "804.247 cm",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "201.061 cm",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "A=π x r<sup>2</sup>",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "A=π + D<sup>2</sup>",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "A=R - π<sup>2</sup>",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "A=π x D",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "307.7 cm<sup>2</sup>",
            "t17correcta":"1",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "62.1 cm<sup>2</sup>",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "1231.0 cm<sup>2</sup>",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "307.9 cm<sup>2</sup>",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "12 cm",
            "t17correcta":"1",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "6  cm",
            "t17correcta":"0",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "36 cm",
            "t17correcta":"0",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "24cm",
            "t17correcta":"0",
          "numeroPregunta":"4"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "",
         "t11pregunta":["Selecciona la respuesta correcta. <br /><br /><center><img src='MI7E_B04_A03_01.png'></center><br /><br />","¿Cuál es el perímetro del círculo?",
                        "¿Cuál es la fórmula del área del círculo?",
                        "<br /><br /><center><img src='MI7E_B04_A03_02.png'></center><br /><br />¿Cuál es el  área del círculo?",
                        "¿Cuál es la medida del radio de un círculo con perímetro de 37.68 cm?"],
       "preguntasMultiples": true
      }
    },
   {
      "respuestas": [
          {
              "t13respuesta": "MI7E_B04_A04_02.png",
              "t17correcta": "0",
              "columna":"0"
          },
          {
              "t13respuesta": "MI7E_B04_A04_03.png",
              "t17correcta": "1",
              "columna":"0"
          },
          {
              "t13respuesta": "MI7E_B04_A04_04.png",
              "t17correcta": "2",
              "columna":"1"
          },
          {
              "t13respuesta": "MI7E_B04_A04_05.png",
              "t17correcta": "3",
              "columna":"1"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "5",
          "t11pregunta": "Utiliza el método que sugiere cada caso y calcula el valor que falta en cada relación de proporcionalidad. Arrastra a cada tabla el valor que corresponde.",
          "tipo": "ordenar",
          "imagen": true,
          "url":"MI7E_B04_A04_01.png",
          "respuestaImagen":true, 
          "tamanyoReal":true,
          "bloques":false,
          "borde":false
          
      },
      "contenedores": [
          {"Contenedor": ["0", "341,107", "cuadrado", "70, 50", ".","transparent"]},
          {"Contenedor": ["1", "341,251", "cuadrado", "70, 50", ".","transparent"]},
          {"Contenedor": ["2", "341,395", "cuadrado", "70, 50", ".","transparent"]},
          {"Contenedor": ["3", "341,539", "cuadrado", "70, 50", ".","transparent"]}
      ]
  },
   {
        "respuestas": [
            {
                "t17correcta": "512"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>$&nbsp"
            },
            {
                "t11pregunta": "&nbsp por alumno."
            }

        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11instruccion": "Lee el problema y realiza lo que se te pide. <br /><br />Los estudiantes del colegio contrataron un autobús con la finalidad de realizar un hermoso paseo de fin de cursos. Si viajan un total de 32 estudiantes, para completar el costo del viaje, cada uno de ellos tendrá que abonar la suma de $400, pero al final solamente viajarán 25 ¿cuánto dinero debe aportar cada uno para cubrir el costo del viaje?",
            "t11pregunta":"Escribe la respuesta correcta.",
            "pintaUltimaCaja": false,
            evaluable:true
        }
    },
  {
      "respuestas":[
         {
            "t13respuesta":     "9",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "18",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "3",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "6",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "5",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "2",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         /////////////////////////////////////////
         {
            "t13respuesta":     "18",
            "t17correcta":"1",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "8",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "3",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11instruccion": "El dueño de una heladería quiere comprar un nuevo uniforme para sus empleados, en la tienda de uniformes le dieron las siguientes opciones, pero no sabe cómo combinarlas. <br /><br />Realiza en tu cuaderno las representaciones gráficas que consideres y elige la respuesta correcta a cada pregunta.",
         "t11pregunta":["Selecciona la respuesta correcta.<center><img src='MI7E_B04_A06_01.png'></center><center><img src='MI7E_B04_A06_02.png'></center><center><img src='MI7E_B04_A06_03.png'></center>","¿Cuántas combinaciones de playeras y pantalones se pueden formar?",
                        "Si el dueño descarta el pantalón negro, ¿cuántas combinaciones de pantalones y playeras se pueden hacer?",
                        "¿Cuántas combinaciones se pueden hacer con todas las playeras, pantalones y gorras?"],
       "preguntasMultiples": true
      }
    },
    {
        "respuestas": [
            {
                "t13respuesta": "<p>Sandwiches<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>bebidas<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>21%<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>Ensaladas<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>Sopa<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>Postres<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>18%<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>35%<\/p>",
                "t17correcta": "8"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<p>De acuerdo con la gráfica circular, el producto que tuvo mayor venta fue&nbsp<\/p>"
            },
            {
                "t11pregunta": "<p>, 9% corresponde a&nbsp <\/p>"
            },
            {
                "t11pregunta": "<p>&nbspy la venta de ensaladas representa&nbsp<\/p>"
            },
            {
                "t11pregunta": "<p>&nbspdel total.<\/p> <br><br><center><img src='MI7E_B04_A07_01.png'></center>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11instruccion": "",
            "t11pregunta": "Completa el párrafo con las palabras del recuadro.",
           "altoImagen":450,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "contieneDistractores":true
        }
    }
];