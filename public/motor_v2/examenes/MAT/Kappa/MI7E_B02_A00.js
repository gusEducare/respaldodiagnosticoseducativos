json=[
  //1
  {
      "respuestas": [
          {
              "t17correcta": "50"
          },
          {
              "t17correcta": "0"
          },
          {
              "t17correcta": "254"
          },
          {
              "t17correcta": "1"
          },
          {
              "t17correcta": "234"
          },
          {
              "t17correcta": "4"
          },
          {
              "t17correcta": "41"
          },
          {
              "t17correcta": "0"
          },
          {
              "t17correcta": "285"
          },
          {
              "t17correcta": "1"
          },
          {
              "t17correcta": "x"
          }
      ],
      "preguntas": [
          {

              "t11pregunta": "<br><style>\n\
                                      .table img{height: 90px !important; width: auto !important; }\n\
                                  </style><table class='table' style='margin-top:-40px;'>\n\
                                  <tr>\n\
                                      <td>Dividendo</td>\n\
                                      <td>Divisor</td>\n\
                                      <td>Cociente</td>\n\
                                      <td>Residuo</td>\n\
                                  </tr><tr>\n\
                                      <td>250</td>\n\
                                      <td>5</td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td>763</td>\n\
                                      <td>3</td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td>1876</td>\n\
                                      <td>8</td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td>164</td>\n\
                                      <td>4</td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td>571</td>\n\
                                      <td>2</td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                      <td>"
          },
          {

              "t11pregunta": "        </td>\n\
                                      </tr></table>"
          }
      ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            evaluable:true,
            "t11pregunta":"Escribe el cociente y el residuo de las siguientes divisiones.",
            "pintaUltimaCaja":false
        }
  },
  //2
  {
     "respuestas":[
        {
           "t13respuesta":     "2x2x5",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "2x2x2x2x2x5",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "2x2x2x2x5",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "2x2x2x5x5",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "5x5",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "2x5",
           "t17correcta":"1",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "2x2x2x3x5x7",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "2x2x2x3x5",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "2x5x3x7",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "2x2x2",
           "t17correcta":"1",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":     "2x2",
           "t17correcta":"0",
           "numeroPregunta":"3"
        },
        {
           "t13respuesta":     "2x2x2x2x2x5",
           "t17correcta":"0",
           "numeroPregunta":"3"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11pregunta":["Identifica y selecciona la multiplicación de factores primos que permite obtener el mínimo común múltiplo o el máximo común múltiplo.<br><br>Mínimo común múltiplo de 160 y 80.",
        "Máximo común divisor de 50 y 40.",
        "Mínimo común múltiplo de 240 y 120.",
        "Máximo común divisor de 32 y 40."],
        "preguntasMultiples": true,
        "t11instruccion": ""
     }
  },
  //3
  {
      "respuestas": [
          {
              "t13respuesta": "2.21",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "-0.33",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "1.9",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "-4.3",
              "t17correcta": "4"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "<span class='fraction'><span class='top'>8</span><span class='bottom'>5</span></span> + <span class='fraction'><span class='top'>7</span><span class='bottom'>9</span></span> - <span class='fraction'><span class='top'>1</span><span class='bottom'>6</span></span> = "
          },
          {
              "t11pregunta": "<span class='fraction'><span class='top'>4</span><span class='bottom'>6</span></span> x <span class='fraction'><span class='top'>2</span><span class='bottom'>8</span></span> - <span class='fraction'><span class='top'>1</span><span class='bottom'>2</span></span> = "
          },
          {
              "t11pregunta": "<span class='fraction'><span class='top'>2</span><span class='bottom'>3</span></span> + <span class='fraction'><span class='top'>9</span><span class='bottom'>8</span></span> + <span class='fraction'><span class='top'>1</span><span class='bottom'>9</span></span> = "
          },
          {
              "t11pregunta": "<span class='fraction'><span class='top'>5</span><span class='bottom'>12</span></span> x <span class='fraction'><span class='top'>10</span><span class='bottom'>6</span></span> - 5 = "
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta":"Relaciona las operaciones de fracciones con su resultado en decimal. "
      }
  },
  //4
  {
      "respuestas": [
          {
              "t13respuesta": "<p><span class='fraction black'><span class='top'>4</span><span class='bottom'>6</span></span><\/p>",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "<p><span class='fraction black'><span class='top'>7</span><span class='bottom'>2</span></span><\/p>",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "<p><span class='fraction black'><span class='top'>15</span><span class='bottom'>7</span></span><\/p>",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "<p><span class='fraction black'><span class='top'>50</span><span class='bottom'>3</span></span><\/p>",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "<p>4<\/p>",
              "t17correcta": "5"
          },
          {
              "t13respuesta": "<p>54<\/p>",
              "t17correcta": "6"
          }
      ],
      "preguntas": [
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "<br><style>\n\
                                      .table img{height: 90px !important; width: auto !important; }\n\
                                  </style><table class='table' style='margin-top:-40px;'>\n\
                                  <tr>\n\
                                      <td><span class='fraction black'><span class='top'>4</span><span class='bottom'>5</span></span></td>\n\
                                      <td> x </td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td> = </td>\n\
                                      <td><span class='fraction black'><span class='top'>8</span><span class='bottom'>15</span></span></td>\n\
                                  </tr><tr>\n\
                                      <td><span class='fraction black'><span class='top'>2</span><span class='bottom'>3</span></span></td>\n\
                                      <td> ÷ </td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td> = </td>\n\
                                      <td><span class='fraction black'><span class='top'>4</span><span class='bottom'>21</span></span></td>\n\
                                  </tr><tr>\n\
                                      <td><span class='fraction black'><span class='top'>5</span><span class='bottom'>7</span></span></td>\n\
                                      <td>x</td>\n\
                                      <td><span class='fraction black'><span class='top'>9</span><span class='bottom'>3</span></span></td>\n\
                                      <td> = </td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td><span class='fraction black'><span class='top'>5</span><span class='bottom'>3</span></span></td>\n\
                                      <td> ÷ </td>\n\
                                      <td><span class='fraction black'><span class='top'>1</span><span class='bottom'>10</span></span></td>\n\
                                      <td> = </td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                  </tr><tr>\n\
                                      <td><span class='fraction black'><span class='top'>2</span><span class='bottom'>13</span></span></td>\n\
                                      <td> x </td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                      <td> = </td>\n\
                                      <td><span class='fraction black'><span class='top'>8</span><span class='bottom'>13</span></span></td>\n\
                                  </tr><tr>\n\
                                      <td><span class='fraction black'><span class='top'>81</span><span class='bottom'>9</span></span></td>\n\
                                      <td> ÷ </td>\n\
                                      <td><span class='fraction black'><span class='top'>1</span><span class='bottom'>6</span></span></td>\n\
                                      <td> = </td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        </td>\n\
                                  </tr></table>"
          },


      ],
      "pregunta": {
          "c03id_tipo_pregunta": "8",
          "t11pregunta": "Arrastra los términos que completan las multiplicaciones o divisiones de fracciones, recuerda que los resultados están simplificados.",
          "contieneDistractores":true,
          "anchoRespuestas": 70,
          "soloTexto": true,
          "respuestasLargas": true,
          "pintaUltimaCaja": false,
          "ocultaPuntoFinal": true
      }
  },
  //5
  {
      "respuestas": [
          {
              "t17correcta": "40"
          },
          {
              "t17correcta": "200"
          },
          {
              "t17correcta": "4"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "<br>Perímetro: P = "
          },
          {
              "t11pregunta": "cm<br>Área: A = "
          },
          {
              "t11pregunta": "cm<sup>2</sup>"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "6",
          evaluable:true,
          "t11pregunta": "Calcula el perímetro y área del siguiente polígono regular.<br><img style='height: 260px' src='MI7E_B02_A05.png'>",
          "pintaUltimaCaja": false
      }
  },
  //6
  {
     "respuestas":[
        {
           "t13respuesta":     "18 cm de base y 9.6 cm de altura.",
           "t17correcta":"1",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "4.5 cm de base y 2.4 cm de altura.",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "9.6 cm de base y 18 cm de altura.",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "2.4 cm de base y 4.5 cm de altura.",
           "t17correcta":"0",
           "numeroPregunta":"0"
        },
        {
           "t13respuesta":     "<span class='fraction black'><span class='top'>4</span><span class='bottom'>9</span></span>",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "<span class='fraction black'><span class='top'>3</span><span class='bottom'>4</span></span>",
           "t17correcta":"1",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "<span class='fraction black'><span class='top'>7</span><span class='bottom'>5</span></span>",
           "t17correcta":"0",
           "numeroPregunta":"1"
        },
        {
           "t13respuesta":     "12.5 litros.",
           "t17correcta":"1",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "10.4 litros.",
           "t17correcta":"0",
           "numeroPregunta":"2"
        },
        {
           "t13respuesta":     "11.2 litros.",
           "t17correcta":"0",
           "numeroPregunta":"2"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "dosColumnas": true,
        "t11pregunta":["Selecciona la opción correcta a cada pregunta.<br><br>Se va a realizar un dibujo a escala del siguiente dibujo.<br><img style='height: 200px' src='MI7E_B02_A06.png'><br>¿Cuáles serían las medidas de la base y altura de un dibujo con una escala al doble?",
        "Karen pagó 27 pesos por una bolsa con 36 paletas de mango. El factor constante de proporcionalidad entre el precio y el número de dulces es <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
        "Si cinco litros de pintura rinden para pintar 18 <i>m<sup>2</sup></i> de pared.¿Con cuántos litros de pintura se pueden pintar 45 <i>m<sup>2</sup></i> de pared?"],
        "preguntasMultiples": true,
     }
  }
]
