json = [
    //1
    {
        "respuestas": [
            {
                "t13respuesta": "<span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>4</span><span class='bottom'>3</span></span>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>1</span><span class='bottom'>2</span></span>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>9</span><span class='bottom'>10</span></span>",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>13</span><span class='bottom'>10</span></span>",
                "t17correcta": "5",
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "0.75"
            },
            {
                "t11pregunta": "1.3"
            },
            {
                "t11pregunta": "0.5"
            },
            {
                "t11pregunta": "0.9"
            },
            {
                "t11pregunta": "1.3"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //2
    {
        "respuestas": [
            {
                "t13respuesta": "MI7E_B01_T1_R02-02.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "MI7E_B01_T1_R02-03.png",
                "t17correcta": "1",
                "columna": "0"
            },
            {
                "t13respuesta": "MI7E_B01_T1_R02-04.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "MI7E_B01_T1_R02-05.png",
                "t17correcta": "3",
                "columna": "1"
            },
            {
                "t13respuesta": "MI7E_B01_T1_R02-06.png",
                "t17correcta": "4",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Relaciona cada imagen con la expresión que represente.<\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "MI7E_B01_T1_R02-01.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,



        },
        "contenedores": [
            { "Contenedor": ["", "200,86", "cuadrado", "93, 92", ".", "transparent"] },
            { "Contenedor": ["", "200,180", "cuadrado", "93, 92", ".", "transparent"] },
            { "Contenedor": ["", "200,274", "cuadrado", "93, 92", ".", "transparent"] },
            { "Contenedor": ["", "200,368", "cuadrado", "93, 92", ".", "transparent"] },
            { "Contenedor": ["", "200,462", "cuadrado", "93, 92", ".", "transparent"] }
        ]
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>0.035</p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>0.108</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>0.507</p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
            {
                "t13respuesta": "<p>0.7</p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p>0.84</p>",
                "t17correcta": "4",
                etiqueta: "5"
            },
            {
                "t13respuesta": "<p>1.4</p>",
                "t17correcta": "5",
                etiqueta: "6"
            },
            {
                "t13respuesta": "<p>2</p>",
                "t17correcta": "6",
                etiqueta: "7"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los elementos de menor a mayor como quedarían en una recta numérica.",
            "t11instruccion": "",
        }
    },
    //4

    {
        "respuestas": [
            {
                "t13respuesta": "<p><span class='fraction'><span class='top'>1</span><span class='bottom'>9</span></span></p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p><span class='fraction'><span class='top'>3</span><span class='bottom'>8</span></span></p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p><span class='fraction'><span class='top'>2</span><span class='bottom'>3</span></span></p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
            {
                "t13respuesta": "<p><span class='fraction'><span class='top'>5</span><span class='bottom'>7</span></span></p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p><span class='fraction'><span class='top'>9</span><span class='bottom'>10</span></span></p>",
                "t17correcta": "4",
                etiqueta: "5"
            },
            {
                "t13respuesta": "<p>2</p>",
                "t17correcta": "5",
                etiqueta: "6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los elementos de menor a mayor como quedarían en una recta numérica.",
            "t11instruccion": "",
        }
    },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "24",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "36",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "80",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "16",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "29",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "31",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "71",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "20",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "30",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "85",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "15",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "29",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "31",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "71",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "24",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "36",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "81",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "18",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "29",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "31",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "71",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "2",
            "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br><br>Selecciona TODOS los números que son divisibles entre 2.",
                "<br><br>Selecciona TODOS los números que son divisibles entre 5.",
                "<br><br>Selecciona TODOS los números que son divisibles entre 3."
            ],
            "preguntasMultiples": true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El 9 es un número primo.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Todos los números primos son impares.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El 13 es un número compuesto.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El 30 es divisible entre 2, 3 y 5.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si un número es divisible entre 10, también lo es entre 2 y 5.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si un número es divisible entre 6, también lo es entre 2 y 3.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El 840 es divisible entre 2 y 5 pero no entre 3.",
                "correcta": "0"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t17correcta": "416"
            },
            {
                "t17correcta": "1100"
            },
            {
                "t17correcta": "18"
            },
            {
                "t17correcta": "60"
            },
            {
                "t17correcta": "80"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>El mínimo común múltiplo de 26 y 32 es "
            },
            {
                "t11pregunta": " <br>El mínimo común múltiplo de 44 y 50 es "
            },
            {
                "t11pregunta": " <br>El mínimo común múltiplo de 18 y 6 es "
            },/*
            {
                "t11pregunta": " <br>El mínimo común múltiplo de 20 y 30 es "
            },
            {
                "t11pregunta": " <br>El mínimo común múltiplo de 8, 16 y 20 es "
            },
            {
                "t11pregunta": " <br>"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta. ",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "6"
            },
            {
                "t17correcta": "10"
            },
            {
                "t17correcta": "4"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>El máximo común divisor de 26 y 32 es "
            },
            {
                "t11pregunta": " <br>El máximo común divisor de 44 y 50 es "
            },
            {
                "t11pregunta": " <br>El máximo común divisor de 18 y 6 es "
            },/*
            {
                "t11pregunta": " <br>El máximo común divisor de 20 y 30 es "
            },
            {
                "t11pregunta": " <br>El máximo común divisor de 8, 16 y 20 es "
            },
            {
                "t11pregunta": " <br>"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta. ",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "<p>1<span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span><\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p><p>5<span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span><\/p><\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p><p>1<span class='fraction'><span class='top'>1</span><span class='bottom'>8</span></span><\/p><\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p><p>1<span class='fraction'><span class='top'>9</span><span class='bottom'>10</span></span><\/p><\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p><p>1<span class='fraction'><span class='top'>19</span><span class='bottom'>20</span></span><\/p><\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                                                        .table img{height: 90px !important; width: auto !important; }\n\
                                                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                                                    <tr>\n\
                                                                        <td style='width:150px'>Alimiento</td>\n\
                                                                        <td style='width:100px'>Compró</td>\n\
                                                                        <td style='width:100px'>Tenía en casa</td>\n\
                                                                        <td style='width:100px'>Total</td>\n\
                                                                    </tr>\n\
                                                                    <tr>\n\
                                                                        <td style='width:100px'>Huevo</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>3</span><span class='bottom'>2</span></span>kg</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>1</span><span class='bottom'>4</span></span>kg</td>\n\
                                                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        kg</td>\n\
                                                                    </tr><tr>\n\
                                                                        <td>Leche</td>\n\
                                                                        <td style='width:100px'>2 L</td>\n\
                                                                        <td style='width:100px'>3<span class='fraction black'><span class='top'>1</span><span class='bottom'>4</span></span>L</td>\n\
                                                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        L</td>\n\
                                                                    </tr><tr>\n\
                                                                        <td>Queso</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>3</span><span class='bottom'>4</span></span>kg</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>3</span><span class='bottom'>8</span></span>kg</td>\n\
                                                                        <td>"
            },/*
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        kg</td>\n\
                                                                    </tr><tr>\n\
                                                                        <td>Pan</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>2</span><span class='bottom'>5</span></span>kg</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>3</span><span class='bottom'>2</span></span>kg</td>\n\
                                                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        kg</td>\n\
                                                                    </tr><tr>\n\
                                                                        <td>Pescado</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>7</span><span class='bottom'>4</span></span>kg</td>\n\
                                                                        <td style='width:100px'><span class='fraction black'><span class='top'>1</span><span class='bottom'>5</span></span>kg</td>\n\
                                                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        kg</td>\n\
                                                                        </tr></table>"
            }*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la respuesta correcta.<br><br>Leonardo fue al supermercado a comprar sus alimentos para la semana. Como no se acordaba qué tenía en su casa, compró un poco de todo. Escribe en la tabla la cantidad final en fracción de cada alimento que tiene, expresa los resultados simplificados y en forma mixta.",
            "contieneDistractores": true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": true,
            "ocultaPuntoFinal": true
        }
    },
    //10
    {
        "respuestas": [
            {
                "t13respuesta": "1",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "1.3",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "0.195",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "0.57",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "0.6",
                "t17correcta": "5",
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "0.25 + <span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span>"
            },
            {
                "t11pregunta": "<span class='fraction'><span class='top'>4</span><span class='bottom'>5</span></span> + <span class='fraction'><span class='top'>1</span><span class='bottom'>2</span></span>"
            },
            {
                "t11pregunta": "<span class='fraction'><span class='top'>1</span><span class='bottom'>8</span></span> + 0.07"
            },
            {
                "t11pregunta": "0.32 + 0.25"
            },
            {
                "t11pregunta": "0.02 + 0.4 + 0.18"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //11
    {
        "respuestas": [
            {
                "t17correcta": "1.65"
            },
            {
                "t17correcta": "0.63"
            },
            {
                "t17correcta": "0.5"
            },
            {
                "t17correcta": "9.5"
            },
            {
                "t17correcta": "0.845"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>Un rollo de malla tiene 4<span class='fraction black'><span class='top'>3</span><span class='bottom'>4</span></span> metros, se utilizarán para colocarlo sobre una superficie de 3.1 metros. ¿Cuántos metros de malla sobrarán? "
            },
            {
                "t11pregunta": " metros. <br><br>Se vierten <span class='fraction black'><span class='top'>3</span><span class='bottom'>8</span></span> de litro en un recipiente que ya contenía 0.255 litros. ¿Cuál es la cantidad total de litros? "
            },
            {
                "t11pregunta": " litros. <br><br>Ayer compré un cuarto de kilogramo de queso, pero cuando llegué a la casa, me di cuenta que aún tenía 0.25 kg. ¿Cuánto queso hay en total? "
            },
            {
                "t11pregunta": " kg."
                },
                /* <br><br>Para una donación a damnificados se juntaron 5<span class='fraction black'><span class='top'>1</span><span class='bottom'>8</span></span> kilogramos de frijol el primer día y 4.375 kg el segundo. En total se recolectaron "
            },
            {
                "t11pregunta": " kg. <br><br>Se juntaron 0.245 kg de tierra en un recipiente que tenía <span class='fraction black'><span class='top'>3</span><span class='bottom'>5</span></span> de kg, por tanto ahora hay "
            },
            {
                "t11pregunta": " kilogramos. <br>"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta a cada enunciado en forma decimal.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //12
    {
        "respuestas": [
            {
                "t13respuesta": "1<span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "3<span class='fraction'><span class='top'>2</span><span class='bottom'>5</span></span>",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "5<span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "7<span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span>",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "1",
                "t17correcta": "5",
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "0.75 + <span class='fraction'><span class='top'>1</span><span class='bottom'>2</span></span>"
            },
            {
                "t11pregunta": "2.95 + 0.45"
            },
            {
                "t11pregunta": "<span class='fraction'><span class='top'>7</span><span class='bottom'>10</span></span> + 4.55"
            },
            {
                "t11pregunta": "<span class='fraction'><span class='top'>5</span><span class='bottom'>4</span></span> + 6.5"
            },
            {
                "t11pregunta": "<span class='fraction'><span class='top'>7</span><span class='bottom'>20</span></span> + 0.65"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //13
    {
        "respuestas": [

            {
                "t13respuesta": "C",
                "t17correcta": "1",
                "numeroPregunta": "1"
            }, {
                "t13respuesta": "B",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },{
                "t13respuesta": "D",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },


            {
                "t13respuesta": "A",
                "t17correcta": "1",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "D",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }, {
                "t13respuesta": "B",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },

/*
             {
                "t13respuesta": "-20, 20",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },            {
                "t13respuesta": "-15, 20",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },            {
                "t13respuesta": "-12, 20",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },

*/

            {
                "t13respuesta": "6",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },            {
                "t13respuesta": "3",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },            {
                "t13respuesta": "5",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },



            {
                "t13respuesta": "7",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "6",
                "t17correcta": "0",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "4",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>","<img src='MI7E_B01_T7_R01-01.png' width=472 height=116px/><br><br>¿Quién se encuentra más cerca del 0?",
                "<br><br><img src='MI7E_B01_T7_R01-02.png' width=472px height=116px/><br><br>¿Quién está más alejado del 0?",
                //"<br><br><img src='MI7E_B01_T7_R01-03.png' width=472px height=116px/><br><br>¿Qué números faltan en la recta?",
                "<br><br><img src='MI7E_B01_T7_R01-04.png' width=472px height=116px/><br><br>Si te posicionas en el lugar que marca la flecha, ¿Cuántos lugares te debes mover para llegar a -3?",
                "¿Cuántos grados de diferencia hay entre -3° y 4°?"
            ],
            //"t11pregunta": "",
            "preguntasMultiples": true
        }
    },
    //14
    {
        "respuestas": [
            {
                "t13respuesta": "F",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "V",
                "t17correcta": "1"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lugar con el mayor grado de temperatura mínima es Verkhoyansk.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Entre la temperatura máxima y mínima en Noruega hay 15° de temperatura.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lugar con menor cantidad de grados de temperatura máxima es Oymyakon.",
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lugar con menor cantidad de grados de temperatura mínima es Alaska.",
                "correcta": "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "El lugar con mayor cantidad de grados de temperatura máxima es Noruega.",
                "correcta": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11instruccion": "<center><img src='MI7E_B01_R14_00.png'/></center>",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<br>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //15

    {
        "respuestas": [
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "12"
            },
            {
                "t17correcta": "13"
            },/*
            {
                "t17correcta": "14"
            },
            {
                "t17correcta": "-17"
            },
            {
                "t17correcta": "38"
            },
            {
                "t17correcta": "30"
            },
            {
                "t17correcta": ""
            }*/
        ],
        "preguntas": [
            {
                "t11pregunta": "-3 - (-6) = "
            },
            {
                "t11pregunta": " <br>-18 + 21 ="
            },
            {
                "t11pregunta": " <br>-32 + (-"
            },
            {
                "t11pregunta": ") = -44 <br>-10 + (-3) =-"
            },
            {
                "t11pregunta": " <br>"
            },/*
            {
                "t11pregunta": " - (-6) = 20 <br>-23 - (-6) ="
            },
            {
                "t11pregunta": " <br>-"
            },
            {
                "t11pregunta": " - (-24) = -14 <br>36 + (-6) = "
            },
            {
                "t11pregunta": " <br>"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Resuelve las siguientes operaciones.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: false
        }
    },
    //16
    {
        "respuestas": [
            {
                "t13respuesta": "17",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },            {
                "t13respuesta": "-17",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },            {
                "t13respuesta": "+-17",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },



            {
                "t13respuesta": "-36",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },            {
                "t13respuesta": "-68",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },            {
                "t13respuesta": "-89",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },


            {
                "t13respuesta": "-53",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "-17",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "17",
                "t17correcta": "0",
                "numeroPregunta": "2"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Si a 9 le restas -8, ¿qué resultado obtienes?",
                "<br><br>Si a -52 le sumas 16, ¿cuál es el resultado? ",
                "<br><br>Si a -35 le sumas -18, ¿cuál es el resultado que obtienes?"
            ],
            "preguntasMultiples": true
        }
    },
    //17
    {
        "respuestas": [

            {

                "t13respuesta": "<span class='fraction'><span class='top'>17</span><span class='bottom'>15</span></span>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>41</span><span class='bottom'>8</span></span>",
                "t17correcta": "2"
            },
              {
                "t13respuesta": "1.127",
                "t17correcta": "3"
            },
           /* {
                "t13respuesta": "<span class='fraction'><span class='top'>8503</span><span class='bottom'>196</span></span>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1.127",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "32.811",
                "t17correcta": "5"
            }*/
        ],
        "preguntas": [
        {
               "t11pregunta":"<span class='fraction'><span class='top'>3</span><span class='bottom'>2</span></span>+<span class='fraction'><span class='top'>4</span><span class='bottom'>6</span></span>-<span class='fraction'><span class='top'>1</span><span class='bottom'>5</span></span>",
               
            },  {
                "t11pregunta": "<br><br><span class='fraction'><span class='top'>7</span><span class='bottom'>2</span></span> - <span class='fraction'><span class='top'>3</span><span class='bottom'>8</span></span> = "
            }, {
                "t11pregunta": " <br><br><span class='fraction'><span class='top'>83</span><span class='bottom'>21</span></span> + <span class='fraction'><span class='top'>39</span><span class='bottom'>8</span></span> - 7.7= "
            },


          /*
            {
                "t11pregunta": " <br><br><span class='fraction'><span class='top'>42</span><span class='bottom'>5</span></span> + <span class='fraction'><span class='top'>2</span><span class='bottom'>45</span></span> - <span class='fraction'><span class='top'>1</span><span class='bottom'>2</span></span> = "
            },
            {
                "t11pregunta": " <br><br> 5 + <span class='fraction'><span class='top'>77</span><span class='bottom'>2</span></span> + <span class='fraction'><span class='top'>13</span><span class='bottom'>98</span></span> - <span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span> ="
            },
           
            {
                "t11pregunta": " <br><br> 21.9 + 11.5 - <span class='fraction'><span class='top'>58</span><span class='bottom'>66</span></span> + <span class='fraction'><span class='top'>25</span><span class='bottom'>86</span></span> = "
            }*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra el resultado en fracción simplificada o en  decimal  de cada operación. Toma tres cifras después del punto decimal.",
            "respuestasLargas": true,
            "pintaUltimaCaja": true,
            "contieneDistractores": true
        }
    },
    //18
    {
        "respuestas": [
            {
                "t13respuesta": "10<span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span>",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "19.307",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>137</span><span class='bottom'>16</span></span>",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "13.8",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>235</span><span class='bottom'>6</span></span>",
                "t17correcta": "5",
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Mariana compró 9<span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span> m de tela blanca, su mamá le pidió 3<span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span> y su tía le pidio 2<span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span>. ¿Cuántos metros de tela blanca le quedaron?"
            },
            {
                "t11pregunta": "Sofía compró  tres costales de croquetas. El primer costal de 3<span class='fraction'><span class='top'>8</span><span class='bottom'>7</span></span> kg, el segundo costal de 5.7 kg y el tercer costal de <span class='fraction'><span class='top'>78</span><span class='bottom'>8</span></span> kg. ¿Cuántos kilos de croquetas suman los costales? "
            },
            {
                "t11pregunta": "Un cultivador siembra <span class='fraction'><span class='top'>63</span><span class='bottom'>24</span></span> de su granja con trigo y <span class='fraction'><span class='top'>95</span><span class='bottom'>16</span></span> con maíz. ¿En total qué fracción de la granja sembró?  "
            },
            {
                "t11pregunta": "Un nadador entrena nadando en una alberca semiolímpica. El primer dia nada 4.5 km, el segundo día nada 4<span class='fraction'><span class='top'>1</span><span class='bottom'>3</span></span> km, el tercer dia nada 3 km y el último día  <span class='fraction'><span class='top'>8</span><span class='bottom'>4</span></span> km. ¿Cuántos kilómetros nadó en los cuatro días? "
            },
            {
                "t11pregunta": "Manuel utilizó <span class='fraction'><span class='top'>47</span><span class='bottom'>6</span></span> kg de harina par hornear un pastel. ¿Cuántos kilogramos de harina utilizará para hornear 5 pasteles? "
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //19
    {
        "respuestas": [
            //tabla 1
            {
                "t17correcta": "37.05"
            },
            {
                "t17correcta": "86.25"
            },
            {
                "t17correcta": "277.2"
            },/*
            {
                "t17correcta": "250.25"
            },
            {
                "t17correcta": "106.25"
            },
            //tabla 2
            {
                "t17correcta": "83.76"
            },
            {
                "t17correcta": "15.75"
            },
            {
                "t17correcta": "76.7"
            },
            {
                "t17correcta": "22.4"
            },
            {
                "t17correcta": "49.98"
            }*/
        ],
        "preguntas": [
            {
                "t11pregunta": "<style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class='table' style= 'margin-top:-20px;'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                       <tr>\n\  <td>Producto</td>\n\
                                                                         <td>Precio por kg</td>\n\
                                                                         <td>Cantidad en kg</td>\n\
                                                                         <td>Cantidad a pagar</td>\n\
                                                                         \n\<tr><td>Huevo</td><td>$24.7</td><td>1.5 kg</td><td>$&nbsp "
            },
            {
                "t11pregunta": "</td></tr><tr><td>Jamón</td><td>$172.5</td><td>0.5</td><td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Queso</td><td>$154</td><td>1.8 kg</td>\n\<td>$&nbsp"
            },/*
            {
                "t11pregunta": "</td></tr><tr><td>Pescado</td><td>$192.5</td><td>1.3 kg</td>\n\<td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Pollo</td><td>$42.5</td><td>2.5 kg</td>\n\<td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Manzana</td><td>$34.9</td><td>2.4 kg</td>\n\<td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Limón</td><td>$22.5</td><td>0.7 kg</td>\n\<td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Aguacate</td><td>$59</td><td>1.3 kg</td>\n\<td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Tortillas</td><td>$11.2</td><td>2 kg</td>\n\<td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><tr><td>Jitomate</td><td>$14.7</td><td>3.4 kg</td>\n\<td>$&nbsp"
            },
            {
                "t11pregunta": "</td></tr><table>"
            }*/

        ],
        "pregunta": {
            "t11pregunta": "Escribe la cantidad total a pagar por cada producto comprado.",
            "c03id_tipo_pregunta": "6",
            "pintaUltimaCaja": true,
            evaluable: true
        }
    },
    //20
    {
        "respuestas": [
            //tabla 1
            {
                "t17correcta": "90"
            },
            //tabla 2
            {
                "t17correcta": "15"
            },
            //tabla 3
            {
                "t17correcta": "30"
            },
            //tabla 4
          /*  {
                "t17correcta": "24"
            },
            //tabla 5
            {
                "t17correcta": "98"
            }*/
        ],
        "preguntas": [
            {
                "t11pregunta": "<style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class='table' style= 'margin-top:-20px;'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                       <tr>\n\  <td>A</td><td>B</td></tr><tr><td>45</td><td>18</td></tr><tr><td>&nbsp"   },

            {
                "t11pregunta": "</td>\n\<td>36</td><table><br><style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class= 'table' style= 'margin-top:-20px;'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                      <tr>\n\  <td>C</td><td>D</td></tr><tr><td>12</td><td>45</td></tr><tr><td>4</td><td>&nbsp"},
            {
                "t11pregunta": "</td></tr><table> <br><style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class= 'table' style= 'margin-top:-20px;'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                      <col width='100'>\n\
                                                                      <tr>\n\  <td>E</td><td>F</td></tr><tr><td>25</td><td>&nbsp"},
                                                                       {
                "t11pregunta": "</td></tr><tr><td>5</td><td>6</td><table>"},

                                                                      /*
            {
                "t11pregunta": "</td></tr><tr><td>5</td><td>6</td><table><br><style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class= 'table' style= 'margin-top:-20px;'>\n\
                                                                        <col width='100'>\n\
                                                                        <col width='100'>\n\
                                                                        <col width='100'>\n\
                                                                        <tr>\n\  <td>G</td><td>H</td></tr><tr><td>&nbsp"},
            {
                "t11pregunta": "</td><td>16</td></tr><tr><td>3</td><td>2</td><table><br><style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class= 'table' style= 'margin-top:-20px;'>\n\
                                                                            <col width='100'>\n\
                                                                            <col width='100'>\n\
                                                                            <col width='100'>\n\
                                                                            <tr>\n\  <td>I</td><td>J</td></tr><tr><td>3</td><td>7</td></tr><tr><td>42</td><td>&nbsp"},*/
           /* {
                "t11pregunta": "</td></table>"
            },*/

        ],
        "pregunta": {
            "t11pregunta": "Escribe el valor faltante en cada tabla.",
            "c03id_tipo_pregunta": "6",
            "pintaUltimaCaja": false,
            evaluable: true
        }
    },
    //21
    {
        "respuestas": [
            {
                "t17correcta": "12250"
            },
            {
                "t17correcta": "72"
            },
            {
                "t17correcta": "1500"
            },/*
            {
                "t17correcta": "448"
            },
            {
                "t17correcta": "10"
            },
            {
                "t17correcta": ""
            }*/
        ],
        "preguntas": [
            {
                "t11pregunta": "Juan obtiene $120 de comisión por cada $3500 de venta que realice. Esta semana obtuvo $420, debido a que realizó ventas por $"
            },
            {
                "t11pregunta": ". <br><br>Por cada $500 de compra se abonarán 25 puntos a un monedero electrónico, ayer compré una mochila de $1440, por lo tanto me abonaron "
            },
            {
                "t11pregunta": " puntos a mi monedero electrónico. <br><br>Héctor reparte 250 volantes de publicidad en 7 horas. Esta semana trabajó 42 horas repartiendo volantes, por tanto entregó "
            },
            {
                "t11pregunta": " volantes." },/* <br><br>Por cada 250 mililitros de un compuesto químico se deben agregar 32 mililitros de un catalizador. Para un requerimiento se utilizaron 3500 mililitros del compuesto, por lo que se le tuvieron que agregar "
            },
            {
                "t11pregunta": " mililitros del catalizador. <br><br>En una tienda regalan 3 boletos para una rifa por cada $360 pesos de compra. El día de ayer un cliente gastó $1200 en la tienda, por tanto le tocaron "
            },
            {
                "t11pregunta": " boletos. <br>"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta a cada enunciado.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: false
        }
    },
    //22
    {
        "respuestas": [
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "16"
            },
            {
                "t17correcta": "31"
            },
            {
                "t17correcta": "1"
            },
            {
                "t17correcta": "112"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "3 + 5 - 7 + 8 &times; 2 - 4 &times; 3 + 5 - 2 &times; 4 ="
            },
            {
                "t11pregunta": " <br>7 + 16 - 2<sup>3</sup> + 5 &times; 2 - 9="
            },
            {
                "t11pregunta": " <br>(7 - 3)+(4 + 5) - 3  &times;  (2 - 8) ="
            },
            {
                "t11pregunta": " <br>((9 - 2)+ 5 + (3 - 2)- 8 -((2 - 3) + (4 + 1)) ="
            },
            {
                "t11pregunta": " <br>4 &times; (6 &times; (12 - 4)- 5 &times; (7 - 3))="
            },
            {
                "t11pregunta": " <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe el resultado final de cada operación.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //23
    {
        "respuestas": [
            {
                "t13respuesta": "<p>5+7x6-4 &times; ((8-3)+9 &times; (12-10))+3 &times; (5<sup>3</sup>-25)</p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>5+7 &times; 6-4 &times; (8-3)+9 &times; (12-10)+3 &times; (5<sup>3</sup>-25)</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>5+7 &times; 6-4 &times; 8-3+9 &times; 12-10+3 &times; 5<sup>3</sup>-25</p>",
                "t17correcta": "2",
                etiqueta: "3"
            },/*/
            {
                "t13respuesta": "<p>5+7 &times; 6-4 &times; (8-3)+9 &times; ((12-10)+3 &times; (5<sup>3</sup>-25))</p>",
                "t17correcta": "3",
                etiqueta: "4"
            },
            {
                "t13respuesta": "<p>5+7 &times; (6-4 &times; 8-3+9 &times; 12-10+3 &times; 5<sup>3</sup>-25)</p>",
                "t17correcta": "4",
                etiqueta: "5"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena los siguientes elementos.<br><br>Realiza la operación indicada en cada ejercicio y ordena de menor a mayor según el resultado.",
            "t11instruccion": "",
        }
    },
    //24
    {
        "respuestas": [
            {
                "t13respuesta": "3118",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "32732",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "524",
                "t17correcta": "3",
            },/*
            {
                "t13respuesta": "0.142",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "262144",
                "t17correcta": "5",
            },*/
        ],
        "preguntas": [
            {
                "t11pregunta": "3<sup>6</sup> + 7<sup>4</sup> - &#8730; 144="
            },
            {
                "t11pregunta": "8<sup>5</sup> - 6<sup>2</sup>"
            },
         
            {
                "t11pregunta": "(5<sup>3</sup> +6)(4<sup>1</sup>)="
            },/*

   {
                "t11pregunta": "(85<sup>3</sup> + 60) (4<sup>1</sup>)="
            },            {
                "t11pregunta": "2<sup>18</sup>"
            },*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las operaciones con su resultado correcto.",
            "altoImagen": "100p &times; ",
            "anchoImagen": "200px"
        }
    },
    //25
    {
        "respuestas": [
            {
                "t13respuesta": "Opción 1 ",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Opción 1",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Opción 3",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Opción 4",
                "t17correcta": "3"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una pista de atletismo con forma cuadrada tiene un área de 3600 m<sup>2</sup>. Si Pedro corre alrededor de la pista y le da 8 vueltas, ¿qué distancia recorre? ",
                "valores": ['1920 m', '2000 m', '1860 m', '3600 m '],
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Andrea cortó un papel cascarón con forma de rombo con un área de 560 cm<sup>2</sup>. Si colocó un listón alrededor y le alcanzó para 7 vueltas, ¿cuánto mide el listón? ",
                "valores": ['367.3 cm', '584.5 cm', '662.6 cm', '854.5 cm'],
                "correcta": "2"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Adrián compró un terreno, donde construirá un vivero cuadrado con un área de 784 m<sup>2</sup>, ¿cuánto debe medir un lado del vivero? ",
                "valores": ['30 m', '28 m', '25 m', '33 m'],
                "correcta": "1"
            },/*
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Fernando construyó un cubo, cuyos lados miden x  y tiene un volumen de 729 cm<sup>3</sup>. ¿Cuánto mide un  lado del cubo?",
                "valores": ['9 cm', '7 cm', '11 cm', '4 cm'],
                "correcta": "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Rodrigo quiere construir un cubo. Si  cada lado va a medir 11 cm, ¿cuál es el volumen del cubo?",
                "valores": ['1450 cm<sup>3</sup>', '1331 cm<sup>3</sup>', '956 cm<sup>3</sup>', '1872 cm<sup>3</sup> '],
                "correcta": "1"
            }*/
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Selecciona la respuesta correcta.<\/p>",
            "descripcion": "Reactivo",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //26
    {
        "respuestas": [
            {
                "t13respuesta": "340 000",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "0.000 034",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "34 000 000",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "0.000 000 34",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "34",
                "t17correcta": "5",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "3.4 &times; 10<sup>5</sup>"
            },
            {
                "t11pregunta": "3.4 &times; 10<sup>-5</sup>"
            },
            {
                "t11pregunta": "3.4 &times; 10<sup>7</sup>"
            },
            {
                "t11pregunta": "3.4 &times; 10<sup>-7</sup>"
            },
            {
                "t11pregunta": "3.4 &times; 10<sup>1</sup>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //27
    {
        "respuestas": [
            {
                "t17correcta": "8.1"
            },
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "319760"
            },
            {
                "t17correcta": "6.4"
            },
            {
                "t17correcta": "4"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El resultado en notación científica de: 2.7  &times;  10<sup>3</sup> + 5.4  &times;  10<sup>3</sup> = "
            },
            {
                "t11pregunta": "  &times;  10<sup>"
            },
            {
                "t11pregunta": " </sup><br>El resultado en forma decimal de: 3.2  &times;  10<sup>5</sup> - 2.4  &times;  10<sup>5</sup> =  "
            },
            {
                "t11pregunta": " <br>El resultado en notación científica de: 54927 + 9073 = "
            },
            {
                "t11pregunta": "  &times;  10<sup>"
            },
            {
                "t11pregunta": " </sup><br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta en el lugar indicado. ",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //28
    {
        "respuestas": [
            {
                "t13respuesta": "<p>9.87  &times;  10<sup>-3</sup></p>",
                "t17correcta": "0",
                etiqueta: "1"
            },
            {
                "t13respuesta": "<p>27</p>",
                "t17correcta": "1",
                etiqueta: "2"
            },
            {
                "t13respuesta": "<p>1.3  &times;  10<sup>2</sup></p>",
                "t17correcta": "2",
                etiqueta: "3"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "9",
            "t11pregunta": "Ordena las siguientes cantidades de menor a mayor.",
            "t11instruccion": "",
        }
    }










];
