
json=[
     //1
     {
      "respuestas": [
          {
              "t13respuesta": "-15",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "-140",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "-36",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "214",
              "t17correcta": "4"
          },
          {
              "t13respuesta": "130",
              "t17correcta": "5"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "-12+(-3)="
          },
          {
              "t11pregunta": "1680÷ (-12)="
          },
          {
              "t11pregunta": "(-18)(2)="
          },
          {
              "t11pregunta": "156-(-58)="
          },
          {
              "t11pregunta": "125+5="
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "12",
          "t11pregunta": "Relaciona las operaciones con su resultado."
      }
  },
  //2
  {
    "respuestas":[
       {
          "t13respuesta":     "730",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "73.0",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "7.30",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "0.730",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "0.0856",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "0.856",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "856",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "8.56",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "25",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "15",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "26",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "60",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "7260",
          "t17correcta":"1",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "726",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "0.726",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "7.26",
          "t17correcta":"0",
        "numeroPregunta":"3"
       }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta.<br><br>7.3 x 100 =",
                      "85.6 ÷ 1000=",
                      "&#8730; 625",
                      "72.6 x 10<sup>2</sup>="],
     "preguntasMultiples": true
    }
  },
  //3
  {
    "respuestas": [
        //tabla 1 
      {
          "t17correcta": ">"
        },
        {
          "t17correcta": "<"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": "<"
        },
        //tabla 2
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": ">"
        },
        {
          "t17correcta": "<"
        },
        {
          "t17correcta": "<"
        },
        {
          "t17correcta": "<"
        },
    ],
    "preguntas": [
        {
          "t11pregunta": "<style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class='table' style= 'margin-top:-20px;'>\n\
          <col width='100'>\n\
          <col width='100'>\n\
          <col width='100'>\n\
           <tr>\n\  <td>5<sup>6</sup></td>\n\<td>&nbsp"   },
        {
          "t11pregunta": "</td>\n\<td>4<sup>4</sup></td>\n\</tr><tr>\n\<td>&#8730;5</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>&#8730;8</td>\n\</tr><tr>\n\<td>2<sup>9</sup></td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>7<sup>3</sup></td>\n\</tr><tr>\n\<td>11<sup>8</sup></td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>10<sup>2</sup></td>\n\</tr><tr>\n\<td>&#8730;58.2</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>&#8730;89.5</td><table><br><br><style>\n\   .table img{height: 90px !important; width: auto !important; }\n\  </style><br><table class= 'table' style= 'margin-top:-20px;'>\n\
          <col width='100'>\n\
          <col width='100'>\n\
          <col width='100'>\n\
           <tr>\n\  <td>&#8730;100</td>\n\<td>&nbsp"},
        {
          "t11pregunta": "</td>\n\<td>&#8730;50</td>\n\</tr><tr>\n\<td>9<sup>5</sup></td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>6<sup>5</sup></td>\n\</tr><tr>\n\<td>&#8730;81</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>&#8730;133</td>\n\</tr><tr>\n\<td>3<sup>7</sup></td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>8<sup>9</sup></td>\n\</tr><tr>\n\<td>&#8730;916.23</td>\n\<td>&nbsp"
        },
        {
          "t11pregunta": "</td>\n\<td>&#8730;916.24</td><table>"
        },
    ],
    "pregunta": {
        "t11pregunta": "Escribe el signo < o >, según corresponda.",
        "c03id_tipo_pregunta": "6",
        "pintaUltimaCaja": false,
        evaluable:true
    }
  },
  //4
  {
    "respuestas":[
       {
          "t13respuesta":     "6n + 3",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "3n - 6",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "6 x 3n",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "-3n x 6",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "2n+5",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "5n+2",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "5x2n",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "-2n+5",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "60n+540",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "60+600n",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "600n+100",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "60n+600",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta.<br><br>Regla general que está implícita en la sucesión:<br>9, 15, 21, 27, 33, 39...",
                      "Regla general que está implícita en la sucesión:<br>7, 9, 11, 13, 15, 17...",
                      "Regla general que está implícita en la sucesión:<br>600, 660, 720, 780, 840, 900..."],
     "preguntasMultiples": true
    }
  },
    //5
    {
        "respuestas": [
            {
                "t13respuesta": "9",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "56.54",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "254.46",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "4071.51",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "1112.48",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Radio de cada círculo en cm"
            },
            {
                "t11pregunta": "Perímetro de cada círculo en cm"
            },
            {
                "t11pregunta": "Área de cada círculo en cm<sup>2</sup>"
            },
            {
                "t11pregunta": "Área total perforada en cm<sup>2</sup>"
            },
            {
                "t11pregunta": "Área del papel cascarón sin perforar en cm<sup>2</sup>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "t11instruccion": "En un papel cascarón cuadrado de 72 cm de lado, se perforaron 16 círculos aprovechando al máximo el espacio, como se observa en la figura.<br><br><center><img src='MI7E_B05_A05_01.png' ></center>"
        }
    },
  //6
  {
    "respuestas": [
        {
            "t13respuesta": "<p>$ 1848<\/p>",
            "t17correcta": "1"
        },
        {
            "t13respuesta": "<p>$ 4704<\/p>",
            "t17correcta": "2"
        },
        {
            "t13respuesta": "<p>$ 4200<\/p>",
            "t17correcta": "3"
        },
        {
            "t13respuesta": "<p>$ 2688<\/p>",
            "t17correcta": "4"
        }
    ],
    "preguntas": [
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "<br><style>\n\
                                    .table img{height: 90px !important; width: auto !important; }\n\
                                </style><table class='table' style='margin-top:-40px;'>\n\
                                <tr>\n\
                                    <td style='width:150px'>Número de personas:</td>\n\
                                    <td style='width:100px'>Días:</td>\n\
                                    <td style='width:100px'>Presupuesto:</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td style='width:100px'>4</td>\n\
                                    <td style='width:100px'>11</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                </tr><tr>\n\
                                    <td>8</td>\n\
                                    <td>14</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                </tr><tr>\n\
                                    <td>20</td>\n\
                                    <td>5</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                </tr><tr>\n\
                                    <td>32</td>\n\
                                    <td>2</td>\n\
                                    <td>"
        },
        {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "        </td>\n\
                                    </tr></table>"
        }
    ],
    "pregunta": {
        "c03id_tipo_pregunta": "8",
        "t11pregunta": "Arrastra las cantidades correctas para completar la tabla.<br><br>En el comedor escolar se requieren $630 pesos diarios para dar de comer a quince niños.",
       "contieneDistractores":true,
        "anchoRespuestas": 70,
        "soloTexto": true,
        "respuestasLargas": true,
        "pintaUltimaCaja": false,
        "ocultaPuntoFinal": true
    }
},
];