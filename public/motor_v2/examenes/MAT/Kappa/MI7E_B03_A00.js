json = [
    //reactivo 1
    {
            "respuestas": [
            {
                "t17correcta": "272.04"
            },
            {
                "t17correcta": "22/5"
            },
            {
                "t17correcta": "111.60"
            },
            {
                "t17correcta": "5/36"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>El resultado en decimal de 226.7 x 1.2 = &nbsp"
            },
            {
                "t11pregunta": "<br>El resultado en fracción de <span class='fraction black'><span class='top'>14</span><span class='bottom'>5</span></span> x <span class='fraction black'><span class='top'>11</span><span class='bottom'>7</span></span> = &nbsp"
            },
            {
                "t11pregunta": "<br>El resultado en decimal de 1250  ÷ 11.2 = &nbsp"
            },
            {
                "t11pregunta": "<br>El resultado en fracción de <span class='fraction black'><span class='top'>1</span><span class='bottom'>2</span></span> ÷ <span class='fraction black'><span class='top'>18</span><span class='bottom'>5</span></span>= &nbsp"
            },
            
        ],
        "pregunta": {
            "t11pregunta": "Escribe el resultado de las siguientes operaciones en la forma en que fueron expresadas, es decir decimal o fracción, recuerda usar / para representar fracciones. Por ejemplo <span class='fraction black'><span class='top'>2</span><span class='bottom'>7</span></span>se expresa 3/4.",
            "c03id_tipo_pregunta": "6",
            evaluable:true
        }
    },
    //reactivo 2
    {
        "respuestas": [
            {
                "t17correcta": "1/20"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Andy pintó su casa. Usó  <span class='fraction black'><span class='top'>1</span><span class='bottom'>4</span></span> de su pintura para pintar cinco paredes.<br><img src='MI7E_B03_A02.png'> <br>¿Qué fracción de su pintura usó Andy en cada pared, si utilizó la misma cantidad de pintura en cada una?"
            },
             {
                "t11pregunta": "de pintura."
            },
            
        ],
        "pregunta": {
            "t11pregunta": "Escribe el resultado en el lugar correspondiente. Recuerda usar / para indicar fracciones",
            "c03id_tipo_pregunta": "6",
            evaluable:true,
            "pintaUltimaCaja": false
        }
    },
    //reactivo 3
     {  
      "respuestas":[
         {  
            "t13respuesta": "x+28=7",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "x+7=28",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "-7x=28",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "-7(21)=28",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "21+7=28",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "21=28-7",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
          {  
            "t13respuesta": "2x+4=56",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "56x=2+4",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "4x+2=56",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
          {  
            "t13respuesta": "56(26)=2+4",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "2(26)+4=56",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "4(26)+2=56",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
        
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta. <br><br>Si a un número le sumas  7, obtienes como resultado 28",
                         "En la ecuación anterior, x-7=28, si sustituyes x por 21, ¿qué operación representa la sustitución?",
                         "El doble de un número “x” más 4 es igual a 56.",
                         "En la ecuación 2x+4=56, si sustituyes x por 26, ¿qué operación representa esa sustitución?"],
         "preguntasMultiples": true
      }
   },
    //reactivo 4
    {
        "respuestas": [
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "120"
            },
            {
                "t17correcta": "4"
            },
            {
                "t17correcta": "90"
            },
            {
                "t17correcta": "5"
            },
            {
                "t17correcta": "72"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "El triángulo equilátero tiene &nbsp"
            },
            {
                "t11pregunta": "lados y la medida del ángulo central es de &nbsp"
            },
            {
                "t11pregunta": "°.<br>El cuadrado tiene &nbsp"
            },
            {
                "t11pregunta": "lados y la medida del ángulo central es de &nbsp"
            },
            {
                "t11pregunta": "°.<br>El pentágono tiene &nbsp"
            },
            {
                "t11pregunta": "lados y la medida del ángulo central es de &nbsp"
            },
             {
                "t11pregunta": "°."
            }
        ],
        "pregunta": {
            "t11pregunta": "Escribe la medida del ángulo central y el número de lados que tiene cada polígono regular. <br><img style='height: 260px' src='MI7E_B03_A04_01.png'> <img style='height: 260px' src='MI7E_B03_A04_02.png'><img style='height: 260px' src='MI7E_B03_A04_03.png'>",
            "c03id_tipo_pregunta": "6",
            evaluable:true,
            "pintaUltimaCaja": false
        }
    },
    //reactivo 5
    {
        "respuestas": [
            {
                "t17correcta": "34.4"
            },
            {
                "t17correcta": "89.44"
            },
            {
                "t17correcta": "30"
            },
            {
                "t17correcta": "69"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>El perímetro del octágono es &nbsp"
            },
            {
                "t11pregunta": "cm.<br>El área del octágono cortando a dos decimales es de &nbsp"
            },
            {
                "t11pregunta": "m<sup>2</sup>.<br>El perímetro del  dodecágono es &nbsp"
            },
            {
                "t11pregunta": "cm.<br>El área del  dodecágono es &nbsp"
            },
            {
                "t11pregunta": "m<sup>2</sup>."
            },
        ],
        "pregunta": {
            "t11pregunta" : "Calcula el perímetro y  área de los siguientes polígonos, escribe el resultado en el lugar correspondiente. <br><img style='height: 260px' src='MI7E_B03_A05_01.png'> <img style='height: 260px' src='MI7E_B03_A05_02.png'>>",
            "c03id_tipo_pregunta": "6",
            evaluable:true,
            "pintaUltimaCaja": false
        }
    },
    //reactivo 6
    {
      "respuestas":[
         {
            "t13respuesta": "4 veces",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "6 veces",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "16 veces",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "4",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "1",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "2",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "El número que salió menos veces en el juego anterior.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
          {
            "t13respuesta": "Cualquiera de los números tiene la misma oportunidad de salir.",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "El seis por ser el número mayor.",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>Se lanzó varias veces un dado y se anotaron los resultados en la siguiente tabla.<br><img style='height:200px' src='MI7E_B03_A06_01.png'><br>¿Cuántas veces se repitió el experimento?",
                         "¿Qué número salió con mayor frecuencia?",
                         "Si se repite el juego, ¿qué número saldrá más veces?"],
         "preguntasMultiples": true,
      }
   },
    //reactivo 7
    {
      "respuestas":[
         {
            "t13respuesta": "3° A",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "2° B",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta": "23 alumnos",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "24 alumnos",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "22 alumnos",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta": "1°A",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "3°B",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta": "2°C",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "t11pregunta": ["Selecciona todas las respuestas correctas para cada pregunta.<br>La tabla muestra la cantidad de alumnos que tienen mascotas en los grupos de secundaria de una escuela.<br><img style='height:200px' src='MI7E_B03_A07_01.png'><br>¿Cuál es el grupo que cuenta con el mayor número de alumnos con mascotas?",
                         "¿Cuántos alumnos de primer grado tienen mascotas?",
                         "¿En qué grupos existen el mismo número de alumnos con mascotas?"],
         "preguntasMultiples": true,
      }
    }

];
