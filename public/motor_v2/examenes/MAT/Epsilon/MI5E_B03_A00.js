
json=[
    {
        "respuestas": [
            {
                "t13respuesta": "<span class='fraction'><span class='top'>35</span><span class='bottom'>7</span></span>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>42</span><span class='bottom'>6</span></span>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>48</span><span class='bottom'>6</span></span>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<span class='fraction'><span class='top'>36</span><span class='bottom'>3</span></span>",
                "t17correcta": "4"
            },
          
        ],
        "preguntas": [
            {
                "t11pregunta": "5"
            },
            {
                "t11pregunta": "7"
            },
            {
                "t11pregunta": "8"
            },
            {
                "t11pregunta": "12"
            },
           
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
            
        }
    },
    //2
   {  
      "respuestas":[
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>5</span><span class='bottom'>4</span></span>",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>4</span><span class='bottom'>6</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>4</span><span class='bottom'>4</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>4</span><span class='bottom'>2</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>8</span><span class='bottom'>5</span></span>",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>8</span><span class='bottom'>10</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>16</span><span class='bottom'>25</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "<span class='fraction black'><span class='top'>16</span><span class='bottom'>5</span></span>",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
          {  
            "t13respuesta": "2.4",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "19",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "2.7",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "5.4",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
          {  
            "t13respuesta": "37.3",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "2.4",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "36.2",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "6.26",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }
      ],
      "pregunta":{  
         "c03id_tipo_pregunta":"1",
         "t11pregunta": ["Selecciona la respuesta correcta.<br> <span class='fraction black'><span class='top'>1</span><span class='bottom'>2</span></span> + <span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span> =",
                         "<span class='fraction black'><span class='top'>12</span><span class='bottom'>5</span></span> - <span class='fraction'><span class='top'>4</span><span class='bottom'>5</span></span> =","10.7-8.3=","21.78+15.52="],
         "preguntasMultiples": true,
         
      }
   
   }, 
   
    //3
    
    {
        "respuestas": [
            {
                "t17correcta": "12"
            },
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "25"
            },
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "27"
            },
            {
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>El cociente de 62÷5 es:&nbsp"
            },
            {
                "t11pregunta": "<br>El residuo de 62÷5 es: &nbsp"
            },
            {
                "t11pregunta": "<br>El cociente de 128÷5 es: &nbsp"
            },
            {
                "t11pregunta": "<br>El residuo de 128÷5 es: &nbsp"
            },
            {
                "t11pregunta": "<br>El cociente de 193÷7 es: &nbsp"
            },
            {
                "t11pregunta": "<br>El residuo de 193÷7 es: &nbsp"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la cifra que responde correctamente al enunciado.",
            evaluable:true
            
        }
    },
    //4
    {
          "respuestas": [
              {
                  "t13respuesta": "Todos sus lados son iguales, tiene seis caras, ocho vértices y doce aristas",
                  "t17correcta": "1"
              },
              {
                  "t13respuesta": "Sus caras están conformadas por triángulos, tiene cinco de ellas, cinco vértices y ocho aristas.",
                  "t17correcta": "2"
              },
              {
                  "t13respuesta": "Está conformado por una cara plana circular y otra cara curva, tiene una arista curva y solamente un vértice",
                  "t17correcta": "3"
              },
              {
                  "t13respuesta": "Tiene dos caras planas circulares y una cara curva, tiene dos aristas curvas y ningún vértice.",
                  "t17correcta": "4"
              }
          ],
          "preguntas": [
              {
                  "t11pregunta": "<img src='MI5E_B03_A04_01.png'>"
              },
              {
                  "t11pregunta": "<img src='MI5E_B03_A04_02.png'>"
              },
              {
                  "t11pregunta": "<img src='MI5E_B03_A04_03.png'>"
              },
              {
                  "t11pregunta": "<img src='MI5E_B03_A04_04.png'>"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "12",
              "t11pregunta":"Relaciona las columnas según corresponda.",
              "altoImagen":"150"
          }
      },
    //5
    {
        "respuestas": [
            {
                "t17correcta": "24"
            },
            {
                "t17correcta": "144"
            }
            
        ],
        "preguntas": [
            {
                "t11pregunta": "<br><img src='MI5E_B03_A05_01.png'><br>¿Cuál es el área del triángulo? &nbsp"
            },
            {
                "t11pregunta": "cm<sup>2</sup><br><img src='MI5E_B03_A05_02.png'><br>¿Cuál es el área del trapecio?&nbsp "
            },
            {t11pregunta:"cm<sup>2</sup><br>"}
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta":"Escribe la cantidad que contesta correctamente lo siguiente.",
            evaluable:true,
            "pintaUltimaCaja":false
            
        }
    },
//6
    {
        "respuestas": [
            {
                "t13respuesta": "<p>300<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>40000<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>8000<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>80<\/p>",
                "t17correcta": "6"
            },
            {
                "t13respuesta": "<p>100<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>0.01<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>0.05<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta":  "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td>cm<sup>2</sup></td>\n\
                                        <td>dm<sup>2</sup></td>\n\
                                        <td>m<sup>2</sup></td>\n\
                                        </tr><tr>\n\
                                        <td>30000</td>\n\
                                        <td>"
                
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>400</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr><tr>\n\
                                        <td>"
                                        
                                        
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
                                        
                                        
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>0.8</td>\n\
                                        </tr><tr>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>1</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr><tr>\n\
                                        <td>500</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr>\n\
                                        </table>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra las cantidades a su lugar correspondiente.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    //7
     {
        "respuestas": [
            {
                "t17correcta": "23"
            },
            {
                "t17correcta": "17"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Paola fue al mercado y compró 20 kg de frijol a $460.<br>¿Cuánto cuesta el kilogramo de frijol?&nbsp"
            },
            {
                "t11pregunta": "pesos<br>¿Cuántos kilogramos se pueden comprar con $391? &nbsp"
            },
            {
                "t11pregunta": "kilogramos"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la cantidad que contesta correctamente cada pregunta.",
            evaluable:true,
            "pintaUltimaCaja":false
            
        }
    },

];

