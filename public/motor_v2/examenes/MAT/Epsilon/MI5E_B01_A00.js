json=[
    // //1
   {
          "respuestas": [
              {
                  "t13respuesta": "Falso",
                  "t17correcta": "0"
              },
              {
                  "t13respuesta": "Verdadero",
                  "t17correcta": "1"
              },
  
          ],
          "preguntas" : [
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Para obtener una fracción equivalente se debe multiplicar o dividir el numerador y el denominador por un mismo número. ",
                 
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Una fracción equivalente de 2/4 es 4/2",
                 "correcta"  : "0"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "2/3 es una fracción equivalente de 10/9",
              "correcta"  : "0"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Una fracción equivalente de 1/2 es 2/4",
               
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "21/6 es una fracción equivalente de 1/8",
             
                  "correcta"  : "1"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
                
              "variante": "editable",
              "anchoColumnaPreguntas": 30,
              "evaluable"  : true
          }        
      } ,
    //2
    {
      "respuestas":[
         {
            "t13respuesta":     "30",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "50",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "78",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "300",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "200",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "100",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "102 &#247; 2",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "100 &#247; 1",
            "t17correcta":"0",
          "numeroPregunta":"2"
         }
         ,
         {
            "t13respuesta":     "200 &#247; 2",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "630 &#247; 6",
            "t17correcta":"1",
          "numeroPregunta":"3"
         }
         ,
         {
            "t13respuesta":     "782 &#247; 2",
            "t17correcta":"0",
          "numeroPregunta":"3"
         }
         ,
         {
            "t13respuesta":     "498 &#247; 6",
            "t17correcta":"0",
          "numeroPregunta":"3"
         } ,
         {
            "t13respuesta":     "10000",
            "t17correcta":"1",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "1000",
            "t17correcta":"0",
          "numeroPregunta":"4"
         },
         {
            "t13respuesta":     "100",
            "t17correcta":"0",
          "numeroPregunta":"4"
         }
  
  
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona la respuesta correcta. <br><br>¿Cuál de los siguientes es el valor más aproximado de 456  12? ",
                        "¿Cuál de los siguientes es el valor más aproximado de 95700 300?",
                        "¿A qué división pertenece el valor aproximado 50?",
                        "¿A qué división pertenece el valor aproximado 100?",
                        "¿Cuál de los siguientes es el valor más aproximado de 62685&#247;5 ?"],
       "preguntasMultiples": true
      }
    },
    //3
     {
        "respuestas": [
            {
                "t13respuesta": "193",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "142",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "1571",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "1577",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "965 &#247; 5",
            },
            {
                "t11pregunta": "2698 &#247; 19"
            },
            {
                "t11pregunta": "65982 &#247; 42"
            },
            {
                "t11pregunta": "9462 &#247; 6"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "<p>Relaciona las columnas según corresponda.<\/p>",
        }
    },//4
    
  {
          "respuestas": [
              {
                  "t13respuesta": "Opcion 1",
                  "t17correcta": "0"
              },
              {
                  "t13respuesta": "Opcion 2",
                  "t17correcta": "1"
              },
              {
                  "t13respuesta": "Opcion 3",
                  "t17correcta": "2"
              },
              {
                  "t13respuesta": "Opcion 4",
                  "t17correcta": "3"
              }
          ],
          "preguntas" : [
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Angulo obtuso",
                  "valores": ['<img src="MI5E_B01_R07-01.png" width="150" height="75">', '<img src="MI5E_B01_R07-02.png" width="150" height="75">', '<img src="MI5E_B01_R07-03.png" width="150" height="75">','<img src="MI5E_B01_R07-04.png" width="150" height="75">'],
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Angulo llano",
                  "valores": ['<img src="MI5E_B01_R07-03.png" width="150" height="75">', '<img src="MI5E_B01_R07-06.png" width="150" height="75">', '<img src="MI5E_B01_R07-01.png" width="150" height="75">','<img src="MI5E_B01_R07-08.png" width="150" height="75">'],
                  "correcta"  : "2"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Angulo recto",
                  "valores": ['<img src="MI5E_B01_R07-03.png" width="150" height="75">', '<img src="MI5E_B01_R07-10.png" width="150" height="75">', '<img src="MI5E_B01_R07-11.png" width="150" height="75">', '<img src="MI5E_B01_R07-12.png" width="150" height="75">',],
                  "correcta"  : "0"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Angulo agudo",
                  "valores": [ '<img src="MI5E_B01_R07-02.png" width="150" height="75">', '<img src="MI5E_B01_R07-14.png" width="150" height="75">','<img src="MI5E_B01_R07-03.png" width="150" height="75">','<img src="MI5E_B01_R07-16.png" width="150" height="75">'],
                  "correcta"  : "3"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada pregunta.<\/p>",
              "descripcion": "Angulos",   
              "variante": "editable",
              "anchoColumnaPreguntas": 30,
              "evaluable"  : true
          }        
      },
    //5
    { 
   "respuestas":[ 
  {
                  "t13respuesta": "Falso",
                  "t17correcta": "0"
              },
              {
                  "t13respuesta": "Verdadero",
                  "t17correcta": "1"
              }
  ], 
   "preguntas": [ 
    {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta":"La cafetería se encuentra al sur de donde se encuentran María y Alicia." ,
            
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta":"El restaurante se encuentra al oeste de donde se encuentra Alicia." ,
  
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                 "t11pregunta":"Para llegar al palacio municipal Alicia debe ir al sur y al este." ,
         
                  "correcta"  : "0"
              },
              {
                  "c03id_tipo_pregunta": "13",
                "t11pregunta":"El hotel se encuentra al este de la ubicación de Alicia y al sur de la ubicación de María." ,
          
                  "correcta"  : "0"
              },
               {
                  "c03id_tipo_pregunta": "13",
                 "t11pregunta":"María recorrió 2 cuadras al oeste y una cuadra al norte y una cuadra al oeste; llegó al museo del cuerpo humano."  ,
  
                  "correcta"  : "1"
              },
               {
                  "c03id_tipo_pregunta": "13",
                 "t11pregunta":"Alicia debe recorrer 6 cuadras al oeste para llegar al restaurante y 2 cuadras al sur para llegar a la tienda de regalos."  ,
          
                  "correcta"  : "0"
              }
   ],
          "pregunta": {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
              "descripcion": "Angulos",   
              "variante": "editable",
              "anchoColumnaPreguntas": 30,
              "evaluable"  : true,
              "t11instruccion":'<img src="MI5E_B01_R05.png" >'
          }    
   } ,
    //6
    {
          "respuestas": [
              {
                  "t13respuesta": "Opcion 1",
                  "t17correcta": "0"
              },
              {
                  "t13respuesta": "Opcion 2",
                  "t17correcta": "1"
              },
              {
                  "t13respuesta": "Opcion 3",
                  "t17correcta": "2"
              },
              {
                  "t13respuesta": "Opcion 4",
                  "t17correcta": "3"
              },
              {
                  "t13respuesta": "Opcion 5",
                  "t17correcta": "4"
              },
              {
                  "t13respuesta": "Opcion 6",
                  "t17correcta": "5"
              }
          ],
          "preguntas" : [
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Las naranjas para hacer un jugo se pueden medir en:",
                  "valores": ['Litros', 'Kilogramos', 'Gramos','Toneladas','Mililitros','Montones'],
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Si quiero pesar unos aretes, estos se pueden medir mediante la unidad:",
                "valores": ['Toneladas', 'Kilogramos', 'Gramos','Litros','Mililitros','Yardas'],
                  "correcta"  : "2"
              },
      
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "El agua es más fácil pesarla en:",
               "valores": ['Litros','Mililitros', 'Kilogramos', 'Gramos', 'Superficie', 'Toneladas'],
                  "correcta"  : "0"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Los animales como los elefantes se pesan en:",
                 "valores": ['pies','Gramos','Kilogramos','Toneladas',  'Mililitros','Litros'],
                  "correcta"  : "3"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada pregunta.<\/p>",
              "descripcion": "Angulos",   
              "variante": "editable",
              "anchoColumnaPreguntas": 30,
              "evaluable"  : true
          }        
      },
    //7
    {
          "respuestas": [
              {
                  "t13respuesta": "Opcion 1",
                  "t17correcta": "0"
              },
              {
                  "t13respuesta": "Opcion 2",
                  "t17correcta": "1"
              },
              {
                  "t13respuesta": "Opcion 3",
                  "t17correcta": "2"
              },
              {
                  "t13respuesta": "Opcion 4",
                  "t17correcta": "3"
              }
          ],
          "preguntas" : [
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Si eran las 6:50 y ahora son las 8:25, ¿cuánto tiempo ha transcurrido?",
                  "valores": ['1 hora 25 minutos', ' 1 hora y 35 minutos', '35 minutos','2 horas 5 minutos'],
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "¿Cuánto tiempo son 5 lustros?",
                "valores": ['25 años', '50 años', '5 años','500 años'],
                  "correcta"  : "0"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Al decir que ha transcurrido 1/4 de hora, ¿cuántos minutos han pasado?",
               "valores": ['10 minutos','45 minutos','15 minutos', '12 minutos'],
                  "correcta"  : "2"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Han pasado 3600 segundos, ¿a cuántos minutos equivale?",
               "valores": ['45 minutos','60 minutos', '50 minutos', '36 minutos'],
                  "correcta"  : "1"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "Un libro tiene 3 siglos desde que fue escrito ¿cuántos años representan?  ",
                     "valores": ['3000 años','30 años','33 años','300 años'],
                  "correcta"  : "3"
              }
              ,
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "30 años son igual a: ",
             
                 "valores": ['3 décadas','30 decadas','3 siglos','13 siglos'],
                  "correcta"  : "0"
              },
              {
                  "c03id_tipo_pregunta": "13",
                  "t11pregunta": "¿Cuántos minutos reúnen 24 horas?",
                 "valores": ['1040 minutos','1400 minutos','1440 minutos','1140 minutos'],
                  "correcta"  : "2"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "13",
              "t11pregunta": "<p>Observa la siguiente matriz y marca la opción que corresponda a cada pregunta.<\/p>",
              "descripcion": "Angulos",   
              "variante": "editable",
              "anchoColumnaPreguntas": 30,
              "evaluable"  : true
          }        
      },
      //8
      {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0"
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1"
            },

        ],
        "preguntas" : [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el primer espacio deben estar escritos 3/16",
               
                "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el segundo espacio deben estar escritos 10/16",
               "correcta"  : "0"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el tercer espacio deben estar escrito 5/8",
            "correcta"  : "1"
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En el último espacio debe estar escrito 1",
           
                "correcta"  : "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "<p>Elige falso (F) o verdadero (V) según corresponda.<\/p>",
            "t11instruccion":"<center><img src='MI5E_B01_R08.png' >",  
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable"  : true
        }        
    } ,
    //9
      { 
            "respuestas": [        
                { 
                   "t17correcta":  "20" 
                 },
                { 
                   "t17correcta":  "3000" 
                 },
                { 
                   "t17correcta":  "1" 
                 },
                { 
                   "t17correcta":  "35" 
                 },
                { 
                   "t17correcta":  "60" 
                 },
                { 
                   "t17correcta":  "350" 
                 },
                { 
                   "t17correcta":"" 
                 } 
            ],
            "preguntas": [ 
              { 
              "t11pregunta": "" 
              }, 
             { 
              "t11pregunta": "minutos = <fraction class='black'><span>1</span><span>3</span></fraction> hora <br>3 m= " 
              }, 
             { 
              "t11pregunta": "mm <br>135 cm = " 
              }, 
             { 
              "t11pregunta": "." 
              }, 
             { 
              "t11pregunta": " m <br>1 minuto = " 
              }, 
             { 
              "t11pregunta": " segundos <br>" 
              }, 
             { 
              "t11pregunta": " m = 3500 dm <br>" 
              }, 
           ],
            "pregunta": { 
              "c03id_tipo_pregunta": "6", 
              "t11pregunta": "Resuelve las operaciones.", 
              "t11instruccion":"", 
               evaluable:true, 
               pintaUltimaCaja:true
            } 
          } ,
        //10
        {
                "respuestas": [        
                    {
                       "t13respuesta":  "4",
                         "t17correcta": "1" 
                     },
                    {
                       "t13respuesta":  "0.5",
                         "t17correcta": "2" 
                     },
                    {
                       "t13respuesta":  "0.12",
                         "t17correcta": "3" 
                     },
                    {
                       "t13respuesta":  "120",
                         "t17correcta": "4" 
                     },
                    {
                       "t13respuesta":  "centésimos",
                         "t17correcta": "5" 
                     },
                    {
                       "t13respuesta":  "6",
                         "t17correcta": "6" 
                     },
             {
                           "t13respuesta":  "80",
                             "t17correcta": "7" 
                         },
                 {
                               "t13respuesta":  "1.5",
                                 "t17correcta": "8" 
                             },
                     {
                                   "t13respuesta":  "1.2",
                                     "t17correcta": "9" 
                                 },
                         {
                                       "t13respuesta":  "0.2",
                                         "t17correcta": "10" 
                                     },
                             {
                                           "t13respuesta":  "unidades",
                                             "t17correcta": "11" 
                                         },
                                 {
                                               "t13respuesta":  "milésimos",
                                                 "t17correcta": "12" 
                                             },
                                     
                ],
                "preguntas": [ 
                  {
                  "t11pregunta": "Si 40 centésimos entre 8 es equivalente a "
                  },
                  {
                  "t11pregunta": " décimos entre 8, entonces: 48 = "
                  },
                  {
                  "t11pregunta": " unidades<br>En una carretera, se hicieron marcas en una sección de 6 km, se dividió en un total de  50 secciones. Cada sección mide "
                  },
                  {
                  "t11pregunta": " km que es equivalente a "
                  },
                  {
                  "t11pregunta": " m<br>6 décimos entre 4 es equivalente a 60 "
                  },
                  {
                  "t11pregunta": " entre 4  y  "
                  },
                  {
                  "t11pregunta": "  4 = 1.5<br>"
                  },
                ],
                "pregunta": {
                  "c03id_tipo_pregunta": "8",
                  "t11pregunta": "Arrastra las respuestas que completen el párrafo.", 
                   "t11instruccion": "", 
                   "respuestasLargas": true, 
                   "pintaUltimaCaja": false, 
                   "contieneDistractores": true 
                 } 
              } ,
            //11
            {
                "respuestas": [
                    {
                        "t13respuesta": "Opcion 1",
                        "t17correcta": "0"
                    },
                    {
                        "t13respuesta": "Opcion 2",
                        "t17correcta": "1"
                    },
                    {
                        "t13respuesta": "Opcion 3",
                        "t17correcta": "2"
                    },
                ],
                "preguntas" : [
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<img src='MI5E_B01_R11_01.png' width='70' height='80'><br>¿Cuál es su segmento opuesto?",
                        "valores": ['<img src="MI5E_B01_R11_02.png" width="70" height="80">', '<img src="MI5E_B01_R11_03.png" width="70" height="80">', '<img src="MI5E_B01_R11_04.png" width="70" height="80">'],
                        "correcta"  : "2"
                    },
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<img src='MI5E_B01_R11_05.png' width='70' height='80'><br>¿Cuál es su segmento opuesto?",
                        "valores": ['<img src="MI5E_B01_R11_06.png" width="70" height="80">', '<img src="MI5E_B01_R11_07.png" width="70" height="80">', '<img src="MI5E_B01_R11_08.png" width="70" height="80">'],
                        "correcta"  : "1"
                    },
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<img src='MI5E_B01_R11_09.png' width='70' height='80'><br>¿Cuál es su segmento opuesto?",
                        "valores": ['<img src="MI5E_B01_R11_10.png" width="70" height="80">', '<img src="MI5E_B01_R11_11.png" width="70" height="80">', '<img src="MI5E_B01_R11_12.png" width="70" height="80">'],
                        "correcta"  : "2"
                    },
                    {
                        "c03id_tipo_pregunta": "13",
                        "t11pregunta": "<img src='MI5E_B01_R11_13.png' width='70' height='80'><br>¿Cuál es su segmento opuesto?",
                        "valores": ['<img src="MI5E_B01_R11_14.png" width="70" height="80">', '<img src="MI5E_B01_R11_15.png" width="70" height="80">', '<img src="MI5E_B01_R11_16.png" width="70" height="80">'],
                        "correcta"  : "0"
                    },
                ],
                "pregunta": {
                    "c03id_tipo_pregunta": "13",
                    "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada pregunta.<\/p>",
                    "descripcion": "Preguntas",   
                    "variante": "editable",
                    "anchoColumnaPreguntas": 30,
                    "evaluable"  : true
                }        
            },
            //12
            {
                "respuestas": [
                    {
                        "t13respuesta": "<img src='MI5E_B01_R12_02.png' width='60' height='70'>",
                        "t17correcta": "1"
                    },
                    {
                        "t13respuesta": "<img src='MI5E_B01_R12_04.png' width='60' height='70'>",
                        "t17correcta": "2"
                    },
                    {
                        "t13respuesta": "<img src='MI5E_B01_R12_06.png' width='60' height='70'>",
                        "t17correcta": "3"
                    },
                    {
                        "t13respuesta": "<img src='MI5E_B01_R12_08.png' width='60' height='70'>",
                        "t17correcta": "4"
                    },
                    {
                        "t13respuesta": "<img src='MI5E_B01_R12_10.png' width='60' height='70'>",
                        "t17correcta": "5"
                    },
                    {
                        "t13respuesta": "<img src='MI5E_B01_R12_12.png' width='60' height='70'>",
                        "t17correcta": "6"
                    },
                    {
                        "t13respuesta": "<img src='MI5E_B01_R12_14.png' width='60' height='70'>",
                        "t17correcta": "7"
                    }
                ],
                "preguntas": [
                    {
                        "t11pregunta": "<img src='MI5E_B01_R12_01.png' width='60' height='70'>",
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R12_03.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R12_05.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R12_07.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R12_09.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R12_11.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R12_13.png' width='60' height='70'>"
                    }
                ],
                "pregunta": {
                    "t11pregunta": "<p>Relaciona las columnas según corresponda<\/p>",
                    "c03id_tipo_pregunta": "12",
                    "altoImagen": "200"
                }
            },
            //13
            {
                "respuestas": [
                    {
                        "t13respuesta": "15",
                        "t17correcta": "1"
                    },
                    {
                        "t13respuesta": "4",
                        "t17correcta": "2"
                    },
                    {
                        "t13respuesta": "32",
                        "t17correcta": "3"
                    },
                    {
                        "t13respuesta": "20",
                        "t17correcta": "4"
                    },
                    {
                        "t13respuesta": "35",
                        "t17correcta": "5"
                    },
                    {
                        "t13respuesta": "18",
                        "t17correcta": "6"
                    },
                    {
                        "t13respuesta": "12",
                        "t17correcta": "7"
                    }
                ],
                "preguntas": [
                    {
                        "t11pregunta": "<img src='MI5E_B01_R13_01.png' width='60' height='70'>",
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R13_02.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R13_03.png' width='50' height='60'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R13_04.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R13_05.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R13_06.png' width='60' height='70'>"
                    },
                    {
                        "t11pregunta": "<img src='MI5E_B01_R13_07.png' width='60' height='70'>"
                    }
                ],
                "pregunta": {
                    "t11pregunta": "<p>Relaciona las columnas según corresponda.<\/p>",
                    "c03id_tipo_pregunta": "12",
                    "altoImagen": "250"
                }
            }
  ]
  