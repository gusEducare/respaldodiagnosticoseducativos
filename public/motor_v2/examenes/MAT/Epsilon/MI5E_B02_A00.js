json=[
  // //1
   {
      "respuestas":[
         {
            "t13respuesta":     "1+<span class='fraction black'><span class='top'>4</span><span class='bottom'>6</span></span>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<span class='fraction black'><span class='top'>5</span><span class='bottom'>6</span></span>+<span class='fraction black'><span class='top'>4</span><span class='bottom'>6</span></span>",
            "t17correcta":"1",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<span class='fraction black'><span class='top'>4</span><span class='bottom'>6</span></span>+<span class='fraction black'><span class='top'>3</span><span class='bottom'>4</span></span>",
            "t17correcta":"0",
            "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "1+<span class='fraction black'><span class='top'>15</span><span class='bottom'>7</span></span>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "2+<span class='fraction black'><span class='top'>1</span><span class='bottom'>7</span></span>",
            "t17correcta":"0",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "3+<span class='fraction black'><span class='top'>4</span><span class='bottom'>7</span></span>",
            "t17correcta":"1",
            "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "8+<span class='fraction black'><span class='top'>2</span><span class='bottom'>5</span></span>",
            "t17correcta":"1",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "8+<span class='fraction black'><span class='top'>3</span><span class='bottom'>5</span></span>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "<span class='fraction black'><span class='top'>15</span><span class='bottom'>5</span></span>+<span class='fraction black'><span class='top'>20</span><span class='bottom'>8</span></span>",
            "t17correcta":"0",
            "numeroPregunta":"2"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"1",
         "t11pregunta":["Selecciona  la opción que resulta en la fracción mostrada.<br><br><span class='fraction black'><span class='top'>9</span><span class='bottom'>6</span></span>",
         "<span class='fraction black'><span class='top'>25</span><span class='bottom'>7</span></span>",
         "<span class='fraction black'><span class='top'>42</span><span class='bottom'>5</span></span>"],
         "preguntasMultiples": true
      }
   },
      //2
      {
          "respuestas": [
              {
                  "t13respuesta": "<span class='fraction'><span class='top'>1</span><span class='bottom'>4</span></span> de kilómetro",
                  "t17correcta": "1"
              },
              {
                  "t13respuesta": "<span class='fraction'><span class='top'>2</span><span class='bottom'>3</span></span> de día",
                  "t17correcta": "2"
              },
              {
                  "t13respuesta": "120 segundos",
                  "t17correcta": "3"
              },
              {
                  "t13respuesta": "75 centimetros",
                  "t17correcta": "4"
              },
              {
                  "t13respuesta": "840 centímetros",
                  "t17correcta": "5"
              },
              {
                  "t13respuesta": "2000 metros",
                  "t17correcta": "6"
              },
              {
                  "t13respuesta": "7 dias",
                  "t17correcta": "7"
              },
              {
                  "t13respuesta": "500 metros",
                  "t17correcta": "8"
              },
              {
                  "t13respuesta": "2 dias",
                  "t17correcta": "9"
              },
              {
                  "t13respuesta": "<span class='fraction'><span class='top'>19</span><span class='bottom'>30</span></span> de hora",
                  "t17correcta": "10"
              }
          ],
          "preguntas": [
              {
                  "t11pregunta": "250 metros"
              },
              {
                  "t11pregunta": "16 horas"
              },
              {
                  "t11pregunta": "2 minutos"
              },
              {
                  "t11pregunta": "<span class='fraction'><span class='top'>3</span><span class='bottom'>4</span></span> de metro"
              },
              {
                  "t11pregunta": "8.4 metros"
              },
              {
                  "t11pregunta": "2 kilómetros"
              },
              {
                  "t11pregunta": "1 semana"
              },
              {
                  "t11pregunta": "<span class='fraction'><span class='top'>1</span><span class='bottom'>2</span></span> kilómetro"
              },
              {
                  "t11pregunta": "<span class='fraction'><span class='top'>2</span><span class='bottom'>7</span></span> semana"
              },
              {
                  "t11pregunta": "38 minutos"
              }
          ],
          "pregunta": {
              "c03id_tipo_pregunta": "12",
              "t11pregunta": "Relaciona las medidas de la primera columna con su equivalente."
          }
      },
      //3
      {
          "respuestas":[
             {
                "t13respuesta":     "$31.75",
                "t17correcta":"0",
                "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "$31.74",
                "t17correcta":"1",
                "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "$32.74",
                "t17correcta":"0",
                "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "$3.37",
                "t17correcta":"0",
                "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "$3.35",
                "t17correcta":"0",
                "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "$3.36",
                "t17correcta":"1",
                "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "$18.26",
                "t17correcta":"1",
                "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "$18.32",
                "t17correcta":"0",
                "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "$17.85",
                "t17correcta":"0",
                "numeroPregunta":"2"
             }
          ],
          "pregunta":{
             "c03id_tipo_pregunta":"1",
             "t11pregunta":["Lee el problema y elige la respuesta correcta a cada pregunta.<br><br>Juan fue a la tienda con $50 a comprar lo siguiente:<br><br><ul><li>¼ kg de queso</li><li>½ lt de crema</li><li>2 aguacates</li><li>3  sobres de sopa.</li></ul><br><br>Al momento de pagar, el dueño de la tienda le dijo los precios de los productos:<br><br><ul><li>1 kg queso a $22.5</li><li>1 lt de crema a $14.75</li><li>6 aguacates a $20.21</li><li>Un sobre de sopa a $4</li></ul><br><br>             ¿Cuánto pagó Juan en la tienda por todo?",
             "¿Cuánto le costó cada aguacate a Juan?",
             "¿Cuánto tiene que dar de cambio a su mamá?"],
             "preguntasMultiples": true
          }
       },
       //4
       {
           "respuestas":[
              {
                 "t13respuesta":     "cuatro ángulos",
                 "t17correcta":"0",
                 "numeroPregunta":"0"
              },
              {
                 "t13respuesta":     "tres vértices",
                 "t17correcta":"1",
                 "numeroPregunta":"0"
              },
              {
                 "t13respuesta":     "tres lados",
                 "t17correcta":"1",
                 "numeroPregunta":"0"
              },
              {
                 "t13respuesta":     "seis alturas",
                 "t17correcta":"0",
                 "numeroPregunta":"0"
              },
              {
                 "t13respuesta":     "El segmento",
                 "t17correcta":"1",
                 "numeroPregunta":"1"
              },
              {
                 "t13respuesta":     "El vértice",
                 "t17correcta":"0",
                 "numeroPregunta":"1"
              },
              {
                 "t13respuesta":     "lado opuesto",
                 "t17correcta":"1",
                 "numeroPregunta":"1"
              },
              {
                 "t13respuesta":     "La altura",
                 "t17correcta":"0",
                 "numeroPregunta":"1"
              },
              {
                 "t13respuesta":     "Alturas",
                 "t17correcta":"1",
                 "numeroPregunta":"2"
              },
              {
                 "t13respuesta":     "Rectángulo",
                 "t17correcta":"0",
                 "numeroPregunta":"2"
              },
              {
                 "t13respuesta":     "Vértices",
                 "t17correcta":"0",
                 "numeroPregunta":"2"
              },
              {
                 "t13respuesta":     "Lados",
                 "t17correcta":"1",
                 "numeroPregunta":"2"
              }
           ],
           "pregunta":{
              "c03id_tipo_pregunta":"2",
              "t11pregunta":["Selecciona las 2 opciones que completen los enunciados.<br><br>Un triángulo tiene <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> y <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
              "La altura de un triángulo está dada por <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> más corto entre un vértice y su <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>.",
              "En un triángulo rectángulo, dos de las <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> coinciden con dos de los <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> del triángulo."],
              "preguntasMultiples": true
           }
        },
        //5
        {
            "respuestas": [
                {
                    "t17correcta": "12"
                },
                {
                    "t17correcta": "36"
                },
                {
                    "t17correcta": "x"
                }
            ],
            "preguntas": [
                {
                  "t11pregunta": "<img src='MI5E_B02_A05_01.png'><br>a=<span class='fraction black'><span class='top'>(Dxd)</span><span class='bottom'>2</span></span> <br>a = "
                },
                {
                    "t11pregunta": " cm<sup>2</sup><br><img src='MI5E_B02_A05_02.png'><br>a=bxh<br>a = "
                },
                {
                    "t11pregunta": " cm<sup>2</sup>"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "6",
                evaluable:true,
                "t11pregunta":"Calcula el área de las siguientes figuras, escribe el resultado en el lugar correspondiente.",
                "pintaUltimaCaja": false
            }
        },
        //6
        {
            "respuestas": [
                {
                    "t13respuesta": "<p>8<\/p>",
                    "t17correcta": "1"
                },
                {
                    "t13respuesta": "<p>64<\/p>",
                    "t17correcta": "2"
                },
                {
                    "t13respuesta": "<p>256<\/p>",
                    "t17correcta": "3"
                },
                {
                    "t13respuesta": "<p>2048<\/p>",
                    "t17correcta": "4"
                },
                {
                    "t13respuesta": "<p>3.5<\/p>",
                    "t17correcta": "5"
                },
                {
                    "t13respuesta": "<p>14<\/p>",
                    "t17correcta": "6"
                },
                {
                    "t13respuesta": "<p>112<\/p>",
                    "t17correcta": "7"
                },
                {
                    "t13respuesta": "<p>448<\/p>",
                    "t17correcta": "8"
                }
            ],
            "preguntas": [
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "<br><style>\n\
                                            .table img{height: 90px !important; width: auto !important; }\n\
                                        </style><table class='table' style='margin-top:-40px;'>\n\
                                        <tr>\n\
                                            <td>4</td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>16</td>\n\
                                            <td style='width:70px;'>32</td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>128</td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>512</td>\n\
                                            <td style='width:70px;'>1024</td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                        </tr><tr>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>7</td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>28</td>\n\
                                            <td>56</td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>224</td>\n\
                                            <td>"
                },
                {
                    "c03id_tipo_pregunta": "8",
                    "t11pregunta": "        </td>\n\
                                            <td>896</td>\n\
                                            <td>1792</td>\n\
                                            </tr></table>"
                }
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "Arrastra los números que hacen falta en la sucesión.",
               "contieneDistractores":true,
                "anchoRespuestas": 70,
                "soloTexto": true,
                "respuestasLargas": true,
                "pintaUltimaCaja": false,
                "ocultaPuntoFinal": true
            }
        }
]
