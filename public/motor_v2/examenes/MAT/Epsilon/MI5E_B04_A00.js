json=[
  // //1
  {
    "respuestas":[
       {
          "t13respuesta":     "IX",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "X",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "XI",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "592",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "492",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "692",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "VII",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "X",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "III",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "<img src='MI5E_B04_A01_05.png'>",
          "t17correcta":"1",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "<img src='MI5E_B04_A01_08.png'>",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "<img src='MI5E_B04_A01_07.png'>",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "8",
          "t17correcta":"0",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "9",
          "t17correcta":"1",
        "numeroPregunta":"4"
       },
       {
          "t13respuesta":     "7",
          "t17correcta":"0",
        "numeroPregunta":"4"
       }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta que equivale al número mostrado.<br>10",
                      "DXCII",
                      "3",
                      "15",
                      "<img src='MI5E_B04_A01_08.png'>"],
     "preguntasMultiples": true
    }
  },
  //2
  {
    "respuestas":[
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>6</span><span class='bottom'>9</span></span>",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>5</span><span class='bottom'>8</span></span>",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>7</span><span class='bottom'>4</span></span>",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>3</span><span class='bottom'>6</span></span>",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>7</span><span class='bottom'>18</span></span>",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>5</span><span class='bottom'>14</span></span>",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>18</span><span class='bottom'>7</span></span>",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>12</span><span class='bottom'>15</span></span>",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>3</span><span class='bottom'>6</span></span>",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>1</span><span class='bottom'>2</span></span>",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>8</span><span class='bottom'>4</span></span>",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "<span class='fraction black'><span class='top'>1</span><span class='bottom'>4</span></span>",
          "t17correcta":"0",
        "numeroPregunta":"2"
       }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"2",
       "t11pregunta":["Selecciona todas las respuestas correctas.<br>Selecciona las fracciones que al sumar, su resultado es <span class='fraction black'><span class='top'>7</span><span class='bottom'>6</span></span>",
                      "Selecciona las fracciones que al sumar, su resultado es <span class='fraction black'><span class='top'>118</span><span class='bottom'>35</span></span>",
                      "Selecciona las fracciones que al sumar su resultado es mayor a <span class='fraction black'><span class='top'>5</span><span class='bottom'>2</span></span>"],
     "preguntasMultiples": true
    }
  },
  //3
  {
      "respuestas": [
          {
              "t13respuesta": "<p>7 y <span class='fraction black'><span class='top'>1</span><span class='bottom'>2</span></span><\/p>",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "<p>12<\/p>",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "<p>12 y <span class='fraction black'><span class='top'>3</span><span class='bottom'>4</span></span><\/p>",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "<p>27 y <span class='fraction black'><span class='top'>1</span><span class='bottom'>2</span></span><\/p>",
              "t17correcta": "4"
          }
      ],
      "preguntas": [
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "<br><style>\n\
                                      .table img{height: 90px !important; width: auto !important; }\n\
                                  </style><table class='table' style='margin-top:-40px;'>\n\
                                  <tr>\n\
                                      <td style='width:70px'>Bolsas</td>\n\
                                      <td style='width:70px'>Cantidad en cada bolsa</td>\n\
                                      <td style='width:70px'>Cantidad en costal</td>\n\
                                  </tr>\n\
                                  <tr>\n\
                                      <td style='width:70px'>15</td>\n\
                                      <td style='width:70px'>½ kg de arroz</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        kilogramos de arroz</td>\n\
                                  </tr><tr>\n\
                                      <td>12</td>\n\
                                      <td>1 kg de frijol</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        kilogramos de frijol</td>\n\
                                  </tr><tr>\n\
                                      <td>17</td>\n\
                                      <td>750 gr de girasol</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        kilogramos de girasol</td>\n\
                                  </tr><tr>\n\
                                      <td>11</td>\n\
                                      <td>2500 gr de maíz</td>\n\
                                      <td>"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "        kilogramos de maíz</td>\n\
                                      </tr></table>"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "8",
          "t11pregunta": " Luis rapartió en cajas diferentes cantidades de semillas que tenía en costales. Arrastra y coloca qué cantidad de cada semilla es la que Luis tenía en cada costal.",
         "contieneDistractores":true,
          "anchoRespuestas": 70,
          "soloTexto": true,
          "respuestasLargas": true,
          "pintaUltimaCaja": false,
          "ocultaPuntoFinal": true
      }
  },
  //4
  {
      "respuestas": [
          {
              "t13respuesta": "<p>Una botella roja con lunares negros<\/p>",
              "t17correcta": "1"
          },
          {
              "t13respuesta": "<p>Una lámpara <\/p>",
              "t17correcta": "2"
          },
          {
              "t13respuesta": "<p>Un cuadro de manzanas<\/p>",
              "t17correcta": "3"
          }
          ,
          {
              "t13respuesta": "<p>Un teléfono<\/p>",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "<p>Un cuadro de frutas<\/p>",
              "t17correcta": "3"
          },
          {
              "t13respuesta": "<p>Una taza blanca<\/p>",
              "t17correcta": "3"
          }
      ],
      "preguntas": [
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "Observa la imagen y responde las preguntas.<br>¿Qué objeto está a lado de la tasa ? "
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "<br>¿Qué objeto está sobre el mueble con un corazón en la puerta?"
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta": "<br>¿Qué objeto está arriba de la lámpara? ",
             
          },
          {
              "c03id_tipo_pregunta": "8",
              "t11pregunta":"<br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<img src='MI5E_B04_A04_01.png'>"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "8",
          "t11pregunta": "Arrastra las palabras que completen el párrafo.",
         "contieneDistractores":true,
          "anchoRespuestas": 70,
          "soloTexto": true,
          "respuestasLargas": true,
          "pintaUltimaCaja": false,
          "ocultaPuntoFinal": true
      }
  },
  //5
  {
      "respuestas": [
          {
              "t17correcta": "22"
          },
          {
              "t17correcta": "16"
          },
          {
              "t17correcta": "30"
          },
          {
              "t17correcta": "x"
          }
      ],
      "preguntas": [
          {
              "t11pregunta": "Calcula el perímetro de las siguientes figuras. <br><img src='MI5E_B04_A05_01.png'><br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspP ="
          },
          {
              "t11pregunta": " cm &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspP = "
          },
          {
              "t11pregunta": " cm &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspP = "
          },
          {
              "t11pregunta": " cm"
          }
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "6",
          evaluable:true,
          "pintaUltimaCaja":false
      }
  },
  //6
  {
    "respuestas":[
       {
          "t13respuesta":     "17.4%",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "23.2%",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "12.8%",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "19.2%",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "18.1%",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "20.2%",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "10%",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "11.2%",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "8.4%",
          "t17correcta":"1",
        "numeroPregunta":"2"
       }
    ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta.<br><center><img src='MI5E_B04_A06_01.png'></center><br>¿Cuál es el porcentaje que representa la obesidad en niños?",
                      "<center><img src='MI5E_B04_A06_01.png'></center><br>¿Cuál es el porcentaje que representa el sobrepeso en niñas?",
                      "<center><img src='MI5E_B04_A06_01.png'></center><br>¿Cuál es la diferencia porcentual entre las niñas con obesidad y con sobrepeso?"],
     "preguntasMultiples": true
    }
  }
]
