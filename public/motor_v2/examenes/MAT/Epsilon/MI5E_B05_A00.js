json=[
  //1
  {  
    "respuestas":[  
        {
            "t13respuesta":     "<img src='MI5E_B05_A01_I02.png' weigth = '100' height = '100'>",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I03.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I04.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I05.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I07.png' weigth = '100' height = '100'>",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I08.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I09.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I10.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I17.png' weigth = '100' height = '100'>",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I18.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I19.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I12.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I12.png' weigth = '100' height = '100'>",
            "t17correcta":"1",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I13.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I14.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"3"
         },
         {
            "t13respuesta":     "<img src='MI5E_B05_A01_I15.png' weigth = '100' height = '100'>",
            "t17correcta":"0",
          "numeroPregunta":"3"
         }
    ],
    "pregunta":{  
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta.<br><br>¿Cuál es el resultado de <img src='MI5E_B05_A01_I01.png' weigth = '100' height = '100'>, expresado en el sistema de numeración maya.",
       "¿Cuál es el resultado de <img src='MI5E_B05_A01_I06.png' weigth = '100' height = '100'>, expresado en sistema decimal. ",
        "¿Cuál es el resultado de <img src='MI5E_B05_A01_I16.png' weigth = '100' height = '100'>, expresado en sistema de numeración maya.",
        "¿Cuál es el resultado de <img src='MI5E_B05_A01_I11.png' weigth = '100' height = '100'>, expresado en sistema decimal. "],
       "preguntasMultiples": true
    }
 },
    //2
    {
        "respuestas": [
            {
                "t17correcta": "24"
            },
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "16"
            },
            {
                "t17correcta": "45"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "Sofía compró una caja de galletas, al llegar a su casa, le compartió 2/6 del total de las galletas a su hermano, a su mamá le dio 2/12 y a su papá 1/6, la caja contiene 6 paquetes con 12 galletas cada uno.<br><br>¿Cuántas galletas le tocaron a Sofía? &nbsp"
            },
            {
                "t11pregunta": " galletas.<br>¿Cuántos paquetes de galletas le dio a su hermano? &nbsp"
            },
            {
                "t11pregunta": ".<br>Si su hermano se comió 8 galletas, ¿cuántas le quedaron? &nbsp"
            },
            {
                "t11pregunta": "galletas."
            },
            {
                "t11pregunta": ""
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Lee el siguiente problema y responde las preguntas.",
            evaluable:true,
            pintaUltimaCaja: false
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>22<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>7<\/p>",
                "t17correcta": "4"
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td style='width:150px'></td>\n\
                                        <td style='width:100px'></td>\n\
                                        <td style='width:100px'></td>\n\
                                        <td style='width:100px'></td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td style='width:100px'>12</td>\n\
                                        <td style='width:100px'>x</span></td>\n\
                                  <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td style='width:100px'>=</span></td>\n\
                                        <td style='width:100px'>48</span></td>\n\
                                    </tr><tr>\n\
                                        <td>11</td>\n\
                                        <td>x</td>\n\
                                        <td>2</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                    <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>x</td>\n\
                                        <td>8</td>\n\
                                        <td>=</td>\n\
                                        <td>40</td>\n\
                                    </tr><tr>\n\
                                        <td>2</td>\n\
                                        <td>x</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>=</td>\n\
                                        <td>14</td>\n\
                                        </tr><tr>\n\
                                        </td>\n\
                                    </tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la imagen según corresponda.",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },

    //4
    {  
        "respuestas":[  
            {
                "t13respuesta":     "6 cm",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "1.5 cm",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "3 cm",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "3 cm",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "1.5 cm",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "6 cm",
                "t17correcta":"0",
              "numeroPregunta":"1"
             }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Observa la imagen y selecciona la respuesta correcta.<br><br><center><img src='MI5E_B05_A04_I01.png' weigth = '200' height = '200'></center><br><br>¿Cuánto mide el diámetro del círculo?",
           "¿Cuánto mide el radio del círculo?"],
           "preguntasMultiples": true
        }
     },
     //5
     {
        "respuestas": [
            {
                "t13respuesta": "Celda",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "Filas y Columnas",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "Horizontal",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "Vertical",
                "t17correcta": "4"
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "Intersección entre filas y columnas."
            },
            {
                "t11pregunta": "¿De qué manera se pueden identificar las celdas de una cuadrícula?"
            },
            {
                "t11pregunta": "¿Cuál es la  dirección en que se forman las filas?"
            },
            {
                "t11pregunta": "¿Cuál es la dirección en que se forman las columnas?"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda."
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "<p>$ 200<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>$ 13.50<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>$ 400<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>$ 24.75<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>$ 31.50<\/p>",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'>\n\
                                    <tr>\n\
                                        <td style='width:150px'>Total de compras</td>\n\
                                        <td style='width:100px'>Dinero electrónico</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                  <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td style='width:100px'>$ 9</span></td>\n\
                                    </tr><tr>\n\
                                        <td>$ 300</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                    <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>$ 18</td>\n\
                                    </tr><tr>\n\
                                        <td>$ 550</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td>$ 700</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        </tr><tr>\n\
                                        </td>\n\
                                    </tr></table>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra la imagen según corresponda.<br><br>En una tienda, por cada $200 de compra te regalan $9 en dinero electrónico. ",
           "contieneDistractores":true,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            "ocultaPuntoFinal": true
        }
    },
    //7
    {  
        "respuestas":[  
            {
                "t13respuesta":     "Brenda",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Toño",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Luis",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Valentina",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "8.6",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "8.5",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "8.3",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "8.2",
                "t17correcta":"0",
              "numeroPregunta":"1"
             }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Lee el problema y selecciona la respuesta correcta.<br><br>Contando solamente los 4 bimestres, ¿quién tiene actualmente un promedio de 8.5?",
           "Si Luis obtiene en el 5° bimestre un promedio de 10, ¿cuál será su promedio total?"],
           "t11instruccion": "En la escuela de Brenda abrieron una convocatoria para ganarse una beca para el siguiente año escolar, quien tuviera un promedio mínimo de 8.5 ganaría la beca. Brenda fue a contarle a sus amigos, Valentina, Toño, Luis y Brenda fueron a la dirección a pedir sus calificaciones para entrar a la convocatoria. A continuación se muestran las calificaciones de los cuatro niños en los cuatro bimestres.<br><br><center><img src='MI5E_B05_A07.png' ></center>",
           "preguntasMultiples": true
        }
     },
];
