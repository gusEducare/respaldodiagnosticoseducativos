json=[ 
   //reactivo 1
   {
       "respuestas": [
           {
               "t13respuesta": "<p>Multiplicar la posición<br> por dos y al resultado sumarle 4.<\/p>",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "<p>a<sub>n</sub>=2n+4<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p>Multiplicar la posición<br> por cuatro y al resultado sumarle 9.<\/p>",
               "t17correcta": "3"
           },
            {
               "t13respuesta": "<p>a<sub>n</sub>=4n+9<\/p>",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "<p>Multiplicar la posición por tres<br> y al resultado sumarle 6.<\/p>",
               "t17correcta": "5"
           },
           {
               "t13respuesta": "<p>a<sub>n</sub>=3n+6<\/p>",
               "t17correcta": "6"
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",


               "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; }\n\
                                   </style><table class='table' style='margin-top:-40px;'><td>Tema.</td><td>Descripción.</td><td>Expresión algebraica.</td>\n\
                                   <tr>\n\
                                       <td>6, 8, 10, 12, ...\n\
                                        <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                       <td>\n\
                                       "
           },
           
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\
                                       <td>13, 17, 21, 25, ...</td>\n\
                                       <td>"
                                       
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                       <td>\
                                       "
           },

            {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                         </tr><tr>\n\
                                       <td>9, 12, 15, 18, ...</td>\n\
                                       <td>"
           },{
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                       <td>\n\
                                       "
           }, 
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                       </tr></table>"
             },
           
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "t11pregunta": "Relaciona y arrastra cada sucesión con la expresión que la describe y la expresión algebraica que le corresponde. ",
          "contieneDistractores":false,
           "anchoRespuestas": 70,
           "soloTexto": true,
           "respuestasLargas": true,
           "pintaUltimaCaja": false,
           
           "ocultaPuntoFinal": true
       }
   },
   
 // reactivo 2
    { 
        "respuestas": [ 
            { 
                "t17correcta": "5/3" 
            }, 
            { 
                "t17correcta": "9/14" 
            }, 
            { 
                "t17correcta": "97/14" 
            } 
             
        ], 
        "preguntas": [ 
            { 
                "t11pregunta": "<br> &nbsp&nbsp&nbsp&nbsp&nbsp2y+7=5y+2;        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp        y=" 
            }, 
            { 
                "t11pregunta": "<br> &nbsp&nbsp&nbsp&nbsp&nbsp10a-15=-18a+3;    &nbsp  &nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp  a=" 
            }, 
            { 
                "t11pregunta": "<br> &nbsp&nbsp&nbsp&nbsp&nbsp15h-18h+50=11h-47;  &nbsp&nbsp&nbsp&nbsp&nbsp    h=" 
            }, 
            { 
                "t11pregunta": "" 
            } 
        ], 
        "pregunta": { 
            "c03id_tipo_pregunta": "6", 
            "t11pregunta":"Despeja la incógnita en cada ecuación y escribe el valor que le corresponde, expresa el resultado en fracción. ", 
            "pintaUltimaCaja": false, 
            evaluable:true 
        } 
    } 
  , 
    //reactivo 3
    {
    "respuestas":[
       {
          "t13respuesta":     "inscrito",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "central",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "externo",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       
       {
          "t13respuesta":     "recto",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "90°",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "66°",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "74°",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       
       {
          "t13respuesta":     "100°",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "central",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "externo",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "Inscrito",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },{
          "t13respuesta":     "recto",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
          ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       
       "t11pregunta":[" Selecciona la opción correcta a cada pregunta.<br> <center> <img src='MI8E_B04_A03_01.png'></center><br>¿Qué tipo de ángulo tiene el círculo?" ,
                      "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <img src='MI8E_B04_A03_01.png'><br>Si el ángulo central es la cuarta parte de toda la circunferencia, ¿cuánto mide el ángulo?",
                      "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  <img src='MI8E_B04_A03_02.png'><br>¿Qué tipo de ángulo tiene el círculo?"],
     "preguntasMultiples": true
    }
  }, 
    ////// reactivo 4
    
    
   
{
       "respuestas": [
           {
               "t13respuesta": "<p>20,10<\/p>",
               "t17correcta": "1",
               
               
           },
           {
               "t13respuesta": "<p>43,12<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p>27,8<\/p>",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "<p>29,6<\/p>",
               "t17correcta": "4"
           }
           
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; }\n\
                                   </style><table class='table' style='margin-top:-40px;'><td>Distancia</td><td>Tiempo</td>\n\
                                   <tr><td>Lunes</td><td>35,8</td>\n\ </tr><tr>\n\
                               <td>martes</td>\n\
                                       <td>\n\ "
           },
           
          

            {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\<td>miercoles</td>\n\<td>"
           }, {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\<td>jueves</td>\n\<td>"
           }, {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\<td>viernes</td>\n\<td>"
              } ,{
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "      </td>\n\
                                   </tr></table>"
           }
           
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "t11pregunta": "Arrastra los elementos y completa la tabla.<br><br>Pedro entrena para participar en una carrera de 21 km. En el siguiente plano cartesiano registró el tiempo y la distancia que recorrió cada día en una semana de preparación.<br>Considera la relación (tiempo, distancia) y coloca la coordenada que corresponde a cada día de la semana, observa el ejemplo. <br>La coordenada que corresponde al lunes muestra que Luis entrenó 35 minutos y recorrió ocho kilómetros.",
           "t11instruccion":"<img src='MI8E_B04_A04_01.png'>",
          "contieneDistractores":false,
           "anchoRespuestas": 70,
           "soloTexto": true,
           "respuestasLargas": true,
           "pintaUltimaCaja": false,
           
           "ocultaPuntoFinal": true
       }
   },

    //reactivo 5
     {
    "respuestas":[
       {
          "t13respuesta":     "90 L",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "89 L",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "98 L",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       
       {
          "t13respuesta":     "9 L",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "250 L",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "120 L",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "0 L",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       
       {
          "t13respuesta":     "339 L",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "y=89x",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "y=339x +250",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "y=89x+250",
          "t17correcta":"1",
        "numeroPregunta":"2"
       },{
          "t13respuesta":     "y=250x-89",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       
       {
          "t13respuesta":     "3200 L",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "4533 L",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
       {
          "t13respuesta":     "3187 L",
          "t17correcta":"1",
        "numeroPregunta":"3"
       },{
          "t13respuesta":     "4580 L",
          "t17correcta":"0",
        "numeroPregunta":"3"
       },
          ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Selecciona la respuesta correcta. <br><br>Una cisterna comenzó a llenarse, pero ya contenía 250 L de agua. La siguiente tabla muestra el llenado de la cisterna.<br><img src='MI8E_B04_A05_01.png'><br>¿Cuántos litros caen por minuto? " ,
                     "Una cisterna comenzó a llenarse, pero ya contenía 250 L de agua. La siguiente tabla muestra el llenado de la cisterna. <br><img src='MI8E_B04_A05_01.png'><br>¿Qué cantidad de agua había en la cisterna cuando comenzó a llenarse?",
                      "Una cisterna comenzó a llenarse, pero ya contenía 250 L de agua. La siguiente tabla muestra el llenado de la cisterna.<br><img src='MI8E_B04_A05_01.png'><br>¿Qué expresión algebraica representa el llenado de la cisterna?",
                      "Una cisterna comenzó a llenarse, pero ya contenía 250 L de agua. La siguiente tabla muestra el llenado de la cisterna.<br><img src='MI8E_B04_A05_01.png'><br>¿Cuánta agua habrá en la cisterna después de 33 minutos de que comenzó a llenarse?"],


     "preguntasMultiples": true
    }
  },
    //reactivo 6 
     {
    "respuestas":[
       {
          "t13respuesta":     "6.4",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "7.5",
          "t17correcta":"1",
        "numeroPregunta":"0"
       },
       {
          "t13respuesta":     "8.1",
          "t17correcta":"0",
        "numeroPregunta":"0"
       },
       
       {
          "t13respuesta":     "7.7",
          "t17correcta":"1",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "8.6",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       {
          "t13respuesta":     "9.8",
          "t17correcta":"0",
        "numeroPregunta":"1"
       },
       
       {
          "t13respuesta":     "8.6 y 8.3",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "8.2 y 8.4",
          "t17correcta":"0",
        "numeroPregunta":"2"
       },
       {
          "t13respuesta":     "8.5 y 8.2",
          "t17correcta":"1",
        "numeroPregunta":"2"
       }
       
       
          ],
    "pregunta":{
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Analiza la información y calcula el promedio de cada estudiante. La siguiente tabla muestra las calificaciones obtenidas por dos estudiantes en cada uno de los rubros que el maestro consideró para dar la calificación del bimestre.<br><center><img src='MI8E_B04_A06_01.png'></center><br>Considerando que los cuatro rubros tienen el mismo peso, ¿cuál es el promedio de Patricia?" ,
                     "<center><img src='MI8E_B04_A06_01.png'></center><br>¿Cuál es el promedio de José?",
                      "<center><img src='MI8E_B04_A06_01.png'></center><br>¿Cuál sería el promedio de ambos si en el rubro de tareas obtuvieran un 10?"],
      "preguntasMultiples": true
    }
  },
  ];                    