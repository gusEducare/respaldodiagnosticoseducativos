json=[
{
   //reactivo 1 relacionar columnas
        "respuestas": [
            {
                "t13respuesta": "20x+3",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "30xy+16y",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "1750yz + 80x<sup>7</sup>- 20y<sup>4</sup>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "4x<sup>2</sup>+5",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "500ax + 300 ay",
                "t17correcta": "5"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<fraction><span>100x<sup>2</sup> + 15x</span><span>5x</span></fraction>="
            },
            {
                "t11pregunta": "(15x + 8)(2y)="
            },
            {
                "t11pregunta": "(175yz  + 8x<sup>7</sup>- 2y<sup>4</sup>)(10)="
            },
            {
                "t11pregunta": "<fraction><span>12x<sup>3</sup>+15x</span><span>3x</span></fraction>="
            },
            {
                "t11pregunta": "(250x + 150 y)(2a)="
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona cada operación con su polinomio resultante."
        }
    },
    //reactivo 2 opcion multiple
    {  
      "respuestas":[
         {  
            "t13respuesta": "3a<sup>2</sup>-2a-16",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "3a<sup>2</sup>+6a-160",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "3a<sup>2</sup>-2a-4",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "3a<sup>2</sup>-4a-8",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },{  
            "t13respuesta": "2y<sup>4</sup>+4xy<sup>3</sup>+9y<sup>8</sup>",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "2y<sup>4</sup>+y<sup>4</sup>+3(3y<sup>8</sup>)",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "2y<sup>4</sup>+3(3y<sup>8</sup>)",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }, 
         
         {  
            "t13respuesta": "2y<sup>4</sup>+8xy<sup>3</sup>+9y<sup>8</sup>",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
          {  
            "t13respuesta": "16 a<sup>2</sup>+50a+6",
            "t17correcta": "1",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "16a<sup>2</sup>+50a+8",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "16 a<sup>2</sup>+2a+58a+6",
            "t17correcta": "0",
            "numeroPregunta":"2"
         }, 
         
         {  
            "t13respuesta": "-16a<sup>2</sup>+48a-6",
            "t17correcta": "0",
            "numeroPregunta":"2"
         },
         {  
            "t13respuesta": "216x+12xy-6y-108",
            "t17correcta": "1",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "216x +108-6y",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "216x +12xy+108-6y ",
            "t17correcta": "0",
            "numeroPregunta":"3"
         }, 
         
         {  
            "t13respuesta": "216x +12xy+10",
            "t17correcta": "0",
            "numeroPregunta":"3"
         },
         {  
            "t13respuesta": "16y<sup>4</sup>+5xy<sup>3</sup>",
            "t17correcta": "1",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "16y<sup>3</sup>+5x",
            "t17correcta": "0",
            "numeroPregunta":"4"
         },
         {  
            "t13respuesta": "16y<sup>8</sup>+5xy<sup>4</sup> ",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }, 
         
         {  
            "t13respuesta": "16y<sup>8</sup>+5xy<sup>3</sup>",
            "t17correcta": "0",
            "numeroPregunta":"4"
         }
        
      ],
      "pregunta":{  
         
         "c03id_tipo_pregunta":"1",
         
         "t11pregunta": ["Selecciona la respuesta correcta.<br><br>(a+2)(3a-8)=","y<sup>3</sup>(2y+4x+9y<sup>5</sup>)=","(2a+6)(8a+1)=","(12x-6)(18+y)=","y<sup>3</sup>(16y+5x)="],

         "preguntasMultiples": true,

         "columnas":1
         
      }
   },

   //reactivo 3  arrastra horizontal una columna
   {
        "respuestas": [
            {
                "t13respuesta": "<p>540<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>180<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>900<\/p>",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "<p>1260<\/p>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",


                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'><td></td><td>Nombre del polígono</td><td></td><td>Suma de los ángulos internos del polígono</td>\n\
                                    <tr>\n\
                                        <td></td>\n\
                                        <td>Pentágono</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            },
            
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>Triángulo</td>\n\
                                        <td>=</td>\n\
                                        <td>\n\
                                        "
            },

             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>Heptágono</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            }, {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>Eneágono</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            }, {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td>"
            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra cada elemento donde corresponda.",
           "contieneDistractores":false,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            
            "ocultaPuntoFinal": true
        }
    },

   
    {
    //reactivo 4 relacionar columnas
    "respuestas": [
            {
                "t13respuesta": "625 cm<sup>3</sup>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "6,250 l",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "62,500 ml",
                "t17correcta": "3"
            },
            {
                "t13respuesta": "625,000 dm<sup>3</sup>",
                "t17correcta": "4"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "625 ml"
            },
            {
                "t11pregunta": "6.25 m<sup>3</sup>"
            },
            {
                "t11pregunta": "62.5 l"
            },
            {
                "t11pregunta": "625 m<sup>3</sup>"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta":"Relaciona las columnas según corresponda."
        }
    },
    //reactivo 5 opcion multiple
    {  
      "respuestas":[
         {  
            "t13respuesta": "y = <fraction class='black'><span>115</span><span>25</span></fraction>x",
            "t17correcta": "1",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "y = <fraction class='black'><span>25</span><span>115</span></fraction>x",
            "t17correcta": "0",
            "numeroPregunta":"0"
         },
         {  
            "t13respuesta": "y=115x+25",
            "t17correcta": "0",
            "numeroPregunta":"0"
         }, 
         {  
            "t13respuesta": "y=40x+600",
            "t17correcta": "1",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "y=600x+40",
            "t17correcta": "0",
            "numeroPregunta":"1"
         },
         {  
            "t13respuesta": "y = <fraction class='black'><span>40</span><span>600</span></fraction>y",
            "t17correcta": "0",
            "numeroPregunta":"1"
         }
        
      ],
      "pregunta":{  
         
         "c03id_tipo_pregunta":"1",
         
         "t11pregunta": ["Selecciona la respuesta correcta.  <br>Elige la expresión algebraica que representa la situación planteada.<br><br>Una cisterna de 115 litros de capacidad se llena en 25 minutos con un flujo de agua constante.\
          ¿Cuánta agua cae al tinaco por minuto?","Roberto abrió una cuenta en el banco con $600 y cada mes deposita $40."],

         "preguntasMultiples": true,

         "columnas":1
         
      }
   },
   //reactivo 6 parrastra corta dos opciones
   {
        "respuestas": [
            {
                "t13respuesta": "<p>2.3 - 2.7<\/p>",
                "t17correcta": "1"
            },
            {
                "t13respuesta": "<p>2<\/p>",
                "t17correcta": "2"
            },
            {
                "t13respuesta": "<p>2.8 - 3.2<\/p>",
                "t17correcta": "3"
            },
             {
                "t13respuesta": "<p>5<\/p>",
                "t17correcta": "4"
            },
            {
                "t13respuesta": "<p>3.3 - 3.7<\/p>",
                "t17correcta": "5"
            },
            {
                "t13respuesta": "<p>3<\/p>",
                "t17correcta": "6"
            },
             {
                "t13respuesta": "<p>3.8 - 4.2<\/p>",
                "t17correcta": "7"
            },
            {
                "t13respuesta": "<p>4<\/p>",
                "t17correcta": "8"
            },
            {
                "t13respuesta": "<p>4.3 - 4.7<\/p>",
                "t17correcta": "9"
            },
            {
                "t13respuesta": "<p>1<\/p>",
                "t17correcta": "10"
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "8",


                "t11pregunta": "<br><style>\n\
                                        .table img{height: 90px !important; width: auto !important; }\n\
                                    </style><table class='table' style='margin-top:-40px;'><td></td><td>Clases</td><td></td><td>Límites de clase</td><td>Frecuencia</td>\n\
                                    <tr>\n\
                                        <td></td>\n\
                                        <td>1</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },
            
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>2</td>\n\
                                        <td>=</td>\n\
                                        <td>\n\
                                        "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },

             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>3</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            },{
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            }, 
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>4</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td>5</td>\n\
                                        <td>=</td>\n\
                                        <td>"
            },
            {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                        <td>\n\
                                        "
            },
             {
                "c03id_tipo_pregunta": "8",
                "t11pregunta": "        </td>\n\
                                    </tr><tr>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td></td>\n\
                                        <td>"

            }
            
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "8",
            "t11pregunta": "Arrastra cada elemento donde corresponda.<br>Los siguientes valores representan el registro acerca del peso de los 15 niños que nacieron durante la última semana en un hospital.<br>\
2.5 , 3.2 , 4.2 , 2.3 , 4.2 , 3.9 , 3.7 , 3.1 , 2.9 , 3.2 , 4.1 , 2.8 , 4.5 , 3.6 , 3.3<br>Rellena correctamente la tabla.\
",
           "contieneDistractores":false,
            "anchoRespuestas": 70,
            "soloTexto": true,
            "respuestasLargas": true,
            "pintaUltimaCaja": false,
            
            "ocultaPuntoFinal": true
        }
    },

   
    //reactivo 7 preguntas abiertas
    {
        "respuestas": [
            {
                "t17correcta": "25.875"
            },
            {
                "t17correcta": "26"
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "<br>La media de temperatura es &nbsp"
            },
            {
            
                "t11pregunta": " °C.<br>La mediana de las temperaturas es  &nbsp"
            },
            {
            
                "t11pregunta": " °C."
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Escribe la respuesta correcta. Usa tres cifras decimales.\
            <br><br>Las temperaturas promedio de seis días consecutivos en una ciudad del país fueron las siguientes:  26° C, 24° C, 28° C, 26° C, 25° C, 28° C, 24° C, 26° C. <br>",
            evaluable:true,
            "pintaUltimaCaja": false,
        }
    }


];