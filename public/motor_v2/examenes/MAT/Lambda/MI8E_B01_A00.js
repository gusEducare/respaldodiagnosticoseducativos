json = [
    {
        "respuestas": [
            {
                "t13respuesta": "-72",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "+4",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "-1710",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "-7",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "+1053",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "+585",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "-1292",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "(+6)&#215(-12)="
            },
            {
                "t11pregunta": "(-64)&#247(-16)="
            },
            {
                "t11pregunta": "(-38)&#215(+45)="
            },
            {
                "t11pregunta": "(-98)&#247(+14)="
            },
            {
                "t11pregunta": "(-39)&#215(-27)="
            },
            {
                "t11pregunta": "(+45)&#247(+13)="
            },
            {
                "t11pregunta": "(+68)&#215(-19)="
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },
    //2
    {
        "respuestas": [
            {
                "t17correcta": "3"
            },
            {
                "t17correcta": "2"
            },
            {
                "t17correcta": "7"
            },
            {
                "t17correcta": "16384"
            },
            {
                "t17correcta": ""
            }
        ],
        "preguntas": [
            {
                "t11pregunta": "En la expresión 6<sup>3</sup>, el exponente es el número "
            },
            {
                "t11pregunta": " <br> 2 x 2 x 2 x 2 x 2 es la expresión de "
            },
            {
                "t11pregunta": "<sup>5</sup> <br> En la expresión 7<sup>2</sup> la base es el número "
            },
            {
                "t11pregunta": " <br> 4<sup>5</sup> x 4<sup>2</sup>= "
            },
            {
                "t11pregunta": " <br>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "6",
            "t11pregunta": "Resuelve las siguientes preguntas.",
            "t11instruccion": "",
            evaluable: true,
            pintaUltimaCaja: true
        }
    },
    //3
    {
        "respuestas": [
            {
                "t13respuesta": "MI8E_B01_R03_01.png",
                "t17correcta": "0",
                "columna": "0"
            },
            {
                "t13respuesta": "60°",
                "t17correcta": "1",
                "columna": "1"
            },
            {
                "t13respuesta": "MI8E_B01_R03_02.png",
                "t17correcta": "2",
                "columna": "1"
            },
            {
                "t13respuesta": "35°",
                "t17correcta": "3",
                "columna": "0"
            }
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "5",
            "t11pregunta": "<p>Arrastra la imagen correspondiente a cada pregunta.<br><\/p>",
            "tipo": "ordenar",
            "imagen": true,
            "url": "MI8E_B01_R03_00.png",
            "respuestaImagen": true,
            "tamanyoReal": true,
            "bloques": false,
            "borde": false,
            "evaluable": true

        },
        "contenedores": [
            { "Contenedor": ["", "48,425", "cuadrado", "110, 105", ".", "transparent"] },
            { "Contenedor": ["", "156,425", "cuadrado", "110, 105", ".", "transparent"] },
            { "Contenedor": ["", "264,425", "cuadrado", "110, 105", ".", "transparent"] },
            { "Contenedor": ["", "370,425", "cuadrado", "110, 105", ".", "transparent"] },

        ]
    },
    //4
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La unicidad del triángulo es la propiedad del triángulo que permite trazar un solo triángulo a partir de cierta información.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "La suma de dos lados de un triángulo siempre será mayor a la longitud del tercero..",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si conoces la medida de los 3 ángulos y del lado comprendido entre ellos, puedes trazar un único triángulo.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Cuando las circunferencias tienen un radio con una medida igual a la del segmento dado, se puede construir un triángulo equilátero.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Para formar un triángulo con un compás, es necesario que las circunferencias se crucen entre sí.",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un dato que permite trazar un único triángulo, es el de la medida de sus tres lados.",
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Elige falso (F) o verdadero (V) según corresponda.",
            "t11instruccion": "",
            "descripcion": "Pregunta",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //5
    {
        "respuestas": [
            //tabla 1
            {
                "t17correcta": "16"
            },
            {
                "t17correcta": "101"
            },
            {
                "t17correcta": "268"
            },
            {
                "t17correcta": "24"
            },
            {
                "t17correcta": "560"
            },

            //tabla 2

        ],
        "preguntas": [
            {
                "t11pregunta": "<style>\n\ .table img{height: 150px !important; width: auto !important; }\n\ </style><br><table class='table' style= 'margin-top:-20px;'>\n\
        <col width='300'>\n\
        <col width='300'>\n\
        <tr>\n <td>Preguntas</td>\n <td>Imagen\n\ </td>\n\ </tr><tr>\n <td>Si el triángulo de la figura 1 tiene una altura de 2, ¿cuánto es el área total de la figura compuesta?"
            },
            {
                "t11pregunta": "cm</td>\n <td><center><img src='MI8E_B01_R05_02.png'></center></td></tr><tr><td>¿Cuál es el área total de la figura 2? "
            },
            {
                "t11pregunta": "cm<sup>2</sup></td>\n <td><center><img src='MI8E_B01_R05_03.png'></center></td></tr><td>¿Cuál es el área de la figura 3? "
            },
            {
                "t11pregunta": "cm<sup>2</sup></td>\n <td><center><img src='MI8E_B01_R05_04.png'></center></td></tr><td>¿Cuánto mide el área de la figura compuesta número 4?"
            },
            {
                "t11pregunta": "cm<sup>2</sup></td>\n <td><center><img src='MI8E_B01_R05_01.png'></center></td></tr><td>¿Cuánto es el área total de la última imagen? "
            },
            {
                "t11pregunta": "cm<sup>2</sup></td>\n <td><center><img src='MI8E_B01_R05_05.png'></center></td></tr></table>"
            },


        ],
        "pregunta": {
            "t11pregunta": "Resuelve los siguientes problemas.",
            "c03id_tipo_pregunta": "6",
            "pintaUltimaCaja": false,
            evaluable: true
        }
    },
    //6
    {
        "respuestas": [
            {
                "t13respuesta": "Falso",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Verdadero",
                "t17correcta": "1",
            },
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A $500 le descontaron 25%, por lo que al final el costo fue de $100",
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "En una organización se echó a perder 35% de los productos que tenían. Si tenían 660 productos, significa que se echaron a perder 231 productos.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un juguete tiene el costo de $698. Si se le descontó 65% el precio final es de $244.30.",
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Una chamarra cuesta $1354 y se ha ofertado con 55% de descuento, en total se pagará $709.3 por la chamarra.",
                "correcta": "0",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Observa la siguiente matriz y marca la opción que corresponda a cada pregunta.",
            "t11instruccion": "",
            "descripcion": "Enunciado",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //7
    {
        "respuestas": [
            {
                "t13respuesta": "Opción 1",
                "t17correcta": "0",
            },
            {
                "t13respuesta": "Opción 2",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "Opción 3",
                "t17correcta": "2",
            }
        ],
        "preguntas": [
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Si se invierten $2000 con una tasa de interés compuesto de 3% anual. ¿Cuánto dinero se tendrá en el primer año?",
                "valores": ['$3212.08', '$2312.08', '$2321.08'],
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A María le dieron un préstamo de $1000 a pagar en 6 meses con interés simple de 7% mensual. ¿Cuánto es la cantidad final del periodo al último mes?",
                "valores": ['$4120', '$14,200', '$1420'],
                "correcta": "2",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Roberto está pagando un préstamo por 14000 en 9 meses con un interés simple de 6%. ¿Cuál es la cantidad final a pagar del 5° mes?",
                "valores": ['$18,200', '$17,360', '$19,040'],
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A un préstamo de 1000 se le aplica un interés compuesto de 10%. ¿Cuánto debe pagar el primer mes?",
                "valores": ['$1,000', '$1,100', '$10,000'],
                "correcta": "1",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "Un depósito de 5000000 tiene una tasa de interés de 1.5%. Al pasar 4 años, ¿cuánto dinero tendrá el depósito?",
                "valores": ['$10,217,391', '$1,021,391', '$101,391'],
                "correcta": "0",
            },
            {
                "c03id_tipo_pregunta": "13",
                "t11pregunta": "A un préstamo de $3456 que se realizó a 10 meses sin intereses se le agrega una tasa de interés mensual de 2%. ¿Cuánto es lo que se le agrega mes con mes?",
                "valores": ['$69.02', '$69.12', '$69'],
                "correcta": "1",
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "13",
            "t11pregunta": "Lee cada situación y elige la respuesta correcta a cada pregunta.",
            "t11instruccion": "",
            "descripcion": "Pregunta",
            "variante": "editable",
            "anchoColumnaPreguntas": 30,
            "evaluable": true
        }
    },
    //8
    {
        "respuestas": [
            {
                "t13respuesta": "Que aparezca el 1, 2, 3 y 6",
                "t17correcta": "1",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Que aparezca el 3 y 6",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "Que aparezca el 2, 3 y 6",
                "t17correcta": "0",
                "numeroPregunta": "0"
            },
            {
                "t13respuesta": "experimento aleatorio.",
                "t17correcta": "1",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "juego de azar.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "experimento probable.",
                "t17correcta": "0",
                "numeroPregunta": "1"
            },
            {
                "t13respuesta": "Que al sumar el resultado sea un número non.",
                "t17correcta": "1",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Que al sumar el resultado sea un número par.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "Que al sumar el resultado sea un 2.",
                "t17correcta": "0",
                "numeroPregunta": "2"
            },
            {
                "t13respuesta": "E= {(águila, águila), (águila, sol), (sol, sol), (sol, águila)}",
                "t17correcta": "1",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "Ex= {(águila, águila), (águila, sol), (sol, sol), (sol, águila)}",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "E= {(águila, águila), (águila, sol), (sol, sol)}",
                "t17correcta": "0",
                "numeroPregunta": "3"
            },
            {
                "t13respuesta": "9 elementos",
                "t17correcta": "1",
                "numeroPregunta": "4"
            },
            {
                "t13respuesta": "3 elementos",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "18 elementos",
                "t17correcta": "0",
                "numeroPregunta": "4"
            }, {
                "t13respuesta": "Es menos probable que se saque una manzana que una pera.",
                "t17correcta": "1",
                "numeroPregunta": "5"
            },
            {
                "t13respuesta": "Es más probable que se saque una manzana que una pera.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            }, {
                "t13respuesta": "Hay la misma probabilidad de sacar tanto una manzana como una pera.",
                "t17correcta": "0",
                "numeroPregunta": "5"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "1",
            "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Qué es más probable que suceda al lanzar un dado? ¿Que caiga un múltiplo de 3 o un número divisor de 6?",
                "Un evento que sucede al azar se conoce como:",
                "Se realizó un experimento aleatorio donde se numeraban 6 papeles, se tomaban 2 al azar y se sumaban los números. ¿Qué es más probable? ",
                "Si se quiere realizar un espacio muestral con las caras de dos monedas, ¿cuál es la manera correcta de representarla? ",
                "¿Cuántos elementos hay en el siguiente espacio muestral? E= {(R,R), (R,B), (R,A), (B,R), (B,B), (B,A), (A,R), (A,B), (A,A)} ",
                "En una canasta hay 2 manzanas y 5 peras. Si sacamos una fruta sin mirar, ¿qué probabilidad es correcta?",],
            "contieneDistractores": true,
            "preguntasMultiples": true,
        }
    },
    //9
    {
        "respuestas": [
            {
                "t13respuesta": "Si los valores de la lista son cercanos entre sí.",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "13 + 21 + 42 + 23 + 13 + 43 + 23 + 42= 220/8 = 27.5",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "Si los valores de la lista son muy dispersos.",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "13, 13, 21, 23, 23, 42, 42, 43 = 23 + 23 = 46/2 = 23",
                "t17correcta": "4",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "¿Cuándo es necesario obtener la media?"
            },
            {
                "t11pregunta": "¿Cuál es el procedimiento para obtener la media en los siguientes datos? 13, 21, 42, 23, 13, 43, 23, 42"
            },
            {
                "t11pregunta": "¿Cuándo es necesario obtener la mediana?"
            },
            {
                "t11pregunta": "¿Cuál es el procedimiento para obtener la mediana en los siguientes datos? 13, 21, 42, 23, 13, 43, 23, 42"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px"
        }
    },

    // reactivo 10
    {
      "respuestas": [
          {
              "t13respuesta": "-5x<sup>2</sup>",
              "t17correcta": "1",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "3x+4",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "4x<sup>2</sup>-2y",
              "t17correcta": "0",
              "numeroPregunta": "0"
          },
          {
              "t13respuesta": "-5xy",
              "t17correcta": "1",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "2x+5y",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "5x<sup>3</sup>-3y",
              "t17correcta": "0",
              "numeroPregunta": "1"
          },
          {
              "t13respuesta": "6&divide;x",
              "t17correcta": "1",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "x<sup>3</sup>y&divide;5",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "<fraction class='black'><span>x</span><span>y</span></fraction>+3",
              "t17correcta": "0",
              "numeroPregunta": "2"
          },
          {
              "t13respuesta": "x<sup>3</sup>y",
              "t17correcta": "1",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "6x+12y",
              "t17correcta": "0",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "3x+(-y)",
              "t17correcta": "0",
              "numeroPregunta": "3"
          },
          {
              "t13respuesta": "-1x<sup>3</sup>",
              "t17correcta": "1",
              "numeroPregunta": "4"
          },
          {
              "t13respuesta": "7x-3y",
              "t17correcta": "0",
              "numeroPregunta": "4"
          }, {
              "t13respuesta": "5x<sup>2</sup>+2y",
              "t17correcta": "0",
              "numeroPregunta": "4"
          },
      ],
      "pregunta": {
          "c03id_tipo_pregunta": "1",
          "t11pregunta": ["Selecciona la respuesta correcta.<br><br>¿Cuál representa un monomio?",
              "¿Cuál representa un monomio?",
              "¿Cuál representa un monomio?",
              "¿Cuál representa un monomio?",
              "¿Cuál representa un monomio?"

              ],
          "contieneDistractores": true,
          "preguntasMultiples": true,
      }
    },

    //reactivo 11
    {
        "respuestas": [
            {
                "t13respuesta": "2x<sup>2</sup>+5x+6",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "4x<sup>2</sup>+4x+1",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "-a+2",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "y<sup>4</sup>-4y<sup>2</sup>+25y+25",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "m<sup>2</sup>+11m+2",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "4m<sup>2</sup>+3mn-3n<sup>2</sup>+6",
                "t17correcta": "6",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "4x<sup>2</sup>+5x-2x<sup>2</sup>+6"
            },
            {
                "t11pregunta": "(2x+1)<sup>2</sup>"
            },
            {
                "t11pregunta": "1+ (-a+1)"
            },
            {
                "t11pregunta": "y<sup>4</sup>+y<sup>2</sup>+25+(-5y<sup>2</sup>+25y)"
            },
            {
                "t11pregunta": "5m+(m<sup>2</sup>+6m+2)"
            },
            {
                "t11pregunta": "(7m<sup>2</sup>-2mn+4n<sup>2</sup>+6)-(3m<sup>2</sup>-5mn+7n<sup>2</sup>)"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },

    // reactivo 12
    {
        "respuestas": [
            {
                "t13respuesta": "A=a(m+n)=a+mn",
                "t17correcta": "1",
            },
            {
                "t13respuesta": "A= a(a+1)=a<sup>2</sup>+a",
                "t17correcta": "2",
            },
            {
                "t13respuesta": "A=a(a+3+3)=a<sup>2</sup>+6a",
                "t17correcta": "3",
            },
            {
                "t13respuesta": "A= 4(m+4))= (m*4)+(4*4)4m+16",
                "t17correcta": "4",
            },
            {
                "t13respuesta": "A= [(2)(a)]<sup>2</sup>+ [(2)<sup>2</sup>]<sup>2</sup>→  [2a]<sup>2</sup>+ [4]<sup>2</sup> →  (4a<sup>2</sup>)+ (16) = 4a<sup>2</sup>+16",
                "t17correcta": "5",
            },
            {
                "t13respuesta": "m(m+2)= m<sup>2</sup>+2m",
                "t17correcta": "6",
            },
            {
                "t13respuesta": "a(a+b)=a<sup>2</sup>+ab",
                "t17correcta": "7",
            },
        ],
        "preguntas": [
            {
                "t11pregunta": "<img style='height: 60px;' src='MI8E_B01_R12_01.png'>"
            },
            {
                "t11pregunta": "<img src='MI8E_B01_R12_02.png' style='height: 60px;'>"
            },
            {
                "t11pregunta": "<img src='MI8E_B01_R12_03.png' style='height: 60px;'>"
            },
            {
                "t11pregunta": "<img src='MI8E_B01_R12_04.png' style='height: 60px;'>"
            },
            {
                "t11pregunta": "<img src='MI8E_B01_R12_05.png' style='height: 60px;'>"
            },
            {
                "t11pregunta": "<img src='MI8E_B01_R12_06.png' style='height: 60px;'>"
            },
            {
                "t11pregunta": "<img src='MI8E_B01_R12_07.png' style='height: 60px;'>"
            },
        ],
        "pregunta": {
            "c03id_tipo_pregunta": "12",
            "t11pregunta": "Relaciona las columnas según corresponda.",
            "altoImagen": "100px",
            "anchoImagen": "200px",
            "evaluable": true
        }
    },
    // reactivo 13
    {
        "respuestas": [
            //tabla 1
            {
                "t17correcta": "1176"
            },
            {
                "t17correcta": "1872"
            },
            {
                "t17correcta": "79.3"
            },

            //tabla 2

        ],
        "preguntas": [
            {
                "t11pregunta": "<style>\n\ .table img{height: 150px !important; width: auto !important; }\n\ </style><br><table class='table' style= 'margin-top:-20px;'>\n\
        <col width='300'>\n\
        <col width='300'>\n\
        <tr>\n <td>Preguntas</td>\n <td>Imagen\n\ </td>\n\ </tr><tr>\n <td> ¿Cuál es el volumen del cubo ?"
            },
            {
                "t11pregunta": "cm<sup>3</sup></td>\n <td><center><img src='MI8E_B01_R13_01.png'></center></td></tr><tr><td>¿Cuál es el volumen de esta figura?"
            },
            {
                "t11pregunta": "cm<sup>3</sup></td>\n <td><center><img src='MI8E_B01_R13_02.png'></center></td></tr><td>¿Cuál es el volumen de esta figura?"
            },
            {
                "t11pregunta": "cm<sup>3</sup></td>\n <td><center><img src='MI8E_B01_R13_03.png'></center></td></tr></table>"
            },



        ],
        "pregunta": {
            "t11pregunta": "Resuelve los siguientes problemas.",
            "c03id_tipo_pregunta": "6",
            "pintaUltimaCaja": false,
            evaluable: true
        }
    },
];
