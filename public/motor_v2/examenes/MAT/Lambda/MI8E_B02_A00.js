json=[
  //1
   {
      "respuestas":[
         {
            "t13respuesta":     "5x<sup>8</sup>y",
            "t17correcta":"1"
         },
         {
            "t13respuesta":     "168+458-85",
            "t17correcta":"0"
         },
         {
            "t13respuesta":     "15x+12xy+98x<sup>5</sup>",
            "t17correcta":"0"
         },
         {
            "t13respuesta":     "a-b",
            "t17correcta":"0"
         },
         {
            "t13respuesta":     "81mn<sup>8</sup>",
            "t17correcta":"1"
         },
         {
            "t13respuesta":     "-84x",
            "t17correcta":"1"
         },
         {
            "t13respuesta":     "2810-854",
            "t17correcta":"0"
         },
         {
            "t13respuesta":     "x",
            "t17correcta":"1"
         },
         {
            "t13respuesta":     "87mnz",
            "t17correcta":"1"
         },
         {
            "t13respuesta":     "3x+12z",
            "t17correcta":"0"
         }
      ],
      "pregunta":{
         "c03id_tipo_pregunta":"2",
         "dosColumnas": false,
         "t11pregunta":"<p style='margin-bottom: 5px;'>Identifica y elige todas las expresiones algebraicas que representa un monomio.</p>"
      }
   },
   //2
   {
       "respuestas": [
           {
               "t13respuesta": "<p>-2x+13y-8<\/p>",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "<p>-7x-3y<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p>-11x+2y+6<\/p>",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "<p>-5x-3y-6<\/p>",
               "t17correcta": "4"
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; }\n\
                                   </style><table class='table' style='margin-top:-40px;'>\n\
                                   <tr>\n\
                                       <td>Operaciones</td>\n\
                                       <td>Resultados</td>\n\
                                   </tr><tr>\n\
                                       <td>5x+7y-8-7x+6y</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\
                                       <td>4x-6y+8-11x-5+3y-3</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\
                                       <td>(-4x+6)-(7x-2y)</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\
                                       <td>(2y-8x)-(-3x+5y+6)</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr></table>"
           },


       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "t11pregunta": "Arrastra los polinomios al resultado correspondiente de cada operación.",
           "contieneDistractores":true,
           "anchoRespuestas": 70,
           "soloTexto": true,
           "respuestasLargas": true,
           "pintaUltimaCaja": false,
           "ocultaPuntoFinal": true
       }
   },
   //3
   {
       "respuestas": [
           {
               "t13respuesta": "<p>8x<sup>2</sup>+40xy<\/p>",
               "t17correcta": "1"
           },
           {
               "t13respuesta": "<p>y<sup>2</sup>-9<\/p>",
               "t17correcta": "2"
           },
           {
               "t13respuesta": "<p>z<sup>2</sup>+yz+6z<\/p>",
               "t17correcta": "3"
           },
           {
               "t13respuesta": "<p>9y+81<\/p>",
               "t17correcta": "4"
           },
           {
               "t13respuesta": "<p>8x<sup>2</sup>+40y<\/p>",
               "t17correcta": ""
           },
           {
               "t13respuesta": "<p>y<sup>2</sup>+9<\/p>",
               "t17correcta": ""
           },
           {
               "t13respuesta": "<p>z<sup>2</sup>-yz+6z<\/p>",
               "t17correcta": ""
           },
           {
               "t13respuesta": "<p>-9y+81<\/p>",
               "t17correcta": ""
           }
       ],
       "preguntas": [
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; }\n\
                                   </style><table class='table' style='margin-top:-40px;'>\n\
                                   <tr>\n\
                                       <td>Productos</td>\n\
                                       <td>Resultados</td>\n\
                                   </tr><tr>\n\
                                       <td>8x(x+5y)</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\
                                       <td>(y+3)(y-3)</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\
                                       <td>z(z+y+6)</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr><tr>\n\
                                       <td>9(y+9)</td>\n\
                                       <td>"
           },
           {
               "c03id_tipo_pregunta": "8",
               "t11pregunta": "        </td>\n\
                                   </tr></table>"
           },


       ],
       "pregunta": {
           "c03id_tipo_pregunta": "8",
           "t11pregunta": "Arrastra los resultados de los siguientes productos al lugar correspondiente.",
           "contieneDistractores":true,
           "anchoRespuestas": 70,
           "soloTexto": true,
           "respuestasLargas": true,
           "pintaUltimaCaja": false,
           "ocultaPuntoFinal": true
       }
   },
   //4
   {
       "respuestas": [
           {
               "t17correcta": "225"
           },
           {
               "t17correcta": "100"
           },
           {
               "t17correcta": "343"
           },
           {
               "t17correcta": "x"
           }
       ],
       "preguntas": [
           {
               "t11pregunta": "<br><style>\n\
                                       .table img{height: 90px !important; width: auto !important; } th{color: white;}\n\
                                   </style><table class='table' style='margin-top:-40px;'>\n\
                                   <thead>\n\
                                       <tr>\n\
                                           <th >Figura</th>\n\
                                           <th>Volumen (cm <sup>3</sup>)</th>\n\
                                       </tr>\n\
                                   </thead>\n\
                                   <tbody>\n\
                                       <tr>\n\
                                       <td>Prisma triangular</td>\n\
                                          <td>"
           },
           {
               "t11pregunta": "</td>\n\
                                      <tr>\n\
                                      <td>Pirámide cuadrangular</td>\n\
                                       <td>"
           },
           {
               "t11pregunta": "</td>\n\
                               <tr>\n\
                               <td>Cubo</td>\n\
                               <td>"
           },
           {
               "t11pregunta": "</td>\n\
                               </tr>\n\
                                   </tbody>\n\
                               </table>"
           }
       ],
       "pregunta": {
           "c03id_tipo_pregunta": "6",
           "t11pregunta":"Escribe el volumen de las figuras que se muestran a continuación.<br><img style='' src='MI8E_B02_A04.png'>",
           "evaluable":true,
           "pintaUltimaCaja": false
       }
   },
   //5
   {
     "respuestas":[
        {
           "t13respuesta":"98 km",
           "t17correcta":"1",
             "numeroPregunta":"0"
        },
        {
           "t13respuesta":"96 km",
           "t17correcta":"0",
             "numeroPregunta":"0"
        },
        {
           "t13respuesta":"99 km",
           "t17correcta":"0" ,
           "numeroPregunta":"0"
        },
        {
          "t13respuesta":"Proporcionalidad directa",
          "t17correcta":"1",
           "numeroPregunta":"1"
       },
       {
          "t13respuesta":"Proporcionalidad inversa",
          "t17correcta":"0",
           "numeroPregunta":"1"
       },
       {
          "t13respuesta":"6 días",
          "t17correcta":"1",
           "numeroPregunta":"2"
       },
       {
          "t13respuesta":"7 días",
          "t17correcta":"0",
           "numeroPregunta":"2"
       },
       {
          "t13respuesta":"8 días",
          "t17correcta":"0",
           "numeroPregunta":"2"
       },
       {
          "t13respuesta":"Proporcionalidad directa",
          "t17correcta":"0",
           "numeroPregunta":"3"
       },
       {
          "t13respuesta":"Proporcionalidad inversa",
          "t17correcta":"1",
           "numeroPregunta":"3"
       }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11pregunta":["Selecciona la respuesta correcta.<br>Un auto gasta un litro de gasolina por cada 14 kilómetros de recorrido.¿Cuántos kilómetros puede recorrer con siete litros?",
        "De la pregunta anterior, ¿qué tipo de proporcionalidad es?",
        "30  conejos se comen un costal de alimento en 8 días. Si se compran 10 conejos más, ¿en cuántos días se comerán la misma cantidad de alimento?",
        "De la pregunta anterior, ¿qué tipo de proporcionalidad es?"],
        "preguntasMultiples": true
     }
   },
   //6
   {
     "respuestas":[
        {
           "t13respuesta":"0.25",
           "t17correcta":"0",
             "numeroPregunta":"0"
        },
        {
           "t13respuesta":"0.375",
           "t17correcta":"1",
             "numeroPregunta":"0"
        },
        {
           "t13respuesta":"0.45",
           "t17correcta":"0",
             "numeroPregunta":"0"
        },
        {
           "t13respuesta":"0.625",
           "t17correcta":"0",
             "numeroPregunta":"1"
        },
        {
           "t13respuesta":"0.75",
           "t17correcta":"1",
             "numeroPregunta":"1"
        },
        {
           "t13respuesta":"0.50",
           "t17correcta":"0",
             "numeroPregunta":"1"
        }
     ],
     "pregunta":{
        "c03id_tipo_pregunta":"1",
        "t11pregunta":["Lee el problema y selecciona la respuesta correcta.<br>Lucía tiene ocho botes de pintura con un litro cada uno, dos de pintura naranja, uno de pintura rosa, tres de pintura negra y dos de pintura amarilla. Los botes no tienen ninguna referencia del color que contiene cada uno.<br>Sí Lucía toma un bote al azar, ¿qué probabilidad hay de que la pintura sea negra?",
        "¿Qué probabilidad hay de que la pintura no sea naranja?"],
        "preguntasMultiples": true
     }
  }
];
