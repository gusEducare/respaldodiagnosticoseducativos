json=[ 
   //1
   {  
    "respuestas":[  
        {
            "t13respuesta":     "x = 16-y",
            "t17correcta":"1",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "x = - 16-y",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "16x = y",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "x = <span class='fraction black'><span class='top'>16</span><span class='bottom'>y</span></span>",
            "t17correcta":"0",
          "numeroPregunta":"0"
         },
         {
            "t13respuesta":     "4(16 - y)-2y = 18",
            "t17correcta":"1",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "4(- 16 + y)-2y = 18",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "4(y) -2y = 18",
            "t17correcta":"0",
          "numeroPregunta":"1"
         },
         {
            "t13respuesta":     "x = <span class='fraction black'><span class='top'>25</span><span class='bottom'>3</span></span> ; y = <span class='fraction black'><span class='top'>23</span><span class='bottom'>3</span></span>",
            "t17correcta":"1",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "x = - <span class='fraction black'><span class='top'>23</span><span class='bottom'>3</span></span> ; y =<span class='fraction black'><span class='top'>25</span><span class='bottom'>3</span></span>",
            "t17correcta":"0",
          "numeroPregunta":"2"
         },
         {
            "t13respuesta":     "y = - <span class='fraction black'><span class='top'>25</span><span class='bottom'>3</span></span> ; x = - <span class='fraction black'><span class='top'>23</span><span class='bottom'>3</span></span>",
            "t17correcta":"0",
          "numeroPregunta":"2"
         }
    ],
    "pregunta":{  
       "c03id_tipo_pregunta":"1",
       "t11pregunta":["Resuelve el sistema de ecuaciones y selecciona la opción correcta a cada pregunta.<br><br>¿Qué expresión resulta al despejar x en la primera ecuación?<br>x+y=16<br>4x-2y=18",
       "¿Qué expresión resulta al sustituir x en la segunda ecuación?",
       "Encuentra el valor de x en la segunda ecuación y resuelve el sistema de ecuaciones, ¿cuál es la solución?"],
       "preguntasMultiples": true
    }
 },
        //2
        {  
            "respuestas":[  
                {
                    "t13respuesta":     "x = - <span class='fraction black'><span class='top'>1</span><span class='bottom'>3</span></span> ; y = <span class='fraction black'><span class='top'>4</span><span class='bottom'>3</span></span>",
                    "t17correcta":"1",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "x = <span class='fraction black'><span class='top'>2</span><span class='bottom'>3</span></span> ; y = <span class='fraction black'><span class='top'>2</span><span class='bottom'>3</span></span>",
                    "t17correcta":"0",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "x = 0 ; y = 3",
                    "t17correcta":"0",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "x = -4 ; y = 2",
                    "t17correcta":"0",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "x = <span class='fraction black'><span class='top'>2</span><span class='bottom'>3</span></span> ; y = <span class='fraction black'><span class='top'>2</span><span class='bottom'>3</span></span>",
                    "t17correcta":"1",
                  "numeroPregunta":"1"
                 },
                 {
                    "t13respuesta":     "x = - <span class='fraction black'><span class='top'>1</span><span class='bottom'>3</span></span> ; y = <span class='fraction black'><span class='top'>4</span><span class='bottom'>3</span></span>",
                    "t17correcta":"0",
                  "numeroPregunta":"1"
                 },
                 {
                    "t13respuesta":     "x = 0 ; y = 3",
                    "t17correcta":"0",
                  "numeroPregunta":"1"
                 },
                 {
                    "t13respuesta":     "x = -4 ; y = 2",
                    "t17correcta":"0",
                  "numeroPregunta":"1"
                 },
                 {
                    "t13respuesta":     "x = 0 ; y = 3",
                    "t17correcta":"1",
                  "numeroPregunta":"2"
                 },
                 {
                    "t13respuesta":     "x = -4 ; y = 2",
                    "t17correcta":"0",
                  "numeroPregunta":"2"
                 },
                 {
                    "t13respuesta":     "x = - 13 ; y = 43",
                    "t17correcta":"0",
                  "numeroPregunta":"2"
                 },
                 {
                    "t13respuesta":     "x =  23 ; y = 23",
                    "t17correcta":"0",
                  "numeroPregunta":"2"
                 },
                 {
                    "t13respuesta":     "x = 0 ; y = 3",
                    "t17correcta":"0",
                  "numeroPregunta":"3"
                 },
                 {
                    "t13respuesta":     "x = -4 ; y = 2",
                    "t17correcta":"1",
                  "numeroPregunta":"3"
                 },
                 {
                    "t13respuesta":     "x = - 13 ; y = 43",
                    "t17correcta":"0",
                  "numeroPregunta":"3"
                 },
                 {
                    "t13respuesta":     "x =  23 ; y = 23",
                    "t17correcta":"0",
                  "numeroPregunta":"3"
                 }
            ],
            "pregunta":{  
               "c03id_tipo_pregunta":"1",
               "t11pregunta":["Selecciona el par de valores que son la solución del siguiente sistema de ecuaciones.<br><br>5x+2y=1<br>-3x+3y=5",
               "5x-2y=2<br>x+2y=2",
               "3x+5y=15<br>2x-3y= -9",
                "-2x+3y=14<br>3x-y=-14"],
               "preguntasMultiples": true
            }
         },
         //3
         {
            "respuestas": [
                {
                    "t17correcta": "8"
                },
                {
                    "t17correcta": "2"
                },
                {
                    "t17correcta": "a"
                },
                {
                    "t17correcta": "b"
                },
            ],
            "preguntas": [
                {
                    "t11pregunta": "<center><img src='MI8E_B05_A03.png' ></center>El segmento [a] representa la ecuación x+y=10<br>Y el segmento [b] representa la ecuación 6x-7y=34<br>El valor de x es &nbsp"
                },
                {
                    "t11pregunta": "<br>El valor de y = &nbsp"
                },
            ],
            "pregunta": {
                "c03id_tipo_pregunta": "6",
                "t11pregunta": "Escribe las respuestas correctas a partir del siguiente sistema de ecuaciones.",
                evaluable:true
            }
        },
        //4
        {  
            "respuestas":[  
                {
                    "t13respuesta":     "perpendiculares",
                    "t17correcta":"1",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "secantes",
                    "t17correcta":"0",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "paralelos",
                    "t17correcta":"0",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "tangentes",
                    "t17correcta":"0",
                  "numeroPregunta":"0"
                 },
                 {
                    "t13respuesta":     "distancia",
                    "t17correcta":"1",
                  "numeroPregunta":"1"
                 },
                 {
                    "t13respuesta":     "unión",
                    "t17correcta":"0",
                  "numeroPregunta":"1"
                 },
                 {
                    "t13respuesta":     "perpendicular",
                    "t17correcta":"0",
                  "numeroPregunta":"1"
                 },
                 {
                    "t13respuesta":     "AB",
                    "t17correcta":"1",
                  "numeroPregunta":"2"
                 },
                 {
                    "t13respuesta":     "CB",
                    "t17correcta":"0",
                  "numeroPregunta":"2"
                 },
                 {
                    "t13respuesta":     "BC",
                    "t17correcta":"0",
                  "numeroPregunta":"2"
                 },
                 {
                    "t13respuesta":     "C'A'B’",
                    "t17correcta":"1",
                  "numeroPregunta":"3"
                 },
                 {
                    "t13respuesta":     "A'B'C",
                    "t17correcta":"0",
                  "numeroPregunta":"3"
                 },
                 {
                    "t13respuesta":     "B'C’A’",
                    "t17correcta":"0",
                  "numeroPregunta":"3"
                 },
            ],
            "pregunta":{  
               "c03id_tipo_pregunta":"1",
               "t11pregunta":["Observa la imagen y elige la opción correcta a cada afirmación.<br><br><center><img src='MI8E_B05_A04.png' weight='200' height='200'></center><br><br>En figuras simétricas los segmentos que unen los puntos son <u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> al eje de simetría.",
               "<center><img src='MI8E_B05_A04.png' weight='200' height='200'></center><br><br>La <u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> del punto A al eje de simetría es la misma que de A' al mismo eje.",
               "<center><img src='MI8E_B05_A04.png' weight='200' height='200'></center><br><br>El segmento <u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> y el segmento A'B' miden lo mismo.",
                "<center><img src='MI8E_B05_A04.png' weight='200' height='200'></center><br><br>El ángulo CAB y el ángulo <u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> miden lo mismo."],
               "preguntasMultiples": true
            }
         },
         //5
         {  
          "respuestas":[  
              {
                  "t13respuesta":     "37.69",
                  "t17correcta":"1",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "340.47",
                  "t17correcta":"0",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "40.05",
                  "t17correcta":"0",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "339.29",
                  "t17correcta":"0",
                "numeroPregunta":"0"
               },
               {
                  "t13respuesta":     "339.29",
                  "t17correcta":"1",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "37.69",
                  "t17correcta":"0",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "340.47",
                  "t17correcta":"0",
                "numeroPregunta":"1"
               },
               {
                  "t13respuesta":     "40.05",
                  "t17correcta":"0",
                "numeroPregunta":"1"
               }
          ],
          "pregunta":{  
             "c03id_tipo_pregunta":"1",
             "t11pregunta":["Selecciona el resultado correcto<br>Considera que son polígonos regulares.<br><br><center><img src='MI8E_B05_A05.png' weight='200' height='200'></center><br>El valor del arco en centímetros es de:",
             "El área del sector circular en centímetros cuadrados es de:"],
             "preguntasMultiples": true
          }
       },
       //6
       {  
        "respuestas":[  
            {
                "t13respuesta":     "Roja",
                "t17correcta":"1",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Azul",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "Amarilla",
                "t17correcta":"0",
              "numeroPregunta":"0"
             },
             {
                "t13respuesta":     "22%",
                "t17correcta":"1",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "20%",
                "t17correcta":"",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "18%",
                "t17correcta":"0",
              "numeroPregunta":"1"
             },
             {
                "t13respuesta":     "21%",
                "t17correcta":"1",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "20%",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "17%",
                "t17correcta":"0",
              "numeroPregunta":"2"
             },
             {
                "t13respuesta":     "20%",
                "t17correcta":"1",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "19%",
                "t17correcta":"0",
              "numeroPregunta":"3"
             },
             {
                "t13respuesta":     "16%",
                "t17correcta":"0",
              "numeroPregunta":"3"
             }
        ],
        "pregunta":{  
           "c03id_tipo_pregunta":"1",
           "t11pregunta":["Lee el problema y selecciona la respuesta correcta a cada pregunta.<br><br>¿De qué color es la pelota cuyo porcentaje de probabilidad frecuencial de salir en este experimento es 2% menor que su probabilidad teórica?",
            "¿Cuál es la probabilidad frecuencial de sacar una pelota de color verde?",
            "¿Cuál es la probabilidad frecuencial de sacar una pelota de color amarillo?",
            "¿Cuál es la probabilidad teórica de obtener una esfera azul?"],
           "t11instruccion": "De un recipiente con cinco pelotas de diferentes colores, Silvia sacaba pelotas de una en una, regresando cada pelota antes de volver a sacar otra. En la siguiente tabla se registraron los resultados del experimento.<br><br><center><img src='MI8E_B05_A06.png'></center>",
           "preguntasMultiples": true
        }
     },
   
  ];                    