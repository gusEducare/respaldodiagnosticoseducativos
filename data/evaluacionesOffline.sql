CREATE DATABASE  IF NOT EXISTS `evaluaciones` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `evaluaciones`;
-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: evaluaciones
-- ------------------------------------------------------
-- Server version	5.5.55-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t37propiedades_extras`
--

DROP TABLE IF EXISTS `t37propiedades_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t37propiedades_extras` (
  `t37id_propiedades_extras` int(11) NOT NULL AUTO_INCREMENT,
  `t11id_pregunta` int(11) DEFAULT NULL,
  `t37valor_propiedad` longtext,
  `c12id_propiedad` int(11) NOT NULL,
  PRIMARY KEY (`t37id_propiedades_extras`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t37propiedades_extras`
--

LOCK TABLES `t37propiedades_extras` WRITE;
/*!40000 ALTER TABLE `t37propiedades_extras` DISABLE KEYS */;
INSERT INTO `t37propiedades_extras` VALUES (1,2352,'contenedor1|contenedor2',1),(2,2353,'C1|C2',1),(3,2354,'[{\"Contenedor\": [\"\", \"234,15\", \"cuadrado\", \"110, 110\", \".\",\"transparent\"]},{\"Contenedor\": [\"\", \"234,141\", \"cuadrado\", \"110, 110\", \".\",\"transparent\"]},{\"Contenedor\": [\"\", \"234,266\", \"cuadrado\", \"110, 110\", \".\",\"transparent\"]},{\"Contenedor\": [\"\", \"234,393\", \"cuadrado\", \"110, 110\", \".\",\"transparent\"]},{\"Contenedor\": [\"\", \"234,519\", \"cuadrado\", \"110, 110\", \".\",\"transparent\"]}]',9),(4,2355,'[{\"Contenedor\": [\"\", \"12,190\", \"izquierda\", \"140, 30\", \".\"]},{\"Contenedor\": [\"\", \"6,510\", \"abajo\", \"140, 30\", \".\"]},{\"Contenedor\": [\"\", \"235,162\", \"izquierda\", \"140, 30\", \".\"]},{\"Contenedor\": [\"\", \"313,405\", \"izquierda\", \"140, 30\", \".\"]}]',9);
/*!40000 ALTER TABLE `t37propiedades_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t35pregunta_propiedad`
--

DROP TABLE IF EXISTS `t35pregunta_propiedad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t35pregunta_propiedad` (
  `t35id_pregunta_propiedad` int(11) NOT NULL AUTO_INCREMENT,
  `c12id_propiedad` int(11) NOT NULL,
  `t11id_pregunta` int(11) NOT NULL,
  `t35valor_propiedad` varchar(100) NOT NULL,
  PRIMARY KEY (`t35id_pregunta_propiedad`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t35pregunta_propiedad`
--

LOCK TABLES `t35pregunta_propiedad` WRITE;
/*!40000 ALTER TABLE `t35pregunta_propiedad` DISABLE KEYS */;
INSERT INTO `t35pregunta_propiedad` VALUES (1,2,2352,'vertical'),(2,2,2353,'horizontal'),(3,3,2354,'true'),(4,4,2354,'true'),(5,5,2354,'true'),(6,6,2354,'true'),(7,7,2354,'true'),(8,8,2354,'img/examenes/geo_4b2_s03_a01_1.png'),(9,2,2354,'ordenar'),(10,8,2355,'img/examenes/esp_db1_s01_a04.png'),(11,2,2355,'ordenar'),(12,7,2355,'true'),(13,10,2355,'vertical'),(14,13,2360,'editable'),(15,14,2360,'30'),(16,15,2360,'true'),(17,16,2360,'true'),(18,17,2361,'0,1'),(19,17,2362,'0,1'),(20,17,2363,'0'),(21,17,2364,'0');
/*!40000 ALTER TABLE `t35pregunta_propiedad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t36respuesta_propiedad`
--

DROP TABLE IF EXISTS `t36respuesta_propiedad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t36respuesta_propiedad` (
  `t36id_respuesta_propiedad` int(11) NOT NULL AUTO_INCREMENT,
  `c12id_propiedad` int(11) NOT NULL,
  `t13id_respuesta` int(11) NOT NULL,
  `t36valor_propiedad` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`t36id_respuesta_propiedad`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t36respuesta_propiedad`
--

LOCK TABLES `t36respuesta_propiedad` WRITE;
/*!40000 ALTER TABLE `t36respuesta_propiedad` DISABLE KEYS */;
INSERT INTO `t36respuesta_propiedad` VALUES (1,11,7602,'1'),(2,11,7603,'2'),(3,11,7604,'3'),(4,11,7605,'4');
/*!40000 ALTER TABLE `t36respuesta_propiedad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t34propiedad_tipo`
--

DROP TABLE IF EXISTS `t34propiedad_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t34propiedad_tipo` (
  `t34id_propiedad_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `c12id_propiedad` int(11) NOT NULL,
  `c03id_tipo_pregunta` int(11) DEFAULT NULL,
  PRIMARY KEY (`t34id_propiedad_tipo`),
  KEY `c12id_propiedad_idx` (`c12id_propiedad`),
  KEY `c03id_tipo_pregunta_idx` (`c03id_tipo_pregunta`),
  CONSTRAINT `c03id_tipo_pregunta_t34` FOREIGN KEY (`c03id_tipo_pregunta`) REFERENCES `c03tipo_pregunta` (`c03id_tipo_pregunta`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `c12id_propiedad_t34` FOREIGN KEY (`c12id_propiedad`) REFERENCES `c12propiedad` (`c12id_propiedad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t34propiedad_tipo`
--

LOCK TABLES `t34propiedad_tipo` WRITE;
/*!40000 ALTER TABLE `t34propiedad_tipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `t34propiedad_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `c12propiedad`
--

DROP TABLE IF EXISTS `c12propiedad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `c12propiedad` (
  `c12id_propiedad` int(11) NOT NULL AUTO_INCREMENT,
  `c12nombre_propiedad` varchar(100) NOT NULL,
  `c12tipo_propiedad` varchar(100) DEFAULT NULL,
  `c12descripcion` varchar(100) DEFAULT NULL,
  `c12nivel_propiedad` enum('PARENT','PREGUNTA','RESPUESTA') DEFAULT NULL,
  PRIMARY KEY (`c12id_propiedad`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `c12propiedad`
--

LOCK TABLES `c12propiedad` WRITE;
/*!40000 ALTER TABLE `c12propiedad` DISABLE KEYS */;
INSERT INTO `c12propiedad` VALUES (1,'contenedores','array','','PARENT'),(2,'tipo','string','','PREGUNTA'),(3,'respuestaImagen','boolean','','PREGUNTA'),(4,'tamanyoReal','boolean','','PREGUNTA'),(5,'bloques','boolean','','PREGUNTA'),(6,'borde','boolean','','PREGUNTA'),(7,'imagen','boolean','','PREGUNTA'),(8,'url','string','','PREGUNTA'),(9,'contenedores','object',NULL,'PARENT'),(10,'orientacion','string','','PREGUNTA'),(11,'etiqueta','string','','RESPUESTA'),(12,'descripcion','string','','PREGUNTA'),(13,'variante','string',NULL,'PREGUNTA'),(14,'anchoColumnaPreguntas','integer',NULL,'PREGUNTA'),(15,'evaluable','boolean',NULL,'PREGUNTA'),(16,'multiplesRespuestas','boolean',NULL,'PREGUNTA'),(17,'correcta','strng','','PREGUNTA');
/*!40000 ALTER TABLE `c12propiedad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'evaluaciones'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-29 16:27:27
