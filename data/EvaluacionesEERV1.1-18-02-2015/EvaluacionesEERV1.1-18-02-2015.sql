SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `evaluaciones` DEFAULT CHARACTER SET utf8 ;
USE `evaluaciones` ;

-- -----------------------------------------------------
-- Table `evaluaciones`.`c01perfil`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c01perfil` (
  `c01id_perfil` INT(11) NOT NULL AUTO_INCREMENT ,
  `c01nombre` VARCHAR(100) NOT NULL ,
  `c01descripcion` TEXT NULL DEFAULT NULL ,
  `c01estatus` CHAR(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`c01id_perfil`) ,
  UNIQUE INDEX `c01_idx1_c01nombre_UNIQUE` (`c01nombre` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c02tipo_examen`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c02tipo_examen` (
  `c02id_tipo_examen` INT(11) NOT NULL AUTO_INCREMENT ,
  `c02tipo_examen` VARCHAR(100) NOT NULL ,
  `c02url_icono_tipo` VARCHAR(200) NULL DEFAULT NULL ,
  `c02estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`c02id_tipo_examen`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c03tipo_pregunta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c03tipo_pregunta` (
  `c03id_tipo_pregunta` INT(11) NOT NULL AUTO_INCREMENT ,
  `c03tipo_pregunta` VARCHAR(50) NOT NULL ,
  `c03clave_tipo` VARCHAR(6) NOT NULL ,
  `c03url_icono_tipo` VARCHAR(200) NOT NULL ,
  `c03instruccion` TEXT NULL DEFAULT NULL ,
  `c03minimo_respuestas` INT(2) NULL DEFAULT NULL ,
  `c03estatus` CHAR(1) NOT NULL ,
  `c03tiempo_limite` TINYINT(1) NULL DEFAULT NULL COMMENT 'Bandera que indica si el tipo de pregunta requiere tiempo limite' ,
  `c03columnas` TINYINT(1) NULL DEFAULT NULL COMMENT 'Bandera que indica si el tipo de pregunta requiere columnas' ,
  `c03renglones` TINYINT(1) NULL DEFAULT NULL COMMENT 'Bander que indica si el tipo de pregunta requiere renglones' ,
  `c03numerar` TINYINT(1) NULL DEFAULT NULL COMMENT 'Bandera que indica si el tipo de pregunta requiere numerar sus respuestas' ,
  `c03aleatorias` TINYINT(1) NULL DEFAULT NULL COMMENT 'Banera que indica si el tipo de pregunta requiere respuestas aleatorias' ,
  PRIMARY KEY (`c03id_tipo_pregunta`) ,
  UNIQUE INDEX `c03tipo_pregunta_UNIQUE` (`c03tipo_pregunta` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c04escala`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c04escala` (
  `c04id_escala` INT(11) NOT NULL AUTO_INCREMENT ,
  `c04escala` VARCHAR(45) NOT NULL ,
  `c04estatus` CHAR(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`c04id_escala`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c05nivel`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c05nivel` (
  `c05id_nivel` INT(11) NOT NULL AUTO_INCREMENT ,
  `c05id_nivel_parent` INT(2) NULL DEFAULT NULL ,
  `c05nivel` VARCHAR(45) NOT NULL ,
  `c05estatus` CHAR(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`c05id_nivel`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t01usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t01usuario` (
  `t01id_usuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `t01nombre` VARCHAR(45) NOT NULL DEFAULT '' ,
  `t01apellidos` VARCHAR(100) NOT NULL DEFAULT '' ,
  `t01correo` VARCHAR(255) NOT NULL DEFAULT '' ,
  `t01contrasena` VARCHAR(255) NOT NULL DEFAULT '' ,
  `t01bloqueada` TINYINT(4) NOT NULL ,
  `t01tiempo_bloqueo` VARCHAR(50) NULL DEFAULT NULL ,
  `t01fecha_registro` DATETIME NOT NULL ,
  `t01fecha_actualiza` DATETIME NOT NULL ,
  `t01demo` TINYINT(4) NOT NULL ,
  `t01acepto_condiciones` TINYINT(4) NOT NULL ,
  `t01provider_id` VARCHAR(50) NULL DEFAULT NULL ,
  `t01provider` VARCHAR(100) NULL DEFAULT NULL ,
  `t01estatus` CHAR(1) NULL ,
  PRIMARY KEY (`t01id_usuario`) ,
  UNIQUE INDEX `t01_idx1_t01correo` (`t01correo` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 13071
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t02usuario_perfil`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t02usuario_perfil` (
  `t02id_usuario_perfil` INT(11) NOT NULL AUTO_INCREMENT ,
  `c01id_perfil` INT(11) NOT NULL ,
  `t01id_usuario` INT(11) NOT NULL ,
  PRIMARY KEY (`t02id_usuario_perfil`) ,
  INDEX `t02_idx1_t01id_usuario` (`t01id_usuario` ASC) ,
  INDEX `t02_idx2_c01id_perfil` (`c01id_perfil` ASC) ,
  CONSTRAINT `t02_fk1_t02id_usuario`
    FOREIGN KEY (`t01id_usuario` )
    REFERENCES `evaluaciones`.`t01usuario` (`t01id_usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t02_fk2_c01id_perfil`
    FOREIGN KEY (`c01id_perfil` )
    REFERENCES `evaluaciones`.`c01perfil` (`c01id_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 11976
DEFAULT CHARACTER SET = utf8
COMMENT = 'licencia_profesor\r\nA=Activa\r\nI=Inactiva\r\nV=Vencida';


-- -----------------------------------------------------
-- Table `evaluaciones`.`t21paquete`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t21paquete` (
  `t21id_paquete` INT NOT NULL ,
  `t21clave_paquete` VARCHAR(45) NULL ,
  `t21descripcion_paquete` VARCHAR(255) NULL ,
  PRIMARY KEY (`t21id_paquete`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t03licencia_usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t03licencia_usuario` (
  `t03id_licencia_usuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `t21id_paquete` INT(11) NOT NULL ,
  `t02id_usuario_perfil` INT(11) NOT NULL ,
  `t03licencia` VARCHAR(10) NOT NULL ,
  `t03fecha_activacion` DATETIME NOT NULL ,
  `t03dias_duracion` INT(10) NOT NULL ,
  `t03estatus` ENUM('DISPONIBLE','VENCIDA','INACTIVA') NULL DEFAULT 'DISPONIBLE' ,
  PRIMARY KEY (`t03id_licencia_usuario`, `t21id_paquete`) ,
  INDEX `t03_idx1_t02id_usuario_perfil` (`t02id_usuario_perfil` ASC) ,
  INDEX `t03_idx2_t21id_paquete` (`t21id_paquete` ASC) ,
  CONSTRAINT `t03_fk1_t03id_usuario_perfil`
    FOREIGN KEY (`t02id_usuario_perfil` )
    REFERENCES `evaluaciones`.`t02usuario_perfil` (`t02id_usuario_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t03_fk2_t21id_paquete`
    FOREIGN KEY (`t21id_paquete` )
    REFERENCES `evaluaciones`.`t21paquete` (`t21id_paquete` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 246
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t12examen`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t12examen` (
  `t12id_examen` INT(11) NOT NULL AUTO_INCREMENT ,
  `c02id_tipo_examen` INT(11) NOT NULL ,
  `c04id_escala` INT(11) NOT NULL ,
  `c05id_nivel` INT(11) NOT NULL ,
  `t12nombre` TINYTEXT NOT NULL ,
  `t12nombre_corto_clave` VARCHAR(100) NULL DEFAULT NULL ,
  `t12instrucciones` VARCHAR(500) NULL DEFAULT NULL ,
  `t12descripcion` TEXT NULL DEFAULT NULL ,
  `t12fecha_registro` DATE NULL DEFAULT NULL ,
  `t12fecha_actualiza` DATE NULL DEFAULT NULL ,
  `t12num_intentos` INT(11) NULL DEFAULT NULL ,
  `t12aleatorio_preguntas` TINYINT(1) NULL DEFAULT NULL ,
  `t12aleatorio_respuestas` TINYINT(1) NULL DEFAULT NULL ,
  `t12demo` TINYINT(1) NULL DEFAULT NULL ,
  `t12minimo_aprobatorio` INT(4) NOT NULL ,
  `t12tiempo` INT(3) NOT NULL ,
  `t12estatus` CHAR(1) NOT NULL COMMENT 'I:INACTIVO,A:ACTIVO,\n' ,
  PRIMARY KEY (`t12id_examen`) ,
  INDEX `t12_idx1_c02id_tipo_examen` (`c02id_tipo_examen` ASC) ,
  INDEX `t12_idx2_c04id_escala` (`c04id_escala` ASC) ,
  INDEX `t12_idx3_c05id_nivel` (`c05id_nivel` ASC) ,
  CONSTRAINT `t12_fk1_c02id_tipo_examen`
    FOREIGN KEY (`c02id_tipo_examen` )
    REFERENCES `evaluaciones`.`c02tipo_examen` (`c02id_tipo_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t12_fk2_c04id_escala`
    FOREIGN KEY (`c04id_escala` )
    REFERENCES `evaluaciones`.`c04escala` (`c04id_escala` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t12_fk3_c05nivel`
    FOREIGN KEY (`c05id_nivel` )
    REFERENCES `evaluaciones`.`c05nivel` (`c05id_nivel` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t04examen_usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t04examen_usuario` (
  `t04id_examen_usuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `t12id_examen` INT(11) NOT NULL ,
  `t03id_licencia_usuario` INT(11) NOT NULL ,
  `t04calificacion` VARCHAR(4) NOT NULL ,
  `t04fecha_inicio` DATETIME NOT NULL COMMENT 'Fecha de inicio\nsolo para historico\nde examene' ,
  `t04fecha_fin` DATETIME NULL DEFAULT NULL COMMENT 'Fecha fin \nsolo para \nhistorico de \nexamen' ,
  `t04tiempo` VARCHAR(8) NULL DEFAULT NULL ,
  `t04examen_json` TEXT NULL DEFAULT NULL ,
  `t04num_intento` INT(2) NULL DEFAULT '1' ,
  `t04estatus` ENUM('APROBADO','REPROBADO','NOPRESENTADO','PRESENTADO') NOT NULL DEFAULT 'NOPRESENTADO' COMMENT 'Estatus Examen\nA:Aprobado\nR:Reprobado\nN:NoPresentado\nP:Presentado\n\n ' ,
  PRIMARY KEY (`t04id_examen_usuario`) ,
  INDEX `t04_idx1_t12id_examen` (`t12id_examen` ASC) ,
  INDEX `t04_idx2_t03id_licencia_usuario` (`t03id_licencia_usuario` ASC) ,
  CONSTRAINT `t04_fk1_t03id_licencia_usuario`
    FOREIGN KEY (`t03id_licencia_usuario` )
    REFERENCES `evaluaciones`.`t03licencia_usuario` (`t03id_licencia_usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t04_fk2_t12id_examen`
    FOREIGN KEY (`t12id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 231
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t05grupo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t05grupo` (
  `t05id_grupo` INT(11) NOT NULL AUTO_INCREMENT ,
  `t05nombre` VARCHAR(100) NULL DEFAULT NULL ,
  `t05ciclo` VARCHAR(25) NOT NULL DEFAULT '' ,
  `t05estatus` CHAR(1) NOT NULL DEFAULT 'A' ,
  PRIMARY KEY (`t05id_grupo`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1130
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t06menu_perfil`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t06menu_perfil` (
  `t06id_menu_perfil` INT(11) NOT NULL AUTO_INCREMENT ,
  `c01id_perfil` INT(11) NOT NULL ,
  `t06texto` VARCHAR(100) NULL DEFAULT NULL ,
  `t06descripcion` VARCHAR(100) NULL DEFAULT NULL ,
  `t06url` VARCHAR(100) NULL DEFAULT NULL ,
  `t06inner_link` TINYINT(1) NOT NULL DEFAULT '0' ,
  `t06estatus` CHAR(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`t06id_menu_perfil`) ,
  INDEX `t06_idx1_t06id_perfil` (`c01id_perfil` ASC) ,
  CONSTRAINT `t06_fk1_t06id_perfil`
    FOREIGN KEY (`c01id_perfil` )
    REFERENCES `evaluaciones`.`c01perfil` (`c01id_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 87
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t07codigo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t07codigo` (
  `t07id_codigo` INT(11) NOT NULL AUTO_INCREMENT ,
  `t05id_grupo` INT(11) NOT NULL ,
  `t07codigo` VARCHAR(25) NOT NULL ,
  `t07tipo` CHAR(20) NOT NULL ,
  `t07fecha_registro` DATETIME NULL ,
  `t07fecha_actualiza` DATETIME NULL ,
  `t07estatus` CHAR(1) NULL ,
  PRIMARY KEY (`t07id_codigo`) ,
  UNIQUE INDEX `t07_idx1_t07codigo` (`t07codigo` ASC) ,
  INDEX `t07_idx2_t07id_grupo` (`t05id_grupo` ASC) ,
  CONSTRAINT `t07_fk1_t07id_grupo`
    FOREIGN KEY (`t05id_grupo` )
    REFERENCES `evaluaciones`.`t05grupo` (`t05id_grupo` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12646
DEFAULT CHARACTER SET = utf8
COMMENT = 'InformaciÃ³n:\r\narchivos/catalogo_estatus_cÃ³digos.txt';


-- -----------------------------------------------------
-- Table `evaluaciones`.`t08administrador_grupo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t08administrador_grupo` (
  `t02id_usuario_perfil` INT(11) NOT NULL ,
  `t05id_grupo` INT(11) NOT NULL ,
  `t08usuario_ligado` CHAR(1) NULL DEFAULT NULL ,
  `t08fecha_registro` DATETIME NOT NULL ,
  `t08fecha_actualiza` DATETIME NOT NULL ,
  PRIMARY KEY (`t02id_usuario_perfil`, `t05id_grupo`) ,
  INDEX `t08_fk2_t05id_grupo` (`t05id_grupo` ASC) ,
  CONSTRAINT `t08_fk1_t02id_usuario_perfil`
    FOREIGN KEY (`t02id_usuario_perfil` )
    REFERENCES `evaluaciones`.`t02usuario_perfil` (`t02id_usuario_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t08_fk2_t05id_grupo`
    FOREIGN KEY (`t05id_grupo` )
    REFERENCES `evaluaciones`.`t05grupo` (`t05id_grupo` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t09agenda`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t09agenda` (
  `t09id_agenda` INT(11) NOT NULL AUTO_INCREMENT ,
  `t02id_examen` INT(11) NOT NULL ,
  `t05id_grupo` INT(11) NOT NULL ,
  `t09fecha_inicio` DATETIME NULL ,
  `t09fecha_final` DATETIME NULL ,
  `t09estatus` ENUM('CANCELADO','HABILITADO') NULL DEFAULT 'HABILITADO' ,
  PRIMARY KEY (`t09id_agenda`) ,
  INDEX `t09_idx1_t12id_examen` (`t02id_examen` ASC) ,
  INDEX `t09_idx2_t05id_grupo` (`t05id_grupo` ASC) ,
  CONSTRAINT `t09_fk1_t12id_examen`
    FOREIGN KEY (`t02id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t09_fk2_t05id_grupo`
    FOREIGN KEY (`t05id_grupo` )
    REFERENCES `evaluaciones`.`t05grupo` (`t05id_grupo` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c06numeracion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c06numeracion` (
  `c06id_numeracion` INT(11) NOT NULL ,
  `c06numeracion` VARCHAR(45) NOT NULL ,
  `c06estatus` CHAR(1) NULL ,
  PRIMARY KEY (`c06id_numeracion`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t11pregunta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t11pregunta` (
  `t11id_pregunta` INT(11) NOT NULL AUTO_INCREMENT ,
  `t11id_pregunta_parent` INT(11) NULL DEFAULT NULL ,
  `c03id_tipo_pregunta` INT(11) NOT NULL ,
  `c06id_numeracion` INT(11) NOT NULL ,
  `t11pregunta` TEXT NULL DEFAULT NULL ,
  `t11nombre_corto_clave` VARCHAR(100) NULL DEFAULT NULL ,
  `t11instruccion` TEXT NULL DEFAULT NULL ,
  `t11tiempo_limite` VARCHAR(5) NOT NULL ,
  `t11columnas` VARCHAR(45) NULL DEFAULT NULL ,
  `t11fecha_registro` DATETIME NULL DEFAULT NULL ,
  `t11fecha_actualiza` DATETIME NULL DEFAULT NULL ,
  `t11estatus` CHAR(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`t11id_pregunta`, `c03id_tipo_pregunta`) ,
  INDEX `t11_idx1_c03id_tipo_pregunta` (`c03id_tipo_pregunta` ASC) ,
  INDEX `t11_idx2_c06numeracion` (`c06id_numeracion` ASC) ,
  CONSTRAINT `t11_fk1_c03id_tipo_pregunta`
    FOREIGN KEY (`c03id_tipo_pregunta` )
    REFERENCES `evaluaciones`.`c03tipo_pregunta` (`c03id_tipo_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t11_fk2_c06numeracion`
    FOREIGN KEY (`c06id_numeracion` )
    REFERENCES `evaluaciones`.`c06numeracion` (`c06id_numeracion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t13respuesta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t13respuesta` (
  `t13id_respuesta` INT(11) NOT NULL AUTO_INCREMENT ,
  `t13id_respuesta_parent` INT(11) NULL DEFAULT NULL ,
  `t13respuesta` TINYTEXT NOT NULL ,
  `t13orden` INT(3) NOT NULL ,
  `t13fecha_registro` DATETIME NULL DEFAULT NULL ,
  `t13fecha_actualiza` DATETIME NULL DEFAULT NULL ,
  `t13estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`t13id_respuesta`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t10respuesta_usuario_examen`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t10respuesta_usuario_examen` (
  `t10id_respuesta_examen` INT(11) NOT NULL AUTO_INCREMENT ,
  `t02id_usuario_perfil` INT(11) NOT NULL ,
  `t11id_pregunta` INT(11) NOT NULL ,
  `t12id_examen` INT(11) NOT NULL ,
  `t13id_respuesta` INT(11) NOT NULL ,
  `t10llave` VARCHAR(10) NOT NULL ,
  `t10num_intento` INT(2) NOT NULL DEFAULT '1' ,
  `t10respuesta_usuario` TINYTEXT NULL DEFAULT NULL ,
  `t10respuesta_correcta` TINYTEXT NULL DEFAULT NULL ,
  `t10fecha_registro` DATETIME NULL DEFAULT NULL ,
  `t10fecha_actualiza` DATETIME NULL DEFAULT NULL ,
  `t10tiempo` VARCHAR(10) NULL DEFAULT NULL ,
  PRIMARY KEY (`t10id_respuesta_examen`) ,
  INDEX `t10_idx1_t12id_examen` (`t12id_examen` ASC) ,
  INDEX `t10_idx2_t02id_usuario_perfil` (`t02id_usuario_perfil` ASC) ,
  INDEX `t10_idx3_t11id_pregunta` (`t11id_pregunta` ASC) ,
  INDEX `t10_idx4_t13id_respuesta` (`t13id_respuesta` ASC) ,
  CONSTRAINT `t10_fk1_t12id_examen`
    FOREIGN KEY (`t12id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t10_fk2_t02id_usuario_perfil`
    FOREIGN KEY (`t02id_usuario_perfil` )
    REFERENCES `evaluaciones`.`t02usuario_perfil` (`t02id_usuario_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t10_fk3_t11id_pregunta`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t10_fk4_t13id_respuesta`
    FOREIGN KEY (`t13id_respuesta` )
    REFERENCES `evaluaciones`.`t13respuesta` (`t13id_respuesta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t14multimedia`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t14multimedia` (
  `t14id_multimedia` INT(11) NOT NULL ,
  `t14tipo` VARCHAR(20) NOT NULL ,
  `t14nombre` VARCHAR(100) NOT NULL ,
  `t14url` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`t14id_multimedia`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t15seccion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t15seccion` (
  `t15id_seccion` INT(11) NOT NULL AUTO_INCREMENT ,
  `t15seccion` TINYTEXT NOT NULL ,
  `t15valor_porcentaje` DECIMAL(10,0) NOT NULL ,
  `t15resp_total_parcial` ENUM('Total','Parcial') NULL DEFAULT 'Total' ,
  `t15estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`t15id_seccion`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t16seccion_pregunta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t16seccion_pregunta` (
  `t15id_seccion` INT(11) NOT NULL ,
  `t11id_pregunta` INT(11) NOT NULL ,
  `t16orden_pregunta` INT(3) NOT NULL DEFAULT 0 ,
  PRIMARY KEY (`t15id_seccion`, `t11id_pregunta`) ,
  INDEX `t16_idx1_t11id_pregunta` (`t11id_pregunta` ASC) ,
  INDEX `t16_idx2_t15id_seccion` (`t15id_seccion` ASC) ,
  CONSTRAINT `t16_fk1_t15id_seccion`
    FOREIGN KEY (`t15id_seccion` )
    REFERENCES `evaluaciones`.`t15seccion` (`t15id_seccion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t16_fk2_t11id_pregunta`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t17pregunta_respuesta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t17pregunta_respuesta` (
  `t11id_pregunta` INT(11) NOT NULL ,
  `t13id_respuesta` INT(11) NOT NULL ,
  `t17correcta` TINYINT(1) NOT NULL DEFAULT '0' ,
  `t17fecha_registro` DATETIME NOT NULL ,
  `t17fecha_actualiza` DATETIME NOT NULL ,
  PRIMARY KEY (`t11id_pregunta`, `t13id_respuesta`) ,
  INDEX `t17_idx1_t13id_respuesta` (`t13id_respuesta` ASC) ,
  INDEX `t17_idx2_t11id_pregunta` (`t11id_pregunta` ASC) ,
  CONSTRAINT `t17_fk1_t11id_pregunta`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t17_fk2_t13id_respuesta`
    FOREIGN KEY (`t13id_respuesta` )
    REFERENCES `evaluaciones`.`t13respuesta` (`t13id_respuesta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t18clasificacion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t18clasificacion` (
  `t18id_clasificacion` INT(11) NOT NULL AUTO_INCREMENT ,
  `t18clasificacion` VARCHAR(100) NOT NULL ,
  `t18estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`t18id_clasificacion`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t19pregunta_clasificacion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t19pregunta_clasificacion` (
  `t11id_pregunta` INT(11) NOT NULL ,
  `t18id_clasificacion` INT(11) NOT NULL ,
  PRIMARY KEY (`t11id_pregunta`, `t18id_clasificacion`) ,
  INDEX `t19_idx1_t18id_clasificacion` (`t18id_clasificacion` ASC) ,
  INDEX `t19_idx2_t11id_pregunta` (`t11id_pregunta` ASC) ,
  CONSTRAINT `t19_fk1_t11id_pregunta`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t19_fk2_t18id_clasificacion`
    FOREIGN KEY (`t18id_clasificacion` )
    REFERENCES `evaluaciones`.`t18clasificacion` (`t18id_clasificacion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t20examen_seccion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t20examen_seccion` (
  `t12id_examen` INT(11) NOT NULL ,
  `t15id_seccion` INT(11) NOT NULL ,
  PRIMARY KEY (`t12id_examen`, `t15id_seccion`) ,
  INDEX `t20_idx1_t12id_seccion` (`t15id_seccion` ASC) ,
  INDEX `t20_idx2_t12id_examen` (`t12id_examen` ASC) ,
  CONSTRAINT `t20_fk1_t12id_examen`
    FOREIGN KEY (`t12id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t20_fk2_t15id_seccion`
    FOREIGN KEY (`t15id_seccion` )
    REFERENCES `evaluaciones`.`t15seccion` (`t15id_seccion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t22examen_paquete`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t22examen_paquete` (
  `t12id_examen` INT(11) NOT NULL ,
  `t21id_paquete` INT(11) NOT NULL ,
  PRIMARY KEY (`t12id_examen`, `t21id_paquete`) ,
  INDEX `t22_idx1_t21id_paquete` (`t21id_paquete` ASC) ,
  INDEX `t22_idx2_t12id_examen` (`t12id_examen` ASC) ,
  CONSTRAINT `t22_fk1_t12id_examen`
    FOREIGN KEY (`t12id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `t22_fk2_t21id_paquete`
    FOREIGN KEY (`t21id_paquete` )
    REFERENCES `evaluaciones`.`t21paquete` (`t21id_paquete` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


INSERT INTO `c03tipo_pregunta` VALUES (1,'Opción Múltiple','OPMULT','ui-icon-disk','DE ENTRE LAS OPCIONES SELECCIONA LA RESPUESTA CORRECTA',3,'A',0,1,1,1,1),(2,'Respuesta Multiple','RESMUL','ui-icon-arrowreturnthick-1-w','DE ENTRE LAS OPCIONES SELECCIONA LAS RESPUESTAS CORRECTAS',3,'A',0,1,1,1,1),(3,'Verdadero Falso','VERFAL','ui-icon-plusthickui-icon-arrowreturnthick-1-w','CONTESTA SI LA PREGUNTA ES FALSA O VERDADERA',2,'A',0,1,1,1,1),(4,'Relaciona Combo','RELCOM','ui-icon-newwin','RELACIONA LAS PALABRAS DE DERECHA CON LAS FRASES DE LA IZQUIERDA',3,'A',0,2,1,1,1),(5,'Arrastra Contenedor','RELCON','','RELACIONA LAS PALABRAS DE DERECHA CON LAS FRASES DE LA IZQUIERDA',3,'A',0,2,1,1,1),(6,'Pregunta Abierta','PREGAB','','CONTESTA CORRECTAMENTE LA PREGUNTA',1,'A',0,1,1,0,0),(7,'Pregunta Corta','PREGCO','','COMPLETA EN LOS ESPACIOS LA RESPUESTA CORRECTA',3,'A',0,1,1,1,1),(8,'Arrastra Corta','ARRACO','','COMPLETA EN LOS ESPACIOS LA RESPUESTA CORRECTA',3,'A',0,1,1,1,1),(9,'Arrastra Ordenar','ARRAOR','','ORDENA CORRECTAMENTE LAS SIGUIENTES PALABRAS',3,'A',0,1,1,1,1),(10,'Número Ordenar','NUMORD','','ORDENA CORRECTAMENTE LAS SIGUIENTES PALABRAS',3,'A',0,1,1,1,1),(11,'Número Relacionar','RELNUM','','RELACIONA LAS SIGUIENTES COLUMNAS',3,'A',0,2,1,1,1),(12,'Relacionar Linea','RELLIN','','RELACIONA LAS SIGUIENTES COLUMNAS',3,'A',0,2,1,1,1),(13,'Matriz','MATRIZ','','Elige la opción que consideres se ajusta a lo que pienses. (Nuevo)',1,'A',0,5,1,1,1),(14,'Rompecabezas','ROMPEC','','Arma el rompecabezas ',NULL,'A',1,6,6,0,1),(15,'Crucigrama','CRUCIG','ui-icon-newwin','Resuelve el siguiente crucigrama. (Nuevo)',3,'A',1,6,6,1,1),(16,'Sopa de Letras','SOPLET','ui-icon-plusthickui-icon-arrowreturnthick-1-w','Encuentra las palabras en la siguiente sopa de letras. (Nuevo)',3,'A',1,3,3,1,1);
INSERT INTO `c02tipo_examen` VALUES (1,'Cuantitativo','ui-icon-disk','A'),(2,'Cualitativo','ui-icon-arrowreturnthick-1-w','A'),(3,'Encuesta','ui-icon-plusthickui-icon-arrowreturnthick-1-w','A');
INSERT INTO `c04escala` VALUES (1,'0 - 10','A'),(2,'0 - 100','A'),(3,'0 - 1000','A');
INSERT INTO `c06numeracion` VALUES (0,'Sin numeración','A'),(1,'a), b), c)...','A'),(2,'A), B), C)...','A'),(3,'1), 2), 3)...','A'),(4,'I, II, III, IV','A');
INSERT INTO `c05nivel` VALUES (1,0,'Básico','A'),(2,0,'Medio','A'),(3,0,'Avanzado','A');
INSERT INTO `c01perfil` VALUES (1,'ALUMNO','ALUMNO DE LA PLATAFORMA','A'),(2,'PROFESOR','PROFESOR DE LA PLATAFORMA','A');


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
