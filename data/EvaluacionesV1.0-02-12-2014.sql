SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `evaluaciones` DEFAULT CHARACTER SET utf8 ;
USE `evaluaciones` ;

-- -----------------------------------------------------
-- Table `evaluaciones`.`c03tipo_pregunta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c03tipo_pregunta` (
  `c03id_tipo_pregunta` INT NOT NULL ,
  `c03tipo_pregunta` VARCHAR(50) NOT NULL ,
  `c03instruccion` TEXT NULL ,
  `c03minimo_respuestas` INT(2) NULL ,
  `c03estatus` CHAR(1) NOT NULL ,
  `c03tiempo_limite` TINYINT(1) NULL COMMENT 'Bandera que indica si el tipo de pregunta requiere tiempo limite' ,
  `c03columnas` TINYINT(1) NULL COMMENT 'Bandera que indica si el tipo de pregunta requiere columnas' ,
  `c03renglones` TINYINT(1) NULL COMMENT 'Bander que indica si el tipo de pregunta requiere renglones' ,
  `c03numerar` TINYINT(1) NULL COMMENT 'Bandera que indica si el tipo de pregunta requiere numerar sus respuestas' ,
  `c03aleatorias` TINYINT(1) NULL COMMENT 'Banera que indica si el tipo de pregunta requiere respuestas aleatorias' ,
  PRIMARY KEY (`c03id_tipo_pregunta`) ,
  UNIQUE INDEX `c03tipo_pregunta_UNIQUE` (`c03tipo_pregunta` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t11pregunta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t11pregunta` (
  `t11id_pregunta` INT NOT NULL ,
  `t11id_pregunta_parent` INT NULL ,
  `c03id_tipo_pregunta` INT NOT NULL ,
  `t11pregunta` TEXT NULL ,
  `t11nombre_corto_clave` VARCHAR(100) NULL ,
  `t11instruccion` TEXT NULL ,
  `t11tiempo_limite` VARCHAR(5) NOT NULL ,
  `t11columnas` VARCHAR(45) NULL ,
  `t11numerar` TINYINT(1) NULL ,
  `t11fecha_registro` DATE NULL ,
  `t11fecha_actualiza` DATE NULL ,
  `t11estatus` CHAR(1) NULL ,
  PRIMARY KEY (`t11id_pregunta`, `c03id_tipo_pregunta`) ,
  INDEX `fk_t11preguntas_c03tipos_pregunta1_idx` (`c03id_tipo_pregunta` ASC) ,
  CONSTRAINT `fk_ge_t11preguntas_ge_c03tipos_pregunta1`
    FOREIGN KEY (`c03id_tipo_pregunta` )
    REFERENCES `evaluaciones`.`c03tipo_pregunta` (`c03id_tipo_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c02tipo_examen`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c02tipo_examen` (
  `c02id_tipo_examen` INT NOT NULL ,
  `c02tipo_examen` VARCHAR(100) NOT NULL ,
  `c02estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`c02id_tipo_examen`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c04escala`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c04escala` (
  `c04id_escala` INT NOT NULL AUTO_INCREMENT ,
  `c04escala` VARCHAR(45) NOT NULL ,
  `c04estatus` CHAR(1) NULL ,
  PRIMARY KEY (`c04id_escala`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c05nivel`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c05nivel` (
  `c05id_nivel` INT NOT NULL AUTO_INCREMENT ,
  `c05id_nivel_parent` INT(2) NULL ,
  `c05nivel` VARCHAR(45) NOT NULL ,
  `c05estatus` CHAR(1) NULL ,
  PRIMARY KEY (`c05id_nivel`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t12examen`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t12examen` (
  `t12id_examen` INT NOT NULL ,
  `c02id_tipo_examen` INT NOT NULL ,
  `c04id_escala` INT NOT NULL ,
  `c05id_nivel` INT NOT NULL ,
  `t12nombre` TINYTEXT NOT NULL ,
  `t12nombre_corto_clave` VARCHAR(100) NULL ,
  `t12descripcion` TEXT NULL ,
  `t12fecha_registro` DATE NULL ,
  `t12fecha_actualiza` DATE NULL ,
  `t12num_intentos` INT NULL ,
  `t12aleatorio_preguntas` TINYINT(1) NULL ,
  `t12aleatorio_respuestas` TINYINT(1) NULL ,
  `t12demo` TINYINT(1) NULL ,
  `t12minimo_aprobatorio` INT(4) NOT NULL ,
  `t12tiempo` INT(2) NOT NULL ,
  `t12estatus` CHAR(1) NOT NULL COMMENT 'I:INACTIVO,A:ACTIVO,\n' ,
  PRIMARY KEY (`t12id_examen`) ,
  INDEX `fk_t12examenes_c02tipos_examenes1_idx` (`c02id_tipo_examen` ASC) ,
  INDEX `fk_t12examen_1_idx` (`c04id_escala` ASC) ,
  INDEX `fk_t12examen_2_idx` (`c05id_nivel` ASC) ,
  CONSTRAINT `fk_ge_t12examenes_ge_c02tipos_examenes1`
    FOREIGN KEY (`c02id_tipo_examen` )
    REFERENCES `evaluaciones`.`c02tipo_examen` (`c02id_tipo_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t12examen_1`
    FOREIGN KEY (`c04id_escala` )
    REFERENCES `evaluaciones`.`c04escala` (`c04id_escala` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t12examen_2`
    FOREIGN KEY (`c05id_nivel` )
    REFERENCES `evaluaciones`.`c05nivel` (`c05id_nivel` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t15seccion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t15seccion` (
  `t15id_seccion` INT NOT NULL ,
  `t12id_examen` INT NOT NULL ,
  `t15seccion` TINYTEXT NOT NULL ,
  `t15valor_porcentaje` DECIMAL NOT NULL ,
  `t15resp_total_parcial` ENUM('Total', 'Parcial') NULL DEFAULT 'Total' ,
  `t15estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`t15id_seccion`, `t12id_examen`) ,
  INDEX `fk_t15secciones_t12examenes1_idx` (`t12id_examen` ASC) ,
  CONSTRAINT `fk_ge_t15secciones_ge_t12examenes1`
    FOREIGN KEY (`t12id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t14multimedia`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t14multimedia` (
  `t14id_multimedia` INT NOT NULL ,
  `t14tipo` VARCHAR(20) NOT NULL ,
  `t14nombre` VARCHAR(100) NOT NULL ,
  `t14url` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`t14id_multimedia`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t13respuesta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t13respuesta` (
  `t13id_respuesta` INT NOT NULL ,
  `t13id_respuesta_parent` INT NULL ,
  `t13respuesta` TINYTEXT NOT NULL ,
  `t03orden` INT(3) NOT NULL ,
  `t13fecha_registro` DATE NULL ,
  `t13fecha_actualiza` DATE NULL ,
  `t13estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`t13id_respuesta`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t17pregunta_respuesta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t17pregunta_respuesta` (
  `t11id_pregunta` INT NOT NULL ,
  `t13id_respuesta` INT NOT NULL ,
  `t17correcta` TINYINT(1) NOT NULL DEFAULT FALSE ,
  `t17fecha_registro` DATE NOT NULL ,
  `t17fecha_actuliza` DATE NOT NULL ,
  PRIMARY KEY (`t11id_pregunta`, `t13id_respuesta`) ,
  INDEX `fk_t17preguntas_respuestas_t13respuestas1_idx` (`t13id_respuesta` ASC) ,
  INDEX `fk_t17preguntas_respuestas_t11preguntas1_idx` (`t11id_pregunta` ASC) ,
  CONSTRAINT `fk_t17preguntas_respuestas_t11preguntas1`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t17preguntas_respuestas_t13respuestas1`
    FOREIGN KEY (`t13id_respuesta` )
    REFERENCES `evaluaciones`.`t13respuesta` (`t13id_respuesta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t18clasificacion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t18clasificacion` (
  `t18id_clasificacion` INT NOT NULL ,
  `t18clasificacion` VARCHAR(100) NOT NULL ,
  `t18estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`t18id_clasificacion`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t19pregunta_clasificacion`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t19pregunta_clasificacion` (
  `t11id_pregunta` INT NOT NULL ,
  `t18id_clasificacion` INT NOT NULL ,
  PRIMARY KEY (`t11id_pregunta`, `t18id_clasificacion`) ,
  INDEX `fk_t19preguntas_clasificaciones_t18clasificacion_idx` (`t18id_clasificacion` ASC) ,
  INDEX `fk_t19preguntas_clasificaciones_t11pregunta_idx` (`t11id_pregunta` ASC) ,
  CONSTRAINT `fk_t19preguntas_clasificaciones_t11preguntas1`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t19preguntas_clasificaciones_t18clasificac1`
    FOREIGN KEY (`t18id_clasificacion` )
    REFERENCES `evaluaciones`.`t18clasificacion` (`t18id_clasificacion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t16seccion_pregunta`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t16seccion_pregunta` (
  `t15id_seccion` INT NOT NULL ,
  `t11id_pregunta` INT NOT NULL ,
  PRIMARY KEY (`t15id_seccion`, `t11id_pregunta`) ,
  INDEX `fk_t16secciones_preguntas_t11preguntas1_idx` (`t11id_pregunta` ASC) ,
  INDEX `fk_t16secciones_preguntas_t15secciones1_idx` (`t15id_seccion` ASC) ,
  CONSTRAINT `fk_t16secciones_preguntas_t15secciones1`
    FOREIGN KEY (`t15id_seccion` )
    REFERENCES `evaluaciones`.`t15seccion` (`t15id_seccion` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t16secciones_preguntas_t11preguntas1`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`c01perfil`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`c01perfil` (
  `c01id_perfil` INT NOT NULL AUTO_INCREMENT ,
  `c01nombre` VARCHAR(100) NOT NULL ,
  `c01descripcion` TEXT NULL ,
  `c01estatus` CHAR(1) NULL ,
  PRIMARY KEY (`c01id_perfil`) ,
  UNIQUE INDEX `c01nombre_UNIQUE` (`c01nombre` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t05grupo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t05grupo` (
  `t05id_grupo` INT(11) NOT NULL AUTO_INCREMENT ,
  `t05descripcion` VARCHAR(50) NULL DEFAULT NULL ,
  `t05ciclo` VARCHAR(25) NOT NULL DEFAULT '' ,
  `t05estatus` CHAR(1) NOT NULL DEFAULT 'A' ,
  PRIMARY KEY (`t05id_grupo`) )
ENGINE = InnoDB
AUTO_INCREMENT = 1130
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t09agenda`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t09agenda` (
  `t09id_agenda` INT NOT NULL AUTO_INCREMENT ,
  `t02id_examen` INT NOT NULL ,
  `t05id_grupo` INT(11) NOT NULL ,
  `t09fecha_inicio` DATETIME NULL ,
  `t09fecha_final` DATETIME NULL ,
  `t09estatus` ENUM('CANCELADO','HABILITADO') NULL DEFAULT 'HABILITADO' ,
  PRIMARY KEY (`t09id_agenda`) ,
  INDEX `fk_t09agenda_t12examen_idx` (`t02id_examen` ASC) ,
  INDEX `fk_t09agenda_t05grupo1_idx` (`t05id_grupo` ASC) ,
  CONSTRAINT `fk_t09agenda_t12examen`
    FOREIGN KEY (`t02id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t09agenda_t05grupo1`
    FOREIGN KEY (`t05id_grupo` )
    REFERENCES `evaluaciones`.`t05grupo` (`t05id_grupo` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t01usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t01usuario` (
  `t01id_usuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `t01nombre` VARCHAR(45) NOT NULL DEFAULT '' ,
  `t01apellidos` VARCHAR(100) NOT NULL DEFAULT '' ,
  `t01correo` VARCHAR(255) NOT NULL DEFAULT '' ,
  `t01contrasena` VARCHAR(255) NOT NULL DEFAULT '' ,
  `t01bloqueda` TINYINT(4) NOT NULL ,
  `t01tiempo_bloqueo` DATETIME NULL DEFAULT NULL ,
  `t01fecha_alta` DATETIME NOT NULL ,
  `t01fecha_actualizacion` DATETIME NULL DEFAULT NULL ,
  `t01demo` TINYINT(4) NOT NULL ,
  `t01acepto_condiciones` TINYINT(4) NOT NULL ,
  `t01provider_id` VARCHAR(50) NULL DEFAULT NULL ,
  `t01provider` VARCHAR(100) NULL DEFAULT NULL ,
  `t01estatus` CHAR(1) NOT NULL DEFAULT '' ,
  PRIMARY KEY (`t01id_usuario`) ,
  UNIQUE INDEX `idx1_t01correo` (`t01correo` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 13071
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t02usuario_perfil`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t02usuario_perfil` (
  `t02id_usuario_perfil` INT(11) NOT NULL AUTO_INCREMENT ,
  `c01id_perfil` INT(11) NOT NULL ,
  `t01id_usuario` INT(11) NOT NULL ,
  PRIMARY KEY (`t02id_usuario_perfil`) ,
  INDEX `fk_t01id_usuario_1_idx` (`t01id_usuario` ASC) ,
  INDEX `fk_t02id_perfil_1_idx` (`c01id_perfil` ASC) ,
  CONSTRAINT `fk_t02id_usuario_1`
    FOREIGN KEY (`t01id_usuario` )
    REFERENCES `evaluaciones`.`t01usuario` (`t01id_usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t02id_perfil_1`
    FOREIGN KEY (`c01id_perfil` )
    REFERENCES `evaluaciones`.`c01perfil` (`c01id_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 11976
DEFAULT CHARACTER SET = utf8
COMMENT = 'licencia_profesor\r\nA=Activa\r\nI=Inactiva\r\nV=Vencida';


-- -----------------------------------------------------
-- Table `evaluaciones`.`t10respuesta_usuario_examen`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t10respuesta_usuario_examen` (
  `t10id_respuesta_examen` INT NOT NULL AUTO_INCREMENT ,
  `t02id_usuario_perfil` INT(11) NOT NULL ,
  `t11id_pregunta` INT NOT NULL ,
  `t12id_examen` INT(11) NOT NULL ,
  `t13id_respuesta` INT NOT NULL ,
  `t10llave` VARCHAR(10) NOT NULL ,
  `t10num_intento` INT(2) NOT NULL DEFAULT 1 ,
  `t10respuesta_usuario` TINYTEXT NULL ,
  `t10respuesta_correcta` TINYTEXT NULL ,
  `t10fecha_registro` DATETIME NULL ,
  `t10fecha_actualiza` DATETIME NULL ,
  `t10tiempo` TIME NULL ,
  PRIMARY KEY (`t10id_respuesta_examen`) ,
  INDEX `fk_t10respuesta_usuario_examen_t12examen1_idx` (`t12id_examen` ASC) ,
  INDEX `fk_t10respuesta_usuario_examen_t02usuario_perfil1_idx` (`t02id_usuario_perfil` ASC) ,
  INDEX `fk_t10respuesta_usuario_examen_t11pregunta1_idx` (`t11id_pregunta` ASC) ,
  INDEX `fk_t10respuesta_usuario_examen_t13respuesta1_idx` (`t13id_respuesta` ASC) ,
  CONSTRAINT `fk_t10respuesta_usuario_examen_t12examen1`
    FOREIGN KEY (`t12id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t10respuesta_usuario_examen_t02usuario_perfil1`
    FOREIGN KEY (`t02id_usuario_perfil` )
    REFERENCES `evaluaciones`.`t02usuario_perfil` (`t02id_usuario_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t10respuesta_usuario_examen_t11pregunta1`
    FOREIGN KEY (`t11id_pregunta` )
    REFERENCES `evaluaciones`.`t11pregunta` (`t11id_pregunta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t10respuesta_usuario_examen_t13respuesta1`
    FOREIGN KEY (`t13id_respuesta` )
    REFERENCES `evaluaciones`.`t13respuesta` (`t13id_respuesta` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `evaluaciones`.`t03licencia_usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t03licencia_usuario` (
  `t03id_licencia_usuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `t02id_usuario_perfil` INT(11) NOT NULL ,
  `t03llave` VARCHAR(10) NOT NULL ,
  `t03estatus` ENUM('DISPONIBLE','VENCIDA','INACTIVA') NULL DEFAULT 'DISPONIBLE' ,
  PRIMARY KEY (`t03id_licencia_usuario`) ,
  INDEX `fk_t03id_usuario_perfil_1_idx` (`t02id_usuario_perfil` ASC) ,
  CONSTRAINT `fk_t03id_usuario_perfil_1`
    FOREIGN KEY (`t02id_usuario_perfil` )
    REFERENCES `evaluaciones`.`t02usuario_perfil` (`t02id_usuario_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 246
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t04examen_usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t04examen_usuario` (
  `t04id_examen_usuario` INT(11) NOT NULL AUTO_INCREMENT ,
  `t12id_examen` INT(11) NOT NULL ,
  `t03id_licencia_usuario` INT(11) NOT NULL ,
  `t04calificacion` VARCHAR(4) NOT NULL ,
  `t04fecha_inicio` DATETIME NOT NULL COMMENT 'Fecha de inicio\nsolo para historico\nde examene' ,
  `t04fecha_fin` DATETIME NULL DEFAULT NULL COMMENT 'Fecha fin \nsolo para \nhistorico de \nexamen' ,
  `t04tiempo` VARCHAR(8) NULL DEFAULT NULL ,
  `t04examen_json` TEXT NULL DEFAULT NULL ,
  `t04num_intentos` INT(2) NULL DEFAULT 1 ,
  `t04estatus` ENUM('APROBADO','REPROBADO','NOPRESENTADO','PRESENTADO') NOT NULL DEFAULT 'NOPRESENTADO' COMMENT 'Estatus Examen\nA:Aprobado\nR:Reprobado\nN:NoPresentado\nP:Presentado\n\n ' ,
  PRIMARY KEY (`t04id_examen_usuario`) ,
  INDEX `fk1_t12id_examen_idx` (`t12id_examen` ASC) ,
  INDEX `fk_t03id_licencia_usuario_1_idx` (`t03id_licencia_usuario` ASC) ,
  CONSTRAINT `fk_t04id_licencia_usuario_1`
    FOREIGN KEY (`t03id_licencia_usuario` )
    REFERENCES `evaluaciones`.`t03licencia_usuario` (`t03id_licencia_usuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t04id_examen_1`
    FOREIGN KEY (`t12id_examen` )
    REFERENCES `evaluaciones`.`t12examen` (`t12id_examen` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 231
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t07codigo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t07codigo` (
  `t07id_codigo` INT(11) NOT NULL AUTO_INCREMENT ,
  `t05id_grupo` INT(11) NOT NULL ,
  `t07codigo` VARCHAR(25) NOT NULL ,
  `t07tipo` CHAR(20) NOT NULL ,
  `t07ultima_actualizacion` DATE NULL DEFAULT NULL ,
  `t07estatus` CHAR(1) NOT NULL ,
  PRIMARY KEY (`t07id_codigo`) ,
  UNIQUE INDEX `idx1_t07codigo` (`t07codigo` ASC) ,
  INDEX `fk_t07id_codigo_1_idx` (`t05id_grupo` ASC) ,
  CONSTRAINT `fk_t07id_codigo_1`
    FOREIGN KEY (`t05id_grupo` )
    REFERENCES `evaluaciones`.`t05grupo` (`t05id_grupo` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 12646
DEFAULT CHARACTER SET = latin1
COMMENT = 'InformaciÃ³n:\r\narchivos/catalogo_estatus_cÃ³digos.txt';


-- -----------------------------------------------------
-- Table `evaluaciones`.`t06menu_perfil`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t06menu_perfil` (
  `t06id_menu_perfil` INT(11) NOT NULL AUTO_INCREMENT ,
  `c01id_perfil` INT(11) NOT NULL ,
  `t06texto` VARCHAR(100) NULL DEFAULT NULL ,
  `t06descripcion` VARCHAR(100) NULL DEFAULT NULL ,
  `t06url` VARCHAR(100) NULL DEFAULT NULL ,
  `t06inner_link` TINYINT(1) NOT NULL DEFAULT '0' ,
  `t06estatus` CHAR(1) NULL ,
  PRIMARY KEY (`t06id_menu_perfil`) ,
  INDEX `fk_t06id_perfil_1_idx` (`c01id_perfil` ASC) ,
  CONSTRAINT `fk_t06id_perfil_1`
    FOREIGN KEY (`c01id_perfil` )
    REFERENCES `evaluaciones`.`c01perfil` (`c01id_perfil` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 87
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `evaluaciones`.`t08administrador_grupo`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `evaluaciones`.`t08administrador_grupo` (
  `t02id_usuario_perfil` INT(11) NOT NULL ,
  `t05id_grupo` INT(11) NOT NULL ,
  `t08usuario_ligado` CHAR(1) NULL DEFAULT NULL ,
  `t08fecha_registro` DATETIME NULL ,
  `t08fecha_actualiza` DATETIME NULL ,
  PRIMARY KEY (`t02id_usuario_perfil`, `t05id_grupo`) ,
  CONSTRAINT `fk_t08administrador_grupo_1`
    FOREIGN KEY (`t02id_usuario_perfil`)
    REFERENCES `evaluaciones`.`t02usuario_perfil` (`t02id_usuario_perfil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_t08administrador_grupo_2`
    FOREIGN KEY (`t05id_grupo`)
    REFERENCES `evaluaciones`.`t05grupo` (`t05id_grupo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

USE `evaluaciones` ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
