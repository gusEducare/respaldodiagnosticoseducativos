<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
$FULL_HTTP = 'evaluaciones.local.com/';
return array(
    'constantes' => array(
    		
		'MSGJUSEROCCUPED'=>'Ya esta Registrado Intente con otro dato por favor',

 		'MSJMINLENGTH3'=>'El campo requiere minimo 3 caracteres',

		'MSJREQUIERE'=>'Campo Obligatorio',

		'MSJMINLENGTH6'=>'El campo requiere minimo 6 caracteres',
                    
                'ID_PERFIL_INVITATO'=>5,
        
		'ID_PERFIL_ALUMNO'=>1,
        
		'ID_PERFIL_PROFESOR'=>2,
        
                'ID_PERFIL_CONSTRUCTOR'=>4,
        
                'RUTA_SITIO'=> 'http://' . $_SERVER['SERVER_NAME'],

                'ESTATUS_LIGADO' => 1,

                'ESTATUS_NO_LIGADO' => 2,

                'ESTATUS_ACTIVO' => 1,

                'ESTATUS_INACTIVO' => 2,

                'CODIGO_TIPO_GRUPO' => 1,

                'ID_SERVICIO_EVALUACIONES' => 30,

                'CODIGO_TIPO_GRUPO' => 1,
                
                'ERROR_PERMISO_DENEGADO' => 404,
        
                'CODIGO_LICENCIA_PROFESOR'=> 999,
        
                'ESTATUS_DISPONIBLE'=> 1,
                
                'ESTATUS_VENCIDA'=> 2,
                
                'ESTATUS_INACTIVA'=> 3,
        
                'USUARIO_ACTIVO' => 'A',
                
                'ESTATUS_EXAMEN_FINALIZADO' => 4,
        		
		//el id de categora intellectus nos sirve para identificar cuales examenes y  generar el reporte especifico de intellectus
                'ID_CATEGORIA_INTELLECTUS' => 12,
                
                'ID_CATEGORIA_INTELLECTUS_POST' => 13,
                
                'WSDL_PORTALGE' => 'http://portalpruebas.com/licencia?wsdl',
        
                //'WSDL_TID' =>'http://tid.pruebas.com/webservice/login?wsdl'
                'WSDL_TID' =>'http://tid.dev.com/webservice/login?wsdl',
        
                'URL_PLANTILLA_CSS' => 'http://evaluaciones.pruebas.com/css/suscripcion.css',
        
                'URL_PLANTILLA_JS' => 'http://evaluaciones.pruebas.com/js/jquery.jqPlantilla_2015.js',
        
                'URL_PLANTILLA_CSS_PROD' => 'http://portalge.grupoeducare.com/css/suscripcion.css',
        
                'URL_PLANTILLA_JS_PROD' => 'http://portalge.grupoeducare.com/js/jquery.jqPlantilla.js',
        
                
                'RUTA_RECURSOS_DISTRIBUIDORES'=>'http://hubbleged.educaredigital.com/recursos_distribuidores/phocadownload/INTELLECTUS/RECURSOS_PARA_PROFESOR/Evaluaciones_Diagnosticas_',
        
                'RUTA_RECURSOS_DISTRIBUIDORES_PRIMARIA'=>'_primaria.pdf',
        
                'RUTA_RECURSOS_DISTRIBUIDORES_SECUNDARIA'=>'_secundaria.pdf',
        
                'RUTA_RECURSOS_DISTRIBUIDORES_PREPA'=>'_preparatoria.pdf',
        
                'RUTA_RECURSOS_DISTRIBUIDORES_KINDER'=>'_preescolar.pdf',
        
                'RUTA_ORIGEN_IMAGENES'=>$_SERVER["DOCUMENT_ROOT"]."/img/agenda/",
        
                'RUTA_DESTINO_IMAGENES'=>$_SERVER["DOCUMENT_ROOT"]."/img/agendacopia/",
        
                'ID_INTELLECTUS_KINDER' => 19,
                'ID_INTELLECTUS_PRIMARIA_1' => 7,
                'ID_INTELLECTUS_PRIMARIA_2' => 8,
                'ID_INTELLECTUS_PRIMARIA_3' => 9,
                'ID_INTELLECTUS_PRIMARIA_4' => 10,
                'ID_INTELLECTUS_PRIMARIA_5' => 11,
                'ID_INTELLECTUS_PRIMARIA_6' => 12,
                'ID_INTELLECTUS_SECUNDARIA_1' => 13,
                'ID_INTELLECTUS_SECUNDARIA_2' => 14,
                'ID_INTELLECTUS_SECUNDARIA_3' => 15,
                'ID_INTELLECTUS_PREPARATORIA_1' => 16,
                'ID_INTELLECTUS_PREPARATORIA_2' => 17,
                'ID_INTELLECTUS_PREPARATORIA_3' => 18,


                'ID_INTELLECTUS_KINDER_POST' => 20,
                'ID_INTELLECTUS_PRIMARIA_1_POST' => 21,
                'ID_INTELLECTUS_PRIMARIA_2_POST' => 22,
                'ID_INTELLECTUS_PRIMARIA_3_POST' => 23,
                'ID_INTELLECTUS_PRIMARIA_4_POST' => 24,
                'ID_INTELLECTUS_PRIMARIA_5_POST' => 25,
                'ID_INTELLECTUS_PRIMARIA_6_POST' => 26,
                'ID_INTELLECTUS_SECUNDARIA_1_POST' => 27,
                'ID_INTELLECTUS_SECUNDARIA_2_POST' => 28,
                'ID_INTELLECTUS_SECUNDARIA_3_POST' => 29,
                'ID_INTELLECTUS_PREPARATORIA_1_POST' => 30,
                'ID_INTELLECTUS_PREPARATORIA_2_POST' => 31,
                'ID_INTELLECTUS_PREPARATORIA_3_POST' => 32,
        
                
        
                        
    ),
        "AMBIENTE" => "DESARROLLO"// esta constante nos dice en que ambiente estamos los valores son DESARROLLO|PRUEBAS|PRODUCCION

        );